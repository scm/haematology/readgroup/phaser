Phaser is distributed under three different licences

[Academic Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Academic_Licence)  
[CCP4 Licence](http://www.phaser.cimr.cam.ac.uk/index.php/CCP4_Licence)  
[Phenix Licence](http://www.phaser.cimr.cam.ac.uk/index.php/Phenix_Licence)  

_**By accessing the source code in this repository you agree to be bound by one of these licences.**_
