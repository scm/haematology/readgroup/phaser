#TEST
from phaser import *
i = InputMR_DAT()
HKLIN = "../tutorial/beta_blip_P3221.mtz"
i.setHKLI(HKLIN)
r = runMR_DAT(i)
print(r.logfile())
i = InputANO()
i.setDATA_REFL(r.getDATA_REFL())
i.setSPAC_HALL(r.getSpaceGroupHall())
i.setCELL6(r.getUnitCell())
r = runANO(i)
print(r.summary())
print("Anisotropy Correction")
print("Data read from: ", HKLIN)
print("Data output to : ", r.getMtzFile())
print("Spacegroup Name (Hall symbol) = %s (%s)" %
      (r.getSpaceGroupName(), r.getSpaceGroupHall()))
print("Unitcell = ", r.getUnitCell())
print("Principal components = ", r.getEigenBs())
print("Range of principal components = ", r.getAnisoDeltaB())
print("Wilson Scale and B-factor = ", r.getWilsonK(), r.getWilsonB())
hkl = r.getMiller()
f = r.getF()
sigf = r.getSIGF()
f_iso = r.getCorrectedF(True)
sigf_iso = r.getCorrectedSIGF(True)
corr = r.getCorrection(True)
nrefl = min(10,hkl.size())
print("First ", nrefl, " reflections")
print("%4s %4s %4s %10s %10s %10s %10s %10s" %
      ("H", "K", "L", "F", "SIGF", "Fano", "SIGFano", "Correction Factor"))
for i in range(0,nrefl):
  print("%4d %4d %4d %10.4f %10.4f %10.4f %10.4f %10.4f" %
        (hkl[i][0], hkl[i][1], hkl[i][2], f[i], sigf[i], f_iso[i], sigf_iso[i], corr[i]))
