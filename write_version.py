from __future__ import print_function

import libtbx.load_env
import time
import sys, os, os.path
import subprocess, re
import stat

def write_version_files(fname,phaser_env,phaser_path):
  d = os.path.dirname(fname)
  if (not os.path.exists(d)):
    os.mkdir(d)
  # if svn repo is present ...
  svnrevnumber = ""
  upgrres = ""
  try:
    # upgrade just in case sources are copied from an older svn client so the revision number can be read
    proc = subprocess.Popen(["svn", "upgrade", phaser_path, "--quiet"], stdout = subprocess.PIPE, stderr = subprocess.PIPE )
    upgrres = proc.communicate()[0]
    # read the revision number
    proc = subprocess.Popen(["svnversion", phaser_path], stdout=subprocess.PIPE, stderr=subprocess.PIPE )
    svnrevnumber = proc.communicate()[0]
    svnrevnumber = svnrevnumber.decode(encoding='utf-8', errors='strict')  # for python3
    svnrevnumber = svnrevnumber.strip()
    if svnrevnumber=="Unversioned directory":
      svnrevnumber=""
    svnrevnumber = "".join(svnrevnumber.split()) # remove any whitespace
  except OSError as e:
    svnrevnumber = ""
    upgrres = ""
  print(upgrres)
  revision = ""
  if svnrevnumber != "":
    try:
      if svnrevnumber[-1] == 'M':
        svnrevnumber=svnrevnumber[:-1]
      revision = str(int(svnrevnumber))
    except Exception as e:
      revision = ""
  if svnrevnumber:
    print("SVN revision:", svnrevnumber)

  # if git repo is present ...
  gitrevnumber = ""
  githash = ""
  gittotalcommits = ""
  gitshorthash = ""
  gitdatetime = ""
  gitbranch = ""
  gittagname = ""
  try:
    # get hash, short hash and author timestamp of current revision in this branch
    timeargs = ["git", "rev-list", "HEAD", "-n", "1", "--pretty=tformat:%h %ad", "--date=iso"]
    process = subprocess.Popen(timeargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (out, err) = process.communicate()
    out = out.decode(encoding='utf-8', errors='strict')  # for python3
    #print out, err
    if process.returncode == 0:
      s = out.split()
      githash = s[1]
      gitshorthash = s[2]
      gitdatetime = " ".join(s[3:6])
      # Construct the current revision number as the total count of revisions in this branch.
      # Assuming the same repo is used with always the same branch this number should be sequential.
      revargs = ["git", "rev-list", "--count", "HEAD"]
      process = subprocess.Popen(revargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict')  # for python3
      gitrevnumber = out.split()[0]
      # get total number of all commits in all branches
      revargs = ["git", "rev-list", "--count", "--all", "HEAD"]
      process = subprocess.Popen(revargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict')  # for python3
      gittotalcommits = out.split()[0]
      # get name of current branch
      branchargs = ["git", "rev-parse", "--abbrev-ref", "HEAD"]
      process = subprocess.Popen(branchargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
      ( out, err ) = process.communicate()
      out = out.decode(encoding='utf-8', errors='strict')  # for python3
      gitbranch = out.split()[0]
      try:
        # get svn version of current commit
        gitsvninfoargs = ["git", "svn", "find-rev", githash]
        process = subprocess.Popen(gitsvninfoargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        ( out, err ) = process.communicate()
        out = out.decode(encoding='utf-8', errors='strict')  # for python3
        gitsvninfo = out.split()[0]
        svnrev = str(int(gitsvninfo))
      except Exception as e: # if git svn hasn't been installed look in git log
        gitsvninfo = "None"
        gitsvninfoargs = ["git", "log", "-1"]
        process = subprocess.Popen(gitsvninfoargs, cwd=phaser_path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        ( out, err ) = process.communicate()
        out = out.decode(encoding='utf-8', errors='strict')  # for python3
        m = re.findall("@([0-9]+)\s", out, re.MULTILINE)
        if len(m):
          gitsvninfo = m[0]

      print("Git commit hash:", githash)
      print("Git commit date:", gitdatetime)
      print("Git svn info:", gitsvninfo)
      print("Current branch:", gitbranch)
      print("Number of commits of current branch:", gitrevnumber)
      print("Total number of commits of all branches:", gittotalcommits)
  except OSError as e:
    gitrevnumber = ""

  if gitrevnumber != "":
    # form a combined version info like "svnrev_7482_git_6534" if svn version can be found
    gittagname = 'svnrev_' + gitsvninfo + '_gitrev_' + str(gitrevnumber)
    try:
      svnrev = gitsvninfo
      gitrev = gitrevnumber
      if len(svnrev):
        revision = str(int(gitsvninfo))
      else:
        if len(gitrev):
          revision = str(int(gitrevnumber))
      svnrevnumber = revision
    except Exception as e:
      try: # otherwise use git commit count
        revision = str(int(gitrevnumber))
      except Exception as e:
        revision = ""

  changelog = open(str(phaser_path + "/codebase/phaser/main/CHANGELOG"), "r")
  changelogstr = changelog.read()
  changelogstr = changelogstr.replace('\n', '\\n').replace('\r', '\\n')
  changelog.close()

  cppstr = """
#include <phaser/main/Version.h>
#include <phaser/io/Output.h>

namespace phaser {

    std::string Output::version_date()
    {
        return \"%s\";
    }
    std::string Output::version_number()
    {
        return PROGRAM_RELEASE;
    }
    std::string Output::svn_raw()
    {
        return \"%s\";
    }
    std::string Output::git_rev()
    {
        return \"%s\";
    }
    std::string Output::git_totalcommits()
    {
        return \"%s\";
    }
    std::string Output::git_tagname()
    {
        return \"%s\";
    }
    std::string Output::git_hash()
    {
        return \"%s\";
    }
    std::string Output::git_shorthash()
    {
        return \"%s\";
    }
    std::string Output::git_commitdatetime()
    {
        return \"%s\";
    }
    std::string Output::git_branchname()
    {
        return \"%s\";
    }
    std::string Output::change_log()
    {
        return \"\\n%s\";
    }

} //phaser
  """ %(time.asctime(time.localtime()), svnrevnumber, gitrevnumber, gittotalcommits,
        gittagname, githash, gitshorthash, gitdatetime, gitbranch, changelogstr)

  versionCCfile = open(fname, "w")
  versionCCfile.write(cppstr)
  versionCCfile.flush() # attempting to circumvent strange race conditions that may cause access denied error
  os.fsync(versionCCfile.fileno())
  versionCCfile.close()
  del versionCCfile

# Now generate header file for the Windows resource file, phaser.res, that holds version
# numbers which will be visible in Windows explorer
  versionHfile = open(str(phaser_path + "/codebase/phaser/main/Version.h"), "r")
  versionstr = versionHfile.read()
  versionHfile.close()
  progname = re.search(r'\#define\s+PROGRAM_NAME\s+\"(.*)\"', versionstr, re.MULTILINE).group(1)
  # find first 3 version numbers in the source file
  progver = re.search(r'\#define\s+PROGRAM_RELEASE\s+\"(.*)\"', versionstr, re.MULTILINE).group(1)
  print(progname,"-",progver)
  # replace dots with commas
  p = re.compile('\.')
  smatch = p.sub(',', progver)
  if revision == "":
    revision = "0"
  fullversion = smatch + "," + revision

  hstr = """
// This always generated file from phaser/SConscript should never be edited or committed.
// Version number convention on Windows is <major version>.<minor version>.<build number>.<revision>
// First 3 numbers are extracted from version.h and revision number from the git or svn database.
// These numbers are used in phaser.rc2 which in turn is included by phaser.rc when building on Windows.
#define FILEVER          %s
#define PRODUCTVER       %s
#define STRFILEVER       \"%s\\0\"
#define STRPRODUCTVER    \"%s\\0\"
""" %(fullversion, fullversion, fullversion, fullversion)

  versresHfile = open(str(libtbx.env.under_build("phaser/versionno.h")), "w")
  versresHfile.write(hstr)
  versresHfile.flush()
  os.fsync(versresHfile.fileno())
  versresHfile.close()
  del versresHfile
