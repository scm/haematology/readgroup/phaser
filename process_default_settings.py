from __future__ import print_function

import libtbx.load_env
import time
import sys, os, os.path
import subprocess, re
import stat

def process_default_settings (phaser_path,mmdb_probe_file_name) :
  defaults_file = os.path.join(phaser_path, "defaults")
  assert os.path.isfile(defaults_file)
  print("phaser: Generating C++ and libtbx.phil defaults files")
  mmdb_include_file = libtbx.env.under_build("include/mmdb.h")
  defaults_include_file = libtbx.env.under_build("include/phaser_defaults.h")
  #print  defaults_include_file
  #defaults_include_file = os.path.join(phaser_path, "include/defaults.h")
  defaults_phil_file = libtbx.env.under_build("include/phaser_defaults.params")
  #print  defaults_phil_file
  nma_defaults_phil_file = libtbx.env.under_build("include/phaser_nma_defaults.phil")
  #defaults_phil_file = os.path.join(phaser_path,
  #  "phaser/phenix_interface/defaults.params")
  f0 = open(mmdb_include_file, "w")
  f1 = open(defaults_include_file, "w")
  f2 = open(defaults_phil_file, "w")
  f3 = open(nma_defaults_phil_file, "w")
  f0.write("// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
  f0.write("// DO NOT EDIT!\n")
  f1.write("// THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
  f1.write("// DO NOT EDIT!\n")
  f1.write("#ifndef PHASER_DEFAULTS\n")
  f1.write("#define PHASER_DEFAULTS 1\n")
  f2.write("# THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
  f2.write("# DO NOT EDIT!\n")
  f3.write("# THIS FILE IS GENERATED AUTOMATICALLY BY SCONS\n")
  f3.write("# DO NOT EDIT!\n")
  if (os.path.isfile(mmdb_probe_file_name)):
    f0.write("\n")
    f0.write("#define PHASER_CONFIGURE_WITH_MMDB2\n")
    f0.write("\n")
  for line in open(defaults_file).readlines() :
    if line.startswith("#") :
      continue
    fields = line.split()
    if len(fields) < 3 :
      raise RuntimeError("""\
Improper formatting of default settings:
%s
""" % line)
    def_name = fields[0]
    phil_name = fields[1]
    value = " ".join(fields[2:])
    f1.write("#define %s %s\n" % (def_name, value))
    if (phil_name != ".") :
      value = re.sub("false", "False", re.sub("true", "True", value))
      if (phil_name.startswith("nma") or phil_name.startswith("sced") or
          phil_name.startswith("ddm") or phil_name.startswith("perturb") or
          phil_name.startswith("enm")) :
        f3.write("keywords.%s = %s\n" % (phil_name, value))
      else :
        f2.write("phaser.keywords.%s = %s\n" % (phil_name, value))
  f1.write("#endif")
  f0.close()
  f1.close()
  f2.close()
  f3.close()
