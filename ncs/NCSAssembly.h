#ifndef PHASER_NCS_NCSASSEMBLY_H
#define PHASER_NCS_NCSASSEMBLY_H

#include <phaser/ncs/NCSSymmetry.h>
#include <iotbx/pdb/hierarchy.h>

namespace phaser {  namespace ncs {

class NCSAssembly : public NCSSymmetry
{
    private:
        std::vector< iotbx::pdb::hierarchy::chain > chains_;

    public:
        NCSAssembly(
            const iotbx::pdb::hierarchy::chain& reference,
            double angular,
            double spatial
            );
        ~NCSAssembly();

        // Accessor
        const std::vector< iotbx::pdb::hierarchy::chain >& get_chains() const;

        // Display method
        std::string name() const;

        // Other methods
        void insert(
            const NCSOperator& op,
            const iotbx::pdb::hierarchy::chain& ch
            );
};

}  } // namespace phaser::ncs

#endif /*PHASER_NCS_NCSASSEMBLY_H*/
