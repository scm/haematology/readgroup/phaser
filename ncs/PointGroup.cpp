#include <phaser/ncs/PointGroup.h>
#include <cassert>

namespace phaser {  namespace ncs {

// Utility functions for point group name determination
bool
compare_rotation(const NCSOperator& l, const NCSOperator& r)
{
    double l_rot = std::abs( l.rotation_angle_radian() );
    double r_rot = std::abs( r.rotation_angle_radian() );

    return l_rot < r_rot;
}


bool
higher_order_operator(int order)
{
    return 2 < order;
}


bool
compare_pair_second(
    const std::pair< int, int >& l,
    const std::pair< int, int >& r
    )
{
    return l.second < r.second;
}

//
// class PointGroup
//

// Constructor
PointGroup::PointGroup(
        const std::vector< NCSOperator >& operators,
        double angular,
        double spatial
        )
    : NCSSymmetry( operators, angular, spatial )
{
}

// Destructor
PointGroup::~PointGroup()
{
}

// Factory methods
PointGroup
PointGroup::from_operators(
    const std::vector< NCSOperator >& operators,
    double angular,
    double spatial
    )
{
    // Check whether it is a group
    if ( obeys_group_axioms( operators, angular, spatial ) )
    {
        return PointGroup( operators, angular, spatial );
    }

    // Factorize the operators
    NCSSymmetry symm( operators, angular, spatial );

    std::vector< std::pair< NCSSymmetry, NCSSymmetry > > factorizations =
        symm.factorize();

    // Find highest order point group of all outer factors
    PointGroup highest =
        PointGroup( std::vector< NCSOperator >(), angular, spatial );

    for(
        std::vector< std::pair< NCSSymmetry, NCSSymmetry > >
            ::const_iterator it = factorizations.begin();
        it != factorizations.end();
        ++it
        )
    {
        // Discard trivial factorization
        if ( it->first.get_operators().size() == 1
            || it->second.get_operators().size() == 1 )
        {
            continue;
        }

        PointGroup trial =
            from_operators( it->first.get_operators(), angular, spatial );

        if ( highest.get_operators().size() < trial.get_operators().size() )
        {
            highest = trial;
        }
    }

    return highest;

    /*
    if ( factors.first.get_operators().size() == 1
        || factors.second.get_operators().size() == 1 )
    {
        return PointGroup( std::vector< NCSOperator >(), angular, spatial );
    }

    // Pointgroup of outer factor
    return from_operators( factors.first.get_operators(), angular, spatial );

    // Prefiltering: keep non-unity op if op-1 and op*op appears in operators
    std::vector< NCSOperator > candidates;

    for ( opIter it = operators.begin(); it != operators.end(); ++it )
    {
        if ( !it->approx_unity( angular, spatial )
            && symm.contains( it->inverse() )
            && symm.contains( (*it) * (*it) )
            )
        {
            candidates.push_back( *it );
        }
    }

    // Create a graph to represent the operators
    NCSSymmetry tmp( candidates, angular, spatial );
    adjacency_list< vecS, vecS, directedS > graph( candidates.size() );

    // Connect operators l and r if l*r in candidates
    for ( size_t outer = 0; outer < candidates.size(); ++outer )
    {
        for ( size_t inner = 0; inner < candidates.size(); ++inner )
        {
            if ( outer == inner )
            {
                continue;
            }

            if ( tmp.contains( candidates[ outer ] * candidates[ inner ] ) )
            {
                add_edge( outer, inner, graph );
            }
        }
    }

    // Find strong components
    std::vector< int > components( candidates.size() );
    int count = strong_components( graph, &components[0] );

    // Determine component sizes
    std::map< int, int > size_of_component;

    for (
        std::vector< int >::const_iterator it = components.begin();
        it != components.end();
        ++it
        )
    {
        size_of_component[ *it ]++;
    }

    std::vector< std::pair< int, int > > index_size_pairs(
        size_of_component.begin(),
        size_of_component.end()
        );
    std::sort(
        index_size_pairs.begin(),
        index_size_pairs.end(),
        compare_pair_second
        );

    // In order or decreasing size, check whether it is a group and whether it
    // is present in the original list of operators
    assert( count == index_size_pairs.size() );
    assert( components.size() == candidates.size() );

    for (
        std::vector< std::pair< int, int > >::const_reverse_iterator it =
            index_size_pairs.rbegin();
        it != index_size_pairs.rend();
        ++it
        )
    {
        // Collect operators belonging to the component
        std::vector< NCSOperator > trials;

        for ( size_t op_index = 0; op_index < candidates.size(); ++op_index )
        {
            if ( components[ op_index ] == it->first )
            {
                trials.push_back( candidates[ op_index ] );
            }
        }

        if ( obeys_group_axioms( trials, angular, spatial ) )
        {
            NCSSymmetry trial_sym = NCSSymmetry( trials, angular, spatial );
            std::vector< NCSOperator > closeds = symm.operators_closed_wrt(
                trial_sym
                );
            if ( closeds.size() == symm.get_operators().size() )
            {
                return PointGroup( closeds, angular, spatial );
            }
        }
    }

    return PointGroup( std::vector< NCSOperator >(), angular, spatial );
    */
}

// Display
std::string
PointGroup::name() const
{
    return "PointGroup '" + hermann_mauguin_symbol() + "'";
}

// Public methods
std::string
PointGroup::hermann_mauguin_symbol() const
{
    // Sort by rotation angle
    std::vector< NCSOperator> ops( operators_ );
    std::stable_sort( ops.begin(), ops.end(), compare_rotation );

    // Find generators (operators whose repeated application generates the
    // group)
    std::vector< size_t > orders;
    std::vector< bool > accounted( ops.size(), false );
    const double& angular = get_angular_tolerance();
    const double& spatial = get_spatial_tolerance();

    for ( opIter it = ops.begin(); it != ops.end(); ++it )
    {
        opIter pos = find_operator( *it );
        assert( pos != operators_.end() );
        size_t index = pos - operators_.begin();

        if ( accounted[ index ] )
        {
            continue;
        }

        orders.push_back( 1 );
        NCSOperator current = *it;
        accounted[ index ] = true;

        while ( ! current.approx_unity( angular, spatial ) )
        {
            pos = find_operator( current );
            assert( pos != get_operators().end() );
            accounted[ pos - operators_.begin() ] = true;
            orders.back()++;
            current = (*pos) * (*it);
        }
    }

    // Analyse rotation folds and number of axes
    int higher_order_operators_count = std::count_if(
        orders.begin(),
        orders.end(),
        higher_order_operator
        );
    int highest_order = *std::max_element( orders.begin(), orders.end() );

    if ( 1 < higher_order_operators_count )
    {
        // Polyhedral symmetry
        if ( highest_order == 5 )
        {
            return std::string( "532" );
        }
        else if ( highest_order == 4 )
        {
            return std::string( "432" );
        }
        else if ( highest_order == 3 )
        {
            return std::string( "23" );
        }
        else
        {
            // This should never happen
            return std::string( "UNKNOWN" );
        }
    }
    else if ( 2 < orders.size() )
    {
        // Dihedral symmetry
        std::ostringstream oss;
        oss << highest_order << "2";

        if ( highest_order != 3 )
        {
            oss << "2";
        }

        return oss.str();
    }
    else if ( orders.size() == 2 )
    {
        // Rotational symmetry
        std::ostringstream oss;
        oss << highest_order;
        return oss.str();
    }
    else
    {
        // No symmetry
        return std::string( "1" );
    }
}

// Static methods
bool
PointGroup::obeys_group_axioms(
    const std::vector< NCSOperator >& ops,
    double angular,
    double spatial
    )
{
    // There is a unit element
    NCSSymmetry symm( ops, angular, spatial );

    if ( ! symm.contains( NCSOperator::unity ) )
    {
        return false;
    }

    // All operators are invertible
    for ( opIter it = ops.begin(); it != ops.end(); ++it )
    {
        if ( !symm.contains( it->inverse() ) )
        {
            return false;
        }
    }

    // Multiplication is closed
    for ( opIter it = ops.begin(); it != ops.end(); ++it )
    {
        std::vector< NCSOperator > products = symm.left_products( *it );

        if ( !symm.equals( products ) )
        {
            return false;
        }
    }

    return true;
}


}  } // namespace phaser
