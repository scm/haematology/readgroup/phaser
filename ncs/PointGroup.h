#ifndef PHASER_NCS_POINTGROUP_NEW_H
#define PHASER_NCS_POINTGROUP_NEW_H

#include <phaser/ncs/NCSSymmetry.h>

namespace phaser {  namespace ncs {

class PointGroup : public NCSSymmetry
{
    public:

        PointGroup(
            const std::vector< NCSOperator >& operators,
            double angular,
            double spatial
            );

    public:
        PointGroup() {}
        ~PointGroup();

        // Factory
        static PointGroup from_operators(
            const std::vector< NCSOperator >& ops,
            double angular,
            double spatial
            );

        // Display method
        std::string name() const;

        // Other methods
        std::string hermann_mauguin_symbol() const;
        static bool obeys_group_axioms(
            const std::vector< NCSOperator >& ops,
            double angular,
            double spatial
            );
};

}  } // namespace phaser

#endif /*PHASER_NCS_POINTGROUP_NEW_H*/
