#include <phaser/ncs/LCSTree.h>

#include <phaser/ncs/SuffixTree.h>


using namespace std;
using namespace boost;

namespace phaser {  namespace ncs {

//
// LCSTree methods
//

// Constructor
LCSTree::LCSTree( const string& separator )
        : _separator( separator ), _word_count( 0 )
{
}


// Destructor
LCSTree::~LCSTree()
{
}


// Accessors
size_t
LCSTree::get_word_count() const
{
    return _word_count;
}


const string&
LCSTree::get_separator() const
{
    return _separator;
}


// LCS behaviour
void
LCSTree::insert_word( const string& word )
{
    _separated_words.append( word + _separator + string( 1, '0' + _word_count ) );
    _word_count++;
}


const string
LCSTree::get_longest_common_substring() const
{
    SuffixTree tree( _separated_words );
    Node root( tree.get_root() );

    root.append_to_set( assign_terminators( root ) );

    return get_lcs_under( root );
}


// Private methods
const set< char >
LCSTree::assign_terminators( const Node& root ) const
{
    set< char > terminators;

    for (
            Node::const_iterator iter = root.begin();
            iter != root.end();
            ++iter )
    {
        shared_ptr< EdgeData > branch_ptr( iter->second );
        Edge branch( branch_ptr );

        size_t separator_index = branch.get_arc().find( _separator );

        // Not found
        if ( separator_index == string::npos )
        {
            Node end_node( branch.get_end_node() );
            set< char > terminators_under_current =
                    assign_terminators( end_node );
            end_node.append_to_set( terminators_under_current );
            terminators.insert(
                    terminators_under_current.begin(),
                    terminators_under_current.end() );

        // Separator found at end of arc
        } else if ( separator_index == ( branch.length() - 1 ) )
        {
            Node end_node( branch.get_end_node() );
            set< char > terminators_under_current;

            for (
                    Node::const_iterator sub_iter = end_node.begin();
                    sub_iter != end_node.end();
                    ++sub_iter )
            {
                shared_ptr< EdgeData > sub_branch_ptr( sub_iter->second );
                Edge sub_branch( sub_branch_ptr );

                terminators_under_current.insert( sub_branch.get_arc()[0] );
            }

            end_node.append_to_set( terminators_under_current );
            terminators.insert(
                    terminators_under_current.begin(),
                    terminators_under_current.end() );

        // Separator found within arc
        } else
        {
            terminators.insert( branch.get_arc()[ separator_index + 1 ] );
        }
    }

    return terminators;
}


const string
LCSTree::get_lcs_under( const Node& root ) const
{
    string longest_string;

    for (
            Node::const_iterator iter = root.begin();
            iter != root.end();
            ++iter )
    {
        shared_ptr< EdgeData > branch_ptr( iter->second );
        Edge branch( branch_ptr );

        if ( branch.get_end_node().inspect_set().size() < get_word_count() )
        {
            continue;
        }

        string trial_longest = branch.get_arc()
                + get_lcs_under( branch.get_end_node() );

        if ( trial_longest[ trial_longest.size() - 1 ] == _separator[0] )
        {
            trial_longest = trial_longest.substr( 0, trial_longest.size() -1 );
        }

        if ( longest_string.size() < trial_longest.size() )
        {
            longest_string = trial_longest;
        }
    }

    return longest_string;
}

}  } // namespace phaser
