#include <phaser/ncs/SuffixTree.h>

using namespace std;

namespace phaser {  namespace ncs {

//
// SuffixTree methods
//

// Constructor and destructor
SuffixTree::SuffixTree( const string& word )
        : _root( Node() ), _active_suffix( word, _root )
{
    construct_tree();
}


SuffixTree::~SuffixTree()
{
}


// Accessors
const Node&
SuffixTree::get_root() const
{
    return _root;
}


// SuffixTree behaviour
void
SuffixTree::dump_tree() const
{
    Edge tmp_edge( "", _root );
    tmp_edge.dump_edge();
}


void
SuffixTree::add_next_char()
{
    Node previous_entry_point( _root );

    while ( true )
    {
        _active_suffix.reduce();
        _active_suffix.prepare_new_entry();

        if ( _active_suffix.entry_point_empty() )
        {
            _active_suffix.extend();
            break;
        }

        Node entry_point( _active_suffix.get_entry_point() );

        Edge new_edge(
                _active_suffix.get_residual_suffix(),
                Node() );

        new_edge.connect_edge_to( entry_point );

        if ( previous_entry_point != _root )
        {
            previous_entry_point.link_suffix_node( entry_point );
        }

        previous_entry_point = entry_point;

        if ( _active_suffix.get_origin() == _root )
        {
            if ( _active_suffix.is_explicit() )
            {
                _active_suffix.extend();
                _active_suffix.decrement();
                break;
            } else
            {
                _active_suffix.decrement();
            }
        } else
        {
            _active_suffix.shift_origin_to_suffix();

        }
    }

    if ( previous_entry_point != _root )
    {
        previous_entry_point.link_suffix_node(
                _active_suffix.get_origin() );
    }
}


void
SuffixTree::construct_tree()
{
    while ( _active_suffix.has_next_char() )
    {
        add_next_char();
    }
}

}  } // namespace phaser::ncs
