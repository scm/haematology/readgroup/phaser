#include <phaser/ncs/Edge.h>

#include <map>
#include <iostream>

using namespace boost;
using namespace std;

namespace phaser  { namespace ncs {

//
// EdgeData methods
//

// Constructor and destructor
EdgeData::EdgeData( const std::string& arc, const Node& end_node )
        : _arc( arc ), _end_node( end_node )
{
}


EdgeData::~EdgeData()
{
}


//
// Edge methods
//

// Constructor and destructor
Edge::Edge( const std::string& arc, const Node& end_node )
    : _edge_data( new EdgeData( arc, end_node ) )
{
}


Edge::Edge( const shared_ptr< EdgeData >& edge_data ) : _edge_data( edge_data )
{
}


Edge::Edge( const char edge_key, const Node& parent_node )
    : _edge_data(  parent_node.get_child( edge_key ) )
{
}


Edge::Edge( const Edge& rhs ) : _edge_data( rhs._edge_data )
{
}


Edge::~Edge()
{
}


// Accessors
size_t
Edge::length() const
{
    return _edge_data->_arc.length();
}


const string&
Edge::get_arc() const
{
    return _edge_data->_arc;
}


const Node&
Edge::get_end_node() const
{
    return _edge_data->_end_node;
}


// Embedded Node management
void
Edge::connect_edge_to( const Node& node ) const
{
    node.insert_child( _edge_data->_arc[0], _edge_data );
}


// Printing
void
Edge::dump_edge( const std::string& separator ) const
{


    std::cout << separator << _edge_data->_arc << std::endl;

    std::string padded_separator = separator + std::string( length(), '|' );

    for (
            Node::const_iterator iter = _edge_data->_end_node.begin();
            iter != _edge_data->_end_node.end();
            ++iter )
    {
        shared_ptr< EdgeData > edge_data_ptr = iter->second;
        Edge( edge_data_ptr ).dump_edge( padded_separator );
    }
}


// Edge spliting
void
Edge::split_edge_at( const int index ) const
{
    std::string new_edge_arc = _edge_data->_arc.substr( index );
    Edge new_edge( new_edge_arc, _edge_data->_end_node );

    _edge_data->_arc = _edge_data->_arc.substr( 0, index );
    _edge_data->_end_node = Node();
    new_edge.connect_edge_to( get_end_node() );
}

}  } // namespace phaser::ncs
