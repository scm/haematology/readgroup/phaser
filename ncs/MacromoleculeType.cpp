#include <phaser/ncs/MacromoleculeType.h>
#include <cassert>

namespace phaser { namespace ncs {

//
// MacromoleculeType
//

// Constructor
MacromoleculeType::MacromoleculeType(
    const std::vector< char >& one_letter_codes,
    const std::vector< std::string >& three_letter_codes,
    char unknown_one_letter,
    const std::string& unknown_three_letter,
    size_t min_chain_length,
    const std::set< iotbx::pdb::str4 >& backbone_atom_names,
    const std::string& type_name
    ) : one_letter_codes_( one_letter_codes ),
        three_letter_codes_( three_letter_codes ),
        unknown_one_letter_( unknown_one_letter ),
        unknown_three_letter_( unknown_three_letter ),
        min_chain_length_( min_chain_length ),
        backbone_atom_names_( backbone_atom_names ),
        type_name_( type_name )
{
    assert( one_letter_codes_.size() == three_letter_codes_.size() );
}

// Destructor
MacromoleculeType::~MacromoleculeType()
{
}

// Accessors
const std::set< iotbx::pdb::str4 >&
MacromoleculeType::get_backbone_atom_names() const
{
    return backbone_atom_names_;
}


const std::string&
MacromoleculeType::get_type_name() const
{
    return type_name_;
}


char
MacromoleculeType::get_unknown_one_letter() const
{
    return unknown_one_letter_;
}

// Translation
std::map< std::string, char >
MacromoleculeType::get_three_to_one() const
{
    std::map< std::string, char > trans;

    for ( size_t index = 0; index < one_letter_codes_.size(); ++index )
    {
        trans[ three_letter_codes_[ index ] ] = one_letter_codes_[index ];
    }

    return trans;
}


std::string
MacromoleculeType::residue_group_sequence(
    const std::vector< iotbx::pdb::hierarchy::residue_group >& rgs
    ) const
{
    std::map< std::string, char > trans = get_three_to_one();
    std::string sequence;

    for (
        std::vector< iotbx::pdb::hierarchy::residue_group >::const_iterator it =
            rgs.begin();
        it != rgs.end();
        ++it
        )
    {
        if ( it->atom_groups().size() == 0 )
        {
            continue;
        }

        std::string resname = it->atom_groups()[0].data->resname;

        if ( trans.find( resname ) != trans.end() )
        {
            sequence.push_back( trans[ resname ] );
        }
        else
        {
            sequence.push_back( get_unknown_one_letter() );
        }
    }

    return sequence;
}


// Conformance
bool
MacromoleculeType::recognize_chain(
    const iotbx::pdb::hierarchy::chain& chain,
    double confidence
    ) const
{
    size_t recognized = count_recognized_residues( chain.residue_groups() );
    size_t threshold = confidence * chain.residue_groups().size();

    if ( min_chain_length_ <= recognized && threshold <= recognized )
    {
        return true;
    }

    return false;
}

size_t
MacromoleculeType::count_recognized_residues(
    const std::vector< iotbx::pdb::hierarchy::residue_group >& rgs
    ) const
{
    std::set< std::string > known_codes(
        three_letter_codes_.begin(),
        three_letter_codes_.end()
        );
    size_t recognized_count = 0;

    for(
        std::vector< iotbx::pdb::hierarchy::residue_group >::const_iterator it =
            rgs.begin();
        it != rgs.end();
        ++it
        )
    {
        if ( it->atom_groups().size() == 0 )
        {
            continue;
        }

        iotbx::pdb::hierarchy::atom_group ag = it->atom_groups()[0];
        std::set< std::string >::const_iterator index_it =
            known_codes.find( ag.data->resname );

        if ( index_it != known_codes.end() )
        {
            ++recognized_count;
        }
    }

    return recognized_count;
}

// Factory methods
void
MacromoleculeType::get_chain_type( const iotbx::pdb::hierarchy::chain& chain )
{
  const MacromoleculeType protein; //default
  const MacromoleculeType unknown('X');
  const std::vector< MacromoleculeType > known_types = boost::assign::list_of
      ( protein ).convert_to_container<std::vector<MacromoleculeType> >();

    for(
        std::vector< MacromoleculeType >::const_iterator it =
            known_types.begin();
        it != known_types.end();
        ++it
        )
    {
        if ( it->recognize_chain( chain ) )
        {
           unknown_one_letter_= 'X' ,
           unknown_three_letter_= "UNK" ,
           min_chain_length_=5,
           backbone_atom_names_= boost::assign::list_of
             ( " CA " )( " N  " )( " C  " )( " O  " ).
             convert_to_container<std::set<iotbx::pdb::str4> >() ,
           type_name_ = "protein",
           three_letter_codes_ = boost::assign::list_of
                 ( "ALA" )( "CYS" )( "ASP" )( "GLU" )( "PHE" )
                 ( "GLY" )( "HIS" )( "ILE" )( "LYS" )( "LEU" )( "MSE" )( "MET" )
                 ( "ASN" )( "PRO" )( "GLN" )( "ARG" )( "SER" )( "THR" )( "VAL" )
                 ( "TRP" )( "TYR" ).
                 convert_to_container<std::vector<std::string> >(),
           one_letter_codes_ = boost::assign::list_of
                  ( 'A' )( 'C' )( 'D' )( 'E' )( 'F' )( 'G' )( 'H' )
                  ( 'I' )( 'K' )( 'L' )( 'M' )( 'M' )( 'N' )( 'P' )( 'Q' )( 'R' )( 'S' )
                  ( 'T' )( 'V' )( 'W' )( 'Y' ).
                  convert_to_container<std::vector<char> >(),
           backbone_atom_names_ = boost::assign::list_of
               ( " CA " )( " N  " )( " C  " ) ( " O  " ).
               convert_to_container<std::set<iotbx::pdb::str4> >();
           return;
 //           return *it;
        }
    }

    unknown_one_letter_= 'X' ;
    unknown_three_letter_= "UNK" ;
    min_chain_length_=0;
    type_name_ = "unknown" ;
   // return unknown;
}

}  } // namespace phaser::ncs
