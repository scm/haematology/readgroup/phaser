#ifndef PHASER_NCS_NODE_H
#define PHASER_NCS_NODE_H

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/noncopyable.hpp>

#include <map>
#include <set>

namespace phaser {  namespace ncs {

    using boost::shared_ptr; // required for C++0x compatibility

    // Forward declaration
    class EdgeData;

    class NodeData : boost::noncopyable
    {
        public:
            // Data members
            std::map< char, boost::shared_ptr< EdgeData > > _edges;
            std::set< char > _terminators;
            boost::weak_ptr< NodeData > _suffix_node;

            // Constructor and destructor
            NodeData();
            virtual ~NodeData();
    };


    class Node
    {
        private:
            boost::shared_ptr< NodeData > _node_data;

        public:
            // Constructors and destructor
            Node();
            Node( boost::shared_ptr< NodeData > node_data );
            Node( const Node& rhs );
            virtual ~Node();

            // Overloaded operators
            Node& operator=( const Node& rhs );
            bool operator==( const Node& rhs ) const;
            bool operator!=( const Node& rhs ) const;

            // Child management
            void insert_child(
                    const char child_key,
                    const boost::shared_ptr< EdgeData >& child_to_add ) const;
            const boost::shared_ptr< EdgeData >&
                    get_child( const char child_key ) const;
            bool has_child( const char child_key ) const;

            // Suffix nodes
            void link_suffix_node( const Node& suffix_node ) const;
            const Node get_suffix_node() const;

            // Embedded set
            void append_to_set( const std::set< char >& values ) const;
            const std::set< char >& inspect_set() const;

            // Iteration interface
            typedef std::map<
                    char,
                    boost::shared_ptr< EdgeData >
                >::const_iterator const_iterator;

            const_iterator begin() const;
            const_iterator end() const;
    };

}  }

#endif /*PHASER_NCS_NODE_H*/
