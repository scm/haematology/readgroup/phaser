#include <phaser/ncs/NCSOperator.h>

#include <sstream>
#include <iomanip>
#include <cmath>
#include <algorithm>

using namespace std;
using namespace scitbx;

namespace phaser {  namespace ncs {

//
// class NCSOperator
//


// Constructor
NCSOperator::NCSOperator(mat3< double > const& rotation, vec3< double > const& translation)
    : rotation_( rotation ), translation_( translation )
{
}

// Destructor
NCSOperator::~NCSOperator()
{
}


// Public methods

// Display method
string
NCSOperator::to_string() const
{
    ostringstream oss;

    oss << "ROTA 1: "
        << setw(8) << setprecision(4) << fixed << rotation_( 0, 0 )
        << setw(8) << setprecision(4) << fixed << rotation_( 0, 1 )
        << setw(8) << setprecision(4) << fixed << rotation_( 0, 2 )
        << std::endl;
    oss << "ROTA 2: "
        << setw(8) << setprecision(4) << fixed << rotation_( 1, 0 )
        << setw(8) << setprecision(4) << fixed << rotation_( 1, 1 )
        << setw(8) << setprecision(4) << fixed << rotation_( 1, 2 )
        << std::endl;
    oss << "ROTA 3: "
        << setw(8) << setprecision(4) << fixed << rotation_( 2, 0 )
        << setw(8) << setprecision(4) << fixed << rotation_( 2, 1 )
        << setw(8) << setprecision(4) << fixed << rotation_( 2, 2 )
        << std::endl;
    oss << "TRANS:  "
        << setw(8) << setprecision(4) << fixed << translation_[0]
        << setw(8) << setprecision(4) << fixed << translation_[1]
        << setw(8) << setprecision(4) << fixed << translation_[2];

    return oss.str();
}


// Information methods
double
NCSOperator::rotation_angle_radian() const
{
  double rotation_angle_cosine =  std::max(
        std::min( ( ( rotation_.trace() - 1.0 ) / 2.0 ), 1.0 ),
        -1.0
        );
    return acos( rotation_angle_cosine );
}


double
NCSOperator::displacement() const
{
    return translation_.length();
}


const scitbx::vec3< double >&
NCSOperator::get_translation() const
{
    return translation_;
}


const scitbx::mat3< double >&
NCSOperator::get_rotation() const
{
    return rotation_;
}


// Mathematical operations
NCSOperator
NCSOperator::operator*(const NCSOperator& other) const
{
    return NCSOperator(
        rotation_ * other.rotation_,
        rotation_ * other.translation_ + translation_
        );
}


NCSOperator
NCSOperator::inverse() const
{
    scitbx::mat3< double > inv_rot = rotation_.inverse();
    return NCSOperator( inv_rot, -inv_rot * translation_ );
}


bool
NCSOperator::approx_unity(double angular, double spatial) const
{
    return ( rotation_angle_radian() <= angular
        && translation_.length() <= spatial );
}


bool
NCSOperator::approx_inverse(
    const NCSOperator& other,
    double angular,
    double spatial
    ) const
{
    return ( other * (*this) ).approx_unity( angular, spatial );
}


bool
NCSOperator::approx_equals(
    const NCSOperator& other,
    double angular,
    double spatial
    ) const
{
    return approx_inverse( other.inverse(), angular, spatial );
}

// Miscellaneous methods
scitbx::vec3< double >
NCSOperator::transform(const scitbx::vec3< double >& point) const
{
    return rotation_ * point + translation_;
}

// Constants
const NCSOperator NCSOperator::unity = NCSOperator(
    scitbx::mat3< double >( 1.0 ),
    scitbx::vec3< double >( 0.0 )
    );

}  } // namespace phaser::ncs
