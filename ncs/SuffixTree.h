#ifndef PHASER_NCS_SUFFIXTREE_H
#define PHASER_NCS_SUFFIXTREE_H

#include <phaser/ncs/Suffix.h>

namespace phaser {  namespace ncs {

    class SuffixTree
    {
        private:
            Node _root;
            Suffix _active_suffix;

        public:
            // Constructor and destructor
            SuffixTree( const std::string& word );
            virtual ~SuffixTree();

            // Accessors
            const Node& get_root() const;

            // Methods
            void dump_tree() const;
            void construct_tree();
            void add_next_char();
    };

}  }

#endif /*PHASER_NCS_SUFFIXTREE_H*/
