#ifndef PHASER_NCS_NCSSYMMETRY_H
#define PHASER_NCS_NCSSYMMETRY_H

#include <phaser/ncs/NCSOperator.h>

#include <vector>

namespace phaser {  namespace ncs {

class NCSSymmetry
{
    public:
        typedef std::vector< NCSOperator >::const_iterator opIter;

    protected:
        std::vector< NCSOperator > operators_;

    private:
        double angular_;
        double spatial_;

    protected:
        opIter find_operator(const NCSOperator& op) const;

    public:
        NCSSymmetry() {}

        NCSSymmetry(
            const std::vector< NCSOperator >& operators,
            double angular,
            double spatial
            );
        virtual ~NCSSymmetry();

        // Accessors
        const std::vector< NCSOperator >& get_operators() const;
        std::vector< scitbx::vec3< double > > get_euler_list() const;
        std::vector< scitbx::vec3< double > > get_orth_list() const;
        double get_angular_tolerance() const;
        double get_spatial_tolerance() const;

        int    multiplicity() const;
        scitbx::vec3< double > get_euler(int) const;
        scitbx::vec3< double > get_orth(int) const;

        // Display methods
        std::string to_string() const;
        virtual std::string name() const;

        // Query methods
        bool contains(const NCSOperator& op) const;
        bool equals(const std::vector< NCSOperator >& ops) const;

        // Other methods
        std::vector< NCSOperator > left_products(const NCSOperator& op) const;
        std::vector< NCSOperator > right_products(const NCSOperator& op) const;
        std::vector< NCSOperator > intersect(
            const std::vector< NCSOperator >& sym
            ) const;
        std::vector< std::pair< NCSSymmetry, NCSSymmetry > > factorize() const;
        std::vector< NCSOperator > quotient(const NCSSymmetry& divisor) const;
        std::vector< NCSOperator > direct_product(const NCSSymmetry& i) const;
};

}  } // namespace phaser::ncs

#endif /*PHASER_NCS_NCSSYMMETRY_H*/
