from __future__ import print_function

class RemoveShort(object):
  """
  Removes residues that would map to a short segment on the model structure
  """

  def __init__(self, min_length):

    assert 0 < min_length
    self.min_length = min_length


  def __call__(self, sample, moribund):

    from phaser import sequtil

    it = sequtil.split(
      sequence = [ rg for rg in sample.chain.residue_groups() if rg not in moribund ],
      consecutivity = sample.mmtype.residue_structural_connectivity(),
      )

    for contiguous in it:
      if len( contiguous ) < self.min_length:
        moribund.update( contiguous )


class UndoShort(object):
  """
  Reinstate residues that would map to a short segment on the model structure
  """

  def __init__(self, max_length):

    assert 0 <= max_length
    self.max_length = max_length


  def __call__(self, sample, moribund):

    from phaser import sequtil
    rgs = sample.chain.residue_groups()
    consecutivity = sample.mmtype.residue_structural_connectivity()

    it = sequtil.split(
      sequence = enumerate( rg for rg in rgs if rg in moribund ),
      consecutivity = lambda l, r: consecutivity( l[1], r[1] ),
      )

    i_last = len( rgs ) - 1

    for contiguous in it:
      if self.max_length < len( contiguous ):
        continue

      ( i_left, rg_left ) = contiguous[0]
      ( i_right, rg_right ) = contiguous[0]

      if i_left == 0 or i_right == i_last:
        continue

      assert 0 < i_left
      assert i_right < i_last

      if not consecutivity( rgs[ i_left -1 ], rg_left ):
        continue

      if not consecutivity( rg_right, rgs[ i_right + 1 ] ):
        continue

      for pair in contiguous:
        moribund.remove( pair[1] )


class KeepRegular(object):
  """
  Reinstates consecutive residues up to max_length if they are in regular
  secondary structure
  """

  def __init__(self, max_length):

    assert 0 <= max_length
    self.max_length = max_length


  def __call__(self, sample, moribund):

    from phaser import sequtil
    consec = sample.mmtype.residue_structural_connectivity()

    for annotation in sample.mmtype.secondary_structure_annotations( chain = sample.chain ):
      sec_str_rgs = annotation.residue_groups
      mbs = [ rg for rg in sec_str_rgs if rg in moribund ]

      for segment in sequtil.split( sequence = mbs, consecutivity = consec ):
        if self.max_length < len( segment ):
          continue

        rgs = set( segment )

        #if sec_str_rgs[0] in rgs or sec_str_rgs[-1] in rgs:
        #  continue

        for rg in segment:
          moribund.remove( rg )
