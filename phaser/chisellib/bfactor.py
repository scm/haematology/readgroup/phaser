from __future__ import print_function

class Scale(object):
  """
  A decorator-like class to scale the values
  """

  def __init__(self, algorithm, scale):

    self.algorithm = algorithm
    self.scale = scale


  def __call__(self, sample):

    return [ self.scale * v for v in self.algorithm( sample = sample ) ]


class Original(object):
  """
  Original bfactor
  """

  def __call__(self, sample):

    return sample.chain.atoms().extract_b()


class AccessibleSurfaceArea(object):
  """
  Accessible surface area
  """

  def __init__(self, probe, precision):

    assert 0 < probe
    assert 0 < precision
    self.probe = probe
    self.precision = precision


  def __call__(self, sample):

    return sample.accessible_surface_area(
      radius = self.probe,
      precision = self.precision
      )


class SequenceSimilarity(object):
  """
  Sequence similarity score.

  NOTE: ss_calc must be on atom frame
  """

  def __init__(self, ss_calc):

    self.ss_calc = ss_calc


  def __call__(self, sample):

    return self.ss_calc( sample = sample )


class RMSError(object):
  """
  Converts RMS estimates into B-factors
  """

  def __init__(self, substitution):

    self.substitution = substitution


  def __call__(self, sample):

    import math
    multiplier = 8.0 / 3.0 * math.pi ** 2
    errors = self.substitution(
      sequence = sample.residue_errors(),
      all_none_substitute = 0.00,
      )

    rg_bfacs = [ multiplier * err ** 2 for err in errors ]

    bfac_for = dict( zip( sample.residues(), rg_bfacs ) )
    results = []

    for rg in sample.chain.residue_groups():
      results.extend( [ bfac_for[ rg ] ] * len( rg.atoms() ) )

    return results
