from __future__ import print_function

class Null(object):
  """
  An algorithm for not pruning anything
  """

  def __call__(self, sample):

    return [ None ] * len( sample.chainalignment.residue_sequence() )


class Schwarzenbacher(object):
  """
  Truncates sidechain if residues types do not agree
  """

  def __init__(self, level):

    assert 0 <= level
    self.level = level


  def __call__(self, sample):

    ca = sample.chainalignment
    return [
      None if t == m else self.level
      for ( t, m ) in zip( ca.target_sequence(), ca.model_sequence() )
      ]


class Similarity(object):
  """
  Sequence similarity defined sidechain truncation:
    Full length if upper_limit < conservation,
    Full truncation if conservation < lower_limit,
    Predefined truncation if upper_limit < conservation < lower_limit
  """

  def __init__(self, ss_calc, lower, upper, level, base):

    assert 0 <= level and lower <= upper and base <= level

    self.ss_calc = ss_calc
    self.lower = lower
    self.upper = upper
    self.level = level
    self.base = base


  def __call__(self, sample):

    return [
      self.pruning_level_for( similarity = s ) for s in self.ss_calc( sample = sample )
      ]


  def pruning_level_for(self, similarity):

    if self.upper <= similarity:
      return None

    elif self.lower <= similarity:
      return self.level

    else:
      return self.base
