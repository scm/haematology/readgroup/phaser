from __future__ import print_function

class completion(object):
  """
  Complete missing atoms if requested
  """

  def __init__(self, builder):

    self.builder = builder


  def __call__(self, mmt, resname, coord_for, occ):

    builder = mmt.builder_dict[ self.builder ]
    updated_atom_data = []
    new_atom_data = []

    from phaser import chisellib

    for ( name, element, xyz ) in builder( resname = resname, coord_for = coord_for ):
      adata = chisellib.AtomProperty(
        name = name,
        element = element,
        hetero = mmt.hetero,
        xyz = xyz,
        b = 0.0,
        occ = occ,
        uij_presence = False,
        )

      if name in coord_for:
        updated_atom_data.append( adata )

      else:
        new_atom_data.append( adata )

    return ( updated_atom_data, new_atom_data )


def no_completion(mmt, resname, coord_for, occ):
  """
  No residue completion
  """

  return ( [], [] )


def residue_group_interacting_atoms(residue_group):

  import collections
  ags_altloced = collections.OrderedDict()

  for ag in residue_group.atom_groups():
    altloc = ag.altloc.strip()
    ags_altloced.setdefault( altloc, [] ).append( ag )

  if "" in ags_altloced:
    commons = ags_altloced[ "" ]
    del ags_altloced[ "" ]

  else:
    commons = []

  if ags_altloced:
    shareds = sum( [ list( ag.atoms() ) for ag in commons ], [] )

    for others in ags_altloced.values():
      yield ( shareds + sum( [ list( ag.atoms() ) for ag in others ], [] ), commons + others )

  else:
    assert commons
    yield ( residue_group.atoms(), commons )


class mainchain_distancer(object):

  def __init__(self, selector, mainchain_distance_for):

    self.selector = selector
    self.mainchain_distance_for = mainchain_distance_for


  def __call__(self, atom, name):

    return self.mainchain_distance_for[ self.selector( atom, name ) ]


  @staticmethod
  def get_compound_connectivity(mmt, resname, atoms, target):

    assert target is not None

    if target == mmt.unknown_three_letter:
      return mmt.unknown_residue_connectivity

    try:
      return mmt.bonds_from_idealized_residues( resname = target )

    except KeyError:
      pass

    try:
      return mmt.bonds_from_library( resname = target )

    except KeyError:
      pass

    if target == resname:
      from phaser import residue_topology_new as residue_topology
      geometry = residue_topology.geometry_from_structure(
        resname = target,
        atoms = atoms,
        )
      return geometry.bonds( tolerance = mmt.bond_length_tolerance )

    else:
      return mmt.unknown_residue_connectivity


  @classmethod
  def get_compound_mainchain_distance(cls, mmt, resname, atoms, target):

    from phaser import residue_topology_new as residue_topology
    return residue_topology.mainchain_distances(
      bonds = cls.get_compound_connectivity(
        mmt = mmt,
        resname = resname,
        atoms = atoms,
        target = target,
        ),
      mainchains = mmt.mainchain_atoms,
      attachment = mmt.sidechain_attachment_atom,
      )


  @classmethod
  def Original(cls, mmt, resname, atoms, target):

    md_for = cls.get_compound_mainchain_distance(
      mmt = mmt,
      resname = resname,
      atoms = atoms,
      target = resname,
      )

    from phaser import identity

    return cls(
      selector = lambda atom, name: atom,
      mainchain_distance_for = dict(
        ( a, md_for.get( identity.Atom.from_iotbx_atom( atom = a ) ) ) for a in atoms
        ),
      )


  @classmethod
  def Sequence(cls, mmt, resname, atoms, target):

    return cls(
      selector = lambda atom, name: name,
      mainchain_distance_for = cls.get_compound_mainchain_distance(
        mmt = mmt,
        resname = resname,
        atoms = atoms,
        target = target,
        ),
      )


def map_and_select_best_mapping(mmt, resname, atoms, target, mapper, pruner, distancer):

  mappings = mapper( mmtype = mmt, resname = resname, atoms = atoms, target = target )
  assert mappings
  pruneds = []

  for amap in mappings:
    assert len( amap ) == len( atoms )
    pmap = []

    for ( atom, name ) in zip( atoms, amap ):
      if name is None:
        continue

      try:
        dist = distancer( atom = atom, name = name )

      except KeyError:
        continue

      if dist is not None and not pruner( distance = dist ):
        pmap.append( ( ( atom, name ), dist ) )

    pruneds.append(
      (
        [ p[0] for p in pmap ],
        sum( p[1] for p in pmap ),
        sum( 1 for ( ( l, r ), d ) in pmap if l.name == r.pdb_name ),
        )
      )

  best = max( pruneds, key = lambda m: ( len( m[0] ), -m[1], m[2] ) )[0]
  new_name_for = dict( best )
  assert len( best ) == len( new_name_for )

  return dict( ( a, new_name_for.get( a ) ) for a in atoms )


class RenameData(object):
  """
  Rename data provided for each atom_group
  """

  def __init__(
    self,
    resname,
    existing_atom_remapped_name_for,
    existing_atom_remapped_xyz_for,
    new_atom_remapped_name_for,
    ):

    self.resname = resname
    self.existing_atom_remapped_name_for = existing_atom_remapped_name_for
    self.existing_atom_remapped_xyz_for = existing_atom_remapped_xyz_for
    self.new_atom_remapped_name_for = new_atom_remapped_name_for


def target(mmt, atom_group, resname):

  return resname


def target_ptm(mmt, atom_group, resname):

  one_letter_for = mmt.one_letter_for()

  if atom_group.resname in one_letter_for:
    ag_rescode = one_letter_for[ atom_group.resname ]
    t_rescode = one_letter_for.get( resname, mmt.unknown_one_letter )

    if ag_rescode == t_rescode:
      return atom_group.resname

  return resname


class Original(object):
  """
  Leaves original sequence, removing atoms that do not map to anything with the
  target sequence
  """

  def __init__(self, mapping, completion, namer):

    self.mapping = mapping
    self.completion = completion
    self.namer = namer


  def atom_name_mapping(self, mmt, resname, atoms, target, pruner):

    from phaser import identity

    distancer = mainchain_distancer.Original(
      mmt = mmt,
      resname = resname,
      atoms = atoms,
      target = target,
      )
    atom_name_mapping_for = map_and_select_best_mapping(
      mmt = mmt,
      resname = resname,
      atoms = atoms,
      target = target,
      mapper = self.mapping,
      pruner = pruner,
      distancer = distancer,
      )

    return dict(
      ( a, identity.Atom.from_iotbx_atom( atom = a ) if atom_name_mapping_for[ a ] is not None else None )
      for a in atoms
      )


  def __call__(self, residue_group, target, mmt, pruner):

    from phaser import chisellib

    new_atoms_for = {}
    new_name_for = {}
    new_xyz_for = {}

    for ( atoms, ags ) in residue_group_interacting_atoms( residue_group = residue_group ):
      assert atoms
      assert ags

      atom_named_as = dict( ( a.name, a ) for a in atoms )

      if len( atom_named_as ) != len( atoms ):
        raise chisellib.HierarchyError("Multiple atoms named the same")

      destination = ags[-1]

      if target is None:
        maptype = mmt.unknown_three_letter

      else:
        maptype = self.namer( mmt = mmt, atom_group = destination, resname = target )

      resname = destination.resname

      ( updated_atom_data, new_atom_data ) = self.completion(
        mmt = mmt,
        resname = resname,
        coord_for = dict( ( a.name, a.xyz ) for a in atoms ),
        occ = destination.atoms()[0].occ,
        )

      representer_for = {}

      for adata in updated_atom_data:
        representer_for[ atom_named_as[ adata.name ] ] = adata.as_iotbx_atom()

      new_atom_property_for = {}

      for adata in new_atom_data:
        new_atom_property_for[ adata.as_iotbx_atom() ] = adata

      mapped_name_for = self.atom_name_mapping(
        mmt = mmt,
        resname = resname,
        atoms = [ representer_for.get( a, a ) for a in atoms ] + new_atom_property_for.keys(),
        target = maptype,
        pruner = pruner,
        )

      for atom in atoms:
        if atom in new_name_for:
          continue

        corresponding = representer_for.get( atom, atom )
        assert corresponding in mapped_name_for
        new_name_for[ atom ] = mapped_name_for[ corresponding ]
        new_xyz_for[ atom ] = corresponding.xyz

      assert all( a in mapped_name_for for a in new_atom_property_for.keys() )
      assert destination not in new_atoms_for
      new_atoms_for[ destination ] = dict(
        ( new_atom_property_for[ a ], mapped_name_for[ a ] )
        for a in new_atom_property_for.keys()
        )

    rename_data_for = {}

    for ag in residue_group.atom_groups():
      rename_data_for[ ag ] = RenameData(
        resname = ag.resname,
        existing_atom_remapped_name_for = dict(
          ( a, new_name_for[ a ] ) for a in ag.atoms()
          ),
        existing_atom_remapped_xyz_for = dict(
          ( a, new_xyz_for[ a ] ) for a in ag.atoms()
          ),
        new_atom_remapped_name_for = new_atoms_for.get( ag, {} ),
        )

    return rename_data_for


  @classmethod
  def Target(cls, mapping, completion):

    return cls( mapping = mapping, completion = completion, namer = target )


  @classmethod
  def TargetPtm(cls, mapping, completion):

    return cls( mapping = mapping, completion = completion, namer = target_ptm )



class Sequence(object):
  """
  Rename according to target sequence
  """

  def __init__(self, mapping, completion, namer, gapname):

    self.mapping = mapping
    self.completion = completion
    self.namer = namer
    self.gapname = gapname


  def __call__(self, residue_group, target, mmt, pruner):

    new_atoms_for = {}
    new_name_for = {}
    new_xyz_for = {}
    resname_for = {}

    for ( atoms, ags ) in residue_group_interacting_atoms( residue_group = residue_group ):
      assert atoms
      assert ags

      destination = ags[-1]

      if target is None:
        maptype = mmt.unknown_three_letter
        resname = self.gapname

      else:
        maptype = self.namer( mmt = mmt, atom_group = destination, resname = target )
        resname = maptype

      for ag in ags:
        if ag not in resname_for:
          resname_for[ ag ] = resname

      distancer = mainchain_distancer.Sequence(
        mmt = mmt,
        resname = destination.resname,
        atoms = atoms,
        target = maptype,
        )
      mapped_name_for = map_and_select_best_mapping(
        mmt = mmt,
        resname = destination.resname,
        atoms = atoms,
        target = maptype,
        mapper = self.mapping,
        pruner = pruner,
        distancer = distancer,
        )

      for atom in atoms:
        if atom in new_name_for:
          continue

        assert atom in mapped_name_for
        new_name_for[ atom ] = mapped_name_for[ atom ]

      atom_renamed_to = dict(
        ( mapped_name_for[ a ].pdb_name, a ) for a in atoms
        if mapped_name_for[ a ] is not None
        )

      ( updated_atom_data, new_atom_data ) = self.completion(
        mmt = mmt,
        resname = maptype,
        coord_for = dict( ( k, a.xyz ) for ( k, a ) in atom_renamed_to.items() ),
        occ = destination.atoms()[0].occ,
        )

      for adata in updated_atom_data:
        corresponding = atom_renamed_to[ adata.name ]

        if corresponding in new_xyz_for:
          continue

        new_xyz_for[ corresponding ] = adata.xyz

      final_name_for = {}

      for adata in new_atom_data:
        name = adata.identifier()

        try:
          dist = distancer.mainchain_distance_for[ name ]

        except KeyError:
          name = None

        else:
          if dist is None or pruner( distance = dist ):
            name = None

        assert adata not in final_name_for
        final_name_for[ adata ] = name

      new_atoms_for[ destination ] = final_name_for

    rename_data_for = {}

    for ag in residue_group.atom_groups():
      rename_data_for[ ag ] = RenameData(
        resname = resname_for[ ag ],
        existing_atom_remapped_name_for = dict(
          ( a, new_name_for[ a ] ) for a in ag.atoms()
          ),
        existing_atom_remapped_xyz_for = dict(
          ( a, new_xyz_for.get( a, a.xyz ) ) for a in ag.atoms()
          ),
        new_atom_remapped_name_for = new_atoms_for.get( ag, {} ),
        )

    return rename_data_for


  @classmethod
  def Target(cls, mapping, completion, gapname):

    return cls(
      mapping = mapping,
      completion = completion,
      namer = target,
      gapname = gapname,
      )


  @classmethod
  def TargetPtm(cls, mapping, completion, gapname):

    return cls(
      mapping = mapping,
      completion = completion,
      namer = target_ptm,
      gapname = gapname,
      )
