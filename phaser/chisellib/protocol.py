from __future__ import print_function

"""
Predefined Sculptor protocols
"""

class ProtocolError(Exception):
  """
  Module exception
  """


class Definition(object):
  """
  Holds protocol data
  """

  def __init__(self, protocols):

    self.singles = protocols
    self.combinations = {}


  def define_custom(self, name, indices):

    assert all( i < len( self.singles ) for i in indices )
    self.combinations[ name ] = indices


  def define_all(self, name = "all"):

    self.combinations[ name ] = range( 1, len( self.singles ) + 1 )


  def keys(self):

    return (
      [ str( i ) for i in range( 1, len( self.singles ) + 1 ) ]
      + sorted( self.combinations.keys() )
      )


  def get(self, keys):

    indices = []

    for k in keys:
      if k in self.combinations:
        indices.extend( self.combinations[ k ] )

      else:
        try:
          i = int( k )

        except ValueError:
          raise ProtocolError("Invalid key: %s" % k)

        if i <= 0 or len( self.singles ) < i:
          raise ProtocolError("Unknown key: %s" % k)

        indices.append( i )

    return [ ( i, self.singles[ i - 1 ] ) for i in sorted( indices ) ]


PROTEIN = Definition(
  protocols = [
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = original
      }

      completion = cbeta
    }
    """, #1
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = original
      }

      completion = cbeta
    }
    """, #2
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = similarity
      }

      bfactor {
        use = original
      }

      completion = cbeta
    }
    """, #3
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = similarity
      }

      bfactor {
        use = original
      }

      completion = cbeta
    }
    """, #4
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = similarity

        similarity {
          factor = -80
        }
      }

      completion = cbeta
    }
    """, #5
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = similarity

        similarity {
          factor = -80
        }
      }

      completion = cbeta
    }
    """, #6
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = asa

        asa {
          factor = 12
        }
      }

      completion = cbeta
    }
    """, #7
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = asa

        asa {
          factor = 12
        }
      }

      completion = cbeta
    }
    """, #8
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = similarity+asa

        asa {
          factor = 8
        }

        similarity {
          factor = -60
        }
      }

      completion = cbeta
    }
    """, #9
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = similarity+asa

        asa {
          factor = 8
        }

        similarity {
          factor = -60
        }
      }

      completion = cbeta
    }
    """, #10
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = gap
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = similarity
      }

      bfactor {
        use = similarity+asa

        asa {
          factor = 8
        }

        similarity {
          factor = -60
        }
      }

      completion = cbeta
    }
    """, #11
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = completeness_based_similarity
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = similarity
      }

      bfactor {
        use = similarity+asa

        asa {
          factor = 8
        }

        similarity {
          factor = -60
        }
      }

      completion = cbeta
    }
    """, #12
    """
    sculpt.protein {
      mainchain {
        deletion {
          use = threshold_based_similarity

          threshold_based_similarity {
            threshold = -0.2
            similarity_calculation {
              matrix = blosum62

              smoothing {
                use = linear

                linear {
                  window = 3
                  weighting = triangular
                }
              }
            }
          }
        }

        polishing {
          use = None
        }
      }

      pruning {
        use = schwarzenbacher
      }

      bfactor {
        use = original
      }

      completion = cbeta
    }
    """,
    ],
  )

PROTEIN.define_all()
PROTEIN.define_custom( name = "minimal", indices = ( 0, 7, 11 ) )
