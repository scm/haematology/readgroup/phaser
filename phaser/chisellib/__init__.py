from __future__ import print_function

from functools import reduce

# Module exceptions
class ChisellibError(Exception):
  """
  Module exception
  """

class AlignmentError(ChisellibError):
  """
  Raised when an alignment is requested, but none is available
  """

class InvalidValueError(ChisellibError):
  """
  Raised when the submitted value has the wrong format and cannot be corrected
  """

class HierarchyError(ChisellibError):
  """
  Raised when there is an error in parent-child relationship
  """


# Chain sample data
class NoAlignment(object):
  """
  No alignment
  """

  @staticmethod
  def target_sequence():
    raise AlignmentError("No alignment available")


  @staticmethod
  def model_sequence():
    raise AlignmentError("No alignment available")


  @staticmethod
  def residue_sequence():
    raise AlignmentError("No alignment available")


  @staticmethod
  def rescode_sequence():
    raise AlignmentError("No alignment available")


  @staticmethod
  def alignment(gap = "-"):
    raise AlignmentError("No alignment available")


  @staticmethod
  def chain_alignment(gap = "-"):
    raise AlignmentError("No alignment available")


  @staticmethod
  def alignment_identity_fraction():

    return None


  @staticmethod
  def chain_alignment_identity_fraction():

    return None


  @staticmethod
  def aligned_residues():

    return []


class ChainAlignment(object):
  """
  A chain aligned with an alignment
  """

  def __init__(self, target, model, residues, rescodes):

    self.positions = len( target )
    assert self.positions == len( model )
    assert self.positions == len( residues )
    assert self.positions == len( rescodes )

    self.target = target
    self.model = model
    self.residues = residues
    self.rescodes = rescodes


  def target_sequence(self):

    return self.target


  def model_sequence(self):

    return self.model


  def residue_sequence(self):

    return self.residues


  def rescode_sequence(self):

    return self.rescodes


  def alignment(self, gap = "-"):

    from iotbx import bioinformatics
    return bioinformatics.clustal_alignment(
      names = [ "target", "model" ],
      alignments = [
        self.format( sequence = self.target_sequence(), gap = gap ),
        self.format( sequence = self.model_sequence(), gap = gap ),
        ],
      gap = gap,
      )


  def chain_alignment(self, gap = "-"):

    from iotbx import bioinformatics
    return bioinformatics.clustal_alignment(
      names = [ "model", "chain" ],
      alignments = [
        self.format( sequence = self.model_sequence(), gap = gap ),
        self.format( sequence = self.rescode_sequence(), gap = gap ),
        ],
      gap = gap,
      )


  def alignment_identity_fraction(self):

    return self.alignment().identity_fraction()


  def chain_alignment_identity_fraction(self):

    return self.chain_alignment().identity_fraction()


  def cumulative_identity_fraction(self):

    return self.alignment_identity_fraction() * self.chain_alignment_identity_fraction()


  def aligned_residues(self):

    return [ r for r in self.residues if r is not None ]


  @staticmethod
  def format(sequence, gap):

    return "".join( c if c is not None else gap for c in sequence )


class Sample(object):
  """
  A chain to be modified
  """

  def __init__(self, name, chain, mmtype, chainalignment, errors = None, superposition_errors = None):

    assert set( chainalignment.aligned_residues() ) <= set( chain.residue_groups() )

    if errors is None:
      errors = [ None ] * len( chain.residue_groups() )

    if superposition_errors is None:
      superposition_errors = [ None ] * len( chain.residue_groups() )

    assert len( chain.residue_groups() ) == len( errors )
    self.name = name
    self.chain = chain
    self.mmtype = mmtype
    self.chainalignment = chainalignment
    self.errors = errors
    self.superposition_errors = superposition_errors

    self.cache = {}

    for a in self.chain.atoms():
      a.set_chemical_element_simple_if_necessary()


  def accessible_surface_area(self, radius, precision):

    key = ( "asa", radius, precision )

    try:
      return self.cache[ key ]

    except KeyError:
      pass

    from mmtbx.geometry import asa
    calc = asa.calculate(
      atoms = self.chain.atoms(),
      probe = radius,
      precision = precision,
      )
    data = calc.areas
    self.cache[ key ] = data

    return data


  def sequence_similarity(self, matrix):

    key = ( "similarity", matrix )

    try:
      return self.cache[ key ]

    except KeyError:
      pass

    from phaser import residue_substitution_new as residue_substitution
    scale = matrix.scale()
    base = matrix.base()

    scores = residue_substitution.sequence_similarity_scores_for(
      alignment = self.chainalignment.alignment(),
      matrix = matrix,
      unknown = base,
      )

    data = [ ( value - base ) / scale for value in scores ]
    self.cache[ key ] = data

    return data


  def residues(self):

    return self.chain.residue_groups()


  def residue_errors(self):

    return self.errors


  def residue_superposition_errors(self):

    return self.superposition_errors


  def aligned_residues(self):

    return self.chainalignment.aligned_residues()


  def unaligned_residues(self):

    aligneds = set( self.aligned_residues() )
    return [ rg for rg in self.residues() if rg not in aligneds ]


class ModelInfo(object):
  """
  Holds some generic information about the structure, and also processings
  """

  def __init__(self, chains):

    self.chains = chains

    from mmtbx.geometry import shared_types
    self.base = shared_types.calculate_base_for_coordinates( xyzs = self.xyzs() )
    self.changes_for = {}


  def xyzs(self):

    from scitbx.array_family import flex
    total = flex.vec3_double()

    for c in self.chains:
      total.extend( c.atoms().extract_xyz() )

    return total


  def hash_indexer(self, step):

    from mmtbx.geometry import indexing
    from mmtbx.geometry import shared_types
    return indexing.hash(
      voxelizer = shared_types.voxelizer( base = self.base, step = ( step, ) * 3 ),
      margin = 1,
      )


  def register_change(self, chaindiff):

    assert chaindiff.chain in self.chains
    self.changes_for[ chaindiff.chain ] = chaindiff


  def lookup_change(self, chain):

    assert chain in self.chains
    return self.changes_for[chain]


  def list_changes(self):

    return list(self.changes_for.keys())


# Types to facilitate equality comparisons
class SmallString(object):
  """
  Checks that the assigned value has the right length
  """

  def __init__(self, length):

    self.length = length


  def __call__(self, value):

    if len( value ) != self.length:
      raise InvalidValueError("len( '%s' ) != %s" % (value, self.length))

    return value


class RoundedFloat(object):
  """
  Rounds float numbers to required precision
  """

  def __init__(self, ndigits):

    self.ndigits = ndigits


  def __call__(self, value):

    return round( value, self.ndigits )


class Tuple(object):
  """
  Standardizes a tuple
  """

  def __init__(self, formats):

    self.formats = formats


  def __call__(self, value):

    if len( value ) != len( self.formats ):
      raise InvalidValueError("len( %s ) != %s" % (value, len(self.formats)))

    return tuple( f( v ) for ( f, v ) in zip( self.formats, value ) )


# Data structures storing modified properties, and also equality comparisions
class AtomProperty(object):
  """
  Properties in iotbx.pdb.hierarchy.atom
  """

  FORMATTER_NAME = SmallString( length = 4 )
  FORMATTER_ELEMENT = SmallString( length = 2 )
  FORMATTER_XYZ = Tuple( formats = [ RoundedFloat( ndigits = 3 ) ] * 3 )
  FORMATTER_B = RoundedFloat( ndigits = 2 )
  FORMATTER_OCC = FORMATTER_B

  def __init__(self, name, element, hetero, xyz, b, occ, uij_presence):

    self.name = name
    self.element = element
    self.hetero = hetero
    self.xyz = xyz
    self.b = b
    self.occ = occ
    self.uij_presence = uij_presence


  @property
  def name(self):

    return self._name


  @name.setter
  def name(self, value):

    self._name = self.FORMATTER_NAME( value )


  @property
  def element(self):

    return self._element


  @element.setter
  def element(self, value):

    self._element = self.FORMATTER_ELEMENT( value )


  @property
  def hetero(self):

    return self._hetero


  @hetero.setter
  def hetero(self, value):

    self._hetero = bool( value )


  @property
  def xyz(self):

    return self._xyz


  @xyz.setter
  def xyz(self, value):

    self._xyz = self.FORMATTER_XYZ( value )


  @property
  def b(self):

    return self._b


  @b.setter
  def b(self, value):

    self._b = self.FORMATTER_B( value )


  @property
  def occ(self):

    return self._occ


  @occ.setter
  def occ(self, value):

    self._occ = self.FORMATTER_OCC( value )


  @property
  def uij_presence(self):

    return self._uij_presence


  @uij_presence.setter
  def uij_presence(self, value):

    self._uij_presence = bool( value )


  def as_iotbx_atom(self):

    import iotbx.pdb

    new_atom = iotbx.pdb.hierarchy.atom()
    new_atom.set_name( self.name )
    new_atom.set_element( self.element )
    new_atom.set_xyz( self.xyz )
    new_atom.set_b( float( self.b ) )
    new_atom.set_hetero( self.hetero )
    new_atom.set_occ( float( self.occ ) )
    new_atom.set_segid( "    " )

    return new_atom


  def identifier(self):

    from phaser import identity
    return identity.Atom( name = self.name.strip(), element = self.element.strip() )


  def modifications_to(self, atom):

    if self.name != atom.name:
      yield atom_name_setter( name = self.name )

    if self.element != atom.element:
      yield atom_element_setter( element = self.element )

    if self.hetero != atom.hetero:
      yield atom_hetero_setter( hetero = self.hetero )

    if self.xyz != self.FORMATTER_XYZ( atom.xyz ):
      yield atom_xyz_setter( xyz = self.xyz )

    if self.b != self.FORMATTER_B( atom.b ):
      yield atom_b_setter( b = self.b )

    if self.occ != self.FORMATTER_OCC( atom.occ ):
      yield atom_occ_setter( occ = self.occ )

    if atom.uij_is_defined() and not self.uij_presence:
      yield atom_uij_unsetter


  def structural_descriptor(self):

    return (
      atom_element_setter( element = self.element ).structural_change_descriptor(),
      atom_xyz_setter( xyz = self.xyz ).structural_change_descriptor(),
      atom_b_setter( b = self.b ).structural_change_descriptor(),
      atom_occ_setter( occ = self.occ ).structural_change_descriptor(),
      )


  @classmethod
  def from_iotbx_atom(cls, atom):

    return cls(
      name = atom.name,
      element = atom.element,
      hetero = atom.hetero,
      xyz = atom.xyz,
      b = atom.b,
      occ = atom.occ,
      uij_presence = atom.uij_is_defined(),
      )


class AtomDiff(object):
  """
  Parallel of iotbx.pdb.hierarchy.atom
  """

  def __init__(self, atom):

    self.atom = atom
    self.property = AtomProperty.from_iotbx_atom( atom = self.atom )
    self.marked_for_removal = False


  def mark_for_removal(self):

    self.marked_for_removal = True


  def is_marked_for_removal(self):

    return self.marked_for_removal


  def is_moribund(self):

    return self.marked_for_removal


  def modifications(self):

    for modification in self.property.modifications_to( atom = self.atom ):
      yield modification


  def entity(self):

    return self.atom


  def __str__(self):

    return "atom '%s'" % self.atom.name


  @staticmethod
  def corresponding_delete_instruction(explanation):

    return atom_deleter( explanation = explanation )


class AtomGroupProperty(object):
  """
  Properties in iotbx.pdb.hierarchy.atom_group
  """

  FORMATTER_RESNAME = SmallString( length = 3 )

  def __init__(self, resname):

    self.resname = resname


  @property
  def resname(self):

    return self._resname


  @resname.setter
  def resname(self, value):

    self._resname = self.FORMATTER_RESNAME( value )


  def modifications_to(self, atom_group):

    if self.resname != atom_group.resname:
      yield atom_group_resname_setter( resname = self.resname )


  @classmethod
  def from_iotbx_atom_group(cls, atom_group):

    return cls( resname = atom_group.resname )


class AtomGroupDiff(object):
  """
  Parallel of iotbx.pdb.hierarchy.atom_group
  """

  FORMATTER_RESNAME = SmallString( length = 3 )

  def __init__(self, atom_group):

    self.atom_group = atom_group
    self.property = AtomGroupProperty.from_iotbx_atom_group(
      atom_group = self.atom_group,
      )
    self.marked_for_removal = False

    from collections import OrderedDict

    self.atom_diff_for = OrderedDict(
      ( a, AtomDiff( atom = a ) ) for a in self.atom_group.atoms()
      )
    assert self.atom_diff_for

    self.atoms_to_append = []


  def children(self):

    return self.atom_diff_for.keys()


  def get_diff_for(self, atom):

    return self.atom_diff_for[ atom ]


  def mark_for_removal(self):

    self.marked_for_removal = True


  def is_marked_for_removal(self):

    return self.marked_for_removal


  def is_moribund(self):

    return (
      self.marked_for_removal
      or (
        not self.atoms_to_append
        and all( a.is_moribund() for a in self.atom_diff_for.values() )
        )
      )


  def add_data_for_atom_to_append(self, data):

    self.atoms_to_append.append( data )


  def modifications(self):

    for modification in self.property.modifications_to( atom_group = self.atom_group ):
      yield modification

    bvals = []

    for adiff in self.atom_diff_for.values():
      bvals.append( adiff.property.b )

    assert bvals
    maxb = max( bvals )

    for adata in self.atoms_to_append:
      yield atom_group_atom_attacher( atom_data = adata, bfac = maxb )


  def existing_atoms_remaining(self):

    return ( a for ( a, mb ) in self.atom_moribund_status() if not mb )


  def existing_atoms_disappearing(self):

    return ( a for ( a, mb ) in self.atom_moribund_status() if mb )


  def entity(self):

    return self.atom_group


  def __str__(self):

    return "atom_group '%s', altloc '%s'" % (
      self.atom_group.resname,
      self.atom_group.altloc,
      )


  @staticmethod
  def corresponding_delete_instruction(explanation):

    return atom_group_deleter( explanation = explanation )


  # Internal
  def atom_moribund_status(self):

    return (
      ( a, self.atom_diff_for[ a ].is_moribund() ) for a in self.atom_group.atoms()
      )


class ResidueGroupProperty(object):
  """
  Parallel of iotbx.pdb.hierarchy.residue_group
  """

  FORMATTER_ICODE = SmallString( length = 1 )

  def __init__(self, resseq_as_int, icode):

    self.resseq_as_int = resseq_as_int
    self.icode = icode


  @property
  def resseq_as_int(self):

    return self._resseq_as_int


  @resseq_as_int.setter
  def resseq_as_int(self, value):

    self._resseq_as_int = int( value )


  @property
  def icode(self):

    return self._icode


  @icode.setter
  def icode(self, value):

    self._icode = self.FORMATTER_ICODE( value )


  def modifications_to(self, residue_group):

    if self.resseq_as_int != residue_group.resseq_as_int():
      yield residue_group_resseq_setter( resseq_as_int = self.resseq_as_int )

    if self.icode != residue_group.icode:
      yield residue_group_icode_setter( icode = self.icode )


  @classmethod
  def from_iotbx_residue_group(cls, residue_group):

    return cls(
      resseq_as_int = residue_group.resseq_as_int(),
      icode = residue_group.icode,
      )


class ResidueGroupDiff(object):
  """
  Parallel of iotbx.pdb.hierarchy.residue_group
  """

  FORMATTER_ICODE = SmallString( length = 1 )

  def __init__(self, residue_group):

    self.residue_group = residue_group
    self.property = ResidueGroupProperty.from_iotbx_residue_group(
      residue_group = self.residue_group,
      )
    self.marked_for_removal = False

    from collections import OrderedDict

    self.atom_group_diff_for = OrderedDict(
      ( ag, AtomGroupDiff( atom_group = ag ) )
      for ag in self.residue_group.atom_groups()
      )

    self.atom_groups_to_remove = set()


  def children(self):

    return self.atom_group_diff_for.keys()


  def get_diff_for(self, atom_group):

    return self.atom_group_diff_for[ atom_group ]


  def mark_for_removal(self):

    self.marked_for_removal = True


  def is_marked_for_removal(self):

    return self.marked_for_removal


  def is_moribund(self):

    return (
      self.marked_for_removal
      or all( ag.is_moribund() for ag in self.atom_group_diff_for.values() )
      )


  def modifications(self):

    for modification in self.property.modifications_to( residue_group = self.residue_group ):
      yield modification


  def existing_atom_groups_remaining(self):

    return ( ag for ( ag, mb ) in self.atom_group_moribund_status() if not mb )


  def existing_atom_groups_disappearing(self):

    return ( ag for ( ag, mb ) in self.atom_group_moribund_status() if mb )


  def entity(self):

    return self.residue_group


  def __str__(self):

    return "residue_group '%s'" % self.residue_group.resid()


  @staticmethod
  def corresponding_delete_instruction(explanation):

    return residue_group_deleter( explanation = explanation )


  # Internal
  def atom_group_moribund_status(self):

    return (
      ( ag, self.atom_group_diff_for[ ag ].is_moribund() )
      for ag in self.residue_group.atom_groups()
      )


class ChainDiff(object):
  """
  Parallel of iotbx.pdb.hierarchy.chain
  """

  def __init__(self, chain):

    self.chain = chain

    from collections import OrderedDict

    self.residue_group_diff_for = OrderedDict(
      ( rg, ResidueGroupDiff( residue_group = rg ) )
      for rg in self.chain.residue_groups()
      )
    self.marked_for_removal = False


  def children(self):

    return self.residue_group_diff_for.keys()


  def get_diff_for(self, residue_group):

    return self.residue_group_diff_for[ residue_group ]


  def mark_for_removal(self):

    self.marked_for_removal = True


  def is_marked_for_removal(self):

    return self.marked_for_removal


  def is_moribund(self):

    return (
      self.marked_for_removal
      or all( rg.is_moribund() for rg in self.residue_group_diff_for.values() )
      )


  def modifications(self):

    return ( m for m in () )


  def existing_residue_groups_remaining(self):

    return ( rg for ( rg, mb ) in self.residue_group_moribund_status() if not mb )


  def existing_residue_groups_disappearing(self):

    return ( rg for ( rg, mb ) in self.residue_group_moribund_status() if mb )


  def entity(self):

    return self.chain


  def __str__(self):

    return "chain '%s'" % self.chain.id


  @staticmethod
  def corresponding_delete_instruction(explanation):

    return chain_deleter( explanation = explanation )


  # Internal
  def residue_group_moribund_status(self):

    return (
      ( rg, self.residue_group_diff_for[ rg ].is_moribund() )
      for rg in self.chain.residue_groups()
      )


#
# Modification instructions
#

# atom
class atom_deleter(object):

  def __init__(self, explanation = ""):

    self.explanation = explanation


  def __call__(self, atom):

    atom_group = atom.parent()
    atom_group.remove_atom( atom )


  def __str__(self):

    if not self.explanation:
      return "delete"

    else:
      return "delete (%s)" % self.explanation


  def structural_change_descriptor(self):

    return self.__class__


  @staticmethod
  def is_structural_change():

    return True


class atom_name_setter(object):

  def __init__(self, name):

    self.name = name


  def __call__(self, atom):

    atom.set_name( self.name )


  def __str__(self):

    return "set name to '%s'" % self.name


  @staticmethod
  def is_structural_change():

    return False


class atom_element_setter(object):

  def __init__(self, element):

    self.element = element


  def __call__(self, atom):

    atom.set_element( self.element )


  def __str__(self):

    return "set element to '%s'" % self.element


  def structural_change_descriptor(self):

    return ( self.__class__, self.element )


  @staticmethod
  def is_structural_change():

    return True


class atom_hetero_setter(object):

  def __init__(self, hetero):

    self.hetero = hetero


  def __call__(self, atom):

    atom.set_hetero( self.hetero )


  def __str__(self):

    return "%s the hetero flag" % ( "set" if self.hetero else "unset" )


  @staticmethod
  def is_structural_change():

    return False


class atom_xyz_setter(object):

  def __init__(self, xyz):

    self.xyz = xyz


  def __call__(self, atom):

    atom.set_xyz( self.xyz )


  def __str__(self):

    return "set coordinates to %s" % str( self.xyz )


  def structural_change_descriptor(self):

    return ( self.__class__, self.xyz )


  @staticmethod
  def is_structural_change():

    return True


class atom_b_setter(object):

  def __init__(self, b):

    self.b = b


  def __call__(self, atom):

    atom.set_b( self.b )


  def __str__(self):

    return "set b-factor to %s" % self.b


  def structural_change_descriptor(self):

    return ( self.__class__, self.b )


  @staticmethod
  def is_structural_change():

    return True


class atom_occ_setter(object):

  def __init__(self, occ):

    self.occ = occ


  def __call__(self, atom):

    atom.set_occ( self.occ )


  def __str__(self):

    return "set occupancy to %s" % self.occ


  def structural_change_descriptor(self):

    return ( self.__class__, self.occ )


  @staticmethod
  def is_structural_change():

    return True


class atom_uij_unsetter(object):

  def __call__(self, atom):

    atom.uij_erase()


  def __str__(self):

    return "reset anisotropic b-factor to isotropic"


  def structural_change_descriptor(self):

    return self.__class__


  @staticmethod
  def is_structural_change():

    return True


# atom_group
class atom_group_deleter(object):

  def __init__(self, explanation = ""):

    self.explanation = explanation


  def __call__(self, atom_group):

    residue_group = atom_group.parent()
    residue_group.remove_atom_group( atom_group )


  def __str__(self):

    if not self.explanation:
      return "delete"

    else:
      return "delete (%s)" % self.explanation


  def structural_change_descriptor(self):

    return self.__class__


  @staticmethod
  def is_structural_change():

    return True


class atom_group_resname_setter(object):

  def __init__(self, resname):

    self.resname = resname


  def __call__(self, atom_group):

    atom_group.resname = self.resname


  def __str__(self):

    return "set resname to '%s'" % self.resname


  @staticmethod
  def is_structural_change():

    return False


class atom_group_atom_attacher(object):

  def __init__(self, atom_data, bfac):

    self.atom_data = atom_data
    self.bfac = bfac


  def __call__(self, atom_group):

    atom = self.atom_data.as_iotbx_atom()
    atom.set_b( self.bfac )
    atom_group.append_atom( atom )


  def __str__(self):

    return "attach new atom '%s' ( coords = %s )" % (
      self.atom_data.name,
      self.atom_data.xyz,
      )


  def structural_change_descriptor(self):

    return ( self.__class__, self.atom_data.structural_descriptor(), self.bfac )


  @staticmethod
  def is_structural_change():

    return True

# residue_group
class residue_group_deleter(object):

  def __init__(self, explanation = ""):

    self.explanation = explanation


  def __call__(self, residue_group):

    chain = residue_group.parent()
    chain.remove_residue_group( residue_group )


  def __str__(self):

    if not self.explanation:
      return "delete"

    else:
      return "delete (%s)" % self.explanation


  def structural_change_descriptor(self):

    return self.__class__


  @staticmethod
  def is_structural_change():

    return True


class residue_group_resseq_setter(object):

  def __init__(self, resseq_as_int):

    self.resseq_as_int = resseq_as_int


  def __call__(self, residue_group):

    residue_group.resseq = self.resseq_as_int


  def __str__(self):

    return "set resseq to '%s'" % self.resseq_as_int


  @staticmethod
  def is_structural_change():

    return False


class residue_group_icode_setter(object):

  def __init__(self, icode):

    self.icode = icode


  def __call__(self, residue_group):

    residue_group.icode = self.icode


  def __str__(self):

    return "set icode to '%s'" % self.icode


  @staticmethod
  def is_structural_change():

    return False


# chain
class chain_deleter(object):

  def __init__(self, explanation):

    self.explanation = explanation


  def __call__(self, chain):

    model = chain.parent()
    model.remove_chain( chain = chain )


  def __str__(self):

    if not self.explanation:
      return "delete"

    else:
      return "delete (%s)" % self.explanation


  def structural_change_descriptor(self):

    return self.__class__


  @staticmethod
  def is_structural_change():

    return True


#
# Sculptor components
#

# Mainchain
class Mainchain(object):
  """
  Process mainchain
  """

  def __init__(self, deletions, polishings, remove_unaligned):

    self.deletions = deletions
    self.polishings = polishings
    self.remove_unaligned = remove_unaligned


  def __call__(self, sample, chaindiff, model_info):

    import operator
    moribunds = reduce(
      operator.or_,
      ( algorithm( sample = sample ) for algorithm in self.deletions ),
      set(),
      )

    # Polishing algorithms modify moribund residues sequentially
    for algorithm in self.polishings:
      algorithm( sample = sample, moribund = moribunds )

    if self.remove_unaligned:
      moribunds.update( sample.unaligned_residues() )

    for rg in moribunds:
      rgdiff = chaindiff.get_diff_for( residue_group = rg )
      rgdiff.mark_for_removal()


class Discard(object):
  """
  Non-alignment based sequence deletion, just keeps named residues
  """

  def __init__(self, keep):

    self.keep = set( keep )


  def __call__(self, sample, chaindiff, model_info):

    for rg in sample.chain.residue_groups():
      rgdiff = chaindiff.get_diff_for( residue_group = rg )

      for ag in rg.atom_groups():
        if ag.resname.strip() not in self.keep:
          agdiff = rgdiff.get_diff_for( atom_group = ag )
          agdiff.mark_for_removal()


class KeepAttached(object):
  """
  Non-alignment based sequence deletion, just keeps connected residues
  if connection point still exists
  """

  STEP = 2.0

  def __init__(self, maxdepth, maxlength):

    self.maxdepth = maxdepth
    self.maxlength = maxlength


  def __call__(self, sample, chaindiff, model_info):

    modindexer = model_info.hash_indexer( step = self.STEP )

    for chain in model_info.list_changes():
      assert chain != sample.chain

      for atom in chain.atoms():
        modindexer.add( object = atom, position = atom.xyz )

    connectivity = self.to_connectivity_graph( chain = sample.chain )

    for seg in connectivity.connected_segments():
      attachments = set()

      for rg in seg:
        for ap in self.attachment_points( residue_group = rg, modindexer = modindexer ):
          a_ag = ap.parent()
          a_rg = a_ag.parent()
          a_chain = a_rg.parent()

          cdiff = model_info.lookup_change( chain = a_chain )
          assert cdiff is not None
          rgdiff = cdiff.get_diff_for( residue_group = a_rg )
          agdiff = rgdiff.get_diff_for( atom_group = a_ag )

          if ( all( not d.is_moribund() for d in [ cdiff, rgdiff, agdiff ] )
            and agdiff.atom_group.resname == agdiff.property.resname ):
            attachments.add( rg )

      if not attachments:
        distance_from = dict( ( rg, None ) for rg in seg )

      else:
        distances = [ connectivity.distances_from( atom = rg ) for rg in attachments ]
        distance_from = dict(
          ( rg, min( d_from[ rg ] for d_from in distances ) ) for rg in seg
          )

      for rg in seg:
        depth = distance_from[ rg ]

        if depth is None or self.maxdepth <= distance_from[ rg ]:
          myrgdiff = chaindiff.get_diff_for( residue_group = rg )
          myrgdiff.mark_for_removal()


  def to_connectivity_graph(self, chain):

    # Slight misuse of the Compound object
    from mmtbx.geometry import topology
    compound = topology.Compound.create()

    for rg in chain.residue_groups():
      compound.add_atom( atom = rg )

    from mmtbx.geometry import indexing
    from mmtbx.geometry import shared_types
    indexer = indexing.hash(
      voxelizer = shared_types.voxelizer(
        base = shared_types.calculate_base_for_coordinates(
          xyzs = chain.atoms().extract_xyz(),
          ),
        step = ( self.STEP, ) * 3,
        ),
      margin = 1,
      )

    for atom in chain.atoms():
      indexer.add( object = atom, position = atom.xyz )

    for rg in compound.atoms:
      containmentpred = indexing.containment_predicate.exclusion(
        selection = rg.atoms(),
        getter = indexing.direct,
        )

      for atom in rg.atoms():
        altlocpred = indexing.altloc_predicate.for_iotbx_atom( atom = atom )
        overlappred = indexing.overlap_predicate.for_iotbx_atom(
          centre = atom.xyz,
          distance = self.maxlength,
          )

        interacting = indexing.filter(
          range = indexer.close_to( centre = atom.xyz ),
          predicate = indexing.composite_predicate(
            conditions = [ containmentpred, altlocpred, overlappred ],
            ),
          )

        for other in interacting:
          parent = other.parent().parent()
          assert rg != parent
          compound.add_bond( left = rg, right = parent )

    return compound


  def attachment_points(self, residue_group, modindexer):

    from mmtbx.geometry import indexing
    aps = []

    for atom in residue_group.atoms():
      altlocpred = indexing.altloc_predicate.for_iotbx_atom( atom = atom )
      overlappred = indexing.overlap_predicate.for_iotbx_atom(
        centre = atom.xyz,
        distance = self.maxlength,
        )

      interacting = indexing.filter(
        range = modindexer.close_to( centre = atom.xyz ),
        predicate = indexing.composite_predicate(
          conditions = [ altlocpred, overlappred ],
          ),
        )

      aps.extend( interacting )

    return aps


# Bfactor
class Bfactor(object):
  """
  Calculate B-factors
  """

  def __init__(self, algorithms, minimum_b):

    assert algorithms
    self.algorithms = algorithms
    self.minimum_b = minimum_b


  def __call__(self, sample, chaindiff, model_info):

    individuals = [ p( sample = sample ) for p in self.algorithms ]
    atoms = sample.chain.atoms()
    length = len( atoms )
    assert all( length == len( i ) for i in individuals )

    cumulatives = [ sum( ind ) for ind in zip( *individuals ) ]
    assert len( cumulatives ) == length

    # Skipped if array is empty or self.minimum_b is None
    if any( b < self.minimum_b for b in cumulatives ):
      assert self.minimum_b is not None
      assert cumulatives
      shift = self.minimum_b - min( cumulatives )
      cumulatives = [ c + shift for c in cumulatives ]

    for ( atom, bfac ) in zip( atoms, cumulatives ):
      ag = atom.parent()
      rg = ag.parent()
      rgdiff = chaindiff.get_diff_for( residue_group = rg )
      agdiff = rgdiff.get_diff_for( atom_group = ag )
      atomdiff = agdiff.get_diff_for( atom = atom )
      atomdiff.property.b = bfac


# Renumber
class Renumber(object):
  """
  Uses the chosen algorithm to determine new resseq and resid
  """

  def __init__(self, algorithm):

    self.algorithm = algorithm


  def __call__(self, sample, chaindiff, model_info):

    resid_for = self.algorithm( sample = sample )

    for rg in sample.chain.residue_groups():
      if rg not in resid_for:
        continue

      rgdiff = chaindiff.get_diff_for( residue_group = rg )
      ( resseq, icode ) = resid_for[ rg ]
      rgdiff.property.resseq_as_int = resseq
      rgdiff.property.icode = icode


# Morphing - sidechain pruning
def no_pruning_limit(distance):

  return False


class PruningLimit(object):

  def __init__(self, limit):

    self.limit = limit


  def __call__(self, distance):

    return self.limit < distance


def get_pruner(limit):

  if limit is None:
    return no_pruning_limit

  else:
    return PruningLimit( limit = limit )


class SidechainPruning(object):
  """
  Process sidechains
  """

  def __init__(self, algorithm, pruning_level_unaligned):

    self.algorithm = algorithm
    self.pruning_level_unaligned = pruning_level_unaligned


  def __call__(self, sample):

    levels = self.algorithm( sample = sample )
    truncation_for = dict(
      ( rg, l ) for ( rg, l ) in zip( sample.chainalignment.residue_sequence(), levels )
      if rg is not None
      )

    pruner_for = {}

    for rg in sample.residues():
      limit = truncation_for.get( rg, self.pruning_level_unaligned )
      pruner_for [ rg ] = get_pruner( limit = limit )

    return pruner_for


# Morphing - sidechain mapping
class Morphing(object):
  """
  Includes sidechain pruning, remapping and completion
  """

  def __init__(self, pruning, rename):

    self.pruning = pruning
    self.rename = rename


  def restype_for(self, sample):

    three_letter_for = sample.mmtype.three_letter_for()
    unknown_three_letter = sample.mmtype.unknown_three_letter

    ca = sample.chainalignment
    restype_for = {}

    for ( rg, target ) in zip( ca.residue_sequence(), ca.target_sequence() ):
      if rg is None:
        continue

      if target is None:
        restype = None

      else:
        restype = three_letter_for.get( target, unknown_three_letter )

      restype_for[ rg ] = restype

    for rg in sample.unaligned_residues():
      restype_for[ rg ] = unknown_three_letter

    return restype_for


  def __call__(self, sample, chaindiff, model_info):

    from phaser import residue_topology_new as residue_topology
    pruner_for = self.pruning( sample = sample )
    restype_for = self.restype_for( sample = sample )

    for rg in sample.residues():
      rgdiff = chaindiff.get_diff_for( residue_group = rg )
      rename_data_for = self.rename(
        residue_group = rg,
        target = restype_for[ rg ],
        mmt = sample.mmtype,
        pruner = pruner_for[ rg ],
        )

      for ag in rg.atom_groups():
        agdiff = rgdiff.get_diff_for( atom_group = ag )
        rename_data = rename_data_for[ ag ]
        agdiff.property.resname = rename_data.resname

        added_new_atoms = False

        for ( adata, new_identity ) in rename_data.new_atom_remapped_name_for.items():
          if new_identity is None:
            continue

          adata.name = new_identity.pdb_name
          adata.element = new_identity.pdb_element

          agdiff.add_data_for_atom_to_append( data = adata )
          added_new_atoms = True

        for a in ag.atoms():
          adiff = agdiff.get_diff_for( atom = a )
          new_identity = rename_data.existing_atom_remapped_name_for[ a ]

          if new_identity is None:
            adiff.mark_for_removal()

          else:
            adiff.property.name = new_identity.pdb_name
            adiff.property.element = new_identity.pdb_element
            adiff.property.hetero = sample.mmtype.hetero

            if added_new_atoms:
              adiff.property.xyz = rename_data.existing_atom_remapped_xyz_for[ a ]


#
# Main sculptor class
#
class Sculptor(object):
  """
  Coordinates mainchain, sidechain and b-factor processing
  """

  def __init__(self, processors):

    self.processors = processors


  def __call__(self, sample, model_info):

    chaindiff = ChainDiff( chain = sample.chain )

    for proc in self.processors:
      proc( sample = sample, chaindiff = chaindiff, model_info = model_info )

    return chaindiff


def Discarder(sample, model_info):

  chaindiff = ChainDiff( chain = sample.chain )
  chaindiff.mark_for_removal()
  return chaindiff
