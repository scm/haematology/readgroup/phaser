from __future__ import print_function

class Original(object):
  """
  Algorithm that does not do anything
  """

  def __call__(self, sample):

    return {}


class Sequence(object):
  """
  Renumbers residue_groups according to sequence
  """

  def __init__(self, get_sequence, start):

    self.get_sequence = get_sequence
    self.start = start


  def __call__(self, sample):

    sequence = self.get_sequence( sample = sample )

    resseq = self.start - 1
    icode = " "

    resid_for = {}

    for ( rescode, rg ) in zip( sequence, sample.chainalignment.residue_sequence() ):
      if rescode is not None:
        resseq += 1
        icode = " "

      else:
        if icode == " " or icode == "Z":
          icode = "A"

        else:
          icode = chr( ord( icode ) + 1 )

      if rg is None:
        continue

      resid_for[ rg ] = ( resseq, icode )

    for ( i, rg ) in enumerate(
      sample.unaligned_residues(),
      start = ( ( resseq // 100 ) + 1 ) * 100 + 1,
      ):
      resid_for[ rg ] = ( i, " " )

    return resid_for


  @staticmethod
  def use_target_sequence(sample):

    return sample.chainalignment.target_sequence()


  @staticmethod
  def use_model_sequence(sample):

    return sample.chainalignment.model_sequence()


  @classmethod
  def Target(cls, start):

    return cls( get_sequence = cls.use_target_sequence, start = start )


  @classmethod
  def Model(cls, start):

    return cls( get_sequence = cls.use_model_sequence, start = start )
