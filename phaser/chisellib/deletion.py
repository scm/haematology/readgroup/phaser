from __future__ import print_function

class All(object):
  """
  Delete all residues
  """

  def __call__(self, sample):

    return set( sample.residues() )


class Gap(object):
  """
  Include residue if present in the search sequence
  """

  def __call__(self, sample):

    ca = sample.chainalignment
    return set(
      rg for ( rg, t ) in zip( ca.residue_sequence(), ca.target_sequence() )
      if t is None and rg is not None
      )


class ThresholdBasedSimilarity(object):
  """
  Include residue if the sequence similarity is higher than a threshold
  """

  def __init__(self, ss_calc, threshold):

    self.ss_calc = ss_calc
    self.threshold = threshold


  def __call__(self, sample):

    return set(
      rg for ( rg, s )
      in zip( sample.chainalignment.residue_sequence(), self.ss_calc( sample = sample ) )
      if s < self.threshold and rg is not None
      )


def find_threshold(scores, target):

  assert scores
  ordered = sorted( set( scores ) )
  previous = ( ordered[0], 0 )

  for threshold in ordered[1:]:
    current = ( threshold, sum( 1 for s in scores if s < threshold ) )

    if target < current[1]:
      assert previous[1] <= target
      break

    previous = current

  else:
    current = ( ordered[-1] + 0.01, len( scores ) )

  if target - previous[1] <= current[1] - target:
    return previous[0]

  else:
    return current[0]


class CompletenessBasedSimilarity(object):
  """
  Discards N residues with the lowest sequence similarity, where N is the number
  of gaps in the alignment
  """

  def __init__(self, ss_calc, offset):

    self.ss_calc = ss_calc
    self.offset = offset


  def __call__(self, sample):

    ca = sample.chainalignment
    gap_count = sum( 1 for p in ca.target_sequence() if p is None )
    model_count = sum(
      1 for p in zip( ca.target_sequence(), ca.residue_sequence() )
      if None not in p
      )
    target = gap_count - self.offset * model_count
    scores = list( self.ss_calc( sample = sample ) )
    threshold = find_threshold( scores = scores, target = target )

    return set(
      rg for ( rg, s ) in zip( ca.residue_sequence(), scores )
      if s < threshold and rg is not None
      )


class ResidueCountBasedSimilarity(object):
  """
  Count only positions for which residues are present
  """

  def __init__(self, ss_calc, offset):

    self.ss_calc = ss_calc
    self.offset = offset


  def __call__(self, sample):

    ca = sample.chainalignment
    seq = ca.target_sequence()
    rgs = ca.residue_sequence()
    gap_count = sum(
      1 for ( s, rg ) in zip( seq, rgs ) if s is None and rg is not None
      )
    model_count = sum( 1 for p in zip( seq, rgs ) if None not in p )
    target = gap_count - self.offset * model_count
    scores = list( self.ss_calc( sample = sample ) )
    assert len( scores ) == len( rgs )
    threshold = find_threshold(
      scores = [ s for ( s, rg ) in zip( scores, rgs ) if rg is not None ],
      target = target,
      )

    return set(
      rg for ( rg, s ) in zip( ca.residue_sequence(), scores )
      if s < threshold and rg is not None
      )


class RemoveLong(object):
  """
  Remove residues only if the deletion is long
  """

  def __init__(self, min_length):

    self.min_length = min_length


  def __call__(self, sample):

    from phaser import sequtil

    it = sequtil.split(
      sequence = zip( ca.target_sequence(), ca.residue_sequence() ),
      consecutivity = lambda left, right: (
        ( right[0] is None and left[0] is None )
        or ( right[0] is not None and left[0] is not None )
        )
      )

    moribund = set()

    for contiguous in it:
      assert contiguous

      if contiguous[0][0] is None and self.min_length <= len( contiguous ):
        moribund.update( rg for ( g, rg ) in contiguous if rg is not None )

    return moribund


class RMSError(object):
  """
  Remove residues if the associated error is larger than a cutoff

  Missing errors are treated as the maximum error encountered
  """

  def __init__(self, threshold, substitution):

    self.threshold = threshold
    self.substitution = substitution


  def __call__(self, sample):

    errors = self.substitution(
      sequence = sample.residue_errors(),
      all_none_substitute = 0.00,
      )

    moribunds = set()

    for ( rg, err ) in zip( sample.residues(), errors ):
      if self.threshold <= err:
        moribunds.add( rg )

    return moribunds


class SuperpositionError(object):
  """
  Remove residues if the associated superposition error is larger than a cutoff

  Missing errors are treated as the maximum error encountered
  """

  def __init__(self, threshold, substitution):

    self.threshold = threshold
    self.substitution = substitution


  def __call__(self, sample):

    errors = self.substitution(
      sequence = sample.residue_superposition_errors(),
      all_none_substitute = 0.00,
      )

    moribunds = set()

    for ( rg, err ) in zip( sample.residues(), errors ):
      if self.threshold <= err:
        moribunds.add( rg )

    return moribunds
