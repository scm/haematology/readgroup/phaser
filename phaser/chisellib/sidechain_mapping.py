from __future__ import print_function

from phaser import residue_topology_new as residue_topology

class TwoDimensional(object):
  """
  Maps sidechains based on chemical connectivity
  """

  def __init__(self, equality):

    self.equality = equality


  def get_source_topology(self, mmtype, resname, atoms):

    try:
      return mmtype.residue_sidechain_topology_from_idealized_residues( resname = resname )

    except KeyError:
      pass

    try:
      return mmtype.residue_sidechain_topology_from_library( resname = resname )

    except KeyError:
      pass

    geometry = residue_topology.geometry_from_structure(
      resname = resname,
      atoms = atoms,
      )
    geometry.discard_atoms(
      names = [ a for a in mmtype.mainchain_atoms
        if a != mmtype.sidechain_attachment_atom ],
      )
    return residue_topology.connectivity_from_bonds(
      bonds = geometry.bonds( tolerance = mmtype.bond_length_tolerance )
      )


  def get_target_topology(self, mmtype, target, source, source_topology):

    if target == mmtype.unknown_three_letter:
      return mmtype.unknown_residue_sidechain_topology()

    try:
      return mmtype.residue_sidechain_topology_from_idealized_residues( resname = target )

    except KeyError:
      pass

    try:
      return mmtype.residue_sidechain_topology_from_library( resname = target )

    except KeyError:
      pass

    if target == source:
      return source_topology

    return mmtype.unknown_residue_sidechain_topology()


  def __call__(self, mmtype, resname, atoms, target):

    # mmtype.mmtype.mainchains_atoms discarded from topologies
    top_source = self.get_source_topology(
      mmtype = mmtype,
      resname = resname,
      atoms = atoms,
      )
    top_target = self.get_target_topology(
      mmtype = mmtype,
      target = target,
      source = resname,
      source_topology = top_source,
      )
    saa = mmtype.sidechain_attachment_atom

    if saa in top_source.descriptor_for and saa in top_target.descriptor_for:
      unpruned_atommaps = residue_topology.match(
        left = top_source,
        right = top_target,
        prematched = [ ( saa, saa ) ],
        vertex_equality = self.equality,
        )

      atommaps = []

      for a_map in unpruned_atommaps:
        pruned = top_target.subset( atoms = [ r for ( l, r ) in a_map ] )
        connecteds = set( pruned.connected_segment_from( atom = saa ) )
        assert saa in connecteds
        atommaps.append( [ ( l, r ) for ( l, r ) in a_map if r in connecteds ] )

    else:
      atommaps = residue_topology.match(
        left = top_source,
        right = top_target,
        vertex_equality = self.equality,
        )

    completeds = []
    missings = [ a for a in mmtype.mainchain_atoms  if a != saa ]
    assert all( a not in top_source.descriptor_for for a in missings )
    assert all( a not in top_target.descriptor_for for a in missings )
    extras = [ ( a, a ) for a in missings ]

    for amap in atommaps:
      extended = amap + extras
      new_name_for = dict( ( l.pdb_name, r ) for ( l, r ) in extended )
      assert len( extended ) == len( new_name_for )
      completeds.append( [ new_name_for.get( atom.name ) for atom in atoms ] )

    return completeds


  @classmethod
  def ElementAware(cls):

    return cls( equality = lambda l, r: l.element == r.element )


  @classmethod
  def ElementAgnostic(cls):

    return cls( equality = lambda l, r: True )


class ThreeDimensional(object):
  """
  Maps sidechains based on geometry
  """

  def __init__(self, vertex_equality, tolerance, fine_sampling):

    self.vertex_equality = vertex_equality
    self.edge_equality = lambda l, r: abs( l - r ) < tolerance
    self.fine_sampling = fine_sampling


  def get_source_topology(self, mmtype, resname, atoms):

    geometry = residue_topology.geometry_from_structure(
      resname = resname,
      atoms = atoms,
      )
    geometry.discard_element( symbol = "H" )
    geometry.discard_atoms( names = mmtype.irrelevant_atoms_for_distance_topology )
    return geometry.distance_topology()


  def get_target_topology(self, mmtype, target, source, source_topology):

    if target == mmtype.unknown_three_letter:
      return mmtype.unknown_residue_rotamer_distance_topology()

    try:
      return mmtype.residue_rotamer_distance_topology_from_idealized_residues(
        resname = target,
        fine_sampling = self.fine_sampling,
        )

    except KeyError:
      pass

    try:
      return mmtype.residue_rotamer_distance_topology_from_library(
        resname = target,
        fine_sampling = self.fine_sampling,
        )

    except KeyError:
      pass

    if target == source:
      return { "current": source_topology }

    else:
      return mmtype.unknown_residue_rotamer_distance_topology()


  def __call__(self, mmtype, resname, atoms, target):

    # mmtype.irrelevant_atoms_for_distance_topology discarded from topologies
    top_source = self.get_source_topology(
      mmtype = mmtype,
      resname = resname,
      atoms = atoms,
      )
    top_target_rotamers = self.get_target_topology(
      mmtype = mmtype,
      target = target,
      source = resname,
      source_topology = top_source,
      )

    iadts = mmtype.irrelevant_atoms_for_distance_topology
    extras = [ ( a, a ) for a in iadts ]
    assert all( a not in top_source.descriptor_for for a in iadts )
    presents = [
      a for a in mmtype.mainchain_atoms if a in top_source.descriptor_for
      ]
    atommaps = []

    for top_target in top_target_rotamers.values():
      assert all( a not in top_target.descriptor_for for a in iadts )
      results = residue_topology.match(
        left = top_source,
        right = top_target,
        prematched = [ ( a, a ) for a in presents if a in top_target.descriptor_for ],
        vertex_equality = self.vertex_equality,
        edge_equality = self.edge_equality,
        )

      for amap in results:
        extended = amap + extras
        new_name_for = dict( ( l.pdb_name, r ) for ( l, r ) in extended )
        assert len( extended ) == len( new_name_for )
        atommaps.append( [ new_name_for.get( atom.name ) for atom in atoms ] )

    return atommaps


  @classmethod
  def ElementAware(cls, tolerance, fine_sampling):

    return cls(
      vertex_equality = lambda l, r: l.element == r.element,
      tolerance = tolerance,
      fine_sampling = fine_sampling,
      )


  @classmethod
  def ElementAgnostic(cls, tolerance, fine_sampling):

    return cls(
      vertex_equality = lambda l, r: True,
      tolerance = tolerance,
      fine_sampling = fine_sampling,
      )


class IdentityIfSame(object):
  """
  Use identity mapping if residues are the same, and mapping algorithm otherwise
  """

  def __init__(self, mapping):

    self.mapping = mapping


  def __call__(self, mmtype, resname, atoms, target):

    if resname == target:
      from phaser import identity
      identities = [ identity.Atom.from_iotbx_atom( atom = a ) for a in atoms ]
      return [
        [ ident if ident.element != "H" else None for ident in identities ],
        ]

    return self.mapping(
      mmtype = mmtype,
      resname = resname,
      atoms = atoms,
      target = target,
      )
