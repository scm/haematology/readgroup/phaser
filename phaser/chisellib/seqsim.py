from __future__ import print_function

class ResidueFrameLinearAveraged(object):
  """
  Sequence similarity with linear averaging
  """

  def __init__(self, matrix, weights):

    norm = sum( weights )
    assert 0 < norm
    self.matrix = matrix
    self.weights = [ w / norm for w in weights ]


  def __call__(self, sample):

    matrix = sample.mmtype.scoring_matrix_dict[ self.matrix ]

    from phaser import sequtil

    return sequtil.convolution(
      sequence = sample.sequence_similarity( matrix = matrix ),
      pattern = self.weights,
      padding = matrix.normalized_gap_penalty,
      )


class AtomFrame(object):
  """
  Transforms the residue-frame result to atom frame
  """

  def __init__(self, ssim):

    self.ssim = ssim


  def __call__(self, sample):

    scores = self.ssim( sample = sample )
    ssim_for = dict(
      ( rg, s ) for ( rg, s ) in zip( sample.chainalignment.residue_sequence(), scores )
      if rg is not None
      )

    matrix = sample.mmtype.scoring_matrix_dict[ self.ssim.matrix ]
    gapvalue = matrix.normalized_gap_penalty
    results = []

    for rg in sample.chain.residue_groups():
      results.extend( [ ssim_for.get( rg, gapvalue ) ] * len( rg.atoms() ) )

    return results


def hole_bleed(similarities, holes, pattern, sim_padding, hole_padding):

  assert len( similarities ) == len( holes )

  from phaser import sequtil

  bleeds = sequtil.convolution(
    sequence = [ ( s if h else 0 ) for ( h, s ) in zip( holes, similarities ) ],
    pattern = pattern,
    padding = sim_padding,
    )
  weights = sequtil.convolution(
    sequence = [ ( 1.0 if h else 0.0 ) for h in holes ],
    pattern = pattern,
    padding = hole_padding,
    )
  #assert len( bleeds ) == len( holes ) - bleed has no len
  #assert len( weights ) == len( holes ) - weights has no len
  norm = sum( pattern )
  return [
    wb * b + ( norm - wb ) * s for ( wb, b, s ) in zip( weights, bleeds, similarities )
    ]


def calculate_centre(chain):

  sites = chain.atoms().extract_xyz()

  if 0 < len( sites ):
    return sites.mean()

  else:
    return ( 0.0, 0.0, 0.0 )


def indexer_factory(centre, step, maxdist):

  from mmtbx.geometry.shared_types import voxelizer
  from mmtbx.geometry import indexing

  voxelizer = voxelizer( base = centre, step = ( step, step, step ) )

  import functools
  return functools.partial(
    indexing.hash,
    voxelizer = voxelizer,
    margin = int( maxdist // step ) + 1,
    )


def calculate_space_average(centre, simposis, maxdist):

  from scitbx.array_family import flex
  sites = flex.vec3_double()
  scores = flex.double()

  for sp in simposis:
    sites.append( sp.position )
    scores.append( sp.similarity )

  diffs = sites - centre
  distance_sqs = diffs.dot( diffs )

  selection = distance_sqs <= ( maxdist ** 2 )

  if True not in selection:
    return None

  weights = flex.exp(
    -flex.sqrt( distance_sqs.select( selection ) ) / ( maxdist / 4.0 )
    )
  norm = flex.sum( weights )
  return flex.sum( scores.select( selection ) * weights ) / norm


class SimilarityPosition(object):

  def __init__(self, similarity, position):

    self.similarity = similarity
    self.position = position


class ResidueFrameSpaceAveraged(object):
  """
  Sequence similarity averaged over space. Averaging is done over residue_groups
  """

  def __init__(self, matrix, maxdist, bleedpattern):

    assert 0 <= maxdist
    self.matrix = matrix
    self.maxdist = maxdist
    self.step = 5

    norm = sum( bleedpattern )
    assert 0 < norm
    self.bleedpattern = [ w / norm for w in bleedpattern ]


  def calculate_positions(self, aligned_residue_groups, mmtype):

    assert mmtype.sidechain_attachment_atom is not None
    from phaser.identity import Atom
    from scitbx.array_family import flex

    positions = []

    for rg in aligned_residue_groups:
      if not rg:
        positions.append( None )
        continue

      atoms_for = {}

      for a in rg.atoms():
        aid = Atom.from_iotbx_atom( atom = a )
        atoms_for.setdefault( aid, [] ).append( a.xyz )

      if mmtype.sidechain_attachment_atom in atoms_for:
        aposis = atoms_for[ mmtype.sidechain_attachment_atom ]
        assert 0 < len( aposis )
        positions.append( flex.vec3_double( aposis ).mean() )

      else:
        positions.append( None )

    return positions


  def index_positions(self, centre, similarities, positions):

    assert len( positions ) == len( similarities )
    indexer = indexer_factory( centre = centre, step = self.step, maxdist = self.maxdist )()

    for ( ssim, posi ) in zip( similarities, positions ):
      if posi is not None:
        indexer.add(
          object = SimilarityPosition( similarity = ssim, position = posi ),
          position = posi,
          )

    return indexer


  def calculate_averaged_value(self, position, similarity, indexer):

    if position is None:
      return similarity

    value = calculate_space_average(
      centre = position,
      simposis = indexer.close_to( centre = position ),
      maxdist = self.maxdist,
      )
    assert value is not None
    return value


  def __call__(self, sample):

    matrix = sample.mmtype.scoring_matrix_dict[ self.matrix ]
    seqsim = sample.sequence_similarity( matrix = matrix )

    if sample.mmtype.sidechain_attachment_atom is None:
      return seqsim

    positions = self.calculate_positions(
      aligned_residue_groups = sample.chainalignment.residue_sequence(),
      mmtype = sample.mmtype,
      )
    similarities = hole_bleed(
      similarities = seqsim,
      holes = [ ( p is None ) for p in positions ],
      pattern = self.bleedpattern,
      sim_padding = matrix.normalized_gap_penalty,
      hole_padding = 0,
      )
    indexer = self.index_positions(
      centre = calculate_centre( chain = sample.chain ),
      similarities = similarities,
      positions = positions,
      )

    results = []

    for ( ssim, posi ) in zip( similarities, positions ):
      results.append(
        self.calculate_averaged_value(
          position = posi,
          similarity = ssim,
          indexer = indexer,
          )
        )

    return results


class AtomFrameSpaceAveraged(object):
  """
  Sequence similarity averaged over space. Averaging is done over atoms
  """

  def __init__(self, matrix, maxdist, bleedpattern):

    assert 0 <= maxdist
    self.matrix = matrix
    self.maxdist = maxdist
    self.step = 5

    norm = sum( bleedpattern )
    assert 0 < norm
    self.bleedpattern = [ w / norm for w in bleedpattern ]


  def index_positions(self, centre, similarities, aligned_residue_groups):

    assert len( aligned_residue_groups ) == len( similarities )

    from mmtbx.geometry import altloc

    factory = indexer_factory( centre = centre, step = self.step, maxdist = self.maxdist )
    indexer = altloc.Indexer( factory = factory )
    inserter = altloc.Inserter( indexer = indexer )

    for ( rg, ssim ) in zip( aligned_residue_groups, similarities ):
      if rg is None:
        continue

      for ag in rg.atom_groups():
        for a in ag.atoms():
          desc = altloc.Description(
            data = SimilarityPosition(
              similarity = ssim,
              position = a.xyz,
              ),
            coordinates = a.xyz,
            altid = ag.altloc,
            )
          desc.accept( processor = inserter )

    return indexer


  def calculate_averaged_value(self, atom, indexer):

    from mmtbx.geometry import altloc
    aggregator = altloc.Aggregator( indexer = indexer )

    description = altloc.Description(
      data = None,
      coordinates = atom.xyz,
      altid = atom.parent().altloc,
      )
    description.accept( processor = aggregator )

    return calculate_space_average(
      centre = atom.xyz,
      simposis = aggregator.entities,
      maxdist = self.maxdist,
      )


  def __call__(self, sample):

    matrix = sample.mmtype.scoring_matrix_dict[ self.matrix ]
    gapvalue = matrix.normalized_gap_penalty
    similarities = hole_bleed(
      similarities = sample.sequence_similarity( matrix = matrix ),
      holes = [ ( rg is None ) for rg in sample.chainalignment.residue_sequence() ],
      pattern = self.bleedpattern,
      sim_padding = gapvalue,
      hole_padding = 0,
      )
    indexer = self.index_positions(
      centre = calculate_centre( chain = sample.chain ),
      similarities = similarities,
      aligned_residue_groups = sample.chainalignment.residue_sequence(),
      )

    results = []

    for atom in sample.chain.atoms():
      score = self.calculate_averaged_value( atom = atom, indexer = indexer )

      if score is not None:
        results.append( score )

      else:
        results.append( gapvalue )

    return results
