from __future__ import print_function

from phaser.logoutput_new import message as msg
from phaser.logoutput_new import scope
from phaser.logoutput_new import LogError


class Package(object):
  """
  A facade that allows all messages and scopes defined within the package
  """

  def __init__(self, channel, scope_check = True):

    self.channel = channel

    if scope_check:
      self.scope_check = self.scope_boundary_check

    else:
      self.scope_check = self.no_scope_boundary_check


  # Events
  def title(self, text):

    message = msg.Title( text = text )
    self.channel.event( message = message )


  def heading(self, text, level):

    message = msg.Heading( text = text, level = level )
    self.channel.event( message = message )


  def text(self, text):

    message = msg.Text( text = text )
    self.channel.event( message = message )


  def flush(self):

    self.channel.event( message = msg.Flush )


  def unformatted_text(self, text, indent = 0, alignment = None):

    message = msg.Paragraph( text = text, indent = indent, alignment = alignment )
    self.channel.event( message = message )


  def preformatted_text(self, text, indent = 0):

    message = msg.Document( text = text, indent = indent )
    self.channel.event( message = message )


  def field(self, name, value, formatter = str):

    message = msg.Field( name = name, value = value, formatter = formatter )
    self.channel.event( message = message )


  def field_float(self, name, value, digits):

    message = msg.Field.Float( name = name, value = value, digits = digits )
    self.channel.event( message = message )


  def field_scientific(self, name, value, digits):

    message = msg.Field.Scientific( name = name, value = value, digits = digits )
    self.channel.event( message = message )


  def field_percentage(self, name, value, digits):

    message = msg.Field.Percentage( name = name, value = value, digits = digits )
    self.channel.event( message = message )


  def field_sequence(self, name, value, formatter = str, separator = " "):

    message = msg.Field.Sequence(
      name = name,
      value = value,
      formatter = formatter,
      separator = separator,
      )
    self.channel.event( message = message )


  def highlight(self, text):

    message = msg.Highlight( text = text )
    self.channel.event( message = message )


  def separator(self):

    self.channel.event( message = msg.Separator )


  def task_begin(self, text):

    self.text( text = text + "..." )
    self.flush()


  def task_end(self, text = "done"):

    message = msg.Text( text = text, linefeed = True )
    self.channel.event( message = message )


  def task_failure(self, text):

    message = msg.Text( text = "failed", linefeed = True )
    self.channel.event( message = message )
    message = msg.Paragraph( text = text )
    self.channel.event( message = message )


  def exception(self, instance):

    message = msg.Exception( instance = instance )
    self.channel.event( message = message )


  def traceback(self, exc_type, exc_value, traceback):

    message = msg.Traceback(
      exc_type = exc_type,
      exc_value = exc_value,
      traceback = traceback,
      )
    self.channel.event( message = message )


  def alignment(self, alignment, indent = 0):

    message = msg.Alignment( alignment = alignment, indent = indent )
    self.channel.event( message = message )


  def sequence(self, sequence, indent = 0):

    message = msg.Sequence( sequence = sequence, indent = indent )
    self.channel.event( message = message )


  # Scopes
  def paragraph_begin(self, indent = 0):

    self.channel.enter( scope = scope.Block( indent = indent ) )


  def paragraph_end(self):

    self.scope_check( expected = scope.Block, got = self.channel.exit() )


  def ordered_list_begin(self, marker = None, start = 1, separate_items = True):

    self.channel.enter(
      scope = scope.OrderedList( marker = marker, start = start, separate_items = separate_items )
      )


  def unordered_list_begin(self, level = 1, separate_items = True):

    self.channel.enter(
      scope = scope.UnorderedList( level = level, separate_items = separate_items )
      )


  def list_end(self):

    self.scope_check(
      expected = ( scope.OrderedList, scope.UnorderedList ),
      got = self.channel.exit(),
      )


  def list_item_begin(self, text):

    self.channel.enter( scope = scope.ListItem( text = text ) )


  def list_item_end(self):

    self.scope_check( expected = scope.ListItem, got = self.channel.exit() )


  def list_item(self, text):

    self.list_item_begin( text = text )
    self.list_item_end()


  def table_start_with_header(self, headings, formatters = None, widths = None, style = None):

    table = scope.HeadedTable(
      headings = headings,
      formatters = formatters,
      widths = widths,
      style = style,
      )

    self.channel.enter( scope = table )


  def table_start_without_header(self, cells, formatters = None, widths = None, style = None):

    table = scope.HeadlessTable(
      cells = cells,
      formatters = formatters,
      widths = widths,
      style = style,
      )

    self.channel.enter( scope = table )


  def table_row(self, cells):

    message = msg.TableRow( cells = cells )
    self.channel.event( message = message )


  def table_end(self):

    self.scope_check(
      expected = ( scope.HeadedTable, scope.HeadlessTable ),
      got = self.channel.exit(),
      )


  def table(
    self,
    body,
    headings = None,
    widths = None,
    rowheadings = 0,
    missing = "",
    style = None,
    ):

    from phaser.logoutput_new import data
    table = data.Table(
      data = body,
      names = headings,
      widths = widths,
      rowheadings = rowheadings,
      missing = missing,
      )
    message = msg.Table( table = table, style = style )
    self.channel.event( message = message )


  def table_rows(
    self,
    rows,
    formatters = None,
    headings = None,
    widths = None,
    rowheadings = 0,
    missing = "",
    style = None,
    ):

    from phaser.logoutput_new import data
    self.table(
      body = data.TableRows( datasets = rows, formatters = formatters ),
      headings = headings,
      widths = widths,
      rowheadings = rowheadings,
      missing = missing,
      style = style,
      )


  def table_columns(
    self,
    columns,
    formatters = None,
    headings = None,
    widths = None,
    rowheadings = 0,
    missing = "",
    style = None,
    ):

    from phaser.logoutput_new import data
    self.table(
      body = data.TableColumns( datasets = columns, formatters = formatters ),
      headings = headings,
      widths = widths,
      rowheadings = rowheadings,
      missing = missing,
      style = style,
      )


  def r_matrix(self, matrix):

    self.heading( text = "Rotation:", level = 6 )
    from phaser.logoutput_new import formatter
    number = formatter.Number.normal( decimal = 3 )
    self.table_rows(
      rows = [ matrix.elems[:3], matrix.elems[3:6], matrix.elems[6:] ],
      formatters = [ number ] * 3,
      widths = [ 10, 10, 10 ],
      style = "matrix",
      )


  def t_vector(self, vector):

    self.heading( text = "Translation:", level = 6 )
    from phaser.logoutput_new import formatter
    number = formatter.Number.normal( decimal = 3 )
    self.table_rows(
      rows = [ vector.elems ],
      formatters = [ number ] * 3,
      widths = [ 10, 10, 10 ],
      style = "matrix",
      )


  def rt_matrix(self, rt):

    self.r_matrix( matrix = rt.r )
    self.t_vector( vector = rt.t )


  def loggraph(self, chart):

    message = msg.Loggraph( chart = chart )
    self.channel.event( message = message )


  # Internal methods
  @staticmethod
  def no_scope_boundary_check(expected, got):

    pass


  @staticmethod
  def scope_boundary_check(expected, got):

    if not isinstance( got, expected ):
      raise LogError("Logging scope termination: expected %s, received %s" % (
        " or ".join(e.__name__ for e in expected)
        if isinstance(expected, tuple)
        else expected.__name__,
        got.__class__.__name__,
      ))


class Stream(object):
  """
  Stream object from channel
  """

  def __init__(self, channel):

    self.channel = channel


  def write(self, text):

    self.channel.text( text = text )


  def flush(self):

    self.channel.flush()
