from __future__ import print_function

class LevelChoice(object):
  """
  Return style associated with requested level

  It is valid to use higher level than specified, in which case the last
  style will be used
  """

  def __init__(self, styles):

    assert styles
    self.styles = styles


  def __call__(self, level):

    if len( self.styles ) < level:
      level = len( self.styles )

    if level <= 0:
      raise ValueError("Invalid level: %s" % level)

    return self.styles[ level - 1 ]


class NamedChoice(object):
  """
  Return style associated with name

  This is primarily used to ease writing code
  """

  def __init__(self, choices, default):

    self.default = default
    self.choices = choices


  def __call__(self, name):

    if name not in self.choices:
      return self.default

    return self.choices[ name ]


  @classmethod
  def setup(cls, choices, aliases = {}, default = None):

    if default == None:
      default = next(iter(choices.values()) )

    else:
      default = choices[ default ]

    if set( choices ) & set( aliases ):
      raise ValueError("Contradictory option specification")

    choices = choices.copy()

    for ( name, redirect ) in aliases.items():
      choices[ name ] = choices[ redirect ]

    return cls( choices = choices, default = default )

  @classmethod
  def from_dict(cls, odict):

    discards = set( [ "default", "aliases" ] )

    return cls.setup(
      choices = dict( ( k, v ) for ( k, v ) in odict.items() if k not in discards ),
      aliases = odict.get( "aliases", {} ),
      default = odict.get( "default" ),
      )
