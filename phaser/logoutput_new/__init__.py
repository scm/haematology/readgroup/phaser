from __future__ import print_function

"""
Logger system

Loggers accept messages that can render themselves using underlying methods from
the handlers. There are simple messages (events), and messages that change the
state of the logger (scopes, e.g. indentation), which apply after entering, and
restore the previous state after leaving the scope.

Loggers can be set up to use channels dedicated to a certain logging level (e.g.
info). The number, names and logging levels of these channels are arbitrary, and
logging classes do not enforce any particular conventions.

A writer logger can direct output to multiple streams. Forwarder loggers relay
all messages to underlying logger. It is the callers responsibility to ensure
that only a single child writes to the underlying root loggers at any point in
time, otherwise formatting will get interleaved.

In case an exception occurs, an attempt is made to undo modifications initiated
through the logger object, so that the stream is returned to its original state.
"""

class LogError(Exception):
  """
  Some error that occurred within the package
  """

class Channel(object):
  """
  An interface facilitating concise logging calls
  """

  def __init__(self, log_event, scope_enter, scope_exit):

    self.event = log_event
    self.enter = scope_enter
    self.exit = scope_exit


class Writer(object):
  """
  Logging class that writes to various streams
  """

  def __init__(self, handlers, level, close_handlers = False, **channels):

    self.handlers = handlers
    self.level = level
    self.close_handlers = close_handlers
    self.channels = channels
    self.scopes = []

    self.log = Channel(
      log_event = self.sorted_log_event,
      scope_enter = self.sorted_scope_enter,
      scope_exit = self.scope_exit,
      )
    self.warn = Channel(
      log_event = self.unsorted_log_event,
      scope_enter = self.unsorted_scope_enter,
      scope_exit = self.scope_exit,
      )

    for ( name, value ) in self.channels.items():
      setattr( self, name, self.channel( level = value ) )


  def __enter__(self):

    return self


  def __exit__(self, exc_type, exc_value, traceback):

    while self.scopes:
      ( scope, affecteds ) = self.scopes[-1]

      while affecteds:
        handler = affecteds.pop()

        try:
          scope.exit( handler = handler )

        except Exception:
          pass

      self.scopes.pop()

    if self.close_handlers:
      for h in self.handlers:
        h.close()

    return False


  def child(self, shift):

    return Forwarder( loggers = [ self ], shift = shift, **self.channels )


  def channel(self, level):

    from functools import partial

    return Channel(
      log_event = partial( self.sorted_log_event, level = level ),
      scope_enter = partial( self.sorted_scope_enter, level = level ),
      scope_exit = self.scope_exit,
      )


  # Internal methods
  def sorted_log_event(self, message, level):

    if self.level <= level:
      self.unsorted_log_event( message = message )


  def unsorted_log_event(self, message):

    for handler in self.handlers:
      message( handler = handler )


  def sorted_scope_enter(self, scope, level):

    affecteds = self.scope_enter_initialize( scope = scope )

    if self.level <= level:
      self.scope_enter_complete( scope = scope, affecteds = affecteds )


  def unsorted_scope_enter(self, scope):

    affecteds = self.scope_enter_initialize( scope = scope )
    self.scope_enter_complete( scope = scope, affecteds = affecteds )


  def scope_exit(self):

    ( scope, affecteds ) = self.scopes[-1]

    while affecteds:
      handler = affecteds[-1]
      scope.exit( handler = handler )
      affecteds.pop()

    self.scopes.pop()
    return scope


  def scope_enter_initialize(self, scope):

    affecteds = []
    self.scopes.append( ( scope, affecteds ) )
    return affecteds


  def scope_enter_complete(self, scope, affecteds):

    for handler in self.handlers:
      scope.enter( handler = handler )
      affecteds.append( handler )


class Forwarder(object):
  """
  Logging class that forwards messages to underlying loggers
  """

  def __init__(self, loggers, shift, **channels):

    self.loggers = loggers
    self.shift = shift
    self.channels = channels
    self.scopes = []

    self.log = Channel(
      log_event = self.sorted_log_event,
      scope_enter = self.sorted_scope_enter,
      scope_exit = self.scope_exit,
      )
    self.warn = Channel(
      log_event = self.unsorted_log_event,
      scope_enter = self.unsorted_scope_enter,
      scope_exit = self.scope_exit,
      )

    for ( name, value ) in self.channels.items():
      setattr( self, name, self.channel( level = value ) )


  def __enter__(self):

    return self


  def __exit__(self, exc_type, exc_value, traceback):

    # This code should only be executed if the call exited with an exception,
    # i.e. there is already an active exception
    while self.scopes:
      ( scope, affecteds ) = self.scopes[-1]

      while affecteds:
        logger = affecteds.pop()

        try:
          logger.scope_exit()

        except Exception:
          pass

      self.scopes.pop()


  def child(self, shift):

    return Forwarder( loggers = [ self ], shift = shift, **self.channels )


  def channel(self, level):

    from functools import partial

    return Channel(
      log_event = partial( self.sorted_log_event, level = level ),
      scope_enter = partial( self.sorted_scope_enter, level = level ),
      scope_exit = self.scope_exit,
      )


  # Internal methods
  def shifted(self, level):

    return max( 0, level - self.shift )


  def sorted_log_event(self, message, level):

    for logger in self.loggers:
      logger.sorted_log_event( message = message, level = self.shifted( level = level ) )


  def unsorted_log_event(self, message):

    for logger in self.loggers:
      logger.unsorted_log_event( message = message )


  def sorted_scope_enter(self, scope, level):

    affecteds = []
    self.scopes.append( ( scope, affecteds ) )

    for logger in self.loggers:
      logger.sorted_scope_enter( scope = scope, level = self.shifted( level = level ) )
      affecteds.append( logger )


  def unsorted_scope_enter(self, scope):

    affecteds = []
    self.scopes.append( ( scope, affecteds ) )

    for logger in self.loggers:
      logger.unsorted_scope_enter( scope = scope )
      affecteds.append( logger )


  def scope_exit(self):

    ( scope, affecteds ) = self.scopes[-1]

    while affecteds:
      logger = affecteds.pop()
      exited = logger.scope_exit()
      assert exited == scope

    self.scopes.pop()
    return scope
