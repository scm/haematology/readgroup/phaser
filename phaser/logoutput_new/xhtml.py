from __future__ import print_function

class Style(object):
  """
  CSS style
  """

  def __init__(self, **attrs):

    self.attrs = attrs


  def __nonzero__(self):

    return bool( any( value is not None for value in self.attrs.values() ) )


  def __str__(self):

    return ";".join(
      "%s:%s" % ( name.replace( "_", "-" ), value )
      for ( name, value ) in self.attrs.items() if value is not None
      )


class StyleElement(object):
  """
  Style information for spelt out style element
  """

  def __init__(self, name, **attrs):

    self.name = name
    self.attrs = Style( **attrs )


  def __nonzero__(self):

    return bool( self.attrs )


  def __str__(self):

    return "%s { %s }" % ( self.name, self.attrs )


  @classmethod
  def Single(cls, element, **attrs):

    return cls( name = element.name, **attrs )


  @classmethod
  def Multiple(cls, elements, **attrs):

    return cls( name = ", ".join( e.name for e in elements ), **attrs )


class Element(object):
  """
  HTML scope
  """

  def __init__(self, name, **attrs):

    self.name = name
    self.attrs = self.attrs_dict( attrs = attrs )


  def enter(self, writer):

    writer.startElement( name = self.name, attrs = self.attrs )


  def exit(self, writer):

    writer.endElement( name = self.name )


  @staticmethod
  def attrs_dict(attrs):

    return dict(
      ( name, str( value ) ) for ( name, value ) in attrs.items()
      if ( value is not None and not isinstance( value, Style ) ) or bool( value )
      )


  @classmethod
  def Paragraph(
    cls,
    margin_left = None,
    alignment = None,
    color = None,
    font_weight = None,
    margin_top = None,
    margin_bottom = None,
    ):

    return cls(
      name = "p",
      style = Style(
        text_align = alignment,
        margin_left = "%spx" % margin_left if margin_left is not None else None,
        color = color,
        font_weight = font_weight,
        margin_top = "%sem" % margin_top if margin_top is not None else None,
        margin_bottom = "%sem" % margin_bottom if margin_bottom is not None else None,
        ),
      )


  @classmethod
  def PreformattedParagraph(cls, margin, color = None):

    return cls(
      name = "pre",
      style = Style( margin = "%spx" % margin, color = color ),
      )


  @classmethod
  def Heading(cls, level, alignment = None, font_weight = None):

    return cls(
      name = "h" + str( level ),
      style = Style( text_align = alignment, font_weight = font_weight ),
      )


  @classmethod
  def HTML(cls, xmlns = "http://www.w3.org/1999/xhtml"):

    return cls( name = "html")


  @classmethod
  def Head(cls):

    return cls( name = "head" )


  @classmethod
  def Title(cls):

    return cls( name = "title" )


  @classmethod
  def Body(cls):

    return cls( name = "body" )


  @classmethod
  def HorizontalRule(cls):

    return cls( name = "hr" )


  @classmethod
  def Style(cls, scoped = None):

    return cls( name = "style", scoped = "scoped" if bool( scoped ) else None )


  @classmethod
  def OrderedList(cls, start = 1, list_type = None):

    return cls( name = "ol", type = list_type, start = start )


  @classmethod
  def UnorderedList(cls, list_type = None):

    return cls( name = "ul", type = list_type )


  @classmethod
  def ListItem(cls):

    return cls( name = "li" )


  @classmethod
  def LineBreak(cls):

    return cls( name = "br" )


  @classmethod
  def Input(cls, value, enabled = True):

    return cls(
      name = "input",
      type = "text",
      disabled = "disabled" if not enabled else None,
      value = value,
      )

  @classmethod
  def Strong(cls, color, alignment = None):

    return cls(
      name = "strong",
      style = Style( color = color, text_align = alignment ),
      )


  @classmethod
  def Table(cls, width = None):

    return cls(
      name = "table",
      style = Style( width = "%.0f" % width if width is not None else None ),
      )


  @classmethod
  def TableRow(cls):

    return cls( name = "tr" )


  @classmethod
  def TableHeaderCell(cls, width = None):

    return cls( name = "th", style = Style( width = width ) )


  @classmethod
  def TableDataCell(cls, width = None):

    return cls( name = "td", style = Style( width = width ) )


class FormatterTable(object):
  """
  Convenience class to keep table formatting attributes together
  """

  def __init__(self, general, header, body):

    self.general = general
    self.header = header
    self.body = body


class Handler(object):
  """
  Output to an XHTML stream
  """

  def __init__(
    self,
    stream,
    title,
    formatter_title,
    formatter_highlight,
    formatter_headings,
    formatter_alignments,
    formatter_ordered_lists,
    formatter_unordered_lists,
    formatter_table,
    char_pixel_width,
    text_formatting_width,
    close_stream,
    ):

    self.stream = stream
    self.indents = []
    self.elements = []
    self.html_system_elements = []

    self.formatter_title = formatter_title
    self.formatter_highlight = formatter_highlight
    self.formatter_headings = formatter_headings
    self.formatter_alignments = formatter_alignments
    self.formatter_ordered_lists = formatter_ordered_lists
    self.formatter_unordered_lists = formatter_unordered_lists
    self.formatter_table = formatter_table

    self.char_pixel_width = char_pixel_width
    self.text_formatting_width = text_formatting_width
    self.current_table_formatters = None

    self.close_stream = close_stream

    from xml.sax.saxutils import XMLGenerator
    self.writer = XMLGenerator( out = stream )
    self.writer.startDocument()
    self.start_system_element( element = Element.HTML() )
    self.start_system_element( element = Element.Head() )
    self.start_system_element( element = Element.Title() )
    self.write( chars = title )
    self.end_system_element()
    self.end_system_element()
    self.start_system_element( element = Element.Body() )


  def __enter__(self):

    return self


  def __exit__(self, exc_type, exc_value, traceback):

    while self.elements:
      self.end_element()

    if exc_type is not None:
      import traceback as tb
      self.traceback(
        lines = tb.format_exception( exc_type, exc_value, traceback ),
        )

    self.close()


  def close(self):

    while self.html_system_elements:
      self.end_system_element()

    self.writer.endDocument()

    if self.close_stream:
      self.stream.close()


  def indent_pixel_width(self):

    return self.char_pixel_width * sum( self.indents )


  def formatwidth(self):

    return self.text_formatting_width


  # Messages
  def text(self, text):

    self.write( chars = text )


  def unformatted_paragraph(self, text, indent, alignment):

    self.indents.append( indent )
    self.start_element(
      element = Element.Paragraph(
        margin_left = self.indent_pixel_width(),
        alignment = alignment,
        )
      )
    self.write( chars = text )
    self.end_element()
    self.indents.pop()


  def formatted_paragraph(self, text, indent):

    self.indents.append( indent )
    self.start_element(
      element = Element.PreformattedParagraph( margin = self.indent_pixel_width() ),
      )
    self.write( chars = text )
    self.end_element()
    self.indents.pop()


  def field(self, name, value, formatter = str):

    self.block_begin( indent = 1 )
    self.write( chars = name )
    self.write( chars = ": " )
    self.empty_element(
      element = Element.Input( value = formatter( value ), enabled = False ),
      )
    self.block_end()


  def title(self, text):

    self.start_element( element = self.formatter_title )
    self.write( chars = text )
    self.end_element()


  def heading(self, text, level = 1):

    self.start_element( element = self.formatter_headings( level = level ) )
    self.write( chars = text )
    self.end_element()


  def highlight(self, text):

    self.start_element( element = self.formatter_highlight )
    self.write( chars = text )
    self.end_element()


  def divider(self):

    self.empty_element( element = Element.HorizontalRule() )


  def exception(self, instance):

    self.highlight( text = "An error has occurred" )
    self.highlight( text = "Exception type: %s" % instance.__class__.__name__ )
    self.highlight( text = "Exception value: %s" % instance )


  def traceback(self, lines):

    self.start_element(
      element = Element.PreformattedParagraph(
        margin = self.indent_pixel_width(),
        color = "red",
        ),
      )
    self.write( chars = "".join( lines ) )
    self.end_element()


  def loggraph_chart(self, chart):

    self.formatted_paragraph( text = str( chart ), indent = 0 )


  def table(self, table, style = None):

    rowiter = table.rows()

    if table.names is None:
      self.table_start_without_header(
        cells = rowiter.next(),
        widths = table.widths,
        style = style,
        )

    else:
      self.table_start_with_header(
        headings = table.names,
        widths = table.widths,
        style = style,
        )

    for row in rowiter:
      self.table_row( cells = row )

    self.table_end()


  # Scopes
  def block_begin(self, indent):

    self.indents.append( indent )
    self.start_element(
      element = Element.Paragraph( margin_left = self.indent_pixel_width() ),
      )


  def block_end(self):

    self.indents.pop()
    self.end_element()


  def ordered_list_begin(self, marker, start, separate_items):

    self.start_element(
      element = self.formatter_ordered_lists( marker )( start = start ),
      )

    self.style(
      elements = [
        StyleElement(
          name = Element.ListItem().name,
          margin_bottom = "1em" if separate_items else "0em",
          ),
        ],
      scoped = True,
      )


  def unordered_list_begin(self, level, separate_items):

    self.start_element(
      element = self.formatter_unordered_lists( level = level ),
      )

    self.style(
      elements = [
        StyleElement(
          name = Element.ListItem().name,
          margin_bottom = "1em" if separate_items else "0em",
          ),
        ],
      scoped = True,
      )


  def list_end(self):

    self.end_element()


  def list_item_begin(self, text):

    self.start_element( element = Element.ListItem() )
    self.write( chars = text )
    self.start_element(
      element = Element.Paragraph( margin_top = 0, margin_bottom = 0 ),
      )


  def list_item_end(self):

    self.end_element()
    self.end_element()


  def table_start_with_header(self, headings, formatters = None, widths = None, style = None):

    if self.current_table_formatters is not None:
      raise ValueError("Call to table_start while table ongoing")

    if not headings:
      raise ValueError("Missing table header")

    ( self.current_table_formatters, widths, fullwidth ) = self.calculate_table_setup_data(
      ncols = len( headings ),
      formatters = formatters,
      widths = widths,
      )
    self.start_element( element = Element.Table( width = fullwidth ) )
    formatter = self.formatter_table( name = style )
    self.style(
      elements = [ formatter.general, formatter.header, formatter.body ],
      scoped = True,
      )
    self.start_element( element = Element.TableRow() )

    for ( head, width ) in zip( headings, widths ):
      self.start_element( element = Element.TableHeaderCell( width = width ) )
      self.write( chars = str( head ) )
      self.end_element()

    self.end_element()


  def table_start_without_header(self, cells, formatters = None, widths = None, style = None):

    if self.current_table_formatters is not None:
      raise ValueError("Call to table_start while table ongoing")

    if not cells:
      raise ValueError("Missing table row cells")

    ( self.current_table_formatters, widths, fullwidth ) = self.calculate_table_setup_data(
      ncols = len( cells ),
      formatters = formatters,
      widths = widths,
      )
    self.start_element( element = Element.Table( width = fullwidth ) )
    formatter = self.formatter_table( name = style )
    self.style( elements = [ formatter.general, formatter.body ], scoped = True )
    self.start_element( element = Element.TableRow() )

    for ( cell, formatter, width ) in zip( cells, self.current_table_formatters, widths ):
      self.start_element( element = Element.TableDataCell( width = width ) )
      self.write( chars = formatter( cell ) )
      self.end_element()

    self.end_element()


  def table_row(self, cells):

    if self.current_table_formatters is None:
      raise ValueError("Call to table_row without table_start")

    if len( cells ) != len( self.current_table_formatters ):
      raise ValueError("Table element mismatch")

    self.start_element( element = Element.TableRow() )

    for ( data, formatter ) in zip( cells, self.current_table_formatters ):
      self.start_element( element = Element.TableDataCell() )

      if data is not None:
        self.write( chars = formatter( data ) )

      self.end_element()

    self.end_element()


  def table_end(self):

    if self.current_table_formatters is None:
      raise ValueError("Call to table_end without table_start")

    self.end_element()
    self.current_table_formatters = None


  # Miscellanous
  def linefeed(self):

    self.empty_element( element = Element.LineBreak() )


  def flush(self):

    self.stream.flush()


  # Internal methods
  def start_element(self, element):

    element.enter( writer = self.writer )
    self.elements.append( element )


  def end_element(self):

    self.elements[-1].exit( writer = self.writer )
    self.elements.pop()


  def start_system_element(self, element):

    element.enter( writer = self.writer )
    self.html_system_elements.append( element )


  def end_system_element(self):

    self.html_system_elements[-1].exit( writer = self.writer )
    self.html_system_elements.pop()


  def empty_element(self, element):

    self.start_element( element = element )
    self.end_element()
    # Hack to make XMLGenerator work as in Python 3.2
    #self.stream.write( "<%s/>" % element.name )


  def style(self, elements, scoped):

    self.start_element( element = Element.Style( scoped = scoped ) )

    for elem in elements:
      if bool( elem ):
        self.write( chars = str( elem ) )

    self.end_element()


  def write(self, chars):

    self.writer.characters( chars )


  def calculate_table_setup_data(self, ncols, formatters, widths):

    if formatters is not None:
      if len( formatters ) != ncols:
        raise ValueError("Number of formatters inconsistent with number of columns")

    else:
      formatters = [ str ] * ncols

    if widths is not None:
      if len( widths ) != ncols:
        raise ValueError("Number of widths inconsistent with number of columns")

      pixelwidths = [ self.char_pixel_width * w for w in widths ]
      widths = [ "%.0fpx" % w for w in pixelwidths ]
      fullwidth = sum( pixelwidths )

    else:
      widths = [ None ] * ncols
      fullwidth = None

    return ( formatters, widths, fullwidth )


  @classmethod
  def FromDefaults(
    cls,
    stream,
    title,
    formatter_title = Element.Heading(
      level = 1,
      alignment = "center",
      font_weight = "bold",
      ),
    formatter_highlight = Element.Paragraph(
      color = "red",
      alignment = "center",
      font_weight = "bold",
      ),
    formatter_headings = [
      Element.Heading( level = 1 ),
      Element.Heading( level = 2 ),
      Element.Heading( level = 3 ),
      Element.Heading( level = 4 ),
      Element.Heading( level = 5 ),
      Element.Heading( level = 6 ),
      ],
    formatter_alignments = {
      "left": Element.Paragraph( alignment = "left" ),
      "center": Element.Paragraph( alignment = "center" ),
      "right": Element.Paragraph( alignment = "right" ),

      "default": "left",
      },
    formatter_ordered_lists = {
      "1": lambda start: Element.OrderedList( start = start ),
      "a": lambda start: Element.OrderedList( start = start, list_type = "a" ),
      "A": lambda start: Element.OrderedList( start = start, list_type = "A" ),
      "i": lambda start: Element.OrderedList( start = start, list_type = "i" ),
      "I": lambda start: Element.OrderedList( start = start, list_type = "I" ),

      "default": "1",
      },
    formatter_unordered_lists = [
      Element.UnorderedList(),
      Element.UnorderedList( list_type = "square" ),
      Element.UnorderedList( list_type = "circle" ),
      ],
    formatter_table = {
      "normal": FormatterTable(
        general = StyleElement.Single(
          element = Element.Table(),
          border = "1px solid black",
          margin = "auto",
          ),
        header = StyleElement.Single(
          element = Element.TableHeaderCell(),
          border = "1px solid black",
          ),
        body = StyleElement.Single(
          element = Element.TableDataCell(),
          border = "1px solid black",
          ),
        ),
      "simple": FormatterTable(
        general = StyleElement.Single(
          element = Element.Table(),
          margin = "auto",
          border_collapse = "collapse",
          ),
        header = StyleElement.Single(
          element = Element.TableHeaderCell(),
          border_bottom = "1px solid black",
          ),
        body = StyleElement.Single(
          element = Element.TableDataCell(),
          ),
        ),

      "default": "normal",
      "aliases": {
        "matrix": "simple",
        },
      },
    char_pixel_width = 10,
    text_formatting_width = 80,
    close_stream = False,
    ):

    from phaser.logoutput_new import utility

    return cls(
      stream = stream,
      title = title,
      formatter_title = formatter_title,
      formatter_highlight = formatter_highlight,
      formatter_headings = utility.LevelChoice( styles = formatter_headings ),
      formatter_alignments = utility.NamedChoice.from_dict(
        odict = formatter_alignments,
        ),
      formatter_ordered_lists = utility.NamedChoice.from_dict(
        odict = formatter_ordered_lists,
        ),
      formatter_unordered_lists = utility.LevelChoice( styles = formatter_unordered_lists ),
      formatter_table = utility.NamedChoice.from_dict( odict = formatter_table ),
      char_pixel_width = char_pixel_width,
      text_formatting_width = text_formatting_width,
      close_stream = close_stream,
      )
