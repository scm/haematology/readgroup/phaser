from __future__ import print_function

class Block(object):

  def __init__(self, indent):

    self.indent = indent


  def enter(self, handler):

    handler.block_begin( indent = self.indent )


  def exit(self, handler):

    handler.block_end()


class OrderedList(object):

  def __init__(self, marker, start, separate_items):

    self.marker = marker
    self.start = start
    self.separate_items = separate_items


  def enter(self, handler):

    handler.ordered_list_begin(
      marker = self.marker,
      start = self.start,
      separate_items = self.separate_items,
      )


  def exit(self, handler):

    handler.list_end()


class UnorderedList(object):

  def __init__(self, level, separate_items):

    self.level = level
    self.separate_items = separate_items


  def enter(self, handler):

    handler.unordered_list_begin(
      level = self.level,
      separate_items = self.separate_items,
      )


  def exit(self, handler):

    handler.list_end()


class ListItem(object):

  def __init__(self, text):

    self.text = text


  def enter(self, handler):

    handler.list_item_begin( text = self.text )


  def exit(self, handler):

    handler.list_item_end()


class HeadedTable(object):

  def __init__(self, headings, formatters = None, widths = None, style = None):

    self.headings = headings
    self.formatters = formatters
    self.widths = widths
    self.style = style


  def enter(self, handler):

    handler.table_start_with_header(
      headings = self.headings,
      formatters = self.formatters,
      widths = self.widths,
      style = self.style,
      )


  def exit(self, handler):

    handler.table_end()


class HeadlessTable(object):

  def __init__(self, cells, formatters = None, widths = None, style = None):

    self.cells = cells
    self.formatters = formatters
    self.widths = widths
    self.style = style


  def enter(self, handler):

    handler.table_start_without_header(
      cells = self.cells,
      formatters = self.formatters,
      widths = self.widths,
      style = self.style,
      )


  def exit(self, handler):

    handler.table_end()
