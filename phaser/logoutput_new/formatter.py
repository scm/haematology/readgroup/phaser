from __future__ import print_function

class Number(object):
  """
  Floating point object
  """

  def __init__(self, formatstring):

    self.format = formatstring


  def __call__(self, value):

    return self.format % value


  @classmethod
  def normal(cls, decimal):

    return cls( formatstring = "%." + str( decimal ) + "f" )


  @classmethod
  def scientific(cls, decimal):

    return cls( formatstring = "%." + str( decimal ) + "e" )


  @classmethod
  def percentage(cls, decimal):

    return cls( formatstring = "%." + str( decimal ) + "f%%" )


class Sequence(object):
  """
  Sequence of something
  """

  def __init__(self, formatter, separator):

    self.formatter = formatter
    self.separator = separator


  def __call__(self, value):

    return self.separator.join( self.formatter( v ) for v in value )
