from __future__ import print_function

from phaser.logoutput_new import LogError
from functools import reduce

class Wrapper(object):
  """
  Singleton object acting as a namespace
  """

  @staticmethod
  def textwrap():

    import textwrap
    return textwrap.wrap


  @staticmethod
  def truncated():

    return Wrapper.truncate_long_lines


  @staticmethod
  def original():

    return Wrapper.keep_original_length


  # Internal methods
  @staticmethod
  def truncate_long_lines(text, width):

    lines = []

    for l in text.split( "\n" ):
      length = len( l )

      if width < length:
        lines.append( l[ : width - 3 ] + "..." )

      else:
        lines.append( l )

    return lines


  @staticmethod
  def keep_original_length(text, width):

    return text.split( "\n" )


class TextParagraph(object):
  """
  A wrapped text
  """

  def __init__(self, lines):

    self.lines = list( lines )
    self.width = max( len( l ) for l in self.lines ) if self.lines else 0


class FormattedParagraph(object):
  """
  A paragraph with extra formatting applied
  """

  def __init__(self, paragraph, formatter, width):

    self.paragraph = paragraph
    self.formatter = formatter
    self.width = width


  @property
  def lines(self):

    return self.formatter.process( paragraph = self.paragraph, maxwidth = self.width )


# Formatters
class Spacing(object):

  def __init__(self, above = 1, below = 1):

    self.above = above
    self.below = below


  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in [ "" ] * self.above:
      yield l

    for l in paragraph.lines:
      yield l

    for l in [ "" ] * self.below:
      yield l


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width,
        ),
      streamwidth,
      )


class Banner(object):

  def __init__(self, char = "#"):

    self.char = char


  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * maxwidth
    yield self.char + " " * ( maxwidth - 2 ) + self.char

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( maxwidth - 4 ), self.char )

    yield self.char + " " * ( maxwidth - 2 ) + self.char
    yield self.char * maxwidth


  def __call__(self, paragraph, streamwidth):

    width = streamwidth - self.width_modifier()

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = width,
        ),
      width,
      )


class TextAlignment(object):

  def __init__(self, method):

    self.method = method


  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield self.method( l, maxwidth )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = streamwidth,
        ),
      streamwidth,
      )

  @classmethod
  def Left(cls):

    return cls( method = str.ljust )

  @classmethod
  def Center(cls):

    return cls( method = str.center )

  @classmethod
  def Right(cls):

    return cls( method = str.rjust )


class Frame(object):

  def __init__(self, char = "#"):

    self.char = char


  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * ( paragraph.width + 4 )

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( paragraph.width ), self.char )

    yield self.char * ( paragraph.width + 4 )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width - self.width_modifier(),
        ),
      streamwidth - self.width_modifier()
      )


class Underline(object):

  def __init__(self, char = "#"):

    self.char = char


  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield l

    yield self.char * paragraph.width


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width,
        ),
      streamwidth,
      )


class Filling(object):

  def __init__(self, char = "="):

    self.char = char


  def width_modifier(self):

    return -2


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield ( " %s " % l ).center( maxwidth, self.char )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = streamwidth,
        ),
      streamwidth,
      )


class Dummy(object):

  def width_modifier(self):

    return 0

  def __call__(self, paragraph, streamwidth):

    return ( paragraph, streamwidth )


class Composite(object):

  def __init__(self, formatters):

    self.formatters = formatters


  def width_modifier(self):

    return sum( f.width_modifier() for f in self.formatters )


  @staticmethod
  def apply(data, formatter):

    return formatter( paragraph = data[0], streamwidth = data[1] )


  def __call__(self, paragraph, streamwidth):

    return reduce(
      self.apply,
      reversed( self.formatters ),
      ( paragraph, streamwidth ),
      )


# Objects holding state information
class ListData(object):
  """
  Holds list details
  """

  def __init__(self, markergen):

    self.markergen = markergen


  def next(self):

    return next(self.markergen)


  @classmethod
  def Bullet(cls, marker, separator = " "):

    import itertools
    return cls( markergen = ( marker + separator for i in itertools.count() ) )


  @classmethod
  def Numbered(cls, start = 1, separator = ". ", width = 1):

    if width < 1:
      raise ValueError("Minimum value for width is 1")

    formatting = "%" + str( width ) + "d" + separator

    import itertools
    return cls( markergen = ( formatting % i for i in itertools.count( start ) ) )


  @classmethod
  def Lowercase(cls, start = 1, separator = ". "):

    import string
    return cls.Sequence(
      sequence = string.ascii_lowercase,
      start = start,
      separator = separator,
      )


  @classmethod
  def Uppercase(cls, start = 1, separator = ". "):

    import string
    return cls.Sequence(
      sequence = string.ascii_uppercase,
      start = start,
      separator = separator,
      )


  @classmethod
  def LowercaseRoman(cls, start = 1, separator = ". "):

    return cls.Sequence(
      sequence = cls.roman(),
      start = start,
      separator = separator,
      )


  @classmethod
  def UppercaseRoman(cls, start = 1, separator = ". "):

    return cls.Sequence(
      sequence = [ c.upper() for c in cls.roman() ],
      start = start,
      separator = separator,
      )

  @classmethod
  def Sequence(cls, sequence, start = 1, separator = ". "):

    selected = sequence[ start - 1 : ]
    width = max( len( s ) for s in selected )
    formatting = "%" + str( width ) + "s" + separator

    return cls( markergen = ( formatting % i for i in iter( selected )  ) )


  @staticmethod
  def roman():

    return [
      "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x",
      "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii", "xviii", "xix", "xx",
      "xxi", "xxii", "xxiii", "xxiv", "xxv", "xxvi", "xxvii", "xxviii", "xxix", "xxx",
      "xxxi", "xxxii", "xxxiii", "xxxiv", "xxxv", "xxxvi", "xxxvii", "xxxviii", "xxxix", "xl",
      "xli", "xlii", "xliii", "xliv", "xlv", "xlvi", "xlvii", "xlviii", "xlix", "l",
      "li", "lii", "liii", "liv", "lv", "lvi", "lvii", "lviii", "lix", "lx",
      "lxi", "lxii", "lxiii", "lxiv", "lxv", "lxvi", "lxvii", "lxviii", "lxix", "lxx",
      "lxxi", "lxxii", "lxxiii", "lxxiv", "lxxv", "lxxvi", "lxxvii", "lxxviii", "lxxix", "lxxx",
      "lxxxi", "lxxxii", "lxxxiii", "lxxxiv", "lxxxv", "lxxxvi", "lxxxvii", "lxxxviii", "lxxxix", "xc",
      "xci", "xcii", "xciii", "xciv", "xcv", "xcvi", "xcvii", "xcviii", "xcix", "c",
      ]


def condensed_row_style(rows):
  """
  Write table rows that are not separated by borders
  """

  return ( rows, [] )


def whitespace_for_multiline_row_style(rows):
  """
  Write table rows that are separated by whitespace if the cell takes up multiple
  lines
  """

  return (
    rows,
    [ [ " " * len( cell ) for cell in rows[-1] ] ] if 1 < len( rows ) else [],
    )


class cell_border(object):
  """
  Write table rows that are separated by border
  """

  def __init__(self, char = "-"):

    self.char = char


  def __call__(self, rows):

    return ( rows, [ [ self.char * len( cell ) for cell in rows[-1] ] ] )


def no_outside_border(table_width):

  return []


class simple_outside_border(object):

  def __init__(self, char = "-"):

    self.char = char


  def __call__(self, table_width):

    return [ self.char * table_width ]


class TableSettings(object):
  """
  Formatting information for tables
  """

  def __init__(
    self,
    aligner_table = str.center,
    aligner_header = str.center,
    aligner_cell = str.rjust,
    border_outside_horizontal = simple_outside_border( char = "=" ),
    border_outside_vertical = "|",
    separator_header_and_body = cell_border( char = "=" ),
    separator_intercolumn = "|",
    separator_interrow = cell_border(),
    ):

    self.aligner_table = aligner_table
    self.aligner_header = aligner_header
    self.aligner_cell = aligner_cell

    self.border_outside_horizontal = border_outside_horizontal
    self.border_outside_vertical = border_outside_vertical

    self.separator_header_and_body = separator_header_and_body
    self.separator_intercolumn = separator_intercolumn
    self.separator_interrow = separator_interrow


  def table_width(self, cellwidths):

    return ( sum( cellwidths )
      + max( len( cellwidths ) - 1, 0 ) * len( self.separator_intercolumn )
      + 2 * len( self.border_outside_vertical )
      )


  def setup(self, streamwidth, formatters, widths = None):

    maxwidth = (
      streamwidth
      - ( len( formatters ) - 1 ) * len( self.separator_intercolumn )
      - 2 * len( self.border_outside_vertical )
      )
    cell_count = len( formatters )

    if widths is not None:
      if len( widths ) != cell_count:
        raise ValueError("Number of widths inconsistent with number of formatters")

      total = float( sum( widths ) )

      if maxwidth < total:
        widths = [ float( w * maxwidth ) / total for w in widths ]

    else:
      widths = [ float( maxwidth ) / cell_count ] * cell_count

    cellwidths = []
    currpos = 0

    for w in widths:
      endpos = currpos + w
      cellwidths.append( int( round( endpos ) - round( currpos ) ) )
      currpos = endpos

    return TableData(
      streamwidth = streamwidth,
      formatters = formatters,
      widths = cellwidths,
      settings = self,
      )


  def assemble_header_lines(self, formatteds, cellwidths):

    celliter = self.assemble_table_row(
      formatteds = formatteds,
      cellwidths = cellwidths,
      aligner = self.aligner_header
      )
    return self.separator_header_and_body( rows = list( celliter ) )


  def assemble_data_lines(self, formatteds, cellwidths):

    celliter = self.assemble_table_row(
      formatteds = formatteds,
      cellwidths = cellwidths,
      aligner = self.aligner_cell
      )
    return self.separator_interrow( rows = list( celliter ) )


  def assemble_table_row(self, formatteds, cellwidths, aligner):

    assert len( formatteds ) == len( cellwidths )
    #print "assemble_table_row:"
    #print "  formatteds:", formatteds
    #print "  cellwidths:", cellwidths

    import textwrap
    wrappeds = [
      ( w, textwrap.wrap( f, w ) ) for ( f, w ) in zip( formatteds, cellwidths )
      ]
    maxlines = max( len( w[1] ) for w in wrappeds )

    for w in wrappeds:
      w[1].extend( [ "" ] * ( maxlines - len( w[1] ) ) )

    #print "  wrappeds:", wrappeds
    lines = []

    for i in range( maxlines ):
      lines.append( [ aligner( w[1][i], w[0] ) for w in wrappeds ] )

    #print "  assembled lines:", lines
    return lines


  def format_table_line(self, cells, streamwidth):

    return self.align_table_line(
      line = (
        self.border_outside_vertical
        + self.separator_intercolumn.join( cells )
        + self.border_outside_vertical
        ),
      streamwidth = streamwidth,
      )


  def align_table_line(self, line, streamwidth):

    return self.aligner_table( line, streamwidth )


class TableData(object):
  """
  Stores table details
  """

  def __init__(self, streamwidth, formatters, widths, settings):

    assert len( formatters ) == len( widths )

    self.streamwidth = streamwidth
    self.formatters = formatters
    self.widths = widths
    self.settings = settings

    self.table_width = self.settings.table_width( cellwidths = self.widths )
    assert self.table_width <= self.streamwidth

    self.delayed_lines = []


  def table_outside_horizontal_border(self):

    for line in self.settings.border_outside_horizontal( table_width = self.table_width ):
      yield self.settings.align_table_line( line = line, streamwidth = self.streamwidth )


  def table_header(self, headings):

    if len( headings ) != len( self.widths ):
      raise ValueError("Header element mismatch")

    ( immediates, self.delayed_lines ) = self.settings.assemble_header_lines(
      formatteds = headings,
      cellwidths = self.widths,
      )

    for cells in immediates:
      yield self.settings.format_table_line( cells = cells, streamwidth = self.streamwidth )


  def table_row(self, cells):

    if len( cells ) != len( self.widths ):
      raise ValueError("Cell element mismatch")

    #print "Call to table_row"
    #print "Current delayed_lines:", self.delayed_lines

    for c in self.delayed_lines:
      yield self.settings.format_table_line( cells = c, streamwidth = self.streamwidth )

    ( immediates, self.delayed_lines ) = self.settings.assemble_data_lines(
      formatteds = [ f( v ) for ( f, v ) in zip( self.formatters, cells ) ],
      cellwidths = self.widths,
      )
    #print "Immediate lines:", immediates
    #print "New delayed_lines:", self.delayed_lines

    for cells in immediates:
      yield self.settings.format_table_line( cells = cells, streamwidth = self.streamwidth )


class Divider(object):
  """
  Returns a dividing line
  """

  def __init__(self, char):

    self.char = char


  def __call__(self, width):

    return self.char * width

# Styling


# Output object
class Handler(object):
  """
  Output to a stream
  """

  def __init__(
    self,
    stream,
    width,
    formatter_title,
    formatter_highlight,
    formatter_divider,
    formatter_headings,
    formatter_alignments,
    formatter_ordered_lists,
    formatter_unordered_lists,
    formatter_table,
    wrapper_unformatted,
    wrapper_formatted,
    close_stream,
    ):

    self.stream = stream
    self.maxwidth = width
    self.indents = []
    self.indent = ""
    self.lists = []
    self.dirty = False
    self.last_line_empty = True

    self.formatter_title = formatter_title
    self.formatter_highlight = formatter_highlight
    self.formatter_divider = formatter_divider

    self.formatter_headings = formatter_headings
    self.formatter_alignments = formatter_alignments
    self.formatter_ordered_list = formatter_ordered_lists
    self.formatter_unordered_list = formatter_unordered_lists
    self.formatter_table = formatter_table

    self.wrapper_formatted = wrapper_formatted
    self.wrapper_unformatted = wrapper_unformatted

    self.close_stream = close_stream

    self.current_table_data = None


  def __enter__(self):

    return self


  def __exit__(self, exc_type, exc_value, traceback):

    if exc_type is not None:
      self.stream_clean()
      self.indents = []
      self.update_indent()
      self.lists = []

      import traceback as tb
      self.traceback(
        lines = tb.format_exception( exc_type, exc_value, traceback )
        )

    self.close()


  def close(self):

    if self.close_stream:
      self.stream.close()


  def indentwidth(self):

    return sum( self.indents )


  def streamwidth(self):

    return self.maxwidth - len( self.indent )


  def formatwidth(self):

    return self.streamwidth()


  # Messages
  def text(self, text):

    if not text:
      return

    from io import StringIO
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    lineiter = StringIO( u"" + text)

    if self.dirty:
      line = next(lineiter)
      self.stream.write( line.rstrip() )

      if line[-1] == "\n":
        self.linefeed()
        self.dirty = False

      self.last_line_empty = False


    for line in lineiter:
      if line[-1] == "\n":
        self.write( line = line.rstrip() )

      else:
        self.stream.write( self.indent )
        self.stream.write( line )
        self.dirty = True
        self.last_line_empty = False


  def unformatted_paragraph(self, text, indent, alignment):

    self.block_begin( indent = indent )
    self.write_formatted(
      text = text,
      formatter = self.formatter_alignments( name = alignment ),
      wrapper = self.wrapper_unformatted,
      )
    self.block_end()
    self.whiteline()


  def formatted_paragraph(self, text, indent):

    self.block_begin( indent = indent )
    self.write_formatted(
      text = text,
      formatter = Dummy(),
      wrapper = self.wrapper_formatted,
      )
    self.block_end()
    self.whiteline()


  def field(self, name, value, formatter = str):

    self.stream_clean()
    self.text( text = "%s: %s" % ( name, formatter( value ) ) )
    self.linefeed()


  def title(self, text):

    self.write_formatted(
      text = text,
      formatter = self.formatter_title,
      wrapper = self.wrapper_formatted,
      )
    self.whiteline()


  def heading(self, text, level):

    self.whiteline()
    self.write_formatted(
      text = text,
      formatter = self.formatter_headings( level = level ),
      wrapper = self.wrapper_formatted,
      )


  def highlight(self, text):

    self.write_formatted(
      text = text,
      formatter = self.formatter_highlight,
      wrapper = self.wrapper_unformatted,
      )
    self.whiteline()


  def divider(self):

    self.stream_clean()
    self.write( line = self.formatter_divider( width = self.streamwidth() ) )
    self.whiteline()


  def exception(self, instance):

    self.stream_clean()
    self.highlight(
      text = "An error has occurred\n\nError type: %s\nError value: %s" % (
        instance.__class__.__name__,
        instance,
        )
      )


  def traceback(self, lines):

    self.divider()
    self.formatted_paragraph( text = "".join( lines ), indent = 0 )
    self.divider()


  def loggraph_chart(self, chart):

    self.write_formatted(
      text = str( chart ),
      formatter = Dummy(),
      wrapper = self.wrapper_formatted,
      )


  def table(self, table, style = None):

    ncolumns = table.data.ncolumns()

    if table.widths is None:
      if table.names is None:
        widths = [ w + 2 for w in table.data.widths() ]

      else:
        widths = [
          max( len( n ), w ) + 2 for ( n, w ) in zip( table.names, table.data.widths() )
          ]

    else:
      widths = table.widths

    others = list( reversed( range( table.rowheadings, ncolumns ) ) )
    formatter = self.formatter_table( name = style )

    while others:
      if len( others ) < ncolumns - table.rowheadings:
        self.formatted_paragraph( text = "Continued...", indent = 0 )

      currents = list(range( table.rowheadings ))

      while others:
        nextcol = others[-1]
        fullwidth = formatter.table_width(
          cellwidths = [ widths[i] for i in currents ] + [ widths[ nextcol ] ]
          )

        if fullwidth <= self.streamwidth():
          currents.append( nextcol )
          others.pop()

        else:
          break

      if len( currents ) == table.rowheadings:
        raise LogError("Stream not wide to print next column (%s)" % others[-1])

      rowiter = table.rows()

      if table.names is None:
        row = next(rowiter)
        self.table_start_without_header(
          cells = [ row[i] for i in currents ],
          widths = [ widths[i] for i in currents ],
          style = style,
          )

      else:
        self.table_start_with_header(
          headings = [ table.names[i] for i in currents ],
          widths = [ widths[i] for i in currents ],
          style = style,
          )

      for row in rowiter:
        self.table_row( cells = [ row[i] for i in currents ] )

      self.table_end()


  # Scopes
  def block_begin(self, indent):

    self.stream_clean()
    self.indents.append( indent )
    self.update_indent()


  def block_end(self):

    self.stream_clean()
    self.indents.pop()
    self.update_indent()


  def ordered_list_begin(self, marker, start, separate_items):

    self.stream_clean()
    self.lists.append(
      ( self.formatter_ordered_list( marker )( start = start ), separate_items )
      )


  def unordered_list_begin(self, level, separate_items):

    self.stream_clean()
    self.lists.append(
      ( self.formatter_unordered_list( level = level ), separate_items )
      )


  def list_end(self):

    self.lists.pop()
    self.whiteline()


  def list_item_begin(self, text):

    self.stream_clean()
    marker = self.lists[-1][0].next()
    lines = self.wrapper_unformatted(
      text = marker + text,
      width = self.streamwidth()
      )
    self.write( line = lines[0] )
    self.block_begin( indent = len( marker ) )

    if 1 < len( lines ):
      self.write_formatted(
        text = " ".join( lines[1:] ),
        formatter = Dummy(),
        wrapper = self.wrapper_unformatted,
        )


  def list_item_end(self):

    self.stream_clean()
    self.block_end()

    if self.lists[-1][-1]:
      self.whiteline()


  def table_start_with_header(self, headings, formatters = None, widths = None, style = None):

    if self.current_table_data is not None:
      raise ValueError("Call to table_start while table ongoing")

    if not headings:
      raise ValueError("Missing table row cells")

    self.current_table_data = self.formatter_table( name = style ).setup(
      streamwidth = self.streamwidth(),
      formatters = formatters if formatters is not None else [ str ] * len( headings ),
      widths = widths,
      )

    for line in self.current_table_data.table_outside_horizontal_border():
      self.write( line = line )

    for line in self.current_table_data.table_header( headings = headings ):
      self.write( line = line )


  def table_start_without_header(self, cells, formatters = None, widths = None, style = None):

    if self.current_table_data is not None:
      raise ValueError("Call to table_start while table ongoing")

    if not cells:
      raise ValueError("Missing table row cells")

    self.current_table_data = self.formatter_table( name = style ).setup(
      streamwidth = self.streamwidth(),
      formatters = formatters if formatters is not None else [ str ] * len( cells ),
      widths = widths,
      )

    for line in self.current_table_data.table_outside_horizontal_border():
      self.write( line = line )

    for line in self.current_table_data.table_row( cells = cells ):
      self.write( line = line )


  def table_row(self, cells):

    if self.current_table_data is None:
      raise ValueError("Call to table_row without table_start")

    for line in self.current_table_data.table_row( cells = cells ):
      self.write( line = line )


  def table_end(self):

    if self.current_table_data is None:
      raise ValueError("Call to table_row without table_start")

    for line in self.current_table_data.table_outside_horizontal_border():
      self.write( line = line )

    self.current_table_data = None
    self.whiteline()


  # Miscellanous
  def linefeed(self):

    self.stream.write( "\n" )

    if self.dirty:
      self.dirty = False

    else:
      self.last_line_empty = True



  def flush(self):

    self.stream.flush()


  # Internal methods
  def update_indent(self):

    self.indent = " " * self.indentwidth()


  def stream_clean(self):

    if self.dirty:
      self.last_line_empty = False
      self.linefeed()


  def write_formatted(self, text, formatter, wrapper):

    self.stream_clean()
    width = self.streamwidth() + formatter.width_modifier()
    paragraph = TextParagraph( lines = wrapper( text = text, width = width ) )
    fparagraph = formatter( paragraph = paragraph, streamwidth = width )[0]

    for line in fparagraph.lines:
      self.write( line = line )


  def write(self, line):

    if line:
      self.stream.write( self.indent )
      self.stream.write( line )
      self.linefeed()
      self.last_line_empty = False

    else:
      self.linefeed()
      self.last_line_empty = True



  def whiteline(self):

    if not self.last_line_empty:
      self.linefeed()
      self.last_line_empty = True

  @classmethod
  def FromDefaults(
    cls,
    stream,
    width,
    formatter_title = Banner(),
    formatter_highlight = Composite(
      formatters = [ TextAlignment.Center(), Frame() ]
      ),
    formatter_divider = Divider( char = "-" ),
    formatter_headings = [ "=", "+", "-", " ", "" ],
    formatter_alignments = {
      "left": Dummy(),
      "center": TextAlignment.Center(),
      "right": TextAlignment.Right(),

      "default": "left",
      },
    formatter_ordered_lists = {
      "1": ListData.Numbered,
      "a": ListData.Lowercase,
      "A": ListData.Uppercase,
      "i": ListData.LowercaseRoman,
      "I": ListData.UppercaseRoman,

      "default": "1",
      },
    formatter_unordered_lists = [
      ListData.Bullet( marker = "*" ),
      ListData.Bullet( marker = "o" ),
      ListData.Bullet( marker = "-" ),
      ],
    formatter_table = {
      "normal": TableSettings(),
      "simple": TableSettings(
        border_outside_horizontal = no_outside_border,
        border_outside_vertical = "",
        separator_header_and_body = cell_border( char = "-" ),
        separator_intercolumn = "",
        separator_interrow = whitespace_for_multiline_row_style,
        ),

      "default": "normal",
      "aliases": {
        "matrix": "simple",
        },
      },
    wrapper_unformatted = Wrapper.textwrap(),
    wrapper_formatted = Wrapper.original(),
    close_stream = False,
    ):

    from phaser.logoutput_new import utility

    return cls(
      stream = stream,
      width = width,
      formatter_title = formatter_title,
      formatter_highlight = formatter_highlight,
      formatter_divider = formatter_divider,
      formatter_headings = utility.LevelChoice(
        styles = [ Underline( char = c ) if c else Dummy() for c in formatter_headings ],
        ),
      formatter_alignments = utility.NamedChoice.from_dict( odict = formatter_alignments ),
      formatter_ordered_lists = utility.NamedChoice.from_dict(
        odict = formatter_ordered_lists,
        ),
      formatter_unordered_lists = utility.LevelChoice( styles = formatter_unordered_lists ),
      formatter_table = utility.NamedChoice.from_dict( odict = formatter_table ),
      wrapper_unformatted = wrapper_unformatted,
      wrapper_formatted = wrapper_formatted,
      close_stream = close_stream,
      )
