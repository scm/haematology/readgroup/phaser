from __future__ import print_function

class Text(object):
  """
  Some text
  """

  def __init__(self, text, linefeed = False):

    self.text = text
    self.linefeed = linefeed


  def __call__(self, handler):

    handler.text( text = self.text )

    if self.linefeed:
      handler.linefeed()


class Paragraph(object):
  """
  A longer text that needs wrapping
  """

  def __init__(self, text, indent = 0, alignment = None):

    self.text = text
    self.indent = indent
    self.alignment = alignment


  def __call__(self, handler):

    handler.unformatted_paragraph(
      text = self.text,
      indent = self.indent,
      alignment = self.alignment,
      )


class Document(object):
  """
  Text document
  """

  def __init__(self, text, indent = 0):

    self.text = text
    self.indent = indent


  def __call__(self, handler):

    handler.formatted_paragraph( text = self.text, indent = self.indent )


class Title(object):
  """
  Title
  """

  def __init__(self, text):

    self.text = text


  def __call__(self, handler):

    handler.title( text = self.text )


class Heading(object):
  """
  Heading
  """

  def __init__(self, text, level):

    self.text = text
    self.level = level


  def __call__(self, handler):

    handler.heading( text = self.text, level = self.level )


class Highlight(object):
  """
  Highlighted text
  """

  def __init__(self, text):

    self.text = text


  def __call__(self, handler):

    handler.highlight( text = self.text )


class Exception(object):
  """
  An exception occurred
  """

  def __init__(self, instance):

    self.instance = instance


  def __call__(self, handler):

    handler.exception( instance = self.instance )


class Traceback(object):
  """
  Full traceback information
  """

  def __init__(self, exc_type, exc_value, traceback):

    import traceback as tb
    self.lines = tb.format_exception( exc_type, exc_value, traceback )


  def __call__(self, handler):

    handler.traceback( lines = self.lines )


def Separator(handler):
  """
  Visible separator
  """

  handler.divider()


def Flush(handler):
  """
  Flush file handles so that message appears on screen
  """

  handler.flush()


class Field(object):
  """
  Data field
  """

  def __init__(self, name, value, formatter):

    self.name = name
    self.value = value
    self.formatter = formatter


  def __call__(self, handler):

    handler.field( name = self.name, value = self.value, formatter = self.formatter )


  @classmethod
  def Float(cls, name, value, digits):

    from phaser.logoutput_new.formatter import Number
    return cls(
      name = name,
      value = value,
      formatter = Number.normal( decimal = digits ),
      )

  @classmethod
  def Scientific(cls, name, value, digits):

    from phaser.logoutput_new.formatter import Number
    return cls(
      name = name,
      value = value,
      formatter = Number.scientific( decimal = digits ),
      )

  @classmethod
  def Percentage(cls, name, value, digits):

    from phaser.logoutput_new.formatter import Number
    return cls(
      name = name,
      value = value,
      formatter = Number.percentage( decimal = digits ),
      )


  @classmethod
  def Sequence(cls, name, value, formatter, separator):

    from phaser.logoutput_new.formatter import Sequence
    return cls(
      name = name,
      value = value,
      formatter = Sequence( formatter = formatter, separator = separator ),
      )


class TableRow(object):
  """
  Table row
  """

  def __init__(self, cells):

    self.cells = cells


  def __call__(self, handler):

    handler.table_row( cells = self.cells )


class Alignment(object):
  """
  Alignment
  """

  def __init__(self, alignment, indent = 0):

    self.alignment = alignment
    self.indent = indent


  def __call__(self, handler):

    document = Document(
      text = self.alignment.simple_format( width = handler.formatwidth() ),
      )
    document( handler = handler )


class Sequence(object):
  """
  Sequence
  """

  def __init__(self, sequence, indent = 0):

    self.sequence = sequence
    self.indent = indent


  def __call__(self, handler):

    document = Document(
      text = self.sequence.format( width = handler.formatwidth() ),
      )
    document( handler = handler )


class Loggraph(object):
  """
  Loggraph
  """

  def __init__(self, chart):

    self.chart = chart


  def __call__(self, handler):

    handler.loggraph_chart( chart = self.chart )


class Table(object):
  """
  Table
  """

  def __init__(self, table, style = None):

    self.table = table
    self.style = style


  def __call__(self, handler):

    handler.table( table = self.table, style = self.style )
