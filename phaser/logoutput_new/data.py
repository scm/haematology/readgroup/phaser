from __future__ import print_function

from phaser.logoutput_new import LogError

class TableColumns(object):
  """
  Contains data in tabular format
  """

  def __init__(self, datasets, formatters = None):

    if not datasets:
      raise LogError("Empty table")

    self.datasets = datasets
    nrows = self.nrows()
    ncolumns = self.ncolumns()

    if any( len( ds ) != nrows for ds in self.datasets ):
      raise LogError("Inconsistent table data columns")

    if formatters is None:
      self.formatters = [ str ] * ncolumns

    else:
      if ncolumns != len( formatters ):
        raise LogError("Formatters inconsistent with table columns")

      else:
        self.formatters = formatters


  def ncolumns(self):

    return len( self.datasets )


  def nrows(self):

    return len( self.datasets[0] )


  def rows(self, missing):

    for i in range( self.nrows() ):
      entry = []

      for ( ds, df ) in zip( self.datasets, self.formatters ):
        entry.append( missing if ds[i] is None else df( ds[i] ) )

      yield entry


  def widths(self):

    return [
      max( len( df( v ) ) if v is not None else 0 for v in ds )
      for ( df, ds ) in zip( self.formatters, self.datasets )
      ]


class TableRows(object):
  """
  Contains data in tabular format
  """

  def __init__(self, datasets, formatters = None):

    if not datasets:
      raise LogError("Empty table")

    self.datasets = datasets
    ncolumns = self.ncolumns()

    if any( len( ds ) != ncolumns for ds in self.datasets ):
      raise LogError("Inconsistent table data rows")

    if formatters is None:
      self.formatters = [ str ] * ncolumns

    else:
      if ncolumns != len( formatters ):
        raise LogError("Formatters inconsistent with table columns")

      else:
        self.formatters = formatters


  def ncolumns(self):

    return len( self.datasets[0] )


  def nrows(self):

    return len( self.datasets )


  def rows(self, missing):

    for row in self.datasets:
      entry = [
        missing if c is None else df( c ) for ( df, c ) in zip( self.formatters, row )
        ]
      yield entry


  def widths(self):

    widths = [ 0 ] * self.ncolumns()

    for row in self.rows( missing = None ):
      currwidths = [ len( c ) if c else 0 for c in row ]
      widths = [ max( w, cw ) for ( w, cw ) in zip( widths, currwidths ) ]

    return widths


class Table(object):
  """
  Table info and data
  """

  def __init__(self, data, names = None, widths = None, rowheadings = 0, missing = ""):

    self.data = data

    if names is not None and len( names ) != data.ncolumns():
      raise LogError("Table headings inconsistent with table columns")

    self.names = names

    if widths is not None and len( widths ) != data.ncolumns():
      raise LogError("Table column widths inconsistent with table columns")

    self.widths = widths

    if data.ncolumns() < rowheadings:
      raise LogError("Table row headings larger than number of columns")

    self.rowheadings = rowheadings
    self.missing = missing


  def rows(self):

    return self.data.rows( missing = self.missing )


class LoggraphTable(object):
  """
  Loggraph information
  """

  def __init__(self, data, names, heading = None):

    self.data = data

    if len( names ) != data.ncolumns():
      raise LogError("Table headings inconsistent with table columns")

    self.names = names
    self.heading = heading


class LoggraphType(object):
  """
  Loggraph type
  """

  def __init__(self, code):

    self.code = code


  def __str__(self):

    return self.code


  @classmethod
  def Auto(cls):

    return cls( code = "AUTO" )


  @classmethod
  def Nought(cls):

    return cls( code = "NOUGHT" )


  @classmethod
  def Defined(cls, xmin, xmax, ymin, ymax):

    return cls( code = "%s|%sx%s|%s" )


class LoggraphGraph(object):
  """
  Contains the graph specification
  """

  def __init__(self, graphname, graphtype, columnlist):

    self.graphname = graphname
    self.graphtype = graphtype
    self.columnlist = columnlist


  def __str__(self):

    return ":%s:%s:%s:" % (
      self.graphname,
      self.graphtype,
      ",".join( str( i ) for i in self.columnlist ),
      )


class LoggraphChart(object):

  def __init__(self, title, charttype, graphs, table):

    self.title = title
    self.charttype = charttype

    if not graphs:
      raise LogError("No graphs defined")

    self.graphs = graphs
    self.table = table

    ncols = self.table.data.ncolumns()

    for g in graphs:
      if any( ncols < c for c in g.columnlist ):
        raise LogError("Non-existent column assigned for graph")

    self.missing = "?"


  def __str__(self):

    lines = [ "$TABLE :%s:" % self.title, "$%s" % self.charttype ]

    for g in self.graphs:
      lines.append( str( g ) )

    lines.append( "$$" )
    lines.append( " ".join( self.table.names ) )
    lines.append( "$$ %s $$" % self.table.heading )

    for entry in self.table.data.rows( missing = self.missing ):
      lines.append( " ".join( entry ) )

    lines.append( "$$" )
    return "\n".join( lines )


  @classmethod
  def Graphs(cls, title, graphs, table):

    return cls( title = title, charttype = "GRAPHS", graphs = graphs, table = table )


  @classmethod
  def Scatter(cls, title, graphs, table):

    return cls( title = title, charttype = "SCATTER", graphs = graphs, table = table )
