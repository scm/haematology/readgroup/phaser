#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     05/04/2011
# Copyright:   (c) oeffner 2011
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from __future__ import print_function

import os, re, sys, shutil, glob, traceback
import time, tempfile
import math
from io import StringIO    # for handling unicode strings
import random, string, pickle
from iotbx import pdb, mtz
import scitbx.math.euler_angles
from phaser import AlterOriginSymmate, BLASTMRutils
from phaser import *
import phaser.test_phaser_parsers, phaser
from phenix.command_line import get_cc_mtz_mtz
from phenix.command_line import get_cc_mtz_pdb
from phaser.utils2 import *
from phaser import SimpleFileProperties
import logging


nulllogger = logging.getLogger("null_logger")
nulllogger.addHandler(logging.NullHandler)



def ShiftMRsols(solfname, outfname):
    def mymatch(m):
        ( xs, ys, zs ) = m.group( 1, 2, 3)
        x = float(xs)%1 if float(xs) > 0  else float(xs)%-1
        y = float(ys)%1 if float(ys) > 0  else float(ys)%-1
        z = float(zs)%1 if float(zs) > 0  else float(zs)%-1
        return "FRAC %.5f %.5f %.5f" % (x, y, z)

    solsstr = open(solfname,"r").read()
    newsolstr = re.sub(r"FRAC\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)",
     mymatch, solsstr)
    mfile = open(outfname,"w")
    mfile.write(newsolstr)
    mfile.close()



def RealizeSoluPDB(xtalsym, pdbfname, euler, frac, outfname):
    mstr = "\nRealizing MR solution, %s\nwith model %s\nat Euler: %s and Frac: %s\n" \
      %(outfname, pdbfname, str(euler), str(frac))
    pdbmrsol = pdb.input(file_name= pdbfname)
    rot = scitbx.math.euler_angles.zyz_matrix(float(euler[0]), float(euler[1]), float(euler[2]))
    trans = (float(frac[0]), float(frac[1]), float(frac[2]))
    mrsolhiearchy = pdbmrsol.construct_hierarchy().deep_copy()
    modelatoms = mrsolhiearchy.atoms()
    # do the rotation in cartesian space
    rotatomsites = rot * modelatoms.extract_xyz()
    ucell = xtalsym.unit_cell()
    # get into fractional space
    fracatomsites = ucell.fractionalize(sites_cart= rotatomsites )
    # position and rotate pdb to the mr solution
    mrsolsites = fracatomsites + trans
    # cast back to cartesian coordinates
    modelatoms.set_xyz(ucell.orthogonalize(sites_frac= mrsolsites))
    lastpdbfname = outfname
    mrsolpdbfile = open(lastpdbfname, "w")
    mrsolpdbfile.write(mrsolhiearchy.as_pdb_string())
    mrsolpdbfile.close()
    return mstr




def getCC(mtz1, fname2, temp, localcclst=None, F1=None, Phi1=None, F2=None,
  Phi2=None, rshell=None):
    outstr = "\n\n--------------------------------------------------------------------------\n"
    resstr =""
    if rshell:
        resstr += "lohires: %s\n" %str(Roundoff(rshell, 4))
    outstr += "Calculating CC between %s and %s" %(mtz1, fname2)
    outstr += resstr
    randstr = next(tempfile._get_candidate_names()) # generate a random folder name
    randtemp = temp + "_" + randstr # ensure uniqueness of tempfolder to avoid race conditions
    ret = (None, None)
    files_2B_read = 0
    #import traceback;   print "".join( traceback.format_stack(limit=4) )
    dmpstr = StringIO()

    while files_2B_read < 3:
        try:
            if fname2[-4:] == ".mtz":
                cmdargs = ["mtz_1="+ mtz1, "mtz_2=" + fname2,
                 "temp_dir=" + randtemp, "output_dir=" + randtemp]
                if F1 and Phi1 and F2 and Phi2:
                    cmdargs.append('labin_1="FP=%s PHIB=%s"' %(F1, Phi1))
                    cmdargs.append('labin_2="FP=%s PHIB=%s"' %(F2, Phi2))
                if rshell:
                    if rshell[0] <= rshell[1]:
                        outstr += "CC omitted because Lores <= Hires\n"
                        ret = (0.0, 0.0)
                        break
                    cmdargs.append('low_resolution=%f' %rshell[0])
                    cmdargs.append('resolution=%f' %rshell[1])
                myobj = get_cc_mtz_mtz.get_cc_mtz_mtz(cmdargs, quiet=True, out=dmpstr)
                outstr += dmpstr.getvalue()
                ret = (Roundoff(float(myobj.final_cc),4), 0.0)
                outstr += "CC between %s and %s is %s\n" %(os.path.basename(mtz1),
                  os.path.basename(fname2), str(ret[0]))
            else:
                cmdargs = ["mtz_in="+ mtz1, "pdb_in=" + fname2,
                  "temp_dir=" + randtemp, "output_dir=" + randtemp]
                myobj = get_cc_mtz_pdb.get_cc_mtz_pdb(cmdargs, quiet=False, out=dmpstr)
                outstr += dmpstr.getvalue()
                ret = (Roundoff(float(myobj.found_overall),4), Roundoff(float(myobj.found_region),4))
# want to cut out the local cc table from the cc.log file and store the first 4 values in a python list
#                    ------ MEAN DENSITY -------              NOTES
#  RESIDUE           CC       ALL     MAIN     SIDE
# ILE     16   A   0.32      1.5      1.6      1.5
# VAL     17   A   0.53      1.5      1.8      1.1
# GLU     18   A   0.41      1.9      2.1      1.1
# GLY     19   A   0.14     -0.3     -0.3      0.0     WEAK DENSITY (All       )
# TRP     20   A   0.28      1.3      0.7      2.5     WEAK DENSITY (Main-chain)
                if localcclst==[]:
                    mstr = ""
                    cclog = open(os.path.join(randtemp, "cc.log"),"r").read()
                    m = 52 + cclog.find("\n  RESIDUE           CC       ALL     MAIN     SIDE")
# table of local CC is between these two strings in cc.log
                    n=cclog.find("\n======================================================================")
                    cctablestr = cclog[m:n]
                    for line in cctablestr.split("\n"):
                        splt = line.split()
                        if len(splt) >=3:
                            lstr = "%s\t%s\t%s\t%s\n" %(splt[0], splt[1], splt[2], splt[3])
                            mstr += lstr
                    localcclst.append(mstr)
            shutil.rmtree(randtemp)
            files_2B_read = 4

        except Exception:
            # try reading files again if access denied at first
            outstr += traceback.format_exc()
            files_2B_read += 1
            time.sleep(2)
    return outstr, ret




def CreateZeroRNPcycleMap(datrun, rano, rootname, pdbfname, targseqnames,
  modelrmsd, overwrite=False, llgref = None, bOccRefine= False, debug = False):
    mtzmapname = rootname + ".1.mtz"
    outstr = "Making map %s from 0 cycle RNP\nof %s with vrms= %2.3f\n" %(mtzmapname, pdbfname, modelrmsd)
    if os.path.exists(mtzmapname) and not overwrite:
        outstr += mtzmapname + " exists already.\n"
        return outstr

    if not bOccRefine:
        rnptargetinput = InputMR_RNP()
        rnptargetinput.setNORM_DATA(rano.getSigmaN())
        rnptargetinput.setMACA_PROT("OFF")
        rnptargetinput.setSPAC_HALL(datrun.getSpaceGroupHall())
        rnptargetinput.setCELL6(datrun.getUnitCell())
        rnptargetinput.setREFL_DATA(datrun.getDATA())
        rnptargetinput.setROOT(rootname)
        rnptargetinput.setJOBS(1)
        rnptargetinput.setXYZO(False) # only want the map
        rnptargetinput.addENSE_PDB_RMS("RNP0cycle", pdbfname, modelrmsd)
        rnptargetinput.addSOLU_ORIG_ENSE("RNP0cycle")
        for i in range(len(targseqnames)):
            rnptargetinput.addCOMP_PROT_SEQ_NUM(targseqnames[i], 1)
        rnptargetinput.setMUTE(debug==False)
        rnptargetinput.setMACM_PROT("OFF") # 0 macrocycles, no refinement
        rnptarget = runMR_RNP(rnptargetinput)
    else:
        occinput = InputMR_OCC()
        occinput.setMACA_PROT("OFF")
        #occinput.setOCCU_REFI_TOP(True)
        #occinput.setOCCU_REFI_REJE(True)
        occinput.setSPAC_HALL(datrun.getSpaceGroupHall())
        occinput.setCELL6(datrun.getUnitCell())
        occinput.setREFL_DATA(datrun.getDATA())
        occinput.setROOT(rootname)
        #occinput.setSOLU(rnptarget.getDotSol())
        occinput.setJOBS(1)
        occinput.setXYZO(False) # only want the map
        occinput.addENSE_PDB_RMS("RNP0cycle", pdbfname, modelrmsd)
        occinput.addSOLU_ORIG_ENSE("RNP0cycle")
        for i in range(len(targseqnames)):
            occinput.addCOMP_PROT_SEQ_NUM(targseqnames[i], 1)
        occinput.setMUTE(debug==False)
        rnptarget = runMR_OCC(occinput)

    if not rnptarget.Success():
        outstr += rnptarget.ErrorName() + " CreateZeroRNPcycleMap ERROR: " + rnptarget.ErrorMessage() + "\n"
        #raise Exception, m
    else:
        if llgref != None:
            llgref.append(rnptarget.getValues()[0] )
    open(rootname + ".log","w").write(rnptarget.logfile())
    return outstr



def Sol2EulerFrac(solstr):
    enseulerfrac = []
    MRsolens = re.findall("SOLU 6DIM ENSE\s+(.+)\s+(EULER.+)\s+(FRAC.+)\s+BFAC.+", solstr, re.MULTILINE)
    for mrsolens in MRsolens:
        euler = re.findall(r"EULER\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)",
          mrsolens[1], re.MULTILINE)
        frac = re.findall(r"FRAC\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)",
          mrsolens[2], re.MULTILINE)
        enseulerfrac.append((mrsolens[0], euler[0], frac[0]))
    return enseulerfrac



def Nrefl2array(dmin, dmax, nrefl):
    """ Make array of resolution shells with elements:
    (#reflections, lores, hires, avgres, avg(1/res^2))
    """
    invdmin2=1.0/(dmin*dmin)
    invdmax2=1.0/(dmax*dmax)
    nshells = int(nrefl/500)
    shells = [(invdmin2-invdmax2)*n/nshells +invdmax2 for n in range(nshells)]
    def Nrefl2(invres2,dmin,dmax,nrefl):
        return (math.pow(invres2, 1.5)-1/(dmax*dmax*dmax))*nrefl/(1/(dmin*dmin*dmin)-1/(dmax*dmax*dmax))
    reflres = [(Nrefl2(shell, dmin, dmax, nrefl), 1.0/math.sqrt(shell), shell) for shell in shells]
    nreflavgres = []
    for i,rfl in enumerate(reflres):
        if (i+2)>len(reflres):
            break
        nreflavgres.append((reflres[i+1][0]-rfl[0], rfl[1], reflres[i+1][1],
          (reflres[i+1][1]+rfl[1])/2.0,(reflres[i+1][2]+rfl[2])/2.0))
    return nreflavgres



def RunMRphaser(datrun, rano, seqfnames, MRpdbensdict, root, perturbation,
                perturbtype, nthreads, nmaxsols, killtime, logger):
    logger.log(logging.INFO, "Searching for MR solutions using perturbation %s\n" %perturbation)
    myinput = InputMR_AUTO()
    myinput.setSPAC_HALL(datrun.getSpaceGroupHall())
    celldims = list(datrun.getUnitCell())
    if perturbtype == "PerturbCelldim":
        celldims[0] *= (1.0 + perturbation)
        celldims[1] *= (1.0 + perturbation)
        celldims[2] *= (1.0 - perturbation)
        #celldims[3] *= (1.0 - perturbation)
        #celldims[4] *= (1.0 + perturbation)
        #celldims[5] *= (1.0 - perturbation)
    myinput.setCELL6(tuple(celldims))
    myinput.setREFL_DATA(datrun.getDATA())
    myinput.setROOT(root)
    myinput.setJOBS(nthreads)
    myinput.setKILL_TIME(killtime)
    myinput.setPURG_RNP_NUMB(nmaxsols)
    #myinput.setENSE_ESTI(rmsestimator)
    for e in MRpdbensdict.items():
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        ensemblename = e[0]
        modelfname = e[1][0]
        errorestimate = e[1][1]
        if perturbtype == "PerturbRMS":
            if type(perturbation)==str:
                nresidues = SimpleFileProperties.GetNumberofResiduesinPDBfile(modelfname)
                seqid = SimpleFileProperties.NewRMS2Seqid(errorestimate, nresidues)
                myinput.addENSE_PDB_ID(ensemblename, modelfname, seqid)
                if perturbation=="OEFFNERHI":
                    myinput.setENSE_ESTI(ensemblename, "OEFFNERHI")
                if perturbation=="OEFFNERLO":
                    myinput.setENSE_ESTI(ensemblename, "OEFFNERLO")
                logger.log(logging.INFO, "NewRMS2Seqid(errorestimate, nresidues) = %f" %seqid)
            else:
                if e[1][2] == "isSeqID":
                    logger.log(logging.INFO, "Using sequence identity for " + ensemblename)
                    myinput.addENSE_PDB_ID(ensemblename,modelfname, (float(errorestimate) * (1.0 + perturbation)))
                else:
                    if e[1][2] == "isRMS":
                        logger.log(logging.INFO, "Using RMSD for " + ensemblename )
                        myinput.addENSE_PDB_RMS(ensemblename,modelfname, (float(errorestimate) * (1.0 + perturbation)))
        else:
            if e[1][2] == "isSeqID":
                logger.log(logging.INFO, "Using sequence identity for " + ensemblename )
                myinput.addENSE_PDB_ID(ensemblename,modelfname, float(errorestimate) )
            else:
                if e[1][2] == "isRMS":
                    logger.log(logging.INFO, "Using RMSD for " + ensemblename )
                    myinput.addENSE_PDB_RMS(ensemblename,modelfname, float(errorestimate) )
        myinput.addSEAR_ENSE_NUM(ensemblename,1)
    if perturbtype == "PerturbFsol":
        logger.log(logging.INFO, "Perturbing Fsol.")
        myinput.setSOLP_BULK_USE(True)
        myinput.setSOLP_BULK_FSOL(perturbation)
        myinput.setSOLP_BULK_BSOL(45.0)
        myinput.setMACM_PROT("CUSTOM")
        myinput.addMACM(ref_rota=True, ref_tran=True, ref_bfac=True,
         ref_vrms=True, ref_cell=False, ref_ofac=False )
    if perturbtype == "PerturbRMS":
        logger.log(logging.INFO, "Perturbing RMSD.")
        myinput.setMACM_PROT("CUSTOM")
        myinput.addMACM(ref_rota=True, ref_tran=True, ref_bfac=True,
         ref_vrms=False, ref_cell=False, ref_ofac=False )
    if perturbtype == "PerturbCelldim":
        logger.log(logging.INFO, "Perturbing cell dimensions.")
        myinput.setMACM_PROT("CUSTOM")
        myinput.addMACM(ref_rota=True, ref_tran=True, ref_bfac=True,
         ref_vrms=True, ref_cell=False, ref_ofac=False )
    for seqfname in seqfnames:
        myinput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
    myinput.setMUTE( True )
    mrrun = runMR_AUTO(myinput)
    if mrrun.Success():
        open(root + ".log","w").write(mrrun.logfile())
        if mrrun.foundSolutions() :
            logger.log(logging.INFO, "Phaser has found MR solutions using perturbation %s\n" %perturbation )
            open(root + ".sol","w").write(mrrun.getDotSol().unparse())
            return mrrun.getDotSol()
        else:
            logger.log(logging.INFO, "Phaser has not found any MR solutions")
    else:
        logger.log(logging.INFO, mrrun.ErrorName(), "ERROR :", mrrun.ErrorMessage() )
    return []




def CalcCCFromMRsolution(targetpdbid, MRfolder, calclabel, workdir, partmodels,
  MRDict, mylock=None, nthreads=1, bOccRefine=False, bCCresolutionShells = True,
  OCCresidwindows=(), debug = False, RMSDrefine = False, FsolBsol=None, nmaxsols=1,
  xtriageres = None, killtime = 400000, perturbstr = "", perturbation=0.0, bCCwithFcalc=False,
  bDoPertMR = False, perturbtype="", bDoRNP=True, RotTraRefine=False,
  out=sys.stdout, logger=nulllogger):
    print (time.asctime( time.localtime(time.time())) + "\n\n", file=out)
    calcdir = os.getcwd()
    CCdict = {}
    MRpdbensdict = {}
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    MRfolder = PosixSlashFilePath(MRfolder)
    MRfolder = os.path.relpath(MRfolder, os.getcwd())
    MRfolder = RecaseFilename(MRfolder)
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    mrinputs = glob.glob(os.path.join(MRfolder, calclabel + "*MRinput.txt")) \
              + glob.glob(os.path.join(MRfolder, "*MRinput.txt"))
    mrinput = open(mrinputs[0], "r").read()
    mrinput = mrinput.replace("\\","/")
    mtzfname = re.findall(r"^HKLIN\s+(\S+)", mrinput, re.MULTILINE)[0] # to get rid of any \r character
# if filepath generated on windows but now running on unix then change \ to /

# get ensemble name and sequence file name from some phaser input file
    MRpdbens = re.findall(r"^ENSEMBLE\s+(\S+)\s+PDB\s+(\S+)\s+IDENTITY\s+(\S+)\s+",
      mrinput, re.MULTILINE)
    for ens in MRpdbens:
        MRpdbensdict[ens[0]] = (RecaseFilename(ens[1]), float(ens[2]), "isSeqID")
    MRpdbensRms = re.findall(r"^ENSEMBLE\s+(\S+)\s+PDB\s+(\S+)\s+RMS\s+(\S+)\s+",
      mrinput, re.MULTILINE)
    for ens in MRpdbensRms:
        MRpdbensdict[ens[0]] = (RecaseFilename(ens[1]), float(ens[2]), "isRMS")

    seqfnames = re.findall(r"^COMPOSITION PROTEIN SEQUENCE\s+(\S+)\s+NUM.+",
      mrinput, re.MULTILINE)
# because strings are immutable we iterate with int index
# rather than list elements in order to change the strings
    for i in range(len(seqfnames)):
        seqfnames[i] = RecaseFilename(seqfnames[i])

    (Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI, exceptmsg) = SimpleFileProperties.GetMtzColumns(mtzfname)
    myinput = InputMR_DAT()
    #mtzfname = PosixSlashFilePath(mtzfname)
    mtzfname= RecaseFilename(mtzfname)
    mtzobj = mtz.object(mtzfname)
    xtalsym = mtzobj.crystals()[0].crystal_symmetry()

    myinput.setHKLI(mtzfname)
    if Icol:
# use intensity columns if present
        print ("Using columns: " + str((Icol, Sigcol)) + "\n", file=out)
        myinput.setLABI_I_SIGI(Icol, Sigcol)
    else:
        myinput.setLABI_F_SIGF(Fcol, Sigcol)
        print ("Using columns: " + str((Fcol, Sigcol)) + "\n", file=out)

    myinput.setMUTE(debug==False)
    datrun = runMR_DAT(myinput)

    if datrun.Success():
        pklfname = calclabel + "{" + workdir + "}" + partmodels + ".pkl"
        pklfname = os.path.join(MRfolder, pklfname)
        solfname = calclabel + "{" + workdir + "}" + partmodels + ".sol"
        solfname = os.path.join(MRfolder, solfname)
        rnpsols = None
        fname = ""
        if os.path.exists(pklfname):
            fname = pklfname
            rnpobj = pickle.loads(BLASTMRutils.TryRawRead([pklfname])) #, ziparchive=ziparx))
            rnpsols = rnpobj.getDotSol()
        else:
            if os.path.exists(solfname):
                fname = solfname
                mrparser = phaser.test_phaser_parsers.SolutionFileParser(open(solfname, "rt"))
                rnpsols = mrparser.get_dot_sol()

        if not rnpsols or not fname:
            logger.log(logging.INFO, "Sadly no solutions were found. Done\n")
            return {}

        mstr = "Calculating LLGs from %s\n" %fname
        if perturbstr:
            mstr = "Perturbing solutions and LLGs in %s\n" %fname
        print(mstr, file=out)
        logger.log(logging.INFO, mstr)

        myano = InputANO()
        myano.setSPAC_HALL(datrun.getSpaceGroupHall())
        myano.setCELL6(datrun.getUnitCell())
        myano.setREFL_DATA(datrun.getDATA())
        myano.setMUTE(debug==False)
        rano = phaser.runANO( myano )
        if not rano.Success():
            mstr = rano.ErrorName() + "ANO ERROR: " + rano.ErrorMessage() + "\n"
            print(mstr, file=out)
            #return stroutput

        #ziparx = zipfile.ZipFile(parent.zipfname) if parent.zipfname else None
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        bestvrmsdic = {} # make a dictionary of vrms values of topmost solution with ensemblename as keys
        for vitem in rnpsols[0].NEWVRMS.items():
            bestvrmsdic[vitem[0]] = vitem[1][0]

        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if RMSDrefine:
            for ens in MRpdbens + MRpdbensRms:
                vrms = bestvrmsdic.get(ens[0], None)
                if vrms:
                    MRpdbensdict[ens[0]] = (RecaseFilename(ens[1]), bestvrmsdic[ens[0]] , "isRMS")

        if bDoPertMR and perturbtype:
            mrroot = os.path.join(MRfolder, calclabel + perturbstr + "CalcMR")
            rnpsols = RunMRphaser(datrun, rano, seqfnames, MRpdbensdict,
                                 mrroot, perturbation, perturbtype, nthreads, nmaxsols,
                                 killtime, logger)
# calculate the ELLG using default rmsd estimate from seqid
        ellginput = InputMR_ELLG()
        ellginput.setSPAC_HALL(datrun.getSpaceGroupHall())
        ellginput.setCELL6(datrun.getUnitCell())
        ellginput.setREFL_DATA(datrun.getDATA())
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for ens in MRpdbens:
            ellginput.addENSE_PDB_ID(ens[0], RecaseFilename(ens[1]), float(ens[2]))
            ellginput.addSEAR_ENSE_NUM(ens[0], 1)
        for seqfname in seqfnames:
            ellginput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
        ellginput.setMUTE(debug==False)
        runellg = runMR_ELLG(ellginput)
        print(runellg.logfile(),file=out)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for k,ens in enumerate(MRpdbens):
# append expected ELLGs of each component to the list
            #import code; code.interact(local=locals())
            ellg = runellg.get_map_ellg_full_resolution()[ens[0]]
            CCdict["ELLGens%s%d" %(perturbstr, k)] = Roundoff(ellg, 3)

# make map of targetpdb with its own mtz file by doing RNP with 0 cycles with an origin solution
        rnptargetrootname = targetpdbid + "_map"
        tmpname = targetpdbid + ".pdb"
        #tmpname2 = PosixSlashFilePath(tmpname)
        targetpdb = RecaseFilename(tmpname)
        if os.path.exists(rnptargetrootname + ".1.mtz") == False:
            print(CreateZeroRNPcycleMap(datrun, rano, rnptargetrootname,
              targetpdb, seqfnames, 0.1, debug = debug), file=out)

        mstr += "Calculating correlation coefficients and MLADs of %s with %d solutions\n" \
          %(os.path.relpath(fname, os.getcwd()), len(rnpsols))
        print(mstr, file=out)
        logger.log(logging.INFO, mstr)

# set up a temp folder for getCC
        tempdir = tempfile.gettempdir()
        if tempdir==os.getcwd():
          tempdir = os.path.expanduser("~") # i.e. the user's home directory
        ccdir = targetpdbid + "_" + workdir + "_"+ partmodels
        CCtemp = os.path.join(tempdir, "CCTEMP", ccdir)
        if not os.path.exists(CCtemp):
          os.makedirs(CCtemp)

        ccroot = os.path.join(MRfolder, calclabel + "CalcCC_")
        dfname = ccroot + "{" + workdir + "}" + partmodels + "_dict.txt"

        nsols = len(rnpsols)
        for (j, rnpsol) in enumerate(rnpsols):
            if j >= nmaxsols:
              break
            i = j+1
            mstr = "\n" + "/"*80 +"\nRNP solution %d out of %d\n" %(i, nsols) + "/"*80 + "\n"
            print(mstr, file=out)
            logger.log(logging.INFO, mstr)
            #root = os.path.join(MRfolder, calclabel + "CalcCC_%d" %i)
            root = os.path.join(MRfolder, calclabel + perturbstr + "CalcCC")
            root = os.path.relpath(root, os.getcwd())
            mtzfname = root + ".1.mtz"

            myinput = InputMR_RNP()
            myinput.setNORM_DATA(rano.getSigmaN())
            myinput.setMACA_PROT("OFF")

            if RotTraRefine and RMSDrefine:
                myinput.setMACM_PROT("CUSTOM")
                myinput.addMACM(ref_rota=True, ref_tran=True, ref_bfac=True,
                 ref_vrms=True, ref_cell=False, ref_ofac=False )
            if RotTraRefine and not RMSDrefine:
                myinput.setMACM_PROT("CUSTOM")
                myinput.addMACM(ref_rota=True, ref_tran=True, ref_bfac=True,
                 ref_vrms=False, ref_cell=False, ref_ofac=False )
            if RMSDrefine and not RotTraRefine:
                myinput.setMACM_PROT("CUSTOM")
                myinput.addMACM(ref_rota=False, ref_tran=False, ref_bfac=True,
                 ref_vrms=True, ref_cell=False, ref_ofac=False )
            if not RMSDrefine and not RotTraRefine:
                myinput.setMACM_PROT("OFF")

            myinput.setSPAC_HALL(datrun.getSpaceGroupHall())
            myinput.setCELL6(datrun.getUnitCell())
            myinput.setREFL_DATA(datrun.getDATA())
            myinput.setROOT(root)
            myinput.setXYZO(True)
            myinput.setHKLO(True)

            solstr = rnpsol.unparse()
            tstr = "\n\n" + "/"*80 + "\n\nUsing solution:\n" + solstr + "\n"
            logstr = tstr
            logstr += "(Fsol, Bsol): " + str(FsolBsol)
            if FsolBsol:
              myinput.setSOLP_BULK_USE(True)
              myinput.setSOLP_BULK_FSOL(FsolBsol[0])
              myinput.setSOLP_BULK_BSOL(FsolBsol[1])

            myinput.setJOBS(nthreads)
            myinput.setMUTE(debug==False)
            solu = phaser.mr_solution([rnpsol], rnpsols.resolution())
            myinput.setSOLU(solu)
            myinput.setOUTP_LEVE("VERBOSE")
            logstr += "\nComposition: \n"
            for seqfname in seqfnames:
                myinput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
                logstr += " %s\n" %seqfname

            #import code; code.interact(local=locals())
            if bOccRefine:
                occinput = InputMR_OCC()
                occinput.setNORM_DATA(rano.getSigmaN())
                occinput.setMACA_PROT("OFF")
                #occroot = os.path.join(MRfolder, calclabel + "CalcOCC_%d" %i)
                occroot = os.path.join(MRfolder, calclabel + perturbstr + "CalcOCC")
                occroot = os.path.relpath(occroot, os.getcwd())
                occmtzfname = os.path.relpath(occroot + ".1.mtz", os.getcwd())
                occinput.setROOT(occroot)
                #occinput.setMACA_PROT("OFF")
                #occinput.setMACA_PROT("ON")
                occinput.setSPAC_HALL(datrun.getSpaceGroupHall())
                occinput.setCELL6(datrun.getUnitCell())
                occinput.setREFL_DATA(datrun.getDATA())
                occinput.setSOLU(solu)
                occinput.setXYZO(True)
                occinput.setHKLO(True)
                occinput.setJOBS(nthreads)
                occinput.setMUTE(debug==False)
                occlogstr = logstr
                occlogstr += "\nComposition: \n"
                for seqfname in seqfnames:
                    occinput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
                    occlogstr += " %s\n" %seqfname

            lastmodelname = ""
            lastsolcomponent = ""
            lastvrms = 0.0

            ensvrmsdic = {} # make a dictionary of vrms values with ensemblename as keys
            for vitem in rnpsol.NEWVRMS.items():
                ensvrmsdic[vitem[0]] = vitem[1][0]

            ensset = set([])
            EnseulerFracs = Sol2EulerFrac(solstr)
            k=0 # enumerating (MRpdbens + MRpdbensRms) would mismatch the index of the current ensemble
            pertval = perturbation
            if bDoPertMR:
                pertval = 0.0
            for enseulerfrac in EnseulerFracs:
                logger.log(logging.INFO, "Ensemble: %s" %enseulerfrac[0])
                vrms = ensvrmsdic.get(enseulerfrac[0], -1.0 ) * (1.0 + pertval)
                if vrms < 0.0 and RMSDrefine:
                    logstr += "No VRMS value for ensemble %s. Continuing with next solution\n" %enseulerfrac[0]
                    continue
# find filename of the pdb file with this ensemble name from the MR input file
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                for ens in (MRpdbens + MRpdbensRms):
                    if ens[0] == enseulerfrac[0]:
                        k += 1
                        mfname = MRpdbensdict[ens[0]][0]
                        modelfname = mfname.replace("\\", os.path.sep).strip() # to get rid of any \r character
                        #modelfname = PosixSlashFilePath(modelfname)
                        modelfname = RecaseFilename(modelfname)
                        if "isRMS" == MRpdbensdict[ens[0]][2]:
                            vrms = MRpdbensdict[ens[0]][1]
                        else: # must be seqID value
                            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                            nresidues = SimpleFileProperties.GetNumberofResiduesinPDBfile(MRpdbensdict[ens[0]][0])
                            vrms = SimpleFileProperties.NeweVRMS(MRpdbensdict[ens[0]][1], nresidues)
# prepare for refinement by adding all components
                        if enseulerfrac[0] not in ensset: # don't over-define the same ensemble
                            myinput.addENSE_PDB_RMS(ens[0], modelfname, vrms)
                        ensset.add(enseulerfrac[0])
                        logstr += "Ensemble: %s using %s with vrms: %s, perturbation: %f\n" \
                         %(enseulerfrac[0], modelfname, Roundoff(vrms,3), pertval)
# create map of this component from RNP 0 cycles with phaser
                        lastroot = os.path.join(MRfolder, calclabel + perturbstr + ens[0] + "." + str(i))
                        lastroot = os.path.relpath(lastroot, os.getcwd())
                        curpdbfname = lastroot +".pdb"
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        print(RealizeSoluPDB(xtalsym, modelfname, enseulerfrac[1],
                         enseulerfrac[2], curpdbfname), file=out)
                        if bOccRefine:
# add transformed model to the OCC refinement by using its transformed coordinates
                            occinput.addENSE_PDB_RMS(ens[0], modelfname, vrms)
                            occlogstr += "Ensemble: %s using %s with vrms: %s\n" \
                             %(enseulerfrac[0], modelfname, Roundoff(vrms,3))
                        llgs = []
                        print(CreateZeroRNPcycleMap(datrun, rano, lastroot,
                          curpdbfname, seqfnames, vrms, overwrite=True, llgref=llgs,
                          debug = debug), file=out)
                        print("LLG of %s component: %f\n" %(ens[0], llgs[0]), file=out)
                        if not perturbtype: # not interested in CCs if doing perturbation runs
# get CC with that map and target map
                            lastrootmtz = os.path.relpath(lastroot + ".1.mtz", os.getcwd())
                            outstr, cc = getCC(rnptargetrootname + ".1.mtz", lastrootmtz, CCtemp)
                            CCdict["CCwt_ensSol%s%d_%d" %(perturbstr,j,k)] = cc[0]
                            print(out, outstr, file=out)
                            if bCCwithFcalc:
                                outstr, ccfc = getCC(rnptargetrootname + ".1.mtz", lastrootmtz, CCtemp,
                                  F1="FC", Phi1="PHIC", F2="FC", Phi2="PHIC")
                                print(outstr, file=out)
                                CCdict["CCfc_ensSol%s%d_%d" %(perturbstr,j,k)] = ccfc[0]
                            # calculate CC for interval with reliable SigmaAs
                            IsigIgrt3res = 1.0 + float(xtriageres[0])
                            resshell = [5.0, IsigIgrt3res]
                            if bCCwithFcalc:
                                outstr, cc = getCC(rnptargetrootname + ".1.mtz", lastrootmtz,
                                 CCtemp, F1="FC", Phi1="PHIC", F2="FC", Phi2="PHIC", rshell=resshell)
                                CCdict["CCgoodSigmaASol%s%d_%d" %(perturbstr,j,k)] = cc[0]
                                print(outstr, file=out)
                            print("\n",file=out)

                            modlcompl = MRDict[ens[0]+"_modelcompleteness"]
                            mtz1obj = mtz.object(rnptargetrootname + ".1.mtz")
                            maxminres = mtz1obj.max_min_resolution()
                            nrefl = mtz1obj.n_reflections()
                            reflres = Nrefl2array(maxminres[1], maxminres[0], nrefl)
                            ln = len(reflres)
                            ncalcs = 10.0
                            step = ln/ncalcs
                            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                            if j==0 and bCCresolutionShells: # only for topmost solutions
                                for m in range(int(ncalcs)):
                                    n = int(round(m*step))
                                    if n >= ln:
                                        break
                                    reflres[n]
                                    resshell = [reflres[n][1], reflres[n][2]]
                                    avginvres2 = reflres[n][4]
                                    SigmaA = math.sqrt(modlcompl)*math.exp(-2.0*math.pow(math.pi*vrms, 2)*avginvres2/3.0)
                                    outstr, cc = getCC(rnptargetrootname + ".1.mtz", lastrootmtz,
                                     CCtemp, F1="FC", Phi1="PHIC", F2="FC", Phi2="PHIC", rshell=resshell)
                                    CCdict["ResolushellSol%s%d_%d" %(perturbstr,j,m)] = Roundoff(resshell,4)
                                    CCdict["CCshellSol%s%d_%d" %(perturbstr,j,m)] = cc[0]
                                    CCdict["Avginvres2Sol%s%d_%d" %(perturbstr,j,m)] = Roundoff(avginvres2, 5)
                                    CCdict["SigmaAshellSol%s%d_%d" %(perturbstr,j,m)] = Roundoff(SigmaA, 5)
                                    CCdict["AvgresshellSol%s%d_%d" %(perturbstr,j,m)] = Roundoff(reflres[n][3], 5)
                                    CCdict["NreflshellSol%s%d_%d" %(perturbstr,j,m)] = int(reflres[n][0])
                                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                                    print(outstr, file=out)
                                    print("\n",file=out)
    # calculate CC between target and pdbfile of last component
                            outstr, ccpdb = getCC(rnptargetrootname + ".1.mtz",
                              curpdbfname, CCtemp)

                            CCdict["CCpdbSol%s%d_%d" %(perturbstr,j,k)] = ccpdb[0]
                            print(outstr, file=out)

                        altobj = AlterOriginSymmate.AltOrigSymmates(fixpdb=curpdbfname,
                          movpdb=targetpdb, writepdbfiles=False)
                        mstr, AllAltOrigPdbFnames, altorigpdbfname = altobj.GetAltOrigSymmates(logger=logger)
                        CCdict["MLADensSol%s%d_%d" %(perturbstr,j,k)] = Roundoff(AllAltOrigPdbFnames[0][0],4)
                        CCdict["LLGensSol%s%d_%d" %(perturbstr,j,k)] = Roundoff(llgs[0],3)
                        print(mstr, file=out)
                        print("\n", file=out)
                        if not debug:
                            files = glob.glob(lastroot + "*")
                            for f in files:
                                try:
                                    os.remove(f)
                                except Exception as m:
                                    print(traceback.format_exc(), file=out)
                        k+=1
            logger.log(logging.INFO, logstr)

            if bDoRNP:
                llgrun = runMR_RNP(myinput)
    # get CC of map of all components against target map
                llgall = None
                CCtotalmap = [None, None]
                CCfctotalmap = [None, None]
                CCtotalpdb = [None, None]
                open(root + ".log","a").write(logstr + "\n" +llgrun.verbose())
                if llgrun.Success():
                    sols = llgrun.getDotSol()
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    CCdict["UsedReflections"] = llgrun.getUsedReflections()
                    # append each RNP solutions to one big solution file as if we ran RNP on them all in one go
                    open(root + ".sol","a").write(sols.unparse() + "\n")

                    pstr= pickle.dumps(llgrun)
                    open(root + ".pkl","wb").write(pstr)

                    llgall = Roundoff(llgrun.getValues()[0], 3)
                    CCdict["LLGallSol%s%d" %(perturbstr,j)] = llgall
                    outstr, CCtotalmap = getCC(rnptargetrootname + ".1.mtz", mtzfname, CCtemp)
                    CCdict["CCwt_globalSol%s%d" %(perturbstr,j)] = CCtotalmap[0]
                    print(outstr, file=out)
                    if bCCwithFcalc:
                        outstr, CCfctotalmap = getCC(rnptargetrootname + ".1.mtz", mtzfname, CCtemp,
                          F1="FC", Phi1="PHIC", F2="FC", Phi2="PHIC")
                        print(outstr, file=out)
                        CCdict["CCfc_globalSol%s%d" %(perturbstr,j)] = CCfctotalmap[0]
                    localcclst = []
                    outstr, CCtotalpdb = getCC(rnptargetrootname + ".1.mtz", root + ".1.pdb", CCtemp, localcclst)
                    CCdict["CC_globalpdbSol%s%d" %(perturbstr,j)] = CCtotalpdb[0]
                    print(outstr, file=out)
                    print("LLG of all components: %f\n" %llgall, file=out)
                    print("CCmap, CCpdb of all components: %s,  %s\n\n" %(CCtotalmap[0], CCtotalpdb[0]), file=out)
                    for p,known in enumerate(sols[0].KNOWN):
                        CCdict["OfacensSol%s%d_%d" %(perturbstr,j,p)] = Roundoff(known.OFAC,4)
    # calculate a very expected ELLG now using vrms as input
                    if RMSDrefine:
                        vellginput = InputMR_ELLG()
                        vellginput.setSPAC_HALL(datrun.getSpaceGroupHall())
                        vellginput.setCELL6(datrun.getUnitCell())
                        vellginput.setREFL_DATA(datrun.getDATA())
                        for v in sols[0].NEWVRMS.items():
                            (modelfname, dummy, dummy2) = MRpdbensdict[v[0]]
                            vellginput.addENSE_PDB_RMS(v[0], modelfname, float(v[1][0]))
                            vellginput.addSEAR_ENSE_NUM(v[0], 1)

                        for seqfname in seqfnames:
                            vellginput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
                        vellginput.setMUTE(debug==False)
                        runellg = runMR_ELLG(vellginput)
                        print(runellg.logfile(), file=out)
                        for h,ens in enumerate(MRpdbens):
        # append expected ELLGs of each component to the list
                            #import code; code.interact(local=locals())
                            ellg = runellg.get_map_ellg_full_resolution()[ens[0]]
                            CCdict["vrmsELLGensSol%s%d_%d" %(perturbstr,j,h)] = Roundoff(ellg, 3)
                else:
                    mstr = llgrun.ErrorName() + " runMR_RNP ERROR: " + llgrun.ErrorMessage() + "\n"
                    #raise Exception, m
                    print(mstr, file=out)
                    logger.log(logging.INFO, mstr)
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

                occllgall = None
                if bOccRefine:
                    def runandstateOCC(inpt,myroot):
                        mstr = "Calculating occupancy refinement\n" + myroot + ".1.pdb"
                        print(mstr, file=out)
                        logger.log(logging.INFO, mstr)
                        myrun = runMR_OCC(inpt)
                        if myrun.Success():
                            sols = myrun.getDotSol()
                            open(myroot + ".sol","a").write(sols.unparse() + "\n")
                        return myrun

                    occrun = runandstateOCC(occinput, occroot)
    # get CC of map of all components against target map
                    OCCtotalmap = [None, None]
                    OCCtotalpdb = [None, None]
                    occwindres = None
                    occllgall = None
                    usednres = None
                    #open(occroot + ".log","w").write(occlogstr + "\n" + occrun.logfile())
                    open(occroot + ".log","a").write(occlogstr + "\n" + occrun.logfile())
                    if occrun.Success():
                        occllgall = Roundoff(occrun.getValues()[0], 3)
                        CCdict["OCCLLGSol%s%d" %(perturbstr,j)] = occllgall
                        outstr, OCCtotalmap = getCC(rnptargetrootname + ".1.mtz", occmtzfname, CCtemp)
                        CCdict["CCwt_OCCSol%s%d" %(perturbstr,j)] = OCCtotalmap[0]
                        print(outstr,file=out)
                        outstr, CCfctotalmap = getCC(rnptargetrootname + ".1.mtz", occmtzfname, CCtemp,
                          F1="FC", Phi1="PHIC", F2="FC", Phi2="PHIC")
                        CCdict["CCfc_OCCSol%s%d" %(perturbstr,j)] = CCfctotalmap[0]
                        print(outstr,file=out)
                        localcclst = []
                        outstr, OCCtotalpdb = getCC(rnptargetrootname + ".1.mtz", occroot + ".1.pdb", CCtemp, localcclst)
                        CCdict["CCpdb_OCCSol%s%d" %(perturbstr,j)] = OCCtotalpdb[0]
                        open(occroot + "localCCSol%d.txt"%j, "w").write(str(localcclst[0]))
                        usednres = occrun.get_occupancy_nresidues()
                        CCdict["OCCnresidSol%s%d" %(perturbstr,j)] = usednres
                        occwindres = Roundoff(occrun.get_occupancy_window_ellg(), 3)
                        CCdict["OCCWindowELLGSol%s%d" %(perturbstr,j)] = occwindres
                        print(outstr,file=out)
    # append overall CCocc, ELLG for used window of residues, LLG of OCCrefinement, size of residuewindow
                    #CCocc = [(OCCtotalmap[0], OCCtotalpdb[0], occwindres, occllgall, usednres)]
    # set suggested window of residues used by the OCC ccalculation
                    for nres in OCCresidwindows:
                        occnresroot = occroot + "_nres%d" %nres
                        occmtzfname = os.path.relpath(occnresroot + ".1.mtz", os.getcwd())
                        occinput.setROOT(occnresroot)
                        occinput.setOCCU_WIND_NRES(nres)
                        occrun = runandstateOCC(occinput, occroot)
                        OCCtotalmap = [None, None]
                        OCCtotalpdb = [None, None]
                        occwindres = None
                        occllgall = None
                        usednres = None
                        open(occnresroot + ".log","w").write(occlogstr + "\n" + occrun.logfile())
                        if occrun.Success():
                            occllgall = Roundoff(occrun.getValues()[0], 3)
                            outstr, OCCtotalmap = getCC(rnptargetrootname + ".1.mtz", occmtzfname, CCtemp)
                            print(outstr, file=out)
                            localcclst = []
                            outstr, OCCtotalpdb = getCC(rnptargetrootname + ".1.mtz", occnresroot + ".1.pdb", CCtemp, localcclst)
                            open(occnresroot + "localCC.txt", "w").write(str(localcclst[0]))
                            usednres = occrun.get_occupancy_nresidues()
                            occwindres = Roundoff(occrun.get_occupancy_window_ellg(), 3)
                            print(outstr, file=out)
                            print("nres, CCmap, CCpdb of all OCC refined components: %d,  %s,  %s\n\n" \
                              %(nres, OCCtotalmap[0], OCCtotalpdb[0]), file=out)

                #open(dfname,"w").write(str(CCdict))
            if not debug:
                files = []
                files.extend(glob.glob(root + "*.mtz"))
                files.extend(glob.glob(root + "*.pdb"))
                files.extend(glob.glob("MinMLAD_*.pdb"))
                for f in files:
                    try:
                        os.remove(f)
                    except Exception as m:
                        print(traceback.format_exc(), file=out)

            if os.path.exists(CCtemp): # clean up just in case a previous calculation crashed
                try:
                    shutil.rmtree(CCtemp)
                except Exception:
                    print(traceback.format_exc(), file=out)
        # merge dictionary with existing old one one
        existingdict = {}
        if os.path.exists(dfname):
            mfile = open(dfname,"r")
            existingdict = eval(mfile.read())
            mfile.close()
        accumdict = existingdict.update(CCdict)
        open(dfname,"w").write(str(accumdict))
    else:
        print(datrun.ErrorName() + " MR_DAT ERROR: " + datrun.ErrorMessage() + "\n", file=out)
    return  MRpdbensdict




def CalcCCFromSCOP_MRcalcs(selectedpdbfname, scoppdbsfname, calcdir):
    mfile = open(selectedpdbfname,"r")
    selectedpdbs = eval(mfile.read())
    mfile.close()

    mfile = open(scoppdbsfname,"r")
    scoppdbs = eval(mfile.read())
    mfile.close()

    logfname = os.path.join(calcdir, os.path.basename(selectedpdbfname) + ".log")

    n = 0
    l = len(selectedpdbs)
    for (n,pdbs) in enumerate(selectedpdbs):
        mstr = ""
        for scoppdb in scoppdbs:
            if scoppdb[0].lower().find(pdbs[0].lower()) > -1:
                scopid = scoppdb[1]
# only want directories named with SCOP fold id
                if scopid == "UnSCOPed":
                    foldID = "UnSCOPed"
                else:
                    foldID = scopid.split(".")[0] + "." + scopid.split(".")[1]

                #foldID = scopid.split(".")[0] + "." + scopid.split(".")[1]
                fold_targetpdb = os.path.join(calcdir,foldID,pdbs[0])
                if not os.path.exists(fold_targetpdb):
                    fold_targetpdb = os.path.join(calcdir,foldID,pdbs[0].lower())

                BLASTtxtchainfname =  os.path.join(fold_targetpdb, "Strider" + pdbs[0] + "_BLASTchain_ids.txt")
                psearches = eval(open(BLASTtxtchainfname,"r").read())

                mstr += "CalcCCFromMRcalcs %s, %s, %d. out of %d\n" %(fold_targetpdb, pdbs[0], n, l)

                for psearch in psearches:
                    workdir = psearch[1][-1] # the last element in ['2WBJ_C0', '2WBJ_C0,1NFD_B0']
                    for partmodels in psearch[1]:

                        MRfolder = os.path.join(fold_targetpdb,"MR", workdir)

                        try:
                            os.chdir(fold_targetpdb)
                            mstr += "Calculating CCs from %s\n" %solfname
                            mstr += CalcCCFromMRsolution(pdbs[0], MRfolder, "Strider", workdir,
                              partmodels)

                        except Exception as m:
                            mstr += "Error in %s, %s, %s\n" %(selectedpdbfname, scoppdbsfname, m)
                            #raise

                        finally:
                            os.chdir(calcdir)

        logfile = open(logfname,"a")
        logfile.write(mstr)
        logfile.close()

    mstr = "Done with " + selectedpdbfname + "\n"
    logfile = open(logfname,"a")
    logfile.write(mstr)
    logfile.close()




def CalcRNP_FromMRcalcs(pdbid, calclabel):
    mstr = ""

    BLASTtxtchainfname = os.path.join(pdbid, calclabel + pdbid + "_BLASTchain_ids.txt")
    if os.path.exists(BLASTtxtchainfname):
        psearches = eval(open(BLASTtxtchainfname,"r").read())

        mstr += "CalcCCFromMRcalcs %s\n" %pdbid
        os.chdir(pdbid)
        for psearch in psearches:
            workdir = psearch[1][-1] # the last element in ['2WBJ_C0', '2WBJ_C0,1NFD_B0']
            partmodels = workdir # on this occassion we're only interested in multicomponent refinements

            MRfolder = os.path.join("MR", workdir)
            templdir = os.path.join("TemplateSolutions", workdir)

            try:
                #print "MRfolder= %s, partmodels = %s" %(MRfolder, partmodels)
                mstr += CalcCCFromMRsolution(pdbid, MRfolder, calclabel, workdir,
                  partmodels,  nthreads=6)

                mstr += CalcCCFromMRsolution(pdbid,
                    templdir, calclabel + "RNP", workdir, ".superposed")

            except Exception as m:
                mstr += "Error in %s, %s\n" %(pdbid, m)
                print("Error in %s, %s\n" % (pdbid, m))
                #raise

    else:
        mstr += "Error: %s doesn't exist" %BLASTtxtchainfname
        print("Error: %s doesn't exist" % BLASTtxtchainfname)

    logfname = os.path.basename(pdbid) + "CalcCC.log"
    logfile = open(logfname,"a")
    logfile.write(mstr)
    mstr = "Done with " + pdbid + "\n"
    logfile.write(mstr)
    logfile.close()




if __name__=="__main__":
    if len(sys.argv) > 3:
        CalcCCFromSCOP_MRcalcs(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        CalcRNP_FromMRcalcs(sys.argv[1], sys.argv[2])

    print("Done for today")
