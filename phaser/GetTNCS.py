from __future__ import print_function

import os, sys, os.path, shutil, random, tempfile
from phaser import *
from phaser.utils2 import *
from phaser import SimpleFileProperties
import iotbx.pdb
import sqlite3
from mmtbx.ncs import tncs
from iotbx import reflection_file_converter
from iotbx.pdb.fetch import get_pdb
from iotbx import mtz
from boost_adaptbx.boost import rational
from decimal import *
from fractions import Fraction
from libtbx import easy_mp
from mmtbx.scaling import xtriage
from io import StringIO

def FindTNCS(mtzfname, modelfname, pdbid, getTNCSinPDB, getXtriageValues,
               getTNCSfromPhaser, getOtherStuff):
  resultsdict = {}
  resultsdict["pdbid"] = ( pdbid, "text" )

  try:
    if getOtherStuff:
      pointgrouporder, spacegrouporder, spginfo, errstr = GetOtherProperties(mtzfname)
      resultsdict["pointgrouporder"] = ( pointgrouporder, "int")
      resultsdict["spacegrouporder"] = ( spacegrouporder, "int")
      resultsdict["Spacegroup"] = ( spginfo, "text" )
      if errstr:
        resultsdict["Error"] = ( errstr, "text")

    if getXtriageValues:
      (nrefl, IsigIgrt3res, hires, lowres, noutlier_outside_rescut, nicerings,
        twinLtest, NonOrigPattersonHeight, errstr) = GetXtriageResults(mtzfname)

      resultsdict["IsigIgrt3res"] = ( IsigIgrt3res, "real")
      resultsdict["Hires"] = ( hires, "real")
      resultsdict["Lowres"] = ( lowres, "real")
      resultsdict["Noutlier_outside_rescut"] = ( noutlier_outside_rescut, "int")
      resultsdict["Nicerings"] = ( nicerings, "int")
      resultsdict["Nreflections"] = ( nrefl, "int")
      resultsdict["TwinLtest"] = ( twinLtest, "real")
      resultsdict["NonOrigPattersonHeight"] = ( NonOrigPattersonHeight, "real")
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      if errstr:
        resultsdict["Error"] = ( errstr, "text")

    if getTNCSfromPhaser or ( modelfname and getTNCSinPDB ):
      xtal = iotbx.pdb.input(file_name= modelfname )
      hroot = xtal.construct_hierarchy()
      HasNucleicAcid = False
      notproteinchains = ""
      for c in hroot.models()[0].chains():
        if c.is_na():
          HasNucleicAcid = True
          #break
        if c.is_protein() == False:
          notproteinchains += c.id
      resultsdict["PDBwithNA"] = ( HasNucleicAcid, "int" )
      resultsdict["PDBnotProteinChains"] = ( notproteinchains, "text" )

    if getTNCSfromPhaser:
      meanmtz = None
      datinput = InputMR_DAT()
      cols = SimpleFileProperties.GetMtzColumnsTuple( mtzfname )
      columnsused = None
      if len(cols[2]) and len(cols[1]): # use mean intensities if present
        datinput.setLABI_I_SIGI( cols[2][0], cols[1][0] )
        columnsused = cols[2][0] + ", " + cols[1][0]
      else:  # otherwise use mean amplitudes if present
        if len(cols[0]) and len(cols[1]):
          datinput.setLABI_F_SIGF( cols[0][0], cols[1][0] )
          columnsused = cols[0][0] + ", " + cols[1][0]
        else: # otherwise convert anomalous data to mean data if present
          # get random filename for temporary mean mtz file
          meanmtz = next(tempfile._get_candidate_names()) + "_" + os.path.basename(mtzfname)
          if ( len(cols[8]) + len(cols[9]) + len(cols[10]) + len(cols[11]) ) >= 4:
            # convert I+,I- to mean I, SigI
            labels = '--label=' + cols[8][0] + ','+ cols[10][0] + ','+ cols[9][0] + ','+ cols[11][0]
            cmdargs = ['--non-anomalous',
                       mtzfname,
                       labels,
                       '--mtz=' + meanmtz
                      ]
            res = reflection_file_converter.run(cmdargs)
            I = cols[8][0][:-3]
            SigI = cols[10][0][:-3]
            datinput.setLABI_I_SIGI( I, SigI )
            columnsused = cols[8][0] + ','+ cols[10][0] + ','+ cols[9][0] + \
               ','+ cols[11][0] + ' --> ' + I + ',' + SigI
          else:
            if ( len(cols[4]) + len(cols[5]) + len(cols[6]) + len(cols[7]) ) >= 4:
              # convert F+,F- to mean F, SigF
              labels = '--label=' + cols[4][0] + ','+ cols[6][0] + ','+ cols[5][0] + ','+ cols[7][0]
              cmdargs = ['--non-anomalous',
                         mtzfname,
                         labels,
                         '--mtz=' + meanmtz
                        ]
              res = reflection_file_converter.run(cmdargs)
              F = cols[4][0][:-3]
              SigF = cols[6][0][:-3]
              datinput.setLABI_F_SIGF( F, SigF )
              columnsused = cols[4][0] + ','+ cols[6][0] + ','+ cols[5][0] + \
                   ','+ cols[7][0] + ' --> ' + F + ',' + SigF
            else:
              resultsdict["Error"] = ( "No useful columns found in mtz file: " + str(cols[4][3:]), "text")
          #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
          mtzfname = meanmtz
      datinput.setHKLI(mtzfname)

      resultsdict["ColumnsUsed"] = ( columnsused, "text" )
      xtalsym = mtz.object(mtzfname).crystals()[0].crystal_symmetry()
      uc = xtalsym.unit_cell()
      spginfo = xtalsym.space_group_info().symbol_and_number()
      resultsdict["Spacegroup"] = ( spginfo, "text" )
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      datinput.setMUTE( True )
      datrun = runMR_DAT(datinput)
      if datrun.Success():
        myinput = InputNCS()
        myinput.setMACA_PROT("off")
        myinput.setMACT_PROT("off")
        myinput.setTNCS_PATT_HIRE(4)
        myinput.setSPAC_HALL(datrun.getSpaceGroupHall())
        myinput.setCELL6(datrun.getUnitCell())
        myinput.setREFL_DATA( datrun.getDATA() )
        ensemblename = "MR_" + pdbid

        if modelfname:
          count = 0
          firstchainid = hroot.models()[0].chains()[0].id
          for c in hroot.models()[0].chains():
            if c.is_protein:
              count += 1

          resultsdict["MtzFirstPDBchain"] = ( firstchainid, "text" )
          resultsdict["MtzChainCount"] = ( count, "int" )
          myinput.addENSE_PDB_ID_SELECT(ensemblename, modelfname, 100, True, firstchainid )
          myinput.addSEAR_ENSE_NUM( ensemblename, 1 )

        myinput.setMUTE( True )
        myinput.setHKLO( False )
        tncsrun = runNCS(myinput)
        if tncsrun.Success():
          orthTraxyz = uc.orthogonalize(tncsrun.translation_vector())
          #resultsdict["MtzHasTNCS"] = ( tncsrun.hasTNCS(), "int")
          resultsdict["MtzTraVec"] = ( tncsrun.translation_vector(), "text" )
          resultsdict["MtzTraVecRational"] = (
                                ( GetCloseRational(tncsrun.translation_vector()[0])[0],
                                  GetCloseRational(tncsrun.translation_vector()[1])[0],
                                  GetCloseRational(tncsrun.translation_vector()[2])[0] ) ,
                              "text" )
          #resultsdict["MtzRotAngl"] = ( tncsrun.rotation_angles(), "text" )
          resultsdict["MtzAnalysisVec"] = ( str ( [ tncsrun.analysis_vector( i )
                                           for i in range( 0, tncsrun.analysis_number() ) ] ), "text")
          #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
          resultsdict["MtzAnalysisVecRational"] = ( str (
                                       [
                                            (
                                             GetCloseRational( tncsrun.analysis_vector( i )[0] )[0],
                                             GetCloseRational( tncsrun.analysis_vector( i )[1] )[0],
                                             GetCloseRational( tncsrun.analysis_vector( i )[2] )[0]
                                             )
                                       for i in range( 0, tncsrun.analysis_number() )
                                      ]     ), "text")
          resultsdict["MtzOrtAnalysisVec"] = ( str ( [ uc.orthogonalize( tncsrun.analysis_vector(i) )
                                           for i in range( 0, tncsrun.analysis_number() ) ] ), "text")
          resultsdict["NMOL"] = ( str ( [ tncsrun.analysis_nmol( i )
                                           for i in range( 0, tncsrun.analysis_number() ) ] ), "text")
          firstnmol = None
          if tncsrun.analysis_number():
            firstnmol = tncsrun.analysis_nmol( 0 )
          resultsdict["FirstNMOL"] = ( firstnmol, "int" )
          resultsdict["MtzPattersonTop"] = ( tncsrun.getPattersonTop(), "real")
          #resultsdict["TwinAlpha"] = ( tncsrun.getTwinAlpha(), "real" )
        else:
          resultsdict["Error"] = ( "tncsrun error:\n" + tncsrun.ErrorMessage(), "text")
      else:
        resultsdict["Error"] = ( "datrun error:\n" + datrun.ErrorMessage(), "text")
      if meanmtz:
        os.remove( meanmtz )

    if getTNCSinPDB:
      xtalsym = xtal.crystal_symmetry()
      #"""
      #tncsinpdb = tncs.groups(pdb_hierarchy = hroot, crystal_symmetry = xtalsym, quiet=True)
      """
      tncsinpdb = tncs.groups(pdb_hierarchy = hroot, crystal_symmetry = xtalsym,
       quiet=True, angular_difference_threshold_deg=3)
      resultsdict["PdbTncsOrder3"] = ( tncsinpdb.tncsresults[0], "int" )
      resultsdict["PdbTncsChain3"] = ( tncsinpdb.tncsresults[1], "text" )
      resultsdict["PdbTraVec3"] = ( str ( [ e[0] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbOrtTraVec3"] = ( str ( [ e[1] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbAngle3"] = ( str ( [ e[2] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbFracscat3"] = ( tncsinpdb.tncsresults[3], "real" )

      tncsinpdb = tncs.groups(pdb_hierarchy = hroot, crystal_symmetry = xtalsym,
       quiet=True, angular_difference_threshold_deg=10)
      resultsdict["PdbTncsOrder10"] = ( tncsinpdb.tncsresults[0], "int" )
      resultsdict["PdbTncsChain10"] = ( tncsinpdb.tncsresults[1], "text" )
      resultsdict["PdbTraVec10"] = ( str ( [ e[0] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbOrtTraVec10"] = ( str ( [ e[1] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbAngle10"] = ( str ( [ e[2] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbFracscat10"] = ( tncsinpdb.tncsresults[3], "real" )
      """

      tncsinpdb = tncs.groups(pdb_hierarchy = hroot, crystal_symmetry = xtalsym,
       quiet=True, angular_difference_threshold_deg=5)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      resultsdict["PdbTncsOrder5"] = ( tncsinpdb.tncsresults[0], "int" )
      resultsdict["PdbTncsChain5"] = ( tncsinpdb.tncsresults[1], "text" )
      resultsdict["PdbTraVec5"] = ( str ( [ e[0] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbOrtTraVec5"] = ( str ( [ e[1] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbAngle5"] = ( str ( [ e[2] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbFracscat5"] = ( tncsinpdb.tncsresults[3], "real" )

      tncsinpdb = tncs.groups(pdb_hierarchy = hroot, crystal_symmetry = xtalsym,
       quiet=True, angular_difference_threshold_deg=15)
      resultsdict["PdbTncsOrder15"] = ( tncsinpdb.tncsresults[0], "int" )
      resultsdict["PdbTncsChain15"] = ( tncsinpdb.tncsresults[1], "text" )
      resultsdict["PdbTraVec15"] = ( str ( [ e[0] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbOrtTraVec15"] = ( str ( [ e[1] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbAngle15"] = ( str ( [ e[2] for e in tncsinpdb.tncsresults[2] ] ), "text")
      resultsdict["PdbFracscat15"] = ( tncsinpdb.tncsresults[3], "real" )
      #"""
  except Exception as e:
    resultsdict["Error"] = (str(e), "text")
  return resultsdict



def PDBinDBase(pdbid, dbfname):
  conn = sqlite3.connect(database = dbfname)
  row = None
  mydb = conn.cursor()
  mydb.execute('select tbl_name from sqlite_master')
  list_tables = mydb.fetchall()
  for t in list_tables:
    if t[0] == "TNCStbl":
      sqlxprs = "SELECT PDBid FROM TNCStbl WHERE PDBid = '%s'" %pdbid
      mydb.execute(sqlxprs)
      row = mydb.fetchone()
  mydb.close()
  if row:
    return True
  return False


def PDBidsinDBase(dbfname):
  conn = sqlite3.connect(database = dbfname)
  rows = []
  mydb = conn.cursor()
  mydb.execute('select tbl_name from sqlite_master')
  list_tables = mydb.fetchall()
  for t in list_tables:
    if t[0] == "TNCStbl":
      sqlxprs = "SELECT PDBid FROM TNCStbl"
      mydb.execute(sqlxprs)
      rows = mydb.fetchall()
  mydb.close()
  return rows




def PutValuesinTable( parmresdict, dbfname ):
  #print "parmresdict= ", parmresdict

  if parmresdict[2]: # error message
    resdict = {}
    resdict["Error"] = (parmresdict[2], "text")
    pdbid = parmresdict[0][0]
  else:
    resdict = parmresdict[1]
    if resdict is None:
      return
    pdbid = resdict["pdbid"][0]

  print(pdbid)
  #print "dumping tncs values for %s into database" %pdbid
  conn = sqlite3.connect(database = dbfname, timeout = 60000)
  mydb = conn.cursor()
# first check for the existence of our table
  mydb.execute('select tbl_name from sqlite_master')
  list_tables = mydb.fetchall()
  createtable = True
  for t in list_tables:
    if t[0] == "TNCStbl":
      createtable = False
      break
  if createtable:
# Otherwise create the table with columns
# First create the main table
    sqlxprs = "CREATE table TNCStbl (p_id integer primary key autoincrement, " \
      + "PDBid text)"
    mydb.execute(sqlxprs)
    sqlxprs = "CREATE INDEX PDBidx ON TNCStbl(PDBid)"
    mydb.execute(sqlxprs)

  mydb.execute("INSERT INTO TNCStbl ( PDBid ) VALUES(?)", (pdbid ,))
  mydb.execute('PRAGMA table_info( TNCStbl )')
  columnnames = mydb.fetchall()
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  for item, (value, type) in resdict.items():
    if item not in zip(*columnnames)[1] and item != "pdbid":
      sqlxprs = "ALTER TABLE TNCStbl ADD COLUMN %s %s" %(item, type)
      print ("Adding column %s to TNCStbl" %item)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      mydb.execute(sqlxprs)
    if type == "text":
      value = str(value)
    sqlexp = "UPDATE TNCStbl SET %s = ? WHERE PDBid = ?" %item
    mydb.execute(sqlexp, (value, pdbid))
  conn.commit()
  mydb.close()



def GetXtriageResults(mtzfname):
  errstr = None
  nrefl = None; IsigIgrt3res = None; hires = None; lowres = None;
  noutlier_outside_rescut = None; NonOrigPattersonHeight = None;
  nicerings = None; twinLtest = None
  try:
    mtzobj = iotbx.mtz.object(file_name = mtzfname)
    nrefl = mtzobj.n_reflections()
    lowres = mtzobj.max_min_resolution()[0]
    hires = mtzobj.max_min_resolution()[1]
    dmpstr = StringIO()
    """
    cols = SimpleFileProperties.GetMtzColumnsTuple( mtzfname )
    IorFcolumn = None
    if len(cols[2]) and len(cols[1]):
      IorFcolumn = cols[2][0]
    else:
      if len(cols[0]) and len(cols[1]):
        IorFcolumn = cols[0][0]
      else:
        raise Exception, "No useful I or F found in mtz file: " + str(cols[4][3:])
    xtrobj = xtriage.run([mtzfname, "scaling.input.xray_data.obs_labels=%s" %IorFcolumn ], out=dmpstr)
    """
    xtrobj = xtriage.run([ mtzfname ], out=dmpstr)
    if xtrobj.data_strength_and_completeness.data_strength:
      IsigIgrt3res = xtrobj.data_strength_and_completeness.data_strength.resolution_cut
    else:
      IsigIgrt3res = hires
    noutlier_outside_rescut = 0
    # get outliers and sort them according to d_spacing
    ac_outliers = xtrobj.wilson_scaling.outliers.acentric_outliers_table.data
    if ac_outliers != None:
      zip_outliers = zip(ac_outliers[0], ac_outliers[1], ac_outliers[2], ac_outliers[3], ac_outliers[4])
      sortoutliers = sorted(zip_outliers, key=lambda tup: tup[0])
      for e in sortoutliers:
        noutlier_outside_rescut +=1
        if e[0] > IsigIgrt3res:
          break
    nicerings = 0
    for e in xtrobj.wilson_scaling.ice_rings.ice_ring_bin_location:
      if e != None:
        nicerings += 1
    twinLtest = xtrobj.twin_results.twin_summary.maha_l
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    NonOrigPattersonHeight = xtrobj.twin_results.twin_summary.patterson_height
  except Exception as m:
    errstr = str(m)
    print ("Error: ", errstr)
  return (nrefl, IsigIgrt3res, hires, lowres, noutlier_outside_rescut, nicerings,
            twinLtest, NonOrigPattersonHeight, errstr)



def GetOtherProperties(mtzfname):
  errstr = None
  pointgrouporder = None
  spacegrouporder = None
  spginfo = None
  try:
    mtzobj = iotbx.mtz.object(file_name = mtzfname)
    pointgrouporder = mtzobj.space_group().order_p()
    spacegrouporder = mtzobj.space_group().order_z()
    spginfo = mtzobj.space_group_info().symbol_and_number()
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  except Exception as m:
    errstr = str(m)
    print ("Error: ", errstr)
  return (pointgrouporder, spacegrouporder, spginfo, errstr)




def GetCloseRational( flt, epsilon = 0.000000000000000001 ):
  """
  Get a close rational fraction as an approximation to flt
  If denominator >> 100 then it's probably not a rational number
  """
  remainder = Decimal(99.9)
  largestdenominator = 50
  while abs(remainder) > Decimal(epsilon) and largestdenominator < 100000:
    largestdenominator += largestdenominator
    f = Fraction( str(flt) ).limit_denominator(largestdenominator)
    remainder = Decimal(f.numerator) / Decimal(f.denominator) - Decimal(flt)
  r = rational.int(f.numerator, f.denominator)
  return r, remainder, largestdenominator



def RunFindTNCS( pdbidtbl, mtzdir, pdbdir ):
  resultsdict = {}
  try:
    pdbid = pdbidtbl
    resultsdict["pdbid"] = ( pdbid, "text" )
    mtzfname = os.path.join(mtzdir, pdbid + ".mtz")
    localpdbfile = os.path.join(pdbdir, pdbid + ".pdb" )
    if not os.path.exists( localpdbfile ):
      pdbfname = get_pdb(pdbid, "pdb", "pdbe", log=sys.stdout, format="pdb")
      shutil.copy(pdbfname, localpdbfile )
      os.remove(pdbfname)

    # set which subroutines to run
    resultsdict = FindTNCS(mtzfname = mtzfname, modelfname = localpdbfile,
                            pdbid = pdbid, getTNCSinPDB = True,
                            getXtriageValues = True, getTNCSfromPhaser = True,
                            getOtherStuff = True )
  except Exception as e:
    print(e)
    resultsdict["Error"] = (str(e), "text")
  return resultsdict






if __name__ == '__main__':

  dbfname = sys.argv[1]  # database file name
  mtzdir = sys.argv[2]  # location of the mtz files
  pdbdir = sys.argv[3]  # location of the pdb files
  nproc = int(sys.argv[4]) #nproc

  #debugging, option to run on one pdb file (pdbs as the 5th arg) then one or more
  # omit this and gets pdb ids (90000) and go
  if len(sys.argv) < 6: # we make a list of pdbids from the mtz files
    mtzfiles = os.listdir( mtzdir )
    pdbids = [ ( e.split(".")[0], mtzdir, pdbdir) for e in mtzfiles ]
  else:
    if sys.argv[5] == "pdbs": # then we have a list of pdbids on the commandline
      pdbids = [ (pdbid, mtzdir, pdbdir) for pdbid in sys.argv[6:] ]
    if sys.argv[5] == "file": # then we have a list in a file
      lst = eval(open(sys.argv[6],"r").read())
      pdbids = [ (pdbid, mtzdir, pdbdir) for pdbid in lst ]

  donepdbs = PDBidsinDBase( dbfname ) # pdbs present in existing database
  remainingpdbids = list( set(pdbids) - set(donepdbs) )
  random.shuffle(remainingpdbids) # shuffle to avoid any speed bias
  nleft = len(remainingpdbids)
  for t in remainingpdbids:
    print(t[0], end=' ')
  print(" ")
  print("Still %d pdbids not in database to be done:" % nleft)
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  if len(pdbids) == 1:
    parmresdict = ( pdbids[0][0], RunFindTNCS(pdbids[0][0], pdbids[0][1], pdbids[0][2]), None )
    PutValuesinTable( parmresdict, dbfname)
  else:
    for i, parmresdict in enumerate(easy_mp.multi_core_run( RunFindTNCS, remainingpdbids, nproc)):
      print(i, (nleft - i))
      PutValuesinTable( parmresdict, dbfname)
