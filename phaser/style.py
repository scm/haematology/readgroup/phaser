from __future__ import print_function

class WrappedParagraph(object):
  """
  A wrapped text
  """

  def __init__(self, lines):

    self.lines = lines
    self.width = max( len( l ) for l in self.lines ) if self.lines else 0


class FormattedParagraph(object):
  """
  A paragraph with extra formatting applied
  """

  def __init__(self, paragraph, formatter, width):

    self.paragraph = paragraph
    self.formatter = formatter
    self.width = width


  @property
  def lines(self):

    return self.formatter( paragraph = self.paragraph, maxwidth = self.width )


# Wrapping
def truncate_long_lines(text, maxwidth):
  """
  Original linebreaks are kept, overly long lines are truncated
  """

  lines = []

  for l in text.split( "\n" ):
    length = len( l )

    if maxwidth < length:
      lines.append( l[ : maxwidth - 3 ] + "..." )

    else:
      lines.append( l )

  return WrappedParagraph( lines = lines )


def wrap_long_lines(text, maxwidth):
  """
  The original linebreaks are kept, overly long lines are wrapped
  """

  import textwrap
  lines = []

  for l in text.split( "\n" ):
    if not l:
      lines.append( "" )
      continue

    wrapped = textwrap.wrap( l, width = maxwidth )
    lines.extend( wrapped )

  return WrappedParagraph( lines = lines )


def rewrap(text, maxwidth):
  """
  Linebreaks are removed and text is wrapped to the given width
  """

  import textwrap
  return WrappedParagraph( lines = textwrap.wrap( joined, width = maxwidth ) )


# Formatters
class Banner(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * maxwidth
    yield self.char + " " * ( maxwidth - 2 ) + self.char

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( maxwidth - 4 ), self.char )

    yield self.char + " " * ( maxwidth - 2 ) + self.char
    yield self.char * maxwidth


  def __call__(self, paragraph, streamwidth):

    width = streamwidth - self.width_modifier

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = width,
        ),
      width,
      )


class Aligned(object):

  def __init__(self, method):

    self.method = method


  @property
  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield self.method( l, maxwidth )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = streamwidth,
        ),
      streamwidth,
      )

  @classmethod
  def Left(cls):

    return cls( method = str.ljust )

  @classmethod
  def Center(cls):

    return cls( method = str.center )

  @classmethod
  def Right(cls):

    return cls( method = str.rjust )


class Framed(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * ( paragraph.width + 4 )

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( paragraph.width ), self.char )

    yield self.char * ( paragraph.width + 4 )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = paragraph.width - self.width_modifier,
        ),
      streamwidth - self.width_modifier
      )


class Underlined(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield l

    yield self.char * paragraph.width


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = paragraph.width,
        ),
      streamwidth,
      )


class Filled(object):

  def __init__(self, char = "="):

    self.char = char


  @property
  def width_modifier(self):

    return -2


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield ( " %s " % l ).center( maxwidth, self.char )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = streamwidth,
        ),
      streamwidth,
      )


class Indented(object):

  def __init__(self, preamble, fillchar = " "):

    self.preamble = preamble
    self.fillchar = fillchar


  @property
  def width_modifier(self):

    return -len( self.preamble )


  def process(self, paragraph, maxwidth):

    lineiter = iter( paragraph.lines )

    try:
      yield self.preamble + next(lineiter)

    except StopIteration:
      pass

    else:
      indent = self.fillchar * len( self.preamble )

      for l in lineiter:
        yield indent + l


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self.process,
        width = paragraph.width - self.width_modifier,
        ),
      streamwidth - self.width_modifier,
      )


  @classmethod
  def Block(cls, indent = 4, fillchar = " "):

    return cls( preamble = fillchar * indent, fillchar = fillchar )


class ListData(object):
  """
  Holds list details
  """

  def __init__(self, markergen, fillchar):

    self.markergen = markergen
    self.fillchar = fillchar


  def next(self):

    return self.markergen.next()


  @classmethod
  def Bullet(cls, marker, fillchar = " "):

    import itertools
    return cls(
      markergen = ( marker for i in itertools.count() ),
      fillchar = fillchar,
      )

  @classmethod
  def Numbered(cls, start = 1, separator = ". ", width = 4, fillchar = " "):

    if width < 1:
      raise ValueError("Minimum value for width is 1")

    formatting = "%" + str( width ) + "d" + separator

    import itertools
    return cls(
      markergen = ( formatting % i for i in itertools.count( start ) ),
      fillchar = fillchar,
      )


class TaskData(object):
  """
  Holds task details
  """

  def __init__(self, reserved, ending):

    self.reserved = reserved
    self.ending = ending


class ProgressBarData(object):
  """
  Holds progress bar details
  """

  def __init__(self, delimiter, progress, hits_per_tick, postwrite):

    self.delimiter = delimiter
    self.progress = progress
    self.hits_per_tick = hits_per_tick
    self.postwrite = postwrite
    self.hits = 0


  def increment(self):

    self.hits += 1

    if ( self.hits - self.hits_per_tick // 2 ) % self.hits_per_tick == 0:
      return self.progress

    else:
      return ""


class State(object):

  def __init__(self, data):

    self.data = data


  @property
  def width_modifier(self):

    return 0


  def __call__(self, paragraph, streamwidth):

    return ( paragraph, streamwidth )


class Dummy(object):

  @property
  def width_modifier(self):

    return 0

  def __call__(self, paragraph, streamwidth):

    return ( paragraph, streamwidth )
