from __future__ import print_function

from __future__ import division
from libtbx import easy_run
import iotbx.pdb

def exercise () :
  from mmtbx.regression import make_fake_anomalous_data
  import mmtbx.ions.utils
  mtz_file, pdb_file = make_fake_anomalous_data.generate_calcium_inputs()
  args = ["phaser.find_anomalous_substructure", pdb_file, mtz_file,
          "wavelength=1.12", "output_file=tst_phaser_find_anom.pdb"]
  result = easy_run.fully_buffered(" ".join(args)).raise_if_errors()
  assert (result.return_code == 0)
  pdb_in = iotbx.pdb.input("tst_phaser_find_anom.pdb")
  atoms = pdb_in.construct_hierarchy().atoms()
  assert atoms.size() == 1
  assert atoms[0].occ > 0.4

if (__name__ == "__main__") :
  exercise()
  print("OK")
