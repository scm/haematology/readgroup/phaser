#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     16/12/2013
# Copyright:   (c) oeffner 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from __future__ import print_function

import sys, os, re, glob
from iotbx import pdb
from cctbx.array_family import flex



def Roundoff(val, precision):
    if isinstance(val, float):
        formatstr = "%2." + str(precision) + "f"
        return float(formatstr %float(val))
    if isinstance(val, list) or isinstance(val, tuple):
        val = list(val)
        for i,v in enumerate(val):
            val[i] = Roundoff(v, precision)
    return val


def RecaseFilename2(path):
    # need to specify current directory if only a filename is supplied
    if os.path.dirname(path) == "":
        path = os.path.join(".", path)
    #Get a case-insensitive path for use on a case sensitive system.
    return _path_insensitive(path) or path


def _path_insensitive(path):
    """
    Recursive part of path_insensitive to do the work.
    """
    if path == '' or os.path.exists(path):
        return path

    base = os.path.basename(path)  # may be a directory or a file
    dirname = os.path.dirname(path)

    suffix = ''
    if not base:  # dir ends with a slash?
        if len(dirname) < len(path):
            suffix = path[:len(path) - len(dirname)]

        base = os.path.basename(dirname)
        dirname = os.path.dirname(dirname)

    if not os.path.exists(dirname):
        dirname = _path_insensitive(dirname)
        if not dirname:
            return
    # at this point, the directory exists but not the file
    try:  # we are expecting dirname to be a directory, but it could be a file
        files = os.listdir(dirname)
    except OSError:
        return

    baselow = base.lower()
    try:
        basefinal = next(fl for fl in files if fl.lower() == baselow)
    except StopIteration:
        return

    if basefinal:
        return os.path.join(dirname, basefinal) + suffix
    else:
        return



# need to identify file paths but in a case insensitive manner before opening files
def RecaseFilename(fname):
    # algorithm works for absolute paths only
    fname2 = PosixSlashFilePath(os.path.abspath(fname) )
    dirs = fname2.split(os.sep)
    (drive, fpath) = os.path.splitdrive(fname2)
    if drive: # as on Windows
        # avoid empty first element in fpath which happens if fname is similar to c:\foo\bar
        nfpath =  [ e for e in fpath.split(os.sep) if e]
        dirs = nfpath
        # glob on Windows only works if first dir element is prepended with drive letter
        dirs[0] = drive + os.sep + dirs[0]
    recasedpath = dirs[0]
    if dirs[0] == "": # for unix paths
        recasedpath = os.sep
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    for dir in dirs:
        files = glob.glob( os.path.join(recasedpath, "*") )
        m = []
        for file in files:
            m = re.findall(dir,file,re.IGNORECASE)
            #print "m=%s, dir=%s, file=%s, recasepath=%s " %( m, dir, file, recasedpath )
            if m != [] and m[0].upper()==dir.upper():
                recasedpath = os.path.join(recasedpath, m[0])
                break
    # Was fname a relative path? If so, convert absolute path back to relative path
    if len(fname) != len(recasedpath):
        recasedpath = os.path.relpath(recasedpath)
    return recasedpath



def PosixSlashFilePath(fpath):
    if sys.platform != "win32":
        mstr = ""
        for c in fpath:
            if c == "\\":
                mstr += "/"
            else:
                mstr += c
        return mstr
    else: # if unix platform
        mstr = ""
        for c in fpath:
            if c == "/":
                mstr += "\\"
            else:
                mstr += c
        return mstr
#        return fpath
