#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     10/03/2017
# Copyright:   (c) oeffner 2017
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from __future__ import print_function
import math


def Nrefl2array(dmin, dmax, nrefl):
    # Make bins of resolution shells with elements:
    # (number of reflections, lores, hires, avgres, avg(1/res^2))
    invdmin2=1.0/(dmin*dmin)
    invdmax2=1.0/(dmax*dmax)
    # put about 100 reflections in each shell. low resolution shells wil have fewer
    nshells = int(nrefl/100)
    # if not enough relfections make at least 20 bins
    nshells = max(nshells, 20 )
    # if plenty of reflections don't make more than 1000 bins
    nshells = min(nshells, 1000 )
    shells = [(invdmin2-invdmax2)*n/nshells +invdmax2 for n in range(nshells+1)]
    def Nrefl2(invres2,dmin,dmax,nrefl):
        return (math.pow(invres2, 1.5)-1/(dmax*dmax*dmax))*nrefl/(1/(dmin*dmin*dmin)-1/(dmax*dmax*dmax))
    reflres = [(Nrefl2(shell, dmin, dmax, nrefl), 1.0/math.sqrt(shell), shell) for shell in shells]
    nreflavgres = []
    for i,rfl in enumerate(reflres):
        if (i+2)>len(reflres):
            break
        nreflavgres.append((reflres[i+1][0]-rfl[0], rfl[1], reflres[i+1][1],
          (reflres[i+1][1]+rfl[1])/2.0, (reflres[i+1][2]+rfl[2])/2.0))
    return nreflavgres



def erf(x):
# Error function for use below
  a1 =  0.254829592
  a2 = -0.284496736
  a3 =  1.421413741
  a4 = -1.453152027
  a5 =  1.061405429
  p  =  0.3275911
  # Save the sign of x
  sign = 1
  if x < 0:
      sign = -1
  x = abs(x)
  # A & S 7.1.26
  t = 1.0/(1.0 + p*x)
  y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)
  return sign*y


# sigmaA^2 for a resolution shell including bulk solvent term
def SigmaASqr(fracscat, dres, rms, Ksol=0.95, Bsol=300):
  return fracscat* math.exp(-4.0*(math.pi*rms/dres)*(math.pi*rms/dres)/3.0) \
    * math.sqrt((1 - Ksol* math.exp(-Bsol/(4.0/(dres*dres)))))


# calculate fracvar(rmsd, resolution, fracscat)
def FracVar(rms, dres, fracscat):
  var1 = 2.0*math.pi*rms/(math.sqrt(3.0)*dres)
  fracvar = (math.sqrt(3.0)*dres*erf(var1)-4.0*math.exp(-var1*var1)
   *math.sqrt(math.pi)*rms)*9.0*dres*dres*float(fracscat)/(
   32.0*math.pow(math.pi,2.5)*rms*rms*rms)
  return fracvar

# compute average sigmaA given rms resolution and fracscat
def avgSigmaA(rms, dres, fracscat):
  var1 = math.sqrt(2.0/3.0)*math.pi*rms/dres
  avgsigmaa = math.sqrt(math.exp(-2.0*var1*var1)*float(fracscat))*(
   -4.0*math.sqrt(math.pi)*rms + math.sqrt(6.0) * dres *math.exp(var1*var1)
   *erf(var1) ) * 9.0*dres*dres/(16.0*math.pow(math.pi,2.5)*rms*rms*rms)
  return avgsigmaa


# compute sigmaA^2 given rms, modelcompletteness for resolution range dmin,dmax
def FracVarreshireslo(fracscat,rms,dmin,dmax):
  fracvar = 0.0
  if dmin < dmax:
    fracvar = (9.0*float(fracscat)*(4.0*(1.0/(dmax*math.exp(
      (4.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.0*math.pow(dmax,2)))) -
      1/(dmin*math.exp((4.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.0*math.pow(
      dmin,2)))))*math.sqrt(math.pi)*rms
      - math.sqrt(3.0)*erf((2.0*math.pi*rms)/(math.sqrt(3.0)*dmax)) + math.sqrt(
      3.0)*erf((2.0*math.pi*rms)/(math.sqrt(3.0)*dmin))))/(
      32.0*(-math.pow(dmax,-3) + math.pow(dmin,-3))*math.pow(math.pi,2.5)*math.pow(rms,3))
  return fracvar


# compute average sigmaA given rms, modelcompletteness for resolution range dmin,dmax
def avgSigmaAreshireslo(fracscat,rms,dmin,dmax):
  avgsigmaa = 0.0
  if dmin < dmax:
    avgsigmaa = (9.0*((math.sqrt(float(fracscat)/math.exp(
      (4.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.0*math.pow(dmax,2))) )*
      (4.0*math.sqrt(math.pi)*rms - math.sqrt(6.0)*dmax*
       math.exp((2.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.*math.pow(dmax,2)))*
       erf((math.sqrt(2.0/3.0)*math.pi*rms)/dmax)))/dmax +
      (math.sqrt(float(fracscat)/math.exp((
        4.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.0*math.pow(dmin,2))))*
      (-4.0*math.sqrt(math.pi)*rms + math.sqrt(6.0)*dmin*
       math.exp((2.0*math.pow(math.pi,2)*math.pow(rms,2))/(3.0*math.pow(dmin,2)))*
       erf((math.sqrt(2.0/3.0)*math.pi*rms)/dmin)))/dmin))/(
       16.*(-math.pow(dmax,-3) + math.pow(dmin,-3))*math.pow(math.pi,2.5)*math.pow(rms,3))
  return avgsigmaa


# Non-linear fit to integral of likelihood target function over Ecalc, Eobs for approximating the LLG
# done in Mathematica
def LLGacentricapprox(k):
  ret = -1.0986918926049707 + 0.9578895607931854* math.exp(0.005909349665949917*k) + \
   3.680032409372382e-8* math.exp(16.960993420657587* k*k) + 3697.047492631997*k - \
   0.7234616302875678* k*k + 157.2519515491649* k*k*k + 0.14080367146969025*  \
   math.cosh(4.18671351835429*k) - 7304.927849292323* math.sinh(0.5061040754028604*k)
  return ret


def LLGcentricapprox(k):
  ret = -160.08294380012046 + 160.07193209560242* math.exp(0.6363478546257842*k) + \
   5.669504894893263e-11* math.exp(23.4917376276848* k*k) - 99.83259947127553*k - \
   32.11951771609019* k*k - 4.162700400826951* k*k*k + 0.011014917074246323* \
   math.cosh(6.09740977467737*k) - 0.6403447890792975* math.sinh(3.16967279847402*k)
  return ret


# get the expected LLG for a hypothetical MR calculation
def eLLG(fracscat, rms, dmin, dmax, nrefl, targetLLG=None, rescut=None):
  if fracscat < 0.0 or fracscat > 1.0 or rms < 0.0:
    return None, None
  LLGacentric=0.0; LLGcentric=0.0
  sumrefl = 0.0; necessaryrefl = 0
  achievedLLGcentric = None; achievedLLGacentric = None
  totalLLGacentric = 0.0; totalLLGcentric =0.0
  requiredhires = 0
  resultlist = []
  # compute resolutions of hypothetical resolution shells
  resshells = Nrefl2array(dmin, dmax, nrefl)
  # Sum the LLG over all reflections divided into resolution shells
  for (nreflshell, lores, hires, avgres, avginvres2) in resshells:
    sigmaAsqr1 = SigmaASqr(fracscat, avgres, rms)
    LLGacentric = LLGacentricapprox( sigmaAsqr1 )*nreflshell
    totalLLGacentric += LLGacentric
    LLGcentric = LLGcentricapprox( sigmaAsqr1 )*nreflshell
    #print ( nreflshell, avgres, sigmaAsqr1, LLGcentric, totalLLGcentric)
    totalLLGcentric += LLGcentric
    sumrefl += nreflshell # how many reflections have we got so far?
    resultlist.append( (nreflshell, avgres, int(sumrefl), LLGacentric,
                         LLGcentric, totalLLGacentric, totalLLGcentric) )
    if rescut: # note where resolution is cut even if there are still more reflections
      if rescut >= hires and necessaryrefl==0:
        achievedLLGacentric = totalLLGacentric
        achievedLLGcentric = totalLLGcentric
        necessaryrefl = sumrefl

    if targetLLG: # stop when enough reflections have been included to achieve target LLG
      if totalLLGacentric >= targetLLG and totalLLGcentric >= targetLLG and requiredhires==0:
        requiredhires = hires
        necessaryrefl = sumrefl

  return totalLLGacentric, totalLLGcentric, achievedLLGacentric, \
          achievedLLGcentric, requiredhires, int(necessaryrefl), resultlist
