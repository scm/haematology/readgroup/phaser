from __future__ import print_function

import boost_adaptbx.boost.python as bp
ext = bp.import_ext("phaser_ext")
from phaser_ext import InputMR_DAT as _InputMR_DAT
from phaser_ext import InputCCA as _InputCCA
from phaser_ext import InputNMA as _InputNMA
from phaser_ext import InputANO as _InputANO
from phaser_ext import InputMR_FRF as _InputMR_FRF
from phaser_ext import InputMR_FTF as _InputMR_FTF
from phaser_ext import InputMR_PAK as _InputMR_PAK
from phaser_ext import InputMR_RNP as _InputMR_RNP
from phaser_ext import InputMR_AUTO as _InputMR_AUTO
from phaser_ext import InputEP_DAT as _InputEP_DAT
from phaser_ext import InputEP_SAD as _InputEP_SAD
from phaser_ext import InputEP_SSD as _InputEP_SSD
from phaser_ext import InputEP_AUTO as _InputEP_AUTO

from .deprecated_keywords_bpl import COMP, EIGE, ENSE, FINA, LABI, PART
from .deprecated_keywords_bpl import PURG, RESC, SEAR, TRAN

import warnings

class InputMR_DAT(_InputMR_DAT, LABI):

    pass


class InputCCA(_InputCCA, COMP, LABI):

    pass


class InputNMA(_InputNMA, EIGE, ENSE, LABI):

    pass


class InputANO(_InputANO, COMP, LABI):

    pass


class InputMR_FRF(_InputMR_FRF, COMP, ENSE, FINA, RESC, SEAR, LABI):

    pass


class InputMR_FTF(_InputMR_FTF, COMP, ENSE, FINA, RESC, LABI):

    pass


class InputMR_PAK(_InputMR_PAK, ENSE):

    pass


class InputMR_RNP(_InputMR_RNP, COMP, ENSE, LABI):

    pass


class InputMR_AUTO(_InputMR_AUTO, COMP, ENSE, FINA, PURG, SEAR, LABI):

    pass


class InputEP_DAT(_InputEP_DAT):

    pass


class InputEP_SAD(_InputEP_SAD, PART):

    pass

class InputEP_SSD(_InputEP_SSD, PART):

    pass

class InputEP_AUTO(_InputEP_AUTO, COMP, PART):

    pass
