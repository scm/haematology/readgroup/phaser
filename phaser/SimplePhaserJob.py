from __future__ import print_function

import os, pickle
from phaser import *
from phaser.utils2 import *
from phaser import SimpleFileProperties



def RunMR_Auto(mtzfname, enspdbdict, seqfnames, root, mute, mrinput=None):
    mrsols = None
    errstr = ""

    (Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI, exceptmsg) = SimpleFileProperties.GetMtzColumns(mtzfname)
    myinput = InputMR_DAT()
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    mtzfname= RecaseFilename(mtzfname)

    datinput = InputMR_DAT()
    datinput.setHKLI(mtzfname)
    if Icol:
      datinput.setLABI_I_SIGI(Icol, Sigcol)
    else:
      datinput.setLABI_F_SIGF(Fcol, Sigcol)
    datinput.setMUTE( mute )
    datrun = runMR_DAT(datinput)
    if not datrun.Success():
      errstr = datrun.ErrorName() + " ERROR :\n"+ datrun.ErrorMessage()[:10000]
      return mrsols, errstr
    with open(root + ".log","w") as f:
      f.write( datrun.logfile())

    if datrun.Success():
      if not mrinput: # then use default settings rather than a user customised
        mrinput = InputMR_AUTO()
      mrinput.setSPAC_HALL(datrun.getSpaceGroupHall())
      mrinput.setCELL6(datrun.getUnitCell())
      mrinput.setREFL_DATA(datrun.getREFL_DATA())

      mrinput.setROOT(root)
      mrinput.setXYZO(True)
      for seqfname in seqfnames:
        mrinput.addCOMP_PROT_SEQ_NUM(seqfname, 1)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      for ensname, (pdbfname, errestim, errtype) in enspdbdict.items():
        modelfname = pdbfname.replace("\\", os.path.sep).strip() # to get rid of any \r character
        modelfname = RecaseFilename(modelfname)
        pdbstr = ""
        with open(modelfname,"r") as f:
          pdbstr = f.read()
        if "isRMS" == errtype:
          mrinput.addENSE_STR_RMS( ensname, pdbstr, modelfname, float(errestim) )
          #mrinput.addENSE_PDB_RMS( ensname, modelfname, float(errestim) )
        else:
          mrinput.addENSE_STR_ID( ensname, pdbstr, modelfname, float(errestim) )
          #mrinput.addENSE_PDB_ID( ensname, modelfname, float(errestim) )
        mrinput.addSEAR_ENSE_NUM( ensname, 1)

      mrinput.setMUTE( mute )
      mrinput.setRESO_AUTO_OFF()
      mrrun = runMR_AUTO(mrinput)
      if mrrun.Success():
        mrsols = mrrun.getDotSol()
        if mrrun.foundSolutions() :
          solfile = open("%s.sol" %root,"w")
          solfile.write(mrsols.unparse())
          solfile.close()
          mrrun.write_mtz()
          mrrun.write_pdb()
        else:
          errstr =  "Phaser has not found any MR solutions"
      else:
        errstr = mrrun.ErrorName() + "ERROR :\n" + mrrun.ErrorMessage()[:10000]

      with open(root + ".log","a") as f:
        f.write( mrrun.logfile())
      pfname = root + ".pkl"
      with open(pfname,"wb") as ofile:
        pickle.dump( mrrun, ofile )
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    else:
      errstr = datrun.ErrorName() + "ERROR :\n" + datrun.ErrorMessage()[:10000]

    return mrsols, errstr
