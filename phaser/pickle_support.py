from __future__ import print_function

import phaser

def mr_rlist_getinitargs(self):

    return ( self.MODLID, self.EULER, self.RF, self.RFZ, self.GRF, self.DEEP )


def mr_ndim_getinitargs(self):

    return (
        self.MODLID,
        self.R,
        self.FRAC,
        self.inFRAC,
        self.TRA,
        self.BFAC,
        self.FIXR,
        self.FIXT,
        self.FIXB,
        )


def MapCoefs_getinitargs(self):

    return (
        self.FC,
        self.PHIC,
        self.FWT,
        self.PHWT,
        self.DELFWT,
        self.PHDELWT,
        self.FOM,
        self.HLA,
        self.HLB,
        self.HLC,
        self.HLD,
        )


def mr_set_getinitargs(self):

    return (
        self.ANNOTATION,
        self.KNOWN,
        self.RLIST,
        self.HALL,
        self.TF,
        self.TFZ,
        self.LLG,
        self.R,
        self.PAK,
        self.ORIG_NUM,
        self.NEWVRMS,
        self.CELL,
        self.MAPCOEFS
        )

def mr_solution_getinitargs(self):

    return (
        list( self ),
        self.resolution()
        )


def SpaceGroup_getstate(self):

    return {
        "space_group_hall": self.getSpaceGroupHall(),
        }


def SpaceGroup_setstate(self, state):

    assert "space_group_hall" in state
    self.setSpaceGroupHall( state[ "space_group_hall" ] )


def af_atom_getstate(self):

    return {
        "log_likelihood": self.LLG,
        "scatterers": self.SET,
        }


def af_atom_setstate(self, state):

    assert "log_likelihood" in state
    self.LLG = state[ "log_likelihood" ]
    assert "scatterers" in state
    self.SET = state[ "scatterers" ]


def UnitCell_getstate(self):

    return {
        "unit_cell": self.getUnitCell(),
        }


def UnitCell_setstate(self, state):

    assert "unit_cell" in state
    self.setUnitCell( state[ "unit_cell" ] )


def Output_getstate(self):
    userruntime = 0
    if hasattr(self, "userrun_time"): # only present when compiling with --boost-timer
        userruntime = self.userrun_time
    return {
        "output_strings": self.output_strings,
        "run_time": self.run_time,
        "userrun_time": userruntime,
        }


def Output_setstate(self, state):
    assert "output_strings" in state
    self.output_strings = state[ "output_strings" ]
    if "run_time" in state:
        self.run_time = state[ "run_time" ]
    if "userrun_time" in state and state[ "userrun_time" ]: # only present when compiling with --boost-timer
        self.userrun_time = state[ "userrun_time" ]


def data_refl_getinitargs(self):

    return (
        self.MILLER,
        self.FMEAN,
        self.SIGFMEAN,
        self.SG_HALL,
        )

def data_spots_getinitargs(self):

    return (
        self.FMEAN,
        self.SIGFMEAN,
        )


def data_atom_getstate(self):

    s = self.SCAT
    return {
        "REJECTED": self.REJECTED,
        "RESTORED": self.RESTORED,
        "n_adp": self.n_adp,
        "n_xyz": self.n_xyz,
        "SCAT": (s.label,s.site,s.u_iso,s.occupancy,s.scattering_type,s.fp,s.fdp),
        }


def data_atom_setstate(self, state):

    from cctbx.xray import scatterer
    self.REJECTED = state[ "REJECTED" ]
    self.RESTORED = state[ "RESTORED" ]
    self.n_adp = state[ "n_adp" ]
    self.n_xyz = state[ "n_xyz" ]
    self.SCAT = scatterer(*state[ "SCAT" ])

def stream_string_getstate(self):

    return { "stream": self.stream, "string": self.string, }

def stream_string_setstate(self, state):

    self.stream = state[ "stream" ]
    self.string = state[ "string" ]

def DataA_getinitargs(self):

    return (
        self.getUnitCell(),
        phaser.data_refl( self.getMiller(), self.getF(), self.getSIGF(), self.getHall() ),
        phaser.data_resharp(),
        self.getSigmaN(),
        phaser.data_outl(),
        )


def ResultANO_getinitargs(self):

    return (
        phaser.Output(),
        )


def ResultANO_getstate(self):

    d = Output_getstate( self )
    d.update(
        {
            "data_a": phaser.DataA(
                self.getUnitCell(),
                phaser.data_refl( self.getMiller(), self.getF(), self.getSIGF(), self.getHall() ),
                phaser.data_resharp(),
                self.getSigmaN(),
                phaser.data_outl(),
                ),
            "data_a_wilson_k": self.getWilsonK(),
            "data_a_wilson_b": self.getWilsonB(),
            "data_a_aniso": self.getAniso(),
            }
        )

    return d


def ResultANO_setstate(self, state):

    assert all( label in state for label in [ "data_a" ] )

    self.setDataA( state[ "data_a" ] )
    self.setAniso( state[ "data_a_aniso" ] )
    self.setWilsonB( state[ "data_a_wilson_b" ] )
    self.setWilsonK( state[ "data_a_wilson_k" ] )
    Output_setstate( self, state )


def ResultMR_getinitargs(self):

    return (
        phaser.Output(),
        )


def ResultMR_getstate(self):

    d = Output_getstate( self )
    d.update(
        {
            "init": (
                self.getResultType(),
                self.getTitle(),
                self.getNumTop(),
                phaser.map_string_data_pdb(),
                self.getChainOcc(),
                ),
            "mr_set": self.getDotSol(),
            "data_a": phaser.DataA(
                self.getUnitCell(),
                phaser.data_refl( self.getMiller(), self.getF(), self.getSIGF(),self.getHall() ),
                phaser.data_resharp(),
                self.getSigmaN(),
                phaser.data_outl(),
                ),
            }
        )

    return d


def ResultMR_setstate(self, state):

    assert all( label in state for label in [ "init", "mr_set", "data_a" ] )

    self.init( *state[ "init" ] )
    self.setDotSol( state[ "mr_set" ] )
    self.setDataA( state[ "data_a" ] )
    Output_setstate( self, state )


def ResultMR_RF_getinitargs(self):

    return (
        phaser.Output(),
        )

def ResultMR_RF_getstate(self):

    d = Output_getstate( self )
    d.update( UnitCell_getstate( self ) )
    d.update(
        {
            "rlist": self.getDotRlist(),
            "init": (
                self.getHall(),
                self.getTitle(),
                self.getNumTop(),
                phaser.map_string_data_pdb(),
                )
            }
        )

    return d


def ResultMR_RF_setstate(self, state):

    assert "rlist" in state and "init" in state

    self.init( *state[ "init" ] )
    self.setDotRlist( state[ "rlist" ] )
    Output_setstate( self, state )
    UnitCell_setstate( self, state )


def ResultMR_TF_getinitargs(self):

    return (
        phaser.Output(),
        )

def ResultMR_TF_getstate(self):

    d = Output_getstate( self )
    d.update( UnitCell_getstate( self ) )
    d.update(
        {
            "dot_sol": self.getDotSol(),
            "init": (
                self.getTitle(),
                self.getNumTop(),
                phaser.map_string_data_pdb(),
                )
            }
        )

    return d


def ResultMR_TF_setstate(self, state):

    assert "dot_sol" in state and "init" in state

    self.init( *state[ "init" ] )
    self.setDotSol( state[ "dot_sol" ] )
    Output_setstate( self, state )
    UnitCell_setstate( self, state )


def BinStats_getstate(self):

    state = {}
    state[ "NUM_bin" ] =  self.NUM_bin
    state[ "FOM_bin" ] =  self.FOM_bin
    state[ "HiRes_bin" ] =  self.HiRes_bin
    state[ "LoRes_bin" ] =  self.LoRes_bin
    return state


def BinStats_setstate(self, state):

    assert "NUM_bin" in state
    assert "FOM_bin" in state
    assert "HiRes_bin" in state
    assert "LoRes_bin" in state
    self.NUM_bin = state[ "NUM_bin" ]
    self.FOM_bin = state[ "FOM_bin" ]
    self.HiRes_bin = state[ "HiRes_bin" ]
    self.LoRes_bin = state[ "LoRes_bin" ]

def HandEP_getstate(self):

    state = SpaceGroup_getstate( self )
    state.update(
        {
        "selected": self.selected,
        "acentStats": self.acentStats,
        "centStats": self.centStats,
        "allStats": self.allStats,
        "FWT": self.FWT,
        "PHWT": self.PHWT,
        "PHIB": self.PHIB,
        "FOM": self.FOM,
        "FPFOM": self.FPFOM,
        "HL": self.HL,
        "atoms": self.atoms,
        "PDBfile": self.PDBfile,
        "MTZfile": self.MTZfile,
        "SOLfile": self.SOLfile,
        "AtomFp": self.AtomFp,
        "AtomFdp": self.AtomFdp,
        "t2atomtype": self.t2atomtype,
        }
       )
    return state


def HandEP_setstate(self, state):

    SpaceGroup_setstate( self, state )
    assert "selected" in state
    assert "acentStats" in state
    assert "centStats" in state
    assert "allStats" in state
    assert "FWT" in state
    assert "PHWT" in state
    assert "PHIB" in state
    assert "FOM" in state
    assert "FPFOM" in state
    assert "HL" in state
    assert "atoms" in state
    assert "PDBfile" in state
    assert "MTZfile" in state
    assert "SOLfile" in state
    assert "AtomFp" in state
    assert "AtomFdp" in state
    assert "t2atomtype" in state
    self.selected = state[ "selected" ]
    self.acentStats = state[ "acentStats" ]
    self.centStats = state[ "centStats" ]
    self.allStats = state[ "allStats" ]
    self.FWT = state[ "FWT" ]
    self.PHWT = state[ "PHWT" ]
    self.PHIB = state[ "PHIB" ]
    self.FOM = state[ "FOM" ]
    self.FPFOM = state[ "FPFOM" ]
    self.HL = state[ "HL" ]
    self.atoms = state[ "atoms" ]
    self.PDBfile = state[ "PDBfile" ]
    self.MTZfile = state[ "MTZfile" ]
    self.SOLfile = state[ "SOLfile" ]
    self.AtomFp = state[ "AtomFp" ]
    self.AtomFdp = state[ "AtomFdp" ]
    self.t2atomtype = state[ "t2atomtype" ]


def ResultEP_getstate(self):

    d = Output_getstate( self )
    d.update( UnitCell_getstate( self ) )
    d.update(
        {
          "MILLER": self.MILLER,
          "second_hand": self.second_hand,
          "hand1": self.getHand( False ),
          "hand2": self.getHand( True ),
        }
        )

    return d


def ResultEP_setstate(self, state):

    assert "hand1" in state and "hand2" in state
    Output_setstate( self, state )
    UnitCell_setstate( self, state )
    self.MILLER = state[ "MILLER" ]
    self.second_hand = state[ "second_hand" ]
    self.setHand( state[ "hand1" ], False )
    self.setHand( state[ "hand2" ], True )


def ep_solution_getinitargs(self):

    return (
        list( self ),
        )

def ResultSSD_getstate(self):

    d = Output_getstate( self )
    d.update(
        {
            "dot_sol": self.getDotSol(),
            "init": (
                self.getDotSol(),
                )
            }
        )

    return d

def ResultSSD_setstate(self, state):

    assert "dot_sol" in state and "init" in state

    self.init( *state[ "init" ] )
    self.setDotSol( state[ "dot_sol" ] )
    Output_setstate( self, state )

GETINITARGS_FOR = {
    phaser.mr_rlist: mr_rlist_getinitargs,
    phaser.mr_ndim: mr_ndim_getinitargs,
    phaser.mr_set: mr_set_getinitargs,
    phaser.mr_solution: mr_solution_getinitargs,
    phaser.ep_solution: ep_solution_getinitargs,
    phaser.MapCoefs: MapCoefs_getinitargs,
    phaser.data_spots: data_spots_getinitargs,
    phaser.data_refl: data_refl_getinitargs,
    phaser.DataA: DataA_getinitargs,
    phaser.ResultMR: ResultMR_getinitargs,
    phaser.ResultMR_RF: ResultMR_RF_getinitargs,
    phaser.ResultMR_TF: ResultMR_TF_getinitargs,
    phaser.ResultANO: ResultANO_getinitargs,
    }

#alternative pickle support (alt to init)
GET_SET_STATE_FOR = {
    phaser.Output: ( Output_getstate, Output_setstate ),
    phaser.UnitCell: ( UnitCell_getstate, UnitCell_setstate ),
    phaser.SpaceGroup: ( SpaceGroup_getstate, SpaceGroup_setstate ),
    phaser.ResultMR: ( ResultMR_getstate, ResultMR_setstate ),
    phaser.ResultMR_RF: ( ResultMR_RF_getstate, ResultMR_RF_setstate ),
    phaser.ResultMR_TF: ( ResultMR_TF_getstate, ResultMR_TF_setstate ),
    phaser.ResultANO: ( ResultANO_getstate, ResultANO_setstate ),
    phaser.BinStats: ( BinStats_getstate, BinStats_setstate ),
    phaser.HandEP: ( HandEP_getstate, HandEP_setstate ),
    phaser.ResultEP: ( ResultEP_getstate, ResultEP_setstate ),
    phaser.ResultSSD: ( ResultSSD_getstate, ResultSSD_setstate ),
    phaser.data_atom: ( data_atom_getstate, data_atom_setstate ),
    phaser.stream_string: ( stream_string_getstate, stream_string_setstate ),
    phaser.af_atom: ( af_atom_getstate, af_atom_setstate ),
    }


def enable():

    for ( phaser_object, getinitargs_method ) in GETINITARGS_FOR.items():
        phaser_object.__getinitargs__ = getinitargs_method

    for ( phaser_object, ( getstate_method, setstate_method ) ) in GET_SET_STATE_FOR.items():
        phaser_object.__getstate__ = getstate_method
        phaser_object.__setstate__ = setstate_method
