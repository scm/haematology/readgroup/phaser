from __future__ import print_function

from phaser.logoutput import FloatFormatter, PercentageFormatter

# Output object
class Handler(object):
  """
  Output to an XHTML stream
  """

  aligner_for = {
    "left": "text-align:left",
    "center": "text-align:center",
    "right": "text-align:right",
    }

  headings = [ "h1", "h2", "h3", "h4", "h5", "h6" ]

  # Formatters
  float = FloatFormatter.normal
  scientific = FloatFormatter.scientific
  percentage = PercentageFormatter
  default = str

  def __init__(self, stream, title):

    self.stack = []
    self.para_stack = []

    from xml.sax.saxutils import XMLGenerator
    self.writer = XMLGenerator( out = stream )
    self.writer.startDocument()
    self.writer.startElement(
      name = "html",
      attrs = { "xmlns": "http://www.w3.org/1999/xhtml" },
      )
    self.writer.startElement( name = "head", attrs = {} )
    self.writer.startElement( name = "title", attrs = {} )
    self.writer.characters( title )
    self.writer.endElement( name = "title" )

    # This is used to create collapsible lists
    self.writer.startElement( name = "style", attrs = { "type": "text/css" } )
    self.writer.characters( "  .row { vertical-align: top; height:auto !important; }\n" )
    self.writer.characters( "  .list {display:none; }\n" )
    self.writer.characters( "  .show {display: none; }\n" )
    self.writer.characters( "  .hide:target + .show {display: inline; }\n" )
    self.writer.characters( "  .hide:target {display: none; }\n" )
    self.writer.characters( "  .hide:target ~ .list {display:inline; }\n" )
    self.writer.characters( "  @media print { .hide, .show { display: none; } }\n" )
    self.writer.endElement( "style" )
    self.writer.endElement( name = "head" )
    self.writer.startElement( name = "body", attrs = {} )


  def close(self):

    self.writer.endElement( name = "body" )
    self.writer.endElement( name = "html" )
    self.writer.endDocument()


  def title(self, text, alignment = "center"):

    self.writer.startElement(
      name = "h1",
      attrs = { "style": self.aligner_for[ alignment ] },
      )
    self.writer.characters( text )
    self.writer.endElement( name = "h1" )


  def heading(self, text, level = 1, alignment = "left"):

    heading = self.headings[ level - 1 ]
    self.writer.startElement(
      name = heading,
      attrs = { "style": self.aligner_for[ alignment ] },
      )
    self.writer.characters( text )
    self.writer.endElement( name = heading )


  def paragraph_begin(self, alignment = "left", rewrap = True):

    key = "p" if rewrap else "pre"

    self.writer.startElement(
      name = key,
      attrs = { "style": self.aligner_for[ alignment ] },
      )
    self.para_stack.append( key )


  def paragraph_end(self):

    key = self.para_stack.pop()
    self.writer.endElement( name = key )


  def text(self, text):

    self.writer.characters( text )
    self.writer.startElement( name = "br", attrs = {} )
    self.writer.endElement( name = "br" )


  def separator(self, text, level):

    self.writer.startElement( name = "hr", attrs = {} )
    self.writer.endElement( name = "hr" )


  def whiteline(self):

    pass


  def ordered_list_begin(self, start = 1):

    self.writer.startElement( name = "ol", attrs = { "start": str( start ) } )
    self.stack.append( "ol" )


  def unordered_list_begin(self):

    self.writer.startElement( name = "ul", attrs = {} )
    self.stack.append( "ul" )


  def list_item_begin(self, text):

    self.writer.startElement( name = "li", attrs = {} )
    self.writer.characters( text )
    self.paragraph_begin()


  def list_item_end(self):

    self.paragraph_end()
    self.writer.endElement( name = "li" )


  def list_end(self):

    self.writer.endElement( name = self.stack.pop() )


  def progress_bar_start(self, caption, parts):

    self.heading( text = caption, level = 4 )
    self.writer.characters( "Consisting of %s parts" % parts )

    self.writer.startElement( name = "div", attrs = { "class": "row" } )
    self.writer.startElement(
      name = "a",
      attrs = { "href": "#hide1", "class": "hide", "id": "hide1" },
      )
    self.writer.characters( "Expand" )
    self.writer.endElement( name = "a" )
    self.writer.startElement(
      name = "a",
      attrs = { "href": "#show1", "class": "show", "id": "show1" },
      )
    self.writer.characters( "Collapse" )
    self.writer.endElement( name = "a" )
    self.writer.startElement( name = "div", attrs = { "class": "list" } )
    self.ordered_list_begin()


  def progress_bar_tick(self, data):

    self.list_item_begin( text = "iteration" )
    self.paragraph_begin()
    self.writer.characters( str( data ) )
    self.paragraph_end()
    self.list_item_end()


  def progress_bar_end(self):

    self.list_end()
    self.writer.endElement( name = "div" )
    self.writer.endElement( name = "div" )


  def task_begin(self, message, ending = "done"):

    self.writer.startElement( name = "p", attrs = {} )
    self.writer.characters( message + "..." )
    self.stack.append( ending )


  def task_end(self):

    self.writer.characters( self.stack.pop() )
    self.writer.endElement( name = "p" )


  def task_failure(self, message):

    self.stack.pop()
    self.writer.characters( message )
    self.writer.endElement( name = "p" )


  def field(self, name, value, formatter = default):

    self.writer.startElement( name = "p", attrs = {} )
    self.writer.characters( formatter( name ) )
    self.writer.characters( ": " )

    self.writer.startElement(
      name = "input",
      attrs = { "type": "text", "disabled": "disabled", "value": str( value ) },
      )
    self.writer.endElement( name = "input" )
    self.writer.endElement( name = "p" )


  def caution(self, text):

    self.paragraph_begin( alignment = "center" )
    self.writer.startElement( name = "strong", attrs = { "style": "color:red" } )
    self.writer.characters( text )
    self.writer.endElement( name = "strong" )
    self.paragraph_end()


  def sequence(self, sequence):

    self.paragraph_begin( rewrap = False )
    self.writer.characters( str( sequence ) )
    self.paragraph_end()


  def alignment(self, alignment):

    self.paragraph_begin( rewrap = False )
    self.writer.characters( str( alignment ) )
    self.paragraph_end()
