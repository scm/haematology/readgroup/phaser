from __future__ import print_function

from phaser.logoutput import FloatFormatter, PercentageFormatter
from functools import reduce

class TextParagraph(object):
  """
  A wrapped text
  """

  def __init__(self, lines):

    self.lines = lines
    self.width = max( len( l ) for l in self.lines ) if self.lines else 0


  @classmethod
  def Wrapped(cls, text, width):

    import textwrap
    return cls( lines = textwrap.wrap( text, width = width ) )


  @classmethod
  def Truncated(cls, text, width):

    lines = []

    for l in text.split( "\n" ):
      length = len( l )

      if width < length:
        lines.append( l[ : width - 3 ] + "..." )

      else:
        lines.append( l )

    return cls( lines = lines )


class FormattedParagraph(object):
  """
  A paragraph with extra formatting applied
  """

  def __init__(self, paragraph, formatter, width):

    self.paragraph = paragraph
    self.formatter = formatter
    self.width = width


  @property
  def lines(self):

    return self.formatter.process( paragraph = self.paragraph, maxwidth = self.width )


# Formatters
class Spacing(object):

  def __init__(self, above = 1, below = 1):

    self.above = above
    self.below = below


  @property
  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in [ "" ] * self.above:
      yield l

    for l in paragraph.lines:
      yield l

    for l in [ "" ] * self.below:
      yield l


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width,
        ),
      streamwidth,
      )


class Banner(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * maxwidth
    yield self.char + " " * ( maxwidth - 2 ) + self.char

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( maxwidth - 4 ), self.char )

    yield self.char + " " * ( maxwidth - 2 ) + self.char
    yield self.char * maxwidth


  def __call__(self, paragraph, streamwidth):

    width = streamwidth - self.width_modifier

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = width,
        ),
      width,
      )


class Alignment(object):

  def __init__(self, method):

    self.method = method


  @property
  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield self.method( l, maxwidth )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = streamwidth,
        ),
      streamwidth,
      )

  @classmethod
  def Left(cls):

    return cls( method = str.ljust )

  @classmethod
  def Center(cls):

    return cls( method = str.center )

  @classmethod
  def Right(cls):

    return cls( method = str.rjust )


class Frame(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return -2 * len( self.char ) - 2


  def process(self, paragraph, maxwidth):

    yield self.char * ( paragraph.width + 4 )

    for l in paragraph.lines:
      yield "%s %s %s" % ( self.char,  l.center( paragraph.width ), self.char )

    yield self.char * ( paragraph.width + 4 )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width - self.width_modifier,
        ),
      streamwidth - self.width_modifier
      )


class Underline(object):

  def __init__(self, char = "#"):

    self.char = char


  @property
  def width_modifier(self):

    return 0


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield l

    yield self.char * paragraph.width


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width,
        ),
      streamwidth,
      )


class Filling(object):

  def __init__(self, char = "="):

    self.char = char


  @property
  def width_modifier(self):

    return -2


  def process(self, paragraph, maxwidth):

    for l in paragraph.lines:
      yield ( " %s " % l ).center( maxwidth, self.char )


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = streamwidth,
        ),
      streamwidth,
      )


class Indent(object):

  def __init__(self, preamble, fillchar = " "):

    self.preamble = preamble
    self.fillchar = fillchar


  @property
  def width_modifier(self):

    return -len( self.preamble )


  def process(self, paragraph, maxwidth):

    lineiter = iter( paragraph.lines )

    try:
      yield self.preamble + lineiter.next()

    except StopIteration:
      pass

    else:
      indent = self.fillchar * len( self.preamble )

      for l in lineiter:
        yield indent + l


  def __call__(self, paragraph, streamwidth):

    return (
      FormattedParagraph(
        paragraph = paragraph,
        formatter = self,
        width = paragraph.width - self.width_modifier,
        ),
      streamwidth - self.width_modifier,
      )


  @classmethod
  def Block(cls, indent = 4, fillchar = " "):

    return cls( preamble = fillchar * indent, fillchar = fillchar )


class State(object):

  def __init__(self, data):

    self.data = data


  @property
  def width_modifier(self):

    return 0


  def __call__(self, paragraph, streamwidth):

    return ( paragraph, streamwidth )


class Dummy(object):

  @property
  def width_modifier(self):

    return 0

  def __call__(self, paragraph, streamwidth):

    return ( paragraph, streamwidth )


# Objects holding state information
class ListData(object):
  """
  Holds list details
  """

  def __init__(self, markergen, fillchar):

    self.markergen = markergen
    self.fillchar = fillchar


  def next(self):

    return self.markergen.next()


  @classmethod
  def Bullet(cls, marker, fillchar = " "):

    import itertools
    return cls(
      markergen = ( marker for i in itertools.count() ),
      fillchar = fillchar,
      )

  @classmethod
  def Numbered(cls, start = 1, separator = ". ", width = 4, fillchar = " "):

    if width < 1:
      raise ValueError("Minimum value for width is 1")

    formatting = "%" + str( width ) + "d" + separator

    import itertools
    return cls(
      markergen = ( formatting % i for i in itertools.count( start ) ),
      fillchar = fillchar,
      )


class TaskData(object):
  """
  Holds task details
  """

  def __init__(self, ending, sequel):

    self.ending = ending
    self.sequel = sequel


class ProgressBarData(object):
  """
  Holds progress bar details
  """

  def __init__(self, hits_per_tick, postwrite):

    self.hits_per_tick = hits_per_tick
    self.postwrite = postwrite
    self.hits = 0


  def increment(self):

    self.hits += 1

    if ( self.hits - self.hits_per_tick // 2 ) % self.hits_per_tick == 0:
      return True

    else:
      return False

# Styles
class HeaderStyle(object):
  """
  Return correct formatter for each style level
  """

  def __init__(self, char):

    self.char = char


  def heading(self):

    if self.char != " ":
      return Underline( char = self.char )

    else:
      return Dummy()


  def separator(self):

    return Filling( char = self.char )


# Output object
class Handler(object):
  """
  Output to a stream
  """

  aligner_for = {
    "left": Dummy(),
    "center": Alignment.Center(),
    "right": Alignment.Right(),
    }

  styles = [
    HeaderStyle( char = "=" ),
    HeaderStyle( char = "+" ),
    HeaderStyle( char = "-" ),
    HeaderStyle( char = " " ),
    ]

  delimiter = "|"
  progress = "="
  bullet = "-"

  # Field formatters
  float = FloatFormatter.normal
  scientific = FloatFormatter.scientific
  percentage = PercentageFormatter
  default = str

  def __init__(self, stream, width):

    self.stream = stream
    self.maxwidth = width
    self.formatters = []
    self.wrappers = [ self.text_wrap ]
    self.whitespace = True


  def close(self):

    pass


  def title(self, text, alignment = "center"):

    self.push( formatter = Banner() )
    self.push( formatter = self.aligner_for[ alignment ] )
    self.text( text = text )
    self.pop()
    self.pop()


  def heading(self, text, level = 1, alignment = "left"):

    self.whiteline()
    self.push( formatter = self.styles[ level - 1 ].heading() )
    self.push( formatter = self.aligner_for[ alignment ] )
    self.text( text = text )
    self.pop()
    self.pop()


  def paragraph_begin(self, alignment = "left", rewrap = True):

    self.push( formatter = self.aligner_for[ alignment ] )

    if rewrap:
      self.wrappers.append( self.text_wrap )

    else:
      self.wrappers.append( self.text_keep )


  def paragraph_end(self):

    self.pop()
    self.wrappers.pop()
    self.whiteline()


  def text(self, text):

    self.write( paragraph = self.wrappers[-1]( text = text ) )
    self.whitespace = False


  def text_wrap(self, text):

    return TextParagraph.Wrapped( text = text, width = self.width )


  def text_keep(self, text):

    return TextParagraph.Truncated( text = text, width = self.width )


  def separator(self, text, level):

    self.push( formatter = self.aligner_for[ "center" ] )
    self.push( formatter = self.styles[ level - 1 ].separator() )
    self.text( text = text )
    self.pop()
    self.pop()


  def whiteline(self):

    if not self.whitespace:
      self.rawwrite( "\n" )
      self.whitespace = True


  def ordered_list_begin(self, start = 1):

    self.push( formatter = State( data = ListData.Numbered( start = start ) ) )
    self.whitespace = True


  def unordered_list_begin(self):

    self.push(
      formatter = State( data = ListData.Bullet( marker = self.bullet + " " ) ),
      )
    self.whitespace = True


  def list_item_begin(self, text):

    self.whiteline()
    owner = self.top()
    marker = owner.data.next()
    self.push( Indent( preamble = marker ) )
    self.text( text = text )
    self.pop()
    self.push(
      Indent.Block( indent = len( marker ), fillchar = owner.data.fillchar )
      )

  def list_item_end(self):

    self.pop()


  def list_end(self):

    self.pop()
    self.whiteline()


  def progress_bar_start(self, caption, parts):

    width = self.width

    if width < len( caption ):
      caption = caption[ : width - 3 ] + "..."

    if width < 2 * len( self.delimiter ) + len( self.progress ):
      raise ValueError("Stream width insufficient to draw shortest progress bar")

    ticks = ( width - 2 * len( self.delimiter ) ) // len( self.progress )
    hits_per_tick = ( parts - 1 ) // ticks + 1
    strdel = "\0" * len( self.delimiter )
    progspace = " " * len( self.progress ) * ( parts // hits_per_tick )

    paragraph = self.format(
      paragraph = TextParagraph(
        lines = [ caption, "", strdel + progspace + strdel ],
        ),
      )
    formatted = "\n".join( paragraph.lines )
    split = formatted.split( strdel )
    assert len( split ) == 3
    self.rawwrite( text = split[0] )
    self.rawwrite( text = self.delimiter )
    self.flush()

    self.push(
      State(
        data = ProgressBarData( hits_per_tick = hits_per_tick, postwrite = split[2] ),
        )
      )


  def progress_bar_tick(self, data):

    owner = self.top()

    if owner.data.increment():
      self.rawwrite( text = self.progress )
      self.flush()


  def progress_bar_end(self):

    owner = self.top()
    self.rawwrite( text = self.delimiter )
    self.rawwrite( text = owner.data.postwrite )
    self.rawwrite( text = "\n" )
    self.pop()


  def task_begin(self, message, ending = "done"):

    strdel = "\0" * len( ending )
    paragraph = self.format(
      paragraph = TextParagraph( lines = [ message + "..." + strdel ] ),
      )
    formatted = "\n".join( paragraph.lines )
    split = formatted.split( strdel )
    assert len( split ) == 2
    self.rawwrite( text = split[0] )
    self.flush()

    self.push( State( data = TaskData( ending = ending, sequel = split[1] ) ) )


  def task_end(self):

    owner = self.top()
    self.rawwrite( text = owner.data.ending )
    self.rawwrite( text = owner.data.sequel )
    self.rawwrite( text = "\n" )
    self.pop()


  def task_failure(self, message):

    owner = self.top()
    self.rawwrite( text = message )
    self.rawwrite( text = owner.data.sequel )
    self.rawwrite( text = "\n" )
    self.pop()


  def field(self, name, value, formatter = default):

    self.text( text = "%s: %s" % ( name, formatter( value ) ) )


  def caution(self, text):

    self.push( formatter = self.aligner_for[ "center" ] )
    self.push( formatter = Frame() )
    self.text( text = text )
    self.pop()
    self.pop()


  def sequence(self, sequence):

    self.whiteline()
    self.paragraph_begin( rewrap = False )
    self.text( text = sequence.format( width = self.width ) )
    self.paragraph_end()


  def alignment(self, alignment):

    self.whiteline()
    self.paragraph_begin( rewrap = False )
    self.text( text = alignment.simple_format( width = self.width ) )
    self.paragraph_end()


  # Internal methods
  def push(self, formatter):

    if self.width + formatter.width_modifier <= 0:
      raise RuntimeError("Stream width zero or below")

    self.formatters.append( formatter )


  def pop(self):

    self.formatters.pop()


  def top(self):

    return self.formatters[-1]


  @property
  def width(self):

    return self.maxwidth + sum( f.width_modifier for f in self.formatters )


  def format(self, paragraph):

    ( formatted, streamwidth ) = reduce(
      self.apply,
      reversed( self.formatters ),
      ( paragraph, self.width ),
      )

    return formatted


  def write(self, paragraph):

    paragraph = self.format( paragraph = paragraph )

    for l in paragraph.lines:
      self.stream.write( l.rstrip() )
      self.stream.write( "\n" )


  def rawwrite(self, text):

    self.stream.write( text )


  def flush(self):

    self.stream.flush()


  @staticmethod
  def apply(data, formatter):

    return formatter( paragraph = data[0], streamwidth = data[1] )


class DebugHandler(Handler):
  """
  Expand all progress bars
  """

  def progress_bar_start(self, caption, parts):

    self.heading( text = caption, level = 4 )
    self.text( text = "Consisting of %s parts" % parts )
    self.ordered_list_begin()


  def progress_bar_tick(self, data):

    self.list_item_begin( text = "iteration" )
    self.text( text = data )
    self.list_item_end()


  def progress_bar_end(self):

    self.list_end()
