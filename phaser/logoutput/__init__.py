from __future__ import print_function

# Text formatters used in multiple handlers
class FloatFormatter(object):
  """
  Floating point object
  """

  def __init__(self, symbol, decimal):

    self.format = "%." + str( decimal ) + symbol


  def __call__(self, value):

    return self.format % value


  @classmethod
  def normal(cls, decimal):

    return cls( symbol = "f", decimal = decimal )


  @classmethod
  def scientific(cls, decimal):

    return cls( symbol = "e", decimal = decimal )


class PercentageFormatter(object):
  """
  Formats as a percentage
  """

  def __init__(self, decimal):

    self.format = "%." + str( decimal ) + "f%%"


  def __call__(self, value):

    return self.format % ( 100.0 * value )


class DummyHandler(object):
  """
  A logger channel that does not do anything

  Note this is a singleton object
  """

  aligner_for = {
    "left": None,
    "center": None,
    "right": None,
    }
  headings = [ None, None, None, None ]

  float = FloatFormatter.normal
  scientific = FloatFormatter.scientific
  percentage = PercentageFormatter
  default = None

  @staticmethod
  def close():

    pass


  @classmethod
  def title(cls, text, alignment = "center"):

    cls.aligner_for[ alignment ]


  @classmethod
  def heading(cls, text, level = 1, alignment = "center"):

    cls.aligner_for[ alignment ]
    cls.headings[ level - 1 ]


  @classmethod
  def paragraph_begin(cls, alignment = "left", rewrap = True):

    cls.aligner_for[ alignment ]


  @staticmethod
  def paragraph_end():

    pass


  @staticmethod
  def text(text):

    pass


  @staticmethod
  def preformatted_text(text):

    pass


  @staticmethod
  def separator(text, level):

    pass


  @staticmethod
  def whiteline():

    pass


  @staticmethod
  def ordered_list_begin(start = 1):

    pass


  @staticmethod
  def unordered_list_begin():

    pass


  @staticmethod
  def list_item_begin(text):

    pass


  @staticmethod
  def list_item_end():

    pass


  @staticmethod
  def list_end():

    pass


  @staticmethod
  def progress_bar_start(caption, parts):

    pass


  @staticmethod
  def progress_bar_tick(data):

    pass


  @staticmethod
  def progress_bar_end():

    pass


  @staticmethod
  def task_begin(message, ending = "done"):

    pass


  @staticmethod
  def task_end():

    pass


  @staticmethod
  def task_failure(message):

    pass


  @staticmethod
  def field(name, value, formatter = default):

    pass


  @staticmethod
  def caution(text):

    pass


  @staticmethod
  def sequence(sequence):

    pass


  @staticmethod
  def alignment(alignment):

    pass


class Output(object):
  """
  Separates output to several streams, and filters according to verbosity level
  """

  def __init__(self, warning, info, verbose, debug):

    self.warning = warning
    self.info = info
    self.verbose = verbose
    self.debug = debug


  def demoted(self):

    return Forward( logger = self )


  @classmethod
  def Warning(cls, handler):

    return cls(
      warning = handler,
      info = DummyHandler,
      verbose = DummyHandler,
      debug = DummyHandler,
      )


  @classmethod
  def Info(cls, handler):

    return cls(
      warning = handler,
      info = handler,
      verbose = DummyHandler,
      debug = DummyHandler,
      )


  @classmethod
  def Verbose(cls, handler):

    return cls(
      warning = handler,
      info = handler,
      verbose = handler,
      debug = DummyHandler,
      )


  @classmethod
  def Debug(cls, handler):

    return cls(
      warning = handler,
      info = handler,
      verbose = handler,
      debug = handler,
      )


FACTORY_FOR = {
  "warning": Output.Warning,
  "info": Output.Info,
  "verbose": Output.Verbose,
  "debug": Output.Debug,
  }


class Forward(object):
  """
  A logger that embeds another logger, and change priorities of channels
  """

  def __init__(self, logger):

    self.logger = logger


  def demoted(self):

    return self.__class__( logger = self )


  @property
  def warning(self):

    return self.logger.warning


  @property
  def info(self):

    return self.logger.verbose


  @property
  def verbose(self):

    return self.logger.debug


  @property
  def debug(self):

    return self.logger.debug
