from __future__ import print_function

# Exceptions
class BenchmarkError(Exception):
  """
  Module exception
  """


class PhaserError(BenchmarkError):
  """
  Error from Phaser
  """

  def __init__(self, result):

    self.result = result


  def __str__(self):

    return "Phaser error (%s): %s" % ( self.result.ErrorName(), self.result.ErrorMessage() )


# Results
class structure_factor_data(object):

  @staticmethod
  def set_data(phaser_input, indices, reflections, sigmas):

    phaser_input.setREFL_F_SIGF( indices, reflections, sigmas )


  @staticmethod
  def set_labels(phaser_input, reflections, sigmas):

    phaser_input.setLABI_F_SIGF( reflections, sigmas )


  @staticmethod
  def get_reflections(result):

    return result.getFobs()


  @staticmethod
  def get_sigmas(result):

    return result.getSigFobs()


class intensity_data(object):

  @staticmethod
  def set_data(phaser_input, indices, reflections, sigmas):

    phaser_input.setREFL_I_SIGI( indices, reflections, sigmas )


  @staticmethod
  def set_labels(phaser_input, reflections, sigmas):

    phaser_input.setLABI_I_SIGI( reflections, sigmas )


  @staticmethod
  def get_reflections(result):

    return result.getIobs()


  @staticmethod
  def get_sigmas(result):

    return result.getSigIobs()


class Data(object):
  """
  Reflection data
  """

  def __init__(self, cell, hall, indices, handler, reflections, sigmas, annotation):

    self.cell = cell
    self.hall = hall
    self.indices = indices
    self.handler = handler
    self.reflections = reflections
    self.sigmas = sigmas
    self.annotation = annotation


  def apply(self, phaser_input):

    self.handler.set_data(
      phaser_input = phaser_input,
      indices = self.indices,
      reflections = self.reflections,
      sigmas = self.sigmas,
      )
    phaser_input.setCELL6( self.cell )
    phaser_input.setSPAC_HALL( self.hall )


  def __str__(self):

    return "Dataset: %s" % self.annotation


  @classmethod
  def structure_factors_from_mtz(cls, filename, fp, sigfp):

    return cls.data_from_mtz(
      filename = filename,
      reflections = fp,
      sigmas = sigfp,
      handler = structure_factor_data,
      )


  @classmethod
  def intensities_from_mtz(cls, filename, i, sigi):

    return cls.data_from_mtz(
      filename = filename,
      reflections = i,
      sigmas = sigi,
      handler = intensity_data,
      )


  @classmethod
  def data_from_mtz(cls, filename, reflections, sigmas, handler):

    import phaser

    inp = phaser.InputMR_DAT()
    inp.setHKLI( filename )
    handler.set_labels( phaser_input = inp, reflections = reflections, sigmas = sigmas )
    inp.setMUTE( True )
    result = phaser.runMR_DAT( inp )

    if result.Failed():
      raise PhaserError( result = result )

    return cls(
      cell = result.getUnitCell(),
      hall = result.getSpaceGroupHall(),
      handler = handler,
      indices = result.getMiller(),
      reflections = handler.get_reflections( result = result ),
      sigmas = handler.get_sigmas( result = result ),
      annotation = "'%s', columns %s, %s" % ( filename, reflections, sigmas ),
      )


class AnisotropyCorrection(object):
  """
  Anisotropy correction
  """

  def __init__(self, normalization):

    self.normalization = normalization


  def apply(self, phaser_input):

    phaser_input.setNORM_DATA( self.normalization )
    phaser_input.setMACA_PROT( "OFF" )


  def __str__(self):

    return "Wilson B: %.2f\nWilson K: %.2f\nAnisotropy: %s" % (
      self.normalization.WILSON_B,
      self.normalization.WILSON_K,
      " ".join( [ "%.3f" ] * 6 ) % self.normalization.ANISO,
      )


  @classmethod
  def from_dataset(cls, dataset):

    import phaser

    inp = phaser.InputANO()
    dataset.apply( phaser_input = inp )
    inp.setMUTE( True )
    inp.setHKLO( False )

    result = phaser.runANO( inp )

    if result.Failed():
      raise PhaserError( result = result )

    return cls( normalization = result.getSigmaN() )


class HasTNCS(object):
  """
  Present TNCS
  """

  def __init__(self, vector, angle):

    self.vector = vector
    self.angle = angle


  def __call__(self, phaser_input):

    phaser_input.setTNCS_ROTA_ANGL( self.angle )
    phaser_input.setTNCS_ROTA_RANG( 0 )
    phaser_input.setTNCS_ROTA_SAMP( 0 )
    phaser_input.setTNCS_TRAN_VECT( self.vector )


  def __str__(self):

    return "TNCS: vector: %s, angle: %s" % ( str( self.vector ), str( self.angle ) )


class NoTNCS(object):
  """
  No TNCS
  """

  def __call__(self, phaser_input):

    phaser_input.setTNCS_USE( False )


  def __str__(self):

    return "No TNCS found"


class Tncs(object):
  """
  TNCS handling
  """

  def __init__(self, handler):

    self.handler = handler


  def apply(self, phaser_input):

    self.handler( phaser_input = phaser_input )


  def __str__(self):

    return str( self.handler )


  @classmethod
  def from_dataset(cls, dataset, aniso, composition):

    import phaser

    inp = phaser.InputNCS()
    dataset.apply( phaser_input = inp )
    aniso.apply( phaser_input = inp )
    composition.apply( phaser_input = inp )
    inp.setMUTE( True )
    inp.setHKLO( False )

    result = phaser.runNCS( inp )

    if result.Failed():
      raise PhaserError( result = result )

    if result.hasTNCS():
      handler = HasTNCS( vector = result.getVector(), angle = result.getAngles() )

    else:
      handler = NoTNCS()

    return cls( handler = handler )

"""
class Chain_old(object):
  ""
  Chain
  ""

  def __init__(self, chain, mmt_name, annotation, component, alignment):

    import iotbx.pdb
    model = iotbx.pdb.hierarchy.model()
    model.append_chain( chain.detached_copy() )
    self.root = iotbx.pdb.hierarchy.root()
    self.root.append_model( model )

    self.mmt_name = mmt_name
    self.annotation = annotation
    self.component = component

    indexer = dict(
      ( rg, i ) for ( i, rg ) in enumerate( chain.residue_groups() )
      )
    self.alignment = [
      indexer[ rg ] if rg is not None else None for rg in alignment
      ]


  @property
  def mmt(self):

    from phaser import mmtype
    return mmtype.MOLECULE_TYPE_CALLED[ self.mmt_name ]


  @property
  def chain(self):

    return self.root.only_chain()


  def identifier(self):

    return self.chain.id


  def residue_groups(self):

    return self.chain.residue_groups()


  def domain_mapping_indexer(self):

    from phaser.pipeline.proxy import domain
    return domain.ResidueGroupIndexer( chain = self.chain )


  def alignment_mapping_indexer(self):

    rgs = self.chain.residue_groups()
    return dict(
      ( rgs[ rgi ], i ) for ( i, rgi ) in enumerate( self.alignment ) if rgi is not None
      )


  def residue_group_index(self, index):

    assert 0 <= index < len( self.alignment )
    return self.alignment[ index ]


  def as_detached_iotbx_chain(self):

    return self.chain.detached_copy()


  def complete_underlying_chain(self):

    return self


  def __str__(self):

    return str( self.annotation )
"""

class Chain(object):
  """
  Chain
  """

  def __init__(self, root, mmt_name, reference, indices, annotation, origin):

    assert len( list( root.chains() ) ) == 1
    assert len( reference ) == len( indices )

    self._root = root
    self._mmt_name = mmt_name
    self._reference = reference
    self._indices = indices
    self._annotation = annotation
    self._origin = origin


  def mmt(self):

    from phaser import mmtype
    return mmtype.MOLECULE_TYPE_CALLED[ self._mmt_name ]


  def iotbx_root(self):

    return self._root


  def iotbx_chain(self):

    return self.iotbx_root().only_chain()


  def identifier(self):

    return self.iotbx_chain().id


  def origin(self):

    return self._origin


  def whole(self):

    return Whole( count = len( self.reference_sequence() ) )


  def reference_sequence(self):

    return self._reference


  def gapless_reference_sequence(self):

    return [ c for c in self.reference_sequence() if c is not None ]


  def aligned_residue_indices(self):

    return self._indices


  def aligned_residue_groups(self):

    rgs = self.iotbx_chain().residue_groups()
    return [ rgs[i] if i is not None else None for i in self.aligned_residue_indices() ]


  def resid_to_residue_indexer(self):

    from phaser.pipeline.proxy import domain
    return domain.ResidueGroupIndexer( chain = self.iotbx_chain() )


  def residue_to_reference_position_indexer(self):

    rgs = self.iotbx_chain().residue_groups()
    return dict(
      ( rgs[ rgi ], i )
      for ( i, rgi ) in enumerate( self.aligned_residue_indices() )
      if rgi is not None
      )


  def reference_position_to_residue_index_indexer(self):

    return dict(
      ( i, ri ) for ( i, ri ) in enumerate( self.aligned_residue_indices() )
      if ri is not None
      )


  def gapless_to_reference_position_indexer(self):

    import itertools
    counter = itertools.count()

    return dict(
      ( next(counter), i ) for ( i, c ) in enumerate( self.reference_sequence() )
      if c is not None
      )


  def reference_to_gapless_position_indexer(self):

    import itertools
    counter = itertools.count()

    return dict(
      ( i, next(counter) ) for ( i, c ) in enumerate( self.reference_sequence() )
      if c is not None
      )


  def __str__(self):

    return str( self._annotation )


  @classmethod
  def from_chain(
    cls,
    chain,
    mmt_name,
    reference_sequence,
    aligned_residues,
    annotation,
    origin,
    reference_gap = "-",
    ):

    import iotbx.pdb
    model = iotbx.pdb.hierarchy.model()
    model.append_chain( chain.detached_copy() )
    root = iotbx.pdb.hierarchy.root()
    root.append_model( model )

    indexer = dict(
      ( rg, i ) for ( i, rg ) in enumerate( chain.residue_groups() )
      )

    return cls(
      root = root,
      mmt_name = mmt_name,
      reference = [ None if c == reference_gap else c for c in reference_sequence ],
      indices = [
        indexer[ rg ] if rg is not None else None for rg in aligned_residues
        ],
      annotation = annotation,
      origin = origin,
      )


class SculptedChain(object):
  """
  Sculpted chain and sequence identity
  """

  def __init__(self, chain, identity):

    self.chain = chain
    self.identity = identity


  def error(self):

    return SeqIdError( identity = self.identity )


"""
class Selection_old(object):
  ""
  Domain
  ""

  def __init__(self, chain, identifier, segments):

    self._chain = chain
    self._identifier = identifier
    self._segments = segments


  def residue_group_positions(self):

    return [
      i for i in self.positions if self.chain.residue_group_index( index = i ) is not None
      ]


  def indices(self):

    import itertools
    return itertools.chain.from_iterable( self._segments )


  def identifier(self):

    return "%s_%s" % ( self.chain().identifier(), self._identifier )


  def detached_iotbx_chain(self):

    import iotbx.pdb
    chain = iotbx.pdb.hierarchy.chain()
    chain.id = self.chain.identifier()
    rgs = self.chain.residue_groups()

    for pos in self.residue_group_positions():
      copy = rgs[ self.chain.residue_group_index( index = pos ) ].detached_copy()
      chain.append_residue_group( copy )

    return chain


  def segments(self):

    from phaser import sequtil
    return sequtil.split(
      sequence = self.positions,
      consecutivity = sequtil.sequence_consecutivity,
      )


  def __str__(self):

    return "Residue ranges:\n%s" % "\n".join( "  %s" % s for s in self._segments )
"""

class Segment(object):
  """
  Segment
  """

  def __init__(self, begin, end):

    assert begin <= end
    self.begin = begin
    self.end = end


  def __call__(self):

    return range( self.begin, self.end + 1 )


  def __iter__(self):

    return iter( self() )


  def __contains__(self, index):

    return self.begin <= index <= self.end


  def __str__(self):

    return "%s -> %s" % ( self.begin + 1, self.end + 1 )


class Selection(object):
  """
  Selection of residue ranges
  """

  def __init__(self, segments):

    self._segments = segments


  def indices(self):

    import itertools
    return itertools.chain.from_iterable( self._segments )


  def segments(self):

    return self._segments


  def as_residue_groups(self, chain):

    from phaser import sequtil
    mapper = sequtil.SegmentMapping(
      indexer = chain.reference_position_to_residue_index_indexer(),
      )
    rgs = chain.iotbx_chain().residue_groups()

    selecteds = []

    for segment in self._segments:
      try:
        ( start, end ) = mapper( segment = segment() )

      except sequtil.SequenceError:
        pass

      else:
        selecteds.extend( rgs[ start : end + 1 ] )

    return selecteds


  def as_detached_iotbx_chain(self, chain):

    import iotbx.pdb
    copy = iotbx.pdb.hierarchy.chain()
    copy.id = chain.iotbx_chain().id

    for rg in self.as_residue_groups( chain = chain ):
      copy.append_residue_group( rg.detached_copy() )

    return copy


  def __str__(self):

    return "ranges: %s" % ", ".join( str( s ) for s in self._segments )


class Whole(object):
  """
  Special object to express that a full chain is selected
  """

  def __init__(self, count):

    self.count = count


  def indices(self):

    return range( self.count )


  def segments(self):

    return [ lambda: self.indices() ]


  def as_residue_groups(self, chain):

    return chain.iotbx_chain().residue_groups()


  def as_detached_iotbx_chain(self, chain):

    return chain.iotbx_chain().detached_copy()


  def __str__(self):

    return "whole chain"


class Model(object):
  """
  Model structure
  """

  def __init__(self, name, chain, selection):

    self.name = name
    self.chain = chain

    assert set( self.chain.whole().indices() ).issuperset( selection.indices() )
    self.selection = selection


  def detached_iotbx_chain(self):

    return self.selection.as_detached_iotbx_chain( chain = self.chain )


  def aligned_residue_group_indices(self):

    aligneds = self.chain.aligned_residue_groups()
    return [ i for i in self.selection.indices() if aligneds[i] ]


  def aligned_residue_groups(self):

    aligneds = self.chain.aligned_residue_groups()
    return [ aligneds[i] for i in self.aligned_residue_group_indices() ]


  def iotbx_root(self):

    import iotbx.pdb
    model = iotbx.pdb.hierarchy.model()
    model.append_chain( self.detached_iotbx_chain() )
    root = iotbx.pdb.hierarchy.root()
    root.append_model( model )

    return root


  def content(self):

    return self.iotbx_root().as_pdb_string()


  def identifier(self):

    return self.name


  def __str__(self):

    return "%s (%s - %s)" % ( self.name, self.chain, self.selection )


class RmsError(object):
  """
  Error expressed as RMS
  """

  def __init__(self, rmsd):

    self.rmsd = rmsd


  def apply(self, phaser_input, modlid, content, name):

    import phaser
    phaser_input.addENSE_STR_RMS(
      modlid = modlid,
      content = content,
      name = name,
      rms = self.rmsd,
      ptgrp_tolerances = phaser.data_ptgrp(),
      )


  def sol_line_segment(self):

    return "RMS %.2f" % self.rmsd


  def __str__(self):

    return "rms error %.2f" % self.rmsd


class SeqIdError(object):
  """
  Error expressed as sequence identity
  """

  def __init__(self, identity):

    self.identity = identity


  def apply(self, phaser_input, modlid, content, name):

    import phaser
    phaser_input.addENSE_STR_ID(
      modlid = modlid,
      content = content,
      name = name,
      identity = self.identity,
      ptgrp_tolerances = phaser.data_ptgrp(),
      )

  def sol_line_segment(self):

    return "ID %.2f" % ( 100.0 * self.identity )


  def __str__(self):

    return "sequence identity %.2f" % ( 100.0 * self.identity )


class Ensemble(object):
  """
  Phaser model
  """

  def __init__(self, model, error):

    self.model = model
    self.error = error


  def identifier(self):

    return self.model.identifier()


  def apply(self, phaser_input):

    self.error.apply(
      phaser_input = phaser_input,
      modlid = self.identifier(),
      content = self.model.content(),
      name = self.identifier(),
      )


  def detached_iotbx_chain(self):

    return self.model.detached_iotbx_chain()


  @classmethod
  def Rms(cls, model, rmsd):

    return cls( model = model, error = RmsError( rmsd = rmsd ) )


  @classmethod
  def Seqid(cls, model, identity):

    return cls( model = model, error = SeqIdError( identity = identity ) )


class Molecule(object):
  """
  Placed ensemble
  """

  def __init__(
    self,
    ensemble,
    rotation,
    translation,
    bfactor,
    fixr = False,
    fixt = False,
    fixb = False,
    ):

    self.ensemble = ensemble
    self.rotation = rotation
    self.translation = translation
    self.bfactor = bfactor
    self.fixr = fixr
    self.fixt = fixt
    self.fixb = fixb


  def mr_ndim(self, cell):

    import phaser
    return phaser.mr_ndim(
      MODLID_ = self.ensemble.identifier(),
      R_ = self.rotation,
      FRAC_ = True,
      inFRAC_ = True,
      TRA_ = cell.fractionalize( self.translation ),
      BFAC_= self.bfactor,
      FIXR_ = self.fixr,
      FIXT_ = self.fixt,
      FIXB_ = self.fixb,
      )

  def sol_file_line(self, cell):

    import scitbx.math
    return "ENSE %s EULER %s TRA %s %s" % (
      self.ensemble.identifier(),
      "%.3f %.3f %.3f" % scitbx.math.euler_angles_zyz_angles( self.rotation ),
      "%.3f %.3f %.3f" % cell.fractionalize( self.translation ),
      self.ensemble.error.sol_line_segment(),
      )


  def transformation(self):

    import scitbx.matrix
    return scitbx.matrix.rt( ( self.rotation, self.translation ) )


  def detached_iotbx_chain(self):

    chain = self.ensemble.detached_iotbx_chain()
    atoms = chain.atoms()
    atoms.set_xyz( self.transformation() * atoms.extract_xyz() )
    return chain


  def __str__(self):

    import scitbx.math
    return "ENSE %s EULER %s ORTH %s %s" % (
      self.ensemble.identifier(),
      "%.3f %.3f %.3f" % scitbx.math.euler_angles_zyz_angles( self.rotation ),
      "%.3f %.3f %.3f" % self.translation,
      self.ensemble.error.sol_line_segment(),
      )


class Structure(object):
  """
  Target structure
  """

  def __init__(self, symmetry):

    self.symmetry = symmetry
    self._molecules = []
    self.ensemble_named = {}


  def add(self, molecule):

    enseid = molecule.ensemble.identifier()

    if enseid in self.ensemble_named and self.ensemble_named[ enseid ] != molecule.ensemble:
      raise BenchmarkError("Ensemble names not unique")

    self.ensemble_named[ enseid ] = molecule.ensemble

    self._molecules.append( molecule )


  def get_ensemble(self, identifier):

    return self.ensemble_named[ identifier ]


  def get_ensembles(self):

    return self.ensemble_named.values()


  def get_ensemble_ids(self):

    return self.ensemble_named.keys()


  def molecules(self):

    return self._molecules


  def mr_set(self):

    import phaser

    cell = self.symmetry.unit_cell()

    return phaser.mr_set(
      " " + self.identifier(),
      [ m.mr_ndim( cell = cell ) for m  in self.molecules() ],
      []
      )


  def apply(self, phaser_input):

    for ense in self.get_ensembles():
      ense.apply( phaser_input = phaser_input )

    phaser_input.setSOLU( [ self.mr_set() ] )


  def as_iotbx_hierarchy(self):

    import iotbx.pdb
    model = iotbx.pdb.hierarchy.model()

    for ( cid, mol ) in zip( iotbx.pdb.systematic_chain_ids(), self.molecules() ):
      chain = mol.detached_iotbx_chain()
      chain.id = cid
      model.append_chain( chain )

    root = iotbx.pdb.hierarchy.root()
    root.append_model( model )

    return root


  def identifier(self):

    return "structure%s" % id( self )


  def __str__(self):

    cell = self.symmetry.unit_cell()
    return "\n".join( m.sol_file_line( cell = cell ) for m in self.molecules() )


class Superposition(object):
  """
  Superposition
  """

  def __init__(self, matrix, rmsd):

    self.matrix = matrix
    self.rmsd = rmsd


class protein_component(object):

  def __init__(self, sequence):

    self.sequence = sequence


  def apply(self, phaser_input):

    phaser_input.addCOMP_PROT_STR_NUM( self.sequence_string(), 1 )


  def sequence_string(self):

    return self.sequence.sequence


  def __str__(self):

    return "Protein component: %s (%s residues)" % (
      self.sequence.name,
      len( self.sequence ),
      )



class nucleic_acid_component(object):

  def __init__(self, sequence):

    self.sequence = sequence


  def apply(self, phaser_input):

    phaser_input.addCOMP_NUCL_STR_NUM( self.sequence_string(), 1 )


  def sequence_string(self):

    return self.sequence.sequence


  def __str__(self):

    return "Nucleic acid component: %s (%s residues)" % (
      self.sequence.name,
      len( self.sequence ),
      )


class Composition(object):
  """
  Target composition
  """

  def __init__(self, components):

    self.components = components


  def apply(self, phaser_input):

    for component in self.components:
      component.apply( phaser_input = phaser_input )


  def matching_component_to(self, chain, mmt, min_sequence_identity = 0.8):

    from phaser import chainalign
    aligner = chainalign.IdentityPostFilter(
      aligner = chainalign.ChainAlignmentAlgorithm(
        aligner = chainalign.ChainAligner.Geometry(
          rgs = chain.residue_groups(),
          mmt = mmt,
          annotation = "Chain_%s" % chain.id,
          ),
        ),
      threshold = min_sequence_identity,
      )

    matchings = []

    for ( index, component ) in enumerate( self.components, start = 1 ):
      trial = chainalign.Trial(
        gapped = component.sequence_string(),
        gap = "-",
        annotation = "component_%s" % index,
        )

      try:
        result = aligner( trial = trial )

      except chainalign.ChainAlignFailure:
        pass

      else:
        matchings.append( ( component, result ) )

    if not matchings:
      raise BenchmarkError("Could not match any components to chain %s" % chain.id)

    return max( matchings, key = lambda p: p[1].identities )


  def __str__(self):

    return "Components:\n%s" % "\n".join( "  %s" % c for c in self.components )


class Case(object):
  """
  Attribute group
  """

  def __init__(self, composition, data, corrections):

    self.composition = composition
    self.data = data
    self.corrections = corrections


  def apply(self, phaser_input):

    self.data.apply( phaser_input = phaser_input )

    for corr in self.corrections:
      corr.apply( phaser_input = phaser_input )

    self.composition.apply( phaser_input = phaser_input )


# Utility functions
def get_pdb_structure_accessor(mirror, cache_dir, logger):

  from phaser import tbx_utils

  return create_database_accessor(
    downloader = tbx_utils.StreamDownload.PdbFetch( mirror = mirror ),
    cachedir = cache_dir,
    postfix = ".pdb",
    serializing = "string",
    title = "PDB structure",
    logger = logger,
    )


def get_pdb_sequence_accessor(mirror, cache_dir, logger):

  from phaser import tbx_utils

  return create_database_accessor(
    downloader = tbx_utils.StreamDownload.PdbFetch(
      data_type = "fasta",
      mirror = mirror,
      ),
    cachedir = cache_dir,
    postfix = ".fa",
    serializing = "string",
    title = "PDB sequence",
    logger = logger,
    )


def get_pdb_reflection_accessor(mirror, cache_dir, logger):

  from phaser import tbx_utils

  return create_database_accessor(
    downloader = tbx_utils.StreamDownload.PdbFetch(
      data_type = "xray",
      format = "cif",
      mirror = mirror,
      ),
    cachedir = cache_dir,
    postfix = ".cif",
    serializing = "string",
    title = "PDB reflection data",
    logger = logger,
    )


def get_pdbe_cath_accessor(cache_dir, logger):

  from iotbx.pdb import pdbe

  return create_database_accessor(
    downloader = pdbe.PDB_SIFTS_MAPPING_CATH.single,
    cachedir = cache_dir,
    postfix = ".cath",
    serializing = "json",
    title = "CATH domain annotation",
    logger = logger,
    )


def create_database_accessor(downloader, cachedir, postfix, serializing, title, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.heading( text = "%s access" % title, level = 1 )

  if cachedir:
    from phaser.pipeline import cache
    import functools

    creator_for = {
      "string": cache.FileCache.String,
      "json": functools.partial( cache.FileCache.Json, prettyprint = False ),
      "pickle": cache.FileCache.Pickle,
      }

    assert serializing in creator_for
    info.field( name = "Access type", value = "cached" )
    info.field( name = "Cache directory", value = cachedir )
    info.field( name = "File extension", value = postfix )

    import os

    if not os.path.exists( cachedir ):
      os.mkdir( cachedir )

    return creator_for[ serializing ](
      getfunc = downloader,
      keygen = lambda s: s.lower(),
      handler = cache.FileHandler.uncompressed(
        dirname = cachedir,
        prefix = "",
        postfix = postfix,
        ),
      )

  else:
    info.field( name = "Access type", value = "not cached" )
    return downloader


def get_composition(pdbid, accessor, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.heading( text = "Composition analysis", level = 1 )
  info.task_begin( text = "Getting sequence file" )
  data = accessor( pdbid )
  info.task_end()

  info.task_begin( text = "Parsing" )

  from iotbx import bioinformatics
  ( sequences, others ) = bioinformatics.fasta_sequence_parse( data )

  info.task_end()

  if others:
    info.field_sequence( name = "Uninterpretable content", value = others )

  info.field( name = "Number of sequences", value = len( sequences ) )

  from phaser import mmtype
  considereds = (
    ( mmtype.DNA, nucleic_acid_component ),
    ( mmtype.RNA, nucleic_acid_component ),
    ( mmtype.PROTEIN, protein_component ),
    )
  components = []
  info.ordered_list_begin()

  for seq in sequences:
    info.list_item_begin( text = "sequence" )
    info.sequence( sequence = seq, indent = 4 )

    typeiter = (
      comp( sequence = seq ) for ( mt, comp ) in considereds
      if mt.recognise_rescode_sequence( sequence = seq.sequence )
      )

    try:
      component = next(typeiter)

    except StopIteration:
      info.unformatted_text( text = "Unknown, discarding" )

    else:
      info.field( name = "Molecule type", value = str( component ) )
      components.append( component )

    info.list_item_end()

  info.list_end()

  composition = Composition( components = components )
  info.heading( text = "Composition:", level = 3 )
  info.preformatted_text( text = str( composition ) )

  return composition


def get_logger(stream = None, stream2 = None, close_stream = False):

  from phaser.logoutput_new import console

  if stream is None:
    import sys
    stream = sys.stdout

  handlers = [
    console.Handler.FromDefaults(
      stream = stream,
      width = 80,
      close_stream = close_stream,
      )
    ]

  if stream2 is not None:
    handlers.append(
      console.Handler.FromDefaults(
        stream = stream2,
        width = 80,
        close_stream = close_stream,
        )
      )

  from phaser import logoutput_new
  import tbx_utils
  return logoutput_new.Writer(
    handlers = handlers,
    level = tbx_utils.LOGGING_LEVEL_INFO,
    close_handlers = True,
    **tbx_utils.LOGGING_LEVELS
    )


def get_engine_creator(filename):

  from libtbx.scheduling import philgen

  engine = philgen.manager(
    constraint = philgen.constraint(
      pickleable_target = True,
      pickleable_return_value = True,
      ),
    multicore = philgen.multicore( backends = [ philgen.job_scheduler_backend() ] ),
    extras = [
      philgen.batch(
        backend = philgen.job_scheduler_backend(),
        conf_cluster = philgen.cluster(
          queue_philgen_for = {
            "file": philgen.cluster_file_queue(),
            "network": philgen.cluster_socket_queue(),
          },
          ),
        )
      ],
    )

  import libtbx.phil
  mphil = libtbx.phil.parse( str( engine ) )

  if filename is not None:
    with open( filename ) as ifile:
      philarg = libtbx.phil.parse( ifile.read() )
      cphil = mphil.fetch( philarg )

  else:
    cphil = mphil

  return engine( params = cphil.extract() )
