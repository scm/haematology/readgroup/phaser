from __future__ import print_function

"""
Cache Interface:
"""

class LimitedDict(object):
  """
  Minimal methods to work as a dictionary

  This is implemented by aggregation instead of subclassing to avoid unnecessary
  imports
  """

  def __init__(self, maxitems):

    if maxitems < 1:
      raise ValueError("Invalid size limit")

    from collections import OrderedDict
    self.store = OrderedDict()
    self.maxitems = maxitems


  def __contains__(self, key):

    return key in self.store


  def __getitem__(self, key):

    return self.store[ key ]


  def __setitem__(self, key, value):

    self.store[ key ] = value

    if self.maxitems < len( self.store ):
      self.store.popitem( last = False )


class MemoryCache(object):
  """
  Stores objects in memory, threadsafe (probably)
  """

  def __init__(self, getfunc, keygen, maxitems = None):

    self.getfunc = getfunc
    self.keygen = keygen

    if maxitems is not None:
      self.backstore =  LimitedDict( maxitems = maxitems )

    else:
      self.backstore = {}


  def __call__(self, key):

    stdkey = self.keygen( key )

    try:
      return self.backstore[ stdkey ]

    except KeyError:
      value = self.getfunc( stdkey )
      self.backstore[ stdkey ] = value
      return value


def simple_read(f):

  return f.read()


def simple_write(data, f):

  return f.write( data )


class PathGenerator(object):
  """
  Generates a path
  """

  def __init__(self, dirname, prefix, postfix):

    import os.path
    self.template = os.path.normpath(
      os.path.join( dirname, prefix + "%s" + postfix )
      )


  def __call__(self, identifier):

    return self.template % identifier


class FileHandler(object):
  """
  Writes and reads from a stream connected to an identifier through a path
  """

  def __init__(self, pathgen, opener):

    self.pathgen = pathgen
    self.opener = opener


  def __call__(self, identifier, mode = "r"):

    path = self.pathgen( identifier )
    return self.opener( path, mode )


  @classmethod
  def uncompressed(cls, dirname, prefix, postfix):

    return cls(
      pathgen = PathGenerator( dirname = dirname, prefix = prefix, postfix = postfix ),
      opener = open,
      )


  @classmethod
  def gzipped(cls, dirname, prefix, postfix):

    import gzip
    return cls(
      pathgen = PathGenerator( dirname = dirname, prefix = prefix, postfix = postfix + ".gz" ),
      opener = gzip.open,
      )


class FileCache(object):
  """
  Stores objects on the disk

  Note that other processes may use the same folder to store data
  """

  def __init__(self, getfunc, keygen, handler, serializer, deserializer, errors = ()):

    self.getfunc = getfunc
    self.keygen = keygen
    self.handler = handler
    self.serializer = serializer
    self.deserializer = deserializer
    self.errors = ( IOError, ) + errors


  def __call__(self, key):

    stdkey = self.keygen( key )

    try:
      with self.handler( stdkey ) as f:
        return self.deserializer( f )

    except self.errors:
      pass

    value = self.getfunc( stdkey )

    with self.handler( stdkey, "w" ) as f:
      self.serializer( value, f )

    return value


  @classmethod
  def String(cls, getfunc, keygen, handler):

    return cls(
      getfunc = getfunc,
      keygen = keygen,
      handler = handler,
      serializer = simple_write,
      deserializer = simple_read,
      )


  @classmethod
  def Pickle(cls, getfunc, keygen, handler):

    import pickle

    return cls(
      getfunc = getfunc,
      keygen = keygen,
      handler = handler,
      serializer = pickle.dump,
      deserializer = pickle.load,
      errors = ( pickle.UnpicklingError, ),
      )


  @classmethod
  def Json(cls, getfunc, keygen, handler, prettyprint = True):

    import json

    if prettyprint:
      import functools
      serializer = functools.partial( json.dump, indent = 4, separators = (',', ': ') )

    else:
      serializer = json.dump

    return cls(
      getfunc = getfunc,
      keygen = keygen,
      handler = handler,
      serializer = serializer,
      deserializer = json.load,
      errors = ( ValueError, ),
      )
