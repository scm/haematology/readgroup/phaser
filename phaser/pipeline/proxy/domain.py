from __future__ import print_function

class SequencePosition(object):
  """
  Residue identifier
  """

  def __init__(self, resseq, icode):

    self.resseq = resseq
    self.icode = icode if icode else " "


  def __hash__(self):

    return hash( self.tuple )


  def __eq__(self, other):

    return self.tuple == other.tuple


  def __ne__(self, other):

    return self.tuple != other.tuple


  @property
  def tuple(self):

    return ( self.resseq, self.icode )


  @property
  def resid(self):

    return "%4s%1s" % ( self.resseq, self.icode )


class Segment(object):
  """
  Segment selection
  """

  def __init__(self, start, end):

    self.start = start
    self.end = end


class Domain(object):
  """
  Collection of segments
  """

  def __init__(self, database, identifier, chain, segments):

    self.identifier = identifier
    self.database = database
    self.chain = chain
    self.segments = segments


class ResidueGroupIndexer(object):
  """
  Indexes residue_groups according to ( resseq, resid )
  """

  def __init__(self, chain):

    self.chain = chain.id
    self.rgs = chain.residue_groups()
    self.rg_index_denoted_by = dict(
      ( SequencePosition( resseq = rg.resseq_as_int(), icode = rg.icode ), i )
      for ( i, rg ) in enumerate( self.rgs )
      )


  def __call__(self, domain):

    if self.chain != domain.chain:
      raise KeyError("Chain identifier mismatch")

    for segment in domain.segments:
      ( start, end ) = domain.database.map(
        segment = segment,
        indexer = self.rg_index_denoted_by,
        )
      yield self.rgs[ start : end ]


class Database(object):
  """
  Database-specific methods
  """

  def __init__(self, name, key, mapper):

    self.name = name
    self.key = key
    self.map = mapper


def simple_mapping_protocol(segment, indexer):

  return ( indexer[ segment.start ], indexer[ segment.end ] + 1 )


def scop_mapping_protocol(segment, indexer):

  if segment.start in indexer:
    start = indexer[ segment.start ]

  else:
    start = None

  if segment.end in indexer:
    end = indexer[ segment.end ] + 1

  else:
    end = None

  return ( start, end )


CATH = Database( name = "CATH", key = "domain", mapper = simple_mapping_protocol )
SCOP = Database( name = "SCOP", key = "scop_id", mapper = scop_mapping_protocol )


def position_from_pdb_api_format(data):

  return SequencePosition(
    resseq = data[ "author_residue_number" ],
    icode = data[ "author_insertion_code" ],
    )


def segments_from_pdbe_api_format(mappings, key):

  segments_for = {}

  for data in mappings:
    segment = Segment(
      start = position_from_pdb_api_format( data[ "start" ] ),
      end = position_from_pdb_api_format( data[ "end" ] ),
      )
    segments_for.setdefault( data[ key ], [] ).append(
      ( data[ "chain_id" ], data[ "segment_id" ], segment )
      )

  return segments_for


def annotation_from_pdbe_api_format(data_for, database):

  for ( identifier, data ) in data_for[ database.name ].items():
    segments_for = segments_from_pdbe_api_format(
      mappings = data[ "mappings" ],
      key = database.key,
      )

    for segdata in segments_for.values():
      assert segdata
      chain = segdata[0][0]

      if any( chain != t[0] for t in segdata[1:] ):
        raise ValueError("Inconsistent domain data: %s" % [t[0] for t in segdata])

      segdata.sort( key = lambda t: t[1] )

      if any( i != t[1] for ( i, t ) in enumerate( segdata, start = 1 ) ):
        raise ValueError("Incomplete domain data %s" % [t[1] for t in segdata])

      segments = [ t[2] for t in segdata ]
      yield Domain(
        database = database,
        identifier = identifier,
        chain = chain,
        segments = segments,
        )
