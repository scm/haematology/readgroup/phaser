# Python 2 and 3: easiest option
from __future__ import print_function

from future.standard_library import install_aliases
install_aliases()

from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request, build_opener
from urllib.error import HTTPError, URLError

def simple(stream):

    return stream.read()

simple.ENCODING = "identity"
simple.ACCEPT = set( [ "", simple.ENCODING ] )


# HTTP functionality
AGENT = "HTTPInterface/0.0.2 +http://www.phenix-online.org/"
OPENER = build_opener()

class Download(object):

    def __init__(self, url, data = None, headers = {}, method = simple):

        request = Request(
            url = url,
            data = urlencode( data ) if data else None,
            headers = dict( headers.items()
                + [
                    ( "User-Agent", AGENT ),
                    ( "Accept-encoding", method.ENCODING ),
                    ]
                )
            )
        stream = OPENER.open( request )

        if stream.headers.get( "Content-Encoding", "" ).lower() not in method.ACCEPT:
            raise RuntimeError("Service does not support encoding %s" % method.ENCODING)

        self.data = method( stream = stream )


def is_online(url):

    try:
        stream = OPENER.open( url )

    except URLError:
        return False

    return True
