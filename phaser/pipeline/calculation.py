from __future__ import print_function

from phaser.pipeline import mr_object
import phaser
from functools import reduce

class Progress(object):
    """
    Class to indicate that a search is in progress
    """

    def __init__(self, calculation):

        self.calculation = calculation


# Results
class Result(object):
    """
    Base class for identification
    """

    def __init__(self, calculation, logfile, data):

        self.calculation = calculation
        self.logfile = logfile
        self.data = data


# Exit status
class Normal(object):
    """
    Normal termination
    """

    def __init__(self, data, logfile):

        self.data = data
        self.logfile = logfile


    def __call__(self):

        return ( self.data, self.logfile )


class Error(object):
    """
    Error exit
    """

    USER_ERRORS = set(
        [ "INPUT", "FILEOPEN", "MEMORY", "KILLFILE", "KILLTIME", "ENSEMBLE" ]
        )

    def __init__(self, function, name, message, logfile):

        self.function = function
        self.name = name
        self.message = message
        self.logfile = logfile


    def __call__(self):

        message = "%s error occurred in %s: %s" % ( self.name, self.function, self.message )

        if self.name in self.USER_ERRORS:
            from libtbx.utils import Sorry
            raise Sorry(message)

        else:
            raise RuntimeError("%s\nLogfile:\n%s" % (message, self.logfile))


# Calculations
class Calculation(object):
    """
    Base class for identification
    """

    def __init__(self, data, partial):

        self.data = data
        self.partial = partial


    def cleanup(self):

        self.data = None


    def __str__(self):

        return self.__class__.__name__


class RotationSearch(Calculation):
    """
    Rotation search
    """

    def __init__(self, data, partial, ensemble, cutoff):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.ensemble = ensemble
        self.cutoff = cutoff


    def run(self, cpus = 1):

        input = phaser.InputMR_FRF()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        ensembles = set( self.partial.ensembles() )
        ensembles.add( self.ensemble )

        for ense in ensembles:
            ense.apply_ensemble_definition( input = input )

        input.setSOLU( [ self.partial.mr_set() ] )
        input.addSEAR_ENSE_NUM( self.ensemble.identifier(), 1 )
        input.setXYZO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )
        input.setPEAK_ROTA_CUTO( self.cutoff )
        input.setPEAK_ROTA_DOWN( 0 )

        result = phaser.GIL_runMR_FRF( input )

        if result.Failed():
            return Error(
                function = "rotation function",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        raw = result.getDotRlist()
        assert len( raw ) == 1
        ms = raw[0]
        assert all( [ r.MODLID == self.ensemble.identifier() for r in ms.RLIST ] )
        rotations = [ ( r.EULER , r.RF, r.RFZ ) for r in ms.RLIST ]
        return Normal( data = rotations, logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        peaks = [
            mr_object.RotationPeak(
                ensemble = self.ensemble,
                rotation = mr_object.Rotation.Euler( angles = a ),
                rf = rf,
                rfz = rfz
                )
            for ( a, rf, rfz ) in definitions
            ]

        return Result( calculation = self, logfile = logfile, data = peaks )


class RestrictedRotationSearch(Calculation):
    """
    Rotation search restricted to a volume
    """

    def __init__(self, data, partial, ensemble, rotation, sweep):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.ensemble = ensemble
        self.rotation = rotation
        self.sweep = sweep


    def run(self, cpus = 1):

        input = phaser.InputMR_FRF()
        input.setJOBS( cpus )
        self.data.apply( input = input )
        ensembles = set( self.partial.ensembles() )
        ensembles.add( self.ensemble )

        for ense in set( ensembles ):
            ense.apply_ensemble_definition( input )

        input.setXYZO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )

        input.setTARG_ROTA( "BRUTE" )
        input.setROTA_VOLU( "AROUND" )
        input.setROTA_EULE( self.rotation.euler() )
        input.setROTA_RANG( self.sweep )

        input.setSOLU( [ self.partial.mr_set() ] )
        input.addSEAR_ENSE_NUM( self.ensemble.identifier(), 1 )
        input.setSOLU( [ self.partial.mr_set() ] )
        input.setTARG_ROTA( "BRUTE" )

        result = phaser.GIL_runMR_FRF( input )

        if result.Failed():
            return Error(
                function = "restricted rotation search",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        raw = result.getDotRlist()
        assert len( raw ) == 1
        ms = raw[0]
        assert all( [ r.MODLID == self.ensemble.identifier() for r in ms.RLIST ] )
        rotations = [ ( r.EULER , r.RF, r.RFZ ) for r in ms.RLIST ]
        return Normal( data = rotations, logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        peaks = [
            mr_object.RotationPeak(
                ensemble = self.ensemble,
                rotation = mr_object.Rotation.Euler( angles = a ),
                rf = rf,
                rfz = rfz
                )
            for ( a, rf, rfz ) in definitions
            ]

        return Result( calculation = self, logfile = logfile, data = peaks )


class TranslationSearch(Calculation):
    """
    Translation search
    """

    def __init__(self, data, partial, rpeak):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.rpeak = rpeak


    def run(self, cpus = 1):

        input = phaser.InputMR_FTF()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        ensembles = set( self.partial.ensembles() )
        ensembles.add( self.rpeak.ensemble )

        for ense in ensembles:
            ense.apply_ensemble_definition( input )

        input.setSOLU(
            [ self.partial.mr_set( rotations = [ self.rpeak ] ) ]
            )
        input.setXYZO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )
        input.setSGAL_SELE( "NONE" )
        input.setZSCO_USE( False )
        input.setSEAR_METH( "FULL" )

        result = phaser.GIL_runMR_FTF( input )

        if result.Failed():
            return Error(
                function = "translation search",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        raw = result.getDotSol()
        assert all(
            [ t.ANNOTATION.startswith( self.partial.identifier() ) for t in raw ]
            )
        assert all( [ 0 < len( t.KNOWN ) for t in raw ] )
        assert all(
            [ t.KNOWN[-1].MODLID == self.rpeak.ensemble.identifier()
                for t in raw ]
            )
        definitions = [
            ( t.KNOWN[-1].TRA, t.TF, t.TFZ, t.KNOWN[-1].BFAC ) for t in raw
            ]

        return Normal( data = definitions, logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        peaks = [
            mr_object.TranslationPeak(
                ensemble = self.rpeak.ensemble,
                rotation = self.rpeak.rotation,
                translation = t,
                tf = tf,
                tfz = tfz,
                bfactor = bfac
                )
            for ( t, tf, tfz, bfac ) in definitions
            ]

        return Result( calculation = self, logfile = logfile, data = peaks )


class RestrictedTranslationSearch(Calculation):
    """
    Rotation search restricted to a volume
    """

    def __init__(self, data, partial, ensemble, rotations, translation, extent):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.ensemble = ensemble
        self.rotations = rotations
        self.translation = translation
        self.extent = extent


    def run(self, cpus = 1):

        input = phaser.InputMR_FTF()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        ensembles = self.partial.ensembles()
        ensembles.append( self.ensemble )

        for ense in set( ensembles ):
            ense.apply_ensemble_definition( input )

        input.setXYZO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )

        input.setTARG_TRAN( "BRUTE" )
        input.setTRAN_VOLU( "AROUND" )
        input.setTRAN_POIN( self.translation )
        input.setTRAN_RANG( self.extent )
        input.setZSCO_USE( False )
        input.setSGAL_SELE( "NONE" )

        input.setSOLU( [ self.partial.mr_set( rotations = self.rpeaks() ) ] )

        result = phaser.GIL_runMR_FTF( input )

        if result.Failed():
            return Error(
                function = "restricted translation search",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        raw = result.getDotSol()

        assert all(
            [ t.ANNOTATION.startswith( self.partial.identifier() ) for t in raw ]
            )
        assert all( [ 0 < len( t.KNOWN ) for t in raw ] )
        assert all(
            [ t.KNOWN[-1].MODLID == self.ensemble.identifier() for t in raw ]
            )
        definitions = [
            ( t.KNOWN[-1].R, t.KNOWN[-1].TRA, t.TF, t.TFZ, t.KNOWN[-1].BFAC )
            for t in raw
            ]

        return Normal( data = definitions, logfile = result.logfile() )


    def rpeaks(self):

        return [
            mr_object.RotationPeak( ensemble = self.ensemble, rotation = r, rf = 0, rfz = 0 )
            for r in self.rotations
            ]


    def process(self, results):

        ( definitions, logfile ) = results()

        peaks = [
            mr_object.TranslationPeak(
                ensemble = self.ensemble,
                rotation = mr_object.Rotation.Matrix( elements = r ),
                translation = t,
                tf = tf,
                tfz = tfz,
                bfactor = bfac
                )
            for ( r, t, tf, tfz, bfac ) in definitions
            ]

        return Result( calculation = self, logfile = logfile, data = peaks )


class SinglePoolPackingCheck(object):
    """
    Packing check
    """

    def __init__(self, data, partials, tpeaks):

        assert len( partials ) == len( tpeaks )
        self.data = data
        self.partials = partials
        self.tpeaks = tpeaks


    def run(self, cpus = 1):

        import operator

        input = phaser.InputMR_PAK()
        input.setJOBS( cpus )

        self.data.apply_symmetry( input = input )
        ensembles = set(
            reduce( operator.add, [ p.ensembles() for p in self.partials ] )
            )
        ensembles.update( [ t.ensemble for t in self.tpeaks ] )

        for ense in ensembles:
            ense.apply_ensemble_definition( input )

        mr_sets = []

        for ( i, ( p, t ) ) in enumerate( zip( self.partials, self.tpeaks ) ):
            ms = p.mr_set( translations = [ t ] )
            ms.ANNOTATION = "%s %s" % ( self.peakid( index = i ), ms.ANNOTATION )
            mr_sets.append( ms )

        input.setSOLU( mr_sets )
        input.setPACK_SELE( "PERCENT" )
        input.setXYZO( False )
        input.setMUTE( True )

        result = phaser.GIL_runMR_PAK( input )

        if result.Failed():
            return Error(
                function = "single pool packing check",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        ms_for = dict(
            [ ( ms.ANNOTATION.split()[0], ms ) for ms in result.getDotSol() ]
            )

        definitions = []

        for ( i, ( p, t ) ) in enumerate( zip( self.partials, self.tpeaks ) ):
            peakid = self.peakid( index = i )

            if peakid not in ms_for:
                definitions.append( [] )
                continue

            ms = ms_for[ peakid ]

            assert 0 < len( ms.KNOWN )
            assert ms.ANNOTATION.split()[1].startswith( p.identifier() )
            assert ms.KNOWN[-1].MODLID == t.ensemble.identifier(), (
                "%s != %s" %  ( ms.KNOWN[-1].MODLID, t.ensemble.identifier() )
                )

            # Need to save both as packing function changes the rotation
            definitions.append(
                ( ms.KNOWN[-1].R, ms.KNOWN[-1].TRA, ms.PAK )
                )

        return Normal( data = definitions, logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        data = []

        for ( d, t ) in zip( definitions, self.tpeaks ):
            if not d:
                data.append( None )

            else:
                ( rot, tra, clashes ) = d
                data.append(
                    mr_object.Peak(
                        ensemble = t.ensemble,
                        rotation = mr_object.Rotation.Matrix( elements = rot ),
                        translation = tra,
                        bfactor = t.bfactor
                        )
                    )

        assert len( data ) == len( self.tpeaks )
        return Result( calculation = self, logfile = logfile, data = data )


    def peakid(self, index):

        return "try%d" % index


    def __str__(self):

        return self.__class__.__name__


class Refinement(Calculation):
    """
    Refinement
    """

    def __init__(self, data, partial, peak, significant, rfz, tfz, b_factor_refinement = True):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak
        self.significant = significant
        self.rfz = rfz
        self.tfz = tfz
        self.b_factor_refinement = b_factor_refinement


    def run(self, cpus = 1):

        input = phaser.InputMR_RNP()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        cell = self.data.uctbx_unit_cell()
        ensembles = self.partial.ensembles()
        disassembled_peak = self.peak.independent_component_list( cell = cell )
        ensembles.extend( [ p.ensemble for p in disassembled_peak ] )

        for ense in set( ensembles ):
            ense.apply_ensemble_definition( input )

        input.setSOLU( [ self.partial.mr_set( translations = disassembled_peak ) ] )
        input.setXYZO( False )
        input.setHKLO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )
        input.setPURG_RNP_NUMB(0)

        input.setMACM_PROT( "CUSTOM" )
        input.addMACM(
            ref_rota = True,
            ref_tran = True,
            ref_bfac = self.b_factor_refinement,
            ref_vrms = False,
            )

        result = phaser.GIL_runMR_RNP( input )

        if result.Failed():
            return Error(
                function = "refinement",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        sol = result.getDotSol()
        assert len( sol ) == 1
        ms = sol[0]
        assert ms.ANNOTATION.startswith( self.partial.identifier() )
        assert len( ms.KNOWN ) == len( ensembles )
        assert all( [ nd.MODLID == e.identifier()
                for( nd, e ) in zip( ms.KNOWN, ensembles ) ] )
        definitions = [ ( nd.R, nd.TRA, nd.BFAC ) for nd in ms.KNOWN ]

        return Normal( data = ( ms.LLG, definitions ), logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        ( llg, data ) = definitions
        cell = self.data.uctbx_unit_cell()

        ensembles = ( self.partial.ensembles()
            + [ p.ensemble for p in self.peak.independent_component_list( cell = cell ) ] )
        assert len( ensembles ) == len( data )
        structure = mr_object.Structure(
            peaks = [
                mr_object.Peak(
                    ensemble = e,
                    rotation = mr_object.Rotation.Matrix( elements = r ),
                    translation = t,
                    bfactor = b
                    )
                for ( e, ( r, t, b ) ) in zip( ensembles, data )
                ],
            significant = self.significant,
            rfz = self.rfz,
            tfz = self.tfz,
            predecessor = self.partial
            )
        return Result(
            calculation = self,
            logfile = logfile,
            data = ( llg, structure )
            )


class PeakRefinement(Calculation):
    """
    Refinement of peak only
    """

    def __init__(self, data, partial, peak, protocol = True):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak
        self.b_factor_refinement = protocol


    def run(self, cpus = 1):

        input = phaser.InputMR_RNP()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        cell = self.data.uctbx_unit_cell()
        ensembles = set( self.partial.ensembles() )
        ensembles.add( self.peak.ensemble )

        for ense in ensembles:
            ense.apply_ensemble_definition( input )

        input.setSOLU(
            [
                self.partial.mr_set(
                    translations = [ self.peak ],
                    sflags = ( True, True, True ),
                    ),
                ]
            )
        input.setXYZO( False )
        input.setHKLO( False )
        input.setMUTE( True )
        input.setPURG_RNP_NUMB(0)
        input.setMACA_PROT( "OFF" )

        input.setMACM_PROT( "CUSTOM" )
        input.addMACM(
            ref_rota = True,
            ref_tran = True,
            ref_bfac = self.b_factor_refinement,
            ref_vrms = False,
            )

        result = phaser.GIL_runMR_RNP( input )

        if result.Failed():
            return Error(
                function = "single peak refinement",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        sol = result.getDotSol()
        assert len( sol ) == 1
        ms = sol[0]
        assert ms.ANNOTATION.startswith( self.partial.identifier() )
        assert 0 < len( ms.KNOWN )
        assert ms.KNOWN[-1].MODLID == self.peak.ensemble.identifier()
        nd = ms.KNOWN[-1]

        definitions = ( nd.R, nd.TRA, nd.BFAC, ms.LLG )

        return Normal( data = definitions, logfile = result.logfile() )


    def process(self, results):

        ( definitions, logfile ) = results()

        ( r, t, b, llg ) = definitions
        peak = mr_object.Peak(
            ensemble = self.peak.ensemble,
            rotation = mr_object.Rotation.Matrix( elements = r ),
            translation = t,
            bfactor = b
            )

        return Result(
            calculation = self,
            logfile = logfile,
            data = ( llg, peak )
            )


class TFZCalculation(Calculation):
    """
    TFZ calculation
    """

    def __init__(self, data, partial, peak):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak


    def run(self, cpus = 1):

        input = phaser.InputMR_FTF()
        input.setJOBS( cpus )

        self.data.apply( input = input )
        ensembles = self.partial.ensembles()
        ensembles.append( self.peak.ensemble )

        for ense in set( ensembles ):
            ense.apply_ensemble_definition( input )

        input.setXYZO( False )
        input.setMUTE( True )
        input.setMACA_PROT( "OFF" )

        input.setTARG_TRAN( "BRUTE" )
        input.setTRAN_VOLU( "AROUND" )
        input.setTRAN_POIN( self.peak.translation )
        input.setTRAN_RANG( 1 )
        input.setTRAN_FRAC( True )
        input.setZSCO_USE( False )
        input.setSGAL_SELE( "NONE" )

        rpeak = mr_object.RotationPeak(
            ensemble = self.peak.ensemble,
            rotation = self.peak.rotation,
            rf = 0,
            rfz = 0
            )

        input.setSOLU( [ self.partial.mr_set( rotations = [ rpeak ] ) ] )

        result = phaser.GIL_runMR_FTF( input )

        if result.Failed():
            return Error(
                function = "tfz calculation",
                name = result.ErrorName(),
                message = result.ErrorMessage(),
                logfile = result.logfile(),
                )

        sol = result.getDotSol()

        # Not sure whether this can happen
        if len( sol ) == 0:
            data = ( 0.00, 0.00, self.peak.translation )

        else:
            assert 0 < len( sol[0].KNOWN )
            data = ( sol[0].TF, sol[0].TFZ, sol[0].KNOWN[-1].TRA )

        return Normal( data = data, logfile = result.logfile() )


    def process(self, results):

        ( ( tf, tfz, tra ), logfile ) = results()

        peak = mr_object.TranslationPeak(
            ensemble = self.peak.ensemble,
            rotation = self.peak.rotation,
            translation = tra,
            tf = tf,
            tfz = tfz,
            bfactor = self.peak.bfactor
            )

        return Result( calculation = self, logfile = logfile, data = peak )


class Evaluation(Calculation):
    """
    Peak evaluation
    """

    def __init__(self, data, partial, peak):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak


    def run(self, cpus = 1):

        # Packing check
        packing = SinglePoolPackingCheck(
            data = self.data,
            partials = [ self.partial ],
            tpeaks = [ self.peak ]
            )

        pak_raw = packing.run( cpus = cpus )

        try:
            pak_result = packing.process( results = pak_raw )

        except Exception:
            return pak_raw

        assert len( pak_result.data ) == 1

        if pak_result.data[0] is None:
            Normal( data = None, logfile = pak_result.logfile )

        # Discard packing rearrangement to remove instability caused by ensemble differences
        tfzcalc = TFZCalculation(
            data = self.data,
            partial = self.partial,
            peak = self.peak
            )
        tfz_raw = tfzcalc.run( cpus = cpus )

        return Normal( data = tfz_raw, logfile = pak_result.logfile )


    def process(self, results):

        ( tfz_raw, logfile ) = results()

        if tfz_raw is None:
            data = ( False, None )

        else:
            # Process here to ensure object bindings are established correctly
            tfzcalc = TFZCalculation(
                data = self.data,
                partial = self.partial,
                peak = self.peak
                )
            result = tfzcalc.process( results = tfz_raw )
            data = ( True, result.data )
            logfile += result.logfile

        return Result( calculation = self, logfile = logfile, data = data )


class RefinedEvaluation(Calculation):
    """
    Peak evaluation
    """

    def __init__(self, data, partial, peak, protocol = True):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak
        self.protocol = protocol


    def run(self, cpus = 1):

        # Packing check
        packing = SinglePoolPackingCheck(
            data = self.data,
            partials = [ self.partial ],
            tpeaks = [ self.peak ]
            )

        pak_raw = packing.run( cpus = cpus )

        try:
            pak_result = packing.process( results = pak_raw )

        except Exception:
            return pak_raw

        assert len( pak_result.data ) == 1

        if pak_result.data[0] is None:
            return Normal( data = None, logfile = pak_result.logfile )

        # Discard packing rearrangement to remove instability caused by ensemble differences
        refinement = PeakRefinement(
            data = self.data,
            partial = self.partial,
            peak = self.peak,
            protocol = self.protocol
            )
        refinement_raw = refinement.run()

        try:
            ref_result = refinement.process( results = refinement_raw )

        except Exception:
            return refinement_raw

        ( llg, peak ) = ref_result.data

        tfzcalc = TFZCalculation(
            data = self.data,
            partial = self.partial,
            peak = peak
            )
        tfz_raw = tfzcalc.run( cpus = cpus )

        try:
            tfz_result = tfzcalc.process( results = tfz_raw )

        except Exception:
            return tfz_raw

        tfz_peak = tfz_result.data # this may change translation to one with highest TF

        return Normal(
            data = (
                peak.rotation,
                peak.translation,
                peak.bfactor,
                tfz_peak.tf,
                tfz_peak.tfz,
                llg,
                ),
            logfile = pak_result.logfile + ref_result.logfile + tfz_result.logfile,
            )


    def process(self, results):

        ( definitions, logfile ) = results()

        if definitions is None:
            data = ( False, None, None )

        else:
            ( r, t, b, tf, tfz, llg ) = definitions
            peak = mr_object.TranslationPeak(
                ensemble = self.peak.ensemble,
                rotation = r,
                translation = t,
                tf = tf,
                tfz = tfz,
                bfactor = self.peak.bfactor
                )
            data = ( True, peak, llg )

        return Result( calculation = self, logfile = logfile, data = data )


class MultiOriginEvaluation(Calculation):
    """
    Peak evaluation
    """

    def __init__(self, data, partial, peak, origins):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak
        self.origins = origins


    def run(self, cpus = 1):

        peaks = self.peaks()

        packing = SinglePoolPackingCheck(
            data = self.data,
            partials = [ self.partial ] * len( peaks ),
            tpeaks = peaks
            )

        pak_raw = packing.run( cpus = cpus )

        try:
            pak_result = packing.process( results = pak_raw )

        except Exception:
            return pak_result

        assert len( pak_result.data ) == len( peaks )
        tdata = []

        for ( p, pak_p ) in zip( peaks, pak_result.data ):
            if pak_p is None:
                tdata.append( None )
                continue

            tfzcalc = TFZCalculation(
                data = self.data,
                partial = self.partial,
                peak = p
                )
            tfz_raw = tfzcalc.run( cpus = cpus )
            tdata.append( tfz_raw )

        return Normal( data = tdata, logfile = pak_result.logfile )


    def peaks(self):

        ( mx, my, mz ) = self.peak.translation

        return [
            mr_object.Peak(
                ensemble = self.peak.ensemble,
                rotation = self.peak.rotation,
                translation = ( mx + x, my + y, mz + z ),
                bfactor = self.peak.bfactor
                )
            for ( x, y, z ) in self.origins
            ]


    def process(self, results):

        ( tdata, logfile ) = results()

        peaks = self.peaks()
        assert len( peaks ) == len( tdata )
        tpeaks = []

        for ( p, td ) in zip( peaks, tdata ):
            if td is None:
                continue

            # Process here to ensure object bindings are established correctly
            tfzcalc = TFZCalculation(
                data = self.data,
                partial = self.partial,
                peak = p
                )
            result = tfzcalc.process( results = td )
            logfile += result.logfile
            tpeaks.append( result.data )

        if not tpeaks:
            data = ( False, None )

        else:
            data = ( True, max( tpeaks, key = lambda tp: tp.tf ) )

        return Result( calculation = self, logfile = logfile, data = data )


class RestrictedSearch(Calculation):
    """
    Rotation and translation searches restricted to a volume
    """

    def __init__(self, data, partial, ensemble, rotation, sweep, translation, extent):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.ensemble = ensemble
        self.rotation = rotation
        self.sweep = sweep
        self.translation = translation
        self.extent = extent


    def run(self, cpus = 1):

        restricted_rotation_search = RestrictedRotationSearch(
            data = self.data,
            partial = self.partial,
            ensemble = self.ensemble,
            rotation = self.rotation,
            sweep = self.sweep,
            )

        rot_tmp_results = restricted_rotation_search.run( cpus = cpus )

        try:
            rot_results = restricted_rotation_search.process( results = rot_tmp_results )

        except Exception:
            return rot_tmp_results

        if not rot_results.data:
            return Normal( data = [], logfile = rot_results.logfile )

        restricted_translation_search = RestrictedTranslationSearch(
            data = self.data,
            partial = self.partial,
            ensemble = self.ensemble,
            rotations = [ p.rotation for p in rot_results.data ],
            translation = self.translation,
            extent = self.extent,
            )

        tra_tmp_results = restricted_translation_search.run( cpus = cpus )

        try:
            tra_results = restricted_translation_search.process( results = tra_tmp_results )

        except Exception:
            return tra_tmp_results

        return Normal(
            data = [ ( p.rotation, p.translation, p.tf, p.tfz, p.bfactor )
                for p in tra_results.data ],
            logfile = rot_results.logfile + "\n" + tra_results.logfile,
            )


    def process(self, results):

        ( definitions, logfile ) = results()

        peaks = [
            mr_object.TranslationPeak(
                ensemble = self.ensemble,
                rotation = r,
                translation = t,
                tf = tf,
                tfz = tfz,
                bfactor = bfac
                )
            for ( r, t, tf, tfz, bfac ) in definitions
            ]

        return Result( calculation = self, logfile = logfile, data = peaks )


class RestrictedSearchEvaluation(Calculation):
    """
    Select best peak from restricted search
    """

    def __init__(self, data, partial, peak, sweep, extent):

        super( self.__class__, self ).__init__( data = data, partial = partial )
        self.peak = peak
        self.sweep = sweep
        self.extent = extent


    def run(self, cpus = 1):

        search = RestrictedSearch(
            data = self.data,
            partial = self.partial,
            ensemble = self.peak.ensemble,
            rotation = self.peak.rotation,
            sweep = self.sweep,
            translation = self.peak.translation,
            extent = self.extent,
            )

        search_tmp = search.run( cpus = cpus )

        try:
            search_results = search.process( results = search_tmp )

        except Exception:
            return search_tmp

        if not search_results.data:
            best = mr_object.TranslationPeak(
                ensemble = self.peak.ensemble,
                rotation = self.peak.rotation,
                translation = self.peak.translation,
                tf = 0,
                tfz = 0,
                bfactor = self.peak.bfac
                )

        else:
            best = max( search_results.data, key = lambda s: s.tf )

        # Packing check
        pak = SinglePoolPackingCheck(
            data = self.data,
            partials = [ self.partial ],
            tpeaks = [ best ]
            )

        pak_raw = pak.run( cpus = cpus )

        try:
            pak_results = pak.process( results = pak_raw )

        except Exception:
            return pak_raw

        assert len( pak_results.data ) == 1

        return Normal(
            data = (
                pak_results.data[0] is not None,
                ( best.rotation, best.translation, best.tf, best.tfz, best.bfactor ),
                ),
            logfile = search_results.logfile + "\n" + pak_results.logfile,
            )


    def process(self, results):

        ( definitions, logfile ) = results()

        ( is_pak, ( r, t, tf, tfz, bfac ) ) = definitions

        peak = mr_object.TranslationPeak(
            ensemble = self.peak.ensemble,
            rotation = r,
            translation = t,
            tf = tf,
            tfz = tfz,
            bfactor = bfac
            )

        return Result( calculation = self, logfile = logfile, data = ( is_pak, peak ) )
