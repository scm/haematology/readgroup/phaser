from __future__ import print_function

from phaser import sequtil


class DomainAnalysisException(Exception):
  """
  Some error raised in this module
  """


class ChainPositionElement(sequtil.Element):
  """
  Denotes a residue_group in a chain
  """

  def __str__(self):

    return self.sequence[ self.index ].resid()


class ChainPositionConsecutivity(object):
  """
  Consecutivity mapped onto a chain
  """

  def __init__(self, consecutivity):

    self.consecutivity = consecutivity


  def __call__(self, lhs, rhs):

    return self.consecutivity( lhs(), rhs() )


class Chain(object):
  """
  A list of residue groups that are aware of their indices
  """

  def __init__(self, chain, mmt):

    assert mmt.recognize_chain_type( chain = chain )
    self.chain = chain
    self.mmt = mmt

    rgs = chain.residue_groups()
    self.sequence = sequtil.Segment(
      begin = ChainPositionElement.Begin( sequence = rgs ),
      end = ChainPositionElement.End( sequence = rgs ),
      )


  def consecutivity(self):

    return ChainPositionConsecutivity(
      consecutivity = self.mmt.residue_structural_connectivity( tolerance = 0.2 )
      )


  def fully_annotated_secondary_structure_elements(self):

    for ann in self.mmt.secondary_structure_full_annotations( chain = self.chain ):
      assert len( ann.rgs ) == len( self.sequence )
      ann.rgs = self.sequence
      yield ann


  def indexer_base(self):

    from mmtbx.geometry import shared_types
    return shared_types.calculate_base_for_coordinates(
      xyzs = self.chain.atoms().extract_xyz(),
      )


class Data(object):
  """
  Store any information in an object
  """

  def __init__(self):

    self.data = {}


  def get(self, key):

    return self.data[ key ]


  def set(self, key, value):

    self.data[ key ] = value


  def delete(self, key):

    del self.data[ key ]


  def keys(self):

    return self.data.keys()


  def copy(self):

    copy = self.__class__()

    for key in self.keys():
      copy.set( key = key, value = self.get( key = key ) )

    return copy


class ResidueSet(object):
  """
  Set of residue groups
  """

  def __init__(self, rgs = [], data = Data()):

    self.data = data.copy()
    self.rgs = set( rgs )


  def residue_count(self):

    return len( self.rgs )


class ResidueSegments(object):
  """
  Residue segments
  """

  def __init__(self, segments = [], data = Data()):

    self.data = data.copy()
    self.segments = segments


  def residue_count(self):

    return sum( len( segment ) for segment in self.segments )


  def segment_count(self):

    return len( self.segments )


  def residue_groups(self):

    return sum( ( list( segment ) for segment in self.segments ), [] )


  def residue_set(self):

    return ResidueSet( data = self.data.copy(), rgs = self.residue_groups() )


  def __str__(self):

    return "\n".join( str( s ) for s in self.segments )


def set_relative_distance(lhs, rhs):
  """
  Calculates the size normalized distance between two sets
  """

  return len( lhs ^ rhs ) / ( len( lhs & rhs ) + 1.0 )


def set_absolute_distance(lhs, rhs):
  """
  Calculates the distance between two domains as the number of different residues
  """

  return len( lhs ^ rhs )


class absolute_equality(object):
  """
  Equality when number of non-common residues is negligible
  """

  def __init__(self, max_difference):

    self.max_difference = max_difference


  def __call__(self, lhs, rhs):

    return set_absolute_distance( lhs = lhs.rgs, rhs = rhs.rgs ) <= self.max_difference


class absolute_compatibility(object):
  """
  Compatibility when number of common residues is negligible
  """

  def __init__(self, max_overlap):

    self.max_overlap = max_overlap


  def __call__(self, lhs, rhs):

    return len( lhs.rgs & rhs.rgs ) <= self.max_overlap


def build_domain_compatibility_graph(domains, compatibility):

  from boost_adaptbx import graph
  g = graph.adjacency_list( graph_type = "undirected" )

  for dom1 in domains:
    v1 = g.add_vertex( label = dom1 )

    for v2 in g.vertices():
      if v1 == v2:
        continue

      dom2 = g.vertex_label( vertex = v2 )

      if compatibility( dom1, dom2 ):
        g.add_edge( vertex1 = v1, vertex2 = v2 )

  return g


class DomainCountAndCoverageScorer(object):
  """
  Scores with sequences coverage and domain count
  """

  def __init__(self, domain_reward = 10, coverage_reward = 1 ):

    self.domain_reward = domain_reward
    self.coverage_reward = coverage_reward


  @staticmethod
  def format(score):

    return str( score )


  def __call__(self, recombination, residue_group_count, domain_count):

    return ( self.coverage_reward * residue_group_count
      + self.domain_reward * domain_count )


class DomainRecombineVisitor(object):
  """
  A Bron-Kerbosch visitor to find the best recombination
  """

  def __init__(
    self,
    graph,
    scorer,
    logger,
    calculate_domain_names,
    calculate_residue_group_count,
    calculate_domain_count,
    ):

    self.graph = graph
    self.scorer = scorer

    from phaser.logoutput_new import facade
    self.info = facade.Package( channel = logger.info )

    self.calculate_domain_names = calculate_domain_names
    self.calculate_residue_group_count = calculate_residue_group_count
    self.calculate_domain_count = calculate_domain_count

    if self.graph.num_vertices() == 1:
      self.best_index = 1
      self.best_solution = self.clique_to_recombination( clique = self.graph.vertices() )
      self.best_score = self.scorer(
        recombination = self.best_solution,
        residue_group_count = self.calculate_residue_group_count( self.best_solution ),
        domain_count = self.calculate_domain_count(self.best_solution ),
        )

    else:
      self.index = 0
      self.best_index = None
      self.best_score = None
      self.best_solution = None

      from boost_adaptbx.graph import maximum_clique
      self.info.ordered_list_begin()

      maximum_clique.bron_kerbosch_all_cliques( graph = self.graph, callable = self )
      self.info.list_end()


  @property
  def formatted_best_score(self):

    return self.scorer.format( self.best_score )


  def __call__(self, clique):

    self.info.list_item_begin( text = "assembly" )
    recombination = self.clique_to_recombination( clique = clique )
    residue_group_count = self.calculate_residue_group_count( recombination )
    domain_count = self.calculate_domain_count( recombination )
    score = self.scorer(
      recombination = recombination,
      residue_group_count = residue_group_count,
      domain_count = domain_count,
      )
    self.info.heading( text = "Domains:", level = 6 )
    self.info.unordered_list_begin( separate_items = False )

    for name in self.calculate_domain_names( recombination ):
      self.info.list_item_begin( text = name )
      self.info.list_item_end()

    self.info.list_end()
    self.info.field( name = "Positions", value = residue_group_count )
    self.info.field( name = "Score", value = score, formatter = self.scorer.format )

    self.index += 1

    if self.best_score < score:
      self.best_index = self.index
      self.best_score = score
      self.best_solution = recombination

    self.info.list_item_end()


  def clique_to_recombination(self, clique):

    return [ self.graph.vertex_label( vertex = v ) for v in clique ]


def cluster_equivalent_domains(domains, clustering_distance, cluster_radius):

  if len( domains ) <= 1:
    return [ domains ]

  from libtbx import cluster
  cl = cluster.HierarchicalClustering(
      data = domains,
      distance_function = clustering_distance
      )
  return cl.getlevel( cluster_radius )


# Domain input-output PHIL
sequence_domain_phil = """
sequence_domain
  .help = "Domain definition"
  .multiple = True
{
  name = None
    .help = "Domain name"
    .optional = False
    .type = str

  segment
    .help = "Residue segment given in sequence positions"
    .multiple = True
    .optional = True
  {
    begin = None
      .help = "Segment start"
      .type = int
      .optional = False

    end = None
      .help = "Segment end"
      .type = int
      .optional = False
  }
}
"""

structure_domain_phil = """
structure_domain
  .help = "Domain definition"
  .multiple = True
{
  name = None
    .help = "Domain name"
    .optional = False
    .type = str

  segment
    .help = "Residue segment given with resids"
    .multiple = True
    .optional = True
  {
    begin = None
      .help = "Segment start"
      .type = str
      .optional = False

    end = None
      .help = "Segment end"
      .type = str
      .optional = False
  }
}
"""

class Domain2Phil(object):
  """
  Translates a domain segment to a PHIL
  """

  def __init__(
    self,
    master,
    caption,
    caption_name = "name",
    caption_segment = "segment",
    caption_begin = "begin",
    caption_end = "end",
    ):

    self.master = master
    self.caption = caption

    self.scope_single = self.find( scope = self.master, caption = caption )
    self.def_name = self.find( scope = self.scope_single, caption = caption_name )
    self.scope_segment = self.find( scope = self.scope_single, caption = caption_segment )
    self.def_begin = self.find( scope = self.scope_segment, caption = caption_begin )
    self.def_end = self.find( scope = self.scope_segment, caption = caption_end )


  @staticmethod
  def find(scope, caption):

    return next( o for o in scope.objects if o.name == caption )


  def _domain2phil(self, name, segments):

    objects = [
      self.def_name.customized_copy( words = [ str( name ) ] ),
      ]

    for seg in segments:
      copy = self.scope_segment.customized_copy(
        objects = [
          self.def_begin.customized_copy( words = [ str( seg[0] ) ] ),
          self.def_end.customized_copy( words = [ str( seg[-1] ) ] ),
          ],
        )
      objects.append( copy )

    return self.scope_single.customized_copy( objects = objects )


  def __call__(self, definitions):

    objects = []

    for ( name, segments ) in definitions:
      objects.append( self._domain2phil( name = name, segments = segments ) )

    return self.master.customized_copy( objects = objects )


  @classmethod
  def SequenceDomain(cls):

    import libtbx.phil
    return cls(
      master = libtbx.phil.parse( sequence_domain_phil ),
      caption = "sequence_domain",
      )


  @classmethod
  def StructureDomain(cls):

    import libtbx.phil
    return cls(
      master = libtbx.phil.parse( structure_domain_phil ),
      caption = "structure_domain",
      )
