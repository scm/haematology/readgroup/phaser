from __future__ import print_function

class Target(object):
    """
    Adapter to MRage calculation
    """

    def __init__(self, calculation, slotsize):

      self.calculation = calculation
      self.slotsize = slotsize


    def __call__(self):

      return self.calculation.run( cpus = self.slotsize )


class MRageJob(object):

    def __init__(self, owner, calculation, extra, result, info):

        self.owner = owner
        self.calculation = calculation
        self.extra = extra
        self.result = result
        self.info = info


    def __str__(self):

      return "(%s) %s" % ( self.info, self.calculation )


class Adapter(object):
    """
    Adapter to libtbx.scheduling.manager
    """

    def __init__(self, manager, info, qslots):

        self.manager = manager
        self.info = info
        self.qslots = qslots

        import collections
        self.data_for = {}
        self.completes = collections.OrderedDict()


    def submit(self, owner, calculation, extra):

        jobid = self.manager.submit(
          target = Target( calculation = calculation, slotsize = self.qslots )
          )
        self.data_for[ jobid ] = ( owner, calculation, extra )


    def empty(self):

        return self.manager.is_empty()


    def full(self):

        return self.manager.is_full()


    def wait(self):

        if not self.empty() and not self.completes:
          self.fetch( resiter = self.manager.results() )


    def join(self):

        self.manager.join()
        self.poll()


    def poll(self):

        resiter = self.manager.results()

        while self.manager.has_results():
          self.fetch( resiter = resiter )


    def completed(self):

        return self.completes.keys()


    def remove(self, job):

        del self.completes[ job ]


    # Internal
    def fetch(self, resiter):

        ( jobid, jresult ) = next(resiter)
        ( owner, calc, extra ) = self.data_for[ jobid ]
        del self.data_for[ jobid ]
        cresult = jresult()
        mjob = MRageJob(
            owner = owner,
            calculation = calc,
            extra = extra,
            result = cresult,
            info = self.info,
            )
        self.completes[ mjob ] = None
