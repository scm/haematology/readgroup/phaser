from __future__ import print_function

from __future__ import division
from __future__ import with_statement

import os.path
import itertools, sys

from phaser import tbx_utils
from phaser import phil_extension

import iotbx.phil

import libtbx.phil
from libtbx import runtime_utils
from libtbx.object_oriented_patterns import lazy_initialization

# Phil extensions
iotbx.phil.default_converter_registry = libtbx.phil.extended_converter_registry(
  additional_converters = [
    phil_extension.paths_converters,
    phil_extension.choices_converters,
    ],
  base_registry = iotbx.phil.default_converter_registry,
  )

# Constants
LOGGING_LEVELS = tbx_utils.LOGGING_LEVELS

PROGRAM = "ensembler"
VERSION = "0.2.0"
MIN_SITE_COUNT = 3
MIN_MODEL_COUNT = 2

class EnsemblerError(Exception):
  """
  Some error occurred during ensembling
  """

# Data types
class Chain(object):
  """
  PDB chain and extra information
  """

  def __init__(self, chain, annotation, mmt):

    self.chain = chain
    self.annotation = annotation
    self.mmt = mmt


  def residue_groups(self):

    return self.chain.residue_groups()


  def chainid(self):

    return self.chain.id


  def __str__(self):

    return str( self.annotation )


class AssemblyAnnotation(object):
  """
  Annotation for an assembly
  """

  maxlength = 20

  def __init__(self, filename, model):

    self.filename = filename
    self.model = model


  def __str__(self):

    filename = os.path.basename( self.filename )

    if self.maxlength < len( filename ):
      filename = filename[ : self.maxlength - 3 ] + "..."

    return "%s, model '%s'" % ( filename, self.model )


  @classmethod
  def from_pdb_object(cls, pdb):

    return cls( filename = pdb.name)


class Assembly(object):
  """
  Set of chains
  """

  def __init__(self, annotation, chains, associateds):

    self.annotation = annotation
    self.chains = chains
    self.associateds = associateds


  @property
  def types(self):

    return [ c.mmt for c in self.chains ]


  @property
  def nchains(self):

    return len( self.chains )


  def all_chains(self):

    return self.chains + self.associateds


  def transform(self, transformation):

    for chain in self.all_chains():
      atoms = chain.chain.atoms()
      atoms.set_xyz( transformation * atoms.extract_xyz() )


  def rename_chains(self, chainiditer):

    for chain in self.all_chains():
      chain.chain.id = next(chainiditer)


  def copy_with_detached_chains(self):

    chains = []

    for mchain in self.chains:
      ichain = mchain.chain.detached_copy()
      chains.append(
        Chain( chain = ichain, annotation = mchain.annotation,  mmt = mchain.mmt )
        )

    associateds = []

    for achain in self.associateds:
      ichain = achain.chain.detached_copy()
      associateds.append(
        Chain( chain = ichain, annotation = achain.annotation,  mmt = achain.mmt )
        )

    return self.__class__(
      annotation = self.annotation,
      chains = chains,
      associateds = associateds,
      )


  def __str__(self):

    return "%s chains %s" % (
      self.annotation,
      "+".join( c.annotation.identifier for c in self.chains ),
      )


class Result(object):
  """
  Result of ensembling calculation
  """

  def __init__(
    self,
    assembly,
    transformation,
    wrmsd,
    unwrmsd,
    overlap,
    clusterids,
    errors,
    ):

    self.assembly = assembly
    self.transformation = transformation
    self.wrmsd = wrmsd
    self.unwrmsd = unwrmsd
    self.overlap = overlap
    self.clusterids = clusterids
    self.errors = errors

# Chain types
class EnsemblerDefault(object):

  def __init__(self, mmtype_name, residue_mapping, atom_selection):

    self._mmtype_name = mmtype_name
    self._residue_mapping = residue_mapping
    self._atom_selection = atom_selection


  def mmtype(self):

    from phaser import mmtype
    return mmtype.MOLECULE_TYPE_CALLED[ self.mmtype_name() ]


  def mmtype_name(self):

    return self._mmtype_name


  def residue_mapping(self):

    return self._residue_mapping


  def atom_selection(self):

    return AtomSelector( atoms = self._atom_selection( mmt = self.mmtype() ) )


SUPERPOSABLE_MMTYPE_DEFAULTS = [
  EnsemblerDefault(
    mmtype_name = "protein",
    residue_mapping = "ssm",
    atom_selection = lambda mmt: [ mmt.representative_mainchain_atom ],
    ),
  EnsemblerDefault(
    mmtype_name = "dna",
    residue_mapping = "muscle",
    atom_selection = lambda mmt: [ mmt.representative_mainchain_atom ],
    ),
  EnsemblerDefault(
    mmtype_name = "rna",
    residue_mapping = "muscle",
    atom_selection = lambda mmt: [ mmt.representative_mainchain_atom ],
    ),
  ]

# Read in chains
def read_chains_from(filename, selection, mmtypes, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  root = read_structure_from(
    filename = filename,
    selection = selection,
    logger = logger,
    )

  info.task_begin( text = "Splitting chains on type" )
  new_chains = tbx_utils.split_all_chains_in( root = root )
  info.task_end()
  info.field( name = "New chains introduced", value = new_chains )

  chain_accessed = process_chains_from(
    root = root,
    mmtypes = mmtypes,
    logger = logger,
    )

  return chain_accessed


def read_structure_from(filename, selection, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  pdb = tbx_utils.PDBObject.from_file( file_name = filename )

  if selection is not None:
    info.field( name = "Selection", value = selection )
    info.task_begin( text = "Selecting atoms" )
    asc = pdb.object.atom_selection_cache().selection( selection )
    root = pdb.object.select( asc, True )
    info.task_end()

  else:
    root = pdb.object

  return root


def process_chains_from(root, mmtypes, logger):

  from phaser import mmtype
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  from collections import OrderedDict
  chain_accessed = OrderedDict()
  info.heading( text = "Chains:", level = 4 )
  info.unordered_list_begin( level = 1 )

  for model in root.models():
    info.list_item_begin( text = "model '%s'" % model.id )
    info.unordered_list_begin( level = 2, separate_items = False )
    chain_named = OrderedDict()

    for chain in model.chains():
      mmt = mmtype.determine( chain = chain, mmtypes = mmtypes )
      annotation = tbx_utils.ChainAnnotation.from_chain( chain = chain )
      info.list_item(
        text = "%s (%s residues): type %s" % (
          annotation,
          len( chain.residue_groups() ),
          mmt.name,
          )
        )

      chain_named.setdefault( chain.id, [] ).append(
        Chain( chain = chain, annotation = annotation, mmt = mmt )
        )

    info.list_end()

    assert chain_named
    chain_accessed[ model.id ] = chain_named

    info.list_item_end()

  info.list_end()

  return chain_accessed


# Assembly handling
class ChainTypeSelector(object):
  """
  Selects first chain with appropriate type
  """

  def __init__(self, mmtypes):

    self.mmtypes = frozenset( mmtypes )


  def __call__(self, chains):

    citer = ( c for c in chains if c.mmt in self.mmtypes )

    try:
      return next(citer)

    except StopIteration:
      raise EnsemblerError("No chain with allowed type found")


def create_single_chain_assemblies(filename, chain_accessed, selector, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  assert len( chain_accessed ) == 1
  modelid = next(iter( chain_accessed ))
  annotation = AssemblyAnnotation( filename = filename, model = modelid )
  chain_named = chain_accessed[ modelid ]
  info.ordered_list_begin()
  assemblies = []
  moribunds = []

  for ( cid, chains ) in chain_named.items():
    info.list_item_begin( text = "assembly: model '%s', %s" % ( modelid, cid ) )

    try:
      selected = selector( chains = chains )

    except EnsemblerError as e:
      info.unformatted_text( text = "%s, SKIPPING" % e )
      moribunds.extend( chains )

    else:
      info.field( name = "Active chain", value = selected )
      associateds = [ c for c in chains if c is not selected ]

      if associateds:
        info.heading( text = "Associated chains", level = 6 )
        info.unordered_list_begin( separate_items = False )

        for chain in associateds:
          info.list_item( text = str( chain ) )

        info.list_end()

      assemblies.append(
        Assembly(
          annotation = annotation,
          chains = [ selected ],
          associateds = associateds,
          )
        )

    info.list_item_end()

  info.list_end()

  if len( assemblies ) == 1 and moribunds:
    info.unformatted_text( text = "Single assembly read from file: associate with extra chains" )
    assemblies[0].associateds.extend( moribunds )

  return assemblies


def create_assemblies_according_to_model(filename, chain_accessed, selector, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.unordered_list_begin()
  assemblies = []

  for ( modelid, chain_named ) in chain_accessed.items():
    annotation = AssemblyAnnotation( filename = filename, model = modelid )
    info.list_item_begin( text = "Model '%s'" % modelid )
    info.unordered_list_begin( separate_items = False )
    founds = []
    associateds = []
    moribunds = []

    for ( cid, chains ) in chain_named.items():
      info.list_item_begin( text = "Trial chainID: %s" % cid )

      try:
        selected = selector( chains = chains )

      except EnsemblerError as e:
        info.unformatted_text( text = "%s, SKIPPING" % e )
        moribunds.extend( chains )

      else:
        info.field( name = "Selected", value = selected )
        associateds = [ c for c in chains if c is not selected ]

        if associateds:
          info.heading( text = "Associated chains", level = 6 )
          info.unordered_list_begin( separate_items = False )

          for chain in associateds:
            info.list_item( text = str( chain ) )

          info.list_end()

        founds.append( selected )
        associateds.extend( associateds )

      info.list_item_end()

    info.list_end()

    assembly = Assembly(
      annotation = annotation,
      chains = founds,
      associateds = associateds + moribunds,
      )

    assemblies.append( assembly )
    info.list_item_end()

  info.list_end()

  return assemblies


class DefinedAssemblyCreator(object):

  def __init__(self, assemblies):

    assert assemblies
    nchains = len( assemblies[0] )
    assert all( len( a ) == nchains for a in assemblies )
    self.assemblies = assemblies

    if len( self.assemblies ) == 1:
      self.process = self.single_assembly_mode

    else:
      self.process = self.multi_assembly_mode


  def single_assembly_mode(self, filename, modelid, chains_named, selector, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )

    founds = self.multi_assembly_mode(
      filename = filename,
      modelid = modelid,
      chains_named = chains_named,
      selector = selector,
      logger = logger,
      )

    assert len( founds ) == 1
    assembly = founds[0]
    knowns = set( assembly.all_chains() )
    extras = sum(
      [ [ c for c in chains if c not in knowns ] for chains in chains_named.values() ],
      [],
      )

    if extras:
      info.unformatted_text( text = "Single assembly read from model: associate with extra chains" )
      assembly.associateds.extend( extras )

    return founds


  def multi_assembly_mode(self, filename, modelid, chains_named, selector, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    info.ordered_list_begin( separate_items = False )
    annotation = AssemblyAnnotation( filename = filename, model = modelid )
    founds = []

    for assem in self.assemblies:
      info.list_item_begin( text = "assembly: chains %s" % "+".join( assem ) )
      chains = []
      associateds = []

      for n in assem:
        if n not in chains_named:
          info.highlight( text = "No chain %s found, required for assembly" % n )
          raise EnsemblerError("Model '%s': no chain '%s' found" % (modelid, n))

        else:
          try:
            selected = selector( chains = chains_named[n] )

          except EnsemblerError as e:
            info.highlight( text = e )
            raise EnsemblerError("Model '%s': no chain '%s' with superposable type found" % (modelid, n))

          chains.append( selected )
          info.field( name = "Selected for %s" % n, value = selected )
          associateds.extend( c for c in chains_named[n] if c is not selected )

      if associateds:
        info.heading( text = "Associated chains", level = 6 )
        info.unordered_list_begin( separate_items = False )

        for chain in associateds:
          info.list_item( text = str( chain ) )

        info.list_end()

      info.list_item_end()
      founds.append(
        Assembly( annotation = annotation, chains = chains, associateds = associateds )
        )

    info.list_end()

    return founds


  def __call__(self, filename, chain_accessed, selector, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    info.unordered_list_begin()
    assemblies = []

    for ( modelid, chains_named ) in chain_accessed.items():
      info.list_item_begin( text = "Model '%s'" % modelid )
      founds = self.process(
        filename = filename,
        modelid = modelid,
        chains_named = chains_named,
        selector = selector,
        logger = logger,
        )
      assemblies.extend( founds )
      info.list_item_end()

    info.list_end()

    return assemblies


# Residue mapping methods
def resid_residue_mapping_method(chains, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.field( name = "Method", value = "resid" )

  # Do the mapping
  equiv_with = {}
  count = len( chains )
  info.ordered_list_begin()

  for ( index, chain ) in enumerate( chains ):
    info.list_item_begin( text = str( chain ) )
    info.task_begin( text = "Mapping" )

    for rg in chain.residue_groups():
      array = equiv_with.setdefault( rg.resid(), [ None ] * count )
      array[ index ] = rg

    info.task_end()
    info.list_item_end()

  info.list_end()

  # Convert it to array mapping
  mapping = [ equiv_with[ resid ] for resid in sorted( equiv_with ) ]

  return mapping


def ssm_residue_mapping_method(chains, logger):

  assert chains

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.field( name = "Method", value = "ssm" )

  from phaser import mmtype
  reference = chains[0]
  assert reference.mmt == mmtype.PROTEIN
  info.field( name = "Reference", value = str( reference ) )
  mappings = [ reference.chain.residue_groups() ]
  n_rgs = len( mappings[0] )

  indexer = lambda chain: dict(
    [ ( ( chain.id, rg.resseq_as_int(), rg.icode ), i )
      for ( i, rg ) in enumerate( chain.residue_groups() ) ]
    )

  import ccp4io_adaptbx
  info.unordered_list_begin( level = 1 )

  for current in chains[1:]:
    info.list_item_begin( text = str( current ) )
    info.task_begin( text = "Mapping" )
    rgs = current.chain.residue_groups()

    try:
      ssm = ccp4io_adaptbx.SecondaryStructureMatching(
        reference = reference.chain,
        moving = current.chain
        )
      alignment = ccp4io_adaptbx.SSMAlignment( match = ssm, indexer = indexer )

    except RuntimeError as e:
      info.task_failure( text = "SSM error: %s" % e )
      raise EnsemblerError("SSM error: %s" % e)

    m = [ None ] * n_rgs

    for ( cur, ref ) in alignment.pairs:
      if ref is None or cur is None:
          continue

      assert ref < n_rgs
      assert cur < len( rgs )

      m[ ref ] = rgs[ cur ]

    info.task_end()
    mappings.append( m )
    info.list_item_end()

  info.list_end()

  # Reformatting mapping
  assert all( len( t ) == len( mappings[0] ) for t in mappings )

  return zip( *mappings )


def ssm_multiple_alignment_residue_mapping_method(chains, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.field( name = "Method", value = "ssm multiple alignment" )
  count = len( chains )

  from phaser import mmtype
  import ccp4io_adaptbx
  managers = []
  indexers = []

  for chain in chains:
    assert chain.mmt == mmtype.PROTEIN

    try:
      managers.append( ccp4io_adaptbx.to_mmdb( root = chain.chain ) )

    except RuntimeError as e:
      raise EnsemblerError(e)

    indexers.append(
      dict( ( ( rg.resseq_as_int(), rg.icode.strip() ), rg )
        for ( i, rg ) in enumerate( chain.residue_groups() ) )
      )

  assert count == len( managers )
  assert count == len( indexers )
  info.task_begin( text = "Mapping" )

  try:
    ssmaln = ccp4io_adaptbx.SSMMultipleAlignment(
      managers = managers,
      selstrings = [ "*" ] * len( managers ),
      )

  except RuntimeError as e:
    info.task_failure( text = "SSM error: %s" % e )
    raise EnsemblerError("SSM error: %s" % e)

  mapping = []

  for equi in ssmaln.alignment:
    assert len( equi ) == count
    rgs = []

    for i in range( count ):
      rd = equi[ i ]

      if rd.resname:
        try:
          rgs.append( indexers[i][ ( rd.resseq, rd.inscode ) ] )

        except KeyError as e:
          info.task_failure(
            text = "Mapping error for chain %s: %s" % ( i + 1, e )
            )
          raise EnsemblerError("Error while mapping chain %s: %s" % (i + 1, e))

      else:
        rgs.append( None )

    mapping.append( rgs )

  info.task_end()

  return mapping


class alignment_based_residue_mapping_method(object):

  def __init__(self, alignment):

    self.alignment = alignment


  def __call__(self, chains, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    verbose = facade.Package( channel = logger.verbose )
    info.heading( text = "Alignment", level = 4 )
    info.alignment( alignment = self.alignment )

    info.heading( text = "Chain mapping", level = 4 )

    from phaser import chainalign
    base_params = chainalign.ChainAlignmentAlgorithm.parsed_master_phil().extract()
    ofilter_params = chainalign.OverlapPrefilter.parsed_master_phil().extract()

    trials = [
      chainalign.Trial(
        gapped = self.alignment.alignments[ i ],
        gap = self.alignment.gap,
        annotation = self.alignment.names[ i ],
        )
      for i in range( self.alignment.multiplicity() )
      ]
    mapping = []

    assert chains
    mtype = chains[0].mmt
    assert all( c.mmt == mtype for c in chains )
    info.ordered_list_begin()

    for chain in chains:
      info.list_item_begin( text = "chain: %s" % chain )
      info.task_begin( text = "Matching with alignment" )
      aligner = chainalign.OverlapPrefilter.from_params(
        aligner = chainalign.ChainAlignmentAlgorithm.from_params(
          rgs = chain.residue_groups(),
          mmt = chain.mmt,
          annotation = "Chain_%s" % chain.chain.id,
          params = base_params,
          ),
        params = ofilter_params,
        )
      accepteds = []

      for trial in trials:
        try:
          result = aligner( trial = trial )

        except chainalign.ChainAlignFailure as e:
          continue

        else:
          accepteds.append( result )

      if not accepteds:
        info.task_failure( text = str( e ) )
        raise EnsemblerError("No matching alignment found")

      info.task_end()
      best = max( accepteds, key = lambda a: a.identities )

      info.field( name = "Best matching", value = best.sequence.annotation )
      info.field( name = "Identities", value = best.identities )
      info.field( name = "Overlaps", value = best.overlaps )
      verbose.heading( text = "Alignment:", level = 5 )
      verbose.alignment( alignment = best.iotbx_alignment() )
      mapping.append( best.residue_alignment() )

      unaligneds = best.unaligned_residues()

      if unaligneds:
        info.unformatted_text( "%s residues not aligned and discarded" % len( unaligneds ) )
        info.ordered_list_begin( separate_items = False )

        for rg in unaligneds:
          info.list_item( text = "'%s'" % rg.resid() )

        info.list_end()

      info.list_item_end()

    info.list_end()

    # Reformatting mapping
    assert all( [ len( t ) == len( mapping[0] ) for t in mapping[1:] ] )
    return zip( *mapping )


class multiple_alignment_residue_mapping_method(object):

  def __init__(self, alignment):

    self.aligner = alignment_based_residue_mapping_method( alignment = alignment )


  def __call__(self, chains, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )

    info.field( name = "Method", value = "multiple alignment" )

    return self.aligner( chains = chains, logger = logger )


def muscle_residue_mapping_method(chains, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.field( name = "Method", value = "muscle" )

  assert chains
  mtype = chains[0].mmt
  assert all( c.mmt == mtype for c in chains )
  sequences = []

  from iotbx import bioinformatics
  info.heading( text = "Chain sequences", level = 4 )
  info.ordered_list_begin()

  for ( i, chain ) in enumerate( chains, start = 1 ):
    info.list_item_begin( text = str( chain ) )
    seq = bioinformatics.sequence(
      name = "Chain_%d" % i,
      sequence = mtype.one_letter_sequence_for_rgs( rgs = chain.residue_groups() ),
      )
    info.sequence( sequence = seq )
    sequences.append( str( seq ) )
    info.list_item_end()

  info.list_end()

  from mmtbx.msa import get_muscle_alignment
  info.task_begin( text = "Running muscle" )
  alignment = get_muscle_alignment( fasta_sequences = "\n".join( sequences ) )[0]
  info.task_end()

  aligner = alignment_based_residue_mapping_method( alignment = alignment )
  return aligner( chains = chains, logger = logger )


class MappingMethod(object):
  """
  Stores parameters for residue mapping
  """

  def __init__(self, method, types):

    self.method = method
    self.types = types


  def is_supported_type(self, mmtype):

    return mmtype in self.types


  def process(self, chains, logger):

    return self.method( chains = chains, logger= logger )


  @classmethod
  def Resid(cls, alignment, logger):

    if alignment is not None:
      cls.warn_superfluous_alignment( filename = alignment, logger = logger )

    from phaser import mmtype
    return cls(
      method = resid_residue_mapping_method,
      types = [ mmtype.PROTEIN, mmtype.RNA, mmtype.DNA ],
      )


  @classmethod
  def SSM_Pairwise(cls, alignment, logger):

    if alignment is not None:
      cls.warn_superfluous_alignment( filename = alignment, logger = logger )

    from phaser import mmtype
    return cls(
      method = ssm_residue_mapping_method,
      types = [ mmtype.PROTEIN ],
      )


  @classmethod
  def SSM_Multiple(cls, alignment, logger):

    if alignment is not None:
      cls.warn_superfluous_alignment( filename = alignment, logger = logger )

    from phaser import mmtype
    return cls(
      method = ssm_multiple_alignment_residue_mapping_method,
      types = [ mmtype.PROTEIN ],
      )


  @classmethod
  def Muscle(cls, alignment, logger):

    if alignment is not None:
      cls.warn_superfluous_alignment( filename = alignment, logger = logger )

    from phaser import mmtype
    return cls(
      method = muscle_residue_mapping_method,
      types = [ mmtype.PROTEIN, mmtype.RNA, mmtype.DNA ],
      )


  @classmethod
  def MultipleAlignment(cls, alignment, logger):

    if not alignment:
      raise EnsemblerError("No alignment specified")

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    info.field( name = "Alignment file", value = alignment )
    aliobj = tbx_utils.AlignmentObject.from_file( file_name = alignment )

    from phaser import mmtype
    return cls(
      method = multiple_alignment_residue_mapping_method( alignment = aliobj.object ),
      types = [ mmtype.PROTEIN, mmtype.RNA, mmtype.DNA ],
      )


  @staticmethod
  def warn_superfluous_alignment(filename, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    info.highlight(
      text = "Alignment %s specified, but will not be used by mapping method" % filename,
      )


MAPPING_CREATOR_FOR = {
  "resid": MappingMethod.Resid,
  "ssm": MappingMethod.SSM_Pairwise,
  "ssm_multiple_alignment": MappingMethod.SSM_Multiple,
  "muscle": MappingMethod.Muscle,
  "multiple_alignment": MappingMethod.MultipleAlignment,
  }


# Atom selection
class AllAtomSelector(object):
  """
  Selects all atoms

  Note this is a singleton object
  """

  @classmethod
  def accepted_atom_names(cls):

    return [ "*" ]


  @classmethod
  def is_accepted(cls, atom):

    return True


class AtomSelector(object):
  """
  Selects atoms
  """

  def __init__(self, atoms):

    self.atoms = set( atoms )


  def accepted_atom_names(self):

    return [ a.name for a in self.atoms ]


  def is_accepted(self, atom):

    from phaser import identity
    return identity.Atom.from_iotbx_atom( atom = atom ) in self.atoms


def parse_atom_selector(names, mmtype):

  uppers = [ n.upper() for n in names.split( "," ) ]

  if "ALL" in uppers:
    return AllAtomSelector

  from phaser import identity
  atoms = set()

  for name in uppers:
    if name == "BACKBONE":
      atoms.update( mmtype.mainchain_atoms )

    else:
      atom = identity.Atom( name = name, element = name[0] )
      atoms.add( atom )

  return AtomSelector( atoms = atoms )


def atom_mapping(residue_map, assemblies, chain_index, selector, logger):

  assert residue_map
  nchains = len( assemblies )
  assert all( [ len( t ) == nchains for t in residue_map ] )

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  atom_mapping = []
  atom_names = []

  for equivalents in residue_map:
    atoms_named = {}

    for ( index, rg ) in enumerate( equivalents ):
      if not rg:
        continue

      assembly = assemblies[ index ]
      chain = assembly.chains[ chain_index ]

      for a in [ a for a in rg.atoms() if selector.is_accepted( atom = a ) ]:
        array = atoms_named.setdefault( a.name, [ None ] * nchains )

        if array[ index ] is not None:
          text = "%s chain %s: resi '%s': multiple atoms %s" % (
            assembly.annotation,
            chain.annotation.identifier,
            rg.resid(),
            a.name,
            )
          info.highlight( text =text )

        else:
          array[ index ] = a

    # Append to mapping
    atom_mapping.extend( [ atoms_named[ n ] for n in sorted( atoms_named ) ] )
    atom_names.extend( "'%s'" % n for n in sorted( atoms_named ) )

  return ( atom_names, atom_mapping )


# Site selection and superposition setup
def intersect_site_selection(row):

  return None not in row

intersect_site_selection.name = "intersect"
intersect_site_selection.description = "Accept positions that contain atoms for every chain"


def overlap_site_selection(row):

  return MIN_MODEL_COUNT <= sum( 1 for a in row if a is not None )

overlap_site_selection.name = "overlap"
overlap_site_selection.description = "Accept positions that contain at least %s atoms" % MIN_MODEL_COUNT


def create_diamond_superposition(sites, weights):

  from scitbx.array_family import flex
  from phaser import multiple_superposition

  if len( sites ) < MIN_SITE_COUNT:
    raise EnsemblerError("Insufficient overlap among structures")

  return multiple_superposition.DiamondAlgorithm(
    site_sets = [ flex.vec3_double( [ a.xyz for a in m ] ) for m in zip( *sites ) ],
    weights = weights,
    )

create_diamond_superposition.name = "Diamond method"


def create_wang_snoeyink_superposition(sites, weights):

  from scitbx.array_family import flex
  from phaser import multiple_superposition

  if len( sites ) < MIN_SITE_COUNT:
    raise EnsemblerError("Insufficient overlap among structures")

  return multiple_superposition.WangSnoeyinkAlgorithm(
    site_sets = [
      flex.vec3_double( [ a.xyz if a else ( 0, 0, 0 ) for a in m ] )
      for m in zip( *sites )
      ],
    selections = [ flex.bool( [ a is not None for a in m ] ) for m in zip( *sites ) ],
    weights = weights,
    )

create_wang_snoeyink_superposition.name = "Wang-Snoeyink method"

class Superposition(object):
  """
  Superposition methods
  """

  def __init__(
    self,
    site_selection_method,
    setup_superposition_method,
    superposition_convergence,
    weighting,
    weight_convergence,
    incremental_damping_factor,
    max_damping_factor,
    ):

    self.site_selection_method = site_selection_method
    self.setup_superposition_method = setup_superposition_method
    self.superposition_convergence = superposition_convergence
    self.weighting = weighting
    self.weight_convergence = weight_convergence
    self.incremental_damping_factor = incremental_damping_factor
    self.max_damping_factor = max_damping_factor


  @classmethod
  def Diamond(cls, weighting, params):

    return cls.FromParams(
      site_selection_method = intersect_site_selection,
      setup_superposition_method = create_diamond_superposition,
      weighting = weighting,
      params = params,
      )


  @classmethod
  def WangSnoeyink(cls, weighting, params):

    return cls.FromParams(
      site_selection_method = overlap_site_selection,
      setup_superposition_method = create_wang_snoeyink_superposition,
      weighting = weighting,
      params = params,
      )


  @classmethod
  def FromParams(cls, site_selection_method, setup_superposition_method, weighting, params):

    return cls(
      site_selection_method = site_selection_method,
      setup_superposition_method = setup_superposition_method,
      superposition_convergence = params.superposition.convergence,
      weighting = weighting,
      weight_convergence = params.weighting.convergence,
      incremental_damping_factor = params.weighting.incremental_damping_factor,
      max_damping_factor = params.weighting.max_damping_factor,
      )


SUPERPOSITION_CREATOR_FOR = {
  "gapless": Superposition.Diamond,
  "gapped": Superposition.WangSnoeyink,
  }


def setup_unit_weighting_scheme(params):

  from phaser import multiple_superposition
  return multiple_superposition.UnitWeightScheme()


def setup_robust_resistant_weighting_scheme(params):

  from phaser import multiple_superposition
  return multiple_superposition.RobustResistantWeightScheme(
    critical_value_square = params.robust_resistant.critical ** 2
    )


WEIGHTING_SCHEME_SETUP_FOR = {
  "unit": setup_unit_weighting_scheme,
  "robust_resistant": setup_robust_resistant_weighting_scheme,
  }


# Superposition
def iterative_weighted_superposition(
  superposition,
  superposition_convergence,
  weighting,
  weights,
  weight_convergence,
  incremental_damping_factor,
  max_damping_factor,
  logger
  ):

  import math
  from scitbx.array_family import flex
  from phaser import multiple_superposition
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.unformatted_text( text = "Iterating weighting with superposition" )
  info.ordered_list_begin()

  while True:
    info.list_item_begin( text = "cycle" )
    info.task_begin( text = "Superposing" )

    try:
      ( rounds, residual ) = multiple_superposition.iterate(
        superposition = superposition,
        convergence = superposition_convergence
        )

    except multiple_superposition.NoConvergence as e:
      info.task_end( text = "stopped" )
      info.highlight( text = "Superposition: %s" % e.args[0] )
      info.unformatted_text( text = "Continue with results from last cycle" )
      ( rounds, residual ) = e.args[1:3]

    else:
      info.task_end()

    info.field( name = "Iterations", value = rounds )
    info.field( name = "Residual", value = residual )

    info.task_begin( text = "Weight adjustment" )
    diff_sq = superposition.distance_squares_from_average_structure()
    total_damping_factor = 1.0

    while True:
      new_weights = weighting.for_difference_squares( squares = diff_sq )

      try:
        superposition.prepare_new_weights( weights = new_weights )

      except multiple_superposition.BadWeights:
        info.task_failure( text = "aborted" )
        info.highlight( text = "Excessive weight shift, damping weight change" )

        if max_damping_factor < total_damping_factor:
          raise EnsemblerError("Weight damping recovery exhausted")

        total_damping_factor *= incremental_damping_factor
        diff_sq = diff_sq / incremental_damping_factor
        continue

      else:
        info.task_end()

      break

    displacement = math.sqrt( flex.mean_sq( new_weights - weights ) )
    info.field_float( name = "Weight change", value = displacement, digits = 5 )
    info.list_item_end()

    if displacement <= weight_convergence:
      break

    weights = new_weights
    superposition.set_new_weights()

  info.list_end()
  info.unformatted_text( text = "Iteration converged" )
  info.field_float( name = "Weighted rmsd", value = superposition.rmsd(), digits = 3 )


# Cluster analysis
def structure_similarity_analysis(superposition, clustering_distances, logger):

  from phaser.logoutput_new import facade
  from phaser.logoutput_new import formatter
  info = facade.Package( channel = logger.info )
  number = formatter.Number.normal( decimal = 3 )

  count = superposition.set_count
  distances = [ [ 0.0 ] * count for i in range( count ) ]

  for l in range( count ):
    for r in range( l + 1, count ):
      distances[ l ][ r ] = superposition.rmsd_between( left = l, right = r )
      distances[ r ][ l ] = distances[ l ][ r ]

  info.heading( text = "Pairwise rms values", level = 4 )

  info.table_rows(
    rows = [ [ i ] + row for ( i, row ) in enumerate( distances, start = 1 ) ],
    formatters = [ str ] + [ number ] * count,
    headings = [ "" ] + [ str( i ) for i in range( 1, count + 1 ) ],
    widths = [ 4 ] + [ 8 ] * count,
    style = "simple",
    )

  if not clustering_distances:
    return [ [] for i in range( count ) ]

  info.heading( text = "Hierarchical clustering", level = 4 )
  from libtbx import cluster
  clusterer = cluster.HierarchicalClustering(
    data = range( count ),
    distance_function = lambda x, y: distances[ x ][ y ],
    )
  results = [ [ None ] * len( clustering_distances ) for i in range( count ) ]

  info.ordered_list_begin()

  for ( s_index, distance ) in enumerate( clustering_distances ):
    info.list_item_begin( text = "clustering" )
    info.field( name = "Clustering distance", value = distance )
    levels = clusterer.getlevel( distance )
    info.field( name = "Number of clusters", value = len( levels ) )
    info.unordered_list_begin()

    for ( index, clust ) in enumerate( levels, start = 1 ):
      info.list_item_begin(
        text = "cluster %s: %s" % (
          index,
          ", ".join ( [ "%d" % ( s + 1 ) for s in sorted( clust ) ] ),
          )
        )

      for i in clust:
        assert results[ i ][ s_index ] is None
        results[ i ][ s_index ] = index

      info.list_item_end()

    info.list_end()
    info.list_item_end()

  info.list_end()

  return results


# Main method
def process(
  assemblies,
  ngroups,
  mappings,
  selections,
  superposition,
  clustering_distances,
  logger,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  info.field( name = "Number of assemblies", value = len( assemblies) )
  info.ordered_list_begin( separate_items = False )

  for assem in assemblies:
    info.list_item( text = str( assem ) )

  info.list_end()

  if len( assemblies ) < MIN_MODEL_COUNT:
    raise EnsemblerError("Less than %s chains for superposition" % MIN_MODEL_COUNT)

  assert all( a.nchains == ngroups for a in assemblies )
  assert ngroups == len( mappings )
  assert ngroups == len( selections )

  info.heading( text = "Residue and atom mapping", level = 2 )
  nchains = len( assemblies )
  sites = []
  sources = []
  overlaps = [ 0 ] * len( assemblies )

  for index in range( ngroups ):
    info.heading( text = "Equivalent chain group %s" % ( index + 1 ), level = 3 )

    method = mappings[index]
    chains = [ a.chains[ index ] for a in assemblies ]

    info.heading( text = "Consisting of:", level = 6 )
    info.ordered_list_begin( separate_items = False )

    for ( assem, chain ) in zip( assemblies, chains ):
      info.list_item( text = "%s, %s" % ( assem.annotation, chain.annotation ) )

    info.list_end()
    info.heading( text = "Residue mapping:", level = 5 )

    with logger.child( shift = 0 ) as childlogger:
      rmap = list(method.process( chains = chains, logger = childlogger ))

    verbose.heading( text = "Residue alignment:", level = 6 )
    verbose.table_rows(
      rows = [
        [ i ] + [
          "%s '%s'" % ( rg.atom_groups()[0].resname, rg.resid() ) if rg else "na"
          for rg in equi
          ]
        for ( i, equi ) in enumerate( rmap, start = 1 )
        ],
      widths = [ 6 ] + [ 12 ] * nchains,
      headings = [ "Posi" ] + [ "Chain %s" % i for i in range( 1, nchains + 1 ) ],
      rowheadings = 1,
      )

    info.heading( text = "Atom and site mapping:", level = 5 )
    info.field_sequence(
      name = "Accepted atom names",
      value = selections[ index ].accepted_atom_names(),
      )

    with logger.child( shift = 0 ) as childlogger:
      ( anames, amap ) = atom_mapping(
        residue_map = rmap,
        assemblies = assemblies,
        chain_index = index,
        selector = selections[ index ],
        logger = childlogger,
        )

    info.field( name = "Mapped atoms", value = len( amap ) )
    siteselmeth = superposition.site_selection_method
    info.field( name = "Site selection", value = siteselmeth.name )
    info.unformatted_text( text = siteselmeth.description )
    sitesel = [ siteselmeth( row = arow ) for arow in amap ]
    smap = [ arow for ( arow, acc ) in zip( amap, sitesel ) if acc ]
    start = len( sites )
    sites.extend( smap )
    sources.append( ( start, len( sites ) ) )
    info.field( name = "Selected sites", value = len( smap ) )

    if not amap or not anames or not sitesel:
      raise EnsemblerError("No sites selected")

    assert len( amap ) == len( anames ) == len( sitesel )

    for equi in smap:
      assert len( equi ) == len( assemblies )

      for ( i, posi ) in enumerate( equi ):
        if posi is not None:
          overlaps[ i ] += 1

    counter = itertools.count( 1 )
    verbose.table_rows(
      rows = [
        [ i, n, next(counter) if acc else "" ]
        + [ "'%s'" % a.parent().parent().resid() if a else "na" for a in equi ]
        for ( i, ( equi, n, acc ) ) in enumerate( list(zip( amap, anames, sitesel )), start = 1 )
        ],
      widths = [ 6, 6, 6 ] + [ 10 ] * nchains,
      headings = [ "Posi", "Atom", "Site" ] + [ "Resid #%s" % i for i in range( 1, nchains + 1 ) ],
      rowheadings = 3,
      )

  info.heading( text = "Superposition", level = 2 )
  from scitbx.array_family import flex
  weights = flex.double( [ 1 ] * len( sites ) )
  info.field( name = "Method", value = superposition.setup_superposition_method.name )
  algorithm = superposition.setup_superposition_method(
    sites = sites,
    weights = weights,
    )

  # Superpose structures
  info.field( name = "Weighting scheme", value = superposition.weighting )
  iterative_weighted_superposition(
    superposition = algorithm,
    superposition_convergence = superposition.superposition_convergence,
    weighting = superposition.weighting,
    weights = weights,
    weight_convergence = superposition.weight_convergence,
    incremental_damping_factor = superposition.incremental_damping_factor,
    max_damping_factor = superposition.max_damping_factor,
    logger = logger,
    )

  # Site errors graph
  errors = [ range( len( sites ) ) ]
  errors.extend( [ [ None ] * len( sites ) for i in range( ngroups ) ] )
  assert len( errors ) == len( sources ) + 1

  squares = algorithm.distance_squares_from_average_structure()
  site_errors_for = {}
  assert len( squares ) == len( sites )
  import math

  for ( array, ( start, end ) ) in zip( errors[ 1 : ], sources ):
    for index in range( start, end ):
      error = math.sqrt( squares[ index ] )
      array[ index ] = error

      for site in sites[ index ]:
        if site is None:
          continue

        site_errors_for.setdefault( site.parent().parent(), [] ).append( error )

  from phaser.logoutput_new import data
  from phaser.logoutput_new import formatter
  number = formatter.Number.normal( decimal = 2 )

  info.loggraph(
    chart = data.LoggraphChart.Graphs(
      title = "Position errors for superposed sites",
      graphs = [
        data.LoggraphGraph(
          graphname = "Position errors for all chain groups",
          graphtype = data.LoggraphType.Auto(),
          columnlist = range( 1, ngroups + 2 ),
          )
        ]
        + [
        data.LoggraphGraph(
          graphname = "Position errors for chain group %s" % ( n - 1 ),
          graphtype = data.LoggraphType.Auto(),
          columnlist = [ 1, n ],
          )
        for n in range( 2, ngroups + 2 )
        ],
      table = data.LoggraphTable(
        data = data.TableColumns(
          datasets = errors,
          formatters = [ str ] + [ number ] * ngroups,
          ),
        names = [ "Posi" ] + [ "Chain_group_%s" % i for i in range( 1, ngroups + 1 ) ],
        heading = "Position errors for superposed positions",
        ),
      ),
    )

  info.heading( text = "Structure similarity analysis", level = 2 )
  clusters = structure_similarity_analysis(
    superposition = algorithm,
    clustering_distances = clustering_distances,
    logger = logger,
    )

  info.heading( text = "", level = 2 )

  # Package up results
  info.heading( text = "Results", level = 2 )
  info.field_float(
    name = "Rmsd between all chains",
    value = algorithm.rmsd(),
    digits = 3,
    )

  info.heading( text = "Transformations", level = 3 )
  info.unformatted_text( text = "Transformations will be reset to superpose on the first assembly" )
  info.ordered_list_begin()
  ( rotations, translations ) = algorithm.transformations()
  wrmsds = algorithm.weighted_rmsds()
  unwrmsds = algorithm.unweighted_rmsds()
  assert len( assemblies ) == len( translations )
  assert len( assemblies ) == len( rotations )
  assert len( assemblies ) == len( wrmsds )
  assert len( assemblies ) == len( unwrmsds )
  assert len( assemblies ) == len( clusters )
  assert len( assemblies ) == len( overlaps )

  import scitbx.matrix
  assert assemblies
  refframe = scitbx.matrix.rt( ( rotations[0], translations[0] ) ).inverse()
  results = []

  for i in range( len( assemblies ) ):
    assembly = assemblies[i]

    errors = []

    for chain in assembly.chains:
      currents = []

      for rg in chain.residue_groups():
        if rg not in site_errors_for:
          currents.append( None )

        else:
          relevants = site_errors_for[ rg ]
          currents.append( sum( relevants ) / len( relevants ) )

      errors.append( currents )

    res = Result(
      assembly = assembly,
      transformation = refframe * scitbx.matrix.rt( ( rotations[i], translations[i] ) ),
      wrmsd = wrmsds[i],
      unwrmsd = unwrmsds[i],
      overlap = overlaps[i],
      clusterids = clusters[i],
      errors = errors,
      )
    info.list_item_begin( text = str( res.assembly ) )
    info.field( name = "Positions", value = res.overlap )
    info.field_float( name = "Weighted rmsd", value = res.wrmsd, digits = 3 )
    info.field_float( name = "Unweighted rmsd", value = res.unwrmsd, digits = 3 )

    for ( clindex, clid ) in enumerate( res.clusterids, start = 1 ):
      info.field( name = "Cluster %s" % clindex, value = clid )

    info.rt_matrix( rt = res.transformation )

    assert len( res.assembly.chains ) == len( res.errors )

    for ( chain, errs ) in zip( res.assembly.chains, res.errors ):
      rgs = chain.residue_groups()
      assert len( rgs ) == len( errs )
      verbose.heading( text = "Errors for %s:" % chain, level = 6 )
      verbose.table_columns(
        columns = [
          [ "%s '%s'" % ( rg.atom_groups()[0].resname, rg.resid() ) for rg in rgs ],
          errs,
          ],
        formatters = [ str, number ],
        headings = [ "Residue", "Error" ],
        widths = [ 12, 6 ],
        missing = "na",
        )

    info.list_item_end()
    results.append( res )

  info.list_end()

  return results


#
# Output
#

# Preparation
def copy_with_detached_chains(assembly):

  chains = []

  for mchain in assembly.chains:
    ichain = mchain.chain.detached_copy()
    chains.append(
      Chain( chain = ichain, annotation = mchain.annotation,  mmt = mchain.mmt )
      )

  associateds = []

  for uchain in assembly.unuseds:
    ichain = uchain.chain.detached_copy()
    ichain.id = citer.next()
    unuseds.append(
      Chain( chain = ichain, annotation = uchain.annotation,  mmt = uchain.mmt )
      )

  return Assembly( chains = chains, annotation = assembly.annotation, unuseds = unuseds )


def transform_chains_in(assembly, transformation):

  for chain in assembly.chains:
    atoms = chain.chain.atoms()
    atoms.set_xyz( transformation * atoms.extract_xyz() )


# Chain trimming
def trim_chains_in(assembly, errorlists, threshold, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )

  assert len( assembly.chains ) == len( errorlists )
  info.unordered_list_begin( level = 1 )

  for ( chain, errors ) in zip( assembly.chains, errorlists ):
    info.list_item_begin( text = str( chain ) )
    verbose.ordered_list_begin()
    rgs = chain.chain.residue_groups()
    assert len( rgs ) == len( errors )
    count = 0

    for ( rg, error ) in zip( rgs, errors ):
      if error is None or threshold < error:
        chain.chain.remove_residue_group( rg )
        report = "not aligned" if error is None else "error = %.2f" % error
        verbose.list_item( text = "resi '%s': %s EXCLUDE" % ( rg.resid(), report ) )
        count += 1

    verbose.list_end()
    info.field( name = "Trimmed residues", value = count )
    info.list_item_end()

  info.list_end()


class SortKey(object):
  """
  Sorting key for indices
  """

  def __init__(self, method, results):

    self.method = method
    self.results = results


  def __call__(self, index):

    return self.method( self.results, index )


  @classmethod
  def Input(cls, results):

    return cls( method = lambda array, index: index, results = results )


  @classmethod
  def WeightedRms(cls, results):

    return cls(
      method = lambda array, index: array[ index ].wrmsd,
      results = results,
      )


  @classmethod
  def UnweightedRms(cls, results):

    return cls(
      method = lambda array, index: array[ index ].unwrmsd,
      results = results,
      )


  @classmethod
  def Overlap(cls, results):

    return cls(
      method = lambda array, index: array[ index ].overlap
      )


  @classmethod
  def Cluster(cls, results):

    return cls(
      method = lambda array, index: array[ index ].clusterids,
      results = results,
      )


SORTING_ON = {
    "input": SortKey.Input,
    "wrmsd": SortKey.WeightedRms,
    "unwrmsd": SortKey.UnweightedRms,
    "overlap": SortKey.Overlap,
    "cluster": SortKey.Cluster,
    }


def write_multiple_singlemodel_pdb_files(prefix, assemblies, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.unformatted_text( text = "Writing separate output files" )
  info.ordered_list_begin()
  import iotbx.pdb

  filenames = []

  for ( index, assembly ) in enumerate( assemblies, start = 1 ):
    filename = "%s_%s.pdb" % ( prefix, index )
    info.list_item_begin( text = "%s -> %s" % ( assembly, filename ) )
    info.unordered_list_begin( level = 1, separate_items = False )
    model = iotbx.pdb.hierarchy.model()

    for annchain in assembly.all_chains():
      chain = annchain.chain
      model.append_chain( chain )
      info.list_item( text = "%s -> chain '%s'" % ( annchain, chain.id ) )

    info.list_end()
    with open(filename, "w") as f:
      f.write( tbx_utils.get_phaser_revision_remark() + "\n")
    root = iotbx.pdb.hierarchy.root()
    root.append_model( model )
    root.write_pdb_file( filename, open_append=True )
    filenames.append( filename )
    info.list_item_end()

  info.list_end()

  return filenames


def write_single_multimodel_pdb_file(prefix, assemblies, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.unformatted_text( text = "Writing one multimodel output file" )
  filename = "%s_merged.pdb" % prefix
  info.field( name = "Output file name", value = filename )
  info.ordered_list_begin()

  import iotbx.pdb
  root = iotbx.pdb.hierarchy.root()

  for ( index, assembly ) in enumerate( assemblies, start = 1 ):
    model = iotbx.pdb.hierarchy.model()
    model.id = str( index )
    info.list_item_begin( text = "%s -> model '%s'" % ( assembly, model.id ) )
    info.unordered_list_begin( level = 1, separate_items = False )

    for annchain in assembly.all_chains():
      chain = annchain.chain
      model.append_chain( chain )
      info.list_item( text = "%s -> chain '%s'" % ( annchain, chain.id ) )

    info.list_end()

    root.append_model( model )
    info.list_item_end()

  info.list_end()
  with open(filename, "w") as f:
    f.write( tbx_utils.get_phaser_revision_remark() + "\n")
  root.write_pdb_file( filename, open_append=True )

  return [ filename ]


def write_error_files(prefix, results, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.ordered_list_begin()
  import csv

  filenames = []

  for ( index, res ) in enumerate( results, start = 1 ):
    info.list_item_begin( text = "%s" % res.assembly )
    info.ordered_list_begin( marker = "a", separate_items = False )
    assert len( res.assembly.chains ) == len( res.errors )

    for ( chain, errs ) in zip( res.assembly.chains, res.errors ):
      filename = "%s_assembly_%s_chain_%s.err" % ( prefix, index, chain.chainid() )
      info.list_item( text = "%s -> %s" % ( chain, filename ) )

      with open( filename, "wb" ) as ofile:
        writer = csv.writer( ofile )
        writer.writerow( [ "resid", "resname", "error" ] )
        rgs = chain.residue_groups()
        assert len( rgs ) == len( errs )

        for ( rg, err )in zip( rgs, errs ):
          writer.writerow( [ rg.resid(), rg.atom_groups()[0].resname, err ] )

      filenames.append( filename )

    info.list_end()
    info.list_item_end()

  info.list_end()

  return filenames


WRITE_OUTPUT_AS = {
  "merged": write_single_multimodel_pdb_file,
  "separate": write_multiple_singlemodel_pdb_files,
  }

input_phil = """
model
  .help = "Input model file"
  .multiple = True
{
  file_name = None
    .help = "Input file name"
    .type = path

  selection = None
    .help = "Selection string"
    .optional = False
    .type = str

  assembly = None
    .help = "Assembly information"
    .type = strings
    .multiple = True

  read_single_assembly_from_file = False
    .help = "Consider contents of the file as a single assembly"
    .type = bool
}
"""

output_phil = """
location = ""
  .type = path
  .short_caption = Output directory
  .style = hidden

gui_output_dir = None
  .type = path
  .short_caption = Output directory
  .help = Sets base output directory for Phenix GUI - not used when \
    run from the command line.
  .style = output_dir bold

root = ensemble
  .help = "Output file root"
  .type = str
  .optional = False
  .short_caption = Output file root

style = %(style)s
  .help = "Output options for ensemble"
  .type = choice
  .optional = False
  .short_caption = Output ensemble as

sort = %(sort)s
  .help = "Sort ensemble components"
  .type = choice
  .optional = False
  .short_caption = Sort models by

error_file_root = None
  .help = "Output file root for superposition error files"
  .type = str

retain_associated_chains = False
  .help = "Keep (and transform) chains that are not assembly members"
  .type = bool
""" % {
  "sort": tbx_utils.choice_string(
    possible = SORTING_ON,
    default = "input"
    ),
  "style": tbx_utils.choice_string(
    possible = WRITE_OUTPUT_AS,
    default = "merged"
    )
  }

mapping_and_superposition_phil = """
mapping = None
  .help = "Residue mapping methods. Valid choices: %(mapping_help)s"
  .type = %(mapping_definition)s

alignment = None
  .help = "Alignment file (only necessary for 'alignments' modes)"
  .type = paths

atoms = None
  .help = "Atom to include in superposition"
  .type = strings
  .short_caption = Atoms to include in superposition

superposition
  .help = "Superposition setup"
{
  method = %(superposition)s
    .help = "Superposition algorithm"
    .type = choice
    .optional = False
    .short_caption = Superposition algorithm

  convergence = 1.0E-4
    .help = "Convergence criterion for superposition"
    .type = float
    .optional = False
    .short_caption = Superposition RMS covergence threshold
}

weighting
  .help = "Parameters for weighting scheme"
  .style = box
{
  scheme = %(weighting)s
    .help = "Weighting scheme"
    .type = choice
    .optional = False
    .short_caption = Weighting scheme

  convergence = 1.0E-3
    .help = "Convergence criterion for weight iteration"
    .type = float
    .optional = False
    .short_caption = Convergence criterion for weight iteration

  incremental_damping_factor = 1.5
    .help = "Damping factor in recovery cycle"
    .type = float
    .optional = False

  max_damping_factor = 3.34
    .help = "Quit recovery if cumulative damping factor is above"
    .type = float
    .optional = False

  robust_resistant
    .help = "Setting for robust-resistance scheme"
  {
    critical = 3
      .help = "tolerance"
      .type = float( value_min = 0.0 )
      .optional = False
      .short_caption = Robust resistant rms weighting term
  }
}

clustering = None
  .help = "Cutoff distance for cluster analysis"
  .type = floats( value_min = 0 )
  .short_caption = Cutoff distance for cluster analysis
""" % {
  "mapping_definition": phil_extension.definition_choices( options = MAPPING_CREATOR_FOR ),
  "mapping_help": phil_extension.help_choices( options = MAPPING_CREATOR_FOR ),
  "superposition": tbx_utils.choice_string(
    possible = SUPERPOSITION_CREATOR_FOR,
    default = "gapless"
    ),
  "weighting": tbx_utils.choice_string(
    possible = WEIGHTING_SCHEME_SETUP_FOR,
    default = "robust_resistant"
    ),
  }

trimming_phil = """
trim = False
  .help = "Trim differing loops"
  .type = bool
  .optional = False
  .short_caption = Trim residues deviating more than threshold

threshold = 3.0
  .help = "Max position RMSD for inclusion"
  .type = float( value_min = 0.0 )
  .optional = False
  .short_caption = Trimming threshold
"""


master_phil = """
input
  .help = "Input files"
{
  %(input)s
}

output
  .help = "Output file(s)"
{
  %(output)s

  include scope libtbx.phil.interface.tracking_params
}

configuration
  .help = "General parameters for ensemble generation"
  .short_caption = Ensemble generation settings
  .style = menu_item
{
  %(mapping_and_superposition)s

  trimming
    .help = "Trimming parameters"
  {
    %(trimming)s
  }
}""" % {
  "input": input_phil,
  "output": output_phil,
  "mapping_and_superposition": mapping_and_superposition_phil,
  "trimming": trimming_phil,
  }


parsed_master_phil = lazy_initialization(
  func = iotbx.phil.parse,
  input_string = master_phil,
  process_includes = True,
  )


def validate_params(params):

  from libtbx.utils import Sorry

  if not params.input.model:
    raise Sorry("No input pdb files")

  return True


def read_in_models(models, superposables, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  import os.path
  from phaser import mmtype
  selector = ChainTypeSelector( mmtypes = superposables )
  assemblies = []

  for scope in models:
    info.heading( text = os.path.basename( scope.file_name ), level = 3 )
    info.field( name = "Full path", value = scope.file_name )
    chain_accessed = read_chains_from(
      filename = scope.file_name,
      selection = scope.selection,
      mmtypes = mmtype.KNOWN_MOLECULE_TYPES,
      logger = logger,
      )

    if not chain_accessed:
      info.highlight( text = "No chains read from file, SKIPPING" )
      continue

    founds = assign_chains_to_assemblies(
      filename = scope.file_name,
      chain_accessed = chain_accessed,
      from_chains = scope.assembly,
      is_single_assembly = scope.read_single_assembly_from_file,
      selector = selector,
      logger = logger,
      )

    assemblies.extend( founds )

  return assemblies


def assign_chains_to_assemblies(
  filename,
  chain_accessed,
  from_chains,
  is_single_assembly,
  selector,
  logger,
  ):

  from libtbx.utils import Sorry
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.heading( text = "Assemblies:", level = 4 )

  if from_chains:
    info.unformatted_text( text = "Assign predefined chains to assemblies" )
    creator = DefinedAssemblyCreator( assemblies = from_chains )

  elif is_single_assembly:
    info.unformatted_text( text = "Assign contents as a single assembly" )

    if len( chain_accessed ) != 1:
      raise Sorry("Cannot treat multiple MODELs as a single assembly")

    creator = create_assemblies_according_to_model

  elif 1 < len( chain_accessed ):
    info.unformatted_text( text = "Assign chains to assemblies according to model" )
    creator = create_assemblies_according_to_model

  else:
    info.unformatted_text( text = "Assign each chain as a separate assembly" )
    creator = create_single_chain_assemblies

  try:
    founds = creator(
      filename = filename,
      chain_accessed = chain_accessed,
      selector = selector,
      logger = logger,
      )

  except EnsemblerError as e:
    raise Sorry(e)

  return founds


def check_assembly_consistency(assemblies, logger):

  from libtbx.utils import Sorry
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.field( name = "Number of assemblies", value = len( assemblies ) )

  if not assemblies:
    raise Sorry("No chains read from input PDB files")

  first = assemblies[0]
  nchains = len( first.chains )
  mtypes = first.types

  info.field( name = "Number of chains in assembly 1", value = nchains )
  info.field_sequence(
    name = "Chain types in assembly 1",
    value = [ mt.name for mt in mtypes ],
    )

  for ( index, other ) in enumerate( assemblies[1:], start = 2 ):
    if nchains != len( other.chains ):
      raise Sorry("Inconsistency between assembly 1 and assembly %s: different number of chains" % index)

    if mtypes != other.types:
      raise Sorry("Inconsistency between assembly 1 and assembly %s: different chain types" % index)

  return ( nchains, mtypes )


def determine_residue_mappings(
  assemblies,
  nchains,
  mmtypes,
  params,
  alignments,
  default_for,
  mapping_creator_for,
  logger,
  ):

  from libtbx.utils import Sorry
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  if sys.version_info[0] > 2:
    ziplongest = itertools.zip_longest
  else:
    ziplongest = itertools.izip_longest

  info.ordered_list_begin()
  mappings = []

  for ( mtype, method, alignment ) in ziplongest(
    mmtypes,
    params[ : nchains ],
    alignments[ : nchains ],
    ):
    info.list_item_begin( text = "Chain type: %s" % mtype.name )

    if method is None:
      info.unformatted_text( text = "Mapping not specified, using default" )
      assert mtype in default_for
      method = default_for[ mtype ]
      info.field( name = "Mapping", value = method )
      mapping = mapping_creator_for[ method ]( alignment = alignment, logger = logger )
      assert mapping.is_supported_type( mmtype = mtype )

    else:
      info.field( name = "Requested mapping", value = method )
      assert method in mapping_creator_for

      try:
        mapping = mapping_creator_for[ method ](
          alignment = alignment,
          logger = logger,
          )

      except EnsemblerError as e:
        raise Sorry(e)

      if not mapping.is_supported_type( mmtype = mtype ):
        raise Sorry("Mapping %s is not applicable to chain type %s" % (
          method,
          mtype.name,
        ))

    mappings.append( mapping )
    info.list_item_end()

  info.list_end()

  return mappings


def determine_atom_selections(assemblies, nchains, mmtypes, params, default_for, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  if sys.version_info[0] > 2:
    ziplongest = itertools.zip_longest
  else:
    ziplongest = itertools.izip_longest

  info.ordered_list_begin()
  selections = []
  selstrings = [] if params is None else params[ : nchains ]

  for ( mtype, selection ) in ziplongest( mmtypes, selstrings ):
    info.list_item_begin( text = "Chain type: %s" % mtype.name )

    if selection is None:
      info.unformatted_text( text = "Atom selection not specified, using default" )
      assert mtype in default_for
      selector = default_for[ mtype ]

    else:
      info.field( name = "Requested atom selection", value = selection )
      selector = parse_atom_selector( names = selection, mmtype = mtype )

    selections.append( selector )
    info.field_sequence(
      name = "Accepted atom names",
      value = selector.accepted_atom_names(),
      )
    info.list_item_end()

  info.list_end()

  return selections


def run(phil, logger):

  from libtbx.utils import Sorry
  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.title( text = "%s version %s" % ( PROGRAM, VERSION ) )
  info.heading( text = "Configuration options", level = 2 )
  info.preformatted_text( text = phil.as_str() )
  info.separator()

  params = phil.extract()

  assert params.input.model # checked in validate_params

  #
  # Input
  #

  # Read in PDB files
  info.heading( text = "Input structure files", level = 1 )
  info.field_sequence(
    name = "Superposable types",
    value = [ d.mmtype_name() for d in SUPERPOSABLE_MMTYPE_DEFAULTS ],
    )

  assemblies = read_in_models(
    models = params.input.model,
    superposables = [ d.mmtype() for d in SUPERPOSABLE_MMTYPE_DEFAULTS ],
    logger = logger,
    )

  # Remove unused chains from assembly if not requested for output
  if not params.output.retain_associated_chains:
    for assem in assemblies:
      assem.associateds = []

  # Sanity check on assemblies
  info.heading( text = "Summary", level = 3 )
  ( nchains, mmtypes ) = check_assembly_consistency(
    assemblies = assemblies,
    logger = logger,
    )

  # Residue mappings
  info.heading( text = "Residue mappings", level = 1 )
  mappings = determine_residue_mappings(
    assemblies = assemblies,
    nchains = nchains,
    mmtypes = mmtypes,
    params = params.configuration.mapping,
    alignments = params.configuration.alignment,
    default_for = dict(
      ( d.mmtype(), d.residue_mapping() ) for d in SUPERPOSABLE_MMTYPE_DEFAULTS
      ),
    mapping_creator_for = MAPPING_CREATOR_FOR,
    logger = logger,
    )

  # Atom selections
  info.heading( text = "Atom selections", level = 1 )
  selections = determine_atom_selections(
    assemblies = assemblies,
    nchains = nchains,
    mmtypes = mmtypes,
    params = params.configuration.atoms,
    default_for = dict(
      ( d.mmtype(), d.atom_selection() ) for d in SUPERPOSABLE_MMTYPE_DEFAULTS
      ),
    logger = logger,
    )

  #
  # Calculation
  #

  # Superposition
  info.heading( text = "Ensembling", level = 1 )

  try:
    results = process(
      assemblies = assemblies,
      ngroups = nchains,
      mappings = mappings,
      selections = selections,
      superposition = SUPERPOSITION_CREATOR_FOR[ params.configuration.superposition.method ](
        weighting = WEIGHTING_SCHEME_SETUP_FOR[ params.configuration.weighting.scheme ](
          params = params.configuration.weighting,
          ),
        params = params.configuration,
        ),
      clustering_distances = params.configuration.clustering,
      logger = logger,
      )

  except EnsemblerError as e:
    raise Sorry(e)

  # Assemblies will be copied since it is difficult to reorder chains in-place
  copies = [ r.assembly.copy_with_detached_chains() for r in results ]

  # Rename chains
  import iotbx.pdb
  chainids = iotbx.pdb.systematic_chain_ids()

  for ( assem, result ) in zip( copies, results ):
    # Unlikely to run out of chainids, so no check
    assem.rename_chains( chainiditer = iter( chainids ) )

  # Transformations
  for ( assem, result ) in zip( copies, results ):
    assem.transform( transformation = result.transformation )

  # Optional trimming
  if params.configuration.trimming.trim:
    info.heading( text = "Trimming", level = 1 )
    info.field_float(
      name = "Trimming threshold",
      value = params.configuration.trimming.threshold,
      digits = 2,
      )
    info.ordered_list_begin()

    for ( assem, result ) in zip( copies, results ):
      info.list_item_begin( text = str( assem.annotation ) )
      trim_chains_in(
        assembly = assem,
        errorlists = result.errors,
        threshold = params.configuration.trimming.threshold,
        logger = logger,
        )
      info.list_item_end()

    info.list_end()

  #
  # Output
  #
  info.heading( text = "Output files", level = 1 )

  info.task_begin( text = "Sorting on %s" % params.output.sort )
  indices = list(range( len( copies ) ))
  indices.sort( key = SORTING_ON[ params.output.sort ]( results = results ) )
  info.task_end()

  info.heading( text = "Sorted assemblies", level = 4 )
  from phaser.logoutput_new import formatter
  number = formatter.Number.normal( decimal = 3 )
  info.table_rows(
    rows = [
      [ str( copies[ i ] ), i + 1, results[i].overlap, results[i].wrmsd, results[i].unwrmsd ]
      for i in indices
      ],
    formatters = [ str, str, str, number, number ],
    headings = [ "Assembly", "Input", "Overlap", "Wrms", "Unwrms" ],
    rowheadings = 1,
    )

  if params.configuration.clustering is not None:
    info.heading( text = "Similarity groups", level = 4 )
    clcount = len( params.configuration.clustering )
    info.table_rows(
      rows = [ [ str( copies[ i ] ) ] + results[i].clusterids for i in indices ],
      headings = [ "Assembly" ] + [ "Cluster %s" % i for i in range( 1, clcount + 1 ) ],
      rowheadings = 1,
      )

  try:
    filenames = WRITE_OUTPUT_AS[ params.output.style ](
      prefix = params.output.root,
      assemblies = [ copies[i] for i in indices ],
      logger = logger,
      )

  except IOError as e:
    raise Sorry(e)

  if params.output.error_file_root is not None:
    info.heading( text = "Error files", level = 4 )
    errornames = write_error_files(
      prefix = params.output.error_file_root,
      results = [ results[i] for i in indices ],
      logger = logger
      )
    filenames.extend( errornames )

  else:
    errornames = []

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return ( filenames, errornames, results )


# XXX: adaptor for Phenix GUI, which handles exceptions separately.
class launcher (runtime_utils.target_with_save_result) :
  def run (self) :

    import os
    os.makedirs(self.output_dir)
    os.chdir(self.output_dir)

    import sys
    from phaser import logoutput_new as logoutput
    from phaser.logoutput_new import console

    handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]

    logger = logoutput.Writer(
      handlers = handlers,
      level = LOGGING_LEVELS[ "info" ],
      close_handlers = True,
      **LOGGING_LEVELS
      )

    with logger:
      ( output_files, error_files ) = run(
        phil = tbx_utils.gui_phil_process(
          master_phil = parsed_master_phil(),
          args = self.args,
          ),
        logger = logger,
        )

    assert output_files
    return (
      [ os.path.abspath(fn) for fn in output_files ],
      [ os.path.abspath(fn) for fn in error_files ],
      )


def finish_job (result) :

  assert len( result ) == 2
  assert result[0]
  files = []

  if len( result[0] ) == 1:
    files.append( ( result[0][0], "Multi-model PDB file") )

  else:
    for file_name in result[0]:
      files.append( ( file_name, "Individual model" ) )

  for file_name in result[1]:
    files.append( ( file_name, "Error associated with model" ) )

  return ( files, [] )
