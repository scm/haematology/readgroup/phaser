from __future__ import print_function

# TODO: more helpful output?

from __future__ import print_function, division # Python 3 stuff
import phaser
from phaser import pickle_support
from phaser.phenix_interface import master_params, phaser_style, match_modes
from phaser.phenix_interface import phaser_result
from phaser.phenix_interface import callbacks
from iotbx import file_reader
import iotbx.phil
from cctbx import crystal, sgtbx
from scitbx.array_family import flex # also required for unpickling?
from libtbx import object_oriented_patterns as oop
from libtbx.utils import Sorry, import_python_object, multi_out, to_str
from libtbx.phil import interface as phil_interface
from libtbx.str_utils import StringIO
from io import BytesIO
from libtbx import easy_pickle
import os
import re
import sys

pickle_support.enable()

mr_modes = ["MR_AUTO","MR_FRF","MR_FTF","MR_PAK","MR_RNP"]
ep_modes = ["EP_AUTO","EP_SAD"]

def translate_keyword (name, style, prefix=None, multiple=False) :
  method_name = "set"
  if (multiple) :
    method_name = "add"
  if prefix is not None :
    method_name = "%s_" % prefix
  fields = [ s[:4] for s in name.split("_") ]
  method_name += "_".join([ s.upper() for s in fields ])
  if (style.as_multiple) and (method_name.startswith("set")) :
    method_name = method_name.replace("set", "add")
  return method_name

def convert_keywords (input,
                      keyword_phil,
                      mode,
                      script_out,
                      inherit_mode=False,
                      default_method_base=None,
                      offset_start=0) :
  s = script_out
  for keyword in keyword_phil.objects[offset_start:] :
    if keyword.is_template != 0 :
      continue
    style = phaser_style(keyword)
    if style.ignore :
      continue
    elif match_modes(mode, style.mode, inherit_mode) :
      if keyword.is_scope :
        method_base = default_method_base
        if method_base is None :
          method_base = translate_keyword(name=keyword.name,
                                              style=style,
                                              multiple=keyword.multiple)
        elif (keyword.multiple) :
          method_base = "add" + method_base[3:]
        if style.listargs or style.array :
          # setXXXX_YYYY(param1, ...)
          args = []
          for subkeywd in keyword.objects :
            assert not subkeywd.multiple
            value = subkeywd.extract()
            if value is None :
              keyword.show()
              raise Sorry("%s must not be None." % subkeywd.full_path())
            args.append(value)
          method = getattr(input, method_base)
          if style.listargs :
            print("input.%s(%s)" % (method_base,
              ", ".join([to_str(a) for a in args])),
              file=s)
            method(*args)
          else :
            print("input.%s([%s])" % (method_base,
              ", ".join([to_str(a) for a in args])),
              file=s)
            method(args)
        elif style.kwargs :
          kwargs = {}
          kwargs_out = []
          for subkeywd in keyword.objects :
            assert not subkeywd.multiple
            value = subkeywd.extract()
            if value is None :
              keyword.show()
              raise Sorry("%s must not be None." % subkeywd.full_path())
            kwargs[subkeywd.name] = value
            value_out = to_str(value)
            if isinstance(value, str) :
              value_out = "'%s'" % value
            kwargs_out.append("%s=%s" % (subkeywd.name, value_out))
          method = getattr(input, method_base)
          print("input.%s(%s)" % (method_base, ", ".join(kwargs_out)), file=s)
          try :
            method(**kwargs)
          except Exception as e :
            if (type(e).__name__ == "ArgumentError") :
              raise RuntimeError(
                "invalid boost.python arguments:\n%s\nKeywords:\n%s" %
                  (to_str(e), to_str(kwargs)))
            else :
              raise
        elif style.silent :
          # XXX: this should work recursively...
          convert_keywords(input=input,
                           keyword_phil=keyword,
                           mode=mode,
                           script_out=script_out,
                           inherit_mode=True)
        else :
          # setXXXX_YYYY(param1)
          # setXXXX_ZZZZ(param2)
          # ...
          current_offset = 0
          for subkeywd in keyword.objects :
            if subkeywd.is_template != 0 :
              continue
            style2 = phaser_style(subkeywd)
            if style2.ignore :
              current_offset += 1
              continue
            if (not match_modes(mode, style2.mode, True)) :
              continue
            if subkeywd.is_scope :
              subkeywd_base = method_base
              if style2.parent_silent :
                subkeywd_base = None
              convert_keywords(input=input,
                               keyword_phil=keyword,
                               mode=mode,
                               script_out=script_out,
                               inherit_mode=True,
                               default_method_base=subkeywd_base,
                               offset_start=current_offset)
              current_offset += 1
              break
              #continue
            value = subkeywd.extract()
            if value is None :
              current_offset += 1
              continue
            method_base = translate_keyword(name=keyword.name,
                                            style=style,
                                            prefix=default_method_base)
            if subkeywd.multiple or style2.as_multiple :
              method_base = "add" + method_base[3:]
            if style2.silent :
              method_name = method_base
            else :
              method_name = translate_keyword(name=subkeywd.name,
                                              style=style2,
                                              prefix=method_base)
            method = getattr(input, method_name)
            if value is not None :
              if isinstance(value, str) :
                print("input.%s('%s')" % (method_name, value), file=s)
                method(value)
              else :
                if isinstance(value, sgtbx.space_group_info) :
                  print("input.%s(%s)" % (method_name, to_str(value)), file=s)
                  method(to_str(value))
                elif (isinstance(value, list)) and (style2.as_multiple) :
                  for val_ in value :
                    print("input.%s(%s)" % (method_name, to_str(val_)), file=s)
                    method(val_)
                else :
                  print("input.%s(%s)" % (method_name, to_str(value)), file=s)
                  method(value)
            else :
              print("input.%s()" % method_name, file=s)
              method()
            current_offset += 1
      elif keyword.is_definition :
        value = keyword.extract()
        if value is None :
          continue
        method_name = translate_keyword(name=keyword.name,
                                        style=style,
                                        prefix=default_method_base,
                                        multiple=keyword.multiple)
        #print(keyword.full_path())
        method = getattr(input, method_name)
        if isinstance(value, str) :
          print("input.%s('%s')" % (method_name, value), file=s)
        else :
          print("input.%s(%s)" % (method_name, to_str(value)), file=s)
        method(value)
    else :
      continue

#-----------------------------------------------------------------------
# REFLECTION DATA HANDLING
def get_miller_array (miller_arrays, labin, file_name) :
  for miller_array in miller_arrays :
    array_labels = miller_array.info().label_string()
    if array_labels == labin :
      return miller_array
  raise Sorry("Couldn't find array %s in file %s." % (labin, file_name))


def is_pg_compatible(master, other):

  if other is None:
    return True

  assert master is not None

  pg_master = master.group().build_derived_point_group()
  pg_other = other.group().build_derived_point_group()

  return pg_master == pg_other


class process_mr_hklin(object):

  def __init__(self, hklin, labin, space_group_info, unit_cell):

    array_name="%s:%s" % ( hklin, labin )
    hkl_file = file_reader.any_file( hklin )
    hkl_file.check_file_type("hkl")

    input_array = get_miller_array(
      miller_arrays = hkl_file.file_object.as_miller_arrays(),
      labin = labin,
      file_name = hklin,
      )

    if not input_array.is_xray_data_array():
      raise Sorry("%s is not X-ray data." % array_name)

    if input_array.sigmas() is None:
      raise Sorry("Experimental sigmas are required.")

    if space_group_info is not None and unit_cell is not None:
      symmetry = crystal.symmetry(
        space_group_info = space_group_info,
        unit_cell = unit_cell,
        raise_sorry_if_incompatible_unit_cell = True,
        )

    else:
      symmetry = input_array.crystal_symmetry()

    if space_group_info is not None:
      symmetry = symmetry.customized_copy(
        space_group_info = space_group_info,
        raise_sorry_if_incompatible_unit_cell = True,
        )

    if unit_cell is not None:
      symmetry = symmetry.customized_copy(
        unit_cell = unit_cell,
        raise_sorry_if_incompatible_unit_cell = True,
        )

    input_array = input_array.customized_copy( crystal_symmetry = symmetry )

    if input_array.anomalous_flag() :
      input_array = input_array.average_bijvoet_mates()

    if not input_array.is_unique_set_under_symmetry():
      input_array = input_array.merge_equivalents().array()

    bad_sigmas = ( input_array.sigmas() <= 0 )

    if True in bad_sigmas:
      input_array = input_array.select( ~bad_sigmas )

    self.input_array = input_array


  def apply(self, inputobj):

    assert len( self.sigmas ) == len( self.data )

    if self.input_array.is_xray_amplitude_array():
      inputobj.setREFL_F_SIGF( self.indices, self.data, self.sigmas )

    elif self.input_array.is_xray_intensity_array():
      inputobj.setREFL_I_SIGI( self.indices, self.data, self.sigmas )


  @property
  def space_group_info(self):

    return self.input_array.space_group_info()


  @property
  def hall_symbol(self):

    return self.space_group_info.type().hall_symbol()


  @property
  def unit_cell(self):

    return self.input_array.unit_cell()


  @property
  def d_max(self):

    return self.input_array.d_max_min()[0]


  @property
  def d_min(self):

    return self.input_array.d_min()


  @property
  def indices(self):

    return self.input_array.indices()


  @property
  def data(self):

    return self.input_array.data()


  @property
  def sigmas(self):

    return self.input_array.sigmas()


class process_ep_crystal(object):

  H_C_PRODUCT = 12398.0

  def __init__(self, crystals, space_group_info, unit_cell, out = sys.stdout):

    self.space_group_info = space_group_info
    self.unit_cell = unit_cell
    self.d_min = None
    self.d_max = None

    from cctbx import miller
    from phaser.phenix_adaptors.hyss_scoring import sad_data_adaptor

    data_read_from = {}
    datasets = []
    self.substructure_pdbs = []
    self.substructure_has = []

    for xtal in crystals:
      xtal_id = xtal.xtal_id

      if xtal.pdb_file is not None and xtal.ha_file is not None:
        raise Sorry(
          "xtal %s: Please enter EITHER a pdb_file (Angstroms)" % xtal_id
          + " OR ha_file OR NEITHER, but not both."
          )

      if xtal.pdb_file is not None :
        self.substructure_pdbs.append( ( xtal_id, xtal.pdb_file ) )

      if xtal.ha_file is not None:
        self.substructure_has.append( ( xtal_id, xtal.ha_file ) )

      for dataset in xtal.dataset :
        if None in [dataset.wave_id, dataset.hklin, dataset.labin] :
          continue

        wave_id = dataset.wave_id

        if [ dataset.wavelength, dataset.energy ].count(None) != 1:
          raise Sorry(
            "Xtal %s, wave %s: Please enter " % ( xtal_id, wave_id )
            + "EITHER a wavelength (Angstroms) OR energy (eV), but not both."
            )

        elif dataset.wavelength is not None : # TODO: more than 1?
          wavelength = dataset.wavelength

        elif dataset.energy is not None :
          wavelength = self.H_C_PRODUCT / dataset.energy

        if not dataset.hklin in data_read_from:
          hkl_file = file_reader.any_file( dataset.hklin )
          hkl_file.check_file_type("hkl")
          data_read_from[ dataset.hklin ] = hkl_file.file_object.as_miller_arrays()

        array_name = "%s:%s" % ( dataset.hklin, dataset.labin )
        input_array = get_miller_array(
          miller_arrays = data_read_from[ dataset.hklin ],
          labin = dataset.labin,
          file_name = dataset.hklin,
          )

        if not input_array.is_xray_data_array():
          raise Sorry( "%s is not X-ray data." % array_name )

        if not input_array.anomalous_flag():
          raise Sorry( "Anomalous data required (offending data: %s)." % array_name )

        if input_array.sigmas() is None:
          raise Sorry( "Experimental sigmas are required." )

        # Space group similarity test
        sgi = input_array.space_group_info()

        if self.space_group_info is None:
          self.space_group_info = sgi

        elif not is_pg_compatible( master = self.space_group_info, other = sgi ):
          raise Sorry(
            "The space group for the data in %s (%s) does " % ( array_name, sgi )
             + "not match the point group of the overall "
             + "space group (%s)." % self.space_group_info
            )

        # Cell similarity test
        cell = input_array.unit_cell()

        if self.unit_cell is None:
          self.unit_cell = cell

        elif cell is not None and not self.unit_cell.is_similar_to( cell ):
          raise Sorry(
            "The unit cell for the data in %s (%s) is " % ( array_name, cell )
            + "significantly different than the overall "
            + "unit cell (%s)." % self.unit_cell
            )

        datasets.append( ( xtal_id, wave_id, wavelength, input_array ) )

    symmetry = crystal.symmetry(
      space_group_info = self.space_group_info,
      unit_cell = self.unit_cell,
      raise_sorry_if_incompatible_unit_cell = True,
      )
    self.datasets = []
    uoi = miller.union_of_indices_registry()

    for ( xtalid, waveid, wavelength, input_array ) in datasets:
      input_array = input_array.customized_copy( crystal_symmetry = symmetry )

      if not input_array.is_unique_set_under_symmetry():
        input_array = input_array.merge_equivalents().array()

      bad_sigmas = ( input_array.sigmas() <= 0 )
      input_array = input_array.select( ~bad_sigmas )

      if input_array.is_xray_intensity_array():
        from cctbx import french_wilson
        input_array = french_wilson.french_wilson_scale(
          miller_array=input_array,
          merge=True,
          log=out)

      assert input_array.is_xray_amplitude_array()
      input_array = input_array.map_to_asu()

      ( d_max, d_min ) = input_array.d_max_min()

      if self.d_max is None or self.d_max < d_max :
        self.d_max = d_max

      if self.d_min is None or self.d_min > d_min :
        self.d_min = d_min

      converted = sad_data_adaptor( input_array )

      self.datasets.append( ( xtalid, waveid, wavelength, converted ) )
      uoi.update( converted.miller )

    self.indices = uoi.as_array()


  def apply(self, inputobj):

    from cctbx import miller
    inputobj.setCRYS_MILLER( self.indices )
    length = len( self.indices )

    for ( xtalid, waveid, wavelength, data ) in self.datasets:
      fplus = flex.double( length, 0.0 )
      sigfplus = flex.double( length, 0.0 )
      pplus = flex.bool( length, False )
      fminus = flex.double( length, 0.0 )
      sigfminus = flex.double( length, 0.0 )
      pminus = flex.bool( length, False )

      matches = miller.match_indices( self.indices, data.miller )
      assert len( matches.singles( 1 ) ) == 0

      ( posi, posd ) = zip( *matches.pairs() )
      seli = flex.size_t( posi )
      seld = flex.size_t( posd )
      fplus.set_selected( seli, data.fplus.select( seld ) )
      sigfplus.set_selected( seli, data.sigfplus.select( seld ) )
      pplus.set_selected( seli, data.pplus.select( seld ) )
      fminus.set_selected( seli, data.fminus.select( seld ) )
      sigfminus.set_selected( seli, data.sigfminus.select( seld ) )
      pminus.set_selected( seli, data.pminus.select( seld ) )

      inputobj.addCRYS_ANOM_DATA(
        xtalid, waveid, fplus, sigfplus, pplus, fminus, sigfminus, pminus,
        )
      inputobj.setWAVE( wavelength )

    for ( xtalid, pdbfile ) in self.substructure_pdbs:
      inputobj.setATOM_PDB( xtalid, pdbfile )

    for ( xtalid, hafile ) in self.substructure_has:
      inputobj.setATOM_HA( xtalid, hafile )


  @property
  def hall_symbol(self):

    return self.space_group_info.type().hall_symbol()


#-----------------------------------------------------------------------
# MAIN DRIVER
class phaser_parameter_interpreter (object) :
  def __init__ (self, index, working_dir, out=sys.stdout) :
    params = index.working_phil.extract()
    self.params = params
    mode = params.phaser.mode
    s = StringIO()
    self._script_out = s

    print("import phaser", file=s)

    phaser_out = phaser.Output()
    phaser_out.setPackagePhenix(out)

    not_alnum = re.compile(r"[^a-zA-Z0-9_\-\.]{1,}")

    if params.phaser.keywords.general.root is None :
      raise Sorry("Please define a root filename for this run.")

  # elif not_alnum.search(params.phaser.keywords.general.root) is not None :
  #   raise Sorry("The root filename must be alphanumeric (underscores OK).")

    #-------------------------------------------------------------------
    # MR DATA (HKLIN)
    process_data = None
    if match_modes(mode, index.get_phaser_modes("phaser.hklin")) : # XXX: MR_*
      if params.phaser.hklin is None :
        raise Sorry("You must specify a reflections file in mode %s." % mode)

      elif params.phaser.labin is None :
        raise Sorry("You must specify reflection data labels in mode %s."%mode)

      process_data = process_mr_hklin(
        hklin = params.phaser.hklin,
        labin = params.phaser.labin,
        space_group_info = params.phaser.crystal_symmetry.space_group,
        unit_cell = params.phaser.crystal_symmetry.unit_cell,
        )

    #-------------------------------------------------------------------
    # EP DATA
    elif match_modes(mode, index.get_phaser_modes("phaser.crystal")) :
      # TODO: eventually, allow multiples of everything
      if len(params.phaser.crystal) == 0 :
        raise Sorry("Crystal data required for mode %s." % mode)

      elif len(params.phaser.crystal) > 1 :
        raise Sorry("Only one crystal supported at this time.")

      xtal = params.phaser.crystal[0]
      if len(xtal.dataset) > 1 :
        raise Sorry("Only one wavelength supported at this time.")

      process_data = process_ep_crystal(
        crystals = params.phaser.crystal,
        space_group_info = params.phaser.crystal_symmetry.space_group,
        unit_cell = params.phaser.crystal_symmetry.unit_cell,
        out = out,
        )

    #-------------------------------------------------------------------
    # Resolution
    if process_data is not None :
      if params.phaser.keywords.resolution.high is None :
        params.phaser.keywords.resolution.high = process_data.d_min
      if params.phaser.keywords.resolution.low is None :
        params.phaser.keywords.resolution.low = process_data.d_max
    #-------------------------------------------------------------------
    # MAIN INPUT OBJECT
    input_class = import_python_object(import_path="phaser.Input%s" % mode,
      error_prefix="",
      target_must_be="",
      where_str="").object
    print("input = phaser.Input%s()" % mode, file=s)
    input = input_class()
    self.input = input
    if process_data is not None:
      if ( process_data.space_group_info is None ) or ( process_data.unit_cell is None ):
        raise Sorry("Couldn't determine automatic crystal symmetry "+
          "information from the provided reflection file(s).  Please "+
          "specify the space group and unit cell.")

      input.setCELL6( process_data.unit_cell.parameters() )
      input.setSPAC_HALL( process_data.hall_symbol )

      if ( mode.startswith("MR") or mode.startswith("EP") ) and not mode in ["MR_PAK"] :
        process_data.apply( inputobj = input )

    del process_data
    #-------------------------------------------------------------------
    # special case: streamlined single-model search (like phenix.automr)
    have_composition = have_search = False
    if (match_modes(mode, index.get_phaser_modes("phaser.model"))) :
      if (len(params.phaser.model) > 0) :
        assert (len(params.phaser.ensemble) == 0)
        assert (len(params.phaser.composition.chain) == 0)
        assert (len(params.phaser.search) == 0)
        assert ((params.phaser.component_copies is not None) and
                (params.phaser.component_copies > 0))
        assert ((params.phaser.search_copies is not None) and
                (params.phaser.search_copies > 0))
        if (params.phaser.model_rmsd is not None) :
          assert (len(params.phaser.model) == len(params.phaser.model_rmsd))
          for pdb_file, rmsd in zip(params.phaser.model,
                                    params.phaser.model_rmsd) :
            input.addENSE_PDB_RMS("ense_1", pdb_file, rmsd)
        elif (params.phaser.model_identity is not None) :
          for pdb_file, identity in zip(params.phaser.model,
                                        params.phaser.model_identity) :
            input.addENSE_PDB_ID("ense_1", pdb_file, identity*0.01)
        else:
          for pdb_file in params.phaser.model :
            input.addENSE_PDB_ID("ense_1", pdb_file, 1.0)
        if (params.phaser.seq_file is not None) :
          if (params.phaser.chain_type == "protein") :
            input.addCOMP_PROT_SEQ_NUM(params.phaser.seq_file,
              params.phaser.component_copies)
          else :
            input.addCOMP_NUCL_SEQ_NUM(params.phaser.seq_file,
              params.phaser.component_copies)
        elif (params.phaser.mol_weight is not None) :
          if (params.phaser.chain_type == "protein") :
            input.addCOMP_PROT_MW_NUM(params.phaser.mol_weight,
              params.phaser.component_copies)
          else :
            input.addCOMP_NUCL_MW_NUM(params.phaser.mol_weight,
              params.phaser.component_copies)
        input.addSEAR_ENSE_NUM("ense_1", params.phaser.search_copies)
        have_composition = have_search = True
        if (mode == "MR_RNP") :
          input.addSOLU_ORIG_ENSE("ense_1")
    #-------------------------------------------------------------------
    # ENSEMBLES
    if match_modes(mode, index.get_phaser_modes("phaser.ensemble")) :
      for i, ensemble in enumerate(params.phaser.ensemble) :
        model_id = ensemble.model_id
        if model_id is None :
          raise Sorry("Missing model ID for ensemble #%d." % (i+1))
        if len(ensemble.coordinates) > 0 :
          if ensemble.map_hklin is not None :
            # XXX: does Phaser already check this?
            raise Sorry(("Please specify either PDB files or an MTZ file "+
              "for ensemble %s, but not both.") % model_id)
          for j, coords in enumerate(ensemble.coordinates) :
            if coords.pdb is None :
              raise Sorry(("Missing PDB file for ensemble %s coordinate set "+
                "%d.") % (model_id, j+1))
            if (not coords.read_variance_from_pdb_remarks) :
              if [coords.rmsd, coords.identity].count(None) != 1 :
                raise Sorry(("Please specify EITHER an RMSD OR fractional "+
                  "identity for PDB file %s.") % coords.pdb)
              elif coords.rmsd is not None :
                print("input.addENSE_PDB_RMS('%s','%s',%s)" % (model_id,
                  coords.pdb, str(coords.rmsd)), file=s)
                input.addENSE_PDB_RMS(model_id, coords.pdb, coords.rmsd)
              else :
                print("input.addENSE_PDB_ID('%s','%s',%s)" % (model_id,
                  coords.pdb, str(coords.identity)), file=s)
                input.addENSE_PDB_ID(model_id, coords.pdb, coords.identity)
            else :
              print("input.addENSE_PDB_CARD('%s', '%s', True)" % (model_id,
                coords.pdb), file=s)
              input.addENSE_PDB_CARD(model_id, coords.pdb, True)
            # TODO: addENSE_CARD
          if ensemble.disable_check :
            input.setENSE_DISA_CHEC(model_id, True)
          if ensemble.use_hetatm :
            input.setENSE_HETA(model_id, True)
        elif ensemble.map_hklin is not None :
          ens = ensemble
          if ens.map_labels is None :
            raise Sorry("You must specify labels for map data for %s." %
                        model_id)
          if ((ens.map_extent is None or len(ens.map_extent) != 3) or
                (ens.map_centre is None or len(ens.map_centre) != 3)) :
            # TODO: confirm this
            raise Sorry(("You must specify the extent and center of the map "+
              "used for ensemble %s; this must both take the form of three "+
              "decimal numbers between zero and one.") % model_id)
          if (None in [ens.map_protein_mw, ens.map_nucleic_mw] or
              (ens.map_protein_mw == 0 and ens.map_nucleic_mw == 0)) :
            raise Sorry(("Please specify protein and/or nucleic acid masses "+
              "for ensemble %s.") % model_id)
          (f_label, phi_label) = ens.map_labels.split(",")
          print(("input.addENSE_MAP('%s', '%s', '%s', '%s', %s, %.3f, %s, "+
                 "%.1f, %.1f, %.1f") % (model_id, ens.map_hklin, f_label, phi_label,
                  str(ens.map_extent), ens.map_rms, str(ens.map_centre),
                  ens.map_protein_mw, ens.map_nucleic_mw,ens.map_cell_scale), file=s)
          input.addENSE_MAP(model_id, ens.map_hklin, f_label, phi_label,
            ens.map_extent, ens.map_rms, ens.map_centre, ens.map_protein_mw,
            ens.map_nucleic_mw,ens.map_cell_scale)
        if ensemble.solution_at_origin :
          print("input.addSOLU_ORIG_ENSE('%s')" % ensemble.model_id, file=s)
          input.addSOLU_ORIG_ENSE(ensemble.model_id)
        if ensemble.ptgroup.coverage :
          input.setENSE_PTGR_COVE(ensemble.model_id, ensemble.ptgroup.coverage)
        if ensemble.ptgroup.identity :
          input.setENSE_PTGR_IDEN(ensemble.model_id, ensemble.ptgroup.identity)
        if ensemble.ptgroup.rmsd :
          input.setENSE_PTGR_RMSD(ensemble.model_id, ensemble.ptgroup.rmsd)
        if ensemble.ptgroup.tolangular :
          input.setENSE_PTGR_TOLA(ensemble.model_id, ensemble.ptgroup.tolangular)
        if ensemble.ptgroup.tolspatial :
          input.setENSE_PTGR_TOLS(ensemble.model_id, ensemble.ptgroup.tolspatial)
        if ensemble.bins.minimum :
          input.setENSE_BINS_MINI(ensemble.model_id, ensemble.bins.minimum)
        if ensemble.bins.maximum :
          input.setENSE_BINS_MAXI(ensemble.model_id, ensemble.bins.maximum)
        if ensemble.bins.width :
          input.setENSE_BINS_WIDT(ensemble.model_id, ensemble.bins.width)
        if ensemble.estimator :
          input.setENSE_ESTI(ensemble.model_id, ensemble.estimator)
        if ensemble.trace.sampling.distance :
          input.setENSE_TRAC_SAMP_DIST(ensemble.model_id, ensemble.trace.sampling.distance)
        if ensemble.trace.sampling.target :
          input.setENSE_TRAC_SAMP_TARG(ensemble.model_id, ensemble.trace.sampling.target)
        if ensemble.trace.sampling.range :
          input.setENSE_TRAC_SAMP_RANG(ensemble.model_id, ensemble.trace.sampling.range)
        if ensemble.trace.sampling.min :
          input.setENSE_TRAC_SAMP_MIN(ensemble.model_id, ensemble.trace.sampling.min)
        if ensemble.trace.sampling.wang :
          input.setENSE_TRAC_SAMP_WANG(ensemble.model_id, ensemble.trace.sampling.wang)
        if ensemble.trace.sampling.use :
          input.setENSE_TRAC_SAMP_USE(ensemble.model_id, ensemble.trace.sampling.use)
        if ensemble.trace.sampling.asa :
          input.setENSE_TRAC_SAMP_ASA(ensemble.model_id, ensemble.trace.sampling.asa)
        if ensemble.trace.sampling.ncycle :
          input.setENSE_TRAC_SAMP_NCYC(ensemble.model_id, ensemble.trace.sampling.ncycle)
    #-------------------------------------------------------------------
    # SEARCH PROCEDURE
    if match_modes(mode, index.get_phaser_modes("phaser.search")) :
      if (not have_search) and (len(params.phaser.search) == 0) :
        raise Sorry("No search procedure defined.")
      for i, component in enumerate(params.phaser.search) :
        if component.ensembles is None :
          raise Sorry("No model IDs supplied for search component %d." % (i+1))
        elif len(component.ensembles) == 1:
          print("input.addSEAR_ENSE_NUM('%s',%s)" % (component.ensembles[0],
              str(component.copies)), file=s)
          input.addSEAR_ENSE_NUM(component.ensembles[0], component.copies)
        else :
          print("input.addSEAR_ENSE_OR_ENSE_NUM(%s,%s)" %
            (str(component.ensembles), str(component.copies)), file=s)
          input.addSEAR_ENSE_OR_ENSE_NUM(component.ensembles, component.copies)
    if match_modes(mode, index.get_phaser_modes("phaser.composition")) :
      if (not have_composition) :
        self.set_composition(input)
    #-------------------------------------------------------------------
    # OLD SOLUTION
    if params.phaser.solution is not None :
      assert os.path.isfile(params.phaser.solution)
      old_result = easy_pickle.load(params.phaser.solution)
      # XXX preserve backwards compatibility
      if hasattr(old_result, "get_phaser_object") :
        old_result = old_result.get_phaser_object()
      if type(old_result).__name__ == "ResultMR_RF" :
        solution = old_result.getDotRlist()
      else :
        solution = old_result.getDotSol()
      if len(solution) == 0 :
        raise Sorry("The specified previous solutions file is empty.")
      if mode in ["MR_FTF"] :
        if not solution.is_rlist() :
          raise Sorry("Input solutions for the translation functions must be "+
            "the output from a rotation function.")
      else :
        if solution.is_rlist() :
          raise Sorry("The specified solutions file is from a rotation "+
            "function; both rotation and translation are required when "+
            "supplying an existing solution in this mode.")
      input.setSOLU(solution)
    #-------------------------------------------------------------------
    # KEYWORD CONVERSION
    keywords = params.phaser.keywords
    keyword_phil = index.get_scope_by_name("phaser.keywords")
    convert_keywords(input, keyword_phil, mode, s)
    index.update_from_python(params)
    self.params = params
    print("result = phaser.run%s(input)" % mode, file=s)
    self.script = s.getvalue()
    self.input = input
    self.mode = mode

  #-------------------------------------------------------------------
  # COMPOSITION
  def set_composition (self, input=None, script_out=None) :
    params = self.params
    if (script_out is None) :
      script_out = self._script_out
    s = script_out
    if (input is None) :
      input = self.input
    if params.phaser.composition.solvent is not None :
      if len(params.phaser.composition.chain) > 0 :
        raise Sorry("Please specify EITHER fractional solvent content OR "+
          "individual components, but not both.")
      print("input.setCOMP_SOLV()", file=s)
      input.setCOMP_SOLV()
      # XXX: is there currently no way to set this?
    elif len(params.phaser.composition.chain) > 0 :
      print("input.setCOMP_ASU()", file=s)
      input.setCOMP_ASU()
      for i, chain in enumerate(params.phaser.composition.chain) :
        mass_attr = ["sequence_file","sequence","nres","mw"]
        masses = [ getattr(chain, name) for name in mass_attr]
        if masses.count(None) != 3 :
          raise Sorry(("Please specify ONE source of mass information for "+
            "chain #%d: sequence file, sequence string, # of residues, or "+
            "molecular weight (Da).") % (i+1))
        if chain.num is None :
          raise Sorry("Please specify a number of copies for component #%d."%
            (i+1))
        if chain.chain_type == "protein" :
          method_name = "addCOMP_PROT_"
        else :
          method_name = "addCOMP_NUCL_"
        if chain.sequence_file is not None :
          method_name += "SEQ_NUM"
          mass_value = chain.sequence_file
        elif chain.sequence is not None :
          method_name += "STR_NUM"
          mass_value = chain.sequence
        elif chain.nres is not None :
          method_name += "NRES_NUM"
          mass_value = chain.nres
        else : # chain.mw is not None
          method_name += "MW_NUM"
          mass_value = chain.mw
        method = getattr(input, method_name)
        if isinstance(mass_value, str) :
          out_value = "'%s'" % mass_value
        else :
          out_value = str(mass_value)
        print("input.%s(%s,%s)"%(method_name,out_value,str(chain.num)),
              file=s)
        method(mass_value, chain.num)
    else :
      print("input.setCOMP_AVER()", file=s)
      input.setCOMP_AVER()
    for i, atom in enumerate(params.phaser.composition.atom) :
      if atom.element is None or atom.num is None :
        raise Sorry(("Please specify an element and number of atoms "+
          "for heavy atom %d.") % (i+1))
      print("input.addCOMP_ATOM_NUM('%s', %d)" % (atom.element,atom.num),
        file=s)
      try :
        input.addCOMP_ATOM_NUM(atom.element, atom.num)
      except ValueError as e :
        raise Sorry(to_str(e))

  def run (self, out=sys.stdout, use_callback=True) :
    run_class = import_python_object(import_path="phaser.run%s" % self.mode,
      error_prefix="",
      target_must_be="",
      where_str="").object
    phaser_out = phaser.Output()
    if use_callback :
      callback = callbacks.manager()
    else :
      callback = oop.null()
    out_wrapper = multi_out()
    log_file = open(self.params.phaser.keywords.general.root + ".log", "w")
    out_wrapper.register("Primary output stream", out)
    out_wrapper.register("Phaser logfile", log_file)
    phaser_out.setPackagePhenix(out_wrapper)
    if use_callback :
      phaser_out.setPhenixCallback(callback)
    try :
      r = run_class(self.input, phaser_out)
      callback.flush_summary()
      handle_runtime_error(r, "running main function")
      self.result = r
    except Exception as e:
      if (type(e).__name__ == 'Abort'):
        pass
      else:
        raise Sorry( "Phaser error: %s" % to_str(e) )
    finally :
      log_file.close()

def handle_runtime_error (result, procedure) :
  if not result.Success() :
    raise Sorry("ERROR %s (%s): %s" % (procedure, result.ErrorName(),
                result.ErrorMessage()))

########################################################################
def run (args, out=sys.stdout, phil_str=None, working_phil=None) :
  try :
    from phenix.utilities import citations
  except ImportError :
    citations = oop.null()
  import iotbx.phil
  citations.clear_citations()
  sources = []
  if phil_str is not None :
    input_phil = iotbx.phil.parse(phil_str)
    sources.append(phil_str)
  if working_phil is not None :
    sources.append(working_phil)
  master_phil = master_params()
  cmdline = iotbx.phil.process_command_line_with_files(
    args=args,
    master_phil=master_phil,
    pdb_file_def="phaser.model",
    reflection_file_def="phaser.hklin",
    seq_file_def="phaser.seq_file")
  working_phil = cmdline.work
  index = phil_interface.index(master_phil, working_phil,
    parse=iotbx.phil.parse)
  params = index.get_python_object()
  if params.show_defaults :
    master_phil.show(out=out)
    return True
  # if labin is None and this was started from the command line, figure out
  # default Miller array to use
  if ((params.phaser.hklin is not None) and (params.phaser.labin is None) and
      (not params.phaser.run_control.gui_run)) :
    from iotbx import file_reader
    hklin = file_reader.any_file(params.phaser.hklin, force_type="hkl")
    hklin.check_file_type("hkl")
    f_label = i_label = None
    for array in hklin.file_server.miller_arrays :
      label = array.info().label_string()
      if (label in ["F,SIGF", "FOBS,SIGFOBS", "F(+),SIGF(+),F(-),SIGF(-)"]) :
        f_label = label
        break
      elif ((array.is_xray_amplitude_array()) and
          (not label.startswith("FC")) and (f_label is None)) :
        f_label = label
      elif (array.is_xray_intensity_array()) and (i_label is None) :
        i_label = label
    if (f_label is not None) :
      params.phaser.labin = f_label
    elif (i_label is not None) :
      params.phaser.labin = i_label
    index.update_from_python(params)
  #if params.dry_run :
  #  index.update("phaser.keywords.mute = True")
  tmp_out = out
  if not params.verbose :
    tmp_out = StringIO()
  run_phaser = phaser_parameter_interpreter(index, os.getcwd(), out=tmp_out)
  if params.show_script :
    out.write(run_phaser.script)
    return True
  elif params.dry_run :
    run_phaser.params.dry_run = False
    index.update_from_python(run_phaser.params)
    index.get_diff().show(out=out)
    return True
  if params.verbose :
    index.get_diff().show(out=out)
  run_phaser.run(out=out)
  params = run_phaser.params # XXX this has updated solvent_fraction
  citations.add_citation("phaser", source="Phaser")
  citations.show(out=out, source="Phaser")
  result = phaser_result(
    phaser_object=run_phaser.result,
    params=params,
    run_dir=os.getcwd())
  return result
