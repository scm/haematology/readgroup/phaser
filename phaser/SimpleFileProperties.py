#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     20/05/2013
# Copyright:   (c) oeffner 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from __future__ import print_function

from iotbx.bioinformatics import seq_sequence_parse
from iotbx import pdb
import ccp4io_adaptbx
from mmtbx.scaling import xtriage
from iotbx import mtz
import re, sys, os, math
import traceback
if sys.version_info[0] > 2:
  from io import StringIO
else:
  from cStringIO import StringIO
from phaser.utils2 import *



def NeweVRMS(seqid, nmodelresidues):
    # new rms formula obtained from RMS v seqid scatter plots
    # eVRMS = A(B + Nres)**1/3 * exp(C H) , H=1-seqid/100
    return 0.0568808 * math.exp(1.52198 * (1.0 - float(seqid)/100.0)) * math.pow(172.53 + nmodelresidues, 1.0/3.0)

def NewRMS2Seqid(rms, nmodelresidues):
    # return sequence identity from the eVRMS
    H = (math.log(float(rms)) - math.log(0.0568808) - (math.log(172.53 + nmodelresidues))/3.0)/1.52198
    return (1.0-H)*100


def ChothiaLesk(seqid):
     return max(0.8, 0.4*math.exp(1.87*(1.0-float(seqid)/100.0)))


def GetSSMSequenceIdentity(pdb1name, pdb2name, chainid1="", chainid2=""):
    try:
        pdbxtal = pdb.input(file_name= pdb1name)
        pdbtest = pdb.input(file_name= pdb2name)
        hierarchy1 = pdbxtal.construct_hierarchy()
        hierarchy2 = pdbtest.construct_hierarchy()

        chain1 = None
        if chainid1=="":
            chain1 = hierarchy1.models()[0].chains()[0]
        else:
            for chain in hierarchy1.models()[0].chains():
                if chainid1[0] == chain.id:
                    chain1 = chain
                    break

        chain2 = None
        if chainid2=="":
            chain2 = hierarchy2.models()[0].chains()[0]
        else:
            for chain in hierarchy2.models()[0].chains():
                if chainid2[0] == chain.id:
                    chain2 = chain
                    break

        ssm = ccp4io_adaptbx.SecondaryStructureMatching( moving = chain1, reference = chain2)

        seqids =[]
        #count any other alignments so we can iterate and calculate seqid of them
        Qvalsindex = [(i,e) for (i,e) in enumerate(ssm.qvalues)]
        # make list with the best SSM Q-score first
        sortQvals = sorted(Qvalsindex, key= lambda Qvindx: Qvindx[1], reverse=True)
        for qi in sortQvals:
            ssm.AlignSelectedMatch(qi[0])

            alignment = ccp4io_adaptbx.SSMAlignment.residue_groups( match = ssm )
            s = alignment.GetSSMSequenceIdentity()
            seqids.append((Roundoff(s[0],3), Roundoff(s[1],3), Roundoff(s[2],3),
              Roundoff(s[3],3), Roundoff(s[4],3), Roundoff(s[5],3), s[6]))

    except Exception as m:
        stdoutstr = "SSM alignment of %s and %s failed." %(pdb1name, pdb2name)
        return [(-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1)]

    return seqids




def GetResolutionFromPDBfile(pdbfname):
    mypdb = pdb.input(file_name= pdbfname)
    remark2str = mypdb.extract_remark_iii_records(2)[0]
# remark 2 looks like 'REMARK   2 RESOLUTION.    2.80 ANGSTROMS.'
    match = re.search(r'REMARK\s+2\s+RESOLUTION\.\s+([-+]?[0-9]*\.?[0-9]+)',
      remarkstr, re.MULTILINE)
    resolution = "%3.2f" % float(match.group(1))

    return resolution




def GetMassOfModelInPDBfile(pdbfname, chainID = ""):
    nO = 0
    nC = 0
    nN = 0
    nS = 0
    mypdb = pdb.input(file_name= pdbfname)
    root = mypdb.construct_hierarchy()
# reject any waters present
#        dryroot = root.select(root.atom_selection_cache().selection(
#          "not (resname HOH or resname 0WO or resname WAT)"), True)
# only count atoms in first model if more models are present
    for chain in root.models()[0].chains():
        if chainID == chain.id or chainID == "":
            for atom in chain.atoms():
                if atom.hetero: # ignore HETATM records such as waters or heme groups
                    continue

                elm = atom.element.strip().upper()
                if elm == "O":
                    nO = nO + 1
                if elm == "C":
                    nC = nC +1
                if elm == "N":
                    nN = nN + 1
                if elm == "S":
                    nS = nS + 1

    mass = nO*16.0 + nC*12.01 + nN*14.01 + nS*32.07
    if chainID:
        mstr = "Atomic weight of chain %s in %s is %f" %(chainID, pdbfname, mass)
    else:
        mstr = "Atomic weight of %s is %f" %(pdbfname, mass)
    return mass, mstr



def GetMassOfPDBfile(pdbfname, chainID=""):
    nprotO = 0
    nprotC = 0
    nprotN = 0
    nprotS = 0
    nprotP = 0
    nnaO = 0
    nnaC = 0
    nnaN = 0
    nnaP = 0
    protmass = 0.0
    namass = 0.0
    mypdb = pdb.input(file_name= pdbfname)
    root = mypdb.construct_hierarchy()
# reject any waters present
#        dryroot = root.select(root.atom_selection_cache().selection(
#          "not (resname HOH or resname 0WO or resname WAT)"), True)
# only count atoms in first model if more models are present
    for chain in root.models()[0].chains():
        if chainID == chain.id or chainID == "":
            if chain.is_protein:
                for atom in chain.atoms():
                    if atom.hetero: # ignore HETATM records such as waters or heme groups
                        continue
                    elm = atom.element.strip().upper()
                    if elm == "O":
                        nprotO = nprotO + 1
                    if elm == "C":
                        nprotC = nprotC +1
                    if elm == "N":
                        nprotN = nprotN + 1
                    if elm == "S":
                        nprotS = nprotS + 1
                protmass = nprotO*16.0 + nprotC*12.01 + nprotN*14.01 + nprotS*32.07
            if chain.is_na():
                for atom in chain.atoms():
                    if atom.hetero: # ignore HETATM records such as waters or heme groups
                        continue
                    elm = atom.element.strip().upper()
                    if elm == "O":
                        nnaO = nnaO + 1
                    if elm == "C":
                        nnaC = nnaC +1
                    if elm == "N":
                        nnaN = nnaN + 1
                    if elm == "P":
                        nnaP = nnaP + 1
                namass = nnaO*16.0 + nnaC*12.01 + nnaN*14.01 + nnaP*30.97
    if chainID:
        mstr = "Weight of chain %s in %s: protein: %f, nucleic acid: %f" %(chainID, pdbfname, protmass, namass)
    else:
        mstr = "Atomic weight of %s: protein: %f, nucleic acid: %f" %(pdbfname, protmass, namass)
    return protmass, namass, mstr




# currently not used
def GetMassFromSequence(seq):
# copied losely from phaser/lib/scattering.cc
    seq_error = False
    seq = seq.upper()
    H={}
    C={}
    N={}
    O={}
    S={}
#atomic composition of amino acids in protein
    H['A']=5;  C['A']=3;  N['A']=1;  O['A']=1
    H['C']=5;  C['C']=3;  N['C']=1;  O['C']=1;  S['C']=1
    H['D']=4;  C['D']=4;  N['D']=1;  O['D']=3
    H['E']=6;  C['E']=5;  N['E']=1;  O['E']=3
    H['F']=9;  C['F']=9;  N['F']=1;  O['F']=1
    H['G']=3;  C['G']=2;  N['G']=1;  O['G']=1
    H['H']=8;  C['H']=6;  N['H']=3;  O['H']=1
    H['I']=11; C['I']=6;  N['I']=1;  O['I']=1
    H['K']=13; C['K']=6;  N['K']=2;  O['K']=1
    H['L']=11; C['L']=6;  N['L']=1;  O['L']=1
    H['M']=9;  C['M']=5;  N['M']=1;  O['M']=1;  S['M']=1
    H['N']=6;  C['N']=4;  N['N']=2;  O['N']=2
    H['P']=7;  C['P']=5;  N['P']=1;  O['P']=1
    H['Q']=8;  C['Q']=5;  N['Q']=2;  O['Q']=2
    H['R']=13; C['R']=6;  N['R']=4;  O['R']=1
    H['S']=5;  C['S']=3;  N['S']=1;  O['S']=2
    H['T']=7;  C['T']=4;  N['T']=1;  O['T']=2
    H['V']=9;  C['V']=5;  N['V']=1;  O['V']=1
    H['W']=10; C['W']=11; N['W']=2;  O['W']=1
    H['Y']=9;  C['Y']=9;  N['Y']=1;  O['Y']=2
    Hydrogens = 2 # One water for unbonded N- and C-termini
    Carbons = 0
    Nitrogens = 0
    Oxygens = 1
    Sulphurs = 0
    for res in seq:
        if res not in set(['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']):
            continue
        Hydrogens += H[res]
        Carbons += C[res]
        Nitrogens += N[res]
        Oxygens += O[res]
        if res in S:
            Sulphurs += S[res]
    weight = Hydrogens + 12.01*Carbons + 14.01*Nitrogens + 16.0*Oxygens + 32.07*Sulphurs
    return weight





def GetNumberofResiduesinPDBfile(pdbfname):
    nres = 0
    mypdb = pdb.input(file_name= pdbfname)
    root = mypdb.construct_hierarchy()
    for chain in root.models()[0].chains():
        for rg in chain.residue_groups(): # don't count HETATOMs or waters
            if rg.atoms()[0].parent().resname in pdb.common_residue_names_amino_acid:
                nres += 1

    return nres




def GetNumberofResiduesFromSequence(sequence, raw=False):
    if not raw:
        seq = seq_sequence_parse(sequence)[0][0].sequence
    else:
        seq = sequence

    return len(seq)


def GetNumberofResiduesFromSequenceFile(seqfile):
    seqstr = open(seqfile,"r").read()
    return GetNumberofResiduesFromSequence(seqstr)


def GetScatteringOfModelInPDBfile(pdbfname):
    scattering = 0
    mypdb = pdb.input(file_name= pdbfname)
    root = mypdb.construct_hierarchy()
# reject any waters present
#    dryroot = root.select(root.atom_selection_cache().selection(
#      "not (resname HOH or resname 0WO or resname WAT)"), True)
# only count atoms in first model if more models are present
#    for chain in dryroot.models()[0].chains():
    for chain in root.models()[0].chains():
        nO = 0
        nC = 0
        nN = 0
        nS = 0
        nH = 0

        for atom in chain.atoms():
            if atom.hetero: # ignore HETATM records such as waters or heme groups
                continue
            elm = atom.element.strip().upper()
            if elm == "H":
                nH += atom.occ
            if elm == "O":
                nO += atom.occ
            if elm == "C":
                nC += atom.occ
            if elm == "N":
                nN += atom.occ
            if elm == "S":
                nS += atom.occ
# scattering is proportional to the square of the numbers of electrons as we are dealing with intensities
        scattering += (nH + nO*8.0*8.0 + nC*6.0*6.0 + nN*7.0*7.0 + nS*16.0*16.0)

    mstr = "Atomic scattering of %s is %4.2f" %(pdbfname, scattering)
    return scattering, mstr




def GetScatteringFromSequence(sequence, raw=False):
# copied losely from phaser/lib/scattering.cc
    if not raw:
        seq = seq_sequence_parse(sequence)[0][0].sequence
    else:
        seq = sequence

    seq_error = False
    seq = seq.upper()
    H={}
    C={}
    N={}
    O={}
    S={}
#atomic composition of amino acids in protein
    H['A']=5;  C['A']=3;  N['A']=1;  O['A']=1
    H['C']=5;  C['C']=3;  N['C']=1;  O['C']=1;  S['C']=1
    H['D']=4;  C['D']=4;  N['D']=1;  O['D']=3
    H['E']=6;  C['E']=5;  N['E']=1;  O['E']=3
    H['F']=9;  C['F']=9;  N['F']=1;  O['F']=1
    H['G']=3;  C['G']=2;  N['G']=1;  O['G']=1
    H['H']=8;  C['H']=6;  N['H']=3;  O['H']=1
    H['I']=11; C['I']=6;  N['I']=1;  O['I']=1
    H['K']=13; C['K']=6;  N['K']=2;  O['K']=1
    H['L']=11; C['L']=6;  N['L']=1;  O['L']=1
    H['M']=9;  C['M']=5;  N['M']=1;  O['M']=1;  S['M']=1
    H['N']=6;  C['N']=4;  N['N']=2;  O['N']=2
    H['P']=7;  C['P']=5;  N['P']=1;  O['P']=1
    H['Q']=8;  C['Q']=5;  N['Q']=2;  O['Q']=2
    H['R']=13; C['R']=6;  N['R']=4;  O['R']=1
    H['S']=5;  C['S']=3;  N['S']=1;  O['S']=2
    H['T']=7;  C['T']=4;  N['T']=1;  O['T']=2
    H['V']=9;  C['V']=5;  N['V']=1;  O['V']=1
    H['W']=10; C['W']=11; N['W']=2;  O['W']=1
    H['Y']=9;  C['Y']=9;  N['Y']=1;  O['Y']=2
#
    Hydrogens = 2 # One water for unbonded N- and C-termini
    Carbons = 0
    Nitrogens = 0
    Oxygens = 1
    Sulphurs = 0
    for res in seq:
        if res not in set(['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']):
            continue
        Hydrogens += H[res]
        Carbons += C[res]
        Nitrogens += N[res]
        Oxygens += O[res]
        if res in S:
            Sulphurs += S[res]
# scattering is proportional to the square of the numbers of electrons as we are dealing with intensities
    scattering = Hydrogens + 6.0*6.0*Carbons + 7.0*7.0*Nitrogens + 8.0*8.0*Oxygens + 16.0*16.0*Sulphurs
    mstr = "Scattering of sequence is %f" %scattering
    return scattering, mstr




def GetXtriageResults(mtzfname, obs_labels=None):
  errstr = None
  nrefl = None; IsigIgrt3res = None; hires = None; lowres = None;
  noutlier_outside_rescut = None; NonOrigPattersonHeight = None;
  nicerings = None; twinLtest = None
  try:
    mtzobj = mtz.object(file_name = mtzfname)
    nrefl = mtzobj.n_reflections()
    lowres = mtzobj.max_min_resolution()[0]
    hires = mtzobj.max_min_resolution()[1]

    dmpstr = StringIO()
    if obs_labels:
      xtrobj = xtriage.run([ mtzfname, "obs_labels=" + obs_labels ], out=dmpstr)
    else:
      xtrobj = xtriage.run([ mtzfname ], out=dmpstr)

    if xtrobj.data_strength_and_completeness.data_strength:
      IsigIgrt3res = xtrobj.data_strength_and_completeness.data_strength.resolution_cut
    else:
      IsigIgrt3res = hires
    noutlier_outside_rescut = 0
    # get outliers and sort them according to d_spacing
    ac_outliers = xtrobj.wilson_scaling.outliers.acentric_outliers_table.data
    if ac_outliers != None:
      zip_outliers = zip(ac_outliers[0], ac_outliers[1], ac_outliers[2], ac_outliers[3], ac_outliers[4])
      sortoutliers = sorted(zip_outliers, key=lambda tup: tup[0])
      for e in sortoutliers:
        noutlier_outside_rescut +=1
        if e[0] > IsigIgrt3res:
          break
    nicerings = 0
    for e in xtrobj.wilson_scaling.ice_rings.ice_ring_bin_location:
      if e != None:
        nicerings += 1
    twinLtest = xtrobj.twin_results.twin_summary.maha_l
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    NonOrigPattersonHeight = xtrobj.twin_results.twin_summary.patterson_height
  except Exception as m:
    errstr = str(m) + "\n"+ mtzfname + "\n" + traceback.format_exc()
    print("Error: ", errstr)
  return (nrefl, IsigIgrt3res, hires, lowres, noutlier_outside_rescut, nicerings,
            twinLtest, NonOrigPattersonHeight, errstr)



def GetMtzColumns(fname):
    #get columns labels from mtz file
    Fcol = ""; Sigcol = ""; Icol = ""; Rfreecol = ""
    avgF = 0.0; avgSig = 0.0; avgI = 0.0
    exceptmsg = ""

    try:
        obj = mtz.object(file_name = fname)
        columns = obj.column_labels()

        for i in range(0,len(columns)):
            columntype = obj.get_column(columns[i]).type()
            values = obj.get_column(columns[i]).extract_values()
            if (Fcol == ''): # to be assigned below
                if (columntype=='F'):
                    Fcol = columns[i]
                    #if min(values) > 0.0:
                    avgF = math.fsum(values)/len(values)

            if (Sigcol == ""):
                if (columntype=='Q'):
                    Sigcol = columns[i]
                    #if min(values) > 0.0:
                    avgSig = math.fsum(values)/len(values)

            if (Icol == ""):
                if (columntype=='J'):
                    Icol = columns[i]
                    if min(values) > 0.0:
                        avgI = math.fsum(values)/len(values)

            if (Rfreecol == ""):
                if (columntype=='I'):
                    Rfreecol = columns[i]

    except Exception as m:
        exceptmsg = m

    return Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI, exceptmsg


class MtzCols():
    def __init__(self):
      self.F = []; self.Sig = []; self.I = []; self.Rfree = []
      self.Fplus = []; self.Fminus = []; self.SigFplus = [];
      self.SigFminus = []; self.Iplus = []; self.Dano = []; self.SigDano = [];
      self.Iminus = []; self.SigIplus = []; self.SigIminus = [];
      self.SigF = []; self.SigI = []



def GetMtzColumnsTuple(fname):
    exceptmsg = None
    cols = MtzCols()
    columns = None
    try:
        obj = mtz.object(file_name = fname)
        columns = obj.column_labels()

        for i in range(0,len(columns)):
            columntype = obj.get_column(columns[i]).type()
            values = obj.get_column(columns[i]).extract_values()
            if (columntype=='F'):
                cols.F.append( columns[i] )
            if (columntype=='Q'):
                cols.Sig.append( columns[i] )
            if (columntype=='J'):
                cols.I.append( columns[i] )
            if (columntype=='D'):
                cols.Dano.append( columns[i] )
            if (columntype=='I'):
                cols.Rfree.append( columns[i] )
            if (columntype=='G') and "(-)" not in columns[i]:
                cols.Fplus.append( columns[i] )
            if (columntype=='G') and "(-)" in columns[i]:
                cols.Fminus.append( columns[i] )
            if (columntype=='L') and "(-)" not in columns[i]:
                cols.SigFplus.append( columns[i] )
            if (columntype=='L') and "(-)" in columns[i]:
                cols.SigFminus.append( columns[i] )
            if (columntype=='K') and "(-)" not in columns[i]:
                cols.Iplus.append( columns[i] )
            if (columntype=='K') and "(-)" in columns[i]:
                cols.Iminus.append( columns[i] )
            if (columntype=='M') and "(-)" not in columns[i]:
                cols.SigIplus.append( columns[i] )
            if (columntype=='M') and "(-)" in columns[i]:
                cols.SigIminus.append( columns[i] )

        for i in range(0,len(columns)):
            columntype = obj.get_column(columns[i]).type()
            values = obj.get_column(columns[i]).extract_values()
            if (columntype=='Q'):
                for f in cols.F:
                    if f in columns[i]:
                        cols.SigF.append( columns[i] )
                        break
                for f in cols.I:
                    if f in columns[i]:
                        cols.SigI.append( columns[i] )
                        break
                for f in cols.Dano:
                    if f in columns[i]:
                        cols.SigDano.append( columns[i] )
                        break

    except Exception as m:
        exceptmsg = m

    return cols, columns, exceptmsg



def mymain(args):
    if len(args) < 3:
        execmd= "phenix.python " + os.path.basename(args[0])
        print(
            "usage: %s <option> <pdbfile>\nwhere option is one of: [nresiduespdb, nresiduesseq, PDBresolution, PDBscattering, PDBmass]" % execmd)
        return

    option = args[1]
    fname = args[2]

    if option.lower() == "ssmseqid":
        val = GetSSMSequenceIdentity(args[2], args[3], args[4], args[5])
        for seqid in val:
            print(str(seqid))

    if option.lower() == "nresiduespdb":
        val = GetNumberofResiduesinPDBfile(fname)
        print(str(val))

    if option.lower() == "nresiduesseq":
        val = GetNumberofResiduesFromSequenceFile(fname)
        print(str(val))

    if option.lower() == "pdbresolution":
        val = GetResolutionFromPDBfile(fname)
        print(str(val))

    if option.lower() == "pdbscattering":
        val, mstr = GetScatteringOfModelInPDBfile(fname)
        print(str(val))

    if option.lower() == "pdbmass":
        val, mstr = GetMassOfModelInPDBfile(fname)
        print(str(val))

    if option.lower() == "mtz":
        Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI, exceptmsg = GetMtzColumns(fname)
        if exceptmsg != "":
            print(exceptmsg)
        else:
            print("%s columns: %s, %s, %s, %s, Averages: %f, %f, %f"
                  % (os.path.basename(fname), Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI))


if __name__ == '__main__':
    mymain(sys.argv)
