from __future__ import print_function

from phaser.identity import Atom

from libtbx.object_oriented_patterns import lazy_initialization


def create_dictionary(keys, values):

  import itertools, sys
  if sys.version_info[0] > 2:
    return dict(
      ( k, v ) for ( k, v ) in itertools.zip_longest( keys, values ) if k is not None
      )
  else:
    return dict(
      ( k, v ) for ( k, v ) in itertools.izip_longest( keys, values ) if k is not None
      )



class DeferredDictionary(object):
  """
  A dictionary where values are deferred
  """

  def __init__(self, items):

    self.data_for = dict( items )


  def keys(self):

    return self.data_for.keys()


  def __getitem__(self, key):

    return self.data_for[ key ]()


  def __contains__(self, key):

    return key in self.data_for


def load_scoring_matrix(name):

  from phaser import residue_substitution_new as residue_substitution
  return residue_substitution.MATRIX_NAMED[ name ]


def calculate_residue_topology(bonds):

  from phaser import residue_topology_new as residue_topology
  return residue_topology.connectivity_from_bonds(
    bonds = residue_topology.discard_bonds_to_hydrogen( bonds = bonds ),
    )


def calculate_residue_sidechain_topology(bonds, mainchain, attachment):

  from phaser import residue_topology_new as residue_topology
  moribund = set(
    [ a for a in mainchain if a != attachment ]
    )
  topol = residue_topology.connectivity_from_bonds(
    bonds = [
      ( a1, a2 ) for ( a1, a2 ) in bonds
      if ( a1.element != "H" and a2.element != "H"
        and a1 not in moribund and a2 not in moribund )
      ],
    )

  if attachment not in topol.descriptor_for:
    assert not topol.descriptor_for
    topol.add_atom( atom = attachment )

  return topol


def calculate_unknown_residue_rotamer_distance_topology(mmtype):

  geometry = mmtype.unknown_residue_geometry()
  geometry.discard_atoms( names = mmtype.irrelevant_atoms_for_distance_topology )
  return {
    "current": geometry.distance_topology()
    }


def get_secondary_structure_annotator(name):

  from phaser import secondary_structure
  return secondary_structure.ANNOTATOR_FOR[ name ]


def get_secondary_structure_full_annotator(name):

  from phaser import secondary_structure
  import functools

  return functools.partial(
    secondary_structure.full_structure_annotations,
    annotator = secondary_structure.ANNOTATOR_FOR[ name ],
    sstype = secondary_structure.Coil,
    )


class CacheInterface(object):
  """
  Remaps call signature to that of cache
  """

  def __init__(self, cache):

    self.cache = cache


  def __call__(self, resname):

    return self.cache( key = resname )


# Types
class MoleculeType(object):
  """
  Base class
  """

  def recognize_chain_type(self, chain, confidence = 0.9):

    return self.recognize_rgs_sequence(
      rgs = chain.residue_groups(),
      confidence = confidence,
      )


  def _recognize_sequence(self, sequence, recogniser, confidence = 0.9):

    count = len( sequence )
    counter = iter( range( int( round( ( 1.0 - confidence ) * count ) ) ) )

    try:
      for elem in sequence:
        if not recogniser( elem ):
          next(counter)

    except StopIteration:
      return False

    else:
      return True


  def recognize_rgs_sequence(self, rgs, confidence = 0.9):

    return self._recognize_sequence(
      sequence = rgs,
      recogniser = self.recognise_residue_group,
      confidence = confidence,
      )


  def recognise_residue_group(self, rg):

    return self.recognise_resname( resname = self.residue_group_resname( rg = rg ) )


  @staticmethod
  def residue_group_resname(rg):

    return rg.atom_groups()[0].resname


class MacromoleculeType(MoleculeType):
  """
  Provides access to macromolecule-specific data
  """

  def __init__(
    self,
    name,

    one_letter_codes = [],
    unknown_one_letter = "X",
    three_letter_codes = [],
    unknown_three_letter = "UNK",
    hetero = False,

    scoring_matrix_getters = [],

    mainchain_atoms = [],
    representative_mainchain_atom = None,
    mainchain_bond_atom_left = None,
    mainchain_bond_atom_right = None,
    mainchain_bond_length = None,
    sidechain_attachment_atom = None,
    unknown_residue_connectivity = [],
    idealized_residues_module_identifier = None,
    unknown_residue_geometry = [],
    irrelevant_atoms_for_distance_topology = [],

    builders = [],

    secondary_structure_annotator_name = None,

    bonds_cache_size = 50,
    geometry_cache_size = 50,
    ):

    self.name = name

    self.one_letter_codes = one_letter_codes
    self.unknown_one_letter = unknown_one_letter

    self.three_letter_codes = three_letter_codes
    #self.three_letter_codes_set = frozenset( self.three_letter_codes )
    self.unknown_three_letter = unknown_three_letter

    self.one_letter_for = lazy_initialization(
      func = create_dictionary,
      keys = self.three_letter_codes,
      values = self.one_letter_codes,
      )
    self.three_letter_for = lazy_initialization(
      func = create_dictionary,
      keys = self.one_letter_codes,
      values = self.three_letter_codes,
      )

    self.hetero = hetero

    self.scoring_matrix_dict = DeferredDictionary( items = scoring_matrix_getters )

    self.mainchain_atoms = mainchain_atoms
    self.representative_mainchain_atom = representative_mainchain_atom
    self.mainchain_bond_atom_left = mainchain_bond_atom_left
    self.mainchain_bond_atom_right = mainchain_bond_atom_right
    self.mainchain_bond_length = mainchain_bond_length
    self.sidechain_attachment_atom = sidechain_attachment_atom

    from phaser.pipeline import cache
    self.bonds_from_library = CacheInterface(
      cache = cache.MemoryCache(
        getfunc = self.bonds_from_library_getter,
        keygen = str.upper,
        maxitems = bonds_cache_size,
        )
      )
    self.bonds_from_idealized_residues = CacheInterface(
      cache = cache.MemoryCache(
        getfunc = self.bonds_from_idealized_residues_getter,
        keygen = str.upper,
        maxitems = bonds_cache_size,
        )
      )

    self.geometry_from_library = CacheInterface(
      cache = cache.MemoryCache(
        getfunc = self.geometry_from_library_getter,
        keygen = str.upper,
        maxitems = geometry_cache_size,
        )
      )
    self.geometry_from_idealized_residues = CacheInterface(
      cache = cache.MemoryCache(
        getfunc = self.geometry_from_idealized_residues_getter,
        keygen = str.upper,
        maxitems = geometry_cache_size,
        )
      )

    self.idealized_residues_module_identifier = idealized_residues_module_identifier
    self.unknown_residue_connectivity = unknown_residue_connectivity
    self.unknown_residue_topology = lazy_initialization(
      func = calculate_residue_topology,
      bonds = self.unknown_residue_connectivity,
      )
    self.unknown_residue_sidechain_topology = lazy_initialization(
      func = calculate_residue_sidechain_topology,
      bonds = self.unknown_residue_connectivity,
      mainchain = self.mainchain_atoms,
      attachment = self.sidechain_attachment_atom,
      )
    self.monlib_topology_cache = {}
    self.idlres_topology_cache = {}
    self.monlib_sidechain_topology_cache = {}
    self.idlres_sidechain_topology_cache = {}

    self.unknown_residue_geometry_data = unknown_residue_geometry
    self.irrelevant_atoms_for_distance_topology = irrelevant_atoms_for_distance_topology
    self.bond_length_tolerance = 0.1
    self.unknown_residue_rotamer_distance_topology = lazy_initialization(
      func = calculate_unknown_residue_rotamer_distance_topology,
      mmtype = self,
      )
    self.monlib_distance_topology_cache = {}
    self.idlres_distance_topology_cache = {}

    self.builder_dict = dict( builders )
    self.secondary_structure_annotator = lazy_initialization(
      func = get_secondary_structure_annotator,
      name = secondary_structure_annotator_name,
      )
    self.secondary_structure_full_annotator = lazy_initialization(
      func = get_secondary_structure_full_annotator,
      name = secondary_structure_annotator_name,
      )


  def recognise_resname(self, resname):

    return resname in self.one_letter_for()


  def recognise_rescode(self, rescode):

    return rescode in self.three_letter_for()


  def recognise_rescode_sequence(self, sequence, confidence = 0.9):

    return self._recognize_sequence(
      sequence = sequence,
      recogniser = self.recognise_rescode,
      confidence = confidence,
      )


  def three_letter_sequence_for_rgs(self, rgs):

    return [ self.residue_group_resname( rg = rg ) for rg in rgs ]


  def one_letter_sequence_for_rgs(self, rgs):

    one_letter_for = self.one_letter_for()

    return "".join(
      [ one_letter_for.get( s, self.unknown_one_letter )
        for s in self.three_letter_sequence_for_rgs( rgs = rgs ) ]
      )


  def alignable(self):

    return ( self.one_letter_codes and self.three_letter_codes
      and len( self.one_letter_codes ) == len( self.three_letter_codes ) )


  def residue_structural_connectivity(self, tolerance = 0.05):

    return StructuralConnectivity(
      pre_bond_atom = self.mainchain_bond_atom_left,
      post_bond_atom = self.mainchain_bond_atom_right,
      distance = self.mainchain_bond_length,
      tolerance = tolerance,
      )


  def residue_topology_from_library(self, resname):

    return self.fetch_topology(
      resname = resname,
      cache = self.monlib_topology_cache,
      getter = self.bonds_from_library,
      )


  def residue_topology_from_idealized_residues(self, resname):

    return self.fetch_topology(
      resname = resname,
      cache = self.idlres_topology_cache,
      getter = self.bonds_from_idealized_residues,
      )


  def residue_sidechain_topology_from_library(self, resname):

    return self.fetch_sidechain_topology(
      resname = resname,
      cache = self.monlib_sidechain_topology_cache,
      getter = self.bonds_from_library,
      )


  def residue_sidechain_topology_from_idealized_residues(self, resname):

    return self.fetch_sidechain_topology(
      resname = resname,
      cache = self.idlres_sidechain_topology_cache,
      getter = self.bonds_from_idealized_residues,
      )


  def unknown_residue_geometry(self):

    from phaser import residue_topology_new as residue_topology
    return residue_topology.Geometry(
      resname = self.unknown_three_letter,
      pairs = self.unknown_residue_geometry_data,
      )


  def residue_rotamer_distance_topology_from_library(self, resname, fine_sampling = False):

    return self.fetch_rotamer_distance_topology(
      resname = resname,
      cache = self.monlib_distance_topology_cache,
      getter = self.geometry_from_library,
      fine_sampling = fine_sampling,
      )


  def residue_rotamer_distance_topology_from_idealized_residues(self, resname, fine_sampling = False):

    return self.fetch_rotamer_distance_topology(
      resname = resname,
      cache = self.idlres_distance_topology_cache,
      getter = self.geometry_from_idealized_residues,
      fine_sampling = fine_sampling,
      )


  def mainchain_distance_for(self, topology):

    if self.sidechain_attachment_atom is not None:
      dist_for = topology.distances_from( atom = self.sidechain_attachment_atom )

    else:
      distance_dicts = [
        topology.distances_from( atom = mca ) for mca in self.mainchain_atoms
        ]
      dist_for = dict(
        ( atom, min( dist_from[ atom ] for dist_from in distance_dicts ) )
        for atom in topology.atoms
        )

    dist_for.update( ( aid, 0 ) for aid in self.mainchain_atoms )

    return dist_for


  def secondary_structure_annotations(self, chain):

    return self.secondary_structure_annotator()( chain = chain )


  def secondary_structure_full_annotations(self, chain):

    return self.secondary_structure_full_annotator()( chain = chain )


  # Getters
  def bonds_from_library_getter(self, resname):

    from phaser import residue_topology_new as residue_topology
    return residue_topology.bonds_from_library( resname = resname )


  def geometry_from_library_getter(self, resname):

    from phaser import residue_topology_new as residue_topology
    return residue_topology.geometry_from_library( resname = resname )


  def bonds_from_idealized_residues_getter(self, resname):

    geometry = self.geometry_from_idealized_residues( resname = resname )
    return geometry.bonds( tolerance = self.bond_length_tolerance )


  def geometry_from_idealized_residues_getter(self, resname):

    from phaser import residue_topology_new as residue_topology
    return residue_topology.geometry_from_idealized_residues(
      restype = self.idealized_residues_module_identifier,
      resname = resname.lower(),
      )

  # Helper methods
  def fetch_topology(self, resname, cache, getter):

    resname = resname.upper()

    if resname not in cache:
      topology = calculate_residue_topology( bonds = getter( resname = resname ) )
      cache[ resname ] = topology

    else:
      topology = cache[ resname ]

    return topology


  def fetch_sidechain_topology(self, resname, cache, getter):

    resname = resname.upper()

    if resname not in cache:
      topology = calculate_residue_sidechain_topology(
        bonds = getter( resname = resname ),
        mainchain = self.mainchain_atoms,
        attachment = self.sidechain_attachment_atom,
        )
      cache[ resname ] = topology

    else:
      topology = cache[ resname ]

    return topology


  def fetch_rotamer_distance_topology(self, resname, cache, getter, fine_sampling):

    resname = resname.upper()

    if resname not in cache:
      geometry = getter( resname = resname )
      geometry.discard_element( symbol = "H" )
      topology = {}

      for ( ann, geo ) in geometry.rotamers( fine_sampling = fine_sampling ):
        geo.discard_atoms( names = self.irrelevant_atoms_for_distance_topology )
        topology[ ann ] = geo.distance_topology()

      cache[ resname ] = topology

    else:
      topology = cache[ resname ]

    return topology


  def __str__(self):

    return self.name


# Component classes
class StructuralConnectivity(object):
  """
  Two residue_groups are consecutive if:
      distance( pre of left, post of right ) <= distance_cutoff
  NB. If either ( pre of left, post of right ) is missing, the two
      are not consecutive
  If a residue_group is disordered, only 'A'-'A' and ' '-'A' type bonds are
      considered, the rest is assumed to be correct
  """

  def __init__(self, pre_bond_atom, post_bond_atom, distance, tolerance):

    self.pre_bond_atom = pre_bond_atom
    self.post_bond_atom = post_bond_atom
    self.distance = distance
    self.tolerance = tolerance


  def __call__(self, left, right):

    pre_atom_in = self.candidates( rg = left, atom = self.pre_bond_atom )
    post_atom_in = self.candidates( rg = right, atom = self.post_bond_atom )

    if not pre_atom_in or not post_atom_in:
      return False

    if "" in pre_atom_in:
      pre_atom = pre_atom_in[ "" ]
      post_atom = post_atom_in[ min( post_atom_in ) ]

    elif "" in post_atom_in:
      post_atom = post_atom_in[ "" ]
      pre_atom = pre_atom_in[ min( pre_atom_in ) ]

    else:
      common = sorted( [ a for a in pre_atom_in if a in post_atom_in ] )

      if not common:
        return False

      pre_atom = pre_atom_in[ common[0] ]
      post_atom = post_atom_in[ common[0] ]

    return pre_atom.distance( post_atom ) <= self.distance + self.tolerance


  @staticmethod
  def candidates(rg, atom):

    pdbname = atom.pdb_name

    return dict(
      [ ( a.parent().altloc, a ) for a in rg.atoms() if a.name == pdbname ]
      )


# Completion
def protein_cbeta(resname, coord_for):
  """
  Calculates C-beta positions uniformly for each residue
  """

  C = " C  "
  CA = " CA "
  N = " N  "
  CB = " CB "
  NEEDED = ( N, CA, C )

  c_for = coord_for

  if resname == "GLY" or CB in c_for or not all( a in c_for for a in NEEDED ):
    return []

  coords = cb_coordinates_from(
    resname = resname,
    ca = c_for[ CA ],
    n = c_for[ N ],
    c = c_for[ C ],
    )

  return [ ( CB, " C", coords.elems ) ]


def cb_coordinates_from(resname, ca, n, c):

  if resname == "PRO":
    ( x_c, y_c, z_c ) = ( 0.11232442, 0.78981372, -1.30620156 )

  else:
    ( x_c, y_c, z_c ) = ( 0.00199448, 0.92989418, -1.20217920 )

  import scitbx.matrix
  # Residue-fixed orthonormal coordinate system (relative to CA):
  #   X along normalised N - normalised C
  #  -Y along normalised N + normalised C
  #   Z to complete a right-handed coordinate system
  ca = scitbx.matrix.col( ca )
  n_rel_norm = ( scitbx.matrix.col( n ) - ca ).normalize()
  c_rel_norm = ( scitbx.matrix.col( c ) - ca ).normalize()
  x = ( n_rel_norm - c_rel_norm ).normalize()
  y = -1 * ( n_rel_norm + c_rel_norm ).normalize()
  z = x.cross( y )

  cb_rel = x_c * x + y_c * y + z_c * z

  return cb_rel + ca


def cbeta_and_pro_sidechain(resname, coord_for):

  if resname == "PRO":
    return protein_specific_sidechain( resname = resname, coord_for = coord_for )

  else:
    return protein_cbeta( resname = resname, coord_for = coord_for )


def protein_specific_sidechain(resname, coord_for):

  if resname == "GLY":
    return []

  if resname == PROTEIN.unknown_three_letter:
    resname = "ALA"

  C = " C  "
  CA = " CA "
  N = " N  "
  CB = " CB "
  NEEDED = ( N, CA, C, " O  " )

  if not all( a in coord_for for a in NEEDED ):
    return []

  from phaser import residue_topology_new as residue_topology

  idealized_aa_dict = residue_topology.idealized_residues[ "protein" ]()

  if resname.lower() not in idealized_aa_dict:
    return []

  from iotbx.pdb import hierarchy
  ag = hierarchy.atom_group()
  ag.resname = resname

  for ( name, xyz ) in coord_for.items():
    atom = hierarchy.atom()
    atom.set_name( name )
    atom.set_xyz( xyz )
    atom.set_chemical_element_simple_if_necessary()
    ag.append_atom( atom )

  if CB not in coord_for:
    atom = hierarchy.atom()
    atom.set_name( CB )
    coords = cb_coordinates_from(
      resname = resname,
      ca = coord_for[ CA ],
      n = coord_for[ N ],
      c = coord_for[ C ],
      )
    atom.set_xyz( coords.elems )
    atom.set_chemical_element_simple_if_necessary()
    ag.append_atom( atom )

  from mmtbx.building import extend_sidechains
  rebuilt = extend_sidechains.extend_residue(
    residue = ag,
    ideal_dict = idealized_aa_dict,
    mon_lib_srv = residue_topology.monomer_library_server(),
    hydrogens = False,
    match_conformation = True,
    )

  return [ ( a.name, a.element, a.xyz ) for a in rebuilt.atoms() ]


# Monomer entities
class MonomerType(MoleculeType):
  """
  Provides access to monomer-specific data
  """

  def __init__(self, name, criterion):

    self.name = name
    self.criterion = criterion


  def recognise_resname(self, resname):

    return self.criterion( resname )


  def alignable(self):

    return False


# Common macromolecule types
from iotbx.pdb import amino_acid_codes as aacodes
import functools

PROTEIN = MacromoleculeType(
  name = "protein",
  # use three_letter_given_one_letter to avoid ambiguity of MSE:M and MET:M
  one_letter_codes = ( list(aacodes.one_letter_given_three_letter_modified_aa.values())
    + list(aacodes.three_letter_given_one_letter.keys()) ),
  unknown_one_letter = "X",
  three_letter_codes = ( list(aacodes.one_letter_given_three_letter_modified_aa.keys())
    + list(aacodes.three_letter_given_one_letter.values()) ),
  unknown_three_letter = "UNK",

  scoring_matrix_getters = [
    ( "identity", functools.partial( load_scoring_matrix, name = "protein-identity" ) ),
    ( "dayhoff", functools.partial( load_scoring_matrix, name = "protein-dayhoff" ) ),
    ( "blosum50", functools.partial( load_scoring_matrix, name = "protein-blosum50" ) ),
    ( "blosum62", functools.partial( load_scoring_matrix, name = "protein-blosum62" ) ),
    ],

  mainchain_atoms = [
    Atom( name = "N", element = "N" ),
    Atom( name ="CA", element = "C" ),
    Atom( name ="C",  element = "C" ),
    Atom( name ="O",  element = "O" ),
    ],
  representative_mainchain_atom = Atom( name = "CA", element = "C" ),
  mainchain_bond_atom_left = Atom( name = "C", element = "C" ),
  mainchain_bond_atom_right = Atom( name = "N", element = "N" ),
  mainchain_bond_length = 1.33,
  sidechain_attachment_atom = Atom( name = "CA", element = "C" ),
  unknown_residue_connectivity = [
    ( Atom( name = "N",  element = "N" ), Atom( name = "CA", element = "C" ), ),
    ( Atom( name = "CA", element = "C" ), Atom( name = "C",  element = "C" ), ),
    ( Atom( name = "C",  element = "C" ), Atom( name = "O",  element = "O" ), ),
    ( Atom( name = "CA", element = "C" ), Atom( name = "CB", element = "C" ), ),
    ],
  idealized_residues_module_identifier = "protein",
  unknown_residue_geometry = [
    ( Atom( name = "N",  element = "N" ), ( 12.209,  10.151,  12.481 ), ),
    ( Atom( name = "CA", element = "C" ), ( 11.069,  11.055,  12.388 ), ),
    ( Atom( name = "C",  element = "C" ), ( 10.347,  10.872,  11.058 ), ),
    ( Atom( name = "CB", element = "C" ), ( 11.525,  12.496,  12.547 ), ),
    ],
  irrelevant_atoms_for_distance_topology = [ Atom( name = "O",  element = "O" ) ],

  builders = [
    ( "cbeta", protein_cbeta ),
    ( "cbeta_and_pro", cbeta_and_pro_sidechain ),
    ( "sidechain", protein_specific_sidechain ),
    ],

  secondary_structure_annotator_name = "protein",
  )

DNA = MacromoleculeType(
  name = "dna",

  one_letter_codes = [ "A", "C", "G", "T" ],
  unknown_one_letter = "X",
  three_letter_codes = [ " DA", " DC", " DG", " DT" ],
  unknown_three_letter = "UNK",

  mainchain_atoms = [
    Atom( name = "C1'", element = "C" ),
    Atom( name = "C2'", element = "C" ),
    Atom( name = "C3'", element = "C" ),
    Atom( name = "O3'", element = "O" ),
    Atom( name = "C4'", element = "C" ),
    Atom( name = "O4'", element = "O" ),
    Atom( name = "C5'", element = "C" ),
    Atom( name = "O5'", element = "O" ),

    Atom( name = "OP1", element = "O" ),
    Atom( name = "OP2", element = "O" ),
    Atom( name = "P",   element = "P" ),
    ],
  representative_mainchain_atom = Atom( name = "P", element = "P" ),
  mainchain_bond_atom_left = Atom( name = "O3'", element = "O" ),
  mainchain_bond_atom_right = Atom( name = "P", element = "P" ),
  mainchain_bond_length = 1.61,
  sidechain_attachment_atom = Atom( name = "C1'", element = "C" ),
  )

RNA = MacromoleculeType(
  name = "rna",

  one_letter_codes = [ "A", "C", "G", "U" ],
  unknown_one_letter = "X",
  three_letter_codes = [ "  A", "  C", "  G", "  U" ],
  unknown_three_letter = "UNK",

  mainchain_atoms = [
    Atom( name = "C1'", element = "C" ),
    Atom( name = "C2'", element = "C" ),
    Atom( name = "O2'", element = "O" ),
    Atom( name = "C3'", element = "C" ),
    Atom( name = "O3'", element = "O" ),
    Atom( name = "C4'", element = "C" ),
    Atom( name = "O4'", element = "O" ),
    Atom( name = "C5'", element = "C" ),
    Atom( name = "O5'", element = "O" ),

    Atom( name = "OP1", element = "O" ),
    Atom( name = "OP2", element = "O" ),
    Atom( name = "P",   element = "P" ),
    ],
  representative_mainchain_atom = Atom( name = "P", element = "P" ),
  mainchain_bond_atom_left = Atom( name = "O3'", element = "O" ),
  mainchain_bond_atom_right = Atom( name = "P", element = "P" ),
  mainchain_bond_length = 1.60,
  sidechain_attachment_atom = Atom( name = "C1'", element = "C" ),
  )

MONOSACCHARIDE = MacromoleculeType(
  name = "monosaccharide",

  three_letter_codes = [
    "AFL", # alpha-L-Fucose
    "AGC", # alpha-D-Glucopyranose
    "BGC", # beta-D-Glucopyranose
    "FCA", # alpha-D-Fucose
    "FCB", # beta-D-Fucose
    "FUC", # Fucose
    "FUL", # beta-L-Fucose
    "G4S", # D-Galactose-4-sulphate
    "GAL", # D-Galactopyranose
    "GLA", # alpha-D-Galactopyranose
    "GLB", # beta-D-Galactopyranose
    "GLC", # D-Glucopyranose
    "GSA", # D-Galactose-4-sulphate
    "NAG", # N-acetyl D-Glucosamin
    "NAN", # 5-N-acetyl alpha-D-Neuraminic Acid
    "NGA", # N-acetyl D-Galactosamin
    "SIA", # 5-N-acetyl D-Neuraminic Acid (Sialic acid)
    "SLB", # 5-N-acetyl beta-D-Neuraminic Acid
    ]
  )

POLYMERS = [ PROTEIN, DNA, RNA, MONOSACCHARIDE ]

solvent_names = frozenset( ( "HOH", "WAT", "0WO" ) )
SOLVENT = MonomerType(
  name = "solvent",
  criterion = lambda r: r in solvent_names,
  )
HETERO = MonomerType(
  name = "hetero",
  criterion = lambda r: not any(
    t.recognise_resname( resname = r ) for t in POLYMERS + [ SOLVENT ]
    ),
  )
UNKNOWN = MonomerType( name = "unknown", criterion = lambda r: True )

KNOWN_MOLECULE_TYPES = ( PROTEIN, DNA, RNA, MONOSACCHARIDE, SOLVENT, HETERO )
MOLECULE_TYPE_CALLED = dict( ( t.name, t ) for t in KNOWN_MOLECULE_TYPES )


def determine(chain, confidence = 0.9, mmtypes = KNOWN_MOLECULE_TYPES):

  for mmt in mmtypes:
    if mmt.recognize_chain_type( chain = chain, confidence = confidence ):
      return mmt

  return UNKNOWN
