from __future__ import print_function

import libtbx.phil

class paths_converters(object):
  """
  Converter for a list of files
  """

  phil_type = "paths"

  def __str__(self):

    return self.phil_type


  def from_words(self, words, master):

    if libtbx.phil.is_plain_none( words = words ):
      return []

    import os.path
    paths = []

    for word in words:
      if libtbx.phil.is_plain_none( words = [ word ] ):
        paths.append( None )

      else:
        paths.append( os.path.expanduser( word.value ) )

    return paths


  def as_words(self, python_object, master):

    return libtbx.phil.strings_as_words( python_object )


class choices_converters(object):
  """
  Converter for a list of predefined choices
  """

  phil_type = "choices"

  def __init__(self, **choices):

    self.choices = choices


  def __str__(self):

    return "%s( %s )" % (
      self.phil_type,
      ", ".join( "%s = %s" % ( k, v ) for ( k, v ) in self.choices.items() ),
      )


  def as_words(self, python_object, master):

    return libtbx.phil.strings_as_words( python_object )


  def from_words(self, words, master):

    if libtbx.phil.is_plain_none( words = words ):
      return []

    results = []

    for ( index, word ) in enumerate( words, start = 1 ):
      value = word.value

      if value not in self.choices:
        from libtbx.utils import Sorry
        raise Sorry(
                "Not a possible choice for %s: %s (setting '%s', index %s)%s\n" % (
          master.full_path(),
          value,
          " ".join(w.value for w in words),
          index,
          word.where_str(),
        )
                + "  Possible choices are:\n"
                + "    " + "\n    ".join(sorted(self.choices, key=self.choices.get))
        )

      results.append( word.value )

    return results


def definition_choices(options):

  return "%s( %s )" % (
    choices_converters.phil_type,
    ", ".join( "%s = %s" % ( k, i ) for ( i, k ) in enumerate( options ) ),
    )


def help_choices(options):

  return ", ".join( options )
