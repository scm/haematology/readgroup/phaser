from __future__ import print_function

# Atom identifier
class Atom(object):

  def __init__(self, name, element):

    self.name = name
    self.element = element


  @property
  def pdb_name(self):

    if len( self.name ) < 4:
      if len( self.element ) <= 1:
        return " %-3s" % self.name

      else:
        return "%-4s" % self.name

    else:
      return self.name


  @property
  def pdb_element(self):

    return "%2s" % self.element


  def __eq__(self, other):

    return self.name == other.name and self.element == other.element


  def __ne__(self, other):

    return not ( self == other )


  def __hash__(self):

    return hash( ( self.name, self.element ) )


  def __repr__(self):

    return "%s(name=%s, element=%s)" % (
      self.__class__.__name__,
      self.name,
      self.element,
      )


  @classmethod
  def from_iotbx_atom(cls, atom):

    return cls( name = atom.name.strip(), element = atom.element.strip() )


  @classmethod
  def from_chem_comp_atom(cls, atom):

    return cls( name = atom.atom_id.strip(), element = atom.type_symbol.strip() )
