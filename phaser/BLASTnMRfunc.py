﻿
from __future__ import print_function

import time
import re, math
import subprocess
from phaser.pipeline import proxies, homology
from phaser import sculptor
from phaser import Output
from phaser import AlterOriginSymmate
import iotbx.mtz
from iotbx import pdb
from iotbx import reflection_file_converter
import logging
from io import StringIO
import traceback
import gzip
import string
import shutil
import glob, zipfile
from phaser import CalcCCFromMRsolutions
import scitbx
import phaser.test_phaser_parsers
import BLASTMRutils
import os, posixpath
from mmtbx.scaling import xtriage
from mmtbx.command_line import cif_as_mtz
from iotbx.bioinformatics import seq_sequence_parse

# Python 2 and 3: easiest option
from future.standard_library import install_aliases
install_aliases()

from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError

import BLASTphaserthread
from phaser import SimpleFileProperties
import SCOPdistiller
from iotbx import bioinformatics
import threading
import itertools
import copy
from phaser import tbx_utils
from phaser.utils2 import *
from phaser import AltOrigSymmatesEngine
from phaser import AvgSigmaA



class customFormatter(logging.Formatter):
  def format(self, record):
  #compute s according to record.levelno
  #for example, by setting self._fmt
  #according to the levelno, then calling
  #the superclass to do the actual formatting
      indentlevel = len(traceback.format_stack())
      indentlevel = max(0,indentlevel - 10)
      indents = ""
      for i in range(0,indentlevel):
          indents = indents + " "
      if record.levelno >= logging.WARN:
          self._fmt = "%(levelname)s: %(message)s"
      else:
          self._fmt = indents + "%(message)s"
      return logging.Formatter.format( self, record )


def openpdb(pdbfname):
    pdbfname = RecaseFilename2(pdbfname)
    return pdb.input(file_name= pdbfname)



LOG_FILENAME = ""
# Exceptions
class BLASTrunnerException(Exception):
    pass



class Buildstr():
  try:
    revision = str(int( Output().svn_revision() ))
  except: # try the git revision
    try:
      revision = str(int( Output().git_rev() )) + Output().git_branchname() \
                      + "_" + Output().git_shorthash()
    except:
      revision = "NoSvnNoGit"
  try:
    buildnumberstr = "_" + str(Output().version_number()) + "." + revision
  except:
    buildnumberstr = revision



# myobj = BLASTnMRfunc.BLASTrunner("1tfx",r"E:\Programs\Putty\plink.exe",r"E:\Programs\Putty\pscp.exe", "rdo20",r"E:\mtz_files",r"E:\Users\oeffner\Phenix\cctbx_build\exe\phaser.exe",r"E:\Programs\ClustalW2\clustalw2.exe", "wibble","","-ssh")
# myobj = BLASTnMRfunc.BLASTrunner("1tfx","ssh","scp","rdo20","/win/mtz_files","phenix.phaser","clustalw2","ln -s")
class BLASTrunner():
    "Pickup user specific settings before running BLAST and MR calculations"
    def __init__(self, targetpdbid, calclabel, sshcmd="", scpcmd="", userid="",
      mtzfolder="", phaserexe="", clustalw="", superposeexe="", passwd="",
      ismultiprocessing=False,
      sshflag=None, fnameinfix="", errorcode = None, customkeysfname = "", subsetfname=""):
        self.sshcmd = sshcmd
        self.sshflag = sshflag
        self.scpcmd = scpcmd
        self.topfolder = ""
        self.userid = userid
        self.ismultiprocessing=ismultiprocessing
        self.mtzfolder = mtzfolder
        self.phaserexe = phaserexe
        self.nmaxsols = 50
        self.fmaxhours = 48.0
        self.superpose = superposeexe
        self.clustalw = clustalw
        self.osdepbool = False
        self.srsPC = "@192.168.102.50"
        #self.srsPC = "@zeus.private.cimr.cam.ac.uk"
        self.blastPC = "@ulysses.private.cimr.cam.ac.uk"
        self.pdbnameset = set([])
        self.nuniqueseq = 0
        self.customkeysstr = ""
        self.startdir = os.getcwd()
        if os.path.exists(customkeysfname):
            self.customkeysstr = open(customkeysfname, "r").read()
        self.errorcodesum = [0]
        # if supplied the external errorcode must be an integer array with one element or None
        if errorcode != None: # assign to errorcode pointer
            self.errorcodesum = errorcode
        self.errdict = {}
        self.subsetlist = None
        if os.path.exists(subsetfname):
            self.subsetlist = eval(open(subsetfname,"r").read())
        self.writeBLASTxml = True
        self.SCOPtxt = ""
        self.DoTemplCalc = True
        self.superposelogfname =  "superposition.log"
        self.sculptprotocol = "1"
        self.targetscattering = ""
        self.neweLLGtarget = ""
        self.OtherSolutions = [["",""]]
        self.PDBTagsets = []
        bstr = Buildstr()
        self.buildnumberstr = bstr.buildnumberstr
        if fnameinfix:
            self.buildnumberstr = fnameinfix

        self.bcontrivedtmpl = False
        self.resolutions = ""
        self.bOccRefine = False
        self.bOccAIC = True
        self.OCCresidwindows = ()
        self.bdebug = False
        self.MRDict = {}
        self.FsolBsol = None
        self.extdbnameprefix = None
        self.TaggedPDBfileliststr = ""
        self.bMRTargetTblwritten = False
        self.RMSDrefine = True
        self.RotTraRefine = True
        self.brunphaser = True

        self.buseIntensities = False
        #self.refinevrmsdefault = False
        self.obsoletes = ""
        self.pretext = "\nsubsetfname = %s\ncustomkeysstr= %s\n" %(subsetfname, self.customkeysstr)
        self.ncomponents = -1
        self.useuniquesequence = False
        self.mydb = None
        self.perturbtype = False
        self.passwd = passwd
        self.calclabel = calclabel
        self.targetpdbid = targetpdbid
        self.fnameinfix=fnameinfix
        self.zipfname = targetpdbid + self.fnameinfix + ".zip" if self.fnameinfix else None
        self.topfolder = RecaseFilename(os.path.abspath(targetpdbid))
        self.baseinputfname = self.topfolder + os.path.sep + "Phaserinput" \
         + os.path.sep + self.calclabel + "input.txt"
        self.logformat = customFormatter()
        self.logger=logging.getLogger(self.__class__.__name__)
        self.formatter=customFormatter()
# only add logging handler if none is present
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if len(self.logger.handlers)==0:
            handler1= logging.StreamHandler(sys.stdout)
            handler1.emit = tbx_utils.customEmit(handler1) # use to omit linebreaks
            handler1.setFormatter(self.formatter)
            self.ismultiprocessing = ismultiprocessing
            if self.ismultiprocessing:
                self.logger.disable = True
            else:
                self.logger.addHandler(handler1)
                self.logger.setLevel(logging.DEBUG)

        #if self.calclabel != "": # we're not just creating an empty database layout
        self.scoppath = os.path.join(self.topfolder,"..", "..", "SCOP_MR")
        if os.path.exists(self.scoppath) == False:
            if "SCOPDIR" in os.environ:
                self.scoppath = os.environ["SCOPDIR"]
        self.scopobj = SCOPdistiller.SCOPdistiller(os.path.join(self.scoppath,"dir.des.scop.txt"),
         os.path.join(self.scoppath,"dir.cla.scop.txt"),
         os.path.join(self.scoppath,"PDBsXray1chainASUto2.5A.txt"),
         os.path.join(self.scoppath,"All_PDB.txt"))

        if os.sys.platform == "win32":
            self.osdepbool = True
# compile re expressions for GatherResults()
# we search for all ncs copies of a partial model so there'll be at most number of
# ncs copies Z and LLG scores in the annotation string
        self.RFZrecmp = re.compile(r"RFZ=([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.TFZrecmp = re.compile(r"TFZ=([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.LLGrecmp = re.compile(r"LLG=([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.PAKrecmp = re.compile(r"PAK=([-+]?[0-9]*)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.TEMPLLLGrecmp = re.compile(r"LLG=([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.TFZrecmp = re.compile(r"TFZ=([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.TFZequivrecmp = re.compile(r"TFZ==([-+]?[0-9]*\.?[0-9]+)",
          re.DOTALL|re.MULTILINE|re.IGNORECASE)
        self.WILSON_Brecmp = re.compile(r"^ \s+ Wilson \s+ B-factor:\s+ ( -? \d+\.\d* )",
          re.MULTILINE | re.VERBOSE)
        self.WILSON_Scalerecmp = re.compile(r"^ \s+ Wilson \s+ Scale:\s+ ( -? \d+\.\d* )",
          re.MULTILINE | re.VERBOSE)
        self.MATTHEWCOEFrecmp = re.compile(r"^\s+Most probable VM for resolution = (-?\d+\.\d*)",
          re.MULTILINE)
        self.RESOLUTIONrecmp = re.compile(r"^\s+Resolution \s of \s Ensembles: \s (\d+\.*\d*)",
          re.MULTILINE| re.VERBOSE)
        self.CPUtimerecmp = re.compile(r"\r*\nCPU Time: [^\n]* \(\s*(\d*\.\d*) secs\)\r*\n",
          re.MULTILINE)
        self.OccupanciesRefinedrecmp = re.compile(r"(Occupancies will be refined)",
          re.DOTALL)
        self.KillTimeErrorrecmp = re.compile(r"(KILL-TIME ELAPSED ERROR)", re.DOTALL)
        self.NrOccRefParamrecmp = re.compile(r"\r*\n\s*(Refined LLG reduced by number of parameters = )([0-9]+)\s*\r*\n",
          re.DOTALL)
        self.SorryNoSolutionrecmp = re.compile(r"\r*\n\s*(Sorry - No solution)\r*\n",
          re.DOTALL)
        self.OnlyPartialSolutionrecmp = re.compile(r"(No solution with all components).*(There was [0-9]* partial solutions*)",
          re.DOTALL)
        """
        Capture selected number of reflection in log file like:

        -------------------------------
        DATA FOR REFINEMENT AND PHASING
        -------------------------------

           High resolution limit imposed by solution list =  2.15

           Outlier Rejection
           -----------------
           There was 1 reflection of 41661 (0.0024%) rejected as Wilson outlier
           Outliers with a probability less than 1e-06 will be rejected
           Measurements with fewer bits of information than 0.01 will be rejected:

              H    K    L   reso        Eo^2       sigma probability  wilson low-info
              2    0   26   1.65      30.354       0.000   3.601e-08  true   false

           Resolution of All Data (Number):        1.45  18.45 (41661)
           Resolution of Selected Data (Number):   2.15  18.45 (13275)

        -------------------
        WILSON DISTRIBUTION

        """

        self.NREFLCTrecmp = re.compile(
            r"""
            \s* Resolution \s* of \s* All \s* Data \s* \(Number\):      \s* \d+ \. \d* \s* \d+ \. \d* \s* \( \d+ \)\n
            \s* Resolution \s* of \s* Selected \s* Data \s* \(Number\): \s* \d+ \. \d* \s* \d+ \. \d* \s* \( ( \d+ )

            """,
            re.MULTILINE | re.DOTALL | re.VERBOSE
            )


        self.HighResLowRMSrecmp = re.compile(
            r"""
            \s* High \s* resolution \s* limit \s* lowered \s* by \s* RMS \s* of \s* ensemble \s* =  \s* ( \d+ \. \d* ) \s*\n
            """,
            re.MULTILINE | re.DOTALL | re.VERBOSE
            )



    def __del__(self):
        if self.logger:
            while len(self.logger.handlers) > 0:
                self.logger.handlers[0].close()
                self.logger.removeHandler(self.logger.handlers[0])
            self.logger.handlers = []
        self.logger = None
        os.chdir(self.startdir) # to where we started

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.logger.log(logging.INFO,"\nRun finished on " + time.asctime( time.localtime(time.time())) + "\n")
        self.__del__()



    @staticmethod
    def GetLogFileName(choice, targetpdbid, calclabel):
        topfolder = RecaseFilename(os.path.abspath(targetpdbid))
        if choice =="b" or choice=="e" :
            LOG_FILENAME = os.path.join(topfolder, calclabel + 'RunMRautoFromlistFile.log')
        if choice =="d":
            LOG_FILENAME = os.path.join(topfolder, calclabel + "MRResults.log")
        if choice =="c":
            LOG_FILENAME = os.path.join(topfolder, calclabel + 'BLASTnMRrun.log')
        if choice=="a" or choice=="f" :
            LOG_FILENAME = os.path.join(topfolder, calclabel + 'BLASTnSetupMRruns.log')
        return LOG_FILENAME



    def SetupLogfile(self, logfname):
        if len(self.logger.handlers) <=1:
            for h in self.logger.handlers:
                if hasattr(h, "baseFilename"):
                    return # use existing file stream if already created
            handler= logging.FileHandler(logfname, mode='w', encoding = "UTF-8")
            handler.setFormatter(self.formatter)
            handler.emit = tbx_utils.customEmit(handler) # use to omit linebreaks
            self.logger.addHandler(handler)
            if self.ismultiprocessing:
                self.logger.setLevel(logging.DEBUG)
            self.pretext += "\nRun started on " + time.asctime( time.localtime(time.time())) + "\n"
            self.logger.log(logging.INFO, self.pretext)



    def MakeMyfolder(self,folderpath):
        try:
            if os.path.exists(folderpath) == False:
                self.logger.log(logging.INFO, "creating folder: %s\n" %folderpath)
                os.makedirs(folderpath)
            else:
                self.logger.log(logging.INFO, "\nFolder " + folderpath + " already exists\n")

        except Exception as m:
            self.failure = True
            self.message = m

        finally:
            return



    def CleanUpFiles(self):
        filetypes = [
              self.calclabel + "*_DoneMRJobs.txt",
              self.calclabel + "{*.log",
              self.calclabel + "{*.sum",
              self.calclabel + "{*.mtz",
              self.calclabel + "{*.pdb",
              self.calclabel + "{*.sol",
              self.calclabel + "{*.pkl",
              "PHASER.mtz",
              self.calclabel + "_VRMS{*.log",
              self.calclabel + "_VRMS{*.sum",
              self.calclabel + "_VRMS{*.mtz",
              self.calclabel + "_VRMS{*.pdb",
              self.calclabel + "_VRMS{*.sol",
              "*_shift{*.sol",
              self.calclabel + "RNP_VRMS*",
              self.calclabel + "*LLG*",
              self.calclabel + "_" + self.targetpdbid + "*.db",
              self.calclabel + "Pert*",
              self.calclabel + ",*",
              self.calclabel + "*Calc*",
              self.calclabel + "*.FTF_Scores.txt",
              "MinMLAD_*.pdb",
              "*_map.1.*",
              "offset.*",
              "AltOrigSymMLAD_" + self.calclabel + "*",
              self.calclabel + "MR_*.pdb",
              self.calclabel + "MR_*.mtz",
              self.calclabel + "MR_*.log",
              self.calclabel + "RNPMR_*.*",
              "CombinedModel*",
              "LLG_RNP_VRMS*",
              "lastens*",
              "RNP{*",
              "BetterRMS*",
              "Sculpt*prtcl*"
        ]
        files = []
        for dirpath, dirnames, filenames in os.walk(self.topfolder):
            for ft in filetypes:
                files.extend(glob.glob( os.path.join(dirpath, ft) ) )
        for f in files:
            try:
                os.remove(f)
            except Exception as m:
                self.logger.log(logging.INFO, str(m) + "\n" + traceback.format_exc() + "\n" )
        LOG_FILENAME = os.path.join(self.topfolder, self.calclabel + 'RunMRautoFromlistFile.log')
        self.SetupLogfile(LOG_FILENAME)
        relfiles = [ os.path.relpath(f, os.path.join(self.topfolder, "..")) for f in files ]
        self.logger.log(logging.INFO, "Deleted previous result files:\n%s\n" %str(relfiles) )



    def ArchiveFiles(self, label=None):
        filetypes = [
              self.calclabel + "*.log",
              self.calclabel + "*.pdb",
              self.calclabel + "*.sol",
              self.calclabel + "*dict.txt",
              self.calclabel + "*.pkl",
              "*map.*"
        ]
        files = []
        for dirpath, dirnames, filenames in os.walk(self.topfolder):
            for ft in filetypes:
                files.extend(glob.glob( os.path.join(dirpath, ft) ) )
        if not label:
            label = self.targetpdbid + "_" + self.calclabel + self.buildnumberstr + ".zip"
        zfname = os.path.join(self.topfolder, label )
        self.logger.log(logging.INFO, "Archiving files in %s:\n" %zfname )
        with zipfile.ZipFile(zfname, 'w', zipfile.ZIP_DEFLATED) as myzip:
            for f in files:
                #self.logger.log(logging.INFO, f + "\n")
                myzip.write( os.path.relpath( f, self.topfolder ) )




    def FetchPDBfile(self, subfolder, modelpdbid):
        pdbid = modelpdbid[:4] # only use the first 4 letters, not the chain ID
        pdbfname = subfolder + pdbid.lower() + '.pdb'
        if os.path.exists(pdbfname) == True:
            self.logger.log(logging.INFO, "%s already present\n" %pdbfname)
            return pdbfname

        fname = 'pdb' + pdbid.lower() + '.ent.gz'
        self.logger.log(logging.INFO, "Fetching pdbfile %s\n" %fname)

        lstcommand = [self.scpcmd,
         #'%s@zeus.private.cimr.cam.ac.uk:/srs/srs/data/pdbcurrent/pdbcurrent/%s'\
         '%s%s:/srs/srs/data/pdbcurrent/pdbcurrent/%s'\
          %(self.userid, self.srsPC, fname), '%s.'% subfolder]

        if self.passwd != "":
            lstcommand.insert(1,self.passwd)
            lstcommand.insert(1,"-pw")

        command = tuple(lstcommand)
        self.logger.log(logging.INFO, "Executing command: " + subprocess.list2cmdline(command) + "\n")
        subprocess.Popen(command, shell=self.osdepbool, stdout=subprocess.PIPE).communicate()[0]
        if os.path.exists(subfolder + fname):
            self.logger.log(logging.INFO, "Unpacking " + subfolder + fname + "\n")
            fileObj = gzip.GzipFile(subfolder + fname, 'rb')
            pdbtext = fileObj.read() # decompress the file
            fileObj.close()
            os.remove(fname) # delete the compressed file
        else:
            self.logger.log(logging.INFO, "pdbfile %s is not present. Trying RCSB instead...\n" %fname)
            u = urlopen("http://www.pdb.org/pdb/files/"+ pdbid + ".pdb")
            pdbtext = u.read()

        self.logger.log(logging.INFO, "Writing %s\n" %pdbfname)
        fileout = open(pdbfname,'w')
        fileout.write(pdbtext)
        fileout.close()

        return pdbfname




    def PDB2SeqLocal(self, pdbchainid, uselocalDB = True):
        self.logger.log(logging.INFO, "PDB2SeqLocal(%s, %s)\n" %(pdbchainid, str(uselocalDB)))

        if uselocalDB == True: # get the sequence form our local database
            lstcommand = [self.sshcmd, "%s%s" %(self.userid, self.srsPC), "(source /etc/profile; getz", "-f",
              "seq", "-sf", "fasta", "'[pdbcurrent:%s]')" % pdbchainid[:4]]# first 4 characters only are the pdbid
            #lstcommand = ["getz", "-f", "seq", "-sf", "fasta", "'[pdb:%s]')" % pdbchainid[:4]]# first 4 characters only are the pdbid

            if self.passwd != "":
                lstcommand.insert(1,self.passwd)
                lstcommand.insert(1,"-pw")

            if self.sshflag != None:
                lstcommand.insert(1,self.sshflag)

            command = tuple(lstcommand)
            self.logger.log(logging.INFO, "Executing command: %s\n" %subprocess.list2cmdline(command))

            fastastr=subprocess.Popen(command,shell=self.osdepbool,
             stdout=subprocess.PIPE).communicate()[0]

            if fastastr.find(">") < 0: # our local pdb server doesn't have this sequence
                return self.PDB2SeqLocal(pdbchainid, False)

            t = re.findall('>[^>]*', re.split('Welcome to SRS 8.1.1 -------------',fastastr)[-1])

        else: # get the sequence from the RCSB
            u = urlopen("http://www.pdb.org/pdb/files/fasta.txt?structureIdList="+ pdbchainid[:4])
            fastastr = u.read()
            t = re.findall('>[^>]*', fastastr)

        if len(pdbchainid) != 4 and len(pdbchainid) != 6 and len(pdbchainid) != 11:
            raise BLASTrunnerException("Is %s supposed to be a pdbid?!" % pdbchainid)

        sequence = ""
        if len(pdbchainid) == 4: # we only supplied the pdbid, not the chainid
            sequence = t # sequence now holds an array of sequences for each chain
        else:
            if len(pdbchainid) == 6: # chainid in pdbchainid is specified as 1BL8_D
                chainid = pdbchainid[5].upper()

            if len(pdbchainid) == 11:  # chainid in pdbchainid is specified as 1BL8_ChainD
                chainid = pdbchainid[10].upper()

            for i in range(0, len(t)):
                if uselocalDB == True:
# chain id in returned sequences from our local database is the 12 character in sequence preamble
# like:
# >1DDL_ChainC
# MEQDKILAHQASLNTKPSLLPPPVGNPPPVISYPFQITLASLGTEDAADSVSIASNSVLA
                    if t[i][11].upper() == chainid.upper():
                        sequence = [t[i],] # we expect an array to be returned, so make one
                else:
# chain id in returned sequences from RCSB is the 6 character in sequence preamble
# like:
# >1TFX:C|PDBID|CHAIN|SEQUENCE
# KPDFCFLEEDPGICRGYITRYFYNNQTKQCERFKYGGCLGNMNNFETLEECKNICEDG
                    if t[i][6].upper() == chainid.upper():
                        sequence = [t[i],] # we expect an array to be returned, so make one

        if sequence == "":
            raise BLASTrunnerException("Could not find sequence for chain %s in pdb %s" \
                                       % (chainid, pdbchainid[:4]))
        else:
            for chainseq in sequence:
                rawseq = seq_sequence_parse(chainseq)[0][0].sequence
                m = re.findall(r"[xzbju]", rawseq,re.MULTILINE|re.IGNORECASE)
                if len(m) > 0:
                    if uselocalDB == True:
                        self.logger.log(logging.WARN,"Found illegal letters %s in the sequence. Trying RCSB instead\n" %m)
                        newseq = self.PDB2SeqLocal(pdbchainid, False)
                        newsequence = []
                        for newchainseq in newseq:
                            seq = seq_sequence_parse(newchainseq)[0][0].sequence
                            name = seq_sequence_parse(newchainseq)[0][0].name
                            chainlbl = name[5]
                            newsequence.append(">" + name[:4] + "_Chain" + chainlbl + "\n" + seq + "\n")

                        return newsequence

                    else:
                        self.errorcodesum[0] += 1
                        raise BLASTrunnerException("Illegal letter, %s, in the sequence\n%s\nfor %s" \
                                                   % (m, chainseq, pdbchainid))

            return sequence




    def GetLocalBLASTHomologues(self, maxpdbs, uniquetargetchains):
        sequence = self.PDB2SeqLocal(self.targetpdbid)
        # When chains are repeated in the pdb file we use a unique list of sequences.
        # To distinguish identical chains labbeled with different letters from one another
        # we strip off the title labels
        untitledseq = []
        for chainseq in sequence:
            print("chainseq= " + str(chainseq))
            rawseq = seq_sequence_parse(chainseq)[0][0].sequence
# Sequence comment has chain label as the last non-whitespace letter. grab it.
# 1DDL_ChainC
# MEQDKILAHQASLNTKPSLLPPPVGNPPPVISYPFQITLASLGTEDAADSVSIASNSVLA
            preamble = seq_sequence_parse(chainseq)[0][0].name
            if preamble.find("|PDBID|CHAIN|SEQUENCE") > -1:  # new convention for preamble
                chainlabel = seq_sequence_parse(chainseq)[0][0].name[5]
            else: # still using old convetion as in ">2D5B_ChainA"
                chainlabel = seq_sequence_parse(chainseq)[0][0].name.rstrip()[-1]

            untitledseq.append([chainlabel, rawseq])

        if self.useuniquesequence:
            # build the unique list
            uniqueseq = []
            uniqueseq.append(["",""]) # dummy element forces definition of this 2dimensional list

            for i in range(0,len(untitledseq)):
                # this will use the first chainid as the key for this dictionary and
                # will thus overwrite entries with different chainids
                foundit = False
                for j in range(0,len(uniqueseq)):
                    if untitledseq[i][1] == uniqueseq[j][1]:
                        foundit = True
                        uniqueseq[j][0] = uniqueseq[j][0] + untitledseq[i][0]

                if not foundit:
                    uniqueseq.append(untitledseq[i])

            uniqueseq.remove(["",""]) # get rid of dummy element

            self.logger.log(logging.INFO, "Sequence list for %s is:\n" %self.targetpdbid)
            self.logger.log(logging.INFO, sequence + "\n")
            self.logger.log(logging.INFO, "Omitting duplicate sequences we have:\n")
        else:
            uniqueseq = untitledseq

        self.logger.log(logging.INFO, uniqueseq + "\n")
        self.logger.log(logging.INFO,"\n")
# pass the uniqueseq on to the external variable
        uniquetargetchains.extend(uniqueseq)
        #uniquetargetchains.remove([])#remove the first empty dummy

        self.nuniqueseq = len(uniqueseq) # store for use in GetCombinedSearchesforTargetChains()
        myids = []
        for chainid, seq in uniqueseq: # loop over the list of [chainid,sequence]
            if self.writeBLASTxml:
                # for each unique sequence in the pdb supplied let's find some homologues
                lstcommand = ["blastp", "-db", r"B:\Programs\Utilities\BLASTDatabase\PDB\NonRedundant\MyNonRedundDb3",
                 "-evalue", "1000", "-num_alignments", str(int(maxpdbs)), "-matrix", "BLOSUM62", "-outfmt", "5"]
# On Windows it's important to use blast executable targetting the same
# C-runtime as the Python executable. Otherwise the stdin pipe will not work.
# Blast 2.2.28 works unlike newer versions that uses OpenMP
                process = subprocess.Popen(
                    lstcommand,
                    stdin = subprocess.PIPE,
                    stdout = subprocess.PIPE,
                    stderr = subprocess.STDOUT
                    )
                ( out, err ) = process.communicate(input = seq)

                #lstcommand = [self.sshcmd, \
                #      self.userid + self.srsPC, "/srs/blast/blast-2.2.20/bin/blastall"]

                #if self.passwd != "":
                #    lstcommand.insert(1,self.passwd)
                #    lstcommand.insert(1,"-pw")

                #if self.sshflag != None:
                #    lstcommand.insert(1,self.sshflag)
                """
                lstcommand = ["blastp", "-h"]
                command = tuple(lstcommand)
                self.logger.log(logging.INFO, "Executing command: " + subprocess.list2cmdline(command))
                myproxy = proxies.NCBIBlastStandalone(command, self.osdepbool)
                blastcmdstr = [""]
                myproxy.submit(program="blastp",
                 database=r"B:\\Programs\\Utilities\\BLASTDatabase\\PDB\\NonRedundant\\MyNonRedundDb",
                 query=seq, maxresults=maxpdbs, expectation = 1000, pcmdstr = blastcmdstr)
                self.logger.log(logging.INFO, blastcmdstr[0])
                # get the BLAST results as xml data
                xmldata = myproxy.results(jobid=0)
                """
                self.logger.log(logging.INFO, subprocess.list2cmdline(lstcommand + [seq]) + "\n")
                xmldata= out
                #print "xmldata=\n", xmldata
                n = xmldata.find("<?xml")
                if n > 0: # strip off garbage such as "Welcome to the SRS"
                    xmldata = xmldata[n:]

                fname = "%s_%s_blast.xml" %(self.targetpdbid, chainid)
                fileout = open(fname,"w")
                fileout.write(xmldata)
                fileout.close()
            else:
                fname = "%s_%s_blast.xml" %(self.targetpdbid, chainid)
                xmldata = open(fname,"r").read()

# parse the xml into python structures
            myparser = homology.get_ncbi_blast_parser()
            results = myparser(xmldata)

            seqlength = results.root.query_len
            length = len(results.root.iterations[0].hits)
            self.logger.log(logging.INFO,"Chain %s yields %d BLAST hits\n" %(chainid,length))
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            for hit in results.root.iterations[0].hits:
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                # label each homologue with sequence identity and the chain id of the unique chain
                identity = hit.hsps[0].identity
                align_len = hit.hsps[0].length
                hit_accession = hit.accession
                hit_length = hit.length
                seqid = "%4.3f" %(float(identity*100)/min(hit_length, seqlength))
#                seqid = float(identity*100)/seqlength
#                seqid = float(identity*100)/align_len
                myids.append([hit_accession, [seqid], chainid])

        return myids



    def GetPDBidsFromPDBLocalWithSequenceIds(self, maxseqid, minseqid, maxpdbs,
       uniquechains = None):
        if (maxseqid < minseqid or maxseqid < 0 or minseqid <0 or maxseqid > 100 or minseqid > 100):
            raise BLASTrunnerException("Check values of maxseqid and/or minseqid")
            self.logger.log(logging.EXCEPTION,"Check values of maxseqid and/or minseqid\n")
            exit(42)
# initialise in case of uniquechains not supplied
        if uniquechains == None:
            uniquechains = []

        myids = self.GetLocalBLASTHomologues(maxpdbs, uniquechains)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        selectedpdbs = []
        self.logger.log(logging.INFO,u"BLAST sequence identities of models are:\n")
        for i in range(0,len(myids)):
            resol = self.GetPDBResolution(myids[i][0])
            if (float(myids[i][1][0]) >= minseqid and float(myids[i][1][0]) <= maxseqid):
                if float(resol) == 0.0 :
                    self.logger.log(logging.WARN, "The " + myids[i][0][:4] +
                     " entry isn't present in the resolution.txt file. Grab the latest from RCSB!\n")
                    continue

                if float(resol) < 0.0 :
                    self.logger.log(logging.INFO, myids[i][0][:4] + " at " + resol \
                     + u" Å was not determined from crystallography and is ignored.\n")
                    continue

                self.logger.log(logging.INFO, myids[i][0][:4] + " at " + resol \
                 + u" Å, seqid: %s target chain: %s is considered\n" %(myids[i][1][0], myids[i][2]))

                selectedpdbs.append(myids[i])
            else:
                self.logger.log(logging.INFO, "%s seqid: %s, target chain: %s is binned\n" \
                 %(myids[i][0], myids[i][1][0], myids[i][2]))

        return selectedpdbs




    def RemovePDBsWithSimilarSeqIdentFromList(self, selectedpdbs, maxseqid, minseqid, myinc):
# we're not interested in many pdbs having almost the same seqid so compose
# a list of pdbs evenly distributed over seqids
# Do this for each of the chains
        bins = self.PutPDBsInChainIdBins(selectedpdbs)

        self.logger.log(logging.INFO,"Purging models with similar sequence identity\n")
        evenlyspread = []
        #inc = 1 # seqid will increase by at least 1% through each element
        inc = max(1.0, myinc)

        for n in range(0,len(bins)):
            imin = minseqid
            imax = minseqid + inc
            while imax <= maxseqid:
                i=0
                found = False
                while found==False and i< len(bins[n]):
# no more than one model between imin and imax
                    if float(bins[n][i][1][0]) >= imin and float(bins[n][i][1][0]) <imax:
                        evenlyspread.append(bins[n][i])
                        found =True

                    i=i+1
                    if i == len(bins[n]) and found==False:
                        self.logger.log(logging.INFO,"No model found within"\
                          + " %d%% and %d%%" %(imin,imax)\
                          + " seq identity for chain %s\n" %bins[n][0][2])

                imin = imin + inc
                imax = imax + inc

#        self.logger.log(logging.INFO,"\nModels to be used are\npdb_id+chain_id, BLAST sequence_id, target chain_id:")
#        mdlist = "\n".join( ["%s, %s, %s"%(a,b,c) for a,b,c in evenlyspread])
#        self.logger.log(logging.INFO,"\n%s\n" %mdlist)
        return evenlyspread




    def RemoveQuarantinedPDBs(self, pdbsseqids):
        approvedPDBs = []
# avoid using falsified or fabricated structures
        condemnedPDBs = set(["1BEF", "1CMW", "1DF9", "2QID", "1G40", "1G44", "1L6L",
         "2OU1", "1RID", "1Y8E", "2A01","2HR0"])
        if self.targetpdbid in condemnedPDBs:
            self.logger.log(logging.CRITICAL,"%s is a condemned structure!\n" %self.targetpdbid)

        for pdb in pdbsseqids:
            if not pdb[0][:4].upper() in condemnedPDBs:
                approvedPDBs.append(pdb)
            else:
                self.logger.log(logging.INFO,"Structure %s has been quarantined.\n" %pdb[0])

        pdbsseqids = approvedPDBs




    def GetPDBResolution(self, pdbid):
        resolution = "0.0"
        validpdb = pdbid[:4].upper()
        try:
            if self.obsoletes == "":
                self.obsoletes = open(self.scoppath + os.path.sep + "obsolete.txt","r").read()
            """
            The obsolete pdb entries file from the RCSB looks like:
            OBSLTE    16-JUN-05 1OG8     2BUH
            OBSLTE    16-JUN-05 1OG9     2BUI
            OBSLTE    27-JAN-09 1OGR
            OBSLTE    09-JUN-10 1OJB     2XFU
            """
            m = self.obsoletes.find(pdbid[:4].upper(), 0)

            if m>0:
                n = self.obsoletes.find("\n", m) # got the line now with the obsolete and the new pdb id
                match = re.search(r"(" + pdbid[:4].upper() + ")\s+([A-Za-z0-9]+)",
                  self.obsoletes[m:n], re.MULTILINE)
                if match != None and len(match.groups()) > 1:
                    validpdb = match.group(2)

            if self.resolutions == "":
                self.resolutions = open(self.scoppath + os.path.sep + "resolution.txt","r").read()
# The resolution file looks like below:
#
# 1XC4  ;       2.8
# 1XC5  ;       -1.00
# 1XC6  ;       2.1
# 1XC7  ;       1.83
            n = self.resolutions.find(validpdb, 0)
            if n<0:
                self.logger.log(logging.WARN,"No resolution found in resolution.txt for %s.\n" %pdbid)
                return resolution

            m = self.resolutions.find("\n",n)
# we found the line with the resolution now. So extract the resolution now
# if resolution is -1.0 then it's NMR data
            match = re.search(r'('+ validpdb +'\s;\s)([-+]?[0-9]*\.?[0-9]+)', \
               self.resolutions[n:m], re.MULTILINE)
            resolution = "%3.2f" % float(match.group(2))
        except Exception as e:
            self.errorcodesum[0] += 30
            self.logger.log(logging.WARN,"No resolution found in %s.pdb\n" %pdbid)
            raise BLASTrunnerException(e)

        return resolution




    def sculptprot(self):
        return "sculpt" + self.sculptprotocol + "_" if self.sculptprotocol != "" else ""



    def DoQuickRefinementOfSuperposition(self, searchmodels):
        print("searchmodels = ", searchmodels)
        # prepare for a quick refinement of superposed models. The output pdb should then
# be a solution that can be used as a template
        sortedmodels = sorted(searchmodels) # to easily locate an already calculated template

        sortednames = []
        for sortedmodel in sortedmodels:
            sortednames.append( sortedmodel[0])
        sortedmodelname = ",".join(sortednames)

        modelname = []
        for model in searchmodels:
            modelname.append( model[0])
        modelsname = ",".join(modelname)
# by comparing with other sorted folder names we avoid many redundant calculations
# given that template solutions {pdb_a,pdb_b,pdb_c} are the same as {pdb_b, pdb_a,pdb_c}
# and similar for all permutations

        templfolder = os.path.join(self.topfolder, "TemplateSolutions",sortedmodelname)
        RNProotname = posixpath.join("TemplateSolutions",sortedmodelname,
         self.calclabel + "RNP{" + modelsname + "}.superposed")
        RNPsolfname = RNProotname + ".sol"

        if os.path.exists(RNPsolfname):
            self.logger.log(logging.INFO,
             "Not making template solution %s as it was already created as %s\n"
             %(modelsname,sortedmodelname))
            return True

        self.MakeMyfolder(templfolder)

        self.logger.log(logging.INFO,
         "Refining superposed search model(s) %s on target structure\n" %modelsname)
        rnpfnameprefix = templfolder + os.path.sep + self.calclabel + sortedmodelname + "_quickRNPinput"
        RNPinputfile = open(rnpfnameprefix + ".txt", "w")
        # must refine Bfac as well for reliable comparison of LLGs in 2 component calculations
        RNPinputfile.write("MODE MR_RNP\nMACMR ROT ON TRA ON BFAC ON VRMS ON\n")
        #RNPinputfile.write("MODE MR_RNP\nMACMR ROT ON TRA ON BFAC OFF VRMS ON\n")
        solstr = ""

        try:
# get models and use their orientations and positions from the superpose output as solutions
            for model in sortedmodels:
                targetpdbchainid = model[2]
                seqid = model[1][0]
                ensname = model[3][0][0]

                pdbfname = self.GetSculptFname(targetpdbchainid, model[0])
                pdbfname = posixpath.relpath(pdbfname, self.topfolder)

                RNPinputfile.write("ENSEMBLE %s  PDB %s  IDENTITY %s\n" %(ensname, pdbfname, seqid))

                for ncscopy in model[3]:
                    euler = ncscopy[1]
                    ortho = ncscopy[2]
                    solstr = solstr + "SOLU 6DIM ENSE " + ensname + "  EULER %2.3f %2.3f %2.3f " \
                     % euler + "  ORTH %2.3f %2.3f %2.3f  BFAC 0.00\n" %ortho

            RNPinputfile.write("\nSOLU SET Superposed\n" + solstr)
            RNPinputfile.write("ROOT " + RNProotname + "\n")
    # this file is concatenated to the one 3 folders above before submitted to phaser
            RNPinputfile.close()
            myphaser = BLASTphaserthread.PhaserRunner(self.baseinputfname,
                 rnpfnameprefix, self.calclabel + "RNP", "", self)
# run Phaser. Shouldn't take long if models superposed well on the target
            self.logger.log(logging.INFO, "Now refining superposed model\n")
            stdoutput = myphaser.PhaserRun(templfolder)
            self.errorcodesum[0] += myphaser.errorcodesum
            phaserlogfile = open(RNProotname + ".log","w")
            phaserlogfile.write(stdoutput)
            phaserlogfile.close()

# extract eulers and frac from the solution file. These are then the proposed template solution
            RNPsolfile = open(RNPsolfname,"r")
            RNPsol = RNPsolfile.read()
            RNPsolfile.close()
            n = RNPsol.find("SOLU 6DIM ENSE",0)
            solutions = RNPsol[n:]

            tmplfname = templfolder + posixpath.sep + self.calclabel + "template{" + sortedmodelname + "}.txt"

            templatefile = open(tmplfname,"w")
            templatefile.write("SOLU TEMPLATE\n" + solutions)
            templatefile.close()

            self.logger.log(logging.INFO,
             "Eulers and fractionals from refinement of superposition: \n"
             + solutions)

        except Exception as m:
            raise
            self.errorcodesum[0] += 1
            self.logger.log(logging.ERROR, "Problem refining " + modelsname + "\n" + str(m))
            return False

        olddir = os.getcwd()
        #os.chdir(templfolder)
# remove superfluous output
        mtzs = glob.glob(os.path.join(templfolder, "*.mtz"))
        #delf = mtzs + dbgs
        delf = mtzs
        for i in range(0,len(delf)):
            os.remove(delf[i])

        #os.chdir(olddir)
        self.logger.log(logging.INFO,"Done DoQuickRefinementOfSuperposition\n")
        return True




    def PrepareTemplateSolutions(self, searchmodels, permutations):
        model = []
        targetpdbchainid = []
        seqid = []

        for pdb_id in searchmodels:
            model.append( pdb_id[0])
            targetpdbchainid.append( pdb_id[2])
            seqid.append( pdb_id[1])

        self.logger.log(logging.INFO, "\nPrepareTemplateSolutions(%s)"
         % ",".join(model))

# successively build up template solutions with more and more models
        for permutedmodels in permutations:
            lmodel = []
            for lmodels in permutedmodels:
                lmodel.append(lmodels)
                if self.DoQuickRefinementOfSuperposition(lmodel) == False:
                    self.logger.log(logging.WARN, "\nPrepareTemplateSolutions(%s) failed.\n" \
                     "No MR calculations with this combined model will be carried out.\n"
                      % ",".join(model))
                    self.errorcodesum[0] += 1
                    return False

        return True




    def GetEuler_and_Translation_of_Model_to_Target(self, modelpdbfname, targetpdbfname,
      targetpdbchainid = ""):
        self.logger.log(logging.DEBUG, "GetEuler_and_Translation_of_Model_to_Target(%s, %s, %s)\n" \
          %(modelpdbfname, targetpdbfname, targetpdbchainid))
        if os.path.exists(modelpdbfname) == False:
            raise BLASTrunnerException(os.path.abspath(modelpdbfname) + " doesn't exist")
        if os.path.exists(targetpdbfname) == False:
            raise BLASTrunnerException(os.path.abspath(targetpdbfname) + " doesn't exist")

        command = (self.superpose, modelpdbfname , targetpdbfname, "-s", targetpdbchainid,
          "-o", modelpdbfname[:-4] + '_superposed_%s.pdb' %targetpdbchainid)
        if targetpdbchainid=="": # target chain id not specified
            command = (self.superpose, posixpath.relpath(modelpdbfname),
            posixpath.relpath(targetpdbfname),
            posixpath.relpath(modelpdbfname[:-4] + '_superposed.pdb'))
        self.logger.log(logging.INFO, "Executing command:\n")
        self.logger.log(logging.INFO, subprocess.list2cmdline(command) + "\n")

        output = subprocess.list2cmdline(command) + "\n" \
          + subprocess.Popen(command, shell=self.osdepbool, stdout=subprocess.PIPE).communicate()[0]
        print("command= " + str(command))

        self.superposelogfname = self.calclabel + "superposition.log"
        soutput = open(self.superposelogfname,"w") # create a logfile for debugging
        soutput.write(output) # dump cmdline and superpose output into logfile
        soutput.close()
# Output from superpose looks like below. We extract euler angles and translation vector
#
# CCP4 format rotation-translation operator
# Polar angles (omega,phi,kappa) :    40.601   -90.131   178.534
# Euler angles (alpha,beta,gamma):   -91.096    81.193   -90.834
# Orthogonal translation (/Angst):    31.247    52.164     5.947
#
        #n = output.find("Euler angles (alpha,beta,gamma):",0)
        #m = output.find("\n",n)

        try:
            #match = re.search(r'(Euler angles \(alpha,beta,gamma\):)(\s*)([\w\W]*)', \
            #   output[n:m], re.MULTILINE)
            #print output[1400:2000]
            match = re.search(r"Rx\s+Ry\s+Rz\s+T\r*\n" + r"\s+(-?\d+\.\d+)"*12, output)
            strmatrix= []
            strmatrix.extend(match.groups()[0:3])
            strmatrix.extend(match.groups()[4:7])
            strmatrix.extend(match.groups()[8:11])
            strtrans = (match.groups()[3], match.groups()[7], match.groups()[11])
            rotmatrix = [float(s) for s in strmatrix]
            orthotrans = [float(s) for s in strtrans]
            eul = scitbx.math.euler_angles_zyz_angles(rotmatrix)
            eulerangles = [Roundoff(float(s),3) for s in eul]
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

            #stringnumbers = tuple(match.group(3).split())
            #eulerangles = [float(stringnumbers[0]), float(stringnumbers[1]), float(stringnumbers[2])]
            self.logger.log(logging.INFO,"model-target Euler angles are: %s\n" %str(eulerangles))

            #n = output.find("Orthogonal translation (/Angst):",0)
            #m = output.find("\n",n)
            #match = re.search(r'(Orthogonal translation \(/Angst\):)(\s*)([\w\W]*)', \
            #   output[n:m], re.MULTILINE)
            #stringnumbers = match.group(3).split()
# must have floats and not strings as args for uc.fractionalize()
            #orthotrans = [float(stringnumbers[0]), float(stringnumbers[1]), float(stringnumbers[2])]
            self.logger.log(logging.INFO,"model-target orthogonal translation is: %s\n"  %str(orthotrans))

        except Exception as m:
            self.logger.log(logging.ERROR,"Problem with superpose:\n%s\n" %output)
            eulerangles = [None, None, None]
            orthotrans = [None, None, None]
            self.errorcodesum[0] += 1
            #raise #BLASTrunnerException, m

        return (tuple(eulerangles), tuple(orthotrans))




    def TrimModel(self, targetpdbchainid, pdbs_seqid, residuenumbers):
        self.logger.log(logging.DEBUG, "TrimModel(%s, %s)\n" %(targetpdbchainid, pdbs_seqid[0]))
# first get the sequence from the target chain we already got
        targetseqfile = open(posixpath.join(self.topfolder,"Phaserinput",
          targetpdbchainid[:4]+"_Chain" + targetpdbchainid[5] + ".seq"),"r")
        targseq = targetseqfile.read()
        targetseqfile.close()

        try:
# then get sequence of the model chain
            if os.path.exists("clustinput.txt") == False:
                sequences = targseq + "\n" + self.PDB2SeqLocal(pdbs_seqid[0])[0] + "\n"
                fileout = open("clustinput.txt","w")
                fileout.write(sequences)
                fileout.close()
            else:
                self.logger.log(logging.INFO, "clustinput.txt already present\n")

            if os.path.exists("clust.aln") == False:
                command = (self.clustalw, '-INFILE=clustinput.txt' ,'-ALIGN',\
                 '-OUTFILE=clust.aln')
                self.logger.log(logging.INFO, "Executing command: %s\n" %subprocess.list2cmdline(command))
                subprocess.Popen(command,shell=self.osdepbool, stdout=subprocess.PIPE).communicate()[0]
            else:
                self.logger.log(logging.INFO, "clust.aln already present\n")

# get rid of ncs copies or other chains in the model, i.e. preserve only one chain
            modelfname = pdbs_seqid[0][:4] + ".pdb"
            sculptarg=  ["output.root = tmp" + self.calclabel, "renumber.use = original", \
             "input.model.file_name = %s" %modelfname, "input.alignment.file_name = clust.aln",
             "input.model.selection = chain '%s'" %pdbs_seqid[0][5],
             "macromolecule.protocols=%d" %int(self.sculptprotocol)]

            self.logger.log(logging.INFO, "Sculptor.run: %s\n" % subprocess.list2cmdline(sculptarg))
# create file handler which logs even debug messages
            fh = open(self.calclabel + "sculptor.log","w")
            from phaser import tbx_utils
            factory = tbx_utils.PhilArgumentFactory( master_phil = sculptor.PHIL_MULTIPROTOCOL_MASTER )
            sculptor.run( [ factory( arg ) for arg in sculptarg ], fh,
             setup = sculptor.SETUP_MULTIPROTOCOL,)
            #sculptor.run(sculptarg, fh)
# find first and last residue numbers for tagging on to the file name
            tmpfname = "tmp" + self.calclabel + "_" + pdbs_seqid[0][:4] + "_" + self.sculptprotocol + ".pdb"
            mypdb = openpdb(tmpfname)
            root = mypdb.construct_hierarchy()
            firstrsd = root.models()[0].chains()[0].residue_groups()[0].resseq_as_int()
            lastrsd = root.models()[0].chains()[0].residue_groups()[-1].resseq_as_int()
            residuenumbers[0] = ""
# To avoid ENSEMBLE name clashes later we note the number of times this model has been used for
# the rare but possible case when BLAST suggest a particular model more than once for our target.
# If used more than once then tag the number of times this model is occuring to the modelpdbid
# Don't do this if the model occurs only once as it makes file and folder names difficult to read
            sn = 0
            while (pdbs_seqid[0] + str(sn)) in self.pdbnameset:
                sn += 1
            if sn > 0:
                pdbs_seqid[0] = pdbs_seqid[0] + str(sn)

            residuenumbers[0] = "{%d-%d}" %(firstrsd,lastrsd)
# watch out if a model is suggested more than once
            self.pdbnameset.add(pdbs_seqid[0])
            #sculptfname = "sculpt%s_%s.pdb" %(self.sculptprotocol, pdbs_seqid[0])
            sculptfname = os.path.basename(self.GetSculptFname(targetpdbchainid, pdbs_seqid[0]))
            if os.path.exists(sculptfname):
                os.remove(sculptfname)

            os.rename(tmpfname, sculptfname)

        except Exception as m:
            #raise
            self.failure = True
            self.message = m
            self.logger.log(logging.ERROR, self.message)
            self.errorcodesum[0] += 1
            return ""

        else:
            return sculptfname




    def PreparePartialModel(self, pdbs_seqid):
# create partial modeldir and store sculpted model there
        self.logger.log(logging.INFO, "\nPreparePartialModel(%s)\n" % pdbs_seqid)
        targetpdbchainid = pdbs_seqid[2]

        modeldir = self.targetpdbid + "_" \
          + targetpdbchainid + posixpath.sep + pdbs_seqid[0] + posixpath.sep
        self.MakeMyfolder(modeldir)

        olddir = os.getcwd()
        os.chdir(modeldir)
# store the model and sculpt files in the modeldir folder
        modelfname = self.FetchPDBfile("." + posixpath.sep, pdbs_seqid[0])
        residuenumbers = [""]
        sculptedpdb = self.TrimModel(self.targetpdbid + "_" + targetpdbchainid[0],
         pdbs_seqid, residuenumbers)

        if not os.path.exists(sculptedpdb):
            self.logger.log(logging.WARN, "There's an issue with " + pdbs_seqid[0]
             + ". No MR calculation will be done with that model.\n")
            os.chdir(olddir)
            return False
# insert model completeness into pdbs_seqid list
# replace the BLAST sequence identity with the ClustalW2 as it is more "true"
        myalignment = open(os.path.join("clust.aln"),"r").read()
        clustparser = bioinformatics.clustal_alignment_parse
        clustaln = clustparser(myalignment)[0]
        pdbs_seqid[1][0] = "%4.3f" %(clustaln.identity_fraction()*100.0)

        sculptedpdb = RecaseFilename2(sculptedpdb)
        modelscattering, mstr = SimpleFileProperties.GetScatteringOfModelInPDBfile(sculptedpdb)

        targetpdbfname = os.path.abspath(self.topfolder) + os.path.sep + self.targetpdbid + ".pdb"
        self.logger.log(logging.INFO, mstr + "\n")
        modelcompleteness = Roundoff(modelscattering/self.targetscattering, 4)
        pdbs_seqid[1].append(modelcompleteness)
# store SSM seqid as well for comparison with clustalw seqid
        SSMSeqIDs = SimpleFileProperties.GetSSMSequenceIdentity(targetpdbfname,
         pdbs_seqid[0][:4] + ".pdb", targetpdbchainid[0], pdbs_seqid[0][5])
        #SSMSeqIDs = SimpleFileProperties.GetSSMSequenceIdentity(targetpdbfname, sculptedpdb, targetpdbchainid[0])
        pdbs_seqid[1].append(Roundoff(SSMSeqIDs[0][0], 4))

        clustalalignlen = clustaln.shortest_seqlen()
        pdbs_seqid[1].append(clustalalignlen)

        ssmalignlen = SSMSeqIDs[0][6]
        pdbs_seqid[1].append(int(ssmalignlen))

        euler_ortho = []
# account for NCS in the target structure by making same number of model superpositions
        samechains = len(targetpdbchainid)
        superposition = []
        ensemblename = "MR_" + pdbs_seqid[0]
        for i in range(0, samechains):
# do a superposition of model onto target for creating a template solution
            eulerorthos = self.GetEuler_and_Translation_of_Model_to_Target(sculptedpdb,
             targetpdbfname, targetpdbchainid[i])
# Did superpose fail?
            if eulerorthos[0] == [None, None, None]:
                self.logger.log(logging.WARN, "Superpose has an issue with " + pdbs_seqid[0]
                 + ". No MR calculation will be done with that model.\n")
                os.chdir(olddir)
                return False

            euler_ortho.append(eulerorthos)
            superposition.append((ensemblename, euler_ortho[i][0], euler_ortho[i][1]))

        pdbs_seqid.append(superposition)
        pdbs_seqid.append(residuenumbers[0])
        os.chdir(olddir)

        return True



    def GetSculptFname(self, targetchainid, modelpdbidchainid):
# locate the trimmed pdb model
        prefix = self.calclabel + "sculpt_"

        sculptedpdb = posixpath.join(self.topfolder, self.targetpdbid \
          + "_" + targetchainid, modelpdbidchainid[:6],
           prefix + modelpdbidchainid + "_singlechain.pdb")
        if os.path.exists(RecaseFilename2(sculptedpdb)) == False:
            sculptedpdb = posixpath.join(self.topfolder, self.targetpdbid \
              + "_" + targetchainid, modelpdbidchainid[:6], prefix + modelpdbidchainid + ".pdb")
# and try without calclabel for backwards compatibility
        prefix = "sculpt_"
        if os.path.exists(RecaseFilename2(sculptedpdb)) == False:
            sculptedpdb = posixpath.join(self.topfolder, self.targetpdbid \
              + "_" + targetchainid, modelpdbidchainid[:6],
               prefix + modelpdbidchainid + "_singlechain.pdb")
        if os.path.exists(RecaseFilename2(sculptedpdb)) == False:
            sculptedpdb = posixpath.join(self.topfolder, self.targetpdbid \
              + "_" + targetchainid, modelpdbidchainid[:6], prefix + modelpdbidchainid + ".pdb")

        return RecaseFilename2(sculptedpdb)



    def PrepareInputFiles(self, searchmodels):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        workdir = []
        for pdb_id in searchmodels:
            workdir.append( pdb_id[0] )

        modelsdir = ",".join( workdir )
        self.logger.log(logging.INFO, "Preparing MR AUTO for models %s\n" %modelsdir)

        mdlfolder = self.topfolder + posixpath.sep + "MR" + posixpath.sep + modelsdir
        self.MakeMyfolder(mdlfolder)

        templatestr = ""
        partstr = ""
        ensemblestr = ""
        searchstr = ""
        templmodels = []

        for i,search in enumerate(searchmodels):
            ensemblename = "MR_" + search[0]
# locate the trimmed pdb model
            sculptedpdb = self.GetSculptFname(search[2], search[0])
            templmodels.append(search[0])
            templates = []
# templatemodels are sorted so we can look them up
            sortedtemplmodels = sorted(templmodels)
            for pdb_id in sortedtemplmodels:
                templates.append(pdb_id)
            templatename = ",".join(templates)
            fulltemplatedir = posixpath.join(self.topfolder, "TemplateSolutions",
             templatename)

            if self.bOccRefine and self.bcontrivedtmpl and i < len(searchmodels[:-1]):
# if occupancy refinement then supply first component occupany refined in 2-component calculations
                sculptedpdb = posixpath.join(fulltemplatedir, self.calclabel +
                 "RNPCalcOCC_1.1.pdb")

            ensmbl = "ENSEMBLE " + ensemblename + " PDB " \
             + posixpath.relpath(sculptedpdb, self.topfolder) + "  IDENTITY " + search[1][0] + "\n"

            if i < len(searchmodels[:-1]):
                solfname = posixpath.join(fulltemplatedir, self.calclabel +
                 "RNPCalcCC_1.sol")
                fallbacks = glob.glob(os.path.join(fulltemplatedir, '*.superposed.sol'))
                sol = BLASTMRutils.TryParseSolfile([solfname] + fallbacks ).mr_sets
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                #print "sol= ", sol
                #print "sol[0].NEWVRMS.items()[0][1] = ", sol[0].NEWVRMS.items()[0][0]
                if sol != []:
                    if sol[0].NEWVRMS.items()[0][0] == ensemblename:
                        vrms = Roundoff(sol[0].NEWVRMS.items()[0][1][0], 4)
                        ensmbl = "ENSEMBLE " + ensemblename + " PDB " \
                         + posixpath.relpath(sculptedpdb, self.topfolder) + "  RMS " + str(vrms) + "\n"
                    else:
                        self.logger.log(logging.WARN, "No VRMS value present for %s in %s. Using seqid instead." %(ensemblename, solfname))

            ensemblestr += ensmbl

            if os.path.exists(sculptedpdb) == False:
                self.logger.log(logging.WARN, "Couldn't find %s " %sculptedpdb)
# find calculated template solutions by sorting order of pdbids and then adding combined
# solutions to each inputfile as in {pdb_a},{pdb_a,pdb_b},{pdb_a,pdb_b,pdb_c},...
        templmodels = []
        modelinputs = []
        for search in searchmodels:

# get number of domains to search for with this chain
            ensemblename = "MR_" + search[0]
            samechains = len(search[2])
            searchstr = "SEARCH ENSEMBLE %s NUM %d\n" %(ensemblename,
             samechains)

            templmodels.append(search[0])
            templates = []
# templatemodels are sorted so we can look them up
            sortedtemplmodels = sorted(templmodels)
            for pdb_id in sortedtemplmodels:
                templates.append(pdb_id)

            templatename = ",".join(templates)
            fulltemplatedir = posixpath.join(self.topfolder, "TemplateSolutions",
             templatename)

            templatefname = posixpath.join(fulltemplatedir, self.calclabel +
             "template{" + templatename + "}.txt")
            templatestr = ""
            if os.path.exists(templatefname)==False: # default to other template string if present
                templatefname = glob.glob(posixpath.join(fulltemplatedir,
                 "*template{" + templatename + "}.txt"))[0]
            if os.path.exists(templatefname):
                templatestr = open(templatefname,"r").read()
            else:
                self.logger.log(logging.WARN, "Couldn't find %s " %templatefname)
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                continue
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            self.logger.log(logging.INFO, "Using template solution: %s\n" %templatefname)
# using partial solutions to seed the next search. These are the previous or current
# template solutions excluding the last model we've added to the search
            partstr = ""
# is length of list of all elements but the last one greater than 0?
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            if len(templmodels[:-1]) > 0:
# find the solution file for the template solution excluding the one we've added to this search
# We need to sort template model order to retrieve correct file name
                if self.bcontrivedtmpl:
                    partstr = "SOLU SET\n"
                    for templmodel in templmodels[:-1]:
                        ensname = "MR_" + templmodel
                        if self.bOccRefine:
                            partstr += "SOLUTION ORIGIN ENSEMBLE " + ensname + "\n"
                        else:
                            coordstrs = re.findall("SOLU 6DIM ENSE.+" + ensname + "\s+.+",
                             templatestr, re.MULTILINE)
                            soluensstrs = re.findall("SOLU ENSE.+" + ensname + "\s+.+",
                             templatestr, re.MULTILINE)
                            partstr += coordstrs[0] + "\n" + soluensstrs[0] + "\n"
                else:
                    partsolname = ",".join(sorted(templmodels[:-1]))
                    fullpartsoldir = posixpath.join(self.topfolder, "TemplateSolutions",
                     partsolname)

                    partialfname = posixpath.join(fullpartsoldir, self.calclabel +
                     "template{" + partsolname + "}.txt")
                    if os.path.exists(partialfname)==False: # default to other template string if present
                        partialfname = glob.glob(posixpath.join(fullpartsoldir,
                         "*template{" + partsolname + "}.txt"))[0]
                    if os.path.exists(partialfname) == True:
                        tmpstr = open(partialfname,"r").read()
                        partstr = tmpstr.replace("SOLU TEMPLATE","SOLU SET")

                    else:
                        self.logger.log(logging.WARN, "Couldn't find %s " %partialfname)
                        continue

                    self.logger.log(logging.INFO, "Seeding MR search with partial solution: %s\n" %partialfname)

            modelsname = ",".join(templmodels)
            modestr = """
MODE MR_AUTO
SEARCH ORDER AUTO OFF
            \n"""
            modelfname = posixpath.join(mdlfolder,
             self.calclabel + "modelinput{" + modelsname + "}.txt")

            tmpstr = modestr + self.customkeysstr + ensemblestr \
               + searchstr + partstr + templatestr + "\n"

            modelinputfile = open(modelfname,"w")
            modelinputfile.write(tmpstr)
            modelinputfile.close()

            self.logger.log(logging.INFO, modelfname + " :\n" + tmpstr)

            modelinputs.append(modelsname)

        return modelinputs



    def erf(self, x):
# Error function for use below in GatherResults
        a1 =  0.254829592
        a2 = -0.284496736
        a3 =  1.421413741
        a4 = -1.453152027
        a5 =  1.061405429
        p  =  0.3275911
        # Save the sign of x
        sign = 1
        if x < 0:
            sign = -1
        x = abs(x)
        # A & S 7.1.26
        t = 1.0/(1.0 + p*x)
        y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)
        return sign*y



    def GatherResults(self, dbase, search, workdir, partmodels, nadditionalmodel, mrsol, suffix,
     xtriagecache, Unitcell, spacegroup, spacegroupnumber, crystalsystemname, ispolarspacegroup,
     nsolvedit, PDBTagsets, WilsonB, WilsonScale, Rfactors, EDSentry, MatthewsCoef,
     TargetScattering, ntargetresidues, ASUvolume):
        fbval = "NULL" # fallback value when llgs or other values aren't present
# note which is the last element in the database so UPDATE matches a corresponding INSERT statement
        #print "mrsol0=" +str(mrsol[0])

        def GetBfacFracvarVrms(sol, dres):
            vrms = []
            fracvarvrms = []
            fracvarhireslores = []
            avgsigmaavrms = []
            avgsigmaahireslores = []
            Bfac = []
            modelids = []
            if len(sol.NEWVRMS.items()) > 0:
            # assuming there is only one VRMS per model
                for m,v in enumerate(sol.NEWVRMS.items()):
                    vrms.append(v[1][0])
                    vrms_ensname = v[0]
                    if hasattr(sol.KNOWN[m],"BFAC"):
                        Bfac.append(sol.KNOWN[m].BFAC )
                    elif hasattr(sol.KNOWN[m], "getBfac"):
                        Bfac.append(sol.KNOWN[m].getBfac())

                    if hasattr(sol.KNOWN[m],"MODLID"):
                        modelids.append(sol.KNOWN[m].MODLID)
                    elif hasattr(sol.KNOWN[m], "getModlid"):
                        modelids.append(sol.KNOWN[m].getModlid())
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    # extract partial model name '1IRU_K0' from ensemble name 'MR_1IRU_K0'
                    partmodelname = vrms_ensname[3:]
                    # find target chainid corresponding to this partial model name in search[0]
                    ch = ""
                    partmodelcompleteness = ""
                    for s in search[0]:
                        if s[0] == partmodelname:
                            ch=s[2]
                            partmodelcompleteness = s[1][1]
                    fracvarvrms.append(AvgSigmaA.FracVar(v[1][0], dres, partmodelcompleteness))
                    avgsigmaavrms.append(AvgSigmaA.avgSigmaA(v[1][0], dres, partmodelcompleteness))
                    if not IsigIgrt3res:
                        fracvarhireslores.append( None)
                        avgsigmaahireslores.append( None)
                    else:
                        fracvarhireslores.append(AvgSigmaA.FracVarreshireslo(partmodelcompleteness,
                         v[1][0], IsigIgrt3res + 1.0, 5.0))
                        avgsigmaahireslores.append(AvgSigmaA.avgSigmaAreshireslo(partmodelcompleteness,
                         v[1][0], IsigIgrt3res + 1.0, 5.0))
            return Bfac, vrms, fracvarvrms, avgsigmaavrms, fracvarhireslores, avgsigmaahireslores, modelids

        def dict2listvalues(keyprefix, ccdict):
            tlst = []
            v = 0
            key = '%s%d' %(keyprefix, v)
            while key not in ccdict and v < 2:
                v += 1
                key = '%s%d' %(keyprefix, v)
            while key in ccdict:
                tlst.append(ccdict[key])
                v += 1
                key = '%s%d' %(keyprefix, v)
            return tlst

        def GetSSMrms(superposelog):
            rms = -1.0
            if os.path.exists(superposelog):
                slogfile = open(superposelog,"r")
                logtxt = slogfile.read()
                slogfile.close()
                m = logtxt.find(" at RMSD =   ",0)
                n = logtxt.find("\n",m)
    # logtxt[m:n] now holds a string like
    # " at RMSD =      3.255 and alignment length 3"
                match1 = re.search(r'(at\sRMSD\s=\s*)([0-9]*\.?[0-9]+)(.*)', logtxt[m:n],
                 re.MULTILINE) # old format
                m = logtxt.find("     r.m.s.d:",0)
                n = logtxt.find("\n",m)
    # logtxt[m:n] now holds a string like
    # "     r.m.s.d:  0.7582  (A)"
                match2 = re.search(r'\s+r\.m\.s\.d:\s+([0-9]*\.?[0-9]+)', logtxt[m:n],
                 re.MULTILINE) # new format
                try:
                    if match1 != None:
                        if len(match1.groups()) == 3:
                            rms = float(match1.group(2))
                    else:
                        if len(match2.groups()) <1:
                            rms = float(match2.group(1))
                except Exception as m:
                    self.tlogger.log(logging.WARN, "No SSM RSM in %s for %s " %(superposelog, searchmodel))
                    rms = -1.0
            return rms

        def GetNusedReflections(logfile):
            nrefl = fbval
            RMSLoweringHighRes = fbval
            try:
                match = self.NREFLCTrecmp.findall( logfile )
                if len(match):
                    intmatch = [int(e) for e in match ]
                    nrefl = min(intmatch)
                match = self.HighResLowRMSrecmp.findall( logfile )
                if len(match):
                    RMSLoweringHighRes = float(match[0])
            except Exception as m:
                self.failure = True
                self.message = m
                self.logger.log(logging.ERROR, str(m))
            return nrefl

        def GetModelResidueNumbers(smodel, chainids):
  # get number of residues in the model PDB
            pdbfname = self.GetSculptFname(chainids, smodel)
            pdbfname = os.path.relpath(pdbfname, os.path.join(self.topfolder))
            nmodelresidues = SimpleFileProperties.GetNumberofResiduesinPDBfile(pdbfname)
            return nmodelresidues


        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        #if mrsol[3] == None or mrsol[0] == None:
        #    return
  # partmodels is an element like '1IK6_A0,3EXF_A0' present in the list ['1IK6_A0', '1IK6_A0,3EXF_A0'] that is present in the list:
  # [[['1IK6_A0', ['42.398', '0.289'], 'B', '{1-326}'], ['3EXF_A0', ['24.607', '0.352'], 'A', '{0-361}']], ['1IK6_A0', '1IK6_A0,3EXF_A0']]
  # the current search model is the last item in partmodellist, i.e. '3EXF_A0'
  # corresponding seqid and model completeness is in ['3EXF_A0', ['24.607', '0.352'], 'A', '{0-361}']
  # Get that by using counts of commas in '1IK6_A0,3EXF_A0' as index for the list:
  # [['1IK6_A0', ['42.398', '0.289'], 'B', '{1-326}'], ['3EXF_A0', ['24.607', '0.352'], 'A', '{0-361}']]

        lastMRModel_id = dbase.GetNumberOfRows("MRModelTbl")+1
        lastAnnotation_id = dbase.GetNumberOfRows("AnnotationTbl")
        lastMRTargetTbl_id = 1
        nrtargetrows= dbase.GetNumberOfRows("MRTargetTbl")
        if nrtargetrows:
            lastMRTargetTbl_id = dbase.SQLexec('SELECT ROWID FROM MRTargetTbl WHERE TargetPDBid = "%s"' %self.targetpdbid )
            if lastMRTargetTbl_id:
                self.bMRTargetTblwritten = True
            else:
                lastMRTargetTbl_id = nrtargetrows + 1

        partmodellist = partmodels.split(",")
        i = len(partmodellist)-1
        searchmodel = partmodellist[i]
        if suffix == "":
            self.logger.log(logging.INFO,"Databasing results for search %s in %s. %d targets, %d models, %d solutions.\n"
             %(partmodels, workdir, lastMRTargetTbl_id, lastMRModel_id-1, lastAnnotation_id))
        else:
            self.logger.log(logging.INFO,"Databasing %s results for the search of %s in %s. %d targets, %d models, %d solutions.\n"
             %(suffix, partmodels, workdir, lastMRTargetTbl_id, lastMRModel_id-1, lastAnnotation_id))

        resolution = float(self.GetPDBResolution(searchmodel))
        seqid = float(search[0][i][1][0])
        modelcompleteness = float(search[0][i][1][1])
        SSMseqid = -1
        ClustalWlength = -1
        SSMlength = -1
        if len(search[0][i][1]) > 2:
            SSMseqid = float(search[0][i][1][2])
            ClustalWlength = int(search[0][i][1][3])
            SSMlength = int(search[0][i][1][4])

        targetchainids = search[0][i][2]
        targetchainid = targetchainids[0]
        clustfname = posixpath.join(self.topfolder, self.targetpdbid + "_" + targetchainids,
                 searchmodel[:6], "clust.aln")

        myalignment = open(RecaseFilename2(clustfname),"r").read()
        clustparser = bioinformatics.clustal_alignment_parse
        clustaln = clustparser(myalignment)[0]
        ClustalWlength = clustaln.shortest_seqlen()
# still need to calculate ssmseqid and ssmalignlen on the fly
        fullmodelcompleteness = 0.0
        for j in range(0,i+1):
            fullmodelcompleteness += float(search[0][j][1][1])

        nmodelresidues = GetModelResidueNumbers(searchmodel, targetchainids)


# get RMS value from SSM superpose.log file
        slog = posixpath.join(self.topfolder, self.targetpdbid + "_" + targetchainids,
         searchmodel, self.superposelogfname)
        SSMrms = GetSSMrms(slog)
# Chotia & Lesk seqid-rms formula
        C_L_rms = SimpleFileProperties.ChothiaLesk(seqid)
# new rms formula obtained from RMS v seqid scatter plots
        new_rms = SimpleFileProperties.NeweVRMS(seqid, nmodelresidues)
# use if we decide to predict rms values with our new fit rather than Chothia and Lesk
#        C_L_rms = new_rms
        scopfamnames = self.scopobj.GetFamilyNameFromPDB(searchmodel[:4], searchmodel[5])
        scopstr = "\t".join(scopfamnames[0])
# count the number of unique scopids this model has been assigned
        modelscopidset = set([])
        for scopid in scopfamnames:
            if scopid[0] != '':
                modelscopidset.add(scopid)

        nmodelscopid = len(modelscopidset)
        modelscopids = " | ".join([e[0] for e in modelscopidset])
        modelclassnames =" | ".join([e[1] for e in modelscopidset])
        modelfoldnames =" | ".join([e[2] for e in modelscopidset])
        modelsuperfamilynames =" | ".join([e[3] for e in modelscopidset])
        modelfamilynames =" | ".join([e[4] for e in modelscopidset])

        targetscopidnames = self.scopobj.GetFamilyNameFromPDB(self.targetpdbid, targetchainid)
# count the number of unique scopids the target has been assigned
        scopidset = set([])
        for scopid in targetscopidnames:
            if scopid[0] != '':
                scopidset.add(scopid)

        ntargetscopid = len(scopidset)
        targetscopids = " | ".join([e[0] for e in scopidset])
        targetclassnames =" | ".join([e[1] for e in scopidset])
        targetfoldnames =" | ".join([e[2] for e in scopidset])
        targetsuperfamilynames =" | ".join([e[3] for e in scopidset])
        targetfamilynames =" | ".join([e[4] for e in scopidset])

        (nrefl, IsigIgrt3res, hires, lowres, noutlier_outside_rescut, nicerings,
          twinLtest, NonOrigPattersonHeight, xtriageerror) = xtriagecache

# get the Wilson B-factor from logfile if we haven't got it already from previous logfile
        logfile = mrsol[8][0] if mrsol[8][0] != None else ""
        if WilsonScale[0] < 0.0: # i.e. it's the first time we pick up results for the target mtz file
            wsc = self.WILSON_Scalerecmp.findall(logfile)
            if len(wsc) > 0:
                WilsonScale[0] = float(wsc[0])

# get the MatthewsCoefficient from logfile if we haven't got it already from previous logfile
        if MatthewsCoef[0] < 0.0: # i.e. it's the first time we pick up results for the target mtz file
            mca = self.MATTHEWCOEFrecmp.findall(logfile)
            if len(mca) > 0:
                MatthewsCoef[0] = float(mca[0])

        if WilsonB[0] < 0.0: # i.e. it's the first time we pick up results for the target mtz file
            wba = self.WILSON_Brecmp.findall(logfile)
            if len(wba) > 0:
                WilsonB[0] = float(wba[0])
# note which is the last element in the database so UPDATE matches a corresponding INSERT statement
        if suffix == "":
            if not self.bMRTargetTblwritten:
                resdict = {}
                #resdict["p_id"] = ( 0, "integer primary key autoincrement")
                resdict["TargetPDBid"] = ( self.targetpdbid, "text")
                resdict["SCOPid"] = (targetscopids, "text")
                resdict["NumberofUniqueSCOPids"] = (ntargetscopid, "int")
                resdict["TargetClass"] = (targetclassnames, "text")
                resdict["TargetFold"] = (targetfoldnames, "text")
                resdict["TargetSuperfamily"] = (targetsuperfamilynames, "text")
                resdict["TargetFamily"] = (targetfamilynames, "text")
                resdict["Unitcell"] = (Unitcell, "text")
                resdict["Spacegroup"] = (spacegroup, "text")
                resdict["Spacegroupnumber"] = (spacegroupnumber, "int")
                resdict["IsPolarSpacegroup"] = (ispolarspacegroup, "int")
                resdict["Crystalsystem"] = (crystalsystemname, "text")
                resdict["TargetScattering"] = (TargetScattering, "real")
                resdict["NumberofResiduesinTarget"] = (ntargetresidues, "int")
                resdict["WilsonB"] = (WilsonB[0], "real")
                resdict["WilsonScale"] = ( WilsonScale[0], "real")
                resdict["MatthewsCoefficient"] = ( MatthewsCoef[0], "real")
                resdict["Allreflections"] = ( nrefl, "int")
                resdict["ASUvolume"] = ( Roundoff(ASUvolume,2), "real")
                resdict["Robs"] = ( Rfactors[0], "text")
                resdict["Rall"] = ( Rfactors[1], "text")
                resdict["Rwork"] = ( Rfactors[2], "text")
                resdict["Rfree"] = ( Rfactors[3], "text")
                resdict["EDSentry"] = ( EDSentry, "int")
                resdict["IsigIgrt3res"] = ( Roundoff(IsigIgrt3res,3), "real")
                resdict["hires"] = ( Roundoff(hires,3), "real")
                resdict["lowres"] = ( Roundoff(lowres,3), "real")
                resdict["outliers"] = ( noutlier_outside_rescut, "int")
                resdict["icerings"] = ( nicerings, "int")
                resdict["twinLtest"] = ( Roundoff(twinLtest,3), "real")
                resdict["NonOrigPattersonHeight"] = ( Roundoff(NonOrigPattersonHeight,3), "real")
                resdict["Error"] = ( self.errdict.get(self.targetpdbid, fbval), "text" )
                resdict["XtriageError"] = ( xtriageerror, "text" )
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                dbase.PutValuesinTable( resdict, "MRTargetTbl")

            self.bMRTargetTblwritten = True


        VRMSLLGmathstr = ""
        if mrsol[4]:
            VRMSLLGmathstr = "{%s, %s, " %(seqid, nmodelresidues)
            VRMSLLGmathstr += mrsol[4] + "}"
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        resdict = {}
        resdict["Fullmodel"] = ( workdir, "text")
        resdict["TargetPDBid"] = ( self.targetpdbid, "text")
        resdict["MRTarget_id"] = ( lastMRTargetTbl_id, "int")
        resdict["PartialModel"] = ( searchmodel, "text")
        resdict["RefinedModel"] = ( partmodels, "text")
        resdict["ModelOrder"] = ( nadditionalmodel+1, "int")
        resdict["ModelResolution"] = ( resolution, "real")
        resdict["SequenceIdentity"] = ( seqid, "real")
        resdict["SSMseqid"] = ( SSMseqid, "real")
        resdict["ModelCompleteness"] = ( modelcompleteness, "real")
        resdict["FullModelCompleteness"] = ( fullmodelcompleteness, "real")
        resdict["ClustalWlength"] = ( ClustalWlength, "int")
        resdict["SSMlength"] = ( SSMlength, "int")
        resdict["VRMSLLGmathstr"] = ( VRMSLLGmathstr, "text")
        dbase.PutValuesinTable( resdict, "MRModelTbl", {"ROWID":(lastMRModel_id, "int") } )

        mrsols = []
        bpartialsolution = 0
        bfullsolution = 0
        if mrsol[0]:
            solresolution = -1
            tfzeq = fbval
            cputime = fbval
            usertime = fbval
            if hasattr(mrsol[0], "get_dot_sol"):
                mrsols = mrsol[0].get_dot_sol()
            if hasattr(mrsol[0], "getDotSol"):
                mrsols = mrsol[0].getDotSol()
            if nsolvedit >= 0 and len(mrsols) > 0:
                if hasattr(mrsols, "resolution"):
                    solresolution = mrsols.resolution()
                elif len(self.RESOLUTIONrecmp.findall(logfile)):
                    solresolution = float(self.RESOLUTIONrecmp.findall(logfile)[0])

            tfzeq = mrsol[0].getTopTFZ() if hasattr(mrsol[0], "getTopTFZeq" ) else 0.0
            cputime = mrsol[0].cpu_time()
            usertime = mrsol[0].user_time()
            dres = hires
            if solresolution > 0:
                dres = solresolution
    # calculate fracvar(resolution, modelcompleteness, seqid)
            fracvar = AvgSigmaA.FracVar(C_L_rms, dres, modelcompleteness)
            occlogfile = mrsol[8][1] if mrsol[8][1] != None else ""
            dt= self.NrOccRefParamrecmp.findall(occlogfile)
            noccref = None
            if len(dt) > 0:
                noccref = int(dt[0][1])
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            avgsigmaa = AvgSigmaA.avgSigmaA(C_L_rms, dres, modelcompleteness)
            ELLGstuff = None
            bOCCrefined = len(self.OccupanciesRefinedrecmp.findall(logfile)) > 0 if logfile else 0
            bKilltime = len(self.KillTimeErrorrecmp.findall(logfile)) > 0 if logfile else 0
            bfullsolution = len(self.SorryNoSolutionrecmp.findall(logfile)) == 0 if logfile else 0
            bpartialsolution = len(self.OnlyPartialSolutionrecmp.findall(logfile)) > 0 if logfile else 0

            CorrCoefs = mrsol[3][4] #if mrsol[3][4] != None else [(fbval,fbval)]

            lastMRTargetTbl_id = dbase.GetNumberOfRows("MRTargetTbl")
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

            nreflct = fbval
            if mrsol[3]:
                nreflct = GetNusedReflections(mrsol[0].logfile())

            if suffix == "":
                resdict = {}
                #resdict["p_id"] = ( 0, "integer primary key autoincrement")
                resdict["Foundit"] = ( nsolvedit, "int")
                resdict["C_L_rms"] = ( Roundoff(C_L_rms,4), "real")
                resdict["SSMrms"] = ( Roundoff(SSMrms,4), "real")
                resdict["NewRMSD"] = ( Roundoff(new_rms,4), "real")
                resdict["Fracvar"] = ( Roundoff(fracvar,5), "real")
                resdict["AvgSigmaA"] = ( Roundoff(avgsigmaa,5), "real")
                resdict["CPUtime"] = ( cputime, "real")
                resdict["Usertime"] = ( usertime, "real")
                resdict["UsedReflections"] = ( nreflct, "int")
                resdict["UsedResolution"] = ( Roundoff(solresolution, 3), "real")
                resdict["RMSLoweringHighRes"] = ( Roundoff(RMSLoweringHighRes, 3), "real")
                resdict["NumberofResiduesinModel"] = ( nmodelresidues, "int")
                resdict["SolutionExists"] = ( int(bfullsolution), "int")
                resdict["PartialSolutionExists"] = ( int(bpartialsolution), "int")
                if self.neweLLGtarget:
                    resdict["ELLG_TARG"] = ( self.neweLLGtarget, "real")
                resdict["TriedOCCrefining"] = ( int(bOCCrefined), "int")
                resdict["Error"] = ( self.errdict.get(partmodels, fbval), "text" )
                resdict["Exitcode"] = ( int(bKilltime), "int")
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                resdict["NumberofSolutions"] = ( len(mrsols), "int")
                resdict["TFZeq"] = ( Roundoff(tfzeq,4), "real")
                match = self.RFZrecmp.findall(mrsols[0].ANNOTATION)
                rfz = float(match[0]) if match else fbval
                tfz = mrsols[0].TFZ
                llg = mrsols[0].LLG
                pak = mrsols[0].PAK
                resdict["RFZ"] = ( Roundoff(rfz,4) , "real")
                resdict["TFZ"] = ( Roundoff(tfz,4), "real")
                resdict["PAK"] = ( pak, "real")
                resdict["LLG"] = ( Roundoff(llg,4), "real")

    # label model according to which set of externally supplied PDB codes it belongs to (say NMR, EM)
            for tagset in PDBTagsets:
                belongstoset = 0
                if searchmodel[:4] in tagset[0]:
                    belongstoset = 1
                resdict[tagset[1]] = ( belongstoset, "int")
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            dbase.PutValuesinTable( resdict, "MRModelTbl", {"ROWID": (lastMRModel_id, "int") } )


            ncomponents = 0
            itemplllg = [None]
            tmpldefCCocclocals = mrsol[3][3]
            tmplCCocclocals = mrsol[3][7]
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            #print "templCorrCoef= " + str(templCorrCoef)

        if mrsol[1]:
            tmplsols = mrsol[1].getDotSol()
            templanno = tmplsols[0].ANNOTATION # get the annotation for template solution
            itemplllg = self.TEMPLLLGrecmp.findall(templanno)
            templCorrCoef = mrsol[3][6] #if mrsol[3][6] != None else [(fbval, fbval)]
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            if hasattr(mrsol[1], "resolution"):
                tmplsolresolution = mrsol[1].resolution()
            elif len(self.RESOLUTIONrecmp.findall(mrsol[1].logfile())):
                tmplsolresolution = float(self.RESOLUTIONrecmp.findall(mrsol[1].logfile())[0])
            tmplresdict = {}
# store VRMS for the template solution if present
# assuming there is only one VRMS per model
            if len(tmplsols[0].NEWVRMS.values()) > 0:
                if suffix == "":
                    tmplresdict["MRTarget_id"] = ( lastMRTargetTbl_id, "int")
                    tmplresdict["MRModel_id"] = ( lastMRModel_id, "int")
                    tmplresdict["RefinedModel"] = ( partmodels, "text")
                    try:
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        tmplBfac, tmplvrms, tmplSigA2, tmplSigA, tmplSigA2hilo, \
                         tmplSigAhilo, tmplmodlids = GetBfacFracvarVrms(tmplsols[0], tmplsolresolution)
                        for k in range(len(tmplvrms)):
                            for (k,p) in enumerate(partmodellist):
                                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                                if p == tmplmodlids[k][3:]:
                                    tmplresdict["VRMS%d" %k] = ( Roundoff(tmplvrms[k],4), "real")
                                    tmplresdict["Bfac%d" %k] = ( Roundoff(tmplBfac[k],3), "real")
                                    tmplresdict["FracvarVrms%d" %k] = ( Roundoff(tmplSigA2[k],5), "real")
                                    tmplresdict["AvgSigmaaVrms%d" %k] = ( Roundoff(tmplSigA[k],5), "real")
                                    tmplresdict["Fracvarhilores%d" %k] = ( Roundoff(tmplSigA2hilo[k],5), "real")
                                    tmplresdict["AvgSigmaahilores%d" %k] = ( Roundoff(tmplSigAhilo[k],5), "real")
                    except Exception as e:
                        print(str(e) + "\n" + traceback.format_exc())
                    #dbase.PutValuesinTable( tmplresdict, "TemplateRMSTbl" )
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    #dbase.PutValuesinTable( tmplresdict, "TemplateRMSTbl", {"MRModel_id": ( lastMRModel_id, "int") } )

                    if templCorrCoef != None:
                        tmplCCglobalmap = fbval
                        tmplCCfcglobalmap = fbval
                        tmplCCglobalpdb = fbval
                        tmplllgvrmsreal = fbval
                        tmplCCensmap = []
                        tmplCCfcensmap = []
                        tmplCCenspdb = []
                        tmplmladens = []
                        tmplllgens = []
                        tmplOCCfac = []
                        knowns = tmplsols[0].KNOWN
                        tmplLLGocc = []
                        tmplCCocc = []
                        tmplCCoccglobal = []
                        tmplCCoccglobalpdb = []
                        tmplELLGoccwnd = []
                        tmplNresoccwnd = []
                        tmpldefCCoccmap = None
                        tmpldefCCfcoccmap = None
                        tmpldefCCoccpdb = None
                        tmpldefELLGoccwnd = None
                        tmpldefLLGocc = None
                        tmpldefNresoccwnd = None

                        if type(templCorrCoef) == dict:
                            tmplCCensmap = dict2listvalues('CCwt_ensSol0_', templCorrCoef)
                            tmplCCfcensmap = dict2listvalues('CCfc_ensSol0_', templCorrCoef)
                            tmplCCenspdb = dict2listvalues('CCpdbSol0_', templCorrCoef)
                            tmplmladens = dict2listvalues('MLADensSol0_', templCorrCoef)
                            tmplllgens = dict2listvalues('LLGensSol0_', templCorrCoef)
                            tmplOCCfac = dict2listvalues('OfacensSol0_', templCorrCoef)
                            tmpldefCCgoodSigmaA = dict2listvalues('CCgoodSigmaASol0_', templCorrCoef)
                            tmplCCglobalmap = templCorrCoef.get('CCwt_globalSol0', fbval)
                            tmplCCfcglobalmap = templCorrCoef.get('CCfc_globalSol0', fbval)
                            tmplCCglobalpdb = templCorrCoef.get('CC_globalpdbSol0', fbval)
                            tmplllgvrmsreal = templCorrCoef.get('LLGallSol0', fbval)
                            tmpldefCCoccmap = templCorrCoef.get('CCwt_OCCSol0', fbval)
                            tmpldefCCfcoccmap = templCorrCoef.get('CCfc_OCCSol0', fbval)
                            tmpldefCCoccpdb = templCorrCoef.get('CCpdb_OCCSol0', fbval)

                            tmpldefELLGoccwnd = templCorrCoef.get('OCCWindowELLGSol0', fbval)
                            tmpldefNresoccwnd = templCorrCoef.get('OCCnresidSol0', fbval)
                            tmpldefLLGocc = templCorrCoef.get('OCCLLGSol0', fbval)
                            ELLGstuff =[[("", e) for e in dict2listvalues('ELLGens', templCorrCoef)], # mimic previous format of eLLGs in a python list
                                        [("", e) for e in dict2listvalues('vrmsELLGensSol0_', templCorrCoef)]]

                            sigmaas = dict2listvalues('SigmaAshellSol0_', templCorrCoef)
                            Resolushells = dict2listvalues('ResolushellSol0_', templCorrCoef)
                            CCshells = dict2listvalues('CCshellSol0_', templCorrCoef)
                            Nreflshells = dict2listvalues('NreflshellSol0_', templCorrCoef)
                            Avgresshells = dict2listvalues('AvgresshellSol0_', templCorrCoef)
                            Avginvres2 = dict2listvalues('Avginvres2Sol0_', templCorrCoef)
                            resdict = {}
                            for k,sigmaa in enumerate(sigmaas):
                                resdict["MRTarget_id"] = (lastMRTargetTbl_id, "int")
                                resdict["MRModel_id"] = (lastMRModel_id, "int")
                                resdict["Shell"] = (k, "int")
                                resdict["Loresshell"] = (Resolushells[k][0], "real")
                                resdict["Hiresshell"] = (Resolushells[k][1], "real")
                                resdict["CCshell"] = (CCshells[k], "real")
                                resdict["SigmaAshell"] = (sigmaa, "real")
                                resdict["Nreflshell"] = (Nreflshells[k], "int")
                                resdict["Avgresshell"] = (Avgresshells[k], "real")
                                resdict["Avginvres2"] = (Avginvres2[k], "real")
                                dbase.PutValuesinTable( resdict, "CCsigmaAtbl" )

                        tmplnreflct = GetNusedReflections(mrsol[1].logfile())
                        llgtemplreflct = float(tmplllgvrmsreal)/tmplnreflct if tmplnreflct and tmplllgvrmsreal and tmplllgvrmsreal != fbval else fbval
                        #tmplresdict = {}
                        tmplresdict["LLGvrms"] = ( Roundoff(tmplllgvrmsreal,3), "real")
                        tmplresdict["iLLGtemplate"] = ( itemplllg[0], "text")
                        tmplresdict["LLGreflvrms"] = ( Roundoff(llgtemplreflct,6), "real")
                        #dbase.PutValuesinTable( tmplresdict, "TemplateRMSTbl", {"MRModel_id": ( lastMRModel_id, "int") } )
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        #tmplresdict = {}
                        for k in range(len(tmplCCensmap)):
                            if len(tmplCCfcensmap) < 1+k:
                                tmplCCfcensmap.append("NULL")
                            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                            tmplresdict["CCglobal"] = ( tmplCCglobalmap, "real")
                            tmplresdict["CCfcglobal"] = ( tmplCCfcglobalmap, "real")
                            tmplresdict["CCmap%d" %k] = ( tmplCCensmap[k], "real")
                            tmplresdict["CCfcmap%d" %k] = ( tmplCCfcensmap[k], "real")
                            tmplresdict["CCpdb%d" %k] = ( tmplCCenspdb[k], "real")
                            tmplresdict["MLAD%d" %k] = ( tmplmladens[k], "real")
                            if len(tmpldefCCgoodSigmaA) < 1+k:
                                tmplresdict["CCgoodSigmaA%d" %k] = ( "NULL", "real")
                            else:
                                tmplresdict["CCgoodSigmaA%d" %k] = ( tmpldefCCgoodSigmaA[k], "real")
                            tmplresdict["LLGensmbl%d" %k] = ( tmplllgens[k], "real")
                            if len(tmplOCCfac) > 1+k:
                                tmplresdict["OCCfac%d" %k] = ( tmplOCCfac[k], "real")

                        if self.bOccRefine:
                            tmplresdict["CCoccglobal"] = ( tmpldefCCoccmap, "real")
                            tmplresdict["CCfcoccglobal"] = ( tmpldefCCfcoccmap, "real")
                            tmplresdict["CCoccglobalpdb"] = ( tmpldefCCoccpdb, "real")
                            tmplresdict["ELLGoccwnd"] = ( tmpldefELLGoccwnd, "real")
                            tmplresdict["LLGocc"] = ( tmpldefLLGocc, "real")
                            tmplresdict["Nresoccwnd"] = ( tmpldefNresoccwnd, "int")
                            tmplresdict["CCocclocals"] = ( tmpldefCCocclocals, "text")

                            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                            for n in range(len(tmplLLGocc)): # loop over OCC residue window sizes
                                nres = tmplNresoccwnd[n]
                                tmplresdict["CCoccglobal%d" %nres] = ( tmplCCoccglobal[n], "real")
                                tmplresdict["CCoccglobalpdb%d"%nres] = ( tmplCCoccglobalpdb[n], "real")
                                tmplresdict["ELLGoccwnd%d"%nres] = ( tmplELLGoccwnd[n], "real")
                                tmplresdict[" LLGocc%d"%nres] = ( tmplLLGocc[n], "real")
                                tmplresdict["Nresoccwnd%d"%nres] = ( tmplNresoccwnd[n], "real")
                                tmplresdict["CCocclocals%d"%nres] = ( tmplCCocclocals[n], "text")

                            #if self.bOccAIC==False:
                            #    sqlexp = "UPDATE TemplateRMSTbl SET CCoccglobalNonAIC=?, CCoccglobalpdbNonAIC=?, \
                            #      ELLGoccwndNonAIC=?, LLGoccNonAIC=?, NresoccwndNonAIC=? WHERE ROWID=?"
            #dbase.PutValuesinTable( tmplresdict, "TemplateRMSTbl", {"MRModel_id": ( lastMRModel_id, "int") } )
            dbase.PutValuesinTable( tmplresdict, "TemplateRMSTbl" )
# get LLG values from separate text files with higher precision than in the annotation strings
        #LLGreals = mrsol[3][0]
        CCocclocals = mrsol[3][1] if type(mrsol[3][1])==str else fbval
# populate ELLGtables if it is one of those 500 calculations
        if mrsol[9] != []:
            for k, line in enumerate(mrsol[9]):
                if k==0 or len(line) < 9:
                    continue
                foundit = eval(line[6])[0] if len(eval(line[6])) > 0 else 0
                if suffix == "":
                    resdict = {}
                    resdict["MRModel_id"] = ( lastMRModel_id, "real")
                    resdict["eLLGtarget"] = ( float(line[0]), "real")
                    resdict["achievedELLG"] = ( float(line[1]), "real")
                    resdict["LLGachievedres"] = ( float(line[2]), "real")
                    resdict["LLGfullres"] = ( float(line[3]), "real")
                    resdict["TFZfullres"] = ( float(line[4]), "real")
                    resdict["TFZachievedres"] = ( float(line[5]), "real")
                    resdict["foundit"] = ( foundit, "int")
                    resdict["vrmsachievedres"] = ( float(line[7]), "real")
                    resdict["achievedresol"] = ( float(line[8]), "real")
                    resdict["foundit2"] = ( line[9], "int")
                    dbase.PutValuesinTable( resdict, "ELLGtbl")
        llgs = []
        maxllgperreflct = -1000 # unlikely to be retained when comparing against llgperreflct
        maxllgreal = -1000
#        if nsolvedit >= 0:
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        ncomponents = len(search[0][i][2])
# if only partially found then count how many components phaser found
        foundcomponents = 0
        if len(mrsols) > 0:
            for component in mrsols[0].KNOWN:
                if component.MODLID.find(searchmodel) > 0:
                    foundcomponents +=1

        if ncomponents != foundcomponents:
            self.logger.log(logging.WARN, "\nSolution file has %d components of %s but %d is expected." \
              %(foundcomponents, searchmodel, ncomponents))

        rfzs = []
        tfzs = []
        llgs = []
        llgfullress = []
        paks = []
        tfzequiv = []
        llgmax = fbval
        tfzmax = fbval

        for (j, sol) in enumerate(mrsols):
            if j > self.nmaxsols-1:
                break
            resdict = {}
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            match = self.RFZrecmp.findall(sol.ANNOTATION)
            rfz = float(match[0]) if match else fbval
            tfz = sol.TFZ
            llg = sol.LLG
            pak = sol.PAK
            annotation = sol.ANNOTATION

            llgvrmsreal = None
            CCglobalmap = fbval
            CCfcglobalmap = fbval
            CCglobalpdb = fbval
            CCensmap = []
            CCfcensmap = []
            CCenspdb = []
            mladens = []
            llgsens = []
            OCCfac = []
            LLGocc = []
            CCocc = []
            CCoccglobal = []
            CCoccglobalpdb = []
            ELLGoccwnd = []
            Nresoccwnd = []
            defCCoccmap = None
            defCCfcoccmap = None
            defCCoccpdb = None
            defELLGoccwnd = None
            defLLGocc = None
            defoccNreswnd = None
            defNresoccwnd = None
            if type(CorrCoefs) == list and len(CorrCoefs) > j and len(CorrCoefs[j]) > 0:
                CCrnp = CorrCoefs[j][0][:5] # first 6 elements are CC values of plain RNP
                CCglobalmap = CCrnp[0]
                CCglobalpdb = CCrnp[1]
                llgvrmsreal = CCrnp[2]
                for v in CCrnp[3:]: # last items are CCs for each compopnent
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    CCensmap.append(v[0])
                    CCenspdb.append(v[1])
                    mladens.append(v[2])
                    llgsens.append(v[3])
                    if len(v) > 4:
                        OCCfac.append(v[4])
                    else:
                        OCCfac.append(None)

                if self.bOccRefine and len(CorrCoefs[j]) == 2:
                    CCoccs = CorrCoefs[j][1] # next elements are from the OCC refinements
                    CCocc = CCoccs[0] # first element is using default residue window size
                    defCCoccmap = CCocc[0] # CCoccglobal in sql table
                    defCCoccpdb = CCocc[1] # CCoccglobalpdb
                    defELLGoccwnd = CCocc[2] # ELLGoccwnd
                    defLLGocc = CCocc[3] # LLGocc
                    defNresoccwnd = CCocc[4] #Nresoccwnd
                    for i, nres in enumerate(self.OCCresidwindows):
                        CCocc = CCoccs[1:][i] # remaining tuples are for the specific residue window size
                        CCoccglobal.append(CCocc[0]) # CCoccglobal%d in sql table
                        CCoccglobalpdb.append(CCocc[1]) # CCoccglobalpdb%d
                        ELLGoccwnd.append(CCocc[2]) # ELLGoccwnd%d
                        LLGocc.append( CCocc[3]) # LLGocc%d
                        Nresoccwnd.append(CCocc[4]) # Nresoccwnd%d
                    if self.bOccAIC==False:
                        # last tuple is for the default residue window size without using AIC
                        CCocc = CCoccs[-1]
                        CCoccglobalNonAIC = CCocc[0] # CCoccglobal%d in sql table
                        CCoccglobalpdbNonAIC = CCocc[1] # CCoccglobalpdb%d
                        ELLGoccwndNonAIC = CCocc[2] # ELLGoccwnd%d
                        LLGoccNonAIC = CCocc[3] # LLGocc%d
                        NresoccwndNonAIC = CCocc[4] # Nresoccwnd%d
                    if len(CorrCoefs) > 0:
                        ELLGstuff = CorrCoefs[1]
            if type(CorrCoefs) == dict:
                CCensmap = dict2listvalues('CCwt_ensSol%d_'%j, CorrCoefs)
                CCfcensmap = dict2listvalues('CCfc_ensSol%d_'%j, CorrCoefs)
                CCenspdb = dict2listvalues('CCpdbSol%d_'%j, CorrCoefs)
                mladens = dict2listvalues('MLADensSol%d_'%j, CorrCoefs)
                llgsens = dict2listvalues('LLGensSol%d_'%j, CorrCoefs)
                OCCfac = dict2listvalues('OfacensSol%d_'%j, CorrCoefs)
                CCgoodSigmaA = dict2listvalues('CCgoodSigmaASol%d_'%j, CorrCoefs)
                CCglobalmap = CorrCoefs.get('CCwt_globalSol%d'%j, fbval)
                CCfcglobalmap = CorrCoefs.get('CCfc_globalSol%d'%j, fbval)
                CCglobalpdb = CorrCoefs.get('CC_globalpdbSol%d'%j, fbval)
                llgvrmsreal = CorrCoefs.get('LLGallSol%d'%j, fbval)
                defCCoccmap = CorrCoefs.get('CCwt_OCCSol%d'%j, fbval)
                defCCfcoccmap = CorrCoefs.get('CCfc_OCCSol%d'%j, fbval)
                defCCoccpdb = CorrCoefs.get('CCpdb_OCCSol%d'%j, fbval)
                defELLGoccwnd = CorrCoefs.get('OCCWindowELLGSol%d'%j, fbval)
                defNresoccwnd = CorrCoefs.get('OCCnresidSol%d'%j, fbval)
                defLLGocc = CorrCoefs.get('OCCLLGSol%d'%j, fbval)
                ELLGstuff =[[("", e) for e in dict2listvalues('ELLGens', CorrCoefs)], # mimic previous format of eLLGs in a python list
                            [("", e) for e in dict2listvalues('vrmsELLGensSol%d_'%j, CorrCoefs)]]
            #CCfullres = CorrCoeffullres[j] if len(CorrCoeffullres) > j else (fbval, fbval)
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            #llgmax = float(llg[0]) # LLG corresponding to TFZequiv for MR without vrms refinement
            #tfzmax = float(tfz[0]) # TFZ -----

            rfz1 = rfz #float(rfz[0]) if len(rfz) > 0 else fbval
            rfzs.append(rfz1)
            tfz1 = tfz #float(tfz[0]) if len(tfz) > 0 else fbval
            tfzs.append(tfz1)
            pak1 = pak #float(pak[0]) if len(pak) > 0 else fbval
            paks.append(pak1)
# assuming the highest llg corresponds to the llg for the solution emitted by phaser
# get it now so we can put it into MRModelTbl
            #maxllgperreflct = max(llgperreflvrms, maxllgperreflct)
            #maxllgreal = max(llgvrmsreal, maxllgreal)
            llgperreflvrms = fbval
            if nreflct:
              llgperreflvrms = llg/nreflct

            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
# and compute fracvarvrms for the full resolution
            vrmsfullres = fbval
            fracvarvrmsfullres = fbval
            if mrsol[7] and len(mrsol[7]) > j and len(mrsol[7][j].NEWVRMS.values()) > 0:
                Bfacfullres, vrmsfullres, fracvarvrmsfullres, avgsigmaavrmsfullres, \
                  dummy1, dummy2, modelids = GetBfacFracvarVrms(mrsol[7][j])
# insert annotations for individual solutions into the second table of the database
            if suffix == "":
                resdict = {}
                resdict["MRTarget_id"] = ( lastMRTargetTbl_id, "int")
                resdict["MRModel_id"] = ( lastMRModel_id, "int")
                resdict["TargetPDBid"] = ( self.targetpdbid, "text")
                resdict["Fullmodel"] = ( workdir, "text")
                resdict["RFZ"] = ( Roundoff(rfz,4) , "real")
                resdict["TFZ"] = ( Roundoff(tfz,4), "real")
                resdict["PAK"] = ( pak, "real")
                resdict["ANNOTATION"] = ( annotation, "text")
                resdict["CCglobal"] = ( Roundoff(CCglobalmap,5), "real")
                resdict["CCfcglobal"] = ( Roundoff(CCfcglobalmap,5), "real")
                resdict["CCglobalpdb"] = ( Roundoff(CCglobalpdb,5), "real")
                resdict["SolutionRank"] = ( (j+1), "int")
                resdict["LLG"] = ( Roundoff(llg,4), "real")
                #resdict["LLGvrms"] = ( Roundoff(llgvrmsreal,3), "real")
                resdict["LLGrefl_vrms"] = ( Roundoff(llgperreflvrms,6), "real")
                #resdict["VRMSfullres"] = ( Roundoff(vrmsfullres,4), "real")
                #resdict["FracvarVrmsfullres"] = ( Roundoff(fracvarvrmsfullres,5), "real")

                #lastAnnotation_id = self.mydb.fetchone()[0]
                lastAnnotation_id = dbase.GetNumberOfRows("AnnotationTbl")
                for k in range(len(CCensmap)):
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    #sqlexp = "UPDATE AnnotationTbl SET CCmap%d=?, CCfcmap%d=?, CCpdb%d=?, MLAD%d=?, \
                    # LLGensmbl%d=?, CCgoodSigmaA%d=?, OCCfac%d=? WHERE ROWID=?" %(k,k,k,k,k,k,k)

                    resdict["CCmap%d" %k] = ( CCensmap[k], "real")
                    if len(CCfcensmap) > k:
                        resdict["CCfcmap%d" %k] = ( CCfcensmap[k], "real")
                    resdict["CCpdb%d" %k] = ( CCenspdb[k], "real")
                    resdict["MLAD%d" %k] = ( mladens[k], "real")
                    resdict["LLGensmbl%d" %k] = ( llgsens[k], "real")
                    if len(CCgoodSigmaA) > k:
                        resdict["CCgoodSigmaA%d" %k] = ( CCgoodSigmaA[k], "real")
                    if len(OCCfac) > k:
                        resdict["OCCfac%d" %k] = ( OCCfac[k], "real")

                if self.bOccRefine:
                    defCCocclocals = mrsol[3][5] if mrsol[3][5] != None else [(fbval,fbval)]
                    sqlexp = "UPDATE AnnotationTbl SET CCoccglobal=?, CCfcoccglobal=?, CCoccglobalpdb=?, \
                      ELLGoccwnd=?, LLGocc=?, Nresoccwnd=?, CCocclocals=? WHERE ROWID=?"
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    resdict["CCoccglobal"] = ( defCCoccmap, "real")
                    resdict["CCfcoccglobal"] = ( defCCfcoccmap, "real")
                    resdict["CCoccglobalpdb"] = ( defCCoccpdb, "real")
                    resdict["ELLGoccwnd"] = ( defELLGoccwnd, "real")
                    resdict["LLGocc"] = ( defLLGocc, "real")
                    resdict["Nresoccwnd"] = ( defNresoccwnd, "int")
                    resdict["CCocclocals"] = ( defCCocclocals[j], "text")

                    for n in range(len(LLGocc)): # loop over OCC residue window sizes
                        nres = Nresoccwnd[n]
                        sqlexp = "UPDATE AnnotationTbl SET CCoccglobal%d=?, CCoccglobalpdb%d=?, \
                          ELLGoccwnd%d=?, LLGocc%d=?, Nresoccwnd%d=?, CCocclocals%d=? WHERE ROWID=?" \
                           %(nres,nres,nres,nres,nres,nres)
                        resdict["CCoccglobal%d" %n] = ( CCoccglobal[n], "real")
                        resdict["CCoccglobalpdb%d" %n] = ( CCoccglobalpdb[n], "real")
                        resdict["ELLGoccwnd%d" %n] = ( ELLGoccwnd[n], "real")
                        resdict["LLGocc%d" %n] = ( LLGocc[n], "real")
                        resdict["Nresoccwnd%d" %n] = ( Nresoccwnd[n], "int")
                        resdict["CCocclocals%d" %n] = ( CCocclocals[j][n], "text")
                    if self.bOccAIC==False:
                        sqlexp = "UPDATE AnnotationTbl SET CCoccglobalNonAIC=?, CCoccglobalpdbNonAIC=?, \
                          ELLGoccwndNonAIC=?, LLGoccNonAIC=?, NresoccwndNonAIC=? WHERE ROWID=?"
                        resdict["CCoccglobalNonAIC"] = ( CCoccglobalNonAIC, "real")
                        resdict["CCoccglobalpdbNonAIC"] = ( CCoccglobalpdbNonAIC, "real")
                        resdict["ELLGoccwndNonAIC"] = ( ELLGoccwndNonAIC, "real")
                        resdict["LLGoccNonAIC"] = ( LLGoccNonAIC, "real")
                        resdict["NresoccwndNonAIC"] = ( NresoccwndNonAIC, "int")
                try:
                    Bfac, vrms, SigA2, SigA, SigA2hilo, SigAhilo, modlids = GetBfacFracvarVrms(mrsols[j])
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    for k in range(len(vrms)):
                        sqlexp = "UPDATE AnnotationTbl SET vrms%d=?, Bfac%d=?, \
                          FracvarVrms%d=?, AvgSigmaaVrms%d=?, Fracvarhilores%d=?, \
                          AvgSigmaahilores%d=? WHERE ROWID=?" %(k,k,k,k,k,k)
                        resdict["vrms%d" %k] = ( Roundoff(vrms[k],4), "real")
                        resdict["Bfac%d" %k] = ( Roundoff(Bfac[k],3), "real")
                        resdict["FracvarVrms%d" %k] = ( Roundoff(SigA2[k],5), "real")
                        resdict["AvgSigmaaVrms%d" %k] = ( Roundoff(SigA[k],5), "real")
                        resdict["Fracvarhilores%d" %k] = ( Roundoff(SigA2hilo[k],5), "real")
                        resdict["AvgSigmaahilores%d" %k] = ( Roundoff(SigAhilo[k],5), "real")
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                except Exception as e:
                    print(str(e) + "\n" + traceback.format_exc())
                dbase.PutValuesinTable( resdict, "AnnotationTbl" )
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
# the very last element are tuples of ensemblename and eLLG of each component plus the residue window used for OCC
        resdict = {}
        if ELLGstuff:
            for k,ensellg in enumerate(ELLGstuff[0]):
                resdict["ELLG%d" %k] = ( ensellg[1], "real")
            if not bpartialsolution and len(ELLGstuff)>1:
                for k,ensellg in enumerate(ELLGstuff[1]):
                    resdict["VELLG%d" %k] = ( ensellg[1], "real")
            dbase.PutValuesinTable( resdict, "MRModelTbl", {"ROWID":(lastMRModel_id, "int") } )



    def multiply(self, lists):
# cartesian product of a list of bins
        if not lists:
            return []
        product=zip(lists[0])
        for l in lists[1:]:
            prods=[]
            for p in product:
                prods.extend([p+(tmp,) for tmp in l])
            product = prods
        return product




    def PutPDBsInChainIdBins(self, pdbs_seqids):
# Get list of unique chain IDs from the pdbs_seqids list.
# pdbs_seqids should already be sorted according to chain IDs
        chainIDs = []
        for i in range(0,len(pdbs_seqids)):
            if len(chainIDs) == 0:
                chainIDs.append(pdbs_seqids[i][2])
            else:
                if pdbs_seqids[i][2] != chainIDs[len(chainIDs)-1]:
                    chainIDs.append(pdbs_seqids[i][2])

        if self.useuniquesequence:
            self.logger.log(logging.INFO,"pdb_chainid models refers to %d unique chains"
                % len(chainIDs))

# make bins of chain labels where each bin holds an array of models for that chain
        models_chains = []
        for i in range(0,len(chainIDs)):#first label each sub array with the chain
            models_chains.append((chainIDs[i],[]))
# sort pdbs_seqids into those bins
        for i in range(0,len(pdbs_seqids)):
            for j in range(0,len(models_chains)):
                if models_chains[j][0] == pdbs_seqids[i][2]:
                    models_chains[j][1].append(pdbs_seqids[i])

#invert indexing and ignore the very first element which is just the chainID
        bins = [models_chains[i][1] for i in range(0,len(models_chains))]

        for i in range(0,len(bins)):
            self.logger.log(logging.INFO,"There are %d models for chain %s"
             %(len(bins[i]), models_chains[i][0]))

        return bins



    def GetCombinedSearchesforTargetChains(self, pdbs_seqids):
        bins = self.PutPDBsInChainIdBins(pdbs_seqids)

        myseq = self.PDB2SeqLocal(self.targetpdbid)
        if len(bins) < self.nuniqueseq:
            self.logger.log(logging.WARN, "Models were not found for all chains in the target structure!")

        self.logger.log(logging.INFO,"pdb_chainid,\tClustalW2 seq identity,\tCompleteness of model\t target pdb chainid(s)")
        for i in range(0, len(pdbs_seqids)):
            self.logger.log(logging.INFO,"%s \t\t %s \t\t %s \t\t %s"
             %(pdbs_seqids[i][0], pdbs_seqids[i][1][0], pdbs_seqids[i][1][1], pdbs_seqids[i][2]))
# do cartesian product of the bins with one another to get all combinations of models
        searches = self.multiply(bins)

        #mfile = open("testlist.txt","w")
        #mfile.write(str(searches))
        #mfile.close()

        uniquesearchdict = {}
        #for e in searches:
        #    print e
        for search in searches:
            residuenumbers = ""
            for pdbseqid in search:
                residuenumbers += pdbseqid[4]

            uniquesearchdict[residuenumbers] = search

        uniquesearches = []
        for e in uniquesearchdict:
            uniquesearches.append( uniquesearchdict[e] )
        #for e in uniquesearches:
        #    print e
# A sorted list of pdbs in the searches is helpful for identifying other searches
# that only differs by permuations from this one
        sortedsearches = []
        for search in uniquesearches:
            sortedsearches.append( sorted(search) )

        self.logger.log(logging.INFO,"Combining those models yields the following %s MR calculations:"
         %len(sortedsearches))
# pretty print as to only display the model ids
        for i in range(0,len(sortedsearches)):
            mrcalcstr = sortedsearches[i][0][0]
            for j in range(1,len(sortedsearches[i])):
                mrcalcstr = mrcalcstr + ", " + sortedsearches[i][j][0]

            self.logger.log(logging.INFO,mrcalcstr)

        return sortedsearches




    def PermuteSearches(self, search):
# make all possible permutations of search orders with these models
        self.logger.log(logging.INFO,"\nPermuting search orders of models:")
# pretty print search models as concatenated PDB codes separated by commas
        modelsdir = []
        for pdb_id in search:
            modelsdir.append( pdb_id[0])

        self.logger.log(logging.INFO, ",".join( modelsdir ) )
# now permute the list
        lst = list( itertools.permutations( search ) )
        permutations = []
        for m in lst:
            permutations.append(copy.deepcopy(list(m)))

        self.logger.log(logging.INFO,"yields the following search orders:")
        for line in permutations:
# pretty print permuted search order as concatenated PDB codes separated by commas
            modelsdir = []
            for pdb_id in line:
                modelsdir.append( pdb_id[0])

            self.logger.log(logging.INFO, ",".join( modelsdir ) )
        """
return a list of permutations like
  search: 1I1A_D,1NFD_E,1W72_H
  yields the following permutations:
  1I1A_D,1NFD_E,1W72_H
  1I1A_D,1W72_H,1NFD_E
  1NFD_E,1I1A_D,1W72_H
  1NFD_E,1W72_H,1I1A_D
  1W72_H,1I1A_D,1NFD_E
  1W72_H,1NFD_E,1I1A_D
        """
        return permutations



    def GetXtriageCachedResults(self):
        fname = posixpath.join(self.topfolder, "Phaserinput", "xtriagecache.pkl")
        import pickle
        if not os.path.exists(fname):
            mtzfname = glob.glob(posixpath.join(self.topfolder, "Phaserinput", "*.mtz"))[0]
            xtriagecache = SimpleFileProperties.GetXtriageResults(mtzfname)
            # only save results if no error using xtriage
            (nrefl, IsigIgrt3res, hires, lowres, noutlier_outside_rescut, nicerings,
              twinLtest, NonOrigPattersonHeight, xtriageerror) = xtriagecache
            if xtriageerror == "NULL":
                with open(fname, "wb") as f:
                    pickle.dump(xtriagecache, f)
        else:
            with open(fname, "rb") as f:
                xtriagecache = pickle.load(f)
        return xtriagecache



    def GetMtzColumns(self, fname, Fcol, sigFcol, Icol, sigIcol):
        fobsisOK = False
        try:
            obj = iotbx.mtz.object(file_name = fname)
            columns = obj.column_labels()

            if self.buseIntensities:
                for i in range(0,len(columns)):
                    columntype = obj.get_column(columns[i]).type()
                    if columntype=='J':
                        Icol[0] = columns[i]

                for i in range(0,len(columns)):
                    columntype = obj.get_column(columns[i]).type()
                    if columntype=='Q' and columns[i].find(Icol[0]) > 1:
                        sigIcol[0] = columns[i]
            else:
                for i in range(0,len(columns)):
                    columntype = obj.get_column(columns[i]).type()
    #                if Fcol[0] != '' and Sigcol[0] != '':
    #                    break # found the first columns of types amplitude and sigma so use these
    #                else:
                    if columntype=='F':
                        Fcol[0] = columns[i]
                        if min(obj.get_column(columns[i]).extract_values()) > 0.0:
                            fobsisOK = True

                for i in range(0,len(columns)):
                    columntype = obj.get_column(columns[i]).type()
                    if columntype=='Q' and columns[i].find(Fcol[0]) > 1:
                        sigFcol[0] = columns[i]

        except Exception as m:
            self.failure = True
            self.message = m
            self.logger.log(logging.ERROR, self.message)
            self.errorcodesum[0] += 50
            #raise BLASTrunnerException, m

        return fobsisOK



    def MakeModelDictionary(self, pdbs_seqids):
        for models in pdbs_seqids:
            for model in models[0]:
                self.MRDict["MR_" + model[0]+"_seqID"] = float(model[1][0])
                self.MRDict["MR_" + model[0]+"_modelcompleteness"] = float(model[1][1])
                self.MRDict["MR_" + model[0] + "_targetchain"] = model[2]
                if len(model) > 3:
                  self.MRDict["MR_" + model[0] + "_targetresidues"] = model[3]



    def RunMRauto(self, psearches, njobs, minmodelorder):
        self.zipfname = ""
        if len(psearches) > 0:
            ndistinctchains = len(psearches[0][1:][0])
            self.logger.log(logging.INFO, "Will now do at most " + str(ndistinctchains* len(psearches))
              + " phaser MR calculations in %d threads\n" % njobs)
        else:
            return
        os.chdir(self.topfolder)
# Spread evenly searches with low seq ids (the lengthy jobs) onto each thread.
# As the searches list begins with models having lowest seq id do this by dishing out
# searches to threads like cards to game players.
        shortlists=[]
        for i in range(0,njobs):
            shortlists.append([])
# distribute searches as evenly as possible into the shortlists
        for i in range(0, len(psearches)):
           shortlists[i%njobs].append(psearches[i])
        mylock = threading.Lock()
        for h in self.logger.handlers:
            h.createLock()
        phaserjobs = []
        for ( index, shortlist ) in enumerate( shortlists ):
            self.logger.log(logging.INFO,"\n%d. thread runs MR jobs with models "\
            "based on the following PDB codes:\n" %(index+1))
            for line in shortlist:
# create modelsdir as concatenated PDB codes separated by commas
                modelsdir = [ [e[0] for e in  e1]  for e1 in line[:-1]][0]
                self.logger.log(logging.INFO, ",".join( modelsdir ) + "\n" )

            phaserjobs.append(BLASTphaserthread.PhaserRunner(
              baseinputfname = self.baseinputfname,
              modelinputfnameprefix = self.calclabel + "modelinput",
              rootstr = "",
              parent = self,
              searches = shortlist,
              mylock = mylock,
              threadnumber = index+1,
              maxthreads = njobs,
              minmodelorder = minmodelorder
              )
            )

        self.logger.log(logging.INFO,
         "\nTo abort the jobs create an empty file named StopThreadsGracefully in %s\n"
          %self.topfolder)

        for i in range(0,njobs):
            phaserjobs[i].start()

        for i in range(0,njobs):
            phaserjobs[i].join()
            self.errorcodesum[0] += phaserjobs[i].errorcodesum
            self.errdict = dict( phaserjobs[i].errdict.items() + self.errdict.items() )

        self.logger.log(logging.INFO, "Done with RunMRauto\n")




    def BLASTnSetupMRruns(self, maxseqid, minseqid, maxpdbs, myinc = 1.0, noRNP=False):
        mstr = "%s, %s, in BLASTnSetupMRruns, maxseqid: %s, \
         minseqid: %s, maxpdbs: %s, similarseqidentmodels: %s\n" \
         %(self.targetpdbid, self.calclabel,maxseqid, minseqid, maxpdbs, myinc)
        reposfile = open("BLASTMRcalculations.txt","a+")
        reposfile.write(mstr)
        reposfile.close()
        self.MakeMyfolder(self.topfolder)
        global LOG_FILENAME
        if LOG_FILENAME == "":
            LOG_FILENAME = os.path.join(self.topfolder, self.calclabel + 'BLASTnSetupMRruns.log')
            self.SetupLogfile(LOG_FILENAME)

        os.chdir(self.topfolder)
        uniquechains = []
        self.logger.log(logging.INFO, mstr)

        pdbsseqids = self.GetPDBidsFromPDBLocalWithSequenceIds(maxseqid, minseqid,
          maxpdbs, uniquechains)

        self.RemoveQuarantinedPDBs(pdbsseqids)

        if myinc > 0.0:
            pdbs_seqids = self.RemovePDBsWithSimilarSeqIdentFromList(pdbsseqids,
              maxseqid, minseqid, myinc)
        else:
            pdbs_seqids = pdbsseqids

        self.logger.log(logging.INFO,"\nModels to be used are\npdb_id+chain_id, BLAST sequence_id, target chain_id:")
        mdlist = "\n".join( ["%s, %s, %s"%(a,b,c) for a,b,c in pdbs_seqids])
        self.logger.log(logging.INFO,"\n%s\n" %mdlist)

        self.logger.log(logging.INFO,"There is(are) %d unique chain(s) in the target." %len(uniquechains))
        self.FetchPDBfile("." + posixpath.sep, self.targetpdbid)
        self.targetscattering = 0.0
        for seq in uniquechains:
            scat, mstr= SimpleFileProperties.GetScatteringFromSequence(seq[1], True)
            self.targetscattering += scat

        self.MakeMyfolder("Phaserinput")
        os.chdir("Phaserinput")
# Hop down in Phaserinput and store common input files there.
        myseq = self.PDB2SeqLocal(self.targetpdbid)
        seqfname =[] # store fasta sequences in folder, say 1tfx/fasta1.seq
        for i in range(0,len(myseq)):
            if myseq[i].find("|PDBID|CHAIN|SEQUENCE") > -1: # new convention for preambling sequences
                seqfname.append(myseq[i][1:5] + "_Chain" + myseq[i][6] + ".seq")
            else: # old preample convention
                seqfname.append(myseq[i][1:12] + '.seq')# use preamble in sequence for naming
            if os.path.exists(seqfname[i]) == True:
                self.logger.log(logging.INFO, "Using existing sequence file, %s" %seqfname[i])
            else:
                self.logger.log(logging.INFO,"Writing sequence file %s", seqfname[i])
                fileout = open(seqfname[i] ,"w")
                fileout.write(myseq[i])
                fileout.close()

        #localmtzname = self.targetpdbid.lower() + '.mtz'
        localmtzname = self.targetpdbid + '.mtz'
        mtzfname = self.mtzfolder + posixpath.sep + localmtzname
# Get the mtz file
        self.logger.log(logging.INFO, "Copying %s to %s" %(mtzfname, os.getcwd()))
        if os.path.exists(mtzfname) != True:
            self.logger.log(logging.WARN,"We don't have an mtz file for "
             + mtzfname + ". Will try downloading from the RCSB...")
# mtz file not avialable so download cif file from RCSB
            u = urlopen("http://www.pdb.org/pdb/files/r" +  self.targetpdbid + "sf.ent.gz")
            cifgz = u.read()
            gzfile = open("tmp.gz",'wb')
            gzfile.write(cifgz)
            gzfile.close()

            self.logger.log(logging.INFO, "Writing tmp.gz")
            fileObj = gzip.GzipFile("tmp.gz", 'rb')
            cifdata = fileObj.read() # decompress the file
            fileObj.close()

            self.logger.log(logging.INFO, "Writing tmp.cif")
            fileout = open("tmp.cif",'w')
            fileout.write(cifdata)
            fileout.close()
            os.remove("tmp.gz") # delete the compressed file
# convert cif file to mtz
# using phenix.cif_as_mtz
            command = ["tmp.cif", "--symmetry=.." + posixpath.sep + self.targetpdbid
             + ".pdb", "--output-file-name=" + self.targetpdbid + ".mtz"]
            self.logger.log(logging.INFO, "Executing command: "
             + subprocess.list2cmdline(command))

            cif_as_mtz.run(command)
            os.remove("tmp.cif") # delete cif file

        if os.path.exists(localmtzname) != True and os.path.exists(mtzfname) == True:
            shutil.copyfile(mtzfname, os.path.join(os.getcwd(), localmtzname))

        psearches = []
        if os.path.exists(localmtzname) != True:
            self.logger.log(logging.FATAL, "No mtz file available!")
            self.errorcodesum[0] = 42
            return psearches

        Fcol = ['']
        Sigcol = ['']
        Icol = ['']
        SigIcol = ['']

        self.GetMtzColumns(localmtzname, Fcol, Sigcol, Icol, SigIcol)
# only intensities avaialable in the mtz file so convert it to amplitudes
        if Fcol[0] == '' and Icol[0] != '' and Sigcol[0] != '':
            self.logger.log(logging.INFO, "Only intensities available in %s. Converting to amplitudes..." %localmtzname)

            mtzwithamplname = self.targetpdbid.lower() + '_I2ampl.mtz'
            if os.path.exists(mtzwithamplname):
                os.remove(mtzwithamplname) # to avoid windows python crash in reflection_file_converter.run

            intensity2amplarg = ["--write-mtz-amplitudes","--mtz-root-label=FOBS","--label=%s"% Icol[0],
             "--mtz=%s" %mtzwithamplname, "%s" %localmtzname]
            self.logger.log(logging.INFO, "reflection_file_converter " + subprocess.list2cmdline(intensity2amplarg))
            reflection_file_converter.run(args = intensity2amplarg)

            Fcol = ['']
            Sigcol = ['']
            Icol = ['']
            SigIcol = ['']
# check that none of the intensities are negative due to measurement errors
            if self.GetMtzColumns(mtzwithamplname, Fcol, Sigcol, Icol, SigIcol) == False:
                self.logger.log(logging.INFO, "Some reflections from the conversion %s "
                 "have amplitude values of zero. Must massage intensities..." %mtzwithamplname)

                mtzwithmssgamplname = self.targetpdbid.lower() + '_mssg_I2ampl.mtz'
                if os.path.exists(mtzwithmssgamplname):
                    os.remove(mtzwithmssgamplname) # to avoid windows python crash in reflection_file_converter.run
# if so then convert again and massage intensities
                intensity2amplarg = ["--write-mtz-amplitudes","--mtz-root-label=FOBS","--label=%s"% Icol[0],
                 "--mtz=%s" %mtzwithmssgamplname, "--massage-intensities", "%s" %localmtzname]
                self.logger.log(logging.INFO, "reflection_file_converter " + subprocess.list2cmdline(intensity2amplarg))
                reflection_file_converter.run(args = intensity2amplarg)

                localmtzname = mtzwithmssgamplname
            else:
                localmtzname = mtzwithamplname

        Fcol = ['']
        Sigcol = ['']
        Icol = ['']
        SigIcol = ['']
        self.GetMtzColumns(localmtzname,Fcol,Sigcol,Icol, SigIcol)
        if (Fcol[0] == '' or Sigcol[0] == '') and (Icol == '' or SigIcol == ''):
            self.logger.log(logging.FATAL,
             "Neither intensity nor amplitude columns seem present in %s" %localmtzname)
            self.errorcodesum[0] += 41
            return psearches

        self.logger.log(logging.INFO, "Writing base input.txt file in " + os.getcwd())
        infname =self.calclabel  + "input.txt"
        if os.path.exists(infname):
            self.logger.log(logging.WARN, infname + " exists and will be overwritten.")

        inputfile = open(infname, "w")
        mtzstr = "Phaserinput" + posixpath.sep + localmtzname
        inputfile.write("HKLIN " + mtzstr + "\n")
        if self.buseIntensities:
            inputfile.write("LABIN I = " + Icol[0] + "  SIGI = " + SigIcol[0] + "\n")
        else:
            inputfile.write("LABIN F = " + Fcol[0] + "  SIGF = " + Sigcol[0] + "\n")

        for i in range(0, len(seqfname)):
            inputfile.write("COMPOSITION PROTEIN SEQUENCE Phaserinput" \
              + posixpath.sep + seqfname[i] + ' NUM 1\n')
        #inputfile.write("XYZOUT ON ENSEMBLE ON\n")
        inputfile.close()

        os.chdir("..")
        preparedmodels = []
        for pdbseqid in pdbs_seqids:
            if (self.PreparePartialModel(pdbseqid)):
                preparedmodels.append(pdbseqid)

        if noRNP:
            return psearches

        searches = self.GetCombinedSearchesforTargetChains(preparedmodels)

        for search in searches:
            psearch = self.PermuteSearches(search)
            if self.PrepareTemplateSolutions(search, psearch):
# create new list of those models that made it through the sculpting and RNP
                psearches.extend(psearch)

        for psearch in psearches:
            for model in psearch:
    # don't need the model superposition eulers and translations anymore as they are very verbose
                model.remove(model[3])
            modelinputs = self.PrepareInputFiles(psearch)
            psearch.append(modelinputs)
#psearches format is like
# [[[['1KCW_A', ['32.615', '0.358', 75.279, 742, 538], 'A', '{1-814}'], ['2ORX_A', ['34.395', '0.159', 40.94, 314, 149], 'B', '{273-586}']], ['1KCW_A', '1KCW_A,2ORX_A']],
#  [[['2ORX_A', ['34.395', '0.159', 40.94, 314, 149], 'B', '{273-586}'], ['1KCW_A', ['32.615', '0.358', 75.279, 742, 538], 'A', '{1-814}']], ['2ORX_A', '2ORX_A,1KCW_A']]]
# changing it to
# [[['1KCW_A', ['32.615', '0.358', 75.279, 742, 538], 'A', '{1-814}'], ['2ORX_A', ['34.395', '0.159', 40.94, 314, 149], 'B', '{273-586}'], ['1KCW_A', '1KCW_A,2ORX_A']],
#  [['2ORX_A', ['34.395', '0.159', 40.94, 314, 149], 'B', '{273-586}'], ['1KCW_A', ['32.615', '0.358', 75.279, 742, 538], 'A', '{1-814}'], ['2ORX_A', '2ORX_A,1KCW_A']]]
#
# last element is a list of succesive search model composition. Want this element to
# be put in a separate list, i.e. convert list from [[m]] into [[m-1],[1]]
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        psearches = [[[e for e in  e1[:-1]], e1[-1]] for e1 in psearches ]
        os.chdir(self.topfolder)
        blastout = open(self.targetpdbid + "_BLASTchain_ids.txt" ,"w")
        blastout.write(str(psearches))
        blastout.close()

        self.logger.log(logging.INFO, "\nSubfolders now set up for MR calculations")
        self.logger.log(logging.INFO, self.targetpdbid + "_BLASTchain_ids.txt lists model pdb_chain ids")
        self.logger.log(logging.INFO,"with sequence identity and the permuted MR searches.")
        self.logger.log(logging.INFO,self.calclabel  + "input.txt is toplevel MR input to be joined with")
        self.logger.log(logging.INFO,self.calclabel  + "modelinput.txt before each MR calulation.")
        self.logger.log(logging.INFO,"\nDone with BLASTnSetupMRruns.")

        return psearches


# if a subset of interesting MR targets and models is provided let's only use those
    def GetSubsetSearches(self, pdbs_seqids):
        if self.subsetlist == None:
            return pdbs_seqids
        pdbs_seqidssubset = []
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for e in pdbs_seqids:
            if [self.targetpdbid.upper(), str(e[1][-1])] in [[g[0].upper(), g[1].upper()] for g in self.subsetlist]:
                pdbs_seqidssubset.append(e)
        return pdbs_seqidssubset



    def CollateMRautoResultsFromlistFile(self, TaggedPDBfileliststr,
      OnlyCreateDBcolumns):
        # compile tab separated tex file to load into a spreadsheet
        reposfile = open("BLASTMRcalculations.txt","a+")
        reposfile.write("%s, %s, in CollateMRautoResultsFromlistFile\n" %(self.targetpdbid, self.calclabel))
        reposfile.close()
# a list of filenames for externally supplied PDB codes and corresponding labels (say NMR, EM)
        self.TaggedPDBfileliststr = TaggedPDBfileliststr
        if OnlyCreateDBcolumns == False:
            searchlistfname =  os.path.join(self.topfolder, self.targetpdbid + "_BLASTchain_ids.txt")
            searchlistfname = RecaseFilename2(searchlistfname)
            psearches = eval(open(searchlistfname,"r").read())
            self.CollateMRautoResultsFromlist(psearches)



    def CollateMRautoResultsFromlist(self, psearches):
        from phaser import sqltbx
        LOG_FILENAME = os.path.join(self.topfolder, self.calclabel + "MRResults.log")
        self.SetupLogfile(LOG_FILENAME)
        #self.logger.log(logging.INFO, "CollateMRautoResultsFromlistFile, pdb: %s, " \
        # "calclabel: %s, database: %s\n" %(self.targetpdbid, self.calclabel, self.extdbname))
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        newpsearches = self.GetSubsetSearches(psearches)
        if len(newpsearches):
            self.ncomponents = len(newpsearches[0][1])
        else:
            self.logger.log(logging.INFO, "No allowed models selected from subset\n")
            return

        TaggedPDBfilelst = self.TaggedPDBfileliststr.split()
        nlen = int(len(TaggedPDBfilelst)/2)
        for i in range(0, nlen):
            fname = TaggedPDBfilelst[i*2]
            tag = TaggedPDBfilelst[i*2+1]
            self.logger.log(logging.INFO, "MR models matching PDB ids in %s will be tagged as %s" %(fname,tag))
# create sets of PDB ids from each file
            mfile = open(fname, "r")
            mtxt = mfile.read()
            mfile.close()
            tpl = (set([]), tag)
            for pdbid in mtxt.split():
                tpl[0].add(pdbid.upper())

            self.PDBTagsets.append(tpl) # being used by GatherResults

        os.chdir(self.topfolder)
        olddir = os.getcwd()

        mtzname = posixpath.join("Phaserinput", self.targetpdbid.lower() + '.mtz')
        mtzname = RecaseFilename2(mtzname)
        mtzobj = iotbx.mtz.object(file_name = mtzname)
        xtalsysname = mtzobj.space_group().crystal_system()
        spgname = mtzobj.space_group_info().type().lookup_symbol()
        spgnumber = mtzobj.space_group_info().type().number()
        ispolarspacegroup = int(AltOrigSymmatesEngine.list_origins(spgnumber)[1])
        ucell = mtzobj.crystals()[0].unit_cell_parameters()
        unitcell = "%3.3f %3.3f %3.3f %3.3f %3.3f %3.3f" %(ucell[0], ucell[1],ucell[2],ucell[3],ucell[4],ucell[5])
# get size of ASU as the unit cell volume divided by the number of symmetry operations
        ASUvolume = mtzobj.crystals()[0].unit_cell().volume()/len(mtzobj.space_group().all_ops())
# get the Rfactors for the target structure
        Rfactors = [-1.0, -1.0, -1.0, -1.0]
        Rfacstr = open(os.path.join(self.scoppath,"Rfactors.txt"),"r").read()
        m = re.search(r"("+ self.targetpdbid + ")\t(.*)\t(.*)\t(.*)\t(.*)\t(.*)\n", Rfacstr,re.IGNORECASE)
        if m != None:
            r = m.groups()
            Rfactors = [Roundoff(r[1],3), Roundoff(r[2],3), Roundoff(r[3],3), Roundoff(r[4],3)]
# check if EDS can reproduce the Rfactor
        EDSstr = open(os.path.join(self.scoppath, "eds_holdings.txt"), "r").read()
        m = re.search(r"("+ self.targetpdbid + ")\n", EDSstr,re.IGNORECASE)
        if m != None:
            EDSreproduce = 1
        else:
            EDSreproduce = 0
        #if OnlyCreateDBcolumns == False:
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        doneruns = ""
        #if self.brunphaser:
        #    completedrunfname = posixpath.join(self.topfolder, self.calclabel
        #     + self.targetpdbid + "_DoneMRJobs.txt")
        #    doneruns = open(completedrunfname,"r").read()

    #            testsol = BLASTMRutils.BLASTMRutils(self.topfolder, self)
        #datares = self.GetResolution(self.targetpdbid)
        #self.DBfname = DBfname
        internaldbname = os.path.join(self.topfolder, self.calclabel + "_" \
          + self.targetpdbid + self.buildnumberstr + ".db")
        # don't create dbase file if it's already there
        if not os.path.exists(internaldbname):
            with sqltbx.DBaseMaker(internaldbname, self.logger) as dbase:
                WilsonB = [-1.0]
                WilsonScale = [-1.0]
                MatthewsCoef = [-1.0]
                #lastMRTargetTbl_id = [0]
                os.chdir(posixpath.join(self.topfolder, "Phaserinput"))
                seqfiles = glob.glob("*.seq")
                ntargetresidues = 0
                TargetScattering = 0
                for seqfile in seqfiles:
                    seqtxt = open(seqfile,"r").read()
                    ntargetresidues += SimpleFileProperties.GetNumberofResiduesFromSequence(seqtxt)
                    scat, msg = SimpleFileProperties.GetScatteringFromSequence(seqtxt)
                    TargetScattering += scat

                xtriagecache = self.GetXtriageCachedResults()
                os.chdir(olddir)
                for s in self.OtherSolutions: # first element is empty as that's for the original Strider calculations
                    OtherSolfname = s[1]
                    suffix = s[0]
                    if OtherSolfname =="":
                        SetOfResults = set([])
                        for search in newpsearches:
            # create modelsdir as concatenated PDB codes separated by commas
                            previousmodelscomponentstabs = ""
                            modelsdir = [ [e[0] for e in  e1]  for e1 in search[:-1]][0]
                            workdir = ",".join( modelsdir )

                            for (i,partmodels) in enumerate(search[1]):
                                if not partmodels in SetOfResults:
                                    testsol = BLASTMRutils.BLASTMRutils(self.topfolder, self,
                                     workdir, partmodels, OtherSolfname)
            # don't bother writing results for the same partmodels more than once
                                    SetOfResults.add(partmodels)
                                    #print "partmodels= " + str(partmodels)
                                    bsolved = [False]
                                    founddir = [] #[""]
                                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        #                                if testsol.PhaserRanPartModelsBefore(founddir,partmodels,bsolved, doneruns):
                                    #if testsol.PhaserRanPartModelsBefore(bsolved) or not self.brunphaser:
            # array that'll hold MR solution, template solution, FTF_scrores data, VRMS and LLG float values
                                    try:
                                        nsolvedit = testsol.IsthereaPartialSolution()
                                        #print "testsol.mrsol[3][6] = " +str(testsol.mrsol[3][6])
                                        self.GatherResults(dbase, search, workdir, partmodels,
                                         i, testsol.mrsol, suffix, xtriagecache, unitcell, spgname, spgnumber,
                                         xtalsysname, ispolarspacegroup, nsolvedit, self.PDBTagsets, WilsonB,
                                         WilsonScale, Rfactors, EDSreproduce, MatthewsCoef, TargetScattering,
                                         ntargetresidues, ASUvolume)
                                    except Exception as m:
                                        #self.logger.log(logging.WARN, m)
                                        self.logger.log(logging.ERROR, str(m) + "\n" \
                                          + traceback.format_exc())
                                    self.ncomponents = len(search[0][i][2])

        extdbname = self.extdbnameprefix + self.buildnumberstr + ".db"
        self.logger.log(logging.INFO, "\nResults written to " + extdbname + "\n")
        #return

        merge1 = sqltbx.Mergeinf()
        merge1.mergetable="MRTargetTbl"

        ups = sqltbx.Updateinf()
        ups.updatecolumn = "MRTarget_id"  # UPDATE tomerge.MRModelTbl SET MRTarget_id =
        ups.reftable = "MRTargetTbl"      # ( SELECT r.ROWID FROM MRTargetTbl r
        ups.reftablecolumn = "ROWID"
        ups.refmatchcolids = ["TargetPDBid"] # WHERE r.TargetPDBid = ( SELECT tr.TargetPDBid FROM tomerge.MRTargetTbl tr WHERE tr.ROWID = MRTarget_id ) )
        merge2 = sqltbx.Mergeinf()
        merge2.mergetable = "MRModelTbl"
        merge2.updates = [ups]

        ups1 = sqltbx.Updateinf()
        ups1.updatecolumn = "MRTarget_id" # UPDATE tomerge.AnnotationTbl SET MRTarget_id =
        ups1.reftable = "MRTargetTbl"     # ( SELECT r.ROWID FROM MRTargetTbl r
        ups1.reftablecolumn = "ROWID"
        ups1.refmatchcolids = ["TargetPDBid"] # WHERE r.TargetPDBid = ( SELECT tr.TargetPDBid FROM tomerge.MRTargetTbl tr WHERE tr.ROWID = MRTarget_id ) )
        ups2 = sqltbx.Updateinf()
        ups2.updatecolumn = "MRModel_id" # UPDATE tomerge.AnnotationTbl SET MRModel_id =
        ups2.reftable = "MRModelTbl"     # ( SELECT r.ROWID FROM MRModelTbl r
        ups2.reftablecolumn = "ROWID"
        ups2.refmatchcolids = ["TargetPDBid",  #  WHERE r.TargetPDBid = ( SELECT tr.TargetPDBid FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )
                               "Fullmodel"] #,    # AND r.Fullmodel = ( SELECT tr.Fullmodel FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )
                               #"ELLG_TARG" ]   # AND r.ELLG_TARG = ( SELECT tr.ELLG_TARG FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )  )
        merge3 = sqltbx.Mergeinf()
        merge3.mergetable = "AnnotationTbl"
        merge3.updates = [ups1, ups2]

        ups1 = sqltbx.Updateinf()
        ups1.updatecolumn = "MRTarget_id" # UPDATE tomerge.TemplateRMSTbl SET MRTarget_id =
        ups1.reftable = "MRTargetTbl"     # ( SELECT r.ROWID FROM MRTargetTbl r
        ups1.reftablecolumn = "ROWID"
        ups1.refmatchcolids = ["TargetPDBid"] # WHERE r.TargetPDBid = ( SELECT tr.TargetPDBid FROM tomerge.MRTargetTbl tr WHERE tr.ROWID = MRTarget_id ) )
        ups2 = sqltbx.Updateinf()
        ups2.updatecolumn = "MRModel_id" # UPDATE tomerge.TemplateRMSTbl SET MRModel_id =
        ups2.reftable = "MRModelTbl"     # ( SELECT r.ROWID FROM MRModelTbl r
        ups2.reftablecolumn = "ROWID"
        ups2.refmatchcolids = ["TargetPDBid",  #  WHERE r.TargetPDBid = ( SELECT tr.TargetPDBid FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )
                               "RefinedModel"] #,    # AND r.Fullmodel = ( SELECT tr.Fullmodel FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )
                               #"ELLG_TARG" ]   # AND r.ELLG_TARG = ( SELECT tr.ELLG_TARG FROM tomerge.MRModelTbl tr WHERE tr.ROWID = MRModel_id )  )
        merge3 = sqltbx.Mergeinf()
        merge3.mergetable = "TemplateRMSTbl"
        merge3.updates = [ups1, ups2]
        # store copy of dbase file as it will get mangled during merging
        tmpinternaldbname = os.path.join(self.topfolder, self.calclabel + "_" \
          + self.targetpdbid + self.buildnumberstr + "tmp.db")
        shutil.copyfile(internaldbname, tmpinternaldbname)
        # merge them dbases
        sqltbx.MergeDbases(extdbname, internaldbname, [merge1, merge2, merge3], self.logger )
        shutil.copyfile(tmpinternaldbname, internaldbname)
        os.remove(tmpinternaldbname)





    def BLASTnMRrun(self, maxseqid, minseqid, maxpdbs, njobs,
     removesimilarseqidentmodels):
        reposfile = open("BLASTMRcalculations.txt","a+")
        reposfile.write("%s, %s, in BLASTnMRrun\n" %(self.targetpdbid, self.calclabel))
        reposfile.close()
        self.MakeMyfolder(self.topfolder)
        LOG_FILENAME = os.path.join(self.topfolder, self.calclabel + 'BLASTnMRrun.log')
        self.SetupLogfile(LOG_FILENAME)
        pdbs_seqids = self.BLASTnSetupMRruns(maxseqid, minseqid, maxpdbs,
             removesimilarseqidentmodels)
        minmodelorder = len(pdbs_seqids[0][1])
        # only do MR on the last component using the template solution of the previous one
        self.RunMRauto(pdbs_seqids, njobs, minmodelorder)
        self.logger.log(logging.INFO,"Done with BlastnMRrun.")



    def RunMRautoFromlistFile(self, njobs):
        searchlistfname = self.targetpdbid + "_BLASTchain_ids.txt"
        reposfile = open("BLASTMRcalculations.txt","a+")
        reposfile.write("%s, %s, in RunMRautoFromlistFile, pdb searchlist: %s\n" \
          %(self.targetpdbid, self.calclabel, searchlistfname))
        reposfile.close()
        self.MakeMyfolder(self.topfolder)
        LOG_FILENAME = os.path.join(self.topfolder, self.calclabel + 'RunMRautoFromlistFile.log')
        self.SetupLogfile(LOG_FILENAME)
        self.logger.log(logging.INFO, "RunMRautoFromlistFile, pdb: %s, pdb searchlist: %s, \
calclabel: %s\n" %(self.targetpdbid, searchlistfname, self.calclabel))
        searchlistfname = os.path.join(self.topfolder, searchlistfname)
        searchlistfname = PosixSlashFilePath(searchlistfname)
        searchlistfname = RecaseFilename2(searchlistfname)
        pdbs_seqids = eval(open(searchlistfname,"r").read())
        self.MakeModelDictionary(pdbs_seqids)
        self.logger.log(logging.INFO, "Models in %s\n" %searchlistfname )
        for e in pdbs_seqids:
            self.logger.log(logging.INFO, e[0][0][0] + "\n")
        pdbs_seqids = self.GetSubsetSearches(pdbs_seqids)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        # only do MR on the last component using the template solution of the previous one
        if len(pdbs_seqids) > 0:
            minmodelorder = len(pdbs_seqids[0][1])
            self.RunMRauto(pdbs_seqids, njobs, minmodelorder)
        else:
            self.logger.log(logging.INFO,"No models to use for MR calculations")

        self.logger.log(logging.INFO,"Finished RunMRautoFromlistFile on %s\n" %self.targetpdbid)
        return pdbs_seqids



    def RunMRAutoGetResultsFromListFile(self, njobs=1):
        psearches = self.RunMRautoFromlistFile(njobs)
        self.CollateMRautoResultsFromlist(psearches)
        return psearches
