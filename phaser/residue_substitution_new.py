from __future__ import print_function

import itertools

def indexer_for(codes):

  return dict( ( code, index ) for ( index, code ) in enumerate( codes ) )


class ScoringMatrix(object):
  """
  Data used for sequence scoring
  """

  def __init__(self, name, indexer, scores, gap_penalty):

    self.name = name
    self.indexer = indexer
    self.scores = scores
    self.gap_penalty = gap_penalty
    self.normalizers = None


  def base(self):

    return self.normalization()[1]


  def scale(self):

    return self.normalization()[0]


  def score_for(self, left, right):

    return self.scores[ self.indexer[ left ] ][ self.indexer[ right ] ]


  def normalization(self):

    if not self.normalizers:
      count = len( self.indexer )
      norm = 1.0 / count
      perfect = norm * sum(
        self.scores[ n ][ n ] for n in range( len( self.indexer ) )
        )
      random = norm ** 2 * sum( sum( row ) for row in self.scores )
      self.normalizers = ( perfect - random, random )

    return self.normalizers


  @property
  def normalized_gap_penalty(self):

    return ( self.gap_penalty - self.base() ) / self.scale()


  def normalized_score_for(self, left, right):

    return ( self.score_for( left = left, right = right ) - self.base() ) / self.scale()


  def __str__(self):

    return "%s scoring matrix" % self.name


# amino acid similarity scores from Dayhoff and Henikoff & Henikoff
amino_acid_codes = [ "A", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N",
  "P", "Q", "R", "S", "T", "V", "W", "Y" ]
amino_acid_codes_indexer = indexer_for( codes = amino_acid_codes )

dayhoff_mdm78_similarity_scores = [
  [  18, -20,   3,   3, -35,  13, -14,  -5, -12, -19, -11,   2,  11,  -4, -15,  11,  12,   2, -58, -35 ],
  [ -20, 119, -51, -53, -43, -34, -34, -23, -54, -60, -52, -36, -28, -54, -36,   0, -22, -19, -78,   3 ],
  [   3, -51,  39,  34, -56,   6,   7, -24,   1, -40, -26,  21, -10,  16, -13,   3,  -1, -21, -68, -43 ],
  [   3, -53,  34,  38, -54,   2,   7, -20,  -1, -34, -21,  14,  -6,  25, -11,   0,  -4, -18, -70, -43 ],
  [ -35, -43, -56, -54,  91, -48, -18,  10, -53,  18,   2, -35, -46, -47, -45, -32, -31, -12,   4,  70 ],
  [  13, -34,   6,   2, -48,  48, -21, -26, -17, -41, -28,   3,  -5, -12, -26,  11,   0, -14, -70, -52 ],
  [ -14, -34,   7,   7, -18, -21,  65, -24,   0, -21, -21,  16,  -2,  29,  16,  -8, -13, -22, -28,  -1 ],
  [  -5, -23, -24, -20,  10, -26, -24,  45, -19,  24,  22, -18, -20, -20, -20, -14,   1,  37, -51,  -9 ],
  [ -12, -54,   1,  -1, -53, -17,   0, -19,  47, -29,   4,  10, -11,   7,  34,  -2,   0, -24, -35, -44 ],
  [ -19, -60, -40, -34,  18, -41, -21,  24, -29,  59,  37, -29, -25, -18, -30, -28, -17,  19, -18,  -9 ],
  [ -11, -52, -26, -21,   2, -28, -21,  22,   4,  37,  64, -17, -21, -10,  -4, -16,  -6,  18, -42, -24 ],
  [   2, -36,  21,  14, -35,   3,  16, -18,  10, -29, -17,  20,  -5,   8,   0,   7,   4, -17, -42, -21 ],
  [  11, -28, -10,  -6, -46,  -5,  -2, -20, -11, -25, -21,  -5,  59,   2,  -2,   9,   3, -12, -56, -49 ],
  [  -4, -54,  16,  25, -47, -12,  29, -20,   7, -18, -10,   8,   2,  40,  13,  -5,  -8, -19, -48, -40 ],
  [ -15, -36, -13, -11, -45, -26,  16, -20,  34, -30,  -4,   0,  -2,  13,  61,  -3,  -9, -25,  22, -42 ],
  [  11,   0,   3,   0, -32,  11,  -8, -14,  -2, -28, -16,   7,   9,  -5,  -3,  16,  13, -10, -25, -28 ],
  [  12, -22,  -1,  -4, -31,   0, -13,   1,   0, -17,  -6,   4,   3,  -8,  -9,  13,  26,   3, -52, -27 ],
  [   2, -19, -21, -18, -12, -14, -22,  37, -24,  19,  18, -17, -12, -19, -25, -10,   3,  43, -62, -25 ],
  [ -58, -78, -68, -70,   4, -70, -28, -51, -35, -18, -42, -42, -56, -48,  22, -25, -52, -62, 173,  -2 ],
  [ -35,   3, -43, -43,  70, -52,  -1,  -9, -44,  -9, -24, -21, -49, -40, -42, -28, -27, -25,  -2, 101 ],
  ]

blosum62_similarity_scores = [
  [  4,  0, -2, -1, -2,  0, -2, -1, -1, -1, -1, -2, -1, -1, -1,  1,  0,  0, -3, -2 ],
  [  0,  9, -3, -4, -2, -3, -3, -1, -3, -1, -1, -3, -3, -3, -3, -1, -1, -1, -2, -2 ],
  [ -2, -3,  6,  2, -3, -1, -1, -3, -1, -4, -3,  1, -1,  0, -2,  0, -1, -3, -4, -3 ],
  [ -1, -4,  2,  5, -3, -2,  0, -3,  1, -3, -2,  0, -1,  2,  0,  0, -1, -2, -3, -2 ],
  [ -2, -2, -3, -3,  6, -3, -1,  0, -3,  0,  0, -3, -4, -3, -3, -2, -2, -1,  1,  3 ],
  [  0, -3, -1, -2, -3,  6, -2, -4, -2, -4, -3,  0, -2, -2, -2,  0, -2, -3, -2, -3 ],
  [ -2, -3, -1,  0, -1, -2,  8, -3, -1, -3, -2,  1, -2,  0,  0, -1, -2, -3, -2,  2 ],
  [ -1, -1, -3, -3,  0, -4, -3,  4, -3,  2,  1, -3, -3, -3, -3, -2, -1,  3, -3, -1 ],
  [ -1, -3, -1,  1, -3, -2, -1, -3,  5, -2, -1,  0, -1,  1,  2,  0, -1, -2, -3, -2 ],
  [ -1, -1, -4, -3,  0, -4, -3,  2, -2,  4,  2, -3, -3, -2, -2, -2, -1,  1, -2, -1 ],
  [ -1, -1, -3, -2,  0, -3, -2,  1, -1,  2,  5, -2, -2,  0, -1, -1, -1,  1, -1, -1 ],
  [ -2, -3,  1,  0, -3,  0,  1, -3,  0, -3, -2,  6, -2,  0,  0,  1,  0, -3, -4, -2 ],
  [ -1, -3, -1, -1, -4, -2, -2, -3, -1, -3, -2, -2,  7, -1, -2, -1, -1, -2, -4, -3 ],
  [ -1, -3,  0,  2, -3, -2,  0, -3,  1, -2,  0,  0, -1,  5,  1,  0, -1, -2, -2, -1 ],
  [ -1, -3, -2,  0, -3, -2,  0, -3,  2, -2, -1,  0, -2,  1,  5, -1, -1, -3, -3, -2 ],
  [  1, -1,  0,  0, -2,  0, -1, -2,  0, -2, -1,  1, -1,  0, -1,  4,  1, -2, -3, -2 ],
  [  0, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1,  0, -1, -1, -1,  1,  5,  0, -2, -2 ],
  [  0, -1, -3, -2, -1, -3, -3,  3, -2,  1,  1, -3, -2, -2, -3, -2,  0,  4, -3, -1 ],
  [ -3, -2, -4, -3,  1, -2, -2, -3, -3, -2, -1, -4, -4, -2, -3, -3, -2, -3, 11,  2 ],
  [ -2, -2, -3, -2,  3, -3,  2, -1, -2, -1, -1, -2, -3, -1, -2, -2, -2, -1,  2,  7 ],
  ]


extended_amino_acid_codes = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "K",
  "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z" ]
extended_amino_acid_codes_indexer = indexer_for( codes = extended_amino_acid_codes )

blosum50_similarity_scores = [
  [  5, -2, -1, -2, -1, -3,  0, -2, -1, -1, -2, -1, -1, -1, -1, -2,  1,  0,  0, -3, -1, -2, -1 ],
  [ -2,  5, -3,  5,  1, -4, -1,  0, -4,  0, -4, -3,  4, -2,  0, -1,  0,  0, -4, -5, -1, -3,  2 ],
  [ -1, -3, 13, -4, -3, -2, -3, -3, -2, -3, -2, -2, -2, -4, -3, -4, -1, -1, -1, -5, -2, -3, -3 ],
  [ -2,  5, -4,  8,  2, -5, -1, -1, -4, -1, -4, -4,  2, -1,  0, -2,  0, -1, -4, -5, -1, -3,  1 ],
  [ -1,  1, -3,  2,  6, -3, -3,  0, -4,  1, -3, -2,  0, -1,  2,  0, -1, -1, -3, -3, -1, -2,  5 ],
  [ -3, -4, -2, -5, -3,  8, -4, -1,  0, -4,  1,  0, -4, -4, -4, -3, -3, -2, -1,  1, -2,  4, -4 ],
  [  0, -1, -3, -1, -3, -4,  8, -2, -4, -2, -4, -3,  0, -2, -2, -3,  0, -2, -4, -3, -2, -3, -2 ],
  [ -2,  0, -3, -1,  0, -1, -2, 10, -4,  0, -3, -1,  1, -2,  1,  0, -1, -2, -4, -3, -1,  2,  0 ],
  [ -1, -4, -2, -4, -4,  0, -4, -4,  5, -3,  2,  2, -3, -3, -3, -4, -3, -1,  4, -3, -1, -1, -3 ],
  [ -1,  0, -3, -1,  1, -4, -2,  0, -3,  6, -3, -2,  0, -1,  2,  3,  0, -1, -3, -3, -1, -2,  1 ],
  [ -2, -4, -2, -4, -3,  1, -4, -3,  2, -3,  5,  3, -4, -4, -2, -3, -3, -1,  1, -2, -1, -1, -3 ],
  [ -1, -3, -2, -4, -2,  0, -3, -1,  2, -2,  3,  7, -2, -3,  0, -2, -2, -1,  1, -1, -1,  0, -1 ],
  [ -1,  4, -2,  2,  0, -4,  0,  1, -3,  0, -4, -2,  7, -2,  0, -1,  1,  0, -3, -4, -1, -2,  0 ],
  [ -1, -2, -4, -1, -1, -4, -2, -2, -3, -1, -4, -3, -2, 10, -1, -3, -1, -1, -3, -4, -2, -3, -1 ],
  [ -1,  0, -3,  0,  2, -4, -2,  1, -3,  2, -2,  0,  0, -1,  7,  1,  0, -1, -3, -1, -1, -1,  4 ],
  [ -2, -1, -4, -2,  0, -3, -3,  0, -4,  3, -3, -2, -1, -3,  1,  7, -1, -1, -3, -3, -1, -1,  0 ],
  [  1,  0, -1,  0, -1, -3,  0, -1, -3,  0, -3, -2,  1, -1,  0, -1,  5,  2, -2, -4, -1, -2,  0 ],
  [  0,  0, -1, -1, -1, -2, -2, -2, -1, -1, -1, -1,  0, -1, -1, -1,  2,  5,  0, -3,  0, -2, -1 ],
  [  0, -4, -1, -4, -3, -1, -4, -4,  4, -3,  1,  1, -3, -3, -3, -3, -2,  0,  5, -3, -1, -1, -3 ],
  [ -3, -5, -5, -5, -3,  1, -3, -3, -3, -3, -2, -1, -4, -4, -1, -3, -4, -3, -3, 15, -3,  2, -2 ],
  [ -1, -1, -2, -1, -1, -2, -2, -1, -1, -1, -1, -1, -1, -2, -1, -1, -1,  0, -1, -3, -1, -1, -1 ],
  [ -2, -3, -3, -3, -2,  4, -3,  2, -1, -2, -1,  0, -2, -3, -1, -1, -2, -2, -1,  2, -1,  8, -2 ],
  [ -1,  2, -3,  1,  5, -4, -2,  0, -3,  1, -3, -1,  0, -1,  4,  0,  0, -1, -3, -2, -1, -2,  5 ],
  ]

identity_similarity_scores = [
  [ int( i == j ) for i in amino_acid_codes ] for j in amino_acid_codes
  ]

identity = ScoringMatrix(
  name = "identity",
  indexer = amino_acid_codes_indexer,
  scores = identity_similarity_scores,
  gap_penalty = -1,
  )

dayhoff = ScoringMatrix(
  name = "dayhoff",
  indexer = amino_acid_codes_indexer,
  scores = dayhoff_mdm78_similarity_scores,
  gap_penalty = -80,
  )

blosum50 = ScoringMatrix(
  name = "blosum50",
  indexer = extended_amino_acid_codes_indexer,
  scores = blosum50_similarity_scores,
  gap_penalty = -10,
  )

blosum62 = ScoringMatrix(
  name = "blosum62",
  indexer = amino_acid_codes_indexer,
  scores = blosum62_similarity_scores,
  gap_penalty = -8,
  )


MATRIX_NAMED = {
  "protein-identity": identity,
  "protein-dayhoff": dayhoff,
  "protein-blosum50": blosum50,
  "protein-blosum62": blosum62,
  }


def average(values):

  total = 0
  count = 0

  for v in values:
    total += v
    count += 1

  return total / count


def sequence_similarity_scores_for(alignment, matrix, unknown, reducer = average):

  multiplicity = alignment.multiplicity()

  if multiplicity == 0:
    return []

  elif multiplicity == 1:
    return [
      single_similarity_score(
        left = c,
        right = c,
        gap = alignment.gap,
        matrix = matrix,
        unknown = unknown,
        )
      for c in alignment.alignments[0]
      ]

  return [
    reducer(
      single_similarity_score(
        left = l,
        right = r,
        gap = alignment.gap,
        matrix = matrix,
        unknown = unknown,
        )
      for ( l, r ) in itertools.combinations( equiv, 2 )
      )
    for equiv in zip( *alignment.alignments )
    ]


def single_similarity_score(left, right, gap, matrix, unknown):

  if left == gap or right == gap:
    return matrix.gap_penalty

  try:
    return matrix.score_for( left = left, right = right )

  except KeyError:
    return unknown
