from __future__ import print_function

from libtbx.utils import Sorry

import os.path
import traceback as trcbck
import sqlite3
from types import *
import logging



def lprint(mstr, logger=None):
  if logger:
    logger.log(logging.INFO, mstr + "\n")
  else:
    print(mstr)


class DBaseMaker():
  """
  If dbfname is specified open database file in advance for IO efficiency like
  >>> with tbx_utils.DBaseMaker(dbasename) as mdb:
  >>>   mdb.PutValuesinTable( columnvaluedict, tablename, columnid, valofprimcol )
  >>>
  If IO efficiency is unimportant open database file like
  >>> mdb = tbx_utils.DBaseMaker()
  >>> mdb( dbasename, columnvaluedict,  tablename, columnid, valofprimcol )
  >>>
  """
  def __init__(self, dbfname=None, logger=None):
    self.dbfname = dbfname
    self.logger = logger
    if self.dbfname:
      self.leaveopen = True
# open the database and wait up to 16.6 hours for concurrent threads writing to the same file
      self.conn = sqlite3.connect(database = self.dbfname, timeout = 60000)
      self.mydb = self.conn.cursor()
      self.mydb.execute('PRAGMA synchronous = FULL')
      self.mydb.execute('PRAGMA journal_mode = DELETE')
      self.mydb.execute("PRAGMA cache_size=200000")


  def __enter__(self):
    return self


  def __exit__(self, exc_type, exc_value, traceback):
    if self.leaveopen:
      self.conn.commit()
      self.mydb.close()


  def __call__(self, dbfname, columnvaluedict, tablename, ConditionalColumnValuedict=None ):
    """
    Dump values for table structured as a key-value dictionary in row with value
    of columnid equal to valofprimcol
    """
    if self.dbfname:
      raise Exception("The database file %s is already open." % self.dbfname)
    self.leaveopen = False
    self.dbfname = dbfname
# open the database and wait up to 16.6 hours for concurrent threads writing to the same file
    self.conn = sqlite3.connect(database = self.dbfname, timeout = 60000)
    self.mydb = self.conn.cursor()
    self.PutValuesinTable( columnvaluedict, tablename, ConditionalColumnValuedict )
    self.conn.commit()
    self.mydb.close()
    self.dbfname = None


  def lprint(self, mstr):
    lprint(mstr, self.logger)


  def GetWHEREstr(self, ConditionalColumnValuedict):
    wherestr = ""
    if ConditionalColumnValuedict:
      wherestr = "WHERE "
      for condcol, (condval, condtype) in ConditionalColumnValuedict.items():
        if wherestr != "WHERE ":
          wherestr += " AND "
        if isinstance(condval, str):
          condval = condval.replace("\"", "'") # mustn't use double quotes within a string
          wherestr += condcol + " = \"" + str(condval) + "\""
        else:
          wherestr += condcol + " = " + str(condval)
    return wherestr


  def _doesTableExist_(self, tablename):
  # first check for the existence of our table
    self.mydb.execute('select tbl_name from sqlite_master')
    list_tables = self.mydb.fetchall()
    for t in list_tables:
      if t[0] == tablename:
        return True
    return False


  def AddColumn(self, tablename, colname, coltype):
    # check if columns exists, if not create them
    self.mydb.execute("PRAGMA table_info( " + tablename + " )")
    columnnames = self.mydb.fetchall()
    if colname not in zip(*columnnames)[1]:
      sqlxprs = "ALTER TABLE %s ADD COLUMN %s %s" %(tablename, colname, coltype)
      self.lprint( column )
      self.mydb.execute(sqlxprs)
    else:
      self.lprint(colname + " already exists in " + tablename)



  def PutValuesinTable(self, colvaldict, tablename, ConditionalColumnValuedict=None,
    matchcolrowdict=None, onlyuniquerows=True ):
    """
    columnvaluedict is a row in a the table 'tablename' structured as a key-value dictionary
    with keys being the names of the columns and their values being the values of the columns.
    If ConditionalColumnValuedict is supplied then row values in columnvaluedict will be used to
    update the row where its values equals the values in ConditionalColumnValuedict.
    Otherwise a row with values in columnvaluedict will be appended to the table.
    """
    if not colvaldict: # sanity checks
      return
    columnvaluedict = {}
    for e in colvaldict.items():
      (name, (value, coltype)) = e
      if value != "NULL" and value != "None" and value != None:
        columnvaluedict[name] = (value, coltype)

    def stringswitchquotes(value):
      if not isinstance(value, str):
        return str(value)
      # sqlxprs may contain either " or ' but not both as this will confuse the sql engine
      if "'" in value:
        v =value.replace("'", "\"")
        value = v
      value = "'" + value + "' "
      return value

    def checkNumberTypes(coltype,value,tablename,column):
      if coltype.lower() == 'real':
        if not isinstance(value, float) and not isinstance(value, int) and value != "NULL" and value != "None":
          #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
          raise Exception("Expected a float value in %s for column %s" % (tablename, column))
      if coltype.lower() == 'int':
        if not isinstance(value, int) and value != "NULL" and value != "None":
          #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
          raise Exception("Expected an integer value in %s for column %s" % (tablename, column))

    defcolumnid = ""
    createtable = not self._doesTableExist_(tablename)
    # create table with columns specified in columnvaluedict
    #print columnvaluedict
    if createtable:
      # compose column-type string as in "LLGocc real, Nresoccwnd int, CCocclocals text"
      createcolstr = ""
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      #self.lprint( "Adding column(s) to %s:\n" %tablename )
      for column, (dummy, coltype) in columnvaluedict.items():
        if createcolstr:
          createcolstr +=", "
        createcolstr += column + " " + coltype
        self.lprint( column )
      # Otherwise create the table with columns
      # First create the main table
      sqlxprs = "CREATE TABLE %s ( %s )" %( tablename, createcolstr )
      self.mydb.execute(sqlxprs)

    # check if columns exists, if not create them
    self.mydb.execute("PRAGMA table_info( " + tablename + " )")
    columnnames = self.mydb.fetchall()
    for column, (value, coltype) in columnvaluedict.items():
      if column not in zip(*columnnames)[1]:
        sqlxprs = "ALTER TABLE %s ADD COLUMN %s %s" %(tablename, column, coltype)
        self.lprint( column )
        self.mydb.execute(sqlxprs)

      if matchcolrowdict:
        wherestr =  self.GetWHEREstr(matchcolrowdict)
        sval = str(value)
        if coltype=="text":
          sval = "\"" + value + "\""
        sqlxprs = "UPDATE %s SET %s = %s %s" %(tablename, column, sval, wherestr)
        print(sqlxprs)
        self.mydb.execute(sqlxprs)

    if onlyuniquerows:
      wherestr = self.GetWHEREstr(columnvaluedict)
      sqlxprs = "SELECT COUNT(*) FROM %s %s" %(tablename, wherestr)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      self.mydb.execute(sqlxprs)
      rows = self.mydb.fetchone()[0]
      if rows > 0:
        self.lprint( "Row in %s already present. It will not be duplicated." %tablename )
        return

    wherestr = self.GetWHEREstr(ConditionalColumnValuedict)
    sqlxprs = "SELECT COUNT(*) FROM %s %s" %(tablename, wherestr)
    self.mydb.execute(sqlxprs)
    crows = self.mydb.fetchone()[0]
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    # if the values in ConditionalColumnValuedict are not present add a new row
    if crows==0 or not ConditionalColumnValuedict or not matchcolrowdict:
      insertcols = ""
      insertvals = ""
      for column, (value, coltype) in columnvaluedict.items():
        if "NULL" == str(value).upper():
          continue
        if "AUTOINCREMENT" in coltype.upper():
          continue # as it would have been created above and must not be assigned values explicitly
        if insertcols:
          insertcols += ", "
          insertvals += ", "
        insertcols += column
        checkNumberTypes(coltype,value,tablename,column)
        if coltype == "text":
          v = stringswitchquotes(value)
        else:
          v= str(value)
        if v == '' or v=='None':
          v = 'NULL'
        if v == 'False':
          v = '0'
        if v == 'True':
          v = '1'
        insertvals += v
      try:
        sqlxprs = "INSERT INTO %s ( %s ) VALUES ( %s )" %(tablename, insertcols, insertvals)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        print(sqlxprs)
        self.mydb.execute(sqlxprs)
      except Exception as e:
        errstr = str(e) + "\nProblem with SQL expression:\n\n%s\n\n%s" %(sqlxprs, trcbck.format_exc())
        self.lprint(errstr)
        raise Sorry(errstr)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    # update the row that meets the conditions in ConditionalColumnValuedict
    else:
      if ConditionalColumnValuedict:
        try:
          sqlxprs=""
          for column, (value, coltype) in columnvaluedict.items():
            if coltype == "text":
              value = stringswitchquotes(value)
            checkNumberTypes(coltype,value,tablename,column)
            if "NULL" in str(value).upper():
              continue
            if rows==0:
              wherestr = ""
            sqlxprs = "UPDATE %s SET %s = %s %s" %(tablename, column, str(value), wherestr)
            print(sqlxprs)
            self.mydb.execute(sqlxprs)
        except Exception as e:
          errstr = str(e) + "\nProblem with SQL expression:\n\n%s\n\n%s" %(sqlxprs, trcbck.format_exc())
          self.lprint(errstr)
          raise Sorry(errstr)
    self.conn.commit()


  def GetNumberOfRows(self, tblname):
    if self._doesTableExist_(tblname):
      self.mydb.execute("SELECT COUNT(*) FROM %s" %tblname)
      return self.mydb.fetchone()[0]
    else:
      return 0


  def SQLexec(self, sqlxpr):
    self.mydb.execute(sqlxpr)
    return self.mydb.fetchone()[0]



def dbexecute(db, sqlxprs, verbose=False, logger = None):
  try:
    if verbose:
      lprint( sqlxprs, logger )
    db.execute(sqlxprs)
  except Exception as e:
    errstr = str(e) + "\nProblem with SQL expression:\n%s\n%s" \
       %(sqlxprs, "".join(trcbck.format_stack(limit=10)) )
    lprint( errstr, logger)
    raise Sorry(errstr)


class Updateinf():
  def __init__(self, updatecolumn = None, reftable = None, reftablecolumn = None, refmatchcolids = [ ]):
    self.updatecolumn = updatecolumn
    self.reftable = reftable
    self.reftablecolumn = reftablecolumn
    self.refmatchcolids = refmatchcolids



class Mergeinf():
  def __init__(self, mergetable = None, updates = [ ]):
    self.mergetable = mergetable
    self.updates = updates
    if self.updates != []:
      for u in self.updates:
        assert isinstance(u, Updateinf)



def MergeDbases(dbname, mergedbname, mergeinfs, logger = None):
# open the database and wait up to 16.6 hours for concurrent threads writing to the same file
  #verbose = True
  verbose = False
  #conn = sqlite3.connect(database = dbname, timeout = 60000, isolation_level="EXCLUSIVE")
  conn = sqlite3.connect(database = dbname, timeout = 60000)
  mydb = conn.cursor()
  mydb.execute('PRAGMA synchronous = FULL')
  mydb.execute('PRAGMA journal_mode = DELETE')
  mydb.execute("PRAGMA cache_size=200000")

  # ensure thread safety; when one process opens database no other may read it
  dbexecute(mydb, "BEGIN EXCLUSIVE TRANSACTION", verbose )
  sqlxprs = ""
  gettablesqlxprs = 'SELECT tbl_name %s FROM %s WHERE type="table" AND tbl_name IS NOT "sqlite_sequence"'
  try:
    dbexecute(mydb, gettablesqlxprs %("", "sqlite_master"), verbose )
    # table list comes out as [(u'MRTargetTbl',), (u'sqlite_sequence',)...]
    list_tables = [ e[0] for e in mydb.fetchall() ]
    dbexecute(mydb, 'ATTACH "%s" AS tomerge' %mergedbname, verbose )
    dbexecute(mydb, gettablesqlxprs %(", sql", "tomerge.sqlite_master"), verbose )
    other_tables_createstr = mydb.fetchall()
    for o, createstr in other_tables_createstr:
      if o not in list_tables:
        lprint( "%s was not found in %s. Creating empty one" %(o, dbname), logger)
        dbexecute(mydb, createstr, verbose)
    # All tables in tomerge.sqlite_master are now also present in sqlite_master
    # Check that every table in sqlite_master has all the columns in tomerge.sqlite_master
    dbexecute(mydb, gettablesqlxprs %("", "sqlite_master"), verbose )
    list_tables = [ e[0] for e in mydb.fetchall() ]
    dbexecute(mydb, gettablesqlxprs %(", sql", "tomerge.sqlite_master"), verbose )
    other_tables = [ e[0] for e in mydb.fetchall() ]

    def AddAnyMissingColumns(rows1, rows2, tablename):
      cols = [ r[1].lower() for r in rows1 ]
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      for dummy, column, coltype, dummy, dummy, dummy in rows2:
        if column.lower() not in cols:
          lprint( "Column %s in %s from %s not present in %s from %s. Adding empty column" \
           %( column, tablename, mergedbname, tablename, dbname), logger)
          sqlxprs = "ALTER TABLE %s ADD COLUMN %s %s" %(tablename, column, coltype)
          dbexecute(mydb, sqlxprs, verbose)

    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    for tablename in list_tables:
      if tablename not in other_tables:
        lprint( "%s not present in %s" %(tablename,  mergedbname), logger)
        continue
      """ list rows like
          (0, u'Comment', u'text', 0, None, 0)
          (1, u'PartialSolutionExists', u'int', 0, None, 0)
          (2, u'SolutionExists', u'int', 0, None, 0)
      """
      dbexecute(mydb, 'PRAGMA table_info(%s)' %tablename, verbose)
      rows = mydb.fetchall()
      dbexecute(mydb, 'PRAGMA tomerge.table_info(%s)' %tablename, verbose)
      otherrows = mydb.fetchall()
      # must have the same columns before executing INSERT INTO tbl SELECT * FROM below
      AddAnyMissingColumns(rows, otherrows, tablename)
      AddAnyMissingColumns(otherrows, rows, "tomerge." + tablename)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    # offset colid with value from refoffsetcolid in reftbl

    def MakeIndex(colname, tblname):
      sqlxprs = """
      SELECT * FROM sqlite_master WHERE type='index' AND name LIKE '%s%%' AND tbl_name='%s'
      """ %(colname, tblname)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      dbexecute(mydb, sqlxprs, verbose)
      rows = mydb.fetchall()
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      if len(rows):
        return
      sqlxprs ="CREATE INDEX %s_%s_idx ON %s(%s)" %(colname, tblname, tblname, colname)
      dbexecute(mydb, sqlxprs, verbose)

    other_tables = [e[0] for e in other_tables_createstr]
    #verbose = True
    for merge_update in mergeinfs:
      assert isinstance(merge_update, Mergeinf)
      if merge_update.mergetable not in other_tables:
        continue

      for update in merge_update.updates:
        lprint( "Updating %s in %s with the one from %s" %(merge_update.mergetable, dbname, mergedbname), logger )
        #setcol, reftbl, matchcols, reftblmatchcol = updateitem
        MakeIndex(update.updatecolumn, merge_update.mergetable)
        matchcolsstr = ""
        for matchcol in update.refmatchcolids:
          MakeIndex(matchcol, update.reftable)
          if matchcolsstr:
            matchcolsstr += " AND "
          #matchcolsstr += """r.%s = ( SELECT tr.%s FROM tomerge.%s tr WHERE tr.%s = %s )
          #""" %(matchcol, matchcol, update.reftable, update.reftablecolumn, update.updatecolumn)
          matchcolsstr += """r.%s = ( SELECT tr.%s FROM tomerge.%s tr WHERE tr.%s = tomerge.%s.%s )
          """ %(matchcol, matchcol, update.reftable, update.reftablecolumn, merge_update.mergetable, update.updatecolumn)
        updatestr = """UPDATE tomerge.%s SET %s = ( SELECT r.%s FROM %s r \n WHERE %s )
        """ %(merge_update.mergetable, update.updatecolumn, update.reftablecolumn, update.reftable, matchcolsstr)
        dbexecute(mydb, updatestr, verbose)

      lprint( "Appending %s in %s with the one from %s" %(merge_update.mergetable, dbname, mergedbname), logger )
      # Merge tables by appending toMerge.<mergetbl> to <mergetbl>
      # First get the exact order the columns appear in <mergetbl> to ensure
      # the INSERT INTO statement don't mess up columns during merging
      dbexecute(mydb, "PRAGMA table_info( " + merge_update.mergetable + " )", verbose)
      tbinfo = mydb.fetchall()
      colids = [ r[1] for r in tbinfo ]
      colidsstr = ""
      for colid in colids:
        if colidsstr:
          colidsstr += ", "
        colidsstr += colid

      sqlxprs = "SELECT %s FROM %s" %(colidsstr,  merge_update.mergetable)
      dbexecute(mydb, sqlxprs, verbose)
      rowsinexttable = mydb.fetchall()
      sqlxprs = "SELECT %s FROM toMerge.%s" %(colidsstr,  merge_update.mergetable)
      dbexecute(mydb, sqlxprs, verbose)
      row = mydb.fetchall()[0]
      #verbose = True
      if row in rowsinexttable:
        continue # don't duplicate rows

      sqlxprs = "INSERT INTO %s ( %s ) SELECT %s FROM toMerge.%s" %( merge_update.mergetable, colidsstr, colidsstr, merge_update.mergetable )
      dbexecute(mydb, sqlxprs, verbose)

  except Exception as e:
    lprint( str(e) + "\nProblem with SQL expression:\n\n%s\n\nfor %s and %s\n%s" \
       %(sqlxprs, dbname, mergedbname, trcbck.format_exc()), logger)
  finally:
    dbexecute(mydb, 'DETACH database tomerge', verbose)
    conn.commit()
    mydb.close()
