from __future__ import print_function

from phaser import style

class Text(object):
  """
  Renders text
  """

  def __init__(self, wrapper, text):

    self.wrapper = wrapper
    self.text = text


  def __call__(self, stream):

    stream.write( text = self.text, wrapper = self.wrapper )


  @classmethod
  def Wrapped(cls, text):

    return cls( wrapper = style.wrap_long_lines, text = text )


  @classmethod
  def Rewrapped(cls, text):

    return cls( wrapper = style.rewrap, text = text )


  @classmethod
  def Truncated(cls, text):

    return cls( wrapper = style.truncate_long_lines, text = text )


class Install(object):
  """
  Installs some formatting
  """

  def __init__(self, formatter):

    self.formatter = formatter


  def __call__(self, stream):

    stream.push( formatter = self.formatter )


  @classmethod
  def Left(cls):

    return cls( formatter = style.Aligned.Left() )


  @classmethod
  def Center(cls):

    return cls( formatter = style.Aligned.Center() )


  @classmethod
  def Right(cls):

    return cls( formatter = style.Aligned.Right() )


  @classmethod
  def Banner(cls):

    return cls( formatter = style.Banner() )


  @classmethod
  def Framed(cls):

    return cls( formatter = style.Framed() )


class Uninstall(object):
  """
  Removes formatting
  """

  def __call__(self, stream):

    stream.pop()


class Heading(object):
  """
  Writes a heading
  """

  def __init__(self, handler, text):

    self.handler = handler
    self.text = text


  def __call__(self, stream):

    stream.push( formatter = self.handler )
    stream.write( text = self.text, wrapper = style.wrap_long_lines )
    stream.pop()


  @classmethod
  def Title(cls, text):

    return cls( handler = style.Banner(), text = text )


  @classmethod
  def Section(cls, text):

    return cls( handler = style.Framed(), text = text )


  @classmethod
  def Underlined(cls, text, char):

    return cls( handler = style.Underlined( char = char ), text = text )


class Separator(object):
  """
  Separator line
  """

  def __init__(self, handler, text):

    self.handler = handler
    self.text = text


  def __call__(self, stream):

    stream.push( formatter = self.handler )
    stream.write( text = self.text, wrapper = style.truncate_long_lines )
    stream.pop()


  @classmethod
  def Filled(cls, text, char):

    return cls( handler = style.Filled( char = char ), text = text )


class SectionStart(object):
  """
  Starts a section
  """

  def __init__(self, indent = True):

    self.indent = indent


  def __call__(self, stream):

    if self.indent:
      stream.push( style.Indented.Block() )

    else:
      stream.push( style.Dummy() )


class SectionEnd(object):
  """
  Ends a section
  """

  def __call__(self, stream):

    stream.pop()



class UnorderedList(object):
  """
  Writes a list
  """

  def __init__(self, marker):

    self.marker = marker


  def __call__(self, stream):

    stream.push(
      style.State( data = style.ListData.Bullet( marker = self.marker ) )
      )
    stream.push( style.Dummy() )


  @classmethod
  def Level1(cls):

    return cls( marker = "o " )


  @classmethod
  def Level2(cls):

    return cls( marker = "- " )


class OrderedList(object):
  """
  Writes a list
  """

  def __init__(self, start):

    self.start = start


  def __call__(self, stream):

    stream.push(
      style.State(
        data = style.ListData.Numbered( start = self.start, width = 1 ),
        )
      )
    stream.push( style.Dummy() )


class ListItem(object):
  """
  Adds new item to list
  """

  def __init__(self, text):

    self.text = text


  def __call__(self, stream):

    stream.pop()
    owner = stream.top()
    marker = owner.data.next()
    stream.push( style.Indented( preamble = marker ) )
    stream.write( text = self.text, wrapper = style.wrap_long_lines )
    stream.pop()
    stream.push(
      style.Indented.Block( indent = len( marker ), fillchar = owner.data.fillchar )
      )


class ListEnd(object):
  """
  Signals end-of-list
  """

  def __call__(self, stream):

    stream.pop()
    stream.pop()


class TaskBegin(object):
  """
  A message and a possibly delayed another message
  """

  def __init__(self, message, endlength = None, delimiter = "... "):

    self.message = message + delimiter
    self.endlength = endlength


  def __call__(self, stream):

    if self.endlength is None:
      self.endlength = stream.width - len( self.message )

    strdel = "\0" * self.endlength
    paragraph = stream.format(
      paragraph = style.WrappedParagraph( lines = [ self.message + strdel ] ),
      )
    formatted = "\n".join( paragraph.lines )
    split = formatted.split( strdel )
    assert len( split ) == 2
    stream.rawwrite( text = split[0] )
    stream.flush()

    tdata = style.TaskData( reserved = self.endlength, ending = split[1] )
    stream.push( style.State( data = tdata ) )


class TaskEnd(object):
  """
  Print out delayed message
  """

  def __init__(self, message):

    self.message = message


  def __call__(self, stream):

    owner = stream.top()
    stream.rawwrite( text = self.message.ljust( owner.data.reserved ) )
    stream.rawwrite( text = owner.data.ending )
    stream.rawwrite( text = "\n" )
    stream.pop()


class ProgressBarStart(object):
  """
  Prepares a progress bar
  """

  def __init__(self, caption, parts, delimiter = "|", progress = "="):

    self.caption = caption
    self.parts = parts
    self.delimiter = delimiter
    self.progress = progress


  def __call__(self, stream):

    width = stream.width

    assert len( self.caption ) <= width

    if width < 2 * len( self.delimiter ) + len( self.progress ):
      raise ValueError("Stream width insufficient to draw shortest progress bar")

    ticks = ( width - 2 * len( self.delimiter ) ) // len( self.progress )
    hits_per_tick = ( self.parts - 1 ) // ticks + 1
    strdel = "\0" * len( self.delimiter )
    progspace = " " * len( self.progress ) * ( self.parts // hits_per_tick )

    paragraph = stream.format(
      paragraph = style.WrappedParagraph(
        lines = [
          self.caption,
          "",
          strdel + progspace + strdel,
          ],
        ),
      )
    formatted = "\n".join( paragraph.lines )
    split = formatted.split( strdel )
    assert len( split ) == 3
    stream.rawwrite( text = split[0] )
    stream.rawwrite( text = self.delimiter )

    pbdata = style.ProgressBarData(
      delimiter = self.delimiter,
      progress = self.progress,
      hits_per_tick = hits_per_tick,
      postwrite = split[2],
      )
    stream.push( style.State( data = pbdata ) )


class ProgressBarTick(object):
  """
  Adds a tick to the progress bar
  """

  def __call__(self, stream):

    owner = stream.top()
    stream.rawwrite( text = owner.data.increment() )
    stream.flush()


class ProgressBarEnd(object):
  """
  Renders as progress bar
  """

  def __call__(self, stream):

    owner = stream.top()
    stream.rawwrite( text = owner.data.delimiter )
    stream.rawwrite( text = owner.data.postwrite )
    stream.rawwrite( text = "\n" )
    stream.pop()


class Formattable(object):
  """
  An object that supports some formatting
  """

  def __init__(self, obj, getter):

    self.obj = obj
    self.getter = getter


  def __call__(self, stream):

    text = Text.Wrapped(
      text = self.getter( obj = self.obj, width = stream.width ),
      )
    text( stream = stream )


  @classmethod
  def FormatMethod(cls, obj):

    return cls(
      obj = obj,
      getter = lambda obj, width: obj.format( width = width ),
      )


  @classmethod
  def IotbxAlignment(cls, obj):

    return cls(
      obj = obj,
      getter = lambda obj, width: obj.simple_format( width = width ),
      )


class Composite(object):
  """
  Multiple actions
  """

  def __init__(self, actions):

    self.actions = actions


  def __call__(self, stream):

    for action in self.actions:
      action( stream = stream )
