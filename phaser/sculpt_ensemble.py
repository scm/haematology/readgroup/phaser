from __future__ import print_function

from phaser.logoutput_new import facade
from phaser import tbx_utils
from phaser import ensembler_new as ensembler
from phaser import sculptor_new as sculptor

import iotbx.phil
from libtbx.object_oriented_patterns import lazy_initialization

# Global constants
VERSION = "0.0.2"
PROGRAM = "SculptEnsemble"

LOGGING_LEVELS = tbx_utils.LOGGING_LEVELS

# Module exception
class SculptEnsembleError(Exception):
  """
  Error occurred in the sculpt_ensemble module
  """


class SequenceReadError(SculptEnsembleError):
  """
  Error occurred in the read_in_target_sequence_files function
  """


class AlignmentReadError(SculptEnsembleError):
  """
  Error occurred in the read_in_alignment_files function
  """

# PHIL
input_phil = """
structure
  .help = "Input structures"
  .optional = True
  .short_caption = PDB file
{
  model
    .help = "Input model file"
    .multiple = True
  {
    file_name = None
      .help = "Input file name"
      .type = path

    selection = None
      .help = "Selection string"
      .optional = True
      .type = str

    assembly = None
      .help = "Assembly information"
      .multiple = True
      .type = strings

    read_single_assembly_from_file = False
      .help = "Consider contents of the file as a single assembly"
      .type = bool

    errors
      .help = "Estimated errors for chain"
      .multiple = True
      .optional = True
    {
      %(errorfiles)s
    }
  }

  remove_alternate_conformations = False
    .help = "Remove alternate conformations"
    .type = bool
    .optional = False

  sanitize_occupancies = False
    .help = "Sets occupancies > 1.0 to 1.0"
    .type = bool
    .optional = False
}

alignment = None
  .help = "Input alignment file"
  .multiple = True
  .optional = True
  .short_caption = Sequence alignment file
  .style = auto_align
  .type = path

homology_search
  .help = "Alignment from homology search file"
  .multiple = True
  .optional = True
{
  file_name = None
    .help = "Homology search file"
    .type = path
    .optional = True

  use = None
    .help = "Which alignments to use"
    .type = ints( value_min = 1 )
    .optional = True
}

target_sequence = None
  .help = "Target sequence for assembly position"
  .type = paths
""" % {
  "alignment": sculptor.PHIL_INPUT_ALIGNMENT,
  "errorfiles": sculptor.PHIL_INPUT_ERRORFILES,
  }

output_phil = """
root = sculpt_ensemble
  .help = "Output file root"
  .type = str
  .optional = False

gui_output_dir = None
  .type = path
  .short_caption = Output directory
  .help = Sets base output directory for Phenix GUI - not used when \
  run from the command line.
  .style = output_dir bold
"""


master_phil = """
input
  .help = "Input files"
{
  %(input)s
}

output
  .help = "Output file(s)"
{
  %(output)s

  include scope libtbx.phil.interface.tracking_params
}

chain_to_alignment_matching
  .help = "Chain-to-alignment matching options"
{
  %(chain_alignment)s
}

error_search
  .help = "Parameters for matching error files and/or estimating errors"
{
  %(error_search)s
}

ensembling
  .help = "Options for ensembling"
{
  %(ensembling)s
}

sculpting
  .help = "Options for sculpting"
{
  disable = False
    .help = "No sculpting"
    .type = bool
    .optional = False

  %(sculpting)s
}""" % {
  "input": input_phil,
  "output": output_phil,
  "chain_alignment": sculptor.PHIL_CHAIN_ALIGNMENT,
  "error_search": sculptor.PHIL_ERROR_SEARCH,
  "ensembling": ensembler.mapping_and_superposition_phil,
  "sculpting": sculptor.PHIL_SCULPTING,
  }

parsed_master_phil = lazy_initialization(
  func = iotbx.phil.parse,
  input_string = master_phil,
  process_includes = True,
  )

master_params = iotbx.phil.parse(master_phil, process_includes=True)

def validate_params(params):

  from libtbx.utils import Sorry

  if not params.input.structure.model:
    raise Sorry("No input pdb files")

  for model in params.input.structure.model:
    for err in model.errors:
      if err.chain_ids is None:
        raise Sorry("Applicable chain_ids not specified for %s" % err.file_name)

  return True


def run_ensembler(assemblies, nchains, mappings, selections, params, logger):

  info = facade.Package( channel = logger.info )

  results = ensembler.process(
    assemblies = assemblies,
    ngroups = nchains,
    mappings = mappings,
    selections = selections,
    superposition = ensembler.SUPERPOSITION_CREATOR_FOR[ params.superposition.method ](
      weighting = ensembler.WEIGHTING_SCHEME_SETUP_FOR[ params.weighting.scheme ](
        params = params.weighting,
        ),
      params = params,
      ),
    clustering_distances = params.clustering,
    logger = logger,
    )

  info.heading( text = "Superposed assemblies", level = 4 )
  from phaser.logoutput_new import formatter
  number = formatter.Number.normal( decimal = 3 )
  from phaser.logoutput_new import LogError
  try:
    info.table_rows(
      rows = [
        [ str( r.assembly ), i, r.overlap, r.wrmsd, r.unwrmsd ]
        for ( i, r ) in enumerate( results, start = 1 )
        ],
      formatters = [ str, str, str, number, number ],
      headings = [ "Assembly", "Input", "Overlap", "Wrms", "Unwrms" ],
      rowheadings = 1,
      )
  except LogError as e:
    info.highlight("Table cannot be displayed")

  return results


def read_in_target_sequence_files(filenames, nchains, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  info.heading( text = "Target sequence files", level = 2 )
  targets = []

  if not filenames:
    info.unformatted_text( text = "No files specified" )
    targets.extend( [ None ] * nchains )

  else:
    import itertools
    info.ordered_list_begin()

    for ( fname, i ) in itertools.zip_longest( filenames, range( 1, nchains + 1 ) ):
      if fname is None:
        info.list_item( text = "Assembly position %s: no files specified" % i )
        targets.append( None )
        continue

      info.list_item_begin( text = "Assembly position %s: %s" % ( i, fname ) )
      p = tbx_utils.SequenceObject.from_file( file_name = fname )

      if not p.object:
        raise SequenceReadError("%s: input sequence is empty" % p.name)

      if 1 < len( p.object ):
        info.highlight( text = "Multiple sequences found, using only first" )

      verbose.heading( text = "Sequence:", level = 6 )
      verbose.sequence( sequence = p.object[0] )

      targets.append( p.object[0] )
      info.list_item_end()

    info.list_end()

  return targets


def read_in_alignment_files(filenames, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  alignments = []

  info.ordered_list_begin()

  for fname in filenames:
    info.list_item_begin( text = fname )
    p = tbx_utils.AlignmentObject.from_file( file_name = fname )

    if not p.object.alignments:
      raise AlignmentReadError("%s: input alignment is empty" % p.name)

    verbose.heading( text = "Alignment:", level = 6 )
    verbose.alignment( alignment = p.object )

    alignments.append( ( fname, p.object ) )
    info.list_item_end()

  info.list_end()

  return alignments


def find_relevant_alignment(
  alignment_data,
  target,
  min_hss_length,
  min_sequence_identity,
  logger,
  ):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )

  if target is None:
    info.unformatted_text( text = "Target sequence not provided for this position" )
    info.unformatted_text( text = "All available alignments will be searched" )
    availables = alignment_data

  else:
    info.heading( text = "Target sequence search", level = 5 )
    verbose.heading( text = "Target sequence:", level = 6 )
    verbose.sequence( sequence = target )
    info.ordered_list_begin()

    from phaser import chainalign
    availables = []

    for ( name, alignment ) in alignment_data:
      info.list_item_begin( text = name )
      info.task_begin( text = "Searching for target sequence" )

      try:
        best = chainalign.best_matching_alignment_sequence(
          master = target,
          aliseqs = alignment.alignments,
          gap = alignment.gap,
          min_hss_length = min_hss_length,
          min_identity_fraction = min_sequence_identity,
          )

      except chainalign.SequenceAlignFailure:
        info.task_end( text = "not found" )

      else:
        info.task_end( text = "found" )
        copy = alignment.copy()
        info.field( name = "Best match", value = copy.names[ best[0] ] )
        info.field( name = "Best match identities", value = best[1][1] )

        info.task_begin( text = "Assigning sequence #%d as target" % ( best[0] + 1 ) )
        copy.assign_as_target( index = best[0] )
        info.task_end()

        verbose.heading( text = "Rearranged alignment:", level = 6 )
        verbose.alignment( alignment = copy )
        availables.append( ( name, copy ) )

      info.list_item_end()

    if availables:
      info.field_sequence(
        name = "Associated alignments",
        value = [ p[0] for p in availables ],
        )

    else:
      info.unformatted_text( text = "No alignments found for this position" )
    info.list_end()

  return availables


def convert_to_alignment_sequence(alignment_data):

  aliseqs = []

  for ( name, alignment ) in alignment_data:
    aliseqs.extend(
      [
        sculptor.AlignmentSequence( name = name, alignment = alignment, index = i )
        for i in range( alignment.multiplicity() )
        ]
      )

  return aliseqs


def create_chain_alignment(
  name,
  chain,
  mmt,
  target_sequence,
  chainalign_params,
  alignments,
  logger,
  ):

  info = facade.Package( channel = logger.info )

  from phaser.chisellib import NoAlignment, ChainAlignment

  if not mmt.alignable():
    info.unformatted_text( text = "This molecule type cannot be aligned" )
    chainalignment = NoAlignment
    errors = sculptor.no_error_search_for

  else:
    try:
      ( strial, residues, rescodes ) = sculptor.chain_alignment(
        chain = chain,
        mmt = mmt,
        trials = alignments,
        params = chainalign_params,
        logger = logger,
        )

    except sculptor.SculptorError as e:
      info.unformatted_text( text = "Aligner: %s" % e )

      if target_sequence is not None:
        info.unformatted_text( text = "Target sequence provided for alignment" )
        ( alignment, index ) = sculptor.align_with_sequence(
          chain = chain,
          mmt = mmt,
          target = target_sequence,
          logger = logger,
          )
        strial = sculptor.AlignmentSequence(
          name = "target alignment",
          alignment = alignment,
          index = index,
          )
        info.task_begin( text = "Aligning with chain" )

        from phaser import chainalign

        aligner = chainalign.ChainAlignmentAlgorithm.from_params(
          rgs = chain.residue_groups(),
          mmt = mmt,
          annotation = "chain_%s" % chain.id,
          params = chainalign_params,
          )

        result = aligner(
          trial = chainalign.Trial(
            gapped = strial.sequence(),
            gap = strial.gap(),
            annotation = strial.name,
            )
          )
        info.task_end()
        residues = result.residue_alignment()
        rescodes = result.rescode_alignment()

      else:
        info.unformatted_text(
          text = "Associating with pseudo-alignment created from chain sequence",
          )
        from iotbx import bioinformatics
        residues = chain.residue_groups()
        rescodes = mmt.one_letter_sequence_for_rgs( rgs = residues )
        alignment = bioinformatics.alignment(
          names = [ "chain_sequence" ],
          alignments = [ rescodes ],
          )
        strial = sculptor.AlignmentSequence(
          name = "Pseudo alignment",
          alignment = alignment,
          index = 0,
          )

    info.unformatted_text( text = "'%s' matched with alignment" % name )
    info.alignment( alignment = strial.alignment() )

    chainalignment = ChainAlignment(
      target = sculptor.reformat_alignment_sequence(
        sequence = strial.target(),
        gap = strial.gap(),
        ),
      model = sculptor.reformat_alignment_sequence(
        sequence = strial.sequence(),
        gap = strial.gap(),
        ),
      residues = residues,
      rescodes = rescodes,
      )
    info.field_percentage(
      name = "Sequence identity",
      value = 100.0 * chainalignment.cumulative_identity_fraction(),
      digits = 2,
      )
    errors = sculptor.find_errors_for

  return ( chainalignment, errors )


# Main function
def run(phil, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  info.title( text = "%s version %s" % ( PROGRAM, VERSION ) )
  info.heading( text = "Configuration options", level = 2 )
  info.preformatted_text( text = phil.as_str() )
  info.separator()

  params = phil.extract()

  #
  # Input
  #

  # Read in PDB files
  info.heading( text = "Input structure files", level = 2 )
  info.field_sequence(
    name = "Superposable types",
    value = [ d.mmtype_name() for d in ensembler.SUPERPOSABLE_MMTYPE_DEFAULTS ],
    )

  processing_steps = [
    ( sculptor.remove_hydrogens, "Removing hydrogens" ),
    ( tbx_utils.split_all_chains_in, "Splitting multitype chains based on residue type" ),
    ]

  if params.input.structure.remove_alternate_conformations:
    processing_steps.append(
      ( sculptor.remove_alternate_conformations, "Removing alternate conformations" )
      )

  if params.input.structure.sanitize_occupancies:
    processing_steps.append(
      ( sculptor.sanitize_occupancies, "Sanitizing occupancies < 0.0 or > 1.0" )
      )

  from libtbx.utils import Sorry
  from phaser import mmtype
  import os.path
  selector = ensembler.ChainTypeSelector(
    mmtypes = [ d.mmtype() for d in ensembler.SUPERPOSABLE_MMTYPE_DEFAULTS ],
    )
  assemblies = []
  error_data_for = {}

  for model in params.input.structure.model:
    info.heading( text = os.path.basename( model.file_name ), level = 3 )
    info.field( name = "Full path", value = model.file_name )
    root = ensembler.read_structure_from(
      filename = model.file_name,
      selection = model.selection,
      logger = logger,
      )

    for ( method, description ) in processing_steps:
      info.task_begin( text = description )
      method( root )
      info.task_end()

    chain_accessed = ensembler.process_chains_from(
      root = root,
      mmtypes = mmtype.KNOWN_MOLECULE_TYPES,
      logger = logger,
      )

    if not chain_accessed:
      info.highlight( text = "No chains read from file, SKIPPING" )
      continue

    founds = ensembler.assign_chains_to_assemblies(
      filename = model.file_name,
      chain_accessed = chain_accessed,
      from_chains = model.assembly,
      is_single_assembly = model.read_single_assembly_from_file,
      selector = selector,
      logger = logger,
      )

    assemblies.extend( founds )

    # Read in errors
    try:
      error_data_for[ model.file_name ] = sculptor.read_in_error_files(
        error_scopes = model.errors,
        caption = "Estimated error files",
        logger = logger,
        )

    except sculptor.ErrorFileReadFailure as e:
      raise Sorry(e)

  # Sanity check on assemblies
  info.heading( text = "Summary", level = 3 )
  ( nchains, mmtypes ) = ensembler.check_assembly_consistency(
    assemblies = assemblies,
    logger = logger,
    )

  # Target sequences
  try:
    targets = read_in_target_sequence_files(
      filenames = params.input.target_sequence,
      nchains = nchains,
      logger = logger,
      )

  except SequenceReadError as e:
    raise Sorry(e)

  assert len( targets ) == nchains

  # Alignments
  info.heading( text = "Alignment files", level = 2 )
  aligroups = []

  if not params.input.alignment:
    info.unformatted_text( text = "No alignment files specified" )
    aligroups.extend( [ None ] * nchains )

  else:
    try:
      alidata = read_in_alignment_files(
        filenames = params.input.alignment,
        logger = logger,
        )

    except AlignmentReadError as e:
      raise Sorry(e)

    info.heading( text = "Alignment sorting", level = 4 )
    info.ordered_list_begin()
    caparams = params.chain_to_alignment_matching

    for i in range( nchains ):
      info.list_item_begin( text = "Alignment position %s" % ( i + 1 ) )
      relevants = find_relevant_alignment(
        alignment_data = alidata,
        target = targets[ i ],
        min_hss_length = caparams.min_hss_length,
        min_sequence_identity = caparams.min_sequence_identity,
        logger = logger,
        )
      aligroups.append( convert_to_alignment_sequence(alignment_data = relevants ) )
      info.list_item_end()

    info.list_end()

  assert len( aligroups ) == nchains

  # Read in homology_search files
  hsearchseqs = sculptor.read_in_homology_search_files(
    params = params.input.homology_search,
    logger = logger,
    )

  # Residue mappings
  info.heading( text = "Residue mappings", level = 1 )
  mappings = ensembler.determine_residue_mappings(
    assemblies = assemblies,
    nchains = nchains,
    mmtypes = mmtypes,
    params = params.ensembling.mapping,
    alignments = params.ensembling.alignment,
    default_for = dict(
      ( d.mmtype(), d.residue_mapping() ) for d in ensembler.SUPERPOSABLE_MMTYPE_DEFAULTS
      ),
    mapping_creator_for = ensembler.MAPPING_CREATOR_FOR,
    logger = logger,
    )

  # Atom selections
  info.heading( text = "Atom selections", level = 1 )
  selections = ensembler.determine_atom_selections(
    assemblies = assemblies,
    nchains = nchains,
    mmtypes = mmtypes,
    params = params.ensembling.atoms,
    default_for = dict(
      ( d.mmtype(), d.atom_selection() ) for d in ensembler.SUPERPOSABLE_MMTYPE_DEFAULTS
      ),
    logger = logger,
    )

  #
  # Calculation
  #

  # Superposition

  # Assemblies will be copied since this allows uniform treatment
  copies = [ a.copy_with_detached_chains() for a in assemblies ]

  # Prepare resulting structure
  import iotbx.pdb
  root = iotbx.pdb.hierarchy.root()
  chainids = iotbx.pdb.systematic_chain_ids()
  origin_remarks = []

  for ( index, assembly, ) in enumerate( copies, start = 1 ):
    model = iotbx.pdb.hierarchy.model()
    model.id = str( index )
    root.append_model( model )
    origin_remarks.append(
      "REMARK MODEL %s: %s, chains %s" % (
        model.id,
        assembly.annotation,
        ",".join( sorted( set( c.annotation.identifier for c in assembly.all_chains() ) ) ),
        )
      )

    # Unlikely to run out of chainids, so no check
    assembly.rename_chains( chainiditer = iter( chainids ) )

    for chain in assembly.all_chains():
      model.append_chain( chain.chain )

  info.heading( text = "Ensembling", level = 1 )

  if 1 < len( assemblies ):
    results = run_ensembler(
      assemblies = assemblies,
      nchains = nchains,
      mappings = mappings,
      selections = selections,
      params = params.ensembling,
      logger = logger,
      )

    assert len( results ) == len( copies )

    for ( assem, result ) in zip( copies, results ):
      assem.transform( transformation = result.transformation )

    errorsets = [ r.errors for r in results ]

  else:
    info.unformatted_text( text = "Single assembly, skipping step" )
    assert assemblies
    errorsets = [ [ None ] * len( assemblies[0].chains ) ]

  assert len( copies ) == len( errorsets )

  remarks = [ tbx_utils.get_phaser_revision_remark() ]

  if not params.sculpting.disable:
    # Create processor objects
    processor_for = sculptor.get_processor_for( params = params.sculpting )

    # Do processing
    from phaser import chisellib
    import itertools, sys

    if sys.version_info[0] > 2:
      ziplongest = itertools.zip_longest
    else:
      ziplongest = itertools.izip_longest

    info.heading( text = "Assembly processing", level = 1 )
    info.ordered_list_begin()
    error_params = params.error_search

    for ( index, ( original, assembly, errors ) ) in enumerate(
      zip( assemblies, copies, errorsets ),
      start = 1,
      ):
      info.list_item_begin( text = str( assembly ) )
      info.unordered_list_begin()
      assert len( assembly.chains ) == len( errors )
      chains = assembly.all_chains()
      ochains = original.all_chains()
      modelinfo = chisellib.ModelInfo( chains = [ c.chain for c in chains ] )
      chain_data = []

      for ( chain, ochain, sup_errors, aliseqs, tseq ) in ziplongest(
        chains,
        ochains,
        errors,
        aligroups,
        targets,
        ):
        info.list_item_begin( text = str( chain ) )
        info.field( name = "Type", value = chain.mmt.name )

        info.heading( text = "Alignment search", level = 3 )
        ( chainalignment, error_search_for ) = create_chain_alignment(
          name = str( chain ),
          chain = chain.chain,
          mmt = chain.mmt,
          target_sequence = tseq,
          chainalign_params = params.chain_to_alignment_matching,
          alignments = ( [] if aliseqs is None else aliseqs ) + hsearchseqs,
          logger = logger,
          )

        rms_errors = error_search_for(
          chain = ochain.chain,
          errors_for = error_data_for[ assembly.annotation.filename ],
          min_identity = error_params.min_sequence_identity,
          min_fraction = error_params.min_sequence_overlap,
          stage = "rms",
          logger = logger,
          )

        if rms_errors is None and error_params.calculate_if_not_provided:
          info.unformatted_text( text = "Requested error estimation when no error file provided" )
          info.paragraph_begin( indent = 2 )
          hmparams = error_params.homology_modelling

          try:
            with logger.child( shift = 0 ) as sublogger:
              rms_errors = sculptor.estimate_error_from_structure(
                chain = chain.chain,
                chainalignment = chainalignment,
                mmt = chain.mmt,
                min_sequence_coverage = error_params.min_sequence_overlap,
                homology_modelling_params = hmparams,
                output_file_prefix = "%s_%s_%s" % (
                  params.output.root,
                  "assembly-%s" % index,
                  "chain-%s" % ochain.chain.id,
                  ),
                logger = sublogger,
                )

          except sculptor.ErrorEstimationFailure as e:
            info.highlight( text = str( e ) )
            info.unformatted_text( text = "Using flat errors" )

          info.paragraph_end()

        sample = chisellib.Sample(
          name = str( chain ),
          chain = chain.chain,
          mmtype = chain.mmt,
          chainalignment = chainalignment,
          errors = rms_errors,
          superposition_errors = sup_errors,
          )

        info.heading( text = "Chain processing", level = 3 )
        processor = processor_for.get( sample.mmtype, chisellib.Discarder )
        info.task_begin( text = "Running processor for '%s'" % sample.mmtype.name )
        chaindiff = processor( sample = sample, model_info = modelinfo )
        info.task_end()

        modelinfo.register_change( chaindiff = chaindiff )
        chain_data.append( ( sample, chaindiff ) )
        info.list_item_end()

      info.list_end()

      cumseqid = sculptor.calculate_cumulative_identity( chain_data = chain_data )
      info.field_percentage(
        name = "Cumulative sequence identity",
        value = cumseqid,
        digits = 2,
        )
      remarks.append(
        tbx_utils.get_phaser_model_identity_remark( mid = index, identity = cumseqid )
        )

      info.heading( text = "Modifications", level = 3 )
      info.unordered_list_begin()

      for ( ( csamp, cdiff ), chain ) in zip( chain_data, chains ):
        info.list_item_begin( text = str( chain ) )
        count = sculptor.ChaindiffProcessor.log_and_execute(
          chaindiff = cdiff,
          channel = verbose,
          simplify = True,
          )
        info.field( name = "Number of modifications", value = count )
        info.list_item_end()

      info.list_end()
      info.list_item_end()

    info.list_end()

  # Output
  info.heading( text = "Output", level = 1 )
  info.field( name = "Output format", value = "PDB" )

  info.task_begin( text = "Regularizing hierarchy" )
  tbx_utils.regularize_hierarchy( root = root )
  info.task_end()

  info.task_begin( text = "Renumbering chains within models" )
  tbx_utils.renumber_chains_in_hierarchy( root = root )
  info.task_end()

  info.task_begin( text = "Preparing output file" )
  file_name = "%s_merged.pdb" % params.output.root

  with open( file_name, "w" ) as fout:
    for line in remarks + origin_remarks:
      fout.write( line )
      fout.write( "\n" )

    fout.write( root.as_pdb_string() )
    fout.write( "\n" )

  info.task_end()
  info.field( name = "PDB file written", value = file_name )
  ###
  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return file_name

# =============================================================================
# GUI-specific class for running command
from phaser import logoutput_new as logoutput
from phaser.logoutput_new import console
from libtbx import runtime_utils
from libtbx.utils import Sorry
import os

class launcher (runtime_utils.target_with_save_result) :
  def run (self) :
    os.makedirs(self.output_dir)
    os.chdir(self.output_dir)
    import sys
    from libtbx.utils import Sorry
    print(self.args)
    handlers = [console.Handler.FromDefaults(stream = sys.stdout, width=80)]
    logger = logoutput.Writer(
      handlers = handlers,
      level = LOGGING_LEVELS['info'],
      close_handlers = True,
      **LOGGING_LEVELS)

    with logger:
      output_file = run(
        phil = tbx_utils.gui_phil_process(
          master_phil = parsed_master_phil(),
          args = self.args),
        logger=logger)
    return output_file
