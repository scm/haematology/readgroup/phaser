#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      oeffner
#
# Created:     02/03/2016
# Copyright:   (c) oeffner 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from __future__ import print_function

import os, sys, os.path, posixpath, time
import math
import logging
from phaser import tbx_utils
from phaser import AlterOriginSymmate
from libtbx.cluster import HierarchicalClustering
from scitbx import golden_section_search
from builtins import input



# prompt user for value if it's not on the commandline
argn = 1
argc = len(sys.argv)
def Inputarg(varname):
  global argn
  global argc
  if argc > 1 and argn < argc:
    myvar = sys.argv[argn]
    argn = argn + 1
    print(varname + " " + myvar)
  else:
    myvar = input(varname)
  return myvar


def CalcFamos(fixsolfname, movsolfname, workdir, MRens, outputprefix, nproc, symfile, targetpdb, mlogger):
  mlogger.log(logging.INFO,"CalcFamos with %s %s\n" \
    %(os.path.relpath(fixsolfname), os.path.relpath(movsolfname)))
  if not os.path.exists(fixsolfname) or not os.path.exists(movsolfname):
    mlogger.log(logging.INFO,"%s or %s is missing. Skipping Calcfamos\n" \
      %(os.path.relpath(fixsolfname), os.path.relpath(movsolfname)))
    return 0, 0, 0, 0, 0, []
  #create phil object with original and perturbed MR solutions for AlterOriginSymmate
  AlterOriginSymmate.MyLogger = mlogger
  defaultphil = AlterOriginSymmate.GetDefaultPhil()
  params = defaultphil.extract()
  params.AltOrigSymMates.fixed.mrsolution.solfname = fixsolfname
  params.AltOrigSymMates.moving.mrsolution.solfname = movsolfname
  params.AltOrigSymMates.writepdbfiles=False
  #params.AltOrigSymMates.goodmatch = 0.0075
  #params.AltOrigSymMates.goodmatch = 0.3
  params.AltOrigSymMates.spacegroupfname = symfile
  params.AltOrigSymMates.workdir = workdir
  params.AltOrigSymMates.outputfileprefix = outputprefix
  params.AltOrigSymMates.nproc = nproc
  params.AltOrigSymMates.quiet = True
  for ens in MRens.keys():
    #creating the moving ensembles element which is multiple scope is ugly
    movens = defaultphil.objects[0].objects[0].objects[1].objects[1].extract()
    movens.name = ens
    movens.xyzfname = [MRens[ens][0]]
    params.AltOrigSymMates.moving.mrsolution.ensembles.append(movens)
    #creating the fixed ensembles element which is multiple scope is ugly
    fixens = defaultphil.objects[0].objects[1].objects[1].objects[1].extract()
    fixens.name = ens
    fixens.xyzfname = [MRens[ens][0]]
    params.AltOrigSymMates.fixed.mrsolution.ensembles.append(fixens)
  #dummy, nsols1, nfixsol, nmovsol, dummy2 = AlterOriginSymmate.RunPhil(params)
  run1 = AlterOriginSymmate.RunPhil()
  run1 = run1(params)
  run2 = AlterOriginSymmate.RunPhil() # just to get initial values
  if targetpdb:
    if run1.ngoodmatches:
      defaultphil = AlterOriginSymmate.Program.master_phil
      params = defaultphil.extract()
      params.AltOrigSymMates.fixed.mrsolution.solfname = outputprefix + ".sol"
      params.AltOrigSymMates.moving.pdb = targetpdb
      params.AltOrigSymMates.writepdbfiles=False
      params.AltOrigSymMates.goodmatch = 1.5
      params.AltOrigSymMates.outputfileprefix = outputprefix + "_" + os.path.splitext(os.path.basename(targetpdb))[0]
      params.AltOrigSymMates.workdir = workdir
      params.AltOrigSymMates.nproc = nproc
      for ens in MRens.keys():
        #creating the fixed ensembles element which is multiple scope is ugly
        fixens = defaultphil.objects[0].objects[1].objects[1].objects[1].extract()
        fixens.name = ens
        fixens.xyzfname = [MRens[ens][0]]
        params.AltOrigSymMates.fixed.mrsolution.ensembles.append(fixens)
      run2 = run2(params)
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  targetmlad = 99.9
  if len(run1.minAllAltOrigPdbFnames):
    targetmlad = run1.minAllAltOrigPdbFnames[0][0]
  mlogger.log(logging.INFO,"run1 ngoodmatches: %d, run2 ngoodmatches: %d, TargetMLAD: %f, run1 nfixsol: %d, run1 nmovsol: %d, run2 nfixsol: %d\n\n" \
   %(run1.ngoodmatches, run2.ngoodmatches, targetmlad, run1.nfixsol, run1.nmovsol, run2.nfixsol))
  return run1.ngoodmatches, run2.ngoodmatches, run1.nfixsol, run1.nmovsol, run2.nfixsol, run1.resultsmtrxlst



def MakeFamosMatrix(calclabel, workdir, MRens, movsolfnames, symfile, targetpdb=None, nthreads=1, mlogger=None):
  if not mlogger:
    mlogger = logging.getLogger("MakeFamosMatrix logger")
    handler= logging.StreamHandler(sys.stdout)
    mlogger.addHandler(handler)
  mlogger.log(logging.INFO,"Calculating the Famos matrix\n")

  bigrow = []
  solsset = set([])
  for n, movsolfname in enumerate(movsolfnames):
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    assert(os.path.exists(movsolfname))
    #nextsolfprefix = posixpath.join(MRfolder, self.calclabel + "{" + workdir + "}" + partmodels)
    #outputprefix = posixpath.join(MRfolder, self.calclabel + ","+ pertstr)
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    #CalcFamos(fixsolfname, movsolfname, MRfolder, MRens, outputprefix, nthreads)
    # successively filter all perturbed solutions that make it through CalcFamos
    #outputprefix = nextsolfprefix + "," + pertstr
    #CalcFamos(nextsolfprefix + ".sol", movsolfname, MRfolder, MRens, outputprefix, nthreads)
    #nextsolfprefix += "," + pertstr
    # make the Famos matrix
    #bigrow = []
    #for pert2 in perturbations[:i]:
      # just calculate lower triangular matrix elements since it's symmetric
      #pert2str = "%2.2f" %pert2
      #fixsol2fname = os.path.join(MRfolder, self.calclabel + pert2str + "CalcMR.sol")
    for m, fixsol2fname in enumerate(movsolfnames[:n]):
      assert(os.path.exists(fixsol2fname))
      #outputprefix = posixpath.join(MRfolder, self.calclabel + "Pert_%d_%d" + pertstr + "," + pert2str)
      outputprefix = posixpath.join(workdir, calclabel + "Pert_%d,%d" %(n,m))
      nsols1, nsols2, nfixsol, nmovsol, dummy, resmatrx = CalcFamos(fixsol2fname,
       movsolfname, workdir, MRens, outputprefix, nthreads, symfile, targetpdb, mlogger)

      bigrow.append((resmatrx, (os.path.basename(fixsol2fname), nfixsol,
        os.path.basename(movsolfname), nmovsol)))
      # make set of all solutions in files
      for i,e in enumerate(range(nfixsol)):
        solsset.add((os.path.basename(fixsol2fname), i))
      for i,e in enumerate(range(nmovsol)):
        solsset.add((os.path.basename(movsolfname), i))

      if nsols1 < 1:
        mlogger.log(logging.INFO,"Attention: %s.sol is empty. \n" %outputprefix)
      else:
        if nsols2 < 1 and targetpdb:
          mlogger.log(logging.INFO,"Attention: AltOrigSymMLAD_%s_%s.sol is empty. \n" \
           %(outputprefix, os.path.splitext(os.path.basename(targetpdb))[0]))
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  # make dictionary of the set
  lst = list(solsset)
  fnamesolsdict = {}
  for n,e in enumerate(lst):
    fnamesolsdict[n] = e
  return bigrow, fnamesolsdict


def GetDist(i, j, rows, fnamesolsdict):
  soli = fnamesolsdict[i]
  solj = fnamesolsdict[j]
  # define solutions from the same file as very distant so they'll never cluster
  if soli[0]==solj[0]:
    return 99.9
  for res in rows:
    for e in res[0]:
      # get MLAD value between solution i from file res[1][0] and solution j from file res[1][2]
      if soli == (res[1][0], e[5]) and solj==(res[1][2], e[6]) \
       or solj == (res[1][0], e[5]) and soli==(res[1][2], e[6]):
        return e[0]


def main():
  calclabel = Inputarg("Enter a name tag for the calculations: ")
  MRens = eval(Inputarg("Enter ensemble dictionary: "))
  movsolfnames = eval(Inputarg("Enter list of solution filenames: "))
  symfile = Inputarg("Enter symmetry filename: ")
  workdir=os.getcwd()

  mlogger = logging.getLogger("FamosClustering")
  mlogger.setLevel(logging.DEBUG)
  handler= logging.FileHandler(calclabel + "FamosCluster.log", mode='w', encoding = "UTF-8")
  handler.emit = tbx_utils.customEmit(handler) # use to omit linebreaks
  mlogger.addHandler(handler)
  handler1= logging.StreamHandler(sys.stdout)
  handler1.emit = tbx_utils.customEmit(handler1)  # use to omit linebreaks
  mlogger.addHandler(handler1)

  rows, fnamesolsdict = MakeFamosMatrix(calclabel, workdir, MRens,
    movsolfnames, symfile, nthreads=16, mlogger=mlogger)
  #print GetDist(26, 0, rows, fnamesolsdict)
  nsolutions = len(fnamesolsdict)
  mlogger.log(logging.INFO,"Clustering %d solutions\n" %nsolutions)
  mlogger.log(logging.INFO,"%s\n" %time.asctime( time.localtime(time.time())))
  cl = HierarchicalClustering(fnamesolsdict.keys(), lambda i,j: GetDist(i, j, rows, fnamesolsdict))
  # assuming every solution file contain the solution and that bad solutions do no cluster
  # we want number of clusters = nsolutions - len(movsolfnames)
  # and number of clusters < nsolutions but biased towards a good Zscore
  # i.e. where one cluster length is unusual
  def bestZscore(r):
    clr = cl.getlevel(r)
    avgln = 0
    avgln2 = 0
    for e in clr:
      avgln += len(e)
      avgln2 += len(e)**2
    avgln = float(avgln)/len(clr)
    avgln2 = float(avgln2)/len(clr)
    maxz = -1
    if avgln==avgln2:
      return maxz
    sigma = math.sqrt(avgln2 - avgln**2)
    for e in clr:
      z = (len(e) - avgln)/sigma
      maxz = max(maxz, z)
    return maxz

  def myfunc(r):
    clr = cl.getlevel(r)
    #ret = 0.5*abs((nsolutions - len(movsolfnames)) - len(clr)) - bestZscore(r)
    mine = 100
    for e in clr: # closer to minimum when a cluster length matches number of solution fil
      mine += min((len(e)-len(movsolfnames))**2, mine)
    return mine - 4*len(clr)

  def showclusters(r):
    lcl = []
    for e in cl.getlevel(r):
      if len(e)>1:
        mlogger.log(logging.INFO, str(e) + " " + str([fnamesolsdict[g] for g in e]) + "\n")
      if len(e) > len(lcl):
        lcl = e
    return lcl
  # find the r for achieveing the best clustering by minimising myfunc
  # myfunc(r) is not a continuous function so minimise with golden section search
  rmin = golden_section_search.gss(myfunc, 0.5, 0.000000005)
  bestclr = cl.getlevel(rmin)
  mlogger.log(logging.INFO, "Best clustering with distance: %f\n" %rmin)
  mlogger.log(logging.INFO, "Other clusters with more than 1 solution are:\n")
  largecluster = showclusters(rmin)
  mlogger.log(logging.INFO, "Largest cluster: %s\n" %str(largecluster))
  for i in largecluster:
    mlogger.log(logging.INFO, str(fnamesolsdict[i]) + "\n")
  mlogger.log(logging.INFO, "%s\n" %time.asctime( time.localtime(time.time())))
  import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )




if __name__ == '__main__':
  main()
