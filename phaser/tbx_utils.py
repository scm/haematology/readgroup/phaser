from __future__ import print_function

from libtbx.utils import Sorry

import os.path
import traceback as trcbck

# Command-line processing
def process_command_line(args, phil_master):

    import libtbx.phil
    from libtbx.phil import command_line

    argument_interpreter = command_line.argument_interpreter(
        master_phil = phil_master
        )

    phil_objects = []
    files = []
    unknowns = []

    for argument in args:
        if os.path.isfile( argument ):
            try:
                file_params = libtbx.phil.parse( open( argument ).read() )
                phil_objects.append( file_params )

            except Exception:
                files.append( argument )

        elif "=" in argument:
            try:
                command_line_params = argument_interpreter.process(
                    arg = argument
                    )
                phil_objects.append( command_line_params )

            except Exception:
                unknowns.append( argument )

        else:
            unknowns.append( argument )

    return ( phil_objects, files, unknowns )


def group_files_on_extension(files, **kwargs):

    sorted = {}
    unknowns = []

    for file_name in files:
        for ( type, extensions ) in kwargs.items():
            if any( [ file_name.endswith( ending ) for ending in extensions ] ):
                sorted.setdefault( type, [] ).append( file_name )
                break

        else:
            unknowns.append( file_name )

    return ( sorted, unknowns )


def phil_assign(parameter, value):

    import libtbx.phil
    return libtbx.phil.parse( "%s=%s" % ( parameter, value ) )


def autoroot(file_name, label):

    return "%s_%s" % ( os.path.splitext( file_name )[0], label )


def autoname(file_name, label, extension):

    return "%s.%s" % ( autoroot( file_name, label ), extension )


def choice_string(possible, default):

    if default is not None:
      return multichoice_string( possible = possible, defaults = [ default ] )

    else:
      return multichoice_string( possible = possible, defaults = [] )


def multichoice_string(possible, defaults):

    for d in defaults:
        if d not in possible:
            raise ValueError("Default not in allowed choices: %s" % d)

    return " ".join(
        [ "%s%s" % ( "*" if n in defaults else "", n ) for n in possible ]
        )


def get_banner(program, version):

    banner = [
        "#" * 79,
        ( "#" + ( "%s %s" % ( program, version ) ).center( 77 ) + "#" ),
        "#" * 79
        ]

    return "\n".join( banner )


def format_table(captions, formats, lines):

    count = len( captions )
    widths = [ len( c ) for c in captions ]
    wformats = [ "%%%ds" % w for w in widths ]
    table = [ "  ".join( captions ) ]

    for l in lines:
        assert len( l ) == count
        table.append(
            "  ".join(
                [ ( wf % ( f % v ) )[:w] for ( f, v, w, wf )
                    in zip( formats, l, widths, wformats ) ]
                )
            )

    return "\n".join( table )


def format_table_entries(captions, formats, lines, max_widths = None):

    columns = []
    widths = []

    if max_widths is None:
        max_widths = [ None ] * len( captions )

    for ( c, f, values, mw ) in zip( captions, formats, zip( *lines ), max_widths ):
        converteds = [ c ] + [ f % v for v in values ]
        assert values
        width = max( [ len( cv ) for cv in converteds ] )

        if mw is not None:
            width = min( width, mw )

        wformat = "%%%ds" % width
        columns.append( [ ( wformat % cv )[ : width ] for cv in converteds ] )

    return zip( *columns )


def assemble_table(lines):

    table = []

    for l in lines:
        table.append( "  ".join( l ) )

    return "\n".join( table )


def get_logger(name, out):

    import logging
    logger = logging.getLogger( name = name )

    if out:
        handler = logging.StreamHandler( out )

    else:
        handler = logging.NullHandler()

    handler.setFormatter( logging.Formatter( "%(message)s" ) )
    logger.addHandler( handler )
    logger.setLevel( logging.INFO )

    return ( logger, handler )


def get_handler(out):

    import logging

    if out:
        handler = logging.StreamHandler( out )

    else:
        handler = logging.NullHandler()

    handler.setFormatter( logging.Formatter( "%(message)s" ) )

    return handler


def create_output_folder(folder, logger):

    if os.path.isdir( folder ):
        logger.info( "Using existing output folder: %s" % folder )

    elif os.path.exists( folder ):
        logger.error(
            "Cannot create output folder: existing non-directory"
            )
        raise Sorry("Existing non-directory: %s" % folder)

    else:
        logger.info( "Creating output directory: %s" % folder )
        os.mkdir( folder )


def read_in_files(file_names, object_type):

    objs = []

    for f in file_names:
        objs.append( object_type.from_file( file_name = f ) )

    return objs


def extension(file_name):

    ( name, extension ) = os.path.splitext( file_name )
    return extension


PLURAL_FOR_NOUN = {
    "a": "",
    }

def inflect(word, count):

    if count == 1:
        return word

    if word in PLURAL_FOR_NOUN:
        return PLURAL_FOR_NOUN[ word ]

    return "%ss" % word


CONJUGATED_FOR_INFINITIVE = {
    "be": ( "is", "are" ),
    }


def conjugate(verb, count):

    if count == 1:
        if verb == "is":
            return "is"

        else:
            return "%ss" % verb

    else:
        if verb == "is":
            return "are"

        else:
            return verb

PHASER_MODEL_IDENTITY_REMARK = "REMARK PHASER ENSEMBLE MODEL %s ID %s"

def get_phaser_model_identity_remark(mid, identity):

    index = 0 if not mid else int( mid )
    return PHASER_MODEL_IDENTITY_REMARK % ( "%d" % index, "%.1f" % identity )


def parse_phaser_model_identity_remark(text):

    import re
    match = re.match(
        PHASER_MODEL_IDENTITY_REMARK % ( r"\s*(\d+)", r"(\d*\.\d+)" ),
        text
        )

    if not match:
        raise ValueError("Incorrect remark format")

    mid = match.group( 1 )
    return ( "" if mid == "0" else "%4s" % mid, float( match.group( 2 ) ) )


def dummy_cryst1_record():

    from iotbx.pdb import format_cryst1_and_scale_records
    from cctbx import uctbx, sgtbx, crystal
    return format_cryst1_and_scale_records(
        crystal_symmetry = crystal.symmetry(
            unit_cell = uctbx.unit_cell( parameters = [ 1, 1, 1, 90, 90, 90 ] ),
            space_group_info = sgtbx.space_group_info( number = 1 )
            )
        )


class ChainInfo(object):
    """
    Stores information about chains
    """

    def __init__(self, pdb, m_index, c_index, identity):

        self.pdb = pdb
        self.m_index = m_index
        self.c_index = c_index
        self.identity = identity


    @property
    def model(self):

        return self.pdb.object.models()[ self.m_index ]


    @property
    def chain(self):

        return self.model.chains()[ self.c_index ]


    @property
    def identifier(self):

        return ( self.pdb.name, self.model.id, self.chain.id, self.c_index )


    def __str__(self):

        return "chain (%s, '%s', '%s')" % (
            self.pdb.name,
            self.model.id,
            self.chain.id
            )


class PDBObject(object):
    """
    Opened and parsed PDB file
    """

    def __init__(self, root, name, remarks = (), crystal = None):

        self.name = name
        self.object = root
        self.remarks = remarks
        self.crystal = crystal


    def match_identity_records(self):

        cid_in = {}

        for r in self.remarks:
            try:
                ( mid, identity ) = parse_phaser_model_identity_remark( text = r )

            except ValueError:
                continue

            cid_in.setdefault( mid, [] ).append( identity )

        identity_for = {}

        for model in self.object.models():
            for ( chain, identity ) in zip( model.chains(), cid_in.get( model.id, [] ) ):
                identity_for[ chain ] = identity

        return identity_for


    def get_chain_infos(self):

        infos = []
        identity_for = self.match_identity_records()

        for ( m_index, model ) in enumerate( self.object.models() ):
            for ( c_index, chain ) in enumerate( model.chains() ):
                infos.append(
                    ChainInfo(
                        pdb = self,
                        m_index = m_index,
                        c_index = c_index,
                        identity = identity_for.get( chain )
                        )
                    )

        return infos


    @classmethod
    def from_file(cls, file_name):

        import iotbx.pdb

        try:
            inp = iotbx.pdb.input(file_name=file_name,
              raise_sorry_if_format_error=True)

        except IOError as e:
            raise Sorry(e)

        return cls(
            root = inp.construct_hierarchy(),
            name = file_name,
            remarks = inp.remark_section(),
            crystal = inp.crystal_symmetry(),
            )


    @classmethod
    def from_gzipped_file(cls, file_name):

        import gzip

        try:
            data = gzip.GzipFile( file_name ).read()

        except IOError as e:
            raise Sorry(e)

        return cls.from_string(
            pdb_str = data,
            name = file_name
            )


    @classmethod
    def from_string(cls, pdb_str, name):

        import iotbx.pdb

        inp = iotbx.pdb.input( lines = pdb_str, source_info = name )
        return cls(
            root = inp.construct_hierarchy(),
            name = name,
            remarks = inp.remark_section(),
            crystal = inp.crystal_symmetry(),
            )


class AlignmentObject(object):
    """
    Opened and parsed alignment file
    """

    def __init__(self, alignment, name):

        if not alignment.alignments:
            raise Sorry("Empty alignment: %s" % name)

        self.name = name
        self.object = alignment


    @classmethod
    def from_file(cls, file_name):

        try:
            ali_str = open( file_name , "rt").read()

        except IOError as e:
            raise Sorry("Error while reading %s: %s" % (file_name, e))

        return cls.from_string(
            ali_str = ali_str,
            extension = extension( file_name = file_name ),
            name = file_name
            )


    @classmethod
    def from_string(cls, ali_str, extension, name):

        from iotbx import bioinformatics
        parser = bioinformatics.alignment_parser_for_extension( extension = extension )

        if not parser:
            raise Sorry("Unknown format: %s" % extension)

        ( ali, errors ) = parser( ali_str )

        if errors:
            raise Sorry("Incorrect format:\n%s" % ali_str)

        assert ali

        return cls( alignment = ali, name = name )


class SequenceObject(object):
    """
    Opened and parsed alignment file
    """

    def __init__(self, sequences, name):

        self.name = name
        self.object = sequences


    @classmethod
    def from_file(cls, file_name):

        try:
            seq_str = open( file_name ).read()

        except IOError as e:
            raise Sorry("Error while reading %s: %s" % (file_name, e))

        return cls.from_string( seq_str = seq_str, name = file_name )


    @classmethod
    def from_string(cls, seq_str, name):

        from iotbx import bioinformatics
        ( seqs, errors ) = bioinformatics.parse_sequence( data = seq_str )

        if errors or not seqs:
            raise Sorry("Incorrect format:\n%s" % seq_str)

        for seq in seqs:
            if not seq.name:
                seq.name = os.path.splitext( os.path.basename( name ) )[0]

        return cls( sequences = seqs, name = name )


class HomologySearchObject(object):
  """
  Opened and parsed homology search data
  """

  def __init__(self, search, name):

    self.name = name
    self.object = search


  @classmethod
  def from_file(cls, file_name):

    instream = open( file_name )

    if file_name.endswith( ".hhr" ):
      from iotbx import bioinformatics

      try:
        result = bioinformatics.hhsearch_parser( output = instream.read() )

      except ValueError as e:
          raise Sorry(e)

    elif file_name.endswith( ".xml" ):
      from iotbx.bioinformatics import ncbi_blast_xml

      parsers = [
        ncbi_blast_xml.parse,
        ]

      for p in parsers:
        try:
          result = p( source = instream )

        except RuntimeError as e:
          instream.seek( 0 )
          continue

        break

      else:
          raise Sorry("No XML parser was able to process this file")

    else:
        raise Sorry("Unknown homology search file")

    return cls( search = result, name = file_name )


class FileArgument(object):
    """
    A file
    """

    def __init__(self, file_name):

        self.file_name = file_name


    def process(self, accumulator):

        accumulator.process_file( argument = self )


    def extension(self):

        return os.path.splitext( self.file_name )[1]


    def __str__(self):

        return "%s <file>" % self.file_name


class PhilArgument(object):
    """
    A phil read from a file
    """

    def __init__(self, phil):

        self.phil = phil


    def process(self, accumulator):

        accumulator.process_phil( argument = self )


    def __str__(self):

        return "Phil:\n%s" % self.phil.as_str()


    @classmethod
    def from_string(cls, string):

        import libtbx.phil

        try:
            phil = libtbx.phil.parse( string )

        except Exception:
            raise ValueError("Incorrect PHIL format")

        return cls( phil = phil )


    @classmethod
    def from_file(cls, file_name):

        return cls.from_string( string = open( file_name ).read() )


    @classmethod
    def command_line_argument(cls, interpreter, argument):

        try:
            phil = interpreter.process( arg = argument )

        except Exception:
            raise ValueError("Incorrect PHIL command line argument")

        return cls( phil = phil )


class UnknownArgument(object):
    """
    An argument that is unknown
    """

    def __init__(self, argument):

        self.argument = argument


    def process(self, accumulator):

        accumulator.process_unknown( argument = self )


    def __str__(self):

        return self.argument


class PhilArgumentFactory(object):

    def __init__(self, master_phil):

        from libtbx.phil import command_line
        self.interpreter = command_line.argument_interpreter(
            master_phil = master_phil
            )


    def __call__(self, argument):

        if os.path.isfile( argument ):
            try:
                phil = PhilArgument.from_file( file_name = argument )

            except ValueError:
                phil = FileArgument( file_name = argument )

        elif "=" in argument:
            try:
                phil = PhilArgument.command_line_argument(
                    interpreter = self.interpreter,
                    argument = argument
                    )

            except ValueError:
                phil = UnknownArgument( argument = argument )

        else:
            phil = UnknownArgument( argument = argument )

        return phil


class PhilAccumulator(object):
    """
    Unifies Phil command line handling (using the visitor pattern)
    """

    def __init__(self, master_phil):

        self.master_phil = master_phil
        self.phils = []

        self.file_handlers = []
        self.unknown_handlers = []


    def register_file_handler(self, handler):

        self.file_handlers.append( handler )


    def process_file(self, argument):

        for handler in self.file_handlers:
            try:
                result = handler( argument = argument, phils = self.phils )

            except RuntimeError:
                continue

            break

        else:
            self.process_unknown( argument = argument )


    def process_phil(self, argument):

        self.phils.append( argument.phil )


    def process_unknown(self, argument):

        for handler in self.unknown_handlers:
            try:
                handler( argument = argument, phils = self.phils )

            except RuntimeError:
                continue

            break

        else:
            raise Sorry("Unknown argument: %s" % argument)


    def merge(self):

        return self.master_phil.fetch( sources = self.phils )


class ExtensionFileHandler(object):

    def __init__(self, extensions, template):

        self.extensions = extensions
        self.template = template


    def __call__(self, argument, phils):

        if argument.extension() in self.extensions:
            import libtbx.phil
            return phils.append(
                libtbx.phil.parse( self.template % argument.file_name )
                )

        raise RuntimeError("Cannot process argument: %s" % argument)


def display_in_coot(pdb_file, mtz_file):

    SCRIPT_TEMPLATE = (
        "read_pdb( '%s' ); graphics_to_ca_representation(0);"
        + "set_show_symmetry_master( 1 ); symmetry_as_calphas( 0, 1 );"
        + "auto_read_make_and_draw_maps( '%s' )"
        )

    from subprocess import Popen
    import os
    env = os.environ.copy()
    del env [ "LD_LIBRARY_PATH" ]
    p=Popen(
        ( "coot", "--python", "-c", SCRIPT_TEMPLATE % ( pdb_file, mtz_file ) ),
        env = env,
        )
    p.communicate()


class PhilChoice(object):
  """
  A convenience class to generate a choice string, but also preserve individual
  choices
  """

  def __init__(self, choices, default):

    self.choices = choices
    self.default = default


  def __str__(self):

    return choice_string( possible = self.choices, default = self.default )


class PhilMultiChoice(object):
  """
  A convenience class to generate a choice string, but also preserve individual
  choices
  """

  def __init__(self, choices, defaults):

    self.choices = choices
    self.defaults = defaults


  def __str__(self):

    return multichoice_string( possible = self.choices, defaults = self.defaults )


def select_array(arrays, labin = None):

  if labin is None:
    if 1 < len( arrays ):
        raise Sorry((
                "Multiple arrays present: use the labin keyword to select\n"
                + "Known arrays:\n%s" % "\n".join(ma.info().label_string() for ma in arrays)
        ))

    selected = arrays[0]

  else:
    for ma in arrays:
      if ma.info().label_string() == labin:
        selected = ma
        break

    if selected is None:
        raise Sorry("Unknown array: %s\nKnown arrays:\n%s" % (
            labin,
            "\n".join(ma.info().label_string() for ma in arrays),
        ))

  return selected


def regularize_hierarchy(root):

  for model in list( root.models() ):
    for chain in list( model.chains() ):
      for rg in list( chain.residue_groups() ):
        for ag in list( rg.atom_groups() ):
          for a in list( ag.atoms() ):
            if a.occ < 0.005:
              ag.remove_atom( a )

          if not ag.atoms():
            rg.remove_atom_group( ag )

        if not rg.atoms():
          chain.remove_residue_group( rg )

      if not chain.atoms():
        model.remove_chain( chain )

    if not model.atoms():
      root.remove_model( model )

  root.atoms_reset_serial()


def renumber_chains_in_hierarchy(root):

  import iotbx.pdb
  chainids = iotbx.pdb.systematic_chain_ids()

  for model in root.models():
    for ( chain, cid ) in zip( model.chains(), chainids ):
      chain.id = cid


def parse_phil(phil, *args, **kwargs):

  import libtbx.phil
  return libtbx.phil.parse( phil, *args, **kwargs )


def download_pdb_chain(identifier, chain, mirror):

  entry = download_pdb_entry( identifier = identifier, mirror = mirror )
  root = parse_pdb_entry( entry = entry, source = mirror )
  return select_pdb_chain( root = root, chain = chain )


def download_pdb_entry(identifier, mirror):

  from iotbx.pdb import fetch

  stream = fetch.fetch( id = identifier, mirror = mirror )
  return stream.read()


def parse_pdb_entry(entry, source = "PDB"):

  import iotbx.pdb
  root = iotbx.pdb.input( lines = entry, source_info = source).construct_hierarchy()

  if not root.atoms():
      raise ValueError("Empty file")

  return root


def select_pdb_chain(root, chain):

  asc = root.atom_selection_cache().selection( "chain '%s'" % chain )
  croot = root.select( asc, True )

  if not croot.atoms():
      raise ValueError("No atoms after chain selection")

  return croot.models()[0].chains()[0]


def residue_groups_in_from_first_appearance_of(rgs, mmtypes):

  iterator = (
    i for ( i, rg ) in enumerate( rgs )
    if any( mmt.recognise_residue_group( rg = rg ) for mmt in mmtypes )
    )

  try:
    index = next(iterator)

  except StopIteration:
    return []

  return [ rgs[ i ] for i in range( index, len( rgs ) ) ]


def residue_groups_in_from_last_appearance_of(rgs, mmtypes):

  others = []

  for rg in reversed( rgs ):
    if any( mmt.recognise_residue_group( rg = rg ) for mmt in mmtypes ):
      break

    others.append( rg )

  others.reverse()

  return others


def transfer_to_new_chain_from(chain, rgs):

  import iotbx.pdb
  other_chain = iotbx.pdb.hierarchy.chain()
  other_chain.id = chain.id

  for rg in rgs:
    chain.remove_residue_group( rg )
    other_chain.append_residue_group( rg )

  return other_chain


def split_pdb_multitype_chain(chain):

  from phaser import mmtype

  rgs = chain.residue_groups()
  solvents = residue_groups_in_from_first_appearance_of(
    rgs = rgs,
    mmtypes = [ mmtype.SOLVENT ],
    )
  nonsolvents = rgs[ : len( rgs ) - len( solvents ) ]

  nonmacromols = residue_groups_in_from_last_appearance_of(
    rgs = nonsolvents,
    mmtypes = [ mmtype.PROTEIN, mmtype.DNA, mmtype.RNA ],
    )
  macromols = nonsolvents[ : len( nonsolvents ) - len( nonmacromols ) ]
  heteros = residue_groups_in_from_last_appearance_of(
    rgs = nonmacromols,
    mmtypes = [ mmtype.MONOSACCHARIDE ],
    )

  nonheteros = nonmacromols[ : len( nonmacromols ) - len( heteros ) ]
  sugars = residue_groups_in_from_first_appearance_of(
    rgs = nonheteros,
    mmtypes = [ mmtype.MONOSACCHARIDE ],
    )

  structurals = nonheteros[ : len( nonheteros ) - len( sugars ) ]
  new_groups = []

  for group in [ macromols, structurals, sugars, heteros, solvents ]:
    if group:
      new_groups.append( group )

  assert sum( len( c ) for c in new_groups ) == len( rgs )
  new_groups.pop( 0 )

  return [
    transfer_to_new_chain_from( chain = chain, rgs = group ) for group in new_groups
    ]


def split_all_chains_in(root):

  count = 0

  for model in root.models():
    newchains = []

    for chain in model.chains():
      newchains.extend( split_pdb_multitype_chain( chain = chain ) )

    for chain in newchains:
      model.append_chain( chain )

    count += len( newchains )

  return count

LOGGING_LEVEL_INFO = 40
LOGGING_LEVEL_VERBOSE = 20
LOGGING_LEVEL_DEBUG = 0

LOGGING_LEVELS = {
  "info": LOGGING_LEVEL_INFO,
  "verbose": LOGGING_LEVEL_VERBOSE,
  "debug": LOGGING_LEVEL_DEBUG,
  }


class ModelAnnotation(object):
  """
  Annotation for a model
  """

  def __init__(self, identifier):

    self.identifier = identifier


  def __str__(self):

    return "model '%s'" % self.identifier


  @classmethod
  def from_model(cls, model):

    return cls( identifier = model.id )


class ChainAnnotation(object):
  """
  Annotation for a chain
  """

  def __init__(self, identifier, resid_begin, resid_end):

    self.identifier = identifier
    self.resid_begin = resid_begin
    self.resid_end = resid_end


  def __str__(self):

    return "chain '%s', resi '%s' -> '%s'" % (
      self.identifier,
      self.resid_begin,
      self.resid_end,
      )


  @classmethod
  def from_chain(cls, chain):

    rgs = chain.residue_groups()

    return cls(
      identifier = chain.id,
      resid_begin = rgs[0].resid(),
      resid_end = rgs[-1].resid(),
      )


def gui_phil_process(master_phil, args):

  # assume all arguments from the GUI are PHIL format files
  import libtbx.phil
  phils = []

  for arg in args:
    assert os.path.isfile( arg )
    with open( arg ) as ifile:
      data = ifile.read()
      phils.append( libtbx.phil.parse( data ) )

  return master_phil.fetch( sources = phils )


class StreamDownload(object):
  """
  A simple class to read a stream
  """

  def __init__(self, opener, use_closing_context_manager = False):

    self.opener = opener

    if use_closing_context_manager:
      self.streamopen = self.closing_wrapped_open

    else:
      self.streamopen = self.direct_open


  def direct_open(self, name):

    return self.opener( name )


  def closing_wrapped_open(self, name):

    from contextlib import closing
    return closing( self.opener( name ) )


  def __call__(self, name):

    with self.streamopen( name ) as stream:
      return stream.read()


  @classmethod
  def PdbFetch(cls, data_type = "pdb", format = "pdb", mirror = "rcsb"):

    import iotbx.pdb.fetch
    import functools

    return cls(
      opener = functools.partial(
        iotbx.pdb.fetch.fetch,
        data_type = data_type,
        format = format,
        mirror = mirror,
        ),
      use_closing_context_manager = True,
      )


def get_atom_by_name(atoms, name):

  for a in atoms:
    if a.name == name:
      return a

  raise KeyError("Not found in list")


def get_phaser_revision_string():
  from phaser import Output
  o = Output()
  vstr = o.version_number() + " svn:" + o.svn_revision()  + " git:" + o.git_rev() + \
    " SHA-1:" +  o.git_shorthash()  + " " +  o.git_branchname()
  return vstr


def get_phaser_revision_remark():
  return "REMARK PHASER REVISION %s" %get_phaser_revision_string()
