#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Robert Oeffner
#
# Created:     17/02/2016
# Copyright:   (c) oeffner 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# This file is used by phaser\phaser\AlterOriginSymmate.py

from __future__ import print_function

from copy import deepcopy
from fractions import Fraction
import itertools
from boost_adaptbx.boost import rational
from types import *
import sys, os, math #, re
import mmtbx.alignment
from iotbx import pdb
from cctbx import sgtbx
from mmtbx import model
from scitbx.array_family import flex
import ccp4io_adaptbx, cctbx
from libtbx.utils import Sorry
from libtbx.phil import parse
from phaser import ensembler
from iotbx.pdb import amino_acid_codes
import logging
from scitbx import golden_section_search
if sys.version_info[0] > 2:
  from io import StringIO
else:
  from cStringIO import StringIO
from phaser.utils2 import *
from mmtbx.command_line import sort_hetatms
import random, string
import libtbx.callbacks # to propagate warnings to the GUI



chainid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
debugfolder = "AltOrigSymMatesFiles"

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
  return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def isCloseOrGreater(a, b):
  if a > b:
    return True
  if isclose(a, b):
    return True
  return False

# Null logger used only for ensembler to work
class NullHandler(logging.Handler):
  #No output
  def emit(self, record):
    pass

class accumulate_print(object):
  def __init__(self, quiet):
    self.quiet = quiet
    self.parts = ""
  def __call__(self, mstr):
    if not self.quiet:
      self.parts += mstr
  def get(self):
    return self.parts


class out_print(object):
  def __init__(self, out, quiet):
    self.quiet = quiet
    self.out = out
    self.parts = ""
  def __call__(self, mstr):
    if not self.quiet:
      self.parts += mstr
      for f in self.out.file_objects:
        end = ''
        if hasattr(f,'name') and (f.name == '<stdout>' or f.name == '<stderr>'):
          end = '\n'
        print(mstr, end=end,file=f)
  def get(self):
    return self.parts


class MLADstuff(object):
  def __init__(self, chainid1, chainid2, mlad=9e99, altorigstr="", mladstr="",
   pdbhiearchy=None, mladaltorigsymtrans=None):
    self.chainid1=chainid1
    self.chainid2=chainid2
    self.mlad = mlad
    self.altorigstr= altorigstr
    self.mladstr= mladstr
    if pdbhiearchy:
      self.pdbhiearchy = pdbhiearchy.deep_copy()
    if mladaltorigsymtrans:
      self.mladaltorigsymtrans = deepcopy(mladaltorigsymtrans)

  def __str__(self):
    return self.__repr__()
  def __hash__(self):
    return hash(str(self))
  def __repr__(self): # for debug printing purposes
    return "MLADstuff(%s, %s, %f, %s)" %(self.chainid1, self.chainid2,self.mlad, self.mladstr)
  def __eq__(self, other):
    if self.mlad == other.mlad: # and self.altorigstr == other.altorigstr \
      return True
    else:
      return False


class MLADstuffSet(set):
  def add(self, s):
    for e in self:
      if s == e:
        return
    super(MLADstuffSet, self).add(s)


class MinMLADstuffSet(object):
  def __init__(self):
    self.myset = MLADstuffSet([])
  def size(self):
    return len(self.myset)
  def AddIfSmallerOrEqual(self, minmladstuff):
    newset = MLADstuffSet([])
    if len(self.myset) ==0:
      self.myset.add(minmladstuff)
      return
    for e in self.myset:
      if isCloseOrGreater(minmladstuff.mlad, e.mlad):
        newset.add(e)
      if e.mlad == minmladstuff.mlad:
        newset.add(e)
        newset.add(minmladstuff)
      if isCloseOrGreater(e.mlad, minmladstuff.mlad):
        newset.add(minmladstuff)
    self.myset = newset



def CalcAltOrigSymmates(moving, fixed, movehetatms, xtalsym,
     chosenorigin, usebestSSM, usesymmop, debug, wpath, writepdbfiles, goodmatch,
     strictly_protein, verbose, mediocre_mlad, nproc, outputfileprefix, pdb_out,
     mlogger, quiet, write_cif=False):

  if nproc == 1:
    thrdprint = out_print(mlogger, quiet) # assuming mlogger is a filestream
  else:
    thrdprint = accumulate_print(quiet) # just concatenate output into one big string

  movingname = moving[0]
  fixedname = fixed[0]
  xtalmovingname, movext = os.path.splitext(os.path.split(movingname)[1])
  fixedrefname, fixext = os.path.splitext(os.path.split(fixedname)[1])
  if write_cif==True:
    movext = ".cif"
    fixext = ".cif"
  elif write_cif==False:
    movext = ".pdb"
    fixext = ".pdb"
  if movehetatms:
    myrand = random.SystemRandom()
# label with unique random string to ensure another process running GetAltOrigSymmates
# on the same file doesn't remove the file pdbxtalmovingname
    randstr = "".join(myrand.sample(string.ascii_letters+string.digits, 5))
    pdbxtalmovingname = xtalmovingname + "_" + randstr + "_movhetatms" + movext
    sort_hetatms.run(args=["file_name=" + movingname, "output_file=" \
     + pdbxtalmovingname, "model_skip_expand_with_mtrix=True"])
  else:
    pdbxtalmovingname = movingname
  mlogstr = ""
# use movingname in the log file rather than pdbxtalmovingname which may contain random characters
  pdbxtal = pdb.input(file_name= pdbxtalmovingname)
  pdbtest = pdb.input(file_name= fixedname)
  logger = logging.getLogger() # Null logger used only for ensembler to work
  logger.addHandler(NullHandler())

  sg = xtalsym.space_group()
  sgnumber = xtalsym.space_group_info().type().number()
  symops = sg.all_ops()
  nsymop = len(symops)
  uc = xtalsym.unit_cell()
  movinghierarchy = pdbxtal.construct_hierarchy()
  fixedhierarchy = pdbtest.construct_hierarchy()

  def IfTooFewPeptides(chain, minnumber=6):
    c = 0
    for rg in chain.residue_groups():
      if amino_acid_codes.one_letter_given_three_letter.get(rg.unique_resnames()[0]):
        c += 1
    return c < minnumber

  nmovchains = 0
  for chain in movinghierarchy.models()[0].chains():
    if strictly_protein and chain.is_protein()==False:
      continue
    if  IfTooFewPeptides(chain):
      continue
    nmovchains +=1

  nfixchains = 0
  for chain in fixedhierarchy.models()[0].chains():
    if strictly_protein and chain.is_protein()==False:
      continue
    if  IfTooFewPeptides(chain):
      continue
    nfixchains +=1

  if nfixchains < 1 or nmovchains < 1:
    raise Sorry("One of the coordinate files has no chains")

# if there is a polar axis it will be present in any of the alternative origins
  origlst = list_origins(sgnumber)
  hasfloatingorig = origlst[1]
  firstorig = origlst[0][0]
  #if hasfloatingorig:
  #  BestRpolarmin = 0.0
# we inspect firstorig below for that
  altorigs = [([0.0, 0.0, 0.0],["0","0","0"])]
  if chosenorigin == "Any":
    altorigs = GetAllOriginsAsFloatsnStrings(sgnumber)
  else:
# parse user's explicit alternative origin string
# change a string like "1/2, 2 /3 , 5/ 6" to ["1/2", "2/3", "5/6"]
    strvec = [ s.strip() for s in chosenorigin.split(",") ]
    def zero_or_rational(v):
      try:
        return rational.from_string(v)
      except ValueError:
        return 0.0
    frcvc = [zero_or_rational(e) for e in strvec]
    floatvec = [float(e) for e in frcvc]
# replace components with x,y,z if there is a polar axis along those directions
    fvecpolar = [
     firstorig[i] if isinstance(firstorig[i], str) else floatvec[i] for i in range(0,3)
    ]
    strvec1 = [
     firstorig[i] if isinstance(firstorig[i], str) else strvec[i] for i in range(0,3)
    ]
# form tuple of floatvector and string vector of user's explicit alternative origin
    altorigs = [(fvecpolar, strvec1)]

  altorigs = list(altorigs)
  norig = len(altorigs)
  isFloatingXorig = isinstance(altorigs[0][0][0], str)
  isFloatingYorig = isinstance(altorigs[0][0][1], str)
  isFloatingZorig = isinstance(altorigs[0][0][2], str)

  stdoutstr = "%s has %d chain(s)" %(xtalmovingname, nmovchains)
  stdoutstr += "\n%s has %d chain(s)" %(fixedrefname, nfixchains)
  stdoutstr += "\nSpacegroup in %s is %s, " %(xtalmovingname, xtalsym.space_group_info().symbol_and_number())
  if isFloatingXorig or isFloatingYorig or isFloatingZorig:
    stdoutstr += "floating origin along: "
    if isFloatingXorig:
      stdoutstr += "X "
    if isFloatingYorig:
      stdoutstr += "Y "
    if isFloatingZorig:
      stdoutstr += "Z "
  stdoutstr += "\n"
  thrdprint(stdoutstr)
# bail out if unequal number of chains in moving and fixed PDB
  if nfixchains != nmovchains:
    stdoutstr = "Warning: Ambiguous input. Not the same number of chains in the two PDB files.\n"
    stdoutstr += "Results may be unexpected"
    thrdprint(stdoutstr)

  altorigpdbfname = pdb_out
  veryhigh = 99
  BestRpolarmin = veryhigh
  sortedAllAmbiguousOrigins = []
  while BestRpolarmin==veryhigh or len(sortedAllAmbiguousOrigins):
    AllAmbiguousOrigins = []
    AllAltOrigPdbFnames = []
    AltOrigSymMLADs = []
    pdbminmladmovhierarchies = []
    pdbminmladmovchainids = []
    goodinfostr =""
    polaraxisminstr = ""
    alignstr = ""
    fixedchainids = []
# ------------------ looping over fixed chains -----------------------------------
    for (ifixch, fixedchain) in enumerate(fixedhierarchy.models()[0].chains()):
      # ignore HETATM chains such as waters, ligands or heme groups
      AltOrigSymMLADs.append([])
      if fixedchain.is_protein() == False and strictly_protein or IfTooFewPeptides(fixedchain):
        continue

      fixedchainids.append( fixedchain.id )
      testhierarchy = pdb.hierarchy.new_hierarchy_from_chain( fixedchain )
      fixed_chainid = testhierarchy.models()[0].chains()[0].id
      testatoms = testhierarchy.atoms()
      testcentre = GetCentreOfMolecule(testatoms)
  # compute centre of coordinates in fractional space
      fractesthierarchy = testhierarchy.deep_copy()
      fractestatoms = fractesthierarchy.atoms()
      testsites_frac = uc.fractionalize(sites_cart=fractestatoms.extract_xyz())
      fractestatoms.set_xyz(testsites_frac)
  # get mean fractional position of the test atoms
      testsites_frac = uc.fractionalize(sites_cart= testatoms.extract_xyz())
      meantestsitesfrac = testsites_frac.mean()
      minmladstr =""
      altorigstr =""
      ranfloat = random.uniform(0.0, 7.0)
      movhierarchyminmlad = [veryhigh + ranfloat, veryhigh]
      movminmladstr = ""
      movminaltorigstr = ""
      pdbminmladmovhierarchy = None
      minfixchainmlad = veryhigh
      minmladstuffset = MinMLADstuffSet() # make an empty set of candidate good matches
       # display selected info on the GUI through callbacks
      libtbx.call_back(message="showinfo", data= ("", (0,0,0), True) )
  # ------------------ looping over moving chains -----------------------------------
      for thischain in movinghierarchy.models()[0].chains():
        # ignore HETATM records such as waters or heme groups
        if thischain.is_protein() == False and strictly_protein or IfTooFewPeptides(thischain):
          continue

        ranfloat = random.uniform(0.0, 7.0)
        minmladrmsd = [veryhigh + ranfloat, veryhigh]

        use_mmtbx_alignment = False
        ssm_crashed = False
        pdbminmlad = None

        if movehetatms:
          pdbhetatmhierarchy = GetHETATM(thischain.id, movinghierarchy)

        pdb_hierarchy = pdb.hierarchy.new_hierarchy_from_chain( thischain )
        pdb_chain_id = pdb_hierarchy.models()[0].chains()[0].id
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        AltOrigSymMLADs[ifixch].append([veryhigh + ranfloat, veryhigh, pdb_chain_id, fixed_chainid, "", "", [], ("","",""), 0])
        original_sites = pdb_hierarchy.atoms().extract_xyz()
  # compute ssm alignments once, before using them in subsequent for-loops
        mov_ref_chains = [pdb_hierarchy.models()[0].chains()[0],
                 testhierarchy.models()[0].chains()[0]]
        alignments = []
        sortQvals = []
        transvec = [0,0,0]

        try:
          ssmobj = ccp4io_adaptbx.SecondaryStructureMatching( moving = mov_ref_chains[0],
           reference = mov_ref_chains[1])
          # store translation if we have P1 symmetry
          transvec = [ssmobj.ssm.t_matrix[3], ssmobj.ssm.t_matrix[7], ssmobj.ssm.t_matrix[11]]
          Qvalues = ssmobj.GetQvalues()
          Qvalsindex = [(i, e, transvec, "SSM") for (i,e) in enumerate(Qvalues)]
  # make list with the best SSM Q-score first
          sortQvals = sorted(Qvalsindex, key= lambda Qvindx: Qvindx[1], reverse=True)

          if usebestSSM: # then use the SSM alignment with the highest Q-score
            sortQvals = [sortQvals[0]]
  # make list of alignments and Q-scores
          for qi in sortQvals:
            ssmobj.AlignSelectedMatch(qi[0])
            #trvec = ssmobj.ssm.
            alignments.append( (ccp4io_adaptbx.SSMAlignment.residue_groups(match = ssmobj), qi) )

        except Exception as m:
          stdoutstr = "SSM alignment of chains %s and %s failed. Trying mmtbx alignment instead." \
           %(fixed_chainid, pdb_chain_id)
          thrdprint(stdoutstr)
          use_mmtbx_alignment = True
          ssm_crashed = True

  # always append an mmtbx alignment just in case a gene duplication in the SSM alignment yields a poor MLAD value
        finally:
          resmap2 = mmtbxalign(mov_ref_chains[0], mov_ref_chains[1])
          alignments.append((resmap2, (0, 1.0, transvec, "MMTBX")))

        stdoutstr = "\n\nIdentifying smallest MLAD between chain %s in %s and chain %s in %s" \
         %(pdb_chain_id, xtalmovingname, fixed_chainid, fixedrefname)
        stdoutstr += "\nfor the %d allowed symmetry operations and %d allowed alternative origin(s) using %d alignment(s)." \
         %(nsymop, norig, len(sortQvals))
        thrdprint(stdoutstr)
  # ------------------ looping over ssm alignments -----------------------------------
        for j,ali in enumerate(alignments):
          n = 0
          ci = 0
          qi = ali[1]
          nssm = (qi[0]+1) # number of this alignment
          alimethod = qi[3]
          allmlads = []
          if xtalsym.space_group_info().type().number() == 1: # P1 xtal
            # if P1 then use the translation vector suggested by ssm as the best translation
            # between molecules
            fracvec = uc.fractionalize(qi[2])
            altorigs = [( fracvec, ["x", "y", "z"] )]

          minshortoutstr = ""
          minaltorigsymopmlad = [veryhigh + ranfloat, veryhigh]
          labels = "#symm_op, symm_op, #alt_origin, alt_origin, UC_shift, #%s, %s(Q), label, mlad\n" \
           %(alimethod, alimethod)
          labelstr = labels
  # try the mmtbx alignment if the SSM alignments produces a bad MLAD value
          if not use_mmtbx_alignment and j == (len(alignments)-1):
            break
          else:
            if use_mmtbx_alignment and j == (len(alignments)-1) and ssm_crashed == False \
             and (debug or verbose):
              stdoutstr = "\nCurrent best MLAD value is poor. Trying an mmtbx alignment.\n"
              thrdprint(stdoutstr)
          if debug and writepdbfiles:
            fname = "%s_UC%d_%s_align%d.cif" %(xtalmovingname, n, fixedrefname, nssm)
            allsitesfname = os.path.join(wpath, debugfolder, fname)
            stdoutstr = "\nWriting sites of the %d. alignment to %s" %(nssm, allsitesfname)
            thrdprint(stdoutstr)
            allsites = open(allsitesfname, "w")
  # ------------------ looping over symmetry operations -----------------------------------
          for (symcount, symop) in enumerate(symops):
            # if we only want to study alternative origins then we only use the identity operation
            if usesymmop == False and symcount > 0:
              break

            rot = symop.r()
            trans = symop.t()
            altorig = [0.0, 0.0, 0.0]
  # ------------------ looping over alternate origins -----------------------------------
            for (oc, genuninealtorig) in enumerate(altorigs):
              # if P1 then use the translation vector suggested by ssm as the best translation
              # between molecules
              if not isFloatingXorig or xtalsym.space_group_info().type().number() == 1: # if P1 xtal
                altorig[0] = genuninealtorig[0][0]
              if not isFloatingYorig or xtalsym.space_group_info().type().number() == 1: # if P1 xtal
                altorig[1] = genuninealtorig[0][1]
              if not isFloatingZorig or xtalsym.space_group_info().type().number() == 1: # if P1 xtal
                altorig[2] = genuninealtorig[0][2]

              atoms = pdb_hierarchy.atoms()
  # put all xtal atoms back to original position
              atoms.set_xyz(original_sites)
  # get us into fractional coordinates
              sites_frac = uc.fractionalize(sites_cart=atoms.extract_xyz())
  # get the symmetry mate w.r.t. this space group
              new_sites = rot.as_double() * sites_frac + trans.as_double()
  # translate atom sites to alternative origin
              altorig_new_sites = new_sites + altorig
  # cast new origin back to unit cell around the test atoms
              na = 0; nb = 0; nc = 0
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[0] > 0.5:
                altorig_new_sites = altorig_new_sites - (1, 0, 0)
                na -=1
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[0] < -0.5:
                altorig_new_sites = altorig_new_sites + (1, 0, 0)
                na +=1
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[1] > 0.5:
                altorig_new_sites = altorig_new_sites - (0, 1, 0)
                nb -=1
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[1] < -0.5:
                altorig_new_sites = altorig_new_sites + (0, 1, 0)
                nb +=1
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[2] > 0.5:
                altorig_new_sites = altorig_new_sites - (0, 0, 1)
                nc -=1
              while DiffVec(altorig_new_sites.mean(), meantestsitesfrac)[2] < -0.5:
                altorig_new_sites = altorig_new_sites + (0, 0, 1)
                nc +=1

              unitcellshifts = (na,nb,nc) # count how many unit cell shifts we do
  # translate xtalpdb to alternative origin as well as integral numbers of unitcells vectors
              atoms.set_xyz(altorig_new_sites)
  # cast back to cartesian coordinates
              atoms.set_xyz(uc.orthogonalize(sites_frac= altorig_new_sites))
  # compute vector between alignments in fractional space for shifting atoms along floating axis
  # compute distance between the two structures using ssm
              resstats = None
              ssm_rmsd_Cachains = None
              if hasattr(ali[0], "pairs"):
                resmap = ali[0].pairs
                resstats = ali[0].stats
                resstats2 = ali[0].stats2
                # Get the subset of atoms used for the ssm rmsd calculation
                # These are the ones counted as Nalign in ssm_superpose
                # i.e the ones where ssmalignment.stats[] is not None
                ssm_rmsd_resmap = [ p for (p,s) in  zip(ali[0].pairs, ali[0].stats) if s ]
                ssm_rmsd_Cachains = ensembler.to_atom_mapping(ssm_rmsd_resmap, lambda atom:atom.name==' CA ', logger)
              else: # just a plain tuple of pairs from mmtbx alignment
                resmap = ali[0]
  # map C-alpha atoms with one another, some possibly to none
              atommap = ensembler.to_atom_mapping(resmap, lambda atom:atom.name==' CA ', logger)
  # don't use gap atoms
              CAchains = ensembler.intersect_selection(atommap, logger)
              if not ssm_rmsd_Cachains:
                ssm_rmsd_Cachains = CAchains
              #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
              if len(CAchains) == 0:
                raise Exception("No C-alpha atoms in chain %s aligned with ones in %s." % (fixed_chainid, pdb_chain_id))
  # identify the same atom in the moved xtal
              CAchainzip = list(zip(*CAchains))

              xtalalignmentcentre = GetCentreOfMolecule(CAchainzip[0]) # only used if we have a floating origin
              testalignmentcentre = GetCentreOfMolecule(CAchainzip[1]) # only used if we have a floating origin
              mladrmsd = (GetMLADfromAtoms(CAchains), GetRMSDfromAtoms(ssm_rmsd_Cachains))
  # if alternative origin has a floating axis then shift it back along this axis
              unitfloataxis = [0.0, 0.0, 0.0]
              if isFloatingXorig:
                unitfloataxis[0] = 1.0
              if isFloatingYorig:
                unitfloataxis[1] = 1.0
              if isFloatingZorig:
                unitfloataxis[2] = 1.0
              polarvec = [0.0, 0.0, 0.0]
              tmpsites = atoms.extract_xyz().deep_copy()
# polaraxisoverlap computes GetMLADfromAtoms for this symmetry copy positioned at r along the polar axis
              def polaraxisoverlap(r):
                polvec = [unitfloataxis[0]*r, unitfloataxis[1]*r, unitfloataxis[2]*r]
                cartfloatingoffset = uc.orthogonalize(polvec)
                atoms.set_xyz(tmpsites - tuple(cartfloatingoffset))
                retval = GetMLADfromAtoms(CAchains)
                return retval

              if BestRpolarmin == veryhigh:
                if debug:
  # make table of MLAD values along floating axis for each symops and origins
                  r_ini = -0.5
                  r1 = r_ini
                  mlads = [ "sym%dorig%d" %(symcount+1, oc+1)] # column label for table
                  incr = 200
                  invincr = 1.0/incr
                  for i in range(1,incr):
                    r1 = r1 + invincr
                    mlad = polaraxisoverlap(r1)
                    mlads.extend([mlad])
                  allmlads.append(mlads)
  # find the best overlap along the polar axis with a golden section minimisation which is fast and robust
                rpolarmin = -9e99
                if not debug:
                  rpolarmin = golden_section_search.gss(polaraxisoverlap, -0.5, 0.5)
                else:
                  dmpstr = StringIO()
                  rpolarmin = golden_section_search.gss(polaraxisoverlap,
                   -0.5, 0.5, out = dmpstr, monitor_progress=True)
                  tmpstr = dmpstr.getvalue()
                  dmpstr.close()
                  # get rid of double line breaks from the gss output
                  stdoutstr = "\n".join( tmpstr.split("\n\n"))
                  thrdprint("\n" + stdoutstr)
  # calling polaraxisoverlap(rpolarmin) will move atom coordinates meaning that
  # calling GetRMSDfromAtoms(CAchains) will yield a corresponding value
                mladrmsd = (polaraxisoverlap(rpolarmin), GetRMSDfromAtoms(ssm_rmsd_Cachains) )
                polaraxisminstr = ", Smallest MLAD for fractional polar offset by: %f" %rpolarmin
                if debug:
                  thrdprint(polaraxisminstr)
              else: # determined the best value along polar axis previously
                rpolarmin = BestRpolarmin
                polaraxisoverlap(rpolarmin)
                sortedAllAmbiguousOrigins = [] # to exit from while loop
              polarvec = [unitfloataxis[0]*rpolarmin,
                          unitfloataxis[1]*rpolarmin,
                          unitfloataxis[2]*rpolarmin
                         ]
              v = flex.vec3_double([altorig]) + flex.vec3_double([polarvec])
              altorigsymtrans = (symop, v, flex.vec3_double([unitcellshifts]), flex.vec3_double([polarvec])) # flex.vec3_double([floatingoffset]))
  # compute mlad between the two structures
              mladrmsd = (GetMLADfromAtoms(CAchains), GetRMSDfromAtoms(ssm_rmsd_Cachains))
              if mladrmsd[0] > goodmatch:
                continue
  # make a deep copy as we are about to change the chain id which otherwise would crash SSM that assumes chain ids remain constant
              filepdbhiearchy = pdb_hierarchy.deep_copy()
              if debug: # because we print out all transformations
                filepdbhiearchy.models()[0].chains()[0].id += chainid[ci]

              symopname = symop.as_xyz()
              altorigstr = "(%s, %s, %s)" %(genuninealtorig[1][0], genuninealtorig[1][1],
               genuninealtorig[1][2])
              outstr = "%d. symm.op: (%s), %d. origin: %s, UC shift: %s, %d. %s Q: %2.3f, %s" \
               %(symcount+1, symopname, oc+1, altorigstr, str(unitcellshifts), nssm, qi[3], qi[1],
               filepdbhiearchy.models()[0].chains()[0].id)
              shortoutstr = "%d. (%s), %d. %s, %s, %d. %2.3f, %s, %3.3f" \
               %(symcount+1, symopname, oc+1, altorigstr, str(unitcellshifts), nssm, qi[1],
               filepdbhiearchy.models()[0].chains()[0].id, mladrmsd[0])

              if isCloseOrGreater(minaltorigsymopmlad[0], mladrmsd[0]):
                minaltorigsymopmlad = mladrmsd
                minshortoutstr = labels + shortoutstr
              if isCloseOrGreater(minmladrmsd[0], mladrmsd[0]):
                minmladrmsd = mladrmsd
                minaltorigstr = altorigstr
                minmladstr = outstr
                if isFloatingXorig or isFloatingYorig or isFloatingZorig:
                  minmladstr = outstr + polaraxisminstr

                pdbminmlad = filepdbhiearchy.deep_copy()
                minmladaltorigsymtrans = deepcopy(altorigsymtrans)

              if isCloseOrGreater(minfixchainmlad, mladrmsd[0]): # display selected info on the GUI through callbacks
                minfixchainmlad = mladrmsd[0]
                if minfixchainmlad < 1.5:
                  colour = (0, 150, 0)
                  goodinfostr += "Fixed chain %s and moving chain %s match the same MR solution\n" \
                   %(fixed_chainid, filepdbhiearchy.models()[0].chains()[0].id)

                else:
                  if minfixchainmlad >= 1.5 and minfixchainmlad < 2.0:
                    colour = (100, 100, 0)
                  else:
                    colour = (200, 0, 0)
                infostr = "Chains fixed: %s, moving: %s, Symm_op: (%s), Origin: %s, MLAD: %3.3f" \
                 %(fixed_chainid, filepdbhiearchy.models()[0].chains()[0].id,
                    symopname,  altorigstr, mladrmsd[0])
                libtbx.call_back(message="showinfo", data= (infostr, colour, False) )

                if resstats:
                  alignstr = "\n" + str(nssm) + ". alignment. " + MakeAlignstr2(zip(atommap, resstats, resstats2))
                else:
                  alignstr = "\n" + str(nssm) + ". alignment. " + MakeAlignstr(atommap)

              if debug or verbose:
                thrdprint("\r" + labelstr + shortoutstr + "                   " )
              labelstr = ""
              minmladstuff = MLADstuff(fixed_chainid, filepdbhiearchy.models()[0].chains()[0].id,
                minmladrmsd[0], minaltorigstr, minmladstr, filepdbhiearchy, minmladaltorigsymtrans)
              minmladstuffset.AddIfSmallerOrEqual(minmladstuff)

              if debug and writepdbfiles:
  # place a gold atom on the centroid for the aligned moving C-alpha atoms and dump the lot as a pdb file
                newxtalalignmentcentre = GetCentreOfMolecule(CAchainzip[0])
                matom = pdb.hierarchy.atom()
                matom.set_xyz((newxtalalignmentcentre[0], newxtalalignmentcentre[1],
                 newxtalalignmentcentre[2]))
                matom.set_name("AU")
                matom.segid = "  AU"
                matom.hetero = True
                CAxtalhierarchy = atoms2pdb(list(CAchainzip[0]) + [matom])
                fname = "%s_Ca_ssm%d_sym%d_orig%d.cif" %(xtalmovingname, nssm,
                 symcount+1, oc+1)
                cntrfname = os.path.join(wpath, debugfolder, fname)
                cntrout = open(cntrfname,"w")
                #cntrout.write(str(CAxtalhierarchy.as_cif_block(xtalsym)))
                cntrout.write(model.manager(None, CAxtalhierarchy, xtalsym).model_as_mmcif())
                cntrout.close()
                allsites.write(model.manager(None, filepdbhiearchy, xtalsym).model_as_mmcif())
                ci += 1
                if ci > 25: # can't exceed more than 26 chains
                  allsites.close() # close previous file
                  n += 1 # prepend filename with n++
                  ci=0 # so we can use with ABCD... again
                  fname = "%s_UC%d_%s_align%d.cif" %(xtalmovingname, n, fixedrefname, nssm)
                  allsitesfname = os.path.join(wpath, debugfolder, fname)
                  stdoutstr = "\nWriting remaining sites of the %d. alignment to %s" \
                   %(nssm, allsitesfname)
                  thrdprint(stdoutstr)
                  allsites = open(allsitesfname, "w")
              # end looping over alternative origins
            # end looping over symmetry operations
          # end looping over ssm alignments
          if debug == False: # print intermediate symop and altorig with best mlad score to logfile
            thrdprint("\n" + minshortoutstr  + "                   ",)

          if debug and writepdbfiles:
            allsites.close()
  # place a gold atom on the centroid for the aligned C-alpha reference atoms and dump the lot as a pdb file
            matom = pdb.hierarchy.atom()
            matom.set_xyz((testalignmentcentre[0], testalignmentcentre[1],
             testalignmentcentre[2]))
            matom.set_name("AU")
            #matom.segid = "  AU"
            matom.hetero = True
            CAtesthierarchy = atoms2pdb(list(CAchainzip[1]) + [matom])
            fname = fixedrefname + "_Ca_ssm" + str(nssm) + ".cif"
            refpdboutfname = os.path.join(wpath, debugfolder, fname)
            refpdbout = open(refpdboutfname,"w")
            refpdbout.write(model.manager(None, CAtesthierarchy, xtalsym).model_as_mmcif())
            refpdbout.close()

            if hasfloatingorig and BestRpolarmin == veryhigh:
  # print table of MLAD values along floating axis for each symops and origins
              r1 = r_ini
              stdoutstr = "\nMLAD values for all symmetry operations and alternative origins when sliding moving model along polar axis\n\n"
              for (i,mlad) in enumerate(allmlads[0]):
                if i==0:
                  stdoutstr += "polar-axis\t "
                  for one in allmlads:
                    stdoutstr += "%s\t " %one[i]
                else:
                  stdoutstr += "%f\t " %r1
                  for one in allmlads:
                    stdoutstr +=  "%2.4f\t " %one[i]
                stdoutstr +=  "\n"
                r1 = r1 + invincr
              thrdprint(stdoutstr)

          #if verbose:
          #  thrdprint("\n" + alignstr,)
          if minmladrmsd[0] > mediocre_mlad:
            use_mmtbx_alignment = True
          else:
            use_mmtbx_alignment = False
        if not pdbminmlad:
          continue
        movhierarchyminmladrmsd = minmladrmsd
        tmphierarchy = pdbminmlad.deep_copy()
        minmladmovchainid = pdb_chain_id
        movminmladstr = minmladstr
        movminaltorigstr = minaltorigstr
        movminmladaltorigsymtrans = deepcopy(minmladaltorigsymtrans)
        if movehetatms and len(pdbhetatmhierarchy.models()) > 0:
          (symm, altorg, ucshift, polarvec) = movminmladaltorigsymtrans
          hetatms = pdbhetatmhierarchy.atoms()
  # get us into fractional coordinates
          sites_frac = uc.fractionalize(sites_cart=hetatms.extract_xyz())
  # get the symmetry mate w.r.t. this space group
          new_sites = symm.r().as_double() * sites_frac + symm.t().as_double() \
            + altorg[0] + ucshift[0] - (2.0*polarvec)[0]
          hetatms.set_xyz(new_sites)
  # cast back into cartesian coordinates and add polarvec sliding hetatoms along polar axis if present
          hetatms.set_xyz(uc.orthogonalize(sites_frac= (hetatms.extract_xyz())))
          pdbminmladmovhierarchylst = [tmphierarchy, pdbhetatmhierarchy.deep_copy()]
        else:
          pdbminmladmovhierarchylst = [tmphierarchy]
  # store MLAD scores indexed by fixed and moving chain ids in a matrix
        pdb_chain_id = pdbminmladmovhierarchylst[0].models()[0].chains()[0].id
        if movehetatms and len(pdbhetatmhierarchy.models()) > 0:
          pdbminmladmovhierarchylst[1].models()[0].chains()[0].id = pdb_chain_id
        pdbminmladmovhierarchylst[0].models()[0].chains()[0].id = minmladmovchainid
        AltOrigSymMLADs[ifixch][-1] = [
           movhierarchyminmladrmsd[0], movhierarchyminmladrmsd[1],
           minmladmovchainid, fixed_chainid, movminmladstr, movminaltorigstr,
           pdbminmladmovhierarchylst, movminmladaltorigsymtrans,
           pdbminmladmovhierarchylst[0].models()[0].chains()[0].atoms_size()
         ]
        # end looping over moving chains
      # workaround ambiguity of redundant alternative origins by using the origin emitting the lowest score
      if BestRpolarmin==veryhigh and minmladstuffset.size()>1:
        AllAmbiguousOrigins.extend( list( minmladstuffset.myset ) )
      # end looping over fixed chains
    # get the best floating origin and run the while loop one last time
    nambiorig= len(AllAmbiguousOrigins)
    if nambiorig:
      sortedAllAmbiguousOrigins = sorted(AllAmbiguousOrigins, key= lambda e: e.mlad)
      if isFloatingXorig:
        BestRpolarmin = sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][0]
      if isFloatingYorig:
        BestRpolarmin = sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][1]
      if isFloatingZorig:
        BestRpolarmin = sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][2]

      mstr =""
      for e in AllAmbiguousOrigins:
        mstr += "%s, %s\n" %( e.mladaltorigsymtrans[1][0], e.mlad)

      chosenorgstr= "Alternative origin chosen as the one with the smallest MLAD score:\n%s" \
       %str(sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0])
      msgstr= "\n\n%d ambiguous floating origins with associated MLAD scores detected:" \
        "\n(a\t b\t c)\t MLAD\n%s%s" %(nambiorig, mstr, chosenorgstr)
      thrdprint(msgstr)
      colour = (0, 0, 0)
      libtbx.call_back(message="showinfo", data= (chosenorgstr, colour, False) )
      altorigs = [( list(sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0]),
                  [ str( sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][0]),
                  str( sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][1]),
                  str( sortedAllAmbiguousOrigins[0].mladaltorigsymtrans[1][0][2])
                  ] )]
    else:
      BestRpolarmin = rpolarmin

  if AltOrigSymMLADs[0] == [[]]:
    raise Exception("No chains have been compared.")
  stdoutstr = "Considering matrix of MLAD values between fixed and moving chains as to\n"\
   "ensure chain matching from fixed to moving pdb is an injective function:"

  stdoutstr += "\nUnsorted matrix:\n"
  nonemptyAltOrigSymMLADs = []
  for e in AltOrigSymMLADs:
    if len(e): # discard any empty elements arising from hetatm chains
      nonemptyAltOrigSymMLADs.append( e )

  def PrintColMatrix(mtrx):
    mstr = ""
    for f in mtrx:
      mstr += f[0][2] + ", "
      for g in f:
        mstr += "%2.3f %s%s, " %(g[0], g[2], g[3])
      mstr += "\n"
    return mstr

  if nonemptyAltOrigSymMLADs != [[]]:
    stdoutstr += PrintColMatrix(nonemptyAltOrigSymMLADs)
  # now identify best matches between chains from fixed and moving pdbs
  # first sort matrix row-wise according to MLAD value
    sortmatrix = []
    for f in nonemptyAltOrigSymMLADs:
      if f == []:
        continue
      sortrow = sorted(f, key = lambda row: row[0])
      sortmatrix.append(sortrow)
    stdoutstr += "\nSorted matrix:\n"
    stdoutstr += PrintColMatrix(sortmatrix)
  # now sort according to first column MLAD value
    colsortmatrix = sorted(sortmatrix, key = lambda row: row[0][0])
    stdoutstr += "\nColumn sorted matrix:\n"
    stdoutstr += PrintColMatrix(colsortmatrix)
  # ensure chain matching from fixed to moving pdb is an injective function
    injective = True
    for (i,f) in enumerate(colsortmatrix):
      for (j,g) in enumerate(colsortmatrix):
        if j > (len(f)-1): # skip moving chains that are in excess of fixed chains
          break
        if f[0][2] == g[0][2] and j > i:
          injective = False
  # keep selecting next best matches as long as one chain accidentally is matching more than one
    while injective == False:
      sortmatrix = []
      for f in colsortmatrix:
        sortrow = sorted(f, key = lambda row: row[0])
        sortmatrix.append(sortrow)
      colsortmatrix = sortmatrix
  # now sort according to first column MLAD value
      colsortmatrix = sorted(sortmatrix, key = lambda row: row[0][0])
  # demote duplicate chainid that have a lower MLAD score from the first column
      for (i,f) in enumerate(colsortmatrix):
        for (j,g) in enumerate(colsortmatrix):
          if f[0][2] == g[0][2] and j > i:
            g[0][0] = 1000.0 # set to a high value as to demote it during next sort
      injective = True
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      for (i,f) in enumerate(colsortmatrix):
        for (j,g) in enumerate(colsortmatrix):
          if j > (len(f)-1): # skip moving chains that are in excess of fixed chains
            break
          if f[0][2] == g[0][2] and j > i:
            injective = False
    stdoutstr += "\nDemoted column sorted matrix:\n"
    stdoutstr += PrintColMatrix(colsortmatrix)
    if debug or verbose:
      thrdprint(stdoutstr)
    for (i,f) in enumerate(colsortmatrix):
      if i > (len(f)-1): # skip moving chains that are in excess of fixed chains
        break
      #j=0
      for j in range(len(f)):
        AllAltOrigPdbFnames.append((f[j][0], f[j][1], f[j][2], f[j][3], f[j][4],
          f[j][5], f[j][6], f[j][7], f[j][8]))
    # now reorder to get the same order of chains as in fixedpdb
    reordered = []
    for cid in fixedchainids:
      for f in AllAltOrigPdbFnames:
        if f[3] == cid:
          reordered.append( f )
    AllAltOrigPdbFnames = reordered
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    for r in AllAltOrigPdbFnames:
      if len(r[6]):
        pdbminmladmovhierarchies.append( r[6][0] )
        if movehetatms and len(r[6])== 2: # HETATMS are the second pdbhierarchy if present
          pdbminmladmovhierarchies.append( r[6][1] )
    thrdprint("\n")

  notprinted = True
  jointpdbminmladmov = pdb.hierarchy.join_roots( pdbminmladmovhierarchies, None)
  if (pdb_out is None) :
    altorigpdbfname = os.path.join(wpath, "MinMLAD_%s_%s%s" %(fixedrefname, xtalmovingname, movext))
    if outputfileprefix:
      altorigpdbfname = os.path.join(wpath, outputfileprefix + movext)
  altorigpdbfname = os.path.abspath(altorigpdbfname)
  finalMladRmsdMchainFchainMovstrAltOrgTrans = []
  resultsmtrxstr = "\n\nMLAD \tRMSD  \t%s\t%s\t Symmetry operation \t Alternate origin \t Unit cell shift\n" %(xtalmovingname, fixedrefname)
  for (mlad, rmsd, movchainid, fixed_chainid, movstr, altorig, hroot, altorigsymtrans, natoms) in AllAltOrigPdbFnames:
    mvstr = ""
    if mlad >= veryhigh: # ignore because mlad never got smaller than goodmatch
      stdoutstr = "No match exists between chain %s in %s and chain %s in %s with MLAD < %f\n" \
       %(movchainid, os.path.basename(movingname), fixed_chainid, os.path.basename(fixedname), goodmatch)
      thrdprint(stdoutstr)
      mvstr += stdoutstr
      resultsmtrxstr = ""
      continue
    stdoutstr = "\nMLAD: %f, RMSD: %f for chain %s in %s with chain %s in %s using\n%s" \
     %(mlad, rmsd, movchainid, os.path.basename(movingname), fixed_chainid, os.path.basename(fixedname), movstr)
    thrdprint(stdoutstr)
    mvstr += stdoutstr
    (symm, altorg, ucshift, floatingoffset) = altorigsymtrans
    resultsmtrxstr += "%s\t %s\t %s\t   %s\t %s\t %s\t %s\n" %(str(Roundoff(mlad,4)),
     str(Roundoff(rmsd,3)), movchainid, fixed_chainid, symm.as_xyz(), Roundoff(list(altorg[0]),4),
     Roundoff(list(ucshift[0]),4))
    if mlad > mediocre_mlad:
      stdoutstr = "\nThese two chains are unlikely to represent the same MR solution.\n\n"
      thrdprint(stdoutstr)
      mvstr += stdoutstr
    for f in AllAltOrigPdbFnames:
      if altorig != f[5] and notprinted and f[0] < veryhigh:
        stdoutstr = "\nWARNING! Not all chains are matched on the same origin. " \
         "At least one of the matches between chains might be spurious!"
        thrdprint(stdoutstr)
        mvstr += stdoutstr
        libtbx.call_back(message="warn", data=stdoutstr, accumulate=True)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        notprinted = False
    finalMladRmsdMchainFchainMovstrAltOrgTrans.append((mlad, rmsd, movchainid, fixed_chainid, mvstr, altorig, altorigsymtrans, natoms))
  if goodinfostr == "":
    goodinfostr = "No pairs of chains in Fixed and and Moving structures match the same MR solution"
  thrdprint("\n\n"+ goodinfostr)
  libtbx.call_back(message="showinfo", data= (goodinfostr, (0, 0, 0), True) )

  thrdprint(resultsmtrxstr)
  if jointpdbminmladmov.atoms().size() > 1: # i.e not empty
    from libtbx.math_utils import roundoff
    from phaser import tbx_utils
    famosremarks = [ tbx_utils.get_phaser_revision_remark().replace("REMARK ", "")]
    famosremarks.append("Fixed: %s" %os.path.basename(fixedname))
    famosremarks.append("MLAD  RMSD  MOVCHAIN  FIXCHAIN  SYMOP  ALTORIG")
    for r in finalMladRmsdMchainFchainMovstrAltOrgTrans:
      r0 = roundoff(r[0], precision=4)
      # avoid scientific notation messing up the regular expression in test_find_alt_orig_sym_mate.py
      if r[0] < 0.0001:
        r0 = "0.000"
      r1 = roundoff(r[1], precision=4)
      if r[1] < 0.0001:
        r1 = "0.000"
      famosremarks.append( "%s %s %s %s (%s) %s" \
       %( r0, r1, r[2], r[3], r[6][0].as_xyz(), roundoff(tuple(r[6][1][0]), precision=4) ))

    thrdprint("\nSuperposed MR solution saved in %s\n" %os.path.relpath(altorigpdbfname))
    pdbminRMSDfile = open(altorigpdbfname, "w")
    if write_cif:
      from iotbx.cif import model as cifmodel
      software_loop = cifmodel.loop(header = (
          '_software.name',
          '_software.description',
          '_software.version',
          '_software.contact_author',
          '_software.contact_author_email',
        ))
      software_loop.add_row(("FAMOS", "?", famosremarks[0], "Robert D. Oeffner", "rdo20@cam.ac.uk", ))
      for remark in famosremarks[1:]:
        software_loop.add_row(("FAMOS", remark, "?", "?", "?", ))
      mycifblock = cifmodel.block()
      mycifblock.add_loop(software_loop)
      pdbminRMSDfile.write(model.manager(None, jointpdbminmladmov,
                                         xtalsym).model_as_mmcif( additional_blocks=[mycifblock], 
                                                                 skip_restraints=True))
    else:
      pdbminRMSDfile.write("REMARK FAMOS " + "\nREMARK FAMOS ".join(famosremarks) + "\n" )
      if (len(pdbxtal.remark_section()) > 0):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        pdbminRMSDfile.write("%s\n" % "\n".join(pdbxtal.remark_section()))
      pdbminRMSDfile.write(jointpdbminmladmov.as_pdb_string(xtalsym))
    pdbminRMSDfile.close()
  if movehetatms and not debug:
    os.remove(pdbxtalmovingname)
  if not len(AllAltOrigPdbFnames):
    thrdprint("No match found between any chains in %s and any chains in %s with a MLAD value smaller than %f\n" \
     %(xtalmovingname, fixedrefname, goodmatch))
  return thrdprint.get(), finalMladRmsdMchainFchainMovstrAltOrgTrans, altorigpdbfname


def GetHETATM(id, mhierarchy): # make a pdbhierachy from hetatoms having the same chain.id as id
  hetatmhierarchies = []
  for chn in mhierarchy.models()[0].chains():
    if chn.is_protein() == False and chn.id == id:
      hetatmhierarchies.append( pdb.hierarchy.new_hierarchy_from_chain( chn ))
  hetatmhierarchy = pdb.hierarchy.join_roots( hetatmhierarchies, None)
  return hetatmhierarchy



def mmtbxalign(movingchain, fixedchain):
# use mmtbx alignment as a fall back if SSM craps out
  movres = []
  fixres = []
  for rg in movingchain.residue_groups():
    movres.append(amino_acid_codes.one_letter_given_three_letter.get(rg.unique_resnames()[0],"X"))
  movres = "".join(movres)
  for rg in fixedchain.residue_groups():
    fixres.append(amino_acid_codes.one_letter_given_three_letter.get(rg.unique_resnames()[0],"X"))
  fixres = "".join(fixres)
  myalign = mmtbx.alignment.align(seq_a = fixres, seq_b = movres)
  mmtbxali =  myalign.extract_alignment()
  resmap2 = []
  for i, i_seq1 in enumerate(mmtbxali.i_seqs_a):
    i_seq2 = mmtbxali.i_seqs_b[i]
    res1 = fixedchain.residue_groups()[i_seq1] if i_seq1 != None else None
    res2 = movingchain.residue_groups()[i_seq2] if i_seq2 != None else None
    resmap2.append((res2, res1))
  return resmap2


def atoms2pdb(atoms):
  r = pdb.hierarchy.root()
  r.append_model(pdb.hierarchy.model())
  r.models()[0].append_chain(pdb.hierarchy.chain(id="A"))
  #r.models()[0].chains()[0].append_residue_group(pdb.hierarchy.residue_group())
  #r.models()[0].chains()[0].residue_groups()[0].append_atom_group(pdb.hierarchy.atom_group())
  for (i,atm) in enumerate(atoms, start = 1):
    rg = pdb.hierarchy.residue_group()
    rg.resseq = i
    r.models()[0].chains()[0].append_residue_group( rg )
    ag = pdb.hierarchy.atom_group()
    rg.append_atom_group( ag )
    ag.append_atom( atm.detached_copy() )
    if atm.hetero == False:
      ag.resname = atm.parent().resname
    else:
      ag.resname = "HEM"
  r.atoms_reset_serial() # make consecutive serial numbers
  return r


def MakeAlignstr(CAchains):
# make alignment strings of residue codes
  Ca1rescodes = ""
  Ca2rescodes = ""
  errstr = ""
  for Capair in CAchains:
    try:
      if type(Capair[0]) is not None:
        Ca1rescodes += amino_acid_codes.one_letter_given_three_letter[Capair[0].parent().resname]
      else:
        Ca1rescodes += "-"
    except Exception as m:
      errstr += "Unknown residue: %s\n" %m
      Ca1rescodes += "?"

    try:
      if type(Capair[1]) is not None:
        Ca2rescodes += amino_acid_codes.one_letter_given_three_letter[Capair[1].parent().resname]
      else:
        Ca2rescodes += "-"
    except Exception as m:
      errstr += "Unknown residue: %s\n" %m
      Ca2rescodes += "?"

  niceprint = ""
  n = 0
  nchar = 80
  while len(CAchains) > n:
    niceprint += Ca1rescodes[n:(n+nchar)] + "\n"
    niceprint += Ca2rescodes[n:(n+nchar)] + "\n"
    niceprint += "\n"
    n +=nchar

  alignstr = "Aligned residues:\n" + niceprint + errstr
  return alignstr


def MakeAlignstr2(CAchains_stats):
# make alignment strings of residue codes
  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  residalnstr = ""
  errstr = ""
  for ([Ca1, Ca2], ssmalignedstat, (resdat1, resdat2)) in CAchains_stats:
    try:
      if Ca1:
        residalnstr += resdat1.resname + " "
        residalnstr += str(resdat1.resseq)
        cid1 = resdat1.chain_id
      else:
        residalnstr += " -     "
    except Exception as m:
      errstr += "Unknown residue: %s\n" %m
      residalnstr += "?"
    if Ca1 and Ca2:
      dstr = "  %2.2f  " %L2norm( DiffVec(Ca1.xyz, Ca2.xyz) )
    else:
      dstr = "         "
    residalnstr += dstr
    try:
      if Ca2:
        residalnstr += resdat2.resname + " "
        residalnstr += str(resdat2.resseq)
        cid2 = resdat2.chain_id
      else:
        residalnstr += " -      "
    except Exception as m:
      errstr += "Unknown residue: %s\n" %m
      residalnstr += "?"
    if ssmalignedstat:
      residalnstr += " ( ssm rmsd )"
    residalnstr += "\n"

  alignstr = "\nAligned residues:\nchain %s  dist(A)  chain %s\n" %(cid1, cid2)
  alignstr += residalnstr + errstr
  return alignstr


def DiffVec(vec1,vec2):
  d=[0,0,0]
  d[0] = vec1[0] - vec2[0]
  d[1] = vec1[1] - vec2[1]
  d[2] = vec1[2] - vec2[2]
  return d


def L2norm(r):
  d = r[0]*r[0] + r[1]*r[1] + r[2]*r[2]
  return math.sqrt(d)


def GetRMSDfromAtoms(atompairs):
  ln = len(atompairs)

  D = 0.0
  for atompair in atompairs:
    dx = atompair[0].xyz[0] - atompair[1].xyz[0]
    dy = atompair[0].xyz[1] - atompair[1].xyz[1]
    dz = atompair[0].xyz[2] - atompair[1].xyz[2]
    D += dx*dx + dy*dy + dz*dz

  return math.sqrt(D/ln)


# Mean Log Absolute Deviation
# is better than RMSD for matching overlapping chains as it downplays atom pairs which are very far apart
def GetMLADfromAtoms(atompairs):
  ln = len(atompairs)
  D = 0.0
  p = 0.9
  for atompair in atompairs:
    dx = atompair[0].xyz[0] - atompair[1].xyz[0]
    dy = atompair[0].xyz[1] - atompair[1].xyz[1]
    dz = atompair[0].xyz[2] - atompair[1].xyz[2]
    sqrD = dx*dx + dy*dy + dz*dz
    D += math.log(sqrD/(math.sqrt(sqrD) + p) + max(p,min(sqrD,1)))
  return D/ln - math.log(p) # offset to avoid values less than zero


def GetCentreOfMolecule(atoms):
  c = [0.0,0.0,0.0]
  for atom in atoms:
    c[0] += float(atom.xyz[0])
    c[1] += float(atom.xyz[1])
    c[2] += float(atom.xyz[2])
  c[0] /= len(atoms)
  c[1] /= len(atoms)
  c[2] /= len(atoms)
  return c


def make_nice( t,d):
  # original code by Peter Zwart
  if t < 0:
    t=t+d
    make_nice(t,d)
  if t >= d:
    t = t-d
    make_nice(t,d)
  return t,d


def translate_v_m(v,m):
# original code by Peter Zwart
  res = [None, None ,None]
#    result = "( "
  xyz=[ 'x','y','z' ]
  isfloatorig = False
  if m==0:
  #find the first no-zero element of the vector
    this_one = 0
    for ii in range(3):
      if v[ii]!=0:
        this_one = ii
        break
    for ii in range(3):
      if v[ii]==0:
#                result += "0 "
        res[ii] = 0
      elif v[ii]==1:
#                result += str(xyz[this_one]) + " "
        res[ii] = str(xyz[this_one])
        isfloatorig = True
      else:
#                result += str(xyz[this_one])+str(v[ii]) + " "
        res[ii] = str(xyz[this_one])+str(v[ii])
        isfloatorig = True
#            if ii<2:
#                result += ", "
#        result += ")"
#        print result
    return (res, isfloatorig)
  else:
    for ii in range(3):
      if v[ii]==0:
#                result += "0 "
        res[ii] = 0
      else:
        t,d = make_nice(v[ii],m)
#                result += str(t)+"/"+str(d)
        #res[ii] = float(t)/float(d)
        #print "v[ii],t,d= %s, %s, %s" %(v[ii], t, d)
        #print "type v[ii],t,d= %s, %s, %s" %(type(v[ii]),type(t), type(d))
        if type(t) == int:
          t= rational.int(t,1)
        if type(d) == int:
          d= rational.int(d,1)
        obj = t/d
        res[ii] = Fraction(obj.numerator(), obj.denominator())

#            if ii<2:
#                result += ", "

#        result += ")"
#        print result
    return (res, isfloatorig)


#get alternative origins for space group
def list_origins(sgnumbersymbol):
  # original code by Peter Zwart
  sginfo = sgtbx.space_group_info(symbol=sgnumbersymbol, space_group_t_den=144)
  #print
  #print
  #sginfo.show_summary()
  #print "Allowed additional origins:"
  vecs = sginfo.structure_seminvariants().vectors_and_moduli()
  sg_type = sginfo.type()
  k2l = sg_type.addl_generators_of_euclidean_normalizer(True, False)
  results = []
  if len(vecs)>0:
    for ii in vecs:
      r = translate_v_m(ii.v, ii.m)
      if r not in results:
        results.append(r)
      """
      if ii.m is not 0:
        if (len(k2l)):
          inv = sgtbx.translation_part_info(k2l[0]).origin_shift().as_rational()
          nm = []
          for a,b in zip(ii.v,inv):
            nm.append( -(a-b) )

          print "k2l[0], inv, nm = %s, %s %s" %(k2l[0], inv, nm)
          r = translate_v_m(nm, ii.m)
          if r not in results:
            results.append(r)
      """
  hasfloatingorigin = False
  resultsoriginf = [[0.0, 0.0, 0.0]] # when only centering vectors provide allowed origin shifts
  for res in results:
    if res[1]:
      hasfloatingorigin = True
    resultsoriginf.append(res[0])
  return (resultsoriginf, hasfloatingorigin)


# cast x,y,z components within the interval [0;1]
def Remainder(v):
  t = list(v)
  if not isinstance(t[0], str):
    t[0] = divmod(t[0], 1)[1]
  if not isinstance(t[1], str):
    t[1] = divmod(t[1], 1)[1]
  if not isinstance(t[2], str):
    t[2] = divmod(t[2], 1)[1]
  return t


def GenerateEquivalentOriginsForVector(origvlst):
  allorigins = set([])
  for origv in origvlst:
# equivalent origins are those that are translated along each origv
    modnewv = tuple(Remainder(origv))
    n=1
    while modnewv not in allorigins:
      allorigins.add(modnewv)
      newv = [0,0,0]
      n += 1
      if isinstance(origv[0], str):
        newv[0] = origv[0]
      else:
        newv[0] = n * origv[0]

      if isinstance(origv[1], str):
        newv[1] = origv[1]
      else:
        newv[1] = n * origv[1]

      if isinstance(origv[2], str):
        newv[2] = origv[2]
      else:
        newv[2] = n * origv[2]
      modnewv = tuple(Remainder(newv))
  return allorigins


def GetOriginVectorCombinations(originvlst):
  origvClst = []
  for n in range(1,len(originvlst)+1):
# make a K combination for each n
    Klst = list( itertools.combinations(originvlst,n))
# want origins translated along the combination of these individual vectors, i.e. the sum of each vector in each combination
    for K in Klst:
      combinedorigv = [0,0,0]
      for v in K:
        if isinstance(v[0], str):
# if this is either x, y or z then it is a floating origin along that axis
          combinedorigv[0] = v[0]
        else:
          if not isinstance(combinedorigv[0], str):
            combinedorigv[0] += v[0]
        if isinstance(v[1], str):
# if this is either x, y or z then it is a floating origin along that axis
          combinedorigv[1] = v[1]
        else:
          if not isinstance(combinedorigv[1], str):
            combinedorigv[1] += v[1]
        if isinstance(v[2], str):
# if this is either x, y or z then it is a floating origin along that axis
          combinedorigv[2] = v[2]
        else:
          if not isinstance(combinedorigv[2], str):
            combinedorigv[2] += v[2]
      origvClst.append(combinedorigv)
  return origvClst


def GetCentringVectors(sgnumber):
  cvecs = []
  sginfo = sgtbx.space_group_info(symbol=sgnumber, space_group_t_den=144)
  if sginfo.type().lookup_symbol()[0] == "A":
    cvecs.append([0.0, Fraction(1,2), Fraction(1,2)])
  if sginfo.type().lookup_symbol()[0] == "C":
    cvecs.append([Fraction(1,2), 0.0, Fraction(1,2)])
  if sginfo.type().lookup_symbol()[0] == "C":
    cvecs.append([Fraction(1,2), Fraction(1,2), 0.0])
  if sginfo.type().lookup_symbol()[0] == "I":
    cvecs.append([Fraction(1,2), Fraction(1,2), Fraction(1,2)])
  if sginfo.type().lookup_symbol()[0] == "F":
    cvecs.append([Fraction(1,2), Fraction(1,2), 0.0])
    cvecs.append([0.0, Fraction(1,2), Fraction(1,2)])
    cvecs.append([Fraction(1,2), 0.0, Fraction(1,2)])
  if sginfo.type().lookup_symbol()[0] == "R":
    # centred rhombohedral: obverse setting
    cvecs.append([Fraction(2,3), Fraction(1,3), Fraction(1,3)])
    cvecs.append([Fraction(1,3), Fraction(2,3), Fraction(2,3)])
    # centred rhombohedral: reverse setting is unconventional so bin it for now
    #cvecs.append([Fraction(1,3), Fraction(2,3), Fraction(1,3)])
    #cvecs.append([Fraction(2,3), Fraction(1,3), Fraction(2,3)])
  if sginfo.type().lookup_symbol()[0] == "H":
    # centred hexagonal
    cvecs.append([Fraction(2,3), Fraction(1,3), 0.0])
    cvecs.append([Fraction(1,3), Fraction(2,3), 0.0])
  return cvecs


def GetAllOrigins(sgnumber):
  origlst = list_origins(sgnumber)[0]
  origlst.extend(GetCentringVectors(sgnumber))
  Coriglst = GetOriginVectorCombinations(origlst)
# impose the floating origin on all other vectors if present
  origfloatX = False; origfloatY = False; origfloatZ = False
  for elm in Coriglst:
    if elm[0] == "x":
      origfloatX = True
    if elm[1] == "y":
      origfloatY = True
    if elm[2] == "z":
      origfloatZ = True
  for elm in Coriglst:
    if origfloatX:
      elm[0] = "x"
  for elm in Coriglst:
    if origfloatY:
      elm[1] = "y"
  for elm in Coriglst:
    if origfloatZ:
      elm[2] = "z"
  eqoriglst = GenerateEquivalentOriginsForVector(Coriglst)
  return list(eqoriglst)


def ListAsFloats(vs):
  lfv = []
  for v in vs:
    fv = [0,0,0]
    if not isinstance(v[0], str):
      fv[0] = float(v[0])
    else:
      fv[0] = v[0]
    if not isinstance(v[1], str):
      fv[1] = float(v[1])
    else:
      fv[1] = v[1]
    if not isinstance(v[2], str):
      fv[2] = float(v[2])
    else:
      fv[2] = v[2]
    lfv.append(fv)
  return lfv


def GetAllOriginsAsFloatsnStrings(sgnumber):
  lst = GetAllOrigins(sgnumber)
  floatlst = ListAsFloats(lst)
  strlst = [[str(e[0]), str(e[1]), str(e[2])] for e in lst]
  fslst = zip(floatlst, strlst)
  return fslst


def PrettyPrintAllOrigins(sgnumber):
  lst = GetAllOrigins(sgnumber)
  strlst = [("(" + str(e[0]) + ", " + str(e[1]) + ", " + str(e[2]) + ")") for e in lst]
  for l in strlst:
    print(l)
