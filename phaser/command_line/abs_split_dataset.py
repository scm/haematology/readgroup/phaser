from __future__ import print_function

def select_half_redundancy(indices):
  """
  Decreases the redundancy in the array
  """

  from scitbx.array_family import flex
  selection = flex.bool()
  selection.reserve( len( indices ) )

  import random
  from phaser import sequtil
  groupiter = sequtil.split( sequence = indices, consecutivity = lambda l, r: l == r )

  for g in groupiter:
    index = set( g )
    length = len( g )
    print("%s: %s copies" % (index, length))

    if length == 1:
      selection.append( True )
      continue

    half = length // 2

    if length % 2 != 0:
      half += random.randint( 0, 1 )

    selection.extend( flex.bool( [ True ] * half ) )
    selection.extend( flex.bool( [ False ] * ( length - half ) ) )

  return selection


import sys

if len( sys.argv ) != 4:
  print("Usage: abs_merge_shelx_hkl res-file hkl-file prefix")
  sys.exit(1)

( res, hkl, outfile ) = sys.argv[1:]

from iotbx import shelx
structdata = shelx.cctbx_xray_structure_from( None, filename = res )

from iotbx import reflection_file_reader
refldata = reflection_file_reader.any_reflection_file( "%s=hklf4" % hkl )
arrays = refldata.as_miller_arrays( crystal_symmetry = structdata.crystal_symmetry() )

assert len( arrays ) == 1
myarray = arrays[0]

assert myarray.anomalous_flag, "Anomalous flag is False"
assert myarray.observation_type() != "xray.intensity", "Data read is not intensities"

print("Data read:")
myarray.show_summary()
print()
print("Splitting dataset:")
tmp_array = myarray.customized_copy(anomalous_flag=True).map_to_asu()
tmp_array = tmp_array.sort("packed_indices")
selection = select_half_redundancy( indices = tmp_array.indices() )
assert len( selection ) == len( tmp_array.indices() )
half = tmp_array.select( selection )
print ("Array length: %s, selected: %s" % (
  len( tmp_array.indices() ),
  sum( selection ),
  ))
"""
from cctbx import miller
tmp_array = myarray.customized_copy(anomalous_flag=True).map_to_asu()
tmp_array = tmp_array.sort("packed_indices")
split_datasets = miller.split_unmerged(
  unmerged_indices=tmp_array.indices(),
  unmerged_data=tmp_array.data(),
  unmerged_sigmas=tmp_array.sigmas(),
  weighted=False)
base_set = miller.set(
  crystal_symmetry=myarray,
  indices=split_datasets.indices,
  anomalous_flag=True)
assert base_set.is_unique_set_under_symmetry()
array1 = miller.array(
  miller_set=base_set,
  data=split_datasets.data_1)
dano1 = array1.anomalous_differences()
array2 = miller.array(
  miller_set=base_set,
  data=split_datasets.data_2)
dano2 = array2.anomalous_differences()
assert dano1.indices().all_eq(dano2.indices())
cc = dano1.correlation(other=dano2, use_binning=False).coefficient()
print "Anomalous cc between two halves: %.2f%%" % ( 100.0 * cc )
"""
print("Exporting in SHELX format to:", outfile)

with open( outfile, "w" ) as ofile:
  half.export_as_shelx_hklf( ofile )
