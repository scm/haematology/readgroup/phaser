from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "test",
    metavar = "JSON_FILE",
    help = "JSON file containing target description",
    )
  parser.add_argument(
    "replacements",
    type = str,
    help = "Domains to replace",
    )
  parser.add_argument(
    "--disable-bfactor-refinement",
    action = "store_false",
    dest = "bfactor",
    help = "Disable Bfactor refinement"
    )
  parser.add_argument(
    "--disable-vrms-refinement",
    action = "store_false",
    dest = "vrms",
    help = "Disable VRMS refinement"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_basescore_test"


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  from libtbx.utils import Sorry
  import json
  info.task_begin( text = "Reading test description" )

  try:
    with open( params.test ) as ifile:
      description_for = json.load( ifile )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  import os.path
  path_to_test = os.path.dirname( params.test )
  info.field( name = "Path for test files", value = path_to_test )

  info.heading( text = "Base structure", level = 1 )

  if not params.replacements:
    raise Sorry("No replacements requested")

  replacements = params.replacements.split( "+" )

  if any( eid not in description_for[ "superposition_with" ] for eid in replacements ):
    info.field_sequence( name = "Replacements requested", value = replacements )
    info.field_sequence( name = "Known domains", value = description_for[ "superposition_with" ] )
    raise Sorry("Unknown ensemble for replacement")

  import pickle

  with open( os.path.join( path_to_test, description_for[ "target_structure_file" ] ) ) as ifile:
    structure = pickle.load( ifile )

  base_structure = benchmark.Structure( symmetry = structure.symmetry )

  for molecule in structure.molecules():
    if molecule.ensemble.identifier() not in replacements:
      base_structure.add( molecule = molecule )

  info.preformatted_text( text = str( base_structure ) )

  info.heading( text = "Phaser refinement", level = 1 )
  results = [ "+".join( base_structure.get_ensemble_ids() ), params.replacements ]

  if not base_structure.molecules():
    info.unformatted_text( text = "Base structure empty, no refinement" )
    results.extend( [ 0.0, 0.0, 0 ] )

  else:
    prefix = "%s-%s" % (
      "%s-base" % params.prefix if params.prefix else "base",
      os.path.splitext( os.path.basename( description_for[ "target_structure_file" ] ) )[0],
      )
    base_structure_file = "%s-%s.structure" % ( prefix, params.replacements )
    info.field( name = "Output file", value = base_structure_file )
    info.task_begin( text = "Writing base output" )

    with open( base_structure_file, "w" ) as ofile:
      pickle.dump( base_structure, ofile )

    info.task_end()

    from phaser.command_line import benchmark_calculate_mr_statistics

    args = [
      os.path.join( path_to_test, description_for[ "case_file" ] ),
      base_structure_file,
      ]

    if not params.bfactor:
      args.append( "--disable-bfactor-refinement" )

    if not params.vrms:
      args.append( "--disable-vrms-refinement" )

    info.paragraph_begin( indent = 4 )
    res = benchmark_calculate_mr_statistics.run(
      params = parse(
        parser = benchmark_calculate_mr_statistics.get_parser(),
        args = args
        ),
      logger = logger,
      )
    results.extend( res )
    info.paragraph_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  head = [
    description_for[ "targetid" ],
    description_for[ "modelid" ],
    ]
  return head + results


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
