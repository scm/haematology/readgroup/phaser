from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "test",
    metavar = "JSON_FILE",
    help = "JSON file containing target description",
    )
  parser.add_argument(
    "phils",
    nargs = "*",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL arguments for sculptor (file name or PHIL command line assignment)",
    )
  parser.add_argument(
    "--min-domain-residue-length-for-replacement",
    type = int,
    default = 50,
    help = "Minimum length of a replacing chain in residues"
    )
  parser.add_argument(
    "--acceptable-domain-fractional-length-for-replacement",
    type = float,
    default = 0.4,
    help = "Acceptable fractional length of domain to be replaced if smaller than absolute limit"
    )
  parser.add_argument(
    "--min-hss-length",
    type = int,
    default = 10,
    help = "Minimum sequence overlap length with replacing chain"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_evaluate_test"


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  from libtbx.utils import Sorry
  import json
  info.task_begin( text = "Reading test description" )

  try:
    with open( params.test ) as ifile:
      description_for = json.load( ifile )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  import os.path
  path_to_test = os.path.dirname( params.test )
  info.field( name = "Path for test files", value = path_to_test )

  info.heading( text = "Model sculpting", level = 1 )

  from phaser.command_line import benchmark_sculpt_model
  params.phils.append(
    os.path.join( path_to_test, description_for[ "alignment_file" ] )
    )

  info.paragraph_begin( indent = 4 )
  sculpt_file_name = benchmark_sculpt_model.run(
    params = parse(
      parser = benchmark_sculpt_model.get_parser(),
      args = [ os.path.join( path_to_test, description_for[ "model_file" ] ) ]
        + params.phils
        + [
          "--prefix",
          "%ssculpt-target-%s-model-%s" % (
            params.prefix + "-" if params.prefix else "",
            description_for[ "targetid" ],
            description_for[ "modelid" ],
            ),
          ],
      ),
    logger = logger,
    )
  info.paragraph_end()

  import pickle
  with open( sculpt_file_name ) as ifile:
    sculpted_chain = pickle.load( ifile )

  info.heading( text = "Chain replacement", level = 1 )
  from phaser.command_line import benchmark_molecule_replacement

  info.paragraph_begin( indent = 4 )
  ( replaced_structure_file, replacement_ensemble_for ) = benchmark_molecule_replacement.run(
    params = parse(
      parser = benchmark_molecule_replacement.get_parser(),
      args = [
        os.path.join( path_to_test, description_for[ "target_structure_file" ] ),
        "--prefix", params.prefix if params.prefix else "replaced",
        "--min-hss-length", params.min_hss_length,
        ]
        + sum(
          ( [ "--replacement", eid, sculpt_file_name, os.path.join( path_to_test, transf ), 1.0 ]
          for ( eid, transf ) in description_for[ "superposition_with" ].items() ),
          []
          ),
      ),
    logger = logger,
    )
  info.paragraph_end()

  assert len( description_for[ "superposition_with" ] ) == len( replacement_ensemble_for )

  results = [
    description_for[ "targetid" ],
    description_for[ "modelid" ],
    sculpted_chain.identity,
    ]

  info.heading( text = "Summary", level = 1 )

  if not replacement_ensemble_for:
    info.unformatted_text( text = "No replacement made" )

  else:
    info.unordered_list_begin()
    replacements = []
    statistics = []

    for ( ense, repldata ) in replacement_ensemble_for.items():
      info.list_item_begin( text = "Chain '%s'" % ense.identifier() )
      length_orig = len( ense.model.aligned_residue_group_indices() )
      length_repl = len( repldata[0].model.aligned_residue_group_indices() )
      fraclength = length_repl / length_orig
      info.field( name = "Length", value = length_orig )
      info.field( name = "Replacement (residues)", value = length_repl )
      info.field_percentage(
        name = "Replacement (%original)",
        value = 100.0 * fraclength,
        digits = 0,
        )

      if params.min_domain_residue_length_for_replacement <= length_repl:
        replacements.append( ense.identifier() )

      elif params.acceptable_domain_fractional_length_for_replacement <= fraclength:
        replacements.append( ense.identifier() )

      statistics.extend( [ ense.identifier(), length_orig, length_repl, fraclength ] )
      info.list_item_end()

    results.append( "+".join( replacements ) )
    results.extend( statistics )

  info.list_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return results


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
