from __future__ import print_function

def run () :
  from phaser import process_default_phil_settings
  process_default_phil_settings()

if (__name__ == "__main__") :
  run()
