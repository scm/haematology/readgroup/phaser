from __future__ import print_function

def apply_atoms_and_composition(inputobj, scatterers):

  count_for = {}

  for scat in scatterers:
    #if scat.scattering_type == "H":
    #  continue

    if scat.flags.use_u_aniso():
      anisotropic_flag = True
      #( u1, u2, u3, u4, u5, u6 ) = scat.u_cart_plus_u_iso( uctbx_cell )
      #uano = ( 10000 * u1, 10000 * u2, 10000 * u3, 10000 * u4, 10000 * u5, 10000 * u6 )
      uano = scat.u_star
      biso = scat.b_iso()

    else:

      anisotropic_flag = False
      uano = ( 0, 0, 0, 0, 0, 0 )
      biso = scat.b_iso()

    elem = scat.scattering_type

    inputobj.addATOM_FULL(
      "xtal",
      elem,
      True,
      scat.site,
      scat.occupancy,
      anisotropic_flag,
      biso,
      True, # u_star flag
      uano,
      scat.scattering_type == "H", # FIXX
      True, # FIXO
      False, # FIXB
      False,
      scat.label,
      )

    elem = scat.scattering_type
    count_for[ elem ] = count_for.get( elem, 0 ) + 1

  for ( symbol, count ) in count_for.items():
    inputobj.addCOMP_ATOM_NUM( symbol, count )


def apply_composition(inputobj, scatterers):

  for scat in scatterers:
    elem = scat.scattering_type
    count_for[ elem ] = count_for.get( elem, 0 ) + 1

  for ( symbol, count ) in count_for.items():
    inputobj.addCOMP_ATOM_NUM( symbol, count )


def get_input_object(cell, spac, data):

  import phaser

  sadinp = phaser.InputEP_SAD()
  sadinp.setCRYS_DATA( data )
  sadinp.setCELL6( cell )
  sadinp.setSPAC_HALL( spac )
  sadinp.setWAVE( 1.5418 )
  sadinp.addSCAT_TYPE( "H", 0, 0, "ON" )
  sadinp.setATOM_CHAN_BFAC_WILS( False )
  #sadinp.setOUTL_REJE( False )
  #sadinp.setOUTL_PROB( 1E-6 )
  #sadinp.setLLGC_COMP( True )
  #sadinp.addLLGC_SCAT( "AX" )
  #sadinp.setRFAC_CUTO( 0.1 )

  return sadinp


def apply_refinement_protocol(inputobj):

  inputobj.setMACS_PROT( "CUSTOM" )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    True,# bool ref_xyz
    False, # bool ref_occ
    True, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    True,# bool ref_xyz
    False, # bool ref_occ
    True, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    True,# bool ref_xyz
    False, # bool ref_occ
    True, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )

  """
  inputobj.addMACS(
    True, # bool ref_pk
    True, # bool ref_pb
    True,# bool ref_xyz
    True, # bool ref_occ
    True, # bool ref_bfac
    True, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  """


def apply_scoring_protocol(inputobj):

  inputobj.setMACS_PROT( "CUSTOM" )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    True, # bool ref_sp
    True, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  inputobj.addMACS(
    False, # bool ref_pk
    False, # bool ref_pb
    False,# bool ref_xyz
    False, # bool ref_occ
    False, # bool ref_bfac
    False, # bool ref_fdp
    False, # bool ref_sa
    False, # bool ref_sb
    False, # bool ref_sp
    False, # bool ref_sd
    50, # int ncyc
    "NOT_ANOM_ONLY", # std::string target
    "BFGS", # std::string minimizer
    )
  inputobj.setOUTL_REJE( False )


import sys

if len( sys.argv ) != 3:
  print("Usage: abs_run_phaser shelxl-file mtz-file")
  sys.exit(1)

( insfile, mtzfile ) = sys.argv[1:]
ipos = "I(+)"
sigipos = "SIGI(+)"
ineg = "I(-)"
sigineg = "SIGI(-)"

import phaser
datinp = phaser.InputEP_DAT()
datinp.setHKLI( mtzfile )
datinp.setCRYS_LABI_IPOS( "xtal", "wave", ipos )
datinp.setCRYS_LABI_SIGIPOS( "xtal", "wave", sigipos )
datinp.setCRYS_LABI_INEG( "xtal", "wave", ineg )
datinp.setCRYS_LABI_SIGINEG( "xtal", "wave", sigineg )

datres = phaser.runEP_DAT( datinp )

if datres.Failed():
  print("Error:", datres.ErrorMessage())
  sys.exit(1)

cell = datres.getUnitCell()
spac = datres.getSpaceGroupHall()
data = datres.getCrysData()

print()
print("Refine sites")
inp1 = get_input_object( cell = cell, spac = spac, data = data )
from iotbx import shelx
structdata = shelx.cctbx_xray_structure_from( None, filename = sys.argv[1] )
apply_atoms_and_composition( inputobj = inp1, scatterers = structdata.scatterers() )
apply_refinement_protocol( inputobj = inp1 )
scatterers = structdata.scatterers()
res1 = phaser.runEP_SAD( inp1 )

if res1.Failed():
  print("Run ended with error")
  sys.exit(1)

selection_d = res1.getSelected( False )
selection_i = res1.getSelected( True )
selection_b = selection_d and selection_i
print("direct:", len(selection_d), selection_d.count(True))
print("inverted:", len(selection_i), selection_i.count(True))
print("both:", len(selection_b), selection_b.count(True))

subdata = data.subset( selection_b )

print()
print("Score direct hand:")
inp2_d = get_input_object( cell = cell, spac = spac, data = subdata )
apply_atoms_and_composition( inputobj = inp2_d, scatterers = res1.getAtoms( False ) )
apply_scoring_protocol( inputobj = inp2_d )
inp2_d.setHAND( "OFF" )
res2_d = phaser.runEP_SAD( inp2_d )

print()
print("Score inverted hand:")
inp2_i = get_input_object( cell = cell, spac = spac, data = subdata )
apply_atoms_and_composition( inputobj = inp2_i, scatterers = res1.getAtoms( True ) )
apply_scoring_protocol( inputobj = inp2_i )
inp2_i.setHAND( "OFF" )
res2_i = phaser.runEP_SAD( inp2_i )

print("LLG(direct)   =", -res2_d.getHand().getLogLikelihood())
print("Number of reflections:", res2_d.getSelected().count(True))
print("LLG(inverted) =", -res2_i.getHand().getLogLikelihood())
print("Number of reflections:", res2_i.getSelected().count(True))
