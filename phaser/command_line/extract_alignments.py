from __future__ import print_function

from phaser import tbx_utils
from libtbx.object_oriented_patterns import lazy_initialization

master_phil = """
input
  .help = "Input data"
{
  homology_search = None
    .help = "Homology search input file"
    .type = path
    .optional = False

  hits = 1
    .help = "Hits to extract"
    .type = ints( value_min = 1 )
    .optional = False
}

output
  .help = "Output options"
{
  prefix = None
    .type = str
    .help = "Prefix to include in alignment names"
}
"""

parsed_master_phil = lazy_initialization(
  func = tbx_utils.parse_phil,
  phil = master_phil,
  )

if __name__== "__main__":
  from libtbx.utils import Sorry
  from phaser import cli_new as cli

  parser = cli.PCLIParser(
    master_phil = parsed_master_phil(),
    description = "Reformat a homology search file as a series of alignments",
    )
  parser.recognize_file_type_as(
    extensions = [ ".xml", ".hhr" ],
    philpath = "input.homology_search",
    )

  args = parser.parse_args()
  params = args.phil.extract()

  if params.input.homology_search is None:
    raise Sorry("No homology_search file is provided")

  if params.output.prefix is None:
    import os.path
    prefix = os.path.splitext( os.path.basename( params.input.homology_search ) )[0]

  else:
    prefix = params.output.prefix

  hfile = tbx_utils.HomologySearchObject.from_file(
    file_name = params.input.homology_search,
    )
  requesteds = set( params.input.hits )

  from iotbx import bioinformatics

  for ( index, hit ) in enumerate( hfile.object.hits(), start = 1 ):
    if index not in requesteds:
      continue

    filename = "%s_%s_%s_%s.aln" % ( prefix, index, hit.identifier, hit.chain )

    if 63 < len( hit.annotation ):
      annotation = hit.annotation[ : 60 ] + "..."

    else:
      annotation = hit.annotation

    print("Hit %s: %s" % (index, annotation))
    print("Output to: %s" % filename)

    with open( filename, "w" ) as ofile:
      alignment = bioinformatics.clustal_alignment(
        alignments = hit.alignment.alignments,
        names = hit.alignment.names,
        gap = hit.alignment.gap,
        )
      ofile.write( str( alignment ) )
      ofile.write( "\n" )

  print("Done!")
