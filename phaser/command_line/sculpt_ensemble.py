# LIBTBX_SET_DISPATCHER_NAME phenix.sculpt_ensemble

from __future__ import print_function

from phaser import cli_new as cli
from phaser import sculpt_ensemble

from iotbx import bioinformatics

parser = cli.PCLIParser(
  master_phil = sculpt_ensemble.parsed_master_phil(),
  enable_text_logfile = True,
  enable_html_logfile = True,
  description = "Superpose structures and apply a user-defined protocol to modify it",
  )
parser.add_argument(
  "-v", "--verbosity",
  action = "store",
  choices = sculpt_ensemble.LOGGING_LEVELS,
  default = "info",
  help = "verbosity level"
  )
parser.add_argument(
  "--version",
  action = "version",
  version = "%s %s" % ( sculpt_ensemble.PROGRAM, sculpt_ensemble.VERSION )
  )

parser.recognize_file_type_as(
  extensions = [ ".pdb", ".ent" ],
  philpath = "input.structure.model.file_name",
  )
parser.recognize_file_type_as(
  extensions = [ ".hhr", ".xml" ],
  philpath = "input.homology_search.file_name",
  )
parser.recognize_file_type_as(
  extensions = bioinformatics.known_alignment_formats(),
  philpath = "input.alignment",
  )

params = parser.parse_args()

# Check parameters
sculpt_ensemble.validate_params( params = params.phil.extract() )

# Run the program
import sys
from phaser import logoutput_new as logoutput
from phaser.logoutput_new import console
from libtbx.utils import Sorry

handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]

if params.text_logfile is not None:
  handlers.append(
    console.Handler.FromDefaults(
      stream = params.text_logfile,
      width = 80,
      close_stream = True,
      )
    )

if params.html_logfile is not None:
  from phaser.logoutput_new import xhtml
  handlers.append(
    xhtml.Handler.FromDefaults(
      stream = params.html_logfile,
      title = sculpt_ensemble.PROGRAM,
      close_stream = True,
      )
    )

with logoutput.Writer( \
  handlers = handlers, \
  level = sculpt_ensemble.LOGGING_LEVELS[ params.verbosity ], \
  close_handlers = True,
  **sculpt_ensemble.LOGGING_LEVELS \
  ) \
  as root:

  try:
    with root.child( shift = 0 ) as logger:
      sculpt_ensemble.run( phil = params.phil, logger = logger )

  except Sorry as e:
    raise

  except Exception:
    from phaser.logoutput_new import facade
    warn = facade.Package( channel = root.warn )
    warn.highlight( text = "An error occurred" )

    ( exc_type, exc_value, tb ) = sys.exc_info()
    warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
    raise
