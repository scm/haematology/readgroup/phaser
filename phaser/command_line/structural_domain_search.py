# LIBTBX_SET_DISPATCHER_NAME phenix.structural_domain_search

from __future__ import print_function

from phaser.pipeline import domain_analysis_lib
from phaser import sequtil
from phaser import tbx_utils

from libtbx.object_oriented_patterns import lazy_initialization
from functools import reduce

LOGGING_LEVELS = {
  "info": 40,
  "verbose": 20,
  "debug": 0,
  }


class StructureChunk(object):
  """
  Stores information about the residue segment
  """

  def __init__(self, name, classification, segment, sse_groups = []):

    self.name = name
    self.classification = classification
    self.segment = segment
    self.sse_groups = sse_groups


  def __str__(self):

    return "%s: %s" % ( self.name, self.segment )


class Structure(object):
  """
  A collection of residue segments, ordered according to consecutivity
  """

  def __init__(self, chunks, chain):

    self.chunks = chunks
    self.chain = chain


  def mmt(self):

    return self.chain.mmt


  def consecutivity(self):

    return self.chain.consecutivity()


  def entity_handler(self):

    return EntityHandler( consecutivity = self.consecutivity() )


  def indexer_base(self):

    return self.chain.indexer_base()


class EntityHandler(object):
  """
  Converts chunks to other entities (residue_groups and atoms)
  """

  def __init__(self, consecutivity):

    self.consecutivity = consecutivity


  @staticmethod
  def residue_group_count_in(chunkset):

    return sum( len( chunk.segment ) for chunk in chunkset )


  @staticmethod
  def atom_count_in(chunkset):

    count = 0

    for chunk in chunkset:
      count += sum( len( elem().atoms() ) for elem in chunk.segment )

    return count


  @staticmethod
  def residue_groups_in(chunkset):

    return sum( ( list( chunk.segment ) for chunk in chunkset ), [] )


  def residue_segments_in(self, chunkset):

    return sequtil.Segment.segmentize(
      sequence = sorted( self.residue_groups_in( chunkset = chunkset ) ),
      consecutivity = self.consecutivity,
      )


  def chunk_segments_in(self, chunkset):

    return sequtil.split(
      sequence = sorted( chunkset, key = lambda c: c.segment[0] ),
      consecutivity = lambda l, r: self.consecutivity( l.segment[-1], r.segment[0] ),
      )


  def residue_segment_from(self, chunkset):

    return domain_analysis_lib.ResidueSegments(
      segments = list( self.residue_segments_in( chunkset = chunkset ) ),
      )


class GraphHandler(object):
  """
  Creates graph from interaction dict
  """

  def __init__(self, chunks, interaction_between, threshold, prune_fraction):

    self.chunks = chunks
    self.interaction_between = interaction_between
    self.threshold = threshold
    self.prune_fraction = prune_fraction


  def average_interaction_weight(self):

    return sum( self.interaction_between.values() ) / len( self.interaction_between )


  def total_interaction_weight_for(self):

    total_weight_for = dict( ( s, 0 ) for s in self.chunks )

    for ( ( left, right ), weight ) in self.interaction_between.items():
      total_weight_for[ left ] += weight
      total_weight_for[ right ] += weight

    return total_weight_for


  def unpruned_interaction_graph(self, vertex_type = "vector", edge_type = "vector"):

    from boost_adaptbx import graph
    g = graph.adjacency_list(
      graph_type = "undirected",
      vertex_type = vertex_type,
      edge_type = edge_type,
      )

    vertex_for = {}

    for chunk in self.chunks:
      label = frozenset( [ chunk ] )
      vertex_for[ label ] = g.add_vertex( label = label )

    for ( ( left, right ), weight ) in self.interaction_between.items():
      if self.threshold <= weight:
        g.add_edge(
          vertex1 = vertex_for[ frozenset( [ left ] ) ],
          vertex2 = vertex_for[ frozenset( [ right ] ) ],
          weight = weight,
          )

    return g


  def get_interaction_graph(self, logger, vertex_type = "vector", edge_type = "vector"):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )

    info.heading( text = "Interaction graph calculation", level = 4 )
    info.paragraph_begin( indent = 2 )
    info.field( name = "Number of chunks", value = len( self.chunks ) )
    info.field( name = "Number of interactions", value = len( self.interaction_between ) )
    info.field_float( name = "Interaction threshold", value = self.threshold, digits = 2 )

    graph = self.unpruned_interaction_graph()
    info.field( name = "Number of vertices", value = graph.num_vertices() )
    info.field( name = "Number of edges", value = graph.num_edges() )
    info.field_float( name = "Prune fraction", value = self.prune_fraction, digits = 2 )
    prune_count = min(
      int( round( self.prune_fraction * graph.num_vertices() ) ),
      graph.num_vertices() - 1,
      )
    info.field( name = "Prune count", value = prune_count )

    if 0 < prune_count:
      info.heading( text = "Interaction graph simplification", level = 4 )
      self.prune_interaction_graph(
        graph = graph,
        prune_count = prune_count,
        logger = logger,
        )

    info.paragraph_end()
    return graph


  def get_segment_graph(
    self,
    segments,
    logger,
    vertex_type = "vector",
    edge_type = "vector",
    ):

    from boost_adaptbx import graph
    g = graph.adjacency_list(
      graph_type = "undirected",
      vertex_type = vertex_type,
      edge_type = edge_type,
      )

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    info.ordered_list_begin()

    for ( index, seg ) in enumerate( segments, start = 1 ):
      count = EntityHandler.residue_group_count_in( chunkset = seg )
      info.list_item_begin( text = "segment (%s residues):" % count )
      info.unordered_list_begin( separate_items = False )

      for chunk in seg:
        info.list_item_begin( text = str( chunk ) )
        info.list_item_end()

      info.list_end()
      g.add_vertex( label = ( seg, count, str( index ) ) )
      info.list_item_end()

    info.list_end()
    info.field( name = "Number of segments", value = g.num_vertices() )

    import itertools

    for ( lhs, rhs ) in itertools.combinations( g.vertices(), 2 ):
      weight = 0.0

      l_segs = g.vertex_label( vertex = lhs )[0]
      r_segs = g.vertex_label( vertex = rhs )[0]

      for ( l, r ) in itertools.product( l_segs, r_segs ):
        weight += self.interaction_between.get( frozenset( [ l, r ] ), 0 )

      if self.threshold < weight:
        g.add_edge( vertex1 = lhs, vertex2 = rhs, weight = weight )
        info.field_float(
          name = "Interaction between segment %s and %s" % (
            g.vertex_label( vertex = lhs )[2],
            g.vertex_label( vertex = rhs )[2],
            ),
          value = weight,
          digits = 2,
          )

    return g


  def prune_interaction_graph(self, graph, prune_count, logger):

    assert prune_count < graph.num_vertices()

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )

    from boost_adaptbx.graph import utility
    import operator
    vertex_contractor = utility.undirected_graph_vertex_contraction(
      joint_vertex_label = operator.or_,
      joint_edge_weight = operator.add,
      )
    info.ordered_list_begin()

    for i in range( prune_count ):
      info.list_item_begin( text = "join" )

      weakest = min(
        graph.vertices(),
        key = lambda v: sum( graph.edge_weight( edge = e ) for e in graph.out_edges( vertex = v ) )
        )
      info.field(
        name = "Vertex connected most weakly overall",
        value = ",".join( str( c ) for c in graph.vertex_label( vertex = weakest ) ),
        )

      strongest = max(
        graph.out_edges( vertex = weakest ),
        key = lambda e: graph.edge_weight( edge = e ),
        )
      info.field(
        name = "Strongest edge weight",
        value = graph.edge_weight( edge = strongest ),
        )

      target = graph.target( edge = strongest )

      assert graph.source( edge = strongest ) == weakest
      assert target != weakest # Non self edges
      info.field(
        name = "Join neighbour",
        value = ",".join( str( c ) for c in graph.vertex_label( vertex = target ) ),
        )

      vertex_contractor( graph, target, weakest )
      info.list_item_end()

    info.list_end()



class InteractionHandler(object):
  """
  Calculates cut properties
  """

  def __init__(self, chunks, interaction_between):

    self.chunkset = set( chunks )
    self.interaction_between = interaction_between


  def subset(self, chunkset):

    return self.__class__(
      chunks = chunkset,
      interaction_between = dict(
        ( k, v ) for ( k, v ) in self.interaction_between.items()
        if k <= chunkset
        )
      )


  def internal_interaction_strength_among(self, chunkset):

    assert chunkset <= self.chunkset
    interactions = 0

    for ( key, strength ) in self.interaction_between.items():
      if key <= chunkset:
        interactions += strength

    return interactions


  def interface_strength_between(self, chunkset):

    assert chunkset <= self.chunkset
    interactions = 0

    for ( key, strength ) in self.interaction_between.items():
      if len( key & chunkset ) == 1:
        interactions += strength

    return interactions


  def compactness_score_for(self, chunkset):

    return (
      float( self.internal_interaction_strength_among( chunkset = chunkset ) )
      / EntityHandler.atom_count_in( chunkset = chunkset )
      )


  def interface_score_for(self, chunkset):

    return (
      float( self.interface_strength_between( chunkset = chunkset ) )
      / self.internal_interaction_strength_among( chunkset = chunkset )
      )


class GraphPartition(object):
  """
  A struct for code clarity
  """

  def __init__(self, labels, chunkset, segment):

    self.labels = labels
    self.chunkset = set( chunkset )
    self.segment = segment


  def chunk_count(self):

    return len( self.chunkset )


  def residue_count(self):

    return self.segment.residue_count()


  def segment_count(self):

    return self.segment.segment_count()


  def residue_groups(self):

    return self.segment.residue_groups()


  @classmethod
  def from_vertices(cls, graph, vertices, entity_handler):

    return cls.from_labels(
      labels = [ graph.vertex_label( vertex = v ) for v in vertices ],
      entity_handler = entity_handler,
      )


  @classmethod
  def from_labels(cls, labels, entity_handler):

    import operator

    chunkset = reduce( operator.or_, labels )
    segment = entity_handler.residue_segment_from( chunkset = chunkset )
    return cls( labels = labels, chunkset = chunkset, segment = segment )


class Cut(object):
  """
  Cut of a graph
  """

  def __init__(self, weight, lhs, rhs):

    self.weight = weight
    self.source = lhs
    self.sink = rhs


  @property
  def partitions(self):

    return [ self.source, self.sink ]


  def score(self, interaction_handler):

    return (
      self.source.segment_count() * self.sink.segment_count()
      * interaction_handler.interface_score_for( chunkset = self.source.chunkset )
      * interaction_handler.interface_score_for( chunkset = self.sink.chunkset )
      / ( self.source.residue_count() * self.sink.residue_count() )
      )


class MultiCut(object):
  """
  Multiple cut of a graph
  """

  def __init__(self, partitions):

    self.partitions = partitions


def cut_absolute_clustering_distance(lhs, rhs):

  l_source_set = lhs[0]
  l_sink_set = lhs[1]
  r_source_set = rhs[0]
  r_sink_set = rhs[1]

  return min(
    len( l_source_set ^ r_source_set ) + len( l_sink_set ^ r_sink_set ),
    len( l_source_set ^ r_sink_set ) + len( l_sink_set ^ r_source_set ),
    )


def no_chunker(sequence):
  """
  A no-op chunker
  """

  yield sequence


def beta_sheet_registrar(ssid, elements, mapping):

  from phaser import secondary_structure
  assert isinstance( ssid, secondary_structure.BetaStrand )
  mapping.setdefault( ssid.sheet, set() ).update( elements )


class SSEAssemblyRegister(object):
  """
  Registers higher order assemblies
  """

  def __init__(self, handler_for):

    self.elements_for = {}
    self.handler_for = handler_for


  def groups_for(self, elements):

    return [
      ident for ( ident, members ) in self.elements_for.items()
      if members.intersection( elements )
      ]


  def __call__(self, ssid, elements):

    if ssid.__class__ in self.handler_for:
      self.handler_for[ ssid.__class__ ](
        ssid = ssid,
        elements = elements,
        mapping = self.elements_for,
        )


class StructureChopper(object):
  """
  Converts a structure into a series of segments
  """

  def __init__(self, default_chunker = no_chunker):

    self.default_chunker = default_chunker
    self.chunker_for = {}
    self.sse_assembly_handler_for = {}


  def register_chunker(self, mmt, classification, chunker):

    self.chunker_for[ ( mmt, classification ) ] = chunker


  def register_sse_assembly_handler(self, mmt, ssclass, handler):

    self.sse_assembly_handler_for.setdefault( mmt, {} )[ ssclass ] = handler


  def __call__(self, chain):

    elements_in = {}
    assembly_register = SSEAssemblyRegister(
      handler_for = self.sse_assembly_handler_for[ chain.mmt ],
      )

    for ann in chain.fully_annotated_secondary_structure_elements():
      ssclass = ann.identifier.__class__
      rgs = ann.residue_groups
      elements_in.setdefault( ssclass, set() ).update( rgs )
      assembly_register( ssid = ann.identifier, elements = rgs )

    chunks = []

    for ( ssclass, elems ) in elements_in.items():
      chunker = self.chunker_for.get( ( chain.mmt, ssclass ), self.default_chunker )
      segiter = sequtil.Segment.segmentize(
        sequence = sorted( elems ),
        consecutivity = chain.consecutivity(),
        )
      prefix = ssclass.classification()

      for ( i_seg, segment ) in enumerate( segiter, start = 1 ):
        for ( i_chunk, s ) in enumerate( chunker( sequence = segment ), start = 1 ):
          rs = StructureChunk(
            name = "%s-%s-chunk-%s" % ( prefix, i_seg, i_chunk ),
            classification = ssclass,
            segment = s,
            sse_groups = assembly_register.groups_for( elements = s ),
            )
          chunks.append( rs )

    assert chunks
    chunks.sort( key = lambda s: s.segment[0] )
    chunkiter = iter( chunks )
    left = chunkiter.next()

    for right in chunkiter:
      assert left.segment[-1] < right.segment[0]
      left = right

    return Structure( chunks = chunks, chain = chain )


  @classmethod
  def from_params(cls, params):

    chopper = cls( default_chunker = no_chunker )

    from phaser import mmtype
    from phaser import secondary_structure

    import functools

    chopper.register_chunker(
      mmt = mmtype.PROTEIN,
      classification = secondary_structure.Coil,
      chunker = functools.partial(
        sequtil.chunk_sequence,
        chunksize = params.coil_max_size,
        ),
      )

    if params.helix_max_size:
      chopper.register_chunker(
        mmt = mmtype.PROTEIN,
        classification = secondary_structure.AlphaHelix,
        chunker = functools.partial(
          sequtil.chunk_sequence,
          chunksize = params.coil_max_size,
          ),
        )

    if params.strand_max_size:
      chopper.register_chunker(
        mmt = mmtype.PROTEIN,
        classification = secondary_structure.BetaStrand,
        chunker = functools.partial(
          sequtil.chunk_sequence,
          chunksize = params.coil_max_size,
          ),
        )

    chopper.register_sse_assembly_handler(
      mmt = mmtype.PROTEIN,
      ssclass = secondary_structure.BetaStrand,
      handler = beta_sheet_registrar,
      )

    return chopper


  @classmethod
  def run(cls, chain, params):

    chopper = cls.from_params( params = params )
    return chopper( chain = chain )


class LongRangeInteractionCalculator(object):
  """
  Calculates long-range interactions between residue segments
  """

  def __init__(self, max_spatial_distance = 4.0):

    self.max_spatial_distance = max_spatial_distance


  def __call__(self, structure):

    from mmtbx.geometry import indexing
    base = structure.indexer_base()
    indexer_for = {}

    for chunk in structure.chunks:
      indexer = indexing.structure_indexer.hash(
        base = base,
        maxdist = self.max_spatial_distance,
        )

      for elem in chunk.segment:
        indexer.add_atoms_in( obj = elem() )

      indexer_for[ chunk ] = indexer

    import itertools
    interaction_between = {}

    for ( left, right ) in itertools.combinations( structure.chunks, 2 ):
      weight = 0.0

      if len( left.segment ) <= len( right.segment ):
        elems = left.segment
        indexer = indexer_for[ right ]

      else:
        elems = right.segment
        indexer = indexer_for[ left ]

      for elem in elems:
        weight += indexer.interaction_counts_with( obj = elem() )

      if 0 < weight:
        interaction_between[ frozenset( ( left, right ) ) ] = weight

    return interaction_between


  def __str__(self):

    return "intersegment long-range contacts"


  @classmethod
  def from_params(cls, params):

    return cls( max_spatial_distance = params.max_spatial_distance )


class MainchainInteractionCalculator(object):
  """
  Calculates interactions between consecutive residue segments
  """

  def __init__(self, default_strength = 10):

    self.default_strength = default_strength
    self.strength_for = {}


  def register_connection_strength(self, mmt, class1, class2, strength):

    self.strength_for[ ( mmt, frozenset( ( class1, class2 ) ) ) ] = strength


  def __call__(self, structure):

    chunkiter = iter( structure.chunks )
    mmt = structure.mmt()
    consecutivity = structure.consecutivity()

    left = chunkiter.next()
    interaction_between = {}

    for right in chunkiter:
      if consecutivity( left.segment[-1], right.segment[0] ):
        key = ( mmt, frozenset( ( left.classification, right.classification ) ) )
        strength = self.strength_for.get( key, self.default_strength )
        interaction_between[ frozenset( ( left, right ) ) ] = strength

    return interaction_between


  def __str__(self):

    return "intersegment sequential connections"


  @classmethod
  def from_params(cls, params):

    calc = cls( default_strength = params.default )

    from phaser import mmtype
    from phaser import secondary_structure

    calc.register_connection_strength(
      mmt = mmtype.PROTEIN,
      class1 = secondary_structure.AlphaHelix,
      class2 = secondary_structure.AlphaHelix,
      strength = params.helix_to_helix_connection,
      )

    calc.register_connection_strength(
      mmt = mmtype.PROTEIN,
      class1 = secondary_structure.BetaStrand,
      class2 = secondary_structure.BetaStrand,
      strength = params.strand_to_strand_connection,
      )

    return calc


class ContextualInteractionCalculator(object):
  """
  Calculates interaction bonus for residue segments based on interactions between
  left- and right-hand neighbours
  """

  def __init__(self, weightcalc, bordercalc):

    self.weightcalc = weightcalc
    self.bordercalc = bordercalc


  def __call__(self, structure, principal_interaction_between):

    consecutivity = structure.consecutivity()
    chunks = structure.chunks
    lociter = sequtil.localize( sequence = chunks, width = 1, padding = None)
    interaction_between = {}

    for ( left, centre, right ) in lociter:
      if left is None or right is None:
        continue

      l_consec = consecutivity( left.segment[-1], centre.segment[0] )
      r_consec = consecutivity( centre.segment[-1], right.segment[0] )

      if l_consec and r_consec:
        key = frozenset( ( left, right ) )
        bonus = self.weightcalc(
          length = len( centre.segment ),
          strength = principal_interaction_between.get( key, 0 ),
          )
        lckey = frozenset( ( left, centre ) )
        interaction_between[ lckey ] = interaction_between.get( lckey, 0 ) + bonus
        rckey = frozenset( ( right, centre ) )
        interaction_between[ rckey ] = interaction_between.get( rckey, 0 ) + bonus

      elif l_consec:
        key = frozenset( ( left, centre ) )
        bonus = self.bordercalc( length = len( centre.segment ) )
        interaction_between[ key ] = interaction_between.get( key, 0 ) + bonus

      elif r_consec:
        key = frozenset( ( centre, right ) )
        bonus = self.bordercalc( length = len( centre.segment ) )
        interaction_between[ key ] = interaction_between.get( key, 0 ) + bonus

    if 2 <= len( chunks ):
      if consecutivity( chunks[0].segment[-1], chunks[1].segment[0] ):
        lkey = frozenset( ( chunks[0], chunks[1] ) )
        bonus = self.bordercalc( length = len( chunks[0].segment ) )
        interaction_between[ lkey ] = interaction_between.get( lkey, 0 ) + bonus

      if consecutivity( chunks[-2].segment[-1], chunks[-1].segment[0] ):
        rkey = frozenset( ( chunks[-2], chunks[-1] ) )
        bonus = self.bordercalc( length = len( chunks[-1].segment ) )
        interaction_between[ rkey ] = interaction_between.get( rkey, 0 ) + bonus

    return interaction_between


  def __str__(self):

    return "intersegment contextual connection bonus"


  @classmethod
  def from_params(cls):

    calc = cls(
      weightcalc = lambda length, strength: pow( 2, min( 3 - length, 0 ) ) * strength,
      bordercalc = lambda length: pow( 2, min( 10 - length, 0 ) ) * 20,
      )

    return calc


class AssemblyInteractionCalculator(object):
  """
  Adds extra strength to higher order secondary structure assemblies
  """

  def __init__(self):

    self.bonus_strength_for = {}


  def register_strength_bonus(self, mmt, ssclass, strength):

    self.bonus_strength_for[ ( mmt, ssclass ) ] = strength


  def __call__(self, structure):

    mmt = structure.mmt()
    segments_in = {}

    for s in structure.chunks:
      for gid in s.sse_groups:
        segments_in.setdefault( gid, set() ).add( s )

    import itertools
    interaction_between = {}

    for ( gid, members ) in segments_in.items():
      key = ( mmt, gid.__class__ )

      if key in self.bonus_strength_for:
        bonus = self.bonus_strength_for[ key ]

        for ( left, right ) in itertools.combinations( members, 2 ):
          ikey = frozenset( ( left, right ) )
          interaction_between[ ikey ] = interaction_between.get( ikey, 0 ) + bonus

    return interaction_between


  def __str__(self):

    return "intersegment assembly bonus"


  @classmethod
  def from_params(cls, params):

    calc = cls()

    from phaser import mmtype
    from phaser import secondary_structure

    calc.register_strength_bonus(
      mmt = mmtype.PROTEIN,
      ssclass = secondary_structure.BetaSheet,
      strength = params.sheet_bonus,
      )

    return calc


class InteractionCalculation(object):
  """
  Holds piecewise results of the interaction calculation
  """

  def __init__(
    self,
    structure,
    interaction_handler_key,
    graph_handler_key,
    graph_interaction_threshold,
    graph_pruning_fraction,
    ):

    self.structure = structure
    self.data = domain_analysis_lib.Data()
    self.interaction_handler_key = interaction_handler_key
    self.graph_handler_key = graph_handler_key
    self.graph_interaction_threshold = graph_interaction_threshold
    self.graph_pruning_fraction = graph_pruning_fraction


  def get_interaction_handler(self):

    return InteractionHandler(
      chunks = self.structure.chunks,
      interaction_between = self.data.get( key = self.interaction_handler_key ),
      )


  def get_graph_handler(self):

    return GraphHandler(
      chunks = self.structure.chunks,
      interaction_between = self.data.get( key = self.graph_handler_key ),
      threshold = self.graph_interaction_threshold,
      prune_fraction = self.graph_pruning_fraction,
      )


class InteractionCalculatorWrapper(object):
  """
  A wrapper object that maps the interface of a generic calculation to that
  of an InteractionCalculation

  This is written explicitly for the Calculators above, and therefore the
  structure property is always passed into the actual call
  """

  def __init__(self, resultname, calculator, **kwargmaps):

    self.resultname = resultname
    self.calculator = calculator
    self.kwargmaps = kwargmaps


  def __call__(self, intcalc):

    kwargs = dict(
      ( kwargname, intcalc.data.get( key = kwargkey ) )
      for ( kwargname, kwargkey ) in self.kwargmaps.items()
      )

    intcalc.data.set(
      key = self.resultname,
      value = self.calculator( structure = intcalc.structure, **kwargs ),
      )


  def __str__(self):

    if self.kwargmaps:
      return "Calculate %s (from structure and using data from %s) and set as '%s'" % (
        self.calculator,
        ",".join( "'%s'" % key for key in self.kwargmaps.values() ),
        self.resultname,
        )

    else:
      return "Calculate %s from structure and set as '%s'" % (
        self.calculator,
        self.resultname,
        )


class InteractionSummer(object):
  """
  Sums a set of interactions
  """

  def __init__(self, resultname, individuals):

    self.resultname = resultname
    self.individuals = individuals


  def __call__(self, intcalc):

    interaction_between = {}

    for intdict in [ intcalc.data.get( key = k ) for k in self.individuals ]:
      for ( key, strength ) in intdict.items():
        interaction_between[ key ] = interaction_between.get( key, 0 ) + strength

    intcalc.data.set( key = self.resultname, value = interaction_between )


  def __str__(self):

    return "Set '%s' as sum of datasets %s" % (
      self.resultname,
      ",".join( "'%s'" % name for name in self.individuals )
      )


class InteractionDeleter(object):
  """
  Removes a set of data from the object
  """

  def __init__(self, identifier):

    self.identifier = identifier


  def __call__(self, intcalc):

    intcalc.data.delete( key = self.identifier )


  def __str__(self):

    return "Remove dataset '%s'" % self.identifier


class ConfigurableInteractionCalculator(object):
  """
  An object to orchestrate the calculation
  """

  def __init__(
    self,
    steps,
    interaction_handler_key,
    graph_handler_key,
    graph_interaction_threshold,
    graph_pruning_fraction,
    ):

    self.steps = steps
    self.interaction_handler_key = interaction_handler_key
    self.graph_handler_key = graph_handler_key
    self.graph_interaction_threshold = graph_interaction_threshold
    self.graph_pruning_fraction = graph_pruning_fraction


  def __call__(self, structure, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    verbose = facade.Package( channel = logger.verbose )

    intcalc = InteractionCalculation(
      structure = structure,
      interaction_handler_key = self.interaction_handler_key,
      graph_handler_key = self.graph_handler_key,
      graph_interaction_threshold = self.graph_interaction_threshold,
      graph_pruning_fraction = self.graph_pruning_fraction,
      )
    info.ordered_list_begin()

    import time

    for step in self.steps:
      info.list_item_begin( text = str( step ) )
      info.task_begin( text = "Doing calculation" )
      start = time.clock()
      step( intcalc = intcalc )
      info.task_end()
      verbose.text(
        text = "Calculation took %.2f seconds" % ( time.clock() - start )
        )
      info.list_item_end()

    info.list_end()

    return intcalc


class CutDynamicScoreFunction(object):
  """
  A score function that is dependent on the search characteristics
  """

  def __init__(self, endweight, normalizer, interaction_handler):

    self.endweight = endweight
    self.normalizer = normalizer
    self.interaction_handler = interaction_handler


  def __call__(self, cut):

    import math
    return ( cut.score( interaction_handler = self.interaction_handler )
      * math.exp( ( cut.weight - self.endweight ) / self.normalizer ) )


def calculate_effective_count(cuts, endweight, normalizer):

  import math
  weights = [ c.weight for c in cuts ]
  lowest = min( weights )
  return sum( math.exp( ( lowest - w ) / normalizer ) for w in weights )


class Report(object):
  """
  Report of domain evaluation
  """

  def __init__(self):

    from collections import OrderedDict
    self.result_for = OrderedDict()
    self.faileds = set()


  def record_success(self, name, value):

    self.result_for[ name ] = value


  def record_failure(self, name, value, message):

    self.faileds.add( name )
    self.result_for[ name ] = ( value, message )


  def results(self):

    for ( name, data ) in self.result_for.items():
      if name in self.faileds:
        ( value, message ) = data
        yield "%s: %s (FAIL: %s)" % ( name, value, message )

      else:
        yield "%s: %s (PASS)" % ( name, data )


  def failures(self):

    for ( name, data ) in self.result_for.items():
      if name in self.faileds:
        ( value, message ) = data
        yield "%s: %s (FAIL: %s)" % ( name, value, message )


  def successes(self):

    for ( name, data ) in self.result_for.items():
      if name not in self.faileds:
        yield "%s: %s (PASS)" % ( name, data )


  def __nonzero__(self):

    return not self.faileds


  def __str__(self):

    return "\n".join( self.results() )


class DomainEvaluator(object):
  """
  Accepts or rejects domains
  """

  def __init__(self, min_size):

    self.min_size = min_size
    self.criteria = []


  def register(self, name, calculation, test, formatter, message):

    self.criteria.append( ( name, calculation, test, formatter, message ) )


  def partitionable(self, partition):

    return 2 * self.min_size <= partition.residue_count()


  def evaluate(self, partition, interaction_handler):

    report = Report()

    for ( name, calculation, test, formatter, message ) in self.criteria:
      value = calculation( partition = partition, interaction_handler = interaction_handler )

      if test( value ):
        report.record_success( name = name, value = formatter( value ) )

      else:
        report.record_failure( name = name, value = formatter( value ), message = message )
        break

    return report


  @staticmethod
  def ge(value):

    import operator
    from functools import partial

    # NOTE: reverse operator because only the first arg can be bound
    return partial( operator.le, value )


  @staticmethod
  def le(value):

    import operator
    from functools import partial

    # NOTE: reverse operator because only the first arg can be bound
    return partial( operator.ge, value )


  @staticmethod
  def truth():

    import operator
    return operator.truth


  @staticmethod
  def not_():

    import operator
    return operator.not_


  @staticmethod
  def is_(value):

    import operator
    from functools import partial
    return partial( operator.is_, value )


  @staticmethod
  def residue_group_count(partition, interaction_handler):

    return partition.residue_count()


  @staticmethod
  def compactness_score(partition, interaction_handler):

    return interaction_handler.compactness_score_for( chunkset = partition.chunkset )


  @staticmethod
  def interface_score(partition, interaction_handler):

    return interaction_handler.interface_score_for( chunkset = partition.chunkset )


  @classmethod
  def from_params(cls, params):

    evaluator = cls( min_size = params.min_size )

    evaluator.register(
      name = "size",
      calculation = cls.residue_group_count,
      test = cls.ge( value = params.min_size ),
      formatter = lambda v: str( v ),
      message = "too small",
      )
    evaluator.register(
      name = "compactness",
      calculation = cls.compactness_score,
      test = cls.ge( value = params.min_compactness ),
      formatter = lambda v: "%.3f" % v,
      message = "compactness too low",
      )
    evaluator.register(
      name = "relative interface strength",
      calculation = cls.interface_score,
      test = cls.le( value = params.max_interface_strength ),
      formatter = lambda v: "%.3f" % v,
      message = "interface too strong",
      )

    return evaluator

# Standalone functions
def find_possible_cuts(
  graph,
  max_iterations,
  target_count,
  evaluator,
  interaction_handler,
  entity_handler,
  logger,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )

  from boost_adaptbx.graph import minimum_cut_enumerate

  stiter = minimum_cut_enumerate.stirling_adaptive_tree_enumeration(
    graph = graph,
    reference = graph.vertices().next(),
    sw_path_vertex_selector = minimum_cut_enumerate.arbitrary_selection_from_set,
    bk_path_vertex_selector = minimum_cut_enumerate.BKEqualSizeSelector(),
    maxiter = max_iterations + 10 * graph.num_vertices() + 5 * graph.num_edges(),
    )

  info.ordered_list_begin()
  cuts = []

  for ( index, ( weight, source, sink ) ) in enumerate( stiter, start = 1 ):
    if max_iterations < index:
      info.unformatted_text(
        text = "Stopping search as maximum iterations has been reached"
        )
      break

    if target_count <= len( cuts ):
      info.unformatted_text(
        text = "Stopping search as number of cuts has reached target"
        )
      break

    part_source = GraphPartition.from_vertices(
      graph = graph,
      vertices = source,
      entity_handler = entity_handler,
      )
    part_sink = GraphPartition.from_vertices(
      graph = graph,
      vertices = sink,
      entity_handler = entity_handler,
      )

    info.list_item_begin(
      text = "partition: ( %s SSEs, %s residues <-> %s SSEs, %s residues )" % (
        part_source.chunk_count(),
        part_source.residue_count(),
        part_sink.chunk_count(),
        part_sink.residue_count(),
        )
      )
    info.field_float( name = "weight", value = weight, digits = 3 )

    rep_source = evaluator.evaluate(
      partition = part_source,
      interaction_handler = interaction_handler,
      )
    rep_sink = evaluator.evaluate(
      partition = part_sink,
      interaction_handler = interaction_handler,
      )

    if bool( rep_source ) and bool( rep_sink ):
      info.unformatted_text( text = "Domain criteria: PASS")
      cut = Cut( weight = weight, lhs = part_source, rhs = part_sink )
      info.field_scientific(
        name = "Cut score",
        value = cut.score( interaction_handler = interaction_handler ),
        digits = 3,
        )
      cuts.append( cut )

    else:
      fails = []

      if not bool( rep_source ):
        fails.append( "left" )

      if not bool( rep_sink ):
        fails.append( "right" )

      info.unformatted_text( text = "Domain criteria FAIL: %s" % ",".join( fails ) )

    info.heading( text = "Left domain:", level = 6 )
    info.preformatted_text( text = str( part_source.segment ), indent = 2 )
    verbose.heading( text = "Results:", level = 6 )
    verbose.preformatted_text( text = str( rep_source ), indent = 2 )

    info.heading( text = "Right domain:", level = 6 )
    info.preformatted_text( text = str( part_sink.segment ), indent = 2 )
    verbose.heading( text = "Results:", level = 6 )
    verbose.preformatted_text( text = str( rep_sink ), indent = 2 )

    info.list_item_end()

  info.list_end()
  return ( cuts, weight )


def adjusted_cut(cut, graph, vertex_for, entity_handler, min_domain_size):

  if len( cut.source.labels ) <= len( cut.sink.labels ):
    small = cut.source
    big = cut.sink

  else:
    small = cut.sink
    big = cut.source

  from boost_adaptbx.graph import maximum_clique
  from boost_adaptbx.graph import connected_component_algorithm as cca
  import operator

  subgraph =maximum_clique.selected_subgraph(
    graph = graph,
    vertices = [ vertex_for[ v ] for v in small.labels ],
    )
  small_comps = cca.connected_components( graph = subgraph )
  small_comps.sort( key = len )

  assert 0 < len( small_comps )
  small_labels_adjusted = [
    subgraph.vertex_label( vertex = v ) for v in small_comps.pop() # Largest component
    ]
  big_labels = list( big.labels ) # Make copy

  for comp in small_comps:
    labels = [ subgraph.vertex_label( vertex = v ) for v in comp ]
    rcount = EntityHandler.residue_group_count_in(
      chunkset = reduce( operator.or_, labels ),
      )

    if rcount < min_domain_size:
      big_labels.extend( labels )

    else:
      small_labels_adjusted.extend( labels )

  subgraph = maximum_clique.selected_subgraph(
    graph = graph,
    vertices = [ vertex_for[ v ] for v in big_labels ],
    )
  big_comps = cca.connected_components( graph = subgraph )
  big_comps.sort( key = len )

  assert 0 < len( big_comps )
  big_labels_adjusted = [
    subgraph.vertex_label( vertex = v ) for v in big_comps.pop()
    ] # Largest components

  for comp in big_comps:
    labels = [ subgraph.vertex_label( vertex = v ) for v in comp ]
    rcount = EntityHandler.residue_group_count_in(
      chunkset = reduce( operator.or_, labels ),
      )

    if rcount < min_domain_size:
      small_labels_adjusted.extend( labels )

    else:
      big_labels_adjusted.extend( labels )

  adjusted = Cut(
    weight = cut.weight,
    lhs = GraphPartition.from_labels(
      labels = small_labels_adjusted,
      entity_handler = entity_handler,
      ),
    rhs = GraphPartition.from_labels(
      labels = big_labels_adjusted,
      entity_handler = entity_handler,
      ),
    )

  return adjusted


def combine_cuts(lhs, rhs, evaluator, interaction_handler, entity_handler, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel =logger.info )

  info.heading( text = "Left:", level = 6 )
  info.ordered_list_begin( marker = "i", separate_items = False )
  l_labels = []

  for part in lhs.partitions:
    info.list_item_begin( text = "cut" )
    info.preformatted_text( text = str( part.segment ), indent = 2 )
    l_labels.append( set( part.labels ) )
    info.list_item_end()

  info.list_end()

  info.heading( text = "Right:", level = 6 )
  info.ordered_list_begin( marker = "i", separate_items = False )
  r_labels = []

  for part in rhs.partitions:
    info.list_item_begin( text = "cut" )
    info.preformatted_text( text = str( part.segment ), indent = 2 )
    r_labels.append( set( part.labels ) )
    info.list_item_end()

  info.list_end()

  partitions = []

  for l_part in l_labels:
    for r_part in r_labels:
      intersect = l_part & r_part

      if intersect:
        partition = GraphPartition.from_labels(
          labels = intersect,
          entity_handler = entity_handler,
          )

        report = evaluator.evaluate(
          partition = partition,
          interaction_handler = interaction_handler,
          )

        if not report:
          info.unformatted_text(
            text = "Reject combination as it leads to bad domains",
            )
          raise domain_analysis_lib.DomainAnalysisException("Not combineable")

        partitions.append( partition )

  combined = MultiCut( partitions = partitions )

  info.heading( text = "Combined:", level = 6 )
  info.ordered_list_begin( marker = "i", separate_items = True )

  for part in combined.partitions:
    info.list_item_begin( text = "cut" )
    info.preformatted_text( text = str( part.segment ), indent = 2 )
    l_labels.append( set( part.labels ) )
    info.list_item_end()

  info.list_end()

  return combined


def connected_component_analysis(graph, entity_handler, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  from boost_adaptbx.graph import connected_component_algorithm as cca
  import operator

  partitions = []
  info.ordered_list_begin()

  for comp in cca.connected_components( graph = graph ):
    info.list_item_begin( text = "component" )
    part = GraphPartition.from_vertices(
      graph = graph,
      vertices = comp,
      entity_handler = entity_handler,
      )
    partitions.append( part )
    info.preformatted_text( text = str( part.segment ) )
    info.list_item_end()

  info.list_end()

  return partitions


def multisegment_divide_contacted_vertex_label(lhs, rhs):

  ( l_segs, l_elems, l_index ) = lhs
  ( r_segs, r_elems, r_index ) = rhs
  return ( l_segs + r_segs, l_elems + r_elems, l_index + "+" + r_index )


def multisegment_domain_split(
  partition,
  min_domain_size,
  graph_handler,
  entity_handler,
  strongly_connected_threshold,
  logger,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  info.heading( text = "Segment graph calculation", level = 5 )
  graph = graph_handler.get_segment_graph(
    segments = entity_handler.chunk_segments_in( chunkset = partition.chunkset ),
    logger = logger,
    )

  if 1 < graph.num_vertices():
    info.heading( text = "Segment graph contraction", level = 5 )

    from boost_adaptbx.graph import utility
    import operator
    vertex_contractor = utility.undirected_graph_vertex_contraction(
      joint_vertex_label = multisegment_divide_contacted_vertex_label,
      joint_edge_weight = operator.add,
      )

    while 1 < graph.num_vertices():
      smallest = min(
        graph.vertices(),
        key = lambda v: graph.vertex_label( vertex = v )[1],
        )
      smallest_label = graph.vertex_label( vertex = smallest )
      info.field( name = "Shortest segment", value = smallest_label[2] )
      info.field( name = "Residue group size", value = smallest_label[1] )

      if min_domain_size <= smallest_label[1]:
        info.unformatted_text(
          text = "Smallest segment longer than domain size, end contraction"
          )
        break

      strongest = max(
        graph.out_edges( vertex = smallest ),
        key = lambda e: graph.edge_weight( edge = e ),
        )

      target = graph.target( edge = strongest )
      target_label = graph.vertex_label( vertex = target )

      assert graph.source( edge = strongest ) == smallest
      assert target != smallest
      info.field( name = "Strongest neighbour", value = target_label[2] )
      info.field_float(
        name = "Connection strength",
        value = graph.edge_weight( edge = strongest ),
        digits = 2,
        )

      info.unformatted_text(
        text = "Contracted segments %s and %s" % ( smallest_label[2], target_label[2] ),
        )
      vertex_contractor( graph, target, smallest )

  partitions = []
  info.field(
    name = "Number of sufficiently long segments",
    value = graph.num_vertices(),
    )

  while 1 < graph.num_vertices():
    weakest = min(
      graph.vertices(),
      key = lambda v: sum(
        graph.edge_weight( edge = e ) for e in graph.out_edges( vertex = v )
        )
      )
    strength = sum(
      graph.edge_weight( edge = e ) for e in graph.out_edges( vertex = weakest )
      )
    info.field(
      name = "Weakest connected segment",
      value = graph.vertex_label( vertex = weakest )[2],
      )
    info.field( name = "Total connection strength", value = strength )

    if strongly_connected_threshold <= strength:
      info.unformatted_text(
        text = "Weakest connection is over threshold, stop split evaluation"
        )
      break

    partitions.append( graph.vertex_label( vertex = weakest )[0] )
    info.unformatted_text(
      text = "Split segment %s as a separate domain" % graph.vertex_label( vertex = weakest )[2],
      )
    graph.remove_vertex( vertex = weakest )

  partitions.append(
    sum( ( graph.vertex_label( vertex = v )[0] for v in graph.vertices() ), [] )
    )

  info.field( name = "Final number of domains", value = len( partitions ) )

  return partitions


class quick_algorithm(object):
  """
  Quick algorithm for finding domains
  """

  def __init__(
    self,
    macrocycles,
    max_cut_iterations,
    target_cut_count,
    domain_evaluator,
    cluster_radius,
    strongly_connected_threshold,
    ):

    self.macrocycles = macrocycles
    self.max_cut_iteration = max_cut_iterations
    self.target_cut_count = target_cut_count
    self.domain_evaluator = domain_evaluator
    self.cluster_radius = cluster_radius
    self.strongly_connected_threshold = strongly_connected_threshold


  def __call__(self, graph_handler, interaction_handler, entity_handler, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    verbose = facade.Package( channel = logger.verbose )

    info.unformatted_text( text = "Using quick algorithm" )
    normalizer = graph_handler.average_interaction_weight()
    info.field_float( name = "Boltzmann normalizer", value = normalizer, digits = 2 )

    graph = graph_handler.get_interaction_graph( logger = logger )
    vertex_for = dict(
      ( graph.vertex_label( vertex = v ), v ) for v in graph.vertices()
      )

    info.heading( text = "Connected component analysis", level = 4 )

    connecteds = connected_component_analysis(
      graph = graph,
      entity_handler = entity_handler,
      logger = logger,
      )

    info.heading( text = "Initial domain analysis", level = 4 )
    decomposables = []

    for partition in connecteds:
      report = self.domain_evaluator.evaluate(
        partition = partition,
        interaction_handler = interaction_handler,
        )
      verbose.heading( text = "Domain report:", level = 5 )
      verbose.preformatted_text( text = str( report ), indent = 2 )

      if not report:
        info.unformatted_text( text = "Domain criteria FAIL, discarding" )

      else:
        info.unformatted_text( text = "Accepting for decomposition" )
        decomposables.append( partition )

    from boost_adaptbx.graph import maximum_clique
    singles = []

    for iternum in range( 1, self.macrocycles + 1 ):
      info.heading( text = "Iteration %s" % iternum, level = 3 )
      info.unformatted_text( text = "%s structures to decompose" % len( decomposables ) )

      info.ordered_list_begin()
      next_dcs = []

      for partition in decomposables:
        info.list_item_begin(
          text = "structure (%s residues)" % partition.residue_count(),
          )
        info.preformatted_text( text = str( partition.segment ), indent = 2 )

        if not self.domain_evaluator.partitionable( partition = partition ):
          info.unformatted_text( text = "Structure too small to be divided" )
          info.unformatted_text( text = "Accepting this segment as a SINGLE domain" )
          singles.append( partition )
          info.list_item_end()
          continue

        info.heading( text = "Structure cut enumeration", level = 4 )
        subset_interaction_handler = interaction_handler.subset(
          chunkset = partition.chunkset,
          )
        ( cuts, endweight ) = find_possible_cuts(
          graph = maximum_clique.selected_subgraph(
            graph = graph,
            vertices = [ vertex_for[ l ] for l in partition.labels ],
            ),
          max_iterations = self.max_cut_iteration,
          target_count = self.target_cut_count,
          evaluator = self.domain_evaluator,
          interaction_handler = subset_interaction_handler,
          entity_handler = entity_handler,
          logger = logger,
          )

        if not cuts:
          info.unformatted_text( text = "No cuts found, treating it as a single domain" )
          singles.append( partition )
          info.list_item_end()
          continue

        info.field( name = "Number of cuts found", value = len( cuts ) )
        info.task_begin( text = "Adjusting cuts for connectivity" )
        adjusteds = [
          adjusted_cut(
            cut = c,
            graph = graph,
            vertex_for = vertex_for,
            entity_handler = entity_handler,
            min_domain_size = self.domain_evaluator.min_size,
            )
          for c in cuts
          ]
        info.task_end()

        info.task_begin( text = "Clustering cuts" )
        resset_cuts = [
          ( set( c.source.residue_groups() ), set( c.sink.residue_groups() ), c )
          for c in adjusteds
          ]
        clusters = domain_analysis_lib.cluster_equivalent_domains(
          domains = resset_cuts,
          clustering_distance = cut_absolute_clustering_distance,
          cluster_radius = self.cluster_radius,
          )
        info.task_end()

        info.unformatted_text( text = "%s equivalent groups found" % len( clusters ) )
        scorer = CutDynamicScoreFunction(
          endweight = endweight,
          normalizer = normalizer,
          interaction_handler = subset_interaction_handler,
          )
        bests = []
        info.ordered_list_begin()

        for group in clusters:
          info.list_item_begin( text = " group: %s cuts" % len( group ) )
          info.text( text = "Best representative:" )
          best = min(
            [ ( scorer( cut = p[-1] ), p[-1] ) for p in group ],
            key = lambda s: s[0],
            )
          info.field_scientific(
            name = "Cut score",
            value = best[-1].score( interaction_handler = subset_interaction_handler ),
            digits = 3,
            )
          info.field_scientific(
            name = "Individual score",
            value = best[0],
            digits = 3,
            )
          effcount = calculate_effective_count(
            cuts = [ p[-1] for p in group ],
            endweight = endweight,
            normalizer = normalizer,
            )
          info.field_scientific(
            name = "Effective cluster count",
            value = effcount,
            digits = 2,
            )
          cscore = best[0] / effcount
          info.field_scientific(
            name = "Cluster score",
            value =  cscore,
            digits = 3,
            )
          bests.append( ( cscore, best[1] ) )

          info.heading( text = "Partition 1:", level = 6 )
          info.preformatted_text( text = str( best[1].source.segment ) )

          info.heading( text = "Partition 2:", level = 6 )
          info.preformatted_text( text = str( best[1].sink.segment ) )

          info.list_item_end()

        info.list_end()
        assert 0 < len( bests )

        if 1 < len( bests ):
          info.heading( text = "Cut combination", level = 5 )
          info.ordered_list_begin( marker = "a" )
          bests.sort( key = lambda s: s[0] )
          selected = bests[0][1]

          for other in bests[1:]:
            info.list_item_begin( text = "Attempting to combine:" )

            try:
              selected = combine_cuts(
                lhs = selected,
                rhs = other[1],
                evaluator = self.domain_evaluator,
                interaction_handler = subset_interaction_handler,
                entity_handler = entity_handler,
                logger = logger,
                )

            except domain_analysis_lib.DomainAnalysisException:
              pass

            info.list_item_end()

          info.list_end()

        else:
          selected = bests[0][1]

        info.heading( text = "Final partitioning:", level = 5 )
        info.ordered_list_begin()

        for spart in selected.partitions:
          info.list_item_begin( text = "partition" )
          info.preformatted_text( text = str( spart.segment ), indent = 2 )
          info.heading( text = "Components:", level = 6 )
          connecteds = connected_component_analysis(
            graph = maximum_clique.selected_subgraph(
              graph = graph,
              vertices = [ vertex_for[ l ] for l in spart.labels ],
              ),
            entity_handler = entity_handler,
            logger = logger,
            )
          next_dcs.extend( connecteds )
          info.list_item_end()

        info.list_end()
        info.list_item_end()

      info.list_end()

      if not next_dcs:
        info.unformatted_text(
          text = "Stopping because there are no more structures to decompose",
          )
        break

      else:
        decomposables = next_dcs

    if next_dcs:
      info.unformatted_text(
        text = "Number of iterations reached preset limit"
        )
      info.unformatted_text(
        text = "The following domains have not yet been confirmed to be single",
        )

      for partition in next_dcs:
        info.preformatted_text( text = str( partition.segment ), indent = 2 )
        singles.append( partition )

      info.unformatted_text(
        text = "Treating these as single domains in further analysis",
        )

    segment_splits = []

    if singles:
      info.heading( text = "Domain multisegment split", level = 3 )
      info.ordered_list_begin()

      for partition in singles:
        info.list_item_begin( text = "single domain" )
        info.preformatted_text( text = str( partition.segment ) )
        splits = multisegment_domain_split(
          partition = partition,
          min_domain_size = self.domain_evaluator.min_size,
          graph_handler = graph_handler,
          entity_handler = entity_handler,
          strongly_connected_threshold = self.strongly_connected_threshold,
          logger = logger,
          )
        segment_splits.extend(
          [ entity_handler.residue_segment_from( chunkset = s ) for s in splits ]
          )
        info.list_item_end()

      info.list_end()

    return segment_splits


class thorough_algorithm(object):
  """
  A wide-search type algorithm for finding domains
  """

  def __init__(
    self,
    macrocycles,
    max_cut_iterations,
    target_cut_count,
    domain_evaluator,
    cluster_radius,
    equality,
    compatibility,
    domain_scorer,
    strongly_connected_threshold,
    ):

    self.macrocycles = macrocycles
    self.max_cut_iteration = max_cut_iterations
    self.target_cut_count = target_cut_count
    self.domain_evaluator = domain_evaluator
    self.cluster_radius = cluster_radius
    self.equality = equality
    self.compatibility = compatibility
    self.domain_scorer = domain_scorer
    self.strongly_connected_threshold = strongly_connected_threshold


  def __call__(self, graph_handler, interaction_handler, entity_handler, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )
    verbose = facade.Package( channel = logger.verbose )
    info.unformatted_text( text = "Using thorough algorithm" )

    normalizer = graph_handler.average_interaction_weight()
    info.field_float( name = "Boltzmann normalizer", value = normalizer, digits = 2 )

    graph = graph_handler.get_interaction_graph( logger = logger )
    vertex_for = dict(
      ( graph.vertex_label( vertex = v ), v ) for v in graph.vertices()
      )

    info.heading( text = "Connected component analysis", level = 4 )

    connecteds = connected_component_analysis(
      graph = graph,
      entity_handler = entity_handler,
      logger = logger,
      )

    info.heading( text = "Initial domain analysis", level = 4 )
    decomposables = []

    for partition in connecteds:
      report = self.domain_evaluator.evaluate(
        partition = partition,
        interaction_handler = interaction_handler,
        )
      verbose.heading( text = "Domain report:", level = 5 )
      verbose.preformatted_text( text = str( report ), indent = 2 )

      if not report:
        info.unformatted_text( text = "Domain criteria FAIL, discarding" )

      else:
        info.unformatted_text( text = "Accepting for decomposition" )
        decomposables.append( partition )

    from boost_adaptbx.graph import maximum_clique
    singles = []
    knowns = []

    for iternum in range( 1, self.macrocycles + 1 ):
      info.heading( text = "Iteration %s" % iternum, level = 3 )
      info.unformatted_text(
        text = "%s structures to decompose" % len( decomposables )
        )

      info.ordered_list_begin()
      next_dcs = []

      for partition in decomposables:
        rset = partition.segment.residue_set()

        info.list_item_begin(
          text = "structure (%s residues)" % rset.residue_count(),
          )
        info.preformatted_text( text = str( partition.segment ), indent = 2 )


        if any( self.equality( rset, o ) for o in knowns ):
          info.unformatted_text(
            text = "Skip structure as an equivalent has already been analysed",
            )
          info.list_item_end()
          continue

        knowns.append( rset )

        if not self.domain_evaluator.partitionable( partition = partition ):
          info.unformatted_text( text = "Structure too small to be divided" )
          info.unformatted_text( text = "Accepting this segment as a SINGLE domain" )
          singles.append( partition )
          info.list_item_end()
          continue

        info.heading( text = "Structure cut enumeration", level = 4 )
        subset_interaction_handler = interaction_handler.subset(
          chunkset = partition.chunkset,
          )
        ( cuts, endweight ) = find_possible_cuts(
          graph = maximum_clique.selected_subgraph(
            graph = graph,
            vertices = [ vertex_for[ l ] for l in partition.labels ],
            ),
          max_iterations = self.max_cut_iteration,
          target_count = self.target_cut_count,
          evaluator = self.domain_evaluator,
          interaction_handler = subset_interaction_handler,
          entity_handler = entity_handler,
          logger = logger,
          )

        if not cuts:
          info.unformatted_text( text = "No cuts found, treating it as a single domain" )
          singles.append( partition )
          info.list_item_end()
          continue

        info.field( name = "Number of cuts found", value = len( cuts ) )
        info.task_begin( text = "Adjusting cuts for connectivity" )
        adjusteds = [
          adjusted_cut(
            cut = c,
            graph = graph,
            vertex_for = vertex_for,
            entity_handler = entity_handler,
            min_domain_size = self.domain_evaluator.min_size,
            )
          for c in cuts
          ]
        info.task_end()

        info.task_begin( text = "Clustering cuts" )
        resset_cuts = [
          ( set( c.source.residue_groups() ), set( c.sink.residue_groups() ), c )
          for c in adjusteds
          ]
        clusters = domain_analysis_lib.cluster_equivalent_domains(
          domains = resset_cuts,
          clustering_distance = cut_absolute_clustering_distance,
          cluster_radius = self.cluster_radius,
          )
        info.task_end()

        info.unformatted_text( text = "%s equivalent groups found" % len( clusters ) )
        scorer = CutDynamicScoreFunction(
          endweight = endweight,
          normalizer = normalizer,
          interaction_handler = subset_interaction_handler,
          )
        info.ordered_list_begin()

        for group in clusters:
          info.list_item_begin( text = " group: %s cuts" % len( group ) )
          info.text( text = "Best representative:" )
          best = min(
            [ ( scorer( cut = p[-1] ), p[-1] ) for p in group ],
            key = lambda s: s[0],
            )
          info.field_scientific(
            name = "Cut score",
            value = best[-1].score( interaction_handler = subset_interaction_handler ),
            digits = 3,
            )
          info.field_scientific(
            name = "Individual score",
            value = best[0],
            digits = 3,
            )
          cscore = best[0] / len( group )
          info.field_scientific(
            name = "Cluster score",
            value =  cscore,
            digits = 3,
            )

          info.heading( text = "Partition 1:", level = 6 )
          info.preformatted_text( text = str( best[1].source.segment ) )
          connecteds = connected_component_analysis(
            graph = maximum_clique.selected_subgraph(
              graph = graph,
              vertices = [ vertex_for[ l ] for l in best[1].source.labels ],
              ),
            entity_handler = entity_handler,
            logger = logger,
            )
          next_dcs.extend( connecteds )

          info.heading( text = "Partition 2:", level = 6 )
          info.preformatted_text( text = str( best[1].sink.segment ) )
          connecteds = connected_component_analysis(
            graph = maximum_clique.selected_subgraph(
              graph = graph,
              vertices = [ vertex_for[ l ] for l in best[1].sink.labels ],
              ),
            entity_handler = entity_handler,
            logger = logger,
            )
          next_dcs.extend( connecteds )

          info.list_item_end()

        info.list_end()
        info.list_item_end()

      info.list_end()

      if not next_dcs:
        info.unformatted_text(
          text = "Stopping because there are no more structures to decompose",
          )
        break

      else:
        decomposables = next_dcs

    info.unformatted_text( text = "%s single domains found" % len( singles ) )

    if singles:
      info.heading( text = "Domain multisegment split", level = 3 )
      info.ordered_list_begin()
      segment_splits = []
      segment_knowns = []

      for partition in singles:
        info.list_item_begin( text = "single domain" )
        info.preformatted_text( text = str( partition.segment ) )
        splits = multisegment_domain_split(
          partition = partition,
          min_domain_size = self.domain_evaluator.min_size,
          graph_handler = graph_handler,
          entity_handler = entity_handler,
          strongly_connected_threshold = self.strongly_connected_threshold,
          logger = logger,
          )

        info.heading( text = "Split domains", level = 6 )
        info.ordered_list_begin( marker = "a" )

        for s in splits:
          rseg = entity_handler.residue_segment_from( chunkset = s )
          rset = rseg.residue_set()
          info.list_item_begin( text = "split" )
          info.preformatted_text( text = str( rseg ) )

          if any( self.equality( rset, o ) for o in segment_knowns ):
            info.unformatted_text(
              text = "Skip structure as an equivalent has already been analysed",
              )

          else:
            segment_splits.append( rseg )
            segment_knowns.append( rset )

          info.list_item_end()

        info.list_end()
        info.list_item_end()

      info.list_end()

      info.heading( text = "Domain recombination", level = 3 )
      info.heading( text = "Domains selected for re-assembly:", level = 5 )
      info.unordered_list_begin()

      for ( index, rseg ) in enumerate( segment_splits, start = 1 ):
        name = "domain %s" % index
        info.list_item_begin( text = name )
        rseg.data.set( key = "name", value = name )
        info.preformatted_text( text = str( rseg ) )
        info.list_item_end()

      info.list_end()

      cgraph = domain_analysis_lib.build_domain_compatibility_graph(
        domains = [ ( d.residue_set(), d ) for d in segment_splits ],
        compatibility = lambda lhs, rhs: self.compatibility( lhs[0], rhs[0] )
        )

      import operator
      reassembly = domain_analysis_lib.DomainRecombineVisitor(
        graph = cgraph,
        scorer = domain_analysis_lib.DomainCountAndCoverageScorer(),
        logger = logger,
        calculate_domain_names = lambda r: [ d[0].data.get( "name" ) for d in r ],
        calculate_residue_group_count = lambda r: len(
          reduce( operator.or_, ( d[0].rgs for d in r ), set() )
          ),
        calculate_domain_count = lambda r: len( r ),
        )
      info.field( name = "Best assembly", value = reassembly.best_index )
      info.field( name = "Best score", value = reassembly.formatted_best_score )
      decomposition = [ d[1] for d in reassembly.best_solution ]

    else:
      decomposition = []

    return decomposition


def process(
  chain,
  chopper,
  interaction_calculator,
  algorithm,
  logger,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.heading( text = "Structure segmentization", level = 3 )
  info.task_begin( text = "Chopping up chain on secondary structure" )
  structure = chopper( chain = chain )
  info.task_end()
  info.heading( text = "Structure segments:", level = 4 )
  info.ordered_list_begin( separate_items = False )

  for seg in structure.chunks:
    info.list_item_begin( text = str( seg ) )
    info.list_item_end()

  info.list_end()

  info.heading( text = "Interaction calculation", level = 3 )
  intcalc = interaction_calculator( structure = structure, logger = logger )
  graph_handler = intcalc.get_graph_handler()
  info.field_float(
    name = "Average interaction weight",
    value = graph_handler.average_interaction_weight(),
    digits = 2,
    )

  info.heading( text = "Total interaction for segment:", level = 4 )
  info.paragraph_begin( indent = 2 )
  stats_for = graph_handler.total_interaction_weight_for()

  for ( chunk, weight ) in sorted( stats_for.items(), key = lambda p: p[1] ):
    info.field_float(
      name = str( chunk.segment ),
      value = weight,
      digits = 2,
      )

  info.paragraph_end()
  interaction_handler = intcalc.get_interaction_handler()

  info.heading( text = "Domain finding", level = 3 )

  singles = algorithm(
    graph_handler = graph_handler,
    interaction_handler = interaction_handler,
    entity_handler = structure.entity_handler(),
    logger = logger,
    )

  info.heading( text = "Domain decomposition found", level = 3 )
  info.ordered_list_begin()

  for dom in singles:
    info.list_item_begin( text = "domain" )
    info.preformatted_text( text = str( dom ) )
    info.list_item_end()

  info.list_end()

  return singles


def create_quick_algorithm(search, evaluator):

  return quick_algorithm(
    macrocycles = search.max_macrocycles,
    max_cut_iterations = search.max_iterations,
    target_cut_count = search.sufficient_count,
    domain_evaluator = evaluator,
    cluster_radius = search.cluster_radius,
    strongly_connected_threshold = search.strong_connection,
    )


def create_thorough_algorithm(search, evaluator):

  return thorough_algorithm(
    macrocycles = search.max_macrocycles,
    max_cut_iterations = search.max_iterations,
    target_cut_count = search.sufficient_count,
    domain_evaluator = evaluator,
    cluster_radius = search.cluster_radius,
    equality = domain_analysis_lib.absolute_equality(
      max_difference = search.max_residue_difference_for_equality,
      ),
    compatibility = domain_analysis_lib.absolute_compatibility(
      max_overlap = search.max_residue_overlap_for_compatibility,
      ),
    domain_scorer = lambda p: p.compactness_score(),
    strongly_connected_threshold = search.strong_connection,
    )


ALGORITHM_FACTORY_FOR = {
  "quick": create_quick_algorithm,
  "thorough": create_thorough_algorithm,
  }


master_phil = """
  input
    .help = "Input files"
  {
    structure = None
      .help = "Structure file"
      .type = path
  }

  output
    .help = "Output files"
  {
    root = "domain"
      .help = "Root for domain files"
      .type = str

    write_pdb_file = False
      .help = "Write PDB file for each domain"
      .type = bool
      .optional = False
  }

  configuration
    .help = "Calculation parameters"
  {
    interaction_strength
      .help = "Parameters to calculate interaction strength between two residues"
    {
      long_range_contact
        .help = "Parameters for defining a long-range contact"
      {
        max_spatial_distance = 4.0
          .help = "Maximum distance for atom-atom contact"
          .type = float( value_min = 0.0 )
          .optional = False
      }

      short_range_contact
        .help = "Parameters controlling the strength of short range contacts"
      {
        default = 10.0
          .help = "Strength of a coil-to-coil and mixed type connections"
          .type = float( value_min = 0.0 )
          .optional = False

        helix_to_helix_connection = 50.0
          .help = "Strength of a helix-to-helix connection"
          .type = float( value_min = 0.0 )
          .optional = False

        strand_to_strand_connection = 50.0
          .help = "Strength of a strand-to-strand connection"
          .type = float( value_min = 0.0 )
          .optional = False
      }

      assembly_membership
        .help = "Parameters rewarding membership in a higher order assembly"
      {
        sheet_bonus = 20.0
          .help = "Extra strength if two strands are within the same sheet"
          .type = float( value_min = 0.0 )
          .optional = False
      }

      significance_threshold = 0.01
        .help = "Discard interaction if below this level"
        .type = float( value_min = 0 )
        .optional = False

      prune_fraction = 0.00
        .help = "Discard lowest connected segments"
        .type = float( value_min = 0 )
        .optional = False
    }

    chunking
      .help = "Parameters controlling chunking of segments"
    {
      coil_max_size = 15
        .help = "Minimum chunksize for a coil"
        .type = int( value_min = 0 )
        .optional = False

      helix_max_size = None
        .help = "Minimum chunksize for a helix"
        .type = int( value_min = 0 )
        .optional = False

      strand_max_size = None
        .help = "Minimum chunksize for a strand"
        .type = int( value_min = 0 )
        .optional = False
    }

    evaluation
      .help = "Parameters controlling domain acceptance"
    {
      min_size = 30
        .help = "Minimum domain size is residues"
        .type = int( value_min = 1 )
        .optional = False

      min_compactness = 0.6
        .help = "Minimum compactness (internal interaction strength to atoms ratio)"
        .type = float( value_min = 0 )
        .optional = False

      max_interface_strength = 0.4
        .help = "Maximum inteface strength to internal interaction strength ratio"
        .type = float( value_min = 0 )
        .optional = False
    }

    search
      .help = "Parameters controlling domain search"
    {
      algorithm = %(algorithm)s
        .help = "Algorithm to use"
        .type = choice
        .optional = False

      max_macrocycles = 5
        .help = "Maximum number of macrocycles"
        .type = int( value_min = 0 )
        .optional = False

      max_iterations = 500
        .help = "Maximum cut iterations to perform"
        .type = int( value_min = 0 )
        .optional = False

      sufficient_count = 31
        .help = "Quit search if sufficient number of domains has been located"
        .type = int( value_min = 0 )
        .optional = False

      cluster_radius = 20
        .help = "Radius for identifying quasi-equivalent domains"
        .type = int( value_min = 0 )
        .optional = False

      max_residue_difference_for_equality = 10
        .help = "Maximum residue differences between two domains"
        .type = int( value_min = 0 )
        .optional = False

      max_residue_overlap_for_compatibility = 5
        .help = "Max number of common residues between two domains"
        .type = int( value_min = 0 )
        .optional = False

      strong_connection = 100
        .help = "Do not separate multisegment domains that are at least as strongly connected"
        .type = float( value_min = 0 )
        .optional = False
    }
  }
  """ % {
    "algorithm": tbx_utils.choice_string(
      possible = ALGORITHM_FACTORY_FOR,
      default = "quick",
      )
    }

parsed_master_phil = lazy_initialization(
  func = tbx_utils.parse_phil,
  phil = master_phil,
  process_includes = True,
  )

PROGRAM = "structural_domain_search"
VERSION = "0.0.1"

def run(phil, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = "%s version %s" % ( PROGRAM, VERSION ) )
  info.heading( text = "Configuration options", level = 2 )
  info.preformatted_text( text = phil.as_str() )
  info.separator()

  params = phil.extract()

  assert params.input.structure # checked in validate_params
  config = params.configuration
  iconfig = config.interaction_strength

  chopper = StructureChopper.from_params( params = config.chunking )
  interaction_calculator = ConfigurableInteractionCalculator(
    steps = [
      InteractionCalculatorWrapper(
        resultname = "long range contacts",
        calculator = LongRangeInteractionCalculator.from_params(
          params = iconfig.long_range_contact,
          ),
        ),
      InteractionCalculatorWrapper(
        resultname = "assembly bonus",
        calculator = AssemblyInteractionCalculator.from_params(
          params = iconfig.assembly_membership,
          ),
        ),
      InteractionSummer(
        resultname = "total nonlocal interactions",
        individuals = [ "long range contacts", "assembly bonus" ],
        ),
      InteractionDeleter( identifier = "long range contacts" ),
      InteractionDeleter( identifier = "assembly bonus" ),
      InteractionCalculatorWrapper(
        resultname = "sequential covalent contacts",
        calculator = MainchainInteractionCalculator.from_params(
          params = iconfig.short_range_contact,
          ),
        ),
      InteractionCalculatorWrapper(
        resultname = "sequential contextual contacts",
        calculator = ContextualInteractionCalculator.from_params(),
        principal_interaction_between = "total nonlocal interactions",
        ),
      InteractionSummer(
        resultname = "total interactions",
        individuals = [
          "total nonlocal interactions",
          "sequential covalent contacts",
          "sequential contextual contacts",
          ],
        ),
      InteractionDeleter( identifier = "sequential covalent contacts" ),
      InteractionDeleter( identifier = "sequential contextual contacts" ),
      ],
    interaction_handler_key = "total nonlocal interactions",
    graph_handler_key = "total interactions",
    graph_interaction_threshold = iconfig.significance_threshold,
    graph_pruning_fraction = iconfig.prune_fraction,
    )
  algorithm = ALGORITHM_FACTORY_FOR[ config.search.algorithm ](
    search = config.search,
    evaluator = DomainEvaluator.from_params( params = config.evaluation ),
    )

  info.heading( text = "Domain search", level = 1 )
  from phaser import mmtype

  pdb = tbx_utils.PDBObject.from_file( file_name = params.input.structure )
  translator = domain_analysis_lib.Domain2Phil.StructureDomain()
  results = []

  for ( index, chain ) in enumerate( pdb.object.models()[0].chains(), start = 1 ):
    identifier = "Chain-%s (chainid '%s', residues '%s' -> '%s')" % (
      index,
      chain.id,
      chain.residue_groups()[0].resid(),
      chain.residue_groups()[-1].resid(),
      )
    info.heading( text = identifier, level = 2 )

    if not mmtype.PROTEIN.recognize_chain_type( chain = chain ):
      info.unformatted_text( text = "Not recognized as PROTEIN, skipping" )
      continue

    singles = process(
      chain = domain_analysis_lib.Chain( chain = chain, mmt = mmtype.PROTEIN ),
      chopper = chopper,
      interaction_calculator = interaction_calculator,
      algorithm = algorithm,
      logger = logger,
      )

    domain_file_name = "%s-%s.eff" % ( params.output.root, index )
    info.field( name = "Domains file", value = domain_file_name )

    with open( domain_file_name, "w" ) as ofile:
      phil = translator(
        definitions = [
          ( "domain %s" % i, d.segments ) for ( i, d ) in enumerate( singles, start = 1 )
          ]
        )
      phil.show( ofile )

    pdb_file_names = []

    if params.output.write_pdb_file:
      import iotbx.pdb

      for ( i, dom ) in enumerate( singles, start = 1 ):
        filename = "%s-%s-d%s.pdb" % ( params.output.root, index, i )
        info.field( name = "PDB file for domain %s" % i, value = filename )

        chain = iotbx.pdb.hierarchy.chain()

        for segment in dom.segments:
          for elem in segment:
            chain.append_residue_group( elem().detached_copy() )

        model = iotbx.pdb.hierarchy.model()
        model.append_chain( chain )
        root = iotbx.pdb.hierarchy.root()
        root.append_model( model )
        root.write_pdb_file( filename )
        pdb_file_names.append( filename )

    results.append( ( chain, identifier, singles, domain_file_name, pdb_file_names ) )

  info.heading( text = "Summary", level = 1 )
  info.ordered_list_begin()

  for ( chain, identifier, domains, domfile, pdbfiles ) in results:
    info.list_item_begin( text = identifier )
    info.field( name = "Domains file", value = domfile )

    if pdbfiles:
      info.field_sequence( name = "PDB files", value = pdbfiles )

    info.ordered_list_begin()

    for dom in domains:
      info.list_item_begin( text = "domain" )
      info.preformatted_text( text = str( dom ) )
      info.list_item_end()

    info.list_end()
    info.list_item_end()

  info.list_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return results


def validate_params(params):

  from libtbx.utils import Sorry

  if params.input.structure is None:
    raise Sorry("No structure specified")


if __name__ == "__main__":
  from phaser import cli_new as cli

  parser = cli.PCLIParser(
    master_phil = parsed_master_phil(),
    enable_text_logfile = True,
    enable_html_logfile = True,
    description = "Decompose structure to domains",
    )
  parser.add_argument(
    "-v", "--verbosity",
    action = "store",
    choices = LOGGING_LEVELS,
    default = "info",
    help = "verbosity level"
    )
  parser.add_argument(
    "--version",
    action = "version",
    version = "%s %s" % ( PROGRAM, VERSION )
    )
  parser.recognize_file_type_as(
    extensions = [ ".pdb", ".ent" ],
    philpath = "input.structure",
    )

  params = parser.parse_args()

  # Check parameters
  validate_params( params = params.phil.extract() )

  # Run the program
  import sys
  from phaser import logoutput_new as logoutput
  from phaser.logoutput_new import console
  from libtbx.utils import Sorry

  handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]

  if params.text_logfile is not None:
    handlers.append(
      console.Handler.FromDefaults(
        stream = params.text_logfile,
        width = 80,
        close_stream = True,
        )
      )

  if params.html_logfile is not None:
    from phaser.logoutput_new import xhtml
    handlers.append(
      xhtml.Handler.FromDefaults(
        stream = params.html_logfile,
        title = PROGRAM,
        close_stream = True,
        )
      )

  with logoutput.Writer( \
    handlers = handlers, \
    level = LOGGING_LEVELS[ params.verbosity ], \
    close_handlers = True,
    **LOGGING_LEVELS \
    ) \
    as root:

    try:
      with logoutput.Forwarder( loggers = [ root ], shift = 0, **LOGGING_LEVELS ) \
        as logger:

        run( phil = params.phil, logger = logger )

    except Sorry as e:
      raise

    except Exception:
      from phaser.logoutput_new import facade
      warn = facade.Package( channel = root.warn )
      warn.highlight( text = "An error occurred" )

      ( exc_type, exc_value, tb ) = sys.exc_info()
      warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
      raise
