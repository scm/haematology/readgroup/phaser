from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "test",
    metavar = "JSON_FILE",
    help = "JSON file containing target description",
    )
  parser.add_argument(
    "errfile",
    metavar = "ERR_FILE",
    help = ".err file containing error estimates"
    )
  parser.add_argument(
    "replacements",
    type = str,
    help = "Domains to replace",
    )
  parser.add_argument(
    "phils",
    nargs = "*",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL arguments for sculptor (file name or PHIL command line assignment)",
    )
  parser.add_argument(
    "--disable-bfactor-refinement",
    action = "store_false",
    dest = "bfactor",
    help = "Disable Bfactor refinement"
    )
  parser.add_argument(
    "--disable-vrms-refinement",
    action = "store_false",
    dest = "vrms",
    help = "Disable VRMS refinement",
    )
  parser.add_argument(
    "--min-hss-length",
    type = int,
    default = 10,
    help = "Minimum sequence overlap length with replacing chain",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_score_error_estimate"


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  from libtbx.utils import Sorry
  import json
  info.task_begin( text = "Reading test description" )

  try:
    with open( params.test ) as ifile:
      description_for = json.load( ifile )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  import os.path
  path_to_test = os.path.dirname( params.test )
  info.field( name = "Path for test files", value = path_to_test )

  info.heading( text = "Model sculpting", level = 1 )

  from phaser.command_line import benchmark_sculpt_model
  params.phils.append(
    os.path.join( path_to_test, description_for[ "alignment_file" ] )
    )

  info.paragraph_begin( indent = 4 )
  sculptargs = [
    "--prefix",
    "sculpt-%s" % os.path.splitext( os.path.basename( params.errfile ) )[0],
    "--error-file",
      params.errfile,
    ]

  sculpt_file_name = benchmark_sculpt_model.run(
    params = parse(
      parser = benchmark_sculpt_model.get_parser(),
      args = [ os.path.join( path_to_test, description_for[ "model_file" ] ) ]
        + params.phils
        + sculptargs,
      ),
    logger = logger,
    )
  info.paragraph_end()

  import pickle
  with open( sculpt_file_name ) as ifile:
    sculpted_chain = pickle.load( ifile )

  info.heading( text = "Chain replacement", level = 1 )
  from phaser.command_line import benchmark_molecule_replacement

  if not params.replacements:
    replacements = []

  else:
    replacements = params.replacements.split( "+" )

  if any( eid not in description_for[ "superposition_with" ] for eid in replacements ):
    info.field_sequence( name = "Replacements requested", value = replacements )
    info.field_sequence( name = "Known domains", value = description_for[ "superposition_with" ] )
    raise Sorry("Unknown ensemble for replacement")

  info.paragraph_begin( indent = 4 )
  ( replaced_structure_file, replacement_ensemble_for ) = benchmark_molecule_replacement.run(
    params = parse(
      parser = benchmark_molecule_replacement.get_parser(),
      args = [
        os.path.join( path_to_test, description_for[ "target_structure_file" ] ),
        "--prefix", params.prefix if params.prefix else "replaced",
        "--min-hss-length", params.min_hss_length,
        ]
        + sum(
          (
            [
              "--replacement",
              eid,
              sculpt_file_name,
              os.path.join( path_to_test, description_for[ "superposition_with" ][ eid ] ),
              description_for.get( "rmsd_with", {} ).get( eid ),
              ]
            for eid in replacements
            ),
          []
          ),
      ),
    logger = logger,
    )

  assert len( replacements ) == len( replacement_ensemble_for )
  info.paragraph_end()

  info.heading( text = "Phaser refinement", level = 1 )
  from phaser.command_line import benchmark_calculate_mr_statistics

  args = [
    os.path.join( path_to_test, description_for[ "case_file" ] ),
    replaced_structure_file,
    ]

  if not params.bfactor:
    args.append( "--disable-bfactor-refinement" )

  if not params.vrms:
    args.append( "--disable-vrms-refinement" )

  info.paragraph_begin( indent = 4 )
  results = benchmark_calculate_mr_statistics.run(
    params = parse(
      parser = benchmark_calculate_mr_statistics.get_parser(),
      args = args
      ),
    logger = logger,
    )
  info.paragraph_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  head = [
    os.path.basename( params.errfile ),
    sculpted_chain.identity,
    ]
  return head + results


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
