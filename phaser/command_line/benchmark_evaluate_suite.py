from phaser import benchmark


def get_parser():

  import argparse
  import sys

  parser = argparse.ArgumentParser()
  parser.add_argument(
    "suite",
    metavar = "SUITE_FILE",
    help = "Suite file containing tests to perform",
    )
  parser.add_argument(
    "phils",
    nargs = "*",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL arguments for sculptor (file name or PHIL command line assignment)",
    )
  parser.add_argument(
    "--min-domain-residue-length-for-replacement",
    type = int,
    default = 50,
    help = "Minimum length of a replacing chain in residues"
    )
  parser.add_argument(
    "--acceptable-domain-fractional-length-for-replacement",
    type = float,
    default = 0.4,
    help = "Acceptable fractional length of domain to be replaced if smaller than absolute limit"
    )
  parser.add_argument(
    "--engine",
    default = None,
    help = "PHIL file containing multiprocessing description"
    )
  parser.add_argument(
    "--outfile",
    type = argparse.FileType( "w" ),
    default = sys.stdout,
    help = "Output csv file"
    )
  parser.add_argument(
    "--min-hss-length",
    type = int,
    default = 10,
    help = "Minimum sequence overlap length with replacing chain"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_evaluate_suite"


def execute_test(
  test,
  phils,
  min_domain_residue_length,
  acceptable_domain_fractional_length,
  min_hss_length,
  prefix,
  ):

  args = [ test ] + phils + [
    "--min-domain-residue-length-for-replacement", min_domain_residue_length,
    "--acceptable-domain-fractional-length-for-replacement", acceptable_domain_fractional_length,
    "--min-hss-length", min_hss_length,
    ]

  if prefix is not None:
    args.extend( [ "--prefix", prefix ] )

  import os.path
  logname = "%s%s.log" % (
    "" if prefix is None else "%s-" % prefix,
    os.path.splitext( os.path.basename( test ) )[0]
    )

  from phaser.command_line import benchmark_evaluate_test

  with open( logname, "w" ) as ofile, benchmark.get_logger( stream = ofile ) as logger:
    result = benchmark_evaluate_test.run(
      params = parse(
        parser = benchmark_evaluate_test.get_parser(),
        args = args,
        ),
      logger = logger,
      )

  return result


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params):

  creator = benchmark.get_engine_creator( filename = params.engine )

  import csv
  writer = csv.writer( params.outfile )
  writer.writerow( [ "Test", "Replacements", "Statistics" ] )

  from libtbx.scheduling import holder
  from libtbx.scheduling import parallel_for
  from phaser.command_line import benchmark_evaluate_suite

  import os.path
  dirname = os.path.dirname( params.suite )

  with open( params.suite ) as ifile, holder( creator = creator ) as manager:
    reader = csv.reader( ifile )

    pfi = parallel_for.iterator(
      calculations = (
        (
          benchmark_evaluate_suite.execute_test,
          (),
          {
            "test": os.path.normpath( os.path.join( dirname, line[0] ) ),
            "phils": params.phils,
            "min_domain_residue_length": params.min_domain_residue_length_for_replacement,
            "acceptable_domain_fractional_length": params.acceptable_domain_fractional_length_for_replacement,
            "min_hss_length": params.min_hss_length,
            "prefix": ( ( "" if params.prefix is None else params.prefix )
              + os.path.splitext( os.path.basename( line[0] ) )[0] ),
            }
            )
          for line in reader if line
        ),
      manager = manager,
      keep_input_order = True,
      )

    for ( calc, res ) in pfi:
      row = [ calc[2][ "test" ] ]

      try:
        results = res()
        row.extend( results[3:] )

      except Exception as e:
        row.append( str( e ) )

      writer.writerow( row )

    manager.join()


if __name__ == "__main__":
  parser = get_parser()
  run( params = parser.parse_args() )
