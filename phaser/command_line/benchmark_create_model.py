from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "pdbid",
    metavar = "PDBID",
    help = "Model structure PDB code",
    )
  parser.add_argument(
    "chainid",
    metavar = "CHAINID",
    help = "Chain ID of model",
    )
  parser.add_argument(
    "--pdb-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded FASTA files",
    )
  parser.add_argument(
    "--seq-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded FASTA files",
    )
  parser.add_argument(
    "--pdb-mirror",
    action = "store",
    choices = [ "rcsb", "pdbe", "pdbj" ],
    default = "pdbe",
    help = "Mirror to download PDB files"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


def get_model(pdbid, chainid, mmt, composition, accessor, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.heading( text = "Structure analysis", level = 1 )
  info.task_begin( text = "Getting structure" )
  data = accessor( pdbid )
  info.task_end()

  info.task_begin( text = "Parsing" )
  import iotbx.pdb
  pdbinp = iotbx.pdb.input( lines = data, source_info = "pdb" )
  root = pdbinp.construct_hierarchy()
  info.task_end()

  from libtbx.utils import Sorry

  if len( root.models() ) != 1:
    info.highlight( text = "Multimodel structure, only first will be used" )

    for model in root.models()[1:]:
      root.remove_model( model )

  assert len( root.models() ) == 1

  from phaser import tbx_utils
  info.task_begin( text = "Splitting chains" )
  nchains = tbx_utils.split_all_chains_in( root = root )
  info.task_end()
  info.field( name = "New chains introduced", value = nchains )
  info.field( name = "Number of chains", value = len( root.models()[0].chains() ) )

  info.heading( text = "Chain search", level = 2 )
  info.field( name = "ChainID", value = chainid )
  info.field( name = "Type", value = mmt.name )
  info.unordered_list_begin()
  mychain = None

  for chain in root.models()[0].chains():
    annotation = tbx_utils.ChainAnnotation.from_chain( chain = chain )
    info.list_item_begin( text = str( annotation ) )

    if chain.id != chainid:
      info.unformatted_text( text = "Wrong chainID, skip" )
      info.list_item_end()
      continue

    if not mmt.recognize_chain_type( chain = chain ):
      info.unformatted_text( text = "Wrong chain type, skip" )
      info.list_item_end()
      continue

    info.unformatted_text( text = "Chain found" )
    mychain = chain
    info.list_item_end()
    break

  else:
    info.list_end()
    raise benchmark.BenchmarkError("No %s chain %s found in PDB %s" % (
      mmt.name,
      chainid,
      pdbid,
    ))

  info.list_end()

  try:
    ( component, match ) = composition.matching_component_to(
      chain = mychain,
      mmt = mmt,
      )

  except benchmark.BenchmarkError:
    raise Sorry("Could not match chain %s to component" % chain.id)

  info.field( name = "Sequence component", value = str( component ) )
  info.field( name = "Unaligneds", value = len( match.unaligneds ) )
  info.field( name = "Identities", value = match.identities )
  info.field(
    name = "Differences",
    value = len( chain.residue_groups() ) - match.identities,
    )

  chainobj = benchmark.Chain.from_chain(
    chain = chain,
    mmt_name = mmt.name,
    reference_sequence = component.sequence_string(),
    aligned_residues = match.residue_alignment(),
    annotation = annotation,
    origin = pdbid,
    )

  model = benchmark.Model(
    name = "%s_%s" % ( pdbid, chainid ),
    chain = chainobj,
    selection = chainobj.whole(),
    )

  return model

PROGRAM = "benchmark_create_model"


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  composition = benchmark.get_composition(
    pdbid = params.pdbid,
    accessor = benchmark.get_pdb_sequence_accessor(
      mirror  = params.pdb_mirror,
      cache_dir = params.seq_cache_dir,
      logger = logger,
      ),
    logger = logger,
    )

  from libtbx.utils import Sorry
  from phaser import mmtype

  try:
    model = get_model(
      pdbid = params.pdbid,
      chainid = params.chainid,
      mmt = mmtype.PROTEIN,
      composition = composition,
      accessor = benchmark.get_pdb_structure_accessor(
        mirror = params.pdb_mirror,
        cache_dir = params.pdb_cache_dir,
        logger = logger,
        ),
      logger = logger,
      )

  except benchmark.BenchmarkError as e:
    raise Sorry(str(e))

  import pickle

  if params.prefix is None:
    params.prefix = model.identifier()

  file_name = "%s.model" % params.prefix
  info.task_begin( text = "Saving results to %s" % file_name )

  with open( file_name, "w" ) as ofile:
    pickle.dump( model, ofile )

  info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return file_name


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
