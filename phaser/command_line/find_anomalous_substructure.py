from __future__ import print_function

from __future__ import division
from libtbx.utils import Sorry, Usage
import libtbx.phil
import time
import sys

def run (args, out=None) :
  if (out is None) :
    out = sys.stdout
  if (len(args) == 0) or ("--help" in args) :
    if ("--help" in args) : args.remove("--help")
    raise Usage("phaser.find_anomalous_substructure model.pdb data.mtz wavelength=0.9792|energy=12141")
  import phaser.substructure
  import mmtbx.command_line
  master_phil = mmtbx.command_line.generate_master_phil_with_inputs(
    phil_string="""
  output_file = phaser_AX.pdb
    .type = path
  """)
  cmdline = mmtbx.command_line.load_model_and_data(
    args=args,
    master_phil=master_phil,
    out=out,
    process_pdb_file=False,
    create_fmodel=True,
    prefer_anomalous=True)
  params = cmdline.params
  if (not cmdline.fmodel.f_obs().anomalous_flag()) :
    raise Sorry("Anomalous data are required to run this program.")
  t1 = time.time()
  result = phaser.substructure.find_anomalous_scatterers(
    fmodel=cmdline.fmodel,
    pdb_hierarchy=cmdline.pdb_hierarchy,
    wavelength=params.input.wavelength,
    verbose=True,
    log=out)
  t2 = time.time()
  print("elapsed time: %.1fs" % (t2 - t1), file=out)
  f = open(cmdline.params.output_file, "w")
  f.write(result.as_pdb_string())
  f.close()
  print("Wrote %s" % cmdline.params.output_file, file=out)

if (__name__ == "__main__") :
  run(sys.argv[1:])
