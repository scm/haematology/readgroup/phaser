from __future__ import print_function

import sys

if len( sys.argv ) != 3:
  print("Usage: abs_extract_shelx_refinement_files cif-file prefix")
  sys.exit( 1 )

with open( sys.argv[1] ) as fin:
  data = fin.read()

import re
#regex = re.compile( r"_shelx_res_file\s+;([^;]*);.*_shelx_hkl_file\s+\;([^;]*)\;", re.DOTALL )
regex = re.compile(
  r"_shelx_res_file\s+;\s+([^;]*)\s+;.+_shelx_hkl_file\s+\;\n([^;]*)\s+;",
  re.DOTALL,
  )
match = regex.search( data )

if not match:
  raise RuntimeError("No shelx refinement found in cif file")

with open( "%s.res" % sys.argv[2], "w" ) as fout:
  fout.write( match.group( 1 ) )
  fout.write( "\n" )

with open( "%s.hkl" % sys.argv[2], "w" ) as fout:
  fout.write( match.group( 2 ) )
  fout.write( "\n" )
