from __future__ import print_function

import sys

if len( sys.argv ) != 4:
  print("Usage: abs_convert_hkl_to_mtz res-file hkl-file hklout")
  sys.exit(1)

( res, hkl, hklout ) = sys.argv[1:]

from iotbx import shelx
structdata = shelx.cctbx_xray_structure_from( None, filename = res )

from iotbx import reflection_file_reader
refldata = reflection_file_reader.any_reflection_file( "%s=hklf4" % hkl )
arrays = refldata.as_miller_arrays( crystal_symmetry = structdata.crystal_symmetry() )

assert len( arrays ) == 1
myarray = arrays[0]

assert myarray.anomalous_flag, "Anomalous flag is False"
assert myarray.observation_type() != "xray.intensity", "Data read is not intensities"

print("Data read:")
myarray.show_summary()
print

print("Exporting in MTZ format: %s" % hklout)

mtzobj = myarray.as_mtz_dataset( "I" ).mtz_object()
mtzobj.write( hklout )
