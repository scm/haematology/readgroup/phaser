from __future__ import print_function

from phaser import benchmark

def get_parser():
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "target",
    metavar = "FILENAME",
    help = "Target structure file",
    )
  parser.add_argument(
    "ensembleid",
    metavar = "ENSEMBLEID",
    help = "Ensemble identifier",
    )
  parser.add_argument(
    "model",
    metavar = "FILENAME",
    help = "Model chain",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = "transformation",
    help = "File name prefix for output data",
    )
  parser.add_argument(
    "--write-transformed-pdb",
    action = "store_true",
    help = "Write out superposed model"
    )

  return parser


def create_ensembler_assembly(model):

  from phaser import ensembler_new as ensembler
  from phaser import tbx_utils
  chain = model.chain.iotbx_chain()
  enschain = ensembler.Chain(
    chain = chain,
    annotation = tbx_utils.ChainAnnotation.from_chain( chain = chain ),
    mmt = model.chain.mmt(),
    )
  return ensembler.Assembly(
    annotation = model.identifier(),
    chains = [ enschain ],
    associateds = [],
    )


class domain_restricted_full_ssm_mapping(object):

  def __init__(self, models):

    self.members = set( [ None ] )

    for m in models:
      self.members.update( m.aligned_residue_groups() )


  def __call__(self, chains, logger):

    from phaser.logoutput_new import facade
    info = facade.Package( channel = logger.info )

    from phaser import ensembler_new as ensembler
    mapping = ensembler.ssm_residue_mapping_method( chains = chains, logger = logger )
    info.field( name = "Mapping length", value = len( mapping ) )
    restricteds = []

    for p in mapping:
      if self.members.issuperset( p ):
        restricteds.append( p )

    info.field(
      name = "Mapping length after restricting to domain positions",
      value = len( restricteds ),
      )


    if len( restricteds ) < ensembler.MIN_SITE_COUNT:
      raise benchmark.BenchmarkError("No overlap between domains")

    return restricteds


PROGRAM = "benchmark_best_superposition"


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )
  info.task_begin( text = "Reading input files" )
  import pickle
  from libtbx.utils import Sorry

  try:
    with open( params.target ) as ifile:
      structure = pickle.load( ifile )

    with open( params.model ) as ifile:
      model = pickle.load( ifile )

  except IOError as e:
    info.task_failure( text = "failed" )
    raise Sorry(e)

  info.task_end()

  try:
    ensemble = structure.get_ensemble( identifier = params.ensembleid )

  except KeyError:
    raise Sorry("Ensemble '%s' unknown (knowns: %s)" % (
      params.ensembleid,
      " ".join(structure.get_ensemble_ids()),
    ))

  from phaser import ensembler_new as ensembler
  from phaser import mmtype

  if ensemble.model.chain.mmt() != mmtype.PROTEIN:
    raise Sorry("Ensemble '%s' is not protein" % params.ensembleid)

  if model.chain.mmt() != mmtype.PROTEIN:
    raise Sorry("Model is not protein")

  info.heading( text = "Superposition setup", level = 1 )

  reference = create_ensembler_assembly( model = ensemble.model )
  moving = create_ensembler_assembly( model = model )

  defaults_for = dict(
    ( d.mmtype_name(), d ) for d in ensembler.SUPERPOSABLE_MMTYPE_DEFAULTS
    )
  eparams = ensembler.parsed_master_phil().extract()
  resmapping = ensembler.MappingMethod(
    method = domain_restricted_full_ssm_mapping(
      models = [ ensemble.model, model ],
      ),
    types = [ mmtype.PROTEIN ],
    )

  try:
    results = ensembler.process(
      assemblies = [ reference, moving ],
      ngroups = 1,
      mappings = [ resmapping ],
      selections = [ defaults_for[ mmtype.PROTEIN.name ].atom_selection() ],
      superposition = ensembler.SUPERPOSITION_CREATOR_FOR[ eparams.configuration.superposition.method ](
        weighting = ensembler.WEIGHTING_SCHEME_SETUP_FOR[ eparams.configuration.weighting.scheme ](
          params = eparams.configuration.weighting,
          ),
        params = eparams.configuration,
        ),
      clustering_distances = eparams.configuration.clustering,
      logger = logger.child( shift = 0 ),
      )

  except ensembler.EnsemblerError as e:
    info.highlight( text = "Error: %s" % e )
    raise benchmark.BenchmarkError(e)

  assert len( results ) == 2
  refmat = results[0].transformation
  movmat = results[1].transformation

  matrix = refmat.inverse() * movmat

  info.heading( text = "Final transformation", level = 2 )
  info.rt_matrix( rt = matrix )

  prefix = "%s-%s-on-%s" % (
    params.prefix,
    model.identifier(),
    ensemble.model.identifier(),
    )
  superposition = benchmark.Superposition( matrix = matrix, rmsd = results[0].wrmsd )
  superposition_file_name = "%s.superposition" % prefix

  info.task_begin( text = "Writing transformation to %s" % superposition_file_name )

  with open( superposition_file_name, "w" ) as ofile:
    pickle.dump( superposition, ofile )

  info.task_end()

  if params.write_transformed_pdb:
    file_name = "%s.pdb" % prefix
    info.task_begin( text = "Writing transformed PDB to %s" % file_name )
    root = model.iotbx_root()
    atoms = root.atoms()
    atoms.set_xyz( matrix * atoms.extract_xyz() )
    root.write_pdb_file( file_name )
    info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return superposition_file_name


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
