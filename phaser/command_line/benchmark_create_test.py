from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "targetid",
    metavar = "PDB_CHAIN",
    help = "Target structure PDB identifier",
    )
  parser.add_argument(
    "modelid",
    metavar = "PDB_CHAIN",
    help = "Model structure PDB identifier",
    )
  parser.add_argument(
    "--cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded files",
    )
  parser.add_argument(
    "--pdb-mirror",
    action = "store",
    choices = [ "rcsb", "pdbe", "pdbj" ],
    default = "pdbe",
    help = "Mirror to download PDB files"
    )
  parser.add_argument(
    "--min-overlap",
    type = int,
    default = 10,
    help = "Minimum sequence overlap between identical chains",
    )
  parser.add_argument(
    "--min-sequence-identity",
    type = float,
    default = 0.9,
    help = "Minimum sequence identity between identical chains",
    )
  parser.add_argument(
    "--max-rmsd",
    type = float,
    default = 1.0,
    help = "Max rmsd between identical chains",
    )
  parser.add_argument(
    "--weighting-critical-value",
    type = float,
    default = 2.0,
    help = "Critical value for robust-resistant weighting scheme",
    )
  parser.add_argument(
    "--max-weight-iterations",
    type = int,
    default = 15,
    help = "Max weight iteration cycles",
    )
  parser.add_argument(
    "--max-weight-displacement",
    type = float,
    default = 1E-3,
    help = "Max weight displacement",
    )
  parser.add_argument(
    "--extend-rfree-flags",
    action = "store_true",
    help = "Extend Rfree flags to full resolution",
    )
  parser.add_argument(
    "--merge-equivalent-reflections",
    action = "store_true",
    help = "Merge symmetry equivalent reflections",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )
  parser.add_argument(
    "--minimum-rms",
    type = float,
    default = 0.4,
    help = "Minimum rms for ensembles"
    )
  parser.add_argument(
    "--alignment",
    type = str,
    default = None,
    help = "Alignment file"
    )
  parser.add_argument(
    "--save-logfile",
    action = "store_true",
    help = "Save stdout to a separate file",
    )
  parser.add_argument(
    "--domain-annotation",
    type = str,
    default = None,
    help = "Domain annotation file"
    )
  parser.add_argument(
    "--predicted-errors-file",
    type = str,
    default = None,
    help = "Predicted errors for model structure"
    )

  return parser


PROGRAM = "benchmark_create_test"

def idsplit(identifier):

  if "_" not in identifier:
    raise benchmark.BenchmarkError("Invalid identifier: '%s'" % identifier)

  ( pdbid, chainid ) = identifier.split( "_" )

  if len( pdbid ) != 4:
    raise benchmark.BenchmarkError("Invalid PDB code: '%s'" % pdbid)

  if 1 < len( chainid ):
    raise benchmark.BenchmarkError("Invalid chainID: '%s'" % chainid)

  return ( pdbid, chainid )


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  from libtbx.utils import Sorry

  try:
    ( targetpdb, targetchain ) = idsplit( identifier = params.targetid )
    ( modelpdb, modelchain ) = idsplit( identifier = params.modelid )

  except benchmark.BenchmarkError as e:
    raise Sorry(e)

  info.heading( text = "Target", level = 1 )
  from phaser.command_line import benchmark_create_case
  args = [
    targetpdb,
    "--pdb-cache-dir", params.cache_dir,
    "--seq-cache-dir", params.cache_dir,
    "--cath-cache-dir", params.cache_dir,
    "--cif-cache-dir", params.cache_dir,
    "--mtz-cache-dir", params.cache_dir,
    "--pdb-mirror", params.pdb_mirror,
    "--min-overlap", params.min_overlap,
    "--min-sequence-identity", params.min_sequence_identity,
    "--max-rmsd", params.max_rmsd,
    "--weighting-critical-value", params.weighting_critical_value,
    "--max-weight-iterations", params.max_weight_iterations,
    "--max-weight-displacement", params.max_weight_displacement,
    "--minimum-rms", params.minimum_rms,
    "--prefix", "%s%s" % ( params.prefix + "-" if params.prefix else "", targetpdb ),
    ]

  if params.extend_rfree_flags:
    args.append( "--extend-rfree-flags" )

  if params.merge_equivalent_reflections:
    args.append( "--merge-equivalent-reflections" )

  if params.domain_annotation:
    args.extend( ( "--domain-annotation", params.domain_annotation ) )

  create_case_params = parse(
    parser = benchmark_create_case.get_parser(),
    args = args,
    )
  info.paragraph_begin( indent = 4 )
  ( case_file, target_structure_file ) = benchmark_create_case.run(
    params = create_case_params,
    logger = logger,
    )
  info.paragraph_end()

  info.heading( text = "Model", level = 1 )
  from phaser.command_line import benchmark_create_model
  create_model_params = parse(
    parser = benchmark_create_model.get_parser(),
    args = [
      modelpdb,
      modelchain,
      "--pdb-cache-dir", params.cache_dir,
      "--seq-cache-dir", params.cache_dir,
      "--pdb-mirror", params.pdb_mirror,
      "--prefix", "%s%s_%s" % (
        params.prefix + "-" if params.prefix else "",
        modelpdb,
        modelchain,
        ),
      ],
    )
  info.paragraph_begin( indent = 4 )
  model_file = benchmark_create_model.run(
    params = create_model_params,
    logger = logger,
    )
  info.paragraph_end()

  info.heading( text = "Superpositions", level = 1 )
  import pickle

  with open( target_structure_file ) as ifile:
    target_structure = pickle.load( ifile )

  from phaser.command_line import benchmark_best_superposition
  parser = benchmark_best_superposition.get_parser()
  superposition_with = {}

  for ense in target_structure.get_ensembles():
    if ense.model.chain.identifier() != targetchain:
      continue

    eid = ense.identifier()
    info.heading( text = "Superposition with ensemble '%s'" % eid, level = 2 )
    info.paragraph_begin( indent = 4 )

    try:
      with logger.child( shift = 0 ) as childlogger:
        superposition_with[ eid ] = benchmark_best_superposition.run(
          params = parse(
            parser = parser,
            args = [
              target_structure_file,
              eid,
              model_file,
              "--prefix", params.prefix if params.prefix else "transformation",
              ],
            ),
          logger = childlogger,
          )

    except benchmark.BenchmarkError as e:
      info.unformatted_text( text = "Skipping this replacement" )

    info.paragraph_end()

  info.heading( text = "Alignment", level = 1 )
  info.paragraph_begin( indent = 4 )

  if params.alignment is not None:
    alignment_file = params.alignment
    info.field( name = "Alignment file", value = params.alignment )

  else:
    from phaser.command_line import benchmark_create_alignment
    alignment_file = benchmark_create_alignment.run(
      params = parse(
        parser = benchmark_create_alignment.get_parser(),
        args = [
          target_structure_file,
          targetchain,
          model_file,
          "--file-name", benchmark_create_alignment.file_name(
            prefix = params.prefix if params.prefix else "alignment",
            target = "%s_%s" % ( targetpdb, targetchain ),
            model = "%s_%s" % ( modelpdb, modelchain ),
            ),
          ],
        ),
      logger = logger,
      )

  info.paragraph_end()

  result_for = {
    "targetid": "%s_%s" % ( targetpdb, targetchain ),
    "modelid": "%s_%s" % ( modelpdb, modelchain ),
    "target_structure_file": target_structure_file,
    "case_file": case_file,
    "model_file": model_file,
    "superposition_with": superposition_with,
    "alignment_file": alignment_file,
    }

  if params.predicted_errors_file is not None:
    result_for[ "predicted_errors_file" ] = params.predicted_errors_file

  import json

  outfile_name = "%s%s_%s-vs-%s_%s.json" % (
    params.prefix + "-" if params.prefix else "",
    targetpdb,
    targetchain,
    modelpdb,
    modelchain,
    )
  info.field( name = "Output file", value = outfile_name )
  info.task_begin( text = "Writing JSON file" )

  with open( outfile_name, "w" ) as ofile:
    json.dump( result_for, ofile, sort_keys=True, indent=4, separators=(',', ': ') )

  info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return outfile_name


if __name__ == "__main__":
  parser = get_parser()
  params = parser.parse_args()

  if params.save_logfile:
    logfile = "%s%s-vs-%s.log" % (
      params.prefix + "-" if params.prefix else "",
      params.targetid,
      params.modelid,
      )

    with open( logfile, "w" ) as ofile, \
      benchmark.get_logger( stream2 = ofile ) as logger:
      run( params = parser.parse_args(), logger = logger )

  else:
    with benchmark.get_logger() as logger:
      run( params = parser.parse_args(), logger = logger )
