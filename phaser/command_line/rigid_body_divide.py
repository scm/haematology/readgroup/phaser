from __future__ import print_function

class RigidBodyDivisionError(Exception):
  """
  Module exception
  """

class AnnotatedChain(object):
  """
  Chain with associated text
  """

  def __init__(self, chain, resids, annotation, mtype):

    self.chain = chain
    self.annotation = annotation
    self.mtype = mtype

    assert len( resids ) == len( self.chain.residue_groups() )
    self.resids = resids
    self.posi_for = {}

    for ( index, ( rid, rg ) ) in enumerate( zip( self.resids, self.chain.residue_groups() ) ):
      try:
        mca = self.get_atom_named(
          rg = rg,
          name = self.mtype.representative_mainchain_atom.pdb_name,
          )

      except KeyError:
        print("No key: %s" % self.mtype.representative_mainchain_atom.pdb_name)
        mca = rg.atoms()[0]

      self.posi_for[ rid ] = Position( index = index, rg = rg, xyz = mca.xyz )


  def unassigned_resids(self, selection):

    for resid in self.resids:
      if resid not in selection:
        yield resid


  def segmentize(self, selection, consecutivity, getter):

    from phaser import sequtil
    return sequtil.split(
      sequence = [ rid for rid in self.resids if rid in selection ],
      consecutivity = lambda l, r: consecutivity(
        getter( self.posi_for[ l ] ),
        getter( self.posi_for[ r ] ),
        ),
      )


  def structural_segmentize(self, selection):

    return self.segmentize(
      selection = selection,
      consecutivity = self.mtype.residue_structural_connectivity(),
      getter = lambda p: p.rg,
      )


  def sequential_segmentize(self, selection):

    from phaser import sequtil
    return self.segmentize(
      selection = selection,
      consecutivity = sequtil.sequence_consecutivity,
      getter = lambda p: p.index,
      )


  def structural_segments(self):

    return self.structural_segmentize( selection = set( self.resids ) )


  def sequential_segments(self):

    return self.sequential_segmentize( selection = set( self.resids ) )


  def get_position(self, resid):

    return self.posi_for[ resid ]


  def identifier(self):

    return self.chain.id


  def __str__(self):

    return str( self.annotation )


  @staticmethod
  def get_atom_named(rg, name):

    for a in rg.atoms():
      if a.name == name:
        return a

    raise KeyError("Not found in residue")


class Position(object):
  """
  Store residue group and representative site
  """

  def __init__(self, index, rg, xyz):

    self.index = index
    self.rg = rg
    self.xyz = xyz


class Chain(object):
  """
  Chain that keeps original order, but allows quick removal
  """

  def __init__(self, chain):

    self.chain = chain
    self.actives = set( self.chain.resids )


  def resids(self):

    for rid in self.chain.resids:
      if rid in self.actives:
        yield rid


  def delete(self, resid):

    self.actives.remove( resid )


  def npolish(self, min_length):

    for segment in self.segments():
      if len( segment ) < min_length:
        for resid in segment:
          self.delete( resid = resid )


  def nsegments(self):

    return self.chain.structural_segmentize( selection = self.actives )


  def get_position(self, resid):

    return self.chain.get_position( resid )


  def __contains__(self, resid):

    return resid in self.actives


  def __iter__(self):

    return iter( self.actives )


  def __len__(self):

    return len( self.actives )


  def __str__(self):

    return str( self.chain )


class Alignment(object):
  """
  Alignment of chains
  """

  def __init__(self, reference, others):

    self.reference = reference
    self.others = others
    self.commons = []
    self.singles = []

    for rid in self.reference.resids():
      for chain in self.others:
        if rid not in chain:
          self.singles.append( rid )
          break

      else:
        self.commons.append( rid )


  def aligneds(self):

    return self.commons


  def unaligneds(self):

    return self.singles


  def chains(self):

    return [ self.reference ] + self.others


  def num_chains(self):

    return len( self.chains )


  def num_aligneds(self):

    return len( self.aligneds() )


  def num_unaligneds(self):

    return len( self.unaligneds() )


  def sitesets(self):

    from scitbx.array_family import flex
    positions = []

    for chain in self.chains():
      positions.append(
        flex.vec3_double(
          [ chain.get_position( resid = resid ).xyz for resid in self.aligneds() ]
          )
        )

    return positions


class Superposition(object):
  """
  Superposition
  """

  def __init__(
    self,
    alignment,
    critical = 3.0,
    convergence = 0.0001,
    max_damping_factor = 3.34,
    incremental_damping_factor = 1.5,
    weight_convergence = 0.001,
    ):

    self.alignment = alignment

    from scitbx.array_family import flex
    self.weights = flex.double( [ 1 ] * self.alignment.num_aligneds() )

    from phaser import multiple_superposition
    self.diamond = multiple_superposition.DiamondAlgorithm(
      site_sets = self.alignment.sitesets(),
      weights = self.weights,
      )
    weighting = multiple_superposition.RobustResistantWeightScheme(
      critical_value_square = critical ** 2,
      )
    weight_convergence_sq = weight_convergence ** 2

    while True:
      try:
        multiple_superposition.iterate(
          superposition = self.diamond,
          convergence = convergence,
          )

      except multiple_superposition.NoConvergence:
        pass

      diff_sq = self.diamond.distance_squares_from_average_structure()
      total_damping_factor = 1.0

      while True:
        new_weights = weighting.for_difference_squares( squares = diff_sq )

        try:
          self.diamond.prepare_new_weights( weights = new_weights )

        except multiple_superposition.BadWeights:
          if max_damping_factor < total_damping_factor:
            raise RigidBodyDivisionError("Weight damping recovery exhausted")

          total_damping_factor *= incremental_damping_factor
          diff_sq = diff_sq / incremental_damping_factor
          continue

        break

      displacement_sq = flex.mean_sq( new_weights - self.weights )

      if displacement_sq <= weight_convergence_sq:
        break

      self.weights = new_weights
      self.diamond.set_new_weights()


  def rmsd(self):

    return self.diamond.rmsd()


  def rigid_segment_selection(self, threshold):

    selection = ( threshold < self.weights )
    aligneds = self.alignment.aligneds()
    assert len( selection ) == len( aligneds )
    return Selection(
      resids = [ rid for ( rid, sel ) in zip( aligneds, selection ) if sel ],
      chains = [ c.chain for c in self.alignment.chains() ],
      )


class Selection(object):
  """
  Residue selection
  """

  def __init__(self, resids, chains):

    self.resids = set( resids )
    self.chains = chains


  def extend(self, iterable):

    self.resids.update( iterable )


  def remove(self, iterable):

    self.resids.difference_update( iterable )


  def __len__(self):

    return len( self.resids )


  def __iter__(self):

    return iter( self.resids )


  def __contains__(self, key):

    return key in self.resids


def polish_gaps(selection, chains, min_length):

  restoreds = []

  for chain in chains:
    restoreds.extend(
      polish_gaps_wrt(
        resids = selection,
        chain = chain,
        min_length = min_length,
        )
      )

  return restoreds


def polish_gaps_wrt(resids, chain, min_length):

  from phaser import sequtil
  restoreds = []

  for segment in chain.structural_segments():
    selecteds = [ i for ( i, rid ) in enumerate( segment ) if rid in resids ]

    if not selecteds:
      continue

    segiter = sequtil.split(
      sequence = selecteds,
      consecutivity = sequtil.sequence_consecutivity,
      )

    previous = segiter.next()

    for seg in segiter:
      if seg[0] - previous[-1] - 1 < min_length:
        restoreds.extend( segment[ i ] for i in range( previous[-1] + 1, seg[0] ) )

  return restoreds


def polish_segments(selection, chains, min_length):

  moribunds = []

  for chain in chains:
    moribunds.extend(
      polish_segments_wrt(
        resids = selection,
        chain = chain,
        min_length = min_length,
        )
      )

  return moribunds


def polish_segments_wrt(resids, chain, min_length):

  from phaser import sequtil
  moribunds = []

  for segment in chain.structural_segments():
    selecteds = [ i for ( i, rid ) in enumerate( segment ) if rid in resids ]

    if not selecteds:
      continue

    segiter = sequtil.split(
      sequence = selecteds,
      consecutivity = sequtil.sequence_consecutivity,
      )

    for seg in segiter:
      if seg[-1] - seg[0] + 1 < min_length:
        moribunds.extend( segment[i] for i in range( seg[0], seg[-1] + 1 ) )

  return moribunds


def find_equivalence_groups(
  chains,
  min_sequence_overlap = 0.3,
  min_sequence_identity = 0.8,
  ):

  from boost_adaptbx import graph

  mygraph = graph.adjacency_list( graph_type = "undirected" )

  for chain in chains:
    mygraph.add_vertex( label = chain )

  import itertools

  for ( vid_left, vid_right ) in itertools.combinations( mygraph.vertices(), 2 ):
    left = mygraph.vertex_label( vertex = vid_left )
    right = mygraph.vertex_label( vertex = vid_right )
    print("Relation %s <-> %s" % (left, right))

    if left.mtype != right.mtype:
      print("Different types, not considered equivalent")

    resids_left = set( left.resids )
    resids_right = set( right.resids )
    normalizer = max( len( resids_left ), len( resids_right ) )
    commons = resids_left & resids_right
    overlap = len( commons ) / normalizer

    if overlap < min_sequence_overlap:
      print("Overlap too small (%.2f), not considered equivalent" % overlap)
      continue

    identicals = [
      rid for rid in commons
      if ( left.get_position( resid = rid ).rg.atom_groups()[0].resname
        == right.get_position( resid = rid ).rg.atom_groups()[0].resname )
      ]
    identity = len( identicals ) / normalizer

    if identity < min_sequence_identity:
      print("Identity too small (%.2f), not considered equivalent" % identity)
      continue

    print( "Type: %s, overlap: %.2f, identity: %.2f, ACCEPTED" % ( left.mtype, overlap, identity ))
    mygraph.add_edge( vertex1 = vid_left, vertex2 = vid_right )

  from graph import connected_component_algorithm as cca
  res = cca.connected_components( graph = mygraph )

  return [ [ mygraph.vertex_label( vertex = v ) for v in comp ] for comp in res ]


def run(
  chains,

  min_sequence_overlap = 0.3,
  min_sequence_identity = 0.8,

  insignificant_segment_size = {
    "protein": 15,
    "dna": 5,
    "rna": 5,
    },

  critical = 3.0,
  convergence = 0.0001,
  max_damping_factor = 3.34,
  incremental_damping_factor = 1.5,
  weight_convergence = 0.001,

  threshold = 0.05,
  short_segment_length = 10,
  short_gap_length = 6,
  ):

  from phaser import mmtype
  from phaser import tbx_utils

  knowns = [ mmtype.PROTEIN, mmtype.DNA, mmtype.RNA ]
  annchains = []
  print ("= Chain type analysis =")

  for chain in chains:
    annotation = tbx_utils.ChainAnnotation.from_chain( chain = chain )
    mtype = mmtype.determine( chain = chain, mmtypes = knowns )
    print ("%s: %s" % ( annotation, mtype ))

    if mtype == mmtype.UNKNOWN:
      print ("  Skipping this chain")

    else:
      print("  Accepting this chain")
      annchains.append(
        AnnotatedChain(
          chain = chain,
          resids = [ rg.resid() for rg in chain.residue_groups() ],
          annotation = annotation,
          mtype = mtype,
          )
        )

  print()
  print("= Chain similarity analysis =")
  groups = find_equivalence_groups(
    chains = annchains,
    min_sequence_overlap = min_sequence_overlap,
    min_sequence_identity = min_sequence_identity,
    )
  results = []

  print("%s identity groups found" % len(groups))
  import itertools

  for ( index, group ) in enumerate( groups, start = 1 ):
    assert group
    mtype = group[0].mtype

    print("== %s. group: %s ==" % (index, mtype))
    print("  Members:")

    for chain in group:
      print("    ", chain)

    alichains = [ Chain( chain = c ) for c in group ]

    counter = itertools.count( start = 1 )
    rigids = []

    while True:
      print("= Iteration =", next(counter))

      threshold_size = insignificant_segment_size[ mtype.name ]
      print()
      print("- Chain review -")
      moribunds = [
        chain for chain in alichains if len( chain ) < threshold_size
        ]
      if moribunds:
        print("Chains too short:")

        for chain in moribunds:
          print("  %s: %s residues" % (chain, len(chain)))

        alichains = [ chain for chain in alichains if chain not in moribunds ]

      else:
        print("No editing necessary")

      if len( alichains ) <= 1:
        print("Quit search as there are less than two chains to analyse")
        break

      print()
      print("- Chain polishing -")

      for chain in alichains:
        before = len( chain )
        moribunds = polish_segments(
          selection = chain,
          chains = [ chain.chain ],
          min_length = short_segment_length,
          )

        for rid in moribunds:
          chain.delete( rid )

        print("%s: before: %s residues, after: %s residues" % (chain, before, len(chain)))

      print()
      print("- Alignment -")

      alignments = [
        Alignment(
          reference = chain,
          others = [ c for c in alichains if c is not chain ],
          )
        for chain in alichains
        ]

      best = min( alignments, key = lambda a: a.num_unaligneds() )
      print("Selected as reference:", best.reference)
      print("  Aligned residues:", best.num_aligneds())
      print("  Unaligned residues:", best.num_unaligneds())

      if best.num_aligneds() < threshold_size:
        print("Aligned residue count below minimum (%s)" % threshold_size)
        print("Remove chain with lowest number of common residues")
        moribund = min( alignments, key = lambda a: a.num_aligneds() )
        print("Selected for removal:", moribund.reference)
        print("  Aligned residues:", moribund.num_aligneds())
        print("  Unaligned residues:", moribund.num_unaligneds())
        alichains.remove( moribund.reference )
        continue

      print()
      print("- Rigidity analysis -")

      try:
        superposition = Superposition(
          alignment = best,
          critical = critical,
          convergence = convergence,
          max_damping_factor = max_damping_factor,
          incremental_damping_factor = incremental_damping_factor,
          weight_convergence = weight_convergence,
          )

      except RigidBodyDivisionError:
        print("Superposition failed - structural differences between domains too large")
        break

      selection = superposition.rigid_segment_selection( threshold = threshold )
      print("Rigid domain size:", len(selection))

      if len( selection ) < threshold_size:
        print("The largest rigid domain is too small, skip further analysis")
        break

      extras = polish_gaps(
        selection = selection,
        chains = selection.chains,
        min_length = short_gap_length,
        )
      selection.extend( extras )
      print("  After gap polishing:", len(selection))

      moribunds = polish_segments(
        selection = selection,
        chains = selection.chains,
        min_length = short_segment_length,
        )
      selection.remove( moribunds )
      print("  After segment polishing:", len(selection))

      rigids.append( selection )

      for resid in selection:
        for chain in alichains:
          if resid in chain:
            chain.delete( resid  = resid )

    results.append( rigids )
    print

  assert len( groups ) == len( results )
  print("== Rigid domains summary ==")

  counter = itertools.count( start = 1 )
  assigneds_for = dict( ( chain, set() ) for chain in annchains )

  for ( group, result ) in zip( groups, results ):
    for selection in result:
      print("NCS group %s" % next(counter))

      for chain in selection.chains:
        assigneds_for[ chain ].update( selection )
        segments = []

        for segment in chain.sequential_segmentize( selection = selection ):
          if len( segment ) == 1:
            segments.append( "'%s'" % segment[0] )

          else:
            segments.append( "'%s' -> '%s'" % ( segment[0], segment[-1] ) )

        print("chain'%s': %s" % (chain.identifier(), ", ".join(segments)))

      print()
  print("== Unassigned residues ==")

  for chain in annchains:
    unassigneds = list(
      chain.unassigned_resids( selection = assigneds_for[ chain ] )
      )

    if unassigneds:
      segments = []

      for ( i, segment ) in enumerate( chain.sequential_segmentize( selection = unassigneds ), start = 1 ):
        if len( segment ) == 1:
          segments.append( "'%s'" % segment[0] )

        else:
          segments.append( "'%s' -> '%s'" % ( segment[0], segment[-1] ) )

      print("chain '%s': %s" % (chain.identifier(), ", ".join(segments)))

    else:
      print("chain '%s': no unassigned residues" % chain.identifier())


if __name__ == "__main__":
  import sys

  if len( sys.argv ) != 2:
    print("Usage: %s PDBFILE" % sys.argv[0])
    sys.exit( 1 )

  import iotbx.pdb

  root = iotbx.pdb.input( sys.argv[1] ).construct_hierarchy()

  from phaser import tbx_utils
  count = tbx_utils.split_all_chains_in( root = root )
  print("%s new chains created by splitting up chains" % count)
  run( chains = root.models()[0].chains() )
