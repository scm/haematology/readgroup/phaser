from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "case",
    metavar = "CASE",
    help = "Case file",
    )
  parser.add_argument(
    "structure",
    metavar = "STRUCTURE",
    help = "Structure file",
    )
  parser.add_argument(
    "--shift",
    type = float,
    default = 0.05,
    help = "Decrement for refined VRMS when restarting refinement",
    )
  parser.add_argument(
    "--min-vrms",
    type = float,
    default = 0.20,
    help = "Minimum value for VRMS",
    )
  parser.add_argument(
    "--disable-bfactor-refinement",
    action = "store_false",
    dest = "bfactor",
    help = "Disable Bfactor refinement"
    )
  parser.add_argument(
    "--disable-vrms-refinement",
    action = "store_false",
    dest = "vrms",
    help = "Disable VRMS refinement"
    )

  return parser


PROGRAM = "benchmark_calculate_mr_statistics"


def apply_updated_ensembles(phaser_input, ensembles, newvrms_for, shift, min_vrms):

  for ense in ensembles:
    eid = ense.identifier()

    if eid in newvrms_for:
      vrms = newvrms_for[ eid ]
      assert len( vrms ) == 1
      newerror = benchmark.RmsError( rmsd = max( vrms[0] - shift, min_vrms) )
      newerror.apply(
        phaser_input = phaser_input,
        modlid = eid,
        content = ense.model.content(),
        name = eid,
        )

    else:
      ense.apply( phaser_input = phaser_input )


def mr_protocol_1(case, structure, bfactor, vrms, shift, min_vrms, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  import phaser
  inp = phaser.InputMR_RNP()
  inp.setMUTE( True )
  inp.setXYZO( False )
  inp.setHKLO( False )

  inp.setMACM_PROT( "CUSTOM" )
  inp.addMACM(
    ref_rota = True,
    ref_tran = True,
    ref_bfac = False,
    ref_vrms = vrms,
    )

  case.apply( phaser_input = inp )
  structure.apply( phaser_input = inp )
  info.task_begin( text = "Running Phaser" )
  result = phaser.runMR_RNP( inp )

  if result.Failed():
    info.task_end( text = "failed" )
    info.preformatted_text( text = result.logfile() )
    raise benchmark.PhaserError( result = result )

  info.task_end()

  if vrms:
    mrsol = result.getDotSol()
    info.heading( text = "Refined solution:", level = 4 )
    info.preformatted_text( text = mrsol.unparse() )

    assert len( mrsol ) == 1
    mrset = mrsol[0]

    inp = phaser.InputMR_RNP()
    inp.setMUTE( True )
    inp.setXYZO( False )
    inp.setHKLO( False )

    inp.setMACM_PROT( "CUSTOM" )
    inp.addMACM(
      ref_rota = True,
      ref_tran = True,
      ref_bfac = False,
      ref_vrms = True,
      )

    case.apply( phaser_input = inp )
    apply_updated_ensembles(
      phaser_input = inp,
      ensembles = structure.get_ensembles(),
      newvrms_for = mrset.NEWVRMS,
      shift = shift,
      min_vrms = min_vrms,
      )
    inp.setSOLU( [ mrset ] )

    info.task_begin( text = "Running Phaser - restarting from final VRMS value" )
    result = phaser.runMR_RNP( inp )

    if result.Failed():
      info.task_end( text = "failed" )
      info.preformatted_text( text = result.logfile() )
      raise benchmark.PhaserError( result = result )

    info.task_end()

  if bfactor:
    mrsol = result.getDotSol()
    info.heading( text = "Refined solution:", level = 4 )
    info.preformatted_text( text = mrsol.unparse() )

    assert len( mrsol ) == 1
    mrset = mrsol[0]

    inp = phaser.InputMR_RNP()
    inp.setMUTE( True )
    inp.setXYZO( False )
    inp.setHKLO( False )

    inp.setMACM_PROT( "CUSTOM" )
    inp.addMACM(
      ref_rota = True,
      ref_tran = True,
      ref_bfac = True,
      ref_vrms = vrms,
      )

    case.apply( phaser_input = inp )
    apply_updated_ensembles(
      phaser_input = inp,
      ensembles = structure.get_ensembles(),
      newvrms_for = mrset.NEWVRMS,
      shift = shift,
      min_vrms = min_vrms,
      )
    inp.setSOLU( [ mrset ] )

    info.task_begin( text = "Running Phaser - restarting with Bfactor ON" )
    result = phaser.runMR_RNP( inp )

    if result.Failed():
      info.task_end( text = "failed" )
      info.preformatted_text( text = result.logfile() )
      raise benchmark.PhaserError( result = result )

    info.task_end()

  mrsol = result.getDotSol()
  info.heading( text = "Refined solution:", level = 4 )
  info.preformatted_text( text = mrsol.unparse() )
  assert len( mrsol ) == 1
  mrset = mrsol[0]
  info.heading( text = "Refined values:", level = 4 )
  info.field_float( name = "Refined LLG", value = mrset.LLG, digits = 3 )
  info.field_float( name = "Refined TFZ", value = mrset.TFZ, digits = 2 )

  atoms_count = len( structure.as_iotbx_hierarchy().atoms() )
  info.field( name = "Number of atoms", value = atoms_count )
  results = [ mrset.LLG, mrset.TFZ, atoms_count ]

  for ( name, value ) in mrset.NEWVRMS.items():
    assert len( value ) == 1
    info.field_float( name = name, value = value[0], digits = 3 )
    results.append( ( name, value[0] ) )

  return results


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  import pickle
  from libtbx.utils import Sorry
  info.task_begin( text = "Reading input file" )

  try:
    with open( params.case ) as ifile:
      case = pickle.load( ifile )

    with open( params.structure ) as ifile:
      structure = pickle.load( ifile )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  info.task_end()
  info.field( name = "Data", value = case.data )
  info.heading( text = "Phaser refinement", level = 1 )

  try:
    results = mr_protocol_1(
      case = case,
      structure = structure,
      bfactor = params.bfactor,
      vrms = params.vrms,
      shift = params.shift,
      min_vrms = params.min_vrms,
      logger = logger,
      )

  except benchmark.PhaserError as e:
    raise Sorry(str(e))

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return results


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
