from __future__ import print_function

from phaser import benchmark


def get_parser():

  import argparse
  import sys

  parser = argparse.ArgumentParser()
  parser.add_argument(
    "suite",
    metavar = "SUITE_FILE",
    help = "Suite file containing tests to perform",
    )
  parser.add_argument(
    "phils",
    nargs = "*",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL arguments",
    )
  parser.add_argument(
    "--repeats",
    type = int,
    default = 1,
    help = "Number of repetitions",
    )
  parser.add_argument(
    "--engine",
    default = None,
    help = "PHIL file containing multiprocessing description"
    )
  parser.add_argument(
    "--outfile",
    type = argparse.FileType( "w" ),
    default = sys.stdout,
    help = "Output csv file"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = "errorpred",
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_error_prediction_suite"


def execute_test(
  test,
  phils,
  prefix,
  ):

  args = [ test ] + phils

  assert prefix is not None
  args.extend( [ "--prefix", prefix ] )

  logname = "%s.log" % prefix

  from phaser.command_line import benchmark_error_prediction_test

  with open( logname, "w" ) as ofile, benchmark.get_logger( stream = ofile ) as logger:
    result = benchmark_error_prediction_test.run(
      params = parse(
        parser = benchmark_error_prediction_test.get_parser(),
        args = args,
        ),
      logger = logger,
      )

  return result


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params):

  creator = benchmark.get_engine_creator( filename = params.engine )

  import csv
  writer = csv.writer( params.outfile )
  writer.writerow( [ "Test", "Homology model", "ProQ model", "Error file", "Time" ] )

  from libtbx.scheduling import holder
  from libtbx.scheduling import parallel_for
  from phaser.command_line import benchmark_error_prediction_suite

  import os.path
  import itertools
  dirname = os.path.dirname( params.suite )

  with open( params.suite ) as ifile:
    reader = csv.reader( ifile )
    lines = list( reader )

  with holder( creator = creator ) as manager:
    iters = []

    for i in range( 1, params.repeats + 1 ):
      iters.append(
        [ (
          benchmark_error_prediction_suite.execute_test,
          (),
          {
            "test": os.path.normpath( os.path.join( dirname, line[0] ) ),
            "phils": params.phils,
            "prefix": "%s_%s_%s" % (
              params.prefix,
              os.path.splitext( os.path.basename( line[0] ) )[0],
              i,
              ),
            }
            )
          for line in lines if line ]
        )

    pfi = parallel_for.iterator(
      calculations = itertools.chain.from_iterable( iters ),
      manager = manager,
      keep_input_order = True,
      )

    for ( calc, res ) in pfi:
      row = [ os.path.basename( calc[2][ "test" ] ) ]

      try:
        results = res()
        row.extend( results[2:] )

      except Exception as e:
        row.append( str( e ) )

      writer.writerow( row )

    manager.join()


if __name__ == "__main__":
  parser = get_parser()
  run( params = parser.parse_args() )
