from __future__ import print_function

from __future__ import division
from phaser.phenix_interface import master_params, phaser_style, match_modes
import iotbx.phil
from cctbx import sgtbx
from libtbx.phil import interface as phil_interface
from optparse import OptionParser
import xml.dom.minidom as xmldom
import xml.etree.ElementTree as ET
import re
import sys

# define mappings for .type to <className>
phil_type_as_class_name = {
  "str" : "CString",
  "int" : "CInt",
  "bool" : "CBoolean",
  "float" : "CFloat",
  "choice" : "CString",
}

def translate_keyword (name, style, prefix=None, multiple=False) :
#  method_name = "set"
#  if (multiple) :
#    method_name = "add"
  method_name = ""
  if prefix is not None :
    method_name = "%s_" % prefix
  fields = [ s[:4] for s in name.split("_") ]
  method_name += "_".join([ s.upper() for s in fields ])
  #if (style.as_multiple) and (method_name.startswith("set")) :
  #  method_name = method_name.replace("set", "add")
  return method_name

def convert_keywords (keyword_phil,
                      root,
                      mode,
                      inherit_mode=False,
                      default_method_base=None,
                      offset_start=0) :
  for keyword in keyword_phil.objects[offset_start:] :
    if keyword.is_template != 0 :
      continue
    style = phaser_style(keyword)
    if style.ignore :
      continue
    elif match_modes(mode, style.mode, inherit_mode) :
      if keyword.is_scope :
        method_base = default_method_base
        if method_base is None :
          method_base = translate_keyword(
            name=keyword.name,
            style=style,
            multiple=keyword.multiple)
        elif (keyword.multiple) :
          pass #method_base = "add" + method_base[3:]
        if style.listargs or style.array :
          continue
          # setXXXX_YYYY(param1, ...)
          args = []
          for subkeywd in keyword.objects :
            assert not subkeywd.multiple
            value = subkeywd.extract()
            if value is None :
              keyword.show()
              raise Sorry("%s must not be None." % subkeywd.full_path())
            args.append(value)
          if style.listargs :
            pass #method(*args)
          else :
            pass
        elif style.kwargs :
          continue
          kwargs = {}
          kwargs_out = []
          for subkeywd in keyword.objects :
            assert not subkeywd.multiple
            value = subkeywd.extract()
            if value is None :
              keyword.show()
              raise Sorry("%s must not be None." % subkeywd.full_path())
            kwargs[subkeywd.name] = value
            value_out = str(value)
            if isinstance(value, str) :
              value_out = "'%s'" % value
            kwargs_out.append("%s=%s" % (subkeywd.name, value_out))
          try :
            method(**kwargs)
          except Exception as e :
            if (type(e).__name__ == "ArgumentError") :
              raise RuntimeError(
                "invalid boost.python arguments:\n%s\nKeywords:\n%s" %
                  (str(e), str(kwargs)))
            else :
              raise
        elif style.silent :
          # XXX: this should work recursively...
          convert_keywords(
            keyword_phil=keyword,
            mode=mode,
            root=root,
            inherit_mode=True)
        else :
          # setXXXX_YYYY(param1)
          # setXXXX_ZZZZ(param2)
          # ...
          current_offset = 0
          for subkeywd in keyword.objects :
            if subkeywd.is_template != 0 :
              continue
            style2 = phaser_style(subkeywd)
            if style2.ignore :
              current_offset += 1
              continue
            if (not match_modes(mode, style2.mode, True)) :
              continue
            if subkeywd.is_scope :
              subkeywd_base = method_base
              if style2.parent_silent :
                subkeywd_base = None
              convert_keywords(
                keyword_phil=keyword,
                mode=mode,
                root=root,
                inherit_mode=True,
                default_method_base=subkeywd_base,
                offset_start=current_offset)
              current_offset += 1
              break
              #continue
            value = subkeywd.extract()
            if value is None :
              current_offset += 1
              continue
            method_base = translate_keyword(name=keyword.name,
                                            style=style,
                                            prefix=default_method_base)
            if subkeywd.multiple or style2.as_multiple :
              method_base = "add" + method_base[3:]
            if style2.silent :
              method_name = method_base
            else :
              method_name = translate_keyword(name=subkeywd.name,
                                              style=style2,
                                              prefix=method_base)
            if value is not None :
              if isinstance(value, str) :
                pass
              else :
                if isinstance(value, sgtbx.space_group_info) :
                  pass
                elif (isinstance(value, list)) and (style2.as_multiple) :
                  for val_ in value :
                    pass
                else :
                  pass
            else :
              pass
            current_offset += 1
      elif keyword.is_definition :
        method_name = translate_keyword(
          name=keyword.name,
          style=style,
          prefix=default_method_base,
          multiple=keyword.multiple)
        value = keyword.extract()
        elem = ET.SubElement(root, "content")
        elem.set("id", method_name)
        elem_class = ET.SubElement(elem, "className")
        elem_class.text = phil_type_as_class_name.get(keyword.type.phil_type,
          "CString")
        qualifiers = ET.SubElement(elem, "qualifiers")
        if (keyword.short_caption is not None) : # control label
          gui_label = ET.SubElement(qualifiers, "guiLabel")
          gui_label.text = keyword.short_caption
        if (keyword.help is not None) : # tooltip
          toolTip = ET.SubElement(qualifiers, "toolTip")
          toolTip.text = keyword.help
        if (keyword.expert_level is not None) :
          expert_level = ET.SubElement(qualifiers, "expertLevel")
          expert_level.text = str(keyword.expert_level)
        if (keyword.type.phil_type == "bool") and (value is not None) :
          default = ET.SubElement(qualifiers, "default")
          default.text = str(value)
        elif (keyword.type.phil_type == "choice") :
          enum = ET.SubElement(qualifiers, "enumerators")
          enum.text = parse_choice_options(keyword)
          default = ET.SubElement(qualifiers, "default")
          default.text = str(value)
          if (keyword.caption is not None) :
            menu = ET.SubElement(qualifiers, "menuText")
            menu.text = parse_captions(keyword)
          if (not keyword.type.multi) :
            only = ET.SubElement(qualifiers, "onlyEnumerators")
            only.text = "1"
        elif (keyword.type.phil_type in ["int", "float"]) :
          default = ET.SubElement(qualifiers, "default")
          default.text = str(value)
          if (keyword.type.value_min is not None) :
            min = ET.SubElement(qualifiers, "min")
            min.text = str(keyword.type.value_min)
          if (keyword.type.value_max is not None) :
            max = ET.SubElement(qualifiers, "max")
            max.text = str(keyword.type.value_max)
    else :
      continue

def parse_choice_options (phil_def) :
  return ",".join([ re.sub("\*", "", word.value) for word in phil_def.words ])

def parse_captions (phil_def) :
  return ",".join([re.sub("_"," ",item) for item in phil_def.caption.split()])

def run (args) :
  parser = OptionParser()
  parser.add_option("--mode", dest="phaser_mode", action="store",
    help="Phaser mode (MR_AUTO, etc.)", default="MR_AUTO")
  options, args = parser.parse_args(args)
  root = ET.Element("container")
  root.set("id", "keywords")
  master_phil = master_params()
  index = phil_interface.index(master_phil,
    working_phil=master_phil.fetch(),
    parse=iotbx.phil.parse)
  keyword_phil = index.get_scope_by_name("phaser.keywords")
  convert_keywords(
    keyword_phil=keyword_phil,
    root=root,
    mode=options.phaser_mode)
  dom = xmldom.parseString(ET.tostring(root))
  print(dom.toprettyxml(indent="  "))


if (__name__ == "__main__") :
  run(sys.argv[1:])
