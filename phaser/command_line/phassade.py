# LIBTBX_SET_DISPATCHER_NAME phenix.phassade
# LIBTBX_SET_DISPATCHER_NAME phaser.phassade

from __future__ import print_function

from phaser import *
import sys
from libtbx.utils import Sorry
from libtbx.phil import parse
import mmtbx.utils
from phaser import SimpleFileProperties

"""
hklin = "tryp_peak.mtz"
root = "phassade"
mute = False
njobs = 1
n_sites =  1
scatterer =  "Se"
wavelength = 0.9790
"""

master_phil = parse("""
phassade
{
  hklin = "tryp_peak.mtz"
    .help = "reflections file"
    .type = path
  labin = None
    .type = str
    .help = "Labels amplitude as well as corresponding sigma columns to use"
  wavelength = 0.97
    .type = float
  hires = None
    .type = float
    .help = "Resolution limit"
  root = "phassade"
    .help = "Prefix of output files"
    .type = str
  mute = True
    .type = bool
  njobs = 4
    .type = int
  n_sites = 1
    .type = int
  scatterer = "Se"
    .type = str
}

""", process_includes=True)


def main( args ):
  h = os.path.basename( __file__ )
  default_message="""\
Usage:

  %s phil_input.txt

where PHIL parameters conform to the following definitions:
""" % h

  if(len(args) <=1):
    print(default_message)
    master_phil.show(prefix="  " )
    return

  inputs = mmtbx.utils.process_command_line_args(args = args[1:],
                                                 master_params = master_phil
                                                 )
  workparams = inputs.params.extract().phassade

  if workparams.hklin is None:
    print("hklin not set")
    return
  print("hklin=", workparams.hklin)
  print("scatterer=",workparams.scatterer)
  print("n_sites=",workparams.n_sites)
  i = InputEP_DAT()
  i.setHKLI(workparams.hklin)
  i.setMUTE(workparams.mute)
  r = runEP_DAT(i)
  if r.Success():
    i = InputEP_SSD()
    i.setCRYS_DATA(r.getCrysData())
    i.setSPAC_HALL(r.getSpaceGroupHall())
    i.setCELL6(r.getUnitCell())
    i.setWAVE(workparams.wavelength)
    i.setROOT(workparams.root)
    if (workparams.mute):
      i.setMUTE(workparams.mute)
    i.setJOBS(workparams.njobs)
    if (workparams.hires):
      i.setRESO_HIGH(workparams.hires)
    print("running phassade...")
    ssd = runEP_SSD(i)
    if not ssd.number_of_solutions():
      print("no phassade solutions")
      sys.exit(1)
    if ssd.number_of_solutions() == 1:
      print("single solution from phassade")
    else:
      print(ssd.number_of_solutions(), " solutions from phassade")
    print("%5s %5s %10s %10s %10s %10s %10s" % ("set", "atom", "x", "y", "z", "occupancy", "u-iso"))
    for s in range(0,ssd.number_of_solutions()):
      atom = ssd.get_atoms_for_solution(s)
      for i in range(0,atom.size()):
        print("%5s %5s %10.4f %10.4f %10.4f %10.4f %10.4f" % \
         (s+1,atom[i].scattering_type,atom[i].site[0],atom[i].site[1],atom[i].site[2],atom[i].occupancy,atom[i].u_iso))
    ep = ssd.getDotSol()
    if ssd.Success():
      i = InputEP_SAD()
      i.setCRYS_DATA(r.getCrysData())
      i.setSPAC_HALL(r.getSpaceGroupHall())
      i.setCELL6(r.getUnitCell())
      i.setWAVE(workparams.wavelength)
      i.setROOT(workparams.root)
      i.setATOM(ep)
      if (workparams.mute):
        i.setMUTE(workparams.mute)
      i.setJOBS(workparams.njobs)
      if (workparams.hires):
        i.setRESO_HIGH(workparams.hires)
      print("refinement and completion...")
      sad = runEP_SAD(i)
      if sad.Success():
          atom = sad.getTopAtoms()
          if atom.size() == 1:
            print ("best solution has 1 refined atom")
          else:
            print("best solution has ", atom.size(), " refined atoms")
          print("%5s %5s %10s %10s %10s %10s %10s" % \
            ("","atom","x","y","z","occupancy","u-iso"))
          for i in range(0,atom.size()):
            print("%5s %5s %10.4f %10.4f %10.4f %10.4f %10.4f" %
                  ("", atom[i].scattering_type, atom[i].site[0], atom[i].site[1], atom[i].site[2], atom[i].occupancy,
                   atom[i].u_iso))


if __name__ == '__main__':
  main( sys.argv )
