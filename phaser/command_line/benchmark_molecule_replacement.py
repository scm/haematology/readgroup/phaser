from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "target",
    metavar = "FILENAME",
    help = "Model chain file",
    )
  parser.add_argument(
    "--replacement",
    action = "append",
    default = [],
    nargs = 4,
    metavar = ( "ENSEMBLEID", "MODEL_CHAIN", "TRANSFORMATION", "RMSD" ),
    help = "Replace ENSEMBLEID with appropriate domain of MODEL_CHAIN transformed by TRANSFORMATION",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = "replaced",
    help = "Prefix for output data",
    )
  parser.add_argument(
    "--write-pdb-file",
    action = "store_true",
    help = "Write sculpted PDB file",
    )
  parser.add_argument(
    "--min-hss-length",
    type = int,
    default = 10,
    help = "Minimum sequence overlap length with replacing chain"
    )

  return parser


PROGRAM = "benchmark_molecule_replacement"

def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )
  info.task_begin( text = "Reading input files" )
  import pickle
  from libtbx.utils import Sorry
  import os.path

  replacement_for = {}
  sculpted_chain_read_from = {}

  try:
    with open( params.target ) as ifile:
      structure = pickle.load( ifile )

    structure_prefix = os.path.splitext( os.path.basename( params.target ) )[0]

    for ( enseid, model_file, transformation_file, rmsd ) in params.replacement:
      ensemble = structure.get_ensemble( identifier = enseid )

      if model_file in sculpted_chain_read_from:
        ( modelid, sculpted_chain ) = sculpted_chain_read_from[ model_file ]

      else:
        with open( model_file ) as ifile:
          sculpted_chain = pickle.load( ifile )

        modelid = os.path.splitext( os.path.basename( model_file ) )[0]
        sculpted_chain_read_from[ model_file ] = ( modelid, sculpted_chain )

      if ensemble.model.chain.mmt() != sculpted_chain.chain.mmt():
        raise benchmark.BenchmarkError("Different molecule types")

      with open( transformation_file ) as ifile:
        transformation = pickle.load( ifile )

      if rmsd.upper() == "NONE":
        error = sculpted_chain.error()

      else:
        try:
          errval = float( rmsd )

        except ValueError as e:
          raise Sorry(e)

        else:
          error = benchmark.RmsError( rmsd = errval )

      replacement_for[ ensemble ] = ( modelid, sculpted_chain, transformation, error )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  except KeyError as e:
    info.task_end( text = "failed" )
    raise Sorry("Ensemble '%s' not known (knowns: %s)" % (
      e.args[0],
      ",".join(structure.get_ensemble_ids()),
    ))

  except benchmark.BenchmarkError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  info.task_end()

  info.heading( text = "Chain replacements", level = 1 )
  info.unordered_list_begin()
  chain_remapping_for = {}

  for ense in replacement_for:
    chain = ense.model.chain
    chain_remapping_for.setdefault( chain, set() ).add( replacement_for[ ense ] )

  from phaser import chainalign
  frameshift_between = {}

  for chain in chain_remapping_for:
    info.list_item_begin(
      text = "Chain '%s_%s'" % ( structure_prefix, chain.identifier() )
      )
    matcher = chainalign.HSSFind( sequence = chain.gapless_reference_sequence() )
    info.unordered_list_begin( level = 2 )

    for ( modelid, sculpted_chain, sup, err ) in chain_remapping_for[ chain ]:
      info.list_item_begin( text = str( modelid ) )
      fragment = chainalign.Fragment(
        index = 0,
        sequence = sculpted_chain.chain.gapless_reference_sequence(),
        )
      matches = matcher.matches_with( fragment = fragment, min_length = params.min_hss_length )

      if not matches:
        raise Sorry("No overlap above %s found between the two chains" % params.min_hss_length)

      best = max( matches, key = lambda m: m.length )
      info.field( name = "Best frameshift", value = best.start )
      info.field( name = "Best length", value = best.length )
      frameshift_between[ ( chain, sculpted_chain ) ] = best.start
      info.list_item_end()

    info.list_end()
    info.list_item_end()

  info.list_end()

  info.heading( text = "Domain segments", level = 1 )
  info.unordered_list_begin()
  from phaser import sequtil

  replacement_ensemble_for = {}

  for ense in replacement_for:
    info.list_item_begin( text = "Chain '%s'" % ense.identifier() )
    shift = frameshift_between[ ( ense.model.chain, replacement_for[ ense ][1] ) ]
    ense_mapper = sequtil.SegmentMapping(
      indexer = ense.model.chain.reference_to_gapless_position_indexer(),
      )
    repl_mapper = sequtil.SegmentMapping(
      indexer = replacement_for[ ense ][1].chain.gapless_to_reference_position_indexer(),
      )

    info.ordered_list_begin()
    segments = []

    for segment in ense.model.selection.segments():
      indices = segment()
      info.list_item_begin(
        text = "segment: positions %s-%s" % ( indices[0] + 1, indices[-1] + 1 )
        )
      info.field( name = "Length", value = len( indices ) )
      ( gstart, gend ) = ense_mapper( segment = indices )
      info.field( name = "Gapless positions", value = "%s-%s" % ( gstart + 1, gend + 1 ) )

      try:
        ( rstart, rend ) = repl_mapper(
          segment = [ e - shift for e in range( gstart, gend + 1 ) ]
          )

      except sequtil.SequenceError:
        info.text( text = "This segment is not present in the model" )

      else:
        info.field( name = "Mapped positions:", value = "%s-%s" % ( rstart, rend ) )
        segments.append( benchmark.Segment( begin = rstart, end = rend ) )

      info.list_item_end()

    info.list_end()

    model = benchmark.Model(
      name = "%s_%s" % ( ense.identifier(), replacement_for[ ense ][0] ),
      chain = replacement_for[ ense ][1].chain,
      selection = benchmark.Selection( segments = segments ),
      )
    length = len( model.aligned_residue_group_indices() )
    info.field( name = "Replacement length (residues)", value = length )

    fraclength = length / len( ense.model.aligned_residue_group_indices() )
    info.field_percentage(
      name = "Replacement length (fraction)",
      value = 100.0 * fraclength,
      digits = 0,
      )

    replacement_ensemble_for[ ense ] = (
      benchmark.Ensemble( model = model, error = replacement_for[ ense ][3] ),
      replacement_for[ ense ][2].matrix,
      )
    info.list_item_end()

  info.list_end()

  replaced_structure = benchmark.Structure( symmetry = structure.symmetry )

  for molecule in structure.molecules():
    if molecule.ensemble in replacement_ensemble_for:
      ( repl, transf ) = replacement_ensemble_for[ molecule.ensemble ]
      transformation = molecule.transformation() * transf
      replaced_mol = benchmark.Molecule(
        ensemble = repl,
        rotation = transformation.r.elems,
        translation = transformation.t.elems,
        bfactor = molecule.bfactor,
        )
      replaced_structure.add( molecule = replaced_mol )

    else:
      replaced_structure.add( molecule = molecule )

  info.preformatted_text( text = str( replaced_structure ) )

  prefix = "-".join(
    [ params.prefix, structure_prefix, "%sdoms" % len( replacement_for ) ]
    + [ "%s-with-%s" % ( ense.identifier(), replacement_for[ ense ][0] )
      for ense in replacement_for ]
    )[:200]
  outfile = "%s.structure" % prefix
  info.field( name = "Output file", value = outfile )
  info.task_begin( text = "Writing replaced output" )

  with open( outfile, "w" ) as ofile:
    pickle.dump( replaced_structure, ofile )

  info.task_end()

  if params.write_pdb_file:
    file_name = "%s.pdb" % prefix
    info.field( name = "Output PDB file", value = file_name )
    info.task_begin( text = "Writing PDB file" )
    replaced_structure.as_iotbx_hierarchy().write_pdb_file( file_name )
    info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return ( outfile, replacement_ensemble_for )


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
