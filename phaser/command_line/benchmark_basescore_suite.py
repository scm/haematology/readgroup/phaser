from __future__ import print_function

from phaser import benchmark


def get_parser():

  import argparse
  import sys

  parser = argparse.ArgumentParser()
  parser.add_argument(
    "suite",
    metavar = "SUITE_FILE",
    help = "Suite file containing tests to perform",
    )
  parser.add_argument(
    "--disable-bfactor-refinement",
    action = "store_false",
    dest = "bfactor",
    help = "Disable Bfactor refinement"
    )
  parser.add_argument(
    "--disable-vrms-refinement",
    action = "store_false",
    dest = "vrms",
    help = "Disable VRMS refinement"
    )
  parser.add_argument(
    "--engine",
    default = None,
    help = "PHIL file containing multiprocessing description"
    )
  parser.add_argument(
    "--outfile",
    type = argparse.FileType( "w" ),
    default = sys.stdout,
    help = "Output csv file"
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_basescore_suite"


def execute_test(
  test,
  replacements,
  bfactor,
  vrms,
  prefix,
  ):

  args = [ test, replacements ]

  if not bfactor:
    args.append( "--disable-bfactor-refinement" )

  if not vrms:
    args.append( "--disable-vrms-refinement" )

  if prefix is not None:
    args.extend( [ "--prefix", prefix ] )

  import os.path
  logname = "%s%s.log" % (
    "" if prefix is None else "%s-" % prefix,
    os.path.splitext( os.path.basename( test ) )[0]
    )

  from phaser.command_line import benchmark_basescore_test

  with open( logname, "w" ) as ofile, benchmark.get_logger( stream = ofile ) as logger:
    result = benchmark_basescore_test.run(
      params = parse(
        parser = benchmark_basescore_test.get_parser(),
        args = args,
        ),
      logger = logger,
      )

  return result


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params):

  creator = benchmark.get_engine_creator( filename = params.engine )

  import csv
  writer = csv.writer( params.outfile )
  writer.writerow( [ "Test", "Remainings", "Removeds", "LLG", "TFZ", "Atoms", "VRMS" ] )

  from libtbx.scheduling import holder
  from libtbx.scheduling import parallel_for
  from phaser.command_line import benchmark_basescore_suite

  import os.path
  dirname = os.path.dirname( params.suite )

  with open( params.suite ) as ifile, holder( creator = creator ) as manager:
    reader = csv.reader( ifile )

    pfi = parallel_for.iterator(
      calculations = (
        (
          benchmark_basescore_suite.execute_test,
          (),
          {
            "test": os.path.normpath( os.path.join( dirname, line[0] ) ),
            "replacements": line[1],
            "bfactor": params.bfactor,
            "vrms": params.vrms,
            "prefix": ( ( "" if params.prefix is None else params.prefix )
              + os.path.splitext( os.path.basename( line[0] ) )[0] ),
            }
            )
          for line in reader if line
        ),
      manager = manager,
      keep_input_order = True,
      )

    for ( calc, res ) in pfi:
      row = [ os.path.basename( calc[2][ "test" ] ) ]

      try:
        results = res()
        row.extend( results[2:7] )
        row.extend( sum( results[7:], () ) )

      except Exception as e:
        row.append( str( e ) )

      writer.writerow( row )

    manager.join()


if __name__ == "__main__":
  parser = get_parser()
  run( params = parser.parse_args() )
