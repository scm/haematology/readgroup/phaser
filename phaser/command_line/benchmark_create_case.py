from __future__ import print_function

from phaser import benchmark

"""
class ChainMatch(object):
  ""
  A match between two chains
  ""

  def __init__(self, rgs, shift, equivalentfetch):

    self.equivalentfetch = equivalentfetch
    self.shift = shift
    self.alignment = []

    for rg in rgs:
      try:
        refdata = self.equivalentfetch.equivalent_for( residue_group = rg, shift = shift )

      except KeyError:
        continue

      self.alignment.append( ( refdata, rg ) )

    self.identities = sum(
      1 for ( refdata, crg ) in self.alignment
      if refdata[0].atom_groups()[0].resname == crg.atom_groups()[0].resname
      )


  def identity_count(self):

    return self.identities


  def overlap_length(self):

    return len( self.alignment )


  def identity_fraction(self):

    length = self.overlap_length()

    if length == 0:
      return 0

    else:
      return self.identity_count() / length


  def matched_sites(self):

    sites = []

    for ( refdata, crg ) in self.alignment:
      if refdata[1] is None:
        continue

      try:
        catom = self.equivalentfetch.representative_atom_in( residue_group = crg )

      except benchmark.BenchmarkError:
        continue

      sites.append( ( refdata[1], catom.xyz ) )

    return sites


  def superposition(self, weights):

    matched_sites = self.matched_sites()
    from scitbx.array_family import flex

    from phaser import multiple_superposition
    return multiple_superposition.PairwiseSuperposition(
      reference_sites = flex.vec3_double( [ p[0] for p in matched_sites ] ),
      moving_sites = flex.vec3_double( [ p[1] for p in matched_sites ] ),
      weights = weights,
      )


class EquivalentFetch(object):
  ""
  Fetches the equivalent of a residue group based on a resid frame shift
  ""

  def __init__(self, rgs, representative_atom_name):

    self.representative_atom_name = representative_atom_name
    self.residue_group_named = {}

    for rg in rgs:
      try:
        xyz = self.representative_atom_in( residue_group = rg ).xyz

      except benchmark.BenchmarkError:
        xyz = None

      self.residue_group_named[ ( rg.resseq_as_int(), rg.icode ) ] = ( rg, xyz )


  def representative_atom_in(self, residue_group):

    for atom in residue_group.atoms():
      if atom.name == self.representative_atom_name:
        return atom

    raise benchmark.BenchmarkError, "No atom '%s' found" % self.representative_atom_name


  def equivalent_for(self, residue_group, shift):

    refid = ( residue_group.resseq_as_int() + shift, residue_group.icode )
    return self.residue_group_named[ refid ]


class FrameshiftFinder(object):
  ""
  Matches two chains
  ""

  def __init__(self, rgs, representative_atom_name, min_overlap):

    self.rgs = rgs
    self.min_overlap = min_overlap

    from phaser import chainalign
    self.matcher = chainalign.HSSFind(
      sequence = [ rg.atom_groups()[0].resname for rg in rgs ],
      )

    self.equivalent_fetch = EquivalentFetch(
      rgs = self.rgs,
      representative_atom_name = representative_atom_name,
      )


  def __call__(self, rgs):

    assert rgs
    matchiter = self.matcher.overlaps_with(
      sequence = [ rg.atom_groups()[0].resname for rg in rgs ],
      )

    ( length, seqstart, refstarts ) = max( matchiter, key = lambda ( l, ss, rs ): l )

    if length < self.min_overlap:
      raise benchmark.BenchmarkError, "Best overlap below minimum"

    assert refstarts
    chainmatchiter = (
      ChainMatch(
        rgs = rgs,
        shift = self.rgs[ rs ].resseq_as_int() - rgs[ seqstart ].resseq_as_int(),
        equivalentfetch = self.equivalent_fetch,
        )
      for rs in refstarts
      )
    best_match = chainmatchiter.next()

    for cmatch in chainmatchiter:
      if best_match.identity_count() < cmatch.identity_count():
        best_match = cmatch

    return best_match
"""

def resname_for(rg):

  return rg.atom_groups()[0].resname


class AtomSelector(object):
  """
  Selects atoms for superposition
  """

  def __init__(self, name):

    self.name = name


  def __call__(self, residue_group):

    for atom in residue_group.atoms():
      if atom.name == self.name:
        return atom

    raise benchmark.BenchmarkError("No atom '%s' found" % self.name)


def domain_superposition(residue_group_alignment, selector, weights):

  from scitbx.array_family import flex
  refsites = flex.vec3_double()
  movsites = flex.vec3_double()

  for ( l, r ) in residue_group_alignment:
    try:
      refxyz = selector( residue_group = l ).xyz
      movxyz = selector( residue_group = r ).xyz

    except benchmark.BenchmarkError:
      pass

    else:
      refsites.append( refxyz )
      movsites.append( movxyz )

  from phaser import multiple_superposition
  return multiple_superposition.PairwiseSuperposition(
    reference_sites = refsites,
    moving_sites = movsites,
    weights = weights,
    )


class Duplicate(object):
  """
  A duplicate of a domain
  """

  def __init__(self, domain, transformation, rmsd):

    self.domain = domain
    self.transformation = transformation
    self.rmsd = rmsd


def find_models_identical_to(
  reference,
  others,
  logger,
  min_overlap = 10,
  min_sequence_identity = 0.8,
  max_rmsd = 0.8,
  weighting_critical_value = 1.0,
  weighting_max_iterations = 15,
  weighting_max_displacement = 1E-3,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  import math
  from scitbx.array_family import flex
  from phaser import multiple_superposition
  weighting = multiple_superposition.RobustResistantWeightScheme(
    critical_value_square = weighting_critical_value ** 2,
    )

  info.unordered_list_begin( level = 2 )
  refpos = set( reference.aligned_residue_group_indices() )
  selector = AtomSelector(
    name = reference.chain.mmt().representative_mainchain_atom.pdb_name,
    )
  refrgs = reference.chain.aligned_residue_groups()
  identicals = []

  for trial in others:
    info.list_item_begin( text = "Trial: %s" % trial )

    if reference.chain.mmt() != trial.chain.mmt():
      info.unformatted_text( text = "Component with different type, skip" )
      info.list_item_end()
      continue

    if reference.chain.reference_sequence() != trial.chain.reference_sequence():
      info.unformatted_text( text = "Component with different sequence, skip" )
      info.list_item_end()
      continue

    trialrgs = trial.chain.aligned_residue_groups()
    overlaps = [ i for i in trial.aligned_residue_group_indices() if i in refpos ]
    info.field( name = "Overlap count", value = len( overlaps ) )

    if len( overlaps ) < min_overlap:
      info.unformatted_text( text = "Overlap below threshold, skip" )
      info.list_item_end()
      continue

    rgali = [ ( refrgs[ i ], trialrgs[ i ] ) for i in overlaps ]
    assert all( p[0] and p[1] for p in rgali )
    identities = sum(
      1 for ( l, r ) in rgali if resname_for( rg = l ) == resname_for( rg = r )
      )
    info.field( name = "Identity count", value = identities )
    idfrac = identities / len( overlaps )
    info.field_float( name = "Identity fraction", value = idfrac, digits = 2 )

    if idfrac < min_sequence_identity:
      info.unformatted_text( text = "Sequence identity below threshold, skip" )
      info.list_item_end()
      continue

    weights = flex.double( [ 1 ] * len( overlaps ) )
    sup = domain_superposition(
      residue_group_alignment = rgali,
      selector = selector,
      weights = weights )

    info.heading( text = "Superposition", level = 6 )
    info.ordered_list_begin()
    cycle = 0

    while cycle < weighting_max_iterations:
      info.list_item_begin( text = "cycle" )
      info.field_float( name = "Rmsd with reference chain", value = sup.rmsd, digits = 3 )
      new_weights = weighting.for_difference_squares(
        squares = sup.positional_error_squares,
        )
      displacement = math.sqrt( flex.mean_sq( new_weights - weights ) )
      info.field_float( name = "Displacement", value = displacement, digits = 3 )

      if displacement <= weighting_max_displacement:
        info.unformatted_text( text = "Convergence reached" )
        info.list_item_end()
        break

      try:
        sup.set_new_weights( weights = new_weights )

      except multiple_superposition.BadWeights:
        info.unformatted_text( text = "Excessive weight shift, skip iteration" )
        info.list_item_end()
        break

      weights = new_weights
      cycle += 1
      info.list_item_end()

    else:
      info.unformatted_text( text = "Number of cycles exceeded" )

    info.list_end()

    if max_rmsd < sup.rmsd:
      info.unformatted_text( text = "RMSD above threshold" )
      info.list_item_end()
      continue

    info.rt_matrix( rt = sup.rt )
    info.unformatted_text( text = "Accepting this chain" )
    identicals.append(
      Duplicate( domain = trial, transformation = sup.rt, rmsd = sup.rmsd )
      )
    info.list_item_end()

  info.list_end()

  return identicals


def get_structure(
  pdbid,
  composition,
  pdb_accessor,
  cath_accessor,
  min_overlap,
  min_sequence_identity,
  max_rmsd,
  critical_value,
  max_weight_iterations,
  max_weight_displacement,
  minimum_rms,
  domain_annotation_file,
  logger,
  ):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.heading( text = "Structure analysis", level = 1 )
  info.task_begin( text = "Getting structure" )
  data = pdb_accessor( pdbid )
  info.task_end()

  info.task_begin( text = "Parsing" )
  import iotbx.pdb
  pdbinp = iotbx.pdb.input( lines = data, source_info = "pdb" )
  root = pdbinp.construct_hierarchy()
  info.task_end()

  from libtbx.utils import Sorry

  if len( root.models() ) != 1:
    raise Sorry("Multimodel structures are not suitable")

  from phaser import tbx_utils
  info.task_begin( text = "Splitting chains" )
  nchains = tbx_utils.split_all_chains_in( root = root )
  info.task_end()
  info.field( name = "New chains introduced", value = nchains )
  info.field( name = "Number of chains", value = len( root.models()[0].chains() ) )

  from phaser import mmtype
  mytypes = [ mmtype.PROTEIN, mmtype.DNA, mmtype.RNA ]
  info.heading( text = "Chain analysis", level = 2 )
  info.unordered_list_begin()
  chains = []

  for chain in root.models()[0].chains():
    annotation = tbx_utils.ChainAnnotation.from_chain( chain = chain )
    info.list_item_begin( text = str( annotation ) )
    mmt = mmtype.determine( chain = chain, mmtypes = mytypes )
    info.field( name = "Type", value = mmt.name )

    if mmt == mmtype.UNKNOWN:
      info.unformatted_text( text = "Discarding this chain" )
      info.list_item_end()
      continue

    try:
      ( component, match ) = composition.matching_component_to(
        chain = chain,
        mmt = mmt,
        )

    except benchmark.BenchmarkError:
      raise Sorry("Could not match chain %s to component" % chain.id)

    info.field( name = "Sequence component", value = str( component ) )
    info.field( name = "Unaligneds", value = len( match.unaligneds ) )
    info.field( name = "Identities", value = match.identities )
    info.field(
      name = "Differences",
      value = len( chain.residue_groups() ) - match.identities,
      )

    cobj = benchmark.Chain.from_chain(
      chain = chain,
      mmt_name = mmt.name,
      reference_sequence = component.sequence_string(),
      aligned_residues = match.residue_alignment(),
      annotation = annotation,
      origin = pdbid,
      )
    chains.append( cobj )
    info.list_item_end()

  info.list_end()

  info.heading( text = "Domain analysis", level = 2 )

  from phaser.pipeline.proxy import domain
  domain_data = None

  if domain_annotation_file is None:
    from iotbx.pdb import download
    info.task_begin( text = "Getting domain annotation from CATH" )

    try:
      domain_data = cath_accessor( pdbid )

    except download.NotFound:
      info.task_end( text = "not found" )
      info.highlight( text = "Each chain will be considered a single domain" )

    else:
      info.task_end()

  else:
    import json
    info.task_begin( text = "Reading domain annotation from %s" % domain_annotation_file )

    try:
      with open( domain_annotation_file ) as ifile:
        domain_data = json.load( ifile )

    except IOError:
      info.task_end( text = "failed" )
      info.highlight( text = "Each chain will be considered a single domain" )

    else:
      info.task_end()

  domain_annotation_for = {}

  if domain_data is not None:
    for ann in domain.annotation_from_pdbe_api_format(
      data_for = domain_data,
      database = domain.CATH,
      ):
      domain_annotation_for.setdefault( ann.chain, [] ).append( ann )

  from phaser import sequtil
  info.unordered_list_begin()
  models = []

  for chain in chains:
    info.list_item_begin( text = str( chain ) )
    chainid = chain.identifier()

    if chainid in domain_annotation_for:
      annotations = domain_annotation_for[ chain.identifier() ]
      info.field( name = "Number of domains", value = len( annotations ) )
      resid_indexer = chain.resid_to_residue_indexer()
      refpos_mapper = sequtil.SegmentMapping(
        indexer = chain.residue_to_reference_position_indexer(),
        )
      aligned_residues = chain.aligned_residue_groups()
      info.unordered_list_begin( level = 2 )

      for ( index, domain ) in enumerate( annotations, start = 1 ):
        info.list_item_begin( text = "domain %s" % index )
        info.ordered_list_begin()
        segments = []

        for rgs in resid_indexer( domain = domain ):
          info.list_item_begin( text = "segment" )
          info.field(
            name = "Residue range (resid)",
            value = "'%s'-'%s'" % ( rgs[0].resid(), rgs[-1].resid() ),
            )
          ( begin, end ) = refpos_mapper( segment = rgs )
          info.field(
            name = "Mapped sequence range (sequence index)",
            value = "%s-%s" % ( begin + 1, end + 1 ),
            )
          begin_res = aligned_residues[ begin ]
          end_res = aligned_residues[ end ]
          assert begin_res
          assert end_res
          info.field(
            name = "Mapped residues (resid)",
            value = "'%s'-'%s'" % ( begin_res.resid(), end_res.resid() ),
            )
          segments.append( benchmark.Segment( begin = begin, end = end ) )
          info.list_item_end()

        info.list_end()

        models.append(
        benchmark.Model(
          name = "%s_%s_d%s" % ( pdbid, chainid, index ),
          chain = chain,
          selection = benchmark.Selection( segments = segments ),
          )
        )
        info.list_item_end()

      info.list_end()

    else:
      info.highlight( text = "No annotation, assuming single domain" )
      models.append(
        benchmark.Model(
          name = "%s_%s" % ( pdbid, chainid ),
          chain = chain,
          selection = chain.whole(),
          )
        )

    info.list_item_end()

  info.list_end()

  info.field( name = "Number of models", value = len( models ) )

  info.heading( text = "Identifying identical models", level = 2 )
  from collections import OrderedDict
  identicals_to = OrderedDict()
  info.unordered_list_begin()

  while models:
    reference = models[0]
    info.list_item_begin( text = "Reference: %s" % reference )

    identicals_to[ reference ] = find_models_identical_to(
      reference = reference,
      others = models[1:],
      logger = logger,
      min_overlap = min_overlap,
      min_sequence_identity = min_sequence_identity,
      max_rmsd = max_rmsd,
      weighting_critical_value = critical_value,
      weighting_max_iterations = max_weight_iterations,
      weighting_max_displacement = max_weight_displacement,
      )
    assigneds = set( dupl.domain for dupl in identicals_to[ reference ] )
    assigneds.add( reference )
    models = [ m for m in models if m not in assigneds ]
    info.list_item_end()

  info.list_end()

  structure = benchmark.Structure( symmetry = pdbinp.crystal_symmetry() )
  info.heading( text = "Structure assembly", level = 3 )
  info.unordered_list_begin()

  for ( reference, others ) in identicals_to.items():
    info.list_item_begin( text = str( reference ) )
    ensemble = benchmark.Ensemble.Rms(
      model = reference,
      rmsd = max( [ minimum_rms ] + [ o.rmsd for o in others ] ),
      )
    info.field( name = "Ensemble", value = ensemble.identifier() )
    info.ordered_list_begin( separate_items = False )
    molecule = benchmark.Molecule(
      ensemble = ensemble,
      rotation = ( 1, 0, 0, 0, 1, 0, 0, 0, 1 ),
      translation = ( 0, 0, 0 ),
      bfactor = 0,
      )

    try:
      structure.add( molecule = molecule )

    except benchmark.BenchmarkError as e:
      raise Sorry(e)

    info.list_item( text = str( molecule ) )

    for o in others:
      invt = o.transformation.inverse()
      molecule = benchmark.Molecule(
        ensemble = ensemble,
        rotation = invt.r.elems,
        translation = invt.t.elems,
        bfactor = 0,
        )
      structure.add( molecule = molecule )
      info.list_item( text = str( molecule ) )

    info.list_end()
    info.list_item_end()

  info.list_end()

  info.heading( text = "Structure:", level = 3 )
  info.preformatted_text( text = str( structure ) )

  return structure


def get_reflections(pdbid, composition, accessor, mtzdir, extend_rfree, merge_equivalents, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.heading( text = "Reflection data analysis", level = 1 )

  import os.path
  mtz_file = os.path.join( mtzdir, "%s.mtz" % pdbid )

  if not os.path.exists( mtz_file ):
    info.task_begin( text = "Getting reflection file" )
    data = accessor( pdbid )
    info.task_end()

    import tempfile
    ( fd, tmpfile ) = tempfile.mkstemp()

    import os
    ofile = os.fdopen( fd, "w" )
    ofile.write( data )
    ofile.close()

    from mmtbx.command_line import cif_as_mtz
    args = [ tmpfile, "--output-file-name=%s" % mtz_file ]

    if extend_rfree:
      args.append( "--extend-flags" )

    if merge_equivalents:
      args.append( "--merge" )

    cif_as_mtz.run( args )
    os.remove( tmpfile )

  from libtbx.utils import Sorry
  from iotbx import reflection_file_reader
  mtz = reflection_file_reader.any_reflection_file( mtz_file )
  arrays = mtz.as_miller_arrays()
  first_amplitude_array = None
  info.heading( text = "Reflection data arrays:", level = 4 )
  info.ordered_list_begin()

  for a in arrays:
    info.list_item_begin( text = "Labels: %s" % a.info().labels )

    if a.anomalous_flag():
      info.unformatted_text( text = "Anomalous array, discarded" )
      info.list_item_end()
      continue

    if a.is_xray_intensity_array():
      info.unformatted_text(
        text = "Intensity array, accepted - terminate search",
        )
      ( i, sigi ) = a.info().labels
      info.field( name = "I", value = "'%s'" % i )
      info.field( name = "SIGI", value = "'%s'" % sigi )
      data = benchmark.Data.intensities_from_mtz(
        filename = mtz_file,
        i = i,
        sigi = sigi,
        )
      info.list_item_end()
      info.list_end()
      break

    if first_amplitude_array is None and a.is_xray_amplitude_array():
      info.unformatted_text(
        text = "Amplitude array - will be used if no intensities are available",
        )
      first_amplitude_array = a

    info.list_item_end()

  else:
    info.list_end()

    if first_amplitude_array is None:
      raise Sorry("No X-ray data found in %s" % mtz_file)

    info.unformatted_text( text = "Using marked amplitude array" )
    ( fp, sigfp ) = first_amplitude_array.info().labels
    info.field( name = "FP", value = "'%s'" % fp )
    info.field( name = "SIGFP", value = "'%s'" % sigfp )
    data = benchmark.Data.structure_factors_from_mtz(
      filename = mtz_file,
      fp = fp,
      sigfp = sigfp,
      )

  info.preformatted_text( text = str( data ) )
  info.task_begin( text = "Doing anisotropy correction" )
  anocorr = benchmark.AnisotropyCorrection.from_dataset( dataset = data )
  info.task_end()
  info.preformatted_text( text = str( anocorr ) )

  info.task_begin( text = "Doing TNCS analysis" )
  tncscorr = benchmark.Tncs.from_dataset(
    dataset = data,
    aniso = anocorr,
    composition = composition,
    )
  info.task_end()
  info.preformatted_text( text = str( tncscorr ) )

  return data, ( anocorr, tncscorr )


def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "pdbid",
    metavar = "PDBID",
    help = "Target structure PDB identifier",
    )

  parser.add_argument(
    "--pdb-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded PDB files",
    )
  parser.add_argument(
    "--seq-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded FASTA files",
    )
  parser.add_argument(
    "--cath-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded CATH files",
    )
  parser.add_argument(
    "--cif-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded cif files",
    )
  parser.add_argument(
    "--mtz-cache-dir",
    metavar = "PATH",
    default = ".",
    help = "Directory to cache downloaded cif files",
    )
  parser.add_argument(
    "--pdb-mirror",
    action = "store",
    choices = [ "rcsb", "pdbe", "pdbj" ],
    default = "pdbe",
    help = "Mirror to download PDB files"
    )
  parser.add_argument(
    "--min-overlap",
    type = int,
    default = 10,
    help = "Minimum sequence overlap between identical chains",
    )
  parser.add_argument(
    "--min-sequence-identity",
    type = float,
    default = 0.9,
    help = "Minimum sequence identity between identical chains",
    )
  parser.add_argument(
    "--max-rmsd",
    type = float,
    default = 1.0,
    help = "Max rmsd between identical chains",
    )
  parser.add_argument(
    "--weighting-critical-value",
    type = float,
    default = 2.0,
    help = "Critical value for robust-resistant weighting scheme",
    )
  parser.add_argument(
    "--max-weight-iterations",
    type = int,
    default = 15,
    help = "Max weight iteration cycles",
    )
  parser.add_argument(
    "--max-weight-displacement",
    type = float,
    default = 1E-3,
    help = "Max weight displacement",
    )
  parser.add_argument(
    "--extend-rfree-flags",
    action = "store_true",
    help = "Extend Rfree flags to full resolution",
    )
  parser.add_argument(
    "--merge-equivalent-reflections",
    action = "store_true",
    help = "Merge symmetry equivalent reflections",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = None,
    help = "Prefix for output data",
    )
  parser.add_argument(
    "--write-reconstructed-pdb-file",
    action = "store_true",
    help = "Write reconstructed structure as pdb",
    )
  parser.add_argument(
    "--minimum-rms",
    type = float,
    default = 0.4,
    help = "Minimum rms for ensembles"
    )
  parser.add_argument(
    "--domain-annotation",
    type = str,
    default = None,
    help = "Domain annotations (EBI JSON format)",
    )

  return parser

PROGRAM = "benchmark_create_case"


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  if params.prefix is None:
    params.prefix = params.pdbid

  composition = benchmark.get_composition(
    pdbid = params.pdbid,
    accessor = benchmark.get_pdb_sequence_accessor(
      mirror = params.pdb_mirror,
      cache_dir = params.seq_cache_dir,
      logger = logger,
      ),
    logger = logger,
    )

  ( data, corrections ) = get_reflections(
    pdbid = params.pdbid,
    composition = composition,
    accessor = benchmark.get_pdb_reflection_accessor(
      mirror = params.pdb_mirror,
      cache_dir = params.cif_cache_dir,
      logger = logger,
      ),
    mtzdir = params.mtz_cache_dir,
    extend_rfree = params.extend_rfree_flags,
    merge_equivalents = params.merge_equivalent_reflections,
    logger = logger,
    )

  case = benchmark.Case(
    composition = composition,
    data = data,
    corrections = corrections,
    )

  import pickle
  case_file_name = "%s.case" % params.prefix
  info.task_begin( text = "Saving compostion and reflections to %s" % case_file_name )

  with open( case_file_name, "w" ) as ofile:
    pickle.dump( case, ofile )

  info.task_end()

  structure = get_structure(
    pdbid = params.pdbid,
    composition = composition,
    pdb_accessor = benchmark.get_pdb_structure_accessor(
      mirror = params.pdb_mirror,
      cache_dir = params.pdb_cache_dir,
      logger = logger,
      ),
    cath_accessor = benchmark.get_pdbe_cath_accessor(
      cache_dir = params.cath_cache_dir,
      logger = logger,
      ),
    min_overlap = params.min_overlap,
    min_sequence_identity = params.min_sequence_identity,
    max_rmsd = params.max_rmsd,
    critical_value = params.weighting_critical_value,
    max_weight_iterations = params.max_weight_iterations,
    max_weight_displacement = params.max_weight_displacement,
    minimum_rms = params.minimum_rms,
    domain_annotation_file = params.domain_annotation,
    logger = logger,
    )

  structure_file_name = "%s.structure" % params.prefix
  info.task_begin( text = "Saving structure to %s" % structure_file_name )

  with open( structure_file_name, "w" ) as ofile:
    pickle.dump( structure, ofile )

  info.task_end()

  if params.write_reconstructed_pdb_file:
    pdb_file_name = "%s-reconstructed.pdb" % params.prefix
    info.task_begin( text = "Saving reconstructed pdb file to %s" % pdb_file_name )
    root = structure.as_iotbx_hierarchy()
    root.write_pdb_file( pdb_file_name )
    info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return ( case_file_name, structure_file_name )


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
