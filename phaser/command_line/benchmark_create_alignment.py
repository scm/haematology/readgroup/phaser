from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "target",
    metavar = "FILENAME",
    help = "Model structure PDB code",
    )
  parser.add_argument(
    "chainid",
    metavar = "CHAINID",
    help = "Chain ID of model",
    )
  parser.add_argument(
    "model",
    metavar = "FILENAME",
    help = "Model chain",
    )
  parser.add_argument(
    "--file-name",
    type = str,
    default = None,
    help = "File name for output alignment",
    )

  return parser

PROGRAM = "benchmark_create_alignment"


def file_name(prefix, target, model):

  return "%s-%s-vs-%s.aln" % ( prefix, target, model )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )
  info.task_begin( text = "Reading input files" )
  import pickle
  from libtbx.utils import Sorry

  try:
    with open( params.target ) as ifile:
      structure = pickle.load( ifile )

    with open( params.model ) as ifile:
      model = pickle.load( ifile )

  except IOError as e:
    info.task_failure( text = "failed" )
    raise Sorry(e)

  info.task_end()

  chain_dict = {}

  for ense in structure.get_ensembles():
    parentid = ense.model.chain.identifier()

    if parentid in chain_dict:
      if chain_dict[ parentid ] != ense.model.chain:
        raise Sorry("Multiple chains with id %s" % parentid)

    else:
      chain_dict[ parentid ] = ense.model.chain

  if params.chainid not in chain_dict:
    raise Sorry("Chain '%s' unknown (knowns: %s)" % (
      params.chainid,
      " ".join(chain_dict),
    ))
  target_chain = chain_dict[ params.chainid ]
  model_chain = model.chain

  if target_chain.mmt() != model_chain.mmt():
    raise Sorry("Different macromolecules cannot be aligned")

  from iotbx import bioinformatics
  targetid = "%s_%s" % ( target_chain.origin(), target_chain.identifier() )
  modelid = model.identifier()
  assert None not in target_chain.reference_sequence()
  assert None not in model_chain.reference_sequence()
  target_seq = bioinformatics.sequence(
    sequence = "".join( target_chain.reference_sequence() ),
    name = targetid,
    )
  model_seq = bioinformatics.sequence(
    sequence = "".join( model_chain.reference_sequence() ),
    name = modelid,
    )

  info.heading( text = "Target sequence:", level = 4 )
  info.sequence( sequence = target_seq )

  info.heading( text = "Model sequence:", level = 4 )
  info.sequence( sequence = model_seq )

  info.task_begin( text = "Aligning sequences" )

  from mmtbx import msa
  alignment = msa.get_muscle_alignment_ordered( sequences = [ target_seq, model_seq ] )
  info.task_end()

  info.heading( text = "Alignment:", level = 4 )
  info.alignment( alignment = alignment )

  if not params.file_name:
    params.file_name = file_name(
      prefix = "alignment",
      target = target_seq.name,
      model = model_seq.name,
      )

  info.task_begin( text = "Writing alignment to %s" % params.file_name )

  with open( params.file_name, "w" ) as ofile:
    ofile.write( str( alignment ) )
    ofile.write( "\n" )

  info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return params.file_name


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
