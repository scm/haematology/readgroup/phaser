from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "model",
    metavar = "FILENAME",
    help = "Model chain file",
    )
  parser.add_argument(
    "phils",
    nargs = "+",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL argument (file name or PHIL command line assignment)",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = "sculpt",
    help = "Prefix for output data",
    )
  parser.add_argument(
    "--write-pdb-file",
    action = "store_true",
    help = "Write sculpted PDB file",
    )
  parser.add_argument(
    "--error-file",
    default = None,
    help = "Error file for model",
    )

  return parser

PROGRAM = "benchmark_sculpt_model"


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )
  info.task_begin( text = "Reading input files" )
  import pickle
  from libtbx.utils import Sorry

  try:
    with open( params.model ) as ifile:
      model = pickle.load( ifile )

  except IOError as e:
    info.task_failure( text = "failed" )
    raise Sorry(e)

  info.task_end()

  from phaser import mmtype

  if model.chain.mmt() != mmtype.PROTEIN:
    raise Sorry("Model chain is not protein")

  from phaser import cli_new as cli
  from phaser import sculptor_new as sculptor

  from iotbx import bioinformatics

  parser = cli.PCLIParser(
    master_phil = sculptor.parsed_master_phil(),
    description = "Apply user-defined protocol to modify search model",
    )
  parser.recognize_file_type_as(
    extensions = bioinformatics.known_alignment_formats(),
    philpath = "input.alignment.file_name",
    )
  parser.recognize_file_type_as(
    extensions = [ ".pdb", ".ent" ],
    philpath = "input.model.file_name",
    )
  parser.recognize_file_type_as(
    extensions = [ ".hhr", ".xml" ],
    philpath = "input.homology_search.file_name",
    )

  info.heading( text = "Sculptor parameters", level = 2 )
  sculptor_cli_params = parser.parse_args( args = params.phils )
  info.preformatted_text( text = sculptor_cli_params.phil.as_str() )
  phil_params = sculptor_cli_params.phil.extract()

  # Read in alignments
  try:
    alignments = sculptor.read_in_alignment_files(
      params = phil_params.input.alignment,
      logger = logger,
      )

  except sculptor.AlignmentReadError as e:
    raise Sorry("Error while reading alignment: %s" % e)

  # Read in homology_search files
  alignments.extend(
    sculptor.read_in_homology_search_files(
      params = phil_params.input.homology_search,
      logger = logger,
      )
    )

  # Read in error files if present
  root = model.iotbx_root()
  chain = root.only_chain()

  if params.error_file:
    info.heading( text = "Structure error file", level = 2 )
    info.field( name = "Filename", value = params.error_file )

    try:
      errdata = sculptor.preprocess_error_file(
        filename = params.error_file,
        logger = logger,
        )

    except IOError as e:
      raise Sorry(e)

    chainid = model.chain.identifier()
    info.field( name = "Associated chain", value = model.chain.identifier() )
    error_params = phil_params.error_search
    errors = sculptor.find_errors_for(
      chain = chain,
      errors_for = { chainid: errdata },
      min_identity = error_params.min_sequence_identity,
      min_fraction = error_params.min_sequence_overlap,
      stage = "rms",
      logger = logger,
      )

  else:
    errors = None

  # Create processor objects
  processor_for = sculptor.get_processor_for( params = phil_params.sculpt )

  from phaser import chisellib

  # Do processing
  info.heading( text = "Sculpting", level = 1 )
  root = model.iotbx_root()
  chain = root.only_chain()
  modelinfo = chisellib.ModelInfo( chains = [ chain ] )

  info.field( name = "Model", value = model.identifier() )
  info.field( name = "Type", value = model.chain.mmt() )

  info.heading( text = "Alignment search", level = 3 )
  name = model.identifier()
  # Ignore error search
  ( chainalignment, error_search_for ) = sculptor.create_chain_alignment(
    name = name,
    chain = chain,
    mmt = model.chain.mmt(),
    sequence_for = {},
    chainalign_params = phil_params.chain_to_alignment_matching,
    alignments = alignments,
    logger = logger,
    )

  sample = chisellib.Sample(
    name = name,
    chain = chain,
    mmtype = model.chain.mmt(),
    chainalignment = chainalignment,
    errors = errors,
    )

  info.heading( text = "Chain processing", level = 3 )
  processor = processor_for.get( sample.mmtype, chisellib.Discarder )
  info.task_begin( text = "Running processor for '%s'" % sample.mmtype.name )
  chaindiff = processor( sample = sample, model_info = modelinfo )
  info.task_end()

  modelinfo.register_change( chaindiff = chaindiff )

  info.heading( text = "Modifications", level = 3 )
  count = sculptor.ChaindiffProcessor.log_and_execute(
    chaindiff = chaindiff,
    channel = facade.Package( channel = logger.verbose ),
    simplify = True,
    )
  info.field( name = "Number of modifications", value = count )

  # Output
  from phaser import tbx_utils
  info.heading( text = "Output", level = 1 )

  info.task_begin( text = "Regularizing hierarchy" )
  tbx_utils.regularize_hierarchy( root = root )
  info.task_end()

  residue_sequence = [
    rg if rg is None or rg.parent() is not None else None for rg in chainalignment.residue_sequence()
    ]
  assert chain.residue_groups() == [ rg for rg in residue_sequence if rg is not None ]
  chainobj = benchmark.Chain.from_chain(
    chain = chain,
    mmt_name = model.chain.mmt().name,
    reference_sequence = chainalignment.target_sequence(),
    aligned_residues = residue_sequence,
    annotation = "sculpted from %s" % model.chain,
    origin = model.chain.origin(),
    )
  sculpted_chain = benchmark.SculptedChain(
    chain = chainobj,
    identity = chainalignment.alignment_identity_fraction(),
    )
  if not params.prefix:
    params.prefix = "sculpt-%s" % model.identifier()

  file_name = "%s.sculpted_model" % params.prefix
  info.field( name = "Output file", value = file_name )
  info.task_begin( text = "Writing output file" )

  with open( file_name, "w" ) as ofile:
    pickle.dump( sculpted_chain, ofile )

  info.task_end()

  if params.write_pdb_file:
    pdb_file_name = "%s.pdb" % params.prefix
    info.field( name = "Output PDB file", value = pdb_file_name )
    info.task_begin( text = "Writing output PDB file" )
    chainobj.iotbx_root().write_pdb_file( pdb_file_name )
    info.task_end()

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return file_name


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
