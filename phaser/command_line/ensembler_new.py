from __future__ import print_function

from phaser import ensembler_new as ensembler
from phaser import cli_new as cli

from iotbx import bioinformatics

parser = cli.PCLIParser(
  master_phil = ensembler.parsed_master_phil(),
  enable_text_logfile = True,
  enable_html_logfile = True,
  description = "Create ensemble by superposing chains",
  )
parser.add_argument(
  "-v", "--verbosity",
  action = "store",
  choices = ensembler.LOGGING_LEVELS,
  default = "info",
  help = "verbosity level"
  )
parser.add_argument(
  "--version",
  action = "version",
  version = "%s %s" % ( ensembler.PROGRAM, ensembler.VERSION )
  )
parser.recognize_file_type_as(
  extensions = [ ".pdb", ".ent" ],
  philpath = "input.model.file_name",
  )
parser.recognize_file_type_as(
  extensions = bioinformatics.known_alignment_formats(),
  philpath = "configuration.alignment",
  )

params = parser.parse_args()

# Check parameters
ensembler.validate_params( params = params.phil.extract() )


import sys
from phaser import logoutput_new as logoutput
from phaser.logoutput_new import console
from libtbx.utils import Sorry

handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]

if params.text_logfile is not None:
  handlers.append(
    console.Handler.FromDefaults(
      stream = params.text_logfile,
      width = 80,
      close_stream = True,
      )
    )

if params.html_logfile is not None:
  from phaser.logoutput_new import xhtml
  handlers.append(
    xhtml.Handler.FromDefaults(
      stream = params.html_logfile,
      title = ensembler.PROGRAM,
      close_stream = True,
      )
    )

with logoutput.Writer( \
  handlers = handlers, \
  level = ensembler.LOGGING_LEVELS[ params.verbosity ], \
  close_handlers = True,
  **ensembler.LOGGING_LEVELS \
  ) \
  as root:

  try:
    with logoutput.Forwarder( loggers = [ root ], shift = 0, **ensembler.LOGGING_LEVELS ) \
      as logger:

      ensembler.run( phil = params.phil, logger = logger )

  except Sorry as e:
    raise

  except Exception:
    from phaser.logoutput_new import facade
    warn = facade.Package( channel = root.warn )
    warn.highlight( text = "An error occurred" )

    ( exc_type, exc_value, tb ) = sys.exc_info()
    warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
    raise
