from __future__ import print_function

from phaser import tbx_utils
from phaser.logoutput_new import facade

class HomologyModellingError(Exception):
  """
  Module exception
  """

class UnsupportedMMTypeError(HomologyModellingError):
  """
  The type of the macromolecule is not supported
  """


class ChainDataError(HomologyModellingError):
  """
  Error in ChainData
  """


class SegmentError(HomologyModellingError):
  """
  Invalid segment
  """


class SetupError(HomologyModellingError):
  """
  Error in modelling setup
  """


class BeforeLoop(object):

  def __init__(self, segment):

    assert segment.is_residue_segment()
    self.segment = segment


  def __str__(self):

    return "<- %s" % self.segment.end


class AfterLoop(object):

  def __init__(self, segment):

    assert segment.is_residue_segment()
    self.segment = segment


  def __str__(self):

    return "%s ->" % self.segment.start


class LoopTooLong(SetupError):
  """
  A loop too long for modelling
  """

  def __init__(self, loop, possibilities = []):

    self.loop = loop
    self.possibilities = possibilities


  def __str__(self):

    return "%s: loop longer than maximum allowed" % self.loop


class SculptorError(HomologyModellingError):
  """
  Error occurred in sculptor
  """


class RosettaError(HomologyModellingError):
  """
  Error occurred in Rosetta
  """


def distance(left, right):

  import math
  ( x1, y1, z1 ) = left
  ( x2, y2, z2 ) = right
  return math.sqrt( ( x1 - x2 ) ** 2 + ( y1 - y2 ) ** 2 + ( z1 - z2 ) ** 2 )


class ChainPosition(object):

  def __init__(self, index, rescode, residue):

    self.index = index
    self.rescode = rescode
    self.residue = residue


  def identifier(self):

    return "%s %s" % ( self.index, self.rescode )


  def __nonzero__(self):

    return self.residue is not None


  def __cmp__(self, other):
    cd = lambda x, y: (x > y) - (x < y)

    return cd( self.index, other.index )

  def __lt__(self, other): return self.__cmp__(other) < 0
  def __le__(self, other): return self.__cmp__(other) <= 0
  def __eq__(self, other): return self.__cmp__(other) == 0
  def __ne__(self, other): return not self.__eq__(other)
  def __gt__(self, other): return self.__cmp__(other) > 0
  def __ge__(self, other): return self.__cmp__(other) >= 0

  def __sub__(self, other):

    return self.index - other.index + 1


  def __str__(self):

    return "%s %s" % ( self.identifier(), "P" if bool( self ) else "M" )


class ChainData(object):

  ATOMNAME = " CA "

  def __init__(self, positions):

    self.positions = positions
    self.index_for = dict( ( e, i ) for ( i, e ) in enumerate( self.positions ) )
    assert len( self.index_for ) == len( self.positions )


  def before(self, position, count = 1):

    index = self.index_for[ position ] - count

    if index < 0:
      raise ChainDataError("Requested position before start")

    return self.positions[ index ]


  def after(self, position, count = 1):

    index = self.index_for[ position ] + count

    if len( self.positions ) <= index:
      raise ChainDataError("Requested position after end")

    return self.positions[ index ]


  def first_present_backwards_from(self, position):

    index = self.index_for[ position ]

    while 0 <= index:
      posi = self.positions[ index ]

      if posi:
        return posi

      index -= 1

    raise ChainDataError("No present positions before %s" % position)


  def first_present_forwards_from(self, position):

    length = len( self.positions )
    index = self.index_for[ position ]

    while index < length:
      posi = self.positions[ index ]

      if posi:
        return posi

      index += 1

    raise ChainDataError("No present positions %s after %s" % position)


  def first_present_with_atom_backwards_from(self, position, atomname):

    index = self.index_for[ position ]

    while 0 <= index:
      posi = self.positions[ index ]

      if posi:
        try:
          atom = tbx_utils.get_atom_by_name(
            atoms = posi.residue.atoms(),
            name = atomname,
            )

        except KeyError:
          pass

        else:
          return ( posi, atom )

      index -= 1

    raise ChainDataError("No atom %s present in positions before %s" % (
      atomname,
      position,
    ))


  def first_present_with_atom_forwards_from(self, position, atomname):

    length = len( self.positions )
    index = self.index_for[ position ]

    while index < length:
      posi = self.positions[ index ]

      if posi:
        try:
          atom = tbx_utils.get_atom_by_name(
            atoms = posi.residue.atoms(),
            name = atomname,
            )

        except KeyError:
          pass

        else:
          return ( posi, atom )

      index += 1

    raise ChainDataError("No atom %s present in positions after %s" % (
      atomname,
      position,
    ))


  def first(self):

    return self.positions[0]


  def last(self):

    return self.positions[-1]


  def distance(self, left, right):

    return self.index_for[ right ] - self.index_for[ left ]


  def segment(self, start, end):

    return (
      self.positions[ i ] for i
      in range( self.index_for[ start ], self.index_for[ end ] + 1 )
      )


  def internal_loop_entry(self, start, end, margin, interdistance):

    assert start <= end

    while True:
      try:
        ( posi_before, atom_before ) = self.first_present_with_atom_backwards_from(
          position = self.before( position = start, count = margin ),
          atomname = self.ATOMNAME,
          )

      except ChainDataError:
        return self.nterminal_loop_entry( end = end )

      try:
        ( posi_after, atom_after ) = self.first_present_with_atom_forwards_from(
          position = self.after( position = end, count = margin ),
          atomname = self.ATOMNAME,
          )

      except ChainDataError:
        return self.cterminal_loop_entry( start = start )

      spatial = distance( left = atom_before.xyz, right = atom_after.xyz )
      sequential = self.distance( left = posi_before, right = posi_after )

      if sequential * interdistance < spatial:
        margin += 1
        continue

      return InternalLoop( start = posi_before, end = posi_after )


  def nterminal_loop_entry(self, end):

    try:
      first = self.first_present_forwards_from( position = end )
      before = self.before( position = first )

    except ChainDataError:
      raise SetupError("Gap open ended on both sides")

    return NTerminalLoop( end = before )


  def cterminal_loop_entry(self, start):

    try:
      first = self.first_present_backwards_from( position = start )
      after = self.after( position = first )

    except ChainDataError:
      raise SetupError("Gap open ended on both sides")

    return CTerminalLoop( start = after )


  def __iter__(self):

    return iter( self.positions )


class GapSegment(object):

  def __init__(self, segment):

    assert segment
    assert not segment[0]
    assert not segment[-1]

    self.segment = segment


  def __call__(self, chaindata, margin, interdistance):

    return chaindata.internal_loop_entry(
      start = self.segment[0],
      end = self.segment[-1],
      margin = margin,
      interdistance = interdistance,
      )


  def __str__(self):

    if len( self.segment ) == 1:
      return "%s GAP" % self.segment[0].identifier()

    else:
      return "%s -> %s GAP" % (
        self.segment[0].identifier(),
        self.segment[-1].identifier(),
        )


class ResidueSegment(object):

  def __init__(self, segment):

    assert segment
    assert segment[0]
    assert segment[-1]
    self.segment = segment


  def __call__(self, chaindata, margin, interdistance):

    return Segment( start = self.segment[0], end = self.segment[-1] )


  def __str__(self):

    if len( self.segment ) == 1:
      return "%s SEGMENT" % self.segment[0].identifier()

    else:
      return "%s -> %s SEGMENT" % (
        self.segment[0].identifier(),
        self.segment[-1].identifier(),
        )


class InterSegmentGap(object):

  def __init__(self, previous, following):

    assert previous
    assert following

    self.previous = previous
    self.following = following


  def __call__(self, chaindata, margin, interdistance):

    return chaindata.internal_loop_entry(
      start = self.previous,
      end = self.following,
      margin = margin - 1,
      interdistance = interdistance,
      )


  def __str__(self):

    return "INTERSEGMENT GAP: %s <-> %s" % (
      self.previous.identifier(),
      self.following.identifier(),
      )


class GapState(object):
  """
  Missing residues
  Note this is a singleton
  """

  @classmethod
  def initialize(cls, segmenter, elem):

    assert segmenter.state is None
    segmenter.state = cls
    segmenter.state_segment = [ elem ]

  @classmethod
  def register_next_gap(cls, segmenter, elem):

    segmenter.state_segment.append( elem )


  @classmethod
  def register_next_elem(cls, segmenter, elem):

    cls.finalize( segmenter = segmenter )
    SegmentState.initialize( segmenter = segmenter, elem = elem )


  @classmethod
  def finalize(cls, segmenter):

    gap = GapSegment( segment = segmenter.state_segment )
    delattr( segmenter, "state_segment" )
    segmenter.segments.append( gap )
    segmenter.state = None


class SegmentState(object):
  """
  Present residues
  Note this is a singleton
  """

  @classmethod
  def initialize(cls, segmenter, elem):

    assert segmenter.state is None
    segmenter.state = cls
    segmenter.state_segment = [ elem ]


  @classmethod
  def register_next_gap(cls, segmenter, elem):

    cls.finalize( segmenter = segmenter )
    GapState.initialize( segmenter = segmenter, elem = elem )


  @classmethod
  def register_next_elem(cls, segmenter, elem):

    if segmenter.consecutivity( segmenter.state_segment[-1], elem ):
      segmenter.state_segment.append( elem )

    else:
      previous = segmenter.state_segment[-1]
      cls.finalize( segmenter = segmenter )
      segmenter.segments.append(
        InterSegmentGap( previous = previous, following = elem )
        )
      cls.initialize( segmenter = segmenter, elem = elem )


  @classmethod
  def finalize(cls, segmenter):

    seg = ResidueSegment( segment = segmenter.state_segment )
    delattr( segmenter, "state_segment" )
    segmenter.segments.append( seg )
    segmenter.state = None


class ChainSegmenter(object):
  """
  Segments a chain
  """

  def __init__(self, consecutivity):

    self.segments = []
    self.consecutivity = consecutivity
    self.state = None


  def register_start(self, elem):

    if elem:
      SegmentState.initialize( segmenter = self, elem = elem )

    else:
      GapState.initialize( segmenter = self, elem = elem )


  def register_next(self, elem):

    if elem:
      self.state.register_next_elem( segmenter = self, elem = elem )

    else:
      self.state.register_next_gap( segmenter = self, elem = elem )


  def register_end(self):

    self.state.finalize( segmenter = self )


class Segment(object):

  def __init__(self, start, end):

    if not end or not start or end < start:
      raise SegmentError("Invalid segment")

    self.start = start
    self.end = end


  def is_residue_segment(self):

    return True


  def remaining_when_flanked_by(self, left, right, chaindata):

    cutleft = left.remaining_from_following_residue_segment(
      segment = self,
      chaindata = chaindata,
      )
    return right.remaining_from_previous_residue_segment(
      segment = cutleft,
      chaindata = chaindata,
      )


  def __len__(self):


    return self.end - self.start


  def __str__(self):

    if self.start == self.end:
      return "%s" % self.start.identifier()

    else:
      return "%s -> %s" % ( self.start.identifier(), self.end.identifier() )


class InternalLoop(object):

  def __init__(self, start, end):

    assert start
    assert end

    if end < start:
      raise SegmentError("Invalid loop")

    self.start = start
    self.end = end


  def is_residue_segment(self):

    return False


  def is_n_terminal_open(self):

    return False


  def is_c_terminal_open(self):

    return False


  def remaining_from_previous_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()
    return Segment(
      start = segment.start,
      end = min( segment.end, chaindata.before( position = self.start ) ),
      )


  def remaining_from_following_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()

    try:
      after = chaindata.after( position = self.end )

    except ChainDataError:
      raise SegmentError("Segment start is beyond C-terminal")

    return Segment( start = max( segment.start, after ), end = segment.end )


  def merged_loop(self, other):

    assert not other.is_residue_segment()

    if other.is_n_terminal_open():
      return NTerminalLoop( end = max( self.end, other.end ) )

    elif other.is_c_terminal_open():
      return CTerminalLoop( start = min( self.start, other.start ) )

    else:
      return InternalLoop(
        start = min( self.start, other.start ),
        end = max( self.end, other.end ),
        )


  def next_chainbuilder_state(self):

    return CB_LoopExtendWithResidue


  def update_chainbuilder_registry(self, builder):

    builder.segments.append( self )


  def __len__(self):

    return self.end - self.start


  def __str__(self):

    if self.start == self.end:
      segstr = "%s" % self.start.identifier()

    else:
      segstr = "%s -> %s" % ( self.start.identifier(), self.end.identifier() )

    return "LOOP %s" % segstr


class NTerminalLoop(object):

  def __init__(self, end):

    self.end = end


  def is_residue_segment(self):

    return False


  def is_n_terminal_open(self):

    return True


  def is_c_terminal_open(self):

    return False


  def remaining_from_previous_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()
    raise SegmentError("Loop extends to N-terminal")


  def remaining_from_following_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()
    return Segment( start = max( segment.start, self.end ), end = segment.end )


  def merged_loop(self, other):

    assert not other.is_residue_segment()

    if other.is_c_terminal_open():
      raise SetupError("Loop merging creates continuous loop over sequence")

    else:
      return NTerminalLoop( end = max( self.end, other.end ) )


  def next_chainbuilder_state(self):

    return CB_LoopExtendWithResidue


  def update_chainbuilder_registry(self, builder):

    pass


  def __str__(self):

    return "LOOP NTERM -> '%s'" % self.end.identifier()


class CTerminalLoop(object):

  def __init__(self, start):

    self.start = start


  def is_residue_segment(self):

    return False


  def is_n_terminal_open(self):

    return False


  def is_c_terminal_open(self):

    return True


  def remaining_from_previous_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()
    return Segment( start = segment.start, end = min( segment.end, self.start ) )


  def remaining_from_following_residue_segment(self, segment, chaindata):

    assert segment.is_residue_segment()
    raise SegmentError("Loop extends to C-terminal")


  def merged_loop(self, other):

    assert not other.is_residue_segment()

    if other.is_n_terminal_open():
      raise SetupError("Loop merging creates continuous loop over sequence")

    else:
      return CTerminalLoop( start = min( self.start, other.start ) )


  def next_chainbuilder_state(self):

    return CB_Reject


  def update_chainbuilder_registry(self, builder):

    pass


  def __str__(self):

    return "LOOP '%s' -> CTERM" % self.start.identifier()


class SetupValidator(object):

  def __init__(
    self,
    min_edge_segment_length = 10,
    min_internal_segment_length = 5,
    max_loop_length = 14,
    ):

    self.min_edge_segment_length = min_edge_segment_length
    self.min_internal_segment_length = min_internal_segment_length
    self.max_loop_length = max_loop_length


  def is_valid_terminal_segment(self, segment):

    return self.min_edge_segment_length <= len( segment )


  def is_valid_internal_segment(self, segment):

    return self.min_internal_segment_length <= len( segment )


  def is_valid_internal_loop(self, segment):

    return len( segment ) <= self.max_loop_length


def CB_NTerminalSearch(builder, segment):
  """
  ChainBuilder searching for N-terminal
  """

  assert not builder.segments

  if segment.is_residue_segment():
    if builder.validator.is_valid_terminal_segment( segment = segment ):
      builder.segments.append( segment )
      builder.state = CB_NTerminalExtend
      return "%s accepted as preliminary N-terminal" % segment

    else:
      return "%s rejected as preliminary N-terminal" % segment

  else:
    return "%s rejected as N-terminal cannot start with a gap" % segment


def CB_NTerminalExtend(builder, segment):
  """
  ChainBuilder extending N-terminal
  """

  assert not segment.is_residue_segment()
  assert len( builder.segments ) == 1
  previous = builder.segments[-1]
  assert previous.is_residue_segment()

  try:
    cutdown = segment.remaining_from_previous_residue_segment(
      segment = previous,
      chaindata = builder.chaindata,
      )

    if builder.validator.is_valid_terminal_segment( segment = cutdown ):
      segment.update_chainbuilder_registry( builder = builder )
      builder.state = segment.next_chainbuilder_state()
      return "%s accepted as loop following N-terminal" % segment

  except SegmentError:
    pass

  builder.segments = []
  builder.state = CB_NTerminalSearch
  return "%s renders preliminary N-terminal invalid, resume search" % segment


def CB_LoopExtendWithResidue(builder, segment):
  """
  ChainBuilder extending, most recent one being a loop segment
  """

  assert segment.is_residue_segment()
  assert 2 <= len( builder.segments )
  previous = builder.segments[-1]
  assert not previous.is_residue_segment()
  assert not previous.is_c_terminal_open()

  try:
    cutdown = previous.remaining_from_following_residue_segment(
      segment = segment,
      chaindata = builder.chaindata,
      )

    if builder.validator.is_valid_internal_segment( segment = cutdown ):
      builder.segments.append( segment )
      builder.state = CB_ResidueExtend
      return "%s accepted as preliminary internal segment" % segment

  except SegmentError:
    pass

  builder.state = CB_LoopExtendWithLoop
  return "%s rejected as preliminary internal segment" % segment


def CB_LoopExtendWithLoop(builder, segment):
  """
  ChainBuilder extending, most recent one being a loop segment, failed with
  """

  assert not segment.is_residue_segment()
  assert 2 <= len( builder.segments )
  previous = builder.segments[-1]
  assert not previous.is_residue_segment()

  merged = previous.merged_loop( other = segment )
  builder.segments.pop()
  merged.update_chainbuilder_registry( builder = builder )
  builder.state = merged.next_chainbuilder_state()
  return "%s merged with previous loop (result: %s)" % ( segment, merged )


def CB_ResidueExtend(builder, segment):
  """
  ChainBuilder extending, most recent one being a residue segment
  """

  assert 2 <= len( builder.segments )

  assert not segment.is_residue_segment()
  ressegment = builder.segments[-1]
  assert ressegment.is_residue_segment()
  left = builder.segments[-2]

  try:
    cutdown = ressegment.remaining_when_flanked_by(
      left = left,
      right = segment,
      chaindata = builder.chaindata,
      )

    if builder.validator.is_valid_internal_segment( segment = cutdown ):
      assert not segment.is_n_terminal_open()
      segment.update_chainbuilder_registry( builder = builder )
      builder.state = segment.next_chainbuilder_state()
      return "%s accepted as loop following internal segment" % segment

  except SegmentError:
    pass

  merged = left.merged_loop( other = segment )
  builder.segments.pop()
  builder.segments.pop()
  merged.update_chainbuilder_registry( builder = builder )
  builder.state = merged.next_chainbuilder_state()
  msg = ( "%s renders previous internal segment invalid, "
    + "reject as merge with previous loop (result: %s)" )
  return msg % ( segment, merged )


def CB_Reject(builder, segment):
  """
  ChainBuilder encountered CTerminalLoop, reject everything from now
  """

  return "%s rejected as encountered after C-terminal loop" % segment


def CB_Final(builder, segment):
  """
  ChainBuilder finalized, extension is an error
  """

  raise SetupError("Request to include segment after structure finalized")


class ChainBuilder(object):

  def __init__(self, chaindata, validator):

    self.chaindata = chaindata
    self.validator = validator

    self.segments = []
    self.state = CB_NTerminalSearch


  def add(self, segment):

    return self.state( builder = self, segment = segment )


  def finalize(self):

    actions = []

    while self.segments:
      current = self.segments[-1]

      if current.is_residue_segment():
        if 2 <= len( self.segments ):
          previous = self.segments[-2]
          assert not previous.is_residue_segment()
          cutdown = previous.remaining_from_following_residue_segment(
            segment = current,
            chaindata = self.chaindata,
            )

        else:
          cutdown = current

        if self.validator.is_valid_terminal_segment( segment = cutdown ):
          break

        else:
          actions.append(
            "%s rejected as preliminary N-terminal" % current
            )

      else:
        actions.append(
          "%s rejected as C-terminal cannot start with a gap" % current
          )

      self.segments.pop()

    self.state = CB_Final

    return actions


  def get(self):

    if self.state != CB_Final:
      raise SetupError("Builder not finalized")

    if not self.segments:
      raise SetupError("No accepted segments")

    residues = []
    loops = []

    for s in self.segments:
      if s.is_residue_segment():
        residues.append( s )

      else:
        loops.append( s )

    assert residues

    for s in loops:
      if not self.validator.is_valid_internal_loop( segment = s ):
        index = self.segments.index( s )
        possibilities = []

        if 0 < index:
          possibilities.append( BeforeLoop( segment = self.segments[ index - 1 ] ) )

        if index < len( self.segments ) - 1:
          possibilities.append( AfterLoop( segment = self.segments[ index + 1 ] ) )

        raise LoopTooLong( loop = s, possibilities = possibilities )

    return ModellingSetup(
      residues = residues,
      loops = loops,
      chaindata = self.chaindata,
      )


class ModellingSetup(object):

  def __init__(self, residues, loops, chaindata):

    assert residues

    import iotbx.pdb
    self.root = iotbx.pdb.hierarchy.root()
    model = iotbx.pdb.hierarchy.model()
    self.root.append_model( model )
    chain = iotbx.pdb.hierarchy.chain()
    chain.id = "A"
    model.append_chain( chain )

    from mmtbx.monomer_library import idealized_aa
    from phaser import mmtype
    three_letter_for = mmtype.PROTEIN.three_letter_for()
    residue_dict = idealized_aa.residue_dict()

    self.filleds = []
    segiter = chaindata.segment( start = residues[0].start, end = residues[-1].end )
    shift = 0

    for ( index, posi ) in enumerate( segiter, start = 1 ):
      if posi:
        copy = posi.residue.detached_copy()
        shift = 0

      else:
        resname = three_letter_for[ posi.rescode ].lower()

        if resname in residue_dict:
          copy = residue_dict[ resname ].only_residue_group().detached_copy()
          atoms = copy.atoms()
          atoms.set_xyz( atoms.extract_xyz() + ( shift, 0, 0 ) )
          shift += 3
          self.filleds.append( copy )

        else:
          raise RuntimeError("Unknown resname:").with_traceback(resname)

      copy.resseq = index
      chain.append_residue_group( copy )

    self.root.atoms_reset_serial()

    shift = residues[0].start - chaindata.first() - 2
    assert chain.residue_groups()[0].resseq_as_int() == 1
    assert residues[0].start.index - shift == 1

    self.loops = []

    for loop in loops:
      self.loops.append( ( loop.start.index - shift, loop.end.index - shift ) )


  def rosetta_run(self, options, logger):

    return RosettaRun(
      root = self.root,
      loops = self.loops,
      options = options,
      tmpdir = ".",
      logger = logger,
      )


REMODEL_OPTIONS_FOR = {
  "kic": [
    "-loops::remodel", "perturb_kic",
    "-loops::kic_leave_centroid_after_initial_closure", "true",
    ],

  "ngk": [
    "-loops::remodel", "perturb_kic",
    "-loops::kic_leave_centroid_after_initial_closure", "true",
    "-loops:kic_omega_sampling",
    "-loops:kic_rama2b",
    "-allow_omega_move", "true",
    ],
  }

REFINE_OPTIONS_FOR = {
  "no": [],

  "default": [
    "-loops:refine", "refine_kic",
    ],

  "quick": [
    "-loops:refine", "refine_kic",
    "-loops:neighbor_dist", "6.0",
    "-loops:optimize_only_kic_region_sidechains_after_move", "true",
    ],

  "fast": [
    "-loops:refine", "refine_kic",
    "-loops:neighbor_dist", "6.0",
    "-loops:optimize_only_kic_region_sidechains_after_move", "true",
    "-loops:fast", "true",
    ],

  "test": [
    "-loops:refine", "refine_kic",
    "-loops:neighbor_dist", "6.0",
    "-loops:optimize_only_kic_region_sidechains_after_move", "true",
    "-run:test_cycles", "true",
    ],
  }


def rosetta_modelling_options(remodel, refine, max_build_attempts, bump_overlap_factor):
  """
  Set of Rosetta parameters
  """

  if remodel not in REMODEL_OPTIONS_FOR:
    raise SetupError("Unknown remodel option: %s" % remodel)

  if refine not in REFINE_OPTIONS_FOR:
    raise SetupError("Unknown refine option: %s" % refine)

  options = REMODEL_OPTIONS_FOR[ remodel ] + REFINE_OPTIONS_FOR[ refine ]
  options.extend(
    [
      "-loops:max_kic_build_attempts", str( max_build_attempts ),
      "-loops:kic_bump_overlap_factor", str( bump_overlap_factor ),
      ]
    )
  return options


class RosettaRun(object):
  """
  Actual Rosetta run
  """

  def __init__(self, root, loops, options, tmpdir, logger):

    self.root = root
    self.loops = loops
    self.options = options
    self.logger = logger

    info = facade.Package( channel = logger.info )
    info.heading( text = "Rosetta", level = 1 )

    import tempfile
    self.tmpdir = tempfile.mkdtemp( prefix = "rosetta", dir = tmpdir )

    import os.path
    self.pdbfile = os.path.join( self.tmpdir, "input.pdb" )
    self.root.write_pdb_file(  self.pdbfile )
    self.loop_file = os.path.join( self.tmpdir, "loop_file" )

    with open( self.loop_file, "w" ) as ofile:
      for ( start, end ) in self.loops:
        ofile.write( "LOOP %s %s 0 0 1\n" % ( start, end ) )

    info.field( name = "Rosetta folder", value = self.tmpdir )
    info.field( name = "Input PDB file", value = self.pdbfile )
    info.field( name = "Loop file", value = self.loop_file )

    self.prefix = os.path.join( self.tmpdir, "simple_homology_model_" )


  def start(self):

    info = facade.Package( channel = self.logger.info )

    from phenix import rosetta

    try:
      binaries = rosetta.rosetta_binaries( program = "loopmodel" )

    except rosetta.InstallationError as e:
      raise RosettaError("Incomplete Rosetta environment setup: %s" % e)

    if len( binaries ) != 1:
      raise RosettaError("Cannot locate loopmodel: %s" % ", ".join(binaries))

    self.loopmodel = binaries[0]
    self.database = rosetta.rosetta_db_path()

    if self.database is None:
      raise RosettaError("Cannot locate Rosetta database")

    cmdl = [
      self.loopmodel,
      "-database", self.database,
      "-in::file::s", self.pdbfile,
      "-out::prefix", self.prefix,

      "-in::file::fullatom",
      "-out::file::fullatom",

      "-loops::loop_file", self.loop_file,
      ]
    cmdl.extend( self.options )
    info.field_sequence( name = "Rosetta options", value = cmdl,separator = " " )
    import subprocess
    process = subprocess.Popen(
      cmdl,
      stdout = subprocess.PIPE,
      stderr = subprocess.STDOUT,
      )

    for line in process.stdout:
      info.text( text = line )

    rc = process.wait()

    if rc != 0:
      raise RosettaError("Rosetta finished with error code %s" % rc)

    import glob
    self.outfiles = glob.glob( self.prefix + "*" )
    self.outpdbs = [ f for f in self.outfiles if f.endswith( ".pdb" ) ]


  def num_pdbs(self):

    return len( self.outpdbs )


  def get_pdb_root(self, index):

    import iotbx.pdb
    return iotbx.pdb.input( self.outpdbs[ index ] ).construct_hierarchy()


  def cleanup(self):

    if self.tmpdir is not None:
      import shutil
      shutil.rmtree( path = self.tmpdir )

      self.tmpdir = None
      self.pdbfile = None
      self.loop_file = None
      self.prefix = None
      self.outfiles = []
      self.outpdbs = []


PHIL_HOMOLOGY_MODELLING = """
min_residue_margin = 2
  .help = "Minimum number of residues to cut back on both sides of loops"
  .type = int
  .optional = False

residue_distance = 2.5
  .help = "Distance covered by a single residue (in A)"
  .type = float( value_min = 1.0 )
  .optional = False

min_edge_segment_length = 5
  .help = "Discard edge segments if shorter (after considering gap margins)"
  .type = int( value_min = 1 )
  .optional = False

min_internal_segment_length = 2
  .help = "Discard internal segments if shorter (after considering gap margins)"
  .type = int( value_min = 1 )
  .optional = False

max_loop_length = 60
  .help = "Maximum loop length"
  .type = int( value_min = 1 )
  .optional = False

rosetta_max_build_attempts = 1000
  .help = "Maximum build attempts to close loop"
  .type = int( value_min = 1 )
  .optional = False

rosetta_bump_overlap_factor = 0.1 # much more robust than 0.36, results just as good
  .help = "Allows some atomic overlap in initial loop closures"
  .type = float( value_min = 0.00 )
  .optional = False

rosetta_loop_closure = %(rosetta_loop_closure)s
  .help = "Algorithm for Rosetta loop closure"
  .type = choice
  .optional = False

rosetta_loop_refinement = %(rosetta_loop_refinement)s
  .help = "Algorithm for Rosetta loop refinement"
  .type = choice
  .optional = False
""" % {
  "rosetta_loop_closure": tbx_utils.choice_string(
    possible = REMODEL_OPTIONS_FOR,
    default = "ngk",
    ),

  "rosetta_loop_refinement": tbx_utils.choice_string(
    possible = REFINE_OPTIONS_FOR,
    default = "no",
    ),
  }


def homology_modelling_setup(
  chaindata,
  mmt,
  min_edge_segment_length,
  min_internal_segment_length,
  max_loop_length,
  min_residue_margin,
  residue_distance,
  logger,
  ):

  info = facade.Package( channel = logger.info )

  info.heading( text = "Homology modelling setup", level = 2 )
  info.heading( text = "Chain segments", level = 3 )
  strconsec = mmt.residue_structural_connectivity( tolerance = 0.4 )

  segmenter = ChainSegmenter(
    consecutivity = lambda l, r: strconsec( l.residue, r.residue )
    )
  positer = iter( chaindata )
  segmenter.register_start( elem = positer.next() )

  for current in positer:
    segmenter.register_next( elem = current )

  segmenter.register_end()
  assert segmenter.segments
  info.ordered_list_begin( separate_items = False )

  for seg in segmenter.segments:
    info.list_item( text = str( seg ) )

  info.list_end()

  info.heading( text = "Modelling setup", level = 3 )
  assert segmenter.segments
  validator = SetupValidator(
    min_edge_segment_length = min_edge_segment_length,
    min_internal_segment_length = min_internal_segment_length,
    max_loop_length = max_loop_length,
    )
  builder = ChainBuilder( chaindata = chaindata, validator = validator )
  info.ordered_list_begin( separate_items = False )

  for segment in segmenter.segments:
    description = segment(
      chaindata = chaindata,
      margin = min_residue_margin,
      interdistance = residue_distance,
      )
    info.list_item( text = builder.add( segment = description ) )

  info.list_end()

  info.task_begin( text = "Finalizing setup" )
  actions = builder.finalize()
  info.task_end()

  if actions:
    info.heading( text = "Revisions taken:", level = 6 )
    info.unordered_list_begin( separate_items = False )

    for line in actions:
      info.list_item( text = line )

    info.list_end()

  else:
    info.unformatted_text( text = "No revisions taken" )

  return builder.get()


def prepare_model_for_sculpting(chainalignment, mmt, logger):

  info = facade.Package( channel = logger.info )
  info.heading( text = "Chain processing", level = 2 )

  import iotbx.pdb
  chain = iotbx.pdb.hierarchy.chain()
  chain.id = "A"
  target = []
  model = []
  residues = []
  rescodes = []

  for ( t, m, rs, rc ) in zip(
    chainalignment.target_sequence(),
    chainalignment.model_sequence(),
    chainalignment.residue_sequence(),
    chainalignment.rescode_sequence(),
    ):
    if t is not None:
      target.append( t )
      model.append( m )
      rescodes.append( rc )

      if rs is not None:
        copy = rs.detached_copy()
        chain.append_residue_group( copy )
        residues.append( copy )

      else:
        residues.append( None )

  from phaser import chisellib
  myca = chisellib.ChainAlignment(
    target = target,
    model = model,
    residues = residues,
    rescodes = rescodes,
    )

  return chisellib.Sample(
    name = "Aligneds",
    chain = chain,
    mmtype = mmt,
    chainalignment = myca,
    errors = None,
    )


def sculpt_model(sample, logger):

  from phaser import sculptor_new as sculptor
  info = facade.Package( channel = logger.info )

  mphil = sculptor.parsed_master_phil()
  params = mphil.extract()

  processor_for = sculptor.get_processor_for( params = params.sculpt )
  info.heading( text = "Chain processing", level = 2 )

  if sample.mmtype not in processor_for:
    raise SculptorError("No processor for %s" % sample.mmtype.name)

  processor = processor_for[ sample.mmtype ]

  from phaser import chisellib
  modelinfo = chisellib.ModelInfo( chains = [ sample.chain ] )
  info.task_begin( text = "Running processor for '%s'" % sample.mmtype.name )
  chaindiff = processor( sample = sample, model_info = modelinfo )
  info.task_end()

  info.task_begin( text = "Applying changes" )

  count = sculptor.ChaindiffProcessor.log_and_execute(
    chaindiff = chaindiff,
    channel = facade.Package( channel = logger.verbose ),
    simplify = True,
    )
  info.task_end()
  info.field( name = "Number of modifications", value = count )


def run(chainalignment, mmt, params, logger):

  from phaser import mmtype

  if mmt != mmtype.PROTEIN:
    raise UnsupportedMMTypeError("The type '%s' is not supported" % mmt.name)

  info = facade.Package( channel = logger.info )

  info.heading( text = "Model preparation", level = 1 )
  sample = prepare_model_for_sculpting(
    chainalignment = chainalignment,
    mmt = mmt,
    logger = logger,
    )
  sculpt_model( sample = sample, logger = logger )

  info.heading( text = "Homology modelling", level = 1 )
  ca = sample.chainalignment
  chaindata = ChainData(
    positions = [
      ChainPosition( index = i, rescode = rc, residue = rg )
      for ( i, ( rc, rg ) )
      in enumerate( zip( ca.target_sequence(), ca.residue_sequence() ) )
      ],
    )

  try:
    setup = homology_modelling_setup(
      chaindata = chaindata,
      mmt = mmt,
      min_edge_segment_length = params.min_edge_segment_length,
      min_internal_segment_length = params.min_internal_segment_length,
      max_loop_length = params.max_loop_length,
      min_residue_margin = params.min_residue_margin,
      residue_distance = params.residue_distance,
      logger = logger,
      )

  except LoopTooLong as e:
    info.highlight(
      text = "%s (length: %s, maximum: %s)" % (
        e,
        len( e.loop),
        params.max_loop_length,
        )
      )
    raise

  rosetta_run = setup.rosetta_run(
    options = rosetta_modelling_options(
      remodel = params.rosetta_loop_closure,
      refine = params.rosetta_loop_refinement,
      max_build_attempts = params.rosetta_max_build_attempts,
      bump_overlap_factor = params.rosetta_bump_overlap_factor,
      ),
    logger = logger,
    )

  import time

  try:
    start = time.time()
    rosetta_run.start()
    info.field_float( name = "Runtime", value = time.time() - start, digits = 2 )

    num_pdbs = rosetta_run.num_pdbs()

    if 0 < num_pdbs:
      if 1 < num_pdbs:
        info.highlight( text = "Multiple models output - using only first" )

      outpdb = rosetta_run.get_pdb_root( index = 0 )

    else:
      info.highlight( text = "No models output" )
      raise RosettaError("No models produced")

  finally:
    info.task_begin( text = "Cleaning up" )
    rosetta_run.cleanup()
    info.task_end()

  return outpdb

from phaser import chainalign

master_phil = """
input
  .help = "Parameters controlling input"
{
  structure = None
    .help = "PDB file name"
    .optional = False
    .type = path

  alignment = None
    .help = "Alignment file name"
    .optional = False
    .type = path

  sequence = None
    .help = "Target sequence"
    .optional = True
    .type = path
}

output
  .help = "Parameters controlling output"
{
  model = model.pdb
    .help = "Output model file name"
    .optional = False
    .type = path
}

chain_to_alignment_matching
  .help = "Chain-to-alignment matching options"
{
  %(chain_alignment)s
}

homology_modelling
  .help = "Parameters controlling homology modelling"
{
  %(homology_modelling)s
}
""" % {
  "chain_alignment": chainalign.PHIL_SCULPTOR_CHAIN_ALIGNMENT,
  "homology_modelling": PHIL_HOMOLOGY_MODELLING,
  }

if __name__ == "__main__":
  from phaser import cli_new as cli
  import libtbx.phil
  from iotbx import bioinformatics
  PROGRAM = "simple_homology_model"

  parsed_master_phil = libtbx.phil.parse( master_phil )
  parser = cli.PCLIParser( master_phil = parsed_master_phil )
  parser.recognize_file_type_as(
    extensions = [ ".pdb", ".ent", ".cif" ],
    philpath = "input.structure",
    )
  parser.recognize_file_type_as(
    extensions = bioinformatics.known_alignment_formats(),
    philpath = "input.alignment",
    )
  parser.recognize_file_type_as(
    extensions = bioinformatics.known_sequence_formats(),
    philpath = "input.sequence",
    )

  params = parser.parse_args()

  from phaser import sculptor_new as sculptor
  from phaser import logoutput_new as logoutput
  from phaser.logoutput_new import console
  from libtbx.utils import Sorry
  import sys

  handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]

  with logoutput.Writer( \
    handlers = handlers, \
    level = 10, \
    close_handlers = True,
    verbose = 0,
    info = 10,
    warn = 20 \
    ) \
    as logger:
    info = facade.Package( channel = logger.info )
    info.title( text = PROGRAM )

    info.heading( text = "Read in files", level = 1 )
    inputpars = params.phil_extract.input
    info.field( name = "PDB file", value = inputpars.structure )

    if inputpars.structure is None:
      raise Sorry("Structure file is missing")

    pdbobj = tbx_utils.PDBObject.from_file( file_name = inputpars.structure )
    chain = pdbobj.object.models()[0].chains()[0]
    rgs = pdbobj.object.models()[0].chains()[0].residue_groups()
    assert rgs

    from phaser import mmtype

    if not mmtype.PROTEIN.recognize_chain_type( chain = chain ):
      raise Sorry("Chain %s is not recognized as protein" % chain.id)

    info.field( name = "Alignment file", value = inputpars.sequence )

    if inputpars.alignment is None:
      raise Sorry("Alignment file is missing")

    caparms = params.phil_extract.chain_to_alignment_matching
    alnobj = tbx_utils.AlignmentObject.from_file( inputpars.alignment )
    alignment = alnobj.object

    if inputpars.sequence is None:
      info.unformatted_text( text = "No sequence file provided" )
      info.unformatted_text( text = "First alignment sequence will be used for target" )
      reformatted = alignment

    else:
      info.field( name = "Sequence file", value = inputpars.sequence )
      seqobj = tbx_utils.SequenceObject.from_file( inputpars.sequence )
      assert seqobj.object
      sequence = seqobj.object[0]

      reformatted = chainalign.canonize_alignment(
        sequence = sequence,
        alignment = alignment,
        min_hss_length = caparms.min_hss_length,
        min_identity_fraction = caparms.min_sequence_identity,
        logger = logger,
        )

    info.heading( text = "Residue alignment", level = 1 )
    strials = [
      sculptor.AlignmentSequence(
        name = alnobj.name,
        alignment = reformatted,
        index = i,
        )
      for i in range( reformatted.multiplicity() )
      ]

    try:
      ( strial, residues, rescodes ) = sculptor.chain_alignment(
        chain = chain,
        mmt = mmtype.PROTEIN,
        trials = strials,
        params = caparms,
        logger = logger,
        )

    except sculptor.SculptorError as e:
      info.highlight( text = "Aligner: %s" % e )
      raise Sorry("Could not match chain with alignment")

    from phaser import chisellib
    chainalignment = chisellib.ChainAlignment(
      target = sculptor.reformat_alignment_sequence(
        sequence = strial.target(),
        gap = strial.gap(),
        ),
      model = sculptor.reformat_alignment_sequence(
        sequence = strial.sequence(),
        gap = strial.gap(),
        ),
      residues = residues,
      rescodes = rescodes,
      )
    info.field_percentage(
      name = "Sequence identity",
      value = 100.0 * chainalignment.cumulative_identity_fraction(),
      digits = 2,
      )
    hmparms = params.phil_extract.homology_modelling
    try:
      outpdb = run(
        chainalignment = chainalignment,
        mmt = mmtype.PROTEIN,
        params = hmparms,
        logger = logger,
        )

    except LoopTooLong as e:
      raise Sorry(e)

    except RosettaError as e:
      raise Sorry(e)

    except Exception:
      warn = facade.Package( channel = logger.warn )
      warn.highlight( text = "An error occurred" )

      ( exc_type, exc_value, tb ) = sys.exc_info()
      warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
      raise

    info.field(
      name = "Output PDB file",
      value = params.phil_extract.output.model,
      )
    outpdb.write_pdb_file( params.phil_extract.output.model )

    info.separator()
    info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
    info.separator()
