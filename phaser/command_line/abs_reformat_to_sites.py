from __future__ import print_function

import sys

if len( sys.argv ) != 2:
  print("Usage: abs_reformat_to_sites shelxl-file")
  sys.exit(1)

from iotbx import shelx
data = shelx.cctbx_xray_structure_from( None, filename = sys.argv[1] )
symm = data.crystal_symmetry()
#cell = symm.unit_cell()
#ustars = data.extract_u_cart_plus_u_iso()
scatterers = data.scatterers()
#assert len( scatterers ) == len( ustars )

for scat in scatterers:
  #if scat.scattering_type == "H":
  #  continue

  if scat.flags.use_u_aniso():
    bfac = "USTAR %.5f %.5f %.5f %.5f %.5f %.5f" % scat.u_star

  else:
    bfac = "ISOB %.5f" % scat.b_iso()

  print("ATOM CRYSTAL xtal0 ELEMENT %s FRAC %s OCC %.3f %s FIXO ON LABEL %s" % (
    scat.element_symbol(),
    "%.6f %.6f %.6f" % scat.site,
    scat.occupancy,
    bfac,
    scat.label,
  ))
