from __future__ import print_function

class Pdb2ErrorConversionFailure(Exception):
  """
  Module exception
  """


def chain_alignment(target, gap, chain, mmtype):

  if not mmtype.recognize_chain_type( chain = chain ):
    raise Pdb2ErrorConversionFailure("Chain '%s' is not %s" % (chain.id, mmtype.name))

  from phaser import chainalign

  aligner = chainalign.ChainAlignmentAlgorithm.from_params(
    rgs = chain.residue_groups(),
    mmt = mmtype,
    annotation = "chain_%s" % chain.id if chain.id else "chain",
    params = chainalign.ChainAlignmentAlgorithm.parsed_master_phil().extract(),
    )

  trial = chainalign.Trial( gapped = target, gap = gap, annotation = "target" )

  try:
    result = aligner( trial = trial )

  except chainalign.ChainAlignFailure:
    raise Pdb2ErrorConversionFailure("PDB could not be matched to sequence")

  return result


def error_mapping(proq2_rgali, tmplt_rgali, chain):

  assert len( tmplt_rgali ) == len( proq2_rgali )

  error_for = dict(
    ( t, p.atoms()[0].b ) for ( t, p ) in zip( tmplt_rgali, proq2_rgali )
    if t is not None and p is not None
    )

  return [
    ( rg, error_for.get( rg ) ) for rg in chain.residue_groups()
    ]


def error_coverage_statistics(errspecs, length, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  seqfrac = sum( 1.0 for es in errspecs if es[1] is not None ) / length
  info.field_percentage(
    name = "Known errors",
    value = 100 * seqfrac,
    digits = 2,
    )

  return seqfrac


def csv_write(errspecs, outstream):

  import csv

  writer = csv.writer( outstream )

  writer.writerow( [ "resid", "resname", "error" ] )

  for ( rg, error ) in errspecs:
    writer.writerow( [ rg.resid(), rg.atom_groups()[0].resname, error ] )


if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser(
    description = "Map errors from ProQ2 result to Sculptor error file"
    )
  parser.add_argument(
    "alignment",
    metavar = "ALNFILE",
    help = "Alignment between target and template sequences"
    )
  parser.add_argument(
    "proqfile",
    metavar = "PDBFILE",
    help = "PDB from ProQ2 (target sequence)",
    )
  parser.add_argument(
    "template",
    metavar = "PDBFILE",
    help = "Template PDB (template sequence)",
    )
  parser.add_argument(
    "--prefix",
    metavar = "STRING",
    help = "Prefix for output file",
    default = "",
    )
  params = parser.parse_args()

  from libtbx.utils import Sorry
  from phaser import tbx_utils
  from phaser import mmtype
  import sys

  from phaser import logoutput_new as logoutput
  from phaser.logoutput_new import console
  from iotbx import bioinformatics

  handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]
  PROGRAM = "proq2_pdb_to_sculptor_error_file"

  with logoutput.Writer(
    handlers = handlers,
    level = tbx_utils.LOGGING_LEVEL_INFO,
    **tbx_utils.LOGGING_LEVELS
    ) as logger:
    try:
      from phaser.logoutput_new import facade
      info = facade.Package( channel = logger.info )
      info.title( text = PROGRAM )

      info.field( name = "Alignment", value = params.alignment )
      aln_obj = tbx_utils.AlignmentObject.from_file( file_name = params.alignment )

      info.heading( text = "Target sequence", level = 6 )
      info.sequence(
        sequence = bioinformatics.sequence(
          sequence = aln_obj.object.alignments[0],
          name = aln_obj.object.names[0],
          ),
        )

      info.heading( text = "Template sequence", level = 6 )
      info.sequence(
        sequence = bioinformatics.sequence(
          sequence = aln_obj.object.alignments[-1],
          name = aln_obj.object.names[-1],
          ),
        )

      info.field( name = "ProQ2 file", value = params.proqfile )
      proq_obj = tbx_utils.PDBObject.from_file( file_name = params.proqfile )

      if len( list( proq_obj.object.chains() ) ) != 1:
        raise Sorry("ProQ model contains multiple chains, should contain only one")

      try:
        proq2_chainalignment = chain_alignment(
          target = aln_obj.object.alignments[0],
          gap = aln_obj.object.gap,
          chain = proq_obj.object.only_chain(),
          mmtype = mmtype.PROTEIN,
          )

      except Pdb2ErrorConversionFailure as e:
        raise Sorry(e)

      info.heading( text = "Target chain alignment", level = 6 )
      info.alignment( alignment = proq2_chainalignment.iotbx_alignment() )

      info.field( name = "Template", value = params.template )
      tmplt_obj = tbx_utils.PDBObject.from_file( file_name = params.template )

      info.heading( text = "Template file alignment", level = 3 )
      info.ordered_list_begin()

      import os.path
      name = os.path.splitext( os.path.basename( params.template ) )[0]

      for chain in tmplt_obj.object.chains():
        info.list_item_begin(
          text = str( tbx_utils.ChainAnnotation.from_chain( chain = chain ) )
          )

        try:
          tmplt_chainalignment = chain_alignment(
            target = aln_obj.object.alignments[-1],
            gap = aln_obj.object.gap,
            chain = chain,
            mmtype = mmtype.PROTEIN,
            )

        except Pdb2ErrorConversionFailure as e:
          raise Sorry(e)

        info.heading( text = "Template chain alignment", level = 6 )
        info.alignment( alignment = tmplt_chainalignment.iotbx_alignment() )

        errors = error_mapping(
          proq2_rgali = proq2_chainalignment.residue_alignment(),
          tmplt_rgali = tmplt_chainalignment.residue_alignment(),
          chain = chain,
          )
        error_coverage_statistics(
          errspecs = errors,
          length = len( chain.residue_groups() ),
          logger = logger,
          )
        outfile = "%s%s%s.err" % (
          params.prefix,
          name, "_%s" % chain.id if chain.id else "",
          )
        info.field( name = "Output file", value = outfile )

        try:
          with open( outfile, "w" ) as ostream:
            csv_write( errspecs = errors, outstream = ostream )

        except IOError as e:
          raise Sorry(e)

        info.list_item_end()


        #
    except Sorry as e:
      raise

    except Exception as e:
      warn = facade.Package( channel = logger.warn )
      warn.highlight( text = "An error occurred" )

      ( exc_type, exc_value, tb ) = sys.exc_info()
      warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
      raise

    info.separator()
    info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
    info.separator()
