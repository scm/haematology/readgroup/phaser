# LIBTBX_SET_DISPATCHER_NAME phenix.subgroup_symmetry_datasets
from __future__ import print_function

class subgroup_relation_graph(object):
  """
  cctbx.sgtbx.pointgroup_tools.point_group_graph as a reversed directed graph
  """

  def __init__(self):

    from boost_adaptbx import graph

    self.graph = graph.adjacency_list(
      graph_type = "directed",
      vertex_type = "vector",
      edge_type = "vector",
      )
    self.descriptor_for = {}


  @property
  def space_group_for(self):

    return dict( zip( self.descriptor_for.values(), self.descriptor_for.keys() ) )


  def add_space_group(self, sg):

    self.descriptor_for[ sg ] = self.graph.add_vertex( label = None )


  def add_relation(self, left, right):

    if left not in self.descriptor_for:
      self.add_space_group( sg = left )

    if right not in self.descriptor_for:
      self.add_space_group( sg = right )

    self.graph.add_edge(
      vertex1 = self.descriptor_for[ left ],
      vertex2 = self.descriptor_for[ right ],
      )


  def breadth_first_order_from(self, space_group):

    if space_group not in self.descriptor_for:
      raise ValueError("Unknown space group: %s" % space_group)

    from boost_adaptbx.graph import breadth_first_search as bfs

    vertex = self.descriptor_for[ space_group ]
    visitor = bfs.vertex_recording_visitor( start_vertex = vertex )
    bfs.breadth_first_search(
      graph = self.graph,
      vertex = vertex,
      visitor = visitor,
      )

    space_group_for = self.space_group_for

    return [ space_group_for[ d ] for d in visitor.visited_vertices ]


  def distance_from(self, space_group):

    if space_group not in self.descriptor_for:
      raise ValueError("Unknown space group: %s" % space_group)

    from graph import breadth_first_search as bfs

    vertex = self.descriptor_for[ space_group ]
    visitor = bfs.distance_recording_visitor( start_vertex = vertex )
    bfs.breadth_first_search(
      graph = self.graph,
      vertex = vertex,
      visitor = visitor,
      )

    space_group_for = self.space_group_for
    return dict(
      ( space_group_for[ d ], v ) for ( d, v ) in visitor.distance_for.items()
      )


def calculate_underlying_subgroup_relations_graph(high):

  from cctbx import sgtbx
  from cctbx.sgtbx import pointgroup_tools
  g = pointgroup_tools.point_group_graph(
    pg_low = sgtbx.space_group_info( "P 1" ).group(),
    pg_high = high,
    enforce_point_group = False, # problem with centered space groups and screws
    )

  sg_called = dict(
    ( name, sgi.group() ) for ( name, sgi ) in g.graph.node_objects.items()
    )

  graph = subgroup_relation_graph()

  for sg in sg_called.values():
    graph.add_space_group( sg = sg )

  # Reverse direction for edges
  for ( source, edge_to ) in g.graph.edge_objects.items():
    source_sg = sg_called[ source ]

    for destination in edge_to.keys():
      graph.add_relation( left = sg_called[ destination ], right = source_sg )

  return graph


def calculate(miller_array):

  to_p = miller_array.change_of_basis_op_to_primitive_setting()
  primitive = miller_array.change_basis( to_p )
  symmetry = primitive.space_group()
  graph = calculate_underlying_subgroup_relations_graph( high = symmetry )
  expanded = primitive.expand_to_p1()
  distance_from = graph.distance_from( space_group = symmetry )
  order = graph.breadth_first_order_from( space_group = symmetry )
  assert order[0] == symmetry

  for sg in order[1:]:
    symmorphic = sg.build_derived_point_group()
    customized = expanded.customized_copy( space_group_info = symmorphic.info() )
    merged = customized.merge_equivalents().array()
    to_m = merged.change_of_basis_op_to_reference_setting()
    yield ( distance_from[ sg ], merged.as_reference_setting(), to_m * to_p )


def run(args):

  from libtbx.utils import Sorry

  if not args.input.hklin:
    raise Sorry("No hklin specified")

  from iotbx import reflection_file_reader
  refl = reflection_file_reader.any_reflection_file( args.input.hklin )

  from phaser import tbx_utils
  selected = tbx_utils.select_array(
    arrays = refl.as_miller_arrays(),
    labin = args.input.labin,
    )

  print("Selected array:")
  selected.show_summary()
  print()
  if not selected.is_xray_data_array():
    raise Sorry("Selected array is not xray data array")

  if selected.is_xray_intensity_array():
    column_root_label = "I"

  else:
    column_root_label = "F"

  wave = selected.info().wavelength
  count_for = {}

  if args.output.root is None:
    import os.path
    args.output.root = os.path.splitext( os.path.basename( args.input.hklin ) )[0]

  for ( dist, data, cbop ) in calculate( miller_array = selected ):
    print("Data in subgroup %s (distance: %s):" % (
      data.space_group_info().symbol_and_number(),
      dist,
    ))
    print("Change of basis: %s" % cbop.as_xyz())
    data.show_summary()
    print()
    csystem = data.space_group().crystal_system().lower()
    next_i = count_for.get( csystem, 0 ) + 1
    count_for[ csystem ] = next_i
    outfile = "%s_%s_%s.mtz" % ( args.output.root, csystem, next_i )

    mtz_d = data.as_mtz_dataset(
      column_root_label = column_root_label,
      wavelength = wave,
      )
    mtz_o = mtz_d.mtz_object()
    mtz_o.write( outfile )
    print("Written to %s" % outfile)
    print()
    print()


master_phil = """
input
{
  hklin = None
    .type = path
    .help = Reflection file

  labin = None
    .type = str
    .help = Data labels
}

output
{
  root = None
    .type = str
    .help = Output root
}
"""


if __name__ == "__main__":
  from phaser import cli_new as cli
  import libtbx.phil

  parser = cli.PCLIParser( master_phil = libtbx.phil.parse( master_phil ) )
  parser.recognize_file_type_as( extensions = [ ".mtz" ], philpath = "input.hklin" )

  args = parser.parse_args()

  run( args = args.phil.extract() )
