from __future__ import print_function

class ProQ2Error(Exception):
  """
  Module exception
  """


def write_and_flush(text, channel):

  channel.text( text = text )
  channel.flush()


def run(pdbstr, logger, waittime = 2, timeout = 600):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )

  from mmtbx import proq2
  info.task_begin( text = "Submitting job" )

  try:
    myjob = proq2.Job( pdbstr = pdbstr, name = "phenix.proq2" )

  except proq2.JobFolderParseError as e:
    info.task_failure( text = "Server did not return job folder" )
    raise ProQ2Error("Unexpected response from server")

  info.task_end()
  info.field( name = "Job folder", value = myjob.job_folder )

  info.task_begin( text = "Waiting for results" )

  from libtbx import progress
  from iotbx.pdb import download
  waiter = progress.complete_on_success( func = myjob, excspec = download.NotFound )

  import functools
  callback = functools.partial( write_and_flush, text = ".", channel = info )

  try:
    progress.wait(
      condition = waiter,
      waittime = waittime,
      timeout = timeout,
      callback = callback,
      )

  except progress.TimeoutError as e:
    info.task_failure( text = str( e ) )
    raise ProQ2Error(e)

  assert hasattr( waiter, "result" )
  result = waiter.result.read()
  waiter.result.close()

  info.task_end()
  return result


if __name__ == "__main__":
  import argparse
  parser = argparse.ArgumentParser(
    description = "Calculate error estimates using the ProQ2 server"
    )
  parser.add_argument( "structure", metavar = "PDBFILE", help = "Input PDB file" )
  parser.add_argument(
    "--timeout",
    type = float,
    metavar = "NUMBER",
    help = "Max time to wait for the job to complete",
    default = 600,
    )
  parser.add_argument(
    "--polltime",
    type = float,
    metavar = "NUMBER",
    help = "Time interval between subsequent job state checks",
    default = 2,
    )
  parser.add_argument(
    "--prefix",
    metavar = "STRING",
    help = "Prefix for output file",
    default = "proq2-",
    )
  params = parser.parse_args()

  from phaser import logoutput_new as logoutput
  from phaser.logoutput_new import console
  from libtbx.utils import Sorry
  import sys

  handlers = [ console.Handler.FromDefaults( stream = sys.stdout, width = 80 ) ]
  PROGRAM = "ProQ2 error estimation"

  with logoutput.Writer( \
    handlers = handlers, \
    level = 10, \
    close_handlers = False,
    verbose = 0,
    info = 10,
    warn = 20 \
    ) \
    as logger:
    from phaser.logoutput_new import facade

    info = facade.Package( channel = logger.info )
    info.title( text = PROGRAM )
    info.field( name = "Input PDB file", value = params.structure )

    try:
      with open( params.structure ) as ifile:
        pdbstr = ifile.read()

    except IOError as e:
      raise Sorry("Cannot open file: %s" % e)

    try:
      outstr = run(
        pdbstr = pdbstr,
        logger = logger,
        waittime = params.polltime,
        timeout = params.timeout,
        )

    except ProQ2Error as e:
      raise Sorry(e)

    except Exception:
      warn = facade.Package( channel = logger.warn )
      warn.highlight( text = "An error occurred" )

      ( exc_type, exc_value, tb ) = sys.exc_info()
      warn.traceback( exc_type = exc_type, exc_value = exc_value, traceback = tb )
      raise

    import os.path
    outname = "%s%s" % ( params.prefix, os.path.basename( params.structure ) )
    info.field( name = "Output PDB file", value = outname )

    try:
      with open( outname, "w" ) as ofile:
        ofile.write( outstr )
        ofile.write( "\n" )

    except IOError as e:
      raise Sorry(e)

    info.separator()
    info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
    info.separator()
