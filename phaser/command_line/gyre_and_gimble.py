
# LIBTBX_SET_DISPATCHER_NAME phenix.gyre_and_gimble
# LIBTBX_SET_DISPATCHER_NAME phaser.gyre_and_gimble

from __future__ import print_function

from phaser import *
import sys, math, os.path, os, shutil, argparse
from iotbx import reflection_file_reader, pdb
from libtbx.utils import Sorry
from libtbx.phil import parse
import mmtbx.utils
from phaser import SimpleFileProperties
from phaser import tbx_utils


"""
moving.pdbin = "../../tutorial/beta.pdb"
moving.rms = 2.0
hklin = "../../tutorial/beta_blip_P3221.mtz"
"""

chainids = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

master_phil = parse("""
gyre_and_gimble
{
  gyre = True
    .type = bool
    .help = "Perform gyre refinement"
  gimble = True
    .type = bool
    .help = "Perform gimble refinement"
  moving
      .help = "gyre and gimble model"
  {
    modlid = "moving"
      .help = "ensemble id of model"
      .type = str
    pdbin = None
      .help = "path to input PDB file"
      .type = path
    rms = None
      .help = "coordinate error estimate"
      .type = float
    domain
       .help = "One or more gyre-domains the user must define"
       .multiple = True
    {
      selection = None
        .help = "User defined phenix pdb selection string"
        .type = str
      chain_id = None
        .help = "If left undefined chain_id will be set to a unique character taken from alphanumeric character list"
        .type = str
    }
  }
  fixed
      .help = "fixed model"
  {
    modlid = "fixed"
      .help = "ensemble id of model"
      .type = str
    pdbin = None
      .help = "path to ensemble PDB file"
      .type = path
    rms = None
      .help = "rms for ensemble PDB file"
      .type = float
  }
  hklin = None
    .help = "reflections file"
    .type = path
  labin = None
    .help = "Labels of either intensity or amplitude as well as corresponding sigma columns to use"
    .type = str
  hires = 0.0
    .help = "Resolution limit"
    .type = float
  fileroot = "gyre_and_gimble"
    .help = "prefix of output files"
    .type = str
  restraint {
    gyre {
      rotation = None
        .type = float
      translation = None
        .type = float
    }
    gimble {
      rotation = None
        .type = float
      translation = None
        .type = float
    }
  }
  mute = True
    .type = bool
  njobs = 4
    .type = int
  gyre_purge_number = None
    .type = int
}

""", process_includes=True)


def main( args ):
  h = os.path.basename( __file__ )
  default_message="""\
Usage:

  %s phil_input.txt

where PHIL parameters conform to the following definitions:
""" % h

  if(len(args) <=1):
    print(default_message)
    master_phil.show(prefix="  " )
    return

  inputs = mmtbx.utils.process_command_line_args(args = args[1:],
                                                 master_params = master_phil
                                                 )
  workparams = inputs.params.extract().gyre_and_gimble

  # if user didn't assign all chain_id parameters assign them to unique default values
  available_int_chainids = { e:i for i,e in enumerate(chainids) }
  for d in workparams.moving.domain:
    if d.chain_id: # remove any chainid user might have already specified
      del available_int_chainids[d.chain_id]

  # create a (int, chainid) list from the dictionary
  available_int_chainids_lst = list(available_int_chainids.items())
  # sort the list according to chainid to get capital letters at the beginning
  #available_int_chainids_lst = sorted(available_int_chainids_lst, key=lambda x,y: (x[1], y[1]))
  # Somehow this works in Python3
  available_int_chainids_lst = sorted(available_int_chainids_lst, key= available_int_chainids_lst.index )
  # assign missing chainids with our default ones
  j=0
  for d in workparams.moving.domain:
    if not d.chain_id:
      d.chain_id = available_int_chainids_lst[j][0]
      j += 1

  pdbxtal = pdb.input(file_name= workparams.moving.pdbin)
  sym = pdbxtal.crystal_symmetry()
  hroot = pdbxtal.construct_hierarchy()
  nallatoms = len( hroot.atoms() )
  indexer = dict( ( a, i) for ( i, a ) in enumerate( hroot.atoms() ) )
  usedatoms = {}

  newroot = pdb.hierarchy.root()
  newroot.append_model(pdb.hierarchy.model())
  pdbin_original = pdbxtal.as_pdb_string(sym)

  print("--- Gyre and Gimble --- ")
  for ncid, d in enumerate(workparams.moving.domain):
    cid = d.chain_id
    selstr = d.selection
    selct = hroot.atom_selection_cache().selection( selstr )
    selctpdb = hroot.select(selct)
    print("new chain \"", cid, "\" selected atoms ", selstr)
    #open("pdbin"+cid, "w").write(selctpdb.as_pdb_string(sym))

    newroot.models()[0].append_chain( pdb.hierarchy.chain( id= cid ) )

    for atm in selctpdb.atoms():
      rg = pdb.hierarchy.residue_group()
      rg.resseq = atm.parent().parent().resseq_as_int()
      newroot.models()[0].chains()[ncid].append_residue_group( rg )
      ag = pdb.hierarchy.atom_group()
      rg.append_atom_group( ag )

      if atm in usedatoms:
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        raise Sorry("Ambiguous selection strings:\n\t"
                      + atm.format_atom_record() + "'\nis selected both by\n\t'"
                      + usedatoms[atm] + "'\nas well as by\n\t'" + selstr
                      + "'\nDon't do that again!")
      else:
        usedatoms[atm] = selstr

      ag.append_atom( atm.detached_copy() )
      ag.resname = atm.parent().resname
    if len( selctpdb.atoms() ) == 0:
      print("Warning: Selection:\n  \"%s\"\nfound no atoms in %s\n" % (selstr, workparams.moving.pdbin))

  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  pdbin_str = newroot.as_pdb_string(sym)
  nselatms = len(newroot.atoms())
  if nselatms ==0:
    print("Warning: No atoms selected from %s, using input chain identifiers" % workparams.moving.pdbin)
    pdbin_str = pdbin_original

  print("--- Read data ---")
  i = InputMR_DAT()
  if workparams.hklin is None:
    print("hklin not set")
    return
  i.setHKLI(workparams.hklin)
  cols = SimpleFileProperties.GetMtzColumnsTuple(workparams.hklin)
  # returns a tuple of lists like
  # (['F', 'FCalc'], ['SIGF', 'SIGDANO_peak', 'SIGIMEAN_peak'], ['IMEAN_peak'], ['FreeR_flag'], '')
  if workparams.labin:
    try:
      col1, col2 = workparams.labin.split(",")
      if col1 not in cols[0] and col1 not in cols[2] or col2 not in cols[1]:
        raise Exception("invalid column(s)")
    except Exception as m:
      print("Separate column labels by a comma.\n" + str(m))
      return -42

    if col1 in cols[0].I and col2 in cols[0].Sig:
      i.setLABI_I_SIGI( col1, col2 )
      print("mtz column labels (I):",  col1, col2)
    else:
      if col1 in cols[0].F and col2 in cols[0].Sig:
        i.setLABI_F_SIGF( col1, col2 )
        print("mtz column labels (F):",  col1, col2)
  else: # pickup default columns
    if len(cols[0].I):
      col1 = cols[0].I[0]
      col2 = cols[0].Sig[0]
      i.setLABI_I_SIGI( col1, col2 )
      print("Default mtz column labels (I):",  col1, col2)
    else:
      if len(cols[0].F):
        col1 = cols[0].F[0]
        col2 = cols[0].Sig[0]
        i.setLABI_F_SIGF( col1, col2 )
        print("Default mtz column labels (F):",  col1, col2)

  #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
  print ("Using columns: %s and %s" %(col1, col2 ))
  print ("--- Fast Rotation Function --- ")

  i.setMUTE(workparams.mute)
  r = runMR_DAT(i)
  success = r.Success()
  i = InputMR_FRF()
  i.setDATA_REFL(r.DATA_REFL)
  i.setSPAC_HALL(r.DATA_REFL.SG_HALL)
  i.addENSE_STR_RMS(workparams.moving.modlid,pdbin_str,workparams.moving.modlid,workparams.moving.rms)
  i.addSEAR_ENSE_NUM( workparams.moving.modlid, 1)
  i.setROOT(workparams.fileroot)
  i.setMUTE(workparams.mute)
  i.setJOBS(workparams.njobs)
  i.setRESO_HIGH(workparams.hires)
  if workparams.fixed.modlid and workparams.fixed.pdbin and workparams.fixed.rms:
    i.addENSE_PDB_RMS(workparams.fixed.modlid, workparams.fixed.pdbin, workparams.fixed.rms)
    i.addSOLU_ORIG_ENSE(workparams.fixed.modlid)
  r_frf = runMR_FRF(i)
  print ("Top Rotation Function LLG/RFZ: ",r_frf.top_rlist().RF, "/", r_frf.top_rlist().RFZ)
  print ("Number of rotations: ", r_frf.numRlist())
  if r_frf.Success() and r_frf.numRlist() :
    print("--- Gyre Refinement --- ")
    i = InputMR_RNP()
    i.setSOLU(r_frf.getDotRlist())
    i.setDATA_REFL(r.DATA_REFL)
    i.setSPAC_HALL(r.DATA_REFL.SG_HALL)
    i.setROOT( workparams.fileroot)
    i.setJOBS( workparams.njobs)
    i.setRESO_HIGH(workparams.hires)
    i.addENSE_STR_RMS(workparams.moving.modlid,pdbin_str,workparams.moving.modlid,workparams.moving.rms)
    if workparams.gyre_purge_number:
      i.setPURG_GYRE_NUMB(workparams.gyre_purge_number)
    if workparams.restraint.gyre.rotation:
      i.setMACR_SIGR(workparams.restraint.gyre.rotation)
    if workparams.restraint.gyre.translation:
      i.setMACR_SIGT(workparams.restraint.gyre.translation)
    if workparams.restraint.gimble.rotation:
      i.setMACM_SIGR(workparams.restraint.gimble.rotation)
    if workparams.restraint.gimble.translation:
      i.setMACM_SIGT(workparams.restraint.gimble.translation)
    if workparams.fixed.modlid and workparams.fixed.pdbin and workparams.fixed.rms:
      i.addENSE_PDB_RMS(workparams.fixed.modlid, workparams.fixed.pdbin, workparams.fixed.rms)
      i.addSOLU_ORIG_ENSE(workparams.fixed.modlid)
    if not workparams.gyre:
      i.setMACG_PROT("OFF")
    i.setMUTE(workparams.mute)
    i.setXYZO(False)
    r_gyre = runMR_GYRE(i)
    print("Top Gyre Refinement LLG: ", r_gyre.getTopSet().LLG)
    print("Number of gyred rotations: ", r_gyre.numRlist())
    if r_gyre.Success() and r_gyre.numRlist() :
      print("--- Fast Translation Function --- ")
      i = InputMR_FTF()
      i.setSOLU(r_gyre.getDotRlist())
      i.setENSE_MAP_PDB(r_gyre.GYRE_PDB)
      i.setDATA_REFL(r.DATA_REFL)
      i.setSPAC_HALL(r.DATA_REFL.SG_HALL)
      i.setROOT( workparams.fileroot)
      i.setJOBS( workparams.njobs)
      i.setRESO_HIGH(workparams.hires)
      i.setPEAK_TRAN_CUTO(95)
      i.setMUTE( workparams.mute)
      if workparams.fixed.modlid and workparams.fixed.pdbin and workparams.fixed.rms:
        i.addENSE_PDB_RMS(workparams.fixed.modlid, workparams.fixed.pdbin, workparams.fixed.rms)
        i.addSOLU_ORIG_ENSE(workparams.fixed.modlid)
      r_ftf = runMR_FTF(i)
      print("Top Translation Function TF/TFZ: ", r_ftf.getTopTF(), "/", r_ftf.getTopTFZ())
      print("Number of translations: ", r_ftf.numSolutions())
      if r_ftf.Success() and r_ftf.foundSolutions() :
        print("--- Gimble Refinement --- ")
        i = InputMR_RNP()
        i.setSOLU(r_ftf.getDotSol())
        if workparams.fixed.modlid and workparams.fixed.pdbin and workparams.fixed.rms:
          i.addENSE_PDB_RMS(workparams.fixed.modlid, workparams.fixed.pdbin, workparams.fixed.rms)
          i.addSOLU_ORIG_ENSE(workparams.fixed.modlid)
        i.setENSE_MAP_PDB(r_gyre.GYRE_PDB)
        i.setDATA_REFL(r.DATA_REFL)
        i.setSPAC_HALL(r.DATA_REFL.SG_HALL)
        i.setROOT( workparams.fileroot)
        i.setJOBS( workparams.njobs)
        i.setRESO_HIGH(workparams.hires)
        i.setMUTE( workparams.mute)
        if not workparams.gimble:
          i.setMACM_CHAI(False)
        r_gmbl = runMR_GMBL(i)
        print("Top Gimble LLG: ", r_gmbl.getTopLLG())
        print("Number of gimble solutions: ", r_gmbl.numSolutions())
        print("--- Gyre and Gimble ---")
        print(r_gmbl.getDotSol().unparse())
        for ipdb in range(0,r_gmbl.numSolutions()):
          xyzstr = str(r_gmbl.get_gyre_pdb_str(ipdb,str(pdbin_original)))
          xyzout = pdb.hierarchy.input(pdb_string=xyzstr)
          filename = workparams.fileroot+"."+str(ipdb+1)+ ".pdb"
          with open(filename, "w") as ofile:
            ofile.write(tbx_utils.get_phaser_revision_remark() + "\n" )
            ofile.write(xyzout.hierarchy.as_pdb_string(sym))
          print( "Output file: ", filename)


if __name__ == '__main__':
  main( sys.argv )
