from __future__ import print_function

from phaser import benchmark

def get_parser():

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument(
    "test",
    metavar = "JSON_FILE",
    help = "JSON file containing target description",
    )
  parser.add_argument(
    "phils",
    nargs = "*",
    metavar = "PHIL",
    action = "store",
    default = [],
    type = str,
    help = "PHIL arguments for homology_modelling and chain_to_alignment_matching",
    )
  parser.add_argument(
    "--prefix",
    type = str,
    default = "error-estimation",
    help = "Prefix for output data",
    )

  return parser


PROGRAM = "benchmark_error_prediction_test"


def parse(parser, args):

  return parser.parse_args( args = [ str( s ) for s in args ] )


def run(params, logger):

  from phaser.logoutput_new import facade
  info = facade.Package( channel = logger.info )
  info.title( text = PROGRAM )

  from libtbx.utils import Sorry
  import json
  info.task_begin( text = "Reading test description" )

  try:
    with open( params.test ) as ifile:
      description_for = json.load( ifile )

  except IOError as e:
    info.task_end( text = "failed" )
    raise Sorry(e)

  info.task_end()

  import os.path
  path_to_test = os.path.dirname( params.test )
  info.field( name = "Path for test files", value = path_to_test )

  import pickle

  with open( os.path.join( path_to_test, description_for[ "model_file" ] ) ) as ifile:
    model = pickle.load( ifile )

  chain = model.chain.iotbx_chain()

  from phaser import mmtype
  assert mmtype.PROTEIN.recognize_chain_type( chain = chain )

  from phaser import tbx_utils
  alnobj = tbx_utils.AlignmentObject.from_file(
    file_name = os.path.join( path_to_test, description_for[ "alignment_file" ] ),
    )
  alignment = alnobj.object

  info.heading( text = "Run parameters", level = 1 )
  from phaser.command_line import simple_homology_model
  from phaser import sculptor_new as sculptor
  from phaser import cli_new as cli
  import libtbx.phil

  master_phil = """
  chain_to_alignment_matching {
  %s
  }

  homology_modelling {
  %s
  }
  """ % (
    sculptor.PHIL_CHAIN_ALIGNMENT,
    simple_homology_model.PHIL_HOMOLOGY_MODELLING,
    )

  parsed_master_phil = libtbx.phil.parse( master_phil )
  argfac = cli.PhilArgumentFactory( master_phil = parsed_master_phil )
  merged = parsed_master_phil.fetch( sources = [ argfac( p ) for p in params.phils ] )
  info.preformatted_text( text = merged.as_str() )
  phil_params = merged.extract()
  caparms = phil_params.chain_to_alignment_matching

  info.heading( text = "Residue alignment", level = 1 )
  strials = [
    sculptor.AlignmentSequence(
      name = alnobj.name,
      alignment = alignment,
      index = i,
      )
    for i in range( alignment.multiplicity() )
    ]

  try:
    ( strial, residues, rescodes ) = sculptor.chain_alignment(
      chain = chain,
      mmt = mmtype.PROTEIN,
      trials = strials,
      params = caparms,
      logger = logger,
      )

  except sculptor.SculptorError as e:
    info.highlight( text = "Aligner: %s" % e )
    raise Sorry("Could not match chain with alignment")

  from phaser import chisellib
  chainalignment = chisellib.ChainAlignment(
    target = sculptor.reformat_alignment_sequence(
      sequence = strial.target(),
      gap = strial.gap(),
      ),
    model = sculptor.reformat_alignment_sequence(
      sequence = strial.sequence(),
      gap = strial.gap(),
      ),
    residues = residues,
    rescodes = rescodes,
    )
  info.field_percentage(
    name = "Sequence identity",
    value = 100.0 * chainalignment.cumulative_identity_fraction(),
    digits = 2,
    )

  info.heading( text = "Homology modelling and error estimation", level = 1 )
  import time
  start = time.time()
  info.paragraph_begin( indent = 2 )
  hmparams = phil_params.homology_modelling

  try:
    with logger.child( shift = 0 ) as sublogger:
      sculptor.estimate_error_from_structure(
        chain = chain,
        chainalignment = chainalignment,
        mmt = mmtype.PROTEIN,
        min_sequence_coverage = 0.0,
        homology_modelling_params = hmparams,
        output_file_prefix = params.prefix,
        logger = sublogger,
        )

  except sculptor.ErrorEstimationFailure as e:
    raise Sorry(e)

  info.paragraph_end()
  results = [
    description_for[ "targetid" ],
    description_for[ "modelid" ],
    "%s-model.pdb" % params.prefix,
    "%s-proq.pdb" % params.prefix,
    "%s.err" % params.prefix,
    time.time() - start,
    ]

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return results


if __name__ == "__main__":
  parser = get_parser()

  with benchmark.get_logger() as logger:
    run( params = parser.parse_args(), logger = logger )
