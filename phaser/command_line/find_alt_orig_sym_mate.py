from __future__ import absolute_import, division, print_function

# LIBTBX_SET_DISPATCHER_NAME phenix.find_alt_orig_sym_mate
# LIBTBX_SET_DISPATCHER_NAME phenix.famos
# LIBTBX_SET_DISPATCHER_NAME phaser.find_alt_orig_sym_mate
# LIBTBX_SET_DISPATCHER_NAME phaser.famos

from phaser import AlterOriginSymmate
from types import *

from iotbx.cli_parser import CCTBXParser
from libtbx.utils import multi_out, show_total_time

from libtbx import runtime_utils
import sys
import os, io
import random, string


def run(args):
  # create parser
  logger = multi_out()
  logger.register('console_output', sys.stdout)

  parser = CCTBXParser(program_class = AlterOriginSymmate.Program, logger=logger)
  namespace = parser.parse_args(sys.argv[1:])

  # start program
  print('Starting job', file=logger)
  print('='*79, file=logger)
  task = AlterOriginSymmate.Program(
    parser.data_manager, parser.working_phil.extract(), logger=logger)

  # validate inputs
  task.validate()

  # run program
  fnames = task.run()

  # stop timer
  print('', file=logger)
  print('='*79, file=logger)
  print('Job complete', file=logger)
  show_total_time(out=logger)
  return fnames


if __name__ == "__main__":
  ret = run(sys.argv[1:])
