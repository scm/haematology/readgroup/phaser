from __future__ import print_function

class SequenceError(Exception):
  """
  Module exception
  """

def localize(sequence, width, padding):
  """
  Groups values and pads them to a fixed length
  """

  assert 0 <= width
  padding = [ padding ] * width

  from collections import deque
  from itertools import chain

  padded = chain( padding, sequence, padding )
  d = deque( maxlen = 2 * width + 1 )

  # assert 2 * maxlen <= len( padded ), but iterators have no len
  for i in range( 2 * width ):
    d.append( next(padded) )

  for v in padded:
    d.append( v )
    yield d


def localize2(sequence, width, l_padding, r_padding):
  """
  Groups values and pads them to a fixed length
  """

  assert 0 <= width
  from collections import deque
  from itertools import chain

  padded = chain( [ l_padding ] * width, sequence, [ r_padding ] * width )
  d = deque( maxlen = 2 * width + 1 )

  # assert 2 * maxlen <= len( padded ), but iterators have no len
  for i in range( 2 * width ):
    d.append( next(padded) )

  for v in padded:
    d.append( v )
    yield d


def split(sequence, consecutivity):
  """
  Breaks up sequences where elements are not consecutive according to a
  property
  """

  iterable = iter( sequence )
  try:
    previous = next(iterable)
  except StopIteration:
    return

  contiguous = [ previous ]

  for elem in iterable:
    if consecutivity( previous, elem ):
      contiguous.append( elem )
      previous = elem
      continue

    yield contiguous
    previous = elem
    contiguous = [ elem ]

  yield contiguous


def convolution(sequence, pattern, padding):

  assert len( pattern ) % 2 == 1
  localized = localize(
    sequence = sequence,
    width = ( len( pattern ) - 1 ) // 2,
    padding = padding
    )

  return ( sum( [ a * b for ( a, b ) in zip( subseq, pattern ) ] )
    for subseq in localized )


def convolution2(sequence, pattern, l_padding, r_padding):

  assert len( pattern ) % 2 == 1
  localized = localize2(
    sequence = sequence,
    width = ( len( pattern ) - 1 ) // 2,
    l_padding = l_padding,
    r_padding = r_padding,
    )

  return ( sum( [ a * b for ( a, b ) in zip( subseq, pattern ) ] )
    for subseq in localized )


def triangular_blurring_pattern(window):

  return list(range( 1, window + 1 )) + [ window + 1 ] + list(range( window, 0, -1 ))


def triangular_pattern_for_interval(length):

  half = length // 2

  if length % 2 == 0:
    centre = []

  else:
    centre = [ half + 1 ]

  return list(range( 1, half + 1 )) + centre + list(range( half, 0, -1 ))


def below_partitioning_threshold_search(sequence, target, infinitesimal = 0.01):

  assert sequence
  assert 0 <= target <= len( sequence )

  ordered = sorted( set( sequence ) )
  previous = ( ordered[0], 0 )

  for threshold in ordered[1:]:
    current = ( threshold, sum( 1 for s in sequence if s < threshold ) )

    if target < current[1]:
      assert previous[1] <= target
      break

    previous = current

  else:
    current = ( ordered[-1] + infinitesimal, len( sequence ) )

  if target - previous[1] <= current[1] - target:
    return previous[0]

  else:
    return current[0]


def above_equal_partitioning_threshold_search(sequence, target, infinitesimal = 0.01):

  return below_partitioning_threshold_search(
    sequence = sequence,
    target = len( sequence ) - target,
    infinitesimal = infinitesimal,
    )


def gapped_sequence_element_to_gapped_sequence_position(sequence, gap = None):
  """
  Associates sequence elements with sequence position
  """

  return dict( ( e, i ) for ( i, e ) in enumerate( sequence ) if e != gap )


def gapped_sequence_position_to_sequence_position(sequence, gap = None):
  """
  Associates gapped sequence position with sequence position (excluding gaps)
  """

  import itertools
  counter = itertools.count()

  return [ next(counter) if e != gap else None for e in sequence ]


class Element(object):
  """
  Allows sequence elements to behave similarly to integer indices

  Arithmetic:
  o __add__, __radd__: calculate element nth following element
  o __sub__: calculate distance between two elements

  Comparison:
  o __cmp__(other)
  o __hash__()
  """

  def __init__(self, sequence, index):

    self.sequence = sequence
    self.set_index( target = index )


  def previous(self):

    return self.__class__( sequence = self.sequence, index = self.index - 1 )


  def following(self):

    return self.__class__( sequence = self.sequence, index = self.index + 1 )


  def is_first(self):

    return self.index == 0


  def is_last(self):

    return self.index == len( self.sequence ) - 1


  def __add__(self, other):

    return self.__class__( sequence = self.sequence, index = self.index + other )


  def __radd__(self, other):

    return self.__class__( sequence = self.sequence, index = self.index + other )


  def __sub__(self, other):

    if not self.is_comparable( other = other ):
      raise TypeError("Elements not comparable")

    return self.index - other.index


  def __cmp__(self, other):

    if not self.is_comparable( other = other ):
      raise TypeError("Elements not comparable")

    cd = lambda x, y: (x > y) - (x < y)

    return cd( self.index, other.index )

  def __lt__(self, other): return self.__cmp__(other) < 0
  def __le__(self, other): return self.__cmp__(other) <= 0
  def __eq__(self, other): return self.__cmp__(other) == 0
  def __ne__(self, other): return not self.__eq__(other)
  def __gt__(self, other): return self.__cmp__(other) > 0
  def __ge__(self, other): return self.__cmp__(other) >= 0

  def __hash__(self):

    return hash( self.index )


  def __call__(self):

    return self.sequence[ self.index ]


  def __int__(self):

    return self.index


  # Utility
  def set_index(self, target):

    if target < 0 or len( self.sequence ) <= target:
      raise IndexError("Element out of range")

    self.index = target


  def is_comparable(self, other):

    return isinstance( other, self.__class__ ) and self.sequence is other.sequence


  @classmethod
  def Begin(cls, sequence):

    return cls( sequence = sequence, index = 0 )


  @classmethod
  def End(cls, sequence):

    return cls( sequence = sequence, index = len( sequence ) - 1 )


def sequence_consecutivity(lhs, rhs):

  return lhs + 1 == rhs


class Segment(object):
  """
  A sequence expressed as the two end points
  """

  def __init__(self, begin, end):

    if end < begin:
      raise ValueError("end < begin")

    self.begin = begin
    self.end = end


  @property
  def first(self):

    return self.begin


  @property
  def last(self):

    return self.end


  def __contains__(self, item):

    return self.begin <= item and item <= self.end


  def __iter__(self):

    current = self.begin

    while current != self.end:
      yield current
      current += 1

    yield current


  def __getitem__(self, index):

    if isinstance( index, slice ):
      ( start, stop, step ) = index.indices( len( self ) )

      if step != 1:
        raise ValueError("Segments can only be sliced with step=1")

      if stop <= start:
        raise ValueError("Slice refers to empty segment")

      return self.__class__( begin = self[ start ], end = self[ stop - 1 ] )

    if 0 <= index:
      return self.begin + index

    else:
      return self.end + ( index + 1 )


  def __len__(self):

    return self.end - self.begin + 1


  def __eq__(self, other):

    return self.begin == other.begin and self.end == other.end


  def __ne__(self, other):

    return not ( self == other )


  def __str__(self):

    if self.begin == self.end:
      return str( self.begin )

    else:
      return "%s -> %s" % ( self.begin, self.end )


  @classmethod
  def segmentize(cls, sequence, consecutivity):
    """
    Calculates segments from a selection
    """

    for segment in split( sequence = sequence, consecutivity = consecutivity ):
      yield cls( begin = segment[0], end = segment[-1] )


def chunk_sequence(sequence, chunksize):
  """
  Chunks segments
  """

  assert 0 < chunksize

  length = len( sequence )
  count = length // chunksize + 1

  stride = float( length ) / count
  start = 0

  for index in range( 1, count ):
    end = int( round( index * stride ) )
    yield sequence[ start :  end ]
    start = end

  yield sequence[ start : ]


class SegmentMapping(object):
  """
  Finds two ends of a segment that are present in an incomplete mapping
  """

  def __init__(self, indexer):

    self.indexer = indexer


  def __call__(self, segment):

    for elem in segment:
      try:
        start = self.indexer[ elem ]

      except KeyError:
        continue

      break

    else:
      raise SequenceError("Entire segment not indexed")

    for elem in reversed( segment ):
      try:
        end = self.indexer[ elem ]

      except KeyError:
        continue

      break

    else:
      raise AssertionError("Reverse search has not found forward result")

    return ( start, end )
