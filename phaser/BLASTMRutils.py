
from __future__ import print_function

import os, re, glob, sys, zipfile, fnmatch, logging
import phaser.test_phaser_parsers
from phaser import matching
from phaser import template
from cctbx import sgtbx
from cctbx import uctbx
import scitbx.math
import iotbx.pdb
import pickle
import phaser

from phaser.utils2 import *



# workaround until ListEdit.cc is fixed
def AreMatching(mtzfname, templsolfnames, mrsolfname, modelfname, mrresolution=2.5):
    mtz = iotbx.mtz.object(mtzfname)
    myparm = mtz.crystals()[0].unit_cell_parameters()
    ucell = uctbx.unit_cell(parameters = myparm)
    space_group_hall = mtz.space_group_info().group()
    crystal = matching.CrystalInfo( space_group = space_group_hall, cell=ucell)
    mroot = iotbx.pdb.input(modelfname).construct_hierarchy()
    ensemble = matching.EnsembleInfo.from_hierarchy( root = mroot )

    matches = []
    foundit = False
# allow reading an array of template solution files
    templsols = TryParseSolfile([templsolfnames[0]])
    if templsols == False:
        return (foundit, matches)

# having read the first template solution file now extend the mrsolution object with
# solutions from additional soution files
    for i, templsolfname in enumerate(templsolfnames):
        if i == 0:
            continue

        if not TryParseSolfile([templsolfname]):
            continue

        templparser = phaser.test_phaser_parsers.SolutionFileParser(open(tempsolfname, "rt"))
        othersols = templparser.get_dot_sol()
        for sol in othersols:
            templsols.append(sol)
# all template solutions from all template solution files are now stored in templsols
#
    reference = []
    for tmpls in templsols:
        for peak in tmpls.KNOWN:
            reference.append(
                matching.Labelled.phaser_mr_solution(
                    rotation = peak.getR(),
                    translation = peak.getFracT(),
                    crystal = crystal,
                    ensemble = ensemble,
                    dmin = mrresolution
                    )
                )

    ref_origins = [ matching.OriginSearchEntity( r ) for r in reference ]
#
# read the mr solution file
    mrsols = TryParseSolfile([mrsolfname])
    if mrsols == False:
        return (foundit, matches)

    for ref_origin in ref_origins:
        match = []
        for sol in mrsols:
            trial = [
                matching.Labelled.phaser_mr_solution(
                    rotation = peak.getR(),
                    translation = peak.getFracT(),
                    crystal = crystal,
                    ensemble = ensemble,
                    dmin = 2.5
                    )
                for peak in sol.KNOWN
                ]
# try matching each template solution with each mr solution
            m = matching.phaser_style_equivalence_match(
              left = [ref_origin],
              right = [ matching.OriginSearchEntity( t ) for t in trial ]
              )
            if m:
                foundit = True
            match.append( m)
        matches.append(match)
#
    return (foundit, matches)



def posixslash(fname):
    s = fname.split("\\")
    ufname= "/".join(s)
    return ufname


def TryRead(rankedfiles, ziparchive=None):
    mstr = None
    if ziparchive:
        for fname in rankedfiles:
            try:
                fobj = ziparchive.open(fname, mode='rU')
                mstr = eval(fobj.read())
                break
            except KeyError as m:
                try:# unix style separators if not found with windows style separators
                    fobj = ziparchive.open(posixslash(fname), mode='rU')
                    mstr = eval(fobj.read())
                    break
                except KeyError as m:
                    mstr = None
    else:
        for fname in rankedfiles:
            try:
                if os.path.exists(str(fname)):
                    mstr = eval(open(fname, "r").read())
                    #print fname
            except Exception:
                mstr = None
                continue
    return mstr



def TryRawRead(rankedfiles, defstr=None, ziparchive=None):
    mstr = defstr
    if ziparchive:
        for fname in rankedfiles:
            try:
                fobj = ziparchive.open(fname, mode='rU')
                mstr = fobj.read()
                break
            except KeyError as m:
                try: # unix style separators if not found with windows style separators
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    fobj = ziparchive.open(posixslash(fname), mode='rU')
                    mstr = fobj.read()
                    break
                except KeyError as m:
                    mstr = defstr
    else:
        for fname in rankedfiles:
            try:
                if os.path.exists(str(fname)):
                    mstr = open(fname, "rb").read()
                    #print fname
            except Exception:
                mstr = defstr
                continue
    return mstr



def TryParseSolfile(rankedsolfiles, usedfile=None, ziparchive=None):
    ret = None
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    try:
        for solfname in rankedsolfiles:
            if ziparchive:
                try:
                    fobj = ziparchive.open(solfname, mode='rU')
                    mstr = fobj.read()
                    mrparser = phaser.test_phaser_parsers.SolutionFileParser.parse_string(mstr)
                    ret = mrparser
                    if usedfile:
                        usedfile[0] = solfname
                    break
                except KeyError as m:
                    try:# unix style separators if not found with windows style separators
                        fobj = ziparchive.open(posixslash(solfname), mode='rU')
                        mstr = fobj.read()
                        mrparser = phaser.test_phaser_parsers.SolutionFileParser.parse_string(mstr)
                        ret = mrparser
                        if usedfile:
                            usedfile[0] = solfname
                        break
                    except KeyError as m:
                        ret = []
            else:
                if os.path.exists(str(solfname)):
                    mrparser = phaser.test_phaser_parsers.SolutionFileParser(open(solfname, "rt"))
                    ret = mrparser
                    if usedfile:
                        usedfile[0] = solfname
                    #print usedfile
                    break
    except Exception as m:
        print(m)
        ret = None
    finally:
        return ret



class BLASTMRutils():
#    def __init__(self, topfolder, parent, completedrunfname="", mylock = None):
    def __init__(self, topfolder, parent, modeldir, partmodels, solutionfname = "",
      completedrunfname="", mylock = None):
        self.topfolder = topfolder
        self.completedrunfname = completedrunfname
        self.mylock = mylock
        self.parent = parent
        self.ziparx = None
        if parent.zipfname:
            try:
                self.ziparx = zipfile.ZipFile(parent.zipfname)
                self.parent.logger.log(logging.INFO, "\nReading archive " + parent.zipfname + "\n")
            except Exception as m:
                self.parent.logger.log(logging.INFO, "\nError reading zip archive " + parent.zipfname + "\n" + str(m) + "\n")
                self.ziparx = None
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        self.OCCresidwindows = parent.OCCresidwindows
        self.partmodels = partmodels
        self.solutionfname = solutionfname
        self.modeldir = modeldir
# array that'll hold MR solution, template solution, FTF_scrores data, VRMS and LLG float values
        self.mrsol = [None,None,None,None,None,None,None,None,[None,None],[]]
        self.sortedpartmodel = ",".join(sorted(self.partmodels.split(",")))
        self.sortedpartmodelname = "{" + self.sortedpartmodel + "}"
        self.modeldirname = "{" + self.modeldir + "}"
        self.templmodeldir = os.path.join(self.topfolder,"TemplateSolutions", self.sortedpartmodel)
        self.templmodeldir = os.path.relpath(self.templmodeldir, self.topfolder)
        self.templunsortmodeldir = os.path.join(self.topfolder,"TemplateSolutions", self.modeldir)
        self.templunsortmodeldir = os.path.relpath(self.templunsortmodeldir, self.topfolder)
        self.workpath = os.path.join(self.topfolder, "MR", self.modeldir)
        self.workpath = os.path.relpath(self.workpath, self.topfolder)
        self.calclabelmodelsname = self.parent.calclabel + self.modeldirname + self.partmodels
        self.occcalclabelname = self.parent.calclabel + "CalcOCC_1"
        self.SorryNoSolutionrecmp = re.compile(r"\r*\n\s*(Sorry - No solution)\r*\n",
          re.DOTALL)
        self.logfname = os.path.join(self.workpath, self.calclabelmodelsname + ".log")
        self.mrsol[8][0] = TryRawRead([self.logfname], ziparchive=self.ziparx)
        self.occlogfname = os.path.join(self.workpath, self.occcalclabelname + ".log")
        self.mrsol[8][1] = TryRawRead([self.occlogfname], ziparchive=self.ziparx)




    def IsthereaPartialSolution(self):
# match the templatesolution folder which is named as an alphanumerically sorted pdbs
# Returns 1 if template = solution otherwise 0. Returns -1 if solution file doesn't exist
        rootname = os.path.join(self.workpath, self.calclabelmodelsname)
# get LLG values from text files that holds float values rather than the int values in the sol files
        CorrCoeffname0 = os.path.join(self.workpath,self.parent.calclabel + "CalcCC_" + self.modeldirname + self.partmodels + ".txt")
        CorrCoeffname = os.path.join(self.workpath,self.parent.calclabel + "CalcCC_" + self.modeldirname + self.partmodels + "_dict.txt")
        #CorrCoeffname = os.path.join(self.workpath,"CalcCC_" + self.modeldirname + self.partmodels + ".txt")
        CorrCoeffname1 = os.path.join(self.workpath, self.parent.calclabel + "CalcCC_" + self.modeldirname + self.partmodels + ".txt")
        CorrCoeffname2 = os.path.join(self.workpath,"CalcCC_RNP_VRMS.txt")
        CorrCoeffname3 = os.path.join(self.workpath,"CalcCC__" + self.modeldirname + self.partmodels + ".txt")
        realLLGsVRMSfname = os.path.join(self.workpath, self.parent.calclabel + "LLG" + self.modeldirname +  self.partmodels + ".txt")
        #realLLGsVRMSfname1 = os.path.join(workpath,"LLG_RNP_VRMS" + modeldirname +  partmodels + ".txt")
        #realLLGsVRMSfname2= os.path.join(workpath,"LLGsFromRNP_VRMS.txt")
        #realLLGsVRMSfname3 = os.path.join(workpath,"LLG_RNP_VRMS.txt")
        CorrCoeffullresfname = os.path.join(self.workpath,"CalcCC_FullResRNP_VRMS_FullRes.txt")
        #realLLGsVRMSfullresfname = os.path.join(workpath,"LLG_FullRes_RNP_VRMS.txt")
        realLLGsfname1 = os.path.join(self.workpath,"LLGsFrom" + self.calclabelmodelsname + ".txt")
        realLLGsfname = os.path.join(self.workpath, self.parent.calclabel +"LLG" + self.modeldirname + self.partmodels + ".txt")
# override default value if supplied
        fname = os.path.join(self.workpath, self.solutionfname) if self.solutionfname != "" else ""
        fname0 = os.path.join(self.workpath, self.parent.calclabel + "_VRMS" + self.modeldirname +  self.partmodels + ".sol")
        fname1 = os.path.join(self.workpath, self.calclabelmodelsname + ".sol")
        pklfname = os.path.join(self.workpath, self.calclabelmodelsname + ".pkl")
        #logfname = os.path.join(self.workpath, self.calclabelmodelsname + ".log")
        #fname2 = os.path.join(workpath, self.parent.calclabel + "RNP_VRMS.sol")
        #fname3 = os.path.join(workpath, "RNP_VRMS" + modeldirname +  partmodels + ".sol")
        #fname4 = os.path.join(workpath, "RNP_VRMS.sol")
# allocate storage for the LLG float values
# 1st and 2nd are arrays for each MR solution. 3rd is the single value from RNP
# 4th and 5th are arrays for correlation coefficients
        self.mrsol[3] = [[None],[None],[None], None, None, None, None, [None]]
        #print "1.mrsol[3][6] = " +str(self.mrsol[3][6])
# store phaser solution in the 0th entry of the mrsol array
# initially solution files were given without vrms refinement.
# using RNP_VRMS.sol provides the vrms values in addition to the phaser solution
        #mrobj = pickle.loads(TryRawRead([pklfname], ziparchive=self.ziparx))
        pklstr = TryRawRead([pklfname], ziparchive=self.ziparx)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if pklstr:
            self.mrsol[0] = pickle.loads(pklstr)
        else:
            mrsolfname = [None]
            self.mrsol[0] = TryParseSolfile([fname, fname0, fname1], mrsolfname,
             ziparchive=self.ziparx)
            print("\nUsing %s instead of %s" % (mrsolfname, pklfname))

#import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        #if not self.mrsol[0]:
        #    return -1 # otherwise bail out
# get ELLGs if present
        fname = os.path.join(self.workpath,"ELLG_*_%s.txt" %self.modeldir)
        ellgfname = glob.glob(fname)[0] if len(glob.glob(fname)) > 0 else ""
        ELLGtbl = ""
        if os.path.exists(ellgfname):
            ELLGtbl = open(ellgfname, "r").read()

        #templfname0 = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC_1.sol")
        pkltemplfname = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC.pkl")
        templfname0 = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC.sol")
        templfname = os.path.join(self.templmodeldir, self.parent.calclabel + "RNP" \
         + self.sortedpartmodelname + ".superposed.sol")
        templfname1 = os.path.join(self.templmodeldir, "RNP" + self.sortedpartmodelname \
         + ".superposed.sol")
        realtemplLLGfname = os.path.join(self.templmodeldir, "LLGsFromRNP"
         + self.sortedpartmodelname + ".txt")
        templCCocclocalfname = os.path.join(self.templmodeldir, self.parent.calclabel
         + "RNPCalcOCClocalCCSol0.txt")

        templCorrCoeffname = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC_"
         + self.sortedpartmodelname + ".superposed.txt")
        templCorrCoeffname1 = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC_"
         + self.modeldirname + ".superposed.txt")
        templCorrCoeffname0 = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC_"
         + self.sortedpartmodelname + ".superposed_dict.txt")

        CCocclocals = []
        defCCocclocals = []
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        if self.mrsol[0]:
            if hasattr(self.mrsol[0], "numSolutions"):
                solrng = range(self.mrsol[0].numSolutions())
            else:
                solrng = range(len(self.mrsol[0].mr_sets))
            for nsol in solrng:
                CCocclocals.append([])
    # read in tables of local CCs with varying window sizes for OCC
                for nres in self.OCCresidwindows:
                    CCocclocalfname = os.path.join(self.workpath, self.parent.calclabel
                     + "CalcOCC_%d_nres%dlocalCC.txt" %(nsol, nres))
                    tbl = TryRawRead([CCocclocalfname], defstr="NULL", ziparchive=self.ziparx)
                    CCocclocals[nsol].append(tbl)
    # and the local CCs with the default OCC window size
                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                CCocclocalfname = os.path.join(self.workpath, self.parent.calclabel
                 + "CalcOCClocalCCSol%d.txt" %nsol)
                defCCocclocals.append(TryRawRead([CCocclocalfname], defstr="NULL", ziparchive=self.ziparx))
            self.mrsol[3][5] = defCCocclocals
            self.mrsol[3][1] = CCocclocals
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
# read in tables of local CCs with varying window sizes for OCC
        tmplCCocclocals = []
        for nres in self.OCCresidwindows:
            tmplCCocclocalfname = os.path.join(self.templmodeldir, self.parent.calclabel
             + "RNPCalcOCC_1_nres%dlocalCC.txt" %nres)
            tbl = TryRawRead([tmplCCocclocalfname], defstr="NULL", ziparchive=self.ziparx)
            tmplCCocclocals.append(tbl)
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
# and the local CCs with the default OCC window size
        self.mrsol[3][7] = tmplCCocclocals
        self.mrsol[3][3] = TryRawRead([templCCocclocalfname], ziparchive=self.ziparx)
        self.mrsol[3][4] = TryRead([CorrCoeffname, CorrCoeffname0], ziparchive=self.ziparx) #, CorrCoeffname, CorrCoeffname1, CorrCoeffname2, CorrCoeffname3])
        self.mrsol[3][6] = TryRead([templCorrCoeffname0, templCorrCoeffname,templCorrCoeffname1], ziparchive=self.ziparx)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        vrmsllgfname = os.path.join(self.templmodeldir, self.parent.calclabel + "RNPCalcCC_1.VRMS_LLG.txt")
        tbl = TryRawRead([vrmsllgfname], ziparchive=self.ziparx)
        if tbl:
            mathstr = "{"
            for e in tbl.split("\n"):
                f = e.split()
                if len(f) > 2:
                    mathstr += "{%s, %s}, " %(f[2], f[3])
            mathstr += "}"
            self.mrsol[4] = mathstr

        pklstr = TryRawRead([pkltemplfname], ziparchive=self.ziparx)
        if pklstr:
            self.mrsol[1] = pickle.loads(pklstr)
        #usedfile=[]
        #self.mrsol[1] = TryParseSolfile([templfname0, templfname, templfname1],
        # usedfile, ziparchive=self.ziparx)

        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        #print "[templfname, templfname1] = " + str([templfname, templfname1])
        #print "usedfile = " + str(usedfile)
        #if len(self.mrsol[1]) < 1:
        #    return -1

# We want the vrms values for the refinement with the full set of reflections
# Get these by storing the solution of the this refinement into the 7th entry of the mrsol array
        solfname = os.path.join(self.workpath, self.parent.calclabel + "RNP_VRMS_FullRes.sol")
        obj = TryParseSolfile([solfname], ziparchive=self.ziparx)
        if obj:
            self.mrsol[7] = obj.get_dot_sol()
# Want Fsol and Bsol if these were calculated
# Open the BsolFsolRNPcalc_1RLH_2D16_A.txt file. TargetPDBid isn't defined here so we use a wildcard
        #Fsolfname = os.path.join(workpath, "BsolFsolRNPcalc.sol")
        #self.mrsol[8] = TryParseSolfile([Fsolfname])
#        self.mrsol[8] = TryRawRead([logfname], ziparchive=self.ziparx)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        return 0



    def NoticeCompletedPhaserRun(self,workdir, partmodels,
                     nPartSolWithThisStrategy):
        bPartSolWithThisStrategy = True
        if nPartSolWithThisStrategy < 1:
            bPartSolWithThisStrategy = False

        if self.mylock != None:
            self.mylock.acquire()

        completedruns = open(self.completedrunfname,"a")
        completedruns.write("%s {%s} %s\n" %(workdir, partmodels,
                     str(bPartSolWithThisStrategy)))
        completedruns.close()

        if self.mylock != None:
            self.mylock.release()



#    def PhaserRanPartModelsBefore(self,founddir,partmodels,bsolved,doneruns=""):
    def PhaserRanPartModelsBefore(self, bsolved):
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
#        """
        os.chdir("MR")
        mrfolders = glob.glob("*")

        for mrfolder in mrfolders:
            n = mrfolder.find(self.partmodels)
            #if n > -1:
            #    founddir.append(mrfolder)

            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        os.chdir(self.topfolder)
        bsolved[0] = True
        bfullsolution = len(self.SorryNoSolutionrecmp.findall(self.mrsol[8][0])) == 0 if self.mrsol[8][0] else None
        return bfullsolution
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

        """
        if self.completedrunfname != "" and os.path.exists(self.completedrunfname) == False:
            return False

        if self.mylock != None:
            self.mylock.acquire()

        if doneruns == "":
            if os.path.exists(self.completedrunfname):
                completedruns = open(self.completedrunfname,"r")
                doneruns = completedruns.read()
                completedruns.close()
            else:
                return True
# Hack: to allow picking up template solutions of first component contrive these from the doneruns file
        firstruns = ""
        for e in doneruns.split("\n"):
            m = re.search(r"(.*)(\{.*\})(.*)", e, re.MULTILINE)
            if m is not None:
                for partmdl in m.group(1).strip().split(","):
                    firstruns += m.group(1) + "{" + partmdl + "}" + m.group(3)

        doneruns += firstruns

        if self.mylock != None:
            self.mylock.release()
# assuming the file looks like
# 1I1A_D,3GIZ_H,1NC2_A {1I1A_D} True
# 1W72_H,1NC2_A,1I1A_D {1W72_H} False
# 1I1A_D,1W72_H,1NC2_A {1I1A_D,1W72_H} True
#
        bsolved[0] = False
        m = doneruns.find("{" + partmodels + "}", 0)
        if m > 0:
# note what calculation was used for searching on the partial model
            n = doneruns.rfind("\n",0,m)
            if n == -1 and m > 0:
                n = 0  # found it in the very first line
            #print "partmodels= %s m= %d, n= %d, doneruns[n:m]= %s" % (partmodels,m,n,doneruns[n:m])
            founddir.append( doneruns[n:m].split()[0])

            n = doneruns.find("\n",m)
        # find the bool value from "{1I1A_D} True"
            if doneruns[m:n].split(" ")[1] == "True":
                bsolved[0] = True
            else:
                bsolved[0] = False

            return True
        else:
            return False

        """
