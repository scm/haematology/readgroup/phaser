from __future__ import print_function

from functools import reduce

class FormattedStream(object):
  """
  Keeps stream and formatting attributes
  """

  def __init__(self, stream, width):

    self.stream = stream
    self.maxwidth = width
    self.formatters = []


  def push(self, formatter):

    if self.width + formatter.width_modifier <= 0:
      raise RuntimeError("Stream width zero or below")

    self.formatters.append( formatter )


  def pop(self):

    self.formatters.pop()


  def top(self):

    return self.formatters[-1]


  @property
  def width(self):

    return self.maxwidth + sum( f.width_modifier for f in self.formatters )


  def format(self, paragraph):

    ( formatted, streamwidth ) = reduce(
      self.apply,
      reversed( self.formatters ),
      ( paragraph, self.width ),
      )

    return formatted


  def write(self, text, wrapper):

    paragraph = self.format(
      paragraph = wrapper( text = text, maxwidth = self.width ),
      )

    for l in paragraph.lines:
      self.stream.write( l.rstrip() )
      self.stream.write( "\n" )


  def rawwrite(self, text):

    self.stream.write( text )


  def flush(self):

    self.stream.flush()


  @staticmethod
  def apply(data, formatter):

    return formatter( paragraph = data[0], streamwidth = data[1] )


# Console logger
class Console(object):
  """
  Output goes to a terminal
  """

  def __init__(self, stream, width, error, warning, info, verbose, debug):

    self.stream = FormattedStream( stream = stream, width = width )
    self._error = error
    self._warning = warning
    self._info = info
    self._verbose = verbose
    self._debug = debug


  def error(self, msg):

    self._error( stream = self.stream, message = msg )


  def warning(self, msg):

    self._warning( stream = self.stream, message = msg )


  def info(self, msg):

    self._info( stream = self.stream, message = msg )


  def verbose(self, msg):

    self._verbose( stream = self.stream, message = msg )


  def debug(self, msg):

    self._debug( stream = self.stream, message = msg )


  @staticmethod
  def detail_rendering(stream, message):

    renderer = message.detail_renderer()
    renderer( stream = stream )


  @staticmethod
  def normal_rendering(stream, message):

    renderer = message.normal_renderer()
    renderer( stream = stream )


  @staticmethod
  def no_rendering(stream, message):

    pass


  @classmethod
  def Error(cls, stream, width):

    return cls(
      stream = stream,
      width = width,
      error = cls.normal_rendering,
      warning = cls.no_rendering,
      info = cls.no_rendering,
      verbose = cls.no_rendering,
      debug = cls.no_rendering,
      )

  @classmethod
  def Warning(cls, stream, width):

    return cls(
      stream = stream,
      width = width,
      error = cls.detail_rendering,
      warning = cls.normal_rendering,
      info = cls.no_rendering,
      verbose = cls.no_rendering,
      debug = cls.no_rendering,
      )

  @classmethod
  def Info(cls, stream, width):

    return cls(
      stream = stream,
      width = width,
      error = cls.detail_rendering,
      warning = cls.detail_rendering,
      info = cls.normal_rendering,
      verbose = cls.no_rendering,
      debug = cls.no_rendering,
      )

  @classmethod
  def Verbose(cls, stream, width):

    return cls(
      stream = stream,
      width = width,
      error = cls.detail_rendering,
      warning = cls.detail_rendering,
      info = cls.detail_rendering,
      verbose = cls.normal_rendering,
      debug = cls.no_rendering,
      )

  @classmethod
  def Debug(cls, stream, width):

    return cls(
      stream = stream,
      width = width,
      error = cls.detail_rendering,
      warning = cls.detail_rendering,
      info = cls.detail_rendering,
      verbose = cls.detail_rendering,
      debug = cls.normal_rendering,
      )


  FACTORY_FOR = {
    "ERROR": Error,
    "WARNING": Warning,
    "INFO": Info,
    "VERBOSE": Verbose,
    "DEBUG": Debug,
    }


class Demote(object):
  """
  Redirect messages to one lower priority
  """

  def __init__(self, logger):

    self.logger = logger


  def error(self, msg):

    self.logger.error( msg = msg )


  def warning(self, msg):

    self.logger.warning( msg = msg )


  def info(self, msg):

    self.logger.verbose( msg = msg )


  def verbose(self, msg):

    self.logger.debug( msg = msg )


  def debug(self, msg):

    self.logger.debug( msg = msg )
