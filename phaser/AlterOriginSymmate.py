﻿#-------------------------------------------------------------------------------
# Name:        AlterOriginSymmate
# Purpose:
#
# Author:      Robert Oeffner
#
# Created:     26/12/2010
# Copyright:   (c) oeffner 2010
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

# This file is used by phaser\phaser\command_line\find_alt_orig_sym_mate.py

from __future__ import print_function

from types import *
import sys, os, re, time, shutil
import random, string
from iotbx import pdb
import ccp4io_adaptbx, cctbx
import phaser.test_phaser_parsers
from phaser import ensembler
import scitbx.math.euler_angles
import pickle
from phaser import phenix_interface
from libtbx.phil import parse
from libtbx.utils import Sorry
from phaser import AltOrigSymmatesEngine
from libtbx.program_template import ProgramTemplate
try:
  from phenix.program_template import ProgramTemplate
except ImportError:
  pass


class Program(ProgramTemplate):
  prog = os.getenv('LIBTBX_DISPATCHER_NAME')
  description="""
  %(prog)s moving.model = model_of_MR_solution1  fixed.model = model_of_MR_solution2
or
  %(prog)s PHILinput.txt

For different molecular replacement solutions from the same dataset %(prog)s
finds for each chain in the moving solution the copy closest to a chain in the fixed
solution with respect to all symmetry operations and alternative origin shifts
permitted by the spacegroup of the crystal necessary to best superpose the moving
chain copy onto the fixed chains.

The PHIL input specifies the MR solution files either as \"moving.model\" and
\"fixed.model\" or as the scopes, \"moving\" and \"fixed\". Both of the scopes
\"moving\" and \"fixed\" must be valid. These two scopes hold the parameter
\"model\" and the scope \"mrsolution\". An MR solution is specified by following
only one of the procedures a or b:

  (a) Assign the "moving.model" or "fixed.model" parameter to the pdb file from one
  of the MR solutions in question. This is useful for simple cases such as when
  the solution comes from the PHENIX AutoMR GUI is or when it is from a different
  MR program other than Phaser.

  (b) Specify a Phaser MR solution by assigning the file name of the solution
  file to the "moving.mrsolution.solfname" or "fixed.mrsolution.solfname"
  parameter as well as assigning the IDs of the ensembles and the corresponding
  MR models to the parameters "moving.mrsolution.ensembles.name" or
  "fixed.mrsolution.ensembles.name" and "moving.mrsolution.ensembles.xyzfname"
  or "fixed.mrsolution.ensembles.model" respectively. Multiple components
  are specified as multiple ensembles. This way of specifying solutions is useful
  for solution files produced when Phaser is run from the command line.

There is no restriction on how to define the fixed scope or the moving scope,
If space group and the unit cell dimensions is not available when for instance (b) is
used for both the moving and the fixed scopes then these need to be specified by
assigning the parameter, \"spacegroupfname\", to a PDB file with a CRYST1 record or
to a reflection data file containing that record. Typically this would be the data
file used for the molecular replacement calculation.

Output:
  The closest match between chains in the moving section to chains in the fixed
  section is saved with the name of the reference_pdb,
  concatenated with the name of the moving.model but prepended with \"MinMLAD_\".
  A log file with the name of the reference_pdb, concatenated with the name of the
  moving.model but prepended with \"AltOrigSymMLAD_\" contains all screen output.
  All files are saved in the folder containing reference_pdb. A good match
  between two chains usually have a MLAD value less than 1.5
  whereas a bad match usually have a value larger than 2. This is a rule of thumb
  and exceptions do occur.

If a Phaser MR solution is supplied, %(prog)s
iterates through each solution and does the respective translation and rotation of
model_pdb as to realize each MR solution as a temporary pdb file. These are in turn
used as moving or as fixed pdb files.

For poor homologues or MR solutions try using \"use_all_SSM=True\".

Algorithm:
  %(prog)s computes configurations by looping over all
  symmetry operations and alternative origin shifts. These are computed using
  procedures outlined in R. W. Grosse-Kunstleve, Acta Cryst. 1999, A55, 383-395.
  An alignment between pairs of C-alpha atoms of moving.model and fixed.model is
  computed using SSM (E. Krissinel* and K. Henrick, Acta Cryst. 2004. D60, 2256-2268).
  If SSM fails or if the MLAD score achieved with the SSM alignment  is larger
  than 2.0 then a sequence alignment algorithm in the CCTBX is used instead.
  To estimate the best match a distance measure between the aligned atoms is
  computed for each configuration. The mean log absolute deviation (MLAD) is
  defined as:

   MLAD(dR) = Sum( log(dr*dr/(|dr| + 0.9) + max(0.9, min(dr*dr,1)))),

  where dr is the difference vector between a pair of aligned C-alpha atoms and the sum
  is taken over all atom pairs in the alignment. MLAD is used as the distance measure.
  MLAD will downplay the contributions of spatially distant C-alpha atom pairs which
  may otherwise lead to incorrect matches when the space group has a floating origin
  and the chains tested against one another consists of large unaligned domains.

   %(prog)s will for each chain in reference_pdb find the
  smallest MLAD with a copy of each chain in moving.model for a given symmetry
  operation and alternative origin. When all chains in reference_pdb have been
  tested these copies will be saved to a file. A warning is issued if not all
  saved copies of chains in moving.model have the same origin.

  For spacegroups with floating origin the minimum MLAD is found
  by doing a Golden Sectioning minimization along the polar axis for each copy
  of chains in moving.model.

  An RMSD value is given next to MLAD value as an aid for users more accustomed
  with RMSD. It is based on the SSM alignment. Note that RMSD can misleadingly
  yield a relatively high value due to a domain movement when in fact the majority
  of the moving structure superposes well on the target structure.

Debug mode:
  Invoking the debug flag produces individual pdb files of the C-alpha atoms used for
  each SSM alignment of moving.model for each permitted symmetry operation and alternative
  origin. A gold atom is placed at the centroid of the C-alpha atoms. Similar files are
  produced for the reference_pdb. These files are stored in a subfolder named
  "AltOrigSymMatesFiles".

  If a floating origin is present in the space group a table of MLAD values is produced by
  sliding a copy of moving.model along the polar axis in the fractional interval [-0.5, 0.5]
  for each permitted symmetry operation and alternative origin.

  """ % locals()

  master_phil_str="""
AltOrigSymMates
{
    moving
      .help = "Specify either a PDB file, an MR solution or a pickle_solution."
    {
        pdb = None
          .help = "Deprecated, use model in the future."
                  "Path to a PDB file from an MR solution. Constituent chains will be moved separately."
          .type = path
          .short_caption = "Moving PDB file"
          .style = bold file_type:pdb input_file
        model = None
          .help = "Path to a PDB file from an MR solution. Constituent chains will be moved separately."
          .type = path
          .short_caption = "Moving PDB file"
          .style = bold file_type:pdb,cif input_file
        mrsolution
          .help = "MR solution produced by command-line version of Phaser"
          .style = noauto
        {
            solfname = None
              .help = "path to the solution file, i.e. the .sol file"
              .type = path
            ensembles
              .help = "Ensembles constituent in this MR solution."
                      "Typically these are chains in the PDB file."
              .multiple = True
            {
                name = None
                  .help = "ID for ensemble in the MR solution, i.e. the name after"
                          "the &quot;SOLU 6DIM ENSE&quot; string in the .sol file."
                  .type = str
                xyzfname = None
                  .help = "Deprecated, use model in the future"
                          "Path to the PDB file with atoms representing this ensemble"
                          "This is the search model used by Phaser for this ensemble"
                          "prior to solving the structure. It is not the solved structure."
                  .multiple = True
                  .type = path
                model = None
                  .help = "Path to the PDB file with atoms representing this ensemble."
                          "This is the search model used by Phaser for this ensemble"
                          "prior to solving the structure. It is not the solved structure."
                  .multiple = True
                  .type = path
            }
        }
    }

    fixed
      .help = "Specify either a PDB file, an MR solution or a pickle_solution."
    {
        pdb = None
          .help = "Deprecated, use model in the future."
                  "Path to a PDB file from an MR solution. Constituent chains will remain fixed."
          .type = path
          .short_caption = "Fixed PDB file"
          .style = bold file_type:pdb input_file
        model = None
          .help = "Path to a PDB file from an MR solution. Constituent chains will remain fixed."
          .type = path
          .short_caption = "Fixed PDB file"
          .style = bold file_type:pdb,cif input_file
        mrsolution
          .help = "MR solution produced by command-line version of Phaser"
          .style = noauto
        {
            solfname = None
              .help = "path to the solution file, i.e. the .sol file"
              .type = path
            ensembles
              .help = "Ensembles constituent in this MR solution."
                      "Typically these are chains in the PDB file."
              .multiple = True
              .style = noauto
            {
                name = None
                  .help = "ID for ensemble in the MR solution, i.e. the name after"
                          "the 'SOLU 6DIM ENSE' string in the .sol file."
                  .type = str
                xyzfname = None
                  .help =  "Deprecated, use model in the future."
                           "Path to the PDB file with atoms representing this ensemble."
                          "This is the search model used by Phaser for this ensemble"
                          "prior to solving the structure. It is not the solved structure."
                  .multiple = True
                  .type = path
                model = None
                  .help = "Path to the PDB file with atoms representing this ensemble"
                          "This is the search model used by Phaser for this ensemble"
                          "prior to solving the structure. It is not the solved structure."
                  .multiple = True
                  .type = path
            }
        }
    }

    spacegroupfname = None
      .help = "A model file or a reflection data file providing the space group"
              "and cell dimension. This is needed in the uncommon situation both"
              " moving and fixed model files do not already contain these,"
              "(CRYST1 record in a pdb file or _cell and _symmetry blocks in a cif file)."
              "Must be based on the data set sed for the MR calculation."
      .type = path
      .short_caption = "Symmetry file"
      .style = file_type:pdb,mtz,cif input_file
    chosenorigin = "Any"
      .help = "By default all allowed origin shifts are probed. Specifying"
              "'2/3, 1/3, 1/2' tells the program to only explore symmetry operations"
              "for the unit cells displaced at fractional positions (2/3, 1/3, 1/2)"
      .short_caption = "Only use origin"
      .type = str
    use_all_SSM = False
      .help = "By default only the best sequence alignment from SSM is used."
              "Setting this value to True will include other suboptimal alignments for testing."
      .type = bool
      .short_caption = "Use all SSM alignments"
    no_symmetry_operations = False
      .help = "When set to False all symmetry operations are probed during the computation."
      .short_caption = "Don't loop over symmetry operators"
      .type = bool
    movehetatms = False
      .help = "Move hetatoms close to a chain in conjunction with that chain."
              "Hetatoms are first associated with individual chains with phenix.sort_hetatms"
              "before undergoing the same spatial transformation as the chains."
      .short_caption = "Also move HETATM"
      .type = bool
    writepdbfiles = True
      .help = "Save final transformed pdb file and any intermediate pdb files realised from MR solution files"
      .type = bool
    outputfileprefix = None
      .help = "Alternative prefix for output files .pdb, .log and .sol instead of AltOrigSymMLAD_*.*"
      .type = str
    nproc = 1
      .help = "Number of CPU cores to use when submitting MR solution files with multiple solutions"
      .type = int
    quiet = False
      .help = "Set to True for silencing output when using multiple cpu cores"
      .type = bool
    debug = False
      .type = bool
    verbose = False
      .type = bool
    write_cif = False
      .type = bool
    goodmatch = 1.5
      .type = float
      .help = "Weed out poor matches with MLAD values larger than goodmatch."
    strictly_protein = True
      .type = bool
      .help = "Relax the condition that structures must only be protein chains with conventional residue names"
              "This might help solving cases where one or both pdb files are flagged as having no chains"
    workdir = None
      .type = path
      .help = "Specify if output should be written elsewhere than the current working directory."
    gui_output_dir = None
      .type = path
      .short_caption = "Output directory"
      .help = For PHENIX GUI only.
      .style = directory output_dir
   include scope libtbx.phil.interface.tracking_params
}
"""
  datatypes = ['model','phil','miller_array']
  data_manager_options = ['model_skip_expand_with_mtrix']
  known_article_ids = ['find_alt_orig_sym_mate']
  maintainers = ["Oeffner"] # not used apparently

  def validate(self):
    #time.sleep(10) # enough for attaching debugger
    # convert old phil names pdb and xyzfname to model if present
    aos = self.params.AltOrigSymMates
    if aos.moving.pdb and not aos.moving.model:
      aos.moving.model = aos.moving.pdb
    if aos.fixed.pdb and not aos.fixed.model:
      aos.fixed.model = aos.fixed.pdb
    for ens in aos.moving.mrsolution.ensembles:
      ens.model.extend(ens.xyzfname)
    for ens in aos.fixed.mrsolution.ensembles:
      ens.model.extend(ens.xyzfname)
    self.data_manager.has_models(raise_sorry=True)

  def custom_init(self):
    self.description = self.description + "\nwibble"

  def run(self):
    #time.sleep(10) # enough for attaching debugger
    myrunphil = RunPhil()
    retval = myrunphil(self) # only return the output filename
    self.results= retval.output_file_names

  def get_results(self):
    return  self.results


# for reference documentation keywords
master_phil_str = Program.master_phil_str


class RunPhil(object):
  def __init__(self):
    self.output_file_names = []
    self.ngoodmatches = 0
    self.nfixsol = 0
    self.nmovsol = 0
    self.resultsmtrxlst = []
    self.minMladRmsdMchainFchainMovstrAltOrgTrans = []

  def __call__(self, progtempl):
    #time.sleep(10)
    params = progtempl.params.AltOrigSymMates
    verbose = params.verbose
    debug = params.debug
    logstr = "%s\n" %time.asctime( time.localtime(time.time()))
    logstr += "---------------------------- Input parameters: ------------------------------\n\n"
    logstr += progtempl.get_program_phil_str(diff=(not verbose and not debug))
    logstr += "\n-----------------------------------------------------------------------------\n"
    usebestSSM = True
    if params.use_all_SSM:
      usebestSSM = False
    chosenorigin = params.chosenorigin
    usesymmop = True
    if params.no_symmetry_operations:
      usesymmop = False
    xtalsym = None
    for mdlname in progtempl.data_manager.get_model_names():
      xtalsym = progtempl.data_manager.get_model(mdlname).crystal_symmetry()
      if xtalsym:
        break
    if xtalsym==None and progtempl.data_manager.has_miller_arrays():
      xtalsym = progtempl.data_manager.get_miller_array().as_miller_arrays()[0].crystal_symmetry()
    movehetatms = params.movehetatms
    goodmatch = params.goodmatch
    strictly_protein = params.strictly_protein
    mediocre_mlad = 2.0
    fixsolfname = params.fixed.mrsolution.solfname
    fixensembles = params.fixed.mrsolution.ensembles
    fixpdb = params.fixed.model
    movsolfname = params.moving.mrsolution.solfname
    movensembles = params.moving.mrsolution.ensembles
    movpdb = params.moving.model
    write_cif = params.write_cif
    writepdbfiles = params.writepdbfiles
    wpath = params.workdir
    outputfileprefix = params.outputfileprefix
    nproc = params.nproc
    quiet = params.quiet
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    aosm = AltOrigSymmates(logstr, fixsolfname, fixensembles, fixpdb,
     movsolfname, movensembles, movpdb, xtalsym, usebestSSM, chosenorigin, usesymmop, movehetatms,
     goodmatch, strictly_protein, mediocre_mlad, verbose, debug, writepdbfiles,
     wpath, outputfileprefix, write_cif, nproc, quiet, progtempl.logger)
    self.output_file_names = aosm.Run()
    self.ngoodmatches = aosm.ngoodmatches
    self.nfixsol = aosm.nfixsol
    self.nmovsol = aosm.nmovsol
    self.minMladRmsdMchainFchainMovstrAltOrgTrans = aosm.minMladRmsdMchainFchainMovstrAltOrgTrans
    self.resultsmtrxlst = aosm.resultsmtrxlst
    return self


class AltOrigSymmates(object):
  """ Initialise object with suitable values, i.e. at least a fixed and a moving
  data element. This may be a PHIL file, PHENIX pickle file, Phaser MR solution
  or a PDB file.
  A file with spacegroup info is also required if this is not present in the PDB files.
  Call Run() to automatically iterate over pairs of PDB files when comparing
  multiple MR solution files.
  Call GetAltOrigSymmates() to do one calculation on a pair of fixed and moving PDB files
  """
  def __init__(self, logstr ="", fixsolfname = None, fixensembles = None, fixpdb = None,
      movsolfname = None, movensembles = None, movpdb = None, xtalsym = None,
      usebestSSM = True, chosenorigin = "Any", usesymmop=True, movehetatms = False,
      goodmatch = 1.5, strictly_protein = True, mediocre_mlad = 2.0, verbose = False,
      debug = False, writepdbfiles = True, wpath = None, outputfileprefix = None,
      write_cif=False, nproc=1, quiet=False, logger=None):
    self.logstr = logstr
    self.fixsolfname = fixsolfname
    self.fixensembles = fixensembles
    self.fixpdb = fixpdb
    self.movsolfname = movsolfname
    self.movensembles = movensembles
    self.movpdb = movpdb
    self.xtalsym = xtalsym
    self.usebestSSM = usebestSSM
    self.chosenorigin = chosenorigin
    self.write_cif = write_cif
    self.usesymmop = usesymmop
    self.movehetatms = movehetatms
    self.goodmatch = goodmatch
    self.strictly_protein = strictly_protein
    self.mediocre_mlad = mediocre_mlad
    self.verbose = verbose
    self.debug = debug
    self.writepdbfiles = writepdbfiles
    self.nproc = nproc
    self.wpath = os.curdir
    if wpath:
      self.wpath = wpath
    self.outputfileprefix = outputfileprefix
    self.output_file_names = []
    self.input_file_names = []
    self.xtalsym = xtalsym
    self.logger = logger
    self.ngoodmatches = 0
    self.nfixsol = 0
    self.nmovsol = 0
    self.quiet = quiet
    self.resultsmtrxlst = []
    self.minMladRmsdMchainFchainMovstrAltOrgTrans = [[0.0]]


  def Run(self):
    if self.debug and os.path.exists(AltOrigSymmatesEngine.debugfolder) == False:
      os.mkdir(AltOrigSymmatesEngine.debugfolder)
    (fixsoltxts, fixensmblspdbs, fixlogfname) = self.GetSolutionsAsPdbs(
     self.fixsolfname, self.fixensembles, self.fixpdb)
    (movsoltxts, movensmblspdbs, movlogfname) = self.GetSolutionsAsPdbs(
     self.movsolfname, self.movensembles, self.movpdb)
    logfname = os.path.join(self.wpath, "AltOrigSymMLAD_%s_%s.log" %(fixlogfname, movlogfname))
    if self.outputfileprefix:
      logfname = os.path.join(self.wpath, self.outputfileprefix + ".log")
    log = open(logfname, mode="w")
    self.logger.register('log', log)
    print(self.logstr, file=self.logger)
    fixpdbs = [self.fixpdb] if self.fixpdb else []
    if len(fixsoltxts) > 0:
      fixpdbs = self.RealizePhaserSolutions(fixsoltxts, fixensmblspdbs, fixlogfname + "_fix_")
    movpdbs = [self.movpdb] if self.movpdb else []
    if len(movsoltxts) > 0:
      movpdbs = self.RealizePhaserSolutions(movsoltxts, movensmblspdbs, movlogfname + "_mov_")
    busetwosolfiles = len(fixsoltxts) > 0 and len(movsoltxts) > 0
    enumfixpdbs = list(enumerate(fixpdbs))
    enummovpdbs = list(enumerate(movpdbs))
    self.nfixsol = len(fixpdbs)
    self.nmovsol = len(movpdbs)
    ln = self.nfixsol * self.nmovsol
    plurstr = "s" if ln > 1 else ""
    print("\n%d comparison%s to be carried out" %(ln,plurstr), file=self.logger)
    self.minMladRmsdMchainFchainMovstrAltOrgTrans = []
    minoutput_file = None
    minsolmlad = 9e99
    minbestsols = [0,0]
    mini = 0
    minj = 0
    out=sys.stdout
    minmovchainid = ""
    minfixchainid = ""
    self.resultsmtrxlst = []
    nogoodmatches = []
    outputfileprefix = self.outputfileprefix
    if self.nproc > 1:
      argsmatrix = []
      for (i,fpdb) in enumfixpdbs: # iterating over solutions
        for (j,mpdb) in enummovpdbs: # iterating over solutions
          if len(enumfixpdbs) > 1 or len(enummovpdbs) > 1 and self.outputfileprefix is not None:
            outputfileprefix="%s_%d_%d" %(self.outputfileprefix, i, j )
          argsmatrix.append( ( (mpdb,j), (fpdb,i), self.movehetatms, self.xtalsym,
                self.chosenorigin, self.usebestSSM, self.usesymmop, self.debug,
                self.wpath, self.writepdbfiles, self.goodmatch, self.strictly_protein,
                self.verbose, self.mediocre_mlad, self.nproc,
                outputfileprefix, None, None, self.quiet, self.write_cif ) )

      from libtbx import easy_mp
      for parmres in easy_mp.multi_core_run(
                                AltOrigSymmatesEngine.CalcAltOrigSymmates,
                                argsmatrix,
                                self.nproc):
        ( params, result, errstr ) = parmres
        if errstr:
          raise Sorry(errstr)
        mstr, MladRmsdMchainFchainMovstrAltOrgTrans, altorigpdbfname = result
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        self.output_file_names.append(altorigpdbfname)
        (mpdb, j) = params[0]
        (fpdb, i) = params[1]
        self.input_file_names.append((fpdb, mpdb))
        self.ngoodmatches += len(MladRmsdMchainFchainMovstrAltOrgTrans)
        if len(MladRmsdMchainFchainMovstrAltOrgTrans) < 1:
          nogoodmatches.append((mpdb, fpdb))
        print("Testing %d. fixed solution against %d. moving solution" %(i, j), file=self.logger)
        print("fixed.model = " + str(fpdb) + "\nmoving.model = " + str(mpdb) + "\n", file=self.logger)
        print(mstr, file=self.logger)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        allatoms=0
        weightMLAD=0.0
        for (mlad, rmsd, movchainid, fixchainid, altorigtransform, dummy2, dummy3, natoms) in MladRmsdMchainFchainMovstrAltOrgTrans:
# if MR solution files are used then print out list of MR solutions sorted w.r.t. MLAD values
          if busetwosolfiles:
            self.resultsmtrxlst.append((mlad, rmsd, altorigtransform, fixsoltxts[i], movsoltxts[j], i, j))
          weightMLAD += mlad * natoms # compute weighted MLAD values in order to select best match if more chains in PDB files
          allatoms += natoms
        if allatoms > 0:
          weightMLAD /= allatoms
          if minsolmlad > weightMLAD:
            minsolmlad = weightMLAD
            mini = i
            minj = j
            minoutput_file = self.output_file_names[-1]
            mininput_files = self.input_file_names[-1]
            minmovchainid = movchainid
            minfixchainid = fixchainid
            self.minMladRmsdMchainFchainMovstrAltOrgTrans = MladRmsdMchainFchainMovstrAltOrgTrans
    else:
      for (i,fpdb) in enumfixpdbs: # iterating over solutions
        for (j,mpdb) in enummovpdbs: # iterating over solutions
          print("Testing %d. fixed solution against %d. moving solution" %(i, j), file=self.logger)
          print("fixed.model = " + str(fpdb) + "\nmoving.model = " + str(mpdb) + "\n", file=self.logger)
          if (len(enumfixpdbs) > 1 or len(enummovpdbs) > 1) and self.outputfileprefix is not None:
            outputfileprefix="%s_%d_%d" %(self.outputfileprefix, i, j )
          dummy, MladRmsdMchainFchainMovstrAltOrgTrans, altorigpdbfname = self.GetAltOrigSymmates(mpdb, fpdb,
                                                  outputfileprefix = outputfileprefix)
          self.output_file_names.append(altorigpdbfname)
          self.input_file_names.append((fpdb, mpdb))
          print("\n", file=self.logger)
          self.ngoodmatches += len(MladRmsdMchainFchainMovstrAltOrgTrans)
          if len(MladRmsdMchainFchainMovstrAltOrgTrans) < 1:
            nogoodmatches.append((mpdb, fpdb))
          allatoms=0
          weightMLAD=0.0
          for (mlad, rmsd, movchainid, fixchainid, altorigtransform, dummy2, dummy3, natoms) in MladRmsdMchainFchainMovstrAltOrgTrans:
  # if MR solution files are used then print out list of MR solutions sorted w.r.t. MLAD values
            if busetwosolfiles:
              self.resultsmtrxlst.append((mlad, rmsd, altorigtransform, fixsoltxts[i], movsoltxts[j], i, j))
            weightMLAD += mlad * natoms # compute weighted MLAD values in order to select best match if more chains in PDB files
            allatoms += natoms
          if allatoms > 0:
            weightMLAD /= allatoms
            if minsolmlad > weightMLAD:
              minsolmlad = weightMLAD
              mini = i
              minj = j
              minoutput_file = self.output_file_names[-1]
              mininput_files = self.input_file_names[-1]
              minmovchainid = movchainid
              minfixchainid = fixchainid
              self.minMladRmsdMchainFchainMovstrAltOrgTrans = MladRmsdMchainFchainMovstrAltOrgTrans
    if len(enumfixpdbs) > 1 or len(enummovpdbs) > 1:
      solstr  = ""
      if len(self.resultsmtrxlst) > 0:
        # list in ascending order of MLAD scores
        sortresultsmtrxlst = sorted(self.resultsmtrxlst, key = lambda row: row[0])
        fixpreamble = (self.fixsolfname if self.fixsolfname else "") + "\n"
        movpreamble = (self.movsolfname if self.movsolfname else "") + "\n"
        for mlad, rmsd, altorigtransform, fixsol, movsol, fixsoln, movsoln in sortresultsmtrxlst:
          solstr += "# MLAD= %f, RMSD= %f , " %(mlad, rmsd)
          solstr += altorigtransform + "\n"
          fixsol = fixsol.replace("SOLU ","# SOLU ")
          solstr += "## fixed solution %d of " %fixsoln
          solstr += fixpreamble + fixsol + "\n"
          solstr += "# moving solution %d of " %movsoln
          solstr += movpreamble + movsol + "\n\n"
        # write an MR solution file with matches
        fname = os.path.join(self.wpath, "AltOrigSymMLAD_%s_%s.sol" %(fixlogfname, movlogfname))
        if self.outputfileprefix:
          fname = os.path.join(self.wpath, self.outputfileprefix + ".sol")
        open(fname,"wb").write(solstr.encode('utf-8'))
      print("\n" + solstr, file=self.logger)
      if len(nogoodmatches):
        if len(self.resultsmtrxlst) == 0 and busetwosolfiles:
          print("\nNo pairs of solutions have MLAD values less than %f" %self.goodmatch, file=self.logger)
        print("The following solution pair files have MLAD values exceeding %f" %self.goodmatch, file=self.logger)
        for e in nogoodmatches:
          mov = os.path.split(e[0])[1]
          fix = os.path.split(e[1])[1]
          print(" %s, %s" %(mov,fix), file=self.logger)
      print("\n" + "-" * 80, file=self.logger)
      outpdb = os.path.relpath(minoutput_file)
      if self.outputfileprefix:
        outpdb = os.path.join(self.wpath, self.outputfileprefix + ".pdb")
        if self.write_cif==True:
          outpdb = os.path.join(self.wpath, self.outputfileprefix + ".cif")
        shutil.move(minoutput_file, outpdb)
      #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
      for (mlad, rmsd, movchainid, fixchainid, altorigtransform, dummy2, dummy3, natoms) in self.minMladRmsdMchainFchainMovstrAltOrgTrans:
        print("Found best MLAD= %f, RMSD= %2.3f for chain %s in %s of moving solution %d with chain %s of fixed solution %d" \
         %(mlad, rmsd, movchainid, outpdb, minj, fixchainid, mini), file=self.logger)
    badmatchfiles = list(set(self.output_file_names) - set([minoutput_file]))
    if not self.writepdbfiles:
      for fname in badmatchfiles:
        if os.path.exists(fname):
          os.remove(fname)
    if not self.writepdbfiles:
      if self.fixsolfname: # fixed solution pdb files where created
        for fname in fixpdbs:
          if fname != mininput_files[0]:
            os.remove(fname)
          else:
            print("The realized fixed model file is %s" %fname, file=self.logger)
      if self.movsolfname: # moving solution pdb files where created
        for fname in movpdbs:
          if fname != mininput_files[1]:
            os.remove(fname)
          else:
            print("The realized moved model file is %s" %fname, file=self.logger)
    print("""\nA good match between two chains usually have a MLAD << 1.5
whereas a poor match usually have a MLAD > 2.
The alternate origin for superpositions of solutions with more chains
should be the same for all chain pairs with a good match.\n""", file=self.logger)
    print("Logfile written to %s\n" %logfname, file=self.logger)
    return self.output_file_names # mandatory for the GUI


  def GetSolutionsAsPdbs(self, mrsolfname, ensembles, model):
  # get list of pdb files either from pickle and phil, a phaser Mr solution file or just a pdb file supplied
    soltxts = []
    solfname = mrsolfname
    pdbfname = model
    ensmblspdbs = []
    halflogfname = ""
    if pdbfname is not None:
      halflogfname = os.path.basename(os.path.splitext(pdbfname)[0])
    if solfname and not pdbfname:
      if not os.path.exists(solfname):
        raise Sorry("The file, %s, could not be found." % solfname)
      mrparser = phaser.test_phaser_parsers.SolutionFileParser(open(solfname, "rt"))
      halflogfname = os.path.basename(os.path.splitext(solfname)[0])
  # make list of mrsolution strings
      soltxts = [sol.unparse() for sol in mrparser.get_dot_sol()]
      for ens in ensembles:
        ensmblspdbs.append((ens.name, ens.model))
    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
    if pdbfname and not solfname:# and (not pklfile or not philfile):
      if not os.path.exists(pdbfname):
        raise Sorry("The file, %s, could not be found." % pdbfname)
    return (soltxts, ensmblspdbs, halflogfname)


  def RealizePhaserSolutions(self, soltxts, ensmblspdbs, prefix):
    fnames = []
    if self.write_cif==True:
      fext = ".cif"
    elif self.write_cif==False:
      fext = ".pdb"
    if not isinstance(self.xtalsym, cctbx.crystal.symmetry):
      raise Sorry("Spacegroup and cell dimensions must be provided.")
    if not isinstance(soltxts, list) or not isinstance(ensmblspdbs, list):
      raise Exception
    for i,soltxt in enumerate(soltxts):
      mrsolhierarchies = []
      for ensmblpdbs in ensmblspdbs:
        ensemblename = ensmblpdbs[0]
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        for pdbmodelfname in ensmblpdbs[1]:# in case of more models constituting this ensemble
  # if processing solutionfilepath generated on windows but now running on unix
          pdbmodelfname = pdbmodelfname.replace("\\", os.path.sep)
          pdbmrsol = pdb.input(file_name= pdbmodelfname)
          solstrs = re.findall(r"(SOLU SET.+\n(SOLU SPAC.+\n)*SOLU 6DIM.+)",soltxt, re.MULTILINE)
          ensemblesolutions = re.findall(r"SOLU 6DIM\s+ENSE\s+%s (.+)" %ensemblename, soltxt, re.MULTILINE)
          if len(ensemblesolutions) < 1:
            raise Sorry("Solution file has no ensemble labeled %s" % ensemblename)
          for ensemblesolution in ensemblesolutions:
            eulers = re.findall(r"EULER\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+FRAC",
             ensemblesolution, re.MULTILINE)[0]
            fracs = re.findall(r"EULER\s+.+\s+FRAC\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+([-+]?[0-9]*\.?[0-9]+)\s+BFAC",
             ensemblesolution, re.MULTILINE)[0]
            rot = scitbx.math.euler_angles.zyz_matrix(float(eulers[0]), float(eulers[1]), float(eulers[2]))
            trans = (float(fracs[0]), float(fracs[1]), float(fracs[2]))
            mrsolhiearchy = pdbmrsol.construct_hierarchy().deep_copy()
            modelatoms = mrsolhiearchy.atoms()
        # do the rotation in cartesian space
            rotatomsites = rot * modelatoms.extract_xyz()
            ucell = self.xtalsym.unit_cell()
        # get into fractional space
            fracatomsites = ucell.fractionalize(sites_cart= rotatomsites )
        # position and rotate pdb to the mr solution
            mrsolsites = fracatomsites + trans
        # cast back to cartesian coordinates
            modelatoms.set_xyz(ucell.orthogonalize(sites_frac= mrsolsites))
            mrsolhierarchies.append( mrsolhiearchy.deep_copy() )
            pdbname = prefix + "sol_" + str(i) + fext
            print("Realizing MR solution %d for %s as %s at\n%s" \
              %(i, ensemblename, pdbname, ensemblesolution), file=self.logger)
      fnames.append(os.path.abspath(os.path.join(self.wpath, pdbname)))
      jointhierarchy = pdb.hierarchy.join_roots( mrsolhierarchies, None )
      for ci, hierarchy in enumerate(jointhierarchy.models()[0].chains()):
        hierarchy.id = AltOrigSymmatesEngine.chainid[ci]
      mrsolpdbfile = open(fnames[-1], "w")
      if self.write_cif:
        from mmtbx import model
        mm = model.manager(None, jointhierarchy, self.xtalsym)
        # skip_restraints=True prevents crash if using plain cctbx build without monomer library
        mrsolpdbfile.write(mm.model_as_mmcif(skip_restraints=True))
      else:
        mrsolpdbfile.write(jointhierarchy.as_pdb_string(self.xtalsym))
      mrsolpdbfile.close()
    return fnames


  def GetAltOrigSymmates(self,
      movingname=None,
      fixedname=None,
      outputfileprefix=None,
      pdb_out=None,
      logger=None):
    if not movingname:
      movingname = self.movpdb
    if not movingname:
      movingname = self.movsolfname
    if not fixedname:
      fixedname = self.fixpdb
    if not fixedname:
      fixedname = self.fixsolfname
    if not isinstance(self.xtalsym, cctbx.crystal.symmetry):
      raise Sorry("Spacegroup and cell dimensions must be provided.")
    if logger:
      self.logger = logger
    return AltOrigSymmatesEngine.CalcAltOrigSymmates((movingname, ), (fixedname, ),
     self.movehetatms, self.xtalsym, self.chosenorigin, self.usebestSSM,
     self.usesymmop, self.debug, self.wpath, self.writepdbfiles, self.goodmatch,
     self.strictly_protein, self.verbose, self.mediocre_mlad, self.nproc,
     outputfileprefix=outputfileprefix, write_cif=self.write_cif, pdb_out= pdb_out, mlogger=self.logger,
     quiet=self.quiet)
