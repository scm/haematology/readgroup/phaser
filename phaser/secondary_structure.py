from __future__ import print_function

class Annotation(object):
  """
  Secondary structure annotation
  """

  def __init__(self, identifier, rgs, start, end):

    assert 0 <= start
    assert start <= end
    assert end < len( rgs )

    self.identifier = identifier
    self.rgs = rgs
    self.start = start
    self.end = end


  @property
  def residue_groups(self):

    return self.rgs[ self.start : self.end + 1 ]


  @property
  def residue_group_indices(self):

    return range( self.start, self.end + 1 )


class AlphaHelix(object):
  """
  An alpha-helix
  """

  def __init__(self, identifier):

    self.identifier = identifier


  def __str__(self):

    return "%s %s" % ( self.classification(), self.identifier )


  @staticmethod
  def classification():

    return "alpha-helix"


class BetaSheet(object):
  """
  Beta-sheet
  """

  def __init__(self, identifier):

    self.identifier = identifier


  def __str__(self):

    return "%s %s" % ( self.classification(), self.identifier )


  @staticmethod
  def classification():

    return "beta-sheet"


class BetaStrand(object):
  """
  A beta-strand
  """

  def __init__(self, identifier, sheet):

    self.identifier = identifier
    self.sheet = sheet


  def __str__(self):

    return "%s %s in %s" % ( self.classification(), self.identifier, self.sheet )


  @staticmethod
  def classification():

    return "beta-strand"


class Coil(object):
  """
  Not regular secondary structure
  """

  def __init__(self, identifier):

    self.identifier = identifier


  def __str__(self):

    return "%s %s" % ( self.classification(), self.identifier )


  @staticmethod
  def classification():

    return "coil"


def protein(chain):

  import iotbx.pdb
  root = iotbx.pdb.hierarchy.root()
  model = iotbx.pdb.hierarchy.model()
  root.append_model( model )
  model.append_chain( chain.detached_copy() )

  import mmtbx.secondary_structure
  ( records, errors ) = mmtbx.secondary_structure.run_ksdssp_direct(
    root.as_pdb_string()
    )

  if errors and not records:
    raise RuntimeError(errors)

  ann = iotbx.pdb.secondary_structure.annotation.from_records( records = records )

  if ann is not None:
    rgs = chain.residue_groups()
    indexer =  dict(
      [ ( ( rg.resseq_as_int(), rg.icode ), i ) for ( i, rg ) in enumerate( rgs ) ]
      )

    for ( i, helix ) in enumerate( ann.helices, start = 1 ):
      yield Annotation(
        identifier = AlphaHelix( identifier = i ),
        rgs = rgs,
        start = indexer[ ( helix.get_start_resseq_as_int(), helix.start_icode ) ],
        end = indexer[ ( helix.get_end_resseq_as_int(), helix.end_icode ) ],
        )

    for ( i_sheet, sheet ) in enumerate( ann.sheets, start = 1 ):
      parent = BetaSheet( identifier = i_sheet )

      for ( i_strand, strand ) in enumerate( sheet.strands, start = 1 ):
        yield Annotation(
          identifier = BetaStrand( identifier = i_strand, sheet = parent ),
          rgs = rgs,
          start = indexer[ ( strand.get_start_resseq_as_int(), strand.start_icode ) ],
          end = indexer[ ( strand.get_end_resseq_as_int(), strand.end_icode ) ],
          )


def full_structure_annotations(chain, annotator, sstype):

  segments = set()

  for ann in annotator( chain = chain ):
    segments.add( ( ann.start, ann.end ) )
    yield ann

  position = 0
  rgs = chain.residue_groups()
  index = 1

  while segments:
    lowest = min( segments, key = lambda p: p[0] )
    segments.remove( lowest )

    if position < lowest[0]:
      yield Annotation(
        identifier = sstype( identifier = index ),
        rgs = rgs,
        start = position,
        end = lowest[0] - 1,
        )
      index += 1

    position = max( position, lowest[1] + 1 )

  if position < len( rgs ):
    yield Annotation(
      identifier = sstype( identifier = index ),
      rgs = rgs,
      start = position,
      end = len( rgs ) - 1,
      )


def dna(chain):

  return ( x for x in () )


def rna(chain):

  return ( x for x in () )


ANNOTATOR_FOR = {
  "protein": protein,
  "dna": dna,
  "rna": rna,
  }
