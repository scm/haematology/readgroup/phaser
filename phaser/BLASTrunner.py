from __future__ import print_function

"""
Run from command line to get help with input
phenix.python cctbx_sources\Develop\phaser\phaser\BLASTrunner.py
"""
import BLASTnMRfunc
import sys
import traceback
import subprocess
import logging
import os
import time
from libtbx import easy_mp
from builtins import input

argn = 1
argc = len(sys.argv)


# prompt user for value if it's not on the commandline
def Inputarg(varname):
    global argn
    global argc
    if argc > 1 and argn < argc:
       myvar = sys.argv[argn]
       argn = argn + 1
       print(varname + " " + myvar)
    else:
       myvar = input(varname)
    return myvar


def GetFullPathExe(exefname):
    whichcmd = "which"
    osdepbool = True
    if os.sys.platform == "win32":
        whichcmd = "where"
    #
    (stdoutstr, stderrstr) = subprocess.Popen("%s %s" %(whichcmd,exefname),
      shell=osdepbool, stdout=subprocess.PIPE,stderr=subprocess.PIPE).communicate()
    #
    if len(stdoutstr) > 0:
        fullpathexe = stdoutstr.splitlines()[0]
    else:
        fullpathexe = ""
    #
    return fullpathexe




def RunNprocPhaser(targetpdbid, nmaxsols, fmaxhours, customkeysfname,
      subsetfname, brunphaser, DoTemplCalc, RMSDrefine, RotTraRefine, bOccRefine,
      OCCresidwindows, calclabel, neweLLGtargets, bcontrivedtmpl, extdbnameprefix,
      bcleanupaftercalc,
    ):
    print("Running MR on target: ", targetpdbid)
    modellst = []
    logfname = BLASTnMRfunc.BLASTrunner.GetLogFileName("e", targetpdbid, calclabel)
    paths, name = os.path.split(logfname)
    fulllogfname = os.path.join(paths, "Full" + name)
    if os.path.exists(fulllogfname):
        os.remove(fulllogfname)
    for neweLLGtarget in neweLLGtargets:
        with BLASTnMRfunc.BLASTrunner(targetpdbid,
                                      calclabel,
                                      ismultiprocessing=True,
                                      customkeysfname=customkeysfname,
                                      subsetfname=subsetfname) as myobj:
            myobj.fmaxhours = fmaxhours
            myobj.nmaxsols = nmaxsols
            myobj.brunphaser = brunphaser
            myobj.DoTemplCalc = DoTemplCalc
            myobj.RMSDrefine = RMSDrefine
            myobj.RotTraRefine = RotTraRefine
            myobj.bOccRefine = bOccRefine
            if bOccRefine:
                myobj.OCCresidwindows = OCCresidwindows
            myobj.neweLLGtarget = neweLLGtarget
            myobj.bcontrivedtmpl = bcontrivedtmpl
            myobj.extdbnameprefix = extdbnameprefix
            zipfname = None
            if neweLLGtarget:
                zipfname = targetpdbid + "_" + calclabel + "_ELLG" + str(neweLLGtarget) + myobj.buildnumberstr + ".zip"
            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
            try:
                myobj.CleanUpFiles()
                psearches = myobj.RunMRAutoGetResultsFromListFile(1)
                modellst = [e[0][0][0] for e in psearches]
            except Exception as m:
                myobj.logger.log(logging.ERROR, "\n" + str(m) + "\n" + traceback.format_exc() + "\n\n")
            finally:
                myobj.ArchiveFiles(zipfname)
                if bcleanupaftercalc:
                    myobj.CleanUpFiles()
                logstr = ""
                with open(logfname,"r") as logfile:
                    logstr = logfile.read()
                with open(fulllogfname, "a") as fulllogfile:
                    fulllogfile.write(logstr)
    return modellst, fulllogfname




if __name__ == "__main__":
    bdebug = False
    if argc > 1:
        for i, arg in enumerate(sys.argv):
            if "--" in arg: # remove flag from argument list
                if arg=="--debug":
                    bdebug = True
                sys.argv.pop(i)
            print(arg)

    print("""You can either \n
a) Do a BLAST homology search for a particular pdb id to set up folders and
   input files to be used in subsequent MR calculations.
b) Do MR calculations on an existing set of folders and input files.
c) Combine (a) and (b).
d) Gather results from folders with finished MR calculations.
e) Combine (b) and (d)
f) Use an existing BLAST output to set up folders and input files to be used
   in subsequent MR calculations.
    """)
    answer = Inputarg("What do you want to do? (a,b,c,d,e,f): ")
    sshexe = "notused"
    scpexe = "notused"
    userid = "notused"
    mtzfolder = "notused"
    phaserexe = "notused"
    clustalwexe = "notused"
    superposeexe = "notused"
    passwd = ""
    noRNP = False
    bOccRefine = False
    bOccAIC = True
    RMSDrefine = True
    fmaxhours = 1
    nmaxsols = 100
    DoTemplCalc = False
    RotTraRefine = True
    brunphaser = True
    targetpdbid = ""
    OnlyDBLayout = False
    calclabel = ""
    winoptssh = None
    customkeysfname = ""
    FsolBsol = None
    cleanupaftercalc = False

    perturbtype = False
    OCCresidwindows = ()
    neweLLGtargets = [""]
    sculptprotocol = "1"
    bcontrivedtmpl = False
    fnameinfix = ""
    extdbnameprefix = None
    buseIntensities = False
    OtherSolutions = [["",""]]

    if answer=="a" or answer=="c" or answer=="f":
        maxseqid = float(Inputarg("Enter maximum allowed value of sequence identity: "))
        minseqid = float(Inputarg("Enter minimum allowed value of sequence identity: "))
        maxpdbs = float(Inputarg("Enter permitted number of pdb ids to be returned: "))

        print("Using ulysses and zeus for BLAST homology searches and your PC for MR runs.")
        print("Do you have passwordless authentication to ulysses and zeus through ssh? (Y/N): ")
        if str(Inputarg("yes/no: ")).lower().find("y") <0:
            passwd = Inputarg("OK, so what is your password?: ")
# Get program filenames if they are in the system path
# Also get rid of \r\n that where on windows append with .strip()
        sshexe = GetFullPathExe("ssh")
        if len(sshexe)==0:
            sshexe = GetFullPathExe("plink")
        if len(sshexe)==0:
            print("Enter fully qualified file paths for the ssh client:")
            sshexe = Inputarg("ssh: ")
        else:
            if os.sys.platform == "win32":
                winoptssh = "-ssh" # necessary option for plink.exe

        scpexe = GetFullPathExe("scp")
        if len(scpexe)==0:
            scpexe = GetFullPathExe("pscp")
        if len(scpexe)==0:
            print("Enter fully qualified file paths for the scp program:")
            scpexe = Inputarg("scp: ")

        mtzfolder = Inputarg("Enter path for the mtz files: ")
        userid = Inputarg("Enter your userid for ulysses and zeus: ")

    if answer=="a":
        print("Do you only want to collect the homolouges without doing subsequent RNP jobs?")
        if str(Inputarg("yes/no: ")).lower().find("y") > -1:
            noRNP = True

    if answer=="a" or answer=="b" or answer=="c" or answer=="f" or answer=="e":
        phaserexe = GetFullPathExe("phenix.phaser")
        if len(phaserexe)==0:
            print("Enter fully qualified file path for the phaser program:")
            phaserexe = Inputarg("phaser: ")

    if answer=="a" or answer=="c" or answer=="f":
        clustalwexe = GetFullPathExe("clustalw")
        if len(clustalwexe)==0:
            clustalwexe = GetFullPathExe("clustalw2")
        if len(clustalwexe)==0:
            print("Enter fully qualified file path for the clustalw program:")
            clustalwexe = Inputarg("clustalw: ")

        myinc = Inputarg("Specify the smallest difference in sequence identities between different models from the BLAST search to be permitted?: ")

        superposeexe = GetFullPathExe("superpose")
        if len(superposeexe)==0:
            print("Enter the fully qualified file path for the superpose program:")
            superposeexe = Inputarg("superpose: ")

    subsetfname=""
    if answer =="b" or answer =="c" or answer=="f" or answer=="e":
        njobs = int(Inputarg("Enter the number of Phaser MR jobs to run concurrently: "))
        nmaxsols = int(Inputarg("Enter maximum number of solutions for each MR calculation : "))
        fmaxhours = float(Inputarg("Enter maximum duration of hours for each MR calculation : "))
        customkeysfname = Inputarg("Enter fully qualified path for custom Phaser keywords script: ")
    if answer =="b" or answer =="c" or answer=="d" or answer=="f" or answer=="e":
        subsetfname = Inputarg("Enter filename of list of subset of allowed targets and models: ")

    if answer =="b" or answer =="c" or answer =="d" or answer=="f" or answer=="e":
        if str(Inputarg("Skip MR calculations? (y/n) ")).lower().find("y") > -1:
            brunphaser = False
        if str(Inputarg("Do RNP on template solutions? (y/n) ")).lower().find("y") > -1:
            DoTemplCalc = True
            if str(Inputarg("Skip refining RMSD during RNP on template solutions? (y/n) ")).lower().find("y") > -1:
                RMSDrefine = False
            if str(Inputarg("Skip refining rotation and translation during RNP on template solutions? (y/n) ")).lower().find("y") > -1:
                RotTraRefine = False

        if str(Inputarg("Occupancy refinement of residues switched on, yes/no? ")).lower().find("y") > -1:
            bOccRefine = True
            OCCresidwindows = eval(Inputarg("Specify tuple of residue window size "\
             "for occupancy refinement, empty tuple for using default size only: "))

    print("Paths for ssh, scp, phaser, clustalw, superpose are:\n%s\n%s\n%s\n%s\n%s"
          % (sshexe, scpexe, phaserexe, clustalwexe, superposeexe))

    calclabel = Inputarg("Enter a name tag for the calculations: ")
    calclabel = calclabel.strip()


    if answer =="b" or answer =="c" or answer=="f" or answer=="e":
        s = Inputarg("Use solvent parameters different from default values? ")
        if s.lower().find("y") > -1:
            fsol = float(Inputarg("Enter Fsol: "))
            bsol = float(Inputarg("Enter Bsol: "))
            FsolBsol = (fsol, bsol)

        s = Inputarg("Perturb Fsol, RMSD or Cell dimensions after MR calculations? (F, R or C) ")
        if s.lower().find("f") > -1:
            perturbtype = "PerturbFsol"
        if s.lower().find("r") > -1:
            perturbtype = "PerturbRMS"
        if s.lower().find("c") > -1:
            perturbtype = "PerturbCelldim"


    if str(Inputarg("Loop over different eLLGtargets than the default value, yes/no? ")).lower().find("y") > -1:
        N = int(Inputarg("How many? "))
        neweLLGtargets = []
        for i in range(N):
            ellg = int(Inputarg("Enter the %d. ELLG_target: " %(i+1)) )
            neweLLGtargets.append(ellg)

    if answer!="d" and answer!="b":
        s = Inputarg("Use a protocol other than default for sculpting models (y/n) ")
        if s.lower().find("y") > -1:
            sculptprotocol = Inputarg("Which protocol should be used for sculpting the models? ")

    if answer =="a" or answer =="b" or answer =="c" or answer=="f" or answer=="e":
        s = Inputarg("Seed multicomponent calculations on components a,b,c with contrived refined solution of a,b (y/n) ")
        if s.lower().find("y") > -1:
            bcontrivedtmpl = True

    if answer =="a" or answer =="c" or answer=="f":
        s = Inputarg("Use intensities instead of amplitudes in the mtz file? (y/n) ")
        if s.lower().find("y") > -1:
            buseIntensities = True


    if answer =="d" or answer =="e":
        extdbnameprefix = Inputarg("Enter a prefix for the database file name that will hold all the results: ").strip()

    if answer =="d":
        s = Inputarg("Get results from zip archive instead of uncompressed folders? (y/n) ")
        if s.lower().find("y") > -1:
            fnameinfix = Inputarg("Name infix of zip archive to get results from: ").strip()

    if answer =="d":
        TaggedPDBfileliststr = ""

        s = Inputarg("Do you supply a list of filenames and label names for PDB codes to label? (Y/N): ")
        if s.lower().find("y") > -1:
            TaggedPDBfileliststr = Inputarg("Enter filenames with PDB codes of special PDBs and a corresponding name to label each MR model matching those PDB codes: ").strip()

        s = Inputarg("Do you have additional solution files with common prefixes? (Y/N): ")
        if s.lower().find("y") > -1:
            N = int(Inputarg("How many? "))
            if N > 0:
                for i in range(N):
                    suffix = Inputarg("Enter database column suffix for the %d. solution: " %(i+1)).strip()
                    solname = Inputarg("Enter filename prefix for the %d. solution: " %(i+1)).strip()
                    OtherSolutions.append([suffix, solname])

    tpdbids = []
    s = Inputarg("Are target PDBs provided from a python list in a file or as arguments (y/n)? ")
    if s.lower().find("y") > -1:
        #tpdbidfname = None
        tpdbidfname = Inputarg("What is the file name? ")
        with open(tpdbidfname, "r") as f:
            tpdbids = eval(f.read())
        nelm = int(Inputarg("Start calculations from which element in this list? "))
        tpdbids = tpdbids[nelm:]
    else:
        npdbs = int( Inputarg("Number of target pdbs? : ") )
        for i in range(npdbs):
            targetpdbid = Inputarg("Enter the %d. target pdbid: " %(i+1))
            tpdbids.append(targetpdbid)

    s =  Inputarg("Archive and tidy up files after each calculation? (y/n) ")
    if s.lower().find("y") > -1:
        cleanupaftercalc = True

    ncpu = int( Inputarg("Number of CPUs for multiprocessing? : ") )
    if ncpu > 1:
        bstr = BLASTnMRfunc.Buildstr()
        alllogsfname = "All_MR" + bstr.buildnumberstr + ".log"
        #if os.path.exists(alllogsfname):
        #    os.remove(alllogsfname)
        argtpllist = []
        for targetpdbid in tpdbids:
            argtpllist.append(
                    (
                    targetpdbid, nmaxsols, fmaxhours, customkeysfname, subsetfname,
                    brunphaser, DoTemplCalc, RMSDrefine, RotTraRefine, bOccRefine,
                    OCCresidwindows, calclabel, neweLLGtargets, bcontrivedtmpl,
                    extdbnameprefix, cleanupaftercalc
                    )
            )

        for i, parmresdict in enumerate(easy_mp.multi_core_run(
                                    RunNprocPhaser,
                                    argtpllist,
                                    ncpu
                      )
        ):
            targetpdbid = parmresdict[0][0]
            logstr = "%d, %s\n" %(i, targetpdbid)
            if parmresdict[2]: # error string has been set
                logstr += parmresdict[2]
                print(logstr)
            else:
                modellst, fulllogfname = parmresdict[1]
                print(i, targetpdbid, modellst)

                with open(fulllogfname,"r") as fulllogfile:
                    logstr += fulllogfile.read()

            with open(alllogsfname, "a") as alllogfile:
                alllogfile.write(logstr + "\n\n")

        exit(0)

    else:
        for targetpdbid in tpdbids:
            for neweLLGtarget in neweLLGtargets:
                errorcode = [0]

                with BLASTnMRfunc.BLASTrunner(targetpdbid=targetpdbid, calclabel=calclabel,
                  sshcmd=sshexe, scpcmd=scpexe,
                  userid=userid, mtzfolder=mtzfolder, phaserexe=phaserexe, clustalw=clustalwexe,
                  superposeexe=superposeexe, passwd=passwd, sshflag=winoptssh, fnameinfix=fnameinfix,
                  errorcode=errorcode, customkeysfname=customkeysfname, subsetfname=subsetfname) as myobj:

                    if answer=="f":
                        myobj.writeBLASTxml = False

                    myobj.fmaxhours = fmaxhours
                    myobj.nmaxsols = nmaxsols
                    myobj.FsolBsol = FsolBsol
                    myobj.perturbtype = perturbtype
                    myobj.brunphaser = brunphaser
                    myobj.DoTemplCalc = DoTemplCalc
                    myobj.RMSDrefine = RMSDrefine
                    myobj.RotTraRefine = RotTraRefine
                    myobj.bOccRefine = bOccRefine
                    myobj.OCCresidwindows = OCCresidwindows
                    myobj.neweLLGtarget = neweLLGtarget
                    myobj.sculptprotocol = sculptprotocol
                    myobj.bcontrivedtmpl = bcontrivedtmpl
                    myobj.extdbnameprefix = extdbnameprefix
                    myobj.buseIntensities = buseIntensities
                    myobj.OtherSolutions = OtherSolutions
                    if not fnameinfix or fnameinfix=="None":
                        myobj.fnameinfix = myobj.buildnumberstr

                    if neweLLGtarget:
                        myobj.zipfname = targetpdbid + "_ELLG" + str(neweLLGtarget) + myobj.fnameinfix + ".zip"
                    else:
                        myobj.zipfname = targetpdbid + "_" + calclabel + myobj.buildnumberstr + ".zip"

                    #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                    try:
                        myobj.bdebug = bdebug
                        if answer=="a" or answer=="f" :
                            myobj.CleanUpFiles()
                            myobj.BLASTnSetupMRruns(maxseqid, minseqid, maxpdbs, float(myinc), noRNP)

                        if answer =="b":
                            myobj.CleanUpFiles()
                            myobj.RunMRautoFromlistFile(njobs)
                            if cleanupaftercalc:
                                myobj.ArchiveFiles()
                                myobj.CleanUpFiles()

                        if answer =="e":
                            myobj.CleanUpFiles()
                            myobj.RunMRAutoGetResultsFromListFile(njobs)
                            myobj.ArchiveFiles()

                        if answer =="c":
                            myobj.CleanUpFiles()
                            myobj.BLASTnMRrun(maxseqid, minseqid, maxpdbs, njobs, float(myinc))

                        if answer =="d":
                            myobj.CollateMRautoResultsFromlistFile(TaggedPDBfileliststr, OnlyDBLayout)

                    except Exception as m:
                        errorcode[0] += 1
                        print(str(m) + "\n" + traceback.format_exc())
