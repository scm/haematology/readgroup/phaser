from __future__ import print_function

from libtbx.object_oriented_patterns import lazy_initialization
from phaser.identity import Atom

# Monomer library server
def get_monomer_library_server():

  from mmtbx.monomer_library import server as mon_lib_server
  return mon_lib_server.server()

monomer_library_server = lazy_initialization( func = get_monomer_library_server )

# 2D graphs
def bonds_from_library(resname):

  cif = monomer_library_server().get_comp_comp_id_direct( comp_id = resname )

  if cif is None:
    raise KeyError("%s not present in the monomer library")

  bonds = []

  for bond in cif.bond_list:
    bonds.append(
      (
        Atom.from_chem_comp_atom( atom = cif.atom_by_id( bond.atom_id_1 ) ),
        Atom.from_chem_comp_atom( atom = cif.atom_by_id( bond.atom_id_2 ) ),
        )
      )

  return bonds


def discard_bonds_to_hydrogen(bonds):

  return [ ( a1, a2 ) for ( a1, a2 ) in bonds
    if a1.element != "H" and a2.element != "H" ]


def connectivity_from_bonds(bonds):

  from mmtbx.geometry import topology

  compound = topology.Compound.create()

  for ( left, right ) in bonds:
    compound.add_bond( left = left, right = right )

  return compound


def mainchain_distances(bonds, mainchains, attachment):

  assert mainchains
  if attachment is not None:
    assert attachment in mainchains
    relevants = [
      ( s, t ) for ( s, t ) in bonds
      if (
        ( s in mainchains and t in mainchains )
        or ( s not in mainchains and t not in mainchains )
        or s == attachment
        or t == attachment
        )
      ]
    topology = connectivity_from_bonds( bonds = relevants )

    if attachment in topology.atoms:
      dist_for = topology.distances_from( atom = attachment )

    else:
      dist_for = dict( ( a, None ) for a in topology.atoms )

  else:
    topology = connectivity_from_bonds( bonds = bonds )
    distance_dicts = []

    for mca in mainchains:
      if mca in topology.atoms:
        distance_dicts.append( topology.distances_from( atom = mca ) )

      else:
        distance_dicts.append( dict( ( a, None ) for a in topology.atoms ) )

    dist_for = dict(
      ( atom, min( d_from[ atom ] for d_from in distance_dicts ) )
      for atom in topology.atoms
      )

  dist_for.update( ( aid, 0 ) for aid in mainchains )

  return dist_for


# 3D matching
def get_monomer_library_idealized_amino_acids():

  from mmtbx.monomer_library import idealized_aa
  return idealized_aa.residue_dict()


idealized_residues = {
  "protein": lazy_initialization( func = get_monomer_library_idealized_amino_acids ),
  }


class Geometry(object):
  """
  Attach residue geometry and resname
  """

  def __init__(self, resname, pairs):

    self.resname = resname
    self.pairs = pairs


  def atoms(self):

    return [ p[0] for p in self.pairs ]


  def bonds(self, tolerance = 0.1):

    import itertools
    from mmtbx.monomer_library import bondlength_defaults

    bonds = []

    for ( ( a1, ( x1, y1, z1 ) ), ( a2, ( x2, y2, z2 ) ) ) in itertools.combinations( self.pairs, 2 ):
      dist2 = ( ( x1 - x2 ) ** 2 + ( y1 - y2 ) ** 2 + ( z1 - z2 ) ** 2 )
      expected = bondlength_defaults.get_default_bondlength( a1.element, a2.element )

      if expected is not None and dist2 < ( expected + tolerance ) ** 2:
        bonds.append( ( a1, a2 ) )

    return bonds


  def discard_atoms(self, names):

    self.pairs = [ ( a, xyz ) for ( a, xyz ) in self.pairs if a not in names ]


  def discard_element(self, symbol):

    self.pairs = [ ( a, xyz ) for ( a, xyz ) in self.pairs if a.element != symbol ]


  def distance_topology(self):

    from mmtbx.geometry import topology
    molecule = topology.Molecule()

    for ( atom, xyz ) in self.pairs:
      molecule.add( atom = atom, xyz = xyz )

    return molecule


  def rotamers(self, fine_sampling):

    from scitbx.array_family import flex
    atoms = []
    xyzs = flex.vec3_double()

    for ( a, xyz ) in self.pairs:
      atoms.append( a )
      xyzs.append( xyz )

    rotiter = rotamer_iterator(
      code = self.resname,
      atoms = atoms,
      coords = xyzs,
      fine_sampling = fine_sampling,
      )

    for ( annotation, coords ) in rotiter:
      assert len( atoms ) == len( coords )
      yield (
        annotation,
        self.__class__( resname = self.resname, pairs = zip( atoms, coords ) ),
        )


def rotamer_iterator(code, atoms, coords, fine_sampling):

  server = monomer_library_server()

  rotiter = server.rotamer_iterator(
    comp_id = code,
    atom_names = [ a.pdb_name for a in atoms ],
    sites_cart = coords,
    fine_sampling = fine_sampling,
    )

  if rotiter is None or rotiter.rotamer_info is None or rotiter.problem_message is not None:
    yield ( "current", coords )

  else:
    for ( ann, xyzs ) in rotiter:
      yield ( ann.id, xyzs )


def geometry_from_library(resname):

  from mmtbx import chemical_components as chemcomp
  cif = chemcomp.get_cif_dictionary( code = resname )

  if cif is None:
    raise KeyError("%s not present in chemical components dictionary")

  pairs = []

  for cifobj in cif.get( "_chem_comp_atom", [] ):
    pairs.append(
      (
        Atom( name = cifobj.atom_id.strip(), element = cifobj.type_symbol.strip() ),
        ( cifobj.model_Cartn_x, cifobj.model_Cartn_y, cifobj.model_Cartn_z ),
        )
      )

  return Geometry( resname = resname, pairs = pairs )


def geometry_from_idealized_residues(restype, resname):

  resdict = idealized_residues[ restype ]()

  root = resdict[ resname ]
  return geometry_from_structure( resname = resname, atoms = root.atoms() )


def geometry_from_structure(resname, atoms):

  return Geometry(
    resname = resname,
    pairs = [ ( Atom.from_iotbx_atom( atom = a ), a.xyz ) for a in atoms ],
    )


# Matching
def match(
  left,
  right,
  prematched = [],
  vertex_equality = None,
  edge_equality = None,
  ):

  from mmtbx.geometry import topology
  match = topology.RascalMatch(
    molecule1 = left,
    molecule2 = right,
    prematched = prematched,
    vertex_equality = vertex_equality,
    edge_equality = edge_equality,
    )
  return match.remapped()
