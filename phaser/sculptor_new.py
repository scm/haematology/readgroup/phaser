from __future__ import print_function

from phaser import tbx_utils
from phaser import mmtype
from phaser import chainalign
from phaser.logoutput_new import facade

from libtbx.object_oriented_patterns import lazy_initialization

# Global constants
VERSION = "0.5.0"
PROGRAM = "sculptor"

LOGGING_LEVELS = tbx_utils.LOGGING_LEVELS

# Module exception
class SculptorError(Exception):
  """
  Error occurred in the sculptor module
  """


class AlignmentReadError(SculptorError):
  """
  Error occurred within read_in_alignment_files
  """


class ErrorEstimationFailure(SculptorError):
  """
  Error occurred within estimate_error_from_structure
  """


class ErrorFileReadFailure(SculptorError):
  """
  Error occurred within read_in_error_files
  """

# PHIL-to-object conversion
def get_unit_weights(window):

  return [ 1 ] * ( 2 * window + 1 )


def get_triangular_weights(window):

  from . import sequtil
  return sequtil.triangular_blurring_pattern( window = window )


WEIGHTING_SCHEME_GETTER_FOR = {
  "uniform": get_unit_weights,
  "triangular": get_triangular_weights,
  }

def residue_frame_linear_averaged_sequence_similarity(matrix, params, weighting_for):

  from phaser.chisellib import seqsim
  return seqsim.ResidueFrameLinearAveraged(
    matrix = matrix,
    weights = weighting_for[ params.linear.weighting ](
      window = params.linear.window,
      ),
    )


def residue_frame_spatial_averaged_sequence_similarity(matrix, params, *args, **kwargs):

  from phaser.chisellib import seqsim
  return seqsim.ResidueFrameSpaceAveraged(
    matrix = matrix,
    maxdist = params.spatial.maximum_distance,
    bleedpattern = get_triangular_weights(
      window = params.spatial.gap_bleed_length + 1,
      ),
    )

RESIDUE_FRAME_AVERAGED_SEQSIM_METHOD_CALLED = {
  "linear": residue_frame_linear_averaged_sequence_similarity,
  "spatial": residue_frame_spatial_averaged_sequence_similarity,
  }


def atom_frame_linear_averaged_sequence_similarity(matrix, params, weighting_for):

  from phaser.chisellib import seqsim
  return seqsim.AtomFrame(
    ssim = residue_frame_linear_averaged_sequence_similarity(
      matrix = matrix,
      params = params,
      weighting_for = weighting_for,
      ),
    )


def atom_frame_spatial_averaged_sequence_similarity(matrix, params, *args, **kwargs):

  from phaser.chisellib import seqsim
  return seqsim.AtomFrameSpaceAveraged(
    matrix = matrix,
    maxdist = params.spatial.maximum_distance,
    bleedpattern = get_triangular_weights(
      window = params.spatial.gap_bleed_length + 1,
      ),
    )

ATOM_FRAME_AVERAGED_SEQSIM_METHOD_CALLED = {
  "linear": atom_frame_linear_averaged_sequence_similarity,
  "spatial": atom_frame_spatial_averaged_sequence_similarity,
  }


def substitute_maximum_value(sequence, all_none_substitute):

  if not sequence:
    maxelem = None

  else:
    maxelem = max( sequence )

    if maxelem is None:
      maxelem = all_none_substitute

  return [ maxelem if elem is None else elem for elem in sequence ]


class SubstituteScaledInterpolatedValue(object):

  def __init__(self, extrapolation_step_scale, interpolation_step_scale):

    self.extrapolation_step_scale = extrapolation_step_scale
    self.interpolation_step_scale = interpolation_step_scale


  def __call__(self, sequence, all_none_substitute):

    forward_not_none = ( ( i, e ) for ( i, e ) in enumerate( sequence ) if e is not None )

    try:
      ( index, elem ) = forward_not_none.next()

    except StopIteration:
      # all None or empty sequence
      return [ all_none_substitute ] * len( sequence )

    edited = list( sequence )

    # Extrapolation
    for i in range( index, 0, -1 ):
      elem *= self.extrapolation_step_scale
      edited[ i - 1 ] = elem

    backward_not_none = (
      ( i, e ) for ( i, e ) in enumerate( reversed( sequence ) ) if e is not None
      )

    ( index, elem ) = backward_not_none.next()

    for i in range( index, 0, -1 ):
      elem *= self.extrapolation_step_scale
      edited[ -i ] = elem

    from phaser import sequtil
    segiter = sequtil.split(
      sequence = [ i for ( i, e ) in enumerate( edited ) if e is None ],
      consecutivity = sequtil.sequence_consecutivity,
      )

    for segment in segiter:
      assert segment
      i_before = segment[0] - 1
      i_after = segment[-1] + 1
      assert 0 <= i_before
      assert i_after < len( edited )
      e_before = edited[ i_before ]
      e_after = edited[ i_after ]
      norm = i_after - i_before
      interpolateds = [
        ( ( i_after - i ) * e_before + ( i - i_before ) * e_after ) / norm for i in segment
        ]
      scales = sequtil.triangular_pattern_for_interval( length = len( segment ) )

      assert len( interpolateds ) == len( segment )
      assert len( scales ) == len( segment )

      for ( index, intval, scalval ) in zip( segment, interpolateds, scales ):
        edited[ index ] = intval * self.interpolation_step_scale ** scalval

    return edited


class PhilGen_Substitution_MaximumValue(object):

  def __call__(self, params):

    return substitute_maximum_value


  def __str__(self):

    return """
  .help = "Substitute maximum from the sequence"
{
}"""


class PhilGen_Substitution_ScaledInterpolatedValue(object):

  def __init__(self, extrapolation_step_scale, interpolation_step_scale):

    self.extrapolation_step_scale = extrapolation_step_scale
    self.interpolation_step_scale = interpolation_step_scale


  def __call__(self, params):

    return SubstituteScaledInterpolatedValue(
      extrapolation_step_scale = params.extrapolation_step_scale,
      interpolation_step_scale = params.interpolation_step_scale,
      )


  def __str__(self):

    return """
  .help = "Substitute interpolated value scaled with the distance"
{
  extrapolation_step_scale = %(extrapolation)s
    .help = "Stepwise scale factor for extrapolation"
    .type = float( value_min = 1.00 )
    .optional = False

  interpolation_step_scale = %(interpolation)s
    .help = "Stepwise scale factor for extrapolation"
    .type = float( value_min = 1.00 )
    .optional = False
}""" % {
  "extrapolation": "%.2f" % self.extrapolation_step_scale,
  "interpolation": "%.2f" % self.interpolation_step_scale,
  }


class PhilGen_SequenceSimilarity(object):
  """
  A class that stores possible choices and defaults for this scope
  """

  def __init__(
    self,
    matrices,
    smoothing_for,
    smoothing_default,
    window,
    weightings,
    bleed_length,
    maxdist,
    ):

    self.matrices = matrices
    self.smoothing_for = smoothing_for
    self.smoothing_default = smoothing_default
    self.window = window
    self.weightings = weightings
    self.bleed_length = bleed_length
    self.maxdist = maxdist


  def __call__(self, params):

    return self.smoothing_for[ params.smoothing.use ](
      matrix = params.matrix,
      params = params.smoothing,
      weighting_for = self.weightings.choices,
      )


  def __str__(self):

    return """
  .help = "Normalized values (~1: perfect match; ~0: random sequence: ~-1: gap)"
  .short_caption = Sequence similarity calculation
  .help = "Configure sequence similarity calculation"
  .style = box auto_align
{
  matrix = %(matrix)s
    .help = "Similarity matrix"
    .short_caption = Similarity matrix
    .type = choice
    .optional = False

  smoothing
    .help = "Configure raw similarity smoothing"
  {
    use = %(smoothing)s
      .help = "Method to use"
      .type = choice
      .optional = False

    linear
      .help = "Parameters for linear averaging"
    {
      window = %(window)s
        .help = "Averaging window width"
        .type = int
        .optional = False

      weighting = %(weighting)s
        .help = "Weighting scheme"
        .type = choice
        .optional = False
    }

    spatial
      .help = "Parameters for spatial averaging"
    {
      gap_bleed_length = %(bleed_length)s
        .help = "Alignment positions affected by gaps"
        .type = int
        .optional = False

      maximum_distance = %(maxdist)s
        .help = "Spatial distance for averaging"
        .type = float
        .optional = False
    }
  }
}
""" % {
  "matrix": self.matrices,
  "smoothing": tbx_utils.choice_string(
    possible = self.smoothing_for,
    default = self.smoothing_default,
    ),
  "window": self.window,
  "weighting": self.weightings,
  "bleed_length": self.bleed_length,
  "maxdist": self.maxdist,
  }


class PhilGen_ChoiceScope(object):

  def __init__(self, philgens, default, extra_infos):

    from collections import OrderedDict
    self.choice_for = OrderedDict( ( k, p ) for ( k, p ) in philgens )
    self.default = default

    if self.default is not None and self.default not in self.choice_for:
      raise RuntimeError("Default not in allowed options")

    self.extra_info_for = OrderedDict( [ ( "type", "choice" ) ] )
    self.extra_info_for.update( extra_infos )


  def options(self):

    return self.choice_for.keys()


  def __call__(self, params):

    return self.choice_for[ params.use ]( params = getattr( params, params.use ) )


  def __str__(self):

    if not self.choice_for:
      return ""

    return """
use = %(choices)s
%(params)s

%(options)s
""" % {
  "choices": tbx_utils.choice_string(
    possible = self.choice_for.keys(),
    default = self.default,
    ),
  "params": "\n".join(
    "  .%s = %s" % ( k, v ) for ( k, v ) in self.extra_info_for.items()
    ),
  "options": "\n".join(
    "%s%s" % ( k, p ) for ( k, p ) in self.choice_for.items()
    ),
  }


class PhilGen_MultiChoiceScope(object):

  def __init__(self, philgens, defaults, extra_infos):

    from collections import OrderedDict
    self.choice_for = OrderedDict( ( k, p ) for ( k, p ) in philgens )
    self.defaults = defaults

    if any( d not in self.choice_for for d in self.defaults ):
      raise RuntimeError("Default not in allowed options")

    self.extra_info_for = OrderedDict( [ ( "type", "choice ( multi = True )" ) ] )
    self.extra_info_for.update( extra_infos )


  def options(self):

    return self.choice_for.keys()


  def __call__(self, params):

    selecteds = []

    for key in params.use:
      selecteds.append( self.choice_for[ key ]( params = getattr( params, key ) ) )

    return selecteds


  def __str__(self):

    if not self.choice_for:
      return ""

    return """
use = %(choices)s
%(params)s

%(options)s
""" % {

  "choices": tbx_utils.multichoice_string(
    possible = self.choice_for.keys(),
    defaults = self.defaults,
    ),
  "params": "\n".join(
    "  .%s = %s" % ( k, v ) for ( k, v ) in self.extra_info_for.items()
    ),
  "options": "\n".join(
    "%s%s" % ( k, p ) for ( k, p ) in self.choice_for.items()
    ),
  }


#
# Convenience sculptor classes to keep the PHIL together with the getter
#

# 1. Mainchain
class PhilGen_Mainchain_Deletion_All(object):

  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.All()


  def __str__(self):

    return """
  .help = "Delete all residues"
{
}"""


class PhilGen_Mainchain_Deletion_Gap(object):

  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.Gap()


  def __str__(self):

    return """
  .short_caption = Filter by residue presence
  .help = "Delete residue if aligned with gap"
  .style = box auto_align
{
}"""


class PhilGen_Mainchain_Deletion_ThresholdBasedSimilarity(object):

  def __init__(self, ssim, threshold):

    self.ssim = ssim
    self.threshold = threshold


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.ThresholdBasedSimilarity(
      ss_calc = self.ssim( params = params.similarity_calculation ),
      threshold = params.threshold,
      )


  def __str__(self):

    return """
  .short_caption = Filter by sequence similarity, truncate by specified threshold
  .help = "Delete residue if sequence similarity is low"
  .style = box auto_align
{
  threshold = %(threshold)s
    .help = "Threshold to accept a residue"
    .short_caption = Threshold to accept a residue
    .type = float
    .optional = False

  similarity_calculation%(ssim)s

}""" % {
  "threshold": "%.2f" % self.threshold,
  "ssim": self.ssim,
  }


class PhilGen_Mainchain_Deletion_CompletenessBasedSimilarity(object):

  def __init__(self, ssim, offset):

    self.ssim = ssim
    self.offset = offset


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.CompletenessBasedSimilarity(
      ss_calc = self.ssim( params = params.similarity_calculation ),
      offset = params.offset,
      )


  def __str__(self):

    return """
  .short_caption = Filter by sequence similarity, truncate to target completeness
  .help = "Delete residues based on sequence similarity to get same number of gaps \
          as the Schwarzenbacher algorithm"
  .style = box auto_align
{
  offset = %(offset)s
    .help = "Completeness in fraction of model length \
        (0.0 = completeness from Schwarzenbacher algorithm, useful range: +/-0.05)"
    .short_caption = Model completeness wrt Schwarzenbacher-algorithm
    .type = float
    .optional = False

  similarity_calculation%(ssim)s
}""" % {
  "offset": self.offset,
  "ssim": self.ssim,
  }


class PhilGen_Mainchain_Deletion_ResidueCountBasedSimilarity(object):

  def __init__(self, ssim, offset):

    self.ssim = ssim
    self.offset = offset


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.ResidueCountBasedSimilarity(
      ss_calc = self.ssim( params = params.similarity_calculation ),
      offset = params.offset,
      )


  def __str__(self):

    return """
  .short_caption = Filter by sequence similarity, delete target number of residue
  .help = "Delete residues based on sequence similarity to delete the same number \
          of residues as the Schwarzenbacher algorithm"
  .style = box auto_align
{
  offset = %(offset)s
    .help = "Completeness in fraction of model length \
        (0.0 = completeness from Schwarzenbacher algorithm, useful range: +/-0.05)"
    .short_caption = Model completeness wrt Schwarzenbacher-algorithm
    .type = float
    .optional = False

  similarity_calculation%(ssim)s
}""" % {
  "offset": self.offset,
  "ssim": self.ssim,
  }

class PhilGen_Mainchain_Deletion_RemoveLong(object):

  def __init__(self, min_length):

    self.min_length = min_length


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.RemoveLong( min_length = params.minimum_length )


  def __str__(self):

    return """
  .short_caption = Filter by residue presence
  .help = "Delete gap segments if longer than a threshold"
  .style = box auto_align
{
  minimum_length = %(min_length)s
    .help = "Minimum length for mainchain segment to remove"
    .type = int
    .optional = False
}""" % {
  "min_length": self.min_length,
  }

class PhilGen_Mainchain_Deletion_RMSError(object):

  def __init__(self, threshold, missing_value_substitution):

    self.threshold = threshold
    self.missing_value_substitution = missing_value_substitution


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.RMSError(
      threshold = params.threshold,
      substitution = self.missing_value_substitution(
        params = params.missing_value_substitution,
        ),
      )


  def __str__(self):

    return """
  .short_caption = Filter by residue error
  .help = "Delete residues if estimated error is larger than a threshold"
  .style = box auto_align
{
  threshold = %(threshold)s
    .help = "Maximum allowed error for a residue"
    .type = float( value_min = 0. )
    .optional = False

  missing_value_substitution
    .help = "Policy to fill in missing values"
  {
    %(substitution)s
  }
}""" % {
  "threshold": self.threshold,
  "substitution": self.missing_value_substitution,
  }


class PhilGen_Mainchain_Deletion_SuperpositionError(object):

  def __init__(self, threshold, missing_value_substitution):

    self.threshold = threshold
    self.missing_value_substitution = missing_value_substitution


  def __call__(self, params):

    from phaser.chisellib import deletion
    return deletion.SuperpositionError(
      threshold = params.threshold,
      substitution = self.missing_value_substitution(
        params = params.missing_value_substitution,
        ),
      )


  def __str__(self):

    return """
  .short_caption = Filter by superposition error
  .help = "Delete residues if estimated error is larger than a threshold"
  .style = box auto_align
{
  threshold = %(threshold)s
    .help = "Maximum allowed error for a residue"
    .type = float( value_min = 0. )
    .optional = False

  missing_value_substitution
    .help = "Policy to fill in missing values"
  {
    %(substitution)s
  }
}""" % {
  "threshold": self.threshold,
  "substitution": self.missing_value_substitution,
  }


class PhilGen_Mainchain_Polishing_RemoveShort(object):

  def __init__(self, min_length):

    self.min_length = min_length


  def __call__(self, params):

    from phaser.chisellib import polishing
    return polishing.RemoveShort( min_length = params.minimum_length )


  def __str__(self):

    return """
  .help = "Delete short unconnected segments"
{
  minimum_length = %(min_length)s
    .help = "Minimum length"
    .type = int
    .optional = False
    .short_caption = Minimum chain length to keep
}""" % {
  "min_length": self.min_length
  }


class PhilGen_Mainchain_Polishing_UndoShort(object):

  def __init__(self, max_length):

    self.max_length = max_length


  def __call__(self, params):

    from phaser.chisellib import polishing
    return polishing.UndoShort( max_length = params.maximum_length )


  def __str__(self):

    return """
  .help = "Delete short gaps"
{
  maximum_length = %(max_length)s
    .help = "Maximum length"
    .type = int
    .optional = False
    .short_caption = Maximum gap length to keep
}""" % {
  "max_length": self.max_length
  }


class PhilGen_Mainchain_Polishing_KeepRegular(object):

  def __init__(self, max_length):

    self.max_length = max_length


  def __call__(self, params):

    from phaser.chisellib import polishing
    return polishing.KeepRegular( max_length = params.maximum_length )


  def __str__(self):

    return """
  .help = "Keep residues in secondary structure"
{
  maximum_length = %(max_length)s
    .help = "Maximum length"
    .type = int
    .optional = False
    .short_caption = Maximum chain length to keep
}""" % {
  "max_length": self.max_length,
  }


class PhilGen_Mainchain(object):

  def __init__(self, deletion, polishing, remove_unaligned):

    self.deletion = deletion
    self.polishing = polishing
    self.remove_unaligned = remove_unaligned


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.Mainchain(
      deletions = self.deletion( params = params.mainchain.deletion ),
      polishings = self.polishing( params = params.mainchain.polishing ),
      remove_unaligned = params.mainchain.remove_unaligned,
      )


  def __str__(self):

    return """
mainchain
  .help = "Options for main chain processing"
{
  deletion
    .help = "Mainchain deletion algorithms"
  {
    %(deletion)s
  }

  polishing
    .help = "Mainchain polishing algorithms"
  {
    %(polishing)s
  }

  remove_unaligned = %(remove_unaligned)s
    .help = "Delete residues that could not be matched to an alignment"
    .type = bool
    .optional = False
}""" % {
  "deletion": self.deletion,
  "polishing": self.polishing,
  "remove_unaligned": self.remove_unaligned,
  }


class PhilGen_Discard(object):

  def __init__(self, keep):

    self.keep = keep


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.Discard(
      keep = params.keep if params.keep is not None else [],
      )


  def __str__(self):

    return """
keep = %(keep)s
  .help = "Keep named hetero residues"
  .type = strings""" % {
  "keep": " ".join( self.keep ) if self.keep else "None",
  }


class PhilGen_KeepAttached(object):

  def __init__(self, maxdepth, maxlength):

    self.maxdepth = maxdepth
    self.maxlength = maxlength


  def __call__(self, params):

    from phaser import chisellib

    maxdepth = params.maximum_depth

    if maxdepth == 0:
      return chisellib.Discard( keep = [] )

    else:
      return chisellib.KeepAttached(
        maxdepth = maxdepth,
        maxlength = params.maximum_bond_length,
        )


  def __str__(self):

    return """
maximum_depth = %(maxdepth)s
  .help = "Keep chain up to maximum depth (None to keep all)"
  .type = int( value_min = 0 )

maximum_bond_length = %(maxlength)s
  .help = "Maximum bond length for glycosidic bond"
  .type = float( value_min = 0.0 )""" % {
  "maxdepth": self.maxdepth,
  "maxlength": self.maxlength,
  }


# 2. Morphing
class PhilGen_Morph_Pruning_Null(object):

  def __call__(self, params):

    from phaser.chisellib import pruning
    return pruning.Null()


  def __str__(self):

    return """
  .help = "Do not impose bond distance threshold"
  .style = box auto_align
{
}"""


class PhilGen_Morph_Pruning_Schwarzenbacher(object):

  def __init__(self, pruning_level):

    self.pruning_level = pruning_level


  def __call__(self, params):

    from phaser.chisellib import pruning
    return pruning.Schwarzenbacher( level = params.pruning_level )


  def __str__(self):

    return """
    .help = "Truncate atoms if target residue != source residue"
{
    pruning_level = %(level)s
        .help = "Level of truncation"
        .type = int
        .optional = False
}""" % {
  "level": self.pruning_level,
  }


class PhilGen_Morph_Pruning_Similarity(object):

  def __init__(self, pruning_level, full_length_limit, full_truncation_limit, ssim):

    self.pruning_level = pruning_level
    self.full_length_limit = full_length_limit
    self.full_truncation_limit = full_truncation_limit
    self.ssim = ssim


  def __call__(self, params):

    from phaser.chisellib import pruning
    return pruning.Similarity(
      ss_calc = self.ssim( params = params.similarity_calculation ),
      lower = params.full_truncation_limit,
      upper = params.full_length_limit,
      level = params.pruning_level,
      base = 1,
      )


  def __str__(self):

    return """
    .help = "Truncate atoms based on sequence similarity"
    .short_caption = Similarity-based truncation
    .style = box auto_align
{
    pruning_level = %(level)s
        .help = "Level of intermediate truncation"
        .type = int
        .optional = False

    full_length_limit = %(full_length)s
        .help = "Limit of no truncation"
        .type = float
        .optional = False

    full_truncation_limit = %(full_trunc)s
        .help = "Limit for full truncation"
        .type = float
        .optional = False

    similarity_calculation%(ssim)s
}""" % {
  "level": self.pruning_level,
  "full_length": self.full_length_limit,
  "full_trunc": self.full_truncation_limit,
  "ssim": self.ssim,
  }


class PhilGen_Morph_Pruning(object):

  def __init__(self, algorithm, pruning_level_unaligned):

    self.algorithm = algorithm
    self.pruning_level_unaligned = pruning_level_unaligned


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.SidechainPruning(
      algorithm = self.algorithm( params = params.pruning ),
      pruning_level_unaligned = params.pruning.pruning_level_unaligned,
      )


  def __str__(self):

    return """
pruning
  .help = "Options for sidechain pruning"
{
  %(algorithm)s

  pruning_level_unaligned = %(pruning_level_unaligned)s
    .help = "Pruning level for residues that could not be matched to an alignment"
    .type = int
    .optional = False
}""" % {
  "algorithm": self.algorithm,
  "pruning_level_unaligned": self.pruning_level_unaligned,
  }


class PhilGen_Morph_Mapping2D(object):

  def __init__(self, match_chemical_elements):

    self.match_chemical_elements = match_chemical_elements


  def __call__(self, params):

    from phaser.chisellib import sidechain_mapping

    if params.match_chemical_elements:
      return sidechain_mapping.TwoDimensional.ElementAware()

    else:
      return sidechain_mapping.TwoDimensional.ElementAgnostic()


  def __str__(self):

    return """
  .help = Match atoms by connectivity
{
  match_chemical_elements = %(match_elements)s
    .help = "Take chemical element into account"
    .type = bool
    .optional = False
}
""" % {
  "match_elements": self.match_chemical_elements,
  }


class PhilGen_Morph_Mapping3D(object):

  def __init__(self, match_chemical_elements, tolerance, fine_sampling):

    self.match_chemical_elements = match_chemical_elements
    self.tolerance = tolerance
    self.fine_sampling = fine_sampling


  def __call__(self, params):

    from phaser.chisellib import sidechain_mapping

    if params.match_chemical_elements:
      creator = sidechain_mapping.ThreeDimensional.ElementAware

    else:
      creator = sidechain_mapping.ThreeDimensional.ElementAgnostic

    return creator( tolerance = params.tolerance, fine_sampling = params.fine_sampling )


  def __str__(self):

    return """
  .help = Match atoms by geometry considering all rotamers
{
  match_chemical_elements = %(match_elements)s
    .help = "Take chemical element into account"
    .type = bool
    .optional = False

  tolerance = %(tolerance)s
    .help = "Distance tolerance"
    .type = float
    .optional = False

  fine_sampling = %(sampling)s
    .help = "Use fine sampling for rotamers"
    .type = bool
    .optional = False
}""" % {
  "match_elements": self.match_chemical_elements,
  "tolerance": self.tolerance,
  "sampling": self.fine_sampling,
  }


class PhilGen_Morph_Mapping(object):

  def __init__(self, algorithm, map_if_identical):

    self.algorithm = algorithm
    self.map_if_identical = map_if_identical


  def __call__(self, params):

    mapper = self.algorithm( params = params.mapping )

    if params.mapping.map_if_identical:
      return mapper

    else:
      from phaser.chisellib import sidechain_mapping
      return sidechain_mapping.IdentityIfSame( mapping = mapper )


  def __str__(self):

    return """
mapping
  .help = "Options for sidechain mapping"
{
  %(algorithm)s

  map_if_identical = %(map_identical)s
    .help = "Do mapping procedure for identical residues types"
    .type = bool
    .optional = False
}""" % {
  "algorithm": self.algorithm,
  "map_identical": self.map_if_identical,
  }


class PhilGen_Morph_Completion(object):

  def __init__(self, choices, default):

    self.choices = choices
    self.default = default

    if self.default is not None and self.default not in self.choices:
      raise RuntimeError("Default not in allowed options")


  def __call__(self, params):

    from phaser.chisellib import rename

    if params.completion is not None:
      return rename.completion( builder = params.completion )

    else:
      return rename.no_completion


  def __str__(self):

    return """
completion = %(choices)s
  .help = "Method to build missing sidechain atoms"
  .type = choice
  .optional = True
""" % {
  "choices": tbx_utils.choice_string(
    possible = self.choices,
    default = self.default,
    ),
  }


def use_original_resnames(mapping, keep_ptm, completion, gapname):

  from phaser.chisellib import rename

  if keep_ptm:
    creator = rename.Original.TargetPtm

  else:
    creator = rename.Original.Target

  return creator( mapping = mapping, completion = completion )


def use_target_sequence_resnames(mapping, keep_ptm, completion, gapname):

  from phaser.chisellib import rename

  if keep_ptm:
    creator = rename.Sequence.TargetPtm

  else:
    creator = rename.Sequence.Target

  return creator( mapping = mapping, completion = completion, gapname = gapname )


class PhilGen_Morph_Rename(object):

  GETTER_FOR = {
    "original": use_original_resnames,
    "target": use_target_sequence_resnames,
    }

  def __init__(self, keep_ptm, gapname, mapping, completion):

    self.keep_ptm = keep_ptm
    self.gapname = gapname
    self.mapping = mapping
    self.completion = completion


  def __call__(self, params):

    return self.GETTER_FOR[ params.rename.use ](
      mapping = self.mapping( params = params ),
      keep_ptm = params.rename.keep_ptm,
      completion = self.completion( params = params ),
      gapname = params.rename.gapname,
      )


  def rename_phil(self):

    return """
rename
  .help = "Residue renaming"
{
  use = %(choice)s
    .help = "Method to use"
    .type = choice
    .optional = False
    .short_caption = "Residue renaming"

  keep_ptm = %(keep_ptm)s
    .help = "Preserve post-translational modification of model residue if base residue types agree"
    .type = bool
    .optional = False

  gapname = %(gapname)s
    .help = "Name residues corresponding to alignment gaps"
    .type = str
    .optional = False
}""" % {
  "choice": tbx_utils.choice_string( possible = self.GETTER_FOR, default = "target" ),
  "keep_ptm": self.keep_ptm,
  "gapname": self.gapname,
  }

  def __str__(self):

    return  "\n".join(
      [ self.rename_phil(), str( self.mapping ), str( self.completion ) ]
      )


class PhilGen_Morph(object):

  def __init__(self, pruning, rename):

    self.pruning = pruning
    self.rename = rename


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.Morphing(
      pruning = self.pruning( params = params ),
      rename = self.rename( params = params ),
      )


  def __str__(self):

    return "\n".join( [ str( self.pruning ), str( self.rename ) ] )


# 3. Renumber
def use_target_sequence_numbering(start):

  from phaser.chisellib import renumber
  return renumber.Sequence.Target( start = start )


def use_model_sequence_numbering(start):

  from phaser.chisellib import renumber
  return renumber.Sequence.Model( start = start )


def use_original_numbering(start):

  from phaser.chisellib import renumber
  return renumber.Original()


class PhilGen_Renumber(object):

  GETTER_FOR = {
    "target": use_target_sequence_numbering,
    "model": use_model_sequence_numbering,
    "original": use_original_numbering,
    }

  def __init__(self, start):

    self.start = start


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.Renumber(
      algorithm = self.GETTER_FOR[ params.renumber.use ](
        start = params.renumber.start,
        ),
      )


  def __str__(self):

    return """
renumber
  .help = "Residue renumbering"
{
  use = %(choice)s
    .help = "Method to use"
    .type = choice
    .optional = False
    .short_caption = Mainchain renumbering

  start = %(start)s
    .help = "Start residue number"
    .type = int
    .optional = False
}""" % {
  "choice": tbx_utils.choice_string( possible = self.GETTER_FOR, default = "target" ),
  "start": self.start,
  }


# 5. Bfactor
class PhilGen_Bfactor_Original(object):

  def __init__(self, factor):

    self.factor = factor


  def __call__(self, params):

    from phaser.chisellib import bfactor
    return bfactor.Scale( algorithm = bfactor.Original(), scale = params.factor )


  def __str__(self):

    return """
  .help = "Use original bfactors to predict new B-values"
  .short_caption = Original B-factors
  .style = auto_align box
{
  factor = %(factor)s
    .help = "Scale factor"
    .type = float
    .optional = False
}""" % {
  "factor": "%.2f" % self.factor,
  }


class PhilGen_Bfactor_ASA(object):

  def __init__(self, precision, probe_radius, factor):

    self.precision = precision
    self.probe_radius = probe_radius
    self.factor = factor


  def __call__(self, params):

    from phaser.chisellib import bfactor
    return bfactor.Scale(
      algorithm = bfactor.AccessibleSurfaceArea(
        probe = params.probe_radius,
        precision = params.precision,
        ),
      scale = params.factor,
      )


  def __str__(self):

    return """
  .help = "Use accessible surface area to predict new B-values"
  .short_caption = Surface area-based
  .style = box auto_align
{
  precision = %(precision)s
    .help = "Number of points per atom"
    .type = int
    .optional = False

  probe_radius = %(probe)s
    .help = "Radius for probing surface accessibility"
    .type = float
    .optional = False

  factor = %(factor)s
    .help = "Scale factor"
    .type = float
    .optional = False
}""" % {
  "precision": self.precision,
  "probe": "%.2f" % self.probe_radius,
  "factor": "%.2f" % self.factor,
  }


class PhilGen_Bfactor_Similarity(object):

  def __init__(self, ssim, factor):

    self.ssim = ssim
    self.factor = factor


  def __call__(self, params):

    from phaser.chisellib import bfactor
    return bfactor.Scale(
      algorithm = bfactor.SequenceSimilarity(
        ss_calc = self.ssim( params = params.similarity_calculation ),
        ),
      scale = params.factor,
      )


  def __str__(self):

    return """
.help = "Use sequence similarity to predict new B-values"
    .short_caption = Sequence similarity-based
    .style = box auto_align
{
  similarity_calculation%(ssim)s

  factor = %(factor)s
    .help = "Scale factor"
    .type = float
    .optional = False
}""" % {
  "ssim": self.ssim,
  "factor": "%.2f" % self.factor,
  }


class PhilGen_Bfactor_RMS(object):

  def __init__(self, factor, missing_value_substitution):

    self.factor = factor
    self.missing_value_substitution = missing_value_substitution


  def __call__(self, params):

    from phaser.chisellib import bfactor
    return bfactor.Scale(
      algorithm = bfactor.RMSError(
        substitution = self.missing_value_substitution(
          params = params.missing_value_substitution,
          ),
        ),
      scale = params.factor,
      )


  def __str__(self):

    return """
.help = "Use external RMS error estimates to calculate B-values"
    .short_caption = Predicted-error based
{
  factor = %(factor)s
    .help = "Scale factor"
    .type = float
    .optional = False

  missing_value_substitution
    .help = "Policy to fill in missing values"
  {
    %(substitution)s
  }
}""" % {
  "factor": "%.2f" % self.factor,
  "substitution": self.missing_value_substitution,
  }


class PhilGen_Bfactors(object):

  def __init__(self, algorithms, minimum_b):

    self.algorithms = algorithms
    self.minimum_b = minimum_b


  def __call__(self, params):

    from phaser import chisellib
    return chisellib.Bfactor(
      algorithms = self.algorithms( params = params.bfactor ),
      minimum_b = params.bfactor.minimum_b,
      )


  def __str__(self):

    return """
bfactor
  .help = "Configure B-factor modification"
{
  %(algorithm)s

  minimum_b = %(min_b)s
    .help = "Minimum B-factor"
    .type = float
    .optional = False
}
""" % {
  "algorithm": self.algorithms,
  "min_b": "%.2f" % self.minimum_b,
  }


# Main Sculptor
class PhilGen_Sculptor(object):

  def __init__(self, processors):

    self.processors = processors


  def __call__(self, params):

    processors = []

    for philgen in self.processors:
      processors.append( philgen( params = params ) )

    from phaser import chisellib
    return chisellib.Sculptor( processors = processors )


  def __str__(self):

    return "\n".join( str( philgen ) for philgen in self.processors )


#
# Phil fragments
#

# Chain alignment
PHIL_CHAIN_ALIGNMENT = chainalign.PHIL_SCULPTOR_CHAIN_ALIGNMENT

from phaser.command_line import simple_homology_model

PHIL_ERROR_SEARCH = """
min_sequence_identity = 0.8
  .help = "Minimum sequence identity to accept error file"
  .type = float( value_min = 0.0, value_max = 1.0 )
  .optional = False

min_sequence_overlap = 0.8
  .help = "Minimum sequence overlap to accept error file"
  .type = float( value_min = 0.0, value_max = 1.0 )
  .optional = False

calculate_if_not_provided = False
  .help = "Use Rosetta and the ProQ2 server to get error estimates"
  .type = bool
  .optional = False

output_prefix = None
  .help = "File name for results of intermediate steps"
  .type = str
  .optional = True

homology_modelling
  .help = "Parameters for homology modelling"
{
  %(homology_modelling)s
}
""" % {
  "homology_modelling": simple_homology_model.PHIL_HOMOLOGY_MODELLING,
  }


# Output
class ChaindiffProcessor(object):

  def __init__(self, execute, simplify):

    self.execute = execute
    self.simplify = simplify


  def single_entity_modifications(self, diff):

    instructions = []
    terminate = False

    if diff.is_moribund():
      if diff.is_marked_for_removal():
        explanation = "marked"
        terminate = True

      else:
        explanation = "all children deleted"

      instructions.append(
        diff.corresponding_delete_instruction( explanation = explanation )
        )

      if self.simplify:
        return ( instructions, True )

    instructions.extend( diff.modifications() )
    return ( instructions, terminate )


  def __call__(self, diff, channel):

    ( modifications, terminate ) = self.single_entity_modifications( diff = diff )
    entity = diff.entity()

    for instruction in modifications:
      self.execute( instruction = instruction, entity = entity, channel = channel )

    return ( len( modifications ), terminate )


  @staticmethod
  def process(processor, chaindiff, channel):

    channel.heading( text = "%s:" % chaindiff, level = 5 )
    channel.unordered_list_begin( separate_items = False )
    ( count, terminate ) = processor( diff = chaindiff, channel = channel )
    channel.list_end()

    if terminate:
      return count

    channel.heading( text = "Residue groups:", level = 5 )
    channel.ordered_list_begin( marker = "1" )

    for rg in chaindiff.children():
      rgdiff = chaindiff.get_diff_for( residue_group = rg )
      channel.list_item_begin( text = str( rgdiff ) )
      channel.unordered_list_begin( separate_items = False )
      ( subcount, terminate ) = processor( diff = rgdiff, channel = channel )
      count += subcount
      channel.list_end()

      if not terminate:
        channel.heading( text = "Atom groups:", level = 5 )
        channel.ordered_list_begin( marker = "a" )

        for ag in rgdiff.children():
          agdiff = rgdiff.get_diff_for( atom_group = ag )
          channel.list_item_begin( text = str( agdiff ) )
          channel.unordered_list_begin( separate_items = False )
          ( subcount, terminate ) = processor( diff = agdiff, channel = channel )
          count += subcount
          channel.list_end()

          if not terminate:
            channel.heading( text = "Atoms:", level = 5 )
            channel.ordered_list_begin( marker = "i" )

            for a in agdiff.children():
              adiff = agdiff.get_diff_for( atom = a )
              channel.list_item_begin( text = str( adiff ) )
              channel.unordered_list_begin( separate_items = False )
              ( subcount, terminate ) = processor( diff = adiff, channel = channel )
              count += subcount
              channel.list_end()
              channel.list_item_end()

            channel.list_end()

          channel.list_item_end()

        channel.list_end()

      channel.list_item_end()

    channel.list_end()

    return count


  @staticmethod
  def log_modification(instruction, entity, channel):

    channel.list_item( text = str( instruction ) )


  @staticmethod
  def log_and_execute_modification(instruction, entity, channel):

    channel.list_item( text = str( instruction ) )
    instruction( entity )


  @classmethod
  def log(cls, chaindiff, channel, simplify):

    return cls.process(
      processor = cls( execute = cls.log_modification, simplify = simplify ),
      chaindiff = chaindiff,
      channel = channel,
      )


  @classmethod
  def log_and_execute(cls, chaindiff, channel, simplify):

    return cls.process(
      processor = cls( execute = cls.log_and_execute_modification, simplify = simplify ),
      chaindiff = chaindiff,
      channel = channel,
      )


def write_pdb_output(prefix, root, crystal, annotations, identities, chaindiffs, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  info.field( name = "Output format", value = "PDB" )
  info.ordered_list_begin()

  for ( cdiff, ann ) in zip( chaindiffs, annotations ):
    info.list_item_begin( text = ann )
    count = ChaindiffProcessor.log_and_execute( chaindiff = cdiff, channel = verbose, simplify = True )
    info.field( name = "Number of modifications", value = count )
    info.list_item_end()

  info.list_end()

  info.task_begin( text = "Regularizing hierarchy" )
  tbx_utils.regularize_hierarchy( root = root )
  info.task_end()

  info.task_begin( text = "Renumbering chains" )
  tbx_utils.renumber_chains_in_hierarchy( root = root )
  info.task_end()

  info.task_begin( text = "Preparing output file" )
  file_name = prefix + ".pdb"

  with open( file_name, "w" ) as fout:
    fout.write( tbx_utils.get_phaser_revision_remark() + "\n")
    if crystal is not None:
      from iotbx.pdb import format_cryst1_and_scale_records
      fout.write( format_cryst1_and_scale_records( crystal_symmetry=crystal ) )
      fout.write( "\n" )

    for ( cdiff, ident ) in zip( chaindiffs, identities ):
      if not ident:
        continue

      model = cdiff.chain.parent()

      if not model or not model.parent():
        continue

      fout.write(
        tbx_utils.get_phaser_model_identity_remark( mid = model.id, identity = ident )
        )
      fout.write( "\n" )

    fout.write( root.as_pdb_string() )
    fout.write( "\n" )

  info.task_end()
  info.field( name = "PDB file written", value = file_name )

  return file_name


OUTPUT_METHOD_FOR = {
  "pdb": write_pdb_output,
  }


# Sequence similarity
DEFAULT_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY = PhilGen_SequenceSimilarity(
  matrices = tbx_utils.PhilChoice(
    choices = mmtype.PROTEIN.scoring_matrix_dict.keys(),
    default = "blosum62",
    ),
  smoothing_for = RESIDUE_FRAME_AVERAGED_SEQSIM_METHOD_CALLED,
  smoothing_default = "linear",
  window = 5,
  weightings = tbx_utils.PhilChoice(
    choices = WEIGHTING_SCHEME_GETTER_FOR,
    default = "triangular",
    ),
  bleed_length = 3,
  maxdist = 10,
  )

PRUNING_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY = PhilGen_SequenceSimilarity(
  matrices = tbx_utils.PhilChoice(
    choices = mmtype.PROTEIN.scoring_matrix_dict.keys(),
    default = "blosum62",
    ),
  smoothing_for = RESIDUE_FRAME_AVERAGED_SEQSIM_METHOD_CALLED,
  smoothing_default = "linear",
  window = 1,
  weightings = tbx_utils.PhilChoice(
    choices = WEIGHTING_SCHEME_GETTER_FOR,
    default = "triangular",
    ),
  bleed_length = 3,
  maxdist = 10,
  )

DEFAULT_PROTEIN_ATOM_FRAME_SEQUENCE_SIMILARITY = PhilGen_SequenceSimilarity(
  matrices = tbx_utils.PhilChoice(
    choices = mmtype.PROTEIN.scoring_matrix_dict.keys(),
    default = "blosum62",
    ),
  smoothing_for = ATOM_FRAME_AVERAGED_SEQSIM_METHOD_CALLED,
  smoothing_default = "linear",
  window = 5,
  weightings = tbx_utils.PhilChoice(
    choices = WEIGHTING_SCHEME_GETTER_FOR,
    default = "triangular",
    ),
  bleed_length = 3,
  maxdist = 10,
  )

DEFAULT_MISSING_VALUE_SUBSTITUTION = PhilGen_ChoiceScope(
  philgens = [
    ( "maximum_value", PhilGen_Substitution_MaximumValue() ),
    (
      "scaled_interpolated_value",
      PhilGen_Substitution_ScaledInterpolatedValue(
        extrapolation_step_scale = 1.20,
        interpolation_step_scale = 1.10,
        ),
      ),
    ],
  default = "maximum_value",
  extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", False ) ],
  )

# Phils
PHIL_SCULPTOR_PROTEIN = PhilGen_Sculptor(
  processors = [
    PhilGen_Mainchain(
      deletion = PhilGen_MultiChoiceScope(
        philgens = [
          ( "gap", PhilGen_Mainchain_Deletion_Gap() ),
          (
            "threshold_based_similarity",
            PhilGen_Mainchain_Deletion_ThresholdBasedSimilarity(
              ssim = DEFAULT_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY,
              threshold = -0.2,
              ),
            ),
          (
            "completeness_based_similarity",
            PhilGen_Mainchain_Deletion_CompletenessBasedSimilarity(
              ssim = DEFAULT_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY,
              offset = 0.0,
              ),
            ),
          (
            "residue_count_based_similarity",
            PhilGen_Mainchain_Deletion_ResidueCountBasedSimilarity(
              ssim = DEFAULT_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY,
              offset = 0.0,
              ),
            ),
          ( "remove_long", PhilGen_Mainchain_Deletion_RemoveLong( min_length = 3 ) ),
          (
            "rms",
            PhilGen_Mainchain_Deletion_RMSError(
              threshold = 5.0,
              missing_value_substitution = DEFAULT_MISSING_VALUE_SUBSTITUTION,
              ),
            ),
          (
            "superposition",
            PhilGen_Mainchain_Deletion_SuperpositionError(
              threshold = 2.0,
              missing_value_substitution = DEFAULT_MISSING_VALUE_SUBSTITUTION,
              ),
            ),
          ],
        defaults = [ "gap" ],
        extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", True ),
                        ( "style", "noauto" ),
                        ( "short_caption", "Mainchain deletion" ) ],
        ),
      polishing = PhilGen_MultiChoiceScope(
        philgens = [
          ( "remove_short", PhilGen_Mainchain_Polishing_RemoveShort( min_length = 3 ) ),
          ( "undo_short", PhilGen_Mainchain_Polishing_UndoShort( max_length = 2 ) ),
          ( "keep_regular", PhilGen_Mainchain_Polishing_KeepRegular( max_length = 1 ) ),
          ],
        defaults = [],
        extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", True ),
                        ( "style", "noauto" ),
                        ( "short_caption", "Mainchain polishing" ) ],
        ),
      remove_unaligned = True,
      ),
    PhilGen_Bfactors(
      algorithms = PhilGen_MultiChoiceScope(
        philgens = [
          ( "original", PhilGen_Bfactor_Original( factor = 1 ) ),
          (
            "asa",
            PhilGen_Bfactor_ASA( precision = 960, probe_radius = 1.4, factor = 2 ),
            ),
          (
            "similarity",
            PhilGen_Bfactor_Similarity(
              ssim = DEFAULT_PROTEIN_ATOM_FRAME_SEQUENCE_SIMILARITY,
              factor = -100,
              ),
            ),
          (
            "rms",
            PhilGen_Bfactor_RMS(
              factor = 1,
              missing_value_substitution = DEFAULT_MISSING_VALUE_SUBSTITUTION,
              ),
            ),
          ],
        defaults = [ "original" ],
        extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", False ),
                        ("style", "noauto"),
                        ( "short_caption", "B-factor predication algorithm" ) ],
        ),
      minimum_b = 10,
      ),
    PhilGen_Renumber( start = 1 ),
    PhilGen_Morph(
      pruning = PhilGen_Morph_Pruning(
        algorithm = PhilGen_ChoiceScope(
          philgens = [
            ( "null", PhilGen_Morph_Pruning_Null() ),
            (
              "schwarzenbacher",
              PhilGen_Morph_Pruning_Schwarzenbacher( pruning_level = 2 ),
              ),
            (
              "similarity",
              PhilGen_Morph_Pruning_Similarity(
                pruning_level = 2,
                full_length_limit = 0.2,
                full_truncation_limit = -0.2,
                ssim = PRUNING_PROTEIN_RESIDUE_FRAME_SEQUENCE_SIMILARITY,
                ),
              ),
            ],
          default =  "schwarzenbacher",
          extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", False ),
                          ( "style", "noauto" ),
                          ( "short_caption", "Sidechain pruning" ) ],
          ),
        pruning_level_unaligned = 2,
        ),
      rename = PhilGen_Morph_Rename(
        keep_ptm = False,
        gapname = "ALA",
        mapping = PhilGen_Morph_Mapping(
          algorithm = PhilGen_ChoiceScope(
            philgens = [
              (
                "connectivity",
                PhilGen_Morph_Mapping2D( match_chemical_elements = True ),
                ),
              (
                "geometry",
                PhilGen_Morph_Mapping3D(
                  match_chemical_elements = False,
                  tolerance = 0.1,
                  fine_sampling = False,
                  ),
                ),
              ],
            default = "connectivity",
            extra_infos = [ ( "help", "Algorithm to use" ),
                            ( "optional", False ) ],
            ),
          map_if_identical = True,
          ),
        completion = PhilGen_Morph_Completion(
          choices = mmtype.PROTEIN.builder_dict.keys(),
          default = None,
          ),
        ),
      ),
    ],
  )

PHIL_SCULPTOR_NUCLEIC = PhilGen_Sculptor(
  processors = [
    PhilGen_Mainchain(
      deletion = PhilGen_MultiChoiceScope(
        philgens = [
          ( "all", PhilGen_Mainchain_Deletion_All() ),
          (
            "superposition",
            PhilGen_Mainchain_Deletion_SuperpositionError(
              threshold = 2.0,
              missing_value_substitution = DEFAULT_MISSING_VALUE_SUBSTITUTION,
              ),
            ),
          ],
        defaults = [ "all" ],
        extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", True ),
                        ( "style", "noauto" ),
                        ( "short_caption", "Mainchain deletion" ) ],
        ),
      polishing = PhilGen_MultiChoiceScope(
        philgens = [
          ( "remove_short", PhilGen_Mainchain_Polishing_RemoveShort( min_length = 3 ) ),
          ],
        defaults = [],
        extra_infos = [ ( "help", "Algorithm to use" ), ( "optional", True ),
                        ( "style", "noauto"),
                        ( "short_caption", "Mainchain polishing" ) ],
        ),
      remove_unaligned = True,
      ),
    ],
  )


PHIL_SCULPTOR_MONOSACCHARIDE = PhilGen_Sculptor(
  processors = [ PhilGen_KeepAttached( maxdepth = 0, maxlength = 1.50 ) ],
  )


PHIL_SCULPTOR_HETERO = PhilGen_Sculptor(
  processors = [ PhilGen_Discard( keep = [] ) ],
  )

# I/O
PHIL_INPUT_ALIGNMENT = """
file_name = None
  .help = "Alignment file name"
  .type = path
  .optional = False

target_sequence = None
  .help = "Target sequence file name (to locate target sequence in alignment)"
  .type = path
  .optional = False
"""

PHIL_INPUT_ERRORFILES = """
file_name = None
  .help = "Error file name"
  .type = path
  .optional = True

chain_ids = None
  .help = "Which chain IDs the error file corresponds to"
  .type = strings
  .optional = False
"""

PHIL_INPUT = """
model
  .help = "Input pdb file"
  .optional = True
  .short_caption = PDB file
{
  file_name = None
    .help = "PDB file name"
    .short_caption = PDB file
    .optional = False
    .type = path
    .style = bold file_type:pdb

  selection = all
    .help = "Selection string"
    .optional = False
    .short_caption = Atom selection
    .input_size = 400
    .type = str
    .style = bold

  remove_alternate_conformations = False
    .help = "Remove alternate conformations"
    .type = bool
    .optional = False
    .style = bold noauto

  sanitize_occupancies = False
    .help = "Sets occupancies > 1.0 to 1.0"
    .type = bool
    .optional = False
    .style = noauto

  keep_crystal_symmetry = False
    .help = "Keeps the crystal symmetry of the model"
    .type = bool
    .optional = False
}

alignment
  .help = "Input alignment file"
  .multiple = True
  .optional = True
  .short_caption = Sequence alignment file
  .style = auto_align
{
  %(alignment)s
}

homology_search
  .help = "Alignment from homology search file"
  .multiple = True
  .optional = True
{
  file_name = None
    .help = "Homology search file"
    .type = path
    .optional = False

  use = None
    .help = "Which alignments to use"
    .type = ints( value_min = 1 )
    .optional = True
}

sequence
  .help = "Input sequence file"
  .multiple = True
  .optional = True
  .style = auto_align
{
  file_name = None
    .help = "Sequence file"
    .short_caption = Sequence file
    .type = path
    .optional = False

  chain_ids = None
    .help = "Which chain IDs the target sequence applies"
    .short_caption = Chain IDs
    .type = strings
    .optional = False
}

errors
  .help = "Estimated errors for chain"
  .multiple = True
  .optional = True
{
  %(errorfiles)s
}

superposition_errors
  .help = "Superposition errors for chain"
  .multiple = True
  .optional = True
{
  file_name = None
    .help = "Error file name"
    .type = path
    .optional = False

  chain_ids = None
    .help = "Which chain IDs the error file corresponds to"
    .type = strings
    .optional = False
}
""" % {
  "alignment": PHIL_INPUT_ALIGNMENT,
  "errorfiles": PHIL_INPUT_ERRORFILES,
  }

PHIL_OUTPUT = """
include scope libtbx.phil.interface.tracking_params

folder = .
  .help = "Output file folder"
  .type = path
  .optional = False
  .short_caption = "Output file folder"
  .input_size = 400
  .style = directory

root = sculpt
  .help = "Output file root"
  .type = str
  .optional = False
  .short_caption = Output file base
  .input_size = 400
  .style = bold

format = %(format)s
  .help = "Output file format"
  .type = choice
  .optional = False
  .style = hidden
""" % {
  "format": tbx_utils.choice_string(
    possible = OUTPUT_METHOD_FOR.keys(),
    default = "pdb",
    )
  }

PHIL_SCULPTING = """
protein
  .help = "Options to process protein chains"
{
  %(protein)s
}

dna
  .help = "Options to process DNA chains"
{
  %(dna)s
}

rna
  .help = "Options to process RNA chains"
{
  %(rna)s
}

hetero
  .help = "Options to process hetero chains"
{
  %(hetero)s
}

monosaccharide
  .help = "Options to process glycosyl chains"
{
  %(monosaccharide)s
}
""" % {
  "protein": PHIL_SCULPTOR_PROTEIN,
  "dna": PHIL_SCULPTOR_NUCLEIC,
  "rna": PHIL_SCULPTOR_NUCLEIC,
  "hetero": PHIL_SCULPTOR_HETERO,
  "monosaccharide": PHIL_SCULPTOR_MONOSACCHARIDE,
  }

def get_processor_for(params):

  return {
    mmtype.PROTEIN: PHIL_SCULPTOR_PROTEIN( params = params.protein ),
    mmtype.DNA: PHIL_SCULPTOR_NUCLEIC( params = params.dna ),
    mmtype.RNA: PHIL_SCULPTOR_NUCLEIC( params = params.rna ),
    mmtype.MONOSACCHARIDE: PHIL_SCULPTOR_MONOSACCHARIDE( params = params.monosaccharide ),
    mmtype.HETERO: PHIL_SCULPTOR_HETERO( params = params.hetero )
    }

# Master
master_phil = """
input
  .help = "Input files"
{
  %(input)s
}

output
  .help = "Output options"
{
  %(output)s
}

chain_to_alignment_matching
  .help = "Chain-to-alignment matching options"
{
  %(chain_alignment)s
}

error_search
  .help = "Parameters for matching error files and/or estimating errors"
{
  %(error_search)s
}

sculpt
  .help = "Parameters for sculpting"
{
  %(sculpt)s
}
""" % {
  "input": PHIL_INPUT,
  "output": PHIL_OUTPUT,
  "chain_alignment": PHIL_CHAIN_ALIGNMENT,
  "error_search": PHIL_ERROR_SEARCH,
  "sculpt": PHIL_SCULPTING,
  }


parsed_master_phil = lazy_initialization(
  func = tbx_utils.parse_phil,
  phil = master_phil,
  process_includes = True,
  )

# Optional workflow steps
def remove_alternate_conformations(root):

  for rg in root.residue_groups():
    ags = rg.atom_groups()
    assert ags

    if ags[0].altloc == "":
      if 2 <= len( ags ):
        ags[1].altloc = ""
        rg.merge_atom_groups( ags[0], ags[1] )

    else:
      ags[0].altloc = ""

    for ag in ags[1:]:
      rg.remove_atom_group( ag )

  for atom in root.atoms():
    atom.set_occ( 1.0 )


def sanitize_occupancies(root):

  for atom in root.atoms():
    atom.set_occ( max( 0, min( atom.occ, 1.0 ) ) )


def remove_hydrogens(root):

  root.remove_hd()


def restrict_model_count(root, max_count):

  for m in root.models()[max_count:]:
    root.remove_model( m )

  if len( root.models() ) == 1:
    root.models()[0].id = str( 0 )


# Workflow steps that are executable on their own from code
class AlignmentSequence(object):
  """
  A sequence from an alignment
  """

  def __init__(self, name, alignment, index):

    self.name = name
    self._alignment = alignment
    self.index = index


  def alignment(self):

    return self._alignment


  def sequence(self):

    return self.alignment().alignments[ self.index ]


  def sequence_name(self):

    return self.alignment().names[ self.index ]


  def target(self):

    return self.alignment().alignments[ 0 ]


  def gap(self):

    return self._alignment.gap


  def __str__(self):

    return "%s sequence %s (%s)" % (
      self.name,
      self.index + 1,
      self.sequence_name(),
      )


class HomologySearchHit(object):
  """
  A sequence from a homology search
  """

  def __init__(self, name, index, hit):

    self.name = name
    self.index = index
    self.hit = hit


  def alignment(self):

    return self.hit.alignment


  def sequence(self):

    return self.hit.model_alignment_sequence()


  def target(self):

    return self.hit.target_alignment_sequence()


  def gap(self):

    return self.alignment().gap


  def __str__(self):

    return "%s hit %s (%s)" % (
      self.name,
      self.index,
      self.hit.identifier,
      )


def chain_alignment(chain, mmt, trials, params, logger):

  if not trials:
    raise SculptorError("No aligned candidates")

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )

  info.task_begin( text = "Creating chain aligner" )
  aligner = chainalign.IdentityPostFilter.from_params(
    aligner = chainalign.OverlapPrefilter.from_params(
      aligner = chainalign.ChainAlignmentAlgorithm.from_params(
        rgs = chain.residue_groups(),
        mmt = mmt,
        annotation = "chain_%s" % chain.id,
        params = params,
        ),
      params = params,
      ),
    params = params,
    )

  info.task_end()

  info.heading( text = "Available alignments", level = 4 )
  info.unordered_list_begin()
  origin_of = {}

  for strial in trials:
    info.list_item_begin( text = str( strial ) )
    verbose.alignment( alignment = strial.alignment() )

    trial = chainalign.Trial(
      gapped = strial.sequence(),
      gap = strial.gap(),
      annotation = "aliseq_%s" % ( strial.index + 1 ),
      )

    try:
      result = aligner( trial = trial )

    except chainalign.ChainAlignFailure as e:
      info.unformatted_text( text = "Aligner: %s, discarding" % e )

    else:
      info.unformatted_text( text = "Aligner: accepted" )
      info.field( name = "identities", value = result.identities )
      info.field( name = "overlap", value = result.overlaps )
      verbose.heading( text = "Alignment:", level = 6 )
      verbose.alignment( alignment = result.iotbx_alignment() )
      origin_of[ result ] = strial

    info.list_item_end()

  info.list_end()

  if not origin_of:
    raise SculptorError("No aligned candidates")

  best = max( origin_of, key = lambda a: a.identities )
  strial = origin_of[ best ]
  info.field( name = "Selected", value = str( strial ) )
  info.field( name = "identities", value = best.identities )
  info.field( name = "overlap", value = best.overlaps )
  info.field(
      name = "unaligned residues",
      value = len( best.unaligned_residues() ),
      )

  return ( strial, best.residue_alignment(), best.rescode_alignment() )


def align_with_sequence(chain, mmt, target, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.info )

  verbose.heading( text = "Target sequence:", level = 4 )
  verbose.sequence( sequence = target )

  from iotbx import bioinformatics
  model = bioinformatics.sequence(
    name = "CHAIN_%s" % chain.id,
    sequence = mmt.one_letter_sequence_for_rgs( rgs = chain.residue_groups() ),
    )
  verbose.heading( text = "Chain sequence:", level = 4 )
  verbose.sequence( sequence = model )

  info.task_begin( text = "Aligning sequences" )

  from mmtbx import msa
  alignment = msa.get_muscle_alignment_ordered( sequences = [ target, model ] )
  info.task_end()

  info.heading( text = "Alignment:", level = 4 )
  info.alignment( alignment = alignment )

  return ( alignment, 1 )


def sequence_agreement_between(chain, rg_for):

  return sum( 1 for rg in chain.residue_groups()
    if ( rg.resid() in rg_for
      and rg.atom_groups()[0].resname == rg_for[ rg.resid() ].atom_groups()[0].resname )
    )


def estimate_error_from_structure(
  chain,
  chainalignment,
  mmt,
  min_sequence_coverage,
  homology_modelling_params,
  output_file_prefix,
  logger,
  ):

  info = facade.Package( channel = logger.info )
  excspec = (
    simple_homology_model.LoopTooLong,
    simple_homology_model.RosettaError,
    simple_homology_model.UnsupportedMMTypeError,
    )

  try:
    modelroot = simple_homology_model.run(
      chainalignment = chainalignment,
      mmt = mmt,
      params = homology_modelling_params,
      logger = logger,
      )

  except excspec as e:
    raise ErrorEstimationFailure("Homology modelling failed: %s" % e)

  if output_file_prefix is not None:
    outfile = "%s-model.pdb" % output_file_prefix
    info.field( name = "Homology model", value = outfile )
    modelroot.write_pdb_file( outfile )

  from phaser.command_line import proq2_error_estimation

  try:
    proqstr = proq2_error_estimation.run(
      pdbstr = modelroot.as_pdb_string(),
      logger = logger,
      waittime = 5,
      timeout = 600,
      )

  except proq2_error_estimation.ProQ2Error as e:
    raise ErrorEstimationFailure("ProQ2 error estimation failed: %s" % e)

  if output_file_prefix is not None:
    outfile = "%s-proq.pdb" % output_file_prefix
    info.field( name = "ProQ PDB", value = outfile )

    with open( outfile, "w" ) as ofile:
      ofile.write( proqstr )

  import iotbx.pdb
  proqinp = iotbx.pdb.input( source_info = "ProQ2", lines = proqstr.split( "\n" ) )
  proqroot = proqinp.construct_hierarchy()

  if len( proqroot.models() ) != 1 or len( proqroot.models()[0].chains() ) != 1:
    raise ErrorEstimationFailure("ProQ2 structure contains multiple chains")

  proqchain = proqroot.models()[0].chains()[0]

  from phaser.command_line import proq2_pdb_to_sculptor_error_file

  try:
    proqca = proq2_pdb_to_sculptor_error_file.chain_alignment(
      target = "".join( "-" if c is None else c for c in chainalignment.target_sequence() ),
      gap = "-",
      chain = proqchain,
      mmtype = mmt,
      )

  except proq2_pdb_to_sculptor_error_file.Pdb2ErrorConversionFailure as e:
    raise ErrorEstimationFailure(e)

  info.heading( text = "ProQ PDB to target sequence alignment", level = 6 )
  info.alignment( alignment = proqca.iotbx_alignment() )

  errspecs = proq2_pdb_to_sculptor_error_file.error_mapping(
    proq2_rgali = proqca.residue_alignment(),
    tmplt_rgali = chainalignment.residue_sequence(),
    chain = chain,
    )

  if output_file_prefix is not None:
    outfile = "%s.err" % output_file_prefix
    info.field( name = "Error file", value = outfile )

    with open( outfile, "w" ) as ofile:
      proq2_pdb_to_sculptor_error_file.csv_write(
        errspecs = errspecs,
        outstream = ofile,
        )

  seqcov = proq2_pdb_to_sculptor_error_file.error_coverage_statistics(
    errspecs = errspecs,
    length = len( chain.residue_groups() ),
    logger = logger,
    )

  if seqcov < min_sequence_coverage:
    raise ErrorEstimationFailure("Sequence coverage too low, discard")

  return [ es[1] for es in errspecs ]


def preprocess_error_file(filename, logger):

  info = facade.Package( channel = logger.info )
  import csv, io
  with io.open( filename, "r", newline='\n' ) as ifile:
    reader = csv.reader( ifile )
    next(reader) # discard header
    error_for = {}

    for ( resid, resname, error ) in list(reader):
      if resid in error_for:
        info.highlight( text = "Duplicate resid: '%s'" % resid )
        continue

      if error:
        try:
          ferror = float( error )

        except ValueError as ve:
          info.highlight( text = "Cannot convert '%s' to floating-point number" % error )
          ferror = None

      else:
        ferror = None

      error_for[ resid ] = ( resname, ferror )

  return ( filename, error_for )


def find_errors_for(chain, errors_for, min_identity, min_fraction, stage, logger):

  info = facade.Package( channel = logger.info )
  info.heading( text = "Lookup for associated %s error files" % stage, level = 3 )

  if chain.id in errors_for:
    ( filename, error_for ) = errors_for[ chain.id ]
    length = len( chain.residue_groups() )
    info.field( name = "Candidate error file", value = filename )
    mappeds = [
      ( rg, error_for.get( rg.resid() ) ) for rg in chain.residue_groups()
      ]
    info.field_percentage(
      name = "Sequence coverage",
      value = 100.0 * sum( 1 for ( rg, data ) in mappeds if data is not None ) / length,
      digits = 2,
      )
    agreement = sum(
      1 for ( rg, data ) in mappeds if data is not None and rg.atom_groups()[0].resname == data[0]
      )
    seqid = 1.0 * agreement / length
    info.field_percentage(
      name = "Sequence agreement",
      value = 100.0 * seqid,
      digits = 2,
      )
    errors = [ data[1] for ( rg, data ) in mappeds if data is not None ]

    seqfrac = sum( 1.0 for err in errors if err is not None ) / length
    info.field_percentage(
      name = "Known errors",
      value = 100.0 * seqfrac,
      digits = 2,
      )

    if min_identity <= seqid and min_fraction <= seqfrac:
      return errors

    else:
      info.highlight( text = "Error file specified for chain fail criteria" )

  info.unformatted_text( "No error file associated with chain %s" % chain.id )
  return None


def no_error_search_for(chain, errors_for, min_identity, min_fraction, stage, logger):

  return None


def reformat_alignment_sequence(sequence, gap):

  return [ c if c != gap else None for c in sequence ]


def create_chain_alignment(
  name,
  chain,
  mmt,
  sequence_for,
  chainalign_params,
  alignments,
  logger,
  ):

  info = facade.Package( channel = logger.info )

  from phaser.chisellib import NoAlignment, ChainAlignment

  if not mmt.alignable():
    info.unformatted_text( text = "This molecule type cannot be aligned" )
    chainalignment = NoAlignment
    errors = no_error_search_for

  else:
    if chain.id in sequence_for:
      info.unformatted_text( text = "Target sequence provided for alignment" )
      ( alignment, index ) = align_with_sequence(
        chain = chain,
        mmt = mmt,
        target = sequence_for[ chain.id ],
        logger = logger,
        )
      strial = AlignmentSequence(
        name = "target alignment",
        alignment = alignment,
        index = index,
        )
      info.task_begin( text = "Aligning with chain" )

      aligner = chainalign.ChainAlignmentAlgorithm.from_params(
        rgs = chain.residue_groups(),
        mmt = mmt,
        annotation = "chain_%s" % chain.id,
        params = chainalign_params,
        )

      result = aligner(
        trial = chainalign.Trial(
          gapped = strial.sequence(),
          gap = strial.gap(),
          annotation = strial.name,
          )
        )
      info.task_end()
      residues = result.residue_alignment()
      rescodes = result.rescode_alignment()

    else:
      try:
        ( strial, residues, rescodes ) = chain_alignment(
          chain = chain,
          mmt = mmt,
          trials = alignments,
          params = chainalign_params,
          logger = logger,
          )

      except SculptorError as e:
        info.unformatted_text( text = "Aligner: %s" % e )
        info.unformatted_text(
          text = "Associating with pseudo-alignment created from chain sequence",
          )
        from iotbx import bioinformatics
        residues = chain.residue_groups()
        rescodes = mmt.one_letter_sequence_for_rgs( rgs = residues )
        alignment = bioinformatics.alignment(
          names = [ "chain_sequence" ],
          alignments = [ rescodes ],
          )
        strial = AlignmentSequence(
          name = "Pseudo alignment",
          alignment = alignment,
          index = 0,
          )

    info.unformatted_text( text = "'%s' matched with alignment" % name )
    info.alignment( alignment = strial.alignment() )

    chainalignment = ChainAlignment(
      target = reformat_alignment_sequence( sequence = strial.target(), gap = strial.gap() ),
      model = reformat_alignment_sequence( sequence = strial.sequence(), gap = strial.gap() ),
      residues = residues,
      rescodes = rescodes,
      )
    info.field_percentage(
      name = "Sequence identity",
      value = 100.0 * chainalignment.cumulative_identity_fraction(),
      digits = 2,
      )
    errors = find_errors_for

  return ( chainalignment, errors )


def create_chain_sample(
  name,
  chain,
  alignments,
  sequence_for,
  errors_for,
  superposition_errors_for,
  chainalign_params,
  error_params,
  logger,
  ):

  info = facade.Package( channel = logger.info )

  mmt = mmtype.determine( chain = chain, mmtypes = mmtype.KNOWN_MOLECULE_TYPES )
  info.field( name = "Type", value = mmt.name )

  info.heading( text = "Alignment search", level = 3 )
  ( chainalignment, error_search_for ) = create_chain_alignment(
    name = name,
    chain = chain,
    mmt = mmt,
    sequence_for = sequence_for,
    chainalign_params = chainalign_params,
    alignments = alignments,
    logger = logger,
    )

  errors = error_search_for(
    chain = chain,
    errors_for = errors_for,
    min_identity = error_params.min_sequence_identity,
    min_fraction = error_params.min_sequence_overlap,
    stage = "rms",
    logger = logger,
    )

  if errors is None and error_params.calculate_if_not_provided:
    info.unformatted_text( text = "Requested error estimation when no error file provided" )
    info.paragraph_begin( indent = 2 )
    hmparams = error_params.homology_modelling

    try:
      with logger.child( shift = 0 ) as sublogger:
        errors = estimate_error_from_structure(
          chain = chain,
          chainalignment = chainalignment,
          mmt = mmt,
          min_sequence_coverage = error_params.min_sequence_overlap,
          homology_modelling_params = hmparams,
          output_file_prefix = error_params.output_prefix,
          logger = sublogger,
          )

    except ErrorEstimationFailure as e:
      info.highlight( text = str( e ) )
      info.unformatted_text( text = "Using flat errors" )

    info.paragraph_end()

  superposition_errors = error_search_for(
    chain = chain,
    errors_for = superposition_errors_for,
    min_identity = error_params.min_sequence_identity,
    min_fraction = error_params.min_sequence_overlap,
    stage = "superposition",
    logger = logger,
    )

  from phaser.chisellib import Sample

  return Sample(
    name = name,
    chain = chain,
    mmtype = mmt,
    chainalignment = chainalignment,
    errors = errors,
    superposition_errors = superposition_errors,
    )


def read_in_alignment_files(
  params,
  logger,
  min_hss_length = 5,
  min_sequence_identity = 0.4,
  ):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  alignments = []

  if params:
    info.heading( text = "Alignment files", level = 2 )
    info.ordered_list_begin()

    for inp in params:
      info.list_item_begin( text = inp.file_name )
      p = tbx_utils.AlignmentObject.from_file( file_name = inp.file_name )

      if not p.object.alignments:
        raise AlignmentReadError("%s: input alignment is empty" % p.name)

      if inp.target_sequence is None:
        info.unformatted_text( text = "No sequence file provided" )
        info.unformatted_text( text = "First alignment sequence will be used for target" )

      else:
        info.field( name = "Target sequence file", value = inp.target_sequence )
        seqobj = tbx_utils.SequenceObject.from_file( inp.target_sequence )

        if not seqobj.object:
          info.highlight( text = "No sequences read from file" )
          info.unformatted_text( text = "First alignment sequence will be used for target" )

        else:
          if 1 < len( seqobj.object ):
            info.highlight( text = "Multiple sequences found, using first" )

          try:
            best = chainalign.best_matching_alignment_sequence(
              master = seqobj.object[0],
              aliseqs = p.object.alignments,
              gap = p.object.gap,
              min_hss_length = min_hss_length,
              min_identity_fraction = min_sequence_identity,
              )

          except chainalign.SequenceAlignFailure:
            raise AlignmentReadError("%s: no alignment sequences match (%s)" % (
              p.name,
              "min_hss_length = %s, min_sequence_identity = %.2f" % (
                min_hss_length,
                min_sequence_identity,
              ),
            ))

          info.field( name = "Best match", value = p.object.names[ best[0] ] )
          info.field( name = "Best match identities", value = best[1][1] )

          info.task_begin( text = "Assigning sequence #%d as target" % ( best[0] + 1 ) )
          p.object.assign_as_target( index = best[0] )
          info.task_end()

      verbose.heading( text = "Alignment:", level = 6 )
      verbose.alignment( alignment = p.object )

      # Convert to AlignmentSequence format
      alignments.extend(
        [
          AlignmentSequence( name = p.name, alignment = p.object, index = i )
          for i in range( p.object.multiplicity() )
          ]
        )
      info.list_item_end()

    info.list_end()

  return alignments


def read_in_homology_search_files(params, logger):

  from libtbx.utils import Sorry

  info = facade.Package( channel = logger.info )
  alignments = []

  if params:
    info.heading( text = "Homology search files", level = 2 )
    info.ordered_list_begin()

    for inp in params:
      info.list_item_begin( text = inp.file_name )
      p = tbx_utils.HomologySearchObject.from_file( file_name = inp.file_name )
      info.field( name = "Number of hits", value = len( p.object ) )

      if inp.use is not None:
        info.field_sequence( name = "Hits to use", value = inp.use )
        indices = set( inp.use )

        for i in inp.use:
          if len( p.object ) <= i:
            raise Sorry("No hit with index %s in %s" % (i, inp.file_name))

      else:
        info.field( name = "Hits to use", value = "all" )
        indices = set( range( 1, len( p.object ) + 1 ) )

      for ( index, hit ) in enumerate( p.object.hits(), start = 1 ):
        if index not in indices:
          continue

        htrial = HomologySearchHit( name = inp.file_name, index = index, hit = hit )
        alignments.append( htrial )

      info.list_item_end()

    info.list_end()

  return alignments


def read_in_error_files(error_scopes, caption, logger):

  info = facade.Package( channel = logger.info )
  errors_for = {}

  if error_scopes:
    info.heading( text = caption, level = 2 )
    info.ordered_list_begin()

    for inp in error_scopes:
      info.list_item_begin( text = inp.file_name )

      try:
        errors = preprocess_error_file( filename = inp.file_name, logger = logger )

      except IOError as e:
        raise ErrorFileReadFailure(e)

      assert inp.chain_ids is not None # checked in validate_params
      info.field_sequence( name = "Associated chains", value = inp.chain_ids )
      errors_for.update( dict( ( k, errors ) for k in inp.chain_ids ) )
      info.list_item_end()

    info.list_end()

  return errors_for


def calculate_cumulative_identity(chain_data):

  from phaser import chisellib
  idcount = 0
  rescount = 0

  for ( csample, cdiff ) in chain_data:
    try:
      alignment = csample.chainalignment.alignment()

    except chisellib.AlignmentError:
      if not cdiff.is_marked_for_removal():
        rescount += sum( 1 for rg in cdiff.existing_residue_groups_remaining() )

    else:
      rcount = len( csample.aligned_residues() )
      rescount += rcount
      idcount += rcount * alignment.identity_fraction()

  if 0 < rescount:
    return 100.0 * idcount / rescount

  else:
    return 100.0


# Main function
def run(phil, logger):

  info = facade.Package( channel = logger.info )
  verbose = facade.Package( channel = logger.verbose )
  info.title( text = "%s version %s" % ( PROGRAM, VERSION ) )
  info.heading( text = "Configuration options", level = 2 )
  info.preformatted_text( text = phil.as_str() )
  info.separator()

  params = phil.extract()

  assert params.input.model.file_name # checked in validate_params

  # Read input
  from libtbx.utils import Sorry

  info.heading( text = "Input files", level = 1 )
  info.heading( text = "Structure file", level = 2 )
  info.field( name = "File name", value = params.input.model.file_name )

  pdb = tbx_utils.PDBObject.from_file( file_name = params.input.model.file_name )

  if params.input.model.selection != "all":
    info.field( name = "Atom selection", value = params.input.model.selection )
    info.task_begin( text = "Selecting atoms" )
    asc = pdb.object.atom_selection_cache().selection( params.input.model.selection )
    root = pdb.object.select( asc, True )

    if not root.atoms():
      info.task_failure( text = "No atoms left atom atom selection" )
      raise Sorry("No atoms left after atom selection")

    pdb = tbx_utils.PDBObject( root = root, name = pdb.name )
    info.task_end()

  if params.input.model.remove_alternate_conformations:
    info.task_begin( text = "Removing alternate conformations" )
    remove_alternate_conformations( root = pdb.object )
    info.task_end()

  if params.input.model.sanitize_occupancies:
    info.task_begin( text = "Sanitizing occupancies < 0.0 or > 1.0" )
    sanitize_occupancies( root = pdb.object )
    info.task_end()

  info.task_begin( text = "Splitting multitype chains based on residue type" )
  count = tbx_utils.split_all_chains_in( root = pdb.object )
  info.task_end()
  info.field( name = "New chains introduced", value = count )

  info.task_begin( text = "Removing hydrogens" )
  pdb.object.remove_hd()
  info.task_end()

  # Read in alignments
  try:
    alignments = read_in_alignment_files(
      params = params.input.alignment,
      logger = logger,
      )

  except AlignmentReadError as e:
    raise Sorry("Error while reading alignment: %s" % e)

  # Read in homology_search files
  alignments.extend(
    read_in_homology_search_files(
      params = params.input.homology_search,
      logger = logger,
      )
    )

  # Read in sequence files
  sequence_for = {}

  if params.input.sequence:
    info.heading( text = "Sequence files", level = 2 )
    info.ordered_list_begin()

    for inp in params.input.sequence:
      info.list_item_begin( text = inp.file_name )
      assert inp.chain_ids is not None # checked in validate_params
      info.field_sequence( name = "Associated chains", value = inp.chain_ids )

      p = tbx_utils.SequenceObject.from_file( file_name = inp.file_name )
      verbose.text( text = "Sequences read:" )

      for o in p.object:
        verbose.sequence( sequence = o )

      if 1 < len( p.object ):
        info.highlight(
          text = "File contains multiple sequences, only the first will be used"
          )

      sequence_for.update( dict( [ ( k, p.object[0] ) for k in inp.chain_ids ] ) )
      info.list_item_end()

    info.list_end()

  # Read in errors
  try:
    errors_for = read_in_error_files(
      error_scopes = params.input.errors,
      caption = "Estimated error files",
      logger = logger,
      )

    superposition_errors_for = read_in_error_files(
      error_scopes = params.input.superposition_errors,
      caption = "Superposition error files",
      logger = logger,
      )

  except ErrorFileReadFailure as e:
    raise Sorry(e)

  # Create processor objects
  processor_for = get_processor_for( params = params.sculpt )

  from phaser import chisellib
  import os.path
  import copy

  # Do processing
  info.heading( text = "Chain processing", level = 1 )
  info.ordered_list_begin()
  chaindiffs = []
  annotations = []
  identities = []
  prefix = os.path.join(
    params.output.folder,
    "%s_%s" % (
      os.path.basename( params.output.root ),
      os.path.splitext( os.path.basename( pdb.name ) )[0],
      ),
    )

  for model in pdb.object.models():
    modelann = tbx_utils.ModelAnnotation.from_model( model = model )
    modelinfo = chisellib.ModelInfo( chains = model.chains() )

    if not model.id:
      mid = ""

    else:
      mid = "_%s" % model.id.strip()

    chain_data = []

    for ( index, chain ) in enumerate( model.chains(), start = 1 ):
      chainann = tbx_utils.ChainAnnotation.from_chain( chain = chain )
      annotation = "%s, %s" % ( modelann, chainann )
      info.list_item_begin( text = annotation )
      annotations.append( annotation )
      error_params = copy.deepcopy( params.error_search )
      suffix = "%s_%s_%s" % ( mid, chain.id, index )

      if error_params.output_prefix:
        error_params.output_prefix += suffix

      else:
        error_params.output_prefix = prefix + suffix

      sample = create_chain_sample(
        name = annotation,
        chain = chain,
        alignments = alignments,
        sequence_for = sequence_for,
        errors_for = errors_for,
        superposition_errors_for = superposition_errors_for,
        chainalign_params = params.chain_to_alignment_matching,
        error_params = error_params,
        logger = logger,
        )

      info.heading( text = "Chain processing", level = 3 )
      processor = processor_for.get( sample.mmtype, chisellib.Discarder )
      info.task_begin( text = "Running processor for '%s'" % sample.mmtype.name )
      chaindiff = processor( sample = sample, model_info = modelinfo )
      info.task_end()

      count = ChaindiffProcessor.log(
        chaindiff = chaindiff,
        channel = verbose,
        simplify = False
        )
      info.field( name = "Number of modifications", value = count )
      modelinfo.register_change( chaindiff = chaindiff )
      chain_data.append( ( sample, chaindiff ) )

      info.list_item_end()

    identities.append( calculate_cumulative_identity( chain_data = chain_data ) )
    chaindiffs.extend( p[1] for p in chain_data )

  info.list_end()

  # Do processing
  info.heading( text = "Output", level = 1 )

  outfile = OUTPUT_METHOD_FOR[ params.output.format ](
    prefix = prefix,
    root = pdb.object,
    crystal = pdb.crystal if params.input.model.keep_crystal_symmetry else None,
    annotations = annotations,
    identities = identities,
    chaindiffs = chaindiffs,
    logger = logger,
    )

  info.separator()
  info.unformatted_text( text = "End of %s" % PROGRAM, alignment = "center" )
  info.separator()

  return outfile


def validate_params(params):

  from libtbx.utils import Sorry

  if params.input.model.file_name is None:
    raise Sorry("Missing PDB file")

  for inp in params.input.sequence:
    if inp.chain_ids is None:
      raise Sorry("Applicable chain_ids not specified for %s" % inp.file_name)

  for inp in params.input.errors:
    if inp.chain_ids is None:
      raise Sorry("Applicable chain_ids not specified for %s" % inp.file_name)

  for inp in params.input.superposition_errors:
    if inp.chain_ids is None:
      raise Sorry("Applicable chain_ids not specified for %s" % inp.file_name)

  return True
