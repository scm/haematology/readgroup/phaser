from __future__ import print_function

import os, time, posixpath
import threading
from threading import Thread
import logging
import traceback
import subprocess
import scitbx
import re, glob
import BLASTMRutils
from phaser import CalcCCFromMRsolutions
#from phaser import AlterOriginSymmate
from phaser import FamosCluster
from phaser import SimplePhaserJob
from phaser.utils2 import *
import phaser



"""
class ThreadLogger(logging.Logger):
    def __init__(self, logger):
        self.logger = logger

    def log(self,loglevel, mstr):
 # protect stdout from multiple output if we're multiprocessing
        for h in self.logger.handlers:
            h.acquire()
        self.logger.log(loglevel, mstr)
        for h in self.logger.handlers:
            h.flush()
            h.release()
        self.logger.propagate = False # don't duplicate output to any parent logger
"""

class ThreadLogger(logging.Logger):
    def __init__(self, logger):
        super(ThreadLogger, self).__init__( logger.name, logger.level)
        #self = logger
        self.parent = logger.parent
        self.propagate = logger.propagate
        self.handlers = logger.handlers
        self.disabled = logger.disabled
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )

    def log(self,loglevel, mstr):
 # protect stdout from multiple output if we're multiprocessing
        for h in self.handlers:
            h.acquire()
        logging.Logger.log(self, loglevel, mstr)
        for h in self.handlers:
            h.flush()
            h.release()
        self.propagate = False # don't duplicate output to any parent logger




class PhaserRunner(Thread):
    def __init__ (self, baseinputfname, modelinputfnameprefix,
      rootstr, parent, searches=None,
      mylock = None, threadnumber = 0, maxthreads = 1,
      minmodelorder = 0):
        Thread.__init__(self)
        self.parent = parent
        self.searches = searches
        self.baseinputfname = baseinputfname
        self.calclabel = self.parent.calclabel
        self.mylock = mylock
        self.tlogger = ThreadLogger(parent.logger)
        self.modelinputfnameprefix = modelinputfnameprefix
        self.modelinputfname = self.modelinputfnameprefix + ".txt"
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        self.brunphaser = self.parent.brunphaser
        self.osdepbool = False
        self.rootstr = rootstr
        self.nmaxsols = self.parent.nmaxsols
        self.fmaxhours = self.parent.fmaxhours
        self.nmaxminutes = int(self.fmaxhours * 60)
        self.errorcodesum = 0
        self.errdict = {}
        self.errdict[self.parent.targetpdbid] = "NULL"
        self.minmodelorder = minmodelorder
        self.prefix = "The "
        self.maxthreads = maxthreads
        self.retval = 0
        self.xtriagecache = self.parent.GetXtriageCachedResults()
# label thread with our tag so we can identify it as ours in GetActivethreads()
        self.name = self.prefix + str(threadnumber) + ". thread"
        if os.sys.platform == "win32":
            self.osdepbool = True



    def PhaserRun(self,path):
# concatenate the two or three input scripts into MRinput.txt that is used by phaser
        baseinputfile = open(self.baseinputfname,"r")
        baseinput = baseinputfile.read()
        baseinputfile.close()

        modelinputfile = open(os.path.join(path, self.modelinputfname),"r")
        mstr = open(os.path.join(path, self.modelinputfname),"r").read()
        modelinputfile.close()
# ignore VRMS in solution for first fixed component and template solutions so get rid of any SOLU ENSE lines
        modelinput_no_vrms = re.sub("SOLU ENSE.*\n", "", mstr)
        self.tlogger.log(logging.DEBUG, "Concatenating: " + self.calclabel + "MRinput.txt = " \
         + self.baseinputfname + " + rootstr + " + self.modelinputfname + "\n")
# Avoid race condition so not just the very first thread continues with self.name being assigned
# but waits a little until all other threads have also had their self.name assigned
        time.sleep(1)
# crank up the throttle by using more CPUs if they are idle
        jobstr = "JOBS %d\n" %self.GetBestOmpNThreads()

        myinput = baseinput + self.rootstr + jobstr + modelinput_no_vrms + "\nEND\n"
        inputfilename = posixpath.join(path, self.calclabel + "MRinput.txt")
        inputfile = open(inputfilename, "w")
        inputfile.write(myinput)
        inputfile.close()

        command = self.parent.phaserexe
        output = subprocess
        self.retval = 0
        try:
            self.tlogger.log(logging.INFO, self.name + " is executing Phaser in folder, "\
             + path + "\n\n" + myinput)

            myproc = subprocess.Popen(command, shell=self.osdepbool,
              stdin = subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = myproc.communicate( input = myinput )
            self.retval = myproc.returncode
        except Exception as m:
            self.failure = True
            self.tlogger.log(logging.ERROR, m)

        finally:
            if (self.retval != 0):
                self.tlogger.log(logging.ERROR, "Something went wrong with the "\
                   "phaser calculation using input file:\n"
                   + os.path.join(self.parent.topfolder, inputfilename) + \
                   "\nBelow is tail end of log file:\n\n" + stdoutdata[-1000:] + "\n" + stderrdata)

                self.errorcodesum += self.retval

            return stdoutdata



    def DoRNP(self, templfolder, rnpinputs, rnpcalclabel, templworkdir):
        mstr = open(rnpinputs[0], "r").read()
        mstr = re.sub("PACK .*\n", "\n", mstr)
        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
        newroot = os.path.join(templfolder, rnpcalclabel + "{" + templworkdir + "}.superposed\n")
        mstr = re.sub("ROOT .*\n", "ROOT " + newroot.replace('\\', '/') , mstr)
# crank up the throttle by using more CPUs if they are idle
        jobstr = "JOBS %d\n" %self.GetBestOmpNThreads()
        RNPinput = re.sub("JOBS .*\n", jobstr, mstr)
        mtzfname = re.findall(r"^HKLIN\s+(\S+)", RNPinput, re.MULTILINE)[0] # to get rid of any \r character
        (Fcol, Sigcol, Icol, Rfreecol, avgF, avgSig, avgI, exceptmsg) = SimpleFileProperties.GetMtzColumns(mtzfname)
        if self.parent.buseIntensities:
            labinstr = "LABIN I = " + Icol + "  SIGI = " + Sigcol + "\n"
        else:
            labinstr = "LABIN F = " + Fcol + "  SIGF = " + Sigcol + "\n"
        RNPinput = re.sub("LABIN .*\n", labinstr, RNPinput)
        mfile = open(os.path.join(templfolder, rnpcalclabel + "RNPinput.txt"), "w")
        mfile.write(RNPinput)
        mfile.close()

        command = self.parent.phaserexe
        output = subprocess
        self.retval = 0
        (stdoutdata, stderrdata) = ("", "")
        try:
            self.tlogger.log(logging.INFO, self.name + " is executing Phaser RNP in folder, "\
             + templfolder + "\n\n" + RNPinput)
            myproc = subprocess.Popen(command, shell=self.osdepbool,
              stdin = subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (stdoutdata, stderrdata) = myproc.communicate( input = RNPinput )
            self.retval = myproc.returncode
        except Exception as m:
            self.failure = True
            self.tlogger.log(logging.ERROR, m)
        finally:
            if (self.retval != 0):
                self.tlogger.log(logging.ERROR, "Something went wrong with the "\
                   "phaser calculation using input file:\n"
                   + os.path.join(self.parent.topfolder, inputfilename) + \
                   "\nBelow is tail end of log file:\n\n" + stdoutdata[-1000:] + "\n" + stderrdata)
                self.errorcodesum += self.retval
            return stdoutdata



    def AbortRequested(self, folder):
# stop further phaser jobs if this file is present
        stop1 = os.path.exists(posixpath.join(self.parent.topfolder,"StopThreadsGracefully"))
# or present in the directory above
        stop2 = os.path.exists(posixpath.join(self.parent.topfolder,"..","StopThreadsGracefully"))
        if stop1 or stop2:
            self.tlogger.log(logging.INFO,"Aborting calculation on %s.\n" %folder)
            return True
        else:
            return False



    def GetActivethreads(self):
# list names of still active threads
        active = [t.name for t in threading.enumerate()]
        setofthreadnames = set(active)
        if self.name in setofthreadnames:
            active.remove(self.name)

        namesofproperthreads = []
# avoid listing names of spurious threads not created explicitly by us
        for t in active:
            if t.find(self.prefix) > -1:
# filter them out by if they are not tagged with our prefix
                namesofproperthreads.append(t)

        return namesofproperthreads


    def GetBestOmpNThreads(self):
  # number of our threads is the count of self.name assigned to "phaser-"
        nactivethreads = len(self.GetActivethreads())
  # crank up the throttle by using more CPUs if they are idle
        return max(1,self.maxthreads/(nactivethreads+1))


    def run(self):
        try:
            #self.xtriagecache = self.parent.GetXtriageCachedResults()
            completedrunfname = posixpath.join(self.parent.topfolder, self.calclabel
             + self.parent.targetpdbid + "_DoneMRJobs.txt")
#            testsol = BLASTMRutils.BLASTMRutils(self.parent.topfolder, self.parent,
#             completedrunfname, self.mylock)
#            testsol.OCCresidwindows = self.parent.OCCresidwindows
            for i, models in enumerate( self.searches ):
# create modelsdir as concatenated PDB codes separated by commas
                modelsdir = [ [e[0] for e in  e1]  for e1 in models[:-1]][0]
                workdir = ",".join( modelsdir )

                self.tlogger.log(logging.INFO,"%d. MR calculation out of %s by %s\n"
                 %((i+1), len(self.searches), self.name))
                # crank up the throttle by using more CPUs if they are idle
                nthreads = self.GetBestOmpNThreads()
                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                if self.parent.DoTemplCalc:
                    for partmodels in models[1]:
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        """
                        self.tlogger.log(logging.INFO,"%s: Adding one more partial model"\
                         %self.name)
                        modelorder = len(partmodels.split(","))
                        if self.minmodelorder > modelorder:
                            continue
                        self.tlogger.log(logging.INFO,
                         "%s: modelorder %s" %(self.name, modelorder))
                        """
                        templfolder = posixpath.join(self.parent.topfolder, "TemplateSolutions", partmodels)
                        #templfolder = posixpath.join("TemplateSolutions", partmodels)
                        templworkdir = partmodels
                        if os.path.exists(templfolder) == False:
    # if 1QZ7\TemplateSolutions\1IAL_A0,3B8E_A0 doesn't exists try 1QZ7\TemplateSolutions\3B8E_A0,1IAL_A0
                            revmodelsdir = []
                            revmodelsdir.extend(modelsdir)  # deep copy rather than assignment
                            revmodelsdir.reverse()
                            templworkdir = ",".join( revmodelsdir )
                            templfolder = posixpath.join(self.parent.topfolder, "TemplateSolutions", templworkdir)
                        rnpcalclabel = self.calclabel + "RNP"
                        if not glob.glob(posixpath.join(templfolder,"*superposed.sol")):
                            RNPinputs = glob.glob(posixpath.join(templfolder,"RNP*MRinput.txt"))
                            if len(RNPinputs):
                                self.tlogger.log(logging.INFO,"Doing RNP to generate missing template solution file\n")
                                self.DoRNP(templfolder, RNPinputs, rnpcalclabel, templworkdir)
                                if self.retval != 0:
                                    self.tlogger.log(logging.INFO,"RNP failed.")
                                    continue
                        logfname = posixpath.join(templfolder, self.calclabel
                         + "RNPCalcCC_" + templworkdir + ".superposed.log")
                        CCfname =  posixpath.join(templfolder, self.calclabel
                         + "RNPCalcCC_{" + templworkdir + "}.superposed_dict.txt")
                        if os.path.exists(CCfname) == False:
                            CalcCCFromMRsolutions.CalcCCFromMRsolution(self.parent.targetpdbid,
                              templfolder, rnpcalclabel, templworkdir, ".superposed",
                              self.parent.MRDict, mylock= self.mylock, nthreads = nthreads,
                              bOccRefine = self.parent.bOccRefine, RotTraRefine = self.parent.RotTraRefine,
                              OCCresidwindows = self.parent.OCCresidwindows,
                              bCCresolutionShells = False, bDoRNP = True,
                              debug = self.parent.bdebug, RMSDrefine = self.parent.RMSDrefine,
                              FsolBsol = self.parent.FsolBsol, nmaxsols = self.nmaxsols,
                              xtriageres = self.xtriagecache, out=open(logfname,"w"),
                              logger = self.tlogger)
                              #bOccRefine = self.parent.bOccRefine, debug = self.parent.bdebug)
                        else:
                            self.tlogger.log(logging.INFO,"%s: %s already done\n" %(self.name, CCfname))
    # also calculate CCs for for the template solutions for the 1st component
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        if modelsdir[0] != partmodels:
                            templfolder = posixpath.join(self.parent.topfolder, "TemplateSolutions", modelsdir[0])
                            templworkdir = modelsdir[0]
                            logfname = posixpath.join(templfolder, self.calclabel +
                             "RNPCalcCC_" + templworkdir + ".superposed.log")
                            CCfname =  posixpath.join(templfolder, self.calclabel +
                             "RNPCalcCC_{" + templworkdir + "}.superposed_dict.txt")
                            if os.path.exists(CCfname) == False:
                                CalcCCFromMRsolutions.CalcCCFromMRsolution(self.parent.targetpdbid,
                                  templfolder, rnpcalclabel, templworkdir, ".superposed",
                                  self.parent.MRDict, mylock= self.mylock, nthreads = nthreads,
                                  bOccRefine = self.parent.bOccRefine, RotTraRefine = self.parent.RotTraRefine,
                                  OCCresidwindows = self.parent.OCCresidwindows,
                                  bCCresolutionShells = False, bDoRNP = True,
                                  debug = self.parent.bdebug, RMSDrefine = self.parent.RMSDrefine,
                                  FsolBsol = self.parent.FsolBsol, nmaxsols = self.nmaxsols,
                                  xtriageres = self.xtriagecache, out=open(logfname,"w"),
                                  logger = self.tlogger)
                                  #bOccRefine = self.parent.bOccRefine, debug = self.parent.bdebug)
                            else:
                                self.tlogger.log(logging.INFO,"%s: %s already done\n" %(self.name, CCfname))
                    #import code; code.interact(local=locals())
    # In 2-component calculations now use vrms for first component and optionally OCC refined first component

                self.parent.PrepareInputFiles(models[:-1][0])
                MRfolder = posixpath.join(self.parent.topfolder, "MR", workdir)
                MRfolder = posixpath.relpath(MRfolder, self.parent.topfolder)

                for partmodels in models[1]:
                    mrerrstr = ""
                    self.errdict[partmodels] = ""
                    try:
                        self.tlogger.log(logging.INFO,"%s: Adding one more partial model\n"\
                         %self.name)
                        modelorder = len(partmodels.split(","))
                        if self.minmodelorder > modelorder:
                            continue
                        self.tlogger.log(logging.INFO,
                         "%s: modelorder %s\n" %(self.name, modelorder))
                        if self.AbortRequested(MRfolder):
                            break
                        bsolved = [False]
                        founddir = [""]
                        solfnameprefix = self.calclabel + "{" + workdir + "}"+  partmodels
                        solfname = solfnameprefix + ".sol"
                        testsol = BLASTMRutils.BLASTMRutils(self.parent.topfolder, self.parent,
                         workdir, partmodels, solfname, completedrunfname, self.mylock)
                        testsol.OCCresidwindows = self.parent.OCCresidwindows
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        poorTemplateSkipMR = False
                        if modelorder > 1:
                            tmplseedsol = BLASTMRutils.BLASTMRutils(self.parent.topfolder,
                             self.parent, modelsdir[0], modelsdir[0])
                            tmplseedsol.IsthereaPartialSolution()
                            try:
                                if tmplseedsol.mrsol[3][6]['CCwt_globalSol0'] < 0.18:
                            # template solution of first component was crap, so bail out
                                    self.tlogger.log(logging.INFO,
                                     "Template solution %s is poor. Skip trying to place additional components.\n" %modelsdir[0])
                                    poorTemplateSkipMR = True
                            except Exception:
                                self.tlogger.log(logging.INFO,
                                 "Template solution %s doesn't exist. Skip trying to place additional components.\n" %modelsdir[0])
                                poorTemplateSkipMR = True
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        notrunbefore = True
                        if testsol.PhaserRanPartModelsBefore(bsolved) == True:
                            if bsolved[0] == True: # and solved it
                                self.tlogger.log(logging.INFO,
                                 "%s: A previous job found the partial structure %s in %s\n"
                                 %(self.name,partmodels, founddir[0]))
                            else:
                                self.tlogger.log(logging.INFO,
                                 "%s: A previous job didn't find the partial structure %s in %s\n"
                                 %(self.name,partmodels, workdir))
                            notrunbefore = False
    # Run phaser with the model created by PrepareInputfiles
                        rootname = posixpath.relpath(MRfolder) + posixpath.sep \
                         + self.calclabel + "{" + workdir + "}" + partmodels
    # stop phaser after some time, limit number of solutions from an MR run, refine occupancies
                        self.rootstr = "ROOT " + rootname + \
    """
    KILL TIME %d
    PURGE RNP NUMB %d
    """ \
                         %(self.nmaxminutes, self.nmaxsols)
                        if self.parent.bOccRefine:
                            self.rootstr = self.rootstr + \
    """
    SEARCH PRUNE ON
    """
                        else:
                            self.rootstr = self.rootstr + \
    """
    SEARCH PRUNE OFF
    """
                        if self.parent.FsolBsol:
                            self.rootstr = self.rootstr + \
    """
    SOLPARAMETERS BULK USE ON
    SOLPARAMETERS BULK FSOL %2.2f BSOL %3.1f
    """                     %self.parent.FsolBsol
                        self.modelinputfname = self.modelinputfnameprefix + "{" + partmodels + "}.txt"
                        #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                        if self.brunphaser and notrunbefore and (not poorTemplateSkipMR or not self.parent.DoTemplCalc):
                            #import code; code.interact(local=locals())
                            mrinput = ""
                            with open(self.baseinputfname,"r") as bf:
                                mrinput = bf.read()
                            with open(os.path.join(MRfolder, self.modelinputfname),"r") as f:
                                mrinput += f.read()

                        # get ensemble name and sequence file name from some phaser input file
                            MRpdbensdict = {}
                            matches = re.findall(r"^ENSEMBLE\s+(\S+)\s+PDB\s+(\S+)\s+IDENTITY\s+(\S+)\s+",
                              mrinput, re.MULTILINE)
                            for ens in matches:
                                MRpdbensdict[ens[0]] = (ens[1], float(ens[2]), "isSeqID")
                            matches = re.findall(r"^ENSEMBLE\s+(\S+)\s+PDB\s+(\S+)\s+RMS\s+(\S+)\s+",
                              mrinput, re.MULTILINE)
                            for ens in matches:
                                MRpdbensdict[ens[0]] = (ens[1], float(ens[2]), "isRMS")

                            seqfnames = re.findall(r"^COMPOSITION PROTEIN SEQUENCE\s+(\S+)\s+NUM.+",
                              mrinput, re.MULTILINE)
                        # because strings are immutable we iterate with int index
                        # rather than list elements in order to change the strings
                            for i in range(len(seqfnames)):
                                seqfnames[i] = PosixSlashFilePath(seqfnames[i])
                                seqfnames[i] = RecaseFilename2(seqfnames[i])

                            #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                            mtzfname = re.findall(r"^HKLIN\s+(\S+)", mrinput, re.MULTILINE)[0] # to get rid of any \r character
                            #import code; code.interact(local=locals())
                            mrinput = phaser.InputMR_AUTO()
                            mrinput.setSEAR_PRUN(self.parent.bOccRefine)
                            mrinput.setJOBS( self.GetBestOmpNThreads() )
                            mrinput.setKILL_TIME( self.nmaxminutes )
                            if self.parent.neweLLGtarget:
                                mrinput.setELLG_TARG( self.parent.neweLLGtarget )
                            mrsols, mrerrstr = SimplePhaserJob.RunMR_Auto( mtzfname,
                                                        MRpdbensdict,
                                                        seqfnames,
                                                        rootname,
                                                        True,
                                                        mrinput
                                                        )

                            #stdoutput = self.PhaserRun(MRfolder)
                            #with open(rootname + ".log","w") as phaserlogfile:
                            #  phaserlogfile.write(stdoutput)

                            if self.retval != 0:
                                self.tlogger.log(logging.INFO,"MR on %s failed." %partmodels)
                                continue
    # see if phaser found a solution for these models
    # "models" is the search order for this MR calculation: pdb_a, pdb_b, pdb_c,...pdb_m, pdb_n
    #                    nPartSolWithThisStrategy = testsol.IsthereaPartialSolution(
    #                      workdir, partmodels, solfname)
                        nPartSolWithThisStrategy = testsol.IsthereaPartialSolution()
                        testsol.NoticeCompletedPhaserRun(workdir, partmodels,
                         nPartSolWithThisStrategy)
                        if nPartSolWithThisStrategy < 0:
                            self.tlogger.log(logging.INFO,
                             "%s: Phaser didn't even write a solution file, %s, for the %s job\n"\
                             %(self.name, partmodels, modelsdir))
                        self.tlogger.log(logging.INFO,
                         "%s: Done placing partial model, %s, for the %s job." \
                         "\nNow calculating correlation coefficients\n" \
                         %(self.name, partmodels, modelsdir))

                        def ShakeRMSDRescoreSolutions(MRfolder, workdir, partmodels, RotTraRefine, RMSDrefine):
                            #bperturbbulksol = True
                            perturbations=[]
                            if self.parent.perturbtype == "PerturbFsol":
                                perturbations = [0.05, 0.2, 0.35] # for varying Fsol parameter
                                self.tlogger.log(logging.INFO,"Perturbing with Fsol values: %s\n" %str(perturbations))
                            if self.parent.perturbtype == "PerturbCelldim":
                                #perturbations = [0.005, 0.01, 0.015]
                                #perturbations = [-0.0005, 0.0005]
                                perturbations = [-0.0005, -0.00025, 0.00025, 0.0005]
                                self.tlogger.log(logging.INFO,"Perturbing cell dimensions %s\n" %str(perturbations))
                            if self.parent.perturbtype == "PerturbRMS":
                                #perturbations = [-0.15, -0.1, -0.05, 0.05, 0.1, 0.15]
                                #perturbations = [-0.1, -0.05, 0.05, 0.1]
                                #perturbations = [-0.2, -0.1, 0.1, 0.2]
                                perturbations= ["OEFFNERHI", "OEFFNERLO"]
                                self.tlogger.log(logging.INFO,"Perturbing RMS with: %s\n" %str(perturbations))

                            for i, pert in enumerate(perturbations):
                                nthreads = self.GetBestOmpNThreads()
                                #import code, traceback; code.interact(local=locals(), banner="".join( traceback.format_stack(limit=10) ) )
                                pertstr = str(pert)
                                logfname = posixpath.join(MRfolder, self.calclabel +
                                  "Pert_" + pertstr + "CalcCC_" + workdir + partmodels + ".log")
                                logfile = open(logfname,"w")
                                MRens = CalcCCFromMRsolutions.CalcCCFromMRsolution(
                                  targetpdbid=self.parent.targetpdbid,
                                  MRfolder=MRfolder,
                                  calclabel=self.calclabel,
                                  workdir=workdir,
                                  partmodels=partmodels,
                                  MRDict=self.parent.MRDict,
                                  mylock= self.mylock,
                                  nthreads = nthreads,
                                  bOccRefine = False,
                                  debug = self.parent.bdebug,
                                  FsolBsol = self.parent.FsolBsol,
                                  nmaxsols = self.nmaxsols,
                                  perturbstr = pertstr,
                                  bDoPertMR = True,
                                  bDoRNP = False,
                                  bCCresolutionShells = False,
                                  perturbation = pert,
                                  perturbtype = self.parent.perturbtype,
                                  RotTraRefine = RotTraRefine,
                                  RMSDrefine = RMSDrefine,
                                  xtriageres = self.xtriagecache,
                                  killtime = self.nmaxminutes,
                                  out=logfile,
                                  logger = self.tlogger
                                  )
                                logfile.close()

                            movsolfnames = [os.path.join(MRfolder, self.calclabel + "CalcCC.sol")]
                            for i, pert in enumerate(perturbations):
                                #if RotTraRefine:
                                movsolfnames.append(os.path.join(MRfolder, self.calclabel + str(pert) + "CalcMR.sol"))
                            targetpdb = os.path.join(self.parent.topfolder, self.parent.targetpdbid + ".pdb")
                            #FamosCluster.MakeFamosMatrix(self.calclabel, MRfolder,
                            #  MRens, movsolfnames, targetpdb, targetpdb, nthreads, self.tlogger)

                        if self.brunphaser and notrunbefore and not poorTemplateSkipMR:
                            MRfolder = posixpath.join(self.parent.topfolder, "MR", workdir)
                            logfname = posixpath.join(MRfolder, self.calclabel + "CalcCC_"
                             + workdir + partmodels + ".log")
                            CCfname =  posixpath.join(MRfolder, self.calclabel + "CalcCC_{"
                             + workdir + "}" + partmodels + "_dict.txt")
                            if self.parent.perturbtype:
                                ShakeRMSDRescoreSolutions(MRfolder, workdir, partmodels, True, False)
                            else:
                                if os.path.exists(CCfname) == False:
                                    CalcCCFromMRsolutions.CalcCCFromMRsolution(self.parent.targetpdbid,
                                      MRfolder, self.calclabel, workdir, partmodels,
                                      self.parent.MRDict, mylock= self.mylock, nthreads = nthreads,
                                      bOccRefine = self.parent.bOccRefine,
                                      OCCresidwindows = self.parent.OCCresidwindows,
                                      bCCresolutionShells = False, bDoRNP = False,
                                      debug = self.parent.bdebug, RotTraRefine = self.parent.RotTraRefine,
                                      FsolBsol = self.parent.FsolBsol, nmaxsols = self.nmaxsols,
                                      xtriageres = self.xtriagecache, out=open(logfname,"w"),
                                      logger = self.tlogger)
                                else:
                                    self.tlogger.log(logging.INFO,"%s: %s already done\n" %(self.name, CCfname))
                    except Exception as m:
                        errstr =  self.name + ": " + str(m) + "\n" + traceback.format_exc()
                        self.errdict[partmodels] = errstr
                        self.tlogger.log(logging.ERROR, "\n" + errstr)
                    finally:
                        self.errdict[partmodels] += mrerrstr
# list names of still active threads
            namesofproperthread = self.GetActivethreads()
            threadnames = ", ".join(namesofproperthread)
            if threadnames == "":
                threadnames = "no others"
            self.tlogger.log(logging.INFO,"%s is done and waits for %s to finish.\n"
             %(self.name,threadnames))

        except Exception as m:
            errstr = self.name + ": " + str(m) + "\n" + traceback.format_exc()
            self.errdict[self.parent.targetpdbid] = errstr
            self.tlogger.log(logging.ERROR, "\n" + errstr)
