from __future__ import print_function

from __future__ import division
from libtbx import easy_run

def exercise () :
  pdb_fixed = """\
CRYST1   25.000   10.000   25.477  90.00 107.08  90.00 P 1 21 1
SCALE1      0.040101  0.000000  0.012321        0.00000
SCALE2      0.000000  0.112790  0.000000        0.00000
SCALE3      0.000000  0.000000  0.041062        0.00000
ATOM      1  N   GLY A   1       8.992   0.474  -6.096  1.00 16.23           N
ATOM      2  CA  GLY A   1       9.033   0.047  -4.707  1.00 16.20           C
ATOM      3  C   GLY A   1       7.998  -1.029  -4.448  1.00 15.91           C
ATOM      4  O   GLY A   1       7.548  -1.689  -5.385  1.00 16.11           O
ATOM      5  N   ASN A   2       7.625  -1.218  -3.185  1.00 15.02           N
ATOM      6  CA  ASN A   2       6.523  -2.113  -2.848  1.00 13.92           C
ATOM      7  C   ASN A   2       5.220  -1.618  -3.428  1.00 12.24           C
ATOM      8  O   ASN A   2       4.955  -0.418  -3.432  1.00 11.42           O
ATOM      9  CB  ASN A   2       6.376  -2.261  -1.340  1.00 14.42           C
ATOM     10  CG  ASN A   2       7.620  -2.786  -0.697  1.00 13.92           C
ATOM     11  OD1 ASN A   2       8.042  -3.915  -0.978  1.00 14.39           O
ATOM     12  ND2 ASN A   2       8.232  -1.975   0.168  1.00 12.78           N
ATOM     13  N   ASN A   3       4.406  -2.553  -3.904  1.00 12.20           N
ATOM     14  CA  ASN A   3       3.164  -2.226  -4.594  1.00 11.81           C
ATOM     15  C   ASN A   3       1.925  -2.790  -3.910  1.00 10.59           C
ATOM     16  O   ASN A   3       1.838  -3.991  -3.653  1.00 10.32           O
ATOM     17  CB  ASN A   3       3.231  -2.727  -6.046  1.00 12.51           C
ATOM     18  CG  ASN A   3       1.973  -2.405  -6.848  1.00 12.59           C
ATOM     19  OD1 ASN A   3       1.662  -1.239  -7.106  1.00 13.64           O
ATOM     20  ND2 ASN A   3       1.260  -3.443  -7.268  1.00 12.39           N
ATOM     21  N   GLN A   4       0.973  -1.913  -3.608  1.00 10.34           N
ATOM     22  CA  GLN A   4      -0.366  -2.335  -3.208  1.00 10.00           C
ATOM     23  C   GLN A   4      -1.402  -1.637  -4.085  1.00 10.21           C
ATOM     24  O   GLN A   4      -1.514  -0.414  -4.070  1.00  8.99           O
ATOM     25  CB  GLN A   4      -0.656  -2.027  -1.736  1.00 10.00           C
ATOM     26  CG  GLN A   4      -1.927  -2.705  -1.229  1.00 10.50           C
ATOM     27  CD  GLN A   4      -2.482  -2.102   0.060  1.00 11.36           C
ATOM     28  OE1 GLN A   4      -2.744  -0.900   0.151  1.00 12.29           O
ATOM     29  NE2 GLN A   4      -2.684  -2.951   1.055  1.00 10.43           N
ATOM     30  N   GLN A   5      -2.154  -2.406  -4.857  1.00 10.48           N
ATOM     31  CA  GLN A   5      -3.247  -1.829  -5.630  1.00 11.24           C
ATOM     32  C   GLN A   5      -4.591  -2.382  -5.178  1.00 11.40           C
ATOM     33  O   GLN A   5      -4.789  -3.599  -5.092  1.00 11.94           O
ATOM     34  CB  GLN A   5      -3.024  -2.023  -7.129  1.00 11.14           C
ATOM     35  CG  GLN A   5      -1.852  -1.222  -7.653  1.00 10.65           C
ATOM     36  CD  GLN A   5      -1.338  -1.748  -8.965  1.00 10.73           C
ATOM     37  OE1 GLN A   5      -0.794  -2.845  -9.028  1.00 10.14           O
ATOM     38  NE2 GLN A   5      -1.511  -0.968 -10.027  1.00 11.31           N
ATOM     39  N   ASN A   6      -5.504  -1.471  -4.872  1.00 11.56           N
ATOM     40  CA  ASN A   6      -6.809  -1.838  -4.359  1.00 12.07           C
ATOM     41  C   ASN A   6      -7.856  -1.407  -5.353  1.00 13.18           C
ATOM     42  O   ASN A   6      -8.257  -0.251  -5.362  1.00 13.64           O
ATOM     43  CB  ASN A   6      -7.053  -1.149  -3.017  1.00 12.12           C
ATOM     44  CG  ASN A   6      -5.966  -1.446  -1.998  1.00 12.31           C
ATOM     45  OD1 ASN A   6      -5.833  -2.579  -1.517  1.00 13.43           O
ATOM     46  ND2 ASN A   6      -5.198  -0.423  -1.645  1.00 11.88           N
ATOM     47  N   TYR A   7      -8.298  -2.332  -6.193  1.00 14.34           N
ATOM     48  CA  TYR A   7      -9.162  -1.980  -7.317  1.00 15.00           C
ATOM     49  C   TYR A   7     -10.603  -1.792  -6.893  1.00 15.64           C
ATOM     50  O   TYR A   7     -11.013  -2.278  -5.838  1.00 15.68           O
ATOM     51  CB  TYR A   7      -9.064  -3.041  -8.412  1.00 15.31           C
ATOM     52  CG  TYR A   7      -7.657  -3.197  -8.931  1.00 15.06           C
ATOM     53  CD1 TYR A   7      -6.785  -4.118  -8.368  1.00 15.24           C
ATOM     54  CD2 TYR A   7      -7.193  -2.400  -9.960  1.00 14.96           C
ATOM     55  CE1 TYR A   7      -5.489  -4.253  -8.830  1.00 14.94           C
ATOM     56  CE2 TYR A   7      -5.905  -2.526 -10.429  1.00 15.13           C
ATOM     57  CZ  TYR A   7      -5.055  -3.451  -9.861  1.00 14.97           C
ATOM     58  OH  TYR A   7      -3.768  -3.572 -10.335  1.00 14.93           O
ATOM     59  OXT TYR A   7     -11.378  -1.149  -7.601  1.00 15.89           O
TER
HETATM   60  C   ACT     1      -0.908  -5.503  -5.572  1.00 18.56           C
HETATM   61  O   ACT     1       0.145  -5.391  -6.241  1.00 19.26           O
HETATM   62  OXT ACT     1      -1.449  -4.510  -5.051  1.00 18.36           O
HETATM   63  CH3 ACT     1      -1.540  -6.820  -5.404  1.00 18.08           C
HETATM   64  O   HOH S   1     -10.466  -2.347  -3.168  1.00 17.57           O
HETATM   65  O   HOH S   2       6.469   1.081  -7.070  1.00 21.27           O
HETATM   66  O   HOH S   3     -11.809   0.108  -9.956  1.00 27.52           O
HETATM   67  O   HOH S   4       1.580  -3.455 -11.035  1.00 44.76           O
"""
  pdb_moving = """\
CRYST1   25.000   10.000   25.477  90.00 107.08  90.00 P 1 21 1      2
SCALE1      0.040000 -0.000000  0.012290        0.00000
SCALE2      0.000000  0.100000 -0.000000        0.00000
SCALE3      0.000000  0.000000  0.041062        0.00000
ATOM      1  N   GLY A   1     -17.791   4.778  -6.053  1.00  2.00           N
ATOM      2  CA  GLY A   1     -17.827   4.323  -7.451  1.00  2.00           C
ATOM      3  C   GLY A   1     -16.781   3.121  -7.731  1.00  2.00           C
ATOM      4  O   GLY A   1     -16.326   2.362  -6.807  1.00  2.00           O
ATOM      5  N   ASN A   2     -16.404   2.935  -8.997  1.00  2.00           N
ATOM      6  CA  ASN A   2     -15.292   1.940  -9.351  1.00  2.00           C
ATOM      7  C   ASN A   2     -13.991   2.495  -8.757  1.00  2.00           C
ATOM      8  O   ASN A   2     -13.734   3.850  -8.726  1.00  2.00           O
ATOM      9  CB  ASN A   2     -15.141   1.803 -10.861  1.00  2.00           C
ATOM     10  CG  ASN A   2     -16.384   1.215 -11.518  1.00  2.00           C
ATOM     11  OD1 ASN A   2     -16.799  -0.067 -11.263  1.00  2.00           O
ATOM     12  ND2 ASN A   2     -17.002   2.141 -12.367  1.00  2.00           N
ATOM     13  N   ASN A   3     -13.168   1.436  -8.300  1.00  2.00           N
ATOM     14  CA  ASN A   3     -11.926   1.800  -7.600  1.00  2.00           C
ATOM     15  C   ASN A   3     -10.679   1.186  -8.293  1.00  2.00           C
ATOM     16  O   ASN A   3     -10.582  -0.162  -8.576  1.00  2.00           O
ATOM     17  CB  ASN A   3     -11.992   1.207  -6.159  1.00  2.00           C
ATOM     18  CG  ASN A   3     -10.734   1.563  -5.347  1.00  2.00           C
ATOM     19  OD1 ASN A   3     -10.432   2.876  -5.063  1.00  2.00           O
ATOM     20  ND2 ASN A   3     -10.012   0.390  -4.948  1.00  2.00           N
ATOM     21  N   GLN A   4      -9.731   2.187  -8.574  1.00  2.00           N
ATOM     22  CA  GLN A   4      -8.385   1.728  -8.980  1.00  2.00           C
ATOM     23  C   GLN A   4      -7.352   2.505  -8.085  1.00  2.00           C
ATOM     24  O   GLN A   4      -7.249   3.885  -8.074  1.00  2.00           O
ATOM     25  CB  GLN A   4      -8.094   2.106 -10.444  1.00  2.00           C
ATOM     26  CG  GLN A   4      -6.814   1.359 -10.963  1.00  2.00           C
ATOM     27  CD  GLN A   4      -6.261   2.068 -12.237  1.00  2.00           C
ATOM     28  OE1 GLN A   4      -6.007   3.427 -12.302  1.00  2.00           O
ATOM     29  NE2 GLN A   4      -6.050   1.131 -13.250  1.00  2.00           N
ATOM     30  N   GLN A   5      -6.594   1.628  -7.329  1.00  2.00           N
ATOM     31  CA  GLN A   5      -5.503   2.271  -6.541  1.00  2.00           C
ATOM     32  C   GLN A   5      -4.151   1.665  -7.002  1.00  2.00           C
ATOM     33  O   GLN A   5      -3.944   0.296  -7.114  1.00  2.00           O
ATOM     34  CB  GLN A   5      -5.728   2.022  -5.047  1.00  2.00           C
ATOM     35  CG  GLN A   5      -6.909   2.908  -4.508  1.00  2.00           C
ATOM     36  CD  GLN A   5      -7.423   2.285  -3.209  1.00  2.00           C
ATOM     37  OE1 GLN A   5      -7.960   1.043  -3.171  1.00  2.00           O
ATOM     38  NE2 GLN A   5      -7.256   3.146  -2.130  1.00  2.00           N
ATOM     39  N   ASN A   6      -3.242   2.705  -7.286  1.00  2.00           N
ATOM     40  CA  ASN A   6      -1.930   2.309  -7.804  1.00  2.00           C
ATOM     41  C   ASN A   6      -0.886   2.783  -6.798  1.00  2.00           C
ATOM     42  O   ASN A   6      -0.493   4.089  -6.763  1.00  2.00           O
ATOM     43  CB  ASN A   6      -1.690   3.114  -9.130  1.00  2.00           C
ATOM     44  CG  ASN A   6      -2.775   2.791 -10.158  1.00  2.00           C
ATOM     45  OD1 ASN A   6      -2.900   1.522 -10.663  1.00  2.00           O
ATOM     46  ND2 ASN A   6      -3.553   3.947 -10.490  1.00  2.00           N
ATOM     47  N   TYR A   7      -0.437   1.727  -5.977  1.00  2.00           N
ATOM     48  CA  TYR A   7       0.425   2.108  -4.844  1.00  2.00           C
ATOM     49  C   TYR A   7       1.869   2.338  -5.260  1.00  2.00           C
ATOM     50  O   TYR A   7       2.285   1.813  -6.325  1.00  2.00           O
ATOM     51  CB  TYR A   7       0.333   0.889  -3.772  1.00  2.00           C
ATOM     52  CG  TYR A   7      -1.077   0.694  -3.260  1.00  2.00           C
ATOM     53  CD1 TYR A   7      -1.943  -0.340  -3.845  1.00  2.00           C
ATOM     54  CD2 TYR A   7      -1.549   1.570  -2.215  1.00  2.00           C
ATOM     55  CE1 TYR A   7      -3.242  -0.509  -3.389  1.00  2.00           C
ATOM     56  CE2 TYR A   7      -2.840   1.410  -1.752  1.00  2.00           C
ATOM     57  CZ  TYR A   7      -3.685   0.373  -2.342  1.00  2.00           C
ATOM     58  OH  TYR A   7      -4.975   0.218  -1.873  1.00  2.00           O
ATOM     59  OXT TYR A   7       2.640   3.054  -4.537  1.00  2.00           O
END
"""
  open("tst_famos_2_moving.pdb", "w").write(pdb_moving)
  open("tst_famos_2_fixed.pdb", "w").write(pdb_fixed)
  args = [
    "phenix.famos",
    "moving.pdb=tst_famos_2_moving.pdb",
    "fixed.pdb=tst_famos_2_fixed.pdb",
  ]
  result = easy_run.fully_buffered(" ".join(args))
  assert (result.return_code == 0), "\n".join(result.stderr_lines)
  assert (not "Error in GetAltOrigSymmates" in "\n".join(result.stdout_lines))
  # TODO check for PDB file?

if (__name__ == "__main__") :
  exercise()
