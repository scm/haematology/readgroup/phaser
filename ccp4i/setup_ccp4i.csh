#!/bin/csh -m

# Copy files from phaser/ccp4i directory to ccp4 installation,
# after backing up ccp4 installation versions
# Execute this file with "sudo ./setup_ccp4i.csh" if you don't own $CCP4.
#
# Assume here that all files to be copied start with "phaser"
# This avoids copying etc/modules.def, which should be updated manually to avoid breaking
# tasks modified in ccp4i since our version of modules.def was updated.
set files=`/bin/ls */phaser*`

foreach file ($files)
  if (-l $CCP4I_TOP/${file}) then
    /bin/rm $CCP4I_TOP/${file} # Don't back up old symbolic link
  else if (-e $CCP4I_TOP/${file}_bak) then
    /bin/rm $CCP4I_TOP/${file} # Original has already been backed up
  else if (-e $CCP4I_TOP/${file}) then
    mv $CCP4I_TOP/$file $CCP4I_TOP/${file}_bak # Still needs to be backed up
  endif
  cp $file $CCP4I_TOP/$file
end
