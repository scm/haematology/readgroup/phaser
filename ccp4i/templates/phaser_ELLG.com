1 #---PHASER COMMAND SCRIPT GENERATED BY CCP4I---
{[IfSet $TITLE]} TITLE $TITLE
1 MODE MR_ELLG
1 ROOT \\"$OUTPUT_FILE_ROOT\\"

1 #---DEFINE DATA---
1 HKLIN \\"$HKLIN_MAIN\\"
IF {$TOG_INTENSITY_OPTION}
  LABELLINE LABIN "I SIGI"
ELSE  
  LABELLINE LABIN "F SIGF"
ENDIF

$TOG_RESOLUTION RESOLUTION HIGH $RESOLUTION_DMIN_MR LOW $RESOLUTION_DMAX

1 #---DEFINE ENSEMBLES---

LOOP E 1 $N_ENSEMBLE
  1 ENSEMBLE $ENSEMBLE_ID($E) 
    IF {[StringSame $ENSEMBLE_OPTION($E) PDB]}
      LOOP PDB 1 $N_PDB($E)
      IF  {[StringSame $RMS_OPTION($E,$PDB) CARD]}
      - 1 PDB \\"$XYZIN($E,$PDB)\\" $RMS_OPTION($E,$PDB) ON
      ELSE 
      - 1 PDB \\"$XYZIN($E,$PDB)\\" $RMS_OPTION($E,$PDB) $RMS($E,$PDB)
      ENDIF
      ENDLOOP
    ELSE 
      - 1 HKLIN \\"$HKLIN($E)\\"
    LABELLINE & "FC PHIC" $E
      - {[IfSet $ENSEMBLE_EXT_X($E)]} EXTENT $ENSEMBLE_EXT_X($E) $ENSEMBLE_EXT_Y($E) $ENSEMBLE_EXT_Z($E)
      - {[IfSet $ENSEMBLE_RMS_MTZ($E)]} RMS $ENSEMBLE_RMS_MTZ($E)
      - {[IfSet $ENSEMBLE_CEN_X($E)]} CENTRE $ENSEMBLE_CEN_X($E) $ENSEMBLE_CEN_Y($E) $ENSEMBLE_CEN_Z($E)
      - {[IfSet $ENSEMBLE_PROTMW($E)]} PROTEIN MW $ENSEMBLE_PROTMW($E)
      - {[IfSet $ENSEMBLE_NUCLMW($E)]} NUCL MW $ENSEMBLE_NUCLMW($E)
    ENDIF  
 {$TOG_HETATM($E)} ENSEMBLE $ENSEMBLE_ID($E) HETATM ON
ENDLOOP

1 #---DEFINE COMPOSITION---

IF {[StringSame $SCATTERING COMPONENT]}
  1 COMPOSITION BY ASU
ELSE
  1 COMPOSITION BY $SCATTERING 
ENDIF
StringSame $SCATTERING SOLVENT]} COMPOSITION PERCENTAGE $SCATTERING_SOLVENT
LOOP C 1 $N_COMPONENT
  {[StringSame $SCATTERING COMPONENT] && [StringSame $COMP_OPTION($C) MW ]} COMPOSITION $PROTDNA($C) MW $MW($C) NUMBER $ASYM($C)
{[StringSame $SCATTERING COMPONENT] && [StringSame $COMP_OPTION($C) FASTA ]} COMPOSITION $PROTDNA($C) SEQ \\"$COMP_FILE($C)\\" NUMBER $ASYM($C)
  {[StringSame $SCATTERING COMPONENT] && [StringSame $COMP_OPTION($C) NRES ]} COMPOSITION $PROTDNA($C) NRES $COMP_NRES($C) NUMBER $ASYM($C)
ENDLOOP

{[IfSet $SET_SOL_FILE_NOTOB]} @ \\"$SET_SOL_FILE_NOTOB\\"

1 #---OUTPUT CONTROL---

{$TOG_VERBOSE} VERBOSE ON
{$TOG_VERBOSE_EXTRA} DEBUG ON
