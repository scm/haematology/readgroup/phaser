# =======================================================================
# phaser_ELLG.tcl --
#
# Maximum Likelihood Molecular Replacement
#
# CCP4Interface
#
# =======================================================================

#------------------------------------------------------------------------------
proc phaser_ELLG_prereq { } {
#------------------------------------------------------------------------------
  if { ![file exists [FindExecutable phaser]] } {
    return 0
  }
  return 1
}

#------------------------------------------------------------------------
  proc phaser_ELLG_setup { typedefVar arrayname } {
#------------------------------------------------------------------------
  upvar #0 $typedefVar typedef
  upvar #0 $arrayname array

  DefineMenu _composition_option \
    [ list "average solvent content for protein crystal" "solvent content of protein crystal" "components in asymmetric unit"] \
    [ list "AVERAGE" "SOLVENT" "COMPONENT"]

  DefineMenu _protnucleic \
    [list "protein" "nucleic acid" ] [list "PROTEIN" "NUCLEIC" ]

  DefineMenu _rmsid_option \
   [list "rms difference" "sequence identity" "from pdb REMARK" "high rms from id" "low rms from id"] [list "RMS" "IDENT" "CARD" "IDHI" "IDLO"]

  set typedef(_mwseqnres_option) \
    { menu  { "molecular weight"  "sequence file" "number of residues"} { "MW"  "FASTA" "NRES"} }

  DefineMenu _ensemble_option \
    [list "pdb file(s)" "map (mtz file)" ] [list "PDB"  "MAP"]

  set typedef(_ensemble_menu)  \
    { varmenu ENSEMBLE_MENU_LIST ENSEMBLE_MENU_ALIAS 15 }
  set typedef(_onoff) \
    { menu { "on" "off" } { ON OFF } }
  set typedef(_tribool) \
    { menu { "on" "off" "auto" } { ON OFF NOT_SET } }
  return 1
  }

#------------------------------------------------------------------------
proc phaser_ELLG_run { arrayname } {
#------------------------------------------------------------------------

  upvar #0 $arrayname array

# The output file array is set up in phaser_ELLG.script
  set array(OUTPUT_FILES) ""

# All output files with the user specified root will be saved to the project dir
#set array(FILENAME_ROOT) [FileJoin [GetDefaultDirPath] $array(ROOT) ]

  if { $array(HKLIN_MAIN) == "" } {
     WarningMessage "NOT SET: mtz reflection data file"
     return 0
  }

  if { ([GetValue $arrayname SCATTERING] == "COMPONENT") } {
  for { set n 1 } { $n <= $array(N_COMPONENT) } { incr n } {
    if { $array(COMP_OPTION,$n) == "sequence file" &&  $array(COMP_FILE,$n) == "" } {
       WarningMessage "NOT SET: sequence file for component # $n"
       return 0
     }
    if { $array(COMP_OPTION,$n) == "molecular weight" &&  $array(MW,$n) == "" } {
       WarningMessage "NOT SET: molecular weight for component # $n"
       return 0
     }
   } }

  if { ([GetValue $arrayname SCATTERING] == "NRES")} { 
    if { ([GetValue $arrayname COMP_NRES] == "")} { 
     WarningMessage "NOT SET: number of residues in the composition"
     return 0
    } }

  if { ([GetValue $arrayname SCATTERING] == "SOLVENT")} { 
    if { ([GetValue $arrayname SCATTERING_SOLVENT] == "")} { 
     WarningMessage "NOT SET: solvent content in the composition"
     return 0
  } }

   for { set n 1 } { $n <= $array(N_ENSEMBLE) } { incr n } {
    if { [ regexp PDB [GetValue $arrayname ENSEMBLE_OPTION,$n ] ] } {
    for { set p 1 } { $p <= $array(N_PDB,$n) } { incr p } {
      if { $array(XYZIN,[subst $n]_$p) == "" } {
         WarningMessage "NOT SET: pdb file for ensemble # $n"
         return 0
      }
      if { $array(RMS_OPTION,[subst $n]_$p) != "from pdb REMARK" } {
      if { $array(RMS,[subst $n]_$p) == "" } {
         WarningMessage "NOT SET: RMS error for PDB # $p of ensemble # $n"
         return 0
      }
      }
    }
    }
    if { [ regexp MAP [GetValue $arrayname ENSEMBLE_OPTION,$n ] ] } {
      if { $array(HKLIN,$n) == "" } {
         WarningMessage "NOT SET: mtz map file for ensemble # $n"
         return 0
      }
      if { $array(ENSEMBLE_PROTMW,$n) == "" } {
         WarningMessage "NOT SET: protein molecular weight for ensemble # $n"
         return 0
      }
      if { $array(ENSEMBLE_NUCLMW,$n) == "" } {
         WarningMessage "NOT SET: nucleic acid molecular weight for ensemble # $n"
         return 0
      }
      if { ($array(ENSEMBLE_EXT_X,$n) == "") 
        || ($array(ENSEMBLE_EXT_Y,$n) == "")
        || ($array(ENSEMBLE_EXT_Z,$n) == "") } {
         WarningMessage "NOT SET: extent of the model for ensemble # $n"
         return 0
      } 
      if { $array(ENSEMBLE_RMS_MTZ,$n) == "" } {
         WarningMessage "NOT SET: RMS error of the model for ensemble # $n"
         return 0
      } 
      if { ($array(ENSEMBLE_CEN_X,$n) == "")
        || ($array(ENSEMBLE_CEN_Y,$n) == "")
        || ($array(ENSEMBLE_CEN_Z,$n) == "") } {
         WarningMessage "NOT SET: centre of the model for ensemble # $n"
         return 0 
       }
    }
   }

# Set up the array of input files: 1 or more mtz and maybe some pdb files
  set array(INPUT_FILES) "HKLIN_MAIN"

  for { set i 1 } { $i <= $array(N_ENSEMBLE) } { incr i } {
   if { [ regexp MAP [GetValue $arrayname ENSEMBLE_OPTION,$i ] ] } {
    append array(INPUT_FILES) " HKLIN,$i"
   }
   if { [ regexp PDB [GetValue $arrayname ENSEMBLE_OPTION,$i ] ] } {
   for { set j 1 } { $j <= $array(N_PDB,$i) } { incr j } {
    append array(INPUT_FILES) " XYZIN,[subst $i]_$j"
    }
  }
  }

  if { ([GetValue $arrayname SCATTERING] == "COMPONENT") } {
  for { set n 1 } { $n <= $array(N_COMPONENT) } { incr n } {
    if { $array(COMP_OPTION,$n) == "sequence file" } {
        append array(INPUT_FILES) " COMP_FILE,$n"
     }
   }
   }

  if { ([GetValue $arrayname SET_SOL_FILE_NOTOB] != "") } { 
        append array(INPUT_FILES) " SET_SOL_FILE_NOTOB"
  }

  return 1
  }

#------------------------------------------------------------------------
 proc phaser_ELLG_component { arrayname counter_component } {
#------------------------------------------------------------------------
# The contents of the "Composition" folder, extending frame 
  upvar #0 $arrayname array

  CreateLine line \
      message "Select for protein or DNA/RNA as a component of the asymmetric unit" \
      label "Component #$counter_component" \
      widget PROTDNA \
      message "Enter the molecular weight of protein/nucleic acid #$counter_component" \
      label "" \
      widget COMP_OPTION -oblig \
      widget MW -oblig \
      message "Enter the number of copies of component #$counter_component in the asu " \
      label "Number in asymmetric unit" \
      widget ASYM -oblig \
     toggle_display COMP_OPTION,$counter_component open MW hide

  CreateLine line \
      message "Select for protein or DNA/RNA as a component of the asymmetric unit" \
      label "Component $counter_component" \
      widget PROTDNA \
      message "Enter the file containing sequence in single letter code or fasta format" \
      label "" \
      widget COMP_OPTION -oblig \
      message "Enter the number of copies of component $counter_component in the asu" \
      label "Number in asymmetric unit" \
      widget ASYM -oblig \
     toggle_display COMP_OPTION,$counter_component open FASTA hide

  CreateInputFileLine line \
      "Select the sequence file" "SEQ file" COMP_FILE DIR_COMP_FILE \
     -toggle_display COMP_OPTION,$counter_component open FASTA hide

  CreateLine line \
      message "Select for protein or DNA/RNA as a component of the asymmetric unit" \
      label "Component $counter_component" \
      widget PROTDNA \
      message "Enter the number of residues of protein/nucleic acid $counter_component" \
      label "" \
      widget COMP_OPTION -oblig \
      widget COMP_NRES -oblig \
      message "Enter the number of copies of component $counter_component in the asymmetric unit" \
      label "Number in asymmetric unit" \
      widget ASYM -oblig \
     toggle_display COMP_OPTION,$counter_component open NRES hide

  }

#------------------------------------------------------------------------
 proc phaser_ELLG_ensemble { arrayname counter_ensemble } {
#------------------------------------------------------------------------
# Sets up the main ensembles definition folder, extending frame

  upvar #0 $arrayname array

# Set a default name for ENSEMBLE_ID of ensemble1, ensemble2, ensemble3 etc
  if { $array(ENSEMBLE_ID,$counter_ensemble) == "" } {
       set array(ENSEMBLE_ID,$counter_ensemble) "ensemble$counter_ensemble" }

  CreateLine line \
      message "Enter unique identifier for ensemble #$counter_ensemble" \
      label "Ensemble Name" widget ENSEMBLE_ID \
        -command "update_phaser_ELLG_ensemble $arrayname" \
        -oblig \
      message "Define the ensemble by PDB file(s) or MAP file" \
      label "Define ensemble via" widget ENSEMBLE_OPTION \
      toggle_display ENSEMBLE_OPTION,$counter_ensemble open MAP

  CreateLine line \
      message "Enter unique identifier for ensemble #$counter_ensemble" \
      label "Ensemble Name" widget ENSEMBLE_ID \
        -command "update_phaser_ELLG_ensemble $arrayname" \
        -oblig \
      message "Define the ensemble by PDB file(s) or MAP file" \
      label "Define ensemble via" widget ENSEMBLE_OPTION \
      toggle_display ENSEMBLE_OPTION,$counter_ensemble open PDB \
      widget TOG_HETATM label "Use HETATM atoms"

  OpenSubFrame frame -toggle_display ENSEMBLE_OPTION,$counter_ensemble open MAP

  CreateInputFileLine line \
      "Enter MTZ map file name (HKLIN) for ensemble # $counter_ensemble" \
      "MTZ     " \
      HKLIN DIR_HKLIN

  CreateLabinLine line \
      "Select calculated amplitude (FC) and calculated phase (PHIC)" \
      HKLIN,$counter_ensemble "FC" FC [list FC] \
      -help labin \
      -sigma "PHIC" PHIC [list PHIC]

  CreateLine line \
      message "Enter molecular weight of protein in the object\
            represented by the map (from MTZ file)" \
      label "Protein MW" widget ENSEMBLE_PROTMW -oblig \
      message "Enter molecular weight of nucleic acid in the object\
               represented by the map (from MTZ file)" \
      label "Nucleic acid MW" widget ENSEMBLE_NUCLMW -oblig \
      message "Enter the extent of the model in the X, Y and Z directions" \
      label "Extent: X" widget ENSEMBLE_EXT_X -oblig \
      label "Y" widget ENSEMBLE_EXT_Y -oblig \
      label "Z" widget ENSEMBLE_EXT_Z -oblig label "Angstroms"

  CreateLine line \
      message "Similarity to the target" \
      label "RMS error" widget ENSEMBLE_RMS_MTZ -oblig \
      message "Enter coordinates of the centre of the model, in Angstroms" \
      label "Centre: X" widget ENSEMBLE_CEN_X -oblig \
      label "Y" widget ENSEMBLE_CEN_Y -oblig \
      label "Z" widget ENSEMBLE_CEN_Z -oblig label "Angstroms" 

  CloseSubFrame

# Within the Add Ensemble toggle frame, if PDB is selected for ENSEMBLE_OPTION,
#   then insert an extending frame for adding pdb files for each ensemble

  OpenSubFrame frame -toggle_display ENSEMBLE_OPTION,$counter_ensemble open PDB 

  CreateExtendingFrame N_PDB phaser_ELLG_pdb \
      "Add another PDB file to the ensemble" \
      "Add superimposed PDB file to the ensemble" \
      [list XYZIN DIR_XYZIN RMS_OPTION RMS ] \
       -counter $counter_ensemble

  CloseSubFrame

# Check and update the ensemble variable menu
  edit_phaser_ELLG_ensemble $arrayname $counter_ensemble

  }

#------------------------------------------------------------------------
 proc phaser_ELLG_pdb { arrayname counter_pdb counter_ensemble } {
#------------------------------------------------------------------------
# The ensemble pdb extending frame lines
  upvar #0 $arrayname array

  CreateInputFileLine line \
      "Select a PDB file associated with ensemble #$counter_ensemble " \
      "PDB #$counter_pdb" \
      XYZIN DIR_XYZIN

  CreateLine line \
      message "Specify RMS error in Angstrom, or \
               via the sequence identity (percentage or a fraction)" \
      label "Similarity of PDB #$counter_pdb to the target structure" \
      widget RMS_OPTION widget RMS -oblig \
      toggle_display RMS_OPTION,[subst $counter_ensemble]_$counter_pdb open [list "sequence identity" "rms difference" "low rms from id" "high rms from id"] hide

  CreateLine line \
      message "Specify RMS error in Angstrom, or \
               via the sequence identity (percentage or a fraction)" \
      label "Similarity of PDB #$counter_pdb to the target structure" \
      widget RMS_OPTION \
      toggle_display RMS_OPTION,[subst $counter_ensemble]_$counter_pdb open [list "from pdb REMARK" ] hide

# puts $array(RMS_OPTION,[subst $counter_ensemble]_$counter_pdb)
# puts RMS_OPTION,[subst $counter_ensemble]_$counter_pdb

  }

#------------------------------------------------------------------------
 proc update_phaser_ELLG_ensemble { arrayname } {
#------------------------------------------------------------------------
# This is called whenever the user clicks into the widget for an
#   ensemble name 
  upvar #0 $arrayname array
# It looks like an empty (redundant) procedure but is needed to ensure
#   that $array(N_ENSEMBLE) gets handed correctly to the menu procedure
  update_phaser_ELLG_ensemble_menu $arrayname $array(N_ENSEMBLE)
  }

#------------------------------------------------------------------------
 proc update_phaser_ELLG_ensemble_menu { arrayname n_ensemble } {
#------------------------------------------------------------------------
# Updates the variable menu of ensembles
  upvar #0 $arrayname array
# Build up list for menu
  set menu {}
  for { set n 1 } { $n <= $n_ensemble } { incr n } { lappend menu $array(ENSEMBLE_ID,$n) }
# Do the update
  UpdateVariableMenu $arrayname initialise 0 ENSEMBLE_MENU_LIST $menu 
  }

#------------------------------------------------------------------------
 proc edit_phaser_ELLG_ensemble { arrayname counter_ensemble } {
#------------------------------------------------------------------------
# When an ensemble is removed or added, need to update the variable menu
#   for ensemble selection.
# counter_ensemble is passed from the calling procedure. It is the 
#   number of ensembles (= number of frames)
  upvar #0 $arrayname array
  set usermenu {}
  set alias {}
# Create the ensemble menu list and its alias
  for { set n 1 } { $n <= $counter_ensemble } { incr n } { lappend usermenu "$n:  $array(ENSEMBLE_ID,$n)" }
  for { set n 1 } { $n <= $counter_ensemble } { incr n } { lappend alias "$array(ENSEMBLE_ID,$n)" }

# Update ENSEMBLE_ID if an ensemble is deleted and its ENSEMBLE_ID
#   is still set to the default ensemble$counter_ensemble
#   (which will now be out by one (one too high))
# regexp as below on "ensemble1" produces j1_whole=ensemble1
#                                         j2_root=ensemble
#                                         j3_num=1

  if { $counter_ensemble > 0 } {
     for { set p 1 } { $p <= $array(N_ENSEMBLE) } { incr p } {
         if { [regexp "^(ensemble)(\[0-9\])$" $array(ENSEMBLE_ID,$p) \
                        j1_whole j2_root j3_num] } {
            # If the trailing number (j3_num) is different from 
            #   the current molecule number (p) then reset the id
            if { $j3_num != $p } { set array(ENSEMBLE_ID,$p) "ensemble$p" }
         }
     }
  }

# Update the variable menu with the list of ensemble names
  update_phaser_ELLG_ensemble_menu $arrayname $counter_ensemble

  }

#------------------------------------------------------------------------
 proc update_ensemble_menu { arrayname n_ensemble } {
#------------------------------------------------------------------------
# Updates the variable menu of ensembles
  upvar #0 $arrayname array
# Build list for menu
  set menu {}
  for { set n 1} {$n <= $n_ensemble } { incr n } { lappend menu $array(ENSEMBLE_ID,$n) }
  UpdateVariableMenu $arrayname initialise 0 ENSEMBLE_MENU_LIST $menu
  }


#------------------------------------------------------------------------
 proc phaser_ELLG_updatevarlabel { arrayname FILE_SPACEGROUP } {
#------------------------------------------------------------------------
# procedure to update the FILE_SPACEGROUP varlabel
  upvar #0 $arrayname array
  set field_list [GetWidget $arrayname FILE_SPACEGROUP]
  foreach field $field_list {
    if { [winfo exists $field] } { $field configure -text $array(FILE_SPACEGROUP) } }
  }

#------------------------------------------------------------------------
  proc phaser_ELLG_set_reso {arrayname} {
#------------------------------------------------------------------------
  upvar #0 $arrayname array
  if { ( $array(RESOLUTION_DMIN) >= 2.5 ) } {
     set array(RESOLUTION_DMIN_MR) [GetValue $arrayname RESOLUTION_DMIN]
     } else {
     set array(RESOLUTION_DMIN_MR) 2.5
     }
}

#------------------------------------------------------------------------
  proc phaser_ELLG_task_window {arrayname} {
#------------------------------------------------------------------------

  upvar #0 $arrayname array
  global configure

  if { [CreateTaskWindow $arrayname \
        "Expected LLG"\
        "Phaser" \
        [ list "Define data" \
               "Define ensembles (models)" \
               "Define composition of the asymmetric unit" \
               "Output control" ] \
       ] == 0 } return

# SetProgramHelpFile phaser


#=PROTOCOL===============================================================

  OpenFolder protocol

  CreateTitleLine line TITLE

#=FILE===================================================================

# The "Define data" folder

  OpenFolder 1 

  CreateInputFileLine line \
      "Enter MTZ file name (HKLIN)" \
      "MTZ in     " \
      HKLIN_MAIN DIR_HKLIN_MAIN \
        -setfileparam resolution_min RESOLUTION_DMAX \
        -setfileparam resolution_max RESOLUTION_DMIN \
        -setfileparam resolution_max RESOLUTION_AUTO_DMIN \
        -setfileparam cell_1 CELL_1 \
        -setfileparam cell_2 CELL_2 \
        -setfileparam cell_3 CELL_3 \
        -setfileparam cell_4 CELL_4 \
        -setfileparam cell_5 CELL_5 \
        -setfileparam cell_6 CELL_6 \
        -setfileparam space_group_name FILE_SPACEGROUP \
        -command "phaser_ELLG_set_reso $arrayname"

  CreateLine line \
      widget TOG_INTENSITY_OPTION \
      label "Input data are merged intensities"

   CreateLabinLine line \
       "Select amplitude (F) and sigma (SIGF)" \
       HKLIN_MAIN "F" F {NULL} \
       -sigma "SIGF" SIGF {NULL} \
      -toggle_display TOG_INTENSITY_OPTION open 0

   CreateLabinLine line \
       "Select intensity (I) and sigma (SIGI)" \
       HKLIN_MAIN "I" I {NULL} \
       -sigma "SIGI" SIGI {NULL} \
      -toggle_display TOG_INTENSITY_OPTION open 1

  CreateLine line \
      message "Space group read from mtz file." \
      label "Space group read from mtz file" \
      varlabel FILE_SPACEGROUP

# $line.l2 configure -width 7 -background $configure(COLOUR_BACKGROUND)

#========================================================================
# The "Define ensembles" folder

  OpenFolder 2 open

  CreateToggleFrame N_ENSEMBLE phaser_ELLG_ensemble \
      "Define another ensemble" \
      "Ensemble #" \
      "Add ensemble" \
      [list ENSEMBLE_ID ENSEMBLE_OPTION HKLIN DIR_HKLIN ENSEMBLE_LABIN FC PHIC N_PDB ] \
       -edit_proc edit_phaser_ELLG_ensemble \
       -child phaser_ELLG_pdb

#========================================================================
# The "Define composition of the asymmetric unit" folder

  OpenFolder 3  open 

  CreateLine line \
      message "Total scattering of atoms in the asymmetric unit" \
      label "Total scattering determined by "\
      widget SCATTERING \
      toggle_display SCATTERING hide SOLVENT 

  CreateLine line \
      message "Total scattering of atoms in the asymmetric unit" \
      label "Total scattering determined by "\
      widget SCATTERING \
      widget SCATTERING_SOLVENT  -oblig \
      toggle_display SCATTERING open SOLVENT 

  OpenSubFrame frame -toggle_display SCATTERING open COMPONENT hide

  CreateExtendingFrame N_COMPONENT phaser_ELLG_component \
      "Define another protein or nucleic acid component" "Define another component" \
      [list PROTDNA MW ASYM COMP_OPTION COMP_NRES COMP_FILE DIR_COMP_FILE ]

  CloseSubFrame

#========================================================================
# The "Output control" folder

  OpenFolder 4 closed

  CreateLine line \
      message "VERBOSE: more information included in log file than by default" \
      widget TOG_VERBOSE \
      label "Verbose output to log file"

  CreateLine line \
      message "DEBUG: debugging information included in log file than by default" \
      widget TOG_VERBOSE_EXTRA \
      label "Debug output to log file" 

  }

#use puts to output debugging info to screen
