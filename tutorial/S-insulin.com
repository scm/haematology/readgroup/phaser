phenix.phaser  --phenix  << eof
MODE EP_AUTO
HKLIN S-insulin.mtz
CRYST S-insulin DATA sad LABIN F+ = F(+) SIGF+ = SIGF(+) F- = F(-) SIGF- = SIGF(-)
ATOM CRYST S-insulin PDB S-insulin_hyss.pdb
WAVE 1.54
COMPOSITION PROTEIN SEQ S-insulin.seq NUM 1
ROOT junk
eof
