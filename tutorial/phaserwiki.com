#--------------beta_blip_ano.com
phaser << eof
MODE ANO
TITLe beta blip data correction
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ROOT beta_blip_ano # not the default
eof
#--------------beta_cca.com
phaser << eof
TITLe BETA-BLIP cell content analysis
MODE CCA
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
RESO 3.0
ROOT beta_blip_cca # not the default
eof
#--------------beta_nma.com
phaser << eof
TITLe beta normal mode analysis
MODE NMA
ENSEmble beta PDB beta.pdb IDENtity 100
XYZOut OFF
ROOT beta_nma # not the default
eof
#--------------beta_nma_pdb.com
phaser << eof
TITLe beta normal mode analysis pdb file generation
MODE NMA
ENSEmble beta PDB beta.pdb IDENtity 100
ROOT beta_nma_pdb # not the default
EIGEn beta_nma.mat
NMAPdb MODE 7 MODE 10 RMS 0.5 FORWARD
eof
#--------------beta_blip_auto.com
phaser << eof
TITLe beta blip automatic
MODE MR_AUTO
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARch ENSEmble beta NUM 1
SEARch ENSEmble blip NUM 1
ROOT beta_blip_auto # not the default
eof
#--------------beta_blip_auto_sg.com
phaser << eof
TITLe beta blip automatic
MODE MR_AUTO
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARch ENSEmble beta NUM 1
SEARch ENSEmble blip NUM 1
PERMutations ON # not the default
FINAl SELEct NUM 1
SGALternative HAND # not the default
ROOT beta_blip_auto_sg # not the default
eof
#--------------beta_frf.com
phaser << eof
TITLe beta FRF
MODE MR_FRF
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARCH ENSEmble beta
ROOT beta_frf
eof
#--------------blip_frf_with_beta.com
phaser << eof
TITLe blip FRF with beta rotation and translation
MODE MR_FRF
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 #beta
COMPosition PROTein MW 17522 #blip
SEARch ENSEmble blip
SOLUtion 6DIM ENSEmble beta EULEr 201 41 184 FRACtional -0.49408 -0.15571 -0.28148
ROOT blip_frf_with_beta
eof
#--------------blip_frf_with_beta_rot.com
phaser << eof
TITLe blip FRF with beta R
MODE MR_FRF
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARch ENSEmble blip
@beta_frf.sol # solution file output by phaser
ROOT blip_frf_with_beta_rot
eof
#--------------beta_brf.com
phaser << eof
TITLe beta BRF
MODE MR_FRF
TARGET ROT BRUTE
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARch ENSEmble beta
ROOT beta_brf
eof
#--------------beta_brf_around.com
phaser << eof
TITLe beta BRF fine sampling
MODE MR_FRF
TARGET ROT BRUTE
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
SEARch ENSEmble beta
ROTAte AROUnd EULEr 201 41 184 RANGE 10
SAMPling ROTation 0.5
XYZOut ON # not the default
TOPFiles 1 # not the default
ROOT beta_brf_around
eof
#--------------beta_ftf.com
phaser << eof
TITLe beta FTF
MODE MR_FTF
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
@beta_frf.rlist
ROOT beta_ftf
eof
#--------------blip_ftf_with_beta.com
phaser << eof
TITLe beta FTF
MODE MR_FTF
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
@blip_frf_with_beta.rlist
ROOT blip_ftf_with_beta
eof
#--------------beta_btf.com
phaser << eof
TITLe beta BTF
MODE MR_FTF
TARGET TRA BRUTE
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
@beta_frf.rlist
TRANslate AROUnd FRACtional POINt -0.49408 -0.15571 -0.28148 RANGe 5
ROOT beta_btf
eof
#--------------beta_btf_degen_x.com
phaser << eof
TITLe beta degenerate X
MODE MR_FTF
TARGET TRA BRUTE
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
@beta_frf.rlist
TRANslate DEGEnerate X
ROOT beta_btf_degen_x
eof
#--------------beta_blip_rnp.com
phaser << eof
TITLe beta blip rigid body refinement
MODE MR_RNP
HKLIn beta_blip_P3221.mtz
LABIn F=Fobs SIGF=Sigma
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
ROOT beta_blip_rnp # not the default
HKLOut OFF # not the default
XYZOut OFF # not the default
@beta_blip_auto.sol
eof
#--------------beta_blip_llg.com
phaser << eof
TITLe beta blip solution 6A P3121
MODE MR_LLG
HKLIn beta_blip_P3221.mtz
LABIn F=F SIGF = SIGF
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
ROOT beta_blip_llg # not the default
RESOlution 6.0
SPACegroup P 31 2 1
@beta_blip_auto.sol
eof
#--------------beta_blip_pak.com
phaser << eof
TITLe beta blip packing check
MODE MR_PAK
HKLIn beta_blip_P3221.mtz
LABIn F=F SIGF=SIGF
ENSEmble beta PDB beta.pdb IDENtity 100
ENSEmble blip PDB blip.pdb IDENtity 100
COMPosition PROTein MW 28853 NUM 1 #beta
COMPosition PROTein MW 17522 NUM 1 #blip
ROOT beta_blip_pak # not the default
PACK 1 # not the default
@beta_blip_auto.sol
eof
#--------------S-insulin_sad.com
phaser << eof
MODE EP_AUTO
TITLe sad phasing insulin with intrinsic sulphurs
HKLIn S-insulin.mtz
CRYStal insulin DATAset sad LABIn F(+)=F(+) SIGF(+)=SIGF(+) F(-)=F(-) SIGF(-)=SIGF(-)
WAVElength 1.5418
LLGComplete COMPLETE ON SCATterer S
ATOM CRYStal insulin PDB S-insulin_hyss.pdb
ROOT insulin_sad
eof
#--------------S-insulin_mr_sad.com
phaser << eof
MODE EP_SAD
TITLe sad phasing insulin with intrinsic sulphurs
HKLIn S-insulin.mtz
CRYStal insulin DATAset sad LABIn F(+)=F(+) SIGF(+)=SIGF(+) F(-)=F(-) SIGF(-)=SIGF(-)
WAVElength 1.5418
LLGComplete COMPLETE ON SCATterer S
ATOM CRYStal insulin PDB S-insulin_hyss.pdb
PART PDB S-insulin_helix.pdb ID 100
ROOT insulin_sad_part
eof
