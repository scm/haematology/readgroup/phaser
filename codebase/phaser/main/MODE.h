//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MODE__
#define __PHASER_MODE__
#include <phaser/io/CCP4base.h>
#include <phaser/io/InputBase.h>
#include <phaser/main/phasermode.h>

namespace phaser {

class MODE : public InputBase, virtual public CCP4base
{
  typedef std::pair<phasermode,std::string> pairmm;
  private:
    phasermode mode;
    std::multimap<phasermode,std::string> alias;

  public:
  MODE(phasermode mode_ = NO_MODE);

  Token_value parse(std::istringstream&);
  std::string unparse();
  void Analyse(void);

  bool setMODE(phasermode);
  bool setMODE(std::string);

  phasermode enumerator() { return mode; }

};

}//phaser

#endif
