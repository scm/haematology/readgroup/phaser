//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/main/MODE.h>
#include <phaser/io/Preprocessor.h>

#ifdef _WIN32

#ifdef _MSC_VER
#include <crtdbg.h> // for the sake of memory leak checks
#endif

// BYTE, INT16 and INT32 are defined in libccp4/ccp4/ccp4_sysdep.h
// and they conflict with Windows typedefs.
#ifdef BYTE
#undef BYTE
#endif
#ifdef INT16
#undef INT16
#endif
#ifdef INT32
#undef INT32
#endif

#include <windows.h>
// define function pointers for Windows exceptionhandler library, XCPTLib.dll,
// available from https://sourceforge.net/projects/xcptlib/
typedef void (*INITXCPT)(LPCTSTR szcompanyname, LPCTSTR szemailaddress, HWND hwnd, bool bshowgui);
typedef void (*USEXCPT)(bool bfulldump, bool binvokedebugger);
typedef void (*STOPXCPT)();
// declare pointers for the exception handler functions
INITXCPT pinitxcpt = NULL;
STOPXCPT pstopxcpt = NULL;
USEXCPT pusexcpt = NULL;
HINSTANCE hXCPTLib = NULL;


#endif


void
enable_Windows_XCPTLibHandler_if_possible()
{
#ifdef _WIN32
#ifdef _MSC_VER
// for the sake of memory leak checks
   _CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
#endif

// Use exception handler in XCPTLib.dll if present
// Catches almost all exceptions that escapes phasers try-catch blocks
// Dynamic loading so phaser won't fail if XCPTLib.dll isn't present
   hXCPTLib = LoadLibrary(TEXT("XCPTLib.dll"));

   if (hXCPTLib != NULL)
   {
// assign function pointers
      pinitxcpt = (INITXCPT) GetProcAddress(hXCPTLib, TEXT("InitialiseXCPTHandler"));
      pstopxcpt = (STOPXCPT) GetProcAddress(hXCPTLib, TEXT("StopUsingXCPTHandler"));
      pusexcpt = (USEXCPT) GetProcAddress(hXCPTLib, TEXT("UseMyXCPTHandler"));
// Initialise exception handler library with no GUI and no email invocation
      if (pinitxcpt)
         pinitxcpt("The Phaser Team", "", NULL, false);


      HKEY hKey(0);
      int nregval(0);
      DWORD dwBufLen=4;
      LONG lRet(0);

      bool binvokedebugger = false;
      bool fulldump = true;

#pragma comment( lib, "Advapi32" )// contains registry functions
// if HKEY_LOCAL_MACHINE\SOFTWARE\Phaser\InvokeDebugger is non-zero then invoke debugger at crash time
      lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,"SOFTWARE\\Phaser", 0, KEY_QUERY_VALUE, &hKey );
      if( lRet == ERROR_SUCCESS )
      {
         lRet = RegQueryValueEx( hKey, "InvokeDebugger", NULL, NULL, (LPBYTE) &nregval, &dwBufLen);
         if( (lRet == ERROR_SUCCESS) )
            nregval == 0 ? binvokedebugger = false : binvokedebugger = true;

         RegCloseKey( hKey );
      }

// if HKEY_LOCAL_MACHINE\SOFTWARE\Phaser\ProduceFullCrashDump is non-zero then write full dump at crash time
      lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,"SOFTWARE\\Phaser", 0, KEY_QUERY_VALUE, &hKey );
      if( lRet == ERROR_SUCCESS )
      {
         lRet = RegQueryValueEx( hKey, "ProduceFullCrashDump", NULL, NULL, (LPBYTE) &nregval, &dwBufLen);
         if( (lRet == ERROR_SUCCESS) )
            nregval == 0 ? fulldump = false : fulldump = true;

         RegCloseKey( hKey );
      }

      if (pusexcpt)
         pusexcpt(fulldump, binvokedebugger);

   }
#endif
}

void
disable_Windows_XCPTLibHandler_if_possible()
{
#ifdef _WIN32
  if (pstopxcpt)
  {// if exception handling library XCPTLib.dll was present then uninitialise and release it
    pstopxcpt();
    FreeLibrary(hXCPTLib);
  }
#endif
}

//Do not make main.cc part of the phaser namespace
//If you do, the fortran libraries will not link to it

#if defined(__linux)
#include <fenv.h>
#endif

void
enable_floating_point_exceptions_if_possible()
{
#if defined(__linux)
  int flags = 0;
  flags |= FE_DIVBYZERO;
  flags |= FE_INVALID;
  flags |= FE_OVERFLOW;
  if (flags != 0) {
    feenableexcept(flags);
  }
  fedisableexcept(FE_INVALID);
#endif
}

  bool
  boost_adptbx_libc_backtrace(int n_frames_skip=0)
  {
    bool result = false;
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_EXECINFO_H)
    static const int max_frames = 1024;
    void *array[max_frames];
    int size = backtrace(array, max_frames);
    fprintf(stderr, "libc backtrace (%d frames, most recent call last):\n",
      size - n_frames_skip);
    fflush(stderr);
    char **strings = backtrace_symbols(array, size);
    for(int i=size-1;i>=n_frames_skip;i--) {
      char* s = strings[i];
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_CXXABI_H)
      const char* m_bgn = 0;
#if defined(__APPLE_CC__)
      if (strlen(s) >= 52 && strncmp(s+40, "0x", 2) == 0) {
        m_bgn = strchr(s+40, ' ');
      }
#else // __linux
      m_bgn = strchr(s, '(');
#endif
      if (m_bgn != 0) {
        m_bgn++;
        const char* m_end = strchr(m_bgn,
#if defined(__APPLE_CC__)
        ' '
#else // __linux
        '+'
#endif
        );
        long n = m_end - m_bgn;
        if (n > 0) {
          char* mangled = static_cast<char*>(malloc(n+1));
          if (mangled != 0) {
            strncpy(mangled, m_bgn, n);
            mangled[n] = '\0';
            char* demangled = abi::__cxa_demangle(mangled, 0, 0, 0);
            free(mangled);
            if (demangled != 0) {
              long n1 = m_bgn - s;
              long n2 = strlen(demangled);
              long n3 = strlen(m_end);
              char* b = static_cast<char*>(
                malloc(static_cast<size_t>(n1+n2+n3+1)));
              if (b != 0) {
                strncpy(b, s, n1);
                strncpy(b+n1, demangled, n2);
                strncpy(b+n1+n2, m_end, n3);
                b[n1+n2+n3] = '\0';
                s = b;
              }
              free(demangled);
            }
          }
        }
      }
#endif // BOOST_ADAPTBX_META_EXT_HAVE_CXXABI_H
      fprintf(stderr, "  %s\n", s);
      fflush(stderr);
      if (s != strings[i]) free(s);
      result = true;
    }
    free(strings);
#endif // defined(BOOST_ADAPTBX_META_EXT_HAVE_EXECINFO_H)
    return result;
  }

void
show_call_stacks_and_exit(const char* what)
{
  bool have_libc_trace = boost_adptbx_libc_backtrace(2);
  const char* hint = "sorry, call stacks not available";
  if (have_libc_trace) {
    hint = "libc call stack above";
  }
  fprintf(stderr, "%s (%s)\n", what, hint);
  fflush(stderr);
  exit(1);
}

  void
  boost_adaptbx_segmentation_fault_backtrace(int)
  {
    show_call_stacks_and_exit("Segmentation fault");
  }

  void
  boost_adaptbx_bus_error_backtrace(int)
  {
    show_call_stacks_and_exit("Bus error");
  }

  void
  boost_adaptbx_floating_point_error_backtrace(int)
  {
    show_call_stacks_and_exit("Floating-point error");
  }

  void
  enable_signals_backtrace_if_possible()
  {
#if defined(BOOST_ADAPTBX_META_EXT_HAVE_SIGNAL_H)
#if defined(SIGSEGV)
    signal(SIGSEGV, boost_adaptbx_segmentation_fault_backtrace);
#endif
#if defined(SIGBUS)
    signal(SIGBUS, boost_adaptbx_bus_error_backtrace);
#endif
#if defined(SIGFPE)
    signal(SIGFPE, boost_adaptbx_floating_point_error_backtrace);
#endif
#endif
  }

#if defined(__linux)

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

int input_timeout (int filedes, unsigned int seconds)
{
  fd_set set;
  struct timeval timeout;

  // Initialize the file descriptor set.
  FD_ZERO (&set);
  FD_SET (filedes, &set);

  // Initialize the timeout data structure.
  timeout.tv_sec = seconds;
  timeout.tv_usec = 0;

  // select returns 0 if timeout, 1 if input available, -1 if error.
  return TEMP_FAILURE_RETRY (select (FD_SETSIZE, &set, NULL, NULL, &timeout));
}
#endif

int main(int argc, char* argv[])
{
  enable_Windows_XCPTLibHandler_if_possible();
  enable_floating_point_exceptions_if_possible();
  enable_signals_backtrace_if_possible();
  phaser::Output output;
  try {
//from phenix.python wrapper, have to enter --version as special case
//in order for this --version code to be called
    std::string svn = output.svn_revision();
    std::string git = output.git_revision();
    std::string version_str = "Phaser-" + output.version_number();
    if (svn.size()) version_str += " (svn " + svn + ")";
    if (git.size()) version_str += git; // assuming we build from a git repo
    int i(1); //only consider first command line argument
    if ((i < argc) && (std::string(argv[i]) == "--version")) //special
    {
      if (git.size())
      { // assuming we build from a git repo
        version_str += "\nSHA-1: " + output.git_hash()
          + "\nTotal number of commits of all branches: " + output.git_totalcommits();
      }
      std::cout << version_str << std::endl;
      std::exit(EXIT_SUCCESS);
    }
    if ((i < argc) && (std::string(argv[i]) == "--changelog")) //special
    {
      version_str += output.change_log();
      std::cout << version_str << std::endl;
      std::exit(EXIT_SUCCESS);
    }
    //set output type (phenix or ccp4)
    output.setPackageCCP4();//default, ccp4=exe with keyword input (not python)
    if ((i < argc) && (std::string(argv[i]) == "--phenix")) //special
    {
      output.setPackagePhenix(); //no loggraphs
    }
    //timeout for input from command line, causes prompt to be generated
#if defined(__linux)
    if (input_timeout (STDIN_FILENO, 0.1) == 0)
#endif
    {
      std::cout << version_str << std::endl;
      std::cout << "Enter keyword input:" << std::endl;
    }

    phaser::MODE PHASER_MODE(phaser::PREPRO);
    //catch the failures and print some info if it fails
    phaser::Preprocessor capture(argc,argv); //eats std::cin
    phaser::MODE inputMode;
    phaser::VERB inputVerb;
    phaser::MUTE inputMute;
    try {
      capture.Analyse(); //will throw error if an included filename does not exist
                         //will throw error if an syntax error in keywords
      inputMode.parseCCP4(capture.Cards());
      inputMode.Analyse();
      inputVerb.parseCCP4(capture.Cards());
      inputVerb.throw_errors();
      inputMute.parseCCP4(capture.Cards());
      inputMute.throw_errors();
    }
    catch (...)
    { //repeat if there is an error thrown above
      //output header first then errors
      //inputVerb keyword not set if error thrown from parsing, can't use here
      output.logHeader(phaser::SUMMARY,phaser::header(phaser::PREPRO));
      throw;
    }
    //OK continue
    output.setLevel(inputVerb.OUTPUT_LEVEL);
    output.setMute(inputMute.MUTE_ON);
    //mute output does not make sense for CCP4, no other way of reporting results
    output.logHeader(phaser::SUMMARY,phaser::header(phaser::PREPRO));
    output.logBlank(phaser::SUMMARY);
    { //scope for input
    PHASER_MODE.setMODE(inputMode.enumerator());
    output.logKeywords(phaser::SUMMARY,capture.Cards());
    output.logTrailer(phaser::LOGFILE);
    }

    switch (PHASER_MODE.enumerator())
    {
      //modes with no reflection data
      case phaser::NMAXYZ: {
        phaser::InputNMA input(capture.Cards());
        output = phaser::runNMAXYZ(input,output);
      }
      break;

      case phaser::SCEDS: {
        phaser::InputNMA input(capture.Cards());
        output = phaser::runSCEDS(input,output);
      }
      break;

      //all modes calling MR_DAT
      case phaser::MR_AUTO:
      case phaser::NCS:
      case phaser::ANO:
      case phaser::DFAC:
      case phaser::CCA:
      case phaser::MR_ELLG:
      case phaser::MR_FRF:
      case phaser::MR_FTF:
      case phaser::MR_RNP:
      case phaser::MR_OCC:
      case phaser::MR_GYRE:
      case phaser::MR_GMBL:
      case phaser::MR_PAK:
      case phaser::MR_ATOM:
      case phaser::SANDBOX:
      {
        phaser::data_refl REFLECTIONS;
        double MTZ_HIRES;
        { //scope for input
          phaser::InputMR_DAT input(capture.Cards());
          input.setRESO(0,DEF_LORES); //read all data
          phaser::ResultMR_DAT result = phaser::runMR_DAT(input,output);
          output = result;
          if (result.Failure())
            PHASER_MODE.setMODE(phaser::NO_MODE); //skip all switches below
          REFLECTIONS = result.DATA_REFL;
          MTZ_HIRES = result.getMtzHiRes();
        }

        switch (PHASER_MODE.enumerator())
        {
          case phaser::MR_AUTO: {
            phaser::InputMR_AUTO input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_AUTO(input,output);
          }
          break;

          case phaser::MR_ELLG: {
            phaser::InputMR_ELLG input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_ELLG(input,output);
          }
          break;

          case phaser::NCS: {
            phaser::InputNCS input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runNCS(input,output);
          }
          break;

          case phaser::ANO: {
            phaser::InputANO input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runANO(input,output);
          }
          break;

          case phaser::DFAC: {
            phaser::InputDFAC input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runDFAC(input,output);
          }
          break;


          case phaser::MR_FRF: {
            phaser::InputMR_FRF input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_FRF(input,output);
          }
          break;

          case phaser::MR_FTF:
          {
            phaser::InputMR_FTF input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_FTF(input,output);
          }
          break;

          case phaser::MR_RNP: {
            phaser::InputMR_RNP input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_RNP(input,output);
          }
          break;

          case phaser::MR_OCC: {
            phaser::InputMR_OCC input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_OCC(input,output);
          }
          break;

          case phaser::MR_GYRE: {
            phaser::InputMR_RNP input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_GYRE(input,output);
          }
          break;

          case phaser::MR_GMBL: {
            phaser::InputMR_RNP input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_GMBL(input,output);
          }
          break;

          case phaser::MR_PAK: {
            phaser::InputMR_PAK input(capture.Cards());
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            output = phaser::runMR_PAK(input,output);
          }
          break;

          case phaser::CCA: {
            phaser::InputCCA input(capture.Cards());
            input.setRESO_HIGH(MTZ_HIRES);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            output = phaser::runCCA(input,output);
          }
          break;

          case phaser::MR_ATOM:
          {
            phaser::InputMR_ATOM input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runMR_ATOM(input,output);
          }
          break;

          case phaser::SANDBOX: {
            phaser::InputMR_ELLG input(capture.Cards());
            input.setREFL_DATA(REFLECTIONS);
            input.setSPAC_HALL(REFLECTIONS.SG_HALL);
            input.setCELL6(REFLECTIONS.UNIT_CELL);
            output = phaser::runSANDBOX(input,output);
          }
          break;

          default: {/*errors*/}

        } //switch separate modes
      } //switch MR_DAT modes
      break;

      //modes calling EP_DAT
      case phaser::EP_AUTO:
      case phaser::EP_SAD:
      case phaser::EP_SSD:
      {
        phaser::data_ep crysdata;
        phaser::af::double6 uc;
        std::string hall;
        { //scope for input
          phaser::InputEP_DAT input(capture.Cards());
          phaser::ResultEP_DAT result = phaser::runEP_DAT(input,output);
          output = result;
          if (result.Failure())
            PHASER_MODE.setMODE(phaser::NO_MODE); //skip all switches below
          uc = result.getCell6();
          hall = result.getHall();
          crysdata = result.getCrysData();
        }

        switch (PHASER_MODE.enumerator())
        {
          case phaser::EP_AUTO:
          {
            phaser::InputEP_AUTO input(capture.Cards());
            input.setCELL6(uc);
            input.setSPAC_HALL(hall);
            input.setCRYS_DATA(crysdata);
            output = phaser::runEP_AUTO(input,output);
          }
          break;

          case phaser::EP_SAD:
          {
            phaser::InputEP_SAD input(capture.Cards());
            input.setCELL6(uc);
            input.setSPAC_HALL(hall);
            input.setCRYS_DATA(crysdata);
            output = phaser::runEP_SAD(input,output);
          }
          break;

          case phaser::EP_SSD:
          {
            phaser::InputEP_SSD input(capture.Cards());
            input.setCELL6(uc);
            input.setSPAC_HALL(hall);
            input.setCRYS_DATA(crysdata);
            output = phaser::runEP_SSD(input,output);
          }
          break;

          default: {/*errors*/}

        } //switch separate modes
      } //switch EP_DAT modes
      break;

      default: {/*errors*/}
    }
  }
  catch (phaser::PhaserError& err) {
    output.logError(phaser::LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    output.logError(phaser::LOGFILE,phaser::PhaserError(phaser::MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    output.logError(phaser::LOGFILE,phaser::PhaserError(phaser::UNHANDLED,err.what()));
  }
  catch (...) {
    output.logError(phaser::LOGFILE,phaser::PhaserError(phaser::UNKNOWN));
  }

  output.logTerminate(phaser::SUMMARY,true);
  disable_Windows_XCPTLibHandler_if_possible();
  return output.ExitCode(); //not return value of phenix.phaser
}
