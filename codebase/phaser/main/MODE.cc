//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/MODE.h>

namespace phaser {

  MODE::MODE(phasermode mode_) : CCP4base(), InputBase()
  {
    Add_Key("MODE");
    //Add to CCP4base;
    inputPtr iPtr(this);
    possible_fns.push_back(iPtr);

    mode = NO_MODE;
    //second and subsequence values are deprecated aliases
    alias.insert(pairmm(ANO,"ANO")); alias.insert(pairmm(ANO,"MR_ANO"));
    alias.insert(pairmm(CCA,"CCA")); alias.insert(pairmm(CCA,"MR_CCA"));
    alias.insert(pairmm(DFAC,"DFAC"));
    alias.insert(pairmm(EP_AUTO,"EP_AUTO"));
    alias.insert(pairmm(EP_SAD,"EP_SAD"));
    alias.insert(pairmm(EP_SSD,"EP_SSD"));
    alias.insert(pairmm(MR_ATOM,"MR_ATOM"));
    alias.insert(pairmm(MR_AUTO,"MR_AUTO"));
    alias.insert(pairmm(MR_ELLG,"MR_ELLG"));
    alias.insert(pairmm(MR_FRF,"MR_FRF")); alias.insert(pairmm(MR_FRF,"MR_ROT")); alias.insert(pairmm(MR_FRF,"ROT"));
    alias.insert(pairmm(MR_FTF,"MR_FTF")); alias.insert(pairmm(MR_FTF,"MR_TRA")); alias.insert(pairmm(MR_FTF,"TRA"));
    alias.insert(pairmm(MR_OCC,"MR_OCC")); alias.insert(pairmm(MR_OCC,"PRUNE"));
    alias.insert(pairmm(MR_PAK,"MR_PAK"));
    alias.insert(pairmm(MR_GYRE,"MR_GYRE")); alias.insert(pairmm(MR_GYRE,"GYRE"));
    alias.insert(pairmm(MR_GMBL,"MR_GMBL")); alias.insert(pairmm(MR_GMBL,"GIMBLE"));
    alias.insert(pairmm(MR_RNP,"MR_RNP"));
    alias.insert(pairmm(NCS,"NCS")); alias.insert(pairmm(NCS,"TNCS"));
    alias.insert(pairmm(NMAXYZ,"NMAXYZ")); alias.insert(pairmm(NMAXYZ,"NMA"));
    alias.insert(pairmm(NO_MODE,"NONE"));
    alias.insert(pairmm(PREPRO,"PREPRO"));
    alias.insert(pairmm(SCEDS,"SCEDS"));
    alias.insert(pairmm(SANDBOX,"SANDBOX"));
    setMODE(mode_);
  }

  Token_value MODE::parse(std::istringstream& input_stream)
  {
    std::string modestr = getString(input_stream);
    bool nomode = !setMODE(modestr);
    if (nomode)
    {
      std::string possible_modes;
      std::multimap<phasermode,std::string>::iterator iter = alias.begin();
      iter++;//skip NO_MODE/NONE
      iter++;//skip PREPRO
      iter++;//skip SANDBOX
      while (iter != alias.end())
      {
        if (iter == alias.lower_bound(iter->first)) //first in list
          possible_modes += iter->second + " ";
        iter++;
      }
      possible_modes.erase(possible_modes.rfind(" "),1);
      possible_modes.replace(possible_modes.rfind(" "),1," or ");
      CCP4base::store(PhaserError(INPUT,keywords,"Mode \"" + modestr + "\" not recognised: Use " + possible_modes));
    }
    return skip_line(input_stream);
  }

  bool MODE::setMODE(phasermode key)
  {
    for (std::multimap<phasermode,std::string>::iterator iter = alias.begin(); iter != alias.end(); iter++)
      if (iter->first == key)
        mode = iter->first;
    return (mode != NO_MODE);
  }

  bool MODE::setMODE(std::string key)
  {
    for (int s = 0; s < key.size(); s++)
      key[s] = std::toupper(key[s]); //convert to uppercase
    for (std::multimap<phasermode,std::string>::iterator iter = alias.begin(); iter != alias.end(); iter++)
      if (iter->second == key)
        mode = iter->first;
    return (mode != NO_MODE);
  }

  std::string MODE::unparse()
  { return "MODE " + alias.find(mode)->second; }

  void MODE::Analyse(void)
  {
    if (mode == NO_MODE)
    CCP4base::store(PhaserError(INPUT,keywords,"No MODE set"));
    CCP4base::throw_errors();
  }

}//phaser
