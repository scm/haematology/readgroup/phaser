//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_PHASERMODE_ENUM__
#define __PHASER_PHASERMODE_ENUM__
#include <string>

namespace phaser {

enum phasermode
{
  NO_MODE,
  PREPRO,
  SANDBOX,
  ANO,
  CCA,
  DFAC,
  EP_AUTO,
  EP_DAT,
  EP_SAD,
  EP_SSD,
  MR_ATOM,
  MR_AUTO,
  MR_DAT,
  MR_PREP,
  MR_ELLG,
  MR_FRF,
  MR_FTF,
  MR_OCC,
  MR_PAK,
  MR_GYRE,
  MR_GMBL,
  MR_RNP,
  NCS,
  NMAXYZ,
  SCEDS
};

std::string header(phasermode);

}//phaser

#endif
