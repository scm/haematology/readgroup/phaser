//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_FLOATTYPE__
#define __PHASER_FLOATTYPE__

namespace phaser {

typedef double floatType;

}

#endif
