//(c) 2000-2016 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PROGRAM_VERSION__
#define __PROGRAM_VERSION__
#define PROGRAM_NAME         "Phaser"
#define PROGRAM_RELEASE      "2.8.3"
#endif
