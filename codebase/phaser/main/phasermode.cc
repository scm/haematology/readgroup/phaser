//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/phasermode.h>

namespace phaser {

std::string header(phasermode mode)
{
  if (mode == PREPRO)  return "PREPROCESSOR";
  if (mode == ANO)     return "ANISOTROPY CORRECTION";
  if (mode == CCA)     return "CELL CONTENT ANALYSIS";
  if (mode == DFAC)    return "EXPERIMENTAL ERROR CORRECTION PREPARATION";
  if (mode == EP_AUTO) return "AUTOMATED EXPERIMENTAL PHASING";
  if (mode == EP_DAT)  return "READ DATA FROM MTZ FILE";
  if (mode == EP_SAD)  return "SINGLE-WAVELENGTH ANOMALOUS DISPERSION";
  if (mode == EP_SSD)  return "PHASSADE SUBSTRUCTURE DETERMINATION";
  if (mode == MR_ATOM) return "SINGLE ATOM MOLECULAR REPLACEMENT";
  if (mode == MR_AUTO) return "AUTOMATED MOLECULAR REPLACEMENT";
  if (mode == MR_DAT)  return "READ DATA FROM MTZ FILE";
  if (mode == MR_PREP) return "DATA PREPARATION";
  if (mode == MR_ELLG) return "EXPECTED LLG OF ENSEMBLES";
  if (mode == MR_FRF)  return "MOLECULAR REPLACEMENT ROTATION FUNCTION";
  if (mode == MR_FTF)  return "MOLECULAR REPLACEMENT TRANSLATION FUNCTION";
  if (mode == MR_OCC)  return "MOLECULAR REPLACEMENT MODEL PRUNING";
  if (mode == MR_PAK)  return "MOLECULAR REPLACEMENT PACKING ANALYSIS";
  if (mode == MR_GYRE) return "MOLECULAR REPLACEMENT GYRE REFINEMENT";
  if (mode == MR_GMBL) return "MOLECULAR REPLACEMENT GIMBLE REFINEMENT";
  if (mode == MR_RNP)  return "MOLECULAR REPLACEMENT REFINEMENT AND PHASING";
  if (mode == NCS)     return "TRANSLATIONAL NON-CRYSTALLOGRAPHIC SYMMETRY";
  if (mode == NMAXYZ)  return "NMA AND COORDINATE PERTURBATION";
  if (mode == SCEDS)   return "NMA AND SCEDS DOMAIN ANALYSIS";
  if (mode == SANDBOX) return "SANDBOX";
  return "NONE";
}

}//phaser
