//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __runPhaser__
#define __runPhaser__
//inputs
#include <phaser/main/MODE.h>
#include <phaser/io/Output.h>
#include <phaser/io/InputANO.h>
#include <phaser/io/InputCCA.h>
#include <phaser/io/InputDFAC.h>
#include <phaser/io/InputEP_AUTO.h>
#include <phaser/io/InputEP_DAT.h>
#include <phaser/io/InputEP_SAD.h>
#include <phaser/io/InputEP_SSD.h>
#include <phaser/io/InputMR_ATOM.h>
#include <phaser/io/InputMR_AUTO.h>
#include <phaser/io/InputMR_DAT.h>
#include <phaser/io/InputMR_ELLG.h>
#include <phaser/io/InputMR_FRF.h>
#include <phaser/io/InputMR_FTF.h>
#include <phaser/io/InputMR_OCC.h>
#include <phaser/io/InputMR_PAK.h>
#include <phaser/io/InputMR_RNP.h>
#include <phaser/io/InputNCS.h>
#include <phaser/io/InputNMA.h>
#include <phaser/io/InputMR_FUSE.h>
//results
#include <phaser/io/ResultMR_DAT.h>
#include <phaser/io/ResultANO.h>
#include <phaser/io/ResultMR_RF.h>
#include <phaser/io/ResultMR_TF.h>
#include <phaser/io/ResultMR.h>
#include <phaser/io/ResultGYRE.h>
#include <phaser/io/ResultNMA.h>
#include <phaser/io/ResultNCS.h>
#include <phaser/io/ResultDFAC.h>
#include <phaser/io/ResultELLG.h>
#include <phaser/io/ResultCCA.h>
#include <phaser/io/ResultEP_DAT.h>
#include <phaser/io/ResultEP.h>
#include <phaser/io/ResultSSD.h>

namespace phaser {
bool          runMR_FUSE(InputMR_FUSE&,ResultMR&);

ResultANO     runANO(InputANO&,Output);
ResultCCA     runCCA(InputCCA&,Output);
ResultDFAC    runDFAC(InputDFAC&,Output);
ResultELLG    runMR_ELLG(InputMR_ELLG&,Output);
ResultEP      runEP_AUTO(InputEP_AUTO&,Output);
ResultEP      runEP_SAD(InputEP_AUTO&,Output);
ResultEP      runMR_ATOM(InputMR_ATOM&,Output);
ResultEP_DAT  runEP_DAT(InputEP_DAT&,Output);
ResultMR      runMR_AUTO(InputMR_AUTO&,Output);
ResultMR      runMR_FAST(InputMR_AUTO&,Output);
ResultMR      runMR_FULL(InputMR_AUTO&,Output);
ResultGYRE    runMR_GYRE(InputMR_RNP&,Output);
ResultMR      runMR_GMBL(InputMR_RNP&,Output);
ResultMR      runMR_OCC(InputMR_OCC&,Output);
ResultMR      runMR_PAK(InputMR_PAK&,Output);
ResultMR      runMR_RNP(InputMR_RNP&,Output);
ResultMR_DAT  runMR_DAT(InputMR_DAT&,Output);
ResultMR_DAT  runMR_PREP(InputMR_DAT&,Output);
ResultMR_RF   runMR_FRF(InputMR_FRF&,Output);
ResultMR_TF   runMR_FTF(InputMR_FTF&,Output);
ResultNCS     runNCS(InputNCS&,Output);
ResultNMA     runNMA(InputNMA&,bool,Output);
ResultNMA     runNMAXYZ(InputNMA&,Output);
ResultNMA     runSCEDS(InputNMA&,Output);
ResultMR      runSANDBOX(InputMR_ELLG&,Output);

//for python scripting
ResultANO     runANO(InputANO&);
ResultCCA     runCCA(InputCCA&);
ResultDFAC    runDFAC(InputDFAC&);
ResultEP      runEP_AUTO(InputEP_AUTO&);
ResultEP      runEP_SAD(InputEP_AUTO&);
ResultEP      runMR_ATOM(InputMR_ATOM&);
ResultEP_DAT  runEP_DAT(InputEP_DAT&);
ResultMR      runMR_AUTO(InputMR_AUTO&);
ResultELLG    runMR_ELLG(InputMR_ELLG&);
ResultMR      runMR_FAST(InputMR_AUTO&);
ResultMR      runMR_FULL(InputMR_AUTO&);
ResultGYRE    runMR_GYRE(InputMR_RNP&);
ResultMR      runMR_GMBL(InputMR_RNP&);
ResultMR      runMR_OCC(InputMR_OCC&);
ResultMR      runMR_PAK(InputMR_PAK&);
ResultMR      runMR_RNP(InputMR_RNP&);
ResultMR_DAT  runMR_DAT(InputMR_DAT&);
ResultMR_RF   runMR_FRF(InputMR_FRF&);
ResultMR_TF   runMR_FTF(InputMR_FTF&);
ResultNCS     runNCS(InputNCS&);
ResultNMA     runNMA(InputNMA&,bool);
ResultNMA     runNMAXYZ(InputNMA&);
ResultNMA     runSCEDS(InputNMA&);
ResultSSD     runEP_SSD(InputEP_SSD&,Output);
ResultSSD     runEP_SSD(InputEP_SSD&);

}

#endif
