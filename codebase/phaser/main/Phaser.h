//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DEFS__
#define __PHASER_DEFS__
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <complex>
#include <exception>
#include <math.h>
#include <scitbx/vec3.h>
#include <scitbx/mat3.h>
#include <boost/smart_ptr.hpp>
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/misc_functions.h>
#include <scitbx/array_family/tiny_types.h>
#include <scitbx/sym_mat3.h>
#include <scitbx/constants.h>
#include <phaser/main/FloatType.h>
#include <phaser/main/phasermode.h>

namespace cctbx { namespace miller { } }
namespace cctbx { namespace xray { } }
namespace phaser { namespace af = scitbx::af; }
namespace phaser { namespace fn = scitbx::fn; }
namespace phaser { namespace miller = cctbx::miller; }
namespace phaser { namespace xray = cctbx::xray; }

namespace phaser {

static const double two_pi_sq_on_three = scitbx::constants::two_pi_sq/3.0;

class llgc_coefs;
class data_pdb;
class MapEnsemble;
class MapTraceMol;
typedef boost::shared_ptr<MapEnsemble> MapEnsPtr;
typedef boost::shared_ptr<MapTraceMol> MapTrcPtr;

typedef std::vector<std::vector<std::vector<std::vector<floatType> > > > float4D;
typedef std::vector<std::vector<std::vector<floatType> > > float3D;
typedef std::vector<std::vector<floatType> > float2D;
typedef std::vector<floatType> float1D;

typedef std::complex<floatType> cmplxType;
typedef std::vector<std::vector<std::vector<cmplxType> > > cmplx3D;
typedef std::vector<std::vector<cmplxType> > cmplx2D;
typedef std::vector<cmplxType> cmplx1D;

typedef std::vector<std::vector<std::vector<bool> > > bool3D;
typedef std::vector<std::vector<bool> > bool2D;
typedef std::vector<bool> bool1D;

typedef std::vector<std::vector<std::vector<unsigned> > > unsigned3D;
typedef std::vector<std::vector<unsigned> > unsigned2D;
typedef std::vector<unsigned> unsigned1D;

typedef std::vector<std::vector<std::vector<std::string> > > string3D;
typedef std::vector<std::vector<std::string> > string2D;
typedef std::vector<std::string> string1D;

typedef std::vector<std::vector<std::vector<int> > > int3D;
typedef std::vector<std::vector<int> > int2D;
typedef std::vector<int> int1D;

typedef scitbx::vec3<floatType> dvect3;
typedef scitbx::mat3<floatType> dmat33;
typedef scitbx::sym_mat3<floatType> dmat6;
typedef scitbx::vec3<int> ivect3;
typedef scitbx::vec3<cmplxType> cvect3;
typedef scitbx::mat3<cmplxType> cmat33;
typedef scitbx::mat3<int> imat33;

typedef std::vector<dvect3>    dvect31D;
typedef std::vector<dmat33>    dmat331D;
typedef std::vector<cvect3>    cvect31D;
typedef std::vector<dvect31D> dvect32D;
typedef std::vector<cvect31D> cvect32D;
typedef std::vector<af::double6> double61D;

static const double u_cart_pdb_factor = 10000.;

typedef af::shared<bool>        af_bool;
typedef af::shared<int>         af_int;
typedef af::shared<floatType>   af_float;
typedef af::shared<cmplxType>   af_cmplx;
typedef af::shared<std::string> af_string;
typedef af::shared<dvect3>      af_dvect3;
typedef af::shared<dmat33>      af_dmat33;
typedef af::shared<dmat6>       af_dmat6;
typedef af::shared<llgc_coefs>  af_llgc_coefs;

typedef std::set<std::string>                      stringset;

typedef std::vector<std::pair<floatType,int> >     dq_type;
typedef std::map<std::string,std::string>          map_str_str;
typedef std::map<std::string,stringset>            map_str_strset;
typedef std::map<std::string,string1D>             map_str_str1D;
typedef std::map<std::string,int>                  map_str_int;
typedef std::map<int,std::string>                  map_int_str;
typedef std::map<std::string,floatType>            map_str_float;
typedef std::map<std::string,dvect3>               map_str_dvect3;
typedef std::map<std::string,dvect31D>             map_str_dvect31D;
typedef std::map<std::string,data_pdb>             map_str_pdb;
typedef std::vector<std::pair<int,std::string> >   changed_list;
typedef std::map<std::string,float1D>              map_str_float1D;
typedef std::map<std::string,float2D>              map_str_float2D;
typedef std::map<std::string,bool1D>               map_str_bool1D;
typedef std::pair<std::string,std::string>         xtalwave;

typedef std::map<std::string,data_pdb>::iterator   pdbIter;
typedef std::map<std::string,floatType>::iterator  map_str_float_iter;

typedef std::pair<int,int>                         pair_int_int;
typedef std::pair<floatType,floatType>             pair_flt_flt;
typedef std::pair<int,floatType>                   pair_int_flt;
} //phaser

#endif
