//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __VarianceBaseClass__
#define __VarianceBaseClass__
#include <phaser/main/Phaser.h>

namespace phaser {

//abstract - reminder! not objects of an abstract
//base class can be instantiated
class VarianceBase  //abstract
{
  public:
    VarianceBase() {}
    virtual ~VarianceBase() {}

    virtual std::string unparse() const;
    virtual af_float    array() const;
};

inline std::string  VarianceBase::unparse() const { return ""; }
inline af_float     VarianceBase::array() const  { return af_float(); }

typedef boost::shared_ptr<VarianceBase> variancePtr;

} //phaser
#endif
