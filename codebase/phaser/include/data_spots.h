//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_SPOTS_Class__
#define __DATA_SPOTS_Class__
#include <phaser/main/Phaser.h>
#include <cctbx/xray/conversions.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/miller/asu.h>

namespace phaser {

class data_spots
{
  public:
    data_spots(af_float F_=af_float(),
               af_float SIGF_=af_float(),
               af_bool PRESENT_=af_bool())
    {
      clear();
      F = F_;
      SIGF = SIGF_;
      PRESENT = PRESENT_;
      INPUT_INTENSITIES=false;
      if_empty_set_I_from_F();
    }
    data_spots(const data_spots & init)
    { set_data_spots(init); }
    const data_spots& operator=(const data_spots& right)
    { if (&right != this) set_data_spots(right); return *this; }

    void set_data_spots(const data_spots& init)
    {
      // deep copy of af::shared for threads
      F_ID = init.F_ID;
      SIGF_ID = init.SIGF_ID;
      I_ID = init.I_ID;
      SIGI_ID = init.SIGI_ID;
      DFAC_ID = init.DFAC_ID;
      INPUT_INTENSITIES = init.INPUT_INTENSITIES;

      F = init.F.deep_copy();
      SIGF = init.SIGF.deep_copy();
      Feff = init.Feff.deep_copy();
      I = init.I.deep_copy();
      SIGI = init.SIGI.deep_copy();
      FfromI = init.FfromI.deep_copy();
      SIGFfromI = init.SIGFfromI.deep_copy();
      IfromF = init.IfromF.deep_copy();
      SIGIfromF = init.SIGIfromF.deep_copy();
      DFAC = init.DFAC.deep_copy();
      PRESENT = init.PRESENT.deep_copy();
    }

  af_float  F,SIGF,I,SIGI,DFAC,Feff;
  af_float  FfromI,SIGFfromI,IfromF,SIGIfromF;
  bool INPUT_INTENSITIES;
  std::string F_ID,SIGF_ID,I_ID,SIGI_ID,DFAC_ID;
  af_bool   PRESENT;

  bool set_default_id(std::string tag)
  {
    if (F.size() && F_ID == "") F_ID = "F"+tag;
    if (SIGF.size() && SIGF_ID == "") SIGF_ID = "SIGF"+tag;
    if (I.size() && I_ID == "") I_ID = "I"+tag;
    if (SIGI.size() && SIGI_ID == "") SIGI_ID = "SIGI"+tag;
    if (DFAC.size() && DFAC_ID == "") DFAC_ID = "DFAC"+tag;
    std::set<std::string> labin;
    if (F.size()) labin.insert(F_ID);
    if (SIGF.size()) labin.insert(SIGF_ID);
    if (I.size()) labin.insert(I_ID);
    if (SIGI.size()) labin.insert(SIGI_ID);
    if (DFAC.size()) labin.insert(DFAC_ID);
    int count_labin(0);
    if (F.size()) count_labin++;
    if (SIGF.size()) count_labin++;
    if (I.size()) count_labin++;
    if (SIGI.size()) count_labin++;
    if (DFAC.size()) count_labin++;
    //not Feff, PRESENT
    //detect duplicates
    return (count_labin != labin.size());
  }

  bool no_data()
  {
    for (unsigned r = 0; r < F.size(); r++) if (F[r] != 0) return true;
    return true;
  }


  private:
  void if_empty_set_default_dfac(int NREFL)
  {
    if (DFAC.size() != NREFL)
    {
      DFAC.resize(NREFL);
      for (unsigned r = 0; r < NREFL; r++) DFAC[r] = 1;
    }
  }

  void if_Ifull_set_F_from_I()
  {
    if (!(FfromI.size() && SIGFfromI.size()) && I.size() && SIGI.size())
    {
      if (I_ID == "") I_ID = "I"; // Labels may not be set from Python
      if (SIGI_ID == "") SIGI_ID = "SIGI";
      FfromI.resize(I.size());
      SIGFfromI.resize(I.size());
      for (int r = 0; r < I.size(); r++)
      {
        cctbx::xray::f_sq_as_f_xtal_3_7<double> fsigf(I[r],SIGI[r]);
        FfromI[r] = fsigf.f;
        SIGFfromI[r] = fsigf.sigma_f;
      }
    }
  }

  void if_empty_set_I_from_F(std::string tag="")
  {
    if (!(IfromF.size() && SIGIfromF.size()) && !I.size() && F.size() && SIGF.size())
    {
      if (F_ID == "") F_ID = "F"+tag; // Labels may not be set from Python
      if (SIGF_ID == "") SIGF_ID = "SIGF"+tag;
      IfromF.resize(F.size());
      SIGIfromF.resize(F.size());
      for (int r = 0; r < F.size(); r++)
      {
        // Reconstruct I and SIGI by reversing simple transformation from
        // I/SIGI to F/SIGF used in truncate.f and f_sq_as_f_xtal_3_7.
        // The only loss of information is for reflections where the original I
        // was negative.
        // This is not appropriate when F/SIGF have been through French&Wilson
        // procedure, so catch elsewhere when relevant
        IfromF[r] = fn::pow2(F[r]);
        SIGIfromF[r] = 2.*F[r]*SIGF[r] + fn::pow2(SIGF[r]);
      }
    }
  }

  public:
  void set_default_dfactor(int NREFL)
  {
    if_empty_set_default_dfac(NREFL);
    if_Ifull_set_F_from_I();
    if_empty_set_I_from_F();
  }

  void erase(int r)
  {
    if (r<F.size())      F.erase(F.begin()+r);
    if (r<SIGF.size())   SIGF.erase(SIGF.begin()+r);
    if (r<I.size())      I.erase(I.begin()+r);
    if (r<SIGI.size())   SIGI.erase(SIGI.begin()+r);
    if (r<DFAC.size())   DFAC.erase(DFAC.begin()+r);
    if (r<Feff.size())   Feff.erase(Feff.begin()+r);
    if (r<PRESENT.size()) PRESENT.erase(PRESENT.begin()+r);
  }

  void clear()
  {
    F_ID = SIGF_ID = I_ID = SIGI_ID = DFAC_ID =  "";
    F.clear();
    SIGF.clear();
    I.clear();
    SIGI.clear();
    DFAC.clear();
    Feff.clear();
    PRESENT.clear();
    FfromI.clear();
    SIGFfromI.clear();
    IfromF.clear();
    SIGIfromF.clear();
  }

  std::string unparse(std::string tag="",bool eol=true)
  {
    std::string Card;
    Card += (F_ID != "") ? " F" + tag + " = " + F_ID : "";
    Card += (SIGF_ID != "") ? " SIGF" + tag + " = " + SIGF_ID : "";
    Card += (I_ID != "") ? " I" + tag + " = " + I_ID : "";
    Card += (SIGI_ID != "") ? " SIGI" + tag + " = " + SIGI_ID : "";
    return (Card == "" || !eol) ? Card : Card + "\n";
  }

  void expand_to_p1(cctbx::sgtbx::space_group sg,af::shared<miller::index<int> > MILLER,bool FriedelFlag)
  {
    if (F.size())
    F = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),F.const_ref()).data;
    if (SIGF.size())
    SIGF = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),SIGF.const_ref()).data;
    if (I.size())
    I = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),I.const_ref()).data;
    if (SIGI.size())
    SIGI = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),SIGI.const_ref()).data;
    if (DFAC.size())
    DFAC = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),DFAC.const_ref()).data;
    if (Feff.size())
    Feff = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),Feff.const_ref()).data;
    if (FfromI.size())
    FfromI = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),FfromI.const_ref()).data;
    if (SIGFfromI.size())
    SIGFfromI = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),SIGFfromI.const_ref()).data;
    if (IfromF.size())
    IfromF = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),IfromF.const_ref()).data;
    if (SIGIfromF.size())
    SIGIfromF = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),SIGIfromF.const_ref()).data;
    if (PRESENT.size())
    PRESENT = cctbx::miller::expand_to_p1<bool>(sg,FriedelFlag,MILLER.const_ref(),PRESENT.const_ref()).data;
  }

  af_float reduce(af::shared<std::size_t> unique,af_float data)
  {
    if (!data.size()) return data;
    af_float tmp;
    for (int i = 0; i < unique.size(); i++)
      tmp.push_back(data[unique[i]]);
    return tmp.deep_copy();
  }

  void reduce(af::shared<std::size_t> unique)
  {
    F = reduce(unique,F);
    SIGF = reduce(unique,SIGF);
    I = reduce(unique,I);
    SIGI = reduce(unique,SIGI);
    DFAC = reduce(unique,DFAC);
    Feff = reduce(unique,Feff);
    FfromI = reduce(unique,FfromI);
    SIGFfromI = reduce(unique,SIGFfromI);
    IfromF = reduce(unique,IfromF);
    SIGIfromF = reduce(unique,SIGIfromF);
    if (!PRESENT.size()) return;
    af_bool tmp;
    for (int i = 0; i < unique.size(); i++)
      tmp.push_back(PRESENT[unique[i]]);
    PRESENT = tmp.deep_copy();
  }

  //map to asu operation generated externally from miller index list
  void map_to_asu(int r, cctbx::miller::index_table_layout_adaptor ila)
  {
    cctbx::miller::map_to_asu_policy<double>::eq(ila, F[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, SIGF[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, Feff[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, I[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, SIGI[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, FfromI[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, SIGFfromI[r], false);
    cctbx::miller::map_to_asu_policy<double>::eq(ila, IfromF[r], false);
  }

};

} //phaser

#endif
