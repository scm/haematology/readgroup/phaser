//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolGYREClass__
#define __ProtocolGYREClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum macg_flags { macg_rot, macg_tra, macg_vrms, macg_anch, macg_sigr, macg_sigt };

class ProtocolGYRE : public ProtocolBase
{
  public:
    bool           FIX_ROT,FIX_TRA,FIX_VRMS,ANCHOR;
    double         SIGROT,SIGTRA;
    double         DEF_SIGROT,DEF_SIGTRA;
    floatType      NCYC;
    data_minimizer MINIMIZER;
    map_str_float  CHAIN_SIGR;
    map_str_float  CHAIN_SIGT;

    void init()
    {
      FIX_ROT  = !bool(DEF_MACG_REF_ROTA);
      FIX_TRA  = !bool(DEF_MACG_REF_TRAN);
      FIX_VRMS = !bool(DEF_MACG_REF_VRMS);
      ANCHOR =    bool(DEF_MACG_ANCHOR);
      DEF_SIGROT  = double(DEF_MACG_SIGR);
      DEF_SIGTRA  = double(DEF_MACG_SIGT);
      SIGROT  = -999;
      SIGTRA  = -999;
      NCYC = floatType(DEF_MACG_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACG_MINI));
      CHAIN_SIGR.clear();
      CHAIN_SIGT.clear();
    }

    bool is_default() const
    {
      return (
      FIX_ROT  == !bool(DEF_MACG_REF_ROTA) &&
      FIX_TRA  == !bool(DEF_MACG_REF_TRAN) &&
      FIX_VRMS == !bool(DEF_MACG_REF_VRMS) &&
      ANCHOR  ==   bool(DEF_MACG_ANCHOR) &&
      DEF_SIGROT == double(DEF_MACG_SIGR) &&
      DEF_SIGTRA == double(DEF_MACG_SIGT) &&
      SIGROT == -999 &&
      SIGTRA == -999 &&
      NCYC == floatType(DEF_MACG_NCYC) &&
      MINIMIZER.is_default() &&
      CHAIN_SIGR.size() == 0 &&
      CHAIN_SIGT.size() == 0
      );
    }

    ProtocolGYRE()
    { init(); }

    ProtocolGYRE(bool fixr,bool fixt,bool fixv)
    {
      init();
      FIX_ROT = fixr;
      FIX_TRA = fixt;
      FIX_VRMS = fixv;
    }

    ProtocolGYRE(bool fixr,bool fixt,bool fixv,bool anch,double rotref,double traref,int ncyc,std::string m)
    {
      init();
      FIX_ROT = fixr;
      FIX_TRA = fixt;
      FIX_VRMS = fixv;
      ANCHOR = anch;
      SIGROT = rotref;
      SIGTRA = traref;
      if (ncyc >= 0) NCYC = ncyc;
      MINIMIZER.set(m);
    }

    void cycle(bool refr,bool reft,bool refv,bool anch,double rotref=-999,double traref=-999)
    {
      FIX_ROT = !refr;
      FIX_TRA = !reft;
      FIX_VRMS = !refv;
      ANCHOR = anch;
      SIGROT = rotref;
      SIGTRA = traref;
    }

  //-------------------------
  //concrete member functions
  //-------------------------
  //  void setDEFAULT() { DEFAULT = true; }
  //  bool is_default() const { return DEFAULT; }
    bool is_off() const
    { return ((FIX_ROT && FIX_TRA && FIX_VRMS) || NCYC == 0); }

    void off()
    { init(); FIX_ROT = FIX_TRA = FIX_VRMS = true; NCYC = 0; }

    void all()
    { init(); FIX_ROT = FIX_TRA = FIX_VRMS = false; }

    void setNUM(int i,double d)
    {
      if (i == macg_sigr && d >= 0) DEF_SIGROT = d;
      else if (i == macg_sigt && d >= 0) DEF_SIGTRA = d;
      else if (d >=0) PHASER_ASSERT(false);
    }

    void setMAP(int i,map_str_float d)
    {
      if (i == macg_sigr) CHAIN_SIGR = d;
      else if (i == macg_sigt) CHAIN_SIGT = d;
    }

    void setFIX(int i,bool b)
    {
      if (i == macg_rot) FIX_ROT = b;
      else if (i == macg_tra) FIX_TRA = b;
      else if (i == macg_vrms) FIX_VRMS = b;
      else if (i == macg_anch) ANCHOR = b;
      else PHASER_ASSERT(false);
    }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineMR must
    //be changed as well - copy through virtual base class pointer
      if (i == macg_rot) return FIX_ROT;
      else if (i == macg_tra) return FIX_TRA;
      else if (i == macg_vrms) return FIX_VRMS;
      else if (i == macg_anch) return ANCHOR;
      else PHASER_ASSERT(false);
      return false;
    }

    double getNUM(int i) const
    {
      if (i == macg_sigr) return SIGROT >=0 ? SIGROT : DEF_SIGROT;
      else if (i == macg_sigt) return SIGTRA >=0 ? SIGTRA : DEF_SIGTRA;
      else PHASER_ASSERT(false);
      return 0;
    }

    map_str_float getMAP(int i) const
    {
      if (i == macg_sigr) return CHAIN_SIGR;
      else if (i == macg_sigt) return CHAIN_SIGT;
      else PHASER_ASSERT(false);
      return map_str_float();
    }


    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    void setNCYC(int ncyc)
    { NCYC = ncyc; }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      std::string Card =
           "MACGYRE ROT " +     std::string(FIX_ROT ? "OFF" : "ON ") +
                  " TRA " +       std::string(FIX_TRA ? "OFF" : "ON ") +
                  " VRMS " +      std::string(FIX_VRMS ? "OFF" : "ON ") +
                  " ANCHOR " +    std::string(ANCHOR ? "ON" : "OFF ") +
                  " SIGROT " +    dtos(getNUM(macg_sigr)) +
                  " SIGTRA " +    dtos(getNUM(macg_sigt)) +
                  " NCYCLE " +    itos(NCYC) +
                  " MINIMIZER " + MINIMIZER.unparse() +
                  "\n";
      for (map_str_float::const_iterator iter = CHAIN_SIGR.begin(); iter != CHAIN_SIGR.end(); iter++)
        Card += "MACGYRE CHAIN \"" + iter->first + "\" SIGR " + dtos(iter->second) + "\n";
      for (map_str_float::const_iterator iter = CHAIN_SIGT.begin(); iter != CHAIN_SIGT.end(); iter++)
        Card += "MACGYRE CHAIN \"" + iter->first + "\" SIGT " + dtos(iter->second) + "\n";
      return Card;
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      std::string logsigr,logsigt;
      if (CHAIN_SIGR.size())
      {
        std::set<double> nsigr;
        for (map_str_float::const_iterator iter = CHAIN_SIGR.begin(); iter != CHAIN_SIGR.end(); iter++)
          nsigr.insert(iter->second);
        if (nsigr.size() > 1)
        {
          for (map_str_float::const_iterator iter = CHAIN_SIGR.begin(); iter != CHAIN_SIGR.end(); iter++)
            logsigr += std::string(iter->second?dtos(iter->second):"OFF") + "(" + iter->first + ") ";
        }
        else  //if there is only one value, report this
        {
          map_str_float::const_iterator iter = CHAIN_SIGR.begin();
          logsigr = std::string(iter->second?dtos(iter->second):"OFF");
        }
      }
      else
        logsigr = std::string(getNUM(macg_sigr)?dtos(getNUM(macg_sigr)):"OFF");
      if (CHAIN_SIGT.size())
      {
        std::set<double> nsigt;
        for (map_str_float::const_iterator iter = CHAIN_SIGT.begin(); iter != CHAIN_SIGT.end(); iter++)
          nsigt.insert(iter->second);
        if (nsigt.size() > 1)
        {
          for (map_str_float::const_iterator iter = CHAIN_SIGT.begin(); iter != CHAIN_SIGT.end(); iter++)
            logsigt += std::string(iter->second?dtos(iter->second):"OFF") + "(" + iter->first + ") ";
        }
        else  //if there is only one value, report this
        {
          map_str_float::const_iterator iter = CHAIN_SIGR.begin();
          logsigt = std::string(iter->second?dtos(iter->second):"OFF");
        }
      }
      else
        logsigt = std::string(getNUM(macg_sigt)?dtos(getNUM(macg_sigt)):"OFF");
      std::string anchorstr = std::string(ANCHOR ?"" : tab + "ANCHOR:      OFF\n");
      return tab + "Refinement protocol for this macgocycle:\n"
           + tab + "ROTATION:    " + std::string(FIX_ROT ? "FIX\n":"REFINE\n")
           + tab + "TRANSLATION: " + std::string(FIX_TRA ? "FIX\n":"REFINE\n")
           + tab + "VRMS:        " + std::string(FIX_VRMS ? "FIX\n":"REFINE\n")
           + anchorstr
           + tab + "SIGR:        " + logsigr + "\n"
           + tab + "SIGT:        " + logsigt + "\n";
    }

};

} //phaser

#endif
