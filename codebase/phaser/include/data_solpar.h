//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_SOLPAR_Class__
#define __DATA_SOLPAR_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/between.h>
#include <phaser_defaults.h>

namespace phaser {

class data_solpar
{
  public:
    floatType SIGA_FSOL,SIGA_BSOL,SIGA_MIN;
    floatType BULK_FSOL,BULK_BSOL;
    bool      BULK_USE;

    void set_data_solpar(const data_solpar & init)
    {
      SIGA_FSOL = init.SIGA_FSOL;
      SIGA_BSOL = init.SIGA_BSOL;
      SIGA_MIN  = init.SIGA_MIN;
      BULK_FSOL = init.BULK_FSOL;
      BULK_BSOL = init.BULK_BSOL;
      BULK_USE  = init.BULK_USE;
    }

    data_solpar(const data_solpar & init)
    { set_data_solpar(init); }

    const data_solpar& operator=(const data_solpar& right)
    { if (&right != this) set_data_solpar(right); return *this; }

    bool is_set()
    { return SIGA_FSOL; }

    data_solpar()
    {
      SIGA_FSOL = floatType(DEF_SOLP_SIGA_FSOL);
      SIGA_BSOL = floatType(DEF_SOLP_SIGA_BSOL);
      SIGA_MIN = floatType(DEF_SOLP_SIGA_MIN);
      BULK_FSOL = floatType(DEF_SOLP_BULK_FSOL);
      BULK_BSOL = floatType(DEF_SOLP_BULK_BSOL);
      BULK_USE = bool(DEF_SOLP_BULK_USE);
    }

    bool is_default() const
    {
      if (!(between(SIGA_FSOL,DEF_SOLP_SIGA_FSOL,1.0e-04))) return false;
      if (!(between(SIGA_BSOL,DEF_SOLP_SIGA_BSOL,1.0e-02))) return false;
      if (!(between(SIGA_MIN,DEF_SOLP_SIGA_MIN,1.0e-02))) return false;
      if (!(between(BULK_FSOL,DEF_SOLP_BULK_FSOL,1.0e-02))) return false;
      if (!(between(BULK_BSOL,DEF_SOLP_BULK_BSOL,1.0e-02))) return false;
      if (BULK_USE != DEF_SOLP_BULK_USE) return false;
      return true;
    }

    std::string unparse()
    {
      if (is_default()) return "";
      std::string Card;
      if (SIGA_FSOL)
      {
        Card += "SOLPAR SIGA FSOL " + dtos(SIGA_FSOL,3) + " BSOL " + dtos(SIGA_BSOL,3) + "\n";
      }
      if (BULK_USE)
      {
        Card += "SOLPAR BULK USE ON\n";
        Card += "SOLPAR BULK FSOL " + dtos(BULK_FSOL,3) + " BSOL " + dtos(BULK_BSOL,1) + "\n";
      }
      else Card += "SOLPAR BULK USE OFF\n";
      return Card;
    }

};

} //phaser

#endif
