//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MINIMIZER_CLASS__
#define __PHASER_MINIMIZER_CLASS__
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>

namespace phaser {

class data_minimizer
{
  public:
    data_minimizer(std::string MINIMIZER_="") : MINIMIZER(MINIMIZER_),IS_DEFAULT(false) { }

    bool set(std::string p)
    {
      p = stoup(p);
      if (p == "BFGS" ||
          p == "NEWTON" ||
          p == "DESCENT")
      {
        MINIMIZER = p;
        IS_DEFAULT = false;
        return false;
      }
      if (p == "") return false;
      return true; //error
    }

    bool set_default(std::string p)
    {
      p = stoup(p);
      if (p == "BFGS" ||
          p == "NEWTON" ||
          p == "DESCENT")
      {
        MINIMIZER = p;
        IS_DEFAULT = true;
        return false;
      }
      return true; //error
    }

  private:
    std::string MINIMIZER;
    bool IS_DEFAULT;

  public:
    bool is_default() const { return IS_DEFAULT; }

    std::string unparse() const
    {
      PHASER_ASSERT(MINIMIZER != "");
      return MINIMIZER;
    }
};

} //phaser
;
#endif
