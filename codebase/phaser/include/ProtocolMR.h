//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolMRClass__
#define __ProtocolMRClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum macm_flags { macm_rot, macm_tra, macm_bfac, macm_vrms, macm_cell, macm_ofac, macm_last, macm_sigr, macm_sigt };

class ProtocolMR : public ProtocolBase
{
  public:
    bool           FIX_ROT,FIX_TRA,FIX_BFAC,FIX_VRMS,FIX_CELL,FIX_OFAC,LAST_ONLY;
    double         SIGROT,SIGTRA;
    double         DEF_SIGROT,DEF_SIGTRA;
    floatType      NCYC;
    data_minimizer MINIMIZER;

    void init()
    {
      FIX_ROT  = !bool(DEF_MACM_REF_ROTA);
      FIX_TRA  = !bool(DEF_MACM_REF_TRAN);
      FIX_BFAC = !bool(DEF_MACM_REF_BFAC);
      FIX_VRMS = !bool(DEF_MACM_REF_VRMS);
      FIX_CELL = !bool(DEF_MACM_REF_CELL);
      FIX_OFAC = !bool(DEF_MACM_REF_OFAC);
      LAST_ONLY = bool(DEF_MACM_LAST_ONLY);
      NCYC = floatType(DEF_MACM_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACM_MINI));
      DEF_SIGROT  = double(DEF_MACM_SIGR);
      DEF_SIGTRA  = double(DEF_MACM_SIGT);
      SIGROT  = -999;
      SIGTRA  = -999;
    }

    bool is_default() const
    {
      return (
      FIX_ROT  == !bool(DEF_MACM_REF_ROTA) &&
      FIX_TRA  == !bool(DEF_MACM_REF_TRAN) &&
      FIX_BFAC == !bool(DEF_MACM_REF_BFAC) &&
      FIX_VRMS == !bool(DEF_MACM_REF_VRMS) &&
      FIX_CELL == !bool(DEF_MACM_REF_CELL) &&
      FIX_OFAC == !bool(DEF_MACM_REF_OFAC) &&
      LAST_ONLY == bool(DEF_MACM_LAST_ONLY) &&
      NCYC == floatType(DEF_MACM_NCYC) &&
      DEF_SIGROT == double(DEF_MACM_SIGR) &&
      DEF_SIGTRA == double(DEF_MACM_SIGT) &&
      SIGROT == -999 &&
      SIGTRA == -999 &&
      MINIMIZER.is_default()
      );
    }

    ProtocolMR()
    { init(); }

    ProtocolMR(bool fixr,bool fixt,bool fixb,bool fixe,bool fixc,bool fixo, bool last_only,
               int ncyc, std::string m, double rotref, double traref)
    {
      init();
      FIX_ROT = fixr;
      FIX_TRA = fixt;
      FIX_BFAC = fixb;
      FIX_VRMS = fixe;
      FIX_CELL = fixc;
      FIX_OFAC = fixo;
      LAST_ONLY = last_only;
      if (ncyc >= 0) NCYC = ncyc;
      MINIMIZER.set(m);
      SIGROT = rotref;
      SIGTRA = traref;
    }

  //-------------------------
  //concrete member functions
  //-------------------------
  //  void setDEFAULT() { DEFAULT = true; }
  //  bool is_default() const { return DEFAULT; }
    bool is_off() const
    { return ((FIX_ROT && FIX_TRA && FIX_BFAC && FIX_VRMS && FIX_CELL && FIX_OFAC) || NCYC == 0); }

    void off()
    { init(); FIX_ROT = FIX_TRA = FIX_BFAC = FIX_VRMS = FIX_CELL = FIX_OFAC = LAST_ONLY = true; NCYC = 0; }

    void all()
    { init(); FIX_ROT = FIX_TRA = FIX_BFAC = FIX_VRMS = FIX_CELL = FIX_OFAC = LAST_ONLY = false; }

    void single_atom()
    { init(); FIX_ROT = FIX_OFAC = true; FIX_TRA = FIX_BFAC = false; FIX_VRMS = true; LAST_ONLY = false; }


    void setFIX(int i,bool b)
    {
      if (i == macm_rot) FIX_ROT = b;
      else if (i == macm_tra) FIX_TRA = b;
      else if (i == macm_bfac) FIX_BFAC = b;
      else if (i == macm_vrms) FIX_VRMS = b;
      else if (i == macm_cell) FIX_CELL = b;
      else if (i == macm_ofac) FIX_OFAC = b;
      else if (i == macm_last) LAST_ONLY = b;
    }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineMR must
    //be changed as well - copy through virtual base class pointer
      if (i == macm_rot) return FIX_ROT;
      else if (i == macm_tra) return FIX_TRA;
      else if (i == macm_bfac) return FIX_BFAC;
      else if (i == macm_vrms) return FIX_VRMS;
      else if (i == macm_cell) return FIX_CELL;
      else if (i == macm_ofac) return FIX_OFAC;
      else if (i == macm_last) return LAST_ONLY;
      return false;
    }

    double getNUM(int i) const
    {
      if (i == macm_sigr) return SIGROT >=0 ? SIGROT : DEF_SIGROT;
      else if (i == macm_sigt) return SIGTRA >=0 ? SIGTRA : DEF_SIGTRA;
      else PHASER_ASSERT(false);
      return 0;
    }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    void setNCYC(int ncyc)
    { NCYC = ncyc; }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      return "MACMR ROT " +       std::string(FIX_ROT ? "OFF" : "ON ") +
                  " TRA " +       std::string(FIX_TRA ? "OFF" : "ON ") +
                  " BFAC " +      std::string(FIX_BFAC ? "OFF" : "ON ") +
                  " VRMS " +      std::string(FIX_VRMS ? "OFF" : "ON ") +
                  " CELL " +      std::string(FIX_CELL ? "OFF" : "ON ") +
                  " OFAC " +      std::string(FIX_OFAC ? "OFF" : "ON ") +
                  " LAST " +      std::string(LAST_ONLY ? "ON" : "OFF ") +
                  " SIGROT " +    dtos(getNUM(macm_sigr)) +
                  " SIGTRA " +    dtos(getNUM(macm_sigt)) +
                  " NCYCLE " +    itos(NCYC) +
                  " MINIMIZER " + MINIMIZER.unparse() + "\n";
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "ROTATION:         " + std::string(FIX_ROT ? "FIX\n":"REFINE\n")
           + tab + "TRANSLATION:      " + std::string(FIX_TRA ? "FIX\n":"REFINE\n")
           + tab + "BFACTOR:          " + std::string(FIX_BFAC ? "FIX\n":"REFINE\n")
           + tab + "MODEL VRMS:       " + std::string(FIX_VRMS ? "FIX\n":"REFINE\n")
           + tab + "CELL SCALE:       " + std::string(FIX_CELL ? "FIX\n":"REFINE\n")
           + tab + "OCCUPANCY FACTOR: " + std::string(FIX_OFAC ? "FIX\n":"REFINE\n")
           + tab + "LAST ONLY:        " + std::string(LAST_ONLY ? "TRUE\n":"FALSE\n")
           + tab + "ROTATION RESTRAINT:    " + std::string(getNUM(macm_sigr)?dtos(getNUM(macm_sigr)):"OFF") + "\n"
           + tab + "TRANSLATION RESTRAINT: " + std::string(getNUM(macm_sigt)?dtos(getNUM(macm_sigt)):"OFF") + "\n";
    }
};

} //phaser

#endif
