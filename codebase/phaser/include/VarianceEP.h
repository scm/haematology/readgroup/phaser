//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __VarianceEP_Class__
#define __VarianceEP_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/include/VarianceBase.h>
#include <cctbx/adptbx.h>

namespace phaser {

class VarianceEP : public VarianceBase
{
  public:
    floatType ScaleK,ScaleU,PartK,PartU;
    af_float  DphiA_bin,DphiB_bin,SP_bin,SDsqr_bin;

  public:
    VarianceEP()
    {
      DphiA_bin.clear(); DphiB_bin.clear(); SP_bin.clear(); SDsqr_bin.clear();
      ScaleK = PartK = 1; ScaleU = PartU = 0;
    }

    bool variance_is_set() const
    {
      return DphiA_bin.size() &&
             DphiB_bin.size() == DphiA_bin.size() &&
             SP_bin.size() == DphiA_bin.size() &&
             SDsqr_bin.size() == DphiA_bin.size();
    }

    bool setVarianceEP(const VarianceEP & init)
    {
      if (init.variance_is_set())
      {
        ScaleK = init.ScaleK;
        ScaleU = init.ScaleU;
        PartK = init.PartK;
        PartU = init.PartU;
        DphiA_bin = init.DphiA_bin.deep_copy();
        DphiB_bin = init.DphiB_bin.deep_copy();
        SP_bin = init.SP_bin.deep_copy();
        SDsqr_bin = init.SDsqr_bin.deep_copy();
        return true;
      }
      return false;
    }

    VarianceEP(const VarianceEP & init)
    { setVarianceEP(init); }

    const VarianceEP& operator=(const VarianceEP& right)
    {
      if (&right != this) setVarianceEP(right);
      return *this;
    }

    const VarianceEP& getVarianceEP() const
    { return *this; }

  //concrete
    std::string unparse() const
    {
      std::string Card("");
      if (variance_is_set())
      {
        Card += "VARS K " + dtos(ScaleK) + "\n";
        Card += "VARS B " + dtos(ScaleU) + "\n";
        Card += "VARS PK " + dtos(PartK) + "\n";
        Card += "VARS PB " + dtos(PartU) + "\n";
        Card += "VARS SA"; for (int i=0; i<DphiA_bin.size(); i++) Card += " " + dtos(DphiA_bin[i]);
        Card += "\n";
        Card += "VARS SB"; for (int i=0; i<DphiB_bin.size(); i++) Card += " " + dtos(DphiB_bin[i]);
        Card += "\n";
        Card += "VARS SP"; for (int i=0; i<SP_bin.size(); i++) Card += " " + dtos(SP_bin[i]);
        Card += "\n";
        Card += "VARS SD"; for (int i=0; i<SDsqr_bin.size(); i++) Card += " " + dtos(SDsqr_bin[i]);
        Card += "\n";
      }
      return Card;
    }

    af_float get_variance_array() const
    {
      af_float Array;
      Array.push_back(ScaleK);
      Array.push_back(ScaleU);
      Array.push_back(PartK);
      Array.push_back(PartU);
      for (int i = 0; i < DphiA_bin.size(); i++) Array.push_back(DphiA_bin[i]);
      for (int i = 0; i < DphiB_bin.size(); i++) Array.push_back(DphiB_bin[i]);
      for (int i = 0; i < SP_bin.size(); i++) Array.push_back(SP_bin[i]);
      for (int i = 0; i < SDsqr_bin.size(); i++) Array.push_back(SDsqr_bin[i]);
      return Array;
    }

    void set_variance_array(af_float Array)
    {
      if (Array.size() >= 8) // K,U,nbins=1 for DphiA_bin DphiB_bin SP_bin SDsqr_bin
      {
        int j(0);
        ScaleK = Array[j++];
        ScaleU = Array[j++];
        PartK = Array[j++];
        PartU = Array[j++];
        int nbins = (Array.size()-j)/4;
        assert(nbins*4+4 == Array.size());
        for (int i = 0; i < nbins; i++) DphiA_bin.push_back(Array[j++]);
        for (int i = 0; i < nbins; i++) DphiB_bin.push_back(Array[j++]);
        for (int i = 0; i < nbins; i++) SP_bin.push_back(Array[j++]);
        for (int i = 0; i < nbins; i++) SDsqr_bin.push_back(Array[j++]);
      }
    }

};

} //phaser

#endif
