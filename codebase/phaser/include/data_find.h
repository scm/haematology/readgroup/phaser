//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_FIND_CLASS__
#define __PHASER_DATA_FIND_CLASS__
#include <phaser_defaults.h>

namespace phaser {

class data_find
{
  public:
    bool        CLUSTER;
    std::string TYPE;
    int         NATOM;
    std::string TARGET;
    bool        SPECIAL_POSITION;
    double      OCCUPANCY;
    double      DIST_CENTRO;
    double      DIST_DUPL;

  data_find()
  {
    CLUSTER = bool(DEF_FIND_CLUS);
    TYPE    = std::string(DEF_FIND_SCAT);
    NATOM   = int(DEF_FIND_NUMB);
    TARGET  = std::string(DEF_FIND_TARG);
    SPECIAL_POSITION = bool(DEF_FIND_SPEC_POSI);
    OCCUPANCY = double(DEF_FIND_OCCU);
    DIST_CENTRO = double(DEF_FIND_DIST_CENT);
    DIST_DUPL = double(DEF_FIND_DIST_DUPL);
  }

  std::string unparse()
  {
    std::string Card("");
    if (NATOM && TYPE != "")
      Card += "FIND SCATTERER " + TYPE + "\n";
    if (NATOM != int(DEF_FIND_NUMB))
      Card += "FIND NUMBER " + itos(NATOM) + "\n";
    if (CLUSTER != bool(DEF_FIND_CLUS))
      Card += "FIND CLUSTER " + std::string(CLUSTER?"ON":"OFF") + "\n";
    if (TARGET != std::string(DEF_FIND_TARG))
      Card += "FIND TARGET " + TARGET + "\n";
    if (SPECIAL_POSITION != bool(DEF_FIND_SPEC_POSI))
      Card += "FIND SPECIAL POSITION " + std::string(SPECIAL_POSITION?"ON":"OFF") + "\n";
    if (OCCUPANCY != double(DEF_FIND_OCCU))
      Card += "FIND OCCUPANCY " + dtos(OCCUPANCY) + "\n";
    if (DIST_CENTRO != DEF_FIND_DIST_CENT)
      Card += "FIND DISTANCE CENTROSYMMETRIC " + dtos(DIST_CENTRO) + "\n";
    if (DIST_DUPL != DEF_FIND_DIST_DUPL)
      Card += "FIND DISTANCE DUPLICATE " + dtos(DIST_DUPL) + "\n";
    return Card;
  }
};

} //phaser

#endif
