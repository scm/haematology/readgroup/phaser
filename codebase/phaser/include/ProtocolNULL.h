//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolNULLClass__
#define __ProtocolNULLClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum macz_flags { macz_ccc };

class ProtocolNULL : public ProtocolBase
{
  public:
    bool            FIX_CCC;
    double          NCYC;
    data_minimizer  MINIMIZER;

    ProtocolNULL()
    {
      FIX_CCC = false;
      NCYC = 50;
      MINIMIZER.set_default("BFGS");
    }

    bool is_default() const
    {
      return (
      !FIX_OCC &&
      NCYC == 50 &&
      MINIMIZER.is_default()
      );
    }

    bool is_off() const { return (FIX_CCC || NCYC == 0); }

    bool getFIX(int i) const
    {
      if (i == macz_ccc) return FIX_CCC;
      return false;
    }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      return "MACZERO CCC " + std::string(FIX_CCC ? "OFF" : "ON ") +
               " NCYCLE " + itos(NCYC) +
               " MINIMIZER " + MINIMIZER.unparse() + "\n";
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "CCC: " + std::string(FIX_CCC ? "FIX\n":"REFINE\n");
    }
};

} //phaser

#endif
