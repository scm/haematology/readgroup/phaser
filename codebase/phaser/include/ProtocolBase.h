//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolBaseClass__
#define __ProtocolBaseClass__
#include <phaser/main/Phaser.h>

namespace phaser {

//abstract - reminder! not objects of an abstract
//base class can be instantiated
class ProtocolBase  //abstract
{
  public:
    ProtocolBase() {}
    virtual ~ProtocolBase() {}

//these functions are called either in callRefine or in setProtocol
//in the base object
    virtual bool           is_off() const;
    virtual void           setFIX(int,bool);
    virtual bool           getFIX(int) const;
    virtual std::string    getMINIMIZER() const;
    virtual unsigned       getNCYC() const;
    virtual void           setNCYC(int);
    virtual void           setNUM(int,double);
    virtual void           setMAP(int,map_str_float);
    virtual std::string    getTXT(int) const;
    virtual floatType      getNUM(int) const;
    virtual map_str_float  getMAP(int) const;
    virtual std::string    unparse() const;
    virtual std::string    logfile() const;
    virtual bool           is_default() const;
};

inline bool           ProtocolBase::is_off() const { return false; }
inline void           ProtocolBase::setFIX(int,bool) { return; }
inline bool           ProtocolBase::getFIX(int) const { return false; }
inline std::string    ProtocolBase::getMINIMIZER() const { return ""; }
inline unsigned       ProtocolBase::getNCYC() const { return 0; }
inline void           ProtocolBase::setNCYC(int i) { return; }
inline void           ProtocolBase::setNUM(int i,double d) { return; }
inline void           ProtocolBase::setMAP(int i,map_str_float d) { return; }
inline std::string    ProtocolBase::getTXT(int i) const { return ""; }
inline floatType      ProtocolBase::getNUM(int i) const { return 0; }
inline map_str_float  ProtocolBase::getMAP(int i) const { return map_str_float(); }
inline std::string    ProtocolBase::unparse() const { return ""; }
inline std::string    ProtocolBase::logfile() const { return ""; }
inline bool           ProtocolBase::is_default() const { return false; }

typedef boost::shared_ptr<ProtocolBase> protocolPtr;

} //phaser

#endif
