//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_FILETYPE_Class__
#define __DATA_FILETYPE_Class__

namespace phaser {

class data_filetype
{
  private:
    std::string PURPOSE;
    std::string FORMAT;
    bool        STRING;
    bool        HETATM;

  public:
    data_filetype()
    { PURPOSE = FORMAT == ""; STRING = HETATM = false; }

    bool is_set() { return PURPOSE != "" && FORMAT != ""; }

    bool set_purpose(std::string p)
    { PURPOSE = p; return (PURPOSE == "MR" || PURPOSE == "EP"); }

    bool set_format(std::string f)
    { FORMAT = f; return (FORMAT == "PDB" || FORMAT == "CIF" || FORMAT == "HA" || FORMAT == "MAP"); }

    void set_string(bool b) { STRING = b; }
    void set_hetatm(bool b) { HETATM = b; }

    bool is_string()  { return STRING; }
    bool use_hetatm() { return HETATM; }

    bool pdb_format() { return FORMAT == "PDB"; }
    bool ha_format()  { return FORMAT == "HA"; }
    bool cif_format() { return FORMAT == "CIF"; }
    bool map_format() { return FORMAT == "MAP"; }

    bool for_ep() { return PURPOSE == "EP"; }
    bool for_mr() { return PURPOSE == "MR"; }
};

} //phaser

#endif
