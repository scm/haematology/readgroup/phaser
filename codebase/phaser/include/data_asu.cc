//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/lib/fasta2str.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/jiffy.h>
#include <phaser/include/data_asu.h>
#include <boost/assign.hpp>

namespace phaser {

std::string data_asu::unparse()
{
  std::string Card("");
  if (TYPE == "PROTEIN" || TYPE == "NUCLEIC")
  {
    if (MW)              Card += TYPE +  " MW " + dtos(MW) + " NUM " + itos(NUM);
    else if (NRES)       Card += TYPE +  " NRES " + itos(NRES) + " NUM " + itos(NUM);
    else if (SEQ.size()) Card += TYPE +  " SEQ \"" + std::string(SEQ) + "\" NUM " + itos(NUM);
    else if (STR.size()) Card += TYPE +  " STR " + std::string(STR) + " NUM " + itos(NUM);
  }
  else if (TYPE == "ATOM")
  {
    Card += TYPE + " " + std::string(STR) + " NUM " + itos(NUM);
  }
  return Card;
}

bool data_asu::check_seq()
{
  if (SEQ.size())
  {
    bool open_error(false);
    STR = fasta2str(SEQ,open_error);
  }
  std::list<char> residues;
  //fill list with appropriate residues one letter codes
  if (TYPE == "PROTEIN")
  {
    residues = boost::assign::list_of
    ('A')('C')('D')('E')('F')('G')('H')('I')('K')('L')
    ('M')('N')('P')('Q')('R')('S')('T')('V')('W')('Y')
    ('*').convert_to_container<std::list<char> >();
  }
  else if (TYPE == "NUCLEIC")
  {
    residues = boost::assign::list_of
    ('A')('T')('G')('C')('U')('*').convert_to_container<std::list<char> >();
  }
  if (TYPE == "PROTEIN" || TYPE == "NUCLEIC")
  {
    for (int i = 0; i < STR.size(); i++)
      if ((std::find(residues.begin(),residues.end(),STR[i]) == residues.end())  && !(isspace)(STR[i]))
        return true; //if it doesn't recognise a residue it returns error
  }
  return false;
}

double data_asu::scattering()
{
  if (TYPE == "PROTEIN")
  {
    scattering_protein protein;
    if (MW)              return protein.ScatMw(MW).scat*NUM;
    else if (NRES)       return protein.ScatNres(NRES).scat*NUM;
    else if (SEQ.size()) return protein.ScatSeqFile(SEQ).scat*NUM;
    else if (STR.size()) return protein.ScatSeq(STR).scat*NUM;
  }
  else if (TYPE == "NUCLEIC")
  {
    scattering_nucleic nucleic;
    if (MW)              return nucleic.ScatMw(MW).scat*NUM;
    else if (NRES)       return nucleic.ScatNres(NRES).scat*NUM;
    else if (SEQ.size()) return nucleic.ScatSeqFile(SEQ).scat*NUM;
    else if (STR.size()) return nucleic.ScatSeq(STR).scat*NUM;
  }
  else if (TYPE == "ATOM")
  {
    cctbx::eltbx::tiny_pse::table cctbxAtom(STR,true); //STR stores atomtype
    return fn::pow2(cctbxAtom.atomic_number())*NUM;
  }
  return 0;
}

double data_asu::mw()
{
  if (TYPE == "PROTEIN")
  {
    scattering_protein protein;
    if (MW)              return MW*NUM;
    else if (NRES)       return protein.ScatNres(NRES).mw*NUM;
    else if (SEQ.size()) return protein.ScatSeqFile(SEQ).mw*NUM;
    else if (STR.size()) return protein.ScatSeq(STR).mw*NUM;
  }
  else if (TYPE == "NUCLEIC")
  {
    scattering_nucleic nucleic;
    if (MW)              return MW*NUM;
    else if (NRES)       return nucleic.ScatNres(NRES).mw*NUM;
    else if (SEQ.size()) return nucleic.ScatSeqFile(SEQ).mw*NUM;
    else if (STR.size()) return nucleic.ScatSeq(STR).mw*NUM;
  }
  else if (TYPE == "ATOM")
  {
    cctbx::eltbx::tiny_pse::table cctbxAtom(STR,true); //STR stores atomtype
    return cctbxAtom.weight()*NUM;
  }
  return 0;
}

bool data_asu::is_type(std::string sel)
{
  if (sel == "ATOM" || sel == "PROTEIN" || sel == "NUCLEIC") return TYPE == sel;
  if (sel == "MIXED") return ( TYPE == "PROTEIN" || TYPE == "NUCLEIC");
  return false;
}

} //phaser
