//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_FORMFACTOR_CLASS__
#define __PHASER_FORMFACTOR_CLASS__
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/eltbx/electron_scattering.h>
#include <cctbx/eltbx/neutron.h>

namespace phaser {

enum formfactor_type {ff_xray, ff_electron, ff_neutron};

class data_formfactors
{
  private:
    formfactor_type ff;

  public:
    data_formfactors(formfactor_type ff_=ff_xray) : ff(ff_) {}

  const cctbx::eltbx::xray_scattering::gaussian fetch(std::string element)
  {
    if (ff == ff_xray)
      return cctbx::eltbx::xray_scattering::wk1995(element).fetch();
    if (ff == ff_electron)
      return cctbx::eltbx::electron_scattering::peng1996(element).fetch();
    if (ff == ff_neutron)
      return cctbx::eltbx::neutron::neutron_news_1992_table(element).fetch();
    return cctbx::eltbx::xray_scattering::wk1995(element).fetch(); //default, xray
  }

  std::string label(std::string element)
  {
    if (ff == ff_xray)
      return cctbx::eltbx::xray_scattering::wk1995(element).label();
    if (ff == ff_electron)
      return cctbx::eltbx::electron_scattering::peng1996(element).label();
    if (ff == ff_neutron)
      return cctbx::eltbx::neutron::neutron_news_1992_table(element).label();
    return cctbx::eltbx::xray_scattering::wk1995(element).label(); //default, xray
  }

  std::string unparse()
  {
    if (ff == ff_electron) return "FORMFACTORS ELECTRON\n";
    if (ff == ff_neutron)  return "FORMFACTORS NEUTRON\n";
    return ""; //default, xray
  }

  std::string TABLE()
  {
    if (ff == ff_xray) return "WK1995";
    if (ff == ff_electron) return "PENG1996";
    if (ff == ff_neutron) return "NEUTRON1992";
    return "WK1995"; //default, xray
  }

};

}
#endif
