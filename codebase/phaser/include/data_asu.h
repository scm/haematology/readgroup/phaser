//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_COMP_Class__
#define __DATA_COMP_Class__

namespace phaser {

//multipurpose container for asu composition
class data_asu
{
  public:
    data_asu(std::string TYPE_="PROTEIN",int NUM_=0) : TYPE(TYPE_),NUM(NUM_)
     { MW = NRES = 0; SEQ = STR = ""; }

    double      MW;
    int         NRES;
    std::string SEQ,STR;
    std::string TYPE;
    int         NUM;

  public:
    std::string unparse();
    bool        check_seq();
    double      scattering();
    double      mw();
    bool        is_type(std::string);
};

} //phaser

#endif
