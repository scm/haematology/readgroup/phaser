//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_ELNEMO_Class__
#define __DATA_ELNEMO_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

struct data_elnemo
{
  double      RADIUS;
  double      FORCE;
  double      MAXBLOCKS;
  int         NRES;
  std::string METHOD;
};

}//end namespace phaser

#endif
