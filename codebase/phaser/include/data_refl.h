//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_REFL_Class__
#define __DATA_REFL_Class__
#include <phaser_defaults.h>
#include <phaser/include/data_spots.h>
#include <phaser/include/data_ftfmap.h>
#include <cctbx/miller/asu.h>

namespace phaser {

class Output;

class data_refl : public data_spots
{
  public:
    af::shared<miller::index<int> > MILLER;
    data_ftfmap FTFMAP;
    std::string SG_HALL;
    af::double6 UNIT_CELL;
    std::string SGALT_BASE;

  public:
    data_refl() : data_spots()
    {
      SG_HALL = SGALT_BASE = "";
      UNIT_CELL = DEF_CELL;
      MILLER.clear();
      FTFMAP = data_ftfmap();
    }
    data_refl(af::shared<miller::index<int> > m,data_spots s,std::string hall,af::double6 cell) : data_spots(s)
    {
      MILLER = m.deep_copy();
      SG_HALL = hall;
      SGALT_BASE = "";
      UNIT_CELL = cell;
      FTFMAP = data_ftfmap();
    }
    data_refl(af::shared<miller::index<int> > m,af_float f,af_float s) : data_spots(f,s)
    {
      MILLER = m.deep_copy();
      SG_HALL = SGALT_BASE = "";
      UNIT_CELL = DEF_CELL;
      FTFMAP = data_ftfmap();
    }
    data_refl(af::shared<miller::index<int> > m,af_float f,af_float s,std::string hall) : data_spots(f,s)
    {
      MILLER = m.deep_copy();
      SG_HALL = hall;
      SGALT_BASE = "";
      UNIT_CELL = DEF_CELL;
      FTFMAP = data_ftfmap();
    }
    data_refl(af::shared<miller::index<int> > m,af_float f,af_float s,std::string hall,af::double6 cell) : data_spots(f,s)
    {
      MILLER = m.deep_copy();
      SG_HALL = hall;
      SGALT_BASE = "";
      UNIT_CELL = cell;
      FTFMAP = data_ftfmap();
    }

    data_refl(const data_refl & init)
    { set_data_refl(init); }
    const data_refl& operator=(const data_refl& right)
    { if (&right != this) set_data_refl(right); return *this; }
    void set_data_refl(const data_refl& init)
    {
      // deep copy of af::shared for threads
      set_data_spots(init);
      FTFMAP = init.FTFMAP;
      MILLER = init.MILLER.deep_copy();
      SG_HALL = init.SG_HALL;
      UNIT_CELL = init.UNIT_CELL;
      SGALT_BASE = init.SGALT_BASE;
    }

  void set_default_dfactor()
  { data_spots::set_default_dfactor(MILLER.size()); }

  void erase(int r)
  {
    if (r<MILLER.size()) MILLER.erase(MILLER.begin()+r);
    data_spots::erase(r);
    FTFMAP.erase(r);
  }

  void expand_to_p1(cctbx::sgtbx::space_group sg,bool=false);
  void change_basis(cctbx::sgtbx::change_of_basis_op);
  void reduce(cctbx::sgtbx::space_group sg,bool=false);
  bool change_space_group(std::string,Output&);
  std::string unparse(std::string);

};

} //phaser

#endif
