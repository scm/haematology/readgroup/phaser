//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DATA_FFTS_CLASS__
#define __PHASER_DATA_FFTS_CLASS__
#include <phaser_defaults.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class data_ffts
{
  public:
    int  MIN,MAX;

  data_ffts()
  {
    MIN = int(DEF_FFTS_MIN);
    MAX = int(DEF_FFTS_MAX);
  }

  std::string unparse()
  {
    std::string Card;
    if (MIN != int(DEF_FFTS_MIN) || MAX != int(DEF_FFTS_MAX))
      Card += "FFTS MIN " + itos(MIN) + " MAX " + itos(MAX) + "\n";
    return Card;
  }
};

} //phaser

#endif
