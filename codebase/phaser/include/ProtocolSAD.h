//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolSADClass__
#define __ProtocolSADClass__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum macs_flags {
macs_k, macs_b, macs_xyz, macs_occ, macs_bfac, macs_fdp, macs_sa, macs_sb, macs_sp, macs_sd,
macs_partk, macs_partu
};

class ProtocolSAD : public ProtocolBase
{
  public:
    bool FIX_K,FIX_B;
    bool FIX_XYZ,FIX_OCC,FIX_BFAC,FIX_FDP;
    bool FIX_SA,FIX_SB,FIX_SP,FIX_SD;
    bool FIX_PARTK,FIX_PARTU;
    int  NCYC;
    data_minimizer  MINIMIZER;
    std::string TARGET;

    void init()
    {
      FIX_K = FIX_B = true;
      FIX_SA = FIX_SB = true;
      FIX_SP = FIX_SD = false;
      FIX_XYZ = FIX_OCC = FIX_BFAC = false;
      FIX_FDP = true;
      FIX_PARTK = FIX_PARTU = false;
      NCYC = floatType(DEF_MACS_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACS_MINI));
      TARGET = std::string(DEF_MACS_TARG);
    }

    bool is_default() const
    {
      return (
      FIX_K && FIX_B &&
      FIX_SA && FIX_SB &&
      !FIX_SP && !FIX_SD &&
      !FIX_XYZ && !FIX_OCC && !FIX_BFAC &&
      FIX_FDP &&
      !FIX_PARTK && !FIX_PARTU &&
      NCYC == floatType(DEF_MACS_NCYC) &&
      TARGET == std::string(DEF_MACS_TARG) &&
      MINIMIZER.is_default()
      );
    }

    ProtocolSAD()
    { init(); }

    ProtocolSAD(
                bool fix_xyz,bool fix_occ,bool fix_bfac,bool fix_fdp,
                bool fix_sa,bool fix_sb,bool fix_sp,bool fix_sd,
                bool fix_partk,bool fix_partu,
                int ncyc,std::string target,std::string m)
    {
      init();
      FIX_XYZ = fix_xyz;
      FIX_OCC = fix_occ;
      FIX_BFAC = fix_bfac;
      FIX_FDP = fix_fdp;
      FIX_SA = fix_sa;
      FIX_SB = fix_sb;
      FIX_SP = fix_sp;
      FIX_SD = fix_sd;
      FIX_PARTK = fix_partk;
      FIX_PARTU = fix_partu;
      if (ncyc >= 0) NCYC = ncyc;
      target = stoup(target);
      if (target == "ANOM_ONLY" || target == "NOT_ANOM_ONLY")
        TARGET = target;
      MINIMIZER.set(m);
    }

    void occ()
    {
      init();
      FIX_OCC = FIX_PARTK = false;
      FIX_PARTU = FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_SA = FIX_SB = FIX_SP = FIX_SD = true;
    }

    void null()
    {
      init();
      FIX_SA = FIX_SB = FIX_SP = FIX_SD = false;
      FIX_OCC = FIX_PARTK = FIX_PARTU = FIX_XYZ = FIX_BFAC = FIX_FDP = true;
    }

    void occ_fdp()
    {
      init();
      FIX_OCC = FIX_FDP = FIX_PARTK = false;
      FIX_XYZ = FIX_BFAC = FIX_SA = FIX_SB = FIX_SP = FIX_SD = FIX_PARTU = true;
    }

    void xyz_occ_bfac_sd_sp(bool fix_sp = false)
    {
      init();
      FIX_XYZ = FIX_OCC = FIX_BFAC = FIX_SD = FIX_PARTK = FIX_PARTU = false;
      FIX_SP = fix_sp; //for NAT protocol (true)
      FIX_SB = FIX_FDP = true;
    }

    void xyz_occ_bfac_fdp_sd_sp(bool fix_sp = false) //this is the default protocol
    {
      init();
      FIX_XYZ = FIX_OCC = FIX_BFAC = FIX_FDP = FIX_SD = FIX_PARTK = FIX_PARTU = false;
      FIX_SP = fix_sp; //for NAT protocol (true)
      FIX_SB = true;
    }

    void off()
    {
      FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_OCC = FIX_SA = FIX_SB = FIX_SP = FIX_SD = FIX_PARTK = FIX_PARTU = true;
      NCYC = 0;
    }

    void all()
    {
      init();
      FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_OCC = FIX_SA = FIX_SB = FIX_SP = FIX_SD = FIX_PARTK = FIX_PARTU = false;
    }

    void var_only()
    {
      FIX_XYZ = FIX_BFAC = FIX_FDP = FIX_OCC = FIX_PARTK = FIX_PARTU = true;
      FIX_SA = FIX_SB = FIX_SP = FIX_SD = false;
    }

    //direct setting of refinement for phenix.refine
    void setRefineSigmaA(bool fix_sa,bool fix_sb,bool fix_sp,bool fix_sd,bool fix_k, bool fix_b, bool fix_pk, bool fix_pb)
    {
      init();
      FIX_XYZ = FIX_OCC = FIX_BFAC = FIX_FDP = true;
      FIX_SA = fix_sa;
      FIX_SB = fix_sb;
      FIX_SP = fix_sp;
      FIX_SD = fix_sd;
      FIX_K =  fix_k;
      FIX_B =  fix_b;
      FIX_PARTK = fix_pk;
      FIX_PARTU = fix_pb;
    }

    //direct setting of refinement for phenix.refine
    void setNCYC(int ncyc)
    { NCYC = ncyc; }

  //-------------------------
  //concrete member functions
  //-------------------------
    bool is_off() const { return ((FIX_XYZ && FIX_BFAC && FIX_FDP && FIX_OCC && FIX_SA && FIX_SB && FIX_SP && FIX_SD && FIX_PARTK && FIX_PARTU) || NCYC == 0); }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineSADi must
    //be changed as well - copy through virtual base class pointer
      if (i == macs_k) return FIX_K;
      else if (i == macs_b) return FIX_B;
      else if (i == macs_xyz) return FIX_XYZ;
      else if (i == macs_occ) return FIX_OCC;
      else if (i == macs_bfac) return FIX_BFAC;
      else if (i == macs_fdp) return FIX_FDP;
      else if (i == macs_sa) return FIX_SA;
      else if (i == macs_sb) return FIX_SB;
      else if (i == macs_sp) return FIX_SP;
      else if (i == macs_sd) return FIX_SD;
      else if (i == macs_partk) return FIX_PARTK;
      else if (i == macs_partu) return FIX_PARTU;
      return false;
    }

    unsigned getNCYC() const
    { return NCYC; }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    std::string getTXT(int i=0) const
    { return TARGET; }

    std::string unparse() const
    {
      if (!FIX_XYZ && !FIX_OCC && !FIX_BFAC && !FIX_FDP && !FIX_SA && !FIX_SB && !FIX_SP && !FIX_SD && !FIX_PARTK && !FIX_PARTU) return "MACSAD ALL"; //so that checking of all flags is not set in finite difference debugging
      std::string Card("");
      Card += "MACSAD";
      Card += " XYZ " +       std::string(FIX_XYZ ? "OFF" : "ON ");
      Card += " OCC " +       std::string(FIX_OCC ? "OFF" : "ON ");
      Card += " BFAC " +      std::string(FIX_BFAC ? "OFF" : "ON ");
      Card += " FDP " +       std::string(FIX_FDP ? "OFF" : "ON ");
      Card += " SA " +        std::string(FIX_SA ? "OFF" : "ON ");
      Card += " SB " +        std::string(FIX_SB ? "OFF" : "ON ");
      Card += " SP " +        std::string(FIX_SP ? "OFF" : "ON ");
      Card += " SD " +        std::string(FIX_SD ? "OFF" : "ON ");
      Card += " PK " +        std::string(FIX_PARTK ? "OFF" : "ON ");
      Card += " PB " +        std::string(FIX_PARTU ? "OFF" : "ON ");
      Card += " NCYCle " +    itos(NCYC);
      Card += " MINImizer " + MINIMIZER.unparse();
      Card += " TARGet " +    TARGET;
      Card += "\n";
      return Card;
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      std::string extra;
      if (!FIX_K || !FIX_B) extra = tab + "SCALE:   "
           + "  K:  " + std::string(FIX_K ? "FIX   ":"REFINE")
           + "  B:  " + std::string(FIX_B ? "FIX   ":"REFINE") + "\n";
      return tab + "Refinement protocol for this macrocycle:\n"
           + extra
           + tab + "VARIANCE:"
           + "  SA: " + std::string(FIX_SA ? "FIX   ":"REFINE")
           + "  SB: " + std::string(FIX_SB ? "FIX   ":"REFINE")
           + "  SP: " + std::string(FIX_SP ? "FIX   ":"REFINE")
           + "  SD: " + std::string(FIX_SD ? "FIX   ":"REFINE") + "\n"
           + tab + "PARTIAL: "
           + "  K:  " + std::string(FIX_PARTK ? "FIX   ":"REFINE")
           + "  B:  " + std::string(FIX_PARTU ? "FIX   ":"REFINE") + "\n"
           + tab + "ATOMIC:  "
           + "  XYZ:" + std::string(FIX_XYZ ? "FIX   ":"REFINE")
           + "  OCC:" + std::string(FIX_OCC ? "FIX   ":"REFINE")
           + "  B:  " + std::string(FIX_BFAC ? "FIX   ":"REFINE")
           + "  FDP:" + std::string(FIX_FDP ? "FIX   ":"REFINE") + "\n";
    }
};

} //phaser

#endif
