//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolANOClass__
#define __ProtocolANOClass__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum maca_flags {
maca_bins, maca_aniso, maca_solk, maca_solb
};

class ProtocolANO : public ProtocolBase
{
  public:
    bool        FIX_BINS,FIX_ANISO,FIX_SOLK,FIX_SOLB;
    floatType   NCYC;
    data_minimizer   MINIMIZER;

    void init()
    {
      FIX_ANISO = !bool(DEF_MACA_REF_ANIS);
      FIX_BINS = !bool(DEF_MACA_REF_BINS);
      FIX_SOLK = !bool(DEF_MACA_REF_SOLK);
      FIX_SOLB = !bool(DEF_MACA_REF_SOLB);
      NCYC = floatType(DEF_MACA_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACA_MINI));
    }

    bool is_default() const
    {
      return (
      FIX_ANISO == !bool(DEF_MACA_REF_ANIS) &&
      FIX_BINS == !bool(DEF_MACA_REF_BINS) &&
      FIX_SOLK == !bool(DEF_MACA_REF_SOLK) &&
      FIX_SOLB == !bool(DEF_MACA_REF_SOLB) &&
      NCYC == floatType(DEF_MACA_NCYC) &&
      MINIMIZER.is_default()
      );
    }

    ProtocolANO()
    { init(); }

    ProtocolANO(bool fix_aniso,bool fix_bins,bool fix_solk,bool fix_solb,int ncyc,std::string m)
    {
      init();
      FIX_ANISO = fix_aniso;
      FIX_BINS = fix_bins;
      FIX_SOLK = fix_solk;
      FIX_SOLB = fix_solb;
      if (ncyc >= 0) NCYC = ncyc;
      MINIMIZER.set(m);
    }

    void off()
    {
      init();
      FIX_BINS = FIX_ANISO = FIX_SOLK = FIX_SOLB = true; NCYC = 0;
    }

    void all()
    {
      init();
      FIX_BINS = FIX_ANISO = FIX_SOLK = FIX_SOLB = false; NCYC = 50;
    }
  //-------------------------
  //concrete member functions
  //-------------------------
    bool is_off() const { return ((FIX_BINS && FIX_ANISO && FIX_SOLK && FIX_SOLB) || NCYC == 0); }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineA must
    //be changed as well - copy through virtual base class pointer
      if (i == maca_bins) return FIX_BINS;
      else if (i == maca_aniso) return FIX_ANISO;
      else if (i == maca_solk) return FIX_SOLK;
      else if (i == maca_solb) return FIX_SOLB;
      return false;
    }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      return "MACANO ANISO " + std::string(FIX_ANISO ? "OFF" : "ON ") +
                   " BINS " + std::string(FIX_BINS ? "OFF" : "ON ") +
                   " SOLK " + std::string(FIX_SOLK ? "OFF" : "ON ") +
                   " SOLB " + std::string(FIX_SOLB ? "OFF" : "ON ") +
                   " NCYCLE " + itos(NCYC) +
                   " MINIMIZER " + MINIMIZER.unparse() + "\n";
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "BIN SCALES: " + std::string(FIX_BINS ? "FIX\n":"REFINE\n")
           + tab + "ANISOTROPY: " + std::string(FIX_ANISO ? "FIX\n":"REFINE\n")
           + tab + "SOLVENT K:  " + std::string(FIX_SOLK ? "FIX\n":"REFINE\n")
           + tab + "SOLVENT B:  " + std::string(FIX_SOLB ? "FIX\n":"REFINE\n");
    }
};

} //phaser

#endif
