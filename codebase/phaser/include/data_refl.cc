//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/lib/sg_prior.h>
#include <phaser/lib/jiffy.h>
#include <phaser/include/data_refl.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>

namespace phaser {

  void data_refl::expand_to_p1(cctbx::sgtbx::space_group sg,bool FriedelFlag)
  {
    data_spots::expand_to_p1(sg,MILLER,FriedelFlag);
    FTFMAP.expand_to_p1(sg,MILLER,FriedelFlag);
    //must be last!!! MILLER used unexpanded above
    MILLER = cctbx::miller::expand_to_p1_indices(sg,FriedelFlag,MILLER.const_ref());
  }

  void data_refl::change_basis(cctbx::sgtbx::change_of_basis_op cb_op)
  {
    MILLER = cb_op.apply(MILLER.const_ref());
  }

  void data_refl::reduce(cctbx::sgtbx::space_group sg,bool FriedelFlag)
  {
    //if (sg.type().hall_symbol() == "P 1") return;
    af::shared<std::size_t> unique = cctbx::miller::unique_under_symmetry_selection(sg.type(),FriedelFlag,MILLER.const_ref());
    data_spots::reduce(unique);
    FTFMAP.reduce(unique);
    af::shared<miller::index<int> > tmp;
    for (int i = 0; i < unique.size(); i++)
      tmp.push_back(MILLER[unique[i]]);
    MILLER = tmp;
  }

  bool data_refl::change_space_group(std::string NEW_HALL,Output& output)
  {
    bool reindexed = false;
    if (!NEW_HALL.size()) return reindexed;
    if (!MILLER.size()) return reindexed;
    cctbx::sgtbx::space_group newSG(NEW_HALL); //mtzHall
    cctbx::sgtbx::space_group datSG(SG_HALL); //mtzHall
    if (newSG == datSG)
    {
      output.logTab(1,SUMMARY,"No data expansion and/or reindexing required");
      output.logBlank(SUMMARY);
      return reindexed;
    }
    else
    {
      output.logUnderLine(SUMMARY,"Change Space Group");
      output.logTabPrintf(1,SUMMARY,"Data Space-Group Name (Hall Symbol): %s (%s)\n",SpaceGroup(datSG).spcgrpname().c_str(),SG_HALL.c_str());
      output.logTabPrintf(1,SUMMARY,"New Space-Group Name (Hall Symbol):  %s (%s)\n",SpaceGroup(newSG).spcgrpname().c_str(),NEW_HALL.c_str());
      output.logBlank(SUMMARY);
    }
    cctbx::sgtbx::change_of_basis_op cb_op_new = newSG.type().cb_op();
    cctbx::sgtbx::change_of_basis_op cb_op_dat = datSG.type().cb_op();
    cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
    bool match(false);
    std::vector<cctbx::sgtbx::space_group> sysabs = groups_sysabs(datSG);
    for (int s = 0; s < sysabs.size(); s++)
      if (newSG.type().hall_symbol() == sysabs[s].type().hall_symbol())
        match = true;
    //if there is a match in the pointgroup, change the spacegroup to the new spacegroup
    if (match && cb_op_dat2new.is_identity_op()) //and there is no reindexing required
    {
      SG_HALL = NEW_HALL;
      output.logTab(1,SUMMARY,"New Space-Group is in same Point-Group");
      output.logTab(1,SUMMARY,"No data expansion required");
      output.logBlank(SUMMARY);
      reindexed = false;
    }
    else
    {
      std::vector<cctbx::sgtbx::space_group> sglist = subgroups(datSG);
      if (sglist.size())
      {
        output.logUnderLine(LOGFILE,"Subgroups of space group");
        output.logTabPrintf(1,LOGFILE,"%-3s %-13s %c %-s\n","Z","Space Group",' ',"Hall Symbol");
        output.logTabPrintf(1,LOGFILE,"----\n");
        int last_order_z(0);
        int count_settings(0);
        bool match_settings(false);
        for (int s = 0; s < sglist.size(); s++)
        {
          std::string hall = sglist[s].type().hall_symbol();
          std::string hm = SpaceGroup(hall).spcgrpname();
          if (hm == "P 1 ") hm[0] = SpaceGroup(SG_HALL).getCctbxSG().conventional_centring_type_symbol();
          std::string base = hall.substr(0,hall.find(" (")); //space required for match (no space at end)
          std::string basenew = NEW_HALL.substr(0,NEW_HALL.find(" (")); //space required for match (no space at end)
          if (base == basenew)
            count_settings++;
          if (hall == NEW_HALL) //unforgiving test, includes settings if included
            match_settings = true;
          if (s && last_order_z != sglist[s].order_z()) output.logTabPrintf(1,LOGFILE,"---\n");
          output.logTabPrintf(1,LOGFILE,"%-3s %-13s %c%-s\n",
             (last_order_z != sglist[s].order_z()) ? itos(sglist[s].order_z()).c_str() : "",
             hm.c_str(),
            char(true?' ':'*'),hall.c_str());
          last_order_z = sglist[s].order_z();
          if (s == sglist.size()-1) output.logTabPrintf(1,LOGFILE,"----\n");
        }
        if (count_settings == 0)
          throw PhaserError(FATAL,"Space group is not a subgroup of data space group: Check setting");
        else if (count_settings > 1 && !match_settings)
          throw PhaserError(FATAL,"Space group has more than one setting and setting not defined");
        else //count_settings==1 or match_settings
        {
          output.logTab(1,SUMMARY,"Space group is a subgroup of data space group");
          output.logTab(1,SUMMARY,"Expand data to lower symmetry");
          expand_to_p1(datSG);
          reduce(newSG);
          SG_HALL = newSG.type().hall_symbol(); //reference setting
          SGALT_BASE = SG_HALL;
          if (!cb_op_dat2new.is_identity_op())
          {
            try
            { //this fails for e.g. some C2 settings of P3221 (beta-blip)
              output.logTab(1,SUMMARY,"Change of basis: " + cb_op_dat2new.as_hkl());
              change_basis(cb_op_dat2new);
              UNIT_CELL = cctbx::uctbx::unit_cell(UNIT_CELL).change_basis(cb_op_dat2new).parameters();
              output.logTab(1,SUMMARY,"Reindexed Unit Cell: " + dvtos(UNIT_CELL));
              // DO change newSG, need setting of input space group to be maintained
              SG_HALL = newSG.change_basis(cb_op_dat2new).type().hall_symbol();
              SGALT_BASE = SG_HALL;
              reindexed = true;
            }
            catch (...)
            { throw PhaserError(FATAL,"Space group not compatible with rotation"); }
          }
        }
      }
    }//in pointgroup or not
    output.logTab(1,SUMMARY,"Number of Reflections: " + itos(MILLER.size()));
    output.logBlank(SUMMARY);
    //PHASER_ASSERT(cctbx::sgtbx::space_group(SG_HALL).type().cb_op().is_identity_op());
    return reindexed;
  }

  std::string data_refl::unparse(std::string keyword)
  {
    std::string Card;
    if (data_spots::unparse().size()) Card += keyword + data_spots::unparse();
    if (FTFMAP.unparse().size())      Card += keyword + FTFMAP.unparse();
    return Card;
  }

} //phaser
