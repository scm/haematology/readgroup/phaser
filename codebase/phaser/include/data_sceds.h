//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DOMAIN_DATA_Class__
#define __DOMAIN_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_sceds
{
  public:
   data_sceds()
   { set_defaults(); }

   void set_defaults()
   {
     NDOM = int(DEF_SCED_NDOM);
     wEQUALITY = floatType(DEF_SCED_WEIG_EQUA);
     wSPHERICITY = floatType(DEF_SCED_WEIG_SPHE);
     wDENSITY = floatType(DEF_SCED_WEIG_DENS);
     wCONTINUITY = floatType(DEF_SCED_WEIG_CONT);
   }

   int         NDOM;
   floatType   wEQUALITY,wDENSITY,wSPHERICITY,wCONTINUITY;

   std::string unparse()
   {
     std::string Card;
     if (NDOM != int(DEF_SCED_NDOM))
     Card += "SCED NDOM " + itos(NDOM) + "\n";
     if (
       wEQUALITY != double(DEF_SCED_WEIG_EQUA) ||
       wSPHERICITY != double(DEF_SCED_WEIG_SPHE) ||
       wDENSITY != double(DEF_SCED_WEIG_DENS) ||
       wCONTINUITY != double(DEF_SCED_WEIG_CONT))
     Card += "SCED WEIGHT EQUALITY " + dtos(wEQUALITY) +
                      " SPHERICITY " + dtos(wSPHERICITY) +
                         " DENSITY " + dtos(wDENSITY) +
                      " CONTINUITY " + dtos(wCONTINUITY) + "\n";
     return Card;
   }
};

} //phaser

#endif
