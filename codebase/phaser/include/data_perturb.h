//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_PERTURB_Class__
#define __DATA_PERTURB_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_perturb
{
  public:
    data_perturb()
    {
      DIRECTION = std::string(DEF_PERT_RMS_DIRE);
      MAX = double(DEF_PERT_RMS_MAXI);
      STEP = double(DEF_PERT_RMS_STEP);
    }

    double      STEP;
    double      MAX;
    std::string DIRECTION;

    bool is_default() const
    {
      return ( DIRECTION == std::string(DEF_PERT_RMS_DIRE) &&
               MAX == double(DEF_PERT_RMS_MAXI) &&
               STEP == double(DEF_PERT_RMS_STEP));
    }

    std::string unparse()
    {
      std::string Card("");
      if (is_default()) return Card;
      Card += "PERTURB";
      Card += " RMS STEP " + dtos(STEP);
      Card += " RMS MAX " + dtos(MAX);
      Card += " RMS DIRECTION " + DIRECTION;
      Card += "\n";
      return Card;
    }
};

} //phaser

#endif
