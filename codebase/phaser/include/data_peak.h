//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_PEAK_Class__
#define __DATA_PEAK_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser_defaults.h>

namespace phaser {

class data_peak
{
  public:
    data_peak()
    {
      SELECT = "ALL";
      CUTOFF = default_cutoff();
      CLUSTER = false;
      DOWN = 0;
      LEVEL = DEF_LEVEL;
      SAFETY = 1;
    }

  private:
    std::string SELECT;

  public:
    double    CUTOFF;
    bool      CLUSTER;
    int       LEVEL;
    double    DOWN;
    double    SAFETY;
    const static int DEF_LEVEL = 1;

    std::string select() const { return SELECT; }
    bool is_set() const { return (SELECT != ""); }
    bool by_percent() const { return (SELECT == "PERCENT"); }
    bool by_sigma() const { return (SELECT == "SIGMA"); }
    bool by_number() const { return (SELECT == "NUMBER"); }
    bool by_all() const { return (SELECT == "ALL"); }

    void set_cutoff(double CUTOFF_) { CUTOFF = CUTOFF_; }
    //this contortion is to do with the phenix interface
    //where the defaults are included in the .eff file
    //the default is detected and recorded as a value 75 with a tolerance
    //so that if it is recorded in the .eff file and then used it works as expected
    //reset internally to exact value of 75
    double get_cutoff() {
                          double cutoff(is_default_cutoff() ? default_cutoff()-double(DEF_TOL) : CUTOFF);
                          if (by_percent()) cutoff = isperc(cutoff);
                          return SAFETY*cutoff;
                        }
    bool is_default_cutoff() { return CUTOFF == default_cutoff(); }
    virtual double default_cutoff() { return 0; } //virtual

    bool set_select(std::string p)
    {
      p = stoup(p);
      if (p == "PERCENT" ||
          p == "SIGMA" ||
          p == "NUMBER" ||
          p == "ALL")
      {
        SELECT = p;
        return false; //error
      }
      return true; //error
    }

    double isperc(double percent)
    { //same as in CCP4base
      assert(percent >= 0);
      if (percent > 1) percent /= 100;
      if (percent > 1 && percent < 1.01) percent = 1;
      assert(percent <= 1);
      return percent;
    }

  bool set_test(std::string test)
  {
    bool error = set_select(test);
    //set defaults
    if (by_percent()) CUTOFF = 0.75;
    if (by_sigma()) CUTOFF = 6;
    if (by_number()) CUTOFF = 1;
    return error;
  }
  double deep_cutoff()
  {
    return get_cutoff() - DOWN;
  }
  data_peak safety_margin()
  {
    data_peak tmp = *this;
    if (by_percent())
      tmp.SAFETY = 0.9;
    else if (by_sigma())
      tmp.SAFETY = 0.9;
    else if (by_number())
      tmp.SAFETY = 10;
    return tmp;
  }
  data_peak percent_to_all() //for FTF, mean not set, can't use percent
  {
    data_peak tmp = *this;
    if (by_percent()) tmp.set_select("ALL");
    return tmp;
  }
  data_peak purge_margin()
  {
    data_peak tmp = *this;
    if (by_number())
      SAFETY = 50;
    return tmp;
  }
  data_peak no_cluster()
  {
    data_peak tmp = *this;
    tmp.CLUSTER = false;
    return tmp;
  }
  std::string logfile()
  {
    if (by_percent()) return "Select by Percentage of Top value: " + dtos(get_cutoff()*100) + "%";
    if (by_sigma()) return "Select by Z-score: " + dtos(get_cutoff(),1,0);
    if (by_number()) return "Select by Number: " + itos(CUTOFF);
    if (by_all()) return "Select All";
    return "Select None";
  }
};

class data_peak_rot : public data_peak
{
  public:
    data_peak_rot()
    {
      CUTOFF = default_cutoff();
      set_select(DEF_PEAK_SELE_ROT);
      CLUSTER = bool(DEF_PEAK_CLUS_ROT);
      DOWN = isperc(DEF_PEAK_ROTA_DOWN);
    }
  double default_cutoff() { return double(DEF_PEAK_CUTO_ROT); }

  bool is_default_select()
  { return (select() == std::string(DEF_PEAK_SELE_ROT) &&
            is_default_cutoff() &&
            DOWN == isperc(DEF_PEAK_ROTA_DOWN)); }

  std::string unparse()
  {
    std::string Card;
    if (!is_default_select())
    {
      if (by_percent())
      {
        Card += "PEAKS ROT SELECT PERCENT CUTOFF " + dtos(get_cutoff()*100);
        if (DOWN != isperc(DEF_PEAK_ROTA_DOWN)) Card += " DOWN " + dtos(DOWN*100);
      }
      else if (by_sigma())
        Card += "PEAKS ROT SELECT SIGMA CUTOFF " + dtos(get_cutoff());
      else if (by_number())
        Card += "PEAKS ROT SELECT NUMBER CUTOFF " + itos(static_cast<int>(get_cutoff()));
      else if (by_all())
        Card += "PEAKS ROT SELECT ALL";
      Card += "\n";
    }
    if (CLUSTER != bool(DEF_PEAK_CLUS_ROT))
      Card += "PEAKS ROT CLUSTER " + std::string(CLUSTER ? "ON" : "OFF") + "\n";
    if (LEVEL != DEF_LEVEL)
      Card += "PEAKS ROT LEVEL " + itos(LEVEL) + "\n";
    return Card;
  }
};

class data_peak_tra : public data_peak
{
  public:
    data_peak_tra()
    {
      CUTOFF = default_cutoff();
      set_select(DEF_PEAK_SELE_TRA);
      CLUSTER = bool(DEF_PEAK_CLUS_TRA);
    }
  double default_cutoff() { return double(DEF_PEAK_CUTO_TRA); }

  bool is_default_select()
  { return (select() == std::string(DEF_PEAK_SELE_TRA) &&
            is_default_cutoff() &&
            DOWN == 0); }

  std::string unparse()
  {
    std::string Card;
    if (!is_default_select())
    {
      if (by_percent())
        Card += "PEAKS TRA SELECT PERCENT CUTOFF " + dtos(get_cutoff()*100);
      else if (by_sigma())
        Card += "PEAKS TRA SELECT SIGMA CUTOFF " + dtos(get_cutoff());
      else if (by_number())
        Card += "PEAKS TRA SELECT NUMBER CUTOFF " + itos(static_cast<int>(get_cutoff()));
      else if (by_all())
        Card += "PEAKS TRA SELECT ALL";
      Card += "\n";
    }
    if (CLUSTER != bool(DEF_PEAK_CLUS_TRA))
      Card += "PEAKS TRA CLUSTER " + std::string(CLUSTER ? "ON" : "OFF") + "\n";
    if (LEVEL != DEF_LEVEL)
      Card += "PEAKS TRA LEVEL " + itos(LEVEL) + "\n";
    return Card;
  }
};

class data_peak_find : public data_peak
{
  public:
    data_peak_find()
    {
      CUTOFF = default_cutoff();
      set_select(DEF_FIND_PEAK_SELE);
      CLUSTER = bool(DEF_FIND_PEAK_CLUS);
    }
  double default_cutoff() { return double(DEF_FIND_PEAK_CUTO); }

  bool is_default_select()
  { return (select() == std::string(DEF_FIND_PEAK_SELE) &&
            is_default_cutoff()); }


  std::string unparse()
  {
    std::string Card;
    if (!is_default_select())
    {
      if (by_percent())
        Card += "FIND PEAKS SELECT PERCENT CUTOFF " + dtos(get_cutoff()*100);
      if (by_sigma())
        Card += "FIND PEAKS SELECT SIGMA CUTOFF " + dtos(get_cutoff());
      else if (by_number())
        Card += "FIND PEAKS SELECT NUMBER CUTOFF " + itos(static_cast<int>(get_cutoff()));
      else if (by_all())
        Card += "FIND PEAKS SELECT ALL";
      Card += "\n";
    }
    if (CLUSTER != bool(DEF_FIND_PEAK_CLUS))
      Card += "FIND PEAKS CLUSTER " + std::string(CLUSTER ? "ON" : "OFF") + "\n";
    //no level
    return Card;
  }
};

class data_peak_purge : public data_peak
{
  public:
    data_peak_purge()
    {
      CUTOFF = default_cutoff();
      set_select(DEF_FIND_PURG_SELE);
      OCCUPANCY = double(DEF_FIND_PURG_OCCU);
    }
    double default_cutoff() { return double(DEF_FIND_PURG_CUTO); }

    bool set_select(std::string p)
    {
      p = stoup(p);
      if (//p == "PERCENT" ||
          p == "SIGMA" ||
          p == "NUMBER" ||
          p == "ALL")
      {
        data_peak::set_select(p); //recursive!
        return false; //error
      }
      return true; //error
    }

  double OCCUPANCY;

  bool is_default_select()
  { return (select() == std::string(DEF_FIND_PURG_SELE) &&
            is_default_cutoff()); }

  std::string unparse()
  {
    std::string Card;
    if (!is_default_select())
    {
      if (by_percent())
        Card += "FIND PURGE SELECT PERCENT CUTOFF " + dtos(get_cutoff()*100);
      if (by_sigma())
        Card += "FIND PURGE SELECT SIGMA CUTOFF " + dtos(get_cutoff());
      else if (by_number())
        Card += "FIND PURGE SELECT NUMBER CUTOFF " + itos(static_cast<int>(get_cutoff()));
      else if (by_all())
        Card += "FIND PURGE SELECT ALL";
      Card += "\n";
    }
    if (OCCUPANCY != double(DEF_FIND_PURG_OCCU))
      Card += "FIND PURGE OCCUPANCY " + dtos(OCCUPANCY) + "\n";
    return Card;
  }
};

} //phaser

#endif
