//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __OUTLIER_DATA_Class__
#define __OUTLIER_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>
#include <phaser/pod/probType.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>

namespace phaser {

class data_outl
{
  public:
    data_outl()
    {
      REJECT = bool(DEF_OUTL_REJE);
      PROB =   double(DEF_OUTL_PROB);
      INFO =   double(DEF_OUTL_INFO);
      SIGESQ_CENT = lookup_sigesq_cent(INFO);
      SIGESQ_ACENT = lookup_sigesq_acent(INFO);
      MAXPROB = 0.001;
      ANO.clear();
      SAD.clear();
    }
    data_outl(const data_outl & init)
    { set_data_outl(init); }
    const data_outl& operator=(const data_outl& right)
    { if (&right != this) set_data_outl(right); return *this; }
    void set_data_outl(const data_outl& init)
    {
      // deep copy of af::shared for threads
      REJECT = init.REJECT;
      PROB = init.PROB;
      INFO = init.INFO;
      MAXPROB = init.MAXPROB;
      ANO = init.ANO.deep_copy();
      SAD = init.SAD.deep_copy();
      SIGESQ_CENT = init.SIGESQ_CENT;
      SIGESQ_ACENT = init.SIGESQ_ACENT;
    }

  bool                  REJECT;
  double                PROB,MAXPROB,INFO,SIGESQ_CENT,SIGESQ_ACENT;
  af::shared<probTypeANO>  ANO;
  af::shared<probTypeSAD>  SAD;

  std::string unparse()
  {
    std::string Card("");
    if (REJECT && ((REJECT != bool(DEF_OUTL_REJE)) || (PROB != double(DEF_OUTL_PROB))))
      Card += "OUTLIER REJECT ON PROBABILITY " + dtos(PROB) + "\n";
    else if (!REJECT && (REJECT != bool(DEF_OUTL_REJE)))
      Card += "OUTLIER REJECT OFF\n";
    if (INFO != double(DEF_OUTL_INFO))
      Card += "OUTLIER INFORMATION " + dtos(INFO) + "\n";
    return Card;
  }

};

} //phaser

#endif
