//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_RRL_Class__
#define __DATA_RRL_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class data_rrl
{
  public:
    int       k,e;
    floatType LLG,ORIG_LLG;
    float1D   DROT,DTRA; //deltaROT,deltaTRA, distances of perturbation

    data_rrl()
    {
      k = e = LLG = ORIG_LLG = 0.;
      DROT = DTRA = float1D(0);
    }

    void setData(const data_rrl & init)
    {
      k = init.k;
      e = init.e;
      LLG = init.LLG;
      ORIG_LLG = init.ORIG_LLG;
      DROT = init.DROT;
      DTRA = init.DTRA;
    }

    data_rrl(const data_rrl & init)
    { setData(init); }

    const data_rrl& operator=(const data_rrl& right)
    {
      if (&right != this)
        setData(right);
      return *this;
    }

    bool operator<(const data_rrl &right) const
    { return LLG < right.LLG; }

};

} //phaser

#endif
