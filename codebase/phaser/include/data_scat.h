//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SCAT_WAVE_Class__
#define __SCAT_WAVE_Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_restraint.h>
#include <phaser/io/Errors.h>
#include <cctbx/eltbx/fp_fdp.h>
#include <phaser_defaults.h>

namespace phaser {

class data_scat
{
  public:
   data_scat()
   {
     FDP.RESTRAINT = bool(DEF_SCAT_REST);
     FDP.SIGMA = floatType(DEF_SCAT_SIGM);
     DEFAULT_FIX_ATOMTYPE = std::string(DEF_SCAT_FIX);
     ATOMTYPE.clear();
   }

  data_restraint FDP;
  std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPE;
  std::string DEFAULT_FIX_ATOMTYPE;

  private:
  std::map<std::string,std::string> FIX_ATOMTYPE;

  public:
  bool addFixFdp(std::string atomtype,std::string p)
  {
    atomtype = stoup(atomtype);
    p = stoup(p);
    if (p == "ON" ||
        p == "OFF" ||
        p == "EDGE")
    {
      FIX_ATOMTYPE[atomtype] = p;
      return false; //error
    }
    return true; //error
  }

  std::string getFixFdp(std::string atomtype)
  {
    atomtype = stoup(atomtype);
    if (FIX_ATOMTYPE.find(atomtype) == FIX_ATOMTYPE.end())
      return DEFAULT_FIX_ATOMTYPE;
    return FIX_ATOMTYPE[atomtype];
  }

};

} //phaser

#endif
