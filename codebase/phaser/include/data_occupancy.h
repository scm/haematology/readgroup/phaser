//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_OCCU_Class__
#define __DATA_OCCU_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_occupancy
{
  public:
    data_occupancy()
      {
        WINDOW_ELLG = double(DEF_OCCU_WIND_ELLG);
        WINDOW_NRES = int(DEF_OCCU_WIND_NRES);
        WINDOW_MAX = double(DEF_OCCU_WIND_MAX);
        MIN = double(DEF_OCCU_MIN);
        MAX = double(DEF_OCCU_MAX);
        MERGE = bool(DEF_OCCU_MERG);
        OFFSET = int(DEF_OCCU_OFFS);
        FRACSCAT = double(DEF_OCCU_FRAC);
        REFINE_ALL = true; //from MR_OCC mode
        BIAS = double(DEF_OCCU_BIAS);
      }

  int    WINDOW_NRES,OFFSET;
  double WINDOW_ELLG,WINDOW_MAX,MIN,MAX,FRACSCAT,BIAS;
  bool   MERGE;

  private:
  bool   REFINE_ALL;

  public:
  bool   refine_all()           { return REFINE_ALL; }
  void   set_refine_all(bool b) { REFINE_ALL = b; }

  std::string unparse()
  {
    std::string Card;
    if (MIN != double(DEF_OCCU_MIN) || MAX != double(DEF_OCCU_MAX))
    Card += "OCCUPANCY MIN " + dtos(MIN) + " MAX " + dtos(MAX) + "\n";
    if (WINDOW_ELLG != double(DEF_OCCU_WIND_ELLG))
    Card += "OCCUPANCY WINDOW ELLG " + dtos(WINDOW_ELLG) + "\n";
    if (WINDOW_MAX != double(DEF_OCCU_WIND_MAX))
    Card += "OCCUPANCY WINDOW MAX " + dtos(WINDOW_MAX) + "\n";
    if (WINDOW_NRES != int(DEF_OCCU_WIND_NRES))
    if (WINDOW_NRES) Card += "OCCUPANCY WINDOW NRES " + itos(WINDOW_NRES) + "\n";
    if (MERGE != bool(DEF_OCCU_MERG))
    Card += "OCCUPANCY MERGE " + std::string(MERGE ? "ON" : "OFF") + "\n";
    if (OFFSET != int(DEF_OCCU_OFFS))
    Card += "OCCUPANCY OFFSET " + itos(OFFSET) + "\n";
    if (FRACSCAT != double(DEF_OCCU_FRAC))
    Card += "OCCUPANCY FRAC " + dtos(FRACSCAT) + "\n";
    if (BIAS != double(DEF_OCCU_BIAS))
    Card += "OCCUPANCY BIAS " + dtos(BIAS) + "\n";
    return Card;
  }
};

} //phaser

#endif
