//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_PACKING_CLASS__
#define __PHASER_PACKING_CLASS__
#include <phaser_defaults.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class data_pack
{
  public:
    data_pack()
    {
      SELECT = std::string(DEF_PACK_SELE);
      CUTOFF = double(DEF_PACK_CUTO)/100.;
      QUICK = bool(DEF_PACK_QUIC);
      COMPACT = bool(DEF_PACK_COMP);
      KEEP_HIGH_TFZ = bool(DEF_PACK_KEEP_HIGH);
      PRUNE = false; //fast mode control
      //internal only
      FTF_LIMIT = -std::numeric_limits<floatType>::max(); // for ftf
      FTF_STOP = false; // for ftf
    }

    bool set_select(std::string p)
    {
      p = stoup(p);
      if (
          p == "PERCENT" ||
          p == "ALL")
      {
        SELECT = p;
        return false; //error
      }
      return true; //error
    }

  public:
    bool        QUICK,COMPACT,KEEP_HIGH_TFZ,PRUNE;
    double      CUTOFF;
    //internal control only
    double      FTF_LIMIT;
    bool        FTF_STOP;

  private:
    std::string SELECT;

  public:
    bool pairwise() { return (SELECT == "PERCENT"); }
    bool all() { return (SELECT == "ALL"); }

   bool is_default() const
   {
      return (
      SELECT == std::string(DEF_PACK_SELE) &&
      CUTOFF == double(DEF_PACK_CUTO) &&
      QUICK == bool(DEF_PACK_QUIC) &&
      COMPACT == bool(DEF_PACK_COMP) &&
      KEEP_HIGH_TFZ == bool(DEF_PACK_KEEP_HIGH)
      );
    }

    std::string unparse() const
    {
      if (is_default()) return "";
      std::string Card;
      if (SELECT != std::string(DEF_PACK_SELE))
      Card += "PACK SELECT " + SELECT + "\n";
      if (CUTOFF != double(DEF_PACK_CUTO)/100.)
      Card += "PACK CUTOFF " + dtos(CUTOFF) + "\n";
      if (QUICK != bool(DEF_PACK_QUIC))
      Card += "PACK QUICK " + std::string(QUICK ? "ON":"OFF") + "\n";
      if (COMPACT != bool(DEF_PACK_COMP))
      Card += "PACK COMPACT " + std::string(COMPACT ? "ON":"OFF") + "\n";
      if (KEEP_HIGH_TFZ != bool(DEF_PACK_KEEP_HIGH))
      Card += "PACK KEEP HIGH TFZ " + std::string(KEEP_HIGH_TFZ ? "ON":"OFF") + "\n";
      return Card;
    }
};

} //phaser

#endif
