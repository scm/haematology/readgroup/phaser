//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_HAND_CLASS__
#define __PHASER_HAND_CLASS__
#include  <phaser/main/Phaser.h>
#include  <phaser/lib/jiffy.h>
#include  <phaser_defaults.h>

namespace phaser {

class data_hand
{
  public:
    data_hand() { HAND = std::string(DEF_HAND); }

    bool set(std::string p)
    {
      p = stoup(p);
      if (p == "ON" ||
          p == "OFF" ||
          p == "BOTH")
      {
        HAND = p;
        return false; //error
      }
      return true; //error
    }

    std::string HAND;

    bool is_on() { return (HAND == "ON"); }
    bool is_off() { return (HAND == "OFF"); }
    bool is_both() { return (HAND == "BOTH"); }

    std::string unparse() const
    {
      return HAND;
    }
};

} //phaser

#endif
