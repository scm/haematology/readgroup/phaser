//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolSIRClass__
#define __ProtocolSIRClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum macj_flags {
macj_k, macj_b, macj_sigma, macj_xyz, macj_occ, macj_bfac,
macj_sn, macj_sd,
macj_dphi, macj_dnn, macj_dll,
macj_partk, macj_partu,
macj_atomic, macj_sigmaa, macj_scales
};

class ProtocolSIR : public ProtocolBase
{
  private:
    bool FIX_K,FIX_B,FIX_SIGMA;
    bool FIX_XYZ,FIX_OCC,FIX_BFAC;
    bool FIX_Sn,FIX_SD;
    bool FIX_Dphi,FIX_Dnn,FIX_Dll;
    bool FIX_PARTK,FIX_PARTU;
    int  NCYC;
    data_minimizer  MINIMIZER;
    std::string TARGET;

  public:
    void init()
    {
      FIX_K = FIX_B = FIX_SIGMA = true;
      FIX_Sn = FIX_SD = false;
      FIX_Dphi = false;
      FIX_Dnn = false;
      FIX_Dll = false;
      FIX_XYZ = FIX_OCC = FIX_BFAC = false;
      FIX_PARTK = FIX_PARTU = false;
      NCYC = floatType(DEF_MACJ_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACJ_MINI));
      TARGET = std::string(DEF_MACJ_TARG);
    }

    bool is_default() const
    {
      return (
      FIX_K && FIX_B && FIX_SIGMA &&
      !FIX_Sn && !FIX_SD &&
      !FIX_Dphi &&
      !FIX_Dnn &&
      !FIX_Dll &&
      !FIX_XYZ && !FIX_OCC && !FIX_BFAC &&
      !FIX_PARTK && !FIX_PARTU &&
      NCYC == floatType(DEF_MACJ_NCYC) &&
      TARGET == std::string(DEF_MACJ_TARG) &&
      MINIMIZER.is_default()
      );
    }

    ProtocolSIR()
    { init(); }

    ProtocolSIR(bool fix_k,bool fix_b,bool fix_sigma,
                bool fix_xyz,bool fix_occ,bool fix_bfac,
                bool fix_sn,bool fix_sd,
                bool fix_dphi,bool fix_dnn,bool fix_dll,
                bool fix_partk,bool fix_partu,
                int ncyc,std::string target,std::string m)
    {
      init();
      FIX_K = fix_k;
      FIX_B = fix_b;
      FIX_SIGMA = fix_sigma;
      FIX_XYZ = fix_xyz;
      FIX_OCC = fix_occ;
      FIX_BFAC = fix_bfac;
      FIX_Sn = fix_sn;
      FIX_SD = fix_sd;
      FIX_Dphi = fix_dphi;
      FIX_Dnn = fix_dnn;
      FIX_Dll = fix_dll;
      FIX_PARTK = fix_partk;
      FIX_PARTU = fix_partu;
      if (ncyc >= 0) NCYC = ncyc;
      target = stoup(target);
      if (target == "JOINT" || target == "SIR" || target == "GENERAL")
        TARGET = target;
      MINIMIZER.set(m);
    }

    void occ()
    {
      init();
      FIX_OCC = FIX_PARTK = false;
      FIX_K = FIX_B = FIX_PARTU = FIX_SIGMA = FIX_XYZ = FIX_BFAC = true;
      FIX_Sn = FIX_SD = true;
      FIX_Dphi = FIX_Dnn = FIX_Dll = true;
    }

    void variances()
    {
      init();
      FIX_OCC = FIX_PARTK = false;
      FIX_K = FIX_B = FIX_PARTU = FIX_SIGMA = FIX_XYZ = FIX_BFAC = true;
      FIX_Sn = FIX_SD = false;
      FIX_Dphi = FIX_Dnn = FIX_Dll = false;
    }

    void off()
    {
      FIX_OCC = FIX_PARTK = true;
      FIX_K = FIX_B = FIX_PARTU = FIX_SIGMA = FIX_XYZ = FIX_BFAC = true;
      FIX_Sn = FIX_SD = true;
      FIX_Dphi = FIX_Dnn = FIX_Dll = true;
      NCYC = 0;
    }

    void all()
    {
      init();
      FIX_OCC = FIX_PARTK = false;
      FIX_K = FIX_B = FIX_PARTU = FIX_SIGMA = FIX_XYZ = FIX_BFAC = false;
      FIX_Sn = FIX_SD = false;
      FIX_Dphi = FIX_Dnn = FIX_Dll = false;
    }

    //direct setting of refinement for phenix.refine
    void setNCYC(int ncyc)
    { NCYC = ncyc; }

  //-------------------------
  //concrete member functions
  //-------------------------
    bool is_off() const { return ((FIX_K && FIX_B && FIX_SIGMA && FIX_XYZ && FIX_BFAC && FIX_OCC && FIX_Sn && FIX_SD && FIX_Dphi && FIX_Dnn && FIX_Dll && FIX_PARTK && FIX_PARTU) || NCYC == 0); }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineSIRi must
    //be changed as well - copy through virtual base class pointer
      if (i == macj_k) return FIX_K;
      else if (i == macj_b) return FIX_B;
      else if (i == macj_sigma) return FIX_SIGMA;
      else if (i == macj_xyz) return FIX_XYZ;
      else if (i == macj_occ) return FIX_OCC;
      else if (i == macj_bfac) return FIX_BFAC;
      else if (i == macj_sn) return FIX_Sn;
      else if (i == macj_sd) return FIX_SD;
      else if (i == macj_dphi) return FIX_Dphi;
      else if (i == macj_dnn) return FIX_Dnn;
      else if (i == macj_dll) return FIX_Dll;
      else if (i == macj_partk) return FIX_PARTK;
      else if (i == macj_partu) return FIX_PARTU;
      else if (i == macj_atomic) return (FIX_XYZ && FIX_OCC && FIX_BFAC);
      else if (i == macj_sigmaa) return (FIX_Sn && FIX_SD && FIX_Dphi && FIX_Dnn && FIX_Dll);
      else if (i == macj_scales) return (FIX_K && FIX_B && FIX_PARTK && FIX_PARTU);
      return false;
    }

    unsigned getNCYC() const
    { return NCYC; }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    std::string getTXT(int i=0) const
    { return TARGET; }

    std::string unparse() const
    {
      if (!FIX_K && !FIX_B && !FIX_SIGMA && !FIX_XYZ && !FIX_OCC && !FIX_BFAC && !FIX_Sn && !FIX_SD && !FIX_Dphi && !FIX_Dnn && !FIX_Dll && !FIX_PARTK && !FIX_PARTU) return "MACJOINT ALL"; //so that checking of all flags is not set in finite difference debugging
      std::string Card("");
      Card += "MACJOINT";
      Card += " K " +         std::string(FIX_K ? "OFF" : "ON ");
      Card += " B " +         std::string(FIX_B ? "OFF" : "ON ");
      Card += " SIGMA " +     std::string(FIX_SIGMA ? "OFF" : "ON ");
      Card += " XYZ " +       std::string(FIX_XYZ ? "OFF" : "ON ");
      Card += " OCC " +       std::string(FIX_OCC ? "OFF" : "ON ");
      Card += " BFAC " +      std::string(FIX_BFAC ? "OFF" : "ON ");
      Card += " Sn " +        std::string(FIX_Sn ? "OFF" : "ON ");
      Card += " SD " +        std::string(FIX_SD ? "OFF" : "ON ");
      Card += " Dphi " +        std::string(FIX_Dphi ? "OFF" : "ON ");
      Card += " Dnn " +        std::string(FIX_Dnn ? "OFF" : "ON ");
      Card += " Dll " +        std::string(FIX_Dll ? "OFF" : "ON ");
      Card += " PK " +        std::string(FIX_PARTK ? "OFF" : "ON ");
      Card += " PB " +        std::string(FIX_PARTU ? "OFF" : "ON ");
      Card += " NCYCLE " +    itos(NCYC);
      Card += " MINIMIZER " + MINIMIZER.unparse();
      Card += " TARGET " +    TARGET;
      Card += "\n";
      return Card;
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "SCALE:   "
           + "  K   " + std::string(FIX_K ? "FIX":"REF")
           + "  B   " + std::string(FIX_B ? "FIX":"REF")
           + "  SIG " + std::string(FIX_SIGMA ? "FIX":"REF") + "\n"
           + tab + "VARIANCE:"
           + "  Sn  " + std::string(FIX_Sn ? "FIX":"REF")
           + "  SD  " + std::string(FIX_SD ? "FIX":"REF")
           + "  Dph " + std::string(FIX_Dphi ? "FIX":"REF")
           + "  Dnn " + std::string(FIX_Dnn ? "FIX":"REF")
           + "  Dll " + std::string(FIX_Dll ? "FIX":"REF") + "\n"
           + tab + "PARTIAL: "
           + "  K   " + std::string(FIX_PARTK ? "FIX":"REF")
           + "  B   " + std::string(FIX_PARTU ? "FIX":"REF") + "\n"
           + tab + "ATOMIC:"
           + "  XYZ " + std::string(FIX_XYZ ? "FIX":"REF")
           + "  OCC " + std::string(FIX_OCC ? "FIX":"REF")
           + "  B   " + std::string(FIX_BFAC ? "FIX":"REF") + "\n";
    }
};

} //phaser

#endif
