//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolOCCClass__
#define __ProtocolOCCClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser/include/data_minimizer.h>
#include <phaser_defaults.h>

namespace phaser {

class ProtocolOCC : public ProtocolBase
{
  private:
    unsigned       NCYC;
    data_minimizer MINIMIZER;

    void init()
    {
      NCYC = double(DEF_MACO_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACO_MINI));
    }

  public:
    bool is_default() const
    {
      return (
      NCYC == double(DEF_MACO_NCYC) &&
      MINIMIZER.is_default()
      );
    }

    ProtocolOCC()
    { init(); }

    ProtocolOCC(double ncyc,std::string m = std::string(DEF_MACO_MINI))
    {
      init();
      if (ncyc >= 0) NCYC = ncyc;
      MINIMIZER.set(m);
    }

    void off()          { init(); NCYC = 0; }
    void all()          { init(); }
    bool is_off() const { return (NCYC == 0); }

  //-------------------------
  //concrete member functions
  //-------------------------
    void setFIX(int i,bool b)
    {
    }

    bool getFIX(int i) const
    {
      return false;
    }

    double getNUM(int i) const
    {
      return 0;
    }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      return "MACOCC NCYCLE " + itos(NCYC) +
             " MINIMIZER " + MINIMIZER.unparse() + "\n";
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "NCYC:      " + itos(NCYC) + "\n"
           + tab + "MINIMIZER: " + MINIMIZER.unparse() + "\n";
    }
};

} //phaser

#endif
