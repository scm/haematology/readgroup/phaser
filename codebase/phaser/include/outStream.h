//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_OUTSTREAM__
#define __PHASER_OUTSTREAM__

namespace phaser {

enum outStream {
  SILENCE, PROCESS, CONCISE, SUMMARY, LOGFILE, VERBOSE, DEBUG, TESTING, ERRATUM
//  0        1        2        3        4       5       6         7            8
};

class outStreamStr
{ //helper class
  std::string str;
  public:
  outStreamStr(std::string str_) : str(str_) { }
  outStreamStr(outStream enm)
  {
    str = "ERRATUM";
    if (enm == SILENCE) str = "SILENCE";
    if (enm == PROCESS) str = "PROCESS";
    if (enm == CONCISE) str = "CONCISE";
    if (enm == SUMMARY) str = "SUMMARY";
    if (enm == LOGFILE) str = "LOGFILE";
    if (enm == VERBOSE) str = "VERBOSE";
    if (enm == DEBUG) str = "DEBUG";
    if (enm == TESTING) str = "TESTING";
  }

  std::string string() { return str; }
  outStream enumerator()
  {
    if (str == "SILENCE") return SILENCE;
    if (str == "PROCESS") return PROCESS;
    if (str == "CONCISE") return CONCISE;
    if (str == "SUMMARY") return SUMMARY;
    if (str == "LOGFILE") return LOGFILE;
    if (str == "VERBOSE") return VERBOSE;
    if (str == "DEBUG") return DEBUG;
    if (str == "TESTING") return TESTING;
    return ERRATUM;
  }
};

} //phaser

#endif
