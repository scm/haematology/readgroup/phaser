//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_BFAC_OCCU_Class__
#define __DATA_BFAC_OCCU_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_bofac
{

  public:
  data_bofac(double b=double(DEF_SEAR_BFAC),double o=double(DEF_SEAR_OFAC)) : BFAC(b),OFAC(o) { }

  double BFAC,OFAC;

  bool defaults()
  {
    if (BFAC != DEF_SEAR_BFAC) return false;
    if (OFAC != DEF_SEAR_OFAC) return false;
    return true;
  }
};

} //phaser

#endif
