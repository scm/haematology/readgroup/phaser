//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_RESTRAINT_Class__
#define __DATA_RESTRAINT_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_restraint
{

  public:
  data_restraint(bool b=false,double o=1) : RESTRAINT(b),SIGMA(o) { }

  bool   RESTRAINT;
  double SIGMA;
};

} //phaser

#endif
