//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_PART_Class__
#define __DATA_PART_Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/data_model.h>
#include <phaser_defaults.h>

namespace phaser {

class data_part : public data_model
{
  public:
    bool        ANOMALOUS;
    std::string ESTIMATOR;

    data_part(std::string PARTIAL_PDB_STR) : data_model()
    { //backwards compatibility with phenix.maps
      data_model::init_string(PARTIAL_PDB_STR,"partial","PDB","RMS",0.8);
      ANOMALOUS = false;
      ESTIMATOR = std::string(DEF_ENSE_ESTI);
    }

    data_part() : data_model()
    {
      ANOMALOUS = false; //default
      ESTIMATOR = std::string(DEF_ENSE_ESTI);
    }

    std::string unparse()
    {
      std::string Card;
      if (!data_model::is_default())
        Card += "PARTIAL " + data_model::unparse() + "\n";
      if (ANOMALOUS) //not default
        Card += "PARTIAL ANOMALOUS ON\n";
      if (ESTIMATOR != std::string(DEF_ENSE_ESTI)) //not default
        Card += "PARTIAL ESTIMATOR " + ESTIMATOR + " \n";
      return Card;
    }
};

} //phaser

#endif
