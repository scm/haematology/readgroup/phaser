//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_COMPOSITION_Class__
#define __DATA_COMPOSITION_Class__
#include <phaser/lib/jiffy.h>
#include <phaser/include/data_asu.h>
#include <phaser_defaults.h>

namespace phaser {

class data_composition
{
  public:
    data_composition()
     {
       BY = "AVERAGE"; //change unparse below
       PERCENTAGE = double(DEF_COMP_PERC); //default 50% if by solvent
       INCREASE = 1;
       MIN_SOLVENT = double(DEF_COMP_MIN_SOLV)/100.;
       ASU.clear();
     }

    std::string BY; //average,solvent,asu
    double      PERCENTAGE;
    std::vector<data_asu> ASU;
    double      INCREASE; //for internal control
    double      MIN_SOLVENT;

  public:
    std::string unparse()
    {
      std::string Card;
      if (BY != "AVERAGE") //default
        Card += "COMPOSITION BY " + BY + "\n";
      if (BY == "SOLVENT")
      {
        Card += "COMPOSITION PERCENTAGE " + dtos(PERCENTAGE*100) + "\n";
      }
      else if (BY == "ASU")
      {
        for (int c = 0; c < ASU.size(); c++)
        if (ASU[c].unparse().size())
          Card += "COMPOSITION " + ASU[c].unparse() + "\n";
      }
      if (INCREASE != 1)
        Card += "COMPOSITION INCREASE " + dtos(INCREASE) + "\n";
      if (MIN_SOLVENT != double(DEF_COMP_MIN_SOLV)/100.) //as above
        Card += "COMPOSITION MIN SOLVENT " + dtos(MIN_SOLVENT*100) + "\n";
      return Card;
    }
};

} //phaser

#endif
