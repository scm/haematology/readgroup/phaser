//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DDM_DATA_Class__
#define __DDM_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser_defaults.h>

namespace phaser {

class data_ddm
{
  public:
   data_ddm()
   { set_defaults(); }

   void set_defaults()
   {
     SLIDER = int(DEF_DDM_SLID);
     DISTANCE_STEP = int(DEF_DDM_DIST_STEP);
     DISTANCE_MIN = int(DEF_DDM_DIST_MIN);
     DISTANCE_MAX = int(DEF_DDM_DIST_MAX);
     JOIN_MIN = floatType(DEF_DDM_JOIN_MIN);
     JOIN_MAX = floatType(DEF_DDM_JOIN_MAX);
     if (JOIN_MIN > 1) JOIN_MIN /= 100.;
     if (JOIN_MAX > 1) JOIN_MAX /= 100.;
     SEQ_MIN = int(DEF_DDM_SEQU_MIN);
     SEQ_MAX = int(DEF_DDM_SEQU_MAX);
     SEPARATION_MIN = floatType(DEF_DDM_SEPA_MIN);
     SEPARATION_MAX = floatType(DEF_DDM_SEPA_MAX);
     CLUSTER = std::string(DEF_DDM_CLUS);
   }

   int         SLIDER;
   int         DISTANCE_STEP,DISTANCE_MIN,DISTANCE_MAX;
   int         SEQ_MIN,SEQ_MAX;
   floatType   SEPARATION_MIN,SEPARATION_MAX;
   floatType   JOIN_MIN,JOIN_MAX;
   std::string CLUSTER;

   std::string unparse()
   {
     std::string Card;
     if (SLIDER != int(DEF_DDM_SLID))
     Card += "DDM SLIDER " + itos(SLIDER) + "\n";
     if (DISTANCE_STEP != int(DEF_DDM_DIST_STEP) ||
         DISTANCE_MIN != int(DEF_DDM_DIST_MIN) ||
         DISTANCE_MAX != int(DEF_DDM_DIST_MAX))
     Card += "DDM DISTANCE STEP " + itos(DISTANCE_STEP) + " MIN " + itos(DISTANCE_MIN) + " MAX " + itos(DISTANCE_MAX) + "\n";
     if (SEQ_MIN != int(DEF_DDM_SEQU_MIN) ||
         SEQ_MAX != int(DEF_DDM_SEQU_MAX))
     Card += "DDM SEQUENCE MIN " + itos(SEQ_MIN) + " MAX " + itos(SEQ_MAX) + "\n";
     if (SEPARATION_MIN != double(DEF_DDM_SEPA_MIN) ||
         SEPARATION_MAX != double(DEF_DDM_SEPA_MAX))
     Card += "DDM SEPARATION MIN " + dtos(SEPARATION_MIN) + " MAX " + dtos(SEPARATION_MAX) + "\n";
     if (JOIN_MIN != double(DEF_DDM_JOIN_MIN)/100.0 ||
         JOIN_MAX != double(DEF_DDM_JOIN_MAX)/100.0)
     Card += "DDM JOIN MIN " + dtos(JOIN_MIN) + " MAX " + dtos(JOIN_MAX) + "\n";
     if (CLUSTER != std::string(DEF_DDM_CLUS))
     Card += "DDM CLUSTER " + CLUSTER + "\n";
     return Card;
   }

   void check_min_max()
   {
     {
     int tmp1 = DISTANCE_MIN;
     int tmp2 = DISTANCE_MAX;
     DISTANCE_MIN = std::min(tmp1,tmp2);
     DISTANCE_MAX = std::max(tmp1,tmp2);
     tmp1 = SEQ_MIN;
     tmp2 = SEQ_MAX;
     SEQ_MIN = std::min(tmp1,tmp2);
     SEQ_MAX = std::max(tmp1,tmp2);
     }
     {
     floatType tmp1 = SEPARATION_MIN;
     floatType tmp2 = SEPARATION_MAX;
     SEPARATION_MIN = std::min(tmp1,tmp2);
     SEPARATION_MAX = std::max(tmp1,tmp2);
     tmp1 = JOIN_MIN;
     tmp2 = JOIN_MAX;
     JOIN_MIN = std::min(tmp1,tmp2);
     JOIN_MAX = std::max(tmp1,tmp2);
     }
   }
};

} //phaser

#endif
