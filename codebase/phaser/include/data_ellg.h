//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_ELLG_Class__
#define __DATA_ELLG_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class data_cllg
{
  public:
  data_cllg() { ELLG=RESO=FP=0; }
  double ELLG,RESO,FP;
};

class data_ellg
{
  public:
  data_ellg()
  {
    ELLG_ADDED.clear(); ELLG_KNOWN.clear(); RESO_KNOWN.clear();
    ELLG=USEFUL_RESO=TARGET_RESO=PERFECT_RESO=RMSD=FP=0;
  }

  float1D ELLG_ADDED,ELLG_KNOWN,RESO_KNOWN;
  double ELLG,USEFUL_RESO,TARGET_RESO,PERFECT_RESO;
  double RMSD,FP;
  std::map<std::string,data_cllg> CHAIN;
};

}//end namespace phaser

#endif
