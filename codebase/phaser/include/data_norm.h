//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_NORM_Class__
#define __DATA_NORM_Class__
#include <phaser/main/Phaser.h>
#include <cctbx/miller.h>
#include <boost/logic/tribool.hpp>
#include <fstream>

namespace phaser {

class data_norm
{
  public:
    af_float  sqrt_epsnSN;
    af_float  BINS;
    dmat6     ANISO;
    floatType SOLK,SOLB;
    floatType WILSON_K,WILSON_B;
    bool      REFINED;

    std::string    FILENAME;
    boost::tribool READ; //true for READ, false for WRITE, indeterminate for neither

    data_norm()
    {
      sqrt_epsnSN.clear();
      BINS.clear();
      ANISO.fill(0);
      SOLK = 0.;
      SOLB = 0.;
      WILSON_K = 1.;
      WILSON_B = 0.;
      REFINED = false;
      FILENAME = "";
      READ = boost::indeterminate;
    }

    void setDataNorm(const data_norm & init)
    {
      ANISO = init.ANISO;
      SOLK = init.SOLK;
      SOLB = init.SOLB;
      WILSON_K = init.WILSON_K;
      WILSON_B = init.WILSON_B;
      BINS = init.BINS.deep_copy();  // deep copy of af::shared for threads
      sqrt_epsnSN = init.sqrt_epsnSN.deep_copy();  // deep copy of af::shared for threads
      REFINED = init.REFINED;
      FILENAME = init.FILENAME;
      READ = init.READ;
    }

    data_norm(const data_norm & init)
    {
      setDataNorm(init);
    }

    const data_norm& operator=(const data_norm& right)
    {
      if (&right != this)
        setDataNorm(right);
      return *this;
    }

    //bool success
    bool write_file(af::shared<miller::index<int> > MILLER)
    {
      if (MILLER.size() != sqrt_epsnSN.size()) return false;
      if (FILENAME.size())
      {
        std::ofstream binfile;
        binfile.open(const_cast<char*>(FILENAME.c_str()), std::ios::out | std::ios::binary);
        if (binfile.is_open())
        {
          binfile.seekp(0);
          binfile.write((char*)&ANISO,sizeof(dmat6));
          binfile.write((char*)&SOLK,sizeof(double));
          binfile.write((char*)&SOLB,sizeof(double));
          binfile.write((char*)&WILSON_K,sizeof(double));
          binfile.write((char*)&WILSON_B,sizeof(double));
          binfile.write((char*)&REFINED,sizeof(bool));
          int nbins = BINS.size();
          binfile.write((char*)&nbins,sizeof(int));
          for (int i = 0; i < nbins; i++)
            binfile.write((char*)&BINS[i],sizeof(double));
          int nrefl = sqrt_epsnSN.size();
          binfile.write((char*)&nrefl,sizeof(int));
          for (int i = 0; i < nrefl; i++)
          {
            binfile.write((char*)&MILLER[i][0],sizeof(int));
            binfile.write((char*)&MILLER[i][1],sizeof(int));
            binfile.write((char*)&MILLER[i][2],sizeof(int));
            binfile.write((char*)&sqrt_epsnSN[i],sizeof(double));
          }
          binfile.close();
          return true;
        }
      }
      return false;
    }

    //bool success
    std::string read_file(af::shared<miller::index<int> > MILLER)
    { //MILLER is the reference miller array, used to find epsn lookup r
      if (FILENAME.size())
      {
        std::ifstream binfile(const_cast<char*>(FILENAME.c_str()), std::ios::in | std::ios::binary);
        if (binfile.peek() == std::ifstream::traits_type::eof())
          return std::string(21,' ') + "File empty";
        if (binfile.is_open())
        {
          binfile.seekg(0);
          if (binfile) binfile.read((char*)&ANISO,sizeof(dmat6));
          if (binfile) binfile.read((char*)&SOLK,sizeof(double));
          if (binfile) binfile.read((char*)&SOLB,sizeof(double));
          if (binfile) binfile.read((char*)&WILSON_K,sizeof(double));
          if (binfile) binfile.read((char*)&WILSON_B,sizeof(double));
          if (binfile) binfile.read((char*)&REFINED,sizeof(bool));
          int nbins(0);
          if (binfile) binfile.read((char*)&nbins,sizeof(int));
          BINS.resize(nbins);
          for (int i = 0; i < nbins; i++)
            if (binfile) binfile.read((char*)&BINS[i],sizeof(double));
          int nrefl(0);
          if (binfile) binfile.read((char*)&nrefl,sizeof(int));
          if (nrefl != MILLER.size())
            return std::string(21,' ') + "Miller array size " + itos(nrefl) + " not reference miller array size " + itos(MILLER.size());
          sqrt_epsnSN.resize(nrefl);
          int range(2);
          cctbx::miller::index<int> HKL;
          for (int i = 0; i < nrefl; i++)
          {
            if (binfile) binfile.read((char*)&HKL[0],sizeof(int));
            if (binfile) binfile.read((char*)&HKL[1],sizeof(int));
            if (binfile) binfile.read((char*)&HKL[2],sizeof(int));
            int lower = std::max(0,i-range); //speed: the index will be nearby, +/- 1 in tests
            af::shared<cctbx::miller::index<int> >::iterator iter = std::find(MILLER.begin()+lower,MILLER.end(),HKL);
            if (iter == MILLER.end()) //backup option
              iter = std::find(MILLER.begin(),MILLER.end(),HKL);
            if (iter == MILLER.end()) return std::string(21,' ') + "Miller index " + ivtos(HKL) + " not in reference miller array"; //now fail
            int r = std::distance(MILLER.begin(), iter);
            if (binfile) binfile.read((char*)&sqrt_epsnSN[r],sizeof(double));
          }
          if (!binfile) return std::string(21,' ') + "Error reading data";
          binfile.close();
          REFINED = true; //so that there is no more refinement, otherwise only set in FAST/FULL
          return ""; //test for zero length
        }
        return std::string(21,' ') + "File not found";
      }
      return std::string(21,' ') + "No filename";
    }

    std::string unparse()
    {
      std::string Card;
      Card += "ANISO " + dvtos(ANISO) + "\n";
      Card += "SOLK " + dtos(SOLK) + "\n";
      Card += "SOLB " + dtos(SOLB) + "\n";
      Card += "WILSON K " + dtos(WILSON_K) + "\n";
      Card += "WILSON B " + dtos(WILSON_B) + "\n";
      Card += "REFINED " + btos(REFINED) + "\n";
      Card += "BINS " + dvtos(BINS) + "\n";
      return Card;
    }
};

} //phaser

#endif
