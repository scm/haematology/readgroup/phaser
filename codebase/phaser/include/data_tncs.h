//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_TNCS_Class__
#define __DATA_TNCS_Class__
#include <phaser/main/Phaser.h>
#include <cctbx/miller.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/include/data_asu.h>
#include <phaser/include/data_nmol.h>
#include <phaser_defaults.h>
#include <boost/logic/tribool.hpp>
#include <fstream>

namespace phaser {

class data_tncs_rot
{
  public:
    dvect31D  SAMP_ANGLE;
    floatType RANGE,SAMPLING;
    dvect3 ANGLE; //"Orthogonal" angles, small

    data_tncs_rot()
    {
      SAMP_ANGLE.clear();
      ANGLE = dvect3(DEF_TNCS_ROTA_ANGL); //-999 flag
      SAMPLING = floatType(DEF_TNCS_ROTA_SAMP); //-999 flag
      RANGE = floatType(DEF_TNCS_ROTA_RANG); //-999 flag
    }

    bool angle_is_set()
    {
      dvect3 defang = dvect3(DEF_TNCS_ROTA_ANGL); //-999 flag
      return (ANGLE[0] >= defang[0]+1 && ANGLE[1] >= defang[1]+1 && ANGLE[2] >= defang[2]+1);
    }

    std::string unparse()
    {
      std::string Card = "";
      if (angle_is_set()) Card += "TNCS ROT ANGLE " +  dvtos(ANGLE) + "\n";
      if (SAMPLING >= 0) Card += "TNCS ROT SAMPLING " + dtos(SAMPLING) + "\n";
      if (RANGE >= 0)    Card += "TNCS ROT RANGE " +  dtos(RANGE) + "\n";
      return Card;
    }

};

class data_tncs_tra
{
  public:
    bool VECTOR_SET;
    dvect3 VECTOR;
    floatType PATT_HIRES,PATT_LORES,PATT_PERCENT,PATT_DIST;
    bool PATT_MAPS;
    bool PERTURB;

    data_tncs_tra()
    {
      VECTOR_SET = false;
      VECTOR = dvect3(DEF_TNCS_TRAN_VECT);
      PATT_HIRES = floatType(DEF_TNCS_PATT_HIRE);
      PATT_LORES = floatType(DEF_TNCS_PATT_LORE);
      PATT_PERCENT = floatType(DEF_TNCS_PATT_PERC/100.0);
      PATT_DIST = floatType(DEF_TNCS_PATT_DIST);
      PERTURB = bool(DEF_TNCS_TRAN_PERT);
      PATT_MAPS = bool(DEF_TNCS_PATT_MAPS);
    }

    std::string unparse()
    {
      std::string Card("");
      if (VECTOR_SET && VECTOR != dvect3(DEF_TNCS_TRAN_VECT))
        Card += "TNCS TRA VECTOR " +  dvtos(VECTOR) + "\n";
      if (PATT_HIRES != floatType(DEF_TNCS_PATT_HIRE))
      Card += "TNCS PATT HIRES " +  dtos(PATT_HIRES) + "\n";
      if (PATT_LORES != floatType(DEF_TNCS_PATT_LORE))
      Card += "TNCS PATT LORES " +  dtos(PATT_LORES) + "\n";
      if (PATT_PERCENT != floatType(DEF_TNCS_PATT_PERC/100.0))
      Card += "TNCS PATT PERCENT " +  dtos(PATT_PERCENT) + "\n";
      if (PATT_DIST != floatType(DEF_TNCS_PATT_DIST))
      Card += "TNCS PATT DISTANCE " +  dtos(PATT_DIST) + "\n";
      if (PATT_MAPS != bool(DEF_TNCS_PATT_MAPS))
      Card += "TNCS PATT MAPS " + std::string(PATT_MAPS ? "ON\n" : "OFF\n");
      if (PERTURB != floatType(DEF_TNCS_TRAN_PERT))
      Card += "TNCS TRA PERTURB " +  std::string(PERTURB?"ON":"OFF") + "\n";
      return Card;
    }
};

class data_tncs_var
{
  public:
    floatType RMSD,FRAC; //initialization only
    float1D   BINS,RESO;

  public:
    data_tncs_var()
    {
      RMSD = floatType(DEF_TNCS_VARI_RMSD);
      FRAC = floatType(DEF_TNCS_VARI_FRAC);
      BINS.clear();
      RESO.clear();
    }

    std::string unparse()
    {
      std::string Card("");
      if (BINS.size())
      {
        Card += "TNCS VARIANCE BINS ";
        for (int a = 0; a < BINS.size(); a++) Card += dtos(BINS[a]) + " ";
        Card += "\n";
        if (RESO.size())
        {
          Card += "TNCS VARIANCE RESO ";
          for (int a = 0; a < RESO.size(); a++) Card += dtos(RESO[a]) + " ";
          Card += "\n";
        }
      }
      else
      {
        if (RMSD != floatType(DEF_TNCS_VARI_RMSD))
        Card += "TNCS VARIANCE RMSD " + dtos(RMSD) + "\n";
        if (FRAC != floatType(DEF_TNCS_VARI_FRAC))
        Card += "TNCS VARIANCE FRAC " + dtos(FRAC) + "\n";
      }
      return Card;
    }
};

class data_tncs_ep
{
  public:
    bool          LINK_RESTRAINT; //EP : Use Occ & Bfac restraint for NCS related sites
    floatType     LINK_SIGMA; //EP : sigma of Occ & Bfac restraint for NCS related sites
    floatType     PAIR_ONLY; //EP : sigma of Occ & Bfac restraint for NCS related sites

    data_tncs_ep()
    {
      LINK_SIGMA = floatType(DEF_TNCS_LINK_SIGM);
      LINK_RESTRAINT = bool(DEF_TNCS_LINK_REST);
      PAIR_ONLY = bool(DEF_TNCS_PAIR_ONLY);
    }

    std::string unparse()
    {
      std::string Card("");
      if (LINK_RESTRAINT)
      {
        if (LINK_RESTRAINT != bool(DEF_TNCS_LINK_REST))
        Card += "TNCS LINK RESTRAINT ON\n";
        if (LINK_SIGMA != floatType(DEF_TNCS_LINK_SIGM))
        Card += "TNCS LINK SIGMA " +  dtos(LINK_SIGMA) + "\n";
      }
      else
      {
        if (LINK_RESTRAINT != bool(DEF_TNCS_LINK_REST))
        Card += "TNCS LINK RESTRAINT OFF\n";
      }
      if (PAIR_ONLY != bool(DEF_TNCS_PAIR_ONLY))
      Card += "TNCS PAIR ONLY " + std::string(PAIR_ONLY ? "ON" : "OFF") + "\n";
      return Card;
    }
};

class data_tncs
{
  public:
    bool          USE;
    data_tncs_rot ROT;
    data_tncs_tra TRA;
    data_tncs_var VAR;
    data_tncs_ep  EP;
    double        GFUNCTION_RADIUS;
    int           NMOL;
    int           MAXNMOL;
    bool          REFINED;
    bool          TWINNED;
    float1D       EPSFAC;
    std::vector<data_nmol> ANALYSIS;
    data_nmol_commensurate COMM;

    std::string    FILENAME;
    boost::tribool READ; //true for READ, false for WRITE, indeterminate for neither

    data_tncs()
    {
      USE = bool(DEF_TNCS_USE);
      TWINNED = false;
      REFINED = false;
      ROT = data_tncs_rot();
      TRA = data_tncs_tra();
      VAR = data_tncs_var();
      EP  = data_tncs_ep();
      COMM = data_nmol_commensurate();
      GFUNCTION_RADIUS = floatType(DEF_TNCS_GFUN_RADI);
      NMOL = int(DEF_TNCS_NMOL);
      MAXNMOL = int(0);
      EPSFAC.clear();
    }

    void set_variances(float1D reso)
    {
      VAR.RESO = reso;
      if (!VAR.BINS.size())
      {
        VAR.BINS.resize(VAR.RESO.size());
        for (int s = 0; s < VAR.BINS.size(); s++)
        {
          floatType Drms(std::exp(-2*fn::pow2(scitbx::constants::pi*VAR.RMSD/VAR.RESO[s])/3.));
          VAR.BINS[s] = 1. - VAR.FRAC*fn::pow2(Drms);
        }
      }
    }

    std::string unparse()
    {
      std::string Card("");
      if (USE != bool(DEF_TNCS_USE))
      Card += "TNCS USE " + std::string(USE ? "ON" : "OFF") + "\n";
      if (USE)
      {
        Card += ROT.unparse();
        Card += TRA.unparse();
        Card += VAR.unparse();
        Card += EP.unparse();
        Card += COMM.unparse();
        if (GFUNCTION_RADIUS != floatType(DEF_TNCS_GFUN_RADI))
        Card += "TNCS GFUNCTION RADIUS " +  dtos(GFUNCTION_RADIUS) + "\n";
        if (NMOL) Card += "TNCS NMOL " +  itos(NMOL) + "\n";
        if (MAXNMOL) Card += "TNCS MAXNMOL " +  itos(MAXNMOL) + "\n";
        //do not output EPSFAC here, use writeSol in ResultNCS
      }
      return Card;
    }

    bool use_and_present() { return (USE && TRA.VECTOR_SET); }

    //bool success
    bool write_file(af::shared<miller::index<int> > MILLER)
    {
      if (MILLER.size() != EPSFAC.size()) return false;
      if (FILENAME.size())
      {
        std::ofstream binfile;
        binfile.open(const_cast<char*>(FILENAME.c_str()), std::ios::out | std::ios::binary);
        if (binfile.is_open())
        {
          binfile.seekp(0);
          // data_tncs_rot
          int nrot = ROT.SAMP_ANGLE.size();
          binfile.write((char*)&nrot,sizeof(int));
          for (int i = 0; i < nrot; i++)
            binfile.write((char*)&ROT.SAMP_ANGLE[i],sizeof(dvect3));
          binfile.write((char*)&ROT.RANGE,sizeof(double));
          binfile.write((char*)&ROT.SAMPLING,sizeof(double));
          binfile.write((char*)&ROT.ANGLE,sizeof(dvect3));
          // data_tncs_tra
          binfile.write((char*)&TRA.VECTOR_SET,sizeof(bool));
          binfile.write((char*)&TRA.VECTOR,sizeof(dvect3));
          binfile.write((char*)&TRA.PATT_HIRES,sizeof(double));
          binfile.write((char*)&TRA.PATT_LORES,sizeof(double));
          binfile.write((char*)&TRA.PATT_PERCENT,sizeof(double));
          binfile.write((char*)&TRA.PATT_DIST,sizeof(double));
          binfile.write((char*)&TRA.PERTURB,sizeof(bool));
          // data_tncs_var
          binfile.write((char*)&VAR.RMSD,sizeof(double));
          binfile.write((char*)&VAR.FRAC,sizeof(double));
          int nbins = VAR.BINS.size();
          binfile.write((char*)&nbins,sizeof(int));
          for (int i = 0; i < nbins; i++)
            binfile.write((char*)&VAR.BINS[i],sizeof(double));
          for (int i = 0; i < nbins; i++)
            binfile.write((char*)&VAR.RESO[i],sizeof(double));
          // data_tncs_ep
          binfile.write((char*)&EP.LINK_RESTRAINT,sizeof(bool));
          binfile.write((char*)&EP.LINK_SIGMA,sizeof(double));
          binfile.write((char*)&EP.PAIR_ONLY,sizeof(double));
          //data_tncs
          binfile.write((char*)&USE,sizeof(bool));
          binfile.write((char*)&GFUNCTION_RADIUS,sizeof(double));
          binfile.write((char*)&NMOL,sizeof(int));
          binfile.write((char*)&MAXNMOL,sizeof(int));
          int nrefl = EPSFAC.size();
          binfile.write((char*)&nrefl,sizeof(int));
          for (int i = 0; i < nrefl; i++)
          {
            binfile.write((char*)&MILLER[i][0],sizeof(int));
            binfile.write((char*)&MILLER[i][1],sizeof(int));
            binfile.write((char*)&MILLER[i][2],sizeof(int));
            binfile.write((char*)&EPSFAC[i],sizeof(double));
          }
          binfile.write((char*)&TWINNED,sizeof(bool));
          binfile.close();
          return true;
        }
      }
      return false;
    }

    //bool success
    std::string read_file(af::shared<miller::index<int> > MILLER)
    {
      if (FILENAME.size())
      {
        std::ifstream binfile(const_cast<char*>(FILENAME.c_str()), std::ios::in | std::ios::binary);
        if (binfile.peek() == std::ifstream::traits_type::eof())
          return std::string(21,' ') + "File empty";
        if (binfile.is_open())
        {
          binfile.seekg(0);
          // data_tncs_rot
          int nrot(0);
          if (binfile) binfile.read((char*)&nrot,sizeof(int));
          ROT.SAMP_ANGLE.resize(nrot);
          for (int i = 0; i < nrot; i++)
            if (binfile) binfile.read((char*)&ROT.SAMP_ANGLE[i],sizeof(dvect3));
          if (binfile) binfile.read((char*)&ROT.RANGE,sizeof(double));
          if (binfile) binfile.read((char*)&ROT.SAMPLING,sizeof(double));
          if (binfile) binfile.read((char*)&ROT.ANGLE,sizeof(dvect3));
          // data_tncs_tra
          if (binfile) binfile.read((char*)&TRA.VECTOR_SET,sizeof(bool));
          if (binfile) binfile.read((char*)&TRA.VECTOR,sizeof(dvect3));
          if (binfile) binfile.read((char*)&TRA.PATT_HIRES,sizeof(double));
          if (binfile) binfile.read((char*)&TRA.PATT_LORES,sizeof(double));
          if (binfile) binfile.read((char*)&TRA.PATT_PERCENT,sizeof(double));
          if (binfile) binfile.read((char*)&TRA.PATT_DIST,sizeof(double));
          if (binfile) binfile.read((char*)&TRA.PERTURB,sizeof(bool));
          // data_tncs_var
          if (binfile) binfile.read((char*)&VAR.RMSD,sizeof(double));
          if (binfile) binfile.read((char*)&VAR.FRAC,sizeof(double));
          int nbins;
          if (binfile) binfile.read((char*)&nbins,sizeof(int));
          VAR.BINS.resize(nbins);
          for (int i = 0; i < nbins; i++)
            if (binfile) binfile.read((char*)&VAR.BINS[i],sizeof(double));
          VAR.RESO.resize(nbins);
          for (int i = 0; i < nbins; i++)
            if (binfile) binfile.read((char*)&VAR.RESO[i],sizeof(double));
          // data_tncs_ep
          if (binfile) binfile.read((char*)&EP.LINK_RESTRAINT,sizeof(bool));
          if (binfile) binfile.read((char*)&EP.LINK_SIGMA,sizeof(double));
          if (binfile) binfile.read((char*)&EP.PAIR_ONLY,sizeof(double));
          //data_tncs
          if (binfile) binfile.read((char*)&USE,sizeof(bool));
          if (binfile) binfile.read((char*)&GFUNCTION_RADIUS,sizeof(double));
          if (binfile) binfile.read((char*)&NMOL,sizeof(int));
          if (binfile) binfile.read((char*)&MAXNMOL,sizeof(int));
          int nrefl(0);
          if (binfile) binfile.read((char*)&nrefl,sizeof(int));
          if (nrefl != MILLER.size())
            return std::string(21,' ') + "Miller array size " + itos(nrefl) + " not reference miller array size " + itos(MILLER.size());
          EPSFAC.resize(nrefl);
          int range(2);
          cctbx::miller::index<int> HKL;
          for (int i = 0; i < nrefl; i++)
          {
            if (binfile) binfile.read((char*)&HKL[0],sizeof(int));
            if (binfile) binfile.read((char*)&HKL[1],sizeof(int));
            if (binfile) binfile.read((char*)&HKL[2],sizeof(int));
            int lower = std::max(0,i-range); //speed: the index will be nearby, +/- 1 in tests
            af::shared<cctbx::miller::index<int> >::iterator iter = std::find(MILLER.begin()+lower,MILLER.end(),HKL);
            if (iter == MILLER.end()) //backup option
              iter = std::find(MILLER.begin(),MILLER.end(),HKL);
            if (iter == MILLER.end()) return std::string(21,' ') + "Miller index " + ivtos(HKL) + " not in reference miller array";
            int r = std::distance(MILLER.begin(), iter);
            if (binfile) binfile.read((char*)&EPSFAC[r],sizeof(double));
          }
          if (binfile) binfile.read((char*)&TWINNED,sizeof(bool));
          if (!binfile) return std::string(21,' ') + "Error reading data";
          binfile.close();
          REFINED = true; //so that there is no more refinement, otherwise only set in FAST/FULL
          return "";
        }
        return std::string(21,' ') + "Error opening file";
      }
      return std::string(21,' ') + "No filename";
    }
};

} //phaser

#endif
