//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ProtocolNCSClass__
#define __ProtocolNCSClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser_defaults.h>
#include <phaser/include/data_minimizer.h>

namespace phaser {

enum mact_flags { mact_tra, mact_rot, mact_var };

class ProtocolNCS : public ProtocolBase
{
  private:
    bool        FIX_TRA,FIX_ROT,FIX_VAR;
    unsigned    NCYC;
    data_minimizer MINIMIZER;

    void init()
    {
      FIX_TRA = FIX_ROT = FIX_VAR = false;
      NCYC = floatType(DEF_MACT_NCYC);
      MINIMIZER.set_default(std::string(DEF_MACT_MINI));
    }

  public:
    bool is_default() const
    {
      return (
      !FIX_TRA && !FIX_ROT && !FIX_VAR &&
      NCYC == double(DEF_MACT_NCYC) &&
      MINIMIZER.is_default()
      );
    }

    ProtocolNCS()
    { init(); }

    ProtocolNCS(bool fix_tra,bool fix_rot,bool fix_var,floatType ncyc,std::string m)
    {
      init();
      FIX_TRA = fix_tra;
      FIX_ROT = fix_rot;
      FIX_VAR = fix_var;
      if (ncyc >= 0) NCYC = ncyc;
      MINIMIZER.set(m);
    }

    void off() { init(); FIX_ROT = FIX_TRA = FIX_VAR = true; NCYC = 0; }
    void all() { init(); FIX_ROT = FIX_TRA = FIX_VAR = false; }
    void var_only() { init(); FIX_ROT = FIX_TRA = true; FIX_VAR = false; }
    bool is_off() const { return ((FIX_ROT && FIX_TRA && FIX_VAR) || NCYC == 0); }

  //-------------------------
  //concrete member functions
  //-------------------------
    void setFIX(int i,bool b)
    {
      if (i == mact_rot) FIX_ROT = b;
      else if (i == mact_tra) FIX_TRA = b;
      else if (i == mact_var) FIX_VAR = b;
    }

    bool getFIX(int i) const
    {
    //if this gets changed, the setProtocol function in RefineA must
    //be changed as well - copy through virtual base class pointer
      if (i == mact_rot) return FIX_ROT;
      else if (i == mact_tra) return FIX_TRA;
      else if (i == mact_var) return FIX_VAR;
      return false;
    }

    std::string getMINIMIZER() const
    { return MINIMIZER.unparse(); }

    unsigned getNCYC() const
    { return NCYC; }

    std::string unparse() const
    {
      return "MACTNCS ROT " + std::string(FIX_ROT ? "OFF" : "ON ") +
                   " TRA " + std::string(FIX_TRA ? "OFF" : "ON ") +
                   " VRMS " + std::string(FIX_VAR ? "OFF" : "ON ") +
                   " NCYCLE " + itos(NCYC) +
                   " MINIMIZER " + MINIMIZER.unparse() + "\n";
    }

    std::string logfile() const
    {
      std::string tab(DEF_TAB,' ');
      return tab + "Refinement protocol for this macrocycle:\n"
           + tab + "ROTATION:    " + std::string(FIX_ROT ? "FIX\n":"REFINE\n")
           + tab + "TRANSLATION: " + std::string(FIX_TRA ? "FIX\n":"REFINE\n")
           + tab + "VARIANCES:   " + std::string(FIX_VAR ? "FIX\n":"REFINE\n");
    }
};

} //phaser

#endif
