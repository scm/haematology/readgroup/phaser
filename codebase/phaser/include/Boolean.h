//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_BOOLEAN__
#define __PHASER_BOOLEAN__

namespace phaser {

enum on_off { OFF,ON,NOT_SET };

class Boolean
{
  bool   DEFAULT;
  on_off USER;
  on_off OVERRIDE;

  public:
  Boolean()
  {
    USER = NOT_SET;
    OVERRIDE = NOT_SET;
    DEFAULT = true;
  }

  void Init() { USER = NOT_SET; OVERRIDE = NOT_SET; DEFAULT = true; }
  void Set(bool b) { USER = b ? ON : OFF; }
  void Override(bool b) { OVERRIDE = b ? ON : OFF; }
  void clear_Override() { OVERRIDE = NOT_SET; }
  void Default(bool b) { DEFAULT = b; }

  bool True()
  {
    //order of precidence - OVERRIDE overrides USER, USER overrides DEFAULT
    if (OVERRIDE == ON) return true;
    if (OVERRIDE == OFF) return false;
    if (USER == ON) return true;
    if (USER == OFF) return false;
    return DEFAULT;
  }

  std::string unparse()
  {
    if (USER == ON) return "ON";
    if (USER == OFF) return "OFF";
    return "";
  }

  std::string debug()
  {
    std::string Card;
    if (OVERRIDE == ON) Card += "OVERRIDE true ";
    if (OVERRIDE == OFF) Card += "OVERRIDE false ";
    if (USER == ON) Card += "USER true ";
    if (USER == OFF) Card += "USER false ";
    if (DEFAULT) Card += "DEFAULT true ";
    if (!DEFAULT) Card += "DEFAULT false ";
    return Card;
  }
};

} //phaser

#endif
