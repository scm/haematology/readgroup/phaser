//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PTGRP_DATA_Class__
#define __PTGRP_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_ptgrp
{
  public:
  data_ptgrp(int d=0) //default
  {
    COVERAGE = double(DEF_ENSE_PTGR_COVE);
    IDENTITY = double(DEF_ENSE_PTGR_IDEN);
    RMSD = double(DEF_ENSE_PTGR_RMSD);
    TOLANG = double(DEF_ENSE_PTGR_TOLA);
    TOLSPA = double(DEF_ENSE_PTGR_TOLS);
  }

  double      COVERAGE,IDENTITY,RMSD,TOLANG,TOLSPA;

  std::string unparse(std::string start)
  {
    std::string Card = start;
    if (COVERAGE != double(DEF_ENSE_PTGR_COVE))
      Card += " COVERAGE " + dtos(COVERAGE);
    if (IDENTITY != double(DEF_ENSE_PTGR_IDEN))
      Card += " IDENTITY " + dtos(IDENTITY);
    if (RMSD != double(DEF_ENSE_PTGR_RMSD))
      Card += " RMSD " + dtos(RMSD);
    if (TOLANG != double(DEF_ENSE_PTGR_TOLA))
      Card += " TOLANG " + dtos(TOLANG);
    if (TOLSPA != double(DEF_ENSE_PTGR_TOLS))
      Card += " TOLSPA " + dtos(TOLSPA) + "\n";
    if (Card == start) Card = "";
    return Card;
  }

  bool is_default() const
  {
    return (
    COVERAGE == double(DEF_ENSE_PTGR_COVE) &&
    IDENTITY == double(DEF_ENSE_PTGR_IDEN) &&
    RMSD == double(DEF_ENSE_PTGR_RMSD) &&
    TOLANG == double(DEF_ENSE_PTGR_TOLA) &&
    TOLSPA == double(DEF_ENSE_PTGR_TOLS));
  }

};

} //phaser

#endif
