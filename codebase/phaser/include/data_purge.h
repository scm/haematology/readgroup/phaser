//All rights reserved
#ifndef __DATA_PURGE_Class__
#define __DATA_PURGE_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser_defaults.h>

namespace phaser {

class data_purge
{
  public:
    double PERCENT;
    bool   ENABLE;
    int    NUMBER;

    data_purge()
    {
      ENABLE = NUMBER = 0;
      PERCENT = default_percent();
    }

    double isperc(double percent)
    { //same as in CCP4base
      PHASER_ASSERT(percent >= 0);
      if (percent > 1) percent /= 100;
      if (percent > 1 && percent < 1.01) percent = 1;
      PHASER_ASSERT(percent <= 1);
      return percent;
    }

    void set_percent(double PERCENT_) { PERCENT = isperc(PERCENT_);}
    double get_percent() { return isperc(is_default_percent() ? default_percent()-double(DEF_TOL): PERCENT); }
    bool is_default_percent() { return PERCENT == default_percent(); }
    virtual double default_percent() { return 0; }

};

class data_purge_rot : public data_purge
{
  public:
    data_purge_rot()
    {
      PERCENT = default_percent();
      ENABLE = bool(DEF_PURG_ROTA_ENAB);
      NUMBER = int(DEF_PURG_ROTA_NUMB);
    }
    double default_percent() { return double(DEF_PURG_ROTA_PERC); }

    std::string unparse()
    {
      std::string Card("");
      if (ENABLE != bool(DEF_PURG_ROTA_ENAB))
        Card += "PURGE ROT ENABLE " + std::string(ENABLE?"ON":"OFF") + "\n";
      if (ENABLE && !is_default_percent())
        Card += "PURGE ROT PERCENT " + dtos(100*get_percent()) + "\n";
      if (ENABLE && NUMBER != int(DEF_PURG_ROTA_NUMB))
        Card += "PURGE ROT NUMBER " + itos(NUMBER) + "\n" ;
      return Card;
    }
};

class data_purge_tra : public data_purge
{
  public:
    data_purge_tra()
    {
      PERCENT = default_percent();
      ENABLE = bool(DEF_PURG_TRAN_ENAB);
      NUMBER = int(DEF_PURG_TRAN_NUMB);
    }
    double default_percent() { return double(DEF_PURG_TRAN_PERC); }

    std::string unparse()
    {
      std::string Card("");
      if (ENABLE != bool(DEF_PURG_TRAN_ENAB))
        Card += "PURGE TRA ENABLE " + std::string(ENABLE?"ON":"OFF") + "\n";
      if (ENABLE && !is_default_percent())
        Card += "PURGE TRA PERCENT " + dtos(100*get_percent()) + "\n";
      if (ENABLE && NUMBER != int(DEF_PURG_TRAN_NUMB))
        Card += "PURGE TRA NUMBER " + itos(NUMBER) + "\n" ;
      return Card;
    }
};

class data_purge_rnp : public data_purge
{
  public:
    data_purge_rnp()
    {
      PERCENT = default_percent();
      ENABLE = bool(DEF_PURG_RNP_ENAB);
      NUMBER = int(DEF_PURG_RNP_NUMB);
    }
    double default_percent() { return double(DEF_PURG_RNP_PERC); }

    std::string unparse()
    {
      std::string Card("");
      if (ENABLE != bool(DEF_PURG_RNP_ENAB))
        Card += "PURGE RNP ENABLE " + std::string(ENABLE?"ON":"OFF") + "\n";
      if (ENABLE && !is_default_percent())
        Card += "PURGE RNP PERCENT " + dtos(100*get_percent()) + "\n";
      if (ENABLE && NUMBER != int(DEF_PURG_RNP_NUMB))
        Card += "PURGE RNP NUMBER " + itos(NUMBER) + "\n" ;
      return Card;
    }
};

class data_purge_gyre : public data_purge
{
  public:
    data_purge_gyre()
    {
      PERCENT = default_percent();
      ENABLE = bool(DEF_PURG_GYRE_ENAB);
      NUMBER = int(DEF_PURG_GYRE_NUMB);
    }
    double default_percent() { return double(DEF_PURG_GYRE_PERC); }

    std::string unparse()
    {
      std::string Card("");
      if (ENABLE != bool(DEF_PURG_GYRE_ENAB))
        Card += "PURGE GYRE ENABLE " + std::string(ENABLE?"ON":"OFF") + "\n";
      if (ENABLE && !is_default_percent())
        Card += "PURGE GYRE PERCENT " + dtos(100*get_percent()) + "\n";
      if (ENABLE && NUMBER != int(DEF_PURG_GYRE_NUMB))
        Card += "PURGE GYRE NUMBER " + itos(NUMBER) + "\n" ;
      return Card;
    }
};

} //phaser

#endif
