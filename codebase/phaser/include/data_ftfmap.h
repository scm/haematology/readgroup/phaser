//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_FFTMAP_Class__
#define __DATA_FFTMAP_Class__
#include <phaser/main/Phaser.h>
#include <cctbx/xray/conversions.h>
#include <cctbx/miller/expand_to_p1.h>

namespace phaser {

class data_ftfmap
{
  public:
    data_ftfmap()
    {
      FMAP_ID=PHMAP_ID=FOM_ID="";
      FMAP = PHMAP = FOM = af_float();
    }

    data_ftfmap(const data_ftfmap & init)
    { set_data_ftfmap(init); }
    const data_ftfmap& operator=(const data_ftfmap& right)
    { if (&right != this) set_data_ftfmap(right); return *this; }

    void set_data_ftfmap(const data_ftfmap& init)
    {
      // deep copy of af::shared for threads
      FMAP_ID = init.FMAP_ID;
      PHMAP_ID = init.PHMAP_ID;
      FOM_ID = init.FOM_ID;
      FMAP = init.FMAP.deep_copy();
      PHMAP = init.PHMAP.deep_copy();
      FOM = init.FOM.deep_copy();
    }

  af_float  FMAP,PHMAP,FOM;
  std::string FMAP_ID,PHMAP_ID,FOM_ID;

  bool set_default_id()
  {
    if (FMAP.size() && FMAP_ID == "") FMAP_ID = "FMAP" ;
    if (PHMAP.size() && PHMAP_ID == "") PHMAP_ID = "PHMAP" ;
    if (FOM.size() && FOM_ID == "") FOM_ID = "FOM";
    std::set<std::string> labin;
    if (FMAP.size()) labin.insert(FMAP_ID);
    if (PHMAP.size()) labin.insert(PHMAP_ID);
    if (FOM.size())  labin.insert(FOM_ID);
    int count_labin(0);
    if (FMAP.size()) count_labin++;
    if (PHMAP.size()) count_labin++;
    if (FOM.size())  count_labin++;
    //detect duplicates
    return (count_labin != labin.size());
  }

  void erase(int r)
  {
    if (r<FMAP.size())   FMAP.erase(FMAP.begin()+r);
    if (r<PHMAP.size())  PHMAP.erase(PHMAP.begin()+r);
    if (r<FOM.size())  FOM.erase(FOM.begin()+r);
  }

  std::string unparse()
  {
    std::string Card;
    Card += (FMAP_ID != "") ? " FMAP = " + FMAP_ID : "";
    Card += (PHMAP_ID != "") ? " PHMAP = " + PHMAP_ID : "";
    Card += (FOM_ID != "") ? " FOM = " + FOM_ID : "";
    return (Card == "") ? "" : Card + "\n";
  }

  void expand_to_p1(cctbx::sgtbx::space_group sg,af::shared<miller::index<int> > MILLER,bool FriedelFlag)
  {
    if (FMAP.size())
    FMAP = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),FMAP.const_ref()).data;
    if (PHMAP.size())
    PHMAP = cctbx::miller::expand_to_p1_phases<double>(sg,FriedelFlag,MILLER.const_ref(),PHMAP.const_ref(),true).data;
    if (FOM.size())
    FOM = cctbx::miller::expand_to_p1<double>(sg,FriedelFlag,MILLER.const_ref(),FOM.const_ref()).data;
  }

  af_float reduce(af::shared<std::size_t> unique,af_float data)
  {
    if (!data.size()) return data;
    af_float tmp;
    for (int i = 0; i < unique.size(); i++)
      tmp.push_back(data[unique[i]]);
    return tmp.deep_copy();
  }

  void reduce(af::shared<std::size_t> unique)
  {
    FMAP = reduce(unique,FMAP);
    PHMAP = reduce(unique,PHMAP);
    FOM = reduce(unique,FOM);
  }

  bool phased(int NREFL)
  {
    return (FMAP.size() == NREFL && PHMAP.size() == NREFL);
  }

};

} //phaser

#endif
