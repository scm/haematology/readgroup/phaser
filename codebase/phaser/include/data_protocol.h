//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_PROTOCOL_CLASS__
#define __PHASER_PROTOCOL_CLASS__
#include <phaser/lib/jiffy.h>

namespace phaser {

class data_protocol
{
  public:
    data_protocol() { PROTOCOL = "DEFAULT"; }

    bool set(std::string p)
    {
      p = stoup(p);
      if (p == "DEFAULT" ||
          p == "CUSTOM" ||
          p == "OFF" ||
          p == "ALL")
      {
        PROTOCOL = p;
        return false; //error
      }
      return true; //error
    }

  private:
    std::string PROTOCOL;

  public:
    bool is_default() const { return (PROTOCOL == "DEFAULT"); }
    bool is_custom() const { return (PROTOCOL == "CUSTOM"); }
    bool is_off() const { return (PROTOCOL == "OFF"); }
    bool is_all() const { return (PROTOCOL == "ALL"); }

    std::string unparse() const
    {
      return PROTOCOL;
    }
};

} //phaser

#endif
