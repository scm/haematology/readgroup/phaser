//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_LLGC_Class__
#define __DATA_LLGC_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>

namespace phaser {

class data_llgc
{
  public:
    data_llgc()
      {
        ATOMTYPE.clear();
        COMPLETE = bool(DEF_LLGC_COMP);
        CLASH = floatType(DEF_LLGC_CLAS);
        SIGMA = floatType(DEF_LLGC_SIGM);
        TOP = bool(DEF_LLGC_TOP);
        NCYC = int(DEF_LLGC_NCYC);
        METHOD = std::string(DEF_LLGC_METH);
        HOLES = bool(DEF_LLGC_HOLE);
      }

  stringset ATOMTYPE;
  bool COMPLETE,TOP,HOLES;
  floatType CLASH,SIGMA;
  int NCYC;
  std::string METHOD;

  std::string unparse()
  {
    std::string Card = "";
    if (COMPLETE != bool(DEF_LLGC_COMP))
    Card += "LLGCOMPLETE COMPLETE " + std::string(COMPLETE ? "ON":"OFF") + "\n";
    if (COMPLETE)
    {
      for (stringset::iterator iter = ATOMTYPE.begin(); iter != ATOMTYPE.end(); iter++)
        Card += "LLGCOMPLETE SCATTERER " + (*iter) + "\n";
      if (SIGMA != floatType(DEF_LLGC_SIGM))
      Card += "LLGCOMPLETE SIGMA " + dtos(SIGMA) + "\n";
      if (TOP != bool(DEF_LLGC_TOP))
      Card += "LLGCOMPLETE TOP " + std::string(TOP ? "ON":"OFF") + "\n";
      if (NCYC != int(DEF_LLGC_NCYC))
      Card += "LLGCOMPLETE NCYCLE " + itos(NCYC) + "\n";
      if (CLASH != floatType(DEF_LLGC_CLAS))
      Card += "LLGCOMPLETE CLASH " + dtos(CLASH) + "\n";
      if (METHOD != std::string(DEF_LLGC_METH))
      Card += "LLGCOMPLETE METHOD " + METHOD + "\n";
      if (HOLES != bool(DEF_LLGC_HOLE))
      Card += "LLGCOMPLETE HOLES " + std::string(HOLES?"ON":"OFF") + "\n";
    }
    return Card;
  }

};

} //phaser

#endif
