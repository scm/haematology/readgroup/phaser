//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_NMOL_Class__
#define __DATA_NMOL_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser_defaults.h>

namespace phaser {

class data_nmol_commensurate
{
  public:
    double PEAK,TOLF,TOLO,PATT,MISS;

    data_nmol_commensurate()
    {
      PEAK = double(DEF_TNCS_COMM_PEAK);
      TOLF = double(DEF_TNCS_COMM_TOLF);
      TOLO = double(DEF_TNCS_COMM_TOLO);
      PATT = double(DEF_TNCS_COMM_PATT);
      MISS = double(DEF_TNCS_COMM_MISS);
    }

    std::string unparse()
    {
      std::string Card = "";
      if (PEAK != double(DEF_TNCS_COMM_PEAK)) Card += "TNCS COMM PEAK " + dtos(PEAK) + "\n";
      if (TOLF != double(DEF_TNCS_COMM_TOLF)) Card += "TNCS COMM TOLF " + dtos(TOLF) + "\n";
      if (TOLO != double(DEF_TNCS_COMM_TOLO)) Card += "TNCS COMM TOLO " + dtos(TOLO) + "\n";
      if (PATT != double(DEF_TNCS_COMM_PATT)) Card += "TNCS COMM PATT " + dtos(PATT) + "\n";
      if (MISS != double(DEF_TNCS_COMM_MISS)) Card += "TNCS COMM MISS " + dtos(MISS) + "\n";
      return Card;
    }
};

class data_nmol_vecpk
{
  public:
    data_nmol_vecpk(dvect3 vec_=dvect3(0,0,0),double pk_=0): vec(vec_),pk(pk_) { }
    dvect3 vec;
    double pk;

    bool operator<(const data_nmol_vecpk &rhs) const
    { return vec*vec < rhs.vec*rhs.vec; }
    bool operator==(const data_nmol_vecpk &rhs) const
    { return vec==rhs.vec; }
};

class data_freq
{
  public:
    data_freq()
    { site = ivect3(0,0,0); nmol = height = distance = 0; }

    ivect3 site;
    int    nmol; //freq
    double height,distance;
};

class data_nmol
{
  public:
    data_nmol(int NMOL_=0,dvect3 vector_frac_=dvect3(0,0,0),double height_=0,double distance_=0) :
      vector_frac(vector_frac_),
      vector_height(height_),
      vector_distance(distance_),
      NMOL(NMOL_)
    {
      freq = data_freq();
      tolfrac = tolorth = 999; //needs to be able to be x1000
      shortdist = std::numeric_limits<double>::max();
      vectorset.clear();
    }

  public:
    dvect3 vector_frac; //fractional coordinates
    double vector_height,vector_distance;
    //not constructor
    int    NMOL;
    //not always set if not commensurate
    data_freq freq;
    double tolfrac,tolorth,shortdist;
    std::vector<data_nmol_vecpk> vectorset;

    dvect3 exact_vector()
    {
     return dvect3(
        (1.0/NMOL)*round(double(NMOL)*vector_frac[0]),
        (1.0/NMOL)*round(double(NMOL)*vector_frac[1]),
        (1.0/NMOL)*round(double(NMOL)*vector_frac[2])
        );
    }
    void add_vector(dvect3 vec,double pk)
    {
      data_nmol_vecpk next(vec,pk);
      if (std::find(vectorset.begin(),vectorset.end(),next) == vectorset.end())
      {
        vectorset.push_back(next);
        std::sort(vectorset.begin(),vectorset.end());
      }
    }
    bool complete(data_nmol_commensurate& COMM) const
    {
      int count(0);
      for (int i = 0; i < vectorset.size(); i++)
        if (vectorset[i].pk > COMM.PATT)
          count++;
      //there are NMOL-1 non-origin peaks in the set
      //repeat 0 (and NMOL) are the origin peak
      //allow 20% of the set to be missing
      //this is the second order minimum function for 5<NMOL<10
      int allowed_missing = std::floor(COMM.MISS*NMOL);
      return (count >= NMOL-1-allowed_missing);
    }
    bool commensurate(double PATT_DIST,data_nmol_commensurate& COMM) const
    {
      return ( NMOL && //check not default!
               shortdist > PATT_DIST &&
                complete(COMM) && //complete
               (freq.height > COMM.PEAK*100 || //this should be the case if the tols are low
               (tolfrac <= COMM.TOLF && tolorth <= COMM.TOLO)));
    }
    std::string log(double PATT_DIST,data_nmol_commensurate& COMM) const
    {
      std::string Card;
      int count(0);
      for (int i = 0; i < vectorset.size(); i++)
        if (vectorset[i].pk > COMM.PATT)
          count++;
      int allowed_missing = std::floor(COMM.MISS*NMOL);
      Card += "NMOL = " + itos(NMOL) + " vector " + dvtos(vector_frac) + " freq= " + ivtos(freq.site);
      Card += "commensurate = " + btos(commensurate(PATT_DIST,COMM));
      Card += "(tolfrac = " + dtos(tolfrac) + "  " + btos(tolfrac <= COMM.TOLF);
      Card += "and tolorth) = " + dtos(tolorth) + " " + btos(tolorth <= COMM.TOLO);
      Card += "or freq height = " + dtos(freq.height)  + "  " + btos(freq.height > COMM.PEAK*100);
      Card += "and complete = " + btos(complete(COMM));
      Card += "and shortdist = " + dtos(shortdist) + " " + btos(shortdist > PATT_DIST);
      Card += "  full-set=" + itos(NMOL-1) + " has=" + itos(count) + " allowed-missing=" + itos(allowed_missing) + " -> must-have=" + itos(NMOL - 1 - allowed_missing) ;
      for (int i = 0; i < vectorset.size(); i++)
        Card += "  vector set =" + itos(i+1) + " vec=" + dvtos(vectorset[i].vec)  + " pk=" + dtos(vectorset[i].pk) +  " present = " + btos(vectorset[i].pk > COMM.PATT) ;
      return Card;
    }
};

inline bool sort_freq_height(const data_freq &left,const data_freq &right)
{ return left.height < right.height; }
inline bool sort_nmol_distance(const data_nmol &left,const data_nmol &right)
{ return left.vector_distance > right.vector_distance; }

class sort_data_nmol
{
  data_nmol_commensurate param;
  public:
    sort_data_nmol(data_nmol_commensurate& p) : param(p) {}
    bool operator()(const data_nmol& lhs, const data_nmol& rhs)
    {
    //can't use both tolorth and tolfrac in criteria for sorting
    //since should only have one distance measure, else conflicts possible
    //tolfrac used for decision making at the end - commensurate or not
    //only compare to one part per 1000 tolerance
    if (lhs.tolorth != std::numeric_limits<double>::max() && //paranoia, can't x1000
        rhs.tolorth != std::numeric_limits<double>::max() )
    {
      int tolerance(1000); //part per thousand
      int lhs_tolorth = round(lhs.tolorth*tolerance);
      int rhs_tolorth = round(rhs.tolorth*tolerance);
      if (lhs_tolorth != rhs_tolorth)
      {
        return lhs_tolorth > rhs_tolorth; //reverse sort
      }
    }
    if (lhs.NMOL != rhs.NMOL) //higher order wins
    {
      return lhs.NMOL < rhs.NMOL; //not reverse sort
    }
    if (lhs.freq.site != rhs.freq.site) //simpler frequencies win
    {
      return lhs.freq.distance > rhs.freq.distance; //reverse sort
    }
    return lhs.freq.height < rhs.freq.height; //not reverse sort
    }
   //call as: sort(v.begin(), v.end(), sort_nmol_table(paramA));
};


} //phaser

#endif
