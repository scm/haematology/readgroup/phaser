//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_SCATTERER_Class__
#define __DATA_SCATTERER_Class__

namespace phaser {

//for input control
class scatterer_input
{
  public:
    scatterer_input()
      { FIXX = FIXO = FIXB = BSWAP = USTAR = false; FRAC = true; ORIG = false; }

  bool ORIG;
  bool FRAC,FIXX,FIXO,FIXB,BSWAP,USTAR;
  bool isFixAll() { return (FIXX && FIXO && FIXB); }
};

class scatterer_extra
{
  public:
    scatterer_extra()
     { REJECTED = RESTORED = false; n_adp = n_xyz = 0; }

  bool  REJECTED; //atom has been rejected for failing some test
  bool  RESTORED; //previously rejected atom resurrected permanently
  unsigned   n_adp,n_xyz;
};

} //phaser

#endif
