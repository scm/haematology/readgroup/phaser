//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MOMENTS_DATA_Class__
#define __MOMENTS_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>

namespace phaser {

class data_moments
{
  public:
   data_moments()
   {
     meanE2_acent = meanE4_acent = expectE4_acent = sigmaE4_acent = 0;
     meanE2_cent = meanE4_cent = expectE4_cent = sigmaE4_cent = 0;
     meanE2_acent_bin.clear();meanE4_acent_bin.clear();expectE4_acent_bin.clear();
     meanE2_cent_bin.clear();meanE4_cent_bin.clear();expectE4_cent_bin.clear();
     hires = max_s = 0;
   }

   floatType meanE2_acent,meanE4_acent,expectE4_acent,sigmaE4_acent;
   floatType meanE2_cent,meanE4_cent,expectE4_cent,sigmaE4_cent;
   float1D meanE2_acent_bin,meanE4_acent_bin,expectE4_acent_bin;
   float1D meanE2_cent_bin,meanE4_cent_bin,expectE4_cent_bin;
   double hires;
   int max_s;

   void zero()
   {
     meanE2_acent = meanE4_acent = expectE4_acent = sigmaE4_acent = 0;
     meanE2_cent = meanE4_cent = expectE4_cent = sigmaE4_cent = 0;
     hires = max_s = 0;
     for (int s = 0; s < meanE2_acent_bin.size(); s++)
       meanE2_acent_bin[s] = meanE4_acent_bin[s] = expectE4_acent_bin[s] = 0;
     for (int s = 0; s < meanE2_cent_bin.size(); s++)
       meanE2_cent_bin[s] = meanE4_cent_bin[s] = expectE4_cent_bin[s] = 0;
   }

   floatType alpha()
   {
     return alpha_from_meanE4(meanE4_acent);
   }

   floatType pvalue()
   {
     return (meanE4_acent >= 2 || meanE4_acent <= 0) ?
        1.0 : scitbx::math::erfc((2-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
   }

   floatType ptwin5percent()
   {
     return (meanE4_acent >= 1.905 || meanE4_acent <= 0) ?
        1.0 : scitbx::math::erfc((1.905-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
   }

   floatType ptwinnedless(floatType twinfrac)
   {
     floatType expectE4a(expectE4a_from_alpha(twinfrac));
     return (meanE4_acent >= expectE4a || meanE4_acent <= 0) ?
        1.0 : scitbx::math::erfc((expectE4a-meanE4_acent)/(std::sqrt(2.)*sigmaE4_acent))/2;
   }

   std::pair<floatType,floatType> alpha_conf_int()
   {
     // 99% confidence interval for hemihedral alpha (for 95%, use +/- 1.96sigma
     // However, the assumptions underlying this are questionable in the presence of tNCS
     return std::pair<floatType,floatType>
                     (alpha_from_meanE4(meanE4_acent+2.57583*sigmaE4_acent),
                      alpha_from_meanE4(meanE4_acent-2.57583*sigmaE4_acent));
   }

  private:
   floatType alpha_from_meanE4(floatType meanE4a)
   {
     if (!meanE4a) return 0;
     if (meanE4a< 1.5002) return 0.5; //value for alpha = 0.49: between 1.5 and 1.5002
     return std::max(0.0,((1 - std::sqrt(-3 + 2*meanE4a))/2.));
   }

   floatType expectE4a_from_alpha(floatType alpha)
   {
     floatType alpha2 = std::max(0.,std::min(alpha,1.-alpha));
     return 2.*(1.-alpha2+fn::pow2(alpha2));
   }

};

} //phaser

#endif
