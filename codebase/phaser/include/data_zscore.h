//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ZSCORE_DATA_Class__
#define __ZSCORE_DATA_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>
#include <phaser_defaults.h>

namespace phaser {

class data_zscore
{
  public:
   data_zscore()
   {
     USE = bool(DEF_ZSCO_USE);
     STOP = bool(DEF_ZSCO_STOP);
     SOLVED = floatType(DEF_ZSCO_SOLV);
     POSSIBLY_SOLVED = floatType(DEF_ZSCO_POSS_SOLV);
     HALF = bool(DEF_ZSCO_HALF);
     TOP = TOP_SET = 0;
   }
   data_zscore(bool USE_) : USE(USE_)
   {
     STOP = bool(DEF_ZSCO_STOP);
     SOLVED = floatType(DEF_ZSCO_SOLV);
     POSSIBLY_SOLVED = floatType(DEF_ZSCO_POSS_SOLV);
     HALF = bool(DEF_ZSCO_HALF);
     TOP = TOP_SET = 0;
   }

  private:
    floatType TOP;
    bool      TOP_SET;

  public:
    bool      USE,STOP;
    floatType SOLVED,POSSIBLY_SOLVED;
    bool      HALF;

    void set_top(floatType z) { TOP_SET = true; TOP = z; }
    void unset_top()          { TOP_SET = false; }

    floatType cutoff()                      { return ((HALF && TOP_SET) ? std::max(SOLVED,TOP/2) : SOLVED); }
    floatType warning_cutoff()              { return ((HALF && TOP_SET) ? cutoff()*3/2 : SOLVED); }
    bool over_cutoff(floatType v)           { return (USE && v > cutoff()); }
    bool over_warning_cutoff(floatType v)   { return (USE && v > warning_cutoff()); }

    std::string unparse()
    {
      std::string Cards;
      if (USE)
      {
        if (USE != bool(DEF_ZSCO_USE))
        Cards += "ZSCORE USE ON\n";
        if (STOP != bool(DEF_ZSCO_STOP))
        Cards += "ZSCORE STOP " + std::string(STOP? "ON" : "OFF") + "\n";
        if (HALF != bool(DEF_ZSCO_HALF))
        Cards += "ZSCORE HALF " + std::string(HALF? "ON" : "OFF") + "\n";
        if (SOLVED != floatType(DEF_ZSCO_SOLV))
        Cards += "ZSCORE SOLVED " + dtos(SOLVED)+ "\n";
        if (POSSIBLY_SOLVED != floatType(DEF_ZSCO_POSS_SOLV))
        Cards += "ZSCORE POSSIBLY SOLVED " + dtos(POSSIBLY_SOLVED)+ "\n";
      }
      else if (USE != bool(DEF_ZSCO_USE))
        Cards += "ZSCORE USE OFF\n";
      return Cards;
    }

};

} //phaser


#endif
