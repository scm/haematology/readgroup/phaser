//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_BINS_Class__
#define __DATA_BINS_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser_defaults.h>

namespace phaser {

class data_bins
{
  public:
    int MAXBINS,MINBINS,WIDTH;

  public:
    data_bins()
    {
      MAXBINS = MINBINS = WIDTH = 0;
    }

    bool not_setup()
    { return !MAXBINS && !MINBINS && !WIDTH; }

    void set_default_data()
    {
      MAXBINS = int(DEF_BINS_DATA_MAXI);
      MINBINS = int(DEF_BINS_DATA_MINI);
      WIDTH =   int(DEF_BINS_DATA_WIDT);
    }

    void set_default_ensembles()
    {
      MAXBINS = int(DEF_ENSE_BINS_MAXI);
      MINBINS = int(DEF_ENSE_BINS_MINI);
      WIDTH =   int(DEF_ENSE_BINS_WIDT);
    }

    bool is_default_data()
    {
      return (
      MAXBINS == int(DEF_BINS_DATA_MAXI) &&
      MINBINS == int(DEF_BINS_DATA_MINI) &&
      WIDTH ==   int(DEF_BINS_DATA_WIDT));
    }

    bool is_default_ensemble()
    {
      return (
      MAXBINS == int(DEF_ENSE_BINS_MAXI) &&
      MINBINS == int(DEF_ENSE_BINS_MINI) &&
      WIDTH ==   int(DEF_ENSE_BINS_WIDT));
    }

    std::string unparse()
    {
      std::string Card;
      Card += "BINS";
      Card += " MIN " + itos(MINBINS);
      Card += " MAX " + itos(MAXBINS);
      Card += " WIDTH " + itos(WIDTH);
      Card += "\n";
      return Card;
    }
};

}

#endif
