//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_VOLUME_CLASS__
#define __PHASER_VOLUME_CLASS__
#include  <phaser/main/Phaser.h>
#include  <phaser_defaults.h>

namespace phaser {

class volume
{
  public:
    volume() { VOLUME = std::string(DEF_TRAN_VOLU); }

    bool set(std::string p)
    {
      p = stoup(p);
      if (p == "FULL" ||
          p == "REGION" ||
          p == "LINE" ||
          p == "AROUND")
      {
        VOLUME = p;
        return false; //error
      }
      return true; //error
    }

  private:
    std::string VOLUME;

  public:
    bool full() { return (VOLUME == "FULL"); }
    bool region() { return (VOLUME == "REGION"); }
    bool line() { return (VOLUME == "LINE"); }
    bool around() { return (VOLUME == "AROUND"); }

    bool is_default() { return (VOLUME == std::string(DEF_TRAN_VOLU)); }

    std::string unparse() const
    {
      return VOLUME;
    }
};

class data_btf
{
  public:
    volume    VOLUME;
    dvect3    START;
    dvect3    END;
    floatType RANGE;
    bool      FRAC;

  data_btf()
  {
    VOLUME.set(DEF_TRAN_VOLU);
    START = dvect3(DEF_TRAN_STAR);
    END   = dvect3(DEF_TRAN_END);
    RANGE = floatType(DEF_TRAN_RANG);
    FRAC  = bool(DEF_TRAN_FRAC);
  }
};

} //phaser

#endif
