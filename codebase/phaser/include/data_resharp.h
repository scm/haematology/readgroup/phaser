//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RESHARPENING_Class__
#define __RESHARPENING_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class data_resharp
{
  public:
   data_resharp()
   {
     IS_DEFAULT = true;
     FRAC = 1;
   }
   void setFrac(floatType F)
   {
     IS_DEFAULT = false;
     FRAC = F;
   }

  bool IS_DEFAULT;
  floatType FRAC;
};

} //phaser

#endif
