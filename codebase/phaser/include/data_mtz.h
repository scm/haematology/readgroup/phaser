//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MTZ_DATA__
#define __PHASER_MTZ_DATA__
#include <phaser/main/Phaser.h>

namespace phaser {

typedef struct {

  std::string labout;
  std::string type;
  af_float    data;

} mtz_data;

typedef struct {

  std::string xtalid;
  std::string waveid;
  double      wavelength;

} mtz_wave;

} //phaser

#endif
