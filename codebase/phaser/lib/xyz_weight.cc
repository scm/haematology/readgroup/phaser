//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/lib/xyz_weight.h>
#include <phaser/io/Errors.h>
#include <scitbx/math/principal_axes_of_inertia.h>
#include <scitbx/math/eigensystem.h>
#include <cctbx/adptbx.h>

namespace phaser {

void xyz_weight::calculate()
{
  std::pair<dmat33,dvect3> P = principal();
  PR = P.first;
  PT = P.second;
  EXTENT = box();
  CENTRE = centre();
  for (int i = 0; i < cw.size(); i++)
    cw[i].xyz = PR*cw[i].xyz + PT;
  PRINCIPAL_EXTENT = box();
  PRINCIPAL_CENTRE = centre();
}

dvect3 xyz_weight::centre()
{
  dvect3 xyz_centre = dvect3(0,0,0);
  if (!cw.size()) return xyz_centre;
  floatType totalMW(0);
  for (unsigned a = 0; a < cw.size(); a++)
  {
    totalMW += cw[a].weight;
    xyz_centre[0] += cw[a].weight*cw[a].xyz[0];
    xyz_centre[1] += cw[a].weight*cw[a].xyz[1];
    xyz_centre[2] += cw[a].weight*cw[a].xyz[2];
  }
  PHASER_ASSERT(totalMW > 0);
  xyz_centre[0] /= totalMW;
  xyz_centre[1] /= totalMW;
  xyz_centre[2] /= totalMW;
  return xyz_centre;
}

std::pair<dmat33,dvect3> xyz_weight::principal()
{
  dmat33 Inertia(1,0,0,0,1,0,0,0,1);
  dvect3 xyz_centre = centre();
  if (!cw.size()) return std::pair<dmat33,dvect3>(Inertia,-Inertia*xyz_centre);

  floatType xx(0),xy(0),xz(0),yy(0),yz(0),zz(0);
  floatType xterm(0),yterm(0),zterm(0),dot(0);
  unsigned i(0),j(0),k(0),a(0);

  for (a = 0; a < cw.size(); a++)
  {
    xterm = cw[a].xyz[0] - xyz_centre[0];
    yterm = cw[a].xyz[1] - xyz_centre[1];
    zterm = cw[a].xyz[2] - xyz_centre[2];

    xx += xterm*xterm*cw[a].weight;
    xy += xterm*yterm*cw[a].weight;
    xz += xterm*zterm*cw[a].weight;
    yy += yterm*yterm*cw[a].weight;
    yz += yterm*zterm*cw[a].weight;
    zz += zterm*zterm*cw[a].weight;
  }

  dmat33 tensor(0,0,0,0,0,0,0,0,0);
  tensor(0,0) = yy + zz;
  tensor(0,1) = -xy;
  tensor(0,2) = -xz;
  tensor(1,0) = -xy;
  tensor(1,1) = xx + zz;
  tensor(1,2) = -yz;
  tensor(2,0) = -xz;
  tensor(2,1) = -yz;
  tensor(2,2) = xx + yy;

  dmat6 sym_tensor(tensor);
  cctbx::adptbx::eigensystem<floatType> eigens(sym_tensor);
  //select the direction for each principal moment axis

  for (int j_ = 0; j_ < 3; j_++)
  {
    Inertia(0,j_) = eigens.vectors(2)[j_];
    Inertia(1,j_) = eigens.vectors(1)[j_];
    Inertia(2,j_) = eigens.vectors(0)[j_];
  }

  for (i = 0; i < 2; i++)
  {
    for (a = 0; a < cw.size(); a++)
    {
      xterm = Inertia(i,0) * (cw[a].xyz[0]-xyz_centre[0]);
      yterm = Inertia(i,1) * (cw[a].xyz[1]-xyz_centre[1]);
      zterm = Inertia(i,2) * (cw[a].xyz[2]-xyz_centre[2]);
      dot = xterm + yterm + zterm;
      if (dot < 0)
      {
        for (k = 0; k < 3; k++)
          Inertia(i,k) = -Inertia(i,k);
      }
      if (dot != 0) goto checkRHS;
    }
  }

  checkRHS:

  //moment axes must give a right-handed coordinate system
  xterm = Inertia(0,0)*(Inertia(1,1)*Inertia(2,2)-Inertia(2,1)*Inertia(1,2));
  yterm = Inertia(0,1)*(Inertia(2,0)*Inertia(1,2)-Inertia(1,0)*Inertia(2,2));
  zterm = Inertia(0,2)*(Inertia(1,0)*Inertia(2,1)-Inertia(2,0)*Inertia(1,1));
  dot = xterm + yterm + zterm;
  if (dot < 0)
    for (j = 0; j < 3; j++)
      Inertia(2,j) = -Inertia(2,j);
  //theEnd:

  //print the moments of PR and the principal axes
  moment = dvect3(xterm,yterm,zterm);

  //Translation after rotation will be given by matrix*(-xyz_centre).

  dvect3 principal_T = Inertia*xyz_centre;
//should be -centre but the operators aren't defined properly
  principal_T[0] *= -1;
  principal_T[1] *= -1;
  principal_T[2] *= -1;
  return std::pair<dmat33,dvect3>(Inertia,principal_T);
}

dvect3 xyz_weight::box()
{
  std::pair<dvect3,dvect3> tmp = box_min_max();
  return dvect3(tmp.second-tmp.first);
}

std::pair<dvect3,dvect3> xyz_weight::box_min_max()
{
  std::pair<dvect3,dvect3> tmp(dvect3(0,0,0),dvect3(0,0,0));
  if (!cw.size()) return tmp;
  floatType FLTMAX = std::numeric_limits<floatType>::max();
  tmp.first = dvect3(FLTMAX,FLTMAX,FLTMAX);
  tmp.second = dvect3(-FLTMAX,-FLTMAX,-FLTMAX);
  for (unsigned a = 0; a < cw.size(); a++)
  {
    tmp.first[0] = std::min(tmp.first[0], cw[a].xyz[0]);
    tmp.first[1] = std::min(tmp.first[1], cw[a].xyz[1]);
    tmp.first[2] = std::min(tmp.first[2], cw[a].xyz[2]);
    tmp.second[0] = std::max(tmp.second[0], cw[a].xyz[0]);
    tmp.second[1] = std::max(tmp.second[1], cw[a].xyz[1]);
    tmp.second[2] = std::max(tmp.second[2], cw[a].xyz[2]);
  }
  return tmp;
}

std::vector<std::pair<floatType,floatType> > xyz_weight::density()
{
  std::vector<std::pair<floatType,floatType> > molecule_density(0);
  if (!cw.size()) return molecule_density;
  dvect3 xyz_centre = centre();
  floatType extent = std::max(PRINCIPAL_EXTENT[0],std::max(PRINCIPAL_EXTENT[1],PRINCIPAL_EXTENT[2]));
  floatType CAbond = 3.8;
  for (floatType d = CAbond; d < extent; d += CAbond)
  {
    int ncount(0);;
    for (unsigned a = 0; a < cw.size(); a++)
    {
      floatType distanceSqr = (cw[a].xyz-xyz_centre)*(cw[a].xyz-xyz_centre);
      if (distanceSqr < fn::pow2(d)) ncount++;
    }
    floatType Vp = (4./3.)*scitbx::constants::pi*fn::pow3(d);
    floatType density = ncount/Vp;
    molecule_density.push_back(std::pair<floatType,floatType>(d,density));
  }
  return molecule_density;
}

floatType xyz_weight::mean_radius()
{
  std::pair<dmat33,dvect3> PRPT = principal();
  dvect3  EXTENT = PRPT.first*box() + PRPT.second;
  floatType principalRad1 = EXTENT[0]/2.0;
  floatType principalRad2 = EXTENT[1]/2.0;
  floatType principalRad3 = EXTENT[2]/2.0;
  return (principalRad1+principalRad2+principalRad3)/3.;
}

}//phaser
