#ifndef __PHASER_MATH__
#define __PHASER_MATH__
#include <cmath>
#include <phaser/main/Phaser.h>
#include <phaser/include/data_solpar.h>
#include <scitbx/math/erf.h>

namespace phaser {

  floatType Fn_alogI0(floatType);
  floatType eBesselI0(floatType);
  floatType eBesselI1(floatType);
  floatType besrat(floatType&);
  floatType sim(floatType&);
  floatType GfuncOfRSsqr(floatType);
  floatType pRiceF(floatType,floatType,floatType);
  floatType pRiceI(floatType,floatType,floatType);
  floatType pWoolfsonF(floatType,floatType,floatType);
  floatType pWoolfsonI(floatType,floatType,floatType);
  floatType expectEFW(floatType,floatType,bool);
  floatType expectEsqFW(floatType,floatType,bool);
  floatType getDfactor(floatType,floatType,bool);
  floatType getEffectiveEobs(floatType,floatType);
  double    DLuzzati(double,double);
  double    pEosqAcen(double,double);
  double    pIobsAcen(double,double,double);
  double    pSmallerEosqAcen(double,double);
  double    pLargerEosqAcen(double,double);
  double    pE2Acen_grad_hess_by_dE2(double,double,bool,double&,bool,double&);
  double    pIobsAcen_grad_hess_by_dSN(double,double,double,bool,double&,bool,double&);
  double    pEosqCen(double,double);
  double    pIobsCen(double,double,double);
  double    pSmallerEosqCen(double,double);
  double    pLargerEosqCen(double,double);
  double    pE2Cen_grad_hess_by_dE2(double,double,bool,double&,bool,double&);
  double    pIobsCen_grad_hess_by_dSN(double,double,double,bool,double&,bool,double&);
  double    pEosqTwinAcen(double,double,double);
  bool      is_FrenchWilsonF(af_float,af_float,bool1D);
  bool      is_FrenchWilsonI(af_float,af_float,bool1D);
  double    EosqInfoBitsAcen(double,double);
  double    EosqInfoBitsCen(double,double);
  double    expectedInfoBitsCen(double);
  double    expectedInfoBitsAcen(double);
  double    expectedFisherWtAcen(double,double,double);
  double    expectedFisherWtCen(double,double,double);
  double    lookup_sigesq_cent(double);
  double    lookup_sigesq_acent(double);

// The getalogch function is now encapsulated in the auxiliary class, Alogch, instantiated
// by DataMR for the sake of thread safety
class Alogch
{
   int tbllen;
   int index;
   floatType tableArg;
   floatType argcut1,argcut2;
   floatType argfac1,argfac2;
   floatType log2;
   std::vector<std::pair<floatType,floatType> > lnchTable1;
   std::vector<std::pair<floatType,floatType> > lnchTable2;
   floatType absArg;
   std::vector<std::pair<floatType,floatType> > getlnchTable(int tbllen, floatType argmax);

public:
   Alogch();
   floatType getalogch(floatType&);
};


// The getalogI0 function is now encapsulated in the auxiliary class, AlogchI0, instantiated
// by DataMR for the sake of thread safety
class AlogchI0
{
  // Finely-sampled linear interpolation trades a small amount
  // of memory for significant speed improvement, compared with
  // quadratic interpolation
   int tbllen1, tbllen2, tbllen3, tbllen4;
   floatType ONE;
   floatType argcut1, argcut2, argcut3;
   floatType argfac1, argfac2;
   floatType argfac3;
   floatType invArgFac;
   std::vector<std::pair<floatType,floatType> > lnI0Table1;
   std::vector<std::pair<floatType,floatType> > lnI0Table2;
   std::vector<std::pair<floatType,floatType> > lnI0Table3;
   std::vector<std::pair<floatType,floatType> > lnInvI0Table;

   std::vector<std::pair<floatType,floatType> > getlnInvI0Table(int tbllen, floatType argcut);
   std::vector<std::pair<floatType,floatType> > getlnI0Table(int tbllen, floatType argmax);

public:
   AlogchI0();
   floatType getalogI0(floatType& arg);
};



}//end namespace phaser

#endif
