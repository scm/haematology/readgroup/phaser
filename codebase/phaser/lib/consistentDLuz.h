//(c) 2000-2016 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/fmat.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/symm_eigen.h>

namespace phaser {

  TNT::Vector<floatType> consistentDLuz(const TNT::Vector<floatType>&,
                                       TNT::Fortran_Matrix<floatType>&,
                                       TNT::Fortran_Matrix<floatType>&);

}//end namespace phaser
