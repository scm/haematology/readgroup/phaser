#include <phaser/lib/sg_prior.h>
#include <map>

namespace phaser {

floatType sg_prior(std::string sg_hall)
{
  std::map<std::string,floatType> prior;
//Triclinic C1
  prior["P1"] = 115;
//Monoclinic C2
  prior["P2"] = 3;
  prior["P21"] = 128;
  prior["C2"] = 75;
//Orthorhombic D2
  prior["P222"] = 1;
  prior["P2221"] = 2; prior["P2122"] = 2; prior["P2212"] = 2;
  prior["P21212"] = 45; prior["P21221"] = 45; prior["P22121"] = 45;
  prior["P212121"] = 212;
  prior["C2221"] = 39;
  prior["C222"] = 3;
  prior["F222"] = 1;
  prior["I222"] = 17;
  prior["I212121"] = 4;
//Tetragonal C4
  prior["P4"] = 1;
  prior["P41"] = 7;
  prior["P42"] = 1;
  prior["P43"] = 6;
  prior["I4"] = 4;
  prior["I41"] = 1;
//Tetragonal D4
  prior["P422"] = 1;
  prior["P4212"] = 4;
  prior["P4122"] = 6;
  prior["P41212"] = 31;
  prior["P4222"] = 1;
  prior["P42212"] = 8;
  prior["P4322"] = 5;
  prior["P43212"] = 45;
  prior["I422"] = 6;
  prior["I4122"] = 4;
//Trigonal C3 (rhombohedral)
  prior["R3"] = 11;
//Trigonal C3 (hexagonal)
  prior["P3"] = 1;
  prior["P31"] = 3;
  prior["P32"] = 5;
//Trigonal D3 (rhombohedral)
  prior["R32"] = 11;
//Trigonal D3 (hexagonal)
  prior["P312"] = 0;
  prior["P321"] = 5;
  prior["P3112"] = 3;
  prior["P3121"] = 36;
  prior["P3212"] = 3;
  prior["P3221"] = 42;
//Hexagonal C6
  prior["P6"] = 4;
  prior["P61"] = 10;
  prior["P65"] = 8;
  prior["P62"] = 2;
  prior["P64"] = 2;
  prior["P63"] = 6;
//Hexagonal D6
  prior["P622"] = 2;
  prior["P6122"] = 18;
  prior["P6522"] = 13;
  prior["P6222"] = 5;
  prior["P6422"] = 5;
  prior["P6322"] = 7;
//Cubic T
  prior["P23"] = 0;
  prior["F23"] = 0;
  prior["I23"] = 4;
  prior["P213"] = 6;
  prior["I213"] = 3;
//Cubic O
  prior["P432"] = 0;
  prior["P4232"] = 1;
  prior["F432"] = 2;
  prior["F4132"] = 1;
  prior["I432"] = 2;
  prior["P4332"] = 1;
  prior["P4132"] = 1;
  prior["I4132"] = 1;

  //convert to Hall symbols
  cctbx::sgtbx::space_group sg(sg_hall);
  for (std::map<std::string,floatType>::iterator iter = prior.begin(); iter != prior.end(); iter++)
    if (cctbx::sgtbx::space_group_symbols(iter->first,"A1983").hall() == sg_hall)
      return iter->second;
  return 1;
}

std::vector<cctbx::sgtbx::space_group>  subgroups(cctbx::sgtbx::space_group reference_sg,bool include_sysabs)
{
  std::vector<cctbx::sgtbx::space_group> subgroups;
  //in place change back to original basis
  cctbx::sgtbx::change_of_basis_op z2p_op = reference_sg.z2p_op();
  cctbx::sgtbx::change_of_basis_op p2z_op = z2p_op.inverse();
  //cctbx::sgtbx::change_of_basis_op cb_op_ref = reference_sg.type().cb_op();
  cctbx::sgtbx::space_group p_parent_group = reference_sg.change_basis(z2p_op);
  p_parent_group.make_tidy();
  for (int i_smx = 0; i_smx <  p_parent_group.order_p(); i_smx++)
  {
    cctbx::sgtbx::space_group P1;
    cctbx::sgtbx::space_group group_i = P1.expand_smx(p_parent_group(i_smx));
    for (int j_smx = i_smx; j_smx <  p_parent_group.order_p(); j_smx++)
    {
      cctbx::sgtbx::space_group subgroup_i = group_i;
      cctbx::sgtbx::space_group subgroup = subgroup_i.expand_smx(p_parent_group(j_smx));
      subgroup.make_tidy();
     // cctbx::sgtbx::change_of_basis_op cb_op_new = subgroup.type().cb_op();
      //cctbx::sgtbx::change_of_basis_op cb_op_ref2new = cb_op_ref.inverse()*cb_op_new;
      try {
   //   cb_op_ref2new.as_hkl(); //throws if not rotational change
   //   deleted for toxd, not reporting P21 as subgroup
        subgroup = subgroup.change_basis(p2z_op);
        if (std::find(subgroups.begin(),subgroups.end(),subgroup) == subgroups.end())
        {
          subgroups.push_back(subgroup);
        }
      }
      catch (...) {}
    }
  }
  if (include_sysabs)
  {
    int smax = subgroups.size();
    for (int s = 0; s < smax; s++)
    {
      std::vector<cctbx::sgtbx::space_group> sysabs = groups_sysabs(subgroups[s]);
      for (int t = 0; t < sysabs.size(); t++)
      if (std::find(subgroups.begin(),subgroups.end(),sysabs[t]) == subgroups.end())
        subgroups.push_back(sysabs[t]);
    }
  }
  std::sort(subgroups.begin(),subgroups.end(),sort_sgz);
  return subgroups;
}

std::vector<cctbx::sgtbx::space_group>  groups_sysabs(cctbx::sgtbx::space_group reference_sg)
{
  std::vector<cctbx::sgtbx::space_group> sglist;
  cctbx::sgtbx::matrix_group::code reference_pg(reference_sg.point_group_type());
  cctbx::sgtbx::space_group        reference_patterson(reference_sg.build_derived_patterson_group());
  char                             reference_centring(reference_sg.conventional_centring_type_symbol());
  cctbx::sgtbx::space_group_symbol_iterator sgiter;
  cctbx::sgtbx::change_of_basis_op cb_op = reference_sg.type().cb_op().inverse();
//std::string hm = reference_sg.type().universal_hermann_mauguin_symbol();
  for(;;)
  {
    cctbx::sgtbx::space_group_symbols symbols = sgiter.next();
    if (symbols.number() == 0) break;
    cctbx::sgtbx::space_group sg(symbols.hall());
    //must apply the change of basis operation in the reference space group to the iterated ones
    //which are only the tabulated settings in the international tables
    try {
          sg = sg.change_basis(cb_op);
      if (sg.conventional_centring_type_symbol() == reference_centring &&
          sg.point_group_type() == reference_pg &&
          sg.build_derived_patterson_group() == reference_patterson &&
          std::find(sglist.begin(),sglist.end(),sg) == sglist.end())
      {
        try {//don't do type() for everything, just matches, too slow
          cctbx::sgtbx::change_of_basis_op cb_op = sg.type().cb_op();
          cb_op.as_hkl(); //throws error if not rotational change
          sglist.push_back(sg);
        } catch (...) {}
      }
    } catch (...) {}
  }
  //special case I222/I212121
  if (reference_sg.type().hall_symbol() == "I 2b 2c") //I212121
  {
    cctbx::sgtbx::space_group sg("I 2 2 2");
    if (std::find(sglist.begin(),sglist.end(),sg) == sglist.end())
      sglist.push_back(sg);
  }
  if (reference_sg.type().hall_symbol() == "I 2 2") //I222
  {
    cctbx::sgtbx::space_group sg("I 21 21 21");
    if (std::find(sglist.begin(),sglist.end(),sg) == sglist.end())
      sglist.push_back(sg);
  }
  //special case I23/I213
  if (reference_sg == cctbx::sgtbx::space_group("I 2 3")) //I23
  {
    cctbx::sgtbx::space_group sg("I 21 3");
    if (std::find(sglist.begin(),sglist.end(),sg) == sglist.end())
      sglist.push_back(sg);
  }
  if (reference_sg == cctbx::sgtbx::space_group("I 21 3")) //I213
  {
    cctbx::sgtbx::space_group sg("I 2 3");
    if (std::find(sglist.begin(),sglist.end(),sg) == sglist.end())
      sglist.push_back(sg);
  }
  return sglist;
}

} //phaser
