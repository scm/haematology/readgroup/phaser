#ifndef __PHASER_BETWEEN__
#define __PHASER_BETWEEN__

namespace phaser {

  template<class T, class U> bool between(const T& v, const U& target, const T& tol)
  {
    T vmin = target-tol;
    T vmax = target+tol;
    if (v >= vmin && v <= vmax) return true;
    return false;
  }

  template<class T, class U> bool between(const std::vector<T>& v, const std::vector<U>& target, const T& tol)
  {
    if (v.size() != target.size()) return false;
    for (int i = 0; i < v.size(); i++)
      if (!between(v[i],target[i],tol)) return false;
    return true;
  }

}

#endif
