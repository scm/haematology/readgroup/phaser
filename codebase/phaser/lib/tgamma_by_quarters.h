#ifndef __phaser_table_tgamma_class__
#define __phaser_table_tgamma_class__
#include <scitbx/constants.h>
#include <scitbx/array_family/shared.h>

namespace phaser {

class tgamma_by_quarters
{
  private:
  // Make table of tgamma values with argument from -3/4 to 100, in steps of 1/4
  // These are the only values needed for the ParabolicCylinderD evaluations
  double minarg = 0;
  double maxarg = 0;
  double argstep = 0;
  scitbx::af::shared<double> tgammaTable;

  public:
  tgamma_by_quarters(double minarg_=-0.75,
                     double maxarg_=100.0,
                     double argstep_=0.25) :
      minarg(minarg_),
      maxarg(maxarg_),
      argstep(argstep_)
  {
    int tbllen = std::ceil(maxarg-minarg)/argstep;
    tgammaTable.resize(tbllen+1); //for interpolation at the end
    for (unsigned i = 0; i < tgammaTable.size(); i++)
    {
      double arg = i*argstep + minarg;
      tgammaTable[i] = arg ? std::tgamma(arg) : 0;
    }
  }

  double
  get(double arg) const
  {
    if ( arg < minarg ) return std::tgamma(arg);
    if ( arg > maxarg ) return std::tgamma(arg);
    double fracTableArg = (arg - minarg)/argstep;
    int i = static_cast<int>(std::floor(fracTableArg+0.5)); //round
    return tgammaTable[i];
  }

};

}
#endif
