#include <phaser/lib/maths.h>
#include <phaser/main/Phaser.h>
#include <scitbx/constants.h>

namespace phaser {

floatType Fn_eBesselI0(floatType arg)
{
/* Calculates approximate values for the modified Bessel function
   of the first kind of order zero multiplied by EXP(-ABS(X)).
   Derived from:
   Authors: W. J. Cody and L. Stoltz
   Mathematics and Computer Science Division
   Argonne National Laboratory
   Argonne, IL 60439
*/
  const floatType LOWERLIM(15.0);
  const floatType EPSILON(1.0e-15);

  floatType ax(0),ax2(0),sumn(0),sumd(0);

  // BEGIN
  ax = fabs(arg);

  if (ax < EPSILON)
    return 1;
  else if (ax < LOWERLIM)
  {
    ax2 = ax*ax;
    sumn =      -2.2335582639474375249e+15 +
           ax2*(-5.5050369673018427753e+14 +
           ax2*(-3.2940087627407749166e+13 +
           ax2*(-8.4925101247114157499e+11 +
           ax2*(-1.1912746104985237192e+10 +
           ax2*(-1.0313066708737980747e+08 +
           ax2*(-5.9545626019847898221e+05 +
           ax2*(-2.4125195876041896775e+03 +
           ax2*(-7.0935347449210549190e+00 +
           ax2*(-1.5453977791786851041e-02 +
           ax2*(-2.5172644670688975051e-05 +
           ax2*(-3.0517226450451067446e-08 +
           ax2*(-2.6843448573468483278e-11 +
           ax2*(-1.5982226675653184646e-14 +
           ax2*(-5.2487866627945699800e-18))))))))))))));
     ax2 = ax2-LOWERLIM*LOWERLIM;
    sumd =      -9.7087946179594019126e+14 +
           ax2*( 3.7604188704092954661e+12 +
           ax2*(-6.5626560740833869295e+09 +
           ax2*( 6.5158506418655165707e+06 +
           ax2*(-3.7277560179962773046e+03 + ax2))));
    return (sumn/sumd)*exp(-ax);
  }
  else
  {
    ax2=(1/ax)-(1/LOWERLIM);
    sumn =      -2.1877128189032726730e-06 +
           ax2*( 9.9168777670983678974e-05 +
           ax2*(-2.6801520353328635310e-03 +
           ax2*(-3.7384991926068969150e-03 +
           ax2*( 4.7914889422856814203e-01 +
           ax2*(-2.4708469169133954315e+00 +
           ax2*( 2.9205384596336793945e+00 +
           ax2*(-3.9843750000000000000e-01)))))));
    sumd =      -5.5194330231005480228e-04 +
           ax2*( 3.2547697594819615062e-02 +
           ax2*(-1.1151759188741312645e+00 +
           ax2*( 1.3982595353892851542e+01 +
           ax2*(-6.0228002066743340583e+01 +
           ax2*( 8.5539563258012929600e+01 +
           ax2*(-3.1446690275135491500e+01 + ax2))))));
    return ((sumn/sumd)+3.984375e-01)/sqrt(ax);
  }
  return 0;
}

af::shared<std::pair<floatType,floatType> > geteBesselI0Table(int tbllen, floatType argmax)
{
  af::shared<std::pair<floatType,floatType> > eBesselI0Table;
  eBesselI0Table.resize(tbllen+1);
  floatType divarg(argmax/tbllen);
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    floatType x0(Fn_eBesselI0((i  )*divarg));
    floatType x1(Fn_eBesselI0((i+1)*divarg));
    eBesselI0Table[i].first = x0;
    eBesselI0Table[i].second = x1 - x0;
  }
  return eBesselI0Table;
}

af::shared<std::pair<floatType,floatType> > geteBesselI0InvTable(int tbllen, floatType argcut)
{
  // sqrt(1/x)eBesselI0(1/x) is nearly linear for small x.
  af::shared<std::pair<floatType,floatType> > eBesselI0InvTable;
  eBesselI0InvTable.resize(tbllen+1);
  floatType divarg(1./(argcut*tbllen));
  floatType arg0(0),arg1(0),x0(0),x1(0);
  floatType invSqrt2Pi(1./std::sqrt(scitbx::constants::two_pi));
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    if (i > 0)
    {
      arg0 = 1./((i  )*divarg);
      x0 = std::sqrt(arg0)*Fn_eBesselI0(arg0);
    }
    else
      x0 = invSqrt2Pi; // Limit as 1/arg->zero, or arg->infinity
    arg1 = 1./((i+1)*divarg);
    x1 = std::sqrt(arg1)*Fn_eBesselI0(arg1);
    eBesselI0InvTable[i].first = x0;
    eBesselI0InvTable[i].second = x1 - x0;
  }
  return eBesselI0InvTable;
}

floatType eBesselI0(floatType arg)
{
  // Accuracy about 1 part in 10^7 or better, 0-infinity
  // Quadratic interpolation would use smaller tables, but is slower
  const floatType ONE(1);
  static int tbllen1(4096),tbllen2(4096),tbllen3(4096);
  static floatType argcut1(4.),argcut2(20.);
  static floatType argfac1(tbllen1/argcut1), argfac2(tbllen2/argcut2);
  static floatType invArgFac(tbllen3*argcut2);
  static af::shared<std::pair<floatType,floatType> > eBesselI0Table1 = geteBesselI0Table(tbllen1,argcut1);
  static af::shared<std::pair<floatType,floatType> > eBesselI0Table2 = geteBesselI0Table(tbllen2,argcut2);
  static af::shared<std::pair<floatType,floatType> > eBesselI0InvTable = geteBesselI0InvTable(tbllen3,argcut2);
  floatType fracTableArg(0);
  int index(0);
  if (arg < argcut1)
  {
    fracTableArg = argfac1*arg;
    index = static_cast<int>(fracTableArg);
    return eBesselI0Table1[index].first + (fracTableArg-index)*eBesselI0Table1[index].second;
  }
  else if (arg < argcut2)
  {
    fracTableArg = argfac2*arg;
    index = static_cast<int>(fracTableArg);
    return eBesselI0Table2[index].first + (fracTableArg-index)*eBesselI0Table2[index].second;
  }
  else
  {
    floatType invArg(ONE/arg);
    fracTableArg = invArgFac*invArg;
    index = static_cast<int>(fracTableArg);
    return std::sqrt(invArg)*(eBesselI0InvTable[index].first + (fracTableArg-index)*eBesselI0InvTable[index].second);
  }
}

std::vector<std::pair<floatType,floatType> > AlogchI0::getlnInvI0Table(int tbllen, floatType argcut)
{
  // Finely-sampled linear interpolation trades a small amount
  // of memory for significant speed improvement, compared with
  // quadratic interpolation
  std::vector<std::pair<floatType,floatType> > lnInvI0Table(0);
  lnInvI0Table.resize(tbllen+1);
  floatType divarg(1./(argcut*tbllen)),arg0,arg1,x0,x1;
  floatType HALF(1./2);
  floatType logSqrt2Pi(std::log(scitbx::constants::two_pi)*HALF);

  for (unsigned i = 0; i < tbllen+1; i++)
  {
    if (i > 0)
    {
      arg0 = 1./((i  )*divarg);
      x0 = std::log(Fn_eBesselI0(arg0)) + HALF*std::log(arg0);
    }
    else
      x0 = -logSqrt2Pi;
    arg1 = 1./((i+1)*divarg);
    x1 = std::log(Fn_eBesselI0(arg1)) + HALF*std::log(arg1);
    lnInvI0Table[i].first = x0;
    lnInvI0Table[i].second = x1 - x0;
  }
  return lnInvI0Table;
}

std::vector<std::pair<floatType,floatType> > AlogchI0::getlnI0Table(int tbllen, floatType argmax)
{
  std::vector<std::pair<floatType,floatType> > lnI0Table(0);
  lnI0Table.resize(tbllen+1);
  floatType divarg(argmax/tbllen);
  floatType arg0(0),arg1(0),x0(0),x1(0);
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    if (i > 0)
    {
      arg0 = (i  )*divarg;
      x0 = std::log(Fn_eBesselI0(arg0)) + arg0;
    }
    else
      x0 = 0.;
    arg1 = (i+1)*divarg;
    x1 = std::log(Fn_eBesselI0(arg1)) + arg1;
    lnI0Table[i].first = x0;
    lnI0Table[i].second = x1 - x0;
  }
  return lnI0Table;
}

floatType AlogchI0::getalogI0(floatType& arg)
{
  floatType HALF(1./2.);
  floatType fracTableArg(0);
  int index(0);
  if (arg < argcut1)
  {
    fracTableArg = argfac1*arg;
    index = static_cast<int>(fracTableArg);
    return double(lnI0Table1[index].first + (fracTableArg-index)*lnI0Table1[index].second);
  }
  else if (arg < argcut2)
  {
    fracTableArg = argfac2*arg;
    index = static_cast<int>(fracTableArg);
    return double(lnI0Table2[index].first + (fracTableArg-index)*lnI0Table2[index].second);
  }
  else if (arg < argcut3)
  {
    fracTableArg = argfac3*arg;
    index = static_cast<int>(fracTableArg);
    return double(lnI0Table3[index].first + (fracTableArg-index)*lnI0Table3[index].second);
  }
  else
  {
    floatType invArg(ONE/arg);
    fracTableArg = invArgFac*invArg;
    index = static_cast<int>(fracTableArg);
    return double((lnInvI0Table[index].first + (fracTableArg-index)*lnInvI0Table[index].second) + arg - HALF*std::log(arg));
  }

}

AlogchI0::AlogchI0()
{
   tbllen1 = tbllen2 = tbllen3 = tbllen4 = 4096;
   ONE = 1;
   argcut1 = 0.5;
   argcut2 = 4.;
   argcut3 = 25.;
   argfac1 = tbllen1/argcut1;
   argfac2 = tbllen2/argcut2;
   argfac3 = tbllen3/argcut3;
   invArgFac = tbllen4*argcut3;
   lnI0Table1 = getlnI0Table(tbllen1,argcut1);
   lnI0Table2 = getlnI0Table(tbllen2,argcut2);
   lnI0Table3 = getlnI0Table(tbllen3,argcut3);
   lnInvI0Table = getlnInvI0Table(tbllen4,argcut3);
}

}
