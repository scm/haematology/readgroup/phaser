#include <phaser/lib/maths.h>
#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <scitbx/constants.h>
#include <boost/math/special_functions/bessel.hpp>

// Functions needed to compute Rice approximation to the LLG including
// measurement error on intensities

namespace phaser {

floatType effSigaRootAcen(floatType ee, floatType eesq, floatType sigaeff)
{
/* Acentric: Equation for which a root is the solution for the value of an
   effective sigmaA giving a Rice function that approximates the effect of
   measurement error in the observed intensities on an effective E value.
   The sigmaA (along with the associated effective E value) is determined
   by matching the first two moments of the Rice function (<E> and <E^2>)
   to the first two moments of p(E;Iobs,sigIobs), which are equivalent to
   the French-Wilson posterior expected values of E and E^2.
   There are one or two roots; if two (which occurs for allowed values of
   sigaeff when eesq<1), the lower one corresponds to an imaginary solution
   for the associated effective E value.
*/

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1.);
  floatType sigbsqr(1.-fn::pow2(sigaeff));
  floatType x(0.5*(eesq-sigbsqr)/sigbsqr);

  floatType root = std::sqrt(scitbx::constants::pi*sigbsqr)/(2.*sigbsqr) *
                   (eesq*eBesselI0(x) + (eesq - sigbsqr)*eBesselI1(x)) - ee;

  return root;
}

floatType deffSigaRootAcen_by_dsa(floatType eesq, floatType sigaeff)
{
/* Acentric: Derivative of effSigaRootAcen with respect to sigaeff
   The derivative is zero (corresponding to the minimum between two roots) if
   the argument of the Bessel function I1 is zero.  This can only happen for
   allowed values of sigaeff (0-1) if eesq<1.
*/

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1.);
  floatType sigbsqr(1.-fn::pow2(sigaeff));
  floatType x(0.5*(eesq-sigbsqr)/sigbsqr);

  floatType droot = std::sqrt(scitbx::constants::pi/sigbsqr) * (sigaeff/2.) *
                    eBesselI1(x);

  return droot;
}

floatType d2effSigaRootAcen_by_dsa2(floatType eesq, floatType sigaeff)
{
// Acentric: Second derivative of effSigaRootAcen with respect to sigaeff

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1. && eesq > 0.);
  floatType sigasqr(fn::pow2(sigaeff));
  floatType sigapow4(fn::pow2(sigasqr));
  floatType sigbsqr(1.-sigasqr);
  floatType xnum(eesq-sigbsqr);
  floatType x(0.5*xnum/sigbsqr);
  PHASER_ASSERT(x >= 0.); // x>0 is condition for real root

  floatType d2root;
  if (xnum > 1.e-10) // Avoid potential divide-by-zero
    d2root = std::sqrt(scitbx::constants::pi/sigbsqr) /
             (2.*fn::pow2(sigbsqr)) * (eesq*sigasqr*eBesselI0(x) +
             (eesq - 1. - (-2. + eesq*(2.+eesq))*sigasqr +
             (eesq - 1.)*sigapow4) * eBesselI1(x) / xnum);
  else // Linear approximation near xnum=0
  {
    floatType samin(std::sqrt(1.-eesq));
    d2root = std::sqrt(scitbx::constants::pi)*samin *
             ((3.+fn::pow2(samin))*sigaeff - 2.*(samin + std::pow(samin,3))) /
             (4.*std::pow(eesq,2.5));
  }

  return d2root;
}

floatType effSigaRootCen(floatType ee, floatType eesq, floatType sigaeff)
{
/* Centric: Equation for which a root is the solution for the value of an
   effective sigmaA giving a Rice function that approximates the effect of
   measurement error in the observed intensities on an effective E value.
   The sigmaA (along with the associated effective E value) is determined
   by matching the first two moments of the Rice function (<E> and <E^2>)
   to the first two moments of p(E;Iobs,sigIobs), which are equivalent to
   the French-Wilson posterior expected values of E and E^2.
   There are one or two roots; if two (which occurs for allowed values of
   sigaeff when eesq<1), the lower one corresponds to an imaginary solution
   for the associated effective E value.
*/

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1.);
  floatType sigbsqr(1.-fn::pow2(sigaeff));
  floatType x(0.5*(eesq-sigbsqr)/sigbsqr);

  floatType root = std::exp(-x)*std::sqrt(2.*sigbsqr/scitbx::constants::pi) +
                   std::sqrt(eesq-sigbsqr)*scitbx::math::erf(std::sqrt(x)) - ee;

  return root;
}

floatType deffSigaRootCen_by_dsa(floatType eesq, floatType sigaeff)
{
/* Centric: Derivative of effSigaRootCen with respect to sigaeff
   The derivative is zero (corresponding to the minimum between two roots) if
   the argument of the Bessel function I1 is zero.  This can only happen for
   allowed values of sigaeff (0-1) if eesq<1.
*/

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1.);
  floatType sigbsqr(1.-fn::pow2(sigaeff));
  floatType xnum(eesq-sigbsqr);
  floatType x(0.5*xnum/sigbsqr);

  floatType droot;
  if (std::abs(xnum) > 1.e-10) // Avoid potential divide-by-zero
    droot = sigaeff*scitbx::math::erf(std::sqrt(x)) / std::sqrt(xnum) -
            std::exp(-x)*std::sqrt(2.*sigbsqr/scitbx::constants::pi) *
            sigaeff / sigbsqr;
  else // Linear approximation near zero
    droot = xnum * std::sqrt(2./scitbx::constants::pi) * sigaeff /
            (3.*std::pow(sigbsqr,1.5));

  return droot;
}

floatType d2effSigaRootCen_by_dsa2(floatType eesq, floatType sigaeff)
{
// Centric: Second derivative of effSigaRootCen with respect to sigaeff

  PHASER_ASSERT(sigaeff > 0. && sigaeff < 1.);
  floatType sigasqr(fn::pow2(sigaeff));
  floatType sigapow4(fn::pow2(sigasqr));
  floatType sigbsqr(1.-sigasqr);
  floatType xnum(eesq-sigbsqr);
  floatType x(0.5*xnum/sigbsqr);
  floatType sigbsqrtpi(std::sqrt(scitbx::constants::pi*sigbsqr));

  floatType d2root,d2rnum;
  if (std::abs(xnum) > 1.e-10) // Avoid potential divide-by-zero
  {
    d2rnum = (eesq - 1.)*fn::pow2(sigbsqr)*sigbsqrtpi *
             scitbx::math::erf(std::sqrt(x));
    if (x < 20.) // Avoid potential underflow with insignificant exp(x).
      d2rnum += std::sqrt(2.*xnum)*std::exp(-x) *
                (1.-eesq + sigasqr*(eesq + fn::pow2(eesq) - 2.) + sigapow4);
    d2root = d2rnum / (fn::pow2(sigbsqr)*std::pow(xnum,1.5)*sigbsqrtpi);
  }
  else // Constant approximation near zero
    d2root = std::sqrt(2./scitbx::constants::pi) * sigaeff /
             (1.5*std::pow(sigbsqr,1.5));

  return d2root;
}

floatType effSigaRoot(floatType ee, floatType eesq, floatType sigaeff, bool centric)
{
  floatType root;
  if (centric)
    root = effSigaRootCen(ee,eesq,sigaeff);
  else
    root = effSigaRootAcen(ee,eesq,sigaeff);
  return root;
}

floatType deffSigaRoot_by_dsa(floatType eesq, floatType sigaeff, bool centric)
{
  floatType droot;
  if (centric)
    droot = deffSigaRootCen_by_dsa(eesq,sigaeff);
  else
    droot = deffSigaRootAcen_by_dsa(eesq,sigaeff);
  return droot;
}

floatType d2effSigaRoot_by_dsa2(floatType eesq, floatType sigaeff, bool centric)
{
  floatType d2root;
  if (centric)
    d2root = d2effSigaRootCen_by_dsa2(eesq,sigaeff);
  else
    d2root = d2effSigaRootAcen_by_dsa2(eesq,sigaeff);
  return d2root;
}

floatType getDfactor(floatType ee, floatType eesq, bool centric)
{
/* Use Halley's method to perform a line search to find the root of the
   SigmaA equation that matches the first two moments of the exact probability
   of E given Iobs/SIGIobs with the first two moments of the Rice function.
   This SigmaA is then a factor that can be applied to Luzzati D-values (or
   sigmaA values) arising from coordinate error, thus accounting very well for
   the effect of observation error in the likelihood targets.
   If there are two roots, we want the top one (the bottom one corresponds to
   an imaginary effective Eobs), so constrain the search between the point
   where the slope is zero and a D-factor of 1.
*/
  const floatType EPS1(1.e-7),EPS2(1.e-10);
  const floatType MAXDFAC(1.-EPS1); // No point refining to something higher

  PHASER_ASSERT(eesq-fn::pow2(ee) > -0.01); // Not FW expected values?
  if (eesq - fn::pow2(ee) <= 0.) return 1.; // No observational error

  // Point where slope is zero (if any in range 0-1) will be below desired root
  // Avoid going as low as zero, because slope is zero here
  // Numerical stability: start slightly to the right to avoid taking the square
  // root of the difference between two nearly identical numbers in effSigaRootCen
  floatType dflo(std::max(std::sqrt(1.-std::min(eesq,1.))+EPS1,EPS1));
  // If this is higher than our effective maximum, return this value
  if (dflo >= MAXDFAC) return dflo;

  //floatType flo = effSigaRoot(ee,eesq,dflo,centric);
  floatType dfhi(MAXDFAC);
  //floatType fhi = effSigaRoot(ee,eesq,dfhi,centric);
  floatType dfmid = (dflo+dfhi)/2.;
  floatType fmid = effSigaRoot(ee,eesq,dfmid,centric);

  floatType slope,curve,dfnew;
  int nloop(1);
  while ((nloop<=50) && (dfhi-dflo > EPS1) && (std::abs(fmid) > EPS2))
  {
    slope = deffSigaRoot_by_dsa(eesq,dfmid,centric);
    curve = d2effSigaRoot_by_dsa2(eesq,dfmid,centric);
    dfnew = dfmid - ( (curve > 0.) ?
            2.*fmid*slope/(2.*(fn::pow2(slope)-fmid*curve)) :
            fmid*slope ); // Halley's method, falling back on Newton-Raphson
    dfmid = ((dfnew > dflo) && (dfnew < dfhi)) ?
            dfnew : // new value is between old limits
            (dflo + dfhi)/2.; // shift failed, so bisect limits
    fmid = effSigaRoot(ee,eesq,dfmid,centric);

    if (fmid < 0.)
    {
   //   flo = fmid; // Desired root is higher than midpoint
      dflo = dfmid;
    }
    else
    {
    //  fhi = fmid; // Desired root is lower than midpoint
      dfhi = dfmid;
    }
    nloop++;
  }
  return dfmid;
}

floatType getEffectiveEobs(floatType eesq, floatType dfactor)
{
  const floatType mftol(-std::numeric_limits<floatType>::epsilon());

  floatType dfacsqr(fn::pow2(dfactor));
  floatType eEobs((eesq+(dfacsqr-1.))/dfacsqr);
  PHASER_ASSERT(eEobs >= mftol); // Ensure we got the correct root of the equation
  if (eEobs > 0.)
    eEobs = std::sqrt(eEobs);
  else
    eEobs = 0.;
  return eEobs;
}

}
