#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>

namespace phaser {

std::vector<std::pair<floatType,floatType> > Alogch::getlnchTable(int tbllen, floatType argmax)
{
  std::vector<std::pair<floatType,floatType> > lnchTable(0);
  lnchTable.resize(tbllen+1);
  floatType darg(argmax/tbllen);
  floatType arg0(0),arg1(0),x0(0),x1(0);
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    arg0 = (i  )*darg;
    arg1 = (i+1)*darg;
    x0 = std::log(std::cosh(arg0));
    x1 = std::log(std::cosh(arg1));
    lnchTable[i].first = x0;
    lnchTable[i].second = x1 - x0;
  }
  return lnchTable;
}

floatType Alogch::getalogch(floatType& arg)
{
  floatType absArg(fabs(arg));
  if (absArg < argcut1)
  {
    tableArg = absArg*argfac1;
    index = static_cast<int>(tableArg);
    return lnchTable1[index].first + (tableArg-index)*lnchTable1[index].second;
  }
  else if (absArg < argcut2)
  {
    tableArg = absArg*argfac2;
    index = static_cast<int>(tableArg);
    return lnchTable2[index].first + (tableArg-index)*lnchTable2[index].second;
  }
  else
    return (absArg-log2);
}

Alogch::Alogch()
{
   tbllen = 2048;
   argcut1 = 2.;
   argcut2 = 10.;
   argfac1 = tbllen/argcut1;
   argfac2 = tbllen/argcut2;
   log2 = std::log(2.);

   lnchTable1 = getlnchTable(tbllen,argcut1);
   lnchTable2 = getlnchTable(tbllen,argcut2);
}

}
