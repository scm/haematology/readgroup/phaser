#include <phaser/main/FloatType.h>
#include <scitbx/constants.h>

namespace phaser {

floatType rad2phi(floatType PHI)
{
  PHI =  scitbx::rad_as_deg(PHI);
  while (PHI > 180) PHI -= 360;
  while (PHI <= -180) PHI += 360;
  return PHI;
}

}
