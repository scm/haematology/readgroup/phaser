#ifndef __PHASER_SCATTERING__
#define __PHASER_SCATTERING__
#include <cctbx/eltbx/tiny_pse.h>
#include <scitbx/array_family/misc_functions.h>
#include <phaser/main/Phaser.h>
#include <phaser/include/data_formfactors.h>

namespace phaser {

struct scatmw { double scat,mw; };
bool   isElement(std::string);

class atomic_composition
{
  public:
    atomic_composition(int H=0,int C=0, int N=0, int O=0, int S=0, int P=0)
    {
      data["H"] = H;
      data["C"] = C;
      data["N"] = N;
      data["O"] = O;
      data["S"] = S;
      data["P"] = P;
    }

  public:
    std::map<std::string,int> data;

  public:
  atomic_composition& operator+=(atomic_composition &rhs)
  {
    for (std::map<std::string,int>::iterator iter = data.begin(); iter != data.end(); iter++)
      iter->second += rhs.data[iter->first];
    return *this;
  }
};

class data_scattering
{
  public:
    data_scattering(std::string atomtype_="",double percent_=0) : percent(percent_)
    {
      weight=atomic_number = 0;
      try {
      cctbx::eltbx::tiny_pse::table atom(atomtype_);
      weight = atom.weight();
      atomic_number = atom.atomic_number();
      }
      catch (...) {}
    }

  double percent,weight,atomic_number;
};

class scattering
{
  public:
    scattering()
    {
      fetch["H"] = cctbx::eltbx::xray_scattering::wk1995("H").fetch();
      fetch["C"] = cctbx::eltbx::xray_scattering::wk1995("C").fetch();
      fetch["N"] = cctbx::eltbx::xray_scattering::wk1995("N").fetch();
      fetch["O"] = cctbx::eltbx::xray_scattering::wk1995("O").fetch();
      fetch["S"] = cctbx::eltbx::xray_scattering::wk1995("S").fetch();
      fetch["P"] = cctbx::eltbx::xray_scattering::wk1995("P").fetch();
    }

  public:
    double molecular_weight_per_residue;
    double water_per_residue;
    double total_weight;
    std::map<char,atomic_composition> residue;
    std::map<std::string,data_scattering> atomtypes;
    std::map<std::string,cctbx::eltbx::xray_scattering::gaussian> fetch;

  public:
    double average_scattering_at_ssqr(double);
    double PERC(std::string);
    double fraction_by_atomtype(std::string);
    double avScat();
    scatmw ScatMw(double);
    scatmw ScatNres(int);
    scatmw ScatSeq(std::string);
    scatmw ScatSeqFile(std::string);
};

class scattering_protein : public scattering
{
  public:
    scattering_protein();
};

class scattering_nucleic : public scattering
{
  public:
    scattering_nucleic();
};

}//end namespace phaser

#endif
