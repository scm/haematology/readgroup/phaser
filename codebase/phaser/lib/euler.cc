#include <phaser/lib/euler.h>
#include <scitbx/constants.h>
#include <scitbx/math/euler_angles.h>
#include <math.h>

namespace phaser {

//Euler angles ZYZ same as AmoRe and MolRep convention

dmat33 euler2matrixRAD(dvect3 euler)
{
  double cosa = cos(euler[0]);
  double sina = sin(euler[0]);
  double cosb = cos(euler[1]);
  double sinb = sin(euler[1]);
  double cosg = cos(euler[2]);
  double sing = sin(euler[2]);
  dmat33 rmat( cosg*cosb*cosa - sing*sina, -sing*cosb*cosa - cosg*sina, sinb*cosa,
               cosg*cosb*sina + sing*cosa, -sing*cosb*sina + cosg*cosa, sinb*sina,
               -cosg*sinb,                  sing*sinb,                  cosb);
  return rmat;
}

dmat33 euler2matrixDEG(dvect3 euler)
{
  for (int i = 0; i < 3; i++)
    euler[i] =  scitbx::deg_as_rad(euler[i]);
  return euler2matrixRAD(euler);
}

//Turn a rotation matrix into Euler angles (Crowther definition)
dvect3 matrix2eulerRAD(dmat33 rmat)
{
  dvect3 euler(0,0,0);
  floatType cosa(0),sina(0),cosb(0),sinb(0),cosg(0),sing(0);
  cosb = rmat(2,2);
  if ( fabs( 1.0-fabs(cosb) ) >  0.000001)
  {
    euler[1] = acos(cosb);
    sinb = sin(euler[1]);
    cosa = rmat(0,2)/sinb;
    sina = rmat(1,2)/sinb;
    euler[0] = atan2(sina,cosa);
    cosg = -rmat(2,0)/sinb;
    sing = rmat(2,1)/sinb;
    euler[2] = atan2(sing,cosg);
  }
  else
/* Case of beta = 0 (only alpha + gamma defined) or
           beta = pi (only alpha - gamma defined)
   one degree of freedom: pick gamma = 0
   By our criteria, abs(cosb) == 1 , so make this exact
*/
  {
    if (cosb >= 0.0)
      cosb = 1.0;
    else
      cosb = -1.0;
    euler[1] = atan2(0.0,cosb);
    euler[2] = 0.0;
    cosa = rmat(0,0)/cosb;
    sina = rmat(1,0)/cosb;
    euler[0] = atan2(sina,cosa);
  }
  for (int i = 0; i < 3; i++)
    if (euler[i] < 0.0)
      euler[i] = euler[i] + scitbx::constants::two_pi;
  return euler;
}

dvect3 matrix2eulerDEG_old(dmat33 rmat)
{
  dvect3 euler(0,0,0);
  floatType cosa(0),sina(0),cosb(0),sinb(0),cosg(0),sing(0);
  cosb = rmat(2,2);
  if ( fabs( 1.0-fabs(cosb) ) >  0.000001)
  {
    euler[1] = acos(cosb);
    sinb = sin(euler[1]);
    cosa = rmat(0,2)/sinb;
    sina = rmat(1,2)/sinb;
    euler[0] = atan2(sina,cosa);
    cosg = -rmat(2,0)/sinb;
    sing = rmat(2,1)/sinb;
    euler[2] = atan2(sing,cosg);
  }
  else
/* Case of beta = 0 (only alpha + gamma defined) or
           beta = pi (only alpha - gamma defined)
   one degree of freedom: pick gamma = 0
   By our criteria, abs(cosb) == 1 , so make this exact
*/
  {
    if (cosb >= 0.0) cosb = 1.0;
    else cosb = -1.0;
    euler[1] = atan2(0.0,cosb);
    euler[2] = 0.0;
    cosa = rmat(0,0)/cosb;
    sina = rmat(1,0)/cosb;
    euler[0] = atan2(sina,cosa);
  }
  for (int i = 0; i < 3; i++)
    if (euler[i] < 0.0)
      euler[i] = euler[i] + scitbx::constants::two_pi;
  for (int i = 0; i < 3; i++)
  {
    euler[i] = scitbx::rad_as_deg(euler[i]);
    euler[i] = fmod(euler[i],360); //truncates at 360
  }
  return euler;
}

inline floatType
regularize_angle(const floatType& angle)
{
  return fmod( 360 + fmod( angle, 360 ), 360 );
}

dvect3 matrix2eulerDEG(const dmat33& rmat)
{
  dvect3 angles = scitbx::math::euler_angles::zyz_angles( rmat, 1E-6 );
  return dvect3(
    regularize_angle( angles[0] ),
    regularize_angle( angles[1] ),
    regularize_angle( angles[2] )
    );
}

dmat33 polar2matrixRAD(dvect3 polar)
{
  //polar angles are omega phi kappa
  floatType omega = polar[0];
  floatType phi = polar[1];
  floatType kappa = polar[2];

  dmat33 rmat;
  floatType someg = sin(omega);
  floatType comeg = cos(omega);
  floatType sinph = sin(phi);
  floatType cosph = cos(phi);
  floatType dc1 = someg*cosph;
  floatType dc2 = someg*sinph;
  floatType dc3 = comeg;
  floatType sinchi = sin(kappa);
  floatType coschi = cos(kappa);
  floatType cosch1 = 1.0 - coschi;
  rmat(0,0) = dc1*dc1*cosch1 + coschi;
  rmat(0,1) = dc1*dc2*cosch1 - dc3*sinchi;
  rmat(0,2) = dc3*dc1*cosch1 + dc2*sinchi;
  rmat(1,0) = dc1*dc2*cosch1 + dc3*sinchi;
  rmat(1,1) = dc2*dc2*cosch1 + coschi;
  rmat(1,2) = -dc1*sinchi + dc2*dc3*cosch1;
  rmat(2,0) = -dc2*sinchi + dc3*dc1*cosch1;
  rmat(2,1) = dc2*dc3*cosch1 + dc1*sinchi;
  rmat(2,2) = dc3*dc3*cosch1 + coschi;
  return rmat;
}

dmat33 polar2matrixDEG(dvect3 polar)
{
  for (int i = 0; i < 3; i++)
    polar[i] =scitbx::deg_as_rad(polar[i]);
  return polar2matrixRAD(polar);
}
}
