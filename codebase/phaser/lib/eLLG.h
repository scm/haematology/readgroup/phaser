#include <phaser/main/Phaser.h>
#include <phaser/pod/ssqr_dfac_solt.h>

namespace phaser {

  double get_eLLG(std::vector<ssqr_dfac_solt >,double,double,float1D=float1D(0),float1D=float1D(0));
  double get_reso_for_eLLG(double,std::vector<ssqr_dfac_solt>,double,double,float1D=float1D(0),float1D=float1D(0));

}//end namespace phaser
