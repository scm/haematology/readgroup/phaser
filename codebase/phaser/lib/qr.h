#ifndef JAMA_QR_H
#define JAMA_QR_H

#include "vec.h"
#include "fmat.h"
#include "tnt_math_utils.h"

namespace JAMA
{

/**
<p>
  Classical QR Decompisition:
   for an m-by-n matrix A with m >= n, the QR decomposition is an m-by-n
   orthogonal matrix Q and an n-by-n upper triangular matrix R so that
   A = Q*R.
<P>
   The QR decompostion always exists, even if the matrix does not have
   full rank, so the constructor will never fail.  The primary use of the
   QR decomposition is in the least squares solution of nonsquare systems
   of simultaneous linear equations.  This will fail if isFullRank()
   returns 0 (false).

<p>
  The Q and R factors can be retrived via the getQ() and getR()
  methods. Furthermore, a solve() method is provided to find the
  least squares solution of Ax=b using the QR factors.

   <p>
  (Adapted from JAMA, a Java Matrix Library, developed by jointly
  by the Mathworks and NIST; see  http://math.nist.gov/javanumerics/jama).

  (Adapted by AJM to accept TNT::Fortran_Matrix type)
*/

template <class Real>
class QR {

/** Array for internal storage of decomposition.
   @serial internal array storage.
*/

  TNT::Fortran_Matrix<Real> QR_;

  /** Row and column dimensions.
  @serial column dimension.
  @serial row dimension.
  */
  int m, n;

  /** Array for internal storage of diagonal of R.
  @serial diagonal of R.
  */
  TNT::Vector<Real> Rdiag;

public:

/**
  Create a QR factorization object for A.
  @param A rectangular (m>=n) matrix.
*/
  QR(const TNT::Fortran_Matrix<Real> &A)    /* constructor */
  {
    QR_ = A; //copy()
    m = A.num_rows();
    n = A.num_cols();
    Rdiag = TNT::Vector<Real>(n);
    int i=0, j=0, k=0;

    // Main loop.
    for (k = 0; k < n; k++) {
      // Compute 2-norm of k-th column without under/overflow.
      Real nrm = 0;
      for (i = k; i < m; i++) {
        nrm = TNT::hypot(nrm,QR_(i+1,k+1));
      }

      if (nrm != 0.0) {
        // Form k-th Householder vector.
        if (QR_(k+1,k+1) < 0) {
          nrm = -nrm;
        }
        for (i = k; i < m; i++) {
          QR_(i+1,k+1) /= nrm;
        }
        QR_(k+1,k+1) += 1.0;

        // Apply transformation to remaining columns.
        for (j = k+1; j < n; j++) {
          Real s = 0.0;
          for (i = k; i < m; i++) {
            s += QR_(i+1,k+1)*QR_(i+1,j+1);
          }
          s = -s/QR_(k+1,k+1);
          for (i = k; i < m; i++) {
            QR_(i+1,j+1) += s*QR_(i+1,k+1);
          }
        }
      }
      Rdiag[k] = -nrm;
    }
  }

/**
  Flag to denote the matrix is of full rank.
  @return 1 if matrix is full rank, 0 otherwise.
*/
  int isFullRank() const
  {
    for (int j = 0; j < n; j++)
    {
      if (Rdiag[j] == 0)
        return 0;
    }
    return 1;
  }

  /** Return the upper triangular factor, R, of the QR factorization
  @return     R
  */

  TNT::Fortran_Matrix<Real> getR() const
  {
    TNT::Fortran_Matrix<Real> R(n,n);
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (i < j) {
          R(i+1,j+1) = QR_(i+1,j+1);
        } else if (i == j) {
          R(i+1,j+1) = Rdiag[i];
        } else {
          R(i+1,j+1) = 0.0;
        }
      }
    }
    return R;
  }

  /**
    Generate and return the (economy-sized) orthogonal factor
  @param     Q the (ecnomy-sized) orthogonal factor (Q*R=A).
  */
  TNT::Fortran_Matrix<Real> getQ() const
  {
    int i=0, j=0, k=0;
    TNT::Fortran_Matrix<Real> Q(m,n);
    for (k = n-1; k >= 0; k--) {
      for (i = 0; i < m; i++) {
        Q(i+1,k+1) = 0.0;
      }
      Q(k+1,k+1) = 1.0;
      for (j = k; j < n; j++) {
        if (QR_(k+1,k+1) != 0) {
          Real s = 0.0;
          for (i = k; i < m; i++) {
            s += QR_(i+1,k+1)*Q(i+1,j+1);
          }
          s = -s/QR_(k+1,k+1);
          for (i = k; i < m; i++) {
            Q(i+1,j+1) += s*QR_(i+1,k+1);
          }
        }
      }
    }
    return Q;
  }

  /** Least squares solution of A*x = b
  @param B     m-length array (vector).
  @return x    n-length array (vector) that minimizes the two norm of Q*R*X-B.
      If B is non-conformant, or if QR.isFullRank() is false,
           the routine returns a null (0-length) vector.
  */
  TNT::Vector<Real> solve(const TNT::Vector<Real> &b) const
  {
    if (b.num_rows() != m)    /* arrays must be conformant */
      return TNT::Vector<Real>();
    if ( !isFullRank() )    /* matrix is rank deficient */
    {
      return TNT::Vector<Real>();
    }
    TNT::Vector<Real> x = b.copy();
    // Compute Y = transpose(Q)*b
    for (int k = 0; k < n; k++)
    {
      Real s = 0.0;
      for (int i = k; i < m; i++)
      {
        s += QR_(i+1,k+1)*x[i];
      }
      s = -s/QR_(k+1,k+1);
      for (int i = k; i < m; i++)
      {
        x[i] += s*QR_(i+1,k+1);
      }
    }
    // Solve R*X = Y;
    for (int k = n-1; k >= 0; k--)
    {
      x[k] /= Rdiag[k];
      for (int i = 0; i < k; i++) {
        x[i] -= x[k]*QR_(i+1,k+1);
      }
    }
    /* return n x nx portion of X */
    TNT::Vector<Real> x_(n);
    for (int i=0; i<n; i++)
      x_[i] = x[i];
    return x_;
  }

  /** Least squares solution of A*X = B
     @param B     m x k Array (must conform).
     @return X     n x k Array that minimizes the two norm of Q*R*X-B. If
                 B is non-conformant, or if QR.isFullRank() is false,
              the routine returns a null (0x0) array.
  */
  TNT::Fortran_Matrix<Real> solve(const TNT::Fortran_Matrix<Real> &B) const
  {
    if (B.num_rows() != m)    /* arrays must be conformant */
      return TNT::Fortran_Matrix<Real>(0,0);
    if ( !isFullRank() )    /* matrix is rank deficient */
    {
      return TNT::Fortran_Matrix<Real>(0,0);
    }
    int nx = B.num_cols();
    TNT::Fortran_Matrix<Real> X = B.copy();
    int i=0, j=0, k=0;
    // Compute Y = transpose(Q)*B
    for (k = 0; k < n; k++) {
      for (j = 0; j < nx; j++) {
        Real s = 0.0;
        for (i = k; i < m; i++) {
          s += QR_(i+1,k+1)*X(i+1,j+1);
        }
        s = -s/QR_(k+1,k+1);
        for (i = k; i < m; i++) {
          X(i+1,j+1) += s*QR_(i+1,k+1);
        }
      }
    }
    // Solve R*X = Y;
    for (k = n-1; k >= 0; k--) {
      for (j = 0; j < nx; j++) {
        X[k][j] /= Rdiag[k];
      }
      for (i = 0; i < k; i++) {
        for (j = 0; j < nx; j++) {
          X(i+1,j+1) -= X[k][j]*QR_(i+1,k+1);
        }
      }
    }
    /* return n x nx portion of X */
    TNT::Fortran_Matrix<Real> X_(n,nx);
    for (i=0; i<n; i++)
      for (j=0; j<nx; j++)
        X(i+1,j+1) = X(i+1,j+1);
    return X_;
  }
};
}
// namespace JAMA

#endif
// JAMA_QR__H
