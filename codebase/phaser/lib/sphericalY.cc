#include <phaser/lib/sphericalY.h>
#include <scitbx/constants.h>
#include <scitbx/array_family/misc_functions.h>
#include <phaser/lib/func_ops.h>
#include <phaser/io/Errors.h>
#include <float.h>

namespace phaser {

p3Dp toPolar(p3Dc x)
{
  p3Dp res;
  floatType ct;
  res.r = sqrt(x.x*x.x+x.y*x.y+x.z*x.z);
  ct = x.z/res.r;
  res.theta = acos(ct);
  if (res.theta == 0) res.phi=0;
  else res.phi = atan2(x.y,x.x);
  return res;
}

p3Dc toCart(p3Dp x)
{
  p3Dc res;
  res.x=x.r*cos(x.phi)*sin(x.theta);
  res.y=x.r*sin(x.phi)*sin(x.theta);
  res.z=x.r*cos(x.theta);
  return res;
}

floatType legendre(floatType x, int l, int m)
{
  /* Calculates associated Legendre polynomials from the recurrence relation
     (l-m)P(m,l)=x(2l-1)P(m,l-1)-(l+m-1)P(m,l-2)
     Written 09.01.2002
     Tested for stability 09.01.2002
     Change to floatType 24.01.2002
  */
  floatType Pmm(1);
  if (m>0) {
    int odd_fact=1;
    floatType sqRoot(sqrt(1.0-x*x));
    for (int i=1; i<=m; i++) {
      Pmm *= -sqRoot*odd_fact;
      odd_fact += 2;
    }
  }
  if (l==m) return Pmm;
  floatType Pmm1=x*(2*m+1)*Pmm;
  if (l==m+1) return Pmm1;
  floatType Pmj(0);
  for (int j=m+2; j<=l; j++) {
    Pmj = (x*(2*j-1)*Pmm1-(j+m-1)*Pmm)/(j-m);
    Pmm = Pmm1;  //increase index by one
    Pmm1 = Pmj;
  }
  return Pmj;
}

floatType spherical(int l, int m, floatType theta,lnfactorial& lnfac)
{
  /* Calculates spherical harmonics from Legendre Polynomials
     Written 09.01.2002
     Updated Log factorial 24.01.2002
     Tested for stability 24.01.2002 - works for l=500
  */
  floatType normalization = sqrt((2*l+1)/scitbx::constants::four_pi);
  floatType scale = normalization * exp(0.5*(lnfac.get(l-scitbx::fn::absolute(m))-lnfac.get(l+scitbx::fn::absolute(m))));
  //floatType scale = normalization * sqrt(ThrdSafeFact(l-scitbx::fn::absolute(m))/ThrdSafeFact(l+scitbx::fn::absolute(m)));
  return scale*legendre(cos(theta),l,m);
}

#define __DO_ELMN_NUMERICAL_STABILITY_TEST__

floatType sphbessel(int n, floatType x)
{
  /* Calculates spherical Bessel polynomials from the recurrence relation
     j(n)=(2n-1)/x j(n-1)-j(n-2)
     For x>n use the forward recurrence
     For x<n use the backward recurrence
     Written 10.01.2002
     Tested for stability 10.01.2002
  */
  floatType DBL_FAC(1.0e15);
  floatType BIGNUM(DBL_MAX/DBL_FAC);
  floatType BIGLOG(std::log(BIGNUM));
  floatType SMALLNUM(DBL_MIN*DBL_FAC);

  floatType j0(0), j1(0), jn(0), norm(0),res(0);
  int m(0);
  if (x<SMALLNUM) {
    if (n==0) return 1;
    else return 0;
  }

  int SD = 6;

  if (x>static_cast<floatType>(n)) {
    floatType sinxonx(sin(x)/x);
    j0=sinxonx;
    if (n==0) return j0;
    else {
      j1=sinxonx/x-cos(x)/x;
      if (n==1) return j1;
      else {
        for (int i=2; i<=n; i++) {
          jn = (2*i-1)/x*j1-j0;   // recurrence
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
          if (jn > BIGNUM)
          return 0;
#endif
          j0 = j1;  //increase index by one
          j1 = jn;
        }
        return jn;
      }
    }
  }
  else {   //recurrence is unstable use inverse recurrence
    m = 2*((n+static_cast<int>(SD*sqrt(static_cast<floatType>(n))))/2); //starting value
    jn = 0.0;
    j1 = 1.0;
    res = 0.0;
    for (int i=m; i>0; i--) {
      j0 = (2*i+1)/x*j1-jn; // inverse recurrence
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
      if (j0 > BIGNUM)
      return 0;
#endif
      jn = j1;
      j1 = j0;
      if (i==n) res = jn; // this is the one we want but unnormalized
    }
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
    if (j1 > BIGNUM)
    return 0;
#endif
    //****TEST FOR J1 = 0! ***
    if (j1 == 0) return 0;
    norm=(sin(x)/x)/j1; // compare j(0) calc with the real one
#ifdef __DO_ELMN_NUMERICAL_STABILITY_TEST__
    floatType absnorm(fabs(norm));
    if (absnorm > 1)
      if (std::log(fabs(res)) > BIGLOG-std::log(absnorm))
        return 0;
#endif
    res *= norm; // and normalize
    return res;
  }
}

}
