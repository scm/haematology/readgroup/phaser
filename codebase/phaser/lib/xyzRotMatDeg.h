#ifndef __PHASER_XYZROTMAT__
#define __PHASER_XYZROTMAT__
#include <phaser/main/Phaser.h>

namespace phaser {
  dmat33 xyzRotMatDeg(int, floatType, bool=false, bool=false);
  floatType xyzAngle(dvect3);
}

#endif
