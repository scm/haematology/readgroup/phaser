#ifndef __PHASER_ROTDIST__
#define __PHASER_ROTDIST__
#include <phaser/main/Phaser.h>

namespace phaser {

  floatType rotmatangleRAD(dmat33);
  floatType rotmatangleDEG(dmat33);
  floatType rotmatdistRAD(dmat33,dmat33);
  floatType rotmatdistDEG(dmat33,dmat33);
  floatType rotdistRAD(dvect3,dvect3);
  floatType rotdistDEG(dvect3,dvect3);
  floatType rotation_angle_cosine(const dmat33& rotation);

  namespace rotation {

  class RotationRMS
  {
    protected:
        dmat33 transposed_reference_;

    protected:
        floatType abs_sin_half_angle_from_cos_angle(floatType cosine) const;

    public:
        RotationRMS();
        ~RotationRMS();

        // Get distance
        floatType distance_from_reference(const dmat33& rotation) const;
        virtual floatType rotation_rmsd(const dmat33& rotation) const = 0;
  };

  class IsotropicObject : public RotationRMS
  {
    private:
        floatType factor_;

    public:
        IsotropicObject(floatType radius);
        ~IsotropicObject();

        // Accessors
        void set_reference(const dmat33& reference);

        // Rmsd of rotation
        floatType rotation_rmsd(const dmat33& rotation) const;
        floatType max_rmsd_for_angle(floatType degrees) const;

        // Constants
        static const floatType PROPORTIONALITY;
  };

  class AnisotropicObject : public RotationRMS
  {
    private:
        dmat33 reference_;
        dvect3 a_direction_;
        dvect3 b_direction_;
        dvect3 c_direction_;
        floatType a_sq_;
        floatType b_sq_;
        floatType c_sq_;
    public:
        AnisotropicObject(
            const dmat33& orientation,
            floatType a,
            floatType b,
            floatType c
            );
        ~AnisotropicObject();

        // Accessors
        void set_reference(const dmat33& reference);

        // Rmsd of rotation
        floatType rotation_rmsd(const dmat33& rotation) const;
        floatType axis_angle_rmsd(const dvect3& axis, floatType angle) const;
        floatType max_rmsd_for_angle(floatType degrees) const;

        // Constants
        static const floatType PROPORTIONALITY;
  };

  } // namespace rotation

} //end namespace phaser

#endif
