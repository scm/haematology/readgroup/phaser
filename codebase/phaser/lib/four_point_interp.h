#ifndef __PHASER_MAP_FOUR_POINT_INTERP__
#define __PHASER_MAP_FOUR_POINT_INTERP__
#include <scitbx/array_family/shared.h>
#include <scitbx/array_family/tiny_algebra.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <scitbx/array_family/accessors/c_grid_padded_periodic.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <scitbx/fftpack/gridding.h>

namespace phaser {

    template <typename T>
    class four_point_interpolation
    {
      public:
        // commented out to avoid compiler warnings
        // four_point_interpolation() {};

        four_point_interpolation(
          af::const_ref<T, scitbx::af::c_grid_padded_periodic<3> > const& a)
        :
          a_(a),
          amax_(a.accessor().focus()[1]),
          cmax_(a.accessor().focus()[2])
        {
         // PHASER_ASSERT(a.accessor().focus()[0] == 1); //never been encountered
        }

        T
        operator()(T const& alpha, T const& gamma)
        {
          int i = static_cast<int>(std::floor(alpha * amax_));
          int j = static_cast<int>(std::floor(gamma * cmax_));
          T u = alpha * amax_ - i;
          T v = gamma * cmax_ - j;
          return (1-u)*(1-v)*a_(0,i,j)
                + u*(1-v)*a_(0,i+1,j)
                + u*v*a_(0,i+1,j+1)
                + (1-u)*v*a_(0,i,j+1);
        }

      protected:
        af::const_ref<T, scitbx::af::c_grid_padded_periodic<3> > a_;
        T amax_;
        T cmax_;
    };
}

#endif
