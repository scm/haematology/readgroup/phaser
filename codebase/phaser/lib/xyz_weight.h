//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_XYZ_WEIGHTS__
#define __PHASER_XYZ_WEIGHTS__
#include <phaser/main/Phaser.h>

namespace phaser {

class xyzw {

  public:
    xyzw(dvect3 a=dvect3(0,0,0),double b=0,bool m=true): xyz(a),weight(b) {}

  dvect3 xyz;
  double weight;
};

class xyz_weight {

  public:
  xyz_weight(int a=0)
  {
    cw.resize(a);
    moment = dvect3(0,0,0);
    PR = dmat33(1,0,0,0,1,0,0,0,1);
    PT = dvect3(0,0,0);
    CENTRE = dvect3(0,0,0);
    EXTENT = dvect3(0,0,0);
    PRINCIPAL_CENTRE = dvect3(0,0,0);
    PRINCIPAL_EXTENT = dvect3(0,0,0);
    MAX_EXTENT = 0;
  }

  private:
    af::shared<xyzw>  cw;
    dvect3            moment;

  public:
    dmat33  PR;
    dvect3  PT;
    dvect3  CENTRE;
    dvect3  EXTENT;
    dvect3  PRINCIPAL_CENTRE;
    dvect3  PRINCIPAL_EXTENT;
    double  MAX_EXTENT;

  public:
    // vector interface
    typedef std::vector<xyzw>::value_type value_type;
    typedef std::vector<xyzw>::size_type size_type;
    typedef std::vector<xyzw>::reference reference;
    typedef std::vector<xyzw>::const_reference const_reference;

    template<typename InputIterator>
    xyz_weight(InputIterator first, InputIterator last) : cw( first, last ) {}

    const_reference operator[](size_type t) const   { return cw[t]; }
    reference operator[](size_type t)               { return cw[t]; }
    size_type size() const                          { return cw.size(); }
    void push_back(const value_type& t)             { cw.push_back(t); }
    void resize(size_type t)                        { cw.resize(t); }
    void clear()                                    { cw.clear(); }

    xyz_weight& operator+=(const xyz_weight &rhs)
    {
      for (int i = 0; i < rhs.cw.size(); i++)
        cw.push_back(rhs.cw[i]);
      return *this;
     }

  private:
    dvect3                    centre();
    std::pair<dmat33,dvect3>  principal(); //also calculates moment
    dvect3                    box();

  public:
    void                      calculate();
    std::pair<dvect3,dvect3>  box_min_max();
    dvect3                    get_moment() { return moment; }
    double                 mean_radius();

    std::vector<std::pair<double,double> >  density();
};

}

#endif
