#if defined(__sgi) && defined(_COMPILER_VERSION) && _COMPILER_VERSION == 741
#include <complex>
#endif
#include <phaser/lib/safe_mtz.h>
#include <phaser/io/Errors.h>
#include <cmtzlib.h>
#include <mtzdata.h>

namespace phaser {

CMtz::MTZ*
safe_mtz_get(std::string const& HKLIN, int read_refs)
{
  CMtz::MTZ *result = CMtz::MtzGet(HKLIN.c_str(), read_refs);
  if (result == 0) {
    throw PhaserError(FILEOPEN,HKLIN);
  }
  return result;
}

CMtz::MTZCOL*
safe_mtz_col_lookup(const CMtz::MTZ* mtzfrom, std::string const& label)
{
  CMtz::MTZCOL* result = CMtz::MtzColLookup(mtzfrom, label.c_str());
  if (result == 0) {
    throw PhaserError(FATAL,"Cannot find MTZ column \"" + label +"\"");
  }
  return result;
}

int MtzReadRefl(CMtz::MTZ *mtz, float adata[], int logmss[], CMtz::MTZCOL *lookup[], int ncols, int iref)
{
  int icol(0);
  unsigned int colin(0);
  float refldata[MCOLUMNS];
  union float_uint_uchar uf;

  /* If we are past the last reflection, indicate this with return value. */
  if (iref > mtz->nref_filein || iref <= 0)
    return 1;

  /* If reflections not in memory, read next record from file. */
  if (!mtz->refs_in_memory) {
    if (CMtz::MtzRrefl( mtz->filein, mtz->ncol_read, refldata) == EOF) return 1;
  }

  if (strncmp (mtz->mnf.amnf,"NAN",3) == 0) {
    uf = CCP4::ccp4_nan();
  } else {
    uf.f = mtz->mnf.fmnf;
  }

  /* loop over columns requested in lookup array. */
  for (icol=0; icol < ncols; icol++) {
    logmss[icol] = 1;
    if (lookup[icol]) {
      if (mtz->refs_in_memory) {
        adata[icol] = lookup[icol]->ref[iref-1];
        logmss[icol] = CMtz::ccp4_ismnf(mtz, adata[icol]);
      } else {
        colin = lookup[icol]->source;
        if (colin) {
          adata[icol] = refldata[colin - 1];
          logmss[icol] = CMtz::ccp4_ismnf(mtz, adata[icol]);
              } else {
          adata[icol] = uf.f;
          logmss[icol] = 1;
              }
      }
    }
  }

  return 0;
}

}//phaser
