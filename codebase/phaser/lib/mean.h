#ifndef __PHASER_MEAN__
#define __PHASER_MEAN__
#include <scitbx/array_family/shared.h>

namespace phaser {

  template <class T>
  T max(const std::vector<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;
    T max_val(List[0]);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++)
      max_val = std::max(max_val,List[i]);
    return max_val;
  }

  template <class T>
  T min(const std::vector<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;
    T min_val(List[0]);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++)
      min_val = std::min(min_val,List[i]);
    return min_val;
  }

  template <class T>
  T mean(const std::vector<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;
    T min_val = min(List, upto);
    T max_val = max(List, upto);
    //distribute roughly about zero
    T sub_val = (max_val+min_val)/2.0;

    //Calculate mean and stddev of sites
    floatType total(0);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++)
    {
      total += List[i]-sub_val;
    }
    return n ? (total/n + sub_val) : 0 ;
  }

  template<class T>
  T sigma(const std::vector<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;
    T min_val = min(List, upto);
    T max_val = max(List, upto);
    //distribute roughly about zero
    T sub_val = (max_val+min_val)/2.0;

    //Calculate mean and stddev of sites
    floatType mean(0),totalsqr(0),total(0);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++) //all rot
    {
      total += List[i]-sub_val;
      totalsqr += fn::pow2(List[i]-sub_val);
    }
    mean = total/n + sub_val;
    totalsqr /= n;
    //use sigma n-1 as this is a sample not population
    return (n-1) ? (n/(n-1))*std::sqrt(totalsqr-fn::pow2(mean-sub_val)) : 0;
  }

  template <class T>
  T mean(scitbx::af::shared<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;

    //Calculate mean and stddev of sites
    T total(0);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++)
    {
      total += List[i];
    }
    return n ? total/n : 0 ;
  }

  template<class T>
  T sigma(scitbx::af::shared<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;

    //Calculate mean and stddev of sites
    T mean(0),sigma(0),totalsqr(0),total(0);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++) //all rot
    {
      total += List[i];
      totalsqr += fn::pow2(List[i]);
    }
    mean = total/n;
    totalsqr /= n;
    //use sigma n-1 as this is a sample not population
    sigma = (n-1) ? (n/(n-1))*std::sqrt(totalsqr-fn::pow2(mean)) : 0;
    return sigma;
  }

  template <class T>
  T sum(std::vector<T>& List, size_t upto = 0)
  {
    if (!List.size()) return 0;
    T sum_val(0);
    double n = upto ? std::min(upto,List.size()) : List.size();
    for (unsigned i = 0; i < n; i++)
      sum_val += List[i];
    return sum_val;
  }

}

#endif
