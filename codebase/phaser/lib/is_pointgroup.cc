#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/is_pointgroup.h>
#include <phaser/lib/rotdist.h>
#include <phaser/lib/euler.h>

namespace phaser {

is_pointgroup::is_pointgroup(std::vector<dvect3> PG_EULER, std::vector<dvect3> PG_ORTH, dvect3 centre)
{
  PHASER_ASSERT(PG_EULER.size() == PG_ORTH.size());
  tol_rot = 3; tol_orth = 0.3;
  ispg = true;
  moves_centre_dist = 0;
  moves_centre = inversion = unpaired = identity = false;

  // Check that operators include the identity
  for (int isym = 0; isym < PG_EULER.size(); isym++)
  {
    floatType tol(0.01);
    if (rotmatangleDEG(euler2matrixDEG(PG_EULER[isym])) < tol &&
        PG_ORTH[isym].length() < tol)
      {
        identity = true;
        break;
      }
  }

  // Check that operators don't move centre of object
  for (int isym = 0; isym < PG_EULER.size(); isym++)
  {
    dmat33 rmat1 = euler2matrixDEG(PG_EULER[isym]);
    dvect3 symcentre = rmat1*centre + PG_ORTH[isym];
    dvect3 shift = symcentre-centre;
    moves_centre_dist = std::max(shift.length(),moves_centre_dist);
    if (shift.length() > tol_orth)
    {
      ispg = false;
      moves_centre = true;
      break;
    }
  }

  // Check that each operator has an inverse
  for (int isym1 = 0; isym1 < PG_EULER.size(); isym1++)
  {
    dmat33 rmat1 = euler2matrixDEG(PG_EULER[isym1]);
    bool found(false);
    for (int isym2 = 0; isym2 < PG_EULER.size(); isym2++)
    {
      dmat33 rmat2 = euler2matrixDEG(PG_EULER[isym2]);
      dmat33 dmat = rmat1*rmat2;
      if (rotmatangleDEG(dmat) < tol_rot)
      {
        found = true;
        break;
      }
    }
    if (!found)
    {
      ispg = false;
      inversion = true;
    }
  }

  // Check that each pair of operators generates another
  for (int isym1 = 0; isym1 < PG_EULER.size(); isym1++)
  {
    dmat33 rmat1 = euler2matrixDEG(PG_EULER[isym1]);
    for (int isym2 = 0; isym2 < PG_EULER.size(); isym2++)
    {
      dmat33 rmat2 = euler2matrixDEG(PG_EULER[isym2]);
      dmat33 prod12 = rmat1*rmat2;
      bool found(false);
      for (int isym3 = 0; isym3 < PG_EULER.size(); isym3++)
      {
        dmat33 rmat3 = euler2matrixDEG(PG_EULER[isym3]);
        if (rotmatdistDEG(prod12,rmat3) < tol_rot)
        {
          found = true;
          break;
        }
      }
      if (!found)
      {
        ispg = false;
        unpaired = true;
      }
    }
  }
}

std::string is_pointgroup::warning()
{
  return "Point group centre of mass has moved by " + dtos(moves_centre_dist) + "Angstroms";
}

std::string is_pointgroup::message()
{
  if (!identity)    return "Point group symmetry does not have identity operator";
  if (moves_centre) return "Point group symmetry moves centre";
  if (inversion)    return "Point group symmetry operator cannot be inverted";
  if (unpaired)     return "Point group symmetry pairs do not generate new operator";
  return "";
}

}
