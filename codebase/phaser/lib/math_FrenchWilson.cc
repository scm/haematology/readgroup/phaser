#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/math_ParabolicCylinderD.h>
#include <phaser/io/Errors.h>

namespace phaser {
const ParabolicCylinderD tbl_pcf = ParabolicCylinderD();

floatType expectEFWacen(floatType eosq, floatType sigesq)
{
/* Acentric: Compute French & Wilson posterior expected value of E, from the
   normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
*/

  const floatType CROSSOVER1(-12.5), CROSSOVER2(18.);
  const floatType SQRT2(std::sqrt(2.));

  floatType ee;
  floatType x((eosq-fn::pow2(sigesq))/sigesq);
  floatType xsqr(fn::pow2(x));

  if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
    ee = std::sqrt(-scitbx::constants::pi*sigesq/x) *
         (-916620705. + xsqr *
         (91891800.   + xsqr *
         (-11531520.  + xsqr *
         (1935360.    + xsqr *
         (-491520.    + xsqr * 262144.))))) /
         (-495452160. + xsqr *
         (55050240.   + xsqr *
         (-7864320.   + xsqr *
         (1572864.    + xsqr *
         (-524288.    + xsqr * 524288.)))));
  else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
    ee = std::sqrt(sigesq) *
         (-45045. + 32.*xsqr *
         (-315.    + 8.*xsqr *
         (-15.    - 16.*xsqr + 128.*fn::pow2(xsqr)))) /
         (32768*std::pow(x,7.5));
  else // Moderate arguments: analytical integral
    ee = std::sqrt(sigesq/2.) * std::exp(-xsqr/4.) *
         tbl_pcf.get(-1.5,-x) / scitbx::math::erfc(-x/SQRT2);

  return ee;
}

floatType expectEsqFWacen(floatType eosq, floatType sigesq)
{
/* Acentric: Compute French & Wilson posterior expected value of E^2, from the
   normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
*/

  const floatType CROSSOVER1(-8.9), CROSSOVER2(5.7);
  const floatType SQRT2BYPI(std::sqrt(2./scitbx::constants::pi));
  const floatType SQRT2(std::sqrt(2.));

  floatType eesq((eosq-fn::pow2(sigesq))); // Default for significantly positive x
  floatType x(eesq/(SQRT2*sigesq));
  floatType xsqr(fn::pow2(x));

  if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
    eesq *= (-135135 + xsqr *
            (20790   + xsqr *
            (-3780   + xsqr *
            (840     + xsqr *
            (-240    + xsqr *
            (96      - xsqr * 64)))))) /
            (-135135 + xsqr *
            (20790   + xsqr *
            (-3780   + xsqr *
            (840     + xsqr *
            (-240    + xsqr *
            (96      + xsqr *
            (-64     + xsqr * 128)))))));
  else if (x <= CROSSOVER2) // Moderate arguments: analytical integral
    eesq += SQRT2BYPI * sigesq / (std::exp(xsqr) * scitbx::math::erfc(-x));

  return eesq;
}

floatType expectEFWcen(floatType eosq, floatType sigesq)
{
/* Centric: Compute French & Wilson posterior expected value of E, from the
   normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
*/

  const floatType CROSSOVER1(-17.5), CROSSOVER2(17.5);
  const floatType SQRTPI(std::sqrt(scitbx::constants::pi));

  floatType pcdratio,ee;
  floatType x(sigesq/2.-eosq/sigesq);
  floatType xsqr(fn::pow2(x));

  if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
    pcdratio = (1024.*SQRTPI*std::pow(-x,6.5)) /
               (3465. + xsqr *
               (840.  + xsqr *
               (384.  + xsqr * 1024.)));
  else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
    pcdratio = (3440640. + xsqr *
               (-491520. + xsqr *
               (98304.   + xsqr *
               (-32768.  + xsqr * 32768.)))) /
               (675675.  + xsqr *
               (-110880. + xsqr *
               (26880.   + xsqr *
               (-12288.  + xsqr * 32768.)))) / std::sqrt(x);
  else // Moderate arguments: analytical integral
    pcdratio = tbl_pcf.get(-1.,x) / tbl_pcf.get(-0.5,x);

  ee = std::sqrt(sigesq/scitbx::constants::pi)*pcdratio;

  return ee;
}

floatType expectEsqFWcen(floatType eosq, floatType sigesq)
{
/* Centric: Compute French & Wilson posterior expected value of E^2, from the
   normalised observed intensity (Eobs^2=Iobs/<I>) and its standard deviation
*/

  const floatType CROSSOVER1(-17.5), CROSSOVER2(17.5);

  floatType pcdratio,eesq;
  floatType x(sigesq/2.-eosq/sigesq);
  floatType xsqr(fn::pow2(x));

  if (x < CROSSOVER1) // Large negative argument: asymptotic approximation
    pcdratio = (45045. + xsqr *
               (10080. + xsqr *
               (3840.  + xsqr *
               (4096.  - xsqr * 32768.)))) /
               (x *
               (55440. + xsqr *
               (13440. + xsqr *
               (6144.  + xsqr * 16384.))));
  else if (x > CROSSOVER2) // Large positive argument: asymptotic approximation
    pcdratio = (11486475. + xsqr *
               (-1441440. + xsqr *
               (241920.   + xsqr *
               (-61440.   + xsqr * 32768.)))) /
               (x *
               (675675.   + xsqr *
               (-110880.  + xsqr *
               (26880.    + xsqr *
               (-12288.   + xsqr * 32768.)))));
  else // Moderate arguments: analytical integral
    pcdratio = tbl_pcf.get(-1.5,x) / tbl_pcf.get(-0.5,x);

  eesq = sigesq*pcdratio/2.;

  return eesq;
}

floatType expectEFW(floatType eosq, floatType sigesq, bool centric)
{
  if (sigesq <= 0.) // Apparently no measurement error
  {
    PHASER_ASSERT(eosq>=0.); // Can only allow zero sigma for non-negative I
    return std::sqrt(eosq);
  }
  floatType eEFW;
  if (centric) eEFW = expectEFWcen(eosq,sigesq);
  else         eEFW = expectEFWacen(eosq,sigesq);
  return eEFW;
}

floatType expectEsqFW(floatType eosq, floatType sigesq, bool centric)
{
  if (sigesq <= 0.)
  {
    PHASER_ASSERT(eosq>=0.);
    return eosq;
  }
  floatType eEsqFW;
  if (centric) eEsqFW = expectEsqFWcen(eosq,sigesq);
  else         eEsqFW = expectEsqFWacen(eosq,sigesq);
  return eEsqFW;
}

bool is_FrenchWilsonF(af_float F, af_float SIGF, bool1D is_centric)
{
  int nviolations(0);
  int NREFL(F.size());
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (F[r] <= 0. || SIGF[r] <= 0.) return false; // French-Wilson always positive
    floatType SIGFoverF(SIGF[r]/F[r]);
    // After French-Wilson, SIGF/F has a maximum for centrics and acentrics
    if (SIGFoverF > 1) return false; // Don't expect any big violations.
    // Maximum ratio of SIGF to F in FW posterior will occur for no information
    // i.e. posterior = Wilson prior, where <E> and Sqrt(<(E-<E>)^2>) are
    //   Sqrt(2/Pi) and Sqrt(1-2/Pi) for centrics, and
    //   Sqrt(Pi)/2 and Sqrt(1-Pi/4) for acentrics
    floatType maxrat = (is_centric[r]) ? 0.756 : 0.523; // Slightly generous to allow for rounding and numerical integration error
    if (SIGFoverF > maxrat) nviolations ++;
  }
  floatType fracviol(floatType(nviolations)/NREFL);
  // std::cout << "Fraction of FW violations: " << fracviol << std::endl;
  return (fracviol <= 0.005);
}
bool is_FrenchWilsonI(af_float I, af_float SIGI, bool1D is_centric)
{
  int nviolations(0);
  int NREFL(I.size());
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (I[r] <= 0. || SIGI[r] <= 0.) return false; // French-Wilson always positive
    floatType SIGIoverI(SIGI[r]/I[r]);
    // After French-Wilson, SIGI/I has a maximum for centrics and acentrics
    if (SIGIoverI > 2.0) return false; // Don't expect any big violations.
    // Maximum ratio of SIGI to I in FW posterior will occur for no information
    // i.e. posterior = Wilson prior, where <E^2>=1 and Sqrt(<(E^2-1)^2>) is
    //   Sqrt(2) for centrics, and 1 for acentrics
    floatType maxrat = (is_centric[r]) ? 1.415 : 1.001; // Slightly generous again
    if (SIGIoverI > maxrat) nviolations ++;
  }
  floatType fracviol(floatType(nviolations)/NREFL);
  // std::cout << "Fraction of FW violations: " << fracviol << std::endl;
  return (fracviol <= 0.005);
}

}
