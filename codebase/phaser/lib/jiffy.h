#ifndef __PHASER_JIFFY__
#define __PHASER_JIFFY__
#include <phaser/main/Phaser.h>
#include <phaser/lib/vec.h>
#include <sstream>

namespace phaser {

std::string stos(const std::string&,int);
std::string dtos(const float&);
std::string dtos(const double&);
std::string dtos(const double&,int);
std::string dtos(const double&,int,int,bool=false);
std::string etos(const double&,int,int,bool=false);
std::string dvtos(const scitbx::vec3<float>&);
std::string dvtos(const scitbx::vec3<double>&,bool=false);
std::string dvtos(const scitbx::vec3<double>&,int,int);
std::string dvtos(const scitbx::vec3<double>&,int);
std::string dvtos(const af::double6&);
std::string dvtos(const af::shared<double>&);
std::string dvtos(const TNT::Vector<double>&);
std::string dvtos(const af::shared<double>&,int,int);
std::string dvtos(const std::vector<double>&);
std::string dvtos(const std::vector<double>&,int,int);
std::string ivtos(const scitbx::vec3<int>&);
std::string ivtos(const std::vector<int>&,char=' ',char=' ',char=' ');
std::string dvtos(const scitbx::vec3<double>&,char,char,char);
std::string cvtos(const scitbx::vec3<cmplxType>&,bool=false);
std::string cmtos(const scitbx::mat3<cmplxType>&);
std::string dmtos(const scitbx::mat3<float>&,int,int);
std::string dmtos(const scitbx::mat3<double>&,int,int);
std::string dmtos(const scitbx::mat3<double>&);
std::string dmtos(const scitbx::sym_mat3<double>&);
std::string dmtos(const std::vector<std::vector<double> >&);
std::string imtos(const scitbx::mat3<int>&);
std::string ftos(const float&);
std::string ntos(const int&);
std::string itos(const int&);
std::string brackets(const int&);
std::string itos(const int&,const int&,bool=false);
std::string itoaniso(const int&);
std::string btos(const bool&);
std::string ctos(const std::complex<float>&);
std::string ctos(const std::complex<double>&);
std::string stoup(const std::string&);
std::string stolo(const std::string&);
int         stod(const std::string&);
std::string stoa(const std::string&);

bool isfloat(std::string);
floatType fmod_pos(floatType, floatType);

//! To avoid locale environment surprises (assumes ASCII or similar).
inline
bool
is_alpha(
  int c)
{
  return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
}

}//end namespace phaser

#endif
