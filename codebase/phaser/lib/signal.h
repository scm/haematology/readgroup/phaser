#ifndef __PHASER_SIGNAL__
#define __PHASER_SIGNAL__
#include <phaser/main/Phaser.h>

namespace phaser {

  floatType signal(floatType,floatType,floatType);

}//end namespace phaser

#endif
