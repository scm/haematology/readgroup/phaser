#include <limits>
#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <phaser/io/Errors.h>
#include <scitbx/constants.h>

namespace phaser {

floatType pRiceF(floatType F1, floatType DF2, floatType V)
{
/* Rice (Sim/Srinivasan) probability distribution of amplitude F1, given
   the amplitude of its expected value, DF2
*/
  PHASER_ASSERT(F1 >= 0);
  PHASER_ASSERT(DF2 >= 0);
  PHASER_ASSERT(V > 0);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  const floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  const floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType X(2*F1*DF2/V);
  floatType exparg(-fn::pow2(F1-DF2)/V);
  exparg = std::max(exparg,minExpArgHalf);
  return (2*F1/V)*std::exp(exparg)*eBesselI0(X);
}

floatType pRiceI(floatType I1, floatType D2I2, floatType V)
{
/* Rice (Sim/Srinivasan) probability distribution of intensity I1, given
   the square of the expected value of the corresponding structure factor
*/
  PHASER_ASSERT(I1 >= 0);
  PHASER_ASSERT(D2I2 >= 0);
  PHASER_ASSERT(V > 0);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  const floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  const floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType F1(std::sqrt(I1)),DF2(std::sqrt(D2I2));
  floatType X(2*F1*DF2/V);
  floatType exparg(-fn::pow2(F1-DF2)/V);
  return (exparg > minExpArgHalf) ? (1.0/V)*std::exp(exparg)*eBesselI0(X) : 0.;
}

floatType pWoolfsonF(floatType F1, floatType DF2, floatType V)
{
// Centric (Woolfson) equivalent of pRiceF
  PHASER_ASSERT(F1 >= 0. && DF2 >= 0. && V > 0.);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  const floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  const floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType X(F1*DF2/V);
  floatType eCoshX = (X < 15.0) ? std::exp(-X)*cosh(X) : 0.5;
  floatType prob(eCoshX/std::sqrt(V*scitbx::constants::pi_2));
  floatType exparg = -fn::pow2(F1-DF2)/(2*V);
  return (exparg > minExpArgHalf) ? prob*std::exp(exparg) : 0.;
}

floatType pWoolfsonI(floatType I1, floatType D2I2, floatType V)
{
// Centric (Woolfson) equivalent of pRiceI
  PHASER_ASSERT(I1 > 0. && D2I2 >= 0. && V > 0.);
  //Arguments for exponential will be negative, with the maximum typically close to zero.
  const floatType maxExpArg(std::log(std::numeric_limits<floatType>::max()));
  const floatType minExpArgHalf(-maxExpArg/2.); //minimum arg, below which probs will be set to zero
  floatType F1 = std::sqrt(I1);
  floatType DF2 = std::sqrt(D2I2);
  floatType X(F1*DF2/V);
  floatType eCoshX = (X < 15.0) ? std::exp(-X)*cosh(X) : 0.5;
  floatType prob(eCoshX/(F1*std::sqrt(V*scitbx::constants::two_pi)));
  floatType exparg = -fn::pow2(F1-DF2)/(2*V);
  return (exparg > minExpArgHalf) ? prob*std::exp(exparg) : 0.;
}

} //phaser
