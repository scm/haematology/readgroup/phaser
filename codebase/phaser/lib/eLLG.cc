#include  <phaser/main/Phaser.h>
#include  <phaser/lib/eLLG.h>
#include  <phaser/lib/maths.h>
#include  <phaser/mr_objects/rms_estimate.h>

namespace phaser {


double get_reso_for_eLLG(
   double ELLG_TARGET,
   std::vector<ssqr_dfac_solt> ssqr_dfac,
   double RMSD,
   double FP,
   float1D KRMSD,
   float1D KFP)
{
  assert(KRMSD.size()==KFP.size());
  double totalFP = FP;
  for (int m = 0; m < KFP.size(); m++)
    totalFP += KFP[m];
  if (totalFP > 1) return 0; //no room for this component, flag with return resolution 0
  //ssqr MUST BE SORTED
  double eLLG(0),ssmax(std::numeric_limits<floatType>::max());
  //assume the ssqr one is the one we are interested in, rest are known
  bool lastrefl(false);
  for (unsigned r = 0; r < ssqr_dfac.size(); r++)
  {
    floatType known_sigmaA_sqr(0);
    for (int m = 0; m < KRMSD.size(); m++)
      known_sigmaA_sqr += fn::pow2(ssqr_dfac[r].dfac_solt*std::sqrt(KFP[m])*DLuzzati(ssqr_dfac[r].ssqr,KRMSD[m]));
    floatType sigmaA_sqr = fn::pow2(ssqr_dfac[r].dfac_solt*std::sqrt(FP*ssqr_dfac[r].bratio)*DLuzzati(ssqr_dfac[r].ssqr,RMSD));
    eLLG += fn::pow2(known_sigmaA_sqr+sigmaA_sqr)/2. - fn::pow2(known_sigmaA_sqr)/2.;
    ssmax = ssqr_dfac[r].ssqr;
    if (eLLG > ELLG_TARGET) break;
    lastrefl = (r == ssqr_dfac.size()-1);
  }
  return  lastrefl ? 0: (1./std::sqrt(ssmax));
  //return 0 for all data
}

double get_eLLG(
   std::vector<ssqr_dfac_solt> ssqr_dfac,
   double RMSD,
   double FP,
   float1D KRMSD,
   float1D KFP)
{
  assert(KRMSD.size()==KFP.size());
  double eLLG(0.);
  for (unsigned r = 0; r < ssqr_dfac.size(); r++)
  {
    double sigmaA_sqr = fn::pow2(ssqr_dfac[r].dfac_solt*std::sqrt(FP*ssqr_dfac[r].bratio)*DLuzzati(ssqr_dfac[r].ssqr,RMSD));
    for (int k = 0; k < KRMSD.size(); k++)
      sigmaA_sqr += fn::pow2(ssqr_dfac[r].dfac_solt*std::sqrt(KFP[k])*DLuzzati(ssqr_dfac[r].ssqr,KRMSD[k]));
    eLLG += fn::pow2(sigmaA_sqr)/2.;
  }
  return eLLG; //total eLLG using all the data
}

}//end namespace phaser
