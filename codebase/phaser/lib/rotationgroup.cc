#include <phaser/main/Phaser.h>
#include <phaser/lib/rotationgroup.h>
#include <phaser/lib/func_ops.h>
#include <phaser/lib/sphericalY.h>
#include <scitbx/constants.h>
#include <scitbx/array_family/misc_functions.h>
#include <float.h>

namespace phaser {

floatType djmn_direct(int& j, int& m, int& n, floatType& beta,lnfactorial& lnfac)
{
  /* Calculates d-Functions directly
     Written 25.01.2002
     Tested for Stability 25.01.2002
     NOTE TO SELF: Only need for m>=0 and n in range [-m,m]
     use djnm = (-1)^(m-n) djmn = dj-m-n to get to other elements
     THIS STILL NEEDS IMPLEMENTATION
  */

  int t(0);
  floatType num(0), denom(0), c(0), s(0), res(0);
  res = 0.0;
  for (t=0; t <= 2*j; t++) {
    if (j+m-t<0 || j-n-t<0 || n-m+t<0) continue;
    else {
      num = 0.5*(lnfac.get(j+m)+lnfac.get(j-m)+lnfac.get(j+n)+lnfac.get(j-n));
      denom = lnfac.get(j+m-t)+lnfac.get(j-n-t)+lnfac.get(t)+lnfac.get(t+n-m);
 //     num = 0.5*(log(ThrdSafeFact(j+m))+log(ThrdSafeFact(j-m))+log(ThrdSafeFact(j+n))+log(ThrdSafeFact(j-n)));
 //     denom = log(ThrdSafeFact(j+m-t))+log(ThrdSafeFact(j-n-t))+log(ThrdSafeFact(t))+log(ThrdSafeFact(t+n-m));
      c = pow(cos(beta/2),2*j+m-n-2*t);
      s = pow(sin(beta/2),2*t+n-m);
      res += pow(-1.0,t)*c*s*std::exp(num-denom);
    }
  }
  return res;
}

floatType djmn(int& j, int& m, int& n, floatType& beta,lnfactorial& lnfac)
{
  return djmn_direct(j, m, n, beta, lnfac);
}

float2D djmn_recursive_table(int& j, floatType& beta,lnfactorial& lnfac)
{
  floatType cosbeta = cos(beta/2.0);
  floatType sinbeta = sin(beta/2.0);
  float2D table;
  table.resize(2*j+1);
  for (int i=0; i<2*j+1; i++)
    table[i].resize(2*j+1);
  if (beta == 0) {
    for (int i=0; i<2*j+1; i++)
      table[i][i] = 1;
    return table;
  }
  floatType djm_nm(0), djm_n(0), djm_np(0), csb(0);

  floatType dbl_fac(1.0e15);
  floatType BIGNUM(DBL_MAX/dbl_fac);
  floatType logBIGNUM(log10(BIGNUM));

  for (int m = -j; m <= j; m++) {
    // Calculate d(j,m,j)(beta)
    djm_np = 0.0;
    floatType powcosbeta(0);
    if (fabs(cosbeta) > 0.01 || fabs(log10(fabs(cosbeta))*(j+m)) < logBIGNUM) powcosbeta = pow(cosbeta,j+m);
    floatType powsinbeta(0);
    if (fabs(sinbeta) > 0.01 || fabs(log10(fabs(sinbeta))*(j-m)) < logBIGNUM) powsinbeta = pow(sinbeta,j-m);
    csb = powsinbeta*powcosbeta;
    floatType tmp(std::exp(0.5*(lnfac.get(2*j)-lnfac.get(j+m)-lnfac.get(j-m))));
   // floatType tmp(sqrt(ThrdSafeFact(2*j))/sqrt(ThrdSafeFact(j+m)*ThrdSafeFact(j-m)));

    if (fabs(csb) > LDBL_MIN*dbl_fac) djm_n = tmp*csb;
    else if (fabs(csb) <= LDBL_MIN*dbl_fac) djm_n = 0;
    if (fabs(djm_n) < DBL_MIN) djm_n = 0;

    table[j+m][j+j] = djm_n;
    table[j-m][j-j] = djm_n*pow(-1.,j-m);
    table[j+j][j+m] = djm_n*pow(-1.,j-m);
    table[j-j][j-m] = djm_n;

    for (int l = j-1; l >= scitbx::fn::absolute(m); l--) {
      djm_nm = -(sqrt(static_cast<floatType>((j+l+2)*(j-l-1)))*
                 djm_np+2*(m-(l+1)*cos(beta))*djm_n/sin(beta))
                /sqrt(static_cast<floatType>((j-l)*(j+l+1)));
      djm_np = djm_n;
      djm_n = djm_nm;
      table[j+m][j+l] = djm_n;
      table[j-m][j-l] = djm_n*pow(-1.,m-l);
      table[j+l][j+m] = djm_n*pow(-1.,m-l);
      table[j-l][j-m] = djm_n;
    }
  }
  return table;
}

floatType djmn_recursive(int& j, int m, int n, floatType& beta,lnfactorial& lnfac)
{
  /* Calculates d-Functions from the recurrence relation
     v/(j-n+1)(j+m)' d(j,m,n-1)=-v/(j+n+1)(j-n)' d(j,m,n+1)+2(m-n cos(b))/sin(b) d(j,m,n)
     Written 21.01.2002
     Corrected 28.01.2003
     Tested for stability (up to l=250) on 30.01.2002
     CURRENTLY UNSTABLE FOR LARGISH J

     NOTE TO SELF: Only need for m>=0 and n in range [-m,m]
     use djnm = (-1)^(m-n) djmn = dj-m-n to get to other elements
  */
  if(beta ==0){
    if (m == n) return 1;
    return 0;
  }
  floatType expftr=1;
  if (n<0) {
    expftr *= pow(-1.,n-m);
    n = -n;
    m = -m;
  }
  if (m>n) {
    expftr *= pow(-1.,m-n);
    int temp = m;
    m = n;
    n = temp;
  }
  if (m<-n) {
    int temp = m;
    m = -n;
    n = -temp;
  }

  floatType djm_nm(0), djm_n(0), djm_np(0), csb(0);
  djm_np = 0.0;
  csb = pow(sin(beta/2.0),j-m)*pow(cos(beta/2.0),j+m);
  const int MAX_FACT = 50; //to avoid overflow
  if (j<MAX_FACT)
    djm_n = sqrt(rfactorial(2*j))/sqrt(rfactorial(j+m)*rfactorial(j-m))*csb;
  else {
    djm_n = exp(0.5*(lnfac.get(2*j)-lnfac.get(j+m)-lnfac.get(j-m)))*csb;
   // djm_n = sqrt(ThrdSafeFact(2*j))/sqrt(ThrdSafeFact(j+m)*ThrdSafeFact(j-m))*csb;
  }
  if (j==n) return expftr*djm_n;
  else {
    for (int l = j; l > n;l--) {
      djm_nm = -(sqrt(static_cast<floatType>((j+l+1)*(j-l)))*
                 djm_np+2*
                 (m-l*cos(beta))*
                 djm_n/sin(beta))
                /sqrt(static_cast<floatType>((j-l+1)*(j+l)));
      djm_np = djm_n;
      djm_n = djm_nm;
    }
    return expftr*djm_n;
  }
}

}
