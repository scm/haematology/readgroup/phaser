#include <phaser/lib/rmdirs.h>

namespace phaser {

//-----------------------------------------------------------------------
// rmdirs - remove the leading directory tree from filename
//-----------------------------------------------------------------------
std::string rmdirs(const std::string & str)
{
  std::string t;
  std::string::size_type start = str.rfind("/")+1;
  if (start == std::string::npos) start = 0; //correct for no directory character found
  std::string::size_type length = str.size() - start;;
  t = str.substr(start,length);
  return t;
}

}
