#ifndef __PHASER_EULER__
#define __PHASER_EULER__
#include <phaser/main/Phaser.h>

namespace phaser {

  dmat33 euler2matrixRAD(dvect3);
  dmat33 polar2matrixRAD(dvect3);
  dvect3 matrix2eulerRAD(dmat33);
  dmat33 euler2matrixDEG(dvect3);
  dmat33 polar2matrixDEG(dvect3);
  dvect3 matrix2eulerDEG_old(dmat33);
  dvect3 matrix2eulerDEG(const dmat33&);

}//end namespace phaser

#endif
