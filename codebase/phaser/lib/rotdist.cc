#include <phaser/main/Phaser.h>
#include <phaser/lib/euler.h>
#include <scitbx/constants.h>
#include <phaser/lib/rotdist.h>
#include <scitbx/math/r3_rotation.h>

namespace phaser {

floatType rotmatangleRAD(dmat33 dmat)
{
  floatType cosdel = (dmat(0,0)+dmat(1,1)+dmat(2,2)-1.)/2.;
  floatType ONE(1.0);
  cosdel = std::max(cosdel,-ONE);
  cosdel = std::min(cosdel,ONE);
  return std::acos(cosdel);
}

floatType rotmatangleDEG(dmat33 dmat)
{
  return scitbx::rad_as_deg(rotmatangleRAD(dmat));
}

floatType rotmatdistRAD(dmat33 rmat1, dmat33 rmat2)
{
  dmat33 dmat = rmat2*rmat1.transpose();
  return rotmatangleRAD(dmat);
}

floatType rotmatdistDEG(dmat33 rmat1, dmat33 rmat2)
{
  return scitbx::rad_as_deg(rotmatdistRAD(rmat1,rmat2));
}

floatType rotdistRAD(dvect3 euler1, dvect3 euler2)
{
  dmat33 rmat1 = euler2matrixRAD(euler1);
  dmat33 rmat2 = euler2matrixRAD(euler2);
  return rotmatdistRAD(rmat1,rmat2);
}

floatType rotdistDEG(dvect3 euler1, dvect3 euler2)
{
  dmat33 rmat1 = euler2matrixDEG(euler1);
  dmat33 rmat2 = euler2matrixDEG(euler2);
  return rotmatdistDEG(rmat1,rmat2);
}

floatType
rotation_angle_cosine(const dmat33& rotation)
{
    return std::max(
        std::min( ( ( rotation.trace() - 1.0 ) / 2.0 ), 1.0 ),
        -1.0
        );
}

namespace rotation {

//
// Class RotationRMS
//
RotationRMS::RotationRMS()
    : transposed_reference_( dmat33( 1.0 ) )
{
}


RotationRMS::~RotationRMS()
{
}

// Public methods
floatType
RotationRMS::distance_from_reference(const dmat33& rotation) const
{
    dmat33 difference = rotation * transposed_reference_;
    return rotation_rmsd( difference );
}



// Protected methods
floatType
RotationRMS::abs_sin_half_angle_from_cos_angle(floatType cosine) const
{
    return std::sqrt( ( 1.0 - cosine ) / 2.0 );
}

//
// Class IsotropicObject
//
IsotropicObject::IsotropicObject(floatType radius)
    : factor_( radius * PROPORTIONALITY )
{
}


IsotropicObject::~IsotropicObject()
{
}

// Accessors
void
IsotropicObject::set_reference(const dmat33& reference)
{
    transposed_reference_ = reference.transpose();
}

// Public methods
floatType
IsotropicObject::rotation_rmsd(const dmat33& rotation) const
{
    floatType angle_cosine = rotation_angle_cosine( rotation );
    return factor_ * abs_sin_half_angle_from_cos_angle( angle_cosine ) ;
}

floatType
IsotropicObject::max_rmsd_for_angle(floatType degrees) const
{
    return factor_ * std::sin( scitbx::deg_as_rad( degrees ) / 2.0 );
}

const floatType IsotropicObject::PROPORTIONALITY = 2.0 / std::sqrt( 5.0 );

//
// Class AnisotropicObject
//
AnisotropicObject::AnisotropicObject(
    const dmat33& orientation,
    floatType a,
    floatType b,
    floatType c
    )
    : reference_( dmat33( 1.0 ) ),
        a_direction_( orientation.get_row( 0 ).normalize() ),
        b_direction_( orientation.get_row( 1 ).normalize() ),
        c_direction_( orientation.get_row( 2 ).normalize() ),
        a_sq_( a * a ),
        b_sq_( b * b ),
        c_sq_( c * c )
{
}


AnisotropicObject::~AnisotropicObject()
{
}

// Accessors
void
AnisotropicObject::set_reference(const dmat33& reference)
{
    reference_ = reference;
    transposed_reference_ = reference.transpose();
}

// Public methods
floatType
AnisotropicObject::rotation_rmsd(const dmat33& rotation) const
{
    struct scitbx::math::r3_rotation::axis_and_angle_from_matrix< floatType >
        aaa( rotation );

    return axis_angle_rmsd( aaa.axis, aaa.angle() );
}


floatType
AnisotropicObject::axis_angle_rmsd(const dvect3& axis, floatType angle) const
{
    dvect3 n_axis = axis.normalize();

    floatType u2 = std::min(
        std::pow( ( reference_ * a_direction_ ) * n_axis, 2 ),
        1.0
        );
    floatType v2 = std::min(
        std::pow( ( reference_ * b_direction_ ) * n_axis, 2 ),
        1.0
        );
    floatType w2 = std::min(
        std::pow( ( reference_ * c_direction_ ) * n_axis, 2 ),
        1.0
        );
    floatType factor = PROPORTIONALITY * std::sqrt(
        a_sq_ * ( 1.0 - u2 ) + b_sq_ * ( 1.0 - v2 ) + c_sq_ * ( 1.0 - w2 )
        );
    return factor * std::abs( std::sin( angle / 2.0 ) );
}


floatType
AnisotropicObject::max_rmsd_for_angle(floatType degrees) const
{
    // Set up shortest axis as it was a, and change if not
    dvect3 shortest_axis = a_direction_;

    if ( ( b_sq_ <= a_sq_ ) &&  ( b_sq_ <= c_sq_ ) )
    {
        shortest_axis = b_direction_;
    }
    else if ( ( c_sq_ <= a_sq_ ) && ( c_sq_ <= b_sq_ ) )
    {
        shortest_axis = c_direction_;
    }

    return rotation_rmsd(
        scitbx::math::r3_rotation::axis_and_angle_as_matrix< floatType >(
            reference_ * shortest_axis,
            scitbx::deg_as_rad( degrees )
            )
        );
}

const floatType AnisotropicObject::PROPORTIONALITY = std::sqrt( 2.0 / 5.0 );

}  } // namespace phaser::rotation
