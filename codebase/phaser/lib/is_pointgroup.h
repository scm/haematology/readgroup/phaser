#ifndef __PHASER_ISPG__
#define __PHASER_ISPG__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>

namespace phaser {


class is_pointgroup
{
  public:
  is_pointgroup() {};
  is_pointgroup(dvect31D,dvect31D,dvect3);

  bool error() { return !identity || moves_centre || inversion || unpaired; }
  bool moves_centre_error() { return moves_centre; }
  bool inversion_error() { return inversion; }
  bool unpaired_error() { return unpaired; }
  bool identity_error() { return !identity; }

  std::string warning();
  std::string message();

  private:
  floatType tol_rot,tol_orth,moves_centre_dist;
  bool moves_centre,inversion,unpaired,identity;
  bool ispg;
};

}//end namespace phaser

#endif
