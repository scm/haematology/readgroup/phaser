#include <phaser/lib/consistentDLuz.h>

namespace phaser {

TNT::Vector<floatType> consistentDLuz(const TNT::Vector<floatType>& DLuz_in,
                                     TNT::Fortran_Matrix<floatType>& CorMat,
                                     TNT::Fortran_Matrix<floatType>& pseudoCorMatInv)
{
  TNT::Vector<floatType> DLuz = DLuz_in;
  // Massaging of input DLuz values considers one pair at a time, and adjusting a later pair for consistency
  // might break the consistency of an earlier pair.  Perhaps a linear programming algorithm could satisfy all
  // constraints simultaneously, but carrying out the adjustment iteratively converges quickly.
  // Any remaining minor issues will be dealt with in the subsequent quadratic optimisation step.
  int NEWALD = DLuz_in.size();
  for (int nround=0; nround <10; nround++)
  {
    int nupdated(0);
    TNT::Vector<floatType> DLuzMin(NEWALD),DLuzMax(NEWALD);
    for (unsigned iewald = 0; iewald < NEWALD; iewald++)
    {
      DLuzMin[iewald] = DLuzMax[iewald] = DLuz[iewald];
    }
    for (int i=1; i <= NEWALD-1; i++)
    {
      for (int j=i+1; j <= NEWALD; j++)
      {
        // Avoid problems with very small and negative correlation matrix values: replace with something
        // around the standard deviation of a zero correlation computed from about 2000 samples.
        floatType cmfloor = std::max(CorMat(i,j),0.02);
        floatType rhoDelta12 = (cmfloor - DLuz[i-1]*DLuz[j-1]) /
                               std::sqrt((1.-fn::pow2(DLuz[i-1]))*(1.-fn::pow2(DLuz[j-1])));
        if (rhoDelta12 < 0.) // Implied negative error correlation: reduce both DLuz values to make zero
        {
          floatType sascale = std::sqrt(cmfloor/(DLuz[i-1]*DLuz[j-1]));
          DLuzMin[i-1] = std::min(DLuzMin[i-1],sascale*DLuz[i-1]);
          DLuzMin[j-1] = std::min(DLuzMin[j-1],sascale*DLuz[j-1]);
        }
        if (rhoDelta12 > cmfloor)
        {
          // Implied error correlation can be reduced by making Luzzati-D values closer, keeping sum constant
          floatType sa1(DLuz[i-1]),sa2(DLuz[j-1]),rho12(cmfloor);
          floatType sasum(sa1+sa2);
          floatType sasum2(fn::pow2(sasum));
          floatType v12(1.-fn::pow2(rho12));
          floatType sqrtarg(0.);
          if (rho12 < 0.9999) sqrtarg = std::max(0., (sasum2 - rho12*(4. + rho12*(4. + sasum2)) +
                                4.*rho12*std::sqrt(fn::pow2(1. + rho12) - v12*sasum2)) / v12);
          floatType sashift = (std::max(sa1,sa2)-std::min(sa1,sa2)-std::sqrt(sqrtarg))/2.;
          if (DLuz[i-1] < DLuz[j-1])
          {
            DLuzMax[i-1] = std::max(DLuzMax[i-1],DLuz[i-1] + sashift);
            DLuzMin[j-1] = std::min(DLuzMin[j-1],DLuz[j-1] - sashift);
          }
          else
          {
            DLuzMin[i-1] = std::min(DLuzMin[i-1],DLuz[i-1] - sashift);
            DLuzMax[j-1] = std::max(DLuzMax[j-1],DLuz[j-1] + sashift);
          }
        }
      }
    }
    for (unsigned iewald=1; iewald<=NEWALD; iewald++)
    {
      if ((DLuz[iewald-1] != DLuzMin[iewald-1]) || (DLuz[iewald-1] != DLuzMax[iewald-1]))
      {
        // If conflicting choices about increasing or decreasing, choose to decrease D (increase RMSD)
        DLuz[iewald-1] = (DLuzMin[iewald-1] < DLuz[iewald-1]) ? DLuzMin[iewald-1] : DLuzMax[iewald-1];
        nupdated++;
      }
    }
    if (nupdated == 0) break;
  }

  // Change DLuz to maximise the conditional variance subject to quadratic restraints to the incoming values.
  // Iterate if necessary, until the conditional variance of the ensemble structure factor is at least as large
  // as expected if all the models had independent errors.
  floatType conditionalVariance(-1.),condVarIndependent(0.);
  int whilecount(0);
  floatType DLuzInvVar(1./fn::pow2(0.15)); // Rough estimate for 20% uncertainty in RMSD
  int filtered(0);
  pseudoCorMatInv = SymmetricPseudoinverse<floatType>(CorMat,filtered,true).getInv();
  //numerical stability correction
  for (int i = 1; i <= NEWALD; i++)
    for (int j = i; j <= NEWALD; j++)
      if (fabs(pseudoCorMatInv(i,j)) < 1.0e-08)  pseudoCorMatInv(i,j) = 0.;
  while (conditionalVariance < condVarIndependent)
  {
    whilecount ++;
    TNT::Fortran_Matrix<floatType>  augmented_pCMI = pseudoCorMatInv;
    TNT::Vector<floatType> wtdDLuz(NEWALD);
    for (int i=1; i <= NEWALD; i++)
    {
      augmented_pCMI(i,i) += DLuzInvVar;
      wtdDLuz[i-1] = DLuz[i-1]*DLuzInvVar;
    }
    filtered = 0;
    DLuz = SymmetricPseudoinverse<floatType>(augmented_pCMI,filtered,true).getInv()*wtdDLuz;
    conditionalVariance = 1. -  TNT::dot_prod(pseudoCorMatInv*DLuz,DLuz);
    // Find conditional variance that would apply if all the models had independent errors.
    // We will assume that model errors can be positively but not negatively correlated, in which
    // case the conditional variance should not be less than this value.
    TNT::Fortran_Matrix<floatType> CorMatIndependent(NEWALD,NEWALD);
    for (int i = 1; i <= NEWALD; i++)
      for (int j = 1; j <= NEWALD; j++)
        CorMatIndependent(i,j) = (i==j) ? 1. : DLuz[i-1]*DLuz[j-1];
    filtered = 0;
    TNT::Fortran_Matrix<floatType>  pseudoCorMatIndepInv =
                                    SymmetricPseudoinverse<floatType>(CorMatIndependent,filtered,true).getInv();
    condVarIndependent = 1. -  TNT::dot_prod(pseudoCorMatIndepInv*DLuz,DLuz);
  }
  return DLuz;
}

}
