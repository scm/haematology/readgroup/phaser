#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <math.h>
#include <cmath>

namespace phaser {

floatType sim(floatType& X)
{
/* Calculate I1(X)/I0(X) where I1 and I0 are the modified
   1st and 0th order Bessel functions.
   This routine is based on a fortran routine originally
   written by W. Kabsch.
*/
  floatType SIM(0),T(0);
  T = std::fabs(X)/3.75;
  if (T <= 1.0)
  {
    T = T*T;
    SIM=X*(0.5+T*(0.87890594+T*(0.51498869+T*(0.15084934+
               T*(0.02658733+T*(0.00301532+T*0.00032411))))))/
          (1.0+T*(3.5156229 +T*(3.0899424 +T*(1.2067492 +
               T*(0.2659732 +T*(0.0360768 +T*0.0045813 ))))));
  }
  else
  {
    T = 1.0/T;
    SIM=(0.39894228+T*(-0.03988024+T*(-0.00362018+T*( 0.00163801+
                    T*(-0.01031555+T*( 0.02282967+T*(-0.02895312+
                    T*( 0.01787654-T*0.00420059))))))))*(X>=0?1:-1)/
        (0.39894228+T*( 0.01328592+T*( 0.00225319+T*(-0.00157565+
                    T*( 0.00916281+T*(-0.02057706+T*( 0.02635537+
                    T*(-0.01647633+T*0.00392377))))))));
  }
  return SIM;
/* the expression (X>=0?1:-1) replaces the Fortran function
   SIGN(1.0,X)
   where X=SIGN(A,B) means if(B>=0) then X=FABS(A) else X=-FABS(A)
   i.e. A function that assigns the sign of the second argument to the
   absolute value of the first.
*/
}

} //phaser
