#ifndef __PHASER_ROUND__
#define __PHASER_ROUND__
#include <math.h>

namespace phaser {

  template<class T> T round(const T& A)
  { return floor(A + 0.5); }

}

#endif
