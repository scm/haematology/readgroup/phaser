#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/fasta2str.h>
#include <boost/assign.hpp>

namespace phaser {

//not in class
bool isElement(std::string scattering_type)
{
  try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(scattering_type,true);
  }
  catch (std::exception const& err) {
    return false;
  }
  return true;
}

scattering_protein::scattering_protein()
{
// protein is made up of 54.76% C 15.68% N 21.22% O 1.47% S and 6.97% H by weight
  atomtypes["C"] = data_scattering("C",0.5476);
  atomtypes["N"] = data_scattering("N",0.1568);
  atomtypes["O"] = data_scattering("O",0.2211);
  atomtypes["S"] = data_scattering("S",0.0147);
  atomtypes["P"] = data_scattering("P",0);
  atomtypes["H"] = data_scattering("H",0.0697);
  molecular_weight_per_residue = double(DEF_MW_PER_RES);
  water_per_residue = double(DEF_WATER_PER_RES);
//atomic composition of amino acids in protein
  residue['A'] = atomic_composition(5 ,3 ,1,1,0,0);
  residue['C'] = atomic_composition(5 ,3 ,1,1,1,0);
  residue['D'] = atomic_composition(4 ,4 ,1,3,0,0);
  residue['E'] = atomic_composition(6 ,5 ,1,3,0,0);
  residue['F'] = atomic_composition(9 ,9 ,1,1,0,0);
  residue['G'] = atomic_composition(3 ,2 ,1,1,0,0);
  residue['H'] = atomic_composition(8 ,6 ,3,1,0,0);
  residue['I'] = atomic_composition(11,6 ,1,1,0,0);
  residue['K'] = atomic_composition(13,6 ,2,1,0,0);
  residue['L'] = atomic_composition(11,6 ,1,1,0,0);
  residue['M'] = atomic_composition(9 ,5 ,1,1,1,0);
  residue['N'] = atomic_composition(6 ,4 ,2,2,0,0);
  residue['P'] = atomic_composition(7 ,4 ,1,1,0,0);
  residue['Q'] = atomic_composition(8 ,5 ,2,2,0,0);
  residue['R'] = atomic_composition(13,6 ,4,1,0,0);
  residue['S'] = atomic_composition(5 ,3 ,1,2,0,0);
  residue['T'] = atomic_composition(7 ,4 ,1,2,0,0);
  residue['V'] = atomic_composition(9 ,5 ,1,1,0,0);
  residue['W'] = atomic_composition(10,11,2,1,0,0);
  residue['Y'] = atomic_composition(9 ,9 ,1,2,0,0);

  total_weight = 0;
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
    if (iter->second.weight)
      total_weight += iter->second.percent/iter->second.weight;
}

scattering_nucleic::scattering_nucleic()
{
// dna is made up of 40.50% C 24.31% N 26.00% O 7.19% P and 1.99% H by weight
  atomtypes["C"] = data_scattering("C",0.4050);
  atomtypes["N"] = data_scattering("N",0.2431);
  atomtypes["O"] = data_scattering("O",0.2600);
  atomtypes["S"] = data_scattering("S",0);
  atomtypes["P"] = data_scattering("P",0.0719);
  atomtypes["H"] = data_scattering("H",0.0199);
  molecular_weight_per_residue = double(DEF_MW_PER_BASE);
  water_per_residue = double(DEF_WATER_PER_BASE);
//atomic composition of bases in nucleic acid
  residue['A'] = atomic_composition(11,10,5,5,0,1);
  residue['T'] = atomic_composition(12,10,2,7,0,1);
  residue['G'] = atomic_composition(11,10,5,6,0,1);
  residue['C'] = atomic_composition(11,9 ,3,6,0,1);
  residue['U'] = atomic_composition(10,9 ,2,8,0,1);

  total_weight = 0;
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
    if (iter->second.weight)
      total_weight += iter->second.percent/iter->second.weight;
}

double scattering::average_scattering_at_ssqr(double ssqr)
{
  double total_scat(0);
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
  {
    assert(iter->second.atomic_number);
    total_scat += fraction_by_atomtype(iter->first)*
                  fn::pow2(fetch[iter->first].at_d_star_sq(ssqr)/iter->second.atomic_number);
  }
  return total_scat;
}

double scattering::PERC(std::string atomid)
{
  atomid = stoup(atomid);
  return (atomtypes.find(atomid) == atomtypes.end()) ? 0 : atomtypes[atomid].percent;
}

double scattering::avScat()
{
  double scat(0);
  for (std::map<std::string,data_scattering>::iterator iter = atomtypes.begin(); iter != atomtypes.end(); iter++)
    if (iter->second.weight)
    {
      scat += (iter->second.percent/iter->second.weight)*scitbx::fn::pow2(iter->second.atomic_number);
    }
  return scat;
}

double scattering::fraction_by_atomtype(std::string atomid)
{
  if (!PERC(atomid)) { return 0; }
  cctbx::eltbx::tiny_pse::table atm(atomid);
  return PERC(atomid)/atm.weight()/total_weight;
}

scatmw scattering::ScatMw(double mw)
{
  scatmw tmp;
  tmp.mw = mw; //duh
  tmp.scat = mw * avScat();
  int nres = std::floor(mw/molecular_weight_per_residue);
  cctbx::eltbx::tiny_pse::table oxygen("O");
  tmp.scat += nres * water_per_residue * fn::pow2(oxygen.atomic_number());
  return tmp;
}

scatmw scattering::ScatNres(int nres)
{
  scatmw tmp;
  cctbx::eltbx::tiny_pse::table oxygen("O");
  tmp.scat = nres * molecular_weight_per_residue * avScat();
  tmp.scat += nres * water_per_residue * fn::pow2(oxygen.atomic_number());
  tmp.mw = nres*molecular_weight_per_residue;
  tmp.mw += nres * water_per_residue * oxygen.weight();
  return tmp;
}

scatmw scattering::ScatSeq(std::string seq)
{
  scatmw tmp; tmp.scat = tmp.mw = 0;
  atomic_composition total(2,0,0,1,0,0); //water
  for (int i = 0; i < seq.size(); i++)
  {
    if (residue.find(seq[i]) != residue.end())
      total += residue[seq[i]];
    else if (!(isspace)(seq[i]) && seq[i] != '*')
      continue;
  }
  for (std::map<std::string,int>::iterator iter = total.data.begin(); iter != total.data.end(); iter++)
  {
    tmp.scat += iter->second * fn::pow2(atomtypes[iter->first].atomic_number);
    tmp.mw += iter->second * atomtypes[iter->first].weight;
  }
  cctbx::eltbx::tiny_pse::table oxygen("O");
  tmp.scat += seq.size()* water_per_residue *fn::pow2(oxygen.atomic_number());
  tmp.mw += seq.size()* water_per_residue *oxygen.weight();
  return tmp;
}

scatmw scattering::ScatSeqFile(std::string seqfile)
{
  bool read_error(false);
  std::string seq = fasta2str(seqfile,read_error);
  return ScatSeq(seq);
}

}
