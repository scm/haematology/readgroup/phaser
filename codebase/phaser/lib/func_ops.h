#ifndef __PHASER_FUNC_OPS__
#define __PHASER_FUNC_OPS__
#include <phaser/main/Phaser.h>

namespace phaser
{
  floatType rfactorial(int);
  floatType ThrdSafeFact(int n);

  class lnfactorial
  {
    private:
    float1D table;
    int ntabmax;

    public:
    lnfactorial(int n=500) // Actually rarely need higher than 200
    {
      ntabmax = std::max(80,n); // Approx good to 1 in 10^15 above 80
      initLnFactorial(ntabmax);
    }

    floatType get(int n)
    {
      if (n <= ntabmax)
        return table[n];
      else // Use Nemes approximation to logGamma (related to Stirling approx)
      {
        const floatType log2pi2(log(scitbx::constants::two_pi)/2.);
        floatType arg(static_cast<floatType>(n)+1.);
        return log2pi2 - log(arg)/2. + arg*(log(arg+1./(12.*arg-0.1/arg))-1.);
      }
    }

    private:
    void initLnFactorial(int ntabmax)
    {
      table.clear();
      table.push_back(0);
      for (int i = 1; i <= ntabmax; i++)
        table.push_back(table[i-1]+log(static_cast<floatType>(i)));
    }
  };

}//end namespace phaser

#endif
