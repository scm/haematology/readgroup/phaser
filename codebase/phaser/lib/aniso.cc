#include <phaser/lib/aniso.h>
#include <scitbx/constants.h>
#include <math.h>

namespace phaser {

//convert internal values of AnisoBeta to anisotropic Bs or Us
dmat6 AnisoBeta2AnisoB(dmat6 anisoBeta, floatType astar, floatType bstar, floatType cstar)
{
/*
  The anisotropic beta is applied as
    exp( -(beta11*h*h + beta22*k*k + beta33*l*l + beta12*h*k + beta13*h*l + beta23*k*l) )

  The anisotropic B is applied as
    exp( -(1/4)*(B11*h*h*astar*astar + B22*k*k*bstar*bstar + B33*l*l*cstar*cstar +
               2*B12*h*k*astar*bstar + 2*B13*h*l*astar*cstar + 2*B23*k*l*bstar*cstar) )
*/
  dmat6 anisoB;
  anisoB[0] = 4.*anisoBeta[0]/(astar*astar);
  anisoB[1] = 4.*anisoBeta[1]/(bstar*bstar);
  anisoB[2] = 4.*anisoBeta[2]/(cstar*cstar);
  anisoB[3] = 4.*anisoBeta[3]/(2.0*astar*bstar);
  anisoB[4] = 4.*anisoBeta[4]/(2.0*astar*cstar);
  anisoB[5] = 4.*anisoBeta[5]/(2.0*bstar*cstar);
  return anisoB;
}

dmat6 AnisoBeta2AnisoU(dmat6 anisoBeta, floatType astar, floatType bstar, floatType cstar)
{
/*
  The anisotropic beta is applied as
    exp( -(beta11*h*h + beta22*k*k + beta33*l*l + beta12*h*k + beta13*h*l + beta23*k*l) )

  The anisotropic U is applied as
    exp(-(2*pi*pi)*( U11*h*h*astar*astar + U22*k*k*bstar*bstar + U33*l*l*cstar*cstar +
                   2*U23*h*k*astar*bstar + 2*U13*h*l*astar*cstar + 2*U23*k*l*bstar*cstar ))

  So the U values of the anisotropy are
  U11 = beta11/(2*pi*pi*  astar*astar)
  U22 = beta22/(2*pi*pi*  bstar*bstar)
  U33 = beta33/(2*pi*pi*  cstar*cstar)
  U12 = beta12/(2*pi*pi*2*astar*bstar)
  U13 = beta13/(2*pi*pi*2*astar*cstar)
  U23 = beta23/(2*pi*pi*2*bstar*cstar)
*/

  floatType TWOPIPI = scitbx::constants::two_pi_sq;
  dmat6 anisoU;
  anisoU[0] = anisoBeta[0]/(TWOPIPI*astar*astar);
  anisoU[1] = anisoBeta[1]/(TWOPIPI*bstar*bstar);
  anisoU[2] = anisoBeta[2]/(TWOPIPI*cstar*cstar);
  anisoU[3] = anisoBeta[3]/(TWOPIPI*2.0*astar*bstar);
  anisoU[4] = anisoBeta[4]/(TWOPIPI*2.0*astar*cstar);
  anisoU[5] = anisoBeta[5]/(TWOPIPI*2.0*bstar*cstar);
  return anisoU;
}

floatType AnisoBeta2IsoB(dmat6 anisoBeta,
                  floatType A, floatType B, floatType C,
                  floatType cosAlpha, floatType cosBeta, floatType cosGamma)
{
/*
  Use the equations for equivalent isotropic displacement
  parameters in Trueblood et al (Acta Cryst. A52, 770-781, 1996).
  Note that our off-diagonal beta elements are multiplied by two
  compared to the ones they use (for computational convenience in
  computing the anisotropy term).

  Ueq = 1/(6*pi*pi)*(beta11*a.a + beta22*b.b + beta33*c.c +
                     beta12*a.b.cosGamma + beta13*a.c.cosBeta + beta23*b.c.cosAlpha)
  Since Beq = 8*pi*pi*Ueq,
  Beq = (4/3)*(beta11*a.a + beta22*b.b + beta33*c.c +
               beta12*a.b.cosGamma + beta13*a.c.cosBeta + beta23*b.c.cosAlpha)
*/
  return (4.0/3.0)*(anisoBeta[0]*(A*A)+
                    anisoBeta[1]*(B*B)+
                    anisoBeta[2]*(C*C)+
                    anisoBeta[3]*(A*B*cosGamma)+
                    anisoBeta[4]*(A*C*cosBeta)+
                    anisoBeta[5]*(B*C*cosAlpha));
}

dmat6 AnisoBeta2IsoBCoeffs(
                  floatType A, floatType B, floatType C,
                  floatType cosAlpha, floatType cosBeta, floatType cosGamma)
{
  return dmat6(
             (4./3.)*A*A,
             (4./3.)*B*B,
             (4./3.)*C*C,
             (4./3.)*A*B*cosGamma,
             (4./3.)*A*C*cosBeta,
             (4./3.)*B*C*cosAlpha );
}

dmat6 AnisoBetaRemoveIsoB(
                    dmat6 anisoBeta,
                    floatType aStar,
                    floatType bStar,
                    floatType cStar,
                    floatType cosAlphaStar,
                    floatType cosBetaStar,
                    floatType cosGammaStar,
                    floatType A,
                    floatType B,
                    floatType C,
                    floatType cosAlpha,
                    floatType cosBeta,
                    floatType cosGamma)
{
  floatType isoB = AnisoBeta2IsoB(anisoBeta,A,B,C,cosAlpha,cosBeta,cosGamma);
  dmat6
     isoBasAnisoBeta = IsoB2AnisoBeta(isoB,aStar,bStar,cStar,cosAlphaStar,cosBetaStar,cosGammaStar);
  dmat6 anisoCorr = anisoBeta;
  for (unsigned n = 0; n < 6; n++)
    anisoCorr[n] -= isoBasAnisoBeta[n];
  return anisoCorr;
}

// Reverse transformations using definitions in AnisoBeta2AnisoUorB.cc

dmat6 AnisoU2AnisoBeta(dmat6 anisoU,
                         floatType astar, floatType bstar, floatType cstar)
{
  floatType TWOPIPI = scitbx::constants::two_pi_sq;
  dmat6 anisoBeta;
  anisoBeta[0] = anisoU[0]*(TWOPIPI*astar*astar);
  anisoBeta[1] = anisoU[1]*(TWOPIPI*bstar*bstar);
  anisoBeta[2] = anisoU[2]*(TWOPIPI*cstar*cstar);
  anisoBeta[3] = anisoU[3]*(TWOPIPI*2.0*astar*bstar);
  anisoBeta[4] = anisoU[4]*(TWOPIPI*2.0*astar*cstar);
  anisoBeta[5] = anisoU[5]*(TWOPIPI*2.0*bstar*cstar);
  return anisoBeta;
}

dmat6 AnisoB2AnisoBeta(dmat6 anisoB,
                         floatType astar, floatType bstar, floatType cstar)
{
  dmat6 anisoBeta;
  anisoBeta[0] = anisoB[0]*astar*astar/4.;
  anisoBeta[1] = anisoB[1]*bstar*bstar/4.;
  anisoBeta[2] = anisoB[2]*cstar*cstar/4.;
  anisoBeta[3] = anisoB[3]*2.0*astar*bstar/4.;
  anisoBeta[4] = anisoB[4]*2.0*astar*cstar/4.;
  anisoBeta[5] = anisoB[5]*2.0*bstar*cstar/4.;
  return anisoBeta;
}

dmat6 IsoB2AnisoBeta(
                         floatType isoB,
                         floatType aStar,
                         floatType bStar,
                         floatType cStar,
                         floatType cosAlphaStar,
                         floatType cosBetaStar,
                         floatType cosGammaStar)
{
/*
  The isotropic B-factor can be expressed in terms of hkl as:
    exp(-B/(4*d*d))  = exp(-(1/4)*B*dstar*dstar) =
    exp(-(1/4)*(B*h*h*astar*astar +
                B*k*k*bstar*bstar +
                B*l*l*cstar*cstar +
              2*B*h*k*astar*bstar*cosGammaStar +
              2*B*h*l*astar*cstar*cosBetaStar  +
              2*B*k*l*bstar*cstar*cosAlphaStar))

  The anisotropic beta is applied as
  exp( -(beta11*h*h +
         beta22*k*k +
         beta33*l*l +
         beta12*h*k +
         beta13*h*l +
         beta23*k*l) )

  So the stored values of the anisotropy are
  anisoBeta[0] = beta11 = (1/4)  *B*astar*astar
  anisoBeta[1] = beta22 = (1/4)  *B*bstar*bstar
  anisoBeta[2] = beta33 = (1/4)  *B*cstar*cstar
  anisoBeta[3] = beta12 = (1/4)*2*B*astar*bstar*cosGammaStar
  anisoBeta[4] = beta13 = (1/4)*2*B*astar*cstar*cosBetaStar
  anisoBeta[5] = beta23 = (1/4)*2*B*bstar*cstar*cosAlphaStar
*/

  floatType QUARTER(1./4.);
  dmat6 anisoBeta;
  anisoBeta[0] = QUARTER   *isoB*aStar*aStar;
  anisoBeta[1] = QUARTER   *isoB*bStar*bStar;
  anisoBeta[2] = QUARTER   *isoB*cStar*cStar;
  anisoBeta[3] = QUARTER*2.*isoB*aStar*bStar*cosGammaStar;
  anisoBeta[4] = QUARTER*2.*isoB*aStar*cStar*cosBetaStar;
  anisoBeta[5] = QUARTER*2.*isoB*bStar*cStar*cosAlphaStar;
  return anisoBeta;
}

} //end namespace phaser
