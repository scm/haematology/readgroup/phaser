#include <phaser/main/Phaser.h>
#include <cmath>

namespace phaser {

double matthewsProb(floatType,floatType,std::string);
double matthewsVM(floatType,std::string);

} //namespace phaser
