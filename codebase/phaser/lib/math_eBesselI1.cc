#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <phaser/io/Errors.h>
#include <scitbx/constants.h>
#include <float.h>

namespace phaser {

floatType dcsevl(floatType x, const floatType* cs, int n)
{
/*
 Evaluate the n-term Chebyshev series cs at x.  Adapted from
 a method presented in the paper by Broucke referenced below.

 Input Arguments:

      x    value at which the series is to be evaluated.
      cs   array of n terms of a Chebyshev series. In evaluating
           cs, only half the first coefficient is summed.
      n    number of terms in array cs.

 References:

      R. Broucke, Ten subroutines for the manipulation of Chebyshev series,
      Algorithm 446, Communications of the A.C.M. 16, (1973) pp. 254-256.

      L. Fox and I. B. Parker, Chebyshev Polynomials in
      Numerical Analysis, Oxford University Press, 1968,  page 56.

 Author: Fullerton W.  Los Alamos National Laboratory
*/

    PHASER_ASSERT(n >= 1); //if (n < 1) throw error("dcsevl: number of terms <= 0");
    PHASER_ASSERT(n <= 1000); //if (n > 1000) throw error("dcsevl:  number of terms > 1000");
    PHASER_ASSERT(fabs(x) <= DBL_EPSILON + 1.0); //if (fabs(x) > DBL_EPSILON + 1.0) throw error("dcsevl: x outside the interval (-1,+1)");

    floatType b0 = 0.0, b1 = 0.0, b2, twox = x * 2.0;
    for (int i = 1; i <= n; i++) {
        b2 = b1;
        b1 = b0;
        b0 = twox * b1 - b2 + cs[n - i];
    }

    return (b0 - b2) * 0.5;
}

int initds(floatType* os, int nos, floatType eta)
{
/*
 Initialize the orthogonal series, represented by the array os, so
 that initds is the number of terms needed to insure the error is no
 larger than eta.  Ordinarily, eta will be chosen to be one-tenth
 machine precision.

 Input Arguments:

   OS     floatType precision array of NOS coefficients in an orthogonal
          series.
   NOS    number of coefficients in OS.
   ETA    single precision scalar containing requested accuracy of
          series.

 Author: Fullerton W.  Los Alamos National Laboratory
*/
    PHASER_ASSERT(nos >= 1); //if (nos < 1) throw error("initds: Number of coefficients is less than 1");

    int i = 0;
    floatType err = 0.0;
    for (int ii = 1; ii <= nos; ii++) {
        i = nos - ii;
        err += fabs(os[i]);
        if (err > eta) break;
    }

    PHASER_ASSERT(i != nos); //if (i == nos) throw error("initds: Chebyshev series too short for specified accuracy");
    return i;
}

//-----------------------------------------------------------------------------//

floatType Fn_eBesselI1 (floatType x)
{
/*
   Calculates approximate values for the modified Bessel function
   of the first kind of order one multiplied by EXP(-ABS(X)).
   Author:  Fullerton W.  Los Alamos National Laboratory

   Series for BI1 on the interval  0. to  9.00000E+00
                                with weighted error   1.44E-32
                                 log weighted error  31.84
                       significant figures required  31.45
                            decimal places required  32.46

   Series for AI1 on the interval  1.25000E-01 to  3.33333E-01
                                with weighted error   2.81E-32
                                 log weighted error  31.55
                       significant figures required  29.93
                            decimal places required  32.38

   Series for AI12 on the interval  0. to  1.25000E-01
                                with weighted error   1.83E-32
                                 log weighted error  31.74
                       significant figures required  29.97
                            decimal places required  32.66

*/
    floatType BI1CS[17] = {
        -0.0019717132610998597316138503218149,
         0.40734887667546480608155393652014,
         0.034838994299959455866245037783787,
         0.0015453945563001236038598401058489,
         4.188852109837778412945883200412e-5,
         7.6490267648362114741959703966069e-7,
         1.0042493924741178689179808037238e-8,
         9.9322077919238106481371298054863e-11,
         7.6638017918447637275200171681349e-13,
         4.741418923816739498038809194816e-15,
         2.4041144040745181799863172032e-17,
         1.0171505007093713649121100799999e-19,
         3.6450935657866949458491733333333e-22,
         1.1205749502562039344810666666666e-24,
         2.9875441934468088832e-27,
         6.9732310939194709333333333333333e-30,
         1.43679482206208e-32
    };

    floatType AI1CS[46] = {
        -0.02846744181881478674100372468307,
        -0.01922953231443220651044448774979,
        -6.115185857943788982256249917785e-4,
        -2.069971253350227708882823777979e-5,
         8.585619145810725565536944673138e-6,
         1.04949824671159086251745399786e-6,
        -2.918338918447902202093432326697e-7,
        -1.559378146631739000160680969077e-8,
         1.318012367144944705525302873909e-8,
        -1.448423418183078317639134467815e-9,
        -2.90851224399314209482504099301e-10,
         1.266388917875382387311159690403e-10,
        -1.66494777291922067062417839858e-11,
        -1.666653644609432976095937154999e-12,
         1.242602414290768265232168472017e-12,
        -2.731549379672432397251461428633e-13,
         2.023947881645803780700262688981e-14,
         7.307950018116883636198698126123e-15,
        -3.332905634404674943813778617133e-15,
         7.17534655851295374354225466567e-16,
        -6.982530324796256355850629223656e-17,
        -1.299944201562760760060446080587e-17,
         8.12094286424279889205467834286e-18,
        -2.194016207410736898156266643783e-18,
         3.630516170029654848279860932334e-19,
        -1.695139772439104166306866790399e-20,
        -1.288184829897907807116882538222e-20,
         5.694428604967052780109991073109e-21,
        -1.459597009090480056545509900287e-21,
         2.514546010675717314084691334485e-22,
        -1.844758883139124818160400029013e-23,
        -6.339760596227948641928609791999e-24,
         3.46144110203101111110814662656e-24,
        -1.017062335371393547596541023573e-24,
         2.149877147090431445962500778666e-25,
        -3.045252425238676401746206173866e-26,
         5.238082144721285982177634986666e-28,
         1.443583107089382446416789503999e-27,
        -6.121302074890042733200670719999e-28,
         1.700011117467818418349189802666e-28,
        -3.596589107984244158535215786666e-29,
         5.448178578948418576650513066666e-30,
        -2.731831789689084989162564266666e-31,
        -1.858905021708600715771903999999e-31,
         9.212682974513933441127765333333e-32,
        -2.813835155653561106370833066666e-32
    };

    floatType AI12CS[69] = {
         0.02857623501828012047449845948469,
        -0.009761097491361468407765164457302,
        -1.105889387626237162912569212775e-4,
        -3.882564808877690393456544776274e-6,
        -2.512236237870208925294520022121e-7,
        -2.631468846889519506837052365232e-8,
        -3.835380385964237022045006787968e-9,
        -5.589743462196583806868112522229e-10,
        -1.897495812350541234498925033238e-11,
         3.252603583015488238555080679949e-11,
         1.412580743661378133163366332846e-11,
         2.03562854414708950722452613684e-12,
        -7.198551776245908512092589890446e-13,
        -4.083551111092197318228499639691e-13,
        -2.101541842772664313019845727462e-14,
         4.272440016711951354297788336997e-14,
         1.042027698412880276417414499948e-14,
        -3.814403072437007804767072535396e-15,
        -1.880354775510782448512734533963e-15,
         3.308202310920928282731903352405e-16,
         2.962628997645950139068546542052e-16,
        -3.209525921993423958778373532887e-17,
        -4.650305368489358325571282818979e-17,
         4.414348323071707949946113759641e-18,
         7.517296310842104805425458080295e-18,
        -9.314178867326883375684847845157e-19,
        -1.242193275194890956116784488697e-18,
         2.414276719454848469005153902176e-19,
         2.026944384053285178971922860692e-19,
        -6.394267188269097787043919886811e-20,
        -3.049812452373095896084884503571e-20,
         1.612841851651480225134622307691e-20,
         3.56091396430992505451027090462e-21,
        -3.752017947936439079666828003246e-21,
        -5.787037427074799345951982310741e-23,
         7.759997511648161961982369632092e-22,
        -1.452790897202233394064459874085e-22,
        -1.318225286739036702121922753374e-22,
         6.116654862903070701879991331717e-23,
         1.376279762427126427730243383634e-23,
        -1.690837689959347884919839382306e-23,
         1.430596088595433153987201085385e-24,
         3.409557828090594020405367729902e-24,
        -1.309457666270760227845738726424e-24,
        -3.940706411240257436093521417557e-25,
         4.277137426980876580806166797352e-25,
        -4.424634830982606881900283123029e-26,
        -8.734113196230714972115309788747e-26,
         4.045401335683533392143404142428e-26,
         7.067100658094689465651607717806e-27,
        -1.249463344565105223002864518605e-26,
         2.867392244403437032979483391426e-27,
         2.04429289250429267028177957421e-27,
        -1.518636633820462568371346802911e-27,
         8.110181098187575886132279107037e-29,
         3.58037935477358609112717370327e-28,
        -1.692929018927902509593057175448e-28,
        -2.222902499702427639067758527774e-29,
         5.424535127145969655048600401128e-29,
        -1.787068401578018688764912993304e-29,
        -6.56547906872281493882392943788e-30,
         7.807013165061145280922067706839e-30,
        -1.816595260668979717379333152221e-30,
        -1.287704952660084820376875598959e-30,
         1.114548172988164547413709273694e-30,
        -1.808343145039336939159368876687e-31,
        -2.231677718203771952232448228939e-31,
         1.619029596080341510617909803614e-31,
        -1.83407990880494141390130843921e-32
    };

    const floatType eta(0.5 * DBL_EPSILON * 0.1),
                 xmin(DBL_MIN * 2.0),
                 xsml(std::sqrt(0.5*DBL_EPSILON * 4.5));

    floatType ret_val;

    bool first_call(true);
    int nti1, ntai1, ntai12;
    if (first_call) {
        nti1 = initds(BI1CS, 17, eta);
        ntai1 = initds(AI1CS, 46, eta);
        ntai12 = initds(AI12CS, 69, eta);
        first_call = false;
    }

    floatType y = fabs(x);
    if (y > 3.0) goto L20;

    ret_val = 0.0;
    if (y == 0.0) return ret_val;

    PHASER_ASSERT(!(y <= xmin)); // x too small, underflow

    if (y > xmin) ret_val = x * 0.5;

    if (y > xsml)
        ret_val = x * (dcsevl(y * y / 4.5 - 1.0, BI1CS, nti1) + 0.875);

    return ret_val * exp(-y);

  L20:
    if (y <= 8.0)
        ret_val = (dcsevl((48.0 / y - 11.0)/5.0, AI1CS, ntai1) + 0.375) / sqrt(y);
    else // if (y > 8.0)
        ret_val = (dcsevl(16.0 / y - 1.0, AI12CS, ntai12) + 0.375) / sqrt(y);

    //Fortran SIGN(ret_val,x)
    if (x >= 0)
      return std::fabs(ret_val);
    return -std::fabs(ret_val);
}

af::shared<std::pair<floatType,floatType> > geteBesselI1Table(int tbllen, floatType argmax)
{
  af::shared<std::pair<floatType,floatType> >  eBesselI1Table;
  eBesselI1Table.resize(tbllen+1);
  floatType divarg(argmax/tbllen);
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    floatType x0(Fn_eBesselI1((i  )*divarg));
    floatType x1(Fn_eBesselI1((i+1)*divarg));
    eBesselI1Table[i].first = x0;
    eBesselI1Table[i].second = x1 - x0;
  }
  return eBesselI1Table;
}

af::shared<std::pair<floatType,floatType> > geteBesselI1InvTable(int tbllen, floatType argcut)
{
  // sqrt(1/x)eBesselI1(1/x) is nearly linear for small x.
  af::shared<std::pair<floatType,floatType> > eBesselI1InvTable;
  eBesselI1InvTable.resize(tbllen+1);
  floatType divarg(1./(argcut*tbllen));
  floatType arg0,arg1,x0,x1;
  floatType invSqrt2Pi(1./std::sqrt(scitbx::constants::two_pi));
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    if (i > 0)
    {
      arg0 = 1./((i  )*divarg);
      x0 = std::sqrt(arg0)*Fn_eBesselI1(arg0);
    }
    else
      x0 = invSqrt2Pi; // Limit as 1/arg->zero, or arg->infinity
    arg1 = 1./((i+1)*divarg);
    x1 = std::sqrt(arg1)*Fn_eBesselI1(arg1);
    eBesselI1InvTable[i].first = x0;
    eBesselI1InvTable[i].second = x1 - x0;
  }
  return eBesselI1InvTable;
}

floatType eBesselI1(floatType arg)
{
  // Accuracy about 1 part in 10^7 or better, 0.1-infinity
  // Quadratic interpolation would use smaller tables, but is slower
  const floatType ONE(1);
  static int tbllen1(4096),tbllen2(4096),tbllen3(4096);
  static floatType argcut1(2.),argcut2(20.);
  static floatType argfac1(tbllen1/argcut1), argfac2(tbllen2/argcut2);
  static floatType invArgFac(tbllen3*argcut2);
  static af::shared<std::pair<floatType,floatType> > eBesselI1Table1 = geteBesselI1Table(tbllen1,argcut1);
  static af::shared<std::pair<floatType,floatType> > eBesselI1Table2 = geteBesselI1Table(tbllen2,argcut2);
  static af::shared<std::pair<floatType,floatType> > eBesselI1InvTable = geteBesselI1InvTable(tbllen3,argcut2);
  floatType fracTableArg;
  int index;
  if (arg < argcut1)
  {
    fracTableArg = argfac1*arg;
    index = static_cast<int>(fracTableArg);
    return eBesselI1Table1[index].first + (fracTableArg-index)*eBesselI1Table1[index].second;
  }
  else if (arg < argcut2)
  {
    fracTableArg = argfac2*arg;
    index = static_cast<int>(fracTableArg);
    return eBesselI1Table2[index].first + (fracTableArg-index)*eBesselI1Table2[index].second;
  }
  else
  {
    floatType invArg(ONE/arg);
    fracTableArg = invArgFac*invArg;
    index = static_cast<int>(fracTableArg);
    return std::sqrt(invArg)*(eBesselI1InvTable[index].first + (fracTableArg-index)*eBesselI1InvTable[index].second);
  }
}

}
