#ifndef __PHASER_ROTATIONGROUP__
#define __PHASER_ROTATIONGROUP__
#include <phaser/main/Phaser.h>
#include <phaser/lib/func_ops.h>

namespace phaser {

  floatType djmn(int&,int&,int&,floatType&,lnfactorial&);
  float2D   djmn_recursive_table(int&,floatType&,lnfactorial&);
  floatType djmn_recursive(int&,int,int,floatType&,lnfactorial&);

}//end namespace phaser

#endif
