#include <phaser/lib/xyzRotMatDeg.h>
#include <scitbx/constants.h>
#include <scitbx/math/r3_rotation.h>

namespace phaser {

dmat33 xyzRotMatDeg(int dir, floatType theta, bool gradient, bool hessian)
{
  int ind0 = (0+dir)%3;
  int ind1 = (1+dir)%3;
  int ind2 = (2+dir)%3;
  floatType cosTheta = cos( scitbx::deg_as_rad(theta));
  floatType sinTheta = sin( scitbx::deg_as_rad(theta));
  floatType c = scitbx::deg_as_rad(1);
  floatType csqr = fn::pow2(c);
  dmat33 xyzmat;
  if (!gradient && !hessian)
  {
    xyzmat(ind0,ind0) = 1.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = cosTheta;
    xyzmat(ind1,ind2) = -sinTheta;
    xyzmat(ind2,ind1) = sinTheta;
    xyzmat(ind2,ind2) = cosTheta;
  }
  else if (gradient)
  {
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = -sinTheta*c;
    xyzmat(ind1,ind2) = -cosTheta*c;
    xyzmat(ind2,ind1) = cosTheta*c;
    xyzmat(ind2,ind2) = -sinTheta*c;
  }
  else // Hessian
  {
    xyzmat(ind0,ind0) = 0.0;
    xyzmat(ind0,ind1) = xyzmat(ind0,ind2) = xyzmat(ind1,ind0) = xyzmat(ind2,ind0) = 0.0;
    xyzmat(ind1,ind1) = -cosTheta*csqr;
    xyzmat(ind1,ind2) = sinTheta*csqr;
    xyzmat(ind2,ind1) = -sinTheta*csqr;
    xyzmat(ind2,ind2) = -cosTheta*csqr;
  }
  return xyzmat;
}

floatType xyzAngle(dvect3 perturbRot)
{
  dmat33 ncsRmat(1,0,0,0,1,0,0,0,1);
  for (int dir = 0; dir < 3; dir++)
    ncsRmat = xyzRotMatDeg(dir,perturbRot[dir])*ncsRmat;
  floatType rad = scitbx::math::r3_rotation::axis_and_angle_from_matrix<floatType>(ncsRmat).angle_rad;
  return std::abs(scitbx::rad_as_deg(rad));
}

}
