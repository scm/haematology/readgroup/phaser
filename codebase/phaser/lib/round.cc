#include <math.h>

namespace phaser {

double round(const double& A)
{ return floor(A + 0.5); }

float round(const float& A)
{ return floor(A + 0.5); }

} //phaser
