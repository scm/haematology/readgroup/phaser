#include <phaser/lib/jiffy.h>
#include <complex>
#include <string>
#include <cstdlib>
#include <ctype.h>
#include <boost/format.hpp>

namespace phaser {

//-----------------------------------------------------------------------
// stos - int to std::string
//-----------------------------------------------------------------------
std::string stos(const std::string & str,int n)
{
  n = std::max(1,n);
  std::string ss;
  for (int i = 0; i < n-1; i++)
  { //ends in ' *' etc
    for (int j = 0; j < str.size()-1; j++)
      if (str[j] != ' ')//only possible at the penultimate position in str
        ss += str[j];
    ss += ",";
  }
  ss += str;
  return ss;
}

//-----------------------------------------------------------------------
// itos - int to std::string
//-----------------------------------------------------------------------
std::string ntos(const int & i)
{
  std::stringstream s;
  s << i;
  std::string ss = s.str();
  if (ss.size() == 1 ) ss = "00" + ss;
  if (ss.size() == 2 ) ss = "0" + ss;
  return ss;
}

//-----------------------------------------------------------------------
// itos - int to std::string
//-----------------------------------------------------------------------
std::string itos(const int & i)
{
  std::stringstream s;
  s << i;
  return s.str();
}

//-----------------------------------------------------------------------
// itos - int to std::string
//-----------------------------------------------------------------------
std::string itos(const int & i,const int & w,bool right_justified)
{
  std::stringstream s;
  s.width(w); //width
  if (!right_justified) s << std::left;
  s << i;
  return s.str();
}

//-----------------------------------------------------------------------
// ftos - float to std::string
//-----------------------------------------------------------------------
std::string ftos(const float & f)
{
  std::stringstream s;
  s << f;
  return s.str();
}

//-----------------------------------------------------------------------
// dtos - double/float to std::string
//-----------------------------------------------------------------------
std::string dtos(const float & f)
{
  std::stringstream s;
  s << f;
  return s.str();
}

std::string dtos(const double & f)
{
  std::stringstream s;
  s << f;
  return s.str();
}

std::string dtos(const double & f,int p)
{
  std::stringstream s;
  s.setf(std::ios_base::fixed);
  s.precision(p); //number of decimal places
  s << f;
  return s.str();
}

std::string dtos(const double & f,int w,int p,bool sign)
{
  int f_cols(p);
  double cp_f(f);
  while (cp_f >= 1) { f_cols++; cp_f/=10.; }
  if (sign) w -= 1; //take off char for sign
  //std::string str;
  std::stringstream s;
  if (sign) s.setf(std::ios_base::showpos);
  if (f_cols <= w)
  {
    s.setf(std::ios_base::fixed);
    s.precision(p); //number of decimal places
  }
  else
  {
    s.setf(std::ios_base::scientific);
    sign ? s.precision(w-7) : s.precision(w-6); //1.pE+10 has 6 chars excluding p
  }
  s.width(w); //width
  s << f;
  //s >> str;
  return s.str();
}

std::string etos(const double & f,int w,int p,bool sign)
{
  std::string str;
  std::stringstream s;
  if (sign) s.setf(std::ios_base::showpos);
  s.setf(std::ios_base::scientific);
  s.precision(p); //number of decimal places
  s.width(w); //width
  s << f;
  return s.str();
}


//-----------------------------------------------------------------------
// ctos - complex to std::string
//-----------------------------------------------------------------------
std::string ctos(const std::complex<float> & c)
{ return "(" + dtos(std::real(c)) + "," +  dtos(std::imag(c)) + ")"; }

std::string ctos(const std::complex<double> & c)
{ return "(" + dtos(std::real(c)) + "," +  dtos(std::imag(c)) + ")"; }

//-----------------------------------------------------------------------
// btos - bool to std::string
//-----------------------------------------------------------------------
std::string btos(const bool& i)
{
  if (i) return "true ";
  return "false";
}

//-----------------------------------------------------------------------
// stoa - std::string to alpha
//-----------------------------------------------------------------------
std::string stoa(const std::string & str)
{
  std::stringstream s;
  std::string t;
  char ch;
  s << str;
  do { if (!s.get(ch)) return "";
     } while (!isalpha(ch)); //get a-z,A-Z
  t = ch;
  while(s.get(ch) && isalpha(ch)) t += ch;
  return t;
}

//-----------------------------------------------------------------------
// stod - std::string to digit
//-----------------------------------------------------------------------
int stod(const std::string & str)
{
  std::stringstream s;
  char ch;
  std::string t;
  s << str;
  do { if (!s.get(ch)) return 0;
     } while (!isdigit(ch)); //get digits
  t = ch;
  while(s.get(ch) && isdigit(ch)) t += ch;
  return std::atoi(t.c_str());
}

//-----------------------------------------------------------------------
// vtos - vector to std::string
//-----------------------------------------------------------------------
std::string ivtos(const scitbx::vec3<int> & f)
{ return itos(f[0]) + " " + itos(f[1]) + " " + itos(f[2]) ; }

std::string ivtos(const std::vector<int> & f,char top,char separator,char end)
{
  if (!f.size()) return "";
  std::string str;
  if (top != ' ') str += top;
  for (int i = 0; i < f.size()-1; i++) str += itos(f[i]) + separator;
  str += itos(f[f.size()-1]);
  if (end != ' ') str += end;
  return str;
}

std::string dvtos(const scitbx::vec3<double> & f,char top,char separator,char end)
{
  if (!f.size()) return "";
  std::string str;
  if (top != ' ') str += top;
  for (int i = 0; i < f.size()-1; i++) str += itos(f[i]) + separator;
  str += itos(f[f.size()-1]);
  if (end != ' ') str += end;
  return str;
}

std::string dvtos(const scitbx::vec3<double> & f,bool c)
{ return std::string(c?"(":"") + dtos(f[0]) + std::string(c?",":" ") + dtos(f[1]) + std::string(c?",":" ") + dtos(f[2]) + std::string(c?")":""); }

std::string cvtos(const scitbx::vec3<cmplxType> & f,bool c)
{ return std::string(c?"(":"") + ctos(f[0]) + std::string(c?",":" ") + ctos(f[1]) + std::string(c?",":" ") + ctos(f[2]) + std::string(c?")":""); }

std::string dvtos(const af::double6 & f)
{
  std::stringstream s;
  std::string str;
  s << boost::format("%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f")%f[0]%f[1]%f[2]%f[3]%f[4]%f[5];
  return s.str();
}

std::string dvtos(const scitbx::vec3<double> & f,int w, int p)
{ return dtos(f[0],w,p) + " " + dtos(f[1],w,p) + " " + dtos(f[2],w,p) ; }

std::string dvtos(const scitbx::vec3<double> & f,int p)
{ return dtos(f[0],p) + " " + dtos(f[1],p) + " " + dtos(f[2],p) ; }

std::string dvtos(const scitbx::vec3<float> & f)
{ return dtos(f[0]) + " " + dtos(f[1]) + " " + dtos(f[2]) ; }

std::string dvtos(const af::shared<double> & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i]) + " ";
  str += dtos(f[f.size()-1]);
  return str;
}

std::string dvtos(const TNT::Vector<double> & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i]) + " ";
  str += dtos(f[f.size()-1]);
  return str;
}

std::string dvtos(const af::shared<double> & f,int w, int p)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],w,p) + " ";
  str += dtos(f[f.size()-1],w,p);
  return str;
}

std::string dvtos(const std::vector<double> & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i]) + " ";
  str += dtos(f[f.size()-1]);
  return str;
}

std::string dmtos(const std::vector<std::vector<double> > & f)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size(); i++) str += "{" + dvtos(f[i]) + "} ";
  return str;
}

std::string dvtos(const std::vector<double> & f,int w, int p)
{
  if (!f.size()) return "";
  std::string str;
  for (int i = 0; i < f.size()-1; i++) str += dtos(f[i],w,p) + " ";
  str += dtos(f[f.size()-1],w,p);
  return str;
}

//-----------------------------------------------------------------------
// mtos - matrix to std::string
//-----------------------------------------------------------------------
std::string imtos(const scitbx::mat3<int> & f)
{
  return itos(f(0,0)) + " " + itos(f(0,1)) + " " + itos(f(0,2)) + " "
       + itos(f(1,0)) + " " + itos(f(1,1)) + " " + itos(f(1,2)) + " "
       + itos(f(2,0)) + " " + itos(f(2,1)) + " " + itos(f(2,2));
}
std::string cmtos(const scitbx::mat3<cmplxType> & f)
{
  return ctos(f(0,0)) + " " + ctos(f(0,1)) + " " + ctos(f(0,2)) + " "
       + ctos(f(1,0)) + " " + ctos(f(1,1)) + " " + ctos(f(1,2)) + " "
       + ctos(f(2,0)) + " " + ctos(f(2,1)) + " " + ctos(f(2,2));
}
std::string dmtos(const scitbx::mat3<float> & f, int w, int p)
{
  return dtos(f(0,0),w,p,true) + " " + dtos(f(0,1),w,p,true) + " " + dtos(f(0,2),w,p,true) + " || "
       + dtos(f(1,0),w,p,true) + " " + dtos(f(1,1),w,p,true) + " " + dtos(f(1,2),w,p,true) + " || "
       + dtos(f(2,0),w,p,true) + " " + dtos(f(2,1),w,p,true) + " " + dtos(f(2,2),w,p,true);
}
std::string dmtos(const scitbx::mat3<double> & f, int w, int p)
{
  return dtos(f(0,0),w,p,true) + " " + dtos(f(0,1),w,p,true) + " " + dtos(f(0,2),w,p,true) + " || "
       + dtos(f(1,0),w,p,true) + " " + dtos(f(1,1),w,p,true) + " " + dtos(f(1,2),w,p,true) + " || "
       + dtos(f(2,0),w,p,true) + " " + dtos(f(2,1),w,p,true) + " " + dtos(f(2,2),w,p,true);
}
std::string dmtos(const scitbx::mat3<double> & f)
{
  return dtos(f(0,0)) + " " + dtos(f(0,1)) + " " + dtos(f(0,2)) + " || "
       + dtos(f(1,0)) + " " + dtos(f(1,1)) + " " + dtos(f(1,2)) + " || "
       + dtos(f(2,0)) + " " + dtos(f(2,1)) + " " + dtos(f(2,2));
}

std::string dmtos(const scitbx::sym_mat3<double> & f)
{
  return dtos(f(0,0)) + " " + dtos(f(0,1)) + " " + dtos(f(0,2)) + " || "
       + dtos(f(1,0)) + " " + dtos(f(1,1)) + " " + dtos(f(1,2)) + " || "
       + dtos(f(2,0)) + " " + dtos(f(2,1)) + " " + dtos(f(2,2));
}

//-----------------------------------------------------------------------
// itoaniso - anisotropic integer to  std::string
//-----------------------------------------------------------------------
std::string itoaniso(const int & i)
{
  if (i == 0) return " HH ";
  if (i == 1) return " KK ";
  if (i == 2) return " LL ";
  if (i == 3) return " HK ";
  if (i == 4) return " HL ";
  if (i == 5) return " KL ";
  return " -- ";
}

//-----------------------------------------------------------------------
// stoup - std::string to uppercase
//-----------------------------------------------------------------------
std::string stoup(const std::string & str)
{
  char ch;
  std::stringstream s;
  std::string t="";
  s << str;
  while (s.get(ch))
    t += toupper(ch); //convert to uppercase
  return t;
}

//-----------------------------------------------------------------------
// stolo - std::string to lowercase
//-----------------------------------------------------------------------
std::string stolo(const std::string & str)
{
  char ch;
  std::stringstream s;
  std::string t="";
  s << str;
  while (s.get(ch))
    t += tolower(ch); //convert to lowercase
  return t;
}

//-----------------------------------------------------------------------
// isfloat - is the std::string a float?
//-----------------------------------------------------------------------
bool isfloat(std::string ch)
{
  std::string separators(" \t");
  std::size_t start = ch.find_first_not_of(separators);
  std::size_t stop = ch.find_last_not_of(separators);
  ch = ch.substr(start,stop-start);

  //first character is different, can't start with exponent
  if (!ch.size()) return false;
  if (!((isdigit)(ch[0]) || ch[0]=='.' || ch[0]=='+'|| ch[0]=='-' ))
    return false;
//rest of string is all digits or . or e or E or + or -(exponent)
  for (int i = 0; i < ch.size(); i++)
    if (!((isdigit)(ch[i]) || ch[i]=='.' || ch[i]=='e' || ch[i]=='E' || ch[i]=='+'|| ch[i]=='-'))
      return false;
  return true;
}

//-----------------------------------------------------------------------
// fmod_pos - return modulus if pos or neg
//-----------------------------------------------------------------------
floatType fmod_pos(floatType x, floatType y)
{
  if (x < 0) return fmod(x+1,y) + y - 1;
  return fmod(x,y);
}

//-----------------------------------------------------------------------
// ktos - known number to std::string
//-----------------------------------------------------------------------
std::string brackets(const int & k)
{
  return "[" + itos(k+1) + "]";
}

}
