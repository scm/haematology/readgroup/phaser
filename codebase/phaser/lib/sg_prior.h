#ifndef __PHASER_SGPRIOR__
#define __PHASER_SGPRIOR__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/sgtbx/space_group_type.h>

namespace phaser {

//helper for sort
class prob_sg
{
  public:
  prob_sg() { }
  prob_sg(floatType r,std::string m):PROB(r),SG(m) { }
  floatType PROB;
  std::string    SG;
  bool operator<(const prob_sg &right)  const
  {return (PROB < right.PROB);}
};

inline bool sort_sgz(const cctbx::sgtbx::space_group &left,const cctbx::sgtbx::space_group &right)
{ return left.order_z() < right.order_z(); }

floatType sg_prior(std::string);

  std::vector<cctbx::sgtbx::space_group> subgroups(cctbx::sgtbx::space_group,bool=true);
  std::vector<cctbx::sgtbx::space_group> groups_sysabs(cctbx::sgtbx::space_group);

}//end namespace phaser

#endif

