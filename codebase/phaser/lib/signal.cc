#include  <phaser/main/Phaser.h>

namespace phaser {

floatType signal(floatType value,floatType mean,floatType sigma)
{
  if (sigma > 0) return (value-mean)/sigma;
  return 0;
}

}
