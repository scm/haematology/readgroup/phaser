#ifndef __PHASER_ANISO__
#define __PHASER_ANISO__
#include <phaser/main/Phaser.h>
#include <scitbx/sym_mat3.h>

namespace phaser {

/* Isotropic <-> Anisotropic  & Beta <-> U or B conversions */
  floatType  AnisoBeta2IsoB(dmat6,floatType,floatType,floatType,floatType,floatType,floatType);
  dmat6 AnisoBeta2IsoBCoeffs(floatType,floatType,floatType,floatType,floatType,floatType);
  dmat6 IsoB2AnisoBeta(floatType,floatType,floatType,floatType,floatType,floatType,floatType);
  dmat6 AnisoBeta2AnisoB(dmat6,floatType,floatType,floatType);
  dmat6 AnisoBeta2AnisoU(dmat6,floatType,floatType,floatType);
  dmat6 AnisoU2AnisoBeta(dmat6,floatType,floatType,floatType);
  dmat6 AnisoB2AnisoBeta(dmat6,floatType,floatType,floatType);
  dmat6 AnisoBetaRemoveIsoB(dmat6,floatType,floatType,floatType,floatType,floatType,floatType,floatType,floatType,floatType,floatType,floatType,floatType);

}//end namespace phaser

#endif
