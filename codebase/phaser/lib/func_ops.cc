#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/func_ops.h>

namespace phaser {

  floatType rfactorial(int n){
    if (n>1)
      return n*rfactorial(n-1);
    else
      return 1.0;
  }

//-------- below is a thread safe and (hopefully) a fast factorial -------------------
//rdo20 January 2008

  floatType ThrdSafeFact(int n)
  {
    floatType x(0), y(0);

    x = (n & 1) ? n-- : 1;
    y = n;
    while(n)
    {
      x *= y;
      y += (n -= 2);
    }
    return x;
  }

}
