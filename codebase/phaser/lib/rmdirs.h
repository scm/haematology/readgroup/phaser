#ifndef __PHASER_RMDIRS__
#define __PHASER_RMDIRS__
#include <string>

namespace phaser {

  std::string rmdirs(const std::string&);

}

#endif
