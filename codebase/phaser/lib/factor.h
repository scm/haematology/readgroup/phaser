#ifndef __PHASER_FACTOR__
#define __PHASER_FACTOR__
#include <phaser/main/Phaser.h>

namespace phaser {

  int factor(int,int,int,bool);
  std::vector<int> factors_of(int); //include original

}//end namespace phaser

#endif
