#include <phaser/lib/maths.h>
#include <phaser/main/Phaser.h>
#include <scitbx/constants.h>

namespace phaser {

floatType Fn_Gfunction(floatType twoPiRS)
{
  // Calculates G-function (Fourier transform of a sphere of radius r at resolution s.
  const floatType EPS(0.001),ONE(1),THREE(3),TEN(10);
  if (std::abs(twoPiRS) >= EPS)
    return THREE*(std::sin(twoPiRS)-twoPiRS*std::cos(twoPiRS))/fn::pow3(twoPiRS);
  else
    return ONE-fn::pow2(twoPiRS)/TEN;
}

floatType Fn_GfuncOfRSsqr(floatType rsSqr)
{
  return Fn_Gfunction(scitbx::constants::two_pi*std::sqrt(rsSqr));
}

af::shared<std::pair<floatType,floatType> > getGfuncOfRSsqrTable(int tbllen, floatType argmax)
{
  af::shared<std::pair<floatType,floatType> > GfuncOfRSsqrTable;
  GfuncOfRSsqrTable.resize(tbllen+1);
  floatType divarg(argmax/tbllen);
  for (unsigned i = 0; i < tbllen+1; i++)
  {
    floatType x0(Fn_GfuncOfRSsqr((i  )*divarg));
    floatType x1(Fn_GfuncOfRSsqr((i+1)*divarg));
    GfuncOfRSsqrTable[i].first = x0;
    GfuncOfRSsqrTable[i].second = x1 - x0;
  }
  return GfuncOfRSsqrTable;
}

floatType GfuncOfRSsqr(floatType arg)
{
  // Accuracy about 1 part in 10^8 or better, zero to first root (return zero for larger args)
  // Quadratic interpolation would use smaller tables, but is slower
  const floatType ZERO(0);
  static int tbllen(8192);
  static floatType argcut(0.51143712);
  static floatType argfac(tbllen/argcut);
  static af::shared<std::pair<floatType,floatType> > GfuncOfRSsqrTable = getGfuncOfRSsqrTable(tbllen,argcut);
  floatType fracTableArg;
  int index;
  if (arg < argcut)
  {
    fracTableArg = argfac*arg;
    index = static_cast<int>(fracTableArg);
    return GfuncOfRSsqrTable[index].first + (fracTableArg-index)*GfuncOfRSsqrTable[index].second;
  }
  else
    return ZERO;
}



}
