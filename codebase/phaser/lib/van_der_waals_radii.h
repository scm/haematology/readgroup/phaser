#ifndef __PHASER_VANDERWAALS__
#define __PHASER_VANDERWAALS__
#include <phaser/main/Phaser.h>

// Van-der-Waals radii for known elements.
//
// Created: Pavel Afonine.
//
// Sources:
// 1) Bondi, J.Phys.Chem., 68, 441, 1964  for atoms:
//      Ag,Ar,As,Au,Br,Cd,Cl,Cu,F,Ga,H,He,Hg,I,In,K,Kr,
//      Li,Mg,Na,Ne,Ni,Pb,Pd,Pt,Se,Si,Sn,Te,Tl,Xe,Zn
// 2) Fokine et al, J. Appl. Cryst. (2003). 36, 352-355 for "protein" atoms:
//    C, O, N, S, P
//
//
namespace phaser {

class vdw
{
  std::map<std::string,double> radii;

  public:

  double lookup(std::string t) { return (radii.find(t) != radii.end()) ? radii[t] : 0; }

  vdw()
  {
    radii.insert(std::pair<std::string,double>("H",  1.20 ));
    radii.insert(std::pair<std::string,double>("D",  1.20 ));
    radii.insert(std::pair<std::string,double>("He", 1.40 ));
    radii.insert(std::pair<std::string,double>("Li", 1.82 ));
    radii.insert(std::pair<std::string,double>("Be", 0.63 ));
    radii.insert(std::pair<std::string,double>("B",  1.75 ));
    radii.insert(std::pair<std::string,double>("C",  1.775));
    radii.insert(std::pair<std::string,double>("N",  1.50 ));
    radii.insert(std::pair<std::string,double>("O",  1.45 ));
    radii.insert(std::pair<std::string,double>("F",  1.47 ));
    radii.insert(std::pair<std::string,double>("Ne", 1.54 ));
    radii.insert(std::pair<std::string,double>("Na", 2.27 ));
    radii.insert(std::pair<std::string,double>("Mg", 1.73 ));
    radii.insert(std::pair<std::string,double>("Al", 1.50 ));
    radii.insert(std::pair<std::string,double>("Si", 2.10 ));
    radii.insert(std::pair<std::string,double>("P",  1.90 ));
    radii.insert(std::pair<std::string,double>("S",  1.80 ));
    radii.insert(std::pair<std::string,double>("Cl", 1.75 ));
    radii.insert(std::pair<std::string,double>("Ar", 1.88 ));
    radii.insert(std::pair<std::string,double>("K",  2.75 ));
    radii.insert(std::pair<std::string,double>("Ca", 1.95 ));
    radii.insert(std::pair<std::string,double>("Sc", 1.32 ));
    radii.insert(std::pair<std::string,double>("Ti", 1.95 ));
    radii.insert(std::pair<std::string,double>("V",  1.06 ));
    radii.insert(std::pair<std::string,double>("Cr", 1.13 ));
    radii.insert(std::pair<std::string,double>("Mn", 1.19 ));
    radii.insert(std::pair<std::string,double>("Fe", 1.26 ));
    radii.insert(std::pair<std::string,double>("Co", 1.13 ));
    radii.insert(std::pair<std::string,double>("Ni", 1.63 ));
    radii.insert(std::pair<std::string,double>("Cu", 1.40 ));
    radii.insert(std::pair<std::string,double>("Zn", 1.39 ));
    radii.insert(std::pair<std::string,double>("Ga", 1.87 ));
    radii.insert(std::pair<std::string,double>("Ge", 1.48 ));
    radii.insert(std::pair<std::string,double>("As", 0.83 ));
    radii.insert(std::pair<std::string,double>("Se", 1.90 ));
    radii.insert(std::pair<std::string,double>("Br", 1.85 ));
    radii.insert(std::pair<std::string,double>("Kr", 2.02 ));
    radii.insert(std::pair<std::string,double>("Rb", 2.65 ));
    radii.insert(std::pair<std::string,double>("Sr", 2.02 ));
    radii.insert(std::pair<std::string,double>("Y",  1.61 ));
    radii.insert(std::pair<std::string,double>("Zr", 1.42 ));
    radii.insert(std::pair<std::string,double>("Nb", 1.33 ));
    radii.insert(std::pair<std::string,double>("Mo", 1.75 ));
    radii.insert(std::pair<std::string,double>("Tc", 2.00 ));
    radii.insert(std::pair<std::string,double>("Ru", 1.20 ));
    radii.insert(std::pair<std::string,double>("Rh", 1.22 ));
    radii.insert(std::pair<std::string,double>("Pd", 1.63 ));
    radii.insert(std::pair<std::string,double>("Ag", 1.72 ));
    radii.insert(std::pair<std::string,double>("Cd", 1.58 ));
    radii.insert(std::pair<std::string,double>("In", 1.93 ));
    radii.insert(std::pair<std::string,double>("Sn", 2.17 ));
    radii.insert(std::pair<std::string,double>("Sb", 1.12 ));
    radii.insert(std::pair<std::string,double>("Te", 1.26 ));
    radii.insert(std::pair<std::string,double>("I",  1.98 ));
    radii.insert(std::pair<std::string,double>("Xe", 2.16 ));
    radii.insert(std::pair<std::string,double>("Cs", 3.01 ));
    radii.insert(std::pair<std::string,double>("Ba", 2.41 ));
    radii.insert(std::pair<std::string,double>("La", 1.83 ));
    radii.insert(std::pair<std::string,double>("Ce", 1.86 ));
    radii.insert(std::pair<std::string,double>("Pr", 1.62 ));
    radii.insert(std::pair<std::string,double>("Nd", 1.79 ));
    radii.insert(std::pair<std::string,double>("Pm", 1.76 ));
    radii.insert(std::pair<std::string,double>("Sm", 1.74 ));
    radii.insert(std::pair<std::string,double>("Eu", 1.96 ));
    radii.insert(std::pair<std::string,double>("Gd", 1.69 ));
    radii.insert(std::pair<std::string,double>("Tb", 1.66 ));
    radii.insert(std::pair<std::string,double>("Dy", 1.63 ));
    radii.insert(std::pair<std::string,double>("Ho", 1.61 ));
    radii.insert(std::pair<std::string,double>("Er", 1.59 ));
    radii.insert(std::pair<std::string,double>("Tm", 1.57 ));
    radii.insert(std::pair<std::string,double>("Yb", 1.54 ));
    radii.insert(std::pair<std::string,double>("Lu", 1.53 ));
    radii.insert(std::pair<std::string,double>("Hf", 1.40 ));
    radii.insert(std::pair<std::string,double>("Ta", 1.22 ));
    radii.insert(std::pair<std::string,double>("W",  1.26 ));
    radii.insert(std::pair<std::string,double>("Re", 1.30 ));
    radii.insert(std::pair<std::string,double>("Os", 1.58 ));
    radii.insert(std::pair<std::string,double>("Ir", 1.22 ));
    radii.insert(std::pair<std::string,double>("Pt", 1.72 ));
    radii.insert(std::pair<std::string,double>("Au", 1.66 ));
    radii.insert(std::pair<std::string,double>("Hg", 1.55 ));
    radii.insert(std::pair<std::string,double>("Tl", 1.96 ));
    radii.insert(std::pair<std::string,double>("Pb", 2.02 ));
    radii.insert(std::pair<std::string,double>("Bi", 1.73 ));
    radii.insert(std::pair<std::string,double>("Po", 1.21 ));
    radii.insert(std::pair<std::string,double>("At", 1.12 ));
    radii.insert(std::pair<std::string,double>("Rn", 2.30 ));
    radii.insert(std::pair<std::string,double>("Fr", 3.24 ));
    radii.insert(std::pair<std::string,double>("Ra", 2.57 ));
    radii.insert(std::pair<std::string,double>("Ac", 2.12 ));
    radii.insert(std::pair<std::string,double>("Th", 1.84 ));
    radii.insert(std::pair<std::string,double>("Pa", 1.60 ));
    radii.insert(std::pair<std::string,double>("U",  1.75 ));
    radii.insert(std::pair<std::string,double>("Np", 1.71 ));
    radii.insert(std::pair<std::string,double>("Pu", 1.67 ));
    radii.insert(std::pair<std::string,double>("Am", 1.66 ));
    radii.insert(std::pair<std::string,double>("Cm", 1.65 ));
    radii.insert(std::pair<std::string,double>("Bk", 1.64 ));
    radii.insert(std::pair<std::string,double>("Cf", 1.63 ));
    radii.insert(std::pair<std::string,double>("Es", 1.62 ));
    radii.insert(std::pair<std::string,double>("Fm", 1.61 ));
    radii.insert(std::pair<std::string,double>("Md", 1.60 ));
    radii.insert(std::pair<std::string,double>("No", 1.59 ));
    radii.insert(std::pair<std::string,double>("Lr", 1.58 ));
  }
};

}
#endif
