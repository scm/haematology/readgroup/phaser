#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/maths.h>
#include <phaser/include/data_solpar.h>

namespace phaser {

double DLuzzati(double Ssqr,double vrms)
{
  return std::exp(-two_pi_sq_on_three*fn::pow2(vrms)*Ssqr);
}

} // phaser
