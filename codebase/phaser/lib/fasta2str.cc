#include <phaser/lib/jiffy.h>
#include <iostream>
#include <fstream>

namespace phaser {

std::string fasta2str(std::string filename,bool& open_error)
{
  open_error = false;
  std::string seq;
  std::ifstream infile(const_cast<char*>(filename.c_str()));
  if (!infile) open_error = true;
  else
  {
    try {
      if (!infile.eof())
      {
        char c;
        std::string buffer;
        while (!infile.eof())
        {
          do {infile.get(c);}
            while ((isspace)(c) && !infile.eof()); //eat whitespace at start
  //comment line can come anywhere in the file (as it does in the pdb download fasta file)
          if (!infile.eof())
          {
            if (c == '>') std::getline(infile, buffer); //fasta comment
            else
            {
              infile.putback(c);
              std::getline(infile, buffer);
              seq += buffer;
            }
          }
        }
      }
    }
    catch (std::exception const& err) {
    //this catches the error  basic_string::_M_check where the read runs out of memory
    }
    infile.close();
  }
  return stoup(seq);
}

}
