#include <phaser/lib/maths.h>
#include <phaser/main/Phaser.h>
#include <scitbx/constants.h>
#include <phaser/lib/math_ParabolicCylinderD.h>
#include <phaser/io/Errors.h>
#include <limits>

namespace phaser {
const ParabolicCylinderD tbl_pcf = ParabolicCylinderD();

double wpcdmhalf_negarg(double);
double wpcd1half_negarg(double);
double wpcdmhalf_posarg(double);
double wpcd1half_posarg(double);

double pEosqAcen(double eosq, double sigesq)
{
  PHASER_ASSERT(sigesq >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double SQRT2(std::sqrt(2.));
  const double CROSSOVER1(6.);
  const double CROSSOVER2(std::log(std::numeric_limits<double>::max()/2.));
  double prob;
  if (sigesq > 0.)
  {
    double erfcarg((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
    if (erfcarg < CROSSOVER1)
      prob = std::exp(-eosq + fn::pow2(sigesq)/2.) *
             scitbx::math::erfc(erfcarg)/2.;
    else
    {
      double exparg(fn::pow2(eosq/sigesq)/2.);
      if (exparg < CROSSOVER2) // Used scaled erfc: erfcx(x) = exp(x^2)*erfc(x)
        prob = std::exp(-exparg) *
               scitbx::math::erfcx(erfcarg)/2.;
      else
        prob = 0.;
    }
  }
  else // Check elsewhere for negative intensity with non-zero sigma
    prob = std::exp(-std::max(eosq,0.));
  return prob;
}

double pSmallerEosqAcen(double eosq, double sigesq)
{
  if (eosq > 0.) return 1.;  // Should only be called for negative intensities
  PHASER_ASSERT(sigesq > 0.); // which can only be paired with positive sigmas

  double prob(0.);
  const double SQRT2(std::sqrt(2.));
  const double CROSSOVER1(6.);
  const double CROSSOVER2(-std::log(std::numeric_limits<double>::max()/2.));
  double erfcarg1((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
  double erfcarg2(-eosq/(SQRT2*sigesq));
  if (erfcarg1 < CROSSOVER1)
  {
    double exparg(-eosq+fn::pow2(sigesq)/2.);
    if (exparg > CROSSOVER2)
      prob -= std::exp(exparg) * scitbx::math::erfc(erfcarg1)/2.;
  }
  else
  {
    double exparg(-fn::pow2(eosq/sigesq)/2.);
    if ((erfcarg1 > -CROSSOVER1) && (exparg > CROSSOVER2))
      prob -= std::exp(exparg) * scitbx::math::erfcx(erfcarg1)/2.;
  }
  if (erfcarg2 < CROSSOVER1)
    prob += scitbx::math::erfc(erfcarg2)/2.;
  if (prob < 0.) prob = 0.;
  return prob;
}

double pLargerEosqAcen(double eosq, double sigesq)
{
  PHASER_ASSERT(sigesq >= 0.);
  double prob(0.);
  const double SQRT2(std::sqrt(2.));
  const double CROSSOVER1(6.);
  const double CROSSOVER2(-std::log(std::numeric_limits<double>::max()/2.));
  if (eosq < 0.) return 1.; // Should only be called for positive intensities
  if (sigesq == 0.)
  {
    if (eosq < -CROSSOVER2) // Avoid underflow
      prob = std::exp(-eosq);
  }
  else
  {
    double erfcarg1((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
    double erfcarg2(eosq/(SQRT2*sigesq));
    if (erfcarg1 < CROSSOVER1)
    {
      double exparg(-eosq+fn::pow2(sigesq)/2.);
      if (exparg > CROSSOVER2)
        prob += std::exp(exparg) * scitbx::math::erfc(erfcarg1)/2.;
    }
    else
    {
      double exparg(-fn::pow2(eosq/sigesq)/2.);
      if ((erfcarg1 > -CROSSOVER1) && (exparg > CROSSOVER2))
        prob += std::exp(exparg) * scitbx::math::erfcx(erfcarg1)/2.;
    }
    if (erfcarg2 < CROSSOVER1)
      prob += scitbx::math::erfc(erfcarg2)/2.;
  }
  return prob;
}

double pE2Acen_grad_hess_by_dE2(double eosq, double sigesq,
       bool do_grad, double& gradient, bool do_hess, double& hessian)
{
  // Return pEosqAcen and, optionally, 1st and 2nd derivatives wrt Eosq
  PHASER_ASSERT(sigesq >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double SQRT2(std::sqrt(2.));
  const double SQRT2PI(std::sqrt(scitbx::constants::two_pi));
  const double CROSSOVER1(6.);
  const double CROSSOVER2(std::log(std::numeric_limits<double>::max()/2.));
  double prob;
  if (sigesq > 0.)
  {
    double exparg(fn::pow2(eosq/sigesq)/2.);
    double expterm = (exparg < CROSSOVER2) ? std::exp(-exparg) : 0.;
    double erfcarg((-eosq + fn::pow2(sigesq))/(SQRT2*sigesq));
    if (erfcarg < CROSSOVER1)
    {
      prob = std::exp(-eosq + fn::pow2(sigesq)/2.) * scitbx::math::erfc(erfcarg)/2.;
      if (do_grad) gradient = expterm/(SQRT2PI*sigesq) - prob;
      if (do_hess) hessian = -expterm*eosq/(SQRT2PI*fn::pow3(sigesq)) -
                             (expterm/(SQRT2PI*sigesq) - prob);
    }
    else
    {
      if (exparg < CROSSOVER2)
      {
        prob = expterm * scitbx::math::erfcx(erfcarg)/2.;
        if (do_grad) gradient = expterm/(SQRT2PI*sigesq) - prob;
        if (do_hess) hessian = -expterm*eosq/(SQRT2PI*fn::pow3(sigesq)) -
                               (expterm/(SQRT2PI*sigesq) - prob);
      }
      else
      {
        prob = 0.;
        if (do_grad) gradient = 0.;
        if (do_hess) hessian = 0.;
      }
    }
  }
  else
  {
    PHASER_ASSERT(eosq >= 0.); // Negative intensity not allowed with zero sigma
    prob = std::exp(-eosq);
    if (do_grad) gradient = -prob;
    if (do_hess) hessian = prob;
  }
  return prob;
}

double pIobsAcen(double iobs, double sigmaN, double sigiobs)
{
  // This will differ very slightly by rounding error compared to grad_hess version (computed in
  // terms of iobs and sigmaN directly), so it is probably better to only pair this with derivatives
  // also computed in terms of Eosq
  PHASER_ASSERT(sigmaN > 0.);
  double prob = pEosqAcen(iobs/sigmaN,sigiobs/sigmaN)/sigmaN;
  return prob;
}

double pIobsAcen_grad_hess_by_dSN(double iobs, double sigmaN, double sigiobs,
       bool do_grad, double& gradient, bool do_hess, double& hessian)
{
  //Acentric probability of Iobs plus 1st & 2nd derivatives wrt sigmaN
  PHASER_ASSERT(sigmaN > 0.);
  PHASER_ASSERT(sigiobs >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double SQRT2(std::sqrt(2.));
  const double SQRTPI(std::sqrt(scitbx::constants::pi));
  const double SQRT2PI(std::sqrt(scitbx::constants::two_pi));
  const double CROSSOVER1(6.);
  const double CROSSOVER2(std::log(std::numeric_limits<double>::max()/2.));
  double prob;
  double sigmaNsqr(fn::pow2(sigmaN));
  if (sigiobs > 0.)
  {
    double exparg(fn::pow2(iobs/sigiobs)/2.);
    double expterm = (exparg < CROSSOVER2) ? std::exp(-exparg) : 0.;
    double sigiobssqr(fn::pow2(sigiobs));
    double erfcarg((-iobs*sigmaN + sigiobssqr)/(SQRT2*sigiobs*sigmaN));
    if (erfcarg < CROSSOVER1)
    {
      prob = std::exp((sigiobssqr-2.*iobs*sigmaN)/(2.*sigmaNsqr)) *
             scitbx::math::erfc(erfcarg)/(2.*sigmaN);
      if (do_grad)
        gradient = (expterm*sigiobs/SQRT2PI -
                   (sigiobssqr + sigmaN*(sigmaN - iobs)) * prob) / fn::pow3(sigmaN);
      if (do_hess)
        hessian = ((fn::pow2(sigiobssqr) + sigiobssqr*sigmaN*(5.*sigmaN-2.*iobs) +
                   sigmaNsqr*(fn::pow2(iobs)-4.*iobs*sigmaN+2.*sigmaNsqr))*prob -
                  expterm*sigiobs*(sigiobssqr+sigmaN*(4.*sigmaN-iobs))/SQRT2PI) /
                  fn::pow3(sigmaNsqr);
    }
    else
    {
      if (exparg < CROSSOVER2)
      {
        double erfcxterm = scitbx::math::erfcx(erfcarg);
        prob = expterm*erfcxterm/(2.*sigmaN);
        if (do_grad)
          gradient = expterm*(SQRT2*sigiobs*sigmaN - SQRTPI*(sigiobssqr+sigmaN*(sigmaN-iobs))*erfcxterm) /
                     (2.*SQRTPI*fn::pow2(sigmaNsqr));
        if (do_hess)
          hessian = expterm*(-SQRT2*sigiobs*sigmaN*(sigiobssqr+sigmaN*(4.*sigmaN-iobs)) +
                    SQRTPI*(fn::pow2(sigiobssqr) + sigiobssqr*sigmaN*(5.*sigmaN-2.*iobs) +
                    sigmaNsqr*(fn::pow2(iobs)-4.*iobs*sigmaN+2.*sigmaNsqr))*erfcxterm) /
                    (2.*SQRTPI*std::pow(sigmaN,7));
      }
      else
      {
        prob = 0.;
        if (do_grad) gradient = 0.;
        if (do_hess) hessian = 0.;
      }
    }
  }
  else
  {
    PHASER_ASSERT(iobs >= 0.); // Negative intensity not allowed with zero sigma
    prob = std::exp(-iobs/sigmaN)/sigmaN;
    if (do_grad)
      gradient = prob*(iobs - sigmaN)/sigmaNsqr;
    if (do_hess)
      hessian = prob * (fn::pow2(iobs)-4.*iobs*sigmaN + 2.*sigmaNsqr) /
                fn::pow2(sigmaNsqr);
  }
  return prob;
}

double pEosqCen(double eosq, double sigesq)
{
  PHASER_ASSERT(sigesq >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double CROSSOVER1(-16.), CROSSOVER2(16.);
  const double SQRT2PI(std::sqrt(scitbx::constants::two_pi));
  const double EPS(std::numeric_limits<double>::min());
  double prob;
  if (sigesq > 0)
  {
    double darg(-eosq/sigesq + sigesq/2.);
    if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
      prob = std::exp((-4.*eosq* (1.+eosq/fn::pow2(sigesq)) + fn::pow2(sigesq))/16.)
             * tbl_pcf.get(-0.5,darg)
             / (2.*std::sqrt(scitbx::constants::pi*sigesq));
    else
      if (darg < 0.) // Avoid overflow on ParabolicCylinderD with asymptotic approximation
        prob = (std::exp((fn::pow2(sigesq)-4.*eosq)/8.) /
               std::sqrt(scitbx::constants::pi*(2.*eosq-fn::pow2(sigesq)))) *
               (675675. + fn::pow2(darg) *
               (110880. + fn::pow2(darg) *
               ( 26880. + fn::pow2(darg) *
               ( 12288. + 32768.*fn::pow2(darg)))))
               / (32768.*std::pow(darg,8));
      else // Avoid underflow
        prob = (std::exp(-fn::pow2(eosq/sigesq)/2.) /
               std::sqrt(scitbx::constants::pi*(fn::pow2(sigesq)-2.*eosq))) *
               ( 675675.  + fn::pow2(darg) *
               (-110880.  + fn::pow2(darg) *
               (  26880.  + fn::pow2(darg) *
               ( -12288.  + 32768.*fn::pow2(darg)))))
               / (32768.*std::sqrt(2.)*std::pow(darg,8));
  }
  else // Check elsewhere for non-positive intensity with zero sigma
  {
    double eosq_pos(std::max(eosq,std::pow(EPS,1./3.)));
    double eo(std::sqrt(eosq_pos));
    if (eo > 1.e-6)
      prob = std::exp(-eosq_pos/2.) / (eo*SQRT2PI);
    else // Approximation good to better than 1 part in 10^12 for small eo
      prob = (1./eo - eo/2.) / SQRT2PI;
  }
  return prob;
}

double pSmallerEosqCen(double eosq, double sigesq)
{
  if (eosq > 0.) return 1.;   // Should only be called for negative intensities
  PHASER_ASSERT(sigesq > 0.); // which can only be paired with positive sigmas
  double prob;
  // Use numerical integration by Simpson's rule
  // 50 points is more than enough to get outlier threshold that agrees
  // to 4 sig figs for outlier probability of 10^-6
  int npts(50);
  double lower(std::min(eosq-3*sigesq,-6*sigesq));
  double dE2((eosq-lower)/(npts-1));
  prob = pEosqCen(lower,sigesq) + pEosqCen(eosq,sigesq);
  for (int i=1; i < npts-1; i++)
  {
    double wt = (i % 2) ? 2 : 4; // Alternating odd/even weights
    prob += wt*pEosqCen(lower+i*dE2,sigesq);
  }
  prob *= dE2/3;
  return prob;
}

double pLargerEosqCen(double eosq, double sigesq)
{
  // This calculation is sufficiently accurate for outlier detection, but
  // can be off significantly under certain circumstances for larger probabilities

  PHASER_ASSERT(sigesq >= 0.);
  if (eosq < 0.) return 1.; // Should only be called for positive intensities
  double prob(0.),erfcarg;
  double maxerfcarg(9.);
  if (sigesq > 22.) // Large measurement error Gaussian approximation
  {
    double z((eosq-1.)/std::sqrt(2.+fn::pow2(sigesq)));
    erfcarg = z/std::sqrt(2.);
    if (erfcarg < maxerfcarg) prob = scitbx::math::erfc(erfcarg)/2.;
    return prob;
  }
  erfcarg = std::sqrt(eosq/2.);
  double upper(4.+8.*sigesq);
  if (erfcarg > maxerfcarg) // Danger of underflow
    prob = 0.;
  else if (sigesq < 0.00001)
    prob = scitbx::math::erfc(erfcarg); // Sigma makes very little difference
  else
  {
    if (eosq > upper) // Good approximation to large outlier integral
      prob = scitbx::math::erfc(erfcarg) *
             pEosqCen(eosq,sigesq) / pEosqCen(eosq,0.);
    else
    {
      // Use numerical integration by Simpson's rule between eosq and
      // value where above approximation works
      // Need up to 200 points to get outlier threshold within a
      // factor of 10 for very large sigesq
      int npts(200);
      double dE2((upper-eosq)/(npts-1));
      prob = pEosqCen(eosq,sigesq) + pEosqCen(upper,sigesq);
      for (int i=1; i < npts-1; i++)
      {
        double wt = (i % 2) ? 2 : 4; // Alternating odd/even weights
        prob += wt*pEosqCen(eosq+i*dE2,sigesq);
      }
      prob *= dE2/3;
      if (std::sqrt(upper/2.) <= maxerfcarg)
        prob += scitbx::math::erfc(std::sqrt(upper/2.)) *
                pEosqCen(upper,sigesq) / pEosqCen(upper,0.);
    }
  }
  return prob;
}

double pE2Cen_grad_hess_by_dE2(double eosq, double sigesq,
       bool do_grad, double& gradient, bool do_hess, double& hessian)
{
  PHASER_ASSERT(sigesq >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double CROSSOVER1(-16.), CROSSOVER2(16.);
  const double SQRT2PI(std::sqrt(scitbx::constants::two_pi));
  const double PI(scitbx::constants::pi);
  const double EPS(std::numeric_limits<double>::min());
  double prob;
  if (sigesq > 0)
  {
    double darg(-eosq/sigesq + sigesq/2.);
    double wpcdmhalf(0),wpcd1half(0),wexpterm(0);
    if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
    {
      wpcdmhalf = tbl_pcf.get(-0.5,darg); // Not actually weighted for moderate arguments
      if (do_grad || do_hess)
        wpcd1half = tbl_pcf.get( 0.5,darg);
      // expterm is not actually weighted here
      wexpterm = std::exp((-4.*eosq* (1.+eosq/fn::pow2(sigesq)) + fn::pow2(sigesq))/16.);
    }
    else
    {
      if (darg < 0.) // Avoid overflow with asymptotic approximations to weighted ParabolicCylinderD
      {
        wpcdmhalf = wpcdmhalf_negarg(darg);
        if (do_grad || do_hess)
          wpcd1half = wpcd1half_negarg(darg);
        // expterm including inverse of weight from approximation to ParabolicCylinderD
        wexpterm = std::exp((fn::pow2(sigesq)-4.*eosq)/8.)/std::sqrt(-darg/2.);
      }
      else // Avoid underflow
      {
        wpcdmhalf = wpcdmhalf_posarg(darg);
        if (do_grad || do_hess)
          wpcd1half = wpcd1half_posarg(darg);
        wexpterm = std::exp(-fn::pow2(eosq/sigesq)/2.)/std::sqrt(darg/2.);
      }
    }
    prob = wexpterm*wpcdmhalf / (2.*std::sqrt(PI*sigesq));
    if (do_grad)
      gradient = wexpterm * (2.*wpcd1half-sigesq*wpcdmhalf)/(4.*std::sqrt(PI*fn::pow3(sigesq)));
    if (do_hess)
      hessian = wexpterm*(sigesq*(fn::pow2(sigesq)-2.)*wpcdmhalf - 2.*(2.*eosq+fn::pow2(sigesq))*wpcd1half) /
                (8.*std::sqrt(PI*std::pow(sigesq,7)));
  }
  else // Check elsewhere for non-positive intensity with zero sigma
  {
    double eosq_pos(std::max(eosq,std::pow(EPS,1./3.)));
    double eo(std::sqrt(eosq_pos));
    if (eo > 1.e-6)
    {
      prob = std::exp(-eosq_pos/2.) / (eo*SQRT2PI);
      if (do_grad) gradient = -prob/2.;
      if (do_hess) hessian = prob/4.;
    }
    else // Approximation good to better than 1 part in 10^12 for small eo
    {
      prob = (1./eo - eo/2.) / SQRT2PI;
      if (do_grad) gradient = -1./(2.*fn::pow3(eo)*SQRT2PI);
      if (do_hess) hessian = 3./(4.*std::pow(eo,5)*SQRT2PI);
    }
  }
  return prob;
}

double pIobsCen(double iobs, double sigmaN, double sigiobs)
{
  // This will differ very slightly by rounding error compared to grad_hess version (computed in
  // terms of iobs and sigmaN directly), so it is probably better to only pair this with derivatives
  // also computed in terms of Eosq
  PHASER_ASSERT(sigmaN > 0.);
  double prob = pEosqCen(iobs/sigmaN,sigiobs/sigmaN)/sigmaN;
  return prob;
}

double pIobsCen_grad_hess_by_dSN(double iobs, double sigmaN, double sigiobs,
       bool do_grad, double& gradient, bool do_hess, double& hessian)
{
  PHASER_ASSERT(sigiobs >= 0.);
  // Allow for zero sigma, falling back on probability without errors
  const double CROSSOVER1(-16.), CROSSOVER2(16.);
  const double SQRT2PI(std::sqrt(scitbx::constants::two_pi));
  const double PI(scitbx::constants::pi);
  const double EPS(std::numeric_limits<double>::min());
  double prob;
  double sigmaNsqr(fn::pow2(sigmaN));
  if (sigiobs > 0)
  {
    double sigiobssqr(fn::pow2(sigiobs));
    double darg((sigiobssqr - 2.*iobs*sigmaN)/(2.*sigiobs*sigmaN));
    double wpcdmhalf(0),wpcd1half(0),pfactor(0),gfactor(0),hfactor(0); //-Wall init
    if (darg >= CROSSOVER1 && darg <= CROSSOVER2) // Moderate arguments
    {
      wpcdmhalf = tbl_pcf.get(-0.5,darg); // Not actually weighted for moderate arguments
      if (do_grad || do_hess)
        wpcd1half = tbl_pcf.get( 0.5,darg);
      // Calculate factors to apply to (linear functions of) D-functions for prob, gradient and hessian
      double expterm = std::exp((fn::pow2(sigiobs/sigmaN)-4.*fn::pow2(iobs/sigiobs)-4.*iobs/sigmaN)/16.);
      pfactor = expterm / (2.*std::sqrt(PI*sigiobs*sigmaN));
    }
    else
    {
      if (darg < 0.) // Avoid overflow for large negative arguments of ParabolicCylinderD,
                     // using asymptotic approximations to weighted ParabolicCylinderD
      {
        wpcdmhalf = wpcdmhalf_negarg(darg);
        if (do_grad || do_hess)
          wpcd1half = wpcd1half_negarg(darg);
        // Factors including inverse of weight from approximation to ParabolicCylinderD
        pfactor = std::exp((sigiobssqr-4.*sigmaN*iobs)/(8.*sigmaNsqr)) /
                  std::sqrt(-2.*PI*darg*sigiobs*sigmaN);
      }
      else // Avoid underflow for large positive arguments of ParabolicCylinderD, different weights
      {
        wpcdmhalf = wpcdmhalf_posarg(darg);
        if (do_grad || do_hess)
          wpcd1half = wpcd1half_posarg(darg);
        pfactor = std::exp(-fn::pow2(iobs/sigiobs)/2.) /
                  (std::sqrt(PI*(sigiobssqr-2.*iobs*sigmaN)));
      }
    }
    if (do_grad) gfactor = pfactor / (4.*fn::pow3(sigmaN));
    if (do_hess) hfactor = pfactor / fn::pow2(4.*fn::pow3(sigmaN));
    prob = pfactor*wpcdmhalf;
    if (do_grad)
      gradient = gfactor*(2.*sigiobs*sigmaN*wpcd1half - (sigiobssqr+2.*sigmaN*(sigmaN-iobs))*wpcdmhalf);
    if (do_hess)
      hessian = hfactor*(
                (sigiobssqr*(sigiobssqr+2.*sigmaN*(7.*sigmaN-2.*iobs)) +
                 4.*sigmaNsqr*(fn::pow2(iobs)-6.*iobs*sigmaN+3.*sigmaNsqr))*wpcdmhalf -
                2.*sigiobs*sigmaN*(sigiobssqr+2.*sigmaN*(6.*sigmaN-iobs))*wpcd1half);
  }
  else // Check elsewhere for non-positive intensity with zero sigma
  {
    double eosq(iobs/sigmaN);
    double eosq_pos(std::max(eosq,std::pow(EPS,1./3.)));
    double eo(std::sqrt(eosq_pos));
    if (eo > 1.e-6)
      prob = std::exp(-eosq_pos/2.) / (eo*sigmaN*SQRT2PI);
    else // Approximation good to better than 1 part in 10^12 for small eo
      prob = (1./eo - eo/2.) / (sigmaN*SQRT2PI);
    if (do_grad) gradient = prob*(iobs-sigmaN)/(2.*sigmaNsqr);
    if (do_hess)
      hessian = prob*(fn::pow2(iobs)-6.*iobs*sigmaN+3.*sigmaNsqr)/(4.*fn::pow2(sigmaNsqr));
  }
  return prob;
}

double pEosqTwinAcen(double eosq, double sigesq, double twinfrac)
{
  // Acentric likelihood function for probability of hemihedrally-twinned normalised intensity
  const double SQRT2(std::sqrt(2.));
  const double SQRT2overPI(std::sqrt(2./scitbx::constants::pi));
  double f = (twinfrac <= 0.5) ? twinfrac : 1.-twinfrac;
  double fcompl(1.-f);
  PHASER_ASSERT(f >= 0.);
  double varesq(fn::pow2(sigesq));
  double prob;
  if (f < 0.001) // Should have continuous approximation if used for optimisation
    prob = pEosqAcen(eosq,sigesq);
  else if (f < 0.499)
  {
    double arg3 = (-eosq*f + varesq) / (SQRT2*f*sigesq);
    double term3 = (arg3 < 0.) ?
                   std::exp(-(2*eosq*f - varesq)/(2*fn::pow2(f)))*scitbx::math::erfc(arg3) :
                   std::exp(-fn::pow2(eosq)/(2*varesq)) * scitbx::math::erfcx(arg3);
    double erfcarg = -(eosq*fcompl - varesq)/(SQRT2*fcompl*sigesq);
    double exparg = -(2*eosq*fcompl - varesq)/(2*fn::pow2(fcompl));
    prob = (std::exp(exparg)*scitbx::math::erfc(erfcarg) - term3) / (2.-4*f);
  }
  else // Should be the quadratic approximation if used for optimisation
  {
    double diff1 = eosq - 2*varesq;
    double erfcarg = -diff1/(SQRT2*sigesq);
    prob = 2*(std::exp(-fn::pow2(eosq)/(2*varesq)) *
           SQRT2overPI*sigesq + std::exp(-2*(eosq-varesq))*diff1*scitbx::math::erfc(erfcarg));
  }
  return prob;
}

double wpcdmhalf_negarg(double darg)
{
  // Asymptotic approximation to wt*ParabolicCylinderD(-1/2,darg) for negative darg
  // where wt = Exp[-darg^2/4]*Sqrt[-darg/2]
  double wpcdmhalf = ( 675675. + fn::pow2(darg) *
                      (110880. + fn::pow2(darg) *
                      ( 26880. + fn::pow2(darg) *
                      ( 12288. + 32768.*fn::pow2(darg)))))
                      / (32768.*std::pow(darg,8));
  return wpcdmhalf;
}

double wpcd1half_negarg(double darg)
{
  // Asymptotic approximation to wt*ParabolicCylinderD(1/2,darg) for negativ darg
  // where wt = Exp[-darg^2/4]*Sqrt[-darg/2]
  double wpcd1half = (11486475. + fn::pow2(darg) *
                      (1441440. + fn::pow2(darg) *
                      ( 241920. + fn::pow2(darg) *
                      ( 61440. + 32768.*fn::pow2(darg)))))
                      / (65536.*std::pow(darg,9));
  return wpcd1half;
}

double wpcdmhalf_posarg(double darg)
{
  // Asymptotic approximation to wt*ParabolicCylinderD(-1/2,darg) for positive darg
  // where wt = Exp[darg^2/4]*Sqrt[darg/2]
  const double SQRT2(std::sqrt(2.));
  double wpcdmhalf = (  675675. + fn::pow2(darg) *
                      (-110880. + fn::pow2(darg) *
                      (  26880. + fn::pow2(darg) *
                      ( -12288. + 32768.*fn::pow2(darg)))))
                      / (32768.*SQRT2*std::pow(darg,8));
  return wpcdmhalf;
}

double wpcd1half_posarg(double darg)
{
  // Asymptotic approximation to wt*ParabolicCylinderD(1/2,darg) for positive darg
  // where wt = Exp[darg^2/4]*Sqrt[darg/2]
  const double SQRT2(std::sqrt(2.));
  double wpcd1half = ( 2297295. + fn::pow2(darg) *
                      (-360360. + fn::pow2(darg) *
                      (  80640. + fn::pow2(darg) *
                      ( -30720. + fn::pow2(darg) *
                      (  32768. + 262144.*fn::pow2(darg))))))
                      / (262144.*SQRT2*std::pow(darg,9));
  return wpcd1half;
}

}
