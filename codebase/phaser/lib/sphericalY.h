#ifndef __PHASER_SPHERICAL_Y__
#define __PHASER_SPHERICAL_Y__
#include <phaser/main/Phaser.h>
#include <phaser/lib/func_ops.h>
#include <algorithm>

namespace phaser {

  struct p3Dc { floatType x,y,z; };
  struct p3Dp { floatType r,theta,phi; };

  p3Dp toPolar(p3Dc);
  p3Dc toCart(p3Dp);

  floatType spherical(int,int,floatType,lnfactorial&);
  floatType legendre(floatType,int,int);
  floatType sphbessel(int,floatType);

  class HKL
  {
    public:
      p3Dc hkl;
      p3Dp htp;
      bool flipped;
      floatType intensity;

      HKL() { hkl.x = hkl.y = hkl.z =  htp.r = htp.theta = htp.phi = intensity = flipped = 0; }

      bool operator<(const HKL &right)  const
      { //sort on phi
        return (right.htp.phi < htp.phi);
      };

  };

  class HKL_clustered
  {
    public:
      std::vector<std::vector<HKL> > clustered;
      HKL_clustered() {clustered.clear();}

      void add(HKL& this_hkl)
      {
        floatType COSTHETA_LIMIT(10.0e-04);
        int c = 0;
        for (; c < clustered.size(); c++)
          if (fabs(std::cos(clustered[c][0].htp.theta) - std::cos(this_hkl.htp.theta)) <  COSTHETA_LIMIT)
          {
            clustered[c].push_back(this_hkl);
            return;
          }
        clustered.push_back(std::vector<HKL>(0));
        clustered[c].push_back(this_hkl);
      }

      void shuffle()
      {
        random_shuffle(clustered.begin(),clustered.end());
      }

      int size()
      {
        int count = 0;
        for (int c = 0; c < clustered.size(); c++)
          count += clustered[c].size();
        return count;
      }
  };

}//end namespace phaser

#endif
