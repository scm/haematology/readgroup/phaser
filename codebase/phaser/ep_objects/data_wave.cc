//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/ep_objects/data_wave.h>

namespace phaser {

  void data_wave::initAnom()
  {
    POS.set_default_id("(+)");
    NEG.set_default_id("(-)");
    ANOMALOUS = true;
  }

  void data_wave::initMean()
  {
    POS.set_default_id("");
    NEG.clear();
    ANOMALOUS = false;
    if (XTALID == "") XTALID = "xtal";
    if (WAVEID == "") WAVEID = "data";
  }

  int data_wave::setup_error()
  {
    if (POS.F_ID != "" && POS.I_ID != "") return 1;
    if (POS.F_ID == "" && POS.I_ID == "") return 2;
    if (ANOMALOUS && POS.F_ID != "" && (POS.SIGF_ID == "" || NEG.F_ID == "" || NEG.SIGF_ID == "")) return 3;
    if (ANOMALOUS && POS.I_ID != "" && (POS.SIGI_ID == "" || NEG.I_ID == "" || NEG.SIGI_ID == "")) return 4;
    return 0;
  }

  af_float data_wave::getFmean()
  {
    if (ANOMALOUS)
    {
      af_float FMEAN(POS.F.size(),0);
      for (int r = 0; r < POS.F.size(); r++)
      {
      bool Fpos_really_present = POS.PRESENT[r] && POS.F[r];
      bool Fneg_really_present = NEG.PRESENT[r] && NEG.F[r];
      if (Fpos_really_present || Fneg_really_present)
      {
        FMEAN[r] = ((Fpos_really_present ? POS.F[r] : 0) + (Fneg_really_present ? NEG.F[r] : 0)) /
                   ((Fpos_really_present ? 1 : 0) + (Fneg_really_present ? 1 : 0));
      }
      }
      return FMEAN;
    }
    return POS.F;
  }

  af_float data_wave::getFpos()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No Fpos for non-anomalous data");
    return POS.F;
  }

  af_float data_wave::getFneg()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No Fneg for non-anomalous data");
    return NEG.F;
  }

  af_float data_wave::getSIGFmean(af_bool cent)
  {
    if (ANOMALOUS)
    {
      af_float SIGFMEAN(POS.SIGF.size(),0);
      for (int r = 0; r < POS.SIGF.size(); r++)
      {
      bool Fpos_really_present = POS.PRESENT[r] && POS.F[r];
      bool Fneg_really_present = NEG.PRESENT[r] && NEG.F[r];
      if  ((Fpos_really_present || Fneg_really_present) && (!cent.size() || (cent.size() && !cent[r])))
      {
        SIGFMEAN[r] = std::sqrt((Fpos_really_present ? fn::pow2(POS.SIGF[r]) : 0) + (Fneg_really_present ? fn::pow2(NEG.SIGF[r]) : 0)) /
                                ((Fpos_really_present ? 1 : 0) + (Fneg_really_present ? 1 : 0));
      }
      else if  ((Fpos_really_present || Fneg_really_present) && (cent.size() && cent[r]))
      { //if centric array is supplied, and refl is centric, then just take the + or - sigma value
        //both POS.PRESENT  and NEG.PRESENT may be set
        SIGFMEAN[r] = Fpos_really_present ? POS.SIGF[r] : NEG.SIGF[r];
      }
      }
      return SIGFMEAN;
    }
    return POS.SIGF;
  }

  af_float data_wave::getSIGFpos()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No SIGFpos for non-anomalous data");
    return POS.SIGF;
  }

  af_float data_wave::getSIGFneg()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No SIGFneg for non-anomalous data");
    return NEG.SIGF;
  }

  af_bool data_wave::getPmean()
  {
    if (ANOMALOUS)
    {
      af_bool PRESENT(POS.PRESENT.size());
      for (int r = 0; r < POS.PRESENT.size(); r++)
        PRESENT[r] = (POS.PRESENT[r] || NEG.PRESENT[r]);
      return PRESENT;
    }
    return POS.PRESENT;
  }

  af_bool data_wave::getPpos()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No present flag for non-anomalous data");
    return POS.PRESENT;
  }

  af_bool data_wave::getPneg()
  {
    if (!ANOMALOUS) throw PhaserError(FATAL,"No present flag for non-anomalous data");
    return NEG.PRESENT;
  }

  std::string data_wave::unparse()
  {
    std::string Card("");
    Card += "CRYSTAL " + XTALID + " DATASET " + WAVEID;
    ANOMALOUS ?
      Card += " LABIN " + POS.unparse("(+)",false) + " " + NEG.unparse("(-)"):
      Card += " LABIN " + POS.unparse("");
    return Card;
  }

  void data_wave::erase(int r)
  {
    POS.F.erase(POS.F.begin()+r);
    POS.SIGF.erase(POS.SIGF.begin()+r);
    POS.PRESENT.erase(POS.PRESENT.begin()+r);
    NEG.F.erase(NEG.F.begin()+r);
    NEG.SIGF.erase(NEG.SIGF.begin()+r);
    NEG.PRESENT.erase(NEG.PRESENT.begin()+r);
  }

  void data_wave::push_back_f(floatType f,floatType sigf,bool p)
  {
    if (inputF())
    {
    POS.F.push_back(f);
    POS.SIGF.push_back(sigf);
    POS.PRESENT.push_back(p);
    return;
    }
    POS.I.push_back(f);
    POS.SIGI.push_back(sigf);
    POS.PRESENT.push_back(p);
  }

  void data_wave::push_back_fneg(floatType f,floatType sigf,bool p)
  {
    if (inputF())
    {
    NEG.F.push_back(f);
    NEG.SIGF.push_back(sigf);
    NEG.PRESENT.push_back(p);
    return;
    }
    NEG.I.push_back(f);
    NEG.SIGI.push_back(sigf);
    NEG.PRESENT.push_back(p);
  }

  void data_wave::convert_NAT_to_SAD()
  {
    PHASER_ASSERT(POS.F.size() || POS.I.size());
    if (POS.I.size())
    {
      NEG.I.resize(POS.I.size());
      NEG.SIGI.resize(POS.I.size());
      NEG.PRESENT.resize(POS.I.size());
      for (int r = 0; r < POS.I.size(); r++)
      {
        NEG.I[r] = 0;
        NEG.SIGI[r] = 0;
        NEG.PRESENT[r] = false;
      }
      NEG.I_ID = "I_ZERO";
      NEG.SIGI_ID = "SIGI_ZERO";
      ANOMALOUS = true;
      return;
    }
    NEG.F.resize(POS.F.size());
    NEG.SIGF.resize(POS.F.size());
    NEG.PRESENT.resize(POS.F.size());
    for (int r = 0; r < POS.F.size(); r++)
    {
      NEG.F[r] = 0;
      NEG.SIGF[r] = 0;
      NEG.PRESENT[r] = false;
    }
    NEG.F_ID = "F_ZERO";
    NEG.SIGF_ID = "SIGF_ZERO";
    ANOMALOUS = true;
  }

} //phaser
