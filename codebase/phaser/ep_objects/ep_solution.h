//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EP_SOLUTION_Class__
#define __EP_SOLUTION_Class__
#include <phaser/ep_objects/af_atom.h>
#include <phaser/include/data_peak.h>

namespace phaser {

class Output;

class ep_solution
{
  private:
  std::vector<af_atom>  SET;

  public:
  ep_solution() { SET.clear(); }
  ep_solution(std::vector<af_atom> const& SET_) : SET(SET_) {}

  double top_LLG();
  void   sort_LLG();
  void   sort_atom();

  // vector interface
  typedef std::vector<af_atom>::value_type value_type;
  typedef std::vector<af_atom>::size_type size_type;
  typedef std::vector<af_atom>::difference_type difference_type;
  typedef std::vector<af_atom>::reference reference;
  typedef std::vector<af_atom>::const_reference const_reference;
  typedef std::vector<af_atom>::iterator iterator;
  typedef std::vector<af_atom>::const_iterator const_iterator;

  template<typename InputIterator>
  ep_solution(InputIterator first, InputIterator last) : SET( first, last ) {}

  const_reference operator[](size_type t) const { return SET[t]; }
  reference operator[](size_type t) { return SET[t]; }
  iterator begin() { return SET.begin(); }
  iterator end() { return SET.end(); }
  const_iterator begin() const { return SET.begin(); }
  const_iterator end() const { return SET.end(); }
  size_type size() const { return SET.size(); }
  int num_packs() const;
  iterator insert(iterator pos, const value_type& mrset) { return SET.insert(pos, mrset); }
  template<typename InputIterator> void insert(iterator pos, InputIterator first, InputIterator last) { SET.insert(pos, first, last); }
  iterator erase(iterator pos) { return SET.erase(pos); }
  iterator erase(iterator first, iterator last) { return SET.erase(first, last); }
  void push_back(const value_type& mrset) { SET.push_back(mrset); }
  void pop_back() { SET.pop_back(); }
  void increment() { SET.push_back(af_atom()); }
  void clear() { SET.clear(); }
  void resize(int i) { SET.resize(i); }

  void set_ep_solution(const ep_solution & init)
  {
    SET.resize(init.SET.size());
    for (int i = 0; i < init.SET.size(); i++)
      SET[i] = init.SET[i];
  }

  ep_solution(const ep_solution & init)
  { set_ep_solution(init); }

  const ep_solution& operator=(const ep_solution& right)
  {
    if (&right != this) set_ep_solution(right);
    return *this;
  }

  ep_solution& operator+=(const ep_solution &rhs)
  {
    for (int i = 0; i < rhs.SET.size(); i++)
      SET.push_back(rhs.SET[i]);
    return *this;
  }

  public:
  double purge(data_peak_purge);
  std::string unparse(std::string xtalid="XTAL",bool logfile=false,int tab=0);
};

} //phaser

#endif
