//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_XTAL_Class__
#define __DATA_XTAL_Class__
#include <phaser/ep_objects/data_wave.h>
#include <phaser/ep_objects/data_atom.h>
#include <phaser/ep_objects/ep_solution.h>

namespace phaser {

class data_xtal
{
  public:
    data_xtal(std::string xtalid="") : ATOM_XTALID(xtalid) { WAVE.clear(); ATOM_SET.clear(); }

  void set_data_xtal(data_xtal& init)
  {
    WAVE = init.WAVE;
    ATOM_SET = init.ATOM_SET;
    ATOM_XTALID = init.ATOM_XTALID;
  }

  std::map<std::string,data_wave> WAVE;
  ep_solution                     ATOM_SET;
  std::string                     ATOM_XTALID;

  std::string getXtalid() const { return ATOM_XTALID; }
  int         size() const      { return WAVE.size(); }

  data_wave& operator[] (int w)
  {
   // if (w < 0 || w >= WAVE.size())
   //   throw PhaserError(FATAL,"w = \"" + itos(w) + "\" not found in data_ep object");
    waveIter wi = WAVE.begin();
    for (int i = 0; i < w; i++) wi++;
    return wi->second;
  }

  data_wave& operator[] (std::string waveid)
  {
    if (waveid == "")
      return WAVE.begin()->second;
  //  if (WAVE.find(waveid) == WAVE.end())
  //    throw PhaserError(FATAL,"waveid = \"" + waveid + "\" not found in data_ep object");
    return WAVE[waveid];
  }

  std::string unparse();
  const ep_solution& getDotSol() { return ATOM_SET; }
};

typedef std::map<std::string,data_xtal>::iterator xtalIter;

} //phaser

#endif
