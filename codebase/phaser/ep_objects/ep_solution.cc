//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/ep_solution.h>
#include <phaser/ep_objects/af_atom.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

  void ep_solution::sort_LLG()
  {
    std::sort(SET.begin(),SET.end());
    std::reverse(SET.begin(),SET.end());
  }

  void ep_solution::sort_atom()
  {
    for (int i = 0; i < SET.size(); i++)
      std::sort(SET[i].begin(),SET[i].end());
  }

  double ep_solution::top_LLG()
  {
    double top = -std::numeric_limits<double>::max();
    for (int i = 0; i < SET.size(); i++)
      top = std::max(top,SET[i].LLG);
    return top;
  }

  std::string ep_solution::unparse(std::string xtalid,bool logfile,int tab)
  {
    std::string Card;
    for (int i = 0; i < SET.size(); i++)
    {
      Card += "ATOM CRYSTAL " + xtalid + " SET\n";
      Card += SET[i].unparse(xtalid,logfile,tab);
    }
    return Card;
  }

  double ep_solution::purge(data_peak_purge PURGE)
  {
    std::sort(SET.begin(),SET.end());
    std::reverse(SET.begin(),SET.end());
    //remove low occupancy first
    for (int t = SET.size()-1; t >= 0; t--) //erase backwards
    {
      if (SET[t].size() == 1) continue;
      double maxocc = SET[t][0].SCAT.occupancy;
      for (int s = 1; s < SET[t].size()-1; s++)
        maxocc = std::max(SET[t][s].SCAT.occupancy,maxocc);
      int s = SET[t].size()-1;
      if (SET[t][s].SCAT.occupancy < maxocc*PURGE.OCCUPANCY)
        SET.erase(SET.begin()+t);
    }
    if (!SET.size()) return 0;
    //now look at other purge criteria
    if (PURGE.by_sigma())
    {
      double proxy_sigma = (SET[0].LLG > 0) ? std::sqrt(SET[0].LLG) : 0;
      double proxy_threshold = SET[0].LLG - PURGE.get_cutoff()*proxy_sigma;
      for (int t = SET.size()-1; t >= 0; t--) //erase backwards
      {
        if (SET[t].LLG < proxy_threshold)
          SET.erase(SET.begin()+t);
      }
    }
    else if (PURGE.by_number() && PURGE.get_cutoff() < SET.size())
    {
      std::vector<af_atom> tmpSET;
      for (int i = 0; i < PURGE.get_cutoff(); i++)
      {
        floatType maxValue = SET[i].LLG;
        for (int t = i; t < SET.size(); t++)
        {
          if (SET[t].LLG >= maxValue)
          {
            maxValue = SET[t].LLG;
            af_atom tmp = SET[i];
            SET[i] = SET[t];
            SET[t] = tmp;
          }
        }
      }
      for (int i = 0; i < PURGE.get_cutoff(); i++)
        tmpSET.push_back(SET[i]);
      SET = tmpSET;
    }
    else if (PURGE.by_percent()) {} //can't do this as there is no mean
    else if (PURGE.by_all()) {}
    if (!SET.size()) return 0;
    //find lowest llg for reporting cutoff
    double cutoff = SET[0].LLG;
    for (int i = 0; i < SET.size(); i++)
      cutoff = std::min(cutoff,SET[i].LLG);
    floatType tol(1.0e-06);
    return cutoff - tol;
  }

} //phaser
