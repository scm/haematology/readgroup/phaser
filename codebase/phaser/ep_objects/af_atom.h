//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __AF_ATOM_Class__
#define __AF_ATOM_Class__
#include <phaser/ep_objects/data_atom.h>
#include <phaser/include/VarianceEP.h>

namespace phaser {

class af_atom
{
  public:
  std::vector<data_atom>  SET;
  int    NUM;
  double RFAC,LLG,FAST_LLG,ZSCORE;
  bool   CENTROSYMMETRIC;
  VarianceEP VAR;

  af_atom(int s=0): NUM(0),RFAC(0),LLG(0),FAST_LLG(0),ZSCORE(0),CENTROSYMMETRIC(false)
  { SET.resize(s);  VAR = VarianceEP(); }

  bool operator<(const af_atom &right) const
  { if (LLG && right.LLG) return LLG < right.LLG; return FAST_LLG < right.FAST_LLG; }

  typedef std::vector<data_atom>::value_type value_type;
  typedef std::vector<data_atom>::size_type size_type;
  typedef std::vector<data_atom>::difference_type difference_type;
  typedef std::vector<data_atom>::reference reference;
  typedef std::vector<data_atom>::const_reference const_reference;
  typedef std::vector<data_atom>::iterator iterator;
  typedef std::vector<data_atom>::const_iterator const_iterator;

  template<typename InputIterator>
  af_atom(InputIterator first, InputIterator last) : SET( first, last ) {}
  template<typename InputIterator>
  void insert(iterator pos, InputIterator first, InputIterator last) { SET.insert(pos, first, last); }

  const_reference operator[](size_type t) const { return SET[t]; }
  reference operator[](size_type t) { return SET[t]; }
  iterator begin() { return SET.begin(); }
  iterator end() { return SET.end(); }
  const_iterator begin() const { return SET.begin(); }
  const_iterator end() const { return SET.end(); }
  size_type size() const { return SET.size(); }
  iterator insert(iterator pos, const value_type& v) { return SET.insert(pos, v); }
  iterator erase(iterator pos) { return SET.erase(pos); }
  iterator erase(iterator first, iterator last) { return SET.erase(first, last); }
  void push_back(const value_type& v) { SET.push_back(v); }
  void pop_back() { SET.pop_back(); }
  void clear() { SET.clear(); }
  void resize(size_type t) { SET.resize(t); }
  const value_type& back() { return SET.back(); }

  // Fake method required for python sets
  bool operator==(const af_atom& other) const
  {
    return
      LLG == other.LLG;
  }


  public:
  std::string unparse(std::string xtalid,bool logfile=false,int tab=0);
  void writePdb(FILE*,cctbx::uctbx::unit_cell);
  bool no_scattering();
  af::shared<xray::scatterer<double> > get_scatterers();
  af_atom other_hand(std::string,af::double6);
};

} //phaser

#endif
