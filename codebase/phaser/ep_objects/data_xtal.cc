//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/data_xtal.h>

namespace phaser {

  std::string data_xtal::unparse()
  {
    std::string Card("");
    for (waveIter wi = WAVE.begin(); wi != WAVE.end(); wi++)
      Card += wi->second.unparse();
    return Card;
  }

} //phaser
