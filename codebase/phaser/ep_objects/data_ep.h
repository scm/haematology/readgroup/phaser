//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_EP_Class__
#define __DATA_EP_Class__
#include <phaser/ep_objects/data_xtal.h>
#include <phaser/include/data_refl.h>
#include <cctbx/uctbx.h>

namespace phaser {

class data_ep
{
  public:
    data_ep() { }

  std::map<std::string,data_xtal> XTAL;
  af::shared<miller::index<int> > MILLER;

  public:
    void      push_back_miller(miller::index<int> hkl) { MILLER.push_back(hkl); }
    int       nrefl()                      { return MILLER.size(); }

  void clear() { XTAL.clear(); MILLER.clear(); }
  int size() const { return XTAL.size(); }

  data_xtal& operator[] (int x)
  {
  //  if (x < 0 || x >= XTAL.size())
  //    throw PhaserError(FATAL,"x = \"" + itos(x) + "\" not found in data_ep object");
    xtalIter xi = XTAL.begin();
    for (int i = 0; i < x; i++) xi++;
    return xi->second;
  }

  data_xtal& operator[] (std::string xtalid)
  {
    if (xtalid == "")
      return XTAL.begin()->second;
  //  if (XTAL.find(xtalid) == XTAL.end())
 //     throw PhaserError(FATAL,"xtalid = \"" + xtalid + "\" not found in data_ep object");
    return XTAL[xtalid];
  }

  data_wave& operator() (int x, int w)
  {
    if (x == 0 && w == 0)
      return XTAL.begin()->second.WAVE.begin()->second;
    xtalIter xi = XTAL.begin();
    for (int i = 0; i < x; i++) xi++;
    if (w == 0) //first dataset
      return xi->second.WAVE.begin()->second;
    waveIter wi = xi->second.WAVE.begin();
    for (int i = 0; i < w; i++) wi++;
    return wi->second;
  }

  data_wave& operator() (std::string xtalid="", std::string waveid="")
  {
    if (xtalid == "" && waveid == "")
      return XTAL.begin()->second.WAVE.begin()->second;
  //  if (XTAL.find(xtalid) == XTAL.end())
  //    throw PhaserError(FATAL,"xtalid = \"" + xtalid + "\" not found in data_ep object");
    if (waveid == "") //first dataset
      return XTAL[xtalid].WAVE.begin()->second;
  //  if (XTAL[xtalid].WAVE.find(waveid) == XTAL[xtalid].WAVE.end())
  //    throw PhaserError(FATAL,"waveid = \"" + xtalid + "\" not found in data_ep object");
    return XTAL[xtalid].WAVE[waveid];
  }

  public:
  void truncate_resolution(cctbx::uctbx::unit_cell,double,double);
  void init(std::string);
  void init(std::string,std::string);
  void initAnom(std::string,std::string);
  void initMean(std::string,std::string);
  std::string unparse();
  std::string experiment_type();
  bool is_experiment_type(std::string);
  bool setAnomData(std::string,std::string,af_float,af_float,af_bool,af_float,af_float,af_bool);
  void convert_NAT_to_SAD();
  data_refl get_data_refl();
  void setNative(af_float,af_float,std::string);
};

} //phaser

#endif
