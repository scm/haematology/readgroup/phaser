//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/data_ep.h>
#include <phaser/include/data_refl.h>
#include <cctbx/uctbx.h>

namespace phaser {

  void data_ep::truncate_resolution(cctbx::uctbx::unit_cell cctbxUC,floatType hires,floatType lores)
  {
    for (int r = MILLER.size()-1; r >= 0; r--)
    {
      floatType resolution = cctbxUC.d(MILLER[r]);
      if (resolution < hires || resolution > lores)
      {
        MILLER.erase(MILLER.begin()+r);
        for (xtalIter xi = XTAL.begin(); xi != XTAL.end(); xi++)
        {
          for (waveIter wi = xi->second.WAVE.begin(); wi != xi->second.WAVE.end(); wi++)
          {
            wi->second.erase(r);
          }
        }
      }
    }
  }

  void data_ep::init(std::string xtalid)
  {
    if (XTAL.find(xtalid) == XTAL.end()) XTAL[xtalid] = data_xtal(xtalid);
  }

  void data_ep::init(std::string xtalid,std::string waveid)
  {
    if (XTAL.find(xtalid) == XTAL.end())
    {
      XTAL[xtalid] = data_xtal(xtalid);
      if (XTAL[xtalid].WAVE.find(xtalid) == XTAL[xtalid].WAVE.end())
        XTAL[xtalid].WAVE[waveid] = data_wave(xtalid,waveid);
    }
  }

  void data_ep::initAnom(std::string xtalid,std::string waveid)
  {
    init(xtalid,waveid);
    XTAL[xtalid].WAVE[waveid].initAnom();
  }

  void data_ep::initMean(std::string xtalid,std::string waveid)
  {
    init(xtalid,waveid);
    XTAL[xtalid].WAVE[waveid].initMean();
  }

  std::string data_ep::unparse()
  {
    std::string Card("");
    for (xtalIter xi = XTAL.begin(); xi != XTAL.end(); xi++)
      Card += xi->second.unparse();
    return Card;
  }

  std::string data_ep::experiment_type()
  {
    std::string type("MIRAS");
    if (XTAL.size() == 1 && XTAL.begin()->second.WAVE.size() == 1)
    {
      if (XTAL.begin()->second.WAVE.begin()->second.isMean())
        type = "NAT";
      if (XTAL.begin()->second.WAVE.begin()->second.isAnom())
        type = "SAD";
    }
    else if (XTAL.size() == 2)
    {
      xtalIter x1 = XTAL.begin();
      xtalIter x2 = x1; x2++;
      if (x1->second.WAVE.size() == 1 && x2->second.WAVE.size() == 1)
      {
        waveIter w1 = x1->second.WAVE.begin();
        waveIter w2 = x2->second.WAVE.begin();
        if (w1->second.isMean() && w2->second.isMean())
          type = "SIR";
        if ((w1->second.isMean() && w2->second.isAnom()) ||
            (w1->second.isAnom() && w2->second.isMean()))
          type = "SIRAS";
      }
    }
    else if (XTAL.size() == 1 && XTAL.begin()->second.WAVE.size() == 2)
    {
      waveIter w1 = XTAL.begin()->second.WAVE.begin();
      waveIter w2 = w1; w2++;
      if (w1->second.isAnom() && w2->second.isAnom())
        type = "MAD";
    }
    return type;
  }

  bool data_ep::is_experiment_type(std::string input_type)
  { return (experiment_type() == input_type); }

  bool data_ep::setAnomData(std::string xtalid,std::string waveid,af_float fpos,af_float sigfpos,af_bool ppos,af_float fneg,af_float sigfneg,af_bool pneg)
  {
    init(xtalid,waveid);
    XTAL[xtalid].WAVE[waveid].POS.F = fpos;
    XTAL[xtalid].WAVE[waveid].POS.SIGF = sigfpos;
    XTAL[xtalid].WAVE[waveid].POS.PRESENT = ppos;
    XTAL[xtalid].WAVE[waveid].NEG.F = fneg;
    XTAL[xtalid].WAVE[waveid].NEG.SIGF = sigfneg;
    XTAL[xtalid].WAVE[waveid].NEG.PRESENT = pneg;
    XTAL[xtalid].WAVE[waveid].initAnom(); //must be after, arrays must be full
    int reference(MILLER.size());
    if (XTAL[xtalid].WAVE[waveid].POS.F.size() != reference ||
        XTAL[xtalid].WAVE[waveid].POS.SIGF.size() != reference ||
        XTAL[xtalid].WAVE[waveid].POS.PRESENT.size() != reference ||
        XTAL[xtalid].WAVE[waveid].NEG.F.size() != reference ||
        XTAL[xtalid].WAVE[waveid].NEG.SIGF.size() != reference ||
        XTAL[xtalid].WAVE[waveid].NEG.PRESENT.size() != reference)
          { return false; }
    return true;
  }

  void data_ep::convert_NAT_to_SAD()
  {
    //PHASER_ASSERT(is_experiment_type("NAT"));
    if (XTAL.size() == 1 && XTAL.begin()->second.WAVE.size() == 1)
      XTAL.begin()->second.WAVE.begin()->second.convert_NAT_to_SAD();
    //PHASER_ASSERT(is_experiment_type("SAD"));
  }

  data_refl data_ep::get_data_refl()
  {
    data_refl REFLECTION;
    REFLECTION.MILLER = MILLER;
    REFLECTION.F = XTAL.begin()->second.WAVE.begin()->second.getFmean();
    REFLECTION.SIGF = XTAL.begin()->second.WAVE.begin()->second.getSIGFmean();
    REFLECTION.F_ID = XTAL.begin()->second.WAVE.begin()->second.POS.F_ID;
    REFLECTION.SIGF_ID = XTAL.begin()->second.WAVE.begin()->second.POS.SIGF_ID;
    return REFLECTION;
  }

  void data_ep::setNative(af_float fmean,af_float sigfmean,std::string RefID)
  {
    std::string xtalid = "xtal";
    std::string waveid = "data";
    init(xtalid,waveid);
    XTAL[xtalid].WAVE[waveid].RefID = RefID;
    XTAL[xtalid].WAVE[waveid].POS.F = fmean;
    XTAL[xtalid].WAVE[waveid].POS.SIGF = sigfmean;
    XTAL[xtalid].WAVE[waveid].POS.PRESENT = af_bool(fmean.size(),true);
    XTAL[xtalid].WAVE[waveid].initMean(); //after
  }

} //phaser
