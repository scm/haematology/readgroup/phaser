//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/data_atom.h>
#include <phaser/io/hybrid_36_c.h>

namespace phaser {

  std::string data_atom::unparse(std::string xtalid,bool logfile,int tab)
  {
    if (SCAT.label == "px") return ""; //partial structure atom, special case
    std::string Card = tab ? std::string(DEF_TAB*tab,' ') : "";
    Card += REJECTED ? "#" : "";
    Card += "ATOM CRYSTAL " + xtalid;
    Card += " TYPE " + SCAT.scattering_type + std::string(SCAT.scattering_type.size()==1?" ":"");
    Card += FRAC ? " FRAC" : " ORTH";
    for (int i = 0; i < 3; i++)
      Card += " " + std::string(SCAT.site[i]<0?"":" ") + dtos(SCAT.site[i],4);
    Card += " OCC " + dtos(SCAT.occupancy,4,2);
    if (!SCAT.flags.use_u_aniso_only())
      Card += " ISOB " + dtos(cctbx::adptbx::u_as_b(SCAT.u_iso),2);
    else if (!logfile)
    {
      Card += USTAR ? " USTAR" : " ANOU";
      double pdbf = USTAR ? 1 : u_cart_pdb_factor;
      for (int n = 0; n < 6; n++) Card += " " + dtos(pdbf*SCAT.u_star[n],10,USTAR?8:0);
    }
    if (!logfile)
    { //flag for logfile output also
      Card += " FIXX " + std::string(FIXX ? "ON":"OFF");
      Card += " FIXO " + std::string(FIXO ? "ON":"OFF");
      Card += " FIXB " + std::string(FIXB ? "ON":"OFF");
      Card += " BSWAP " + std::string(BSWAP ? "ON":"OFF");
    }
    if (SCAT.label.size())
      Card += " LABEL " + SCAT.label;
    return Card;
  }

  void data_atom::Card(FILE* outFile,int AtmNum,cctbx::uctbx::unit_cell UC)
  {
    if (SCAT.label == "px") return; //special case
    if (REJECTED) return; //special case
    char hybrid36_AtmNum[6]; //leave room for \0
    hy36encode(5,AtmNum,hybrid36_AtmNum);
    char hybrid36_ResNum[5]; //leave room for \0
    hy36encode(4,AtmNum,hybrid36_ResNum);
    dvect3 frac = SCAT.site;
    dvect3 orth(UC.orthogonalization_matrix()*frac);
    if (orth[0] > 9999.999 || orth[1] > 9999.999 || orth[2] > 9999.999 ||
        orth[0] < -999.999 || orth[1] < -999.999 || orth[2] < -999.999)
    { //try to put into unit cell
      for (int i = 0; i < 3; i++) { while (frac[i] < 0) frac[i]++; while (frac[i] >= 1) frac[i]--; }
      orth = UC.orthogonalization_matrix()*frac;//reset
    }
    dmat6 u_star = SCAT.u_star;
    if (!USTAR)
      u_star = cctbx::adptbx::u_cart_as_u_star(UC,SCAT.u_star);
    floatType atom_iso_u = SCAT.flags.use_u_aniso_only() ?
    cctbx::adptbx::u_star_as_u_iso(UC,u_star) : SCAT.u_iso;
    char c_str[DEF_WIDTH] = "";
    sprintf(c_str,
            "ATOM  %5s%3s   SUB A%4s    %8.3f%8.3f%8.3f%6.2f%6.2f          %2s\n",
              hybrid36_AtmNum,
              SCAT.scattering_type.c_str(),
              hybrid36_ResNum,
              orth[0],orth[1],orth[2],
              SCAT.occupancy,
              cctbx::adptbx::u_as_b(atom_iso_u),
              SCAT.scattering_type.c_str());
    std::string str;
    if (SCAT.flags.use_u_aniso_only())
    {
      char c_aniso[DEF_WIDTH] = "";
      //pdb output must be cartesian*10000
      dmat6 anisou = USTAR ? cctbx::adptbx::u_star_as_u_cart(UC,SCAT.u_star) : SCAT.u_star;
      anisou *= u_cart_pdb_factor;
      for (int i = 0; i < 6; i++) //write limit
        anisou[i] = std::min(anisou[i],9999999.);
      sprintf(c_aniso,
         "ANISOU%5s%3s   SUB A%4s  %7.0f%7.0f%7.0f%7.0f%7.0f%7.0f      %2s\n",
         hybrid36_AtmNum,
         SCAT.scattering_type.c_str(),
         hybrid36_ResNum,
         anisou[0],anisou[1],anisou[2],anisou[3],anisou[4],anisou[5],
         SCAT.scattering_type.c_str());
      char two_line_str[2*DEF_WIDTH] = "";
      strcpy(two_line_str,c_str);
      strcat(two_line_str,c_aniso);
      str = std::string(two_line_str);
    }
    else str = std::string(c_str);
    fprintf(outFile,"%s",str.c_str());
  }

} //phaser
