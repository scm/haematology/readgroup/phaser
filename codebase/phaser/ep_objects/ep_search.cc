//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/ep_search.h>
#include <phaser/lib/signal.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

ep_solution ep_search::select_and_log(
         std::string SG_HALL,
         af::double6 UNIT_CELL,
         double HIRES,
         double WILSON_B,
         af_atom STARTSET,
         data_find FINDSITES,
         data_peak FIND_PEAKS,
         VarianceEP VAR,
         Output& output)
{
  cctbx::uctbx::unit_cell cctbxUC = UnitCell(UNIT_CELL).getCctbxUC();
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup(SG_HALL).getCctbxSG();
  ep_solution final_atom_sets;
  bool1D selected(heights.size(),false);
  output.logUnderLine(LOGFILE,"Peak Selection");
  output.logTab(1,DEBUG,"Selection Top (Mean) " + dtos(top) +  " (" + dtos(mean) + ")");
  output.logTab(1,LOGFILE,FIND_PEAKS.logfile());

  if (FIND_PEAKS.by_sigma())
  {
    for (unsigned i = 0; i < heights.size(); i++)
      selected[i] = (signal(heights[i],mean,sigma) > FIND_PEAKS.get_cutoff());
  }
  else if (FIND_PEAKS.by_number())
  {
    std::set<int> topset;
    for (int top = 0; top < FIND_PEAKS.get_cutoff(); top++)
    {
      double pk(-std::numeric_limits<double>::max());
      int topi;
      for (unsigned i = 0; i < heights.size(); i++)
      if (pk < heights[i] && topset.find(i) == topset.end())
      {
        pk = heights[i];
        topi = i;
      }
      topset.insert(topi);
    }
    for (std::set<int>::iterator iter = topset.begin(); iter != topset.end(); iter++)
      selected[*iter] = true;
  }
  else if (FIND_PEAKS.by_percent())
  {
    for (unsigned i = 0; i < heights.size(); i++)
      selected[i] = (heights[i] > FIND_PEAKS.get_cutoff()*(top-mean)+mean);
  }
  else if (FIND_PEAKS.by_all())
  {
    for (unsigned i = 0; i < heights.size(); i++)
      selected[i] = true;
  }

  // Impose positivity on score, which is either an LLG or a log-likelihood-gradient
  for (unsigned i = 0; i < heights.size(); i++)
    selected[i] = (selected[i] && (heights[i] > 0.));

  if (std::find(selected.begin(),selected.end(),true) == selected.end())
  {
    output.logTab(1,LOGFILE,"No peaks selected");
    output.logBlank(LOGFILE);
    return final_atom_sets;
  }

  int sel(0);
  for (int i = 0; i < heights.size(); i++)
    if (selected[i]) sel++;
  output.logTab(1,LOGFILE,itos(sel) + " peaks were selected");
  output.logBlank(LOGFILE);

  bool1D cluster_top(heights.size(),true);
  int1D cluster_top_num(heights.size());
  float1D cluster_top_dist(heights.size());
  //optical resolution 0.715*resolution
  floatType max_bond_dist(10.0),optical_res(0.715*HIRES);
  floatType separation_dist = std::min(max_bond_dist,optical_res);
  if (FIND_PEAKS.CLUSTER)
  {
  //cluster the peaks - i.e. find the top peaks within separation_dist
  //all the flagged peaks are guaranteed to be separation_dist apart
  SpaceGroup sg(SG_HALL);
  for (unsigned j = 0; j < sites.size(); j++)
  {
    if (selected[j])
    {
      dvect3 site_j(sites[j]);
      dvect3 orth_j = cctbxUC.orthogonalization_matrix()*(site_j);
      floatType min_sqrtDelta = std::numeric_limits<floatType>::max();
      for (unsigned i = 0; i < j; i++) //check peaks higher up the list
      if (selected[i])
      {
        dvect3 site_i(sites[i]);
        for (unsigned isym = 0; isym < cctbxSG.order_z(); isym++)
        {
          dvect3 frac_i = sg.doSymXYZ(isym,site_i);
          dvect3 cellTrans;
          int shift(1);
          for (cellTrans[0] = -shift; cellTrans[0] <= shift; cellTrans[0]++)
          for (cellTrans[1] = -shift; cellTrans[1] <= shift; cellTrans[1]++)
          for (cellTrans[2] = -shift; cellTrans[2] <= shift; cellTrans[2]++)
          {
            dvect3 fracCell_i = frac_i + cellTrans;
            dvect3 orth_i = cctbxUC.orthogonalization_matrix()*(fracCell_i);
            floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
            if (sqrtDelta < min_sqrtDelta)
            {
              min_sqrtDelta = sqrtDelta; //find closest one higer up
              cluster_top_num[j] = i;
              cluster_top_dist[j] = sqrtDelta;
              if (sqrtDelta < separation_dist) cluster_top[j] = false;
            }
          }
        }
      }
    }
  }
  }

  bool1D old_site(heights.size(),false);
  if (STARTSET.size())
  {
  SpaceGroup sg(SG_HALL);
  for (unsigned j = 0; j < sites.size(); j++)
  {
    if (selected[j] && cluster_top[j])
    {
      dvect3& site_j(sites[j]);
      dvect3 orth_j = cctbxUC.orthogonalization_matrix()*(site_j);
      for (unsigned i = 0; i < STARTSET.size(); i++)
      {
        dvect3 site_i(STARTSET[i].SCAT.site);
        for (unsigned isym = 0; isym < cctbxSG.order_z(); isym++)
        {
          dvect3 frac_i = sg.doSymXYZ(isym,site_i);
          dvect3 cellTrans;
          int shift(1);
          for (cellTrans[0] = -shift; cellTrans[0] <= shift; cellTrans[0]++)
          for (cellTrans[1] = -shift; cellTrans[1] <= shift; cellTrans[1]++)
          for (cellTrans[2] = -shift; cellTrans[2] <= shift; cellTrans[2]++)
          {
            dvect3 fracCell_i = frac_i + cellTrans;
            dvect3 orth_i = cctbxUC.orthogonalization_matrix()*(fracCell_i);
            floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
            if (sqrtDelta < separation_dist) old_site[j] = true;
          }
        }
      }
    }
  }
  }

  bool1D accepted(heights.size());
  for (unsigned i = 0; i < heights.size(); i++)
    accepted[i] = (selected[i] && cluster_top[i] && !old_site[i]);

  {
    output.logUnderLine(LOGFILE,"Cluster Analysis");
    output.logTab(1,LOGFILE,"Cluster distance " + dtos(separation_dist));
    output.logTabPrintf(1,LOGFILE,"%4s %4s %4s %-7s  %6s %6s %6s  %10s %4s %8s %7s\n",
         "#out","=#out","#in","Z-score","X","Y","Z","Top","New","Distance","Nearest");
    int jj(1);
    for (unsigned j = 0; j < sites.size(); j++)
    if (selected[j])
    {
      output.logTabPrintf(1,LOGFILE,"%4s %4s %4s %7.1f   %6.3f %6.3f %6.3f  %10s %4s %8s %7s\n",
        accepted[j] ? itos(jj).c_str() : "---",
        std::string(!accepted[j] ? itos(cluster_top_num[j]+1) : "---").c_str(),
        itos(j+1).c_str(),
        signal(heights[j],mean,sigma),sites[j][0],sites[j][1],sites[j][2],
        cluster_top[j] ? " Y " : " N ",
        old_site[j] ? " N " : " Y ",
        std::string((!accepted[j]) ? dtos(cluster_top_dist[j],7,3)  : "---").c_str(),
        std::string((accepted[j]) ? dtos(cluster_top_dist[j],7,3)  : "---").c_str());
      if (accepted[j]) jj++;
    }
    output.logBlank(LOGFILE);
  }

  output.logUnderLine(LOGFILE,"Substructure Atoms (" + FINDSITES.TYPE + ")");

  if (std::find(accepted.begin(),accepted.end(),true) == accepted.end())
    output.logTab(1,LOGFILE,"No new atoms identified");
  else
  {
    output.logTabPrintf(1,LOGFILE,"%-3s %7s %7s %7s %9s %5s\n","#","X","Y","Z","Fast-LLG","Z-scr");
    for (unsigned i = 0; i < heights.size(); i++)
    if (accepted[i])
    {
      output.logTabPrintf(1,LOGFILE,"%-3d %7.4f %7.4f %7.4f %9.3f %5.1f\n",
                   i+1,sites[i][0],sites[i][1],sites[i][2],heights[i],
                   signal(heights[i],mean,sigma));
      af_atom next_sites = STARTSET;
      data_atom new_site;
      new_site.SCAT.site = sites[i];
      new_site.ZSCORE = signal(heights[i],mean,sigma);
      new_site.SCAT.occupancy = FINDSITES.OCCUPANCY;
      new_site.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
      new_site.SCAT.u_iso = cctbx::adptbx::b_as_u(WILSON_B);
      new_site.SCAT.scattering_type = FINDSITES.TYPE;
      next_sites.push_back(new_site);
      next_sites.FAST_LLG = heights[i];
      next_sites.LLG = 0;
      next_sites.VAR = VAR;
      final_atom_sets.push_back(next_sites);
    }
  }
  output.logBlank(LOGFILE);
  return final_atom_sets;
}

} //phaser
