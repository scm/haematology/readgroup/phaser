//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EP_SEARCH_Class__
#define __EP_SEARCH_Class__
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/ep_solution.h>
#include <phaser/include/data_peak.h>
#include <phaser/include/data_find.h>
#include <phaser/io/Output.h>

namespace phaser {

class ep_search
{
  public:
  af::shared<double>  heights;
  af::shared<dvect3>  sites;
  double mean,sigma,top;

  ep_search(int s=0) { heights.resize(s); sites.resize(0); mean = sigma = top = 0; }

  ep_solution select_and_log(
        std::string,
        af::double6,
        double,
        double,
        af_atom,
        data_find,
        data_peak,
        VarianceEP,
        Output&);
};

} //phaser

#endif
