//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_ATOM_Class__
#define __DATA_ATOM_Class__
#include <cctbx/xray/scatterer.h>
#include <phaser/lib/jiffy.h>
#include <phaser_defaults.h>
#include <cctbx/uctbx.h>

namespace phaser {

class data_atom
{
  public:
    data_atom() { ZSCORE = 0; set_input_defaults(); set_extra_defaults(); }

  xray::scatterer<double> SCAT;
  bool  ORIG,FRAC,FIXX,FIXO,FIXB,BSWAP,USTAR;
  bool  REJECTED; //atom has been rejected for failing some test
  bool  RESTORED; //previously rejected atom resurrected permanently
  unsigned n_adp,n_xyz;
  double   ZSCORE;

  bool isFixAll() { return (FIXX && FIXO && FIXB); }

  void copy_input(data_atom& init)
  {
    ORIG = init.ORIG;
    FRAC = init.FRAC;
    FIXX = init.FIXX;
    FIXO = init.FIXO;
    FIXB = init.FIXB;
    BSWAP = init.BSWAP;
    USTAR = init.USTAR;
  }

  void copy_extra(data_atom& init)
  {
    REJECTED = init.REJECTED;
    RESTORED = init.RESTORED;
    n_adp = init.n_adp;
    n_xyz = init.n_xyz;
  }

  void set_input_defaults()
  { FIXX = FIXO = FIXB = BSWAP = USTAR = false; FRAC = true; ORIG = false; }

  void set_extra_defaults()
  { REJECTED = RESTORED = false; n_adp = n_xyz = 0; }

  public:
  std::string unparse(std::string xtalid,bool logfile=false,int tab=0);
  void Card(FILE*,int,cctbx::uctbx::unit_cell);

  bool operator<(const data_atom &right) const
  {
    if (SCAT.site[0] == right.SCAT.site[0]  &&
        SCAT.site[1] == right.SCAT.site[1])
      return SCAT.site[2] < right.SCAT.site[2];
    if (SCAT.site[0] == right.SCAT.site[0])
      return SCAT.site[1] < right.SCAT.site[1];
    return SCAT.site[0] < right.SCAT.site[0];
  }

};

} //phaser

#endif
