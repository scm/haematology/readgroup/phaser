//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_WAVE_Class__
#define __DATA_WAVE_Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_spots.h>

namespace phaser {

class data_wave
{
  public:
    data_wave(std::string xtalid="",std::string waveid="") : XTALID(xtalid), WAVEID(waveid)
      { POS.clear(); NEG.clear(); WAVELENGTH = 0; ANOMALOUS = false; RefID = ""; }

  public:
    data_spots  POS,NEG;
    floatType   WAVELENGTH;
    bool        ANOMALOUS;
    std::string XTALID,WAVEID,RefID;

  public:

  bool        isAnom()        { return ANOMALOUS; }
  bool        isMean()        { return !ANOMALOUS; }
  xtalwave    getXtalWave()   { return std::pair<std::string,std::string>(XTALID,WAVEID); }
  bool        inputF()        { return !POS.INPUT_INTENSITIES; } //pos only
  bool        inputI()        { return POS.INPUT_INTENSITIES; } //pos only

  void initAnom();
  void initMean();
  int setup_error();
  af_float getFmean();
  af_float getFpos();
  af_float getFneg();
  af_float getSIGFmean(af_bool cent = af_bool());
  af_float getSIGFpos();
  af_float getSIGFneg();
  af_bool getPmean();
  af_bool getPpos();
  af_bool getPneg();
  std::string unparse();
  void erase(int);
  void push_back_f(floatType,floatType,bool);
  void push_back_fneg(floatType,floatType,bool);
  void convert_NAT_to_SAD();
};

typedef std::map<std::string,data_wave>::iterator waveIter;

} //phaser

#endif
