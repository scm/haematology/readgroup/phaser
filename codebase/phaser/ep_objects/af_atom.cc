//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/ep_objects/af_atom.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

  std::string af_atom::unparse(std::string xtalid,bool logfile,int tab)
  {
    std::string Card;
    for (int a = 0; a < SET.size(); a++)
      Card += SET[a].unparse(xtalid,logfile,tab) + "\n";
    return Card;
  }

  void af_atom::writePdb(FILE* outFile,cctbx::uctbx::unit_cell UC)
  {
    for (int a = 0; a < SET.size(); a++)
      SET[a].Card(outFile,a+1,UC);
  }

  bool af_atom::no_scattering()
  {
    for (int a = 0; a < SET.size(); a++)
      if (SET[a].SCAT.occupancy) return false;
    return true;
  }

  af::shared<xray::scatterer<double> > af_atom::get_scatterers()
  {
    af::shared<xray::scatterer<double> > scatterers;
    for (unsigned a = 0; a < SET.size(); a++)
     // if (!atoms[a].REJECTED) //rejected have zero occupancy, include all
        scatterers.push_back(SET[a].SCAT);
    return scatterers;
  }

  af_atom af_atom::other_hand(std::string hall,af::double6 cell)
  {
    af_atom HAND = *this;
    SpaceGroup SG(hall);
    UnitCell UC(cell);
    cctbx::sgtbx::space_group sg(SG.getCctbxSG());
    cctbx::sgtbx::space_group_type sg_type(sg);
    cctbx::sgtbx::change_of_basis_op cb_op = sg_type.change_of_hand_op();
    for (int a = 0; a < SET.size(); a++)
    {
      dvect3 frac = SET[a].SCAT.site;
      cctbx::fractional<double> sg_x(frac);
      dvect3 frac_hand = cb_op(sg_x);
      for (unsigned j = 0; j < 3; j++)
      {
        while (frac_hand[j] <= -1.0) { frac_hand[j] += 1; }
        while (frac_hand[j] >   0.0) { frac_hand[j] -= 1; }
      }
      HAND[a].SCAT.site = frac_hand;
    }
    return HAND;
  }

} //phaser
