//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/CCP4base.h>
#include <phaser/io/Preprocessor.h>
#include <phaser/io/Output.h>
#include <phaser/io/END.h>

namespace phaser {

void CCP4base::Add_Key(std::string card)
{ possible_keys.push_back(card); }

Token_value CCP4base::skip_line(std::istringstream& input_stream)
{
  //This routine returns either END or ENDLINE after skipping to the end
  //of a line, warning of any unexpected tokens in the process.
  size_t keywords_size = keywords.size();
  if (curr_tok == ENDLINE || curr_tok == END)
    return curr_tok;
  get_token(input_stream);
  if (curr_tok == ENDLINE || curr_tok == END)
    return curr_tok;
  else
  {
    do { get_token(input_stream); }
    while (curr_tok != ENDLINE && curr_tok != END);
    size_t keywords_size2 = keywords.size();
    std::string remainder = keywords.substr(keywords_size,keywords_size2-keywords_size);
    if (annotate_keywords)
      keywords += "\tWarning - Additional tokens ignored: " + remainder;
  }
  return curr_tok;
}

Token_value CCP4base::get_token(std::istringstream& input_stream)
{
  std::string tmp;
  bool test_number = true;
  char ch;
  do { if (!input_stream.get(ch))
       {
         keywords += '\n';
         return curr_tok = END;
       }
       else
         keywords += ch;
     } while ((std::isspace)(ch) && ch != '\n'); //skip space, tabs, feedforms


  //if line continuation, skip all spaces including \n
  if (ch == '&')
    do { if (!input_stream.get(ch))
         {
           keywords += '\n';
           return curr_tok = END;
         }
         else
           keywords += ch;
       } while ((std::isspace)(ch));

  switch (ch)
  {
    case '#': case '!':
      std::getline(input_stream,string_value,'\n');
      keywords += string_value;
      keywords += '\n';
      return curr_tok=ENDLINE;
      break;

    case '\n':
      return curr_tok=ENDLINE;
      break;

    case '=':
      return curr_tok=ASSIGN;
      break;

    case '+': case '-': case '.':
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      string_value = ch;
      // Obsolete: allow digits and dots only for subsequent chars with
      // while(input_stream.get(ch) && (isdigit(ch) || ch=='.')) here
      while(input_stream.get(ch) && (std::isprint)(ch) && !(std::isspace)(ch) && ch != '=' && ch !='#')
      {
//rest of string is all digits or . or e or E or + or -(exponent)
        if (!((std::isdigit)(ch) || ch=='.' || ch=='e' || ch=='E' || ch=='+'|| ch=='-'))
          test_number = false;
        string_value += ch;
      }
      input_stream.putback(ch); // oops - read one too far
      tmp = string_value;
      keywords += tmp.erase(0,1);// take off already added ch from front of keywords string
      if (test_number)
      {
        number_value = atof(string_value.c_str());
        return curr_tok = NUMBER; //at end, string_value is the number too
      }
      else //this allows std::strings starting with any character except = or #
        return curr_tok = NAME;
      break;

    default:
     // if ((std::isprint)(ch)) //unicode! "ch" does not have to be printable
      {
        string_value = ch;
        //allow printable characters except spaces and ASSIGN
        while(input_stream.get(ch) &&
              //(std::isprint)(ch) && //unicode! "ch" does not have to be printable
               !(std::isspace)(ch) && ch != '=')
          string_value += ch;
        input_stream.putback(ch); // oops - read one too far
        tmp = string_value;
        keywords += tmp.erase(0,1);// take off already added ch from front of keywords string
        return curr_tok = NAME;
      }
    }

    throw PhaserError(SYNTAX,keywords,"Unrecognised (unicode) character");
    return curr_tok = ENDLINE;
}

Token_value CCP4base::get_key(std::istringstream& input_stream)
{
  get_token(input_stream);

  switch (curr_tok)
  {
    case NAME:
    {
      std::vector<std::string> end_keys = END_KEYS();
      for (int i = 0; i < end_keys.size(); i++)
        if (!stoup(string_value).find(end_keys[i]))
          return curr_tok = END;
      for (int i = 0; i < possible_keys.size(); i++)
        if (keyIs(possible_keys[i]))
          return curr_tok = possible_fns[i]->parse(input_stream);
    }
    break;

    case ENDLINE:
      return ENDLINE;
      break;

    case END:
      return END;
      break;

    default:
      ;
  }
  //Keyword not recognised
  std::string key = stoup(string_value);
  //This routine returns either END or ENDLINE after skipping to the end
  //of a line
  do { get_token(input_stream); }
  while (curr_tok != ENDLINE && curr_tok != END);
 // if (annotate_keywords && key.find('@')) //i.e. ignore preprocessor
  if (annotate_keywords)
  {
    size_t erase_end = keywords.rfind("\n"); //needs "\n" flag at start for first unused keyword to be deleted
    size_t erase_start =  keywords.rfind("\n",erase_end-1);
    if (erase_end != std::string::npos && erase_start != std::string::npos)
    {
      size_t erase_length = erase_end-erase_start;
      keywords.erase(erase_start,erase_length);
   // keywords += "\tWarning - Keyword " + key + " not relevant\n";
    }
  }
  return curr_tok;
}

bool CCP4base::getBoolean(std::istringstream& input_stream)
{
  if (!tokenIs(input_stream,2,NAME,NUMBER))
  throw PhaserError(SYNTAX,keywords,"Boolean not present or not valid");
  std::string raw = stoup(string_value);
  if (raw == "TRUE") raw = "ON";
  if (raw == "YES") raw = "ON";
  if (raw == "1") raw = "ON";
  if (raw == "FALSE") raw = "OFF";
  if (raw == "NO") raw = "OFF";
  if (raw == "0") raw = "OFF";
  std::string tmp = string_value; //store original class member value
  string_value = raw; //for the compulsoryKey test
  compulsoryKey(2,"ON","OFF"); //so that error is thrown
  bool Boolean(false);
  if (keyIs("ON")) Boolean = true;
  if (keyIs("OFF")) Boolean = false;
  string_value = tmp; //restore
  return Boolean;
}

std::string CCP4base::getFileName(std::istringstream& input_stream)
{
 //allows =,#,! in filenames
  //skip leading spaces (there must be leading spaces)
  bool quoted_string=false;
  for (;;)
  {
    char ch;
    if (input_stream.get(ch))
    {
      if (ch == '\n')
      {
        input_stream.putback(ch); //so that ENDLINE token is found
        return "";
      }
      else if ((std::isspace)(ch))
      {
        keywords += ch; //accept and ignore space
      }
      else if (ch == '\"')
      {
        keywords += ch;
        quoted_string=true;
        break;
      }
      else if (ch == '&') //ampersand
      {
        keywords += ch;
        do { if (!input_stream.get(ch))
             {
               keywords += '\n';
               return "";
             }
             else
               keywords += ch;
           } while ((std::isspace)(ch) || ch == '\n'); //skip over spaces until end of line
        input_stream.putback(ch); //found non-space non-return
      }
      else //it is an interesting char and needs to be read again
      {
        input_stream.putback(ch);
        break;
      }
    }
    else
    {
      input_stream.putback(ch); //so that END can be found
      return "";
    }
  }
  std::string filename;
  for (;;)
  {
    char ch;
    if (input_stream.get(ch))
    {
      if ((std::isspace)(ch) && !quoted_string)
      {
        input_stream.putback(ch); //so that ENDLINE token is found
        return filename;
      }
      else if (ch == '\"' && quoted_string)
      {
        keywords += ch;
        return filename;
      }
      else
      {
        keywords += ch;
        filename += ch;
      }
    }
    else
    {
      input_stream.putback(ch); //so that END can be found
      return filename;
    }
  }
}

std::string CCP4base::getLine(std::istringstream& input_stream)
{
  std::string line;
  std::getline(input_stream,line,'\n');
  keywords += line + '\n'; //have to add this explicitly as it is not going through the tokenizer
  return line;
}

std::string CCP4base::getAssignString(std::istringstream& input_stream)
{
  get_token(input_stream);
  if (tokenIs(1,ASSIGN)) get_token(input_stream); //assignment token is optional
  if (!tokenIs(2,NAME,NUMBER))
  throw PhaserError(SYNTAX,keywords,"Assignment string not present or not valid");
  return string_value;
}

floatType CCP4base::getAssignNumber(std::istringstream& input_stream)
{
  get_token(input_stream);
  if (tokenIs(1,ASSIGN)) get_token(input_stream); //assignment token is optional
  if (!tokenIs(1,NUMBER))
  throw PhaserError(SYNTAX,keywords,"Assignment number not present or not valid");
  return number_value;
}

std::string CCP4base::getString(std::istringstream& input_stream)
{
  if (!tokenIs(input_stream,2,NAME,NUMBER))
  throw PhaserError(SYNTAX,keywords,"Character string not present or not valid");
  return string_value;
}

floatType CCP4base::get1num(std::istringstream& input_stream)
{
  if (!tokenIs(input_stream,1,NUMBER))
  throw PhaserError(SYNTAX,keywords,"Number not present or not valid");
  return number_value;
}

dvect3 CCP4base::get3nums(std::istringstream& input_stream)
{
  dvect3 temp;
  temp[0] = get1num(input_stream);
  temp[1] = get1num(input_stream);
  temp[2] = get1num(input_stream);
  return temp;
}

dmat6 CCP4base::get6nums(std::istringstream& input_stream)
{
  dmat6 temp;
  temp[0] = get1num(input_stream);
  temp[1] = get1num(input_stream);
  temp[2] = get1num(input_stream);
  temp[3] = get1num(input_stream);
  temp[4] = get1num(input_stream);
  temp[5] = get1num(input_stream);
  return temp;
}

dmat33 CCP4base::get9nums(std::istringstream& input_stream)
{
  dmat33 temp;
  temp(0,0) = get1num(input_stream);
  temp(0,1) = get1num(input_stream);
  temp(0,2) = get1num(input_stream);
  temp(1,0) = get1num(input_stream);
  temp(1,1) = get1num(input_stream);
  temp(1,2) = get1num(input_stream);
  temp(2,0) = get1num(input_stream);
  temp(2,1) = get1num(input_stream);
  temp(2,2) = get1num(input_stream);
  return temp;
}

bool CCP4base::keyIs(std::string key)
{
  int len(std::min(key_len,key.size()));
  std::string upper_string_value(stoup(string_value));
  if (upper_string_value.size() >= len)
  {
    for (int i = 0; i < len; i++)
      if (upper_string_value[i] != key[i])
        return false;
    return true;
  }
  return false;
}

bool CCP4base::tokenIs(std::istringstream& input_stream,int va_len, ...)
{
  get_token(input_stream);
  bool tokenFound(false);
  va_list tokens;
  va_start(tokens,va_len);
  //va_arg return type Token_value, but the compiler wants this promoted to int
  for (int i = 0; i < va_len; i++)
    if (curr_tok == va_arg(tokens,int))
      tokenFound = true;
  va_end(tokens);
  return tokenFound;
}

bool CCP4base::tokenIs(int va_len, ...)
{
  bool tokenFound(false);
  va_list tokens;
  va_start(tokens,va_len);
  //va_arg return type Token_value, but the compiler wants this promoted to int
  for (int i = 0; i < va_len; i++)
    if (curr_tok == va_arg(tokens,int))
      tokenFound = true;
  va_end(tokens);
  return tokenFound;
}

bool CCP4base::compulsoryKey(int va_len, ...)
{
  bool keyFound(false);
  va_list keys;
  va_start(keys,va_len);
  if (tokenIs(2,NAME,NUMBER)) //boolean 0,1 allowed
  {
//va_arg will not accept std::string
    for (int i = 0; i < va_len; i++)
      if (keyIs(std::string(va_arg(keys,char*))))
        keyFound = true;
  }
  else
    keyFound = false;
  va_end(keys);

  if (!(keyFound))
  {
    va_start(keys,va_len);
    std::string useMessage = "Use ";
    if (va_len > 1)
    {
      for (int i = 0; i < va_len-1; i++)
        useMessage += std::string(va_arg(keys,char*)) + " ";
      useMessage += "or " + std::string(va_arg(keys,char*));
    }
    else
      useMessage += std::string(va_arg(keys,char*));

    va_end(keys);
    throw PhaserError(SYNTAX,keywords,useMessage);
  }
  return keyFound;
}

bool CCP4base::compulsoryKey(std::istringstream& input_stream,int va_len, ...)
{
  get_token(input_stream);
  bool keyFound(false);
  va_list keys;
  va_start(keys,va_len);
  if (tokenIs(1,NAME))
  {
//va_arg will not accept std::string
    for (int i = 0; i < va_len; i++)
      if (keyIs(std::string(va_arg(keys,char*))))
        keyFound = true;
  }
  else
    keyFound = false;
  va_end(keys);

  if (!(keyFound))
  {
    va_start(keys,va_len);
    std::string useMessage = "Use ";
    if (va_len > 1)
    {
      for (int i = 0; i < va_len-1; i++)
        useMessage += std::string(va_arg(keys,char*)) + " ";
      useMessage += "or " + std::string(va_arg(keys,char*));
    }
    else
      useMessage += std::string(va_arg(keys,char*));

    va_end(keys);
    throw PhaserError(SYNTAX,keywords,useMessage);
  }
  return keyFound;
}

bool CCP4base::optionalKey(std::istringstream& input_stream,int va_len, ...)
{
  get_token(input_stream);
  bool keyFound(false);
  va_list keys;
  va_start(keys,va_len);
  if (tokenIs(1,NAME))
  {
//va_arg will not accept std::string
    for (int i = 0; i < va_len; i++)
      if (keyIs(std::string(va_arg(keys,char*))))
        keyFound = true;
  }
  va_end(keys);

  //we expect ENDLINE or END if none of the keywords are found
  if ((tokenIs(1,NAME) && !keyFound) || !tokenIs(3,NAME,ENDLINE,END))
  {
    va_start(keys,va_len);
    std::string useMessage = "Use (optionally) ";
    if (va_len > 1)
    {
      for (int i = 0; i < va_len - 1; i++)
        useMessage += std::string(va_arg(keys,char*)) + " ";
      useMessage += "or " + std::string(va_arg(keys,char*));
    }
    else
      useMessage += std::string(va_arg(keys,char*));

    va_end(keys);
    throw PhaserError(SYNTAX,keywords,useMessage);
  }
  return keyFound;
}

bool CCP4base::optionalKey(int va_len, ...)
{
  bool keyFound(false);
  va_list keys;
  va_start(keys,va_len);
  if (tokenIs(1,NAME))
  {
    for (int i = 0; i < va_len; i++)
    {
//va_arg will not accept std::string
      std::string lookup = std::string(va_arg(keys,char*));
      if (keyIs(lookup)) keyFound = true;
    }
  }
  else
    keyFound = false;
  va_end(keys);

  //we expect ENDLINE or END if none of the keywords are found
  if ((tokenIs(1,NAME) && !keyFound) || !tokenIs(3,NAME,ENDLINE,END))
  {
    va_start(keys,va_len);
    std::string useMessage = "Use (optionally) ";
    if (va_len > 1)
    {
      for (int i = 0; i < va_len - 1; i++)
        useMessage += std::string(va_arg(keys,char*)) + " ";
      useMessage += "or " + std::string(va_arg(keys,char*));
    }
    else
      useMessage += std::string(va_arg(keys,char*));

    va_end(keys);
    throw PhaserError(SYNTAX,keywords,useMessage);
  }

  return keyFound;
}

void CCP4base::parseCCP4(std::string cards)
{
  std::istringstream input_stream(const_cast<char*>(cards.c_str()));

  // Private initial values
  curr_tok = ENDLINE;

  // Do the parsing
  input_stream.seekg(0);
  PHASER_ASSERT(possible_keys.size() == possible_fns.size());
  while (input_stream)
  {
    get_key(input_stream);
    if (curr_tok == END) break;
    if (curr_tok == ENDLINE) continue;
  }
}

floatType CCP4base::ispos(floatType x)
{
  if (x <= 0)
  store(PhaserError(INPUT,keywords,"Number " + dtos(x) + " is not positive"));
  return x;
}

unsigned CCP4base::isposi(floatType x)
{
  floatType round = floor(x + 0.5);
  if (x <= 0 || x != round)
  store(PhaserError(INPUT,keywords,"Number " + dtos(x) + " is not a positive integer"));
  return static_cast<unsigned>(round);
}

floatType CCP4base::iszeropos(floatType x)
{
  if (x < 0)
  store(PhaserError(INPUT,keywords,"Number " + dtos(x) + " is not zero or positive"));
  return x;
}

unsigned CCP4base::iszeroposi(floatType x)
{
  floatType round = floor(x + 0.5);
  if (x < 0 || x != round)
  store(PhaserError(INPUT,keywords,"Number " + dtos(x) + " is not zero or a positive integer"));
  return static_cast<unsigned>(round);
}

floatType CCP4base::isperc(floatType percent)
{
  std::string errormsg("Value " + dtos(percent) + " is not a percentage in range 0%-100%");
  if (percent < 0) store(PhaserError(INPUT,keywords,errormsg));
  if (percent > 1) percent /= 100;
  if (percent > 1 && percent < 1.01) percent = 1;
  if (percent > 1) store(PhaserError(INPUT,keywords,errormsg));
  return percent; //which is actually a fraction
}

std::string CCP4base::Keywords()
{ return keywords; }

//if there is currently no error, store it
void CCP4base::store(PhaserError err)
{
  //cannot specify operator== with base class std::exception
  bool newerr(true);
  for (int e = 0; e < inputError.size(); e++)
    if (inputError[e].ErrorMessage() == err.ErrorMessage() && inputError[e].ErrorType() == err.ErrorType())
      newerr=false;
  if (newerr) inputError.push_back(err);
}

void CCP4base::throw_errors()
{ //throw first failure error
  for (int e = 0; e < inputError.size(); e++)
    if (inputError[e].Failure()) throw inputError[e];
}

void CCP4base::throw_advisory(Output& output)
{
  for (int e = 0; e < inputError.size(); e++)
    if (inputError[e].Success())
      output.logAdvisory(SUMMARY,inputError[e].ErrorMessage());
}

}//phaser
