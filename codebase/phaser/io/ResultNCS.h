//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultNCS__Class__
#define __ResultNCS__Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_tncs.h>
#include <phaser/include/data_moments.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>
#include <phaser/pod/epspod.h>

namespace phaser {

class ResultNCS : public Output, public DataA
{
  public:
    ResultNCS();
    ResultNCS(Output&);
    virtual ~ResultNCS() throw() {}
    data_tncs PTNCS;
    data_moments MOMENTS;
    double    ptwin5percent;
    epspod    PTNCS_NMOL;

  private:
    std::string MtzFile,SolFile;

  public:
    std::string  getMtzFile() { return MtzFile; }
    std::string  getSolFile() { return SolFile; }

    af_string getFilenames();
    void writeMtz(std::string="");
    void setPtNcs(data_tncs& t) { PTNCS = t; }
    data_tncs getPtNcs() { return PTNCS; }

    void      setMoments(data_moments t) { MOMENTS = t; }
    floatType getCentricE2() { return MOMENTS.meanE2_cent; }
    floatType getCentricE4() { return MOMENTS.meanE4_cent; }
    floatType getACentricE2() { return MOMENTS.meanE2_acent; }
    floatType getACentricE4() { return MOMENTS.meanE4_acent; }
    floatType getTwinAlpha() { return MOMENTS.alpha(); }

    bool     hasTNCS() { return PTNCS.TRA.VECTOR_SET; } //legacy
    int      getNMOL() { return PTNCS.NMOL; }
    dvect3   getVector() { return PTNCS.TRA.VECTOR; }
    dvect3   getAngles() { return PTNCS.ROT.ANGLE; }
    af_float getEpsFac();

    bool     has_tncs() { return PTNCS.ANALYSIS.size(); } //has been detected
    bool     use_tncs() { return PTNCS.USE; } //has been used
    int      nmol() { return PTNCS.NMOL; }
    int      maxnmol() { return PTNCS.MAXNMOL; }
    dvect3   translation_vector() { return PTNCS.TRA.VECTOR; }
    dvect3   rotation_angles() { return PTNCS.ROT.ANGLE; }
    int      analysis_number() { return PTNCS.ANALYSIS.size(); } //number of alternatives
    dvect3   analysis_vector(int i) { PHASER_ASSERT(i < PTNCS.ANALYSIS.size()); return PTNCS.ANALYSIS[i].vector_frac; }
    int      analysis_nmol(int i) { PHASER_ASSERT(i < PTNCS.ANALYSIS.size()); return PTNCS.ANALYSIS[i].NMOL; }
    bool     commensurate(int i) { PHASER_ASSERT(i < PTNCS.ANALYSIS.size()); return PTNCS.ANALYSIS[i].commensurate(PTNCS.TRA.PATT_DIST,PTNCS.COMM); }
    bool     twinned() { return ptwin5percent < 0.01; }

    double   patterson_top_all() const { return PTNCS_NMOL.PattersonTopAll; }
    double   patterson_top_nonorigin() const { return PTNCS_NMOL.PattersonTopNonOrigin; }
    double   patterson_top_analysis() const { return PTNCS_NMOL.PattersonTopAnalysis; }
    double   patterson_top_largecell() const { return PTNCS_NMOL.PattersonTopLargeCell; }
    double   patterson_top_cutoff() const { return PTNCS_NMOL.PattersonTopCutoff; }
    ivect3   frequency_top_site() const { return PTNCS_NMOL.FrequencyTopSite; }
    double   frequency_top_peak() const { return PTNCS_NMOL.FrequencyTopPeak; }
    bool     warning_reflections() const { return PTNCS_NMOL.WarningReflections; }
    bool     warning_pathology() const { return PTNCS_NMOL.WarningPathology; }
};

}
#endif
