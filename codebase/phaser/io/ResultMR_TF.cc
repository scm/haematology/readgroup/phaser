//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultMR_TF.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/between.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/PdbHandler.h>
#include <phaser/io/ResultFile.h>
#include <phaser/io/Errors.h>
#include <phaser/src/SpaceGroup.h>
#include <limits>

namespace phaser {

ResultMR_TF::ResultMR_TF() : Output(),UnitCell()
{
  TITLE = SolFile = "";;
  NTOP = TopRfac = 0;
}

ResultMR_TF::ResultMR_TF(Output& output) : Output(output),UnitCell()
{
  TITLE = SolFile = "";;
  NTOP = TopRfac = 0;
}

void ResultMR_TF::init(std::string t,unsigned n,map_str_pdb& pdb)
{
  SolFile = "";
  TITLE = t;
  NTOP = n;
  set_pdb_chainids(pdb);
}

void ResultMR_TF::writeSol()
{
  SolFile = Output::Fileroot() + ".sol";
  ResultFile opened(SolFile);

  fprintf(opened.outFile,"# %s \n",TITLE.c_str());
  if (MRSET.size())
    fprintf(opened.outFile,"%s\n",MRSET.unparse().c_str());
  else
    fprintf(opened.outFile,"#NO TRANSLATION FUNCTION SOLUTIONS\n");
  fclose(opened.outFile);
}

void ResultMR_TF::writePdb(xyzout DO_XYZOUT,MapTrcPtr tracemol)
{
  for (unsigned t = 0; t < getNumTop(); t++)
  {
    int num_chainid(0);
    for (unsigned k = 0; k < MRSET[t].KNOWN.size(); k++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[k].MODLID) != ense_chainids.end()) //could be a map
      {
        std::string m = MRSET[t].KNOWN[k].MODLID;
        num_chainid += ense_chainids[m].size();
      }
    }

    pdb_chains_2chainid chains(num_chainid);

    std::string filename = Output::Fileroot() + "." + itos(t+1) + ".pdb";
    ResultFile opened(filename);

    PdbFiles.push_back(filename);

    fprintf(opened.outFile,"REMARK TITLE %s \n",TITLE.c_str());
    fprintf(opened.outFile,"REMARK Log-Likelihood Gain:  %8.3f\n",MRSET[t].TF);
    fprintf(opened.outFile,"REMARK %s\n",MRSET[t].ANNOTATION.c_str());
    for (unsigned k = 0; k < MRSET[t].KNOWN.size(); k++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[k].MODLID) != ense_chainids.end()) //could be a map
        fprintf(opened.outFile,"REMARK ENSEMBLE %s EULER %6.2f %6.2f %6.2f FRAC % 5.3f % 5.3f % 5.3f\n",
          MRSET[t].KNOWN[k].MODLID.c_str(),
          MRSET[t].KNOWN[k].getEulerAng(0),MRSET[t].KNOWN[k].getEulerAng(1),MRSET[t].KNOWN[k].getEulerAng(2),
          MRSET[t].KNOWN[k].getCoord(0),MRSET[t].KNOWN[k].getCoord(1),MRSET[t].KNOWN[k].getCoord(2));
    }
    bool Space_Group_of_Solution_Set(MRSET[t].HALL != "");
    PHASER_ASSERT(Space_Group_of_Solution_Set);
    std::string SGNAME = space_group_name(MRSET[t].HALL,true).CCP4;
    int Z = SpaceGroup(MRSET[t].HALL).getSpaceGroupNSYMM()*MRSET[t].Z();
    fprintf(opened.outFile,"%s",UnitCell::CrystCard(SGNAME,Z).c_str());
    int atom_number(1);
    for (unsigned k = 0; k < MRSET[t].KNOWN.size(); k++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[k].MODLID) != ense_chainids.end()) //could be a map
      {
        std::string m = MRSET[t].KNOWN[k].MODLID;
        //the mapping of the input ensemble chain to the output chain
        map_str_str map_chains;
        for (int i = 0; i < ense_chainids[m].size(); i++)
        {
          if (DO_XYZOUT.CHAIN_COPY)
          {
            map_chains[ense_chainids[m][i]] = ense_chainids[m][i];
          }
          else
          {
            map_chains[ense_chainids[m][i]] = chains.allocate(ense_chainids[m][i]); //map the input to the next allowed output
            if (chains.allocate_failed())
              logWarning(SUMMARY,chains.message());
          }
        }
        std::string hall = MRSET[t].HALL;
        af::double6 unit_cell = getCell6();
        bool renumber_atoms(MRSET[t].KNOWN.size() > 1);
        bool delete_overlaps(true);
        fprintf(opened.outFile,"%s",
          pdb_string_maker(PDB,map_chains,MRSET[t].KNOWN[k],renumber_atoms,atom_number,hall,unit_cell,delete_overlaps,relative_wilsonB[m],tracemol).c_str());
      }
    }
    fprintf(opened.outFile,"END\n");
    fclose(opened.outFile);
  }
}

af_string ResultMR_TF::getFilenames()
{
  af_string filenames;
  filenames.insert(filenames.end(),PdbFiles.begin(),PdbFiles.end());
  if (SolFile.size()) filenames.push_back(SolFile);
  return filenames;
}

void ResultMR_TF::setKnownOrigin(mr_set mrset,std::string HALL,floatType value)
{
  mrset.ANNOTATION += " TF*0";
  mrset.HALL = HALL;
  mrset.TF = value;
  mrset.TFZ = 0;
  mrset.NROT = 0;
  //mrset.KNOWN.clear(); don't clear!!!! need backgroud, even if it overlaps
  int e(0);
  mr_ndim new_known(mrset.RLIST[e].MODLID,mrset.RLIST[e].EULER);
  mrset.KNOWN.push_back(new_known);
  mrset.RLIST.clear();
  MRSET.clear();
  MRSET.push_back(mrset);
}

} //phaser
