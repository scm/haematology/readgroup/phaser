//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputEP_AUTO__Class__
#define __InputEP_AUTO__Class__
#include <phaser/keywords/keywords.h>
#include <phaser/lib/scattering.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputEP_AUTO :
                  public ATOM
                 ,public BFAC
                 ,public BINS
                 ,public CELL
                 ,public CLUS
                 ,public COMP
                 ,public CRYS
                 ,public FFTS
                 ,public HAND
                 ,public HKLI
                 ,public HKLO
                 ,public JOBS
                 ,public KEYW
                 ,public LLGC
                 ,public LLGM
                 ,public MACA
                 ,public MACJ
                 ,public MACS
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public PART
                 ,public PEAK
                 ,public RESH
                 ,public RESO
                 ,public RFAC
                 ,public ROOT
                 ,public SAMP
                 ,public SCAT
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public VARS
                 ,public VERB
                 ,public WAVE
                 ,public XYZO
                 ,public KILL
{
  public:
    InputEP_AUTO(std::string);
    InputEP_AUTO();
    ~InputEP_AUTO();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
