//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultGYRE__Class__
#define __ResultGYRE__Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/mr_objects/data_map.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>
#include <phaser/io/ResultCore.h>

namespace phaser {

class ResultGYRE : public Output, public DataA, public ResultCore
{
  public:
    ResultGYRE();
    ResultGYRE(Output&);
    virtual ~ResultGYRE() throw() {}

    mr_solution MRSET;
    map_str_pdb GYRE_PDB;
    mr_solution GYRE_MRSET;

    af_string   PdbFiles;
    std::string SolFile;
    std::string RlistFile;
    std::string TITLE;

  public:
    void      init(map_str_pdb&);
    af_string getFilenames();
    void      store(bool,size_t,std::string);
    std::string  pdbString(int);
    void      writePdb();
    void      writeSol();
    void      writeRlist();
    void      writeEns();

    const mr_solution& getDotSol()        { return MRSET; }
    const mr_solution& getDotRlist()      { return GYRE_MRSET; }
    af_float  getLLG();
    int       numRlist();

    std::vector<mr_gyre>  top_gyre()      { return MRSET.top_gyre(); }
    mr_set getTopSet() { MRSET.sort_LLG(); return MRSET.size() ? MRSET[0] : mr_set(); }
};

}
#endif
