//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputBase__Class__
#define __InputBase__Class__
#include <sstream>

namespace phaser {

enum Token_value {
  NAME, NUMBER, END, ENDLINE, ASSIGN
};

//reminder! no objects of an abstract base class can be instantiated
class InputBase  //abstract
{
  public:
    InputBase() {}
    ~InputBase() { }
    virtual Token_value parse(std::istringstream&) = 0;
};

typedef InputBase* inputPtr;

} //phaser

#endif
