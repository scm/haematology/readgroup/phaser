//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_ELLG__Class__
#define __InputMR_ELLG__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_ELLG :
                  public BINS
                 ,public CELL
                 ,public COMP
                 ,public ENSE
                 ,public ELLG
                 ,public JOBS
                 ,public LABI
                 ,public OUTL
                 ,public MACA
                 ,public MACT
                 ,public MUTE
                 ,public OCCU
                 ,public RESH
                 ,public RESO
                 ,public ROOT
                 ,public SEAR
                 ,public SOLP
                 ,public SOLU
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public NORM
                 ,public VERB
                 ,public KILL
{
  public:
    InputMR_ELLG(std::string);
    InputMR_ELLG();
    ~InputMR_ELLG();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
