//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __Preprocessor__Class__
#define __Preprocessor__Class__
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <phaser/io/Errors.h>

namespace phaser {

class Preprocessor
{
  private:
    std::string echo;
    PhaserError ppErr;

  public:
    Preprocessor();
    Preprocessor(std::string);
    Preprocessor(int, char*[]);
    ~Preprocessor();
    std::string Cards();
    std::string stoup(const std::string & str);
    void recursive_processor(std::istream&,int=0);
    std::string getFileName(std::istringstream&);
    void store(PhaserError);
    void Analyse();
};

} //phaser

#endif
