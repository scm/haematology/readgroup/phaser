//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputANO__Class__
#define __InputANO__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputANO :
                  public BINS
                 ,public CELL
                 ,public COMP
                 ,public HKLI
                 ,public HKLO
                 ,public LABI
                 ,public MACA
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public ROOT
                 ,public RESH
                 ,public RESO
                 ,public SGAL
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public VERB
                 ,public KILL
{
  public:
    InputANO(std::string);
    InputANO();
    ~InputANO();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
