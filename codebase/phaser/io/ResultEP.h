//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultEP__Class__
#define __ResultEP__Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_mtz.h>
#include <phaser/include/data_spots.h>
#include <phaser/io/Output.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/HandEP.h>
#include <phaser/ep_objects/ep_solution.h>
#include <iotbx/pdb/hierarchy.h>
#include <cctbx/hendrickson_lattman.h>

namespace phaser {

class ResultEP : public Output, public UnitCell
{
  public:
    ResultEP();
    ResultEP(Output&);
    virtual ~ResultEP() throw() {}

  public:
    af::shared<miller::index<int> >  MILLER;
    bool          second_hand;
    floatType     TopRfac;

    std::vector<HandEP> hand1,hand2;

  public:
    void          setMiller(af::shared<miller::index<int> > array) { MILLER = array; }
    void          addDataEP(bool,SpaceGroup,HandEP);
    void          setTopRfac(floatType s)           { TopRfac = s; }

    bool          hasSecondHand()                   { return second_hand; }
    floatType     getTopRfac()                      { return TopRfac; }
    af::shared<miller::index<int> > getMiller()     { return MILLER; }

    floatType     getLogLikelihood(bool h=false,int t=0)    { return h ? hand2[t].atoms.LLG : hand1[t].atoms.LLG; }
    std::string   getPdbFile(bool h=false,int t=0)          { return h ? hand2[t].PDBfile : hand1[t].PDBfile; }
    std::string   getMtzFile(bool h=false,int t=0)          { return h ? hand2[t].MTZfile : hand1[t].MTZfile; }
    std::string   getMapFile(bool h=false,int t=0)          { return h ? hand2[t].MAPfile : hand1[t].MAPfile; }
    std::string   getSolFile(bool h=false,int t=0)          { return h ? hand2[t].SOLfile : hand1[t].SOLfile; }
    af_bool       getSelected(bool h=false,int t=0)         { return h ? hand2[t].selected : hand1[t].selected; }
    af_float      getFOM(bool h=false,int t=0)              { return h ? hand2[t].FOM : hand1[t].FOM; }
    af_float      getFPFOM(bool h=false,int t=0)            { return h ? hand2[t].FPFOM : hand1[t].FPFOM; }
    af_float      getFWT(bool h=false,int t=0)              { return h ? hand2[t].FWT : hand1[t].FWT; }
    af_float      getPHWT(bool h=false,int t=0)             { return h ? hand2[t].PHWT : hand1[t].PHWT; }
    af_float      getPHIB(bool h=false,int t=0)             { return h ? hand2[t].PHIB : hand1[t].PHIB; }
    af::shared<cctbx::hendrickson_lattman<double> >
                  getHL(bool h=false,int t=0)               { return h ? hand2[t].HL : hand1[t].HL; }
    af_float      getLLG_F(std::string,bool h=false,int t=0);
    af_float      getLLG_PHI(std::string,bool h=false,int t=0);
    std::string   getSpaceGroupHall(bool h=false)   { return h ? hand2[0].getHall(): hand1[0].getHall(); }
    std::string   getSpaceGroupName(bool h=false)   { return h ? hand2[0].spcgrpname(): hand1[0].spcgrpname(); }
    int           getSpaceGroupNum(bool h=false)    { return h ? hand2[0].spcgrpnumber(): hand1[0].spcgrpnumber(); }
    scitbx::af::shared<cctbx::xray::scatterer<double> >
                  getAtoms(bool h=false,int t=0)            { return h ? hand2[t].getAtoms() : hand1[t].getAtoms(); }
    map_str_int   getAtomNum(bool h=false,int t=0)          { return h ? hand2[t].getAtomNum() : hand1[t].getAtomNum(); }

    int           stats_numbins(bool h=false,int t=0)       { return h ? hand2[t].acentStats.numbins() : hand1[t].acentStats.numbins(); }
    floatType     stats_hires(bool h=false,int t=0)         { return h ? hand2[t].acentStats.HiRes() : hand1[t].acentStats.HiRes(); }
    floatType     stats_lores(bool h=false,int t=0)         { return h ? hand2[t].acentStats.LoRes() : hand1[t].acentStats.LoRes(); }
    floatType     stats_acentric_fom(bool h=false,int t=0)  { return h ? hand2[t].acentStats.FOM() : hand1[t].acentStats.FOM(); }
    int           stats_acentric_num(bool h=false,int t=0)  { return h ? hand2[t].acentStats.Num() : hand1[t].acentStats.Num(); }
    floatType     stats_centric_fom(bool h=false,int t=0)   { return h ? hand2[t].centStats.FOM() : hand1[t].centStats.FOM(); }
    int           stats_centric_num(bool h=false,int t=0)   { return h ? hand2[t].centStats.Num() : hand1[t].centStats.Num(); }
    floatType     stats_fom(bool h=false,int t=0)           { return h ? hand2[t].allStats.FOM() : hand1[t].allStats.FOM(); }
    int           stats_num(bool h=false,int t=0)           { return h ? hand2[t].allStats.Num() : hand1[t].allStats.Num(); }
    bool          stats_is_set(bool h=false,int t=0)        { return h ? hand2[t].acentStats.numbins() : hand1[t].acentStats.numbins(); }
    floatType     stats_singleton_fom(bool h=false,int t=0) { return h ? hand2[t].singleStats.FOM() : hand1[t].singleStats.FOM(); }
    int           stats_singleton_num(bool h=false,int t=0) { return h ? hand2[t].singleStats.Num() : hand1[t].singleStats.Num(); }

    af_string     getFilenames();

    void  writeScript(bool h=false,int t=0,std::string title="",std::string xtalid="");
    void  writePdb(bool h=false,int t=0,std::string title="");
    void  writeMtz(bool h,int,std::string,std::string,double);
    void  writeMtz(bool h,int,std::string,std::string,data_spots,data_spots,double);
    void  writeMap(bool h=false,int t=0,std::string="",std::string="");

    HandEP        getHand(bool h=false,int t=0) const       {return h ? (t < hand2.size()?hand2[t]:HandEP()) : (t < hand1.size()?hand1[t]:HandEP()); }
    void          addHand(HandEP& dat,bool h=false) { h ? hand2.push_back(dat) : hand1.push_back(dat); }
    void          setHand(HandEP& dat,bool h=false) { h ? hand2.clear() : hand1.clear();  h ? hand2.push_back(dat) : hand1.push_back(dat); }
    iotbx::pdb::hierarchy::root getIotbx(bool h=false,int t=0);

    af::shared<cctbx::hendrickson_lattman<double> >
                  getHL_ANOM(bool h=false,int t=0)          { return h ? hand2[t].HL_ANOM : hand1[t].HL_ANOM; }
    floatType     getFp(std::string,bool h=false,int t=0);
    floatType     getFdp(std::string,bool h=false,int t=0);
    af_float      get_variance_array(bool h=false,int t=0)  { return h ? hand2[t].get_variance_array() : hand1[t].get_variance_array(); }
    int           getNum(bool h=false)              { return h ? hand2.size() : hand1.size(); }

    void          sort();
    void          unsort();
    std::pair<int,int> getTop();
    scitbx::af::shared<cctbx::xray::scatterer<double> > getTopAtoms();
    std::string   getTopPdbFile();
    std::string   getTopMtzFile();

    ep_solution   getDotSol(bool);

  private:
    af_float      p1_generator(bool,int,af_float,bool=false);
    af::shared<cctbx::hendrickson_lattman<double> >
                  p1_generator(bool,int,af::shared<cctbx::hendrickson_lattman<double> >);

};

}
#endif
