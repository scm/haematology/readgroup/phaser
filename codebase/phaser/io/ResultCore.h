//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultCoreClass__
#define __ResultCoreClass__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/data_pdb.h>

namespace phaser {

class ResultCore
{
  public:
    ResultCore()
    {
      relative_wilsonB.clear();
      ense_chainids.clear();
      PDB.clear();
    }
    virtual ~ResultCore() throw() {}

  public:
    map_str_float2D relative_wilsonB;
    map_str_str1D   ense_chainids;
    map_str_pdb     PDB;
    bool            CHAINOCC;

  public:
    void set_pdb_chainids(map_str_pdb&,bool=false);
    void store_relative_wilsonB(map_str_float2D);

    bool       getChainOcc() { return CHAINOCC; }
};

} //phaser

#endif
