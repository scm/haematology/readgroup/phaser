//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <stdarg.h>
#include <math.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/io/END.h>
#include <phaser/main/Version.h>
#include <phaser/lib/jiffy.h>
#include <cassert>
#include <cctype>
#if BOOST_VERSION < 104600
#include <boost/filesystem/operations.hpp>
#elif BOOST_VERSION < 105000
#include <boost/filesystem/v3/operations.hpp>
#else
#include <boost/filesystem/operations.hpp>
#endif


namespace phaser {

#include <ios>
#include <iostream>
#include <fstream>
#include <string>

//////////////////////////////////////////////////////////////////////////////
//
// process_mem_usage(double &, double &) - takes two doubles by reference,
// attempts to read the system-dependent data for a process' virtual memory
// size and resident set size, and return the results in KB.
//
// On failure, returns 0.0, 0.0

void Output::process_mem_usage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;
#if defined(_MSC_VER) || defined(__MINGW32__)
  // to be implemented with Win32 API
#else
   #include <unistd.h>

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
#endif
}

void Output::logMemory(outStream where)
{
  double vm, rss;
  process_mem_usage(vm, rss);
  if (vm != virtual_memory || rss != resident_set_size)
    logTab(0,where,"Memory: virtual-memory=" + dtos(vm) + " resident-set-size=" + dtos(rss));
  virtual_memory = vm;
  resident_set_size = rss;
}

}//namespace phaser
