//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_ATOM.h>

namespace phaser {

InputMR_ATOM::InputMR_ATOM(std::string cards)
{ parseCCP4(cards); }

InputMR_ATOM::InputMR_ATOM() {}
InputMR_ATOM::~InputMR_ATOM() {}

//performs the analysis between correlated keyword Input
void InputMR_ATOM::Analyse(Output& output)
{
  //ATOM::analyse();
  CELL::analyse();
  COMP::analyse();
  LABI::analyse();
  CRYS_DATA.MILLER = REFLECTIONS.MILLER;
  af_float Fdat,SIGFdat; // Replace with I/SIGI when EP changed to use these
  std::string RefID;
  if (REFLECTIONS.INPUT_INTENSITIES)
  {
    Fdat    = REFLECTIONS.FfromI;
    SIGFdat = REFLECTIONS.SIGFfromI;
    RefID = REFLECTIONS.I_ID;
  }
  else
  {
    Fdat    = REFLECTIONS.F;
    SIGFdat = REFLECTIONS.SIGF;
    RefID = REFLECTIONS.F_ID;
  }
  CRYS_DATA.setNative(Fdat,SIGFdat,RefID);
  CRYS::analyse();
  ELLG::analyse();
  ENSE::analyse();
  HKLI::analyse();
  HKLO::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LLGC::analyse();
  LLGM::analyse();
  MACA::analyse();
  MACM::analyse();
  MACS::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  PACK::analyse();
  PEAK::analyse();
  PERM::analyse();
  PURG::analyse();
  RESC::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  SAMP::analyse();
  SEAR::analyse();
  SGAL::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  WAVELENGTH = 1;
  WAVE::analyse();
  VERB::analyse();
  XYZO::analyse();
  ZSCO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE

  MACM_CHAINS.Override(false); //overwrite user input
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputMR_ATOM::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += ATOM::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += CRYS::unparse();
  cards += ELLG::unparse();
  cards += ENSE::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LABI::unparse();
  cards += LLGC::unparse();
  cards += LLGM::unparse();
  cards += MACA::unparse();
  cards += MACM::unparse();
  cards += MACS::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += PACK::unparse();
  cards += PEAK::unparse();
  cards += PERM::unparse();
  cards += PURG::unparse();
  cards += RESC::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += SAMP::unparse();
  cards += SEAR::unparse();
  cards += SGAL::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += WAVE::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += ZSCO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
