//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputEP_SAD.h>

namespace phaser {

InputEP_SAD::InputEP_SAD(std::string cards) : InputEP_AUTO(cards) {}
InputEP_SAD::InputEP_SAD() {}
InputEP_SAD::~InputEP_SAD() {}

} //phaser
