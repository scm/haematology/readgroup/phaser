//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_FUSE__Class__
#define __InputMR_FUSE__Class__
#include <phaser/io/InputMR_AUTO.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_FUSE :
                  public InputMR_AUTO
{
  public:
    InputMR_FUSE(std::string);
    InputMR_FUSE();
    ~InputMR_FUSE();

   size_t best_known_size;
   int    num_remaining;
   bool   smaller_than_best;

   bool   some_amalgamated;
   bool   none_to_amalgamate;
   bool   amalgamation_attempted;
};

} //phaser

#endif
