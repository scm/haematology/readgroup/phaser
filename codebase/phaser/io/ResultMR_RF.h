//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultMR_RF_Class__
#define __ResultMR_RF_Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/pod/xyzout.h>
#include <phaser/include/data_peak.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/io/Output.h>
#include <phaser/src/UnitCell.h>
#include <phaser/io/ResultCore.h>

namespace phaser {

class RF_RFZ_t_e
{
  public:
  RF_RFZ_t_e() { }
  RF_RFZ_t_e(floatType l,floatType z,floatType r,int k):RF(l),RFZ(z),t(r),e(k) { }
  floatType RF,RFZ;
  int t,e;
  bool operator<(const RF_RFZ_t_e &right)  const
  {return (RF < right.RF);}
};

class ResultMR_RF : public Output, public UnitCell, public ResultCore
{
  public:
    ResultMR_RF();
    ResultMR_RF(Output&);
    virtual ~ResultMR_RF() throw() {}

  private:
    mr_solution MRSET;
    std::string TITLE;
    unsigned    NTOP;
    std::string RlistFile;
    af_string   PdbFiles;
    floatType   SAMPLING;
    floatType   TopRfac;
    std::string RF_HALL; //for giving point group of rotations only, when TF has not been run (space group not fixed)

    std::vector<RF_RFZ_t_e>           sorted_index;

  public:
    void                init(std::string,std::string,unsigned,map_str_pdb&);
    void                storeRotlist(std::string,mr_set,dvect31D,float1D,float1D,bool1D,int1D);
    void                setRlistOrigin(std::string,std::string,mr_set,floatType);
    void                storePurgeMean(floatType);
    void                storeResolution(floatType,bool);
    void                setDotRlist(const mr_solution&);

    void                setSampling(floatType s) { SAMPLING = s; }
    void                setTopRfac(floatType s)  { TopRfac = s; }
    void                set_as_rescored(bool b)  { MRSET.set_as_rescored(b); }
    floatType           getSampling() const { return SAMPLING; }

    void                setHall(std::string h)   { RF_HALL = h; }
    std::string         getHall() const     { return RF_HALL; }

    af_string           getPdbFiles()    { return PdbFiles; }
    std::string         getRlistFile()   { return RlistFile; }
    const mr_solution&  getDotRlist()    { return MRSET; }
    mr_set              getSet(int t)    { PHASER_ASSERT(t < MRSET.size()); return MRSET[t]; }
    unsigned            getNumTop()      { return std::min(NTOP,static_cast<unsigned>(sorted_index.size())); }
    const std::string   getTitle() const { return TITLE; }
    mr_rlist            top_rlist()      { return MRSET.top_rlist(); }
    floatType           getTopRfac()     { return TopRfac; }
    int                 numRlist();
    int                 numSolutions()   { return MRSET.size(); }
    int                 numDeep()        { return MRSET.ndeep(); }
    int                 not_numDeep()    { return MRSET.not_ndeep(); }
    af_float            getRF();
    af_float            getRFZ();

    af_string getFilenames();
    void writeRlist();
    void writePdb(xyzout,MapTrcPtr);

    std::vector<delel>  finalSelection(floatType,floatType,int);
};

}
#endif
