//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Preprocessor.h>
#include <phaser/io/END.h>
#include <phaser/lib/jiffy.h>
#include <cctype>
#include <boost/version.hpp>
#include <boost/algorithm/string/predicate.hpp>
#if BOOST_VERSION < 104600
#include <boost/filesystem/operations.hpp>
#elif BOOST_VERSION < 105000
#include <boost/filesystem/v3/operations.hpp>
#else
#include <boost/filesystem/operations.hpp>
#endif

namespace phaser {

Preprocessor::Preprocessor()
{}

Preprocessor::Preprocessor(std::string e)
{ echo = e; }

Preprocessor::Preprocessor(int argc, char* argv[])
{
  echo = "";
/*
  //turns the command line into keyworded input
  if (argc >= 4) //phaser pdb mtz seq
  {
    echo += "MODE MR_AUTO\n";
    for(int i=1; i<argc; i++)
    {
      if (boost::algorithm::ends_with(argv[i], ".mtz"))
        echo += "HKLIN " + std::string(argv[i]) + "\n";
      else if (boost::algorithm::ends_with(argv[i], ".pdb"))
        echo += "ENSEMBLE ensemble" + itos(i) + " PDB " + std::string(argv[i]) + " ID 100\n";
      else if (boost::algorithm::ends_with(argv[i], ".seq"))
        echo += "COMPOSITION PROTEIN SEQUENCE " + std::string(argv[i]) + "\n";
    }
  }
  else
*/
  {
  recursive_processor(std::cin);
//in-place removal of blank lines in echo
  size_t pos(0);
  while (pos < echo.size())
  {
    size_t pos_return = echo.find('\n',pos);
    std::string new_line = echo.substr(pos,pos_return-pos);
    bool blank_line(true);
    for (int i = 0; i < new_line.size(); i++)
      if (!(std::isspace)(new_line[i])) blank_line = false;
    if (blank_line) echo.erase(pos,pos_return-pos+1);
    else pos = pos_return+1;
  }
  }
}

Preprocessor::~Preprocessor() { }

std::string Preprocessor::Cards()
{ return echo; }

std::string Preprocessor::stoup(const std::string & str)
{
  char ch;
  std::stringstream s;
  std::string upper_str="";
  s << str;
  while (s.get(ch))
    upper_str += std::toupper(ch); //convert to uppercase
  return upper_str;
}

void Preprocessor::recursive_processor(std::istream& input_stream,int counter)
{
  counter++;
  while (!input_stream.eof() && counter)
  {
    std::string new_line;
    std::getline(input_stream, new_line,'\n');
    new_line += "\n";
    std::istringstream line_stream(const_cast<char*>(new_line.c_str()));
    char ch;
    do {
         if (!line_stream.get(ch))
         break;
    } while ((std::isspace)(ch) || ch == '\n'); //skip space, tabs, feedforms
    if (ch == '@')
    {
      echo += '#' + new_line;
      std::string filename = getFileName(line_stream); //take next white space separated (quoted) string
      std::ifstream file_stream(const_cast<char*>(filename.c_str()));
      if (!filename.size())
        store(PhaserError(FILEOPEN,echo,"(null filename)"));
      else if (!file_stream) //file does not exist
        store(PhaserError(FILEOPEN,echo,filename));
      //else if (boost::filesystem::is_directory(filename))
        //store(PhaserError(FILEOPEN,echo,filename + " (directory)"));
      else recursive_processor(file_stream,counter);
    }
    else
    {
      line_stream.putback(ch);
      echo += new_line;
      std::vector<std::string> end_keys = END_KEYS();
      std::string keyword;
      line_stream >> keyword; //remove leading white spaces
      for (int i = 0; i < end_keys.size(); i++)
        if (!stoup(keyword).find(end_keys[i]))
        {
//special case for END at the command line (flagged by counter=1)
//forces while loop condition fail (counter=0) and exit from command line
          if (counter==1) counter--;
          break;
        }
    }
  }
  counter--;
}

std::string Preprocessor::getFileName(std::istringstream& input_stream)
{
  //allows =,#,! in filenames
  //skip leading spaces (there must be leading spaces)
  bool quoted_string=false;
  for (;;)
  {
    char ch;
    if (input_stream.get(ch))
    {
      if (ch == '\n')
      {
        input_stream.putback(ch); //so that ENDLINE token is found
        return "";
      }
      else if ((std::isspace)(ch))
      {
        ; //accept and ignore space
      }
      else if (ch == '\"')
      {
        quoted_string=true;
        break;
      }
      else //it is an interesting char and needs to be read again
      {
        input_stream.putback(ch);
        break;
      }
    }
    else
    {
      input_stream.putback(ch); //so that END can be found
      return "";
    }
  }
  std::string filename;
  for (;;)
  {
    char ch;
    if (input_stream.get(ch))
    {
      if ((std::isspace)(ch) && !quoted_string)
      {
        input_stream.putback(ch); //so that ENDLINE token is found
        return filename;
      }
      else if (ch == '\"' && quoted_string)
      {
        return filename;
      }
      else
      {
        filename += ch;
      }
    }
    else
    {
      input_stream.putback(ch); //so that END can be found
      return filename;
    }
  }
}

void Preprocessor::store(PhaserError err)
{ if (ppErr.Success()) ppErr = err; } //store first one

void Preprocessor::Analyse()
{ if (ppErr.Failure()) throw ppErr; }

} //phaser
