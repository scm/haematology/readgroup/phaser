//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultMR.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rotdist.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <phaser/io/PdbHandler.h>
#include <phaser/io/Errors.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/src/ListEditing.h>
#include <cstring>

#if defined(__linux)
#include <sys/stat.h>
#include <sys/types.h>
#elif defined(_WIN32)
// BYTE, INT16 and INT32 are defined in libccp4/ccp4/ccp4_sysdep.h
// and they conflict with Windows typedefs.
#ifdef BYTE
#undef BYTE
#endif
#ifdef INT16
#undef INT16
#endif
#ifdef INT32
#undef INT32
#endif

#include <windows.h>
#endif

namespace phaser {

ResultMR::ResultMR() : Output(),DataA()
{
  NTOP = PACK_CUTOFF = OCCUPANCY_NRES = OCCUPANCY_WINDOW_ELLG = 0;
  RESULT_TYPE = "LLG";
}

ResultMR::ResultMR(Output& output) : Output(output),DataA()
{
  NTOP = PACK_CUTOFF = OCCUPANCY_NRES = OCCUPANCY_WINDOW_ELLG = 0;
  RESULT_TYPE = "LLG";
}

ResultMR::ResultMR(Output& output,DataA& data,std::string t,unsigned n)
         : Output(output),DataA(data),TITLE(t),NTOP(n)
{
  //this is the constructor for llg to be used from Python
  OCCUPANCY_NRES = OCCUPANCY_WINDOW_ELLG = 0;
  RESULT_TYPE = "LLG";
}

void ResultMR::init(std::string rt,std::string t,unsigned n,map_str_pdb& pdb,bool chainocc)
{
  RESULT_TYPE = rt;
  TITLE = t;
  NTOP = n;
  set_pdb_chainids(pdb,chainocc);
  PHASER_ASSERT(rt == "PAK" || rt == "LLG");
}

void ResultMR::writeSol()
{
  if (MRSET.size())
  {
    SolFile = Output::Fileroot() + std::string(MRSET.is_rlist() ? ".rlist" : ".sol");
    ResultFile opened(SolFile);

    fprintf(opened.outFile,"# %s \n",TITLE.c_str());
    fprintf(opened.outFile,"%s\n",MRSET.unparse().c_str());
    fclose(opened.outFile);
  }
}

void ResultMR::writeSolReject()
{
  if (MRSET_REJECT.size())
  {
    SolFileReject = Output::Fileroot() + ".pak_reject_high_tfz.sol";
    ResultFile opened(SolFileReject);

    fprintf(opened.outFile,"# %s \n",TITLE.c_str());
    for (unsigned k = 0; k < MRSET_REJECT.size(); k++)
      fprintf(opened.outFile,"%s\n",MRSET_REJECT[k].unparse().c_str());
    fclose(opened.outFile);
  }
}

void ResultMR::write_pdb()
{ //chainocc = write only non-zero occ atoms, for chain refinement
  xyzout DO_XYZOUT(true);
  bool chainocc = false;
  for (unsigned t = 0; t < getNumTop(); t++)
    writePdbFull(t,DO_XYZOUT,chainocc,false,false,false);
}

void ResultMR::writePdb(xyzout DO_XYZOUT,map_str_pdb& PDB,bool chainocc)
{ //chainocc = write only non-zero occ atoms, for chain refinement
  for (unsigned t = 0; t < getNumTop(); t++)
    writePdbFull(t,DO_XYZOUT,chainocc,false,false,false);
  if (!DO_XYZOUT.True() || !DO_XYZOUT.PRINCIPAL) return;
  writePdbPrincipal(PDB);
}

void ResultMR::writePdbPrincipal(map_str_pdb& PDB)
{
  std::string pdbstr;
  for (map_str_pdb::iterator iter = PDB.begin(); iter != PDB.end(); iter++)
  for (int m = 0; m < iter->second.nMols(); m++)
  {
    int NMODEL(1);
    std::string filename = Output::Fileroot() + ".principal." + iter->second.ENSEMBLE[m].basename();
    if (std::find(PrincipalFiles.begin(), PrincipalFiles.end(), filename) == PrincipalFiles.end())
      PrincipalFiles.push_back(filename);
    Molecule pdb_mol = iter->second.Coords(m);
    ResultFile opened(filename);
    bool no_models(iter->second.nMols() == 1 && pdb_mol.nMols() == 1);
    for (int mm = 0; mm < pdb_mol.nMols(); mm++)
    {
      if (!no_models)
      {
        pdbstr = "MODEL " + itos(NMODEL,8,true) + "\n";
        fprintf(opened.outFile,"%s",pdbstr.c_str());
      }
      for (unsigned a = 0; a < pdb_mol.nAtoms(mm); a++)
      {
        pdb_record card = pdb_mol.card(mm,a);
        card.X = iter->second.PR*card.X + iter->second.PT;
        pdbstr = card.Card();
        fprintf(opened.outFile,"%s",pdbstr.c_str());
      }
      if (!no_models)
      {
        pdbstr = "ENDMDL\n";
        fprintf(opened.outFile,"%s",pdbstr.c_str());
      }
      NMODEL++;
    }
    fclose(opened.outFile);
  }
}

void ResultMR::writePdbRealOcc(xyzout DO_XYZOUT)
{
  for (unsigned t = 0; t < getNumTop(); t++)
  if (MRSET[t].REALOCC.size()) //flag for set, cleared in runMR_OCC if no improvement
    writePdbFull(t,DO_XYZOUT,false,false,false,true);
}

string1D ResultMR::string_pdbs()
{
  string1D PDBSTR;
  for (unsigned t = 0; t < getNumTop(); t++)
    PDBSTR.push_back(writePdbFull(t,false,false,true,true,false)[0]);
  return PDBSTR;
}

string1D ResultMR::writePdbFull(int t,xyzout DO_XYZOUT,bool chainocc,bool rtnstr,bool catstr,bool realocc)
{
  //all the correction terms for DATACELL here
  bool Space_Group_of_Solution_Set(MRSET[t].HALL != "");
  PHASER_ASSERT(Space_Group_of_Solution_Set);
  std::string sgname = space_group_name(MRSET[t].HALL,true).CCP4;
  int Z = SpaceGroup(MRSET[t].HALL).getSpaceGroupNSYMM()*MRSET[t].Z();
  af::double6 unit_cell = UnitCell::getCell6();
  std::string crystcard;
  {
    af::double6 scaled_cell = UnitCell::getCell6();
    scaled_cell[0] *= MRSET[t].DATACELL;
    scaled_cell[1] *= MRSET[t].DATACELL;
    scaled_cell[2] *= MRSET[t].DATACELL;
    crystcard = UnitCell(scaled_cell).CrystCard(sgname,Z);
  }

  string1D pdbstr(1);
  if (DO_XYZOUT.True()) catstr = true;
  if (!catstr) pdbstr.resize(MRSET[t].KNOWN.size()+1);
  if (DO_XYZOUT.True() || rtnstr)
  {
    int num_chainid(0);
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[s].MODLID) != ense_chainids.end()) //could be a map
      {
        std::string m = MRSET[t].KNOWN[s].MODLID;
        num_chainid += ense_chainids[m].size();
      }
    }

    pdb_chains_2chainid chains(num_chainid);

    int index(0);
    pdbstr[index] += "REMARK TITLE " + TITLE + "\n";
    if (RESULT_TYPE == "LLG")
      pdbstr[index] +=  "REMARK Log-Likelihood Gain: " + dtos(MRSET[t].LLG,8,3) + "\n";
    pdbstr[index] +=  "REMARK " + MRSET[t].ANNOTATION + "\n";
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[s].MODLID) != ense_chainids.end()) //could be a map
      {
        dvect3 outR = matrix2eulerDEG(MRSET[t].KNOWN[s].R);
        dvect3 outT = MRSET[t].KNOWN[s].TRA;
        std::string m = MRSET[t].KNOWN[s].MODLID;
        pdbstr[index] += "REMARK ENSEMBLE " + m + " EULER " + dvtos(outR,6,2) + " FRAC " + dvtos(outT,5,3) + "\n";
      }
    }
    pdbstr[index] += crystcard;
    int atom_number(1);
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      if (!catstr) index++;
      if (ense_chainids.find(MRSET[t].KNOWN[s].MODLID) != ense_chainids.end()) //could be a map
      {
        std::string m = MRSET[t].KNOWN[s].MODLID;
        //the mapping of the input ensemble chain to the output chain
        map_str_str map_chains;
        for (int i = 0; i < ense_chainids[m].size(); i++)
        {
          if (DO_XYZOUT.CHAIN_COPY)
          {
            map_chains[ense_chainids[m][i]] = ense_chainids[m][i];
          }
          else
          {
            map_chains[ense_chainids[m][i]] = chains.allocate(ense_chainids[m][i]); //map the input to the next allowed output
            if (chains.allocate_failed())
              logWarning(SUMMARY,chains.message());
          }
        }
        std::string hall = MRSET[t].HALL;
        bool renumber_atoms(MRSET[t].KNOWN.size() > 1);
        bool delete_overlaps(true);
        float1D OCC = realocc ? MRSET[t].REALOCC[s] : MRSET[t].bool_to_real_occupancies(s);
        float1D* OCCptr = OCC.size() ? &OCC:NULL;
        std::string card = pdb_string_maker(PDB,map_chains,MRSET[t].KNOWN[s],renumber_atoms,atom_number,hall,unit_cell,delete_overlaps,relative_wilsonB[m],tracemol,chainocc,OCCptr,MRSET[t].DATACELL);
        pdbstr[index] += card;
      }
    }
    if (catstr) pdbstr[index] += "END\n";

    if (DO_XYZOUT.True())
    {
      std::string filename = Output::Fileroot() + "." + itos(t+1) + std::string(realocc?".refined_occupancy.pdb": ".pdb");
      if (std::find(PdbFiles.begin(), PdbFiles.end(), filename) == PdbFiles.end())
        PdbFiles.push_back(filename);
      ResultFile opened(filename);
      // the string will be in index = 0
      fprintf(opened.outFile,"%s",pdbstr[0].c_str());
      fclose(opened.outFile);
    }
  }

  if (DO_XYZOUT.True() && DO_XYZOUT.ENSEMBLE)
  {
    bool there_are_ensembles(false);
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      std::string modlid = MRSET[t].KNOWN[s].MODLID;
      bool no_models(PDB[modlid].allRms().size() == 1);
      there_are_ensembles = there_are_ensembles || !no_models;
    }

    if (there_are_ensembles)
    {
      string1D EnsFile;
      for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
      {
        if (ense_chainids.find(MRSET[t].KNOWN[s].MODLID) != ense_chainids.end()) //could be a map
        {
          std::string filename =  Output::Fileroot() + "." + itos(t+1) + "." + itos(s+1) + std::string(realocc?".refined_occupancy.pdb": ".pdb");
          ResultFile opened(filename);
          EnsFile.push_back(filename);
          fprintf(opened.outFile,"REMARK TITLE %s \n",TITLE.c_str());
          if (RESULT_TYPE == "LLG")
            fprintf(opened.outFile,"REMARK Log-Likelihood Gain: %8.3f\n",MRSET[t].LLG);
          fprintf(opened.outFile,"REMARK %s\n",MRSET[t].ANNOTATION.c_str());
          dvect3 outR = matrix2eulerDEG(MRSET[t].KNOWN[s].R);
          dvect3 outT = MRSET[t].KNOWN[s].TRA;
          std::string m = MRSET[t].KNOWN[s].MODLID;
          fprintf(opened.outFile,"REMARK ENSEMBLE %s EULER %6.2f %6.2f %6.2f FRAC % 5.3f % 5.3f % 5.3f\n",
                     m.c_str(),outR[0],outR[1],outR[2],outT[0],outT[1],outT[2]);
          std::string sgname = space_group_name(MRSET[t].HALL,true).CCP4;
          fprintf(opened.outFile,"%s",crystcard.c_str());
          //the mapping of the input ensemble chain to the output chain
          map_str_str map_chains;
          for (int i = 0; i < ense_chainids[m].size(); i++)
            map_chains[ense_chainids[m][i]] = ense_chainids[m][i]; //map the input to itself: no change in chainids
          std::string hall = MRSET[t].HALL;
          bool renumber_atoms(false);
          int atom_number(1);
          bool delete_overlaps(true);
          float1D OCC = realocc ? MRSET[t].REALOCC[s] : MRSET[t].bool_to_real_occupancies(s);
          float1D* OCCptr = OCC.size() ? &OCC:NULL;
          std::string ens = ens_string_maker(PDB,map_chains,MRSET[t].KNOWN[s],renumber_atoms,atom_number,hall,unit_cell,delete_overlaps,relative_wilsonB[m],tracemol,OCCptr,MRSET[t].DATACELL);
          fprintf(opened.outFile,"%s",ens.c_str());
          fprintf(opened.outFile,"END\n");
          fclose(opened.outFile);
        }
      }
      if (std::find(EnsFiles.begin(), EnsFiles.end(), EnsFile) == EnsFiles.end())
        EnsFiles.push_back(EnsFile);
    }
  }

  if (DO_XYZOUT.True() && DO_XYZOUT.PACKING)
  {
    std::string pdbstr;
    int num_chainid(0);
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      if (ense_chainids.find(MRSET[t].KNOWN[s].MODLID) != ense_chainids.end()) //could be a map
      {
        std::string m = MRSET[t].KNOWN[s].MODLID;
        num_chainid += ense_chainids[m].size();
      }
    }

    pdb_chains_2chainid chains(num_chainid);

    pdbstr += "REMARK TITLE " + TITLE + "\n";
    if (RESULT_TYPE == "LLG")
      pdbstr +=  "REMARK Log-Likelihood Gain: " + dtos(MRSET[t].LLG,8,3) + "\n";
    pdbstr +=  "REMARK " + MRSET[t].ANNOTATION + "\n";
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      dvect3 outR = matrix2eulerDEG(MRSET[t].KNOWN[s].R);
      dvect3 outT = MRSET[t].KNOWN[s].TRA;
      std::string m = MRSET[t].KNOWN[s].MODLID;
      pdbstr += "REMARK ENSEMBLE " + m + " EULER " + dvtos(outR,6,2) + " FRAC " + dvtos(outT,5,3) + "\n";
    }
    pdbstr += crystcard;
    for (unsigned s = 0; s < MRSET[t].KNOWN.size(); s++)
    {
      std::string m = MRSET[t].KNOWN[s].MODLID;
      map_str_str map_chains;
      for (int i = 0; i < ense_chainids[m].size(); i++)
        map_chains[ense_chainids[m][i]] = chains.allocate(ense_chainids[m][i]); //map the input to the next allowed output
      std::string Chain =  map_chains.size() ? map_chains.begin()->second : "";
      pdbstr += pak_string_maker(PDB,MRSET[t].KNOWN[s],unit_cell,tracemol,Chain);
    }
    pdbstr += "END\n";
    std::string filename = Output::Fileroot() + "." + itos(t+1) + ".pak.pdb";
    if (std::find(PakFiles.begin(), PakFiles.end(), filename) == PakFiles.end())
      PakFiles.push_back(filename);
    ResultFile opened(filename);
    fprintf(opened.outFile,"%s",pdbstr.c_str());
    fclose(opened.outFile);
  }
  return pdbstr;
}

void ResultMR::writeMtz(std::string HKLIN)
{
  //can only call this function if it is not a packing analysis
  //i.e. if the reflections have been set
  bool not_subset(false);
  if (MILLER.size())
  {
    std::string REFID = INPUT_INTENSITIES ? I_ID : F_ID;
    logTab(1,DEBUG,"The reference LABIN for appending is " + REFID);
    for (unsigned t = 0; t < getNumTop(); t++)
    if (MRSET[t].MAPCOEFS.size())//double-check
    {
      bool Space_Group_of_MapCoeffs_Set(MRSET[t].HALL != "");
      PHASER_ASSERT(Space_Group_of_MapCoeffs_Set);
      SpaceGroup this_sg(MRSET[t].HALL);
      af::double6 unit_cell = UnitCell::getCell6();
      unit_cell[0] *= MRSET[t].DATACELL;
      unit_cell[1] *= MRSET[t].DATACELL;
      unit_cell[2] *= MRSET[t].DATACELL;
      UnitCell this_uc(unit_cell);
      MtzHandler mtzOut(this_sg,this_uc,MILLER,true,HKLIN); //convert to int silent
      std::string HKLOUT = Output::Fileroot() + "." + itos(t+1) + ".mtz";
      not_subset = not_subset || mtzOut.not_subset();
      af_bool PRESENT(selected.size(),true);
      if (std::find(MtzFiles.begin(), MtzFiles.end(), HKLOUT) == MtzFiles.end())
        MtzFiles.push_back(HKLOUT);
      if (F.size())
        mtzOut.addData(MILLER,PRESENT,F_ID,"F",F,REFID);
      if (SIGF.size())
        mtzOut.addData(MILLER,PRESENT,std::string(SIGF_ID.size()?SIGF_ID:"SIGF"),"Q",SIGF,REFID);
      if (FTFMAP.FMAP.size())
        mtzOut.addData(MILLER,selected,std::string(FTFMAP.FMAP_ID.size()?FTFMAP.FMAP_ID:"FMAP"),"F",FTFMAP.FMAP,REFID);
      if (FTFMAP.PHMAP.size())
        mtzOut.addData(MILLER,selected,std::string(FTFMAP.PHMAP_ID.size()?FTFMAP.PHMAP_ID:"PHMAP"),"P",FTFMAP.PHMAP,REFID);
      if (I.size() && SIGI.size())
      {
        mtzOut.addData(MILLER,PRESENT,I_ID,"J",I,REFID);
        mtzOut.addData(MILLER,PRESENT,SIGI_ID,"Q",SIGI,REFID);
      }
      if (level(VERBOSE))
      {
        mtzOut.addData(MILLER,PRESENT,F_ID.size()?F_ID+"_ISO":"F_ISO","F",getCorrectedF(),REFID);
        mtzOut.addData(MILLER,PRESENT,SIGF_ID.size()?SIGF_ID+"_ISO":"SIGF_ISO","Q",getCorrectedSIGF(),REFID);
      }
      mtzOut.addData(MILLER,selected,"FC","F",MRSET[t].MAPCOEFS.FC,REFID);
      mtzOut.addData(MILLER,selected,"PHIC","P",MRSET[t].MAPCOEFS.PHIC,REFID);
      mtzOut.addData(MILLER,selected,"FWT","F",MRSET[t].MAPCOEFS.FWT,REFID);
      mtzOut.addData(MILLER,selected,"PHWT","P",MRSET[t].MAPCOEFS.PHWT,REFID);
      mtzOut.addData(MILLER,selected,"DELFWT","F",MRSET[t].MAPCOEFS.DELFWT,REFID);
      mtzOut.addData(MILLER,selected,"PHDELWT","P",MRSET[t].MAPCOEFS.PHDELWT,REFID);
      mtzOut.addData(MILLER,selected,"FOM","W",MRSET[t].MAPCOEFS.FOM,REFID);
      if (level(VERBOSE))
      {
        mtzOut.addData(MILLER,selected,"HLA","A",MRSET[t].MAPCOEFS.HLA,REFID);
        mtzOut.addData(MILLER,selected,"HLB","A",MRSET[t].MAPCOEFS.HLB,REFID);
        mtzOut.addData(MILLER,selected,"HLC","A",MRSET[t].MAPCOEFS.HLC,REFID);
        mtzOut.addData(MILLER,selected,"HLD","A",MRSET[t].MAPCOEFS.HLD,REFID);
      }
      mtzOut.writeMtz(HKLOUT);
    }
  }
  if (not_subset)
    logAdvisory(SUMMARY,"Output reflections not a subset of " + HKLIN);
}

af_string ResultMR::getEnsFile(int f)
{
  af_string filenames;
  for (int t = 0; t < EnsFiles[f].size(); t++)
    filenames.push_back(EnsFiles[f][t]);
  return filenames;
}

af_string ResultMR::getEnsFiles()
{
  af_string filenames;
  for (int t = 0; t < EnsFiles.size(); t++)
  {
    af_string af_ensfiles = getEnsFile(t);
    filenames.insert(filenames.end(),af_ensfiles.begin(),af_ensfiles.end());
  }
  return filenames;
}

af_string ResultMR::getFilenames()
{
  af_string filenames;
  filenames.insert(filenames.end(),PdbFiles.begin(),PdbFiles.end());
  af_string af_ensfiles = getEnsFiles(); //vector2D->1D af_string
  filenames.insert(filenames.end(),af_ensfiles.begin(),af_ensfiles.end());
  filenames.insert(filenames.end(),MtzFiles.begin(),MtzFiles.end());
  filenames.insert(filenames.end(),PakFiles.begin(),PakFiles.end());
  if (SolFile.size()) filenames.push_back(SolFile);
  if (SolFileReject.size()) filenames.push_back(SolFileReject);
  filenames.insert(filenames.end(),PrincipalFiles.begin(),PrincipalFiles.end());
  return filenames;
}

af_string  ResultMR::getTopEnsFile()
{
  if (!EnsFiles.size()) return af_string();
  sort_LLG();
  return getEnsFile(0);
}

af_float ResultMR::getPAK()
{
  af_float values(MRSET.size());
  for (int t = 0; t < MRSET.size(); t++)
    values[t] = MRSET[t].PAK;
  return values;
}

af_float ResultMR::getLLG()
{
  af_float values(MRSET.size());
  for (int t = 0; t < MRSET.size(); t++)
    values[t] = MRSET[t].LLG;
  return values;
}

int1D ResultMR::getTemplatesForSolution(int k)
{ return (k-1 < MRSET.size()) ? MRSET[k-1].TMPLT : int1D(0); }

int1D ResultMR::getSolutionsForTemplate(int k)
{
  int1D t;
  for (int j = 0; j < MRSET.size(); j++)
    for (int i = 0; i < MRSET[j].TMPLT.size(); i++)
      if (MRSET[j].TMPLT[i] == k) t.push_back(j+1);
  return t;
}

pair_int_int ResultMR::calcDuplicateSolutions(floatType dmin,mr_solution MRTEMPLATE)
{
  ListEditing dat;
  return dat.calculate_duplicates(PDB,UnitCell::getCell6(),dmin,MRSET,MRTEMPLATE);
}

af_string ResultMR::phenixPDBNames()
{
  int nroot = Output::Fileroot().size();
  af_string filenames;
  for (int i = 0; i < PdbFiles.size(); i++)
  {
    std::string PDBextension = PdbFiles[i].substr(nroot);
    if (std::find(filenames.begin(), filenames.end(), PDBextension) == filenames.end())
      filenames.push_back(PDBextension);
  }
  return filenames;
}

int ResultMR::getUsedReflections()
{
  af::shared<miller::index<int> > sel = getSelectedMiller();
  return sel.size();
}

std::string ResultMR::get_gyre_pdb_str(int t,std::string chainref)
{
  string1D newstr;
  //gyred is last
  int last = MRSET[t].KNOWN.size(); //index=0 is header, others are index KNOWN array
  {//memory
    std::string newline;
    std::istringstream newiss(writePdbFull(t,false,false,true,false,false)[last]);
    while(std::getline(newiss, newline)) newstr.push_back(newline);
  }//memory
  std::istringstream chainiss(chainref);
  std::string chainline,newline;
  bool1D used(newstr.size(),false);
  for (int j = 0; j < newstr.size(); j++)
    if (newstr[j].size() < 66)
      used[j] = true;
  //assumes that pdbin and modified pdb have atoms in same order
  //basic check that the atomname,altloc,resname,renum are the same
  while(std::getline(chainiss, chainline))
  {
    if (chainline.size() >= 66)
    {
      for (int j = 0; j < newstr.size(); j++)
      if (!used[j] && //excludes same atomname,altloc,resname,resnum, different chain found earlier
          chainline.substr(12,8) == newstr[j].substr(12,8) && //atomname,altloc,resname
          chainline.substr(22,4) == newstr[j].substr(22,4)) //resnum
      {
        newstr[j].replace(20,2,chainline.substr(20,2)); //chainid
        used[j] = true;
        break;
      }
    }
  }
  std::string returnstr;
  for (int j = 0; j < newstr.size(); j++)
    returnstr += newstr[j] + "\n";
  return returnstr;
}

} //end namespace phaser
