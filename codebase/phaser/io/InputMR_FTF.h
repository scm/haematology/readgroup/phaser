//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_FTF__Class__
#define __InputMR_FTF__Class__
#include <phaser/keywords/keywords.h>

namespace phaser {

class InputMR_FTF :
                  public BINS
                 ,public BOXS
                 ,public CELL
                 ,public COMP
                 ,public ENSE
                 ,public FORM
                 ,public JOBS
                 ,public KEYW
                 ,public LABI
                 ,public MACA
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public PEAK
                 ,public PURG
                 ,public RESC
                 ,public RESH
                 ,public RESO
                 ,public RFAC
                 ,public ROOT
                 ,public SAMP
                 ,public SEAR
                 ,public SGAL
                 ,public SOLP
                 ,public SOLU
                 ,public SPAC
                 ,public TARG
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public TRAN
                 ,public VERB
                 ,public XYZO
                 ,public ZSCO
                 ,public KILL
{
  public:
    InputMR_FTF(std::string);
    InputMR_FTF();
    ~InputMR_FTF();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
