//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultEP_DAT__Class__
#define __ResultEP_DAT__Class__
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/data_ep.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
//#include <scitbx/array_family/tiny_types.h>

namespace phaser {

class ResultEP_DAT : public Output, public SpaceGroup, public UnitCell
{
  public:
    ResultEP_DAT();
    ResultEP_DAT(Output&);
    virtual ~ResultEP_DAT() throw() {}

  private:
    data_ep   CRYS_DATA;
    floatType HIRES,LORES;
    floatType MTZ_HIRES,MTZ_LORES;

  public:
    void setCrysData(data_ep m)              { CRYS_DATA = m; }
    void setReso(floatType r,floatType s)    { HIRES = std::min(r,s); LORES = std::max(r,s); }
    void setMtzReso(floatType r,floatType s) { MTZ_HIRES = std::min(r,s); MTZ_LORES = std::max(r,s); }

    floatType getMtzLores() { return MTZ_LORES; }
    floatType getMtzHires() { return MTZ_HIRES; }
    floatType getLores()    { return LORES; }
    floatType getHires()    { return HIRES; }
    data_ep   getCrysData() { return CRYS_DATA; }

    af::shared<miller::index<int> > getMiller()             { return CRYS_DATA.MILLER; }
    af_float  getF(std::string x="",std::string w="")       { return CRYS_DATA(x,w).getFmean(); }
    af_float  getFpos(std::string x="",std::string w="")    { return CRYS_DATA(x,w).getFpos(); }
    af_float  getFneg(std::string x="",std::string w="")    { return CRYS_DATA(x,w).getFneg(); }
    af_float  getSIGF(std::string x="",std::string w="")    { return CRYS_DATA(x,w).getSIGFmean(); }
    af_float  getSIGFpos(std::string x="",std::string w="") { return CRYS_DATA(x,w).getSIGFpos(); }
    af_float  getSIGFneg(std::string x="",std::string w="") { return CRYS_DATA(x,w).getSIGFneg(); }
    af_bool   getP(std::string x="",std::string w="")       { return CRYS_DATA(x,w).getPmean(); }
    af_bool   getPpos(std::string x="",std::string w="")    { return CRYS_DATA(x,w).getPpos(); }
    af_bool   getPneg(std::string x="",std::string w="")    { return CRYS_DATA(x,w).getPneg(); }

};

}
#endif
