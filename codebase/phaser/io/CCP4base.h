//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CCP4base__Class__
#define __CCP4base__Class__
#include <vector>
#include <set>
#include <cctype>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/InputBase.h>
#include <phaser/io/Errors.h>

namespace phaser {

class Output;

class CCP4base
{
  protected: //members
    std::string  keywords;
    floatType    number_value;
    std::string  string_value;
    Token_value  curr_tok;
    af_string    possible_keys;
    af::shared<inputPtr> possible_fns;
    std::vector<PhaserError>  inputError; //order is important

  private:
    bool annotate_keywords;
    std::size_t  key_len;

  private:
    Token_value get_key(std::istringstream& input_stream);

  public: //member functions
    CCP4base()
    {
      key_len = 4;
      string_value = "";
      number_value = 0;
      curr_tok = END ;
      //don't clear the arrays of pointers to member functions or else the
      //class will not build up the list as functions are added

      //clear the keywords so that instantiating a new input object in the scope
      //of another clears the keyword list
      keywords = "\n"; //flag for end of last keyword, line deletion
      annotate_keywords = true; //with the Warnings
      possible_keys.clear();
      possible_fns.clear();
      inputError.clear();
    }
    virtual ~CCP4base() { }

    void        Add_Key(std::string card);
    void        parseCCP4(std::string);
    Token_value skip_line(std::istringstream& input_stream);
    void        store(PhaserError);
//get functions
    Token_value get_token(std::istringstream& input_stream);
    bool        getBoolean(std::istringstream& input_stream);
    std::string getLine(std::istringstream& input_stream);
    std::string getFileName(std::istringstream& input_stream);
    std::string getAssignString(std::istringstream& input_stream);
    floatType   getAssignNumber(std::istringstream& input_stream);
    std::string getString(std::istringstream& input_stream);
    floatType   get1num(std::istringstream& input_stream);
    dvect3      get3nums(std::istringstream& input_stream);
    dmat6       get6nums(std::istringstream& input_stream);
    dmat33      get9nums(std::istringstream& input_stream);
//parsing functions
    bool        keyIs(std::string key);
    bool        tokenIs(std::istringstream& input_stream,int va_len, ...);
    bool        tokenIs(int va_len, ...);
    bool        compulsoryKey(int va_len, ...);
    bool        compulsoryKey(std::istringstream& input_stream,int va_len, ...);
    bool        optionalKey(std::istringstream& input_stream,int va_len, ...);
    bool        optionalKey(int va_len, ...);
    void        setKeyLength(int len) { key_len = len; }
    int         getKeyLength() { return key_len; }
//unparsing functions
    std::string card(bool b) { return std::string(b ? "ON":"OFF"); }
//check functions
    floatType   ispos(floatType x);
    unsigned    isposi(floatType x);
    floatType   iszeropos(floatType x);
    unsigned    iszeroposi(floatType x);
    floatType   isperc(floatType percent);
//output functions
    std::string Keywords();
    void        throw_errors();
    void        throw_advisory(Output&);
};

}//phaser

#endif
