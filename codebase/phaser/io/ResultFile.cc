//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultFile.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>

namespace phaser {

ResultFile::ResultFile(std::string filename)
{
  if (!filename.size())
    throw PhaserError(FILEOPEN,"NULL Filename");
  char* cfilename = const_cast<char*>(filename.c_str());
  if ((outFile = fopen(cfilename, "w")) == 0)
  {
    throw PhaserError(FILEOPEN,filename);
  }
}

void ResultFile::open(std::string filename)
{
  if (!filename.size())
    throw PhaserError(FILEOPEN,"NULL Filename");
  char* cfilename = const_cast<char*>(filename.c_str());
  if ((outFile = fopen(cfilename, "w")) == 0)
    throw PhaserError(FILEOPEN,filename);
}

} //end namespace phaser
