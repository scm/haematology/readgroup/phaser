//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <stdarg.h>
#include <math.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/io/END.h>
#include <phaser/main/Version.h>
#include <phaser/lib/jiffy.h>
#include <cassert>
#include <cctype>
#if BOOST_VERSION < 104600
#include <boost/filesystem/operations.hpp>
#elif BOOST_VERSION < 105000
#include <boost/filesystem/v3/operations.hpp>
#else
#include <boost/filesystem/operations.hpp>
#endif

namespace phaser {

//----
// master controller - every output goes through this function
//----
void Output::logOutput(outStream where, const std::string& text)
{
//set blank controls before output_strings has been extended, return if double blank
  if (where <= LEVEL &&
     (output_strings.size() ? output_strings.back().stream <= LEVEL : false))
  {
    if (text == "\n" && TOG_BLANK ) { return; }
    else if (text == "\n" && !TOG_BLANK ) { TOG_BLANK = true; }
    else { TOG_BLANK = false; }
  }

//set toggle before output_strings has been extended
  bool TOG_CCP4_SUMMARY = output_strings.size() ? output_strings.back().stream <= CCP4_SUMMARY_LEVEL : false;

//collect strings even if MUTE, to reconstruct output strings
  if (where <= LEVEL && text.size())
  {
    if (!output_strings.size() || output_strings.back().stream != where)
      output_strings.push_back(stream_string(where,text));
    else
      output_strings.back().string.append(text); //append last element
  }
//Every time something is output the run_time gets updated.
  IncrementRunTime();
//If mute don't check for kill (parallel section)
  if (MUTE) return;
  CheckForKill();

//This is the bit that does the output to the output stream IF THEY ARE OPEN
//if they aren't, the output is just stored in the strings with the code above
//until the streams are opened, and then the strings go to the streams

  //store output string in recontructable form
  if (isPackageCCP4() && text.size())
  {
    if (where <= LEVEL)
    {
      //logfile is sent directly to standard output
      if (!TOG_CCP4_SUMMARY && where <= CCP4_SUMMARY_LEVEL)
        std::cout << "<!--SUMMARY_BEGIN-->" << std::endl;
      else if (output_strings.size() && TOG_CCP4_SUMMARY && where > CCP4_SUMMARY_LEVEL)
        std::cout << "<!--SUMMARY_END-->" << std::endl;
      std::cout << text;
    }
  }
  else if (isPackagePhenix() && text.size())
  {
    if (where <= PHENIX_CALLBACK_LEVEL && use_phenix_callbacks)
      phenix_callback_->call_back("summary",text);
    if (where <= PHENIX_LOG_LEVEL) //short logfile output
      phenix_out_->write(text);
  }
}

void Output::logFlush()
{
  std::cout << std::flush;
  phenix_out_->flush();
}

void Output::setFiles(std::string fileroot,std::string filekill,floatType timekill)
{
  FILEROOT = fileroot;
  FILEKILL = filekill;
  TIMEKILL = timekill;
}

//constructor
Output::Output() : PhaserError()
{
//USE THE SCONS COMPILER FLAG TO CHANGE THE HEADERS "scons --ccp4"
//DO NOT EDIT THESE LINES
#ifdef build_phaser_for_ccp4
  PACKAGE_PHENIX = false;
#else
  PACKAGE_PHENIX = true;
#endif
  FILEROOT = "";
  FILEKILL = "";
  TIMEKILL = 0;
  start_clock = std::clock();
  run_time = elapsed_time = 0;
  virtual_memory = resident_set_size = 0;
  jumpnum = 1;
  output_strings.clear();
  phenix_out_ = boost::shared_ptr<phenix_out>(new phenix_out);
  use_phenix_callbacks = true;
  phenix_callback_ = boost::shared_ptr<phenix_callback>(new phenix_callback);
  LEVEL = outStreamStr(DEF_OUTP_LEVE).enumerator();
  CCP4_SUMMARY_LEVEL = outStreamStr(DEF_CCP4_SUMM_LEVE).enumerator();
  PHENIX_LOG_LEVEL = outStreamStr(DEF_PHEN_LOG_LEVE).enumerator();
  PHENIX_CALLBACK_LEVEL = outStreamStr(DEF_PHEN_CALL_LEVE).enumerator();
  MUTE = false;
  BOOKEND = true;
  progressCount = -999;
  progressBars = progressBarSize = progressChunk = 0;
}

void Output::setPackageCCP4()
{
  //The style of output for ccp4 is to markup the summary parts of the standard output
  //with <<!--SUMMARY_BEGIN-->> and to have a flag for verbose output that sends more output to standard out
  //This flag is set when the package is set
  PACKAGE_PHENIX = false;
}

void Output::setPackagePhenix()
{
  PACKAGE_PHENIX = true;
}

void Output::setIO(boost::shared_ptr<phenix_out> const& out)
{
  //AJM delete line below for phenix output to stdout through output object only (not cout)
  phenix_out_ = out;
}

void Output::setCallback(boost::shared_ptr<phenix_callback> const& callback)
{
  if (use_phenix_callbacks) phenix_callback_ = callback;
}

void Output::setOutput(const Output & init)
{
  setPhaserError(init);
  PACKAGE_PHENIX = init.PACKAGE_PHENIX;
  FILEROOT = init.FILEROOT;
  FILEKILL = init.FILEKILL;
  TIMEKILL = init.TIMEKILL;
  MUTE = init.MUTE;
  start_clock = init.start_clock;
  run_time = init.run_time;
  virtual_memory = init.virtual_memory;
  resident_set_size = init.resident_set_size;
  elapsed_time = init.elapsed_time;
  output_strings = init.output_strings;
  jumpnum = init.jumpnum;
  progressCount = init.progressCount;
  progressBars = init.progressBars;
  progressBarSize = init.progressBarSize;
  progressChunk = init.progressChunk;
  phenix_out_ = init.phenix_out_;
  phenix_callback_ = init.phenix_callback_;
  use_phenix_callbacks = init.use_phenix_callbacks;
  WARNINGS = init.WARNINGS;
  ADVISORY = init.ADVISORY;
  BOOKEND = init.BOOKEND;
  LOGGRAPH = init.LOGGRAPH;
  LEVEL = init.LEVEL;
  CCP4_SUMMARY_LEVEL = init.CCP4_SUMMARY_LEVEL;
  PHENIX_LOG_LEVEL = init.PHENIX_LOG_LEVEL;
  PHENIX_CALLBACK_LEVEL = init.PHENIX_CALLBACK_LEVEL;
}

Output::Output(const Output & init)
{ setOutput(init); }

const Output& Output::operator=(const Output& right)
{
  if (&right != this)   // check for self-assignment
    setOutput(right);
  return *this;
}

void Output::IncrementRunTime()
{
//This stops overflows in the number of ticks and prevents
//negative times being output
//Have to check that the integer std::clock() has increased,
//otherwise just get accumulation of small numbers
  std::clock_t now_clock = std::clock();
  if (now_clock > start_clock)
  {
    unsigned difference = static_cast<unsigned>(now_clock)
        - static_cast<unsigned>(start_clock);
    run_time += difference/double(CLOCKS_PER_SEC);
    elapsed_time += difference/double(CLOCKS_PER_SEC);
    start_clock = now_clock;
  }
//overflow - clock() has returned to start of long int
//have to miss out the accumulated time between now_clock and start_clock
  else if (now_clock < start_clock)
  {
    start_clock = now_clock;
  }
}

void Output::CheckForKill()
{
  if (ErrorType() == KILLTIME || ErrorType() == KILLFILE) return; //prevents recursion
  if (progressCount >= 0) return; //if in a parallel section (progress bar active)
//check for kill file
  if (boost::filesystem::exists(FILEKILL))
  {
    throw PhaserError(KILLFILE,"Job killed by detection of file \"" + std::string(FILEKILL) + "\"");
  }
  if (TIMEKILL > 0 && (run_time > TIMEKILL*60))
  {
    throw PhaserError(KILLTIME,"Job killed for elapsed time exceeding limit (" + dtos(TIMEKILL) + std::string(TIMEKILL == 1?" min)":" mins)"));
  }
}

void Output::logTime(outStream where, double seconds)
{
  int Days = seconds / 24 / 60 / 60;
  int Hrs = seconds / 60 / 60 - Days * 24;
  int Mins = seconds / 60 - Days * 24 * 60 - Hrs * 60;
  double Secs = seconds - Days * 24 * 60 * 60 - Hrs * 60 * 60 - Mins * 60;
  logTabPrintf(0, where, "CPU Time: %i days %i hrs %i mins %4.2f secs (%10.2f secs)\n", Days, Hrs, Mins, Secs, seconds);
}

void Output::startClock()
{ elapsed_time = 0; }

void Output::logElapsedTime(outStream where)
{
  int Days = elapsed_time / 24 / 60 / 60;
  int Hrs = elapsed_time / 60 / 60 - Days * 24;
  int Mins = elapsed_time / 60 - Days * 24 * 60 - Hrs * 60;
  double Secs = elapsed_time - Days * 24 * 60 * 60 - Hrs * 60 * 60 - Mins * 60;
  logTabPrintf(0, where, "Elapsed Time: %i days %i hrs %i mins %4.2f secs (%10.2f secs)\n", Days, Hrs, Mins, Secs, elapsed_time);
}

void Output::logGraph(outStream where,std::string title,std::vector<std::string> col,std::string data_labels,std::string data,bool scatter)
{
  std::string loggraph;
  loggraph += "$TABLE : " + title + ":\n";
  loggraph += scatter ? "$SCATTER \n" : "$GRAPHS \n";
  for (int c = 0; c < col.size(); c++) loggraph += ":" + col[c] + ": ";
  loggraph += "\n$$\n";
  loggraph +=  data_labels;
  loggraph += "\n$$ loggraph $$\n";
  loggraph += data;
  loggraph += "$$\n";
  if (isPackageCCP4()) logTab(0,where,loggraph);
  if (use_phenix_callbacks) phenix_callback_->loggraph(title,loggraph);
  LOGGRAPH.insert(loggraph);
  Loggraph();
}

void Output::phenixCallback(std::string message,std::string data)
{
  if (use_phenix_callbacks) phenix_callback_->call_back(message,data);
}

//================
//HELPER FUNCTIONS
//================
std::string Output::tab(int depth)
{ if (depth) return std::string(DEF_TAB*depth,' '); else return ""; }

std::string Output::formatMessage(const std::string text,int depth,bool add_return,bool add_amp,bool short_width)
{
  std::string split_text("");
  std::string amp(add_amp ? " &" : "");
  int tab_max_line_width(unsigned(short_width? DEF_HEADER_WIDTH:DEF_WIDTH)-tab(depth).size()-amp.size());
  int line_width(0);
  int start_line(0);
  int last_space(-1);
  for (int i = 0; i < text.size(); i++)
  {
    if ((std::isspace)(text[i]))
      last_space = i;
    if (i > 0 && text[i-1] == '\n')
      line_width = 1; //don't reset start_line
    else if (text[i] != '\n')
      line_width++;
    if (line_width > tab_max_line_width)
    {
      if (last_space == start_line-1)
      {
        bool strictly_max_line_width(false); //interrupt words over a whole line long with amp e.g. a long filename
        if (strictly_max_line_width)
        {
        split_text += tab(depth) + text.substr(start_line,tab_max_line_width) + amp + '\n';
        start_line += tab_max_line_width;
        last_space = i = start_line-1;
        line_width = 1;
        }
      }
      else if (i+1 < text.size() && text[i+1] == '\n') //look ahead for adding amp
      {
        int length = last_space - start_line;
        if (length)
          split_text +=  tab(depth) + text.substr(start_line,length);
        start_line = i = last_space + 1; //reset the start of the line and the text counter
        line_width = 1;
      }
      else
      {
        int length = last_space - start_line;
        if (length)
          split_text +=  tab(depth) + text.substr(start_line,length) + amp + '\n';
        start_line = i = last_space + 1; //reset the start of the line and the text counter
        line_width = 1;
      }
    }
  }
  int length = text.size() - start_line;
  split_text +=  tab(depth) + text.substr(start_line,length);
  if (add_return && split_text.size() > 0 && split_text[split_text.size()-1] != '\n')
    split_text += '\n';
  return split_text;
}

std::string Output::getLine(unsigned len,char c)
{
  std::string line;
  for (unsigned i = 0; i < std::min(len,unsigned(DEF_WIDTH)); i++) line += c;
  return line;
}

void Output::logUnderLine(outStream where, const std::string text)
{
  logBlank(where);
  logTab(1,where,text);
  logTab(1,where,getLine(text.size(),'-'));
}

void Output::logBlank(outStream where)
{
  logOutput(where,"\n");
  logFlush();
}

void Output::logKeywords(outStream where, std::string text)
{
  if (isPackageCCP4()) logTab(0,where,"$TEXT:Script: $$ Baubles Markup $$");
  int max_char_keywords(10000);
  if (text.size() >= max_char_keywords)
  {
    std::stringstream stream(text);
    std::string res;
    std::vector<std::string> sol;
    std::string endkey;
    std::vector<std::string> endkeys = END_KEYS();
    std::string solmsg = "#SOLUTION keywords removed for brevity\n";
    while (true)
    {
      std::string buffer;
      std::getline(stream,buffer);
      if (!stream.good())
        break;
      std::string ubuffer = stoup(buffer);
      if (!ubuffer.find("SOLU"))
        sol.push_back(buffer);
      else if (std::find(endkeys.begin(), endkeys.end(), stoup(buffer)) != endkeys.end())
        endkey = buffer + "\n";
      else
        res += buffer + "\n";
    }
    //now add the sol lines back on the end up to max length limit
    if (false)
      for (int i = 0; i < sol.size(); i++)
      if ((res.size() + sol[i].size() + 1 + endkey.size() + solmsg.size()) < max_char_keywords)
      res += sol[i] + "\n";
    if (sol.size())
      res += solmsg;
    res += endkey;
    text = res;
  }
  if (text.size() < max_char_keywords) logOutput(where,formatMessage(text,0,true,true));
  else logTab(1,where,"Keyword input too long to echo to output (>" + itos(max_char_keywords) + " characters)");
  logBlank(where);
  if (isPackageCCP4()) logTab(0,where,"$$");
}

void Output::logTab(unsigned t,outStream where, const std::string text,bool short_width)
{
  std::string formatted = formatMessage(text,t,true,false,short_width);
  //hack the string to introduce *** at the front
  if (where == PROCESS && formatted.size()>=2)
  { formatted[0] = '*'; formatted[1] = '*'; }
  logOutput(where,formatted);
}

void Output::logEllipsisStart(outStream where, const std::string text)
{ logOutput(where,formatMessage(text+"...",1,true)); logFlush();  }

void Output::logEllipsisEnd(outStream where)
{ logOutput(where,formatMessage("Done",2,true));  }

void Output::logWarning(outStream where,const std::string intext,bool no_warnings)
{
  if (no_warnings) return;
  int warn_size = WARNINGS.size();
  WARNINGS.insert(intext);
  if (WARNINGS.size() == warn_size) return;
  //else it is a new warning
  if (isPackageCCP4()) logTab(0,where,"<B><FONT COLOR=\"#FF8800\">");
  logTab(0,where,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
  logTab(0,where,"Warning: " + intext);
  logTab(0,where,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
  if (isPackageCCP4()) logTab(0,where,"</FONT></B>");
  logBlank(where);
  if (use_phenix_callbacks) phenix_callback_->warn(intext);
}

void Output::logAdvisory(outStream where,const std::string intext)
{
  int warn_size = ADVISORY.size();
  bool strong_advisory_represent(false);
  for (std::set<std::string>::iterator iter = ADVISORY.begin(); iter != ADVISORY.end(); iter++)
  { //test for the special case that eLLG "very difficult" is not already present
    std::string thiswarn = *iter;
    size_t pos = thiswarn.rfind("very ");
    if (pos != std::string::npos)
      thiswarn.erase(pos,5);
    if (intext == thiswarn) strong_advisory_represent = true;
  }
  if (!strong_advisory_represent) ADVISORY.insert(intext);
  if (ADVISORY.size() == warn_size) return;
  //else it is a new warning
  if (isPackageCCP4()) logTab(0,where,"<B><FONT COLOR=\"#0077FF\">"); //complementary to #FF8800
  logTab(0,where,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
  logTab(0,where,"Advisory: " + intext);
  logTab(0,where,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
  if (isPackageCCP4()) logTab(0,where,"</FONT></B>");
  logBlank(where);
  if (use_phenix_callbacks) phenix_callback_->warn(intext);
}

void Output::logProgressBarStart(outStream where,const std::string title, const int length)
{
  if (where != LEVEL) return;
  progressCount = 0;
  progressBars = 0;
  float progressBarSizeIdeal(unsigned(DEF_HEADER_WIDTH)-tab(1).size()-7); //7 = "|| DONE"
  float ratio = float(length)/std::max(1.f,progressBarSizeIdeal);
  float FloatChunk = ceil(ratio);
  progressChunk = std::max(static_cast<int>(1),static_cast<int>(FloatChunk));
  progressBarSize = std::max(static_cast<int>(1),static_cast<int>(floor(length/static_cast<float>(progressChunk))));
  logTab(1,where,title);
  logTabPrintf(1,DEBUG,"One progress bar represents %d steps of function\n",progressChunk);
  logTabPrintf(1,DEBUG,"There will be %d progress bars in total\n",progressBarSize);
  std::string percent_bar = "0%";
  for (int i = 0; i < progressBarSize; i++) percent_bar += " ";
  percent_bar +="100%";
  logTab(1,where,percent_bar);
  logOutput(where,tab(1) + "|");
  logFlush();
  if (use_phenix_callbacks) phenix_callback_->startProgressBar(title,length);
}

void Output::logProgressBarNext(outStream where)
{
  if (where != LEVEL) return;
  //Since alot of time is spent on progress bars we need to increment
  //runtime here each time
  IncrementRunTime();
  CheckForKill();

  progressCount++;
  if (progressCount >= progressChunk) {
    logOutput(where,"=");
    logFlush();
    progressCount -= progressChunk;
    progressBars++;
  }
  if (use_phenix_callbacks) phenix_callback_->incrementProgressBar();
}

void Output::logProgressBarPause(outStream where)
{
  if (where != LEVEL) return;
  logTab(1,where," \n");
}

void Output::logProgressBarAgain(outStream where)
{
  if (where != LEVEL) return;
  std::string percent_bar = "0%";
  for (int i = 0; i < progressBarSize; i++) percent_bar += " ";
  percent_bar +="100%";
  logTab(1,where,percent_bar);
  logOutput(where,tab(1) + "|");
  logFlush();
  for (int i = 0; i < progressBars; i++) logOutput(where,"=");
}

void Output::logProgressBarEnd(outStream where)
{
  if (where != LEVEL) return;
  logTab(0,where,"=| DONE");
  logBlank(where);
  logFlush();
  progressCount = -999; //set progress bar active flag to false
  if (use_phenix_callbacks) phenix_callback_->endProgressBar();
}

void Output::logTabPrintf(unsigned t,outStream where, const char* format, ...)
{
  static const std::size_t temp_size = 8192;
  char temp[temp_size];
  temp[temp_size-1] = '\0';
  va_list arglist;
  va_start(arglist,format);
  vsprintf(temp,format,arglist);
  va_end(arglist);
  assert(temp_size-1 >= 0 && temp[temp_size-1] == '\0');
  std::string formatted = formatMessage(temp,t,true);
  //hack the string to introduce *** at the front
  if (where == PROCESS && formatted.size()>=2)
  { formatted[0] = '*'; formatted[1] = '*'; }
  logOutput(where,formatted);
  logFlush();
}

void Output::logSectionHeader(outStream where,std::string header)
{
  std::string jumpstr;
  if (jumpnum == 1) {
    jumpstr = "Jump to: <a href=\"#jump2\">next section</a>\n";
  }
  else {
    jumpstr = "<a name=\"jump" + itos(jumpnum) + "\">"; //<a name="jumpn">
    jumpstr += "Jump to: <a href=\"#jump"+itos(jumpnum+1)+"\">next section</a>"; //Jump to: <a href="#jump(n+1)">next section</a>
    jumpstr += " <a href=\"#jump"+itos(jumpnum-1)+"\">last section</a>\n";
  }
  int len(0),max_len(0);
  for (int i = 0; i < header.size(); i++)
    if (header[i] != '\n') max_len = std::max(max_len,++len);
    else len = 0;
  logTab(0,where,getLine(max_len,'-'));
  logTab(0,where,header);
  logTab(0,where,getLine(max_len,'-'));
  logBlank(where);
  jumpnum++;
  logFlush();
}


void Output::logHeader(outStream where,std::string module)
{
  if (use_phenix_callbacks)
  {
    phenix_callback_->call_back("MODE",module);
    if (BOOKEND)
    {
      phenix_callback_->call_back("phaser_start","");
      logTab(0,DEBUG,">>>phenix start callback sent<<<");
    }
  }
  if (BOOKEND && isPackageCCP4())
  {
    time_t start_time;
    std::time(&start_time);
    //this bit works out what the header will look like from define parameters set
    //in Version.h
    //Header will take the form
    //#######################################################################");
    //#######################################################################
    //#######################################################################
    //### CCP4 PROGRAM SUITE: PROGRAM_NAME                PROGRAM_RELEASE ###
    //#######################################################################

    std::string progtag = "### CCP4 PROGRAM SUITE: " + std::string(PROGRAM_NAME);
    std::string progversion =  " " + version_number() + " ###";
    int max_len = unsigned(DEF_HEADER_WIDTH);
    int min_len = progtag.size() + progversion.size();
    int spacer_len = std::max(max_len - min_len,0);
    std::string  progheader = progtag + getLine(spacer_len,' ') + progversion;

    logTab(0,LOGFILE,"<pre>");
    logTab(0,LOGFILE,"<B><FONT COLOR=\"#FF0000\">");
    logTab(0,LOGFILE,"<html><!-- CCP4 HTML LOGFILE -->");
    logBlank(where);
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,progheader);
    logTab(0,where,getLine(progheader.size(),'#'));
    (getenv("USER") != NULL) ?
    logTab(0,where,"User:         " + std::string(getenv("USER"))):
    logTab(0,where,"User:         (unknown)");
    logTab(0,where,"Run time:     " + std::string(std::ctime(&start_time)));
    std::string svntxt = svn_revision();
    logTab(0,where,"Version:      " + version_number());
    if (getenv("OSTYPE") != NULL)
    logTab(0,where,"OS type:      " + std::string(getenv("OSTYPE")));
    logTab(0,where,"Release Date: " + version_date() + std::string(svntxt.size()?" (svn " + svntxt + ")":"")
      + std::string(git_rev().size() ? " (git " + git_rev() : "")
      + std::string(git_shorthash().size() ? ", " + git_shorthash() + "... )" : ""));
    logBlank(where);
    logTab(0,where,"If you use this software please cite:");
    logTab(0,where,"$TEXT:Reference1: $$ $$");
    logTab(0,where,"\"Phaser Crystallographic Software\"");
    logTab(0,where,"A.J. McCoy, R.W. Grosse-Kunstleve, P.D. Adams, M.D. Winn, L.C. Storoni & R.J. Read");
    logTab(0,where,"J. Appl. Cryst. (2007). 40, 658-674");
    logTab(0,where,"<a href=\"http://journals.iucr.org/j/issues/2007/04/00/he5368/he5368.pdf\">PDF</a>");
    logBlank(where);
    logTab(0,where,"$$");
    logTab(0,LOGFILE,"<!--END--></FONT></B>");
    logBlank(where);
  }
  else if (BOOKEND && isPackagePhenix())
  {
    time_t start_time;
    std::time(&start_time);
    //this bit works out what the header will look like from define parameters set
    //in Version.h
    //Header will take the form
    //#######################################################################
    //#######################################################################
    //#######################################################################
    //### PHENIX: PROGRAM_NAME                            PROGRAM_RELEASE ###
    //#######################################################################

    std::string progtag = "### PHENIX: " + std::string(PROGRAM_NAME);
    std::string progversion =  " " + version_number() + " ###";
    int max_len = unsigned(DEF_HEADER_WIDTH);
    int min_len = progtag.size() + progversion.size();
    int spacer_len = std::max(max_len - min_len,0);
    std::string  progheader = progtag + getLine(spacer_len,' ') + progversion;

    logTab(0,where,"<pre>");
    logTab(0,where,"<B><FONT COLOR=\"#FF0000\">");
    logTab(0,where,"<html><!-- PHENIX HTML LOGFILE -->");
    logBlank(where);
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,getLine(progheader.size(),'#'));
    logTab(0,where,progheader);
    logTab(0,where,getLine(progheader.size(),'#'));
    if (getenv("USER") != NULL)
    logTab(0,where,"User:         " + std::string(getenv("USER")));
    logTab(0,where,"Run time:     " + std::string(std::ctime(&start_time)));
    std::string svntxt = svn_revision();
    logTab(0,where,"Version:      " + version_number());
    if (getenv("OSTYPE") != NULL)
    logTab(0,where,"OS type:      " + std::string(getenv("OSTYPE")));
    logTab(0, where, "Release Date: " + version_date() + std::string(svntxt.size() ? " (svn " + svntxt + ")" : "")
      + std::string(git_rev().size() ? " (git " + git_rev() : "")
      + std::string(git_shorthash().size() ? ", " + git_shorthash() + "... )" : ""));
    logBlank(where);
    logTab(0,where,"If you use this software please cite:");
    logTab(0,where,"\"Phaser Crystallographic Software\"");
    logTab(0,where,"A.J. McCoy, R.W. Grosse-Kunstleve, P.D. Adams, M.D. Winn, L.C. Storoni & R.J. Read");
    logTab(0,where,"J. Appl. Cryst. (2007). 40, 658-674");
    logTab(0,where,"<a href=\"http://journals.iucr.org/j/issues/2007/04/00/he5368/he5368.pdf\">PDF</a>");
    logBlank(where);
    logTab(0,where,"<!--END--></FONT></B>");
    logBlank(where);
  }
//module
  where = SUMMARY;
  //hack the string for "Phaser Module" to introduce *** at the front
  //write to log with PROCESS call below
  std::string start_module = "*** Phaser Module: ";
  std::string end_module = version_number() + " ***";
  int spacer_width = unsigned(DEF_HEADER_WIDTH) - start_module.size() - end_module.size() - module.size();
  PHASER_ASSERT(spacer_width > 0);
  std::string spacer_module(spacer_width,' ');
  logTab(0,PROCESS,getLine(unsigned(DEF_HEADER_WIDTH),'*'));
  logTab(0,PROCESS,start_module + module + spacer_module + end_module);
  logTab(0,PROCESS,getLine(unsigned(DEF_HEADER_WIDTH),'*'));
  logBlank(where);
  jumpnum++;
  logFlush();
  BOOKEND = false; //always turn off after first Header
}

void Output::logTrailer(outStream where)
{
  time_t finish_time;
  std::time(&finish_time);
  IncrementRunTime();
  CheckForKill();
  logTime(SUMMARY,run_time);
  logTab(0,SUMMARY,"Finished: " + std::string(std::ctime(&finish_time)));
  logBlank(SUMMARY);
  logFlush();
}

void Output::logTerminate(outStream where,bool incoming_bookend)
{
  if (!incoming_bookend) return;
  if (Success()) setPhaserError(NULL_ERROR);
  BOOKEND = true;
  if (Success() && ADVISORY.size())
  {
    if (isPackageCCP4()) logTab(0,SUMMARY,"<B><FONT COLOR=\"##0077FF\">");
    logSectionHeader(SUMMARY,"ADVISORY");
    //group all the advisories together, not separate
    if (isPackageCCP4()) logTab(0,SUMMARY,"$TEXT:Advisory: $$ Baubles Markup $$");
    logBlank(SUMMARY);
    for (stringset::iterator iter = ADVISORY.begin(); iter != ADVISORY.end(); iter++)
    {
      logTab(0,SUMMARY,*iter);
      logBlank(SUMMARY);
    }
    if (isPackageCCP4()) logTab(0,SUMMARY,"$$");
    if (isPackageCCP4()) logTab(0,where,"</FONT></B>");
  }
  if (Success() && WARNINGS.size())
  {
    if (isPackageCCP4()) logTab(0,SUMMARY,"<B><FONT COLOR=\"#FF8800\">");
    logSectionHeader(SUMMARY,"WARNINGS");
    for (stringset::iterator iter = WARNINGS.begin(); iter != WARNINGS.end(); iter++)
    {
      if (isPackageCCP4()) logTab(0,SUMMARY,"$TEXT:Warning: $$ Baubles Markup $$");
      logTab(0,SUMMARY,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
      logTab(0,SUMMARY,"Warning: " + *iter);
      logTab(0,SUMMARY,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
      if (isPackageCCP4()) logTab(0,SUMMARY,"$$");
      logBlank(SUMMARY);
    }
    if (isPackageCCP4()) logTab(0,where,"</FONT></B>");
  }
  logBlank(SUMMARY);
  outStream where2 = Failure() ? SUMMARY : LOGFILE;
  std::string trailer = Failure() ? "EXIT STATUS: FAILURE" : "EXIT STATUS: SUCCESS";
  logBlank(where2);
  logTab(0,where2,getLine(trailer.size(),'-'));
  logTab(0,where2,trailer);
  logTab(0,where2,getLine(trailer.size(),'-'));
  logBlank(where2);
  logTrailer(where);
  if (isPackageCCP4() || isPackagePhenix())
  {
    logTab(0,LOGFILE,"</pre>");
    logTab(0,LOGFILE,"</html>");
  }
  if (use_phenix_callbacks)
  {
    phenix_callback_->call_back("phaser_end","");
    logTab(0,DEBUG,">>>phenix final callback sent<<<");
  }
  bool TOG_CCP4_SUMMARY = (output_strings.size() && output_strings.back().stream <= CCP4_SUMMARY_LEVEL);
  if (TOG_CCP4_SUMMARY && isPackageCCP4()) //close the SUMMARY BRACKETS
  {
    std::cout << "<!--SUMMARY_END-->" << std::endl;
  }
}

void Output::logError(outStream where,PhaserError err)
{
  setPhaserError(err);
  if (Failure())
  {
    logBlank(SUMMARY);
    if (err.Cards().size()) logKeywords(SUMMARY,err.Cards());
    if (isPackageCCP4()) logTab(0,SUMMARY,"<B><FONT COLOR=\"#FF8800\">");
    if (isPackageCCP4()) logTab(0,SUMMARY,"$TEXT:Warning: $$ Baubles Markup $$");
    logTab(0,SUMMARY,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
    logTab(0,SUMMARY,ErrorName() + " ERROR: " + ErrorMessage());
    logTab(0,SUMMARY,getLine(unsigned(DEF_HEADER_WIDTH),'-'));
    if (isPackageCCP4()) logTab(0,SUMMARY,"$$");
    phenix_callback_->warn(ErrorMessage());
    logTab(0,DEBUG,">>>phenix warning callback sent<<<");
  }
  logFlush();
}

std::string Output::reconstruct(outStream where)
{
  std::string tmp;
  for (int o = 0; o <  output_strings.size(); o++)
    if (output_strings[o].stream <= where) tmp += output_strings[o].string;
  return tmp;
}

std::string Output::svn_revision()
{
    std::string svn = svn_raw();
    size_t p = svn.find("R");
    std::string svntxt = (p != std::string::npos) ? svn.replace(p, 1, "r") : svn;
    size_t M = svntxt.find("M");
    svntxt = (M != std::string::npos) ? svn.replace(M, 1, "") : svn;
    return svntxt;
}

std::string Output::git_revision()
{
  std::string gittxt = "";
  if (git_rev().size())
  {
    gittxt = " (commits: " + git_rev() + ", SHA-1: "
      + git_shorthash() + "... , branch: " + git_branchname()
      + ")\ncommitted at: " + git_commitdatetime() + "\nTag name: "
      + git_tagname();
  }
  return gittxt;
}

void Output::logHessian(outStream where,TNT::Fortran_Matrix<double>& Hessian)
{
  int max_dim(13);
  logTab(1,where,"Hessian for refined parameters - Matrix");
  if (Hessian.num_rows() >= max_dim)
    logTab(1,where,"Hessian is too large to write to log: size = "+ itos(Hessian.num_rows()));
  for (int i=1; i<=Hessian.num_rows() && i < max_dim; i++)
  {
    std::string hess;
    std::string line;
    for (int j=1; j<=Hessian.num_cols() && j < max_dim; j++)
    {
      {
      std::stringstream s;
      s.setf(std::ios_base::showpos);
      s.setf(std::ios_base::scientific);
      s.precision(0); //number of decimal places
      s.width(1); //width
      s << Hessian(i,j);
      hess = s.str();
      }
      line += (Hessian(i,j)) ? hess + " " : " --0-- " ;
    }
    line = line.substr(0,line.length()-1);
    (Hessian.num_rows() >= max_dim) ?
      logTabPrintf(1,where,"%3d [%s etc...]\n",i,line.c_str()) :
      logTabPrintf(1,where,"%3d [%s]\n",i,line.c_str());
  }
  if (Hessian.num_rows() >= max_dim) logTabPrintf(0,where," etc...\n");
  logBlank(where);
  logTab(1,where,"Hessian for refined parameters - Diagonals");
  for (int i=1; i<=Hessian.num_rows(); i++)
  {
    //int j=i-1;
    logTabPrintf(1,where,"%3d [% 5.3e] \n", i,Hessian(i,i));
  }
  logBlank(where);
}

}//namespace phaser
