//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputCCA.h>

namespace phaser {

InputCCA::InputCCA(std::string cards)
{ parseCCP4(cards); }

InputCCA::InputCCA() {}
InputCCA::~InputCCA() {}

//performs the analysis between correlated keyword Input
void InputCCA::Analyse(Output& output)
{
  CELL::analyse();
  COMP::analyse();
  MUTE::analyse();
  RESO::analyse();
  ROOT::analyse();
  SPAC::analyse();
  TITL::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  if (HIRES < 0) HIRES = 1.199; //highest resolution bin from the paper
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputCCA::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += MUTE::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
