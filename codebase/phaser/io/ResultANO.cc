//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultANO.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

ResultANO::ResultANO() : Output(), DataA()
{
  MtzFile = "";
}

ResultANO::ResultANO(Output& output) : Output(output), DataA()
{
  MtzFile = "";
}

void ResultANO::writeMtz(std::string HKLIN)
{
  MtzHandler mtzOut((*this),(*this),MILLER,true,HKLIN);
  std::string REFID = INPUT_INTENSITIES ? I_ID : F_ID;
  INPUT_INTENSITIES ?
    mtzOut.addData(MILLER,selected,getLaboutI(),"J",getCorrectedI(),REFID):
    mtzOut.addData(MILLER,selected,getLaboutF(),"F",getCorrectedF(),REFID);
  INPUT_INTENSITIES ?
    mtzOut.addData(MILLER,selected,getLaboutSIGI(),"Q",getCorrectedSIGI(),REFID):
    mtzOut.addData(MILLER,selected,getLaboutSIGF(),"Q",getCorrectedSIGF(),REFID);
  MtzFile = Output::Fileroot() + ".mtz";
  mtzOut.writeMtz(MtzFile);
  if (mtzOut.not_subset())
    logAdvisory(SUMMARY,"Output reflections not a subset of " + HKLIN);
}

af_string ResultANO::getFilenames()
{
  af_string Filenames;
  if (MtzFile.size()) Filenames.push_back(MtzFile);
  return Filenames;
}

af_float ResultANO::getScaledCorrected(af::shared<miller::index<int> > ref_miller,af_float array)
{
  af_float corrected = getCorrection();
  af::shared<miller::index<int> > miller_cp = MILLER.deep_copy();
  af_float array_cp = array.deep_copy();
  double WilsonK = SIGMAN.WILSON_K;
  for (int rr = 0; rr < ref_miller.size(); rr++)
  {
    bool found_match(false);
    for (int r = 0; r < miller_cp.size(); r++)
      if (ref_miller[rr] == miller_cp[r])
      {
        array_cp[rr] *= corrected[r]*WilsonK;
        miller_cp.erase(miller_cp.begin()+r);
        corrected.erase(corrected.begin()+r);
        found_match = true;
        break;
      }
    if (!found_match)
    throw PhaserError(FATAL,"getScaledCorrected: missing correction factor for an input miller index");
  }
  return array_cp;
}

} //phaser
