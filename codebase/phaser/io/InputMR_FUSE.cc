//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_FUSE.h>

namespace phaser {

InputMR_FUSE::InputMR_FUSE(std::string cards) : InputMR_AUTO(cards)
{
  best_known_size = false;
  num_remaining = false;
  smaller_than_best = false;

  some_amalgamated = false; //altered by runMR_FUSE
  none_to_amalgamate = false; //altered by runMR_FUSE
  amalgamation_attempted = false; //altered by runMR_FUSE
}

InputMR_FUSE::InputMR_FUSE() {}
InputMR_FUSE::~InputMR_FUSE() {}

} //phaser
