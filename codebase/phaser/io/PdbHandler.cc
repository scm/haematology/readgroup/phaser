#include <phaser/io/PdbHandler.h>
#include <cctbx/uctbx.h>
#include <phaser/io/Errors.h>
#include <phaser/src/MapTraceMol.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace  phaser {

std::string ens_string_maker(
                            map_str_pdb& PDB, //the mtrace molecule
                            map_str_str& map_chains,
                            mr_ndim& NDIM,
                            bool renumber_atoms,
                            int& atom_number,
                            std::string SG_HALL,
                            af::double6 UNIT_CELL,
                            bool delete_overlaps,
                            float2D relative_wilsonB,
                            MapTrcPtr  tracemol,
                            float1D* OCCptr,
                            double DATACELL)
{
  std::string pdbstr;
  //create the molecules locally on the fly, do not store - files must be present in directory!
  std::string modlid = NDIM.MODLID;
  dmat33 outR = NDIM.R;
  cctbx::uctbx::unit_cell cctbxUC(UNIT_CELL);
  dvect3 outT = cctbxUC.orthogonalization_matrix()*NDIM.TRA;
  af::double6 SCALED_CELL = UNIT_CELL;
  double DATACELLinv = 1.0/DATACELL;

  if (PDB[modlid].map_format())
  {
    PHASER_ASSERT(tracemol->find(modlid) != tracemol->end());
    TraceMol* Trace = &tracemol->find(modlid)->second;
    for (unsigned a = 0; a < Trace->size(); a++)
    {
      pdb_record card = Trace->card(a);
      if (DATACELL != 1)
        card.X = DATACELLinv*card.X + PDB[modlid].PR.transpose()*((1-DATACELL)*PDB[modlid].PT);
      card.X = outR*card.X + outT;
      card.X = DATACELL*card.X;
      card.Chain = map_chains[card.Chain];
      pdbstr += card.Card();
    }
    PHASER_ASSERT(pdbstr.size());
    return pdbstr;
  }

  SpaceGroup sg(SG_HALL);
  cctbx::sgtbx::space_group cctbxsg  = sg.getCctbxSG();
  UnitCell uc(SCALED_CELL);
  cctbx::uctbx::unit_cell cctbxuc = uc.getCctbxUC();
  if (OCCptr != NULL) PHASER_ASSERT(PDB[modlid].allRms().size() == 1); //only one model
  {
    int NMODEL(1);
    for (int m = 0; m < PDB[modlid].nMols(); m++)
    {
      Molecule Coords = PDB[modlid].Coords(m);
      if (OCCptr != NULL) PHASER_ASSERT(OCCptr->size() == Coords.nAtoms()); //the trace molecule
      bool no_models(PDB[modlid].nMols() == 1 && Coords.nMols() == 1);
      for (int mm = 0; mm < Coords.nMols(); mm++)
      {
        //get the occupancies for this member of the ensemble
        float1D OCC = (OCCptr == NULL) ? float1D(0) : *OCCptr;
        if (!no_models) pdbstr += "MODEL " + itos(NMODEL,8,true) + "\n";
        overlap ol(NDIM,cctbxsg,cctbxuc,PDB[modlid].COORD_CENTRE);
        stringset delete_chains;
        if (ol.MULT != 1) delete_chains = Coords.get_overlap_chains(NDIM,sg,uc,mm);
        bool relative_wilsonB_is_set(m < relative_wilsonB.size() && mm < relative_wilsonB[m].size());
        for (unsigned a = 0; a < Coords.nAtoms(mm); a++)
        if (delete_chains.find(Coords.card(mm,a).Chain) == delete_chains.end())
        {
          pdb_record card = Coords.card(mm,a);
          if (DATACELL != 1)
            card.X = DATACELLinv*card.X + PDB[modlid].PR.transpose()*((1-DATACELL)*PDB[modlid].PT);
          card.X = outR*card.X + outT;
          card.X = DATACELL*card.X;
          card.B = (relative_wilsonB_is_set) ? std::max(2.0,card.B+relative_wilsonB[m][mm]+NDIM.BFAC) : card.B;
          //occupancy is refined occupancy if set
          if (OCCptr != NULL) //overwrite input
          {
           // card.O = (OCC[a] > 0.5) ? 1.0 : 0.0;
            card.O = OCC[a];
          }
          pdbstr += card.Card(atom_number++);
        }
        if (!no_models) pdbstr += "ENDMDL\n";
        NMODEL++;
      }
    }
  }
  return pdbstr;
}

std::string pdb_string_maker(
                            map_str_pdb& PDB, //the mtrace molecule
                            map_str_str& map_chains,
                            mr_ndim& NDIM,
                            bool renumber_atoms,
                            int& atom_number,
                            std::string SG_HALL,
                            af::double6 UNIT_CELL,
                            bool delete_overlaps,
                            float2D relative_wilsonB,
                            MapTrcPtr  tracemol,
                            bool chainocc, //write only non-zero occ atoms, for chain refinement
                            float1D* OCCptr,
                            double DATACELL)
{
  std::string pdbstr;
  //create the molecules locally on the fly, do not store - files must be present in directory!
  dmat33 outR = NDIM.R;
  cctbx::uctbx::unit_cell cctbxUC(UNIT_CELL);
  dvect3 outT = cctbxUC.orthogonalization_matrix()*NDIM.TRA;
  std::string modlid = NDIM.MODLID;
 // af::double6 SCALED_CELL = UNIT_CELL;
  double DATACELLinv = 1.0/DATACELL;

  if (PDB[modlid].map_format())
  {
    PHASER_ASSERT(tracemol->find(modlid) != tracemol->end());
    TraceMol* Trace = &tracemol->find(modlid)->second;
    for (unsigned a = 0; a < Trace->size(); a++)
    {
      pdb_record card = Trace->card(a);
      if (DATACELL != 1)
        card.X = DATACELLinv*card.X + PDB[modlid].PR.transpose()*((1-DATACELL)*PDB[modlid].PT);
      card.X = outR*card.X + outT;
      card.X = DATACELL*card.X;
      card.Chain = map_chains[card.Chain];
      pdbstr += card.Card();
    }
    PHASER_ASSERT(pdbstr.size());
    return pdbstr;
  }

  SpaceGroup sg(SG_HALL);
  cctbx::sgtbx::space_group cctbxsg  = sg.getCctbxSG();
  UnitCell uc(UNIT_CELL);
  cctbx::uctbx::unit_cell cctbxuc = uc.getCctbxUC();
  if (OCCptr != NULL) PHASER_ASSERT(PDB[modlid].allRms().size() == 1); //only one model
  //only write single highest sequence identity model
  {
    Molecule Coords = PDB[modlid].Coords();
    int m = PDB[modlid].MTRACE;
    int mm = Coords.mtrace;
    float1D OCC = (OCCptr == NULL) ? float1D(0) : *OCCptr;
    overlap ol(NDIM,cctbxsg,cctbxuc,PDB[modlid].COORD_CENTRE);
    stringset delete_chains;
    if (ol.MULT != 1) delete_chains = Coords.get_overlap_chains(NDIM,sg,uc,mm);
    bool relative_wilsonB_is_set(m < relative_wilsonB.size() && mm < relative_wilsonB[m].size());
    for (unsigned a = 0; a < Coords.nAtoms(); a++)
    if (delete_chains.find(Coords.card(a).Chain) == delete_chains.end())
    {
      pdb_record card = Coords.card(a);
      card.Chain = map_chains[card.Chain];
      if (DATACELL != 1)
        card.X = DATACELLinv*card.X + PDB[modlid].PR.transpose()*((1-DATACELL)*PDB[modlid].PT);
      card.X = outR*card.X + outT;
      card.X = DATACELL*card.X;
      card.B = (relative_wilsonB_is_set) ? std::max(2.0,card.B+relative_wilsonB[m][mm]+NDIM.BFAC) : card.B;
      //occupancy is refined occupancy if set
      if (OCCptr != NULL) //overwrite input
      {
        //card.O = (OCC[a] > 0.5) ? 1.0 : 0.0;
        card.O = OCC[a];
      }
      if (!chainocc || (chainocc && card.O))
      pdbstr += card.Card(atom_number++);
    }
  }
  return pdbstr;
}

std::string pak_string_maker(
                            map_str_pdb& PDB, //the mtrace molecule
                            mr_ndim& NDIM,
                            af::double6 UNIT_CELL,
                            MapTrcPtr tracemol,
                            std::string Chain
                            )
{
  std::string pdbstr;
  //create the molecules locally on the fly, do not store - files must be present in directory!
  dmat33 outR = NDIM.R;
  cctbx::uctbx::unit_cell cctbxUC(UNIT_CELL);
  dvect3 outT = cctbxUC.orthogonalization_matrix()*NDIM.TRA;
  std::string modlid = NDIM.MODLID;

  PHASER_ASSERT(tracemol->find(modlid) != tracemol->end());
  TraceMol* Trace = &tracemol->find(modlid)->second;
  for (unsigned a = 0; a < Trace->size(); a++)
  {
    pdb_record card = Trace->card(a);
    card.X = outR*card.X + outT;
    if (Chain.size()) card.Chain = Chain;
    pdbstr += card.Card();
  }
  PHASER_ASSERT(pdbstr.size());
  return pdbstr;
}

std::string gyre_string_maker(
                            map_str_pdb& PDB, //the mtrace molecule
                            mr_gyre& GYRE,
                            af::double6 UNIT_CELL
                            )
{
  std::string pdbstr;
  //create the molecules locally on the fly, do not store - files must be present in directory!
  cctbx::uctbx::unit_cell cctbxUC(UNIT_CELL);
  dvect3 outT = cctbxUC.orthogonalization_matrix()*GYRE.TRA;

  Molecule Coords = PDB[GYRE.MODLID].Coords();
  int MTRACE = PDB[GYRE.MODLID].MTRACE;
  for (unsigned a = 0; a < Coords.nAtoms(); a++)
  if (!PDB[GYRE.MODLID].ENSEMBLE[MTRACE].CHAIN.size() ||
      (PDB[GYRE.MODLID].ENSEMBLE[MTRACE].CHAIN.size() && Coords.card(a).Chain == PDB[GYRE.MODLID].ENSEMBLE[MTRACE].CHAIN))
  {
    pdb_record card = Coords.card(a);
    card.X = GYRE.R*card.X + outT;
    pdbstr += card.Card();
  }
  return pdbstr;
}

}//phaser
