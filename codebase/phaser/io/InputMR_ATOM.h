//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_ATOM__Class__
#define __InputMR_ATOM__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {


class InputMR_ATOM :
                  public ATOM
                 ,public CELL
                 ,public COMP
                 ,public CRYS
                 ,public ELLG
                 ,public ENSE
                 ,public HKLI
                 ,public HKLO
                 ,public JOBS
                 ,public KEYW
                 ,public LABI
                 ,public LLGC
                 ,public LLGM
                 ,public MACA
                 ,public MACM
                 ,public MACS
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public PACK
                 ,public PEAK
                 ,public PERM
                 ,public PURG
                 ,public RESC
                 ,public RESO
                 ,public RFAC
                 ,public ROOT
                 ,public SAMP
                 ,public SEAR
                 ,public SGAL
                 ,public SOLU
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public VERB
                 ,public WAVE
                 ,public XYZO
                 ,public ZSCO
                 ,public KILL
{
  public:
    InputMR_ATOM(std::string);
    InputMR_ATOM();
    ~InputMR_ATOM();
    void Analyse(Output&);
    std::string Cards();

    bool single_atom_mr()
    { return (CRYS_DATA.is_experiment_type("NAT") &&
              PDB.begin()->second.is_atom);
    }

};

} //phaser

#endif
