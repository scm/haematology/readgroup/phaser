//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultNMA__Class__
#define __ResultNMA__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/Molecule.h>
#include <phaser/lib/fmat.h>

namespace phaser {

class data_sceds;
class data_ddm;

class ResultNMA : public Output
{
  public:
    ResultNMA();
    ResultNMA(Output&);
    virtual ~ResultNMA() throw() {}

  private:
    af_string            PdbFiles;
    std::vector<dq_type> dqmodes;
    std::string          SolFile,MatFile;
    std::string          TITLE,MODLID;
    floatType            RMS;
    Molecule             molecule;
    int                  NDOM;
    bool                 use_trace;
    int1D                best_domain;
    std::vector<std::pair<int,int> > best_natoms_dom;

  public:
    TNT::Fortran_Matrix<float> U; //displacement vector

  public:
    void setDqModes(std::vector<dq_type> dq) { dqmodes = dq; }
    void setPdbFiles(af_string f)            { PdbFiles = f; }
    void setTitle(std::string title)         { TITLE = title; }
    void setModlid(std::string m)            { MODLID = m; }
    void setMolecule(Molecule  mol)          { molecule = mol; }
    void setRms(floatType rms)               { RMS = rms; }
    void setMatFile(std::string m)           { MatFile = m; }

    unsigned      getNumPdb()        { return PdbFiles.size(); }
    af_string     getPdbFiles()      { return PdbFiles; }
    std::string   getPdbFile(int i)  { PHASER_ASSERT(i < PdbFiles.size()); return PdbFiles[i]; }
    std::string   getSolFile()       { return SolFile; }
    std::string   getMatFile()       { return MatFile; }

    af_float      getDisplacements(int);
    af_int        getModes(int);

    af_string getFilenames();
    void writeScript();
    void writePdb(af_bool,bool);
    void calcDomains(af_bool,data_sceds,data_ddm);
    void writeDomains(bool);
};

}
#endif
