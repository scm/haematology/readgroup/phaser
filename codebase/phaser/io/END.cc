//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/END.h>

namespace phaser {

std::vector<std::string> END_KEYS()
{
  std::vector<std::string> end_keys;
// end_keys.push_back("END"); //to avoid ignoring END in pdb file input
  end_keys.push_back("QUIT");
  end_keys.push_back("STOP");
// end_keys.push_back("KILL"); //to avoid ignoring of KILL keyword
  end_keys.push_back("EXIT");
  end_keys.push_back("GO");
  end_keys.push_back("RUN");
  end_keys.push_back("START");
  return end_keys;
}

}
