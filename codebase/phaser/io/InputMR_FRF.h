//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_FRF__Class__
#define __InputMR_FRF__Class__
#include <phaser/keywords/keywords.h>
#include <phaser/io/Output.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_FRF :
                  public BINS
                 ,public BOXS
                 ,public CELL
                 ,public COMP
                 ,public ENSE
                 ,public FORM
                 ,public ELLG
                 ,public JOBS
                 ,public KEYW
                 ,public LABI
                 ,public MACA
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public PEAK
                 ,public PURG
                 ,public RESC
                 ,public RESH
                 ,public RESO
                 ,public RFAC
                 ,public ROOT
                 ,public ROTA
                 ,public SAMP
                 ,public SEAR
                 ,public SGAL
                 ,public SOLP
                 ,public SOLU
                 ,public SPAC
                 ,public TARG
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public VERB
                 ,public XYZO
                 ,public KILL
{
  public:
    InputMR_FRF(std::string);
    InputMR_FRF();
    ~InputMR_FRF();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
