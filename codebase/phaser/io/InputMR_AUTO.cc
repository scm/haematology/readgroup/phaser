//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_AUTO.h>

namespace phaser {

InputMR_AUTO::InputMR_AUTO(std::string cards)
{ parseCCP4(cards); }

InputMR_AUTO::InputMR_AUTO() { }
InputMR_AUTO::~InputMR_AUTO() { }

void InputMR_AUTO::Analyse(Output& output)
{
  BFAC::analyse();
  BINS::analyse();
  BOXS::analyse();
  CELL::analyse();
  COMP::analyse();
  ELLG::analyse();
  ENSE::analyse();
  FORM::analyse();
  HKLI::analyse();
  HKLO::analyse();
  INFO::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LABI::analyse();
  MACA::analyse();
  MACM::analyse();
  MACG::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OCCU::analyse();
  OUTL::analyse();
  PACK::analyse();
  PEAK::analyse();
  PERM::analyse();
  PURG::analyse();
  RESC::analyse();
  RESH::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  ROTA::analyse();
  SAMP::analyse();
  SEAR::analyse();
  SGAL::analyse();
  SOLP::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TARG::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  TRAN::analyse();
  VERB::analyse();
  XYZO::analyse();
  ZSCO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE
  //know what final spacegroup is going to be, sequence of overrides
  std::string FINAL_SG_HALL = REFLECTIONS.SG_HALL;
  if (REFLECTIONS.SGALT_BASE.size()) FINAL_SG_HALL = REFLECTIONS.SGALT_BASE;
  else if (SGALT_BASE.size()) FINAL_SG_HALL = SGALT_BASE;
  setupSGAL(FINAL_SG_HALL);

  for (int r = 0; r < SEARCH_MODLID.size(); r++)
  if (PDB.find(SEARCH_MODLID[r]) == PDB.end())
  throw PhaserError(INPUT,keywords,"No model for search ensemble " + SEARCH_MODLID[r]);

  for (int k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      if (PDB.find(MRSET[k].KNOWN[s].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for ensemble " + MRSET[k].KNOWN[s].MODLID);
    }

  //change the default
  if (PEAKS_ROT.get_cutoff() > PURGE_ROT.get_percent())
  {
    if (!PEAKS_ROT.is_default_cutoff() && PURGE_ROT.is_default_percent())
      PURGE_ROT.set_percent(PEAKS_ROT.get_cutoff());
    else if  (PEAKS_ROT.is_default_cutoff() && !PURGE_ROT.is_default_percent())
      PEAKS_ROT.set_cutoff(PURGE_ROT.get_percent());
    else if (!PEAKS_ROT.is_default_cutoff() && !PURGE_ROT.is_default_percent())
      throw PhaserError(INPUT,keywords,"Rotation Peaks percent cutoff > Purge percent cutoff");
  }
  //change the default
  if (PEAKS_TRA.get_cutoff() > PURGE_TRA.get_percent())
  {
    if (!PEAKS_TRA.is_default_cutoff() && PURGE_TRA.is_default_percent())
      PURGE_TRA.set_percent(PEAKS_TRA.get_cutoff());
    else if  (PEAKS_TRA.is_default_cutoff() && !PURGE_TRA.is_default_percent())
      PEAKS_TRA.set_cutoff(PURGE_TRA.get_percent());
    else if (!PEAKS_TRA.is_default_cutoff() && !PURGE_TRA.is_default_percent())
      throw PhaserError(INPUT,keywords,"Translation Peaks percent cutoff > Purge percent cutoff");
  }

  if (MRSET.size() && !MRSET.all_space_groups_set())
  {
    if (SGALT_HALL.size() == 0) MRSET.if_not_set_apply_space_group(SG_HALL);
    else if (SGALT_HALL.size() == 1) MRSET.if_not_set_apply_space_group(SGALT_HALL[0]);
    else MRSET.apply_space_group_expansion(SGALT_HALL); //fix
  }

  if (!AUTO_SEARCH.size() && MRSET.size())
  {
    throw PhaserError(INPUT,keywords,"Default search cannot be set with partial solution");
  }

  MACM_CHAINS.Override(false); //overwrite user input
}

std::string InputMR_AUTO::Cards()
{
  std::string cards;
  cards += BFAC::unparse();
  cards += BINS::unparse();
  cards += BOXS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ELLG::unparse();
  cards += ENSE::unparse();
  cards += FORM::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += INFO::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LABI::unparse();
  cards += MACA::unparse();
  cards += MACM::unparse();
  cards += MACG::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OCCU::unparse();
  cards += OUTL::unparse();
  cards += PACK::unparse();
  cards += PEAK::unparse();
  cards += PERM::unparse();
  cards += PURG::unparse();
  cards += RESC::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += ROTA::unparse();
  cards += SAMP::unparse();
  cards += SEAR::unparse();
  cards += SGAL::unparse();
  cards += SOLP::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TARG::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += TRAN::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += ZSCO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
