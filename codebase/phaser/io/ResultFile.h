//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultFile__Class__
#define __ResultFile__Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class ResultFile
{
  public:
    ResultFile() {}
    ResultFile(std::string f);
    void open(std::string f);
    FILE* outFile;
};

}
#endif
