//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputDFAC.h>

namespace phaser {

InputDFAC::InputDFAC(std::string cards) : InputNCS(cards) {}
InputDFAC::InputDFAC() {}
InputDFAC::~InputDFAC() {}

} //phaser
