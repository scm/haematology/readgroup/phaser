//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <cstring>
#include <phaser/io/MtzHandler.h>
#include <phaser/lib/round.h>
#include <phaser/lib/sg_prior.h>
#include <phaser/io/Errors.h>
#include <cctbx/miller/asu.h>

namespace phaser {

class miller_r
{
  public:
  miller_r(miller::index<int> MILLER_=miller::index<int>(0,0,0),int r_=0):MILLER(MILLER_),r(r_) { }
  miller::index<int> MILLER;
  int r;
  bool operator<(const miller_r &right) const
  { //fast_less_than, mtz sort order
    for(std::size_t i=0;i<3;i++) {
      if (MILLER[i] < right.MILLER[i]) return true;
      if (MILLER[i] > right.MILLER[i]) return false;
    }
    return false;
  }
};

MtzHandler::MtzHandler(SpaceGroup& s,
                       UnitCell& u,
                       af::shared<miller::index<int> > MILLER,
                       bool fromMR,
                       std::string HKLIN,
                       std::string XTALID,
                       std::string WAVEID) :
  SpaceGroup(s),UnitCell(u)
{
  NREFL = MILLER.size();
  hklin_subset = false;
  hklin = HKLIN.size();
  lookup.resize(NREFL);
  for (int r = 0; r < NREFL; r++)
    lookup[r] = r;
  if (HKLIN.size()) //there is a reference mtz file to merge output with
  {
    int read_reflections(1);
    mtz = CMtz::MtzGet(HKLIN.c_str(), read_reflections);
    if (mtz == 0) throw PhaserError(FILEOPEN,HKLIN);
    if (mtz->nref >= MILLER.size()) //MILLER can't be a subset, breaks lookup conditions
    {
    mtzH = CMtz::MtzColLookup(mtz,"H");
    if (mtzH == 0) throw PhaserError(FILEOPEN,"Column H could not be found for writing mtz file");

    mtzK = CMtz::MtzColLookup(mtz,"K");
    if (mtzK == 0) throw PhaserError(FILEOPEN,"Column K could not be found for writing mtz file");

    mtzL = CMtz::MtzColLookup(mtz,"L");
    if (mtzL == 0) throw PhaserError(FILEOPEN,"Column L could not be found for writing mtz file");
    //loop through the mtz structure finding the r corresponding to the mtzr
    //and loading the ColData[r] into the mtzCol->ref[mtzr]
    int nout(0);
    //use lists because erase is fast from lists
    std::vector<miller_r> SortMtzMiller;
    std::vector<miller_r> SortMiller;
    for (unsigned r = 0; r < MILLER.size(); r++)
      SortMiller.push_back(miller_r(MILLER[r],r));
    //sort on HKL so that reflections in same order as sorted MTZ
    cctbx::sgtbx::space_group_type SgInfo(SpaceGroup::getCctbxSG());
    cctbx::sgtbx::reciprocal_space::asu asu(SgInfo);
    for (unsigned mtzr = 0; mtzr < mtz->nref; mtzr++)
    {
      int H = phaser::round(mtzH->ref[mtzr]);
      int K = phaser::round(mtzK->ref[mtzr]);
      int L = phaser::round(mtzL->ref[mtzr]);
      miller::index<int> mtz_miller(H,K,L);
      if (!fromMR)
      {
        cctbx::miller::asym_index ai(SpaceGroup::getCctbxSG(), asu, mtz_miller);
        cctbx::miller::index_table_layout_adaptor ila = ai.one_column(false);
        mtz_miller = ila.h();
        mtzH->ref[mtzr] = mtz_miller[0];
        mtzK->ref[mtzr] = mtz_miller[1];
        mtzL->ref[mtzr] = mtz_miller[2];
        for (int ix = 0; ix < mtz->nxtal; ix++)  // not much point in optimising this
        for (int is = 0; is < mtz->xtal[ix]->nset; is++)
        for (int ic = 3; ic < mtz->xtal[ix]->set[is]->ncol; ic++)
        { //0,1,2 = HKL
          //change the ones corresponding to phases
          if (mtz->xtal[ix]->set[is]->col[ic]->type[0] == 'P')
            cctbx::miller::map_to_asu_policy<double>::eq(ila, mtz->xtal[ix]->set[is]->col[ic]->ref[mtzr], true);
          else if (mtz->xtal[ix]->set[is]->col[ic]->type[0] == 'A')
          {
            PHASER_ASSERT(ic+3 < mtz->xtal[ix]->set[is]->ncol);
            cctbx::hendrickson_lattman<double> hl(mtz->xtal[ix]->set[is]->col[ic+0]->ref[mtzr],
                                                  mtz->xtal[ix]->set[is]->col[ic+1]->ref[mtzr],
                                                  mtz->xtal[ix]->set[is]->col[ic+2]->ref[mtzr],
                                                  mtz->xtal[ix]->set[is]->col[ic+3]->ref[mtzr]);
            cctbx::miller::map_to_asu_policy<cctbx::miller::data_classes::hendrickson_lattman_type>::eq(ila, hl, false);
            mtz->xtal[ix]->set[is]->col[ic+0]->ref[mtzr] = hl.a();
            mtz->xtal[ix]->set[is]->col[ic+1]->ref[mtzr] = hl.b();
            mtz->xtal[ix]->set[is]->col[ic+2]->ref[mtzr] = hl.c();
            mtz->xtal[ix]->set[is]->col[ic+3]->ref[mtzr] = hl.d();
            ic += 4;
          }
          //else do nothing
        }
      }
      SortMtzMiller.push_back(miller_r(mtz_miller,mtzr));
    }
    std::sort(SortMiller.begin(),SortMiller.end());
    std::sort(SortMtzMiller.begin(),SortMtzMiller.end());
    int last_rcount(0);
    for (int mtzr = 0; mtzr < SortMtzMiller.size(); mtzr++)
    {
      for (int r = last_rcount; r < SortMiller.size(); r++)
      {
        if (SortMtzMiller[mtzr].MILLER == SortMiller[r].MILLER)
        {
          lookup[SortMiller[r].r] = SortMtzMiller[mtzr].r;
          last_rcount++; //one more for next
          nout++;
          break;
        }
      }
    }
    hklin_subset = (nout == NREFL);
    }
  }
  if (!hklin_subset)
  {
    int nxtal(1),nset[1];
    nset[0] = 1;
    mtz = CMtz::MtzMalloc(nxtal,nset);
    //initialize header
    mtz->nref = NREFL;
    mtz->nxtal = nxtal;
    mtzH  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"H","H");
    mtzK  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"K","H");
    mtzL  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"L","H");
    for (int r = 0; r < MILLER.size(); r++)
    {
      mtzH->ref[r] = MILLER[r][0];
      mtzK->ref[r] = MILLER[r][1];
      mtzL->ref[r] = MILLER[r][2];
    }
    PHASER_ASSERT(mtzH != 0 && mtzK != 0 && mtzL != 0);
    //point group will not change with phaser
    std::strcpy(mtz->mtzsymm.pgname,SpaceGroup::pgname().c_str());
    CMtz::MTZCOL *colsort[5];
    colsort[0] = mtzH;
    colsort[1] = mtzK;
    colsort[2] = mtzL;
    colsort[3] = NULL;
    colsort[4] = NULL;
    CMtz::MtzSetSortOrder(mtz,colsort);
    std::strcpy(mtz->xtal[0]->pname,"project");
    if (XTALID.size()) std::strcpy(mtz->xtal[0]->xname,XTALID.c_str());
    if (WAVEID.size()) std::strcpy(mtz->xtal[0]->set[0]->dname,WAVEID.c_str());
  }

  // SPACEGROUP
  mtz->mtzsymm.nsym = SpaceGroup::NSYMM;
  mtz->mtzsymm.nsymp = SpaceGroup::NSYMP;
  mtz->mtzsymm.spcgrp = SpaceGroup::spcgrpnumber();
  mtz->mtzsymm.symtyp = SpaceGroup::spcgrpcentring();
  std::strcpy(mtz->mtzsymm.spcgrpname,SpaceGroup::spcgrpname().c_str());
  for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
    for (int i = 0; i < 3; i++)
    {
      mtz->mtzsymm.sym[isym][i][3] = SpaceGroup::Trasym(isym)[i];
      for (int j = 0; j < 3; j++)
        mtz->mtzsymm.sym[isym][j][i] = SpaceGroup::Rotsym(isym)(i,j);
    }
  //UNITCELL set for individual columns
}

void MtzHandler::addData(af::shared<miller::index<int> > MILLER,af_bool selected,std::string ColName,std::string ColType,float1D ColData,std::string LABIN)
{
  PHASER_ASSERT(ColData.size() == NREFL); //this is the only check possible - order must be same
  af_float v(NREFL); //deep copy required
  for (int r = 0; r < NREFL; r++) v[r] = ColData[r];
  addCol(MILLER,selected,ColName,ColType,v,LABIN);
}

void MtzHandler::addData(af::shared<miller::index<int> > MILLER,af_bool selected,std::string ColName,std::string ColType,af_float ColData,std::string LABIN)
{
  PHASER_ASSERT(ColData.size() == NREFL); //this is the only check possible - order must be same
  addCol(MILLER,selected,ColName,ColType,ColData,LABIN);
}

void MtzHandler::addData(af::shared<miller::index<int> > MILLER,af_bool selected,std::string ColName,std::string ColType,af::shared<cctbx::hendrickson_lattman<double> > ColData,std::string LABIN)
{
  PHASER_ASSERT(ColData.size() == NREFL); //this is the only check possible - order must be same
  std::string hla(ColName),hlb(ColName),hlc(ColName),hld(ColName);
  for (int i = 0; i < ColName.length(); i++)
  {
    if (hla[i] == '#') hla.replace(i,1,"A");
    if (hlb[i] == '#') hlb.replace(i,1,"B");
    if (hlc[i] == '#') hlc.replace(i,1,"C");
    if (hld[i] == '#') hld.replace(i,1,"D");
  }
  af_float tmp(NREFL);
  for (unsigned r = 0; r < NREFL; r++) tmp[r] = ColData[r].a();
  addCol(MILLER,selected,hla,ColType,tmp,LABIN);
  for (unsigned r = 0; r < NREFL; r++) tmp[r] = ColData[r].b();
  addCol(MILLER,selected,hlb,ColType,tmp,LABIN);
  for (unsigned r = 0; r < NREFL; r++) tmp[r] = ColData[r].c();
  addCol(MILLER,selected,hlc,ColType,tmp,LABIN);
  for (unsigned r = 0; r < NREFL; r++) tmp[r] = ColData[r].d();
  addCol(MILLER,selected,hld,ColType,tmp,LABIN);
}

void MtzHandler::addCol(af::shared<miller::index<int> > MILLER,af_bool selected,std::string ColName,std::string ColType,af_float ColData,std::string LABIN)
{
  //The order of the data in ColData must be in the same order as the initializing miller array
  //There is no way of checking this.
  PHASER_ASSERT(lookup.size() == ColData.size());
  int mtzx(0),mtzs(0);
  if (hklin_subset)
  {
    char *path1, path2[200];
    // complete the right-justified path
    CMtz::MtzRJustPath( path2, LABIN.c_str(), 3 );
    // now find the matching column
    for (int ix = 0; ix < mtz->nxtal; ix++)  // not much point in optimising this
      for (int is = 0; is < mtz->xtal[ix]->nset; is++)
        for (int ic = 0; ic < mtz->xtal[ix]->set[is]->ncol; ic++)
        {
          path1 = CMtz::MtzColPath(mtz, mtz->xtal[ix]->set[is]->col[ic]);
          if ( CMtz::MtzPathMatch( path1, path2 ) )
            mtzx = ix, mtzs = is;
          free(path1);
        }
  }

  // UNITCELL
  for (int a = 0; a < 6; a++) mtz->xtal[mtzx]->cell[a] = UnitCell::Param(a);

  // NEW COLUMNS
  mtzCol = 0;
  if (hklin_subset) //if a column by same name exists, overwrite it
    mtzCol = CMtz::MtzColLookup(mtz,ColName.c_str()); //returns 0 if column does not exist
  if (mtzCol == 0) //if above returns 0 or if there is no HKLIN
    mtzCol = CMtz::MtzAddColumn(mtz,mtz->xtal[mtzx]->set[mtzs],ColName.c_str(),ColType.c_str());
  PHASER_ASSERT(mtzCol != 0);

  for (unsigned mtzr = 0; mtzr < mtz->nref; mtzr++)
    mtzCol->ref[mtzr] = CCP4::ccp4_nan().f;
  // values not set remain nan

  if (hklin_subset)
  {
    for (int r = 0; r < NREFL; r++)
      if (selected[r])
      {
        PHASER_ASSERT(lookup[r] < mtz->nref);
        mtzCol->ref[lookup[r]] = ColData[r];
      }
  }
  else
  {
    //loop through the reflections adding each ColData[r] in same order
    PHASER_ASSERT(MILLER.size() == ColData.size());
    for (int r = 0; r < ColData.size(); r++)
      if (selected[r])
        mtzCol->ref[r] = ColData[r];
  }
}

void MtzHandler::setWave(floatType wavelength,std::string LABIN)
{
  //The order of the data in ColData must be in the same order as the initializing miller array
  //There is no way of checking this.
  int mtzx(0),mtzs(0);
  if (hklin_subset)
  {
    char *path1, path2[200];
    // complete the right-justified path
    CMtz::MtzRJustPath( path2, LABIN.c_str(), 3 );
    // now find the matching column
    for (int ix = 0; ix < mtz->nxtal; ix++)  // not much point in optimising this
      for (int is = 0; is < mtz->xtal[ix]->nset; is++)
        for (int ic = 0; ic < mtz->xtal[ix]->set[is]->ncol; ic++)
        {
          path1 = CMtz::MtzColPath(mtz, mtz->xtal[ix]->set[is]->col[ic]);
          if ( CMtz::MtzPathMatch( path1, path2 ) )
            mtzx = ix, mtzs = is;
          free(path1);
        }
  }
  mtz->xtal[mtzx]->set[mtzs]->wavelength = wavelength;
}


void MtzHandler::writeMtz(std::string HKLOUT)
{
  PHASER_ASSERT(HKLOUT != "");
  CMtz::MtzPut(mtz, const_cast<char*>(HKLOUT.c_str()));
  CMtz::MtzFree(mtz);
}

}//phaser
