//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputEP_DAT__Class__
#define __InputEP_DAT__Class__
#include <phaser/keywords/keywords.h>

//also add CCP4base "analyse" functions to list in .cc file

namespace phaser {

class InputEP_DAT :
                  public CELL
                 ,public CRYS
                 ,public HKLI
                 ,public JOBS
                 ,public MUTE
                 ,public RESO
                 ,public ROOT
                 ,public SPAC
                 ,public TITL
                 ,public VERB
                 ,public KILL
{
  public:
    InputEP_DAT(std::string);
    InputEP_DAT();
    ~InputEP_DAT();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
