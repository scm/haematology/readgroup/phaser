//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PdbChains__
#define __PdbChains__
#include <phaser/main/Phaser.h>

namespace phaser {

class pdb_chains_2chainid
{
  private:
    std::vector<std::string> allowed_chains;
    std::string error_message;
    bool failure;
  public:
  pdb_chains_2chainid(int num_id)
  {
    std::string char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    std::vector<std::string> low;
    for (int i = 0; i < char_set.size(); i++)
      low.push_back(" " + char_set.substr(i,1));
    char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::vector<std::string> intuitive = low;
    for (int i = 0; i < char_set.size(); i++)
      for (int j = 0; j < char_set.size(); j++)
        intuitive.push_back(char_set.substr(i,1) + char_set.substr(j,1));
    char_set="abcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < char_set.size(); i++)
      for (int j = 0; j < char_set.size(); j++)
        intuitive.push_back(char_set.substr(i,1) + char_set.substr(j,1));
    char_set="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    std::vector<std::string> more;
    for (int i = 0; i < char_set.size(); i++)
      for (int j = 0; j < char_set.size(); j++)
        more.push_back(char_set.substr(i,1) + char_set.substr(j,1));
    char_set="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!\"#$%&'()*+,-./:;<=>?@.substr(i,1)^_`{|}~";
    std::vector<std::string> mostest;
    for (int i = 0; i < char_set.size(); i++)
      for (int j = 0; j < char_set.size(); j++)
        mostest.push_back(char_set.substr(i,1) + char_set.substr(j,1));

    if (num_id <= low.size())
      allowed_chains = low;
    else if (num_id <= intuitive.size())
      allowed_chains = intuitive;
    else if (num_id <= more.size())
      allowed_chains = more;
    else if (num_id <= mostest.size())
      allowed_chains = mostest;

    error_message = "Unique 2-characters (ascii set) for chain ID's exhausted";
    failure = false;
  }
  std::string message() { return error_message; }
  bool allocate_failed()
  {
    return failure;
  }
  std::string allocate(std::string test)
  {
    //if the test chain id is in the allowed set, use it, otherwise return the next available chainid
    if (allowed_chains.size())
    {
      bool found(false);
      std::vector<std::string>::iterator testptr = allowed_chains.begin();
      for (int i = 0; i < allowed_chains.size(); i++, testptr++)
      {
        if (allowed_chains[i] == test)
        {
          found = true;
          break;
        }
      }
      std::string next_chainid = found ? test : allowed_chains[0];
      found ? allowed_chains.erase(testptr) : allowed_chains.erase(allowed_chains.begin()); //delete this possibility from the allowed list
      return next_chainid;
    }
    else
    {
      failure = true;
      return "  ";
    }
  }
};

} //phaser
#endif
