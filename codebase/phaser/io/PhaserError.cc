//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/PhaserError.h>

namespace phaser {

std::string PhaserError::ErrorName() const
{
  if (type == NULL_ERROR) return "NO";
  else if (type == SYNTAX) return "SYNTAX";
  else if (type == INPUT) return "INPUT";
  else if (type == FILEOPEN) return "FILE OPENING";
  else if (type == FATAL) return "FATAL RUNTIME";
  else if (type == KILLFILE) return "KILL-FILE DETECTED";
  else if (type == KILLTIME) return "KILL-TIME ELAPSED";
  else if (type == MEMORY) return "OUT OF MEMORY";
  else if (type == UNHANDLED) return "UNHANDLED";
  else if (type == UNKNOWN) return "UNKNOWN";
  return "";
}

} //phaser
