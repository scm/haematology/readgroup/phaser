//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultMR_DAT__Class__
#define __ResultMR_DAT__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <phaser/include/data_refl.h>
#include <scitbx/array_family/tiny_types.h>

namespace phaser {

class ResultMR_DAT : public Output, public SpaceGroup, public UnitCell
{
  public:
    ResultMR_DAT();
    ResultMR_DAT(Output&);
    virtual ~ResultMR_DAT() throw() {}

    data_refl   DATA_REFL;

  private:
    af_string   filenames;

  public:
    void storeData(data_refl DATA)
    {
      DATA_REFL = DATA; DATA_REFL.set_default_id("");
      setSpaceGroup(DATA_REFL.SG_HALL);
      setUnitCell(DATA_REFL.UNIT_CELL);
    }

    void writeMtz(bool);

    af::shared<miller::index<int> >   getMiller()    { return DATA_REFL.MILLER; }
    af_float    getFobs()      { return DATA_REFL.F; }
    af_float    getSigFobs()   { return DATA_REFL.SIGF; }
    af_float    getFMAP()      { return DATA_REFL.FTFMAP.FMAP; }
    af_float    getPHMAP()     { return DATA_REFL.FTFMAP.PHMAP; }
    af_float    getFOM()       { return DATA_REFL.FTFMAP.FOM; }
    af_float    getF()         { return DATA_REFL.F; }
    af_float    getSIGF()      { return DATA_REFL.SIGF; }
    af_float    getIobs()      { return DATA_REFL.I; }
    af_float    getSigIobs()   { return DATA_REFL.SIGI; }
    af_float    getI()         { return DATA_REFL.I; }
    af_float    getSIGI()      { return DATA_REFL.SIGI; }
    data_refl   getDATA()      { return DATA_REFL; }
    std::string getLabinF()    { return DATA_REFL.F_ID; }
    std::string getLabinSIGF() { return DATA_REFL.SIGF_ID; }
    std::string getLabinFMAP() { return DATA_REFL.FTFMAP.FMAP_ID; }
    std::string getLabinPHMAP(){ return DATA_REFL.FTFMAP.PHMAP_ID; }
    std::string getLabinFOM()  { return DATA_REFL.FTFMAP.FOM_ID; }
    floatType   getMtzHiRes()  { return cctbx::uctbx::d_star_sq_as_d(cctbx::uctbx::unit_cell(DATA_REFL.UNIT_CELL).min_max_d_star_sq(DATA_REFL.MILLER.const_ref())[1]); }
    floatType   getMtzLoRes()  { return cctbx::uctbx::d_star_sq_as_d(cctbx::uctbx::unit_cell(DATA_REFL.UNIT_CELL).min_max_d_star_sq(DATA_REFL.MILLER.const_ref())[0]); }
    af_string   getFilenames() { return filenames; }
};

}
#endif
