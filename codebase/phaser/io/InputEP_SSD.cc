//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputEP_SSD.h>

namespace phaser {

InputEP_SSD::InputEP_SSD(std::string cards)
{ parseCCP4(cards); }

InputEP_SSD::InputEP_SSD() {}
InputEP_SSD::~InputEP_SSD() {}

//performs the analysis between correlated keyword Input
void InputEP_SSD::Analyse(Output& output)
{
  ATOM::analyse();
  BFAC::analyse();
  BINS::analyse();
  CELL::analyse();
  CLUS::analyse();
  COMP::analyse();
  CRYS::analyse();
  FFTS::analyse();
  FIND::analyse();
  HAND::analyse();
  HKLI::analyse();
  HKLO::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LLGC::analyse();
  LLGM::analyse();
  MACA::analyse();
  MACJ::analyse();
  MACS::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  PART::analyse();
  RESH::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  SAMP::analyse();
  SCAT::analyse();
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  VARS::analyse();
  VERB::analyse();
  WAVE::analyse();
  XYZO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);
}

std::string InputEP_SSD::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += ATOM::unparse();
  cards += BFAC::unparse();
  cards += BINS::unparse();
  cards += CELL::unparse();
  cards += CLUS::unparse();
  cards += COMP::unparse();
  cards += CRYS::unparse();
  cards += FFTS::unparse();
  cards += FIND::unparse();
  cards += HAND::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LLGC::unparse();
  cards += LLGM::unparse();
  cards += MACA::unparse();
  cards += MACJ::unparse();
  cards += MACS::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += PART::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += SAMP::unparse();
  cards += SCAT::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += VARS::unparse();
  cards += VERB::unparse();
  cards += WAVE::unparse();
  cards += XYZO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
