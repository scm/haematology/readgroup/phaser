//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_PAK.h>

namespace phaser {

InputMR_PAK::InputMR_PAK(std::string cards)
{
  NO_WARNINGS = false;
  parseCCP4(cards);
}

InputMR_PAK::InputMR_PAK() { NO_WARNINGS = false; }
InputMR_PAK::~InputMR_PAK() { NO_WARNINGS = false; }

//performs the analysis between correlated keyword Input
void InputMR_PAK::Analyse(Output& output)
{
  CELL::analyse();
  ENSE::analyse();
  JOBS::analyse();
  KEYW::analyse();
  MUTE::analyse();
  PACK::analyse();
  ROOT::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TITL::analyse();
  TOPF::analyse();
  VERB::analyse();
  XYZO::analyse();
  ZSCO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  if (!MRSET.size())
  throw PhaserError(INPUT,keywords,"No solution sets");

  for (unsigned k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      std::string m = MRSET[k].KNOWN[s].MODLID;
      if (PDB.find(m) == PDB.end())
      throw PhaserError(INPUT,keywords,"No input for ensemble " + m);
    }

  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputMR_PAK::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += CELL::unparse();
  cards += ENSE::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += MUTE::unparse();
  cards += PACK::unparse();
  cards += ROOT::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TOPF::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += ZSCO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
