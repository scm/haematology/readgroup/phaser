//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultCCA__Class__
#define __ResultCCA__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

class ResultCCA : public Output, public SpaceGroup, public UnitCell
{
  public:
    ResultCCA();
    ResultCCA(Output&);
    virtual ~ResultCCA() throw() {}

  private:
    af_float    VM,probVM; //the VM of the most probable assembly, and it's probability
    af_int      Z; //the most probable Z for the input assembly
    floatType   optimalMW,optimalVM,assemblyMW;
    bool        fitError;

  public:
    void        setZ(floatType m,af_int z,af_float v,af_float p) { assemblyMW = m; Z = z; VM = v; probVM = p; }
    void        setOptimal(floatType mw,floatType vm)            { optimalMW = mw; optimalVM = vm; }

    int         getNum()        { return probVM.size(); }
    af_float    getProb()       { return probVM; }
    af_float    getVM()         { return VM; }
    af_int      getZ()          { return Z; }
    floatType   getAssemblyMW() { return assemblyMW; }
    floatType   getBestProb()   { return getNum() ? probVM[getBest()] : 0; }
    floatType   getBestVM()     { return getNum() ? VM[getBest()] : 0; }
    int         getBestZ()      { return getNum() ? Z[getBest()] : 0; }
    floatType   getOptimalMW()  { return optimalMW; }
    floatType   getOptimalVM()  { return optimalVM; }

    int         getBest();
    af_string   getFilenames();

    void        setFitError() { fitError = true; }
    bool        getFitError() { return fitError; }
};

}
#endif
