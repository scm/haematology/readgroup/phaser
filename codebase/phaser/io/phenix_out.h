//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PhenixOutput__Class__
#define __PhenixOutput__Class__
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <boost/shared_ptr.hpp>

namespace phaser {

struct phenix_out {
  virtual ~phenix_out();
  virtual void write(const std::string& text);
  virtual void flush();
};

struct phenix_callback {
  virtual ~phenix_callback();
  virtual void startProgressBar(const std::string& label, int size);
  virtual void incrementProgressBar();
  virtual void endProgressBar();
  virtual void warn(const std::string& message);
  virtual void loggraph(const std::string& title, const std::string& data);
  virtual void call_back(const std::string& message, const std::string& data);
};

} //phaser

#endif
