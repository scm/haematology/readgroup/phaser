//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultDFAC.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

ResultDFAC::ResultDFAC() : Output(), DataA()
{
  MtzFile = "";
}

ResultDFAC::ResultDFAC(Output& output) : Output(output), DataA()
{
  MtzFile = "";
}

void ResultDFAC::writeMtz(std::string HKLIN)
{
  MtzHandler mtzOut((*this),(*this),MILLER,true,HKLIN);
  std::string REFID = INPUT_INTENSITIES ? I_ID : F_ID;
  mtzOut.addData(MILLER,selected,getLaboutF(),"F",getCorrectedF(),REFID);
  mtzOut.addData(MILLER,selected,getLaboutSIGF(),"Q",getCorrectedSIGF(),REFID);
  if (DFAC.size()) mtzOut.addData(MILLER,selected,"Dfactor","R",DFAC,REFID);
  MtzFile = Output::Fileroot() + ".mtz";
  mtzOut.writeMtz(MtzFile);
  if (mtzOut.not_subset())
    logAdvisory(SUMMARY,"Output reflections not a subset of " + HKLIN);
}

af_string ResultDFAC::getFilenames()
{
  af_string Filenames;
  if (MtzFile.size()) Filenames.push_back(MtzFile);
  return Filenames;
}


} //phaser
