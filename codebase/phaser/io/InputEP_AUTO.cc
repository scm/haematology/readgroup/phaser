//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputEP_AUTO.h>
#include <phaser/include/space_group_name.h>
#include <cctbx/eltbx/sasaki.h>
#include <cmtzlib.h>
#include <mtzdata.h>
#include <ccp4_errno.h>
#include <phaser/lib/safe_mtz.h>

namespace phaser {

InputEP_AUTO::InputEP_AUTO(std::string cards)
{ parseCCP4(cards); }

InputEP_AUTO::InputEP_AUTO() {}
InputEP_AUTO::~InputEP_AUTO() {}

//performs the analysis between correlated keyword Input
void InputEP_AUTO::Analyse(Output& output)
{
  ATOM::analyse();
  BFAC::analyse();
  BINS::analyse();
  CELL::analyse();
  CLUS::analyse();
  COMP::analyse();
  CRYS::analyse();
  // Replace with I/SIGI when EP changed to use these
  if (CRYS_DATA().POS.INPUT_INTENSITIES)
  {
    CRYS_DATA().POS.F = CRYS_DATA().POS.FfromI;
    CRYS_DATA().POS.SIGF = CRYS_DATA().POS.SIGFfromI;
  }
  if (CRYS_DATA().NEG.INPUT_INTENSITIES)
  {
    CRYS_DATA().NEG.F = CRYS_DATA().NEG.FfromI;
    CRYS_DATA().NEG.SIGF = CRYS_DATA().NEG.SIGFfromI;
  }
  FFTS::analyse();
  HAND::analyse();
  HKLI::analyse();
  HKLO::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LLGC::analyse();
  LLGM::analyse();
  MACA::analyse();
  MACJ::analyse();
  MACS::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  PEAK::analyse();
  PART::analyse();
  RESH::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  SAMP::analyse();
  SCAT::analyse();
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  VARS::analyse();
  VERB::analyse();
  WAVE::analyse();
  XYZO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  //transfer from ATOM to CRYS
  if (!CRYS_DATA[""].ATOM_SET.size()) //second time through, AUTO->SAD, CRYS not empty
    for (int i = 0; i < ATOM_SET.size(); i++)
      if (ATOM_SET[i].size() ||
         (PARTIAL.filename() != "" && !CRYS_DATA[""].ATOM_SET.size())) //only one empty set with partial
      {
        CRYS_DATA[""].ATOM_SET.push_back(ATOM_SET[i]);
      }

  if (!CRYS_DATA[""].ATOM_SET.size() && PARTIAL.filename() == "")
  throw PhaserError(INPUT,keywords,"No atoms input\n");

// collect the atomtypes starting with the set from the llgc
// from the list of scatterers
  stringset ALL_ATOMTYPES = LLGCOMPLETE.ATOMTYPE;
  for (unsigned t = 0 ; t < CRYS_DATA[""].ATOM_SET.size(); t++)
  for (unsigned a = 0 ; a < CRYS_DATA[""].ATOM_SET[t].size(); a++)
    ALL_ATOMTYPES.insert(CRYS_DATA[""].ATOM_SET[t][a].SCAT.scattering_type);

  for (stringset::iterator iter = ALL_ATOMTYPES.begin(); iter != ALL_ATOMTYPES.end(); iter++)
  {
    std::string atomtype = *iter;
    if (!(isElement(atomtype) || atomtype == "RX" || atomtype == "AX" || atomtype == "TX" || atomtype == "XX"))
      throw PhaserError(INPUT,keywords,"Type " + atomtype + " not element or cluster");
    if (atomtype == "XX")
    {
      if (CLUSTER_PDB == "")
      throw PhaserError(INPUT,keywords,"No CLUSTER definition pdb file");
      if (SCATTERING.ATOMTYPE.find(atomtype) == SCATTERING.ATOMTYPE.end()) //unknown cluster
      throw PhaserError(INPUT,keywords,"Use SCATTERING to specify f' and f\" for CLUSTER (TYPE XX)");
    }
    if (isElement(atomtype) &&
        SCATTERING.ATOMTYPE.find(atomtype) == SCATTERING.ATOMTYPE.end())
    {
      cctbx::eltbx::sasaki::table sasaki(atomtype,false,false);
      if (sasaki.is_valid() && !sasaki.at_angstrom(WAVELENGTH).is_valid())
      throw PhaserError(INPUT,keywords,"Wavelength " + dtos(WAVELENGTH) + " outside range of Sasaki table.\nUse SCATTERING to specify f' and f\".");
    }
  }

  if (ATOM_XTALID == "") ATOM_XTALID = CRYS_DATA("","").XTALID;

  //Check partial structure mtz file
  if (PARTIAL.filename().size() && PARTIAL.map_format())
  {
  //It would be better if the partial structure data were on the same input MTZ file
  //as the data, this would avoid the indexing problems that arise if the two MTZ
  //files are in different asus
  int read_reflections(0);
  CMtz::MTZ *mtz = safe_mtz_get(PARTIAL.filename(),read_reflections);

/* UNITCELL */
  af::double6 unitcell;
  CMtz::MTZCOL *Fcol = safe_mtz_col_lookup(mtz,PARTIAL.LABI_F);
  CMtz::MTZXTAL* xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, Fcol));
  PHASER_ASSERT(xtal != 0);
  af::double6 xcell;
  for (int c = 0; c < 6; c++) xcell[c] = xtal->cell[c];
  for (int c = 0; c < 6; c++)
   if (!between(double(xtal->cell[c]),UNIT_CELL[c],UNIT_CELL[c]/100.))
   throw PhaserError(INPUT,keywords,"Partial structure map (" + dvtos(xcell) + ") must be in same cell as target (" + dvtos(UNIT_CELL) + ")");

/* SPACEGROUP */
  cctbx::sgtbx::space_group mtzSG;
  for (unsigned isym = 0; isym < mtz->mtzsymm.nsym; isym++)
  {
    scitbx::vec3<floatType> trasym;
    scitbx::mat3<floatType> rotsym;
    for (int i = 0; i < 3; i++)
    {
      trasym[i] = mtz->mtzsymm.sym[isym][i][3];
      for (int j = 0; j < 3; j++)
        rotsym(i,j) = mtz->mtzsymm.sym[isym][j][i];
    }
    cctbx::sgtbx::rt_mx nextSym(rotsym.transpose(),trasym);
    mtzSG.expand_smx(nextSym);
  }
  cctbx::sgtbx::space_group cctbxSG(SG_HALL);
  std::string sg1 = space_group_name(mtzSG.type().hall_symbol(),true).CCP4;
  std::string sg2 = space_group_name(cctbxSG.type().hall_symbol(),true).CCP4;
  if (cctbxSG != mtzSG)
    throw PhaserError(INPUT,keywords,"Partial structure map (" + sg1 + ") must be in same space group as target (" + sg2 + ")");
  }
}

std::string InputEP_AUTO::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += ATOM::unparse();
  cards += BFAC::unparse();
  cards += BINS::unparse();
  cards += CELL::unparse();
  cards += CLUS::unparse();
  cards += COMP::unparse();
  cards += CRYS::unparse();
  cards += FFTS::unparse();
  cards += HAND::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LLGC::unparse();
  cards += LLGM::unparse();
  cards += MACA::unparse();
  cards += MACJ::unparse();
  cards += MACS::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += PART::unparse();
  cards += PEAK::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += SAMP::unparse();
  cards += SCAT::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += VARS::unparse();
  cards += VERB::unparse();
  cards += WAVE::unparse();
  cards += XYZO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
