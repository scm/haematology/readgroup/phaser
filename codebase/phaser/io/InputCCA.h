//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputCCA__Class__
#define __InputCCA__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputCCA :
                  public CELL
                 ,public COMP
                 ,public MUTE
                 ,public RESO
                 ,public ROOT
                 ,public SPAC
                 ,public TITL
                 ,public VERB
                 ,public KILL
{
  public:
    InputCCA(std::string);
    InputCCA();
    ~InputCCA();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
