//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_RNP__Class__
#define __InputMR_RNP__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_RNP :
                  public BFAC
                 ,public BINS
                 ,public BOXS
                 ,public CELL
                 ,public COMP
                 ,public ENSE
                 ,public FORM
                 ,public HKLI
                 ,public HKLO
                 ,public JOBS
                 ,public KEYW
                 ,public LABI
                 ,public MACA
                 ,public MACT
                 ,public MACM
                 ,public MACG
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public PURG
                 ,public RESH
                 ,public RESO
                 ,public ROOT
                 ,public SEAR
                 ,public SOLP
                 ,public SOLU
                 ,public SORT
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public VERB
                 ,public XYZO
                 ,public ZSCO
                 ,public KILL
{
  public:
    InputMR_RNP(std::string);
    InputMR_RNP();
    ~InputMR_RNP();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
