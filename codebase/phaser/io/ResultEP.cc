//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/lib/jiffy.h>
#include <phaser/lib/rad2phi.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>
#include <phaser/io/PdbHandler.h>
#include <phaser/io/ResultEP.h>
#include <phaser/io/ResultFile.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/sgtbx/reciprocal_space_asu.h>

namespace phaser {

ResultEP::ResultEP(): Output(),UnitCell() { second_hand = false; TopRfac = 0; }
ResultEP::ResultEP(Output& output): Output(output),UnitCell() { second_hand = false; TopRfac = 0; }

void ResultEP::addDataEP(bool h, SpaceGroup sg,HandEP hand)
{
  hand.setSpaceGroup(sg);
  h ? hand2.push_back(hand) : hand1.push_back(hand);
  if (h) second_hand = true;
  for (int t = 0; t < hand1.size(); t++) hand1[t].atoms.NUM = t+1;
  for (int t = 0; t < hand2.size(); t++) hand2[t].atoms.NUM = t+1;
}

af_string ResultEP::getFilenames()
{
  af_string filenames;
  for (int t = 0; t < hand1.size(); t++)
  {
    if (hand1[t].PDBfile.size()) filenames.push_back(hand1[t].PDBfile);
    if (hand1[t].MTZfile.size()) filenames.push_back(hand1[t].MTZfile);
    if (hand1[t].MAPfile.size()) filenames.push_back(hand1[t].MAPfile);
    if (hand1[t].SOLfile.size()) filenames.push_back(hand1[t].SOLfile);
  }
  for (int t = 0; t < hand2.size(); t++)
  {
    if (hand2[t].PDBfile.size()) filenames.push_back(hand2[t].PDBfile);
    if (hand2[t].MTZfile.size()) filenames.push_back(hand2[t].MTZfile);
    if (hand2[t].MAPfile.size()) filenames.push_back(hand2[t].MAPfile);
    if (hand2[t].SOLfile.size()) filenames.push_back(hand2[t].SOLfile);
  }
  return filenames;
}

void ResultEP::writeMtz(bool h,int t,std::string RefID,std::string HKLIN,double WAVELENGTH)
{
  if (h && t >= hand2.size()) return ;
  if (!h && t >= hand1.size()) return ;
  HandEP& hand = h ? hand2[t]: hand1[t];
  PHASER_ASSERT(HKLIN.size());
  if (hand.mtz_is_set())
  {
    hand.MTZfile = Output::Fileroot() + "." + itos(t+1) + std::string(h ? ".hand.mtz" : ".mtz");
    MtzHandler mtzOut(hand,(*this),MILLER,false,HKLIN);
    mtzOut.setWave(WAVELENGTH,RefID);
    cctbx::sgtbx::space_group cctbxSG = hand.getCctbxSG();
    mtzOut.addData(MILLER,hand.selected,"FWT","F",hand.FWT,RefID);
    mtzOut.addData(MILLER,hand.selected,"PHWT","P",hand.PHWT,RefID);
    mtzOut.addData(MILLER,hand.selected,"PHIB","P",hand.PHIB,RefID);
    mtzOut.addData(MILLER,hand.selected,"FOM","W",hand.FOM,RefID);
    mtzOut.addData(MILLER,hand.selected,"FPFOM","W",hand.FPFOM,RefID);
    mtzOut.addData(MILLER,hand.selected,"HL#","A",hand.HL,RefID);
    if (hand.HL_ANOM.size() == MILLER.size())
      mtzOut.addData(MILLER,hand.selected,"HLanom#","A",hand.HL_ANOM,RefID);
    mtzOut.writeMtz(hand.MTZfile);
  }
}

void ResultEP::writeMtz(bool h,int t,std::string XTALID,std::string WAVEID,data_spots POS,data_spots NEG,double WAVELENGTH)
{
  if (h && t >= hand2.size()) return ;
  if (!h && t >= hand1.size()) return ;
  HandEP& hand = h ? hand2[t]: hand1[t];
  if (hand.mtz_is_set())
  {
    hand.MTZfile = Output::Fileroot() + "." + itos(t+1) + std::string(h ? ".hand.mtz" : ".mtz");
    MtzHandler mtzOut(hand,(*this),MILLER,false,"",XTALID,WAVEID);
    std::string RefID("");
    mtzOut.setWave(WAVELENGTH,RefID);
    cctbx::sgtbx::space_group cctbxSG = hand.getCctbxSG();
    PHASER_ASSERT(MILLER.size() == POS.PRESENT.size());
    af_bool NEG_PRESENT = NEG.PRESENT.deep_copy();
    for (int r = 0; r < MILLER.size(); r++)
      if (cctbxSG.is_centric(MILLER[r])) NEG_PRESENT[r] = false;
    if (POS.INPUT_INTENSITIES && NEG.INPUT_INTENSITIES)
    {
      mtzOut.addData(MILLER,POS.PRESENT,POS.I_ID,"K",POS.I,RefID);
      mtzOut.addData(MILLER,POS.PRESENT,POS.SIGI_ID,"M",POS.SIGI,RefID);
      mtzOut.addData(MILLER,NEG_PRESENT,NEG.I_ID,"K",NEG.I,RefID);
      mtzOut.addData(MILLER,NEG_PRESENT,NEG.SIGI_ID,"M",NEG.SIGI,RefID);
    }
    else if (!POS.INPUT_INTENSITIES && !NEG.INPUT_INTENSITIES)
    {
      mtzOut.addData(MILLER,POS.PRESENT,POS.F_ID,"G",POS.F,RefID);
      mtzOut.addData(MILLER,POS.PRESENT,POS.SIGF_ID,"L",POS.SIGF,RefID);
      mtzOut.addData(MILLER,NEG_PRESENT,NEG.F_ID,"G",NEG.F,RefID);
      mtzOut.addData(MILLER,NEG_PRESENT,NEG.SIGF_ID,"L",NEG.SIGF,RefID);
    }
    mtzOut.addData(MILLER,hand.selected,"FWT","F",hand.FWT,RefID);
    mtzOut.addData(MILLER,hand.selected,"PHWT","P",hand.PHWT,RefID);
    mtzOut.addData(MILLER,hand.selected,"PHIB","P",hand.PHIB,RefID);
    mtzOut.addData(MILLER,hand.selected,"FOM","W",hand.FOM,RefID);
    mtzOut.addData(MILLER,hand.selected,"FPFOM","W",hand.FPFOM,RefID);
    mtzOut.addData(MILLER,hand.selected,"HL#","A",hand.HL,RefID);
    if (hand.HL_ANOM.size() == MILLER.size())
      mtzOut.addData(MILLER,hand.selected,"HLanom#","A",hand.HL_ANOM,RefID);
    mtzOut.writeMtz(hand.MTZfile);
  }
}

void ResultEP::writeMap(bool h,int t,std::string RefID,std::string HKLIN)
{
  if (h && t >= hand2.size()) return ;
  if (!h && t >= hand1.size()) return ;
  HandEP& hand = h ? hand2[t]: hand1[t];
  std::string MAPfile = Output::Fileroot() + "." + itos(t+1) + std::string(h ? ".hand.llgmaps.mtz" : ".llgmaps.mtz");
  if (hand.mtz_is_set() && hand.LLGCMAPS.size())
  {
    hand.MAPfile = MAPfile;
    if (hand.LLGCMAPS[0].FLLG.size() == MILLER.size())
    {
      MtzHandler mtzOut(hand,(*this),MILLER,false,HKLIN);
      for (int i = 0; i < hand.LLGCMAPS.size(); i++)
      {
        mtzOut.addData(MILLER,hand.selected,"FLLG_" +hand.LLGCMAPS[i].ATOMTYPE,"F",hand.LLGCMAPS[i].FLLG,RefID);
        mtzOut.addData(MILLER,hand.selected,"PHLLG_"+hand.LLGCMAPS[i].ATOMTYPE,"P",hand.LLGCMAPS[i].PHLLG,RefID);
      }
      mtzOut.writeMtz(hand.MAPfile);
    }
    else
    {
      cctbx::sgtbx::space_group SgOps = hand.getCctbxSG();
      af::shared<miller::index<int> > p1_miller;
      af_bool  p1_selected;
      for (unsigned r = 0; r < MILLER.size(); r++)
      {
        miller::sym_equiv_indices s_e_i(SgOps,MILLER[r]);
        for (std::size_t isym = 0; isym < s_e_i.multiplicity(false); isym++)
        {
          miller::sym_equiv_index h_eq = s_e_i(isym);
          if (cctbx::sgtbx::reciprocal_space::is_in_reference_asu_1b(h_eq.h()))
          {
            p1_miller.push_back(h_eq.h());
            p1_selected.push_back(hand.selected[r]);
          }
        }
      }
      PHASER_ASSERT(p1_miller.size() == hand.LLGCMAPS[0].FLLG.size());
      SpaceGroup P1("P 1");
      MtzHandler mtzOut(P1,(*this),p1_miller,false,"");
      for (int i = 0; i < hand.LLGCMAPS.size(); i++)
      {
        mtzOut.addData(p1_miller,p1_selected,"FLLG_" +hand.LLGCMAPS[i].ATOMTYPE,"F",hand.LLGCMAPS[i].FLLG, RefID);
        mtzOut.addData(p1_miller,p1_selected,"PHLLG_"+hand.LLGCMAPS[i].ATOMTYPE,"P",hand.LLGCMAPS[i].PHLLG,RefID);
      }
      mtzOut.writeMtz(hand.MAPfile);
    }
  }
}

af_float ResultEP::getLLG_F(std::string a,bool h,int t)
{
  if (h)
  {
    PHASER_ASSERT( t < hand2.size() );
    for (int i = 0; i < hand2[t].LLGCMAPS.size(); i++)
      if (hand2[t].LLGCMAPS[i].ATOMTYPE == a) return hand2[t].LLGCMAPS[i].FLLG;
  }
  else
  {
    PHASER_ASSERT( t < hand1.size() );
    for (int i = 0; i < hand1[t].LLGCMAPS.size(); i++)
      if (hand1[t].LLGCMAPS[i].ATOMTYPE == a) return hand1[t].LLGCMAPS[i].FLLG;
  }

  return af_float();
}

af_float ResultEP::getLLG_PHI(std::string atomtype,bool h,int t)
{
  if (h)
  {
    PHASER_ASSERT( t < hand2.size() );
    for (int i = 0; i < hand2[t].LLGCMAPS.size(); i++)
      if (hand2[t].LLGCMAPS[i].ATOMTYPE == atomtype) return hand2[t].LLGCMAPS[i].PHLLG;
  }
  else
  {
    PHASER_ASSERT( t < hand1.size() );
    for (int i = 0; i < hand1[t].LLGCMAPS.size(); i++)
      if (hand1[t].LLGCMAPS[i].ATOMTYPE == atomtype) return hand1[t].LLGCMAPS[i].PHLLG;
  }

  return af_float();
}

iotbx::pdb::hierarchy::root ResultEP::getIotbx(bool h,int t)
{
  //output for SAD pdb file
  iotbx::pdb::hierarchy::root output_root;
  HandEP& hand = h ? hand2[t]: hand1[t];
  pdb_chains_2chainid chains(2);
  std::string chain_id = chains.allocate("A");
  iotbx::pdb::hierarchy::model output_model;
  iotbx::pdb::hierarchy::chain output_chain(chain_id.c_str());
  int ResNum(1);
  for (unsigned a = 0; a < hand.atoms.size(); a++)
  {
    if (hand.atoms[a].SCAT.label != "px")
    {
      if (ResNum > 999)
      {
        output_model.append_chain(output_chain);
        ResNum = 1;
        chain_id = chains.allocate("A");
        if (chains.allocate_failed()) logWarning(SUMMARY,chains.message());
        output_chain = iotbx::pdb::hierarchy::chain(chain_id.c_str()); //reinitialize
      }
      iotbx::pdb::hierarchy::atom output_atom;
      output_atom.data->xyz = UnitCell::doFrac2Orth(hand.atoms[a].SCAT.site);
      output_atom.data->uij = hand.atoms[a].SCAT.u_star;
      output_atom.data->occ = hand.atoms[a].SCAT.occupancy;
      output_atom.data->element = hand.atoms[a].SCAT.scattering_type.c_str();
      output_atom.data->name = hand.atoms[a].SCAT.scattering_type.c_str();
      output_atom.data->serial = itos(a+1).c_str();
      iotbx::pdb::hierarchy::atom_group output_atom_group;
      output_atom_group.data->resname = "SUB";
      output_atom_group.append_atom(output_atom);
      iotbx::pdb::hierarchy::residue_group output_residue_group;
      output_residue_group.data->resseq = itos(ResNum).c_str();
      output_residue_group.append_atom_group(output_atom_group);
      output_chain.append_residue_group(output_residue_group);
      ResNum++;
    }
  }
  output_model.append_chain(output_chain);
  output_root.append_model(output_model);
  return output_root;
}

void ResultEP::writeScript(bool h,int t,std::string title,std::string xtalid)
{
  if (h && t >= hand2.size()) return;
  if (!h && t >= hand1.size()) return;
  HandEP& hand = h ? hand2[t]: hand1[t];
  if (hand.atoms_is_set() || hand.scat_is_set() || hand.variance_is_set())
  {
    hand.SOLfile = Output::Fileroot() + "." + itos(t+1) + std::string(h ? ".hand.sol" : ".sol");
    ResultFile opened(hand.SOLfile);
    fprintf(opened.outFile,"# TITLE %s \n",title.c_str());
    fprintf(opened.outFile,"SPACEGROUP %s\n",hand.spcgrpname().c_str());
    if (hand.atoms_is_set())
    {
      //write out sol file with cartesian anisotropic parameters
      for (int a = 0; a < hand.atoms.size(); a++)
      if (hand.atoms[a].SCAT.flags.use_u_aniso_only() &&
          hand.atoms[a].USTAR)
      {
        hand.atoms[a].SCAT.u_star = cctbx::adptbx::u_star_as_u_cart(getCctbxUC(),hand.atoms[a].SCAT.u_star);
        hand.atoms[a].USTAR = false;
      }
      bool logfile=true;
      fprintf(opened.outFile,"%-s",hand.atoms.unparse(xtalid,logfile).c_str());
    }
    if (hand.scat_is_set())
    {
      fprintf(opened.outFile,"# Refined scattering terms \n");
      std::map<std::string,cctbx::eltbx::fp_fdp> FpFdp = hand.getFpFdp();
      for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator iter = FpFdp.begin(); iter != FpFdp.end(); iter++)
        fprintf(opened.outFile,"SCATTERING TYPE %s FP=%5.2f FDP=%5.2f FIX OFF\n",
          iter->first.c_str(),iter->second.fp(),iter->second.fdp());
    }
    if (hand.variance_is_set())
    {
      fprintf(opened.outFile,"# Refined variance terms \n");
      fprintf(opened.outFile,"%s\n",hand.unparse().c_str());
    }
    fclose(opened.outFile);
  }
}

void ResultEP::writePdb(bool h,int t,std::string title)
{
  if (h && t >= hand2.size()) return;
  if (!h && t >= hand1.size()) return;
  HandEP& hand = h ? hand2[t]: hand1[t];
  if (hand.atoms_is_set() || hand.scat_is_set() || hand.variance_is_set()) //same condition as script, there may be no atoms
  {
    hand.PDBfile = Output::Fileroot() + "." + itos(t+1) + std::string(h ? ".hand.pdb" : ".pdb");
    ResultFile opened(hand.PDBfile);
    fprintf(opened.outFile,"REMARK TITLE %s \n",title.c_str());
    int Z = hand.getSpaceGroupNSYMM();
    fprintf(opened.outFile,"%s",UnitCell::CrystCard(hand.spcgrpname(),Z).c_str());
    hand.atoms.writePdb(opened.outFile,getCctbxUC());
    fclose(opened.outFile);
  } //atoms set
}


af_float ResultEP::p1_generator(bool h,int t,af_float array,bool is_phase)
{
  HandEP& hand = h ? hand2[t]: hand1[t];
  cctbx::sgtbx::space_group SgOps = hand.getCctbxSG();
  af_float p1_array;
  for (unsigned r = 0; r < MILLER.size(); r++)
  {
    miller::sym_equiv_indices s_e_i(SgOps,MILLER[r]);
    for (std::size_t isym = 0; isym < s_e_i.multiplicity(false); isym++)
    {
      miller::sym_equiv_index h_eq = s_e_i(isym);
      if (cctbx::sgtbx::reciprocal_space::is_in_reference_asu_1b(h_eq.h()))
      {
        bool deg(true); //default is false
        is_phase ? p1_array.push_back(h_eq.phase_eq(array[r],deg)) : p1_array.push_back(array[r]);
      }
    }
  }
  return p1_array;
}

af::shared<cctbx::hendrickson_lattman<double> > ResultEP::p1_generator(bool h,int t,af::shared<cctbx::hendrickson_lattman<double> > array)
{
  HandEP& hand = h ? hand2[t]: hand1[t];
  cctbx::sgtbx::space_group SgOps = hand.getCctbxSG();
  af::shared<cctbx::hendrickson_lattman<double> > p1_array;
  for (unsigned r = 0; r < MILLER.size(); r++)
  {
    miller::sym_equiv_indices s_e_i(SgOps,MILLER[r]);
    for (std::size_t isym = 0; isym < s_e_i.multiplicity(false); isym++)
    {
      miller::sym_equiv_index h_eq = s_e_i(isym);
      if (cctbx::sgtbx::reciprocal_space::is_in_reference_asu_1b(h_eq.h()))
      {
        p1_array.push_back(h_eq.hendrickson_lattman_eq(array[r]));
      }
    }
  }
  return p1_array;
}

floatType ResultEP::getFp(std::string atomtype,bool h,int t)
{
  HandEP& hand = h ? hand2[t]: hand1[t];
  std::map<std::string,cctbx::eltbx::fp_fdp> FpFdp = hand.getFpFdp();
  if (FpFdp.count(atomtype)) return FpFdp[atomtype].fp();
  return 0;
}

floatType ResultEP::getFdp(std::string atomtype,bool h,int t)
{
  HandEP& hand = h ? hand2[t]: hand1[t];
  std::map<std::string,cctbx::eltbx::fp_fdp> FpFdp = hand.getFpFdp();
  if (FpFdp.count(atomtype)) return FpFdp[atomtype].fdp();
  return 0;
}

struct llg_comp : std::binary_function<HandEP, HandEP, bool>
{
  llg_comp() {}
  bool operator() (const HandEP& m1, const HandEP& m2) { return m1.atoms.LLG < m2.atoms.LLG; }
};

void ResultEP::sort()
{
  std::sort(hand1.begin(),hand1.end(),llg_comp());
  std::sort(hand2.begin(),hand2.end(),llg_comp());
}

struct num_comp : std::binary_function<HandEP, HandEP, bool>
{
  num_comp() {}
  bool operator() (const HandEP& m1, const HandEP& m2) { return m1.atoms.NUM < m2.atoms.NUM; }
};

void ResultEP::unsort()
{
  std::sort(hand1.begin(),hand1.end(),num_comp());
  std::sort(hand2.begin(),hand2.end(),num_comp());
}

std::pair<int,int> ResultEP::getTop()
{
  std::pair<int,int> maxth(0,0);
  if (!hand1.size()) return maxth;
  double maxllg = hand1[0].atoms.LLG;
  for (int t = 1; t < hand1.size(); t++)
  if (hand1[t].atoms.LLG < maxllg) //negative llg
  {
    maxllg = hand1[t].atoms.LLG;
    maxth.first = t;
    maxth.second = 0;
  }
  double tol=1.0e-06;//take the first hand if they are the same
  for (int t = 0; t < hand2.size(); t++)
  if (hand2[t].atoms.LLG < maxllg-tol) //negative llg
  {
    maxllg = hand2[t].atoms.LLG;
    maxth.first = t;
    maxth.second = 1;
  }
  return maxth;
}

scitbx::af::shared<cctbx::xray::scatterer<double> > ResultEP::getTopAtoms()
{
  std::pair<int,int> maxth = getTop();
  if (hand1.size())
    return maxth.second ? hand2[maxth.first].getAtoms() : hand1[maxth.first].getAtoms();
  return scitbx::af::shared<cctbx::xray::scatterer<double> >();
}

std::string  ResultEP::getTopPdbFile()
{
  std::pair<int, int> maxth = getTop();
  if (hand1.size())
    return maxth.second ? hand2[maxth.first].PDBfile : hand1[maxth.first].PDBfile;
  return "";
}


std::string  ResultEP::getTopMtzFile()
{
  std::pair<int, int> maxth = getTop();
  if (hand1.size())
    return maxth.second ? hand2[maxth.first].MTZfile : hand1[maxth.first].MTZfile;
  return "";
}

ep_solution ResultEP::getDotSol(bool hand)
{
  ep_solution SADSET;
  if (hand && hand2.size())
  {
    for (unsigned a = 0; a < hand2.size(); a++)
      SADSET.push_back(hand2[a].atoms);
  }
  else if (!hand && hand1.size())
  {
    for (unsigned a = 0; a < hand1.size(); a++)
      SADSET.push_back(hand1[a].atoms);
  }
  return SADSET;
}

}//namespace phaser
