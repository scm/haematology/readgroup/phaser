//All rights reserved
#ifndef __ResultMR_TF__Class__
#define __ResultMR_TF__Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/pod/xyzout.h>
#include <phaser/include/data_peak.h>
#include <phaser/io/Output.h>
#include <phaser/io/ResultCore.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

class gyre_file
{
  public:
  std::string modlid;
  std::string filename;
  floatType   rms;

  gyre_file(std::string m="",std::string f="",floatType r=0) : modlid(m),filename(f),rms(r) {}
};

class ResultMR_TF : public Output, public UnitCell, public ResultCore
{
  public:
    ResultMR_TF();
    ResultMR_TF(Output&);
    virtual ~ResultMR_TF() throw() {}

  private:
    mr_solution MRSET;
    std::string TITLE;
    size_t      NTOP;
    af_string   PdbFiles;
    std::string SolFile;
    double      TopRfac;

  public:
    void init(std::string,unsigned,map_str_pdb&);

    void                storeTralist(const mr_solution& m)  { MRSET = m; MRSET.sort_TF(); }
    void                setKnownOrigin(mr_set,std::string,floatType);
    void                setDotSol(const mr_solution& d)     { MRSET = d; }
    void                setTopRfac(floatType s)             { TopRfac = s; }

    const mr_solution&  getDotSol()      { return MRSET; }
    af_string           getPdbFiles()    { return PdbFiles; }
    int                 getNumPdbFiles() { return PdbFiles.size(); }
    std::string         getSolFile()     { return SolFile; }
    af_float            getTF()          { return MRSET.tf(); }
    af_float            getTFZ()         { return MRSET.tfz(); }
    mr_set              getSet(int t)    { MRSET.sort_TF(); return t < MRSET.size() ? MRSET[t] : mr_set(); }
    mr_set              getTopSet()      { MRSET.sort_TF(); return MRSET.size() ? MRSET[0] : mr_set(); }
    floatType           getTopTF()       { return MRSET.top_TF(); }
    floatType           getTopTFZ()      { return MRSET.top_TFZ(); }
    floatType           getTopRfac()     { return TopRfac; }
    unsigned            getNumTop()      { MRSET.sort_TF(); return std::min(NTOP,MRSET.size()); }
    int                 numSolutions()   { return MRSET.size(); }
    bool                foundSolutions() { return MRSET.size() ? true : false; }
    const std::string   getTitle() const { return TITLE; }

    af_string getFilenames();
    void writeSol();
    void writePdb(xyzout,MapTrcPtr);
};

} //phaser
#endif
