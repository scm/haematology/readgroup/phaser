//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_FRF.h>

namespace phaser {

InputMR_FRF::InputMR_FRF(std::string cards)
{ parseCCP4(cards); }

InputMR_FRF::InputMR_FRF() {}
InputMR_FRF::~InputMR_FRF() {}

//performs the analysis between correlated keyword Input
void InputMR_FRF::Analyse(Output& output)
{
  BINS::analyse();
  BOXS::analyse();
  CELL::analyse();
  COMP::analyse();
  ENSE::analyse();
  FORM::analyse();
  ELLG::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LABI::analyse();
  MACA::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  PEAK::analyse();
  PURG::analyse();
  RESC::analyse();
  RESH::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  ROTA::analyse();
  SAMP::analyse();
  SEAR::analyse();
  SGAL::analyse();
  SOLP::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TARG::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  VERB::analyse();
  XYZO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE
  //know what final spacegroup is going to be, sequence of overrides
  std::string FINAL_SG_HALL = REFLECTIONS.SG_HALL;
  if (SGALT_BASE.size()) FINAL_SG_HALL = SGALT_BASE;
  setupSGAL(FINAL_SG_HALL);

  for (int r = 0; r < SEARCH_MODLID.size(); r++)
  if (PDB.find(SEARCH_MODLID[r]) == PDB.end())
  throw PhaserError(INPUT,keywords,"No model for search ensemble " + SEARCH_MODLID[r]);

  for (int k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      if (PDB.find(MRSET[k].KNOWN[s].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for ensemble " + MRSET[k].KNOWN[s].MODLID);
    }

  DO_XYZOUT.Default(false);
  MRSET.check_init();

  if (PEAKS_ROT.by_percent())
  {
    if (PEAKS_ROT.is_default_select() && !PURGE_ROT.is_default_percent())
      PEAKS_ROT.set_cutoff(PURGE_ROT.get_percent());
    else if (!PEAKS_ROT.is_default_select() && PURGE_ROT.is_default_percent())
      PURGE_ROT.set_percent(PEAKS_ROT.get_cutoff());
  }

  if (!SEARCH_MODLID.size())
    throw PhaserError(INPUT,keywords,"Unknown number for search");

  bool or_search_atom(false),or_search_ensemble(false);
  for (unsigned frf = 0; frf < SEARCH_MODLID.size(); frf++)
  {
    std::string& ROT_MODLID = SEARCH_MODLID[frf];
    if (PDB[ROT_MODLID].is_atom) or_search_atom = true;
    if (!PDB[ROT_MODLID].is_atom) or_search_ensemble = true;
  }
  if (or_search_atom && or_search_ensemble)
    throw PhaserError(INPUT,keywords,"Rotation function OR search for single atom and ensemble not allowed");

  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputMR_FRF::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BINS::unparse();
  cards += BOXS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ENSE::unparse();
  cards += FORM::unparse();
  cards += ELLG::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LABI::unparse();
  cards += MACA::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += PEAK::unparse();
  cards += PURG::unparse();
  cards += RESC::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += ROTA::unparse();
  cards += SAMP::unparse();
  cards += SEAR::unparse();
  cards += SGAL::unparse();
  cards += SOLP::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TARG::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
