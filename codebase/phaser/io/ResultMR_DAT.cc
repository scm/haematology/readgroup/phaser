//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultMR_DAT.h>
#include <phaser/io/MtzHandler.h>

namespace phaser {

ResultMR_DAT::ResultMR_DAT() : Output(),UnitCell()
{
}

ResultMR_DAT:: ResultMR_DAT(Output& output) : Output(output),UnitCell()
{
}

void ResultMR_DAT::writeMtz(bool reindexed)
{
  SpaceGroup this_sg(DATA_REFL.SG_HALL);
  UnitCell this_uc(DATA_REFL.UNIT_CELL);
  std::string REFID("");
  af_bool selected(DATA_REFL.MILLER.size(),true);
  MtzHandler mtzOut(this_sg,this_uc,DATA_REFL.MILLER,true); //convert to int silent
  std::string HKLOUT = Output::Fileroot() + std::string(reindexed?".reindex":"") + ".mtz";
  if (DATA_REFL.F.size())
    mtzOut.addData(DATA_REFL.MILLER,selected,DATA_REFL.F_ID,"F",DATA_REFL.F,REFID);
  if (DATA_REFL.SIGF.size())
    mtzOut.addData(DATA_REFL.MILLER,selected,DATA_REFL.SIGF_ID,"Q",DATA_REFL.SIGF,REFID);
  if (DATA_REFL.I.size())
    mtzOut.addData(DATA_REFL.MILLER,selected,DATA_REFL.I_ID,"J",DATA_REFL.I,REFID);
  if (DATA_REFL.SIGI.size())
    mtzOut.addData(DATA_REFL.MILLER,selected,DATA_REFL.SIGI_ID,"Q",DATA_REFL.SIGI,REFID);
  mtzOut.writeMtz(HKLOUT);
  (reindexed) ?
  logTab(1,SUMMARY,"Reindexed data written to file: " + HKLOUT):
  logTab(1,SUMMARY,"Selected data written to file: " + HKLOUT);
  logBlank(SUMMARY);
}

} //phaser
