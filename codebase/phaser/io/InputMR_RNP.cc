//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_RNP.h>

namespace phaser {

InputMR_RNP::InputMR_RNP(std::string cards)
{ parseCCP4(cards); }

InputMR_RNP::InputMR_RNP() {}
InputMR_RNP::~InputMR_RNP() {}

//performs the analysis between correlated keyword Input
void InputMR_RNP::Analyse(Output& output)
{
  BFAC::analyse();
  BINS::analyse();
  BOXS::analyse();
  CELL::analyse();
  COMP::analyse();
  ENSE::analyse();
  FORM::analyse();
  HKLI::analyse();
  HKLO::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LABI::analyse();
  MACM::analyse();
  MACG::analyse();
  MACA::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  PURG::analyse();
  RESH::analyse();
  RESO::analyse();
  ROOT::analyse();
  //SEAR::analyse();
  SOLP::analyse();
  SOLU::analyse();
  SORT::analyse();
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  VERB::analyse();
  XYZO::analyse();
  ZSCO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE

  for (int k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      if (PDB.find(MRSET[k].KNOWN[s].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for ensemble " + MRSET[k].KNOWN[s].MODLID);
    }

  if (MACM_CHAINS.True() && MRSET.size())
  {
    for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
    {
      if (iter->second.ENSEMBLE.size() > 1)
      throw PhaserError(FATAL,"CHAIN refinement only possible with single model ensembles");
      if (iter->second.ENSEMBLE[0].CHAIN.size())
      throw PhaserError(FATAL,"Ensemble chain selection not possible for CHAIN refinement");
    }
  }
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");

}

std::string InputMR_RNP::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BFAC::unparse();
  cards += BINS::unparse();
  cards += BOXS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ENSE::unparse();
  cards += FORM::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LABI::unparse();
  cards += MACM::unparse();
  cards += MACG::unparse();
  cards += MACA::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += PURG::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SEAR::unparse();
  cards += SOLP::unparse();
  cards += SOLU::unparse();
  cards += SORT::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += ZSCO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
