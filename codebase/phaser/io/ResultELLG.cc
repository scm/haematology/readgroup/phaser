//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultELLG.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

ResultELLG::ResultELLG() : Output(), DataA()
{ }

ResultELLG::ResultELLG(Output& output) : Output(output), DataA()
{ }

map_str_float ResultELLG::get_map_ellg_full_resolution()
{
  map_str_float ellg_full_resolution;
  for (std::map<std::string,data_ellg>::iterator iter = ELLG_DATA.begin(); iter != ELLG_DATA.end(); iter++)
    ellg_full_resolution[iter->first] = iter->second.ELLG;
  return ellg_full_resolution;
}

} //phaser
