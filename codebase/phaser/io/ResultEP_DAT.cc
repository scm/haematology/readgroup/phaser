//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultEP_DAT.h>

namespace phaser {

ResultEP_DAT::ResultEP_DAT() : Output(),SpaceGroup(),UnitCell()
{
  HIRES = LORES = MTZ_HIRES = MTZ_LORES = 0;
}

ResultEP_DAT::ResultEP_DAT(Output& output) : Output(output),SpaceGroup(),UnitCell()
{
  HIRES = LORES = MTZ_HIRES = MTZ_LORES = 0;
}

} //phaser
