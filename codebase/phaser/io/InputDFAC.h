//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputDFAC__Class__
#define __InputDFAC__Class__
#include <phaser/io/InputNCS.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputDFAC :
                  public InputNCS
{
  public:
    InputDFAC(std::string);
    InputDFAC();
    ~InputDFAC();
};

} //phaser

#endif
