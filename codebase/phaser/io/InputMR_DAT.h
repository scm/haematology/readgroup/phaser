//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_DAT__Class__
#define __InputMR_DAT__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_DAT :
                  public CELL
                 ,public HKLI
                 ,public HKLO
                 ,public LABI
                 ,public MUTE
                 ,public RESO
                 ,public ROOT
                 ,public SGAL
                 ,public SPAC
                 ,public TITL
                 ,public VERB
                 ,public KILL
{
  public:
    InputMR_DAT(std::string);
    InputMR_DAT();
    ~InputMR_DAT();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
