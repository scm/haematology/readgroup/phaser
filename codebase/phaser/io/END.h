//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EndKeys__Class__
#define __EndKeys__Class__
#include <string>
#include <vector>

namespace phaser {
 std::vector<std::string> END_KEYS();
}

#endif
