//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <cstdio>

namespace phaser {

error::error(std::string const& msg) throw()
{
  msg_ = std::string("Program Error: ") + msg;
}

error::error(const char* file, long line, std::string const& msg,
             bool internal) throw()
{
  const char *s = "";
  if (internal) s = " internal";
  char buf[64];
  std::string sfile(file);
  std::string::size_type i = sfile.rfind("/"); //unix file separator
  sfile.erase(sfile.begin(),sfile.begin()+i+1);
  std::string::size_type j = sfile.rfind("\\"); //microsoft file separator
  sfile.erase(sfile.begin(),sfile.begin()+j+1);
  std::sprintf(buf, "%ld", line);
  msg_ =   std::string("Program") + s + " error in source file "
            + sfile + " (line " + buf + ")\n";
  if (msg.size()) msg_ += "*** "+ msg + " ***";
  if (internal) msg_ += "\nPlease email this log file with some supporting information and data to cimr-phaser@lists.cam.ac.uk";
}

error::~error() throw() {}

const char* error::what() const throw()
{
   return msg_.c_str();
}

const char* PhaserError::ErrorNameChar() const
{
  if (type == NULL_ERROR) return "NO";
  else if (type == SYNTAX) return "SYNTAX";
  else if (type == INPUT) return "INPUT";
  else if (type == FILEOPEN) return "FILE OPENING";
  else if (type == FATAL) return "FATAL RUNTIME";
  else if (type == KILLFILE) return "KILL-FILE DETECTED";
  else if (type == KILLTIME) return "KILL-TIME ELAPSED";
  else if (type == MEMORY) return "OUT OF MEMORY";
  else if (type == UNHANDLED) return "UNHANDLED";
  else if (type == UNKNOWN) return "UNKNOWN";
  return "";
}

void PhaserError::MakeLongMessage()
{
  longmessage += ErrorNameChar();
  longmessage += ": ";
  longmessage += ErrorMessage();
}

const char* PhaserError::what() const throw() {
  return longmessage.c_str();
} //more useful than message



} //phaser
