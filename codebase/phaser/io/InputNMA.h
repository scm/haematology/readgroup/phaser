//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputNMA__Class__
#define __InputNMA__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputNMA :
                  public DDM
                 ,public EIGE
                 ,public ENM
                 ,public ENSE
                 ,public JOBS
                 ,public KEYW
                 ,public MUTE
                 ,public NMA
                 ,public PERT
                 ,public ROOT
                 ,public SCED
                 ,public TITL
                 ,public VERB
                 ,public XYZO
                 ,public KILL
{
  public:
    InputNMA(std::string);
    InputNMA();
    ~InputNMA();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
