//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PdbHandler__
#define __PdbHandler__
#include <phaser/keywords/ENSE.h>
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_ndim.h>
#include <phaser/mr_objects/mr_gyre.h>
#include <phaser/include/data_ptgrp.h>
#include <phaser/io/pdb_chains_2chainid.h>

namespace phaser {

std::string pdb_string_maker(
                            map_str_pdb&,
                            map_str_str&,
                            mr_ndim&,
                            bool,
                            int&, //modfied and returned, increments each ATOM
                            std::string,
                            af::double6,
                            bool,
                            float2D,
                            MapTrcPtr,
                            bool=false,
                            float1D* OCCptr=NULL,
                            double=1);

std::string ens_string_maker(
                            map_str_pdb&,
                            map_str_str&,
                            mr_ndim&,
                            bool,
                            int&, //modfied and returned, increments each ATOM
                            std::string,
                            af::double6,
                            bool,
                            float2D,
                            MapTrcPtr,
                            float1D* OCCptr=NULL,
                            double=1);

std::string pak_string_maker(
                            map_str_pdb&,
                            mr_ndim&,
                            af::double6,
                            MapTrcPtr,
                            std::string);

std::string gyre_string_maker(
                            map_str_pdb&,
                            mr_gyre&,
                            af::double6);

} //phaser
#endif
