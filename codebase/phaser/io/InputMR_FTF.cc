//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_FTF.h>

namespace phaser {

InputMR_FTF::InputMR_FTF(std::string cards)
{ parseCCP4(cards); }

InputMR_FTF::InputMR_FTF() {}
InputMR_FTF::~InputMR_FTF() {}

//performs the analysis between correlated keyword Input
void InputMR_FTF::Analyse(Output& output)
{
  BINS::analyse();
  BOXS::analyse();
  CELL::analyse();
  COMP::analyse();
  ENSE::analyse();
  FORM::analyse();
  JOBS::analyse();
  KEYW::analyse();
  LABI::analyse();
  MACA::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  PEAK::analyse();
  PURG::analyse();
  RESC::analyse();
  RESH::analyse();
  RESO::analyse();
  RFAC::analyse();
  ROOT::analyse();
  SAMP::analyse();
 // SEAR::analyse(); //don't want to throw error about search not set
  SGAL::analyse();
  SOLP::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TARG::analyse();
  TITL::analyse();
  TNCS::analyse();
  TOPF::analyse();
  TRAN::analyse();
  VERB::analyse();
  XYZO::analyse();
  ZSCO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE
  //know what final spacegroup is going to be, sequence of overrides
  std::string FINAL_SG_HALL = REFLECTIONS.SG_HALL;
  if (SGALT_BASE.size()) FINAL_SG_HALL = SGALT_BASE;
  if (TARGET_TRA == "PHASED") DO_SGALT = "NONE"; //the space group is defined on mtz
  setupSGAL(FINAL_SG_HALL);

  for (int k = 0; k < MRSET.size(); k++)
    for (int e = 0; e < MRSET[k].RLIST.size(); e++)
      if (PDB.find(MRSET[k].RLIST[e].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for search ensemble " + MRSET[k].RLIST[e].MODLID);

  for (int k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
      if (PDB.find(MRSET[k].KNOWN[s].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for ensemble " + MRSET[k].KNOWN[s].MODLID);

  if (!MRSET.size())
    throw PhaserError(INPUT,keywords,"No solution sets");

  for (unsigned k = 0; k < MRSET.size(); k++)
    if (!MRSET[k].RLIST.size())
    throw PhaserError(INPUT,keywords,"No orientations to test for SET #" + itos(k+1));

  //RESCORE default has to be here not in analyse(), because it must not be called
  //in MR_AUTO input script
  if (DO_RESCORE_TRA == NOT_SET) DO_RESCORE_TRA = ON;

  if (PEAKS_TRA.by_percent())
  {
    if (PEAKS_TRA.is_default_select() && !PURGE_TRA.is_default_percent())
      PEAKS_TRA.set_cutoff(PURGE_TRA.get_percent());
    else if (!PEAKS_TRA.is_default_select() && PURGE_TRA.is_default_percent())
      PURGE_TRA.set_percent(PEAKS_TRA.get_cutoff());
  }

  if (TARGET_TRA == "PHASED" && !REFLECTIONS.FTFMAP.FMAP.size())
    throw PhaserError(INPUT,keywords,"No FMAP (structure factor amplitudes) for Phased Translation Function");
  if (TARGET_TRA == "PHASED" && !REFLECTIONS.FTFMAP.PHMAP.size())
    throw PhaserError(INPUT,keywords,"No PHMAP (phases) for Phased Translation Function");
  //FOM defaults if not present

  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputMR_FTF::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BINS::unparse();
  cards += BOXS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ENSE::unparse();
  cards += FORM::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += LABI::unparse();
  cards += MACA::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += PEAK::unparse();
  cards += PURG::unparse();
  cards += RESC::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += RFAC::unparse();
  cards += ROOT::unparse();
  cards += SAMP::unparse();
  cards += SEAR::unparse();
  cards += SGAL::unparse();
  cards += SOLP::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TARG::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += TOPF::unparse();
  cards += TRAN::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += ZSCO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
