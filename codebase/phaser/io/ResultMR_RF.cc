//(i) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultMR_RF.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/PdbHandler.h>
#include <phaser/io/ResultFile.h>
#include <phaser/src/SpaceGroup.h>
#include <limits>

namespace phaser {

ResultMR_RF::ResultMR_RF() : Output(), UnitCell()
{
  RF_HALL = TITLE = RlistFile = "";;
  NTOP = SAMPLING = TopRfac = 0;
}

ResultMR_RF::ResultMR_RF(Output& output) : Output(output), UnitCell()
{
  RF_HALL = TITLE = RlistFile = "";;
  NTOP = SAMPLING = TopRfac = 0;
}

void ResultMR_RF::init(std::string h,std::string t,unsigned n,map_str_pdb& pdb)
{
  RlistFile = "";
  RF_HALL = h;
  TITLE = t;
  NTOP = n;
  TopRfac = floatType(0);
  set_pdb_chainids(pdb);
}

void ResultMR_RF::storePurgeMean(floatType purge_mean) { MRSET.set_purge_mean(purge_mean); }
void ResultMR_RF::storeResolution(floatType h,bool c)  { MRSET.set_resolution(h); MRSET.can_lower_resolution(c); }

void ResultMR_RF::storeRotlist(std::string modlid,mr_set mrset,dvect31D eulers,float1D values,float1D zscores,bool1D deeps,int1D clusters)
{
  PHASER_ASSERT(!mrset.MAPCOEFS.size()); //previously cleared
  PHASER_ASSERT(eulers.size() == values.size());
  PHASER_ASSERT(eulers.size() == zscores.size());
  PHASER_ASSERT(!deeps.size() || (deeps.size() == eulers.size()));
  mrset.RLIST.resize(eulers.size());
  for (int e = 0; e < mrset.RLIST.size(); e++)
  {
    mrset.RLIST[e].MODLID = modlid;
    mrset.RLIST[e].EULER = eulers[e];
    mrset.RLIST[e].RF = values[e];
    mrset.RLIST[e].RFZ = zscores[e];
    mrset.RLIST[e].NCLUS = clusters[e];
    mrset.RLIST[e].DEEP = e < deeps.size() ? deeps[e] : false;
    int t = MRSET.size();
    sorted_index.push_back(RF_RFZ_t_e(values[e],zscores[e],t,e));
  }
  std::sort(mrset.RLIST.begin(),mrset.RLIST.end());
  std::reverse(mrset.RLIST.begin(),mrset.RLIST.end());
  MRSET.push_back(mrset);
  std::sort(sorted_index.begin(),sorted_index.end());
  std::reverse(sorted_index.begin(),sorted_index.end());//highest at top
}

void ResultMR_RF::setRlistOrigin(std::string modlid,std::string HALL,mr_set mrset,floatType value)
{
  mrset.ANNOTATION += " RF*0";
  mrset.HALL = HALL;
  mrset.RLIST.resize(1);
  mrset.RLIST[0].MODLID = modlid;
  mrset.RLIST[0].EULER = dvect3(0,0,0);
  mrset.RLIST[0].RF = value;
  mrset.RLIST[0].RFZ = 0;
  mrset.RLIST[0].DEEP = false;
  sorted_index.push_back(RF_RFZ_t_e(value,0,1,0));
  MRSET.push_back(mrset);
}

void ResultMR_RF::writeRlist()
{
  RlistFile = Output::Fileroot() + ".rlist";
  ResultFile opened(RlistFile);

  fprintf(opened.outFile,"# %s \n",TITLE.c_str());
  if (MRSET.size())
    fprintf(opened.outFile,"%s \n",MRSET.unparse().c_str());
  else fprintf(opened.outFile,"#NO ROTATION FUNCTION SOLUTIONS\n");
  fclose(opened.outFile);
}

af_string ResultMR_RF::getFilenames()
{
  af_string filenames;
  filenames.insert(filenames.end(),PdbFiles.begin(),PdbFiles.end());
  if (RlistFile.size()) filenames.push_back(RlistFile);
  return filenames;
}

af_float ResultMR_RF::getRF()
{
  af_float values(sorted_index.size());
  for (int s = 0; s < sorted_index.size(); s++)
    values[s] = sorted_index[s].RF;
  return values;
}

af_float ResultMR_RF::getRFZ()
{
  af_float values(sorted_index.size());
  for (int s = 0; s < sorted_index.size(); s++)
    values[s] = sorted_index[s].RFZ;
  return values;
}

void ResultMR_RF::writePdb(xyzout DO_XYZOUT,MapTrcPtr tracemol)
{
 // only write out the oriented molecule, as its translation with respect to any
 // other components is unknown
  for (int s = 0; s < getNumTop(); s++)
  {
    int t = sorted_index[s].t;
    int e = sorted_index[s].e;

    int num_chainid(0);
    if (ense_chainids.find(MRSET[t].RLIST[e].MODLID) != ense_chainids.end()) //could be a map
    {
      std::string m = MRSET[t].RLIST[e].MODLID;
      num_chainid += ense_chainids[m].size();
    }

    pdb_chains_2chainid chains(num_chainid);

    std::string filename = Output::Fileroot() + "." + itos(e+1) + ".pdb";
    ResultFile opened(filename);
    PdbFiles.push_back(filename);

    fprintf(opened.outFile,"REMARK %s \n",TITLE.c_str());
    fprintf(opened.outFile,"REMARK Log-Likelihood Gain %8.3f Z-Score %8.3f\n",
                       MRSET[t].RLIST[e].RF,MRSET[t].RLIST[e].RFZ);
    if (ense_chainids.find(MRSET[t].RLIST[e].MODLID) != ense_chainids.end())
    {
      dvect3 outR = MRSET[t].RLIST[e].EULER;
      dvect3 outT = dvect3(0,0,0);
      std::string m = MRSET[t].RLIST[e].MODLID;
      fprintf(opened.outFile,"REMARK ENSEMBLE %s EULER %6.2f %6.2f %6.2f FRAC % 5.3f % 5.3f % 5.3f\n",
                 m.c_str(),outR[0],outR[1],outR[2],outT[0],outT[1],outT[2]);
    }
    bool Space_Group_of_Solution_Set(RF_HALL != "");
    PHASER_ASSERT(Space_Group_of_Solution_Set);
    std::string SGNAME = space_group_name(RF_HALL,true).CCP4;
    int Z = SpaceGroup(RF_HALL).getSpaceGroupNSYMM()*MRSET[t].Z();
    fprintf(opened.outFile,"%s",UnitCell::CrystCard(SGNAME,Z).c_str());
    if (ense_chainids.find(MRSET[t].RLIST[e].MODLID) != ense_chainids.end())
    {
      dmat33 outR = euler2matrixDEG(MRSET[t].RLIST[e].EULER);
      std::string m = MRSET[t].RLIST[e].MODLID;
      mr_ndim tmp(m,outR);
      //the mapping of the input ensemble chain to the output chain
      map_str_str map_chains;
      for (int i = 0; i < ense_chainids[m].size(); i++)
      {
        if (DO_XYZOUT.CHAIN_COPY)
        {
          map_chains[ense_chainids[m][i]] = ense_chainids[m][i];
        }
        else
        {
          map_chains[ense_chainids[m][i]] = chains.allocate(ense_chainids[m][i]); //map the input to the next allowed output
          if (chains.allocate_failed())
            logWarning(SUMMARY,chains.message());
        }
      }
      std::string hall = RF_HALL;
      af::double6 unit_cell = getCell6();
      bool renumber_atoms(false);
      int atom_number(1); // not used as renumber is false;
      bool delete_overlaps(false);
      fprintf(opened.outFile,"%s",
        pdb_string_maker(PDB,map_chains,tmp,renumber_atoms,atom_number,hall,unit_cell,delete_overlaps,relative_wilsonB[m],tracemol).c_str());
    }
    fprintf(opened.outFile,"END\n");
    fclose(opened.outFile);
  }
}

void ResultMR_RF::setDotRlist(const mr_solution& rlist)
{
  MRSET.clear();
  //MRSET.copy_extras(rlist);//save for gyre purging
  sorted_index.clear();

  for (size_t index = 0; index < rlist.size(); index++)
  {
    dvect31D eulers;
    float1D  rfs;
    float1D  z_scores;
    mr_set   current = rlist[ index ];
    std::string model_name = "";

    if ( 0 < current.RLIST.size() )
    {
      std::set< std::string > model_ids;
      for(size_t rpi = 0; rpi < current.RLIST.size(); ++rpi)
      {
        eulers.push_back( current.RLIST[rpi].EULER );
        rfs.push_back( current.RLIST[rpi].RF );
        z_scores.push_back( current.RLIST[rpi].RFZ );
        model_ids.insert( current.RLIST[rpi].MODLID );
      }
      PHASER_ASSERT( model_ids.size() == 1 );
      model_name = *model_ids.begin();
    }
    current.RLIST.clear();
    bool1D deeps(rfs.size(),false);
    int1D clusters(rfs.size(),0);
    storeRotlist( model_name, current, eulers, rfs, z_scores, deeps, clusters);
  }
}

int ResultMR_RF::numRlist()
{
  int count(0);
  for (int t = 0; t < MRSET.size(); t++) count += MRSET[t].RLIST.size();
  return count;
}

std::vector<delel>  ResultMR_RF::finalSelection(floatType PURGE_PERCENT,floatType DOWN_PERCENT,int PURGE_NUMBER)
{
  return MRSET.purge_rf(PURGE_PERCENT,DOWN_PERCENT,PURGE_NUMBER);
}

} //phaser
