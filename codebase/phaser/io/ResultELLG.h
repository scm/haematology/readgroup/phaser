//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultELLG__Class__
#define __ResultELLG__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/include/data_ellg.h>

namespace phaser {

class ResultELLG : public Output, public DataA
{
  public:
    ResultELLG();
    ResultELLG(Output&);
    virtual ~ResultELLG() throw() {}

  private:
    search_array  SORT_SEARCH;
    void          check(std::string s) { PHASER_ASSERT(ELLG_DATA.find(s) != ELLG_DATA.end()); }
    std::map<double,int> TARGET_NRES;
    std::map<std::string,std::pair<int,double> > ELLG_COPY;

  public:
    std::map<std::string,data_ellg> ELLG_DATA;

  public:
    search_array getSearch() { return SORT_SEARCH; }
    void         setSearch(search_array& ss) { SORT_SEARCH = ss; }
    double       get_useful_resolution(std::string ens) { check(ens); return ELLG_DATA[ens].USEFUL_RESO; }
    double       get_target_resolution(std::string ens) { check(ens); return ELLG_DATA[ens].TARGET_RESO; }
    double       get_perfect_data_resolution(std::string ens) { check(ens); return ELLG_DATA[ens].PERFECT_RESO; }
    double       get_ellg_full_resolution(std::string ens) { check(ens); return ELLG_DATA[ens].ELLG; }
    void         add_target_nres(double rms,int nres)   { TARGET_NRES[rms] = nres; }
    int          get_target_nres() { return  !TARGET_NRES.size() ?  0: TARGET_NRES.begin()->second; }
    map_str_float get_map_chain_ellg(std::string ens)
    {
      check(ens); map_str_float tmp;
      for (std::map<std::string,data_cllg>::iterator iter = ELLG_DATA[ens].CHAIN.begin(); iter != ELLG_DATA[ens].CHAIN.end(); iter++)
        tmp[iter->first] = iter->second.ELLG;
      return tmp;
    }
    std::pair<double,int>  get_ellg_target_nres(int i)
    {
      std::map<double,int>::iterator iter = TARGET_NRES.begin();
      for (int j = 0; j < i && i < TARGET_NRES.size(); j++) iter++;
      return *iter;
    }

    map_str_float get_map_ellg_full_resolution();

    int get_modlid_ncopy(std::string s) { return ELLG_COPY.find(s)==ELLG_COPY.end()?0:ELLG_COPY[s].first; }
    double get_modlid_ncopy_ellg(std::string s) { return ELLG_COPY.find(s)==ELLG_COPY.end()?0:ELLG_COPY[s].second; }
    void add_modlid_ncopy_ellg(std::string s,int n,double e) { ELLG_COPY[s] = std::pair<int,double>(n,e); }

};

}

#endif
