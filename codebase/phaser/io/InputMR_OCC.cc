//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_OCC.h>

namespace phaser {

InputMR_OCC::InputMR_OCC(std::string cards) : InputMR_RNP(), MACO(), OCCU()  { parseCCP4(cards); }
InputMR_OCC::InputMR_OCC() {}
InputMR_OCC::~InputMR_OCC() {}

void InputMR_OCC::Analyse(Output& output)
{
  InputMR_RNP::Analyse(output);
  MACO::analyse();
  OCCU::analyse();
  //DO_XYZOUT.Override(true);

  if (!MRSET.size())
  throw PhaserError(INPUT,"","No solutions input");
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputMR_OCC::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += InputMR_RNP::Cards();
  cards += MACO::unparse();
  cards += OCCU::unparse();
  return cards;
}

} //phaser
