//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputEP_DAT.h>

namespace phaser {

InputEP_DAT::InputEP_DAT(std::string cards)
{ parseCCP4(cards); }

InputEP_DAT::InputEP_DAT() {}
InputEP_DAT::~InputEP_DAT() {}

//performs the analysis between correlated keyword Input
void InputEP_DAT::Analyse(Output& output)
{
  CELL::analyse();
  CRYS::analyse();
  HKLI::analyse();
  JOBS::analyse();
  MUTE::analyse();
  RESO::analyse();
  ROOT::analyse();
  SPAC::analyse();
  TITL::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);
}

std::string InputEP_DAT::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += CELL::unparse();
  cards += CRYS::unparse();
  cards += HKLI::unparse();
  cards += JOBS::unparse();
  cards += MUTE::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
