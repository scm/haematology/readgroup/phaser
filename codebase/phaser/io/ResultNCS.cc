//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultNCS.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

ResultNCS::ResultNCS() : Output(), DataA()
{
  ptwin5percent = false;
  MtzFile = SolFile= "";
}

ResultNCS::ResultNCS(Output& output) : Output(output), DataA()
{
  ptwin5percent = false;
  MtzFile = SolFile= "";
}

void ResultNCS::writeMtz(std::string HKLIN)
{
  MtzHandler mtzOut((*this),(*this),MILLER,true,HKLIN);
  std::string REFID = INPUT_INTENSITIES ? I_ID : F_ID;
  mtzOut.addData(MILLER,selected,getLaboutF(),"F",getCorrectedF(),REFID);
  mtzOut.addData(MILLER,selected,getLaboutSIGF(),"Q",getCorrectedSIGF(),REFID);
  if (PTNCS.EPSFAC.size())    mtzOut.addData(MILLER,selected,"NcsEps","R",PTNCS.EPSFAC,REFID);
  MtzFile = Output::Fileroot() + ".mtz";
  mtzOut.writeMtz(MtzFile);
  if (mtzOut.not_subset())
    logAdvisory(SUMMARY,"Output reflections not a subset of " + HKLIN);
}

af_string ResultNCS::getFilenames()
{
  af_string Filenames;
  if (MtzFile.size()) Filenames.push_back(MtzFile);
  if (SolFile.size()) Filenames.push_back(SolFile);
  return Filenames;
}

af_float ResultNCS::getEpsFac()
{
  af_float tmp(PTNCS.EPSFAC.size());
  for (int r = 0; r < PTNCS.EPSFAC.size(); r++) tmp[r] = PTNCS.EPSFAC[r];
  return tmp;
}

} //phaser
