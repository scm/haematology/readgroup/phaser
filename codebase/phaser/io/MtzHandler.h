//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MtzHandler__Class__
#define __MtzHandler__Class__
#include <phaser/main/Phaser.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <phaser/lib/safe_mtz.h>
#include <cctbx/hendrickson_lattman.h>

namespace phaser {

class MtzHandler : public SpaceGroup, public UnitCell
{
  public:
    MtzHandler(SpaceGroup&,UnitCell&,af::shared<miller::index<int> >,bool,
               std::string="",std::string="",std::string="");
    ~MtzHandler() {}

  private:
    int           NREFL;
    af_int        lookup;
    bool          hklin;
    bool          hklin_subset;
    CMtz::MTZ    *mtz;
    CMtz::MTZCOL *mtzH,*mtzK,*mtzL;
    CMtz::MTZCOL *mtzCol;
    void addCol(af::shared<miller::index<int> >,af_bool,std::string,std::string,af_float,std::string);

  public:
    void setWave(floatType,std::string);
    void addData(af::shared<miller::index<int> >,af_bool,std::string,std::string,float1D,std::string);
    void addData(af::shared<miller::index<int> >,af_bool,std::string,std::string,af_float,std::string);
    void addData(af::shared<miller::index<int> >,af_bool,std::string,std::string,af::shared<cctbx::hendrickson_lattman<double> >,std::string);

    void writeMtz(std::string);
    bool not_subset() { return hklin && !hklin_subset; }
};

}//phaser
#endif
