//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_AUTO__Class__
#define __InputMR_AUTO__Class__
#include <phaser/keywords/keywords.h>
#include <phaser/io/Output.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_AUTO :
                  public BFAC
                 ,public BINS
                 ,public BOXS
                 ,public CELL
                 ,public COMP
                 ,public ELLG
                 ,public ENSE
                 ,public FORM
                 ,public HKLI
                 ,public HKLO
                 ,public INFO
                 ,public JOBS
                 ,public KEYW
                 ,public LABI
                 ,public MACA
                 ,public MACM
                 ,public MACG
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public OCCU
                 ,public OUTL
                 ,public PACK
                 ,public PEAK
                 ,public PERM
                 ,public PURG
                 ,public RESC
                 ,public RESH
                 ,public RESO
                 ,public RFAC
                 ,public ROOT
                 ,public ROTA
                 ,public SAMP
                 ,public SEAR
                 ,public SGAL
                 ,public SOLP
                 ,public SOLU
                 ,public SPAC
                 ,public TARG
                 ,public TITL
                 ,public TNCS
                 ,public TOPF
                 ,public TRAN
                 ,public VERB
                 ,public XYZO
                 ,public ZSCO
                 ,public KILL
{
  public:
    InputMR_AUTO(std::string);
    InputMR_AUTO();
    ~InputMR_AUTO();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
