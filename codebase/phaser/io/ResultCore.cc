//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultCore.h>

namespace phaser {

void ResultCore::store_relative_wilsonB(map_str_float2D relative_wilsonB_)
{
  //adds or overwrites value stored in array
  for (map_str_float2D::iterator iter = relative_wilsonB_.begin(); iter != relative_wilsonB_.end(); iter++)
    relative_wilsonB[iter->first] = iter->second;
}

void ResultCore::set_pdb_chainids(map_str_pdb& pdb,bool chainocc)
{
  PDB = pdb;
  CHAINOCC = chainocc;
  //get the set of chains in each ensemble
  ense_chainids.clear();
  //maps modlid to a set of chains in that modlid
  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
    if (!iter->second.ENSEMBLE[0].map_format())
      ense_chainids[iter->first] = iter->second.Coords().getChains(chainocc); //only need to map the mtrace molecule chainids
    else
      ense_chainids[iter->first].push_back(std::string(DEF_MAP_CHAIN));
}

} //phaser
