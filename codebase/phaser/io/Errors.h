//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PhaserErrorClass__
#define __PhaserErrorClass__
#include <exception>
#include <string>
#include <cstdlib>

namespace phaser {

#define PHASER_NOT_IMPLEMENTED() ::phaser::error(__FILE__, __LINE__, \
              "Not implemented.")
#define PHASER_ASSERT(bool) \
  if (!(bool)) throw ::phaser::error(__FILE__, __LINE__,\
    "Consistency check (" # bool ") failed.")

class error : public std::exception
{
  public:
    explicit
    error(std::string const& msg) throw();

    error(const char* file, long line, std::string const& msg = "",
          bool internal = true) throw();

    virtual ~error() throw();

    virtual const char* what() const throw();

  protected:
    std::string msg_;
};

// these error codes are offset with 64 as in
// /usr/include/sysexists.h
enum errorType {
  NULL_ERROR, //0
  SYNTAX,     //64
  INPUT,      //65
  FILEOPEN,   //66
  MEMORY,     //67
  KILLFILE,   //68
  KILLTIME,   //69
  FATAL,      //70
  UNHANDLED,  //71
  UNKNOWN     //72
};

class PhaserError : public std::exception
{
  protected:
    errorType   type;
    std::string longmessage;
    std::string message;
    std::string echo;
    static const int EXIT_BASE = 63;
  public:
    PhaserError() : type(NULL_ERROR), longmessage(""), message(""), echo("") { MakeLongMessage(); }
    PhaserError(errorType t): type(t), message(""),echo("") { MakeLongMessage(); }
    PhaserError(errorType t,std::string m): type(t), message(m),echo("") { MakeLongMessage(); }
    PhaserError(errorType t,const char* m): type(t), message(m),echo("") { MakeLongMessage(); }
    PhaserError(errorType t,std::string e,std::string m): type(t), message(m),echo(e) { MakeLongMessage(); }

    void setPhaserError(PhaserError err) { type=err.ErrorType(); message=err.ErrorMessage(); echo=err.Cards(); }
    void setPhaserError(errorType t,std::string m) { type=t; message=m; }
    void MakeLongMessage();
    errorType   ErrorType() const  { return type; }
    std::string ErrorMessage() const { return ErrorMessageChar(); }
    const char* ErrorMessageChar() const { return message.c_str(); }
    std::string ErrorName() const { return ErrorNameChar(); }
    const char* ErrorNameChar() const;
    bool        Success() const { return (type == NULL_ERROR); }
    bool        Failure() const { return (type != NULL_ERROR); }
    int         ExitCode() const { return Success() ? EXIT_SUCCESS: (type + EXIT_BASE); }
    std::string Cards() const   { return echo; }
//virtual
    virtual const char* what() const throw();
    virtual ~PhaserError() throw() {}
};




} //phaser


#endif
