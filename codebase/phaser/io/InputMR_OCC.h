//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_OCC__Class__
#define __InputMR_OCC__Class__
#include <phaser/io/InputMR_RNP.h>
#include <phaser/keywords/ELLG.h>
#include <phaser/keywords/MACO.h>
#include <phaser/keywords/OCCU.h>

namespace phaser {

class InputMR_OCC : public InputMR_RNP,
                    public MACO,
                    public OCCU
{
  public:
    InputMR_OCC(std::string);
    InputMR_OCC();
    ~InputMR_OCC();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
