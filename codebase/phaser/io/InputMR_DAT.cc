//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_DAT.h>

namespace phaser {

InputMR_DAT::InputMR_DAT(std::string cards)
{ parseCCP4(cards); }

InputMR_DAT::InputMR_DAT() {}
InputMR_DAT::~InputMR_DAT() {}

//performs the analysis between correlated keyword Input
void InputMR_DAT::Analyse(Output& output)
{
  CELL::analyse();
  HKLI::analyse();
  HKLO::analyse();
  LABI::analyse();
  MUTE::analyse();
  RESO::analyse();
  ROOT::analyse();
  SGAL::analyse();
  SPAC::analyse();
  TITL::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  if (HKLIN == "" && !REFLECTIONS.MILLER.size())
  throw PhaserError(INPUT,keywords,"No HKLIN filename");

  if (SG_HALL.size() && !REFLECTIONS.SG_HALL.size())
    REFLECTIONS.SG_HALL = SG_HALL;
  if (!SG_HALL.size() && REFLECTIONS.SG_HALL.size())
    SG_HALL = REFLECTIONS.SG_HALL;
  if (!SGALT_BASE.size() && REFLECTIONS.SG_HALL.size())
    SGALT_BASE = REFLECTIONS.SG_HALL;

  DO_HKLOUT.Default(false);
}

std::string InputMR_DAT::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += CELL::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += LABI::unparse();
  cards += MUTE::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SGAL::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
