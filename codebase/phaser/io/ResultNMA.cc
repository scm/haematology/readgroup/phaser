//(i)/2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultNMA.h>
#include <phaser/io/ResultFile.h>
#include <phaser/src/Molecule.h>
#include <phaser/src/UnitCell.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>
#include <phaser/io/pdb_chains_2chainid.h>
#include <phaser/include/data_sceds.h>
#include <phaser/include/data_ddm.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultNMA::ResultNMA() : Output()
{
  RMS = 0;
}

ResultNMA::ResultNMA(Output& output) : Output(output)
{
  RMS = 0;
}

af_float ResultNMA::getDisplacements(int i)
{
  af_float displacements;
  if (i > dqmodes.size()) return displacements;
  for (int j = 0; j < dqmodes[i].size(); j++)
    displacements.push_back(dqmodes[i][j].first);
  return displacements;
}

af_int  ResultNMA::getModes(int i)
{
  af_int modes;
  if (i > dqmodes.size()) return modes;
  for (int j = 0; j < dqmodes[i].size(); j++)
    modes.push_back(dqmodes[i][j].second);
  return modes;
}

void ResultNMA::writePdb(af_bool write_pdb,bool write_separate)
{
//write the files
  int totpdb(0);
  for (int r = 0; r < dqmodes.size(); r++) if (write_pdb[r]) totpdb++;
  int npdb(0);
  PHASER_ASSERT(dqmodes.size() == write_pdb.size());
  std::string pdbfile = Output::Fileroot() + ".nma.pdb";
  PdbFiles.push_back(pdbfile);
  ResultFile models(pdbfile);
  fprintf(models.outFile,"REMARK TITLE %s\n",TITLE.c_str());
  fprintf(models.outFile,"REMARK Phaser Normal Mode Analysis\n");
  fprintf(models.outFile,"REMARK Perturb %s\n",MODLID.c_str());
  int NMODEL(1);
  for (int r = 0; r < dqmodes.size(); r++)
  if (write_pdb[r])
  {
    fprintf(models.outFile,"MODEL %8i\n",NMODEL++);
    npdb++;
    std::string pdbfile = Output::Fileroot() + "."
                          + std::string((totpdb > 999 && npdb < 1000) ? "0" : "")
                          + std::string((totpdb > 99 && npdb < 100) ? "0" : "")
                          + std::string((totpdb > 9 && npdb < 10) ? "0" : "")
                          + itos(npdb) + ".pdb";
    ResultFile opened;
    if (write_separate)
    {
    opened.open(pdbfile);
    PdbFiles.push_back(pdbfile);
    fprintf(opened.outFile,"REMARK TITLE %s\n",TITLE.c_str());
    fprintf(opened.outFile,"REMARK Phaser Normal Mode Analysis\n");
    fprintf(opened.outFile,"REMARK Perturb %s\n",MODLID.c_str());
    }
    for (int u = 0; u < dqmodes[r].size(); u++)
    {
      std::string remark = "REMARK Displacement dq= " + dtos(dqmodes[r][u].first) + " along mode=" + itos(dqmodes[r][u].second);
      if (write_separate) fprintf(opened.outFile,"%s\n",remark.c_str());
    }
    pdb_record last_card;
    for (unsigned a = 0; a < molecule.nAtoms(); a++)
    {
      pdb_record this_card = molecule.card(a);
      for (int i = 0; i < 3; i++)
      {
        for (int u = 0; u < dqmodes[r].size(); u++)
          this_card.X[i] -= dqmodes[r][u].first*U(3*a+i+1,U.num_cols()-dqmodes[r][u].second-1);
      }

      if (a && last_card.ResNum < this_card.ResNum-1 && last_card.Chain == this_card.Chain &&
          std::sqrt((last_card.X-this_card.X)*(last_card.X-this_card.X)) > 4.0) //not antibody numbering
      {
        if (write_separate) this_card.writeTer(opened.outFile);
        this_card.writeTer(models.outFile);
      }
      if (write_separate) this_card.writeCard(opened.outFile);
      this_card.writeCard(models.outFile);
      last_card = this_card;
    }
    if (write_separate) { fclose(opened.outFile); }

    logTab(1,LOGFILE,itos(npdb) + ": " + pdbfile);
    for (int u = 0; u < dqmodes[r].size(); u++)
      logTab(2,LOGFILE,"Displacement dq= " + dtos(dqmodes[r][u].first) + " along mode=" + itos(dqmodes[r][u].second));
    fprintf(models.outFile,"ENDMDL\n");
  }
  fprintf(models.outFile,"END\n");
  fclose(models.outFile);
  if (npdb) logBlank(LOGFILE);
}

void ResultNMA::writeScript()
{
  if (PdbFiles.size())
  {
    SolFile = Output::Fileroot() + ".sol";
    ResultFile opened(SolFile);

    fprintf(opened.outFile,"# %s\n",TITLE.c_str());
    for (int f = 0; f < PdbFiles.size(); f++)
    {
      std::string modlid(MODLID+"_"+itos(f+1));
      fprintf(opened.outFile,"ENSEMBLE %s PDB %s RMS %5.3f\n",
        modlid.c_str(),PdbFiles[f].c_str(),RMS);
    }
    fprintf(opened.outFile,"SEARCH &\n");
    for (int f = 0; f < PdbFiles.size()-1; f++)
    {
      std::string modlid(MODLID+"_"+itos(f+1));
      fprintf(opened.outFile,"  ENSEMBLE %s OR &\n", modlid.c_str());
    }
    std::string modlid(MODLID+"_"+itos(PdbFiles.size()));
    fprintf(opened.outFile,"  ENSEMBLE %s\n", modlid.c_str());

    fclose(opened.outFile);
  }
}

af_string ResultNMA::getFilenames()
{
  af_string filenames;
  filenames.insert(filenames.end(),PdbFiles.begin(),PdbFiles.end());
  if (SolFile.size()) filenames.push_back(SolFile);
  if (MatFile.size()) filenames.push_back(MatFile);
  return filenames;
}

void ResultNMA::calcDomains(af_bool write_pdb,data_sceds SCEDS,data_ddm NMADDM)
{
  use_trace = true;
  NDOM = SCEDS.NDOM;
  std::vector<dq_type> dqmodes_select;
  for (int r = 0; r < dqmodes.size(); r++)
  if (write_pdb[r])
    dqmodes_select.push_back(dqmodes[r]);
  int nTrace(0);
  for (unsigned a = 0; a < molecule.nAtoms(); a++) //<= for off end test
     if (molecule.isTraceAtom(0,a)) nTrace++;
  int array_size = use_trace ? nTrace : molecule.nAtoms();
  int JOIN_MIN = std::ceil(array_size*NMADDM.JOIN_MIN); //stored as <1 2% default
  int JOIN_MAX = std::ceil(array_size*NMADDM.JOIN_MAX); //stored as <1 12% default
  //both odd or both even
  if (!(JOIN_MIN % 2) && (JOIN_MAX %2)) JOIN_MAX++;
  else if ((JOIN_MIN % 2) && !(JOIN_MAX %2)) JOIN_MAX++;
  PHASER_ASSERT(JOIN_MIN <= JOIN_MAX);
  logUnderLine(LOGFILE,"Domain Analysis Parameters");
  logTab(1,LOGFILE,"Number of Domains (NDOM): " + itos(NDOM));
  logTab(1,LOGFILE,"DDM Matrix Size         : " + itos(array_size));
  logTab(1,LOGFILE,"DDM Cutoff Range (Step) : " + dtos(floatType(NMADDM.DISTANCE_MIN)/floatType(NMADDM.DISTANCE_STEP)*100,5,2) + "%->" + dtos(floatType(NMADDM.DISTANCE_MAX)/floatType(NMADDM.DISTANCE_STEP)*100,5,2) + "% (" + dtos(100./floatType(NMADDM.DISTANCE_STEP),5,2)  + "%)");
  logTab(1,LOGFILE,"DDM Sequence Separation : " + itos(NMADDM.SEQ_MIN) + "->" + itos(NMADDM.SEQ_MAX));
  logTab(1,LOGFILE,"DDM Distance Separation : " + dtos(NMADDM.SEPARATION_MIN) + "->" + dtos(NMADDM.SEPARATION_MAX)  + " Angstroms");
  (NMADDM.SLIDER) ?
  logTab(1,LOGFILE,"DDM Smoothing Window    : " + itos(-NMADDM.SLIDER) + "->" + itos(NMADDM.SLIDER)):
  logTab(1,LOGFILE,"DDM Smoothing Window    : None");
  logTab(1,VERBOSE,"DDM Cluster Assignment  : " + NMADDM.CLUSTER);
  (JOIN_MAX == JOIN_MIN) ?
  logTab(1,LOGFILE,"Join Fragments # (%)    : " + itos(JOIN_MIN) + " (" + dtos(100.*NMADDM.JOIN_MIN) +  "->" +  dtos(100*NMADDM.JOIN_MAX) + "%)"):
  logTab(1,LOGFILE,"Join Fragments # (%)    : " + itos(JOIN_MIN) + "->" + itos(JOIN_MAX) + " (" + dtos(100.*NMADDM.JOIN_MIN) +  "->" +  dtos(100*NMADDM.JOIN_MAX) + "%)");
  bool CLUSTER_FIRST(NMADDM.CLUSTER == "FIRST");
  bool CLUSTER_LAST(NMADDM.CLUSTER == "LAST");
  bool CLUSTER_MAX(NMADDM.CLUSTER == "MAX");
  PHASER_ASSERT(CLUSTER_FIRST || CLUSTER_LAST || CLUSTER_MAX);
  logUnderLine(LOGFILE,"SCEDS Weights");
  logTab(1,LOGFILE,"Sphericity : " + std::string(SCEDS.wSPHERICITY? dtos(SCEDS.wSPHERICITY) : "0 (Not Used)"));
  logTab(1,LOGFILE,"Continuity : " + std::string(SCEDS.wCONTINUITY?  dtos(SCEDS.wCONTINUITY) : "0 (Not Used)"));
  logTab(1,LOGFILE,"Equality   : " + std::string(SCEDS.wEQUALITY? dtos(SCEDS.wEQUALITY) : "0 (Not Used)"));
  logTab(1,LOGFILE,"Density    : " + std::string(SCEDS.wDENSITY?  dtos(SCEDS.wDENSITY) : "0 (Not Used)"));

  floatType maximal_density = use_trace ? 0.0071 : 0.06;
  //values calculated from a set of structures: see #if 0 code in EnsemblePDB.cc

  logBlank(LOGFILE);
  logTab(1,LOGFILE,"There are "+itos(dqmodes_select.size())+" mode combinations");

  int nthreads = 1;
#pragma omp parallel
  {
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
#endif
  }
  if (nthreads > 1)
    logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
  float1D  omp_best_DS(nthreads,0);
  float1D  omp_best_distance(nthreads,0);
  int1D    omp_best_istep(nthreads,0);
  int1D    omp_best_join(nthreads,0);
  int1D    omp_best_seq_sep(nthreads,0);
  int1D    omp_best_dq(nthreads);
  int1D    omp_best_ndom(nthreads);
  float1D  omp_best_continuity(nthreads,0);
  int2D    omp_best_domain(nthreads);
  float1D  omp_best_sphericity(nthreads,0);
  float1D  omp_best_equality(nthreads,0);
  float1D  omp_best_density(nthreads,0);
  std::vector<std::vector<std::pair<int,int> > >  omp_best_natoms_dom(nthreads);

  logProgressBarStart(LOGFILE,"Analysing Mode Combinations",dqmodes_select.size()/nthreads);

#pragma omp parallel for
  for (int r = 0; r < dqmodes_select.size(); r++)
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
    PHASER_ASSERT(nthread < nthreads);
#endif
    dvect31D X(array_size);
    dvect31D Y(array_size);
    int ta(0);
    for (unsigned a = 0; a < molecule.nAtoms(); a++)
    if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
    {
      //error scaled
      pdb_record this_card = molecule.card(a);
      X[ta] = this_card.X;
      for (int i = 0; i < 3; i++)
      {
        for (int u = 0; u < dqmodes_select[r].size(); u++)
          this_card.X[i] -= dqmodes_select[r][u].first*U(3*a+i+1,U.num_cols()-dqmodes_select[r][u].second-1);
      }
      Y[ta] = this_card.X;
      ta++;
    }

    //calcalate stats and create average ddm matrix
    float2D slider_ddm;
    floatType minE(1000000000),maxE(0),avE(0);
    { //memory
      floatType nav(0);
      float2D ddm(array_size);
      slider_ddm.resize(array_size);
      int ta(0);
      for (unsigned a = 0; a < molecule.nAtoms(); a++)
      if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
      {
        ddm[ta].resize(array_size);
        slider_ddm[ta].resize(array_size);
        int tb(0);
        for (unsigned b = 0; b < molecule.nAtoms(); b++)
        if (!use_trace || (use_trace && molecule.isTraceAtom(0,b)))
        {
          dvect3 d1 = X[ta]-X[tb];
          floatType d1sqr = d1*d1;
          dvect3 d2 = Y[ta]-Y[tb];
          floatType d2sqr = d2*d2;
          floatType E = std::sqrt(d1sqr)-std::sqrt(d2sqr);
          ddm[ta][tb] = E;
          tb++;
        }
        ta++;
      }
      ta = 0;
      for (unsigned a = 0; a < ddm.size(); a++)
      if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
      {
        int tb(0);
        for (unsigned b = 0; b < ddm[a].size(); b++) //must start from zero for trace
        if (!use_trace || (use_trace && molecule.isTraceAtom(0,b)))
        {
          floatType av_ddm(0),nslider(0);
          for (int sa = -NMADDM.SLIDER; sa <= NMADDM.SLIDER; sa++)
          for (int sb = -NMADDM.SLIDER; sb <= NMADDM.SLIDER; sb++)
          {
            if (ta+sa >=0  && ta+sa < ddm.size() &&
                tb+sb >=0  && tb+sb < ddm[ta].size())
            {
              floatType weight = (1.0/std::exp(std::fabs(floatType(sa))))*(1.0/std::exp(std::fabs(floatType(sb))));
              av_ddm += weight*std::fabs(ddm[ta+sa][tb+sb]);
              nslider += weight;
            }
          }
          av_ddm /= nslider;
         // av_ddm = std::fabs(ddm[ta][tb]);
          slider_ddm[ta][tb] = av_ddm;
          minE = std::min(minE,av_ddm);
          maxE = std::max(maxE,av_ddm);
          avE += av_ddm;
          nav++;
          tb++;
        }
        ta++;
      }
      avE /= nav;
    }
    floatType stepE = (maxE-minE)/floatType(NMADDM.DISTANCE_STEP);
    for (int seq_sep = NMADDM.SEQ_MIN; seq_sep <= NMADDM.SEQ_MAX; seq_sep++)
    for (floatType distance = NMADDM.SEPARATION_MIN; distance <= NMADDM.SEPARATION_MAX; distance++)
    for (int istep = NMADDM.DISTANCE_MIN; istep <= NMADDM.DISTANCE_MAX; istep++)
    {
      floatType nextE = minE + istep*stepE ;
      int1D this_domain(array_size,0); //flag 0 unassigned atom
      int this_ndom(1);
      int ta(0);
      for (unsigned a = 0; a < molecule.nAtoms(); a++)
      if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
      {
        if (this_domain[ta]) { ta++; continue; }
        std::set<int> similar;
        int tb(0);
        for (unsigned b = 0; b < molecule.nAtoms(); b++) //have to start from 0 for tb count
        if (!use_trace || (use_trace && molecule.isTraceAtom(0,b)))
        {
          if (tb > ta + seq_sep) //make selection for b > a here
          {
            floatType distXYsqr = (X[ta]-X[tb])*(X[ta]-X[tb]);
            //this is the condition for making them the same
            //ddm the same
            //distance within limits
            if (slider_ddm[ta][tb] < nextE && distXYsqr < fn::pow2(distance))
              similar.insert(tb);
          }
          tb++;
        }
        //loop over the set of atoms in the same domain, first pass
        //set the domain to the domain of the first non-zero in the set, if any
        //this will be the closest in sequence
        int ddfirst(0);
        std::map<int,int> ddcount;
        for (std::set<int>::iterator iter = similar.begin(); iter != similar.end(); iter++)
        if (this_domain[*iter])
        {
          if (((CLUSTER_FIRST || CLUSTER_MAX) && !ddfirst) || CLUSTER_LAST)
            ddfirst = this_domain[*iter];
          ddcount[this_domain[*iter]]++;
        }
        int dd = ddfirst;
        if (CLUSTER_MAX)
        {
          int ddmax = ddcount[ddfirst];
          //set the domain to the domain count highest in the set, if any
          for (std::map<int,int>::iterator iter = ddcount.begin(); iter != ddcount.end(); iter++)
          if (iter->second > ddmax)
          {
            dd = iter->first;
            ddmax = iter->second;
          }
        }
        if (dd)
        {
          this_domain[ta] = dd; //first is labelled as same as in similar list
          for (std::set<int>::iterator iter = similar.begin(); iter != similar.end(); iter++)
            if (!this_domain[*iter]) this_domain[*iter] = dd;
        }
        else
        {
          this_domain[ta] = this_ndom;
          for (std::set<int>::iterator iter = similar.begin(); iter != similar.end(); iter++)
            this_domain[*iter] = this_ndom;
          this_ndom++;
        }
        ta++;
      }
      PHASER_ASSERT(this_ndom > 0);
      int1D natoms(this_ndom,0);
      for (unsigned t = 0; t < array_size; t++)
        natoms[this_domain[t]]++;
      std::vector<std::pair<int,int> > natoms_dom; //sort on first
      for (unsigned d = 0; d < natoms.size(); d++)
        natoms_dom.push_back(std::pair<int,int>(natoms[d],d));
      std::sort(natoms_dom.begin(),natoms_dom.end());
      std::reverse(natoms_dom.begin(),natoms_dom.end());
      std::vector<std::pair<int,int> > natoms_dom_orig = natoms_dom; //sort on first
      for (int join = JOIN_MIN; join <= JOIN_MAX; join+=2)
      {
        natoms_dom = natoms_dom_orig;
        int1D before_natom(NDOM);
        std::string before_ostr;
        int before_nextra(0);
        { //memory
          for (int n = NDOM;  n < natoms_dom.size(); n++)
            before_nextra += natoms_dom[n].first;
          for (int n = 0; n < std::min(this_ndom,NDOM)-1; n++)
            before_ostr += itos(natoms_dom[n].first) + "/";
          if (NDOM < natoms_dom.size())
            before_ostr +=  itos(natoms_dom[NDOM-1].first);
          for (unsigned dom = 0; dom < std::min(this_ndom,NDOM); dom++)
          {
            int ta(0);
            int last_ta(-999);
            for (unsigned a = 0; a < molecule.nAtoms(); a++) //<= for off end test
            if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)) || a == molecule.nAtoms()) //off the end
            {
              if (this_domain[ta] == natoms_dom[dom].second || ta == nTrace-1 || ta == nTrace)
              { //if ta matches the domain of interest
                if (last_ta >= 0 && (ta - last_ta) <= join)
                { //if the last section does not
                  for (int tb = last_ta; tb <= ta && tb < this_domain.size(); tb++) //<= for last atom
                  if (this_domain[tb] != natoms_dom[dom].second)
                  {
                    this_domain[tb] = natoms_dom[dom].second;
                  }
                }
                if (ta < nTrace-1) last_ta = ta;
              }
              ta++;
            }
          }
          for (unsigned dom = 0; dom < natoms_dom.size(); dom++)
          {
            natoms_dom[dom].first = 0;
            for (unsigned t = 0; t < this_domain.size(); t++)
            {
              if (this_domain[t] == natoms_dom[dom].second)
                natoms_dom[dom].first++;
            }
          }
          std::sort(natoms_dom.begin(),natoms_dom.end());
          std::reverse(natoms_dom.begin(),natoms_dom.end());
        }

        floatType sphericity(1),density(1);
        //if (SCEDS.wSPHERICITY|| SCEDS.wDENSITY)
        {
          for (int dom = 0; dom < std::min(this_ndom,NDOM); dom++)
          {
            xyz_weight cw;
            for (int ta = 0; ta < this_domain.size(); ta++)
            if (this_domain[ta] == natoms_dom[dom].second)
            {
              xyzw this_xyzw;
              this_xyzw.xyz = X[ta];
              this_xyzw.weight = 1;
              cw.push_back(this_xyzw);
            }
            cw.calculate();
            dvect3 box = cw.PRINCIPAL_EXTENT;
            std::sort(box.begin(),box.end());

            //sphericity = 1; //product
            //if (SCEDS.wSPHERICITY)
            {
              floatType dom_sphericity(1);
              if (box[2] && box[1] && box[0])
              {
                //Knud Thomsen's Formula approximating surface area
                floatType p = 1.6075;
                floatType radius(2);
                floatType ap = std::pow(box[0]/radius,p);
                floatType bp = std::pow(box[1]/radius,p);
                  floatType cp = std::pow(box[2]/radius,p);
                floatType Ap = 4.*scitbx::constants::pi*std::pow(((ap*bp+ap*cp+bp*cp)/3),1/p);
                floatType Vp = 4./3.*scitbx::constants::pi*box[0]*box[1]*box[2]/fn::pow3(radius);
                dom_sphericity = std::pow(scitbx::constants::pi,1./3.)*std::pow(6*Vp,2./3.)/Ap;
                  dom_sphericity = std::min(dom_sphericity,1.); //to account for (maximal) 1.061% error
              }
              else dom_sphericity = 0;
              sphericity *= dom_sphericity; //Citrate synthase needs upweighted sphericity term
            } //sphericity

            //density = 1; //product
            //if (SCEDS.wDENSITY)
            {
              floatType dom_density(1);
              if (box[2] && box[1] && box[0])
              {
                //if there are only a few atoms then they can all be at the edge, and the volume is too low
                floatType CAbond = 3.8;
                floatType box0 = box[0]+CAbond;
                floatType box1 = box[1]+CAbond;
                floatType box2 = box[2]+CAbond;
                floatType Vp = 4./3.*scitbx::constants::pi*box0*box1*box2/8.; //radius /2/2/2
                dom_density = cw.size()/Vp;
                dom_density /= maximal_density; //calculated over many structures
                dom_density = std::min(dom_density,1.); //maximally top density
              }
              else dom_density = 0;
              density *= dom_density;
            } //density
          }
        } //sphericity and density

        floatType continuity(1);
        //if (SCEDS.wCONTINUITY)
        {
          for (int dom = 0; dom < std::min(this_ndom,NDOM); dom++)
          {
            floatType nchange(1); //number of breaks between domains
            int ta(0),start_dom(false);
            int1D intron;
            for (unsigned a = 0; a < molecule.nAtoms(); a++)
            if (!use_trace || (use_trace && molecule.isTraceAtom(0,a))) //off the end
            {
              if (!start_dom && this_domain[ta] == natoms_dom[dom].second)
                start_dom = true;
              else if (start_dom && this_domain[ta] != natoms_dom[dom].second)
                intron.push_back(ta);
              else
              {
                bool different_dom(false);
                for (unsigned tb = 0; tb < intron.size() && !different_dom; tb++)
                  for (int d = 0; d < std::min(this_ndom,NDOM) && !different_dom; d++)
                    if (d != dom && this_domain[intron[tb]] == natoms_dom[d].second)
                      different_dom = true;
                if (different_dom) nchange++;
                intron.clear();
              }
              ta++;
            }
            continuity *= 1./nchange;
          }
        }

        floatType equality(1);
        //if (SCEDS.wEQUALITY)
        {
          floatType fNDOM = NDOM;//compilation error on mac
          floatType this_domain_size = this_domain.size();//compilation error on mac
          equality = std::pow(fNDOM,NDOM)/std::pow(this_domain_size,NDOM);
          for (int d = 0; d < NDOM; d++)
            equality *= natoms_dom[d].first;
          floatType nextra(0);
          for (int n = NDOM;  n < best_natoms_dom.size(); n++)
            nextra += natoms_dom[n].first;
        }

        //TARGET FUNCTION
        floatType DS = SCEDS.wEQUALITY*   equality +
                       SCEDS.wSPHERICITY* sphericity +
                       SCEDS.wDENSITY*    density +
                       SCEDS.wCONTINUITY* continuity;
        if (this_ndom < NDOM) DS = 0;
        bool less_than_NDOM(false);
        for (int i = 0; i < natoms_dom.size(); i++)
          if (i < NDOM && natoms_dom[i].first == 0) less_than_NDOM = true;
        if (less_than_NDOM) DS = 0;

        if (DS > omp_best_DS[nthread])
        {
          omp_best_DS[nthread] = DS;
          omp_best_domain[nthread] = this_domain;
          omp_best_natoms_dom[nthread] = natoms_dom;
          omp_best_distance[nthread] = distance;
          omp_best_seq_sep[nthread] = seq_sep;
          omp_best_istep[nthread] = istep;
          omp_best_join[nthread] = join;
          omp_best_dq[nthread] = r;
          omp_best_ndom[nthread] = this_ndom;
          omp_best_sphericity[nthread] = sphericity;
          omp_best_density[nthread] = density;
          omp_best_equality[nthread] = equality;
          omp_best_continuity[nthread] = continuity;
          int nextra(0);
          for (int n = NDOM;  n < natoms_dom.size(); n++)
            nextra += natoms_dom[n].first;
          if (nthreads == 1)
          {
            logBlank(LOGFILE);
            std::string ostr;
            for (int n = 0; n < NDOM-1; n++)
              ostr +=  itos(natoms_dom[n].first) + "/";
            ostr +=  itos(natoms_dom[NDOM-1].first);
            std::string mstr;
            for (int u = 0; u < dqmodes_select[r].size(); u++)
            if (dqmodes_select[r][u].first != 0)
              mstr += itos(dqmodes_select[r][u].second) + " ";
            logTab(2,LOGFILE,"New Best SCEDS = " + dtos(DS) + " (" + itos(this_ndom-1) + " domains)"); // 0 is not a domain (flag)
            logTab(3,LOGFILE,"Mode Combination #" + itos(r+1) + " = " + mstr);
            logTab(3,LOGFILE,"Domain Division #" + std::string(use_trace ? "CA " : "") + "atoms: " + ostr + " (" + itos(nextra) + ")" + " (before join " + before_ostr + " (" + itos(before_nextra) + "))");
            std::string tmp;
            tmp += std::string(SCEDS.wSPHERICITY ? "Sphericity: " + dtos(sphericity,4,2) + " ": "");
            tmp += std::string(SCEDS.wCONTINUITY ? "Continuity: " + dtos(continuity,4,2): "");
            tmp += std::string(SCEDS.wEQUALITY ? "Equality: " + dtos(equality,4,2) + " ": "");
            tmp += std::string(SCEDS.wDENSITY ? "Density: " + dtos(density,4,2) + " ": "");
            logTab(4,LOGFILE,tmp);
            logTab(4,LOGFILE,"Sequence: " + itos(seq_sep)+  "  Distance: " + dtos(distance) + "  DDM: " + dtos(100*floatType(istep)/floatType(NMADDM.DISTANCE_STEP),5,2) + "%  Join: " + itos(join));
            logProgressBarAgain(LOGFILE);
          }
        } //best
      } //join
    } //triple loop
    if (nthread == 0) logProgressBarNext(LOGFILE);
  }
  logProgressBarEnd(LOGFILE);

//collect best overall from threads
  floatType   best_DS(0),best_distance(0),best_sphericity(0),best_density(0),best_equality(0),best_continuity(0);
  int         best_ndom(0),best_dq(0),best_seq_sep(0),best_istep(0),best_join(0);
  for (int nthread = 0; nthread < omp_best_DS.size(); nthread++)
  if (omp_best_DS[nthread] > best_DS)
  {
    best_DS = omp_best_DS[nthread];
    best_domain = omp_best_domain[nthread];
    best_natoms_dom = omp_best_natoms_dom[nthread];
    best_seq_sep = omp_best_seq_sep[nthread];
    best_distance = omp_best_distance[nthread];
    best_istep = omp_best_istep[nthread];
    best_join = omp_best_join[nthread];
    best_dq = omp_best_dq[nthread];
    best_ndom = omp_best_ndom[nthread];
    best_sphericity = omp_best_sphericity[nthread];
    best_density = omp_best_density[nthread];
    best_equality = omp_best_equality[nthread];
    best_continuity = omp_best_continuity[nthread];
  }
  logUnderLine(LOGFILE,"Best Domain Division");
  logTab(1,LOGFILE,"SCEDS = ws*Sphericity + wc*Continuity + we*Equality + wd*Density");
  std::string tmp;
  tmp += dtos(SCEDS.wSPHERICITY) + "*" + dtos(best_sphericity,4,2) + " + ";
  tmp += dtos(SCEDS.wCONTINUITY) + "*" + dtos(best_continuity,4,2) + " + ";
  tmp += dtos(SCEDS.wEQUALITY) + "*" + dtos(best_equality,4,2) + " + ";
  tmp += dtos(SCEDS.wDENSITY) + "*" + dtos(best_density,4,2);
  logTab(3,LOGFILE,"= " + tmp);
  tmp  = dtos(SCEDS.wSPHERICITY*best_sphericity,4,2) + " + ";
  tmp += dtos(SCEDS.wCONTINUITY*best_continuity,4,2) + " + ";
  tmp += dtos(SCEDS.wEQUALITY*best_equality,4,2) + " + ";
  tmp += dtos(SCEDS.wDENSITY*best_density,4,2);
  logTab(3,LOGFILE,"= " + tmp);
  logTab(3,LOGFILE,"= " + dtos(best_DS,4,2));
  std::string mstr;
  for (int u = 0; u < dqmodes_select[best_dq].size(); u++)
  if (dqmodes_select[best_dq][u].first != 0)
    mstr += itos(dqmodes_select[best_dq][u].second) + " ";
  std::string ostr;
  for (int n = 0; n < NDOM-1; n++)
    ostr += itos(best_natoms_dom[n].first) + "/";
  ostr += itos(best_natoms_dom[NDOM-1].first);
  logTab(1,LOGFILE,"Mode Combination = " + mstr);
  int nextra(0);
  for (int n = NDOM;  n < best_natoms_dom.size(); n++)
    nextra += best_natoms_dom[n].first;
  logTab(1,LOGFILE,"Domain Division #" + std::string(use_trace ? "CA " : "") + "atoms: " + ostr + " (" + itos(nextra) + " excluded)");
  std::vector<std::set<int> >  domain_set(NDOM+1);
  for (unsigned dom = 0; dom < best_natoms_dom.size(); dom++)
  {
    int ta(0);
    for (unsigned a = 0; a < molecule.nAtoms(); a++)
    if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
    {
      if (best_domain[ta] == best_natoms_dom[dom].second)
      {
        if (!use_trace)
        {
          if (dom < NDOM) domain_set[dom].insert(molecule.card(a).ResNum);
          else domain_set[NDOM].insert(molecule.card(a).ResNum);
        }
        else
        {
          for (int r = 0; r < molecule.trace_to_atom_list(ta).size(); r++)
          {
            int aa = molecule.trace_to_atom_list(ta)[r];
            if (dom < NDOM) domain_set[dom].insert(molecule.card(aa).ResNum);
            else domain_set[NDOM].insert(molecule.card(aa).ResNum);
          }
        }
      }
      ta++;
    }
  }
  for (unsigned dom = 0; dom < NDOM+1; dom++)
  {
    std::string keyword = dom < NDOM ? "Domain #" + itos(dom+1) : "Excluded ";
    int first_segment(true);
    int start_a = *domain_set[dom].begin();
    if (domain_set[dom].size() == 0)
    {
      logTab(2,LOGFILE,keyword + ": None");
      continue;
    }
    if (domain_set[dom].size() == 1)
    {
      logTab(2,LOGFILE,keyword + ": " + itos(start_a));
      continue;
    }
    std::set<int>::iterator start_iter = domain_set[dom].begin();
    start_iter++;
    for (std::set<int>::iterator iter = start_iter; iter != domain_set[dom].end(); iter++)
    {
      std::set<int>::iterator last_iter = iter;
      last_iter--;
      int last_a = *last_iter;
      if (*iter != last_a+1)
      {
        (first_segment) ?
          logTab(2,LOGFILE,keyword +": "  + itos(start_a,4) + " -> " + itos(last_a,4)):
          logTab(2,LOGFILE,"         + " + itos(start_a,4) + " -> " + itos(last_a,4));
        start_a = *iter;
        first_segment = false;
      }
    }
    std::set<int>::iterator last_iter = domain_set[dom].end();
    last_iter--;
    int last_a = *last_iter;
    (first_segment) ?
      logTab(2,LOGFILE,keyword + ": "  + itos(start_a,4) + " -> " + itos(last_a,4)):
      logTab(2,LOGFILE,"         + " + itos(start_a,4) + " -> " + itos(last_a,4));
  }
  logTab(3,VERBOSE,"Excluded segments consists of " + itos(best_ndom-NDOM-1) + " domains"); // 0 is not a domain (flag)
  logTab(3,VERBOSE,"Seq: " + itos(best_seq_sep)+  "  Distance: " + dtos(best_distance) + "  DDM: " + dtos(100*floatType(best_istep)/floatType(NMADDM.DISTANCE_STEP),5,2) + "%  Join: " + itos(best_join));
  logBlank(LOGFILE);
  floatType warning_percent(0.75);
  if (1.0-(nextra/array_size) < warning_percent)
    logWarning(SUMMARY,"Percentage of residues assigned to top " + itos(NDOM) + " domains is less than " + dtos(warning_percent*100) + "%. Domain division may not be real. To avoid this warning message try assigning fewer or more domains or use different search parameters requesting the same number of domains.");
}

void ResultNMA::writeDomains(bool write_separate)
{
  int npdb(0);
  std::string pdbfile = Output::Fileroot()+".sceds.pdb";
  PdbFiles.push_back(pdbfile);
  ResultFile full(pdbfile);
  fprintf(full.outFile,"REMARK TITLE %s\n",TITLE.c_str());
  fprintf(full.outFile,"REMARK Phaser Normal Mode Analysis\n");
  fprintf(full.outFile,"REMARK Domains Analysis %s\n",MODLID.c_str());
  fprintf(full.outFile,"%s",UnitCell(dvect3(1,1,1)).CrystCard("P 1",1).c_str());
  pdb_chains_2chainid chains(std::max(26,NDOM+1)); //so that X is included
  for (unsigned dom = 0; dom < NDOM; dom++)
  {
    std::string Chain = chains.allocate(" A"); //overwrite, first
    npdb++;
    ResultFile opened;
    if (write_separate)
    {
    std::string pdbfile = Output::Fileroot()+".domain."+itos(dom+1)+".pdb";
    opened.open(pdbfile);
    PdbFiles.push_back(pdbfile);
    fprintf(opened.outFile,"REMARK TITLE %s\n",TITLE.c_str());
    fprintf(opened.outFile,"REMARK Phaser Normal Mode Analysis\n");
    fprintf(opened.outFile,"REMARK Domains Analysis %s\n",MODLID.c_str());
    fprintf(opened.outFile,"REMARK Domain #%d\n",dom+1);
    fprintf(opened.outFile,"%s",UnitCell(dvect3(1,1,1)).CrystCard("P 1",1).c_str());
    }
    int ta(0);
    for (unsigned a = 0; a < molecule.nAtoms(); a++) {
      if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
      {
        if (best_domain[ta] == best_natoms_dom[dom].second) {
          if (!use_trace)
          {
            pdb_record card = molecule.card(a);
            if (write_separate) card.writeCard(opened.outFile);
            card.Chain = Chain;
            card.writeCard(full.outFile);
          }
          else
          {
            for (int r = 0; r < molecule.trace_to_atom_list(ta).size(); r++)
            {
              int aa = molecule.trace_to_atom_list(ta)[r];
              pdb_record card = molecule.card(aa);
              if (write_separate) card.writeCard(opened.outFile);
              card.Chain = Chain;
              card.writeCard(full.outFile);
            }
          }
        }
        ta++;
      }
    }
    fprintf(full.outFile,"TER\n");
    if (write_separate) fclose(opened.outFile);
  }
  if (true)
  {
    std::string Chain = chains.allocate(" X"); //overwrite, first
    ResultFile opened;
    if (write_separate)
    {
    std::string pdbfile = Output::Fileroot()+".excluded.pdb";
    opened.open(pdbfile);
    PdbFiles.push_back(pdbfile);
    fprintf(opened.outFile,"REMARK TITLE %s\n",TITLE.c_str());
    fprintf(opened.outFile,"REMARK Phaser Normal Mode Analysis\n");
    fprintf(opened.outFile,"REMARK Domains Analysis %s\n",MODLID.c_str());
    fprintf(opened.outFile,"REMARK Atoms not assigned to domains\n");
    fprintf(opened.outFile,"%s",UnitCell(dvect3(1,1,1)).CrystCard("P 1",1).c_str());
    }
    int ta(0);
    for (unsigned a = 0; a < molecule.nAtoms(); a++)
    {
      if (!use_trace || (use_trace && molecule.isTraceAtom(0,a)))
      {
        for (unsigned dom = NDOM; dom < best_natoms_dom.size(); dom++)
        if (best_domain[ta] == best_natoms_dom[dom].second)
        {
          if (!use_trace)
          {
            pdb_record card = molecule.card(a);
            if (write_separate) card.writeCard(opened.outFile);
            card.Chain = Chain;
            card.writeCard(full.outFile);
          }
          else
          {
            for (int r = 0; r < molecule.trace_to_atom_list(ta).size(); r++)
            {
              int aa = molecule.trace_to_atom_list(ta)[r];
              pdb_record card = molecule.card(aa);
              if (write_separate) card.writeCard(opened.outFile);
              card.Chain = Chain;
              card.writeCard(full.outFile);
            }
          }
          break;
        }
        ta++;
      }
    }
    fprintf(full.outFile,"TER\n");
    if (write_separate) fprintf(opened.outFile,"END\n");
    if (write_separate) fclose(opened.outFile);
  }
  fprintf(full.outFile,"END\n");
  fclose(full.outFile);
  if (npdb) logBlank(LOGFILE);
}

} //phaser
