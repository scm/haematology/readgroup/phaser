//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputNCS__Class__
#define __InputNCS__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputNCS :
                  public BINS
                 ,public CELL
                 ,public COMP
                 ,public ENSE
                 ,public HKLI
                 ,public HKLO
                 ,public INFO
                 ,public JOBS
                 ,public LABI
                 ,public MACA
                 ,public MACT
                 ,public MUTE
                 ,public NORM
                 ,public OUTL
                 ,public RESH
                 ,public RESO
                 ,public ROOT
                 ,public SEAR
                 ,public SPAC
                 ,public TITL
                 ,public TNCS
                 ,public VERB
                 ,public KILL
{
  public:
    InputNCS(std::string);
    InputNCS();
    ~InputNCS();
    void Analyse(Output&);
    std::string Cards();
};

} //phaser

#endif
