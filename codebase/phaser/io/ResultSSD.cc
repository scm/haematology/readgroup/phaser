//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultSSD.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

void ResultSSD::storeAtoms(ep_solution SET)
{ ATOM_SET = SET; }

void ResultSSD::writeScript(std::string title,std::string xtalid)
{
  std::string SOLfile = Output::Fileroot() + ".sol";
  filenames.push_back(SOLfile);
  ResultFile opened(SOLfile);
  fprintf(opened.outFile,"# TITLE %s \n",title.c_str());
  fprintf(opened.outFile,"%-s",ATOM_SET.unparse(xtalid,true).c_str());
  fclose(opened.outFile);
}

void ResultSSD::writePdb(std::string title,SpaceGroup SG,UnitCell UC)
{
  for (unsigned t = 0; t < ATOM_SET.size(); t++)
  {
    std::string PDBfile = Output::Fileroot() + "."
                          + std::string((ATOM_SET.size() > 999 && t+1 < 1000) ? "0" : "")
                          + std::string((ATOM_SET.size() > 99 && t+1 < 100) ? "0" : "")
                          + std::string((ATOM_SET.size() > 9 && t+1 < 10) ? "0" : "")
                          + itos(t+1) + ".pdb";
    filenames.push_back(PDBfile);
    ResultFile opened(PDBfile);
    fprintf(opened.outFile,"REMARK TITLE %s \n",title.c_str());
    int Z = SG.getSpaceGroupNSYMM();
    std::string SGNAME = SG.spcgrpname();
    fprintf(opened.outFile,"%s",UC.CrystCard(SGNAME,Z).c_str());
    fprintf(opened.outFile,"REMARK Atoms in chain A\n");
    ATOM_SET[t].writePdb(opened.outFile,UC.getCctbxUC());
    fclose(opened.outFile);
  }
}

af::shared<xray::scatterer<double> > ResultSSD::get_atoms_for_solution(int t)
{
  af::shared<xray::scatterer<double> > atoms;
  if (t >= ATOM_SET.size()) return atoms;
  if (t < 0) return atoms;
  for (unsigned a = 0; a < ATOM_SET[t].size(); a++)
    atoms.push_back(ATOM_SET[t][a].SCAT);
  return atoms;
}

}//namespace phaser
