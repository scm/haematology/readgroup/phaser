//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultANO__Class__
#define __ResultANO__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>
#include <scitbx/array_family/tiny_types.h>

namespace phaser {

class ResultANO : public Output, public DataA
{
  public:
    ResultANO();
    ResultANO(Output&);
    virtual ~ResultANO() throw() {}

  private:
    std::string MtzFile;

  public:
    std::string getMtzFile() { return MtzFile; }
    af_string   getFilenames();
    af_float    getScaledCorrected(af::shared<miller::index<int> >,af_float);

    void writeMtz(std::string="");
};

}
#endif
