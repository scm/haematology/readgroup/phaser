//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultMR__Class__
#define __ResultMR__Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/mr_objects/mr_clash.h>
#include <phaser/pod/xyzout.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>
#include <phaser/io/ResultCore.h>
#include <phaser/src/MapTraceMol.h>

namespace phaser {

class ResultMR : public Output, public DataA, public ResultCore
{
  public:
    ResultMR();
    ResultMR(Output&);
    ResultMR(Output&,DataA&,std::string,unsigned);
    virtual ~ResultMR() throw() {}

  private:
    mr_solution MRSET;
    mr_solution MRSET_REJECT;

    af_string   PdbFiles,MtzFiles,PakFiles,PrincipalFiles;
    string2D    EnsFiles;
    std::string SolFile,SolFileReject;
    std::string RESULT_TYPE;
    std::string TITLE;
    size_t      NTOP;
    floatType   PACK_CUTOFF,eLLG_HIRES;
    int         OCCUPANCY_NRES,OCCUPANCY_NWIN;
    double      OCCUPANCY_WINDOW_ELLG;

  public:
    MapTrcPtr   tracemol;

  public:
    pair_int_int calcDuplicateSolutions(floatType,mr_solution);

    void init(std::string,std::string,unsigned,map_str_pdb&,bool=false);

    void               sort_LLG()         { MRSET.sort_LLG(); }
    void               unsort()           { MRSET.unsort(); }
    const mr_solution& getDotSol()        { return MRSET; }
    int                getNumPdbFiles()   { return PdbFiles.size(); }
    int                getNumEnsFiles()   { return EnsFiles.size(); }
    int                getNumPakFiles()   { return PakFiles.size(); }
    int                getNumMtzFiles()   { return MtzFiles.size(); }
    unsigned           getNumTop()        { return std::min(NTOP,MRSET.size()); }
    af_string          getPdbFiles()      { return PdbFiles; }
    af_string          getEnsFiles();
    af_string          getMtzFiles()      { return MtzFiles; }
    std::string        getPdbFile(int f)  { return (f < PdbFiles.size()) ? PdbFiles[f]:std::string(""); }
    af_string          getEnsFile(int f);
    std::string        getPakFile(int f)  { return (f < PakFiles.size()) ? PakFiles[f]:std::string(""); }
    std::string        getMtzFile(int f)  { return (f < MtzFiles.size()) ? MtzFiles[f]:std::string(""); }
    std::string        getSolFile()       { return SolFile; }
    std::string        getSolFileReject() { return SolFileReject; }
    floatType          getTopLLG()        { sort_LLG(); return MRSET.top_LLG(); }
    floatType          getTopTFZ()        { sort_LLG(); return MRSET.top_TFZ(); }
    floatType          getTopTFZeq()      { sort_LLG(); return MRSET.top_TFZeq(); }
    std::string        getTopPdbFile()    { sort_LLG(); return PdbFiles.size() ? PdbFiles[0]:std::string(""); }
    af_string          getTopEnsFile();
    std::string        getTopMtzFile()    { sort_LLG(); return MtzFiles.size() ? MtzFiles[0]:std::string(""); }
    mr_set             getTopSet()        { sort_LLG(); return MRSET.size() ? MRSET[0] : mr_set(); }
    mr_set             getSet(int t)      { return (t < MRSET.size())? MRSET[t]: mr_set(); }
    bool               foundSolutions()   { return MRSET.size(); }
    int                numSolutions()     { return MRSET.size(); }
    bool               uniqueSolution()   { return (MRSET.size() == 1); }
    af_float           getValues()        { return (RESULT_TYPE.compare("PAK") == 0) ? getPAK() : getLLG(); }

    int1D              getTemplatesForSolution(int);
    int1D              getSolutionsForTemplate(int);
    af_float           getPAK();
    af_float           getLLG();
    af_string          phenixPDBNames();

  private:
    string1D writePdbFull(int,xyzout,bool chainocc,bool rtnstr,bool catstr,bool realocc);
  public:
    af_string getFilenames();
    void writePdb(xyzout,map_str_pdb&,bool chainocc=false); //from c++
    void writePdbPrincipal(map_str_pdb&); //from c++
    void writePdbRealOcc(xyzout); //from c++
    void writeSol(); //from c++
    void writeSolReject(); //from c++
    void writeMtz(std::string=""); //from c++
    string1D string_pdbs();
    void write_pdb();
    void write_mtz() { return writeMtz(); } //python
    void append_mtz(std::string hklin) { return writeMtz(hklin); } //python

    // State setting for Python
    void setDotSol(const mr_solution& mrset)  { MRSET = mrset; }
    void setDotSolReject(const mr_solution& mrset)  { MRSET_REJECT = mrset; }
    void setMapCoefs(int k,MapCoefs m)  { if (k < MRSET.size()) MRSET[k].MAPCOEFS = m; }

    const std::string  getTitle() const               { return TITLE; }
    const std::string  getResultType() const          { return RESULT_TYPE; }

    std::string last_modlid() { return (MRSET.size()?(MRSET[0].KNOWN.size()?MRSET[0].KNOWN.back().MODLID:""):""); }
    int         getUsedReflections();
    int         get_occupancy_nwindows()  { return OCCUPANCY_NWIN; }
    void        set_occupancy_nwindows(int s)  { OCCUPANCY_NWIN = s; }
    int         get_occupancy_nresidues() { return OCCUPANCY_NRES; }
    void        set_occupancy_nresidues(int& s) { OCCUPANCY_NRES = s; }
    double      get_occupancy_window_ellg() { return OCCUPANCY_WINDOW_ELLG; }
    void        set_occupancy_window_ellg(double s) { OCCUPANCY_WINDOW_ELLG = s; }
    void        clear_real_occupancies() { MRSET.clear_real_occupancies(); }
    std::string get_gyre_pdb_str(int,std::string);
};

}
#endif
