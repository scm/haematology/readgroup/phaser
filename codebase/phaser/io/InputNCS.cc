//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputNCS.h>

namespace phaser {

InputNCS::InputNCS(std::string cards)
{ parseCCP4(cards); }

InputNCS::InputNCS() { }
InputNCS::~InputNCS() { }

//performs the analysis between correlated keyword Input
void InputNCS::Analyse(Output& output)
{
  BINS::analyse();
  CELL::analyse();
  COMP::analyse();
  ENSE::analyse();
  HKLI::analyse();
  HKLO::analyse();
  INFO::analyse();
  JOBS::analyse();
  LABI::analyse();
  MACA::analyse();
  MACT::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  RESH::analyse();
  RESO::analyse();
  ROOT::analyse();
  //SEAR::analyse(); //does not have to be set, used for NMOL analysis if present
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");

  for (int r = 0; r < SEARCH_MODLID.size(); r++)
  if (PDB.find(SEARCH_MODLID[r]) == PDB.end())
  throw PhaserError(INPUT,keywords,"No model for search ensemble " + SEARCH_MODLID[r]);
}

std::string InputNCS::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BINS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ENSE::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += INFO::unparse();
  cards += JOBS::unparse();
  cards += LABI::unparse();
  cards += MACA::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SEAR::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
