//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultDFAC__Class__
#define __ResultDFAC__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/DataA.h>

namespace phaser {

class ResultDFAC : public Output, public DataA
{
  public:
    ResultDFAC();
    ResultDFAC(Output&);
    virtual ~ResultDFAC() throw() {}

  private:
    std::string  MtzFile;

  public:
    void         setReflData(data_refl& d) { set_data_refl(d); }
    data_refl    getReflData() { return *this; }
    std::string  getMtzFile() { return MtzFile; }
    af_string    getFilenames();
    void         writeMtz(std::string="");
};

}
#endif
