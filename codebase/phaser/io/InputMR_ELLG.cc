//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputMR_ELLG.h>

namespace phaser {

InputMR_ELLG::InputMR_ELLG(std::string cards)
{ parseCCP4(cards); }

InputMR_ELLG::InputMR_ELLG() { }
InputMR_ELLG::~InputMR_ELLG() { }

//performs the analysis between correlated keyword Input
void InputMR_ELLG::Analyse(Output& output)
{
  BINS::analyse();
  CELL::analyse();
  COMP::analyse();
  ENSE::analyse();
  ELLG::analyse();
  JOBS::analyse();
  LABI::analyse();
  OUTL::analyse();
  MACA::analyse();
  MACT::analyse();
  MUTE::analyse();
  OCCU::analyse();
  RESH::analyse();
  RESO::analyse();
  ROOT::analyse();
  //SEAR::analyse();
  SOLP::analyse();
  SOLU::analyse();
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  NORM::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE

  for (int k = 0; k < MRSET.size(); k++)
    for (int s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      if (PDB.find(MRSET[k].KNOWN[s].MODLID) == PDB.end())
      throw PhaserError(INPUT,keywords,"No model for ensemble " + MRSET[k].KNOWN[s].MODLID);
    }
  if (!PDB.size())
    throw PhaserError(INPUT,keywords,"No ensembles entered");

  if (!PDB.size())
  throw PhaserError(INPUT,keywords,"No ensembles");

  if (!SEARCH_MODLID.size())
  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
    SEARCH_MODLID.push_back(iter->first);

  for (int r = 0; r < SEARCH_MODLID.size(); r++)
  if (PDB.find(SEARCH_MODLID[r]) == PDB.end())
  throw PhaserError(INPUT,keywords,"No model for search ensemble " + SEARCH_MODLID[r]);
}

std::string InputMR_ELLG::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BINS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += ENSE::unparse();
  cards += ELLG::unparse();
  cards += JOBS::unparse();
  cards += LABI::unparse();
  cards += OUTL::unparse();
  cards += MACA::unparse();
  cards += MACT::unparse();
  cards += MUTE::unparse();
  cards += OCCU::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SEAR::unparse();
  cards += SOLP::unparse();
  cards += SOLU::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += NORM::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
