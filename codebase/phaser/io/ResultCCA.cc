//(i) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultCCA.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

ResultCCA::ResultCCA() : Output(),SpaceGroup(),UnitCell()
{
  optimalMW = optimalVM = assemblyMW = fitError = 0;
}

ResultCCA::ResultCCA(Output& output) : Output(output),SpaceGroup(),UnitCell()
{
  optimalMW = optimalVM = assemblyMW = fitError = 0;
}

int ResultCCA::getBest()
{
  floatType bestProb(0);
  int best(0);
  for (int i = 0; i < probVM.size(); i++)
    if (probVM[i] > bestProb) bestProb = probVM[i],best = i;
  return best;
}

af_string ResultCCA::getFilenames()
{
  af_string filenames;
  return filenames;
}

} //phaser
