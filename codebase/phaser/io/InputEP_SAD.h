//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputEP_SAD__Class__
#define __InputEP_SAD__Class__
#include <phaser/io/InputEP_AUTO.h>

namespace phaser {

class InputEP_SAD : public InputEP_AUTO
{
  public:
    InputEP_SAD(std::string);
    InputEP_SAD();
    ~InputEP_SAD();
};

} //phaser

#endif
