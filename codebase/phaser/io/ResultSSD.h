//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ResultSSD__Class__
#define __ResultSSD__Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <phaser/ep_objects/data_xtal.h>
#include <phaser/ep_objects/ep_solution.h>

namespace phaser {

class ResultSSD : public data_xtal,public Output
{
  public:
    ResultSSD() : data_xtal(),Output() {}
    ResultSSD(data_xtal& dx_,Output& o_) : data_xtal(dx_),Output(o_) {}
    ResultSSD(Output& o_) : data_xtal(),Output(o_) {}
    virtual ~ResultSSD() throw() {}

  private:
    af_string filenames;

  public:
    void      storeAtoms(ep_solution);
    af_string getFilenames() { return filenames; }
    void      writeScript(std::string,std::string);
    void      writePdb(std::string,SpaceGroup,UnitCell);

    int number_of_solutions() { return ATOM_SET.size(); }
    af::shared<xray::scatterer<double> > get_atoms_for_solution(int);
};

}
#endif
