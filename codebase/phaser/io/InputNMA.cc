//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputNMA.h>

namespace phaser {

InputNMA::InputNMA(std::string cards)
{ parseCCP4(cards); }

InputNMA::InputNMA() {}
InputNMA::~InputNMA() {}

//performs the analysis between correlated keyword Input
void InputNMA::Analyse(Output& output)
{
  DDM::analyse();
  EIGE::analyse();
  ENM::analyse();
  ENSE::analyse();
  JOBS::analyse();
  KEYW::analyse();
  MUTE::analyse();
  NMA::analyse();
  PERT::analyse();
  ROOT::analyse();
  SCED::analyse();
  TITL::analyse();
  VERB::analyse();
  XYZO::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
  {
    if (iter->second.ENSEMBLE.size() > 1)
      throw PhaserError(INPUT,keywords,"Ensemble " + iter->first + " has more than one model");
  }
}

std::string InputNMA::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += DDM::unparse();
  cards += EIGE::unparse();
  cards += ENM::unparse();
  cards += ENSE::unparse();
  cards += JOBS::unparse();
  cards += KEYW::unparse();
  cards += MUTE::unparse();
  cards += NMA::unparse();
  cards += PERT::unparse();
  cards += ROOT::unparse();
  cards += SCED::unparse();
  cards += TITL::unparse();
  cards += VERB::unparse();
  cards += XYZO::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
