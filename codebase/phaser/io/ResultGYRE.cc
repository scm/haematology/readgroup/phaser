//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/ResultGYRE.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/PdbHandler.h>
#include <phaser/io/Errors.h>
#include <phaser/io/ResultFile.h>
#include <phaser/lib/safe_mtz.h>
#include <cstring>

#if defined(__linux)
#include <sys/stat.h>
#include <sys/types.h>
#elif defined(_WIN32)
// BYTE, INT16 and INT32 are defined in libccp4/ccp4/ccp4_sysdep.h
// and they conflict with Windows typedefs.
#ifdef BYTE
#undef BYTE
#endif
#ifdef INT16
#undef INT16
#endif
#ifdef INT32
#undef INT32
#endif

#include <windows.h>
#endif

namespace phaser {

ResultGYRE::ResultGYRE() : Output() { }
ResultGYRE::ResultGYRE(Output& output) : Output(output) { }

void ResultGYRE::init(map_str_pdb& pdb)
{
  set_pdb_chainids(pdb);
}

void ResultGYRE::store(bool amalgamate_background,size_t NUM,std::string TITLE_)
{
  TITLE = TITLE_;
  if (!MRSET.size() || !NUM) return;
  //create the python accessible PDB object
  NUM = std::min(NUM,MRSET.size());
  MRSET.erase(MRSET.begin()+NUM,MRSET.end());
  string1D modlid(MRSET.size());
  for (unsigned t = 0; t < MRSET.size(); t++)
    modlid[t] = MRSET[t].GYRE[0].MODLID.substr(0,MRSET[t].GYRE[0].MODLID.find_last_of("[")) + "."
   + std::string((MRSET.size() > 999 && t < 1000) ? "0" : "")
   + std::string((MRSET.size() > 99 && t < 100) ? "0" : "")
   + std::string((MRSET.size() > 9 && t < 10) ? "0" : "")
   + itos(t+1);

  for (unsigned t = 0; t < MRSET.size(); t++)
  {
    data_model model;
    std::string MODLID = MRSET[t].GYRE[0].MODLID;//all the same, initialized before break up
    //STR, FORMAT, VARTYPE, RMSID
    model.init_filename(pdbString(t),"PDB","RMS",MRSET[t].VRMS[MODLID][0]);
    //STRNAME
    model.set_as_string(Output::Fileroot() + "." + itos(t+1) + ".pdb");
    data_pdb pdb; pdb.addModel(model);
    GYRE_PDB[modlid[t]] = pdb;
  }
  //GYRE_MRSET.copy_extras(MRSET);
  if (amalgamate_background)
  { //all the backgrounds were the same, can end up on same rlist
    GYRE_MRSET.resize(1);
    GYRE_MRSET[0] = MRSET[0]; //background (probably empty)
    GYRE_MRSET[0].GYRE.clear();
    GYRE_MRSET[0].RLIST.resize(MRSET.size());
    GYRE_MRSET[0].HISTORY.clear(); //because it only refers to the top TRIAL
    GYRE_MRSET[0].ANNOTATION.clear(); //because it only refers to the top TRIAL
    for (unsigned t = 0; t < MRSET.size(); t++)
    {
      //replace rlist entry with 0 0 0 and new pdb and turn TFZ back into RFZ
      GYRE_MRSET[0].RLIST[t] = mr_rlist(modlid[t],dvect3(0,0,0),MRSET[t].TF,MRSET[t].TFZ,MRSET[t].LLG);
      GYRE_MRSET[0].RLIST[t].GYRE = MRSET[t].GYRE;
    }
  }
  else
  {
    GYRE_MRSET.resize(NUM);
    for (unsigned t = 0; t < MRSET.size(); t++)
    {
      GYRE_MRSET[t] = MRSET[t];
      GYRE_MRSET[t].RLIST.resize(1);
      //replace rlist entry with 0 0 0 and new pdb and turn TFZ back into RFZ
      GYRE_MRSET[t].RLIST[0] = mr_rlist(modlid[t],dvect3(0,0,0),MRSET[t].TF,MRSET[t].TFZ,MRSET[t].LLG);
      GYRE_MRSET[t].RLIST[0].GYRE = MRSET[t].GYRE;
    }
  }
}

void ResultGYRE::writeRlist()
{
  RlistFile = Output::Fileroot() + std::string(".gyre.rlist");
  ResultFile openedrlist(RlistFile);
  //short unparse, only PDB possiible
  if (TITLE.size()) fprintf(openedrlist.outFile,"# %s \n",TITLE.c_str());
  for (pdbIter iter = GYRE_PDB.begin(); iter != GYRE_PDB.end(); iter++)
  {
    std::string modlid = iter->first;
    modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
    modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
    std::string t = modlid.substr(modlid.find_last_of(".")+1,std::string::npos);
    std::string filename = Output::Fileroot() + "." + t + ".pdb";
    filename = (filename.find_first_of(" ")!=std::string::npos) ? "\"" + filename + "\"" : filename;
    fprintf(openedrlist.outFile,"ENSE %s PDB %s ID 100\n",
      std::string(modlid).c_str(),
      std::string(filename).c_str());
  }
  fprintf(openedrlist.outFile,"%s\n",GYRE_MRSET.unparse().c_str());
  fclose(openedrlist.outFile);
}

void ResultGYRE::writeSol()
{
  SolFile = Output::Fileroot() + std::string(".sol");
  ResultFile openedsol(SolFile);
  if (TITLE.size()) fprintf(openedsol.outFile,"# %s \n",TITLE.c_str());
  fprintf(openedsol.outFile,"%s\n",MRSET.unparse().c_str());
  fclose(openedsol.outFile);
}

std::string ResultGYRE::pdbString(int t)
{
  PHASER_ASSERT(t < MRSET.size());
  std::string pdbstr;
  pdbstr += "REMARK TITLE " + TITLE + "\n";
  pdbstr += "REMARK Log-Likelihood Gain: " + dtos(MRSET[t].LLG,8,3) + "\n";
  pdbstr += "REMARK Rank: " + itos(MRSET[t].NUM) + "\n";
  for (unsigned p = 0; p < MRSET[t].GYRE.size(); p++)
  {
   // dvect3 outR = matrix2eulerDEG(MRSET[t].GYRE[p].R);
   // dvect3 outT = MRSET[t].GYRE[p].TRA;
    std::string m = MRSET[t].GYRE[p].MODLID;
    pdbstr += "REMARK ENSEMBLE " + m + " PERTURB ROT " + dvtos(MRSET[t].GYRE[p].perturbRot,6,2)
                  + " DROT " + dtos(MRSET[t].GYRE[p].angle(),6,2)
                  + " TRA " + dvtos(MRSET[t].GYRE[p].perturbTrans,6,2)
                  + " DTRA " + dtos(MRSET[t].GYRE[p].distance(),6,2) + "\n";
  }
  bool Space_Group_of_Solution_Set(MRSET[t].HALL != "");
  PHASER_ASSERT(Space_Group_of_Solution_Set);
  std::string sgname = space_group_name(MRSET[t].HALL,true).CCP4;
  int Z = SpaceGroup(MRSET[t].HALL).getSpaceGroupNSYMM()*MRSET[t].Z();
  pdbstr += UnitCell::CrystCard(sgname,Z);
  af::double6 unit_cell = getCell6();
  //bool renumber_atoms(MRSET[t].GYRE.size() > 1);
  for (unsigned p = 0; p < MRSET[t].GYRE.size(); p++)
    pdbstr += gyre_string_maker(PDB,MRSET[t].GYRE[p],unit_cell);
  pdbstr += "END\n";
  return pdbstr;
}

void ResultGYRE::writePdb()
{
  PdbFiles.clear();
  for (unsigned t = 0; t < MRSET.size(); t++)
  {
    std::string pdbstr = pdbString(t);
    std::string filename = Output::Fileroot() + "." + itos(t+1) + ".pdb";
    PdbFiles.push_back(filename);
    ResultFile sopened(filename);
    fprintf(sopened.outFile,"%s",pdbstr.c_str());
    fclose(sopened.outFile);
  }
}

af_string ResultGYRE::getFilenames()
{
  af_string filenames;
  filenames.insert(filenames.end(),PdbFiles.begin(),PdbFiles.end());
  if (SolFile.size()) filenames.push_back(SolFile);
  if (RlistFile.size()) filenames.push_back(RlistFile);
  return filenames;
}

af_float ResultGYRE::getLLG()
{
  af_float values(MRSET.size());
  for (int t = 0; t < MRSET.size(); t++)
    values[t] = MRSET[t].LLG;
  return values;
}

int ResultGYRE::numRlist()
{
  int count(0);
  for (int t = 0; t < GYRE_MRSET.size(); t++) count += GYRE_MRSET[t].RLIST.size();
  return count;
}

} //end namespace phaser
