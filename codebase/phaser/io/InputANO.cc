//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/InputANO.h>

namespace phaser {

InputANO::InputANO(std::string cards)
{ parseCCP4(cards); }

InputANO::InputANO() { }
InputANO::~InputANO() { }

//performs the analysis between correlated keyword Input
void InputANO::Analyse(Output& output)
{
  BINS::analyse();
  CELL::analyse();
  COMP::analyse();
  HKLI::analyse();
  HKLO::analyse();
  LABI::analyse();
  MACA::analyse();
  MUTE::analyse();
  NORM::analyse();
  OUTL::analyse();
  RESH::analyse();
  RESO::analyse();
  ROOT::analyse();
  SGAL::analyse();//for PREP
  SPAC::analyse();
  TITL::analyse();
  TNCS::analyse();
  VERB::analyse();
  KILL::analyse();
  CCP4base::throw_errors();
  CCP4base::throw_advisory(output);

  setREFL_CELL(UNIT_CELL);
  setREFL_HALL(SG_HALL); //assumed to be in same point group NO CHECK POSSIBLE
  if (!cctbx::sgtbx::space_group(SG_HALL,"A1983").is_compatible_unit_cell(cctbx::uctbx::unit_cell(UNIT_CELL)))
    throw PhaserError(INPUT,keywords,"Unit Cell not compatible with Space Group");
}

std::string InputANO::Cards()
{
  //post-analysis of keyword input, cleaning up input
  std::string cards;
  cards += BINS::unparse();
  cards += CELL::unparse();
  cards += COMP::unparse();
  cards += HKLI::unparse();
  cards += HKLO::unparse();
  cards += LABI::unparse();
  cards += MACA::unparse();
  cards += MUTE::unparse();
  cards += NORM::unparse();
  cards += OUTL::unparse();
  cards += RESH::unparse();
  cards += RESO::unparse();
  cards += ROOT::unparse();
  cards += SGAL::unparse();
  cards += SPAC::unparse();
  cards += TITL::unparse();
  cards += TNCS::unparse();
  cards += VERB::unparse();
  cards += KILL::unparse();
  return cards;
}

} //phaser
