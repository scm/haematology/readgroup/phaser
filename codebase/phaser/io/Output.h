//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __Output__Class__
#define __Output__Class__
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <set>
#include <boost/shared_ptr.hpp>
#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <phaser/io/phenix_out.h>
#include <phaser/pod/stream_string.h>
#include <phaser_defaults.h>
#include <phaser/lib/fmat.h>

namespace phaser {

class Output : public PhaserError
{
  public:
    Output();
    void setOutput(const Output &);
    Output(const Output &);
    const Output& operator=(const Output&);
    virtual ~Output() throw() { }
    Output& operator+=(const Output &rhs)
    {
      stringset warn = rhs.Warnings();
      for (stringset::iterator iter = warn.begin(); iter != warn.end(); iter++)
        WARNINGS.insert(*iter);
      stringset advisory = rhs.Advisory();
      for (stringset::iterator iter = advisory.begin(); iter != advisory.end(); iter++)
        ADVISORY.insert(*iter);
      stringset logg = rhs.Loggraph();
      for (stringset::iterator iter = logg.begin(); iter != logg.end(); iter++)
        LOGGRAPH.insert(*iter);
      return *this;
    }

    Output& operator << (const Output &rhs)
    {
      for (int x = 0; x < rhs.output_strings.size(); x++)
        logOutput(static_cast<outStream>(rhs.output_strings[x].stream),rhs.output_strings[x].string);
      return *this;
    }

  private:
    outStream     LEVEL,CCP4_SUMMARY_LEVEL,PHENIX_LOG_LEVEL,PHENIX_CALLBACK_LEVEL;
    bool          MUTE,PACKAGE_PHENIX,BOOKEND,TOG_BLANK;
    std::string   FILEROOT,FILEKILL;
    floatType     TIMEKILL;
    int           jumpnum,progressCount,progressBars,progressBarSize,progressChunk;
    std::clock_t  start_clock;
    double        elapsed_time;

    stringset     WARNINGS,ADVISORY,LOGGRAPH;
    bool          use_phenix_callbacks;

  protected:
    boost::shared_ptr<phenix_out> phenix_out_;
    boost::shared_ptr<phenix_callback> phenix_callback_;

  public:
    std::vector<stream_string> output_strings;
    double        run_time;

  private:
    void        logOutput(outStream,const std::string&); //master output function
    std::string formatMessage(const std::string,int,bool,bool=false,bool=false);
    std::string tab(int);
    std::string getLine(unsigned,char);
    void        IncrementRunTime();
    void        CheckForKill();
    std::string reconstruct(outStream);

  public:
    void startClock();

    void logHeader(outStream,std::string);
    void logSectionHeader(outStream,std::string);
    void logTrailer(outStream);
    void logError(outStream,PhaserError=PhaserError(NULL_ERROR));
    void logTerminate(outStream,bool);
    void logBlank(outStream);
    void logFlush();
    void logUnderLine(outStream,const std::string);
    void logKeywords(outStream,std::string);
    void logTab(unsigned,outStream,const std::string,bool=true);
    void logTabPrintf(unsigned,outStream,const char*,...);
    void logEllipsisStart(outStream,const std::string);
    void logEllipsisEnd(outStream);
    void logProgressBarStart(outStream,const std::string, const int);
    void logProgressBarNext(outStream);
    void logProgressBarPause(outStream);
    void logProgressBarEnd(outStream);
    void logProgressBarAgain(outStream);
    void logWarning(outStream,const std::string,bool=false);
    void logAdvisory(outStream,const std::string);
    void logElapsedTime(outStream);
    void logTime(outStream,double);
    void logUserTime(outStream where, double seconds);
    void logGraph(outStream,std::string,string1D,std::string,std::string,bool=false);
    void logHessian(outStream,TNT::Fortran_Matrix<double>&);

    void setPackageCCP4();
    void setPackagePhenix();
    void setIO(boost::shared_ptr<phenix_out> const& out);
    void setCallback(boost::shared_ptr<phenix_callback> const& callback);
    void useCallback(bool t) { use_phenix_callbacks = t; }
    void phenixCallback(std::string,std::string);

    void setFiles(std::string,std::string,floatType);
    void setLevel(outStream v)      { LEVEL = v; PHENIX_LOG_LEVEL = v; }
    void setMute(bool b)            { MUTE = b; }
    void setBookend(bool f)         { BOOKEND = f; }

    bool        getBookend()        { return BOOKEND; }
    bool        isPackageCCP4()     { return (!PACKAGE_PHENIX); }
    bool        isPackagePhenix()   { return (PACKAGE_PHENIX); }
    outStream   getLevel()          { return LEVEL; }
    bool        level(outStream l)  { return LEVEL >= l; }
    std::string Fileroot()          { return FILEROOT; }
    std::string Package()           { return PACKAGE_PHENIX ? "PACKAGE_PHENIX" : "PACKAGE_CCP4"; }
    stringset   Warnings() const    { return WARNINGS; }
    stringset   Advisory() const    { return ADVISORY; }
    stringset   Loggraph() const    { return LOGGRAPH; }
    bool        isMute() const      { return MUTE; }
    double      cpu_time() const { return run_time; }

    std::string process()           { return reconstruct(PROCESS); }
    std::string concise()           { return reconstruct(CONCISE); }
    std::string logfile()           { return reconstruct(LOGFILE); }
    std::string summary()           { return reconstruct(SUMMARY); }
    std::string verbose()           { return reconstruct(VERBOSE); }
    std::string debug()             { return reconstruct(DEBUG); }

    std::string version_date(); //function code in separate Version.cc file
                                //created by SCons so thate version date updated
                                //each time phaser is compiled
    std::string version_number(); //function code in separate Version.cc file
                                  //created by SCons so that version number
                                  //corresponds to release or development
    std::string svn_raw(); //function code in separate Version.cc file
                                  //created by SCons so that version number
                                  //corresponds to release or development

    std::string git_rev();  // git revision number constructed by SConscript
    std::string git_hash(); // git hash value of commit extracted by SConscript
    std::string git_shorthash(); //short git hash value of commit extracted by SConscript
    std::string git_commitdatetime(); // date time of current git commit extacted by SConscript
    std::string git_branchname(); // git branch used for this build
    std::string git_tagname(); // git tag name of current commit
    std::string git_totalcommits(); // total number of commits of all branches in git repo
    std::string svn_revision();
    std::string git_revision();
    std::string change_log();

    double virtual_memory,resident_set_size;
    void   process_mem_usage(double&, double&);
    void   logMemory(outStream);
};

} //phaser

#endif
