//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __InputMR_PAK__Class__
#define __InputMR_PAK__Class__
#include <phaser/keywords/keywords.h>

//also add analyse functions to list in .cc file

namespace phaser {

class InputMR_PAK :
                  public CELL
                 ,public ENSE
                 ,public JOBS
                 ,public KEYW
                 ,public MUTE
                 ,public PACK
                 ,public ROOT
                 ,public SOLU
                 ,public SPAC
                 ,public TITL
                 ,public TOPF
                 ,public VERB
                 ,public XYZO
                 ,public ZSCO
                 ,public KILL
{
  public:
    InputMR_PAK(std::string);
    InputMR_PAK();
    ~InputMR_PAK();
    void Analyse(Output&);
    std::string Cards();

  public:
    bool NO_WARNINGS;
};

} //phaser

#endif
