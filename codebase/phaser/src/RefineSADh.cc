//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineSAD.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/src/Integration.h>

// Hessian related code

namespace phaser {

floatType RefineSAD::hessianFn(TNT::Fortran_Matrix<floatType>& Hessian,bool& is_diagonal)
{
  //initialization
  floatType totLogLike(-wilson_llg);
  Hessian.newsize(npars_ref,npars_ref);
  for (int i = 0; i < Hessian.num_rows(); i++)
    for (int j = 0; j < Hessian.num_cols(); j++)
      Hessian(i+1,j+1) = 0.;

  calcIntegrationPoints();
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    if (both[r])
      totLogLike += acentricReflHessSAD2(r,!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
    else if (!sad_target_anom_only && cent[r])
      totLogLike += centricReflHessSAD2(r,!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
    else if (!sad_target_anom_only)
      totLogLike += singletonReflHessSAD2(r,plus[r],!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
    //chain rule
    if (both[r] || !sad_target_anom_only)
    {
      if (!FIX_ATOMIC) d2L_by_dAtomicPar2(r);
      if (!FIX_SCALES) d2L_by_dScalesPar2(r);
      if (!FIX_SIGMAA) d2L_by_dSigmaaPar2(r);
    //now add to total: only diagonal elements
      for (int m = 0; m < npars_ref; m++)
        Hessian(m+1,m+1) += d2L_by_dPar2[m];
    }
  }

  totLogLike += constrain_restrain_hessian(Hessian);

//#define __2ND_DER_OVERALL__
#ifdef __2ND_DER_OVERALL__
floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
TNT::Fortran_Matrix<floatType> fd_hess;
fd_hess.newsize(npars_ref,npars_ref);
floatType f = finiteGDiffHessian(fd_hess,shift);
   for (int i = 0; i < npars_ref; i++)
     for (int j = i; j < npars_ref; j++)
       if (i == j)
       std::cout << btos(!(fabs(Hessian(i+1,j+1)) > fabs(fd_hess(i+1,j+1)*10) ||
                         fabs(Hessian(i+1,j+1)) < fabs(fd_hess(i+1,j+1)/10.0)))
      << " " <<  i << " " << j << " " << Hessian(i+1,j+1) << " " << fd_hess(i+1,j+1) <<  std::endl;
 // std::exit(1);
#endif

  is_diagonal = true;
  return totLogLike;
}

void RefineSAD::d2L_by_dAtomicPar2(unsigned r)
{
//#define __FDHESSATOMIC__
#ifdef __FDHESSATOMIC__
static int hess_refl = 0;
   //need to set SADM ALL
  if (hess_refl == 0)
  for (int i = nscales_ref; i < npars_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  int t = atomtype2t[atoms[0].scattering_type];
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  PHASER_ASSERT(ipar >= nscales_ref);
  PHASER_ASSERT(ipar < npars_ref);
  int itest(nscales_all);
  floatType *test;
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[0]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[1]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[2]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.occupancy);
    if (!atoms[a].SCAT.flags.use_u_aniso_only())
    { if (itest++ == ipar) test = &(atoms[a].SCAT.u_iso); }
    else
    { for (int n = 0; n < 6; n++) if (itest++ == ipar) test = &(atoms[a].SCAT.u_star[n]); }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (itest++ == ipar) test = &(AtomFdp[t]); //fp

  floatType &dL_by_dtest = dL_by_dPar[ipar];
  floatType &d2L_by_dtest2 = d2L_by_dPar2[ipar];

  TNT::Vector<floatType> g(npars_ref);
  calcAtomicData();
  calcScalesData();
  floatType f_start;
  if (both[r])      f_start = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_start = centricReflGradSAD2(r,true,true);
  else              f_start = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dAtomicPar(r);
  floatType grad_start = dL_by_dtest;
  (*test) += shift;
  calcAtomicData();
  calcScalesData();
  floatType f_forward;
  if (both[r])      f_forward = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_forward = centricReflGradSAD2(r,true,true);
  else              f_forward = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dAtomicPar(r);
  floatType grad_forward = dL_by_dtest;
  (*test) -= shift;
  (*test) -= shift;
  calcAtomicData();
  calcScalesData();
  floatType f_backward;
  if (both[r])      f_backward = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_backward = centricReflGradSAD2(r,true,true);
  else              f_backward = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dAtomicPar(r);
  floatType grad_backward = dL_by_dtest;
  (*test) += shift;
  calcAtomicData();
  calcScalesData();
  floatType FD_test_forward = (grad_forward-grad_start)/shift;
  floatType FD_test_on_f = (f_forward-2*f_start+f_backward)/shift/shift;
  floatType FD_test_backward = (grad_start-grad_backward)/shift;
#endif

  floatType ZERO(0.);
  //initialize dFH_by_dAtomicPar
  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    dReFHpos_by_dX[a] = dImFHpos_by_dX[a] = ZERO;
    dReFHpos_by_dY[a] = dImFHpos_by_dY[a] = ZERO;
    dReFHpos_by_dZ[a] = dImFHpos_by_dZ[a] = ZERO;
    dReFHneg_by_dX[a] = dImFHneg_by_dX[a] = ZERO;
    dReFHneg_by_dY[a] = dImFHneg_by_dY[a] = ZERO;
    dReFHneg_by_dZ[a] = dImFHneg_by_dZ[a] = ZERO;
   //O and B are not incremented, just set with equality
    for (int n = 0; n < 6; n++)
    {
      dReFHpos_by_dAB[a][n] = dImFHpos_by_dAB[a][n] = ZERO;
      dReFHneg_by_dAB[a][n] = dImFHneg_by_dAB[a][n] = ZERO;
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    dReFHpos_by_dFdp[t] = dImFHpos_by_dFdp[t] = ZERO;
    dReFHneg_by_dFdp[t] = dImFHneg_by_dFdp[t] = ZERO;
  }

  //initialize d2FH_by_dAtomicPar2
  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    d2ReFHpos_by_dX2[a] = d2ImFHpos_by_dX2[a] = ZERO;
    d2ReFHpos_by_dY2[a] = d2ImFHpos_by_dY2[a] = ZERO;
    d2ReFHpos_by_dZ2[a] = d2ImFHpos_by_dZ2[a] = ZERO;
    d2ReFHneg_by_dX2[a] = d2ImFHneg_by_dX2[a] = ZERO;
    d2ReFHneg_by_dY2[a] = d2ImFHneg_by_dY2[a] = ZERO;
    d2ReFHneg_by_dZ2[a] = d2ImFHneg_by_dZ2[a] = ZERO;
   //O and B are not incremented, just set with equality
    for (int n = 0; n < 6; n++)
    {
      d2ReFHpos_by_dAB2[a][n] = d2ImFHpos_by_dAB2[a][n] = ZERO;
      d2ReFHneg_by_dAB2[a][n] = d2ImFHneg_by_dAB2[a][n] = ZERO;
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    d2ReFHpos_by_dFdp2[t] = d2ImFHpos_by_dFdp2[t] = ZERO;
    d2ReFHneg_by_dFdp2[t] = d2ImFHneg_by_dFdp2[t] = ZERO;
  }

  //calculate dFH_by_dAtomicPar
  floatType ReFHpos,ImFHpos,ReFHneg,ImFHneg;

  float1D linearTerm(atoms.size()),debye_waller_u_iso(atoms.size(),ZERO);
  floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    linearTerm[a] = symFacPCIF*ScaleK*atoms[a].SCAT.occupancy/M;
    if (!atoms[a].SCAT.flags.use_u_aniso_only()) debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
  }
  floatType b_as_u_term(0.25*scitbx::constants::eight_pi_sq);

  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    int t = atomtype2t[atoms[a].SCAT.scattering_type];
    floatType isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
    if (debye_ScaleB) isoB *= std::exp(-ssqr[r]*debye_ScaleB);
    floatType symmOccBfac(linearTerm[a]*isoB);
    floatType scat(fo_plus_fp[r][t]*symmOccBfac);
              symmOccBfac*=CLUSTER[t].debye(ssqr[r]); //clusters atomtype XX, 1 otherwise
    floatType fpp(AtomFdp[t]*symmOccBfac);
    // Add A and B components for all symmetry elements
    ReFHpos = ImFHpos = ReFHneg = ImFHneg = ZERO;
    for (unsigned isym = 0; isym < NSYMP; isym++)
    {
      floatType anisoB(1);
      floatType anisoScat(scat);
      floatType anisoFpp(fpp);
      if (atoms[a].SCAT.flags.use_u_aniso_only())
      {
        anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],atoms[a].SCAT.u_star);
        anisoScat = scat*anisoB;
        anisoFpp = fpp*anisoB;
      }
      floatType theta = rotMiller[r][isym][0]*atoms[a].SCAT.site[0] +
                        rotMiller[r][isym][1]*atoms[a].SCAT.site[1] +
                        rotMiller[r][isym][2]*atoms[a].SCAT.site[2] +
                        traMiller[r][isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
      cmplxType sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
      floatType costheta = std::real(sincostheta);
      floatType sintheta = std::imag(sincostheta);
#else
      theta *= scitbx::constants::two_pi;
      floatType costheta = std::cos(theta);
      floatType sintheta = std::sin(theta);
#endif
      floatType anisoScat_costheta(anisoScat*costheta);
      floatType anisoScat_sintheta(anisoScat*sintheta);
      floatType anisoFpp_costheta(anisoFpp*costheta);
      floatType anisoFpp_sintheta(anisoFpp*sintheta);
      floatType symReFHpos(anisoScat_costheta - anisoFpp_sintheta);
      floatType symImFHpos(anisoScat_sintheta + anisoFpp_costheta);
      ReFHpos += symReFHpos;
      ImFHpos += symImFHpos;
      //Store the complex conjugate
      floatType symReFHneg(anisoScat_costheta + anisoFpp_sintheta);
      floatType symImFHneg(anisoScat_sintheta - anisoFpp_costheta);
      ReFHneg += symReFHneg;
      ImFHneg += symImFHneg;
      if (!protocol.FIX_XYZ)
      {
        dvect3 TWOPI_rotMiller;
        for (int i = 0; i < 3; i++)
          TWOPI_rotMiller[i] = rotMiller[r][isym][i]*scitbx::constants::two_pi;

        dReFHpos_by_dX[a] -= TWOPI_rotMiller[0]*symImFHpos;
        dImFHpos_by_dX[a] += TWOPI_rotMiller[0]*symReFHpos;
        dReFHneg_by_dX[a] -= TWOPI_rotMiller[0]*symImFHneg;
        dImFHneg_by_dX[a] += TWOPI_rotMiller[0]*symReFHneg;

        dReFHpos_by_dY[a] -= TWOPI_rotMiller[1]*symImFHpos;
        dImFHpos_by_dY[a] += TWOPI_rotMiller[1]*symReFHpos;
        dReFHneg_by_dY[a] -= TWOPI_rotMiller[1]*symImFHneg;
        dImFHneg_by_dY[a] += TWOPI_rotMiller[1]*symReFHneg;

        dReFHpos_by_dZ[a] -= TWOPI_rotMiller[2]*symImFHpos;
        dImFHpos_by_dZ[a] += TWOPI_rotMiller[2]*symReFHpos;
        dReFHneg_by_dZ[a] -= TWOPI_rotMiller[2]*symImFHneg;
        dImFHneg_by_dZ[a] += TWOPI_rotMiller[2]*symReFHneg;

        d2ReFHpos_by_dX2[a] -= fn::pow2(TWOPI_rotMiller[0])*symReFHpos;
        d2ImFHpos_by_dX2[a] -= fn::pow2(TWOPI_rotMiller[0])*symImFHpos;
        d2ReFHneg_by_dX2[a] -= fn::pow2(TWOPI_rotMiller[0])*symReFHneg;
        d2ImFHneg_by_dX2[a] -= fn::pow2(TWOPI_rotMiller[0])*symImFHneg;

        d2ReFHpos_by_dY2[a] -= fn::pow2(TWOPI_rotMiller[1])*symReFHpos;
        d2ImFHpos_by_dY2[a] -= fn::pow2(TWOPI_rotMiller[1])*symImFHpos;
        d2ReFHneg_by_dY2[a] -= fn::pow2(TWOPI_rotMiller[1])*symReFHneg;
        d2ImFHneg_by_dY2[a] -= fn::pow2(TWOPI_rotMiller[1])*symImFHneg;

        d2ReFHpos_by_dZ2[a] -= fn::pow2(TWOPI_rotMiller[2])*symReFHpos;
        d2ImFHpos_by_dZ2[a] -= fn::pow2(TWOPI_rotMiller[2])*symImFHpos;
        d2ReFHneg_by_dZ2[a] -= fn::pow2(TWOPI_rotMiller[2])*symReFHneg;
        d2ImFHneg_by_dZ2[a] -= fn::pow2(TWOPI_rotMiller[2])*symImFHneg;
      }

      if (!(protocol.FIX_FDP || input_fix_fdp[t]))
      {
        dReFHpos_by_dFdp[t] -= sintheta*symmOccBfac*anisoB;
        dImFHpos_by_dFdp[t] += costheta*symmOccBfac*anisoB;
        dReFHneg_by_dFdp[t] += sintheta*symmOccBfac*anisoB;
        dImFHneg_by_dFdp[t] -= costheta*symmOccBfac*anisoB;
      }

      if (atoms[a].SCAT.flags.use_u_aniso_only() && !protocol.FIX_BFAC)
      {
        dmat6 millerSqr = cctbx::adptbx::debye_waller_factor_u_star_gradient_coefficients(rotMiller[r][isym],scitbx::type_holder<floatType>());
        for (unsigned n = 0; n < 6; n++)
        {
          millerSqr[n] *= scitbx::constants::two_pi_sq;
          dReFHpos_by_dAB[a][n] -= millerSqr[n]*symReFHpos;
          dImFHpos_by_dAB[a][n] -= millerSqr[n]*symImFHpos;
          dReFHneg_by_dAB[a][n] -= millerSqr[n]*symReFHneg;
          dImFHneg_by_dAB[a][n] -= millerSqr[n]*symImFHneg;
          d2ReFHpos_by_dAB2[a][n] += fn::pow2(-millerSqr[n])*symReFHpos;
          d2ImFHpos_by_dAB2[a][n] += fn::pow2(-millerSqr[n])*symImFHpos;
          d2ReFHneg_by_dAB2[a][n] += fn::pow2(-millerSqr[n])*symReFHneg;
          d2ImFHneg_by_dAB2[a][n] += fn::pow2(-millerSqr[n])*symImFHneg;
        }
      }
    }//end sym

    if (!protocol.FIX_OCC && atoms[a].SCAT.occupancy)
    {
      dReFHpos_by_dO[a] = atoms[a].SCAT.occupancy ? ReFHpos/atoms[a].SCAT.occupancy : ZERO;
      dImFHpos_by_dO[a] = atoms[a].SCAT.occupancy ? ImFHpos/atoms[a].SCAT.occupancy : ZERO;
      dReFHneg_by_dO[a] = atoms[a].SCAT.occupancy ? ReFHneg/atoms[a].SCAT.occupancy : ZERO;
      dImFHneg_by_dO[a] = atoms[a].SCAT.occupancy ? ImFHneg/atoms[a].SCAT.occupancy : ZERO;

      d2ReFHpos_by_dO2[a] = ZERO;
      d2ImFHpos_by_dO2[a] = ZERO;
      d2ReFHneg_by_dO2[a] = ZERO;
      d2ImFHneg_by_dO2[a] = ZERO;
    }

    if (!atoms[a].SCAT.flags.use_u_aniso_only() && !protocol.FIX_BFAC)
    {
      dReFHpos_by_dIB[a] = -ssqr[r]*ReFHpos*b_as_u_term;
      dImFHpos_by_dIB[a] = -ssqr[r]*ImFHpos*b_as_u_term;
      dReFHneg_by_dIB[a] = -ssqr[r]*ReFHneg*b_as_u_term;
      dImFHneg_by_dIB[a] = -ssqr[r]*ImFHneg*b_as_u_term;

      d2ReFHpos_by_dIB2[a] = fn::pow2(-ssqr[r]*b_as_u_term)*ReFHpos;
      d2ImFHpos_by_dIB2[a] = fn::pow2(-ssqr[r]*b_as_u_term)*ImFHpos;
      d2ReFHneg_by_dIB2[a] = fn::pow2(-ssqr[r]*b_as_u_term)*ReFHneg;
      d2ImFHneg_by_dIB2[a] = fn::pow2(-ssqr[r]*b_as_u_term)*ImFHneg;
    }
  } //end loop over atoms

  //add gradients for linked occupancy
  dReFHpos_by_dO_px = dImFHpos_by_dO_px = dReFHneg_by_dO_px = dImFHneg_by_dO_px = ZERO;
  int npx(0);
  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED && atoms[a].SCAT.label == "px")
  {
    dReFHpos_by_dO_px += dReFHpos_by_dO[a];
    dImFHpos_by_dO_px += dImFHpos_by_dO[a];
    dReFHneg_by_dO_px += dReFHneg_by_dO[a];
    dImFHneg_by_dO_px += dImFHneg_by_dO[a];
    npx++;
  }
  if (npx)
  {
    dReFHpos_by_dO_px /= npx;
    dImFHpos_by_dO_px /= npx;
    dReFHneg_by_dO_px /= npx;
    dImFHneg_by_dO_px /= npx;
  }

  //initialize Atomic part of d2L_by_dPar2 array
  for (int j = nscales_ref; j < npars_ref; j++) d2L_by_dPar2[j] = ZERO;

  //accumulate derivatives using the chain rule
  int i(nscales_ref),m(nscales_all);

  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
        dvect3 x_diag_hess;
    x_diag_hess[0] =    d2ImFHneg_by_dX2[a]*dL_by_dImFHneg +
                        d2ReFHneg_by_dX2[a]*dL_by_dReFHneg +
                        d2ImFHpos_by_dX2[a]*dL_by_dImFHpos +
                        d2ReFHpos_by_dX2[a]*dL_by_dReFHpos +
     dImFHneg_by_dX[a]*(dImFHneg_by_dX[a]*d2L_by_dImFHneg2 + dReFHneg_by_dX[a]*d2L_by_dReFHneg_dImFHneg +
                        dImFHpos_by_dX[a]*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dX[a]*d2L_by_dReFHpos_dImFHneg) +
     dReFHneg_by_dX[a]*(dImFHneg_by_dX[a]*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dX[a]*d2L_by_dReFHneg2 +
                        dImFHpos_by_dX[a]*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dX[a]*d2L_by_dReFHpos_dReFHneg) +
     dImFHpos_by_dX[a]*(dImFHneg_by_dX[a]*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dX[a]*d2L_by_dImFHpos_dReFHneg +
                        dImFHpos_by_dX[a]*d2L_by_dImFHpos2 + dReFHpos_by_dX[a]*d2L_by_dReFHpos_dImFHpos) +
     dReFHpos_by_dX[a]*(dImFHneg_by_dX[a]*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dX[a]*d2L_by_dReFHpos_dReFHneg +
                        dImFHpos_by_dX[a]*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dX[a]*d2L_by_dReFHpos2);

    x_diag_hess[1] =    d2ImFHneg_by_dY2[a]*dL_by_dImFHneg +
                        d2ReFHneg_by_dY2[a]*dL_by_dReFHneg +
                        d2ImFHpos_by_dY2[a]*dL_by_dImFHpos +
                        d2ReFHpos_by_dY2[a]*dL_by_dReFHpos +
     dImFHneg_by_dY[a]*(dImFHneg_by_dY[a]*d2L_by_dImFHneg2 + dReFHneg_by_dY[a]*d2L_by_dReFHneg_dImFHneg +
                        dImFHpos_by_dY[a]*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dY[a]*d2L_by_dReFHpos_dImFHneg) +
     dReFHneg_by_dY[a]*(dImFHneg_by_dY[a]*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dY[a]*d2L_by_dReFHneg2 +
                        dImFHpos_by_dY[a]*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dY[a]*d2L_by_dReFHpos_dReFHneg) +
     dImFHpos_by_dY[a]*(dImFHneg_by_dY[a]*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dY[a]*d2L_by_dImFHpos_dReFHneg +
                        dImFHpos_by_dY[a]*d2L_by_dImFHpos2 + dReFHpos_by_dY[a]*d2L_by_dReFHpos_dImFHpos) +
     dReFHpos_by_dY[a]*(dImFHneg_by_dY[a]*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dY[a]*d2L_by_dReFHpos_dReFHneg +
                        dImFHpos_by_dY[a]*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dY[a]*d2L_by_dReFHpos2);

    x_diag_hess[2] =    d2ImFHneg_by_dZ2[a]*dL_by_dImFHneg +
                        d2ReFHneg_by_dZ2[a]*dL_by_dReFHneg +
                        d2ImFHpos_by_dZ2[a]*dL_by_dImFHpos +
                        d2ReFHpos_by_dZ2[a]*dL_by_dReFHpos +
     dImFHneg_by_dZ[a]*(dImFHneg_by_dZ[a]*d2L_by_dImFHneg2 + dReFHneg_by_dZ[a]*d2L_by_dReFHneg_dImFHneg +
                        dImFHpos_by_dZ[a]*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dZ[a]*d2L_by_dReFHpos_dImFHneg) +
     dReFHneg_by_dZ[a]*(dImFHneg_by_dZ[a]*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dZ[a]*d2L_by_dReFHneg2 +
                        dImFHpos_by_dZ[a]*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dZ[a]*d2L_by_dReFHpos_dReFHneg) +
     dImFHpos_by_dZ[a]*(dImFHneg_by_dZ[a]*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dZ[a]*d2L_by_dImFHpos_dReFHneg +
                        dImFHpos_by_dZ[a]*d2L_by_dImFHpos2 + dReFHpos_by_dZ[a]*d2L_by_dReFHpos_dImFHpos) +
     dReFHpos_by_dZ[a]*(dImFHneg_by_dZ[a]*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dZ[a]*d2L_by_dReFHpos_dReFHneg +
                        dImFHpos_by_dZ[a]*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dZ[a]*d2L_by_dReFHpos2);

        if (atoms[a].n_xyz == 3)
          for (int x = 0; x < 3; x++) d2L_by_dPar2[i++] += x_diag_hess[x];
        else
        {
          af_float  hess6(6,ZERO); //upper triangle matrix (symmetric)
          int d[3] = {0,3,5};  //index to unconstrained diagonal elements
          for (int x = 0; x < 3; x++) hess6[d[x]] = x_diag_hess[x];
          //return array n_xyz*n_xyz/2 elements i.e upper triangle
          af::small<floatType,6> hess_constrained = site_sym_table.get(a).site_constraints().independent_curvatures(hess6.const_ref());
          int1D c(atoms[a].n_xyz,0); //index to constrained diagonal elements
          if (atoms[a].n_xyz == 2) c[1] = 2;
          for (int x = 0; x < atoms[a].n_xyz; x++) d2L_by_dPar2[i++] += hess_constrained[c[x]];
        }
    }

    if (refinePar[m++])
    {
      if (atoms[a].SCAT.label != "px")
        d2L_by_dPar2[i++] =   fn::pow2(dReFHpos_by_dO[a])*d2L_by_dReFHpos2
                            + fn::pow2(dImFHpos_by_dO[a])*d2L_by_dImFHpos2
                            + fn::pow2(dReFHneg_by_dO[a])*d2L_by_dReFHneg2
                            + fn::pow2(dImFHneg_by_dO[a])*d2L_by_dImFHneg2
                            + 2*dReFHpos_by_dO[a]*dImFHpos_by_dO[a]*d2L_by_dReFHpos_dImFHpos
                            + 2*dReFHpos_by_dO[a]*dReFHneg_by_dO[a]*d2L_by_dReFHpos_dReFHneg
                            + 2*dReFHpos_by_dO[a]*dImFHneg_by_dO[a]*d2L_by_dReFHpos_dImFHneg
                            + 2*dReFHneg_by_dO[a]*dImFHneg_by_dO[a]*d2L_by_dReFHneg_dImFHneg
                            + 2*dReFHneg_by_dO[a]*dImFHpos_by_dO[a]*d2L_by_dImFHpos_dReFHneg
                            + 2*dImFHpos_by_dO[a]*dImFHneg_by_dO[a]*d2L_by_dImFHpos_dImFHneg ;
      else //linked gradients for joint occupancy of px atoms
        d2L_by_dPar2[i++] =   fn::pow2(dReFHpos_by_dO_px)*d2L_by_dReFHpos2
                            + fn::pow2(dImFHpos_by_dO_px)*d2L_by_dImFHpos2
                            + fn::pow2(dReFHneg_by_dO_px)*d2L_by_dReFHneg2
                            + fn::pow2(dImFHneg_by_dO_px)*d2L_by_dImFHneg2
                            + 2*dReFHpos_by_dO_px*dImFHpos_by_dO_px*d2L_by_dReFHpos_dImFHpos
                            + 2*dReFHpos_by_dO_px*dReFHneg_by_dO_px*d2L_by_dReFHpos_dReFHneg
                            + 2*dReFHpos_by_dO_px*dImFHneg_by_dO_px*d2L_by_dReFHpos_dImFHneg
                            + 2*dReFHneg_by_dO_px*dImFHneg_by_dO_px*d2L_by_dReFHneg_dImFHneg
                            + 2*dReFHneg_by_dO_px*dImFHpos_by_dO_px*d2L_by_dImFHpos_dReFHneg
                            + 2*dImFHpos_by_dO_px*dImFHneg_by_dO_px*d2L_by_dImFHpos_dImFHneg ;
    }

    if (refinePar[m++])
    {
      if (!atoms[a].SCAT.flags.use_u_aniso_only())
      {
        d2L_by_dPar2[i++] = d2ImFHneg_by_dIB2[a]*dL_by_dImFHneg +
                            d2ReFHneg_by_dIB2[a]*dL_by_dReFHneg +
                            d2ImFHpos_by_dIB2[a]*dL_by_dImFHpos +
                            d2ReFHpos_by_dIB2[a]*dL_by_dReFHpos +
         dImFHneg_by_dIB[a]*(dImFHneg_by_dIB[a]*d2L_by_dImFHneg2 + dReFHneg_by_dIB[a]*d2L_by_dReFHneg_dImFHneg +
                            dImFHpos_by_dIB[a]*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dIB[a]*d2L_by_dReFHpos_dImFHneg) +
         dReFHneg_by_dIB[a]*(dImFHneg_by_dIB[a]*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dIB[a]*d2L_by_dReFHneg2 +
                            dImFHpos_by_dIB[a]*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dIB[a]*d2L_by_dReFHpos_dReFHneg) +
         dImFHpos_by_dIB[a]*(dImFHneg_by_dIB[a]*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dIB[a]*d2L_by_dImFHpos_dReFHneg +
                            dImFHpos_by_dIB[a]*d2L_by_dImFHpos2 + dReFHpos_by_dIB[a]*d2L_by_dReFHpos_dImFHpos) +
         dReFHpos_by_dIB[a]*(dImFHneg_by_dIB[a]*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dIB[a]*d2L_by_dReFHpos_dReFHneg +
                            dImFHpos_by_dIB[a]*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dIB[a]*d2L_by_dReFHpos2) ;
      }
      else
      {
        dmat6 u_star_diag_hess;
        for (unsigned n = 0; n < 6; n++)
          u_star_diag_hess[n] =
                              d2ReFHpos_by_dAB2[a][n]*dL_by_dReFHpos
                            + d2ImFHpos_by_dAB2[a][n]*dL_by_dImFHpos
                            + d2ReFHneg_by_dAB2[a][n]*dL_by_dReFHneg
                            + d2ImFHneg_by_dAB2[a][n]*dL_by_dImFHneg
     + dReFHpos_by_dAB[a][n]*(dReFHpos_by_dAB[a][n]*d2L_by_dReFHpos2 + dImFHpos_by_dAB[a][n]*d2L_by_dReFHpos_dImFHpos
                            + dReFHneg_by_dAB[a][n]*d2L_by_dReFHpos_dReFHneg + dImFHneg_by_dAB[a][n]*d2L_by_dReFHpos_dImFHneg)
     + dImFHpos_by_dAB[a][n]*(dReFHpos_by_dAB[a][n]*d2L_by_dReFHpos_dImFHpos + dImFHpos_by_dAB[a][n]*d2L_by_dImFHpos2
                            + dReFHneg_by_dAB[a][n]*d2L_by_dImFHpos_dReFHneg + dImFHneg_by_dAB[a][n]*d2L_by_dImFHpos_dImFHneg)
     + dReFHneg_by_dAB[a][n]*(dReFHpos_by_dAB[a][n]*d2L_by_dReFHpos_dReFHneg + dImFHpos_by_dAB[a][n]*d2L_by_dImFHpos_dReFHneg
                            + dReFHneg_by_dAB[a][n]*d2L_by_dReFHneg2 + dImFHneg_by_dAB[a][n]*d2L_by_dReFHneg_dImFHneg)
     + dImFHneg_by_dAB[a][n]*(dReFHpos_by_dAB[a][n]*d2L_by_dReFHpos_dImFHneg + dImFHpos_by_dAB[a][n]*d2L_by_dImFHpos_dImFHneg
                            + dReFHneg_by_dAB[a][n]*d2L_by_dReFHneg_dImFHneg + dImFHneg_by_dAB[a][n]*d2L_by_dImFHneg2) ;
        if (atoms[a].n_adp == 6)
          for (int n = 0; n < 6; n++) d2L_by_dPar2[i++] += u_star_diag_hess[n];
        else
        {
          af_float  hess21(21,ZERO); //upper triangle matrix (symmetric)
          int d[6] = {0,6,11,15,18,20};  //index to unconstrained diagonal elements
          for (int n = 0; n < 6; n++) hess21[d[n]] = u_star_diag_hess[n];
          //return array n_adp*n_adp/2 elements i.e upper triangle
          af_float hess_constrained = site_sym_table.get(a).adp_constraints().independent_curvatures(hess21.const_ref());
          int1D c(atoms[a].n_adp,0); //index to constrained diagonal elements
          if (atoms[a].n_adp == 2) c[1] = 2;
          else if (atoms[a].n_adp == 3) {c[1] = 3; c[2] = 5;}
          else if (atoms[a].n_adp == 4) {c[1] = 4; c[2] = 7; c[3] = 9;}
          else if (atoms[a].n_adp == 5) {c[1] = 5; c[2] = 9; c[3] = 12; c[4] = 14;}
          for (int n = 0; n < atoms[a].n_adp; n++) d2L_by_dPar2[i++] += hess_constrained[c[n]];
        }
      }
    }
  } //loop over atoms
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    if (refinePar[m++])
    d2L_by_dPar2[i++] = fn::pow2(dReFHpos_by_dFdp[t])*d2L_by_dReFHpos2 +
                        fn::pow2(dImFHpos_by_dFdp[t])*d2L_by_dImFHpos2 +
                        fn::pow2(dReFHneg_by_dFdp[t])*d2L_by_dReFHneg2 +
                        fn::pow2(dImFHneg_by_dFdp[t])*d2L_by_dImFHneg2 +
                        2*dReFHpos_by_dFdp[t]*dImFHpos_by_dFdp[t]*d2L_by_dReFHpos_dImFHpos +
                        2*dReFHpos_by_dFdp[t]*dReFHneg_by_dFdp[t]*d2L_by_dReFHpos_dReFHneg +
                        2*dReFHpos_by_dFdp[t]*dImFHneg_by_dFdp[t]*d2L_by_dReFHpos_dImFHneg +
                        2*dReFHneg_by_dFdp[t]*dImFHneg_by_dFdp[t]*d2L_by_dReFHneg_dImFHneg +
                        2*dReFHneg_by_dFdp[t]*dImFHpos_by_dFdp[t]*d2L_by_dImFHpos_dReFHneg +
                        2*dImFHpos_by_dFdp[t]*dImFHneg_by_dFdp[t]*d2L_by_dImFHpos_dImFHneg;
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
#ifdef __FDHESSATOMIC__
  std::cout << "=== Second Derivative Test (r=" << r << ") ===\n";
  std::cout <<"Ratio      forward "<<d2L_by_dtest2/FD_test_forward <<
                " backward " << d2L_by_dtest2/FD_test_backward <<
                " on function " << d2L_by_dtest2/FD_test_on_f << " <===\n";
  std::cout <<"Analytic   "<<d2L_by_dtest2<< std::endl;
  std::cout <<"FD forward "<<FD_test_forward <<
                " FD backward " << FD_test_backward <<
                " FD on function " << FD_test_on_f << "\n";
  if (hess_refl++ >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
#endif
}

void RefineSAD::d2L_by_dSigmaaPar2(unsigned r)
{
//#define __FDHESSSIGMAA__
#ifdef __FDHESSSIGMAA__
   //need to set SADM ALL
static int hess_refl = 0;
  if (hess_refl == 0)
  for (int i = 0; i < nsigmaa_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  //hint: set BINS NUM 1
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  PHASER_ASSERT(ipar < nsigmaa_ref);
  int itest(0);
  floatType *test;
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (itest++ == ipar) test = &(DphiA_bin[rbin[r]]);
    if (itest++ == ipar) test = &(DphiB_bin[rbin[r]]);
    if (itest++ == ipar) test = &(SP_bin[rbin[r]]);
    if (itest++ == ipar) test = &(SDsqr_bin[rbin[r]]);
  }
  calcSigmaaData();
  floatType &dL_by_dtest = dL_by_dPar[ipar];
  floatType &d2L_by_dtest2 = d2L_by_dPar2[ipar];
  floatType f_start = acentricReflGradSAD2(r);
  dL_by_dSigmaaPar(r); //chain rule to get dL_by_dtest
  floatType grad_start = dL_by_dtest;
  (*test) += shift;
  calcSigmaaData();
  floatType f_forward = acentricReflGradSAD2(r);
  dL_by_dSigmaaPar(r);
  floatType grad_forward = dL_by_dtest;
  (*test) -= shift;
  (*test) -= shift;
  calcSigmaaData();
  floatType f_backward = acentricReflGradSAD2(r);
  dL_by_dSigmaaPar(r);
  floatType grad_backward = dL_by_dtest;
  (*test) += shift;
  calcSigmaaData();
  floatType FD_test_forward = (grad_forward-grad_start)/shift;
  floatType FD_test_on_f = (f_forward-2*f_start+f_backward)/shift/shift;
  floatType FD_test_backward = (grad_start-grad_backward)/shift;
  acentricReflHessSAD2(r);
#endif

  //initialize Sigmaa part of d2L_by_dPar2 array
  for (int j = 0; j < nsigmaa_ref; j++) d2L_by_dPar2[j] = 0;

  int i(0),m(0);
  floatType ZERO(0.);
  for (unsigned s = 0; s < bin.numbins(); s++)
    {
      bool bin_match(s == rbin[r]);
      if (refinePar[m++]) d2L_by_dPar2[i++] = bin_match ? d2L_by_dSA2 : ZERO;
      if (refinePar[m++]) d2L_by_dPar2[i++] = bin_match ? d2L_by_dSB2 : ZERO;
      if (refinePar[m++]) d2L_by_dPar2[i++] = bin_match ? d2L_by_dSP2 : ZERO;
      if (refinePar[m++]) d2L_by_dPar2[i++] = bin_match ? d2L_by_dSDsqr2 : ZERO;
    }
  PHASER_ASSERT(m == nsigmaa_all);
  PHASER_ASSERT(i == nsigmaa_ref);

#ifdef __FDHESSSIGMAA__
  std::cout << "=== Second Derivative Test (r=" << r << ") ===\n";
  std::cout <<"Ratio    forward "<<d2L_by_dtest2/FD_test_forward <<
                " backward " << d2L_by_dtest2/FD_test_backward <<
                " on function " << d2L_by_dtest2/FD_test_on_f << "\n";
  std::cout <<"Analytic "<< d2L_by_dtest2 << "\n";
  std::cout <<"FD       forward "<<FD_test_forward <<
                " backward " << FD_test_backward <<
                " on function " << FD_test_on_f << "\n";
  if (hess_refl++ >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
#endif
}

floatType RefineSAD::constrain_restrain_hessian(TNT::Fortran_Matrix<floatType>& Hessian)
{
  //Add special position constraints to hessian
  //Add sphericity restraint terms to hessian
  int m(nscales_all),i(nscales_ref);
  int twenty_one(6*(6+1)/2); //upper triangle matrix
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) i+=atoms[a].n_xyz;
    if (refinePar[m++])
    {
      //half, because mean is a function of the occupancy too (a-(a+b)/2)
      if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
        Hessian(i+1,i+1) += 0.5/fn::pow2(PTNCS.EP.LINK_SIGMA);
      i++;
    }
    if (refinePar[m++])
    {
      floatType iso_hess_add(0);
      af::small<floatType, 6> aniso_hess_add(6,0.0);
  // 0, 1, 2, 3, 4, 5,
  // 1, 6, 7, 8, 9,10,
  // 2, 7,11,12,13,14,
  // 3, 8,12,15,16,17,
  // 4, 9,13,16,18,19,
  // 5,10,14,17,19,20
      if (SPHERICITY.RESTRAINT)
      {
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          af_float  sphericity_hess21(twenty_one,0.0); //upper triangle matrix (symmetric)
          // Add sphericity restraint terms
        //Convert isotropic equivalent as in largeShifts
          dmat6 sigmaSphericityBeta;
          sigmaSphericityBeta[0] = 0.25*SPHERICITY.SIGMA*aStar()*aStar();
          sigmaSphericityBeta[1] = 0.25*SPHERICITY.SIGMA*bStar()*bStar();
          sigmaSphericityBeta[2] = 0.25*SPHERICITY.SIGMA*cStar()*cStar();
          sigmaSphericityBeta[3] = 0.25*SPHERICITY.SIGMA*aStar()*bStar();
          sigmaSphericityBeta[4] = 0.25*SPHERICITY.SIGMA*aStar()*cStar();
          sigmaSphericityBeta[5] = 0.25*SPHERICITY.SIGMA*bStar()*cStar();
          dmat6 u_star_sphericity = cctbx::adptbx::beta_as_u_star(sigmaSphericityBeta);
          cctbx::adptbx::factor_u_star_u_iso<floatType> factor(getCctbxUC(),atoms[a].SCAT.u_star);
          dmat6 u_iso_coeffs = cctbx::adptbx::u_iso_as_u_star(getCctbxUC(),1.0);
          dmat6 u_star_coeffs;
          for (unsigned n = 0; n < 6; n++)
          {
            dmat6 pick(n==0?1:0,n==1?1:0,n==2?1:0,n==3?1:0,n==4?1:0,n==5?1:0);
            u_star_coeffs[n] = cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),pick); //pick out the coefficients
          }
          int ni_nj(0); //index for upper triangle
          for (unsigned ni = 0; ni < 6; ni++)
            for (unsigned nj = ni; nj < 6; nj++,ni_nj++) //only calculate upper triangle
            {
              for (unsigned n = 0; n < 6; n++)
              {
                floatType dBetaIso_by_dBetaAnoI = u_star_coeffs[ni]*u_iso_coeffs[n];
                floatType dBetaAno_by_dBetaAnoI = (n == ni) ? 1. : 0.;
                floatType dBetaIso_by_dBetaAnoJ = u_star_coeffs[nj]*u_iso_coeffs[n];
                floatType dBetaAno_by_dBetaAnoJ = (n == nj) ? 1. : 0.;
                // Hessian at this point refers to LL, not minusLL, so subtract
                sphericity_hess21[ni_nj] += (dBetaAno_by_dBetaAnoJ-dBetaIso_by_dBetaAnoJ) *
                                          (dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
                                                fn::pow2(u_star_sphericity[n]);
              }
            }
          //return array n_adp*n_adp/2 elements i.e upper triangle
          af_float sphericity_hess_constrained = site_sym_table.get(a).adp_constraints().independent_curvatures(sphericity_hess21.const_ref());
          int1D c(atoms[a].n_adp,0); //index to constrained diagonal elements
          if (atoms[a].n_adp == 2) c[1] = 2;
          else if (atoms[a].n_adp == 3) {c[1] = 3; c[2] = 5;}
          else if (atoms[a].n_adp == 4) {c[1] = 4; c[2] = 7; c[3] = 9;}
          else if (atoms[a].n_adp == 5) {c[1] = 5; c[2] = 9; c[3] = 12; c[4] = 14;}
          for (int n = 0; n < atoms[a].n_adp; n++) aniso_hess_add[n] += sphericity_hess_constrained[c[n]];
        }
      }
      if (WILSON.RESTRAINT)
      {
        floatType sigmaWilsonU = cctbx::adptbx::b_as_u(WILSON.SIGMA);
        //floatType WilsonU = cctbx::adptbx::b_as_u(WilsonB);
        if (!atoms[a].SCAT.flags.use_u_aniso_only())
        {
          iso_hess_add += 1/fn::pow2(sigmaWilsonU);
        }
        else
        {
          af_float  wilson_hess21(twenty_one,0.0); //upper triangle matrix (symmetric)
          int d[6] = {0,6,11,15,18,20};  //index to unconstrained diagonal elements
         // floatType u_iso = cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),atoms[a].SCAT.u_star);
          for (unsigned n = 0; n < 6; n++)
          {
            dmat6 pick(n==0?1:0,n==1?1:0,n==2?1:0,n==3?1:0,n==4?1:0,n==5?1:0);
            wilson_hess21[d[n]] = fn::pow2(cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),pick)/sigmaWilsonU);
          }
          //return array n_adp*n_adp/2 elements i.e upper triangle
          af_float wilson_hess_constrained = site_sym_table.get(a).adp_constraints().independent_curvatures(wilson_hess21.const_ref());
          int1D c(atoms[a].n_adp,0); //index to constrained diagonal elements
          if (atoms[a].n_adp == 2) c[1] = 2;
          else if (atoms[a].n_adp == 3) {c[1] = 3; c[2] = 5;}
          else if (atoms[a].n_adp == 4) {c[1] = 4; c[2] = 7; c[3] = 9;}
          else if (atoms[a].n_adp == 5) {c[1] = 5; c[2] = 9; c[3] = 12; c[4] = 14;}
          for (int n = 0; n < atoms[a].n_adp; n++) aniso_hess_add[n] += wilson_hess_constrained[c[n]];
        }
      } //wilson restraint
      if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
      {
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          for (int n = 0; n < atoms[a].n_adp; n++)
            aniso_hess_add[n] += 0.5/fn::pow2(PTNCS.EP.LINK_SIGMA/10000);
        }
        else
          iso_hess_add += 0.5/fn::pow2(PTNCS.EP.LINK_SIGMA);
      }
      if (!atoms[a].SCAT.flags.use_u_aniso_only())
        Hessian(i+1,i+1) += iso_hess_add;
      else
        for (int n = 0; n < atoms[a].n_adp; n++)
          Hessian(i+n+1,i+n+1) += aniso_hess_add[n];
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) i++;
      else i += atoms[a].n_adp;
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++])
    {
      floatType AtomSigmaFdp = FDP.SIGMA*AtomFdpInit[t];
      Hessian(i+1,i+1) += FDP.RESTRAINT ?  1/fn::pow2(AtomSigmaFdp) : 0;
      i++;
    }

  PHASER_ASSERT(m == refinePar.size());
  PHASER_ASSERT(i == npars_ref);

  floatType totLogLike(0);
  if (SPHERICITY.RESTRAINT)
    totLogLike += sphericity_restraint_likelihood();
  if (WILSON.RESTRAINT)
    totLogLike += wilson_restraint_likelihood();
  if (FDP.RESTRAINT)
    totLogLike += fdp_restraint_likelihood();
  if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
    totLogLike += ptncs_link_restraint_likelihood();
  return totLogLike;
}

void RefineSAD::d2L_by_dScalesPar2(unsigned r)
{
//#define __FDHESSSCALE__
#ifdef __FDHESSSCALE__
static int hess_refl = 0;
   //need to set SADM ALL
  if (hess_refl == 0)
  for (int i = nsigmaa_ref; i < nscales_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  int t = atomtype2t[atoms[0].scattering_type];
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  PHASER_ASSERT(ipar >= nsigmaa_ref);
  PHASER_ASSERT(ipar < nscales_ref);
  int itest(nsigmaa_all);
  floatType *test;
//-- scales --
  if (input_atoms)
  {
    if (itest++ == ipar) test = &(ScaleK);
    if (itest++ == ipar) test = &(ScaleU);
  }
  if (input_partial)
  {
    if (itest++ == ipar) test = &(PartK);
    if (itest++ == ipar) test = &(PartU);
  }
  floatType &dL_by_dtest = dL_by_dPar[ipar];
  floatType &d2L_by_dtest2 = d2L_by_dPar2[ipar];

  TNT::Vector<floatType> g(npars_ref);
  calcScalesData();
  floatType f_start;
  if (both[r])      f_start = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_start = centricReflGradSAD2(r,true,true);
  else              f_start = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dScalesPar(r);
  floatType grad_start = dL_by_dtest;
  (*test) += shift;
  calcScalesData();
  floatType f_forward;
  if (both[r])      f_forward = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_forward = centricReflGradSAD2(r,true,true);
  else              f_forward = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dScalesPar(r);
  floatType grad_forward = dL_by_dtest;
  (*test) -= shift;
  (*test) -= shift;
  calcScalesData();
  floatType f_backward;
  if (both[r])      f_backward = acentricReflGradSAD2(r,true,true);
  else if (cent[r]) f_backward = centricReflGradSAD2(r,true,true);
  else              f_backward = singletonReflGradSAD2(r,plus[r],true,true);
  dL_by_dScalesPar(r);
  floatType grad_backward = dL_by_dtest;
  (*test) += shift;
  calcScalesData();
  floatType FD_test_forward = (grad_forward-grad_start)/shift;
  floatType FD_test_on_f = (f_forward-2*f_start+f_backward)/shift/shift;
  floatType FD_test_backward = (grad_start-grad_backward)/shift;
#endif

  floatType ZERO(0.);
  //initialize dFH_by_dScalesPar
  dReFHpos_by_dK = dImFHpos_by_dK = dReFHneg_by_dK = dImFHneg_by_dK = ZERO;
  dReFHpos_by_dB = dImFHpos_by_dB = dReFHneg_by_dB = dImFHneg_by_dB = ZERO;
  dReFHpos_by_dPartK = dImFHpos_by_dPartK = dReFHneg_by_dPartK = dImFHneg_by_dPartK = ZERO;
  dReFHpos_by_dPartU = dImFHpos_by_dPartU = dReFHneg_by_dPartU = dImFHneg_by_dPartU = ZERO;

  //initialize d2FH_by_dScalesPar2
  d2ReFHpos_by_dK2 = d2ImFHpos_by_dK2 = d2ReFHneg_by_dK2 = d2ImFHneg_by_dK2 = ZERO;
  d2ReFHpos_by_dB2 = d2ImFHpos_by_dB2 = d2ReFHneg_by_dB2 = d2ImFHneg_by_dB2 = ZERO;
  d2ReFHpos_by_dPartK2 = d2ImFHpos_by_dPartK2 = d2ReFHneg_by_dPartK2 = d2ImFHneg_by_dPartK2 = ZERO;
  d2ReFHpos_by_dPartU2 = d2ImFHpos_by_dPartU2 = d2ReFHneg_by_dPartU2 = d2ImFHneg_by_dPartU2 = ZERO;

  //calculate dFH_by_dScalesPar
  floatType b_as_u_term(0.25*scitbx::constants::eight_pi_sq);

  if (!(protocol.FIX_K && protocol.FIX_B) && input_atoms)
  {
    floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
    cmplxType K = ScaleK*std::exp(-ssqr[r]*debye_ScaleB);
    cmplxType KFApos = K*FApos[r];
    cmplxType KFAneg = K*FAneg[r];
    if (!protocol.FIX_K && ScaleK)
    {
      dReFHpos_by_dK = std::real(KFApos)/ScaleK;
      dReFHneg_by_dK = std::real(KFAneg)/ScaleK;
      dImFHpos_by_dK = std::imag(KFApos)/ScaleK;
      dImFHneg_by_dK = std::imag(KFAneg)/ScaleK;
    }
    if (!protocol.FIX_B)
    {
      dReFHpos_by_dB = -ssqr[r]*b_as_u_term*std::real(KFApos);
      dReFHneg_by_dB = -ssqr[r]*b_as_u_term*std::real(KFAneg);
      dImFHpos_by_dB = -ssqr[r]*b_as_u_term*std::imag(KFApos);
      dImFHneg_by_dB = -ssqr[r]*b_as_u_term*std::imag(KFAneg);
      d2ReFHpos_by_dB2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::real(KFApos);
      d2ReFHneg_by_dB2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::real(KFAneg);
      d2ImFHpos_by_dB2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::imag(KFApos);
      d2ImFHneg_by_dB2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::imag(KFAneg);
    }
  }

  //add partial scattering, which is real
  if (!(protocol.FIX_PARTK && protocol.FIX_PARTU) && input_partial)
  {
    floatType debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
    double K = PartK*std::exp(-ssqr[r]*debye_PartB);
    cmplxType KFPpos = K*FPpos[r];
    cmplxType KFPneg = K*FPneg[r];
    if (!protocol.FIX_PARTK && PartK)
    {
      dReFHpos_by_dPartK = std::real(KFPpos)/PartK;
      dReFHneg_by_dPartK = std::real(KFPneg)/PartK;
      dImFHpos_by_dPartK = std::imag(KFPpos)/PartK;
      dImFHneg_by_dPartK = std::imag(KFPneg)/PartK;
    }
    if (!protocol.FIX_PARTU)
    {
      dReFHpos_by_dPartU = -ssqr[r]*b_as_u_term*std::real(KFPpos);
      dReFHneg_by_dPartU = -ssqr[r]*b_as_u_term*std::real(KFPneg);
      dImFHpos_by_dPartU = -ssqr[r]*b_as_u_term*std::imag(KFPpos);
      dImFHneg_by_dPartU = -ssqr[r]*b_as_u_term*std::imag(KFPneg);
      d2ReFHpos_by_dPartU2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::real(KFPpos);
      d2ReFHneg_by_dPartU2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::real(KFPneg);
      d2ImFHpos_by_dPartU2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::imag(KFPpos);
      d2ImFHneg_by_dPartU2 = fn::pow2(-ssqr[r]*b_as_u_term)*std::imag(KFPneg);
    }
  }

  //initialize Scales part of d2L_by_dPar2 array
  for (int j = nsigmaa_ref; j < nscales_ref; j++) d2L_by_dPar2[j] = ZERO;

  //accumulate derivatives using the chain rule
  int i(nsigmaa_ref),m(nsigmaa_all);

 //ScaleK
  if (input_atoms && refinePar[m++])
  d2L_by_dPar2[i++] =   fn::pow2(dReFHpos_by_dK)*d2L_by_dReFHpos2
                      + fn::pow2(dImFHpos_by_dK)*d2L_by_dImFHpos2
                      + fn::pow2(dReFHneg_by_dK)*d2L_by_dReFHneg2
                      + fn::pow2(dImFHneg_by_dK)*d2L_by_dImFHneg2
                      + 2*dReFHpos_by_dK*dImFHpos_by_dK*d2L_by_dReFHpos_dImFHpos
                      + 2*dReFHpos_by_dK*dReFHneg_by_dK*d2L_by_dReFHpos_dReFHneg
                      + 2*dReFHpos_by_dK*dImFHneg_by_dK*d2L_by_dReFHpos_dImFHneg
                      + 2*dReFHneg_by_dK*dImFHneg_by_dK*d2L_by_dReFHneg_dImFHneg
                      + 2*dReFHneg_by_dK*dImFHpos_by_dK*d2L_by_dImFHpos_dReFHneg
                      + 2*dImFHpos_by_dK*dImFHneg_by_dK*d2L_by_dImFHpos_dImFHneg ;

 //ScaleU
  if (input_atoms && refinePar[m++])
    d2L_by_dPar2[i++] = d2ImFHneg_by_dB2*dL_by_dImFHneg +
                        d2ReFHneg_by_dB2*dL_by_dReFHneg +
                        d2ImFHpos_by_dB2*dL_by_dImFHpos +
                        d2ReFHpos_by_dB2*dL_by_dReFHpos +
     dImFHneg_by_dB*(dImFHneg_by_dB*d2L_by_dImFHneg2 + dReFHneg_by_dB*d2L_by_dReFHneg_dImFHneg +
                        dImFHpos_by_dB*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dB*d2L_by_dReFHpos_dImFHneg) +
     dReFHneg_by_dB*(dImFHneg_by_dB*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dB*d2L_by_dReFHneg2 +
                        dImFHpos_by_dB*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dB*d2L_by_dReFHpos_dReFHneg) +
     dImFHpos_by_dB*(dImFHneg_by_dB*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dB*d2L_by_dImFHpos_dReFHneg +
                        dImFHpos_by_dB*d2L_by_dImFHpos2 + dReFHpos_by_dB*d2L_by_dReFHpos_dImFHpos) +
     dReFHpos_by_dB*(dImFHneg_by_dB*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dB*d2L_by_dReFHpos_dReFHneg +
                        dImFHpos_by_dB*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dB*d2L_by_dReFHpos2); ;

 //PartK
  if (input_partial && refinePar[m++])
  d2L_by_dPar2[i++] =   fn::pow2(dReFHpos_by_dPartK)*d2L_by_dReFHpos2
                      + fn::pow2(dImFHpos_by_dPartK)*d2L_by_dImFHpos2
                      + fn::pow2(dReFHneg_by_dPartK)*d2L_by_dReFHneg2
                      + fn::pow2(dImFHneg_by_dPartK)*d2L_by_dImFHneg2
                      + 2*dReFHpos_by_dPartK*dImFHpos_by_dPartK*d2L_by_dReFHpos_dImFHpos
                      + 2*dReFHpos_by_dPartK*dReFHneg_by_dPartK*d2L_by_dReFHpos_dReFHneg
                      + 2*dReFHpos_by_dPartK*dImFHneg_by_dPartK*d2L_by_dReFHpos_dImFHneg
                      + 2*dReFHneg_by_dPartK*dImFHneg_by_dPartK*d2L_by_dReFHneg_dImFHneg
                      + 2*dReFHneg_by_dPartK*dImFHpos_by_dPartK*d2L_by_dImFHpos_dReFHneg
                      + 2*dImFHpos_by_dPartK*dImFHneg_by_dPartK*d2L_by_dImFHpos_dImFHneg ;

 //PartU
  if (input_partial && refinePar[m++])
    d2L_by_dPar2[i++] = d2ImFHneg_by_dPartU2*dL_by_dImFHneg +
                        d2ReFHneg_by_dPartU2*dL_by_dReFHneg +
                        d2ImFHpos_by_dPartU2*dL_by_dImFHpos +
                        d2ReFHpos_by_dPartU2*dL_by_dReFHpos +
     dImFHneg_by_dPartU*(dImFHneg_by_dPartU*d2L_by_dImFHneg2 + dReFHneg_by_dPartU*d2L_by_dReFHneg_dImFHneg +
                        dImFHpos_by_dPartU*d2L_by_dImFHpos_dImFHneg + dReFHpos_by_dPartU*d2L_by_dReFHpos_dImFHneg) +
     dReFHneg_by_dPartU*(dImFHneg_by_dPartU*d2L_by_dReFHneg_dImFHneg + dReFHneg_by_dPartU*d2L_by_dReFHneg2 +
                        dImFHpos_by_dPartU*d2L_by_dImFHpos_dReFHneg + dReFHpos_by_dPartU*d2L_by_dReFHpos_dReFHneg) +
     dImFHpos_by_dPartU*(dImFHneg_by_dPartU*d2L_by_dImFHpos_dImFHneg + dReFHneg_by_dPartU*d2L_by_dImFHpos_dReFHneg +
                        dImFHpos_by_dPartU*d2L_by_dImFHpos2 + dReFHpos_by_dPartU*d2L_by_dReFHpos_dImFHpos) +
     dReFHpos_by_dPartU*(dImFHneg_by_dPartU*d2L_by_dReFHpos_dImFHneg + dReFHneg_by_dPartU*d2L_by_dReFHpos_dReFHneg +
                        dImFHpos_by_dPartU*d2L_by_dReFHpos_dImFHpos + dReFHpos_by_dPartU*d2L_by_dReFHpos2); ;

  PHASER_ASSERT(m == nscales_all);
  PHASER_ASSERT(i == nscales_ref);
#ifdef __FDHESSSCALE__
  std::cout << "=== Second Derivative Test (r=" << r << ") ===\n";
  std::cout <<"Ratio      forward "<<d2L_by_dtest2/FD_test_forward <<
                " backward " << d2L_by_dtest2/FD_test_backward <<
                " on function " << d2L_by_dtest2/FD_test_on_f << " <===\n";
  std::cout <<"Analytic   "<<d2L_by_dtest2<< std::endl;
  std::cout <<"FD forward "<<FD_test_forward <<
                " FD backward " << FD_test_backward <<
                " FD on function " << FD_test_on_f << "\n";
  if (hess_refl++ >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
#endif
}

Curvatures RefineSAD::d2L_by_dFC2()
{
  Curvatures curvatures;
  scitbx::vec3<floatType> second_derivs;
  floatType totLogLike(0.);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    // In dL_by_dReFHpos etc, FH includes refined ScaleK/U but FC does not
    // Remove this scale factor from dL_by_dFC
    floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
    floatType scale = fn::pow2(ScaleK*std::exp(-ssqr[r]*debye_ScaleB));
    if (cent[r])
    {
      totLogLike += centricReflGradSAD2(r,true,false);
      curvatures.miller.push_back(miller[r]);
      second_derivs[0] = d2L_by_dReFHpos2*scale;
      second_derivs[1] = d2L_by_dImFHpos2*scale;
      second_derivs[2] = d2L_by_dReFHpos_dImFHpos*scale;
      curvatures.d2L_by_dFC2.push_back(second_derivs);
    }
    else if (both[r])
    {
      totLogLike += acentricReflGradSAD2(r,true,false);
      curvatures.miller.push_back(miller[r]);
      second_derivs[0] = d2L_by_dReFHpos2*scale;
      second_derivs[1] = d2L_by_dImFHpos2*scale;
      second_derivs[2] = d2L_by_dReFHpos_dImFHpos*scale;
      curvatures.d2L_by_dFC2.push_back(second_derivs);
      curvatures.miller.push_back(-miller[r]);
      second_derivs[0] = d2L_by_dReFHpos2*scale;
      second_derivs[1] = d2L_by_dImFHpos2*scale;
      second_derivs[2] = d2L_by_dReFHpos_dImFHpos*scale;
      curvatures.d2L_by_dFC2.push_back(second_derivs);
    }
    else if (plus[r])
    {
      totLogLike += singletonReflGradSAD2(r,plus[r],true,false);
      curvatures.miller.push_back(miller[r]);
      second_derivs[0] = d2L_by_dReFHpos2*scale;
      second_derivs[1] = d2L_by_dImFHpos2*scale;
      second_derivs[2] = d2L_by_dReFHpos_dImFHpos*scale;
      curvatures.d2L_by_dFC2.push_back(second_derivs);
    }
    else if (!plus[r])
    {
      totLogLike += singletonReflGradSAD2(r,plus[r],true,false);
      curvatures.miller.push_back(-miller[r]);
      second_derivs[0] = d2L_by_dReFHpos2*scale;
      second_derivs[1] = d2L_by_dImFHpos2*scale;
      second_derivs[2] = d2L_by_dReFHpos_dImFHpos*scale;
      curvatures.d2L_by_dFC2.push_back(second_derivs);
    }
  }
  return curvatures;
}

}//phaser
