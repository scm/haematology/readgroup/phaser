//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/main/Phaser.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/maths.h>
#include <phaser/src/Integration.h>

namespace phaser {

//-- acentric reflections

acentric
RefineSAD::acentricReflIntgSAD1(unsigned& r, bool ANOM_ONLY)
{
  // Phase with likelihood integral in which diagonal elements of covariance matrix
  // are augmented by experimental errors, instead of combining as in integral itself
  // This seems to make little practical difference, but avoids an extra approximation.
  // Also, option to produce phase probabilities (and hence HL coefficients) lacking
  // partial structure contribution.
  int i = ibin[r];
  acentric thisIntg(acentPhsIntg[i].nStep);
  floatType Like(0);
  floatType TWO(2.0);
  cmplxType Dphi(DphiA[r],DphiB[r]);
  cmplxType sigPhi(sigDsqr[r]*Dphi); // Compute back sigPhi term from covariance matrix
  floatType SigmaNeg(sigDsqr[r] + fn::pow2(NEG.SIGF[r]));
  cmplxType Dphi_error(sigPhi/SigmaNeg);
  floatType sigPhisqr(std::max(0.,sigDsqr[r]*(sigDsqr[r]-sigPlus[r])));
  floatType SigmaPos(sigDsqr[r] + fn::pow2(POS.SIGF[r]) - sigPhisqr/SigmaNeg);
  PHASER_ASSERT(SigmaNeg > 0);
  PHASER_ASSERT(SigmaPos > 0);
  floatType SigmaNegInv(1.0/SigmaNeg);
  floatType SigmaPosInv(1.0/SigmaPos);

  //Arguments for exponential will be negative, with the maximum typically close to zero.
  //To attempt to avoid underflow, set initial exponential offset that is small enough
  //to still avoid overflow when summing and squaring for FOM calculation.
  //If necessary, this will be updated and the integral repeated.
  static floatType logMaxFloat(std::log(std::numeric_limits<floatType>::max()));
  static floatType maxArgAllow(logMaxFloat/TWO-std::log(360.)); //maximum arg to allow for exponential
  static floatType minArgAllow(-maxArgAllow); // minimum arg, below which probs will be set to zero

  floatType foneg(NEG.F[r]),fopos(POS.F[r]);
  cmplxType fhneg(FHneg[r]),fhpos(FHpos[r]);
  floatType topExpArg(-std::numeric_limits<floatType>::max());
  floatType expArgOffset(maxArgAllow);
  for (int oloop = 0; oloop < 2; oloop++)
  {
    Like = 0.0;
    for (int p = 0; p < acentPhsIntg[i].nStep; p++)
    {
      cmplxType FCneg(foneg*acentPhsIntg[i].CosAng[p],foneg*acentPhsIntg[i].SinAng[p]);
      floatType expFHneg(std::norm(FCneg - fhneg)*SigmaNegInv);
      floatType absFCpos(std::abs(fhpos + Dphi_error*(FCneg - fhneg)));
      floatType expFOpos(fn::pow2(fopos - absFCpos)*SigmaPosInv);
      floatType expArg = ANOM_ONLY ? -expFOpos : -expFHneg-expFOpos ;
      topExpArg = std::max(topExpArg,expArg);
      floatType BessTerm(eBesselI0(TWO*fopos*absFCpos*SigmaPosInv));
      floatType expArgTotal(expArg+expArgOffset);
      floatType expTerm = (expArgTotal > minArgAllow) ? std::exp(expArgTotal) : 0.;
      floatType phsProb(expTerm*BessTerm);
      Like += phsProb;
      thisIntg.setProb(p,phsProb,acentPhsIntg[i].Ang[p]);
    } //loop over p
    if (Like > 0.) break;
    expArgOffset = maxArgAllow - topExpArg;
  } //loop to apply new offset if necessary

  //Overall scaling factors, except for exponential offset
  Like *= acentPhsIntg[i].Weight;
  Like *= fopos/(scitbx::constants::pi*SigmaPos);
  if (!ANOM_ONLY) Like *= TWO*foneg/SigmaNeg;

  PHASER_ASSERT(Like > 0); // Could probably eliminate now
  thisIntg.LogLike = -log(Like) + expArgOffset; // Remove offset
  //Store measure of relative influence of partial structure for
  //bias-corrected map coefficients
  floatType Xano(std::norm(fhpos-fhneg)*SigmaPosInv);
  floatType Xpart(std::abs(fhneg)*foneg*SigmaNegInv);
  thisIntg.WtPart = (Xpart > 0.) ? Xpart/(Xano + Xpart) : 0.;
  return thisIntg;
}

}//phaser
