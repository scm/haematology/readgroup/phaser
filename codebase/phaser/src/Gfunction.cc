//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Gfunction.h>
#include <phaser/lib/maths.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

Gfunction::Gfunction(SpaceGroup sg,UnitCell uc,int NMOL) : SpaceGroup(sg),UnitCell(uc)
{
  max_ncsT = std::max(2,NMOL);
  NREFL = dE_by_dD = refl_epsfac = refl_G_Vterm = refl_Gsqr_Vterm = 0;
  int dim = max_ncsT*max_ncsT*SpaceGroup::NSYMP;
  PHASER_ASSERT(dim > 0);
  ncsDeltaT.resize(dim,dvect3(0,0,0));
  GfunTensorArray.resize(dim,af::double6(0,0,0.0,0,0));
#ifdef CCTBX_MATH_COS_SIN_TABLE_H
  fast_cos_sin = cctbx::math::cos_sin_lin_interp_table<floatType>(4000);
#endif
}

void Gfunction::init(SpaceGroup sg,UnitCell uc,int NMOL)
{
  max_ncsT = std::max(2,NMOL);
  NREFL = dE_by_dD = refl_epsfac = refl_G_Vterm = refl_Gsqr_Vterm = 0;
  SpaceGroup::setSpaceGroup(sg);
  UnitCell::setUnitCell(uc.getUnitCell());
  int dim = max_ncsT*max_ncsT*SpaceGroup::NSYMP;
  PHASER_ASSERT(dim > 0);
  ncsDeltaT.resize(dim,dvect3(0,0,0));
  GfunTensorArray.resize(dim,af::double6(0,0,0.0,0,0));
}

void Gfunction::initArrays(dvect3 VECTOR,dmat33 ncsRmat,floatType MOLRADIUS,af::shared<miller::index<int> > miller)
{
  NREFL = miller.size();
  G.resize(max_ncsT*max_ncsT*SpaceGroup::NSYMP*NREFL);
  cosTerm.resize(max_ncsT*max_ncsT*SpaceGroup::NSYMP*NREFL);
  sinTerm.resize(max_ncsT*max_ncsT*SpaceGroup::NSYMP*NREFL);

  dmat33 ncsRmat_t = ncsRmat.transpose();

  dmat33 identity(1,0,0,0,1,0,0,0,1);
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();

  // Pre-compute some results that don't depend on reflection number
  floatType radSqr = fn::pow2(MOLRADIUS);
  for (int incs = 0; incs < max_ncsT; incs++)
  for (int jncs = incs; jncs < max_ncsT; jncs++)
  {
    dmat33 ncsR_incs,ncsR_jncs;
    dvect3 ncsT_incs,ncsT_jncs;
    if (incs != jncs) // values of GfunTensorArray and ncsDeltaT won't be used otherwise
    {
      ncsR_incs = (incs ? ncsRmat_t : identity); //always identity for max_ncsT>2
      ncsR_jncs = (jncs ? ncsRmat_t : identity); //always identity for max_ncsT>2
      ncsT_incs = floatType(incs)*VECTOR;
      ncsT_jncs = floatType(jncs)*VECTOR;
      for (int isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        /*
          Metric matrix (represented as tensor) allows quick computation of |s_klmm|^2 and therefore rs^2 for G-function
          If F=fractionalization_matrix, R=Rotsym and (T) indicates transpose, then
          s_klmm = (ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h
          |s_klmm|^2 = s_klmm(T).s_klmm = h(T)*R*F*(ncsR_incs-ncsR_jncs)*(ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h
          The metrix matrix is the part between h(T) and h.  In the GfunTensorArray, factors of 2 are included so
          the unique terms need to be computed only once.
        */
        int i = ((incs*max_ncsT)+jncs)*SpaceGroup::NSYMP+isym;
        ncsDeltaT[i] = SpaceGroup::Rotsym(isym).transpose()*(ncsT_incs-ncsT_jncs); // symmetry translation cancels
        dmat33 metric_matrix = SpaceGroup::Rotsym(isym).transpose()*cctbxUC.fractionalization_matrix()*(ncsR_incs-ncsR_jncs);
        metric_matrix = radSqr*metric_matrix*metric_matrix.transpose();
        GfunTensorArray[i] = af::double6(metric_matrix(0,0),metric_matrix(1,1),metric_matrix(2,2),
                                         2*metric_matrix(0,1),2*metric_matrix(0,2),2*metric_matrix(1,2));
      }
    }
  }

  for (int incs = 0; incs < max_ncsT; incs++)
  for (int jncs = incs; jncs < max_ncsT; jncs++)
  for (int isym = 0; isym < SpaceGroup::NSYMP; isym++)
  for (int r = 0; r < NREFL; r++)
  if (incs != jncs)
  { // prepare NCS-related contribution to epsilon, to be weighted by interference, G-function and Drms terms
    int hh(miller[r][0]), kk(miller[r][1]), ll(miller[r][2]);
    int i = ((incs*max_ncsT)+jncs)*SpaceGroup::NSYMP+isym;
    int ir = i*NREFL + r;
    af::double6 GfunTensor = GfunTensorArray[i];
    floatType rsSqr = GfunTensor[0]*hh*hh + GfunTensor[1]*kk*kk + GfunTensor[2]*ll*ll +
                      GfunTensor[3]*hh*kk + GfunTensor[4]*hh*ll + GfunTensor[5]*kk*ll;
    G[ir] = GfuncOfRSsqr(rsSqr);
    floatType h_dot_T = (miller[r]*ncsDeltaT[i]);
    cmplxType cos_sin;
#if defined CCTBX_MATH_COS_SIN_TABLE_H
    cos_sin = fast_cos_sin.get(h_dot_T); //multiplies by 2*pi internally
#else
    h_dot_T *= scitbx::constants::two_pi;
    cos_sin = cmplxType(std::cos(h_dot_T),std::sin(h_dot_T));
#endif
    cosTerm[ir] = std::real(cos_sin);
    sinTerm[ir] = std::imag(cos_sin);
  }
}

void Gfunction::calcReflTerms(unsigned r)
{
  refl_G_Vterm = refl_Gsqr_Vterm = 0.;
  refl_G.resize(SpaceGroup::NSYMP);
  refl_Gsqr.resize(SpaceGroup::NSYMP);
  for (int isym = 0; isym < refl_G.size(); isym++)
  {
    refl_G[isym] = 0.;
    refl_Gsqr[isym] = 0.;
  }

  //Use half-triple summation rather than full quadruple summation:
  //assume NCS chosen closest to pure translation, ignore other NCS+symm combinations
  //pair off-diagonal contributions from jncs>incs with jncs<incs
  floatType symfac(1./(max_ncsT*SpaceGroup::NSYMP)); // (diagonals in loop)
  floatType diagfac(1./(max_ncsT-1.));  // correction for including diagonal term as part of off-diagonal

  for (int incs = 0; incs < max_ncsT; incs++)
  for (int jncs = incs; jncs < max_ncsT; jncs++)
  for (int isym = 0; isym < SpaceGroup::NSYMP; isym++)
  {
    if (incs == jncs)
    {
      refl_G_Vterm += symfac;
    }
    else
    { // tNCS terms, weighted by interference and G-function terms
      int i = ((incs*max_ncsT)+jncs)*SpaceGroup::NSYMP+isym;
      int ir = i*NREFL + r;
      floatType GcosTerm = 2.*G[ir]*cosTerm[ir];
      // These are the stored parameters
      refl_G_Vterm += GcosTerm*symfac;
      floatType thisGsqrTerm = 2*fn::pow2(G[ir])*(diagfac + cosTerm[ir])*symfac;
      refl_Gsqr_Vterm += thisGsqrTerm;
      refl_Gsqr[isym] += thisGsqrTerm; //weight for orientation error of symm copy in FRF
      //refl_G is only used when halfR is true, i.e. when the same orientation
      //is used for two copies and refl_G compensates for an orientation error
      //of known size. For max_ncsT>2, refl_G will currently always be 1 but
      //this code would have to be modified if orientational differences were
      //characterised
      refl_G[isym] = G[ir];
      // end stored parameters
    }
  }
}

//full calculation
void Gfunction::calcArrays(dvect3 VECTOR,dmat33 ncsRmat,floatType MOLRADIUS)
{
  dmat33 ncsRmat_t = ncsRmat.transpose();

  dmat33 identity(1,0,0,0,1,0,0,0,1);
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();

  // Pre-compute some results that don't depend on reflection number
  floatType radSqr = fn::pow2(MOLRADIUS);
  for (int incs = 0; incs < max_ncsT; incs++)
  for (int jncs = incs; jncs < max_ncsT; jncs++)
  {
    dmat33 ncsR_incs,ncsR_jncs;
    dvect3 ncsT_incs,ncsT_jncs;
    if (incs != jncs) // values of GfunTensorArray and ncsDeltaT won't be used otherwise
    {
      ncsR_incs = (incs ? ncsRmat_t : identity); //always identity for max_ncsT>2
      ncsR_jncs = (jncs ? ncsRmat_t : identity); //always identity for max_ncsT>2
      ncsT_incs = floatType(incs)*VECTOR;
      ncsT_jncs = floatType(jncs)*VECTOR;
      for (int isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        /*
          Metric matrix (represented as tensor) allows quick computation of |s_klmm|^2 and therefore rs^2 for G-function
          If F=fractionalization_matrix, R=Rotsym and (T) indicates transpose, then
          s_klmm = (ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h
          |s_klmm|^2 = s_klmm(T).s_klmm = h(T)*R*F*(ncsR_incs-ncsR_jncs)*(ncsR_incs(T)-ncsR_jncs(T))*F(T)*R(T)*h
          The metrix matrix is the part between h(T) and h.  In the GfunTensorArray, factors of 2 are included so
          the unique terms need to be computed only once.
        */
        int i = ((incs*max_ncsT)+jncs)*SpaceGroup::NSYMP+isym;
        ncsDeltaT[i] = SpaceGroup::Rotsym(isym).transpose()*(ncsT_incs-ncsT_jncs); // symmetry translation cancels
        dmat33 metric_matrix = SpaceGroup::Rotsym(isym).transpose()*cctbxUC.fractionalization_matrix()*(ncsR_incs-ncsR_jncs);
        metric_matrix = radSqr*metric_matrix*metric_matrix.transpose();
        GfunTensorArray[i] = af::double6(metric_matrix(0,0),metric_matrix(1,1),metric_matrix(2,2),
                                         2*metric_matrix(0,1),2*metric_matrix(0,2),2*metric_matrix(1,2));
      }
    }
  }
}

void Gfunction::calcRefineTerms(bool& do_gradient,bool& do_hessian,floatType DRMS,miller::index<int>& miller)
{
  refl_epsfac = dE_by_dD = 0;
  dE_by_dT = d2E_by_dT2 = dvect3(0,0,0);

  //Use half-triple summation rather than full quadruple summation
  floatType symfac(1./(max_ncsT*SpaceGroup::NSYMP)); // (diagonals in loop)

  for (int incs = 0; incs < max_ncsT; incs++)
  for (int jncs = incs; jncs < max_ncsT; jncs++)
  {
    for (int isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      //floatType G(0),GcosTerm(0),GsinTerm(0);
      floatType GcosTerm(0),GsinTerm(0);
      if (incs == jncs)
      {
        refl_epsfac += symfac; // Normal symmetry contribution to epsilon
      }
      else
      { // NCS-related contribution to epsilon, weighted by interference, G-function and Drms terms
        int hh(miller[0]), kk(miller[1]), ll(miller[2]);
        int i = ((incs*max_ncsT)+jncs)*SpaceGroup::NSYMP+isym;
        af::double6 GfunTensor = GfunTensorArray[i];
        floatType rsSqr = GfunTensor[0]*hh*hh + GfunTensor[1]*kk*kk + GfunTensor[2]*ll*ll +
                          GfunTensor[3]*hh*kk + GfunTensor[4]*hh*ll + GfunTensor[5]*kk*ll;
        floatType G = GfuncOfRSsqr(rsSqr);
        floatType h_dot_T = (miller*ncsDeltaT[i]);
        cmplxType cos_sin;
#if defined CCTBX_MATH_COS_SIN_TABLE_H
        cos_sin = fast_cos_sin.get(h_dot_T); //multiplies by 2*pi internally
#else
        h_dot_T *= scitbx::constants::two_pi;
        cos_sin = cmplxType(std::cos(h_dot_T),std::sin(h_dot_T));
#endif
        floatType cosTerm = std::real(cos_sin);
        if (do_gradient || do_hessian) //cosine and sine needed, use table lookup
        {
          GcosTerm = 2.*G*cosTerm;
          GsinTerm = 2.*G*std::imag(cos_sin);
        }
        else
        {
          GcosTerm = 2.*G*cosTerm;
        }
        refl_epsfac += GcosTerm*symfac*DRMS; // stored parameter
      }
      if (do_gradient || do_hessian)
      {
        if (incs != jncs)
        {
          dE_by_dD += GcosTerm*symfac;
          dvect3 twoPiHdotR = -scitbx::constants::two_pi*(SpaceGroup::Rotsym(isym)*miller);
          dE_by_dT -= twoPiHdotR*((jncs-incs)*symfac*DRMS*GsinTerm);
          if (do_hessian)
          {
            for (int tra = 0; tra < 3; tra++)
            {
              floatType d2hdotT_by_dT2 = -fn::pow2(twoPiHdotR[tra]);
              d2E_by_dT2[tra] += d2hdotT_by_dT2*fn::pow2(jncs-incs)*symfac*DRMS*GcosTerm;
            }
          }
        }
      }
    }
  }
}

} //end namespace phaser
