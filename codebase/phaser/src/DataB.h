//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DataBClass__
#define __DataBClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_refl.h>
#include <phaser/src/SigmaN.h>
#include <phaser/include/data_outl.h>
#include <phaser/include/data_tncs.h>
#include <phaser/include/data_moments.h>
#include <phaser/include/data_composition.h>
#include <phaser/src/DataA.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/Gfunction.h>
#include <cctbx/math/cos_sin_table.h>

namespace phaser {
class DataB : public DataA
{
  public:
    //Constructor and Destructor
    DataB(data_refl&,
          data_resharp&,
          data_norm&,
          data_outl&,
          data_tncs&,
          data_bins&,
          data_composition&,
          double=0,double=DEF_LORES);
    DataB() { LLwilson = HIRES = LORES = 0; TOTAL_SCAT_B = 1; }
    virtual ~DataB() { }

  protected:
    //Single values
    floatType HIRES,LORES;
    data_moments MOMENTS;
    floatType LLwilson;
    double TOTAL_SCAT_B;

    //arrays
    float1D sqrt_epsn; //for speed
    bool1D  is_centric; //for speed

    //functions, not stored
    floatType  ssqr(const int&);
    floatType  reso(const int&);
    floatType  best(const unsigned&); // Popov factor by reflection index
    floatType  best(const double&);   // Popov factor by resolution
    bool       cent(const int&);
    floatType  epsn(const int&);

    //PtNcs related
    Gfunction  gfun;
    float1D    G_DRMS;
    float1D    G_Vterm;

    //Data dependent on reflection and symmetry [r][isym]
    floatType  traMiller(const int &,const int &);
    miller::index<int>     rotMiller(const int &,const int &);
    std::vector<miller::index<int> >   rotMiller(const int &);
    float1D    traMiller(const int &);
    bool       duplicate(const int&,const std::vector<miller::index<int> > &); //reuse rotMiller

    void       checkData() const;

  public:
    void initCos_Sin();
    cmplxType dphi(unsigned&, unsigned&, dvect3&);
    cmplxType dphi(unsigned&, unsigned&, miller::index<int>&, dvect3&);
#if defined CCTBX_MATH_COS_SIN_TABLE_H
    cctbx::math::cos_sin_lin_interp_table<floatType> fast_cos_sin;
#endif

  public:
    void selectReso(floatType,floatType);
    void changeSpaceGroup(std::string,floatType,floatType);
    void calcOutliers();
    void calcSelected();
    void logInfoBits(outStream,bool,Output&);
    void calcMoments(Output&);
    void set_twin_bin_reso();

    //-- Log Functions
    void logSigmaN(outStream,Output&,bool=false);
    void logScale(outStream,Output&);
    void logDataStats(outStream,Output&);
    void logOutliers(outStream,Output&);
    void logDataStatsExtra(outStream,unsigned,Output&);
    void logMoments(std::string,outStream,bool,Output&);

    unsigned  NumC();
    unsigned  NumAC();
    unsigned  NumInBinC(const unsigned&);
    unsigned  NumInBinAC(const unsigned&);
    floatType maxSsqr();
    SigmaN    getBinSigmaN();
    data_moments getMoments() const { return MOMENTS; }

    floatType binAnisoFactor(const unsigned&,floatType&,dmat6&,floatType&,floatType&,floatType); // anisotropic by reflection
    floatType binIsoFactor(const double&,floatType&,double&,floatType&,floatType&,floatType); // isotropic by resolution

    void      logWilson(outStream,floatType,Output&);
    void      setLLwilson(floatType LL) { LLwilson = LL; }
    floatType WilsonLL();
    void      initialize_sigman();
    void      calculate_sigman_parameters();
    void      initGfunction(bool);
    std::pair<int,int> number_selected();

};

} //phaser

#endif
