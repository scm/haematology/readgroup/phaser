//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineANOClass__
#define __RefineANOClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolANO.h>
#include <phaser/include/data_composition.h>
#include <phaser/src/DataB.h>
#include <phaser/src/RefineBase2.h>

namespace phaser {

class RefineANO : public RefineBase2, public DataB
{
  public:
    RefineANO() { }
    RefineANO(data_refl&,
              data_resharp&,
              data_norm&,
              data_outl&,
              data_tncs&,
              data_bins&,
              data_composition&,
              double=0,double=DEF_LORES);
    ~RefineANO() { TOTAL_SCAT = 0; }

    double TOTAL_SCAT;
    double absolute_scale_F() { return SIGMAN.WILSON_K*std::sqrt(TOTAL_SCAT); }

   //concrete member functions
    floatType    targetFn();  //this is where main body goes
    floatType    gradientFn(TNT::Vector<floatType>&);
    floatType    hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    void         applyShift(TNT::Vector<floatType>&);
    void         rejectOutliers(outStream,Output&);
    void         logCurrent(outStream,Output&);
    void         logInitial(outStream,Output&);
    void         logFinal(outStream,Output&);
    void         finalize(outStream,Output&);
    bool1D       getRefineMask(protocolPtr);
    void         cleanUp(outStream,Output&);
    std::string  whatAmI(int&);
    std::vector<reparams>   getRepar();
    std::vector<bounds>     getLowerBounds();
    std::vector<bounds>     getUpperBounds();
    TNT::Vector<floatType>  getRefinePars();
    TNT::Vector<floatType>  getLargeShifts();

  private:
    dmat6 sigmaSphericityBeta;
    bool1D getRefineAnisoMask();
};

} //phaser

#endif
