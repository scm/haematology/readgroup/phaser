//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/SiteList.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

void SiteList::archiveValues()
{ for (unsigned i = 0; i < Size(); i++) archiveValue(i); }

void SiteList::renumber()
{ for (unsigned i = 0; i < Size(); i++) setNum(i,i+1); }

floatType SiteList::getTop()
{
 // return getValue(0); //assumes sorted
  floatType top = getValue(0);
  for (unsigned i = 0; i < Size(); i++)
     top = std::max(top,getValue(i));
  return top;
}

floatType SiteList::getTail()
{
 // return getValue(0); //assumes sorted
  floatType tail = getValue(0);
  for (unsigned i = 0; i < Size(); i++)
     tail = std::min(tail,getValue(i));
  return tail;
}

floatType SiteList::getTopTFZ()
{
 // return getValue(0); //assumes sorted
  floatType top = getValue(0);
  int topi(0);
  for (unsigned i = 0; i < Size(); i++)
  if (getValue(i) > top)
  {
    top = std::max(top,getValue(i));
    topi = i;
  }
  return getSignal(topi);
}

//-------------------------------------------------------------------------------------------------
void SiteList::selectClusteredTop()
{
  unsigned numSites(1);
  if (PEAKS.by_sigma())
  for (int i = 0; i < Size(); i++)
    (isTopSite(i) && isSelected(i) && getSignal(i) > PEAKS.get_cutoff()) ?
        setSelected(i,true) : setSelected(i,false);
  else if (PEAKS.by_number())
  {
    Sort();
    for (int i = 0; i < Size(); i++)
      (isTopSite(i) && isSelected(i) && numSites <= PEAKS.get_cutoff()) ?
       numSites++, setSelected(i,true) : setSelected(i,false);
  }
  else if (PEAKS.by_percent())
  {
    for (int i = 0; i < Size(); i++)
    {
      //apply new higher non-conservative cutoff, after clustering
      (isTopSite(i) && isSelected(i) && (getValue(i) > ((PEAKS.deep_cutoff())*(TOP-MEAN)+MEAN) || ZSCORE.over_cutoff(getSignal(i))) ) ?
          setSelected(i,true) : setSelected(i,false);
      ((getValue(i) > (PEAKS.get_cutoff()*(TOP-MEAN)+MEAN) ) || ZSCORE.over_cutoff(getSignal(i)) ) ?
          setDeep(i,false) : setDeep(i,true);
    }
  }
  else if (PEAKS.by_all())
  for (int i = 0; i < Size(); i++)
    (isTopSite(i) && isSelected(i)) ?
       setSelected(i,true) : setSelected(i,false);
}

void  SiteList::selectRawTop()
{
  if (PEAKS.by_sigma())
  for (unsigned i = 0; i < Size(); i++)
    if (getSignal(i) > PEAKS.get_cutoff())
    { setTopSite(i,true); setSelected(i,true); }
    else { setTopSite(i,false); setSelected(i,false); }
  else if (PEAKS.by_number())
  {
    Sort();
    for (unsigned i = 0; i < Size(); i++)
      if (i < PEAKS.get_cutoff())
      { setTopSite(i,true); setSelected(i,true); }
      else { setTopSite(i,false); setSelected(i,false); }
  }
  else if (PEAKS.by_percent())
  {
    for (unsigned i = 0; i < Size(); i++)
    {
      (getValue(i) > (PEAKS.deep_cutoff()*(TOP-MEAN)+MEAN) || ZSCORE.over_cutoff(getSignal(i))) ?
          setTopSite(i,true) : setTopSite(i,false);
      (getValue(i) > (PEAKS.deep_cutoff()*(TOP-MEAN)+MEAN) || ZSCORE.over_cutoff(getSignal(i))) ?
          setSelected(i,true) : setSelected(i,false);
      (getValue(i) > (PEAKS.get_cutoff()*(TOP-MEAN)+MEAN) || ZSCORE.over_cutoff(getSignal(i)) ) ?
          setDeep(i,false) : setDeep(i,true);
    }
  }
  else if (PEAKS.by_all())
  for (unsigned i = 0; i < Size(); i++)
  { setTopSite(i,true); setSelected(i,true); }
}

void  SiteList::selectRawTopByNumber(floatType NUM)
{
  for (unsigned i = 0; i < Size(); i++)
    if (i < NUM)
    { setTopSite(i,true); setSelected(i,true); }
    else { setTopSite(i,false); setSelected(i,false); }
}

void SiteList::logRawTopInfo(outStream where,Output& output)
{
  outStream fixed_where = LOGFILE;
  if (PEAKS.by_sigma())
  {
  output.logTab(1,fixed_where,"Select peaks over "+dtos(PEAKS.get_cutoff())+" sigma");
  unsigned num_selected = numSelected();
  if (!num_selected) output.logTab(1,where,"No peaks within selection criteria");
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 site over "+dtos(PEAKS.get_cutoff())+" sigma");
    else output.logTab(1,where,"There were "+itos(num_selected)+" sites over "+dtos(PEAKS.get_cutoff())+" sigma");
    (num_selected == 1) ?  output.logTab(1,where,"1 peak selected") :
                           output.logTab(1,where,itos(num_selected) + " peaks selected");
    output.logTab(1,where,"The sites over " + dtos(PEAKS.get_cutoff()) + " sigma are:");
  }
  }
  if (PEAKS.by_number())
  {
  Sort();
  output.logTab(1,fixed_where,"Select top "+dtos(PEAKS.get_cutoff())+" peaks");
  unsigned num_selected = numSelected();
  if (!num_selected) output.logTab(1,where,"No peaks within selection criteria");
  else
  {
    if (PEAKS.get_cutoff() > num_selected && num_selected > 1) output.logTabPrintf(1,where,"There were only %d sites\n",num_selected);
    if (PEAKS.get_cutoff() > num_selected && num_selected == 1) output.logTab(1,where,"There was only 1 site");
    (num_selected == 1) ?  output.logTab(1,where,"1 peak selected") :
                           output.logTab(1,where,itos(num_selected) + " peaks selected");
    if (num_selected == 1) output.logTab(1,where,"The top site is:");
    else output.logTab(1,where,"The top sites are:");
  }
  }
  if (PEAKS.by_percent())
  {
  output.logTab(1,fixed_where,"Select peaks over "+dtos(PEAKS.get_cutoff()*100)+"% of top (i.e. "+ dtos(PEAKS.get_cutoff()) + "*(top-mean)+mean)");
  if (PEAKS.deep_cutoff() < PEAKS.get_cutoff())
    output.logTab(2,where,"Also store peaks over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
  unsigned num_selected = numSelected();
  if (!num_selected) output.logTab(1,where,"No peaks within selection criteria");
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 site over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
    else output.logTab(1,where,"There were "+itos(num_selected)+" sites over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
    (num_selected == 1) ?  output.logTab(1,where,"1 peak selected") :
                           output.logTab(1,where,itos(num_selected) + " peaks selected");
    output.logTab(1,where,"The sites over " + dtos(PEAKS.deep_cutoff()*100) + "% are:");
  }
  }
  if (PEAKS.by_all())
  {
  unsigned num_selected = numSelected();
  output.logTab(1,fixed_where,"Select all peaks");
  if (!num_selected) output.logTab(1,where,"No peaks within selection criteria");
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 peak");
    else output.logTab(1,where,"There were "+itos(num_selected)+" peaks");
    (num_selected == 1) ?  output.logTab(1,where,"1 peak selected") :
                           output.logTab(1,where,itos(num_selected) + " peaks selected");
    output.logTab(1,where,"The sites are:");
  }
  }
}

void SiteList::logClusteredTopInfo(outStream where,Output& output)
{
  if (PEAKS.by_sigma())
  {
  output.logTab(1,where,"Select peaks over "+dtos(PEAKS.get_cutoff())+" sigma");
  unsigned num_selected(0);
  for (int i = 0; i < Size(); i++)
    if (isTopSite(i))
      num_selected++;
  if (!num_selected)
  {
    output.logTab(1,where,"No sites within selection criteria");
    output.logBlank(where);
  }
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 site over "+dtos(PEAKS.get_cutoff())+" sigma");
    else output.logTab(1,where,"There were "+itos(num_selected)+" sites over "+dtos(PEAKS.get_cutoff())+" sigma");
    output.logTab(1,where,"The sites over " + dtos(PEAKS.get_cutoff()) + " sigma are:");
  }
  }
  if (PEAKS.by_number())
  {
  Sort();
  output.logTab(1,where,"Select top "+dtos(PEAKS.get_cutoff())+" peaks");
  unsigned num_selected = numSelected();
  if (!num_selected)
  {
    output.logTab(1,where,"No sites within selection criteria");
    output.logBlank(where);
  }
  else
  {
    if (PEAKS.get_cutoff() > num_selected && num_selected > 1) output.logTabPrintf(1,where,"There were only %d sites\n",num_selected);
    if (PEAKS.get_cutoff() > num_selected && num_selected == 1) output.logTab(1,where,"There was only 1 site");
    if (num_selected == 1) output.logTab(1,where,"The top " + itos(num_selected) + " sites are:");
    else output.logTab(1,where,"The top sites are:");
  }
  }
  if (PEAKS.by_percent())
  {
  output.logTab(1,where,"Select peaks over "+dtos(PEAKS.get_cutoff()*100)+"% of top (i.e. "+ dtos(PEAKS.get_cutoff()) + "*(top-mean)+mean)");
  if (PEAKS.deep_cutoff() < PEAKS.get_cutoff())
    output.logTab(2,where,"Also store peaks over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
  unsigned num_selected = numSelected();
  if (!num_selected)
  {
    output.logTab(1,where,"No sites within selection criteria");
    output.logBlank(where);
  }
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 site over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
    else output.logTab(1,where,"There were "+itos(num_selected)+" sites over "+dtos(PEAKS.deep_cutoff()*100)+"% of top");
    output.logTab(1,where,"The sites over " + dtos(PEAKS.deep_cutoff()*100) + "% are:");
  }
  }
  if (PEAKS.by_all())
  {
  output.logTab(1,where,"Select all peaks");
  unsigned num_selected = numSelected();
  if (!num_selected)
  {
    output.logTab(1,where,"No sites within selection criteria");
    output.logBlank(where);
  }
  else
  {
    if (num_selected == 1) output.logTab(1,where,"There was 1 peak");
    else output.logTab(1,where,"There were "+itos(num_selected)+" peaks");
  }
  }
}

void SiteList::purgeUnselected()
{
  int j(0);
  for (int i = 0; i < Size(); i++)
    if (isSelected(i)) Copy(i,j++);
  Erase(j,Size());
}

void SiteList::select_and_log(outStream where,Output& output,bool NEW_CLUSTERING)
{
  output.logTab(1,where,"Top Peaks " + std::string(PEAKS.CLUSTER?"With":"Without") + " Clustering");
  outStream wherelegend(VERBOSE);
  if (RESCORE)
  {
    output.logTab(1,wherelegend,"#       Rank of the peak after rescoring search points");
    output.logTab(1,wherelegend,"LLG     Log-Likelihood Gain");
    output.logTab(1,wherelegend,"Z-Score Number of standard deviations of LLG above the mean");
    output.logTab(1,wherelegend,"FSS     Fast Search Score");
    output.logBlank(wherelegend);
  }
  else
  {
    output.logTab(1,wherelegend,"#       Rank of the peak");
    if (FAST) output.logTab(1,wherelegend,"FSS     Fast Search Score");
    else      output.logTab(1,wherelegend,"LLG     Log-Likelihood Gain");
    if (FAST) output.logTab(1,wherelegend,"Z-Score Number of standard deviations of FSS above the mean");
    else      output.logTab(1,wherelegend,"Z-Score Number of standard deviations of LLG above the mean");
    output.logBlank(wherelegend);
  }
  if (!PEAKS.CLUSTER)
  //we are going to save the unclustered peaks
  {
    selectRawTop();
    purgeUnselected();
    logRawTopInfo(where,output);
    if (RESCORE)
    {
      logRawTopRescored(where,output);
      graphRawTopRescored(DEBUG,output);
    }
    else
    {
      logRawTop(where,output);
      graphRawTop(DEBUG,output);
    }
  }
  else
  {
    selectRawTop();
    purgeUnselected();
    output.logEllipsisStart(VERBOSE,"Clustering");
    if (NEW_CLUSTERING)
    flagClusteredTopNew(PEAKS.LEVEL);
    else
    flagClusteredTop(PEAKS.LEVEL);
    output.logEllipsisEnd(VERBOSE);
    selectClusteredTop();
    purgeUnselected();
    logClusteredTopInfo(where,output);
    if (RESCORE)
    {
      logClusteredTopRescored(where,output);
      graphClusteredTopRescored(VERBOSE,output);
    }
    else
    {
      logClusteredTop(where,output);
      graphClusteredTop(VERBOSE,output);
    }
  }
}

int SiteList::Over(double f_top)
{
  int count(0);
  for (unsigned i = 0; i < Size(); i++)
    if (getValue(i) > f_top) count++;
  return count;
}


} //end namespace phaser
