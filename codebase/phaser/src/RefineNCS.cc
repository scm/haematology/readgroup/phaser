//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/src/RefineNCS.h>
#include <phaser/src/TncsVar.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <phaser/src/Gfunction.h>

namespace phaser {

RefineNCS::RefineNCS(DataB init,dvect3 input_angle) :
   RefineBase2(), DataB(init), tncs_angle(input_angle)
{
  likelihood = 0;
  tncs_gfunction_radius = PTNCS.GFUNCTION_RADIUS;
  tncs_vector = PTNCS.TRA.VECTOR;
  PHASER_ASSERT(PTNCS.VAR.BINS.size() == bin.numbins());
  tncs_varbins = PTNCS.VAR.BINS;
  //reset to 1 so refinement always starts from same initial eps values
  tncs_epsfac.resize(NREFL,1);
  calcNcsEpsn();
  SigmaN.resize(NREFL); //delete init
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned r = 0; r < NREFL; r++)
    SigmaN[r] = binAnisoFactor(r,SIGMAN.BINS[rbin(r)],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
}

void RefineNCS::calcNcsEpsn()
{
  TNT::Vector<floatType> Gradient;
  TNT::Fortran_Matrix<floatType> Hessian;
  bool is_diagonal; //returned
  bool do_target(false),do_gradient(false),do_hessian(false);
  target_gradient_hessian(do_target,do_gradient,Gradient,do_hessian,Hessian,is_diagonal);
}

floatType RefineNCS::targetFn()
{
  TNT::Vector<floatType> Gradient;
  TNT::Fortran_Matrix<floatType> Hessian;
  bool is_diagonal; //returned
  bool do_target(true),do_gradient(false),do_hessian(false);
  return likelihood = target_gradient_hessian(do_target,do_gradient,Gradient,do_hessian,Hessian,is_diagonal);
}

floatType RefineNCS::gradientFn(TNT::Vector<floatType>& Gradient)
{
  TNT::Fortran_Matrix<floatType> Hessian;
  bool is_diagonal; //returned
  bool do_target(true),do_gradient(true),do_hessian(false);
  return target_gradient_hessian(do_target,do_gradient,Gradient,do_hessian,Hessian,is_diagonal);
}

floatType RefineNCS::hessianFn(TNT::Fortran_Matrix<floatType>& Hessian,bool& is_diagonal)
{
  TNT::Vector<floatType> Gradient;
  bool do_target(true),do_gradient(false),do_hessian(true);
  return target_gradient_hessian(do_target,do_gradient,Gradient,do_hessian,Hessian,is_diagonal);
}

floatType RefineNCS::target_gradient_hessian(bool do_target,bool do_gradient,TNT::Vector<floatType>& Gradient,
                        bool do_hessian,TNT::Fortran_Matrix<floatType>& Hessian, bool& is_diagonal)
{
  //initialization
  af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
  af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  static double EPS(std::numeric_limits<double>::min());
  if (do_gradient)
  {
    Gradient.newsize(npars_ref);
    for (int g = 0; g < Gradient.size(); g++) Gradient[g] = 0;
  }
  if (do_hessian)
  {
    Hessian.newsize(npars_ref,npars_ref);
    for (int i = 1; i <= npars_ref; i++)
      for (int j = 1; j <= npars_ref; j++)
        Hessian(i,j) = 0.;
  }

  //finite difference for R parameters
  if (do_gradient)
  {
  // Fraction of large shift to use to approximate the gradient
  // by finite differences was chosen by numerical tests, varying
  // by factors of two and choosing something from the middle of
  // the stable range.
    floatType fracLarge(1./2.e+7);
    floatType f(0),fplus(0),sz(0);
    TNT::Vector<floatType> x(npars_ref),old_x(npars_ref);
    TNT::Vector<floatType> largeShifts = getLargeShifts();
    TNT::Vector<floatType> unrepar_x(getRefinePars());
    TNT::Vector<floatType> unrepar_oldx(unrepar_x);
    f = targetFn();
    old_x = reparRefinePars(unrepar_x);
    x = old_x;
    int i(0);
    for (int m = 0; m < 3; m++)
    if (refinePar[m])
    {
      sz = fracLarge*largeShifts[i];
      x[i] = old_x[i] + sz;
      unrepar_x = reparRefineParsInv(x);
      applyShift(unrepar_x);
      fplus = targetFn();
      x[i] = old_x[i];
      Gradient[i] = (fplus - f)/sz;
      i++;
    }
    applyShift(unrepar_oldx);
  }

  if (do_hessian)
  {
    floatType fracLarge(0.025);
    floatType f(0),fplus(0),fminus(0),sz(0);
    TNT::Vector<floatType> x(npars_ref),old_x(npars_ref);
    TNT::Vector<floatType> largeShifts = getLargeShifts();
    TNT::Vector<floatType> unrepar_x(getRefinePars());
    TNT::Vector<floatType> unrepar_oldx(unrepar_x);
    f = targetFn();
    old_x = reparRefinePars(unrepar_x);
    x = old_x;
    int i(0);
    for (int m = 0; m < 3; m++)
    if (refinePar[m])
    {
      sz = fracLarge*largeShifts[i];
      x[i] = old_x[i] + sz;
      unrepar_x = reparRefineParsInv(x);
      applyShift(unrepar_x);
      fplus = targetFn();
      x[i] = old_x[i] - sz;
      unrepar_x = reparRefineParsInv(x);
      applyShift(unrepar_x);
      fminus = targetFn();
      x[i] = old_x[i];
      Hessian(i+1,i+1) = (fplus - 2*f + fminus)/(sz*sz);
      i++;
    }
    applyShift(unrepar_oldx);
  }

  //function, and analytic gradient and hessian for T and V parameters
  floatType minusLL(0);

  dmat33 identity(1,0,0,0,1,0,0,0,1);
  dmat33 ncsRmat(identity);
  for (int dir = 0; dir < 3; dir++) ncsRmat = xyzRotMatDeg(dir,tncs_angle[dir])*ncsRmat;
  dvect3 zeroT(0,0,0);

  // Pre-compute some results that don't depend on reflection number
  float1D   DRMS(tncs_varbins.size());
  for (int s = 0; s < bin.numbins(); s++) { DRMS[s] = std::sqrt(1.-tncs_varbins[s]); }

  SpaceGroup sg = this->getSpaceGroup();
  UnitCell uc = this->getUnitCell();
  Gfunction gfun(sg,uc,PTNCS.NMOL);
            gfun.calcArrays(tncs_vector,ncsRmat,PTNCS.GFUNCTION_RADIUS);

  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType dLL_by_dEps(0),d2LL_by_dEps2(0);

    unsigned s = rbin(r);

    //recalculate tncs_epsn from changed parameters tncs_angle,tncs_vector,tncs_varbins
    gfun.calcRefineTerms(do_gradient,do_hessian,DRMS[s],MILLER[r]);
    tncs_epsfac[r] = gfun.refl_epsfac;

    if (do_target || do_gradient || do_hessian)
    {
      //Compute effective gfun with half-triple summation rather than full quadruple summation:
      //   assume NCS chosen closest to pure translation, ignore other NCS+symm combinations
      //   pair off-diagonal contributions from jncs>incs with jncs<incs

      floatType epsnSigmaN = epsn(r)*tncs_epsfac[r]*SigmaN[r];
      bool do_grad = (do_gradient || do_hessian); // Need pIobs gradient for both
      floatType grad,hess;
      // For French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2, ignore errors
      floatType    thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                             Idat[r];
      floatType thisSIGI = is_FWamplitudes ? 0. : SIGIdat[r];
      floatType prob = (cent(r)) ?
          pIobsCen_grad_hess_by_dSN( thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hessian,hess) :
          pIobsAcen_grad_hess_by_dSN(thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hessian,hess);
      floatType dLL_by_dEpsnSigmaN,d2LL_by_dEpsnSigmaN2;
      if (prob < EPS)
      {
        prob = EPS;
        dLL_by_dEpsnSigmaN = d2LL_by_dEpsnSigmaN2 = 0.;
      }
      else
      {
        if (do_gradient || do_hessian)
        {
          dLL_by_dEpsnSigmaN = -grad/prob;
          if (do_hessian)
            d2LL_by_dEpsnSigmaN2 = -(hess/prob - fn::pow2(grad/prob));
        }
      }
      minusLL -= std::log(prob); // function, gradient and hessian are all for -LL
      if (do_gradient || do_hessian)
      {
        floatType dEpsnSigmaN_by_dEps = epsnSigmaN/tncs_epsfac[r];
        dLL_by_dEps = dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dEps;
        if (do_hessian)
          d2LL_by_dEps2 = d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dEps);
      }
    }

    if (do_gradient)
    {
      int i(0),m(0);
      for (int j = 0; j < 3; j++) if (refinePar[m++]) i++;
      for (int tra = 0; tra < 3; tra++)
        if (refinePar[m++])
          Gradient[i++] += dLL_by_dEps*gfun.dE_by_dT[tra];
      if (refinePar[6+s])
      {
        floatType dD_by_dV(-0.5/DRMS[s]);
        Gradient[i+s] += dLL_by_dEps*gfun.dE_by_dD*dD_by_dV;
      }
    }
    if (do_hessian)
    {
      int i(0),m(0);
      for (int j = 0; j < 3; j++) if (refinePar[m++]) i++;
      for (int tra = 0; tra < 3; tra++)
        if (refinePar[m++])
        { Hessian(i+1,i+1) += d2LL_by_dEps2*fn::pow2(gfun.dE_by_dT[tra]) + dLL_by_dEps*gfun.d2E_by_dT2[tra]; i++; }
      if (refinePar[6+s])
      {
        floatType dD_by_dV(-0.5/DRMS[s]);
        floatType d2D_by_dV2(-0.25/fn::pow3(DRMS[s]));
        Hessian(i+1+s,i+1+s) += d2LL_by_dEps2*fn::pow2(gfun.dE_by_dD*dD_by_dV) + dLL_by_dEps*gfun.dE_by_dD*d2D_by_dV2;
      }
      is_diagonal = true;
    }
  } // loop over reflections
  if (do_target || do_gradient || do_hessian)
  {
  const floatType varbinwt(1./(2*fn::pow2(0.05))); // sigma of 0.05 for varbin smoothness
  int offset_vb(0); // Get offset for number of refined variables before bins
  if (do_gradient || do_hessian)
  {
    int m(0);
    for (int j = 0; j < 6; j++) if (refinePar[m++]) offset_vb++;
  }
  for (int s = 1; s < bin.numbins()-1; s++) // restraints over inner bins
  {
    floatType vmean = (tncs_varbins[s-1] + tncs_varbins[s+1])/2.;
    floatType delta = tncs_varbins[s] - vmean;
    minusLL += varbinwt*fn::pow2(delta);
    if (do_gradient && refinePar[6+s])
    {
      Gradient[offset_vb+s-1] -= varbinwt*delta;
      Gradient[offset_vb+s]   += 2.*varbinwt*delta;
      Gradient[offset_vb+s+1] -= varbinwt*delta;
    }
    if (do_hessian && refinePar[6+s])
    {
      Hessian(offset_vb+s  ,offset_vb+s  ) += varbinwt/2.;
      Hessian(offset_vb+s+1,offset_vb+s+1) += 2.*varbinwt;
      Hessian(offset_vb+s+2,offset_vb+s+2) += varbinwt/2.;
    }
  }
  }
  return minusLL;
}

bool1D RefineNCS::getRefineMask(protocolPtr protocol)
{
  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
  for (int rot = 0; rot < 3; rot++)
    protocol->getFIX(mact_rot) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  for (int tra = 0; tra < 3; tra++)
    protocol->getFIX(mact_tra) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  for (int s = 0; s < bin.numbins(); s++)
    protocol->getFIX(mact_var) ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

TNT::Vector<floatType> RefineNCS::getLargeShifts()
{
  TNT::Vector<floatType> largeShifts(npars_ref);
  floatType dminBy64(HiRes()/64.);
  int m(0),i(0);
  for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++]) largeShifts[i++] = dminBy64/scitbx::deg_as_rad(tncs_gfunction_radius);
  if (refinePar[m++]) largeShifts[i++] = dminBy64/UnitCell::A();
  if (refinePar[m++]) largeShifts[i++] = dminBy64/UnitCell::B();
  if (refinePar[m++]) largeShifts[i++] = dminBy64/UnitCell::C();
  for (int s = 0; s < bin.numbins(); s++)
    //if (refinePar[m++]) largeShifts[i++] = 0.1; // if repar
    if (refinePar[m++]) largeShifts[i++] = 0.1*tncs_varbins[s]; // if no repar
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return largeShifts;
}

void RefineNCS::applyShift(TNT::Vector<floatType>& newx)
{
  int m(0),i(0);
  for (int rot = 0; rot < 3; rot++)
    if (refinePar[m++]) tncs_angle[rot] = newx[i++];
  for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++]) tncs_vector[tra] = newx[i++];
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) tncs_varbins[s] = newx[i++];
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  calcNcsEpsn();
}

void RefineNCS::logInitial(outStream where,Output& output)
{  }

void RefineNCS::logFinal(outStream where,Output& output)
{  }

void RefineNCS::logCurrent(outStream where,Output& output)
{ logNcsEpsn(where,output); }

std::string RefineNCS::whatAmI(int& ipar)
{
  int i(0),m(0);
  if (refinePar[m++]) if (i++ == ipar) return "Rotation X";
  if (refinePar[m++]) if (i++ == ipar) return "Rotation Y";
  if (refinePar[m++]) if (i++ == ipar) return "Rotation Z";
  if (refinePar[m++]) if (i++ == ipar) return "Translation X";
  if (refinePar[m++]) if (i++ == ipar) return "Translation Y";
  if (refinePar[m++]) if (i++ == ipar) return "Translation Z";
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) if (i++ == ipar) return "Vrms in bin #" + itos(s+1);
  PHASER_ASSERT(i == npars_ref);
  return "Undefined Parameter";
}

TNT::Vector<floatType> RefineNCS::getRefinePars()
{
  int m(0),i(0);
  TNT::Vector<floatType> pars(npars_ref);
  for (int rot = 0; rot < 3; rot ++)
    if (refinePar[m++]) pars[i++] = tncs_angle[rot];
  for (int tra = 0; tra < 3; tra ++)
    if (refinePar[m++]) pars[i++] = tncs_vector[tra];
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) pars[i++] = tncs_varbins[s];
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return pars;
}

std::vector<reparams> RefineNCS::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  for (int rot = 0; rot < 3; rot++)
    if (refinePar[m++]) repar[i++].off();
  for (int tra = 0; tra < 3; tra++)
    if (refinePar[m++]) repar[i++].off();
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) repar[i++].off(); //on(0.1); //Vrms
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return repar;
}

std::vector<bounds> RefineNCS::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  int i(0),m(0);
  for (int rot = 0; rot < 3; rot++)
    if (refinePar[m++]) Lower[i++].on(-20);
  for (int tra = 0; tra < 3; tra++)
    if (refinePar[m++]) Lower[i++].off();
  TncsVar tncsvar(PTNCS);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Lower[i++].on(tncsvar.lowerVAR(s));
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

std::vector<bounds > RefineNCS::getUpperBounds()
{
  std::vector<bounds > Upper(npars_ref);
  int i(0),m(0);
  for (int rot = 0; rot < 3; rot++)
    if (refinePar[m++]) Upper[i++].on(20);
  for (int tra = 0; tra < 3; tra++)
    if (refinePar[m++]) Upper[i++].off();
  TncsVar tncsvar(PTNCS);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Upper[i++].on(tncsvar.upperVAR(s));
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

void RefineNCS::logNcsEpsn(outStream where,Output& output)
{
  getNcsEpsfac(); // to calc epsfac for all reflections, after refinement
  output.logTab(1,where,"Ncs Translation Vector                        = " + dvtos(tncs_vector,7,4));
  output.logTab(1,where,"Rotational Deviation of NCS related structure = " + dvtos(tncs_angle,7,4));
  output.logTab(2,where,"for effective molecular radius = " + dtos(tncs_gfunction_radius,5,3));
  bool printD(false);
  floatType tol(1.0e-04);
  for (int s = 0; s < bin.numbins(); s++)
    if (NumInBinAC(s))
      if (tncs_varbins[s] < tncs_varbins[0]-tol || tncs_varbins[s] > tncs_varbins[0]+tol) printD = true;
  if (printD)
  {
    float1D DRMS(tncs_varbins.size());
    for (int s = 0; s < bin.numbins(); s++) { DRMS[s] = std::sqrt(1.-tncs_varbins[s]); }
    output.logTab(1,where,"D corresponding to RMS deviation of NCS related structure:");
    int smax = bin.numbins()-1;
    output.logTab(2,where,"Range (Resolution "+dtos(bin.LoRes(0),5,3)+" - "+dtos(bin.HiRes(smax),5,3)+"):             "
             +dtos(DRMS[0],6,4)+" - "+dtos(DRMS[smax],6,4));
    output.logTabPrintf(2,DEBUG,"%-4s %12s   %-5s\n","Bin","resolution","V");
    for (int s = 0; s < bin.numbins(); s++)
    if (NumInBinAC(s))
      output.logTabPrintf(2,DEBUG,"#%-3d %6.2f-%5.2f   %5.3f\n",s+1,bin.LoRes(s),bin.HiRes(s),tncs_varbins[s]);
  }
  else
    output.logTab(1,where,"Proportional variance for RMS between NCS related structures (all resolution bins) = " + dtos(tncs_varbins[0]));
  output.logBlank(where);
  floatType max_tncs_epsfac(-10000),min_tncs_epsfac(10000),mean_tncs_epsfac(0.);
  for (int r = 0; r < NREFL; r++)
  {
    max_tncs_epsfac = std::max(tncs_epsfac[r],max_tncs_epsfac);
    min_tncs_epsfac = std::min(tncs_epsfac[r],min_tncs_epsfac);
    mean_tncs_epsfac += tncs_epsfac[r];
  }
  output.logTab(1,TESTING,"                    Max.   Min.   Mean");
  output.logTab(1,TESTING,"ncs-epsn factor : " + dtos(max_tncs_epsfac,6,4) + " " + dtos(min_tncs_epsfac,6,4) + " " + dtos(mean_tncs_epsfac/NREFL,6,4));
  output.logBlank(TESTING);
  int nrefl(6);
  for (int t = 1; t < 3; t++)
  {
    if (t == 1) output.logTab(1,TESTING,"First " + itos(nrefl) + " highest ncs-epsn factor reflections:");
    if (t == 2) output.logTab(1,TESTING,"First " + itos(nrefl) + " lowest ncs-epsn factor reflections:");
    std::vector<std::pair<floatType,int> > sort_ncs_epsfac(NREFL);
    for (int r = 0; r < NREFL; r++) sort_ncs_epsfac[r] = std::pair<floatType,int>(tncs_epsfac[r],r);
    std::sort(sort_ncs_epsfac.begin(),sort_ncs_epsfac.end());
    if (t == 1) std::reverse(sort_ncs_epsfac.begin(),sort_ncs_epsfac.end());
    if (INPUT_INTENSITIES)
      output.logTabPrintf(1,TESTING,"%-3s %-3s %-3s %-8s %-4s %10s %12s %12s\n",
                                    "H","K","L","(refl#)","epsn","epsn\'","I","SIGI");
    else
      output.logTabPrintf(1,TESTING,"%-3s %-3s %-3s %-8s %-4s %10s %12s %12s\n",
                                    "H","K","L","(refl#)","epsn","epsn\'","F","SIGF");
    for (int countr = 0; countr < nrefl; countr++)
    {
      int r = sort_ncs_epsfac[countr].second;
      int H(MILLER[r][0]);
      int K(MILLER[r][1]);
      int L(MILLER[r][2]);
      if (INPUT_INTENSITIES)
        output.logTabPrintf(1,TESTING,"% 3d % 3d % 3d (%6d) %4.0f %10.4f %12.3f %12.3f\n",
                                      H,K,L,r+1,epsn(r),tncs_epsfac[r],I[r],SIGI[r]);
      else
        output.logTabPrintf(1,TESTING,"% 3d % 3d % 3d (%6d) %4.0f %10.4f %12.3f %12.3f\n",
                                      H,K,L,r+1,epsn(r),tncs_epsfac[r],F[r],SIGF[r]);
    }
    output.logBlank(TESTING);
  }
}

float1D RefineNCS::getNcsEpsfac()
{
  dmat33 identity(1,0,0,0,1,0,0,0,1);
  dmat33 ncsRmat(identity);
  for (int dir = 0; dir < 3; dir++) ncsRmat = xyzRotMatDeg(dir,tncs_angle[dir])*ncsRmat;
  float1D DRMS(tncs_varbins.size());
  for (int s = 0; s < bin.numbins(); s++) { DRMS[s] = std::sqrt(1.-tncs_varbins[s]); }
  Gfunction gfun(*this,*this,PTNCS.NMOL);
            gfun.calcArrays(tncs_vector,ncsRmat,PTNCS.GFUNCTION_RADIUS);
  bool do_gradient(false),do_hessian(false);
  for (unsigned r = 0; r < NREFL; r++)
  {
    unsigned s = rbin(r);
    gfun.calcRefineTerms(do_gradient,do_hessian,DRMS[s],MILLER[r]);
    tncs_epsfac[r] = gfun.refl_epsfac;
  }
  return tncs_epsfac;
}

void RefineNCS::cleanUp(outStream where,Output& output)
{
  for (int i = 0; i < 3; i++)
    if (between(tncs_vector[i],0,1.0e-06)) tncs_vector[i] = 0;
}


}//phaser
