//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/SiteListXyz.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>

namespace phaser {

// --------
// concrete
// --------

unsigned SiteListXyz::numSelected()
{
  unsigned num_selected(0);
  for (int i = 0; i < siteList.size(); i++) if (siteList[i].selected) num_selected++;
  return num_selected;
}

unsigned  SiteListXyz::Size()
{ return siteList.size(); }

void  SiteListXyz::Erase(int i,int j)
{
  siteList.erase(siteList.begin() + i,siteList.begin() + j);
}

void  SiteListXyz::Copy(int i,int j)
{
  PHASER_ASSERT(j <= i); // copy up list only
  siteList[j] = siteList[i];
}

bool      SiteListXyz::isSelected(unsigned i)
{ return siteList[i].selected; }

bool      SiteListXyz::isTopSite(unsigned i)
{ return siteList[i].topsite; }

floatType SiteListXyz::getValue(unsigned i)
{ return siteList[i].value; }

std::vector<dvect3> SiteListXyz::getFracsXYZ(size_t limit)
{
  limit = limit ? std::min(limit+1,siteList.size()) : siteList.size();
  std::vector<dvect3> xyz(limit);
  for (int i = 0; i < limit; i++)
    xyz[i] = getFracXYZ(i);
  return xyz;
}

float1D SiteListXyz::getValues(size_t limit)
{
  limit = limit ? std::min(limit+1,siteList.size()) : siteList.size();
  float1D values(limit);
  for (int i = 0; i < limit; i++)
    values[i] =  siteList[i].value;
  return values;
}

int1D SiteListXyz::getClusters(size_t limit)
{
  limit = limit ? std::min(limit+1,siteList.size()) : siteList.size();
  int1D values(limit,0);
  if (!PEAKS.CLUSTER) return values;
  for (int i = 0; i < limit; i++)
    values[i] =  siteList[i].clusterNr+1;  //must be non-zero
  return values;
}

float1D SiteListXyz::getSignals(size_t limit)
{
  limit = limit ? std::min(limit+1,siteList.size()) : siteList.size();
  float1D zscores(limit);
  for (int i = 0; i < limit; i++)
    zscores[i] =  siteList[i].zscore;
  return zscores;
}

dvect3 SiteListXyz::getFracXYZ(unsigned i)
{ return siteList[i].getFracXYZ(*this); }

dvect3 SiteListXyz::getOrthXYZ(unsigned i)
{ return siteList[i].getOrthXYZ(); }

unsigned  SiteListXyz::getNum(unsigned i)
{ return siteList[i].num; }

floatType SiteListXyz::getSignal(unsigned i)
{ return siteList[i].zscore; }

void      SiteListXyz::setNum(unsigned i,unsigned n)
{ siteList[i].num = n; }

void      SiteListXyz::setValue(unsigned i,floatType v)
{ siteList[i].value = v; }

void      SiteListXyz::setTopSite(unsigned i,bool b)
{ siteList[i].topsite = b; }

void      SiteListXyz::setSelected(unsigned i,bool b)
{ siteList[i].selected = b; }

void      SiteListXyz::archiveValue(unsigned i)
{ siteList[i].archiveValue(); }

void SiteListXyz::push_back(xyzsite& s)
{ siteList.push_back(s); }

void SiteListXyz::Resize(unsigned maxsize)
{ siteList.resize(maxsize); }

void SiteListXyz::Reserve(unsigned maxsize)
{ siteList.reserve(maxsize); }

void SiteListXyz::Sort()
{ std::sort(siteList.begin(),siteList.end()); }

void SiteListXyz::SortTop(double limit)
{
  double top = -std::numeric_limits<double>::max();
  if (limit == top) { Sort(); return; } //no need for partial sort
  int count(0);
  for (unsigned i = 0; i < siteList.size(); i++)
  if (getValue(i) > limit)
  { //move it to the top of the list
    if (i != count)
    {
      xyzsite tmp = siteList[count];
      siteList[count] = siteList[i];
      siteList[i] = tmp;
    }
    count++;
  }
  std::sort(siteList.begin(),siteList.begin()+count);
}

void SiteListXyz::truncate(unsigned maxSiteList)
{ siteList.erase(siteList.begin()+maxSiteList+1,siteList.end()); }

//-----------------------

void SiteListXyz::flagClusteredTop(int LEVEL)
{
  Sort();
  //must sort before clustering
  dvect3 fracPrevCell(0,0,0),orthPrev(0,0,0),fracPrev(0,0,0),orthRef(0,0,0),cellTrans(0,0,0);
  unsigned actual_num(0);

  //now square it so don't have to keep taking sqrt(distance) for comparison
  floatType DIST2 = fn::pow2(CLUSTER_DIST);

  //assume all sites are NOT unique to start with
  for (unsigned tra = 0; tra < siteList.size(); tra++) //all tra
    siteList[tra].topsite = false;

  for (unsigned tra = 0; tra < siteList.size(); tra++) //all tra
  {
    if (siteList[tra].selected)
    {
      orthRef = siteList[tra].getOrthXYZ();

      //This is a really cheap way of checking for centrosymmetry
      //Flag check_centrosymmetry is set when the search is for an atom
      //If the LLG is the same for neighbouring peaks, then they are taken as centrosymmetric
      //If test is wrong there is no harm, just accepting more peaks in the clustering
      if (check_centrosymmetry && tra >=1)
      {
        int tra_prev = tra-1; //only neighbours
        double tol=1.0e-10*siteList[tra].value; //very tight agreement
        if (siteList[tra].value < siteList[tra_prev].value+tol && //between
            siteList[tra].value > siteList[tra_prev].value-tol)
        {
          actual_num++;
          siteList[tra].topsite = siteList[tra_prev].topsite; //take flag of higher peak of pair
          if (siteList[tra].topsite)
          {
            siteList[tra].clusterNr = tra;
            siteList[tra].clusterNum = siteList[tra].num;
            siteList[tra].clusterMul = 1;
            siteList[tra].value3 = siteList[tra].value2;
            siteList[tra].distance = 10000.0;
          }
          goto next_tra;
        }
      }

      //Look backwards as you are most likely to find a
      //site to cluster with close by in the list,not
      //at the top of the list each time (list in order)
      for (int tra_prev = tra-1; tra_prev >= 0 ; tra_prev--)
      {
      //Look at the sites not considering symmetry first
      //as you are most likely not going to need symmetry to
      //find a neighbouring site
        fracPrev = siteList[tra_prev].getFracXYZ(*this);
        //no whole cell translations first
        fracPrevCell = fracPrev;
        orthPrev = doFrac2Orth(fracPrevCell);
        floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
        if (delta < DIST2)
        {
          siteList[tra].clusterNr = siteList[tra_prev].clusterNr;
          siteList[siteList[tra].clusterNr].clusterMul++;
          if (siteList[tra].num < siteList[siteList[tra_prev].clusterNr].clusterNum)
             siteList[siteList[tra_prev].clusterNr].clusterNum = siteList[tra].num;
          if (siteList[tra].value2 > siteList[siteList[tra_prev].clusterNr].value3)
             siteList[siteList[tra_prev].clusterNr].value3 = siteList[tra].value2;
          goto next_tra;
        }

        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
            for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
            {
              fracPrevCell = fracPrev+cellTrans;
              orthPrev = doFrac2Orth(fracPrevCell);
              delta = (orthPrev-orthRef)*(orthPrev-orthRef);
              if (delta < DIST2)
              {
                siteList[tra].clusterNr = siteList[tra_prev].clusterNr;
                siteList[siteList[tra].clusterNr].clusterMul++;
                if (siteList[tra].num < siteList[siteList[tra_prev].clusterNr].clusterNum) siteList[siteList[tra_prev].clusterNr].clusterNum = siteList[tra].num;
                if (siteList[tra].value2 > siteList[siteList[tra_prev].clusterNr].value3) siteList[siteList[tra_prev].clusterNr].value3 = siteList[tra].value2;
                goto next_tra;
              }
            }
      }
      //Now consider symmetry related translations
      for (int tra_prev = tra-1; tra_prev >= 0 ; tra_prev--)
      {
        for (unsigned isym = 1; isym < NSYMM; isym++)
        {
          fracPrev = doSymXYZ(isym,siteList[tra_prev].getFracXYZ(*this));

          //no whole cell translations first
          fracPrevCell = fracPrev;
          orthPrev = doFrac2Orth(fracPrevCell);
          floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
          if (delta < DIST2)
          {
            siteList[tra].clusterNr = siteList[tra_prev].clusterNr;
            siteList[siteList[tra].clusterNr].clusterMul++;
            if (siteList[tra].num < siteList[siteList[tra_prev].clusterNr].clusterNum) siteList[siteList[tra_prev].clusterNr].clusterNum = siteList[tra].num;
            if (siteList[tra].value2 > siteList[siteList[tra_prev].clusterNr].value3) siteList[siteList[tra_prev].clusterNr].value3 = siteList[tra].value2;
            goto next_tra;
          }

          for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
            for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
              for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
              {
                fracPrevCell = fracPrev+cellTrans;
                orthPrev = doFrac2Orth(fracPrevCell);
                delta = (orthPrev-orthRef)*(orthPrev-orthRef);
                if (delta < DIST2)
                {
                  siteList[tra].clusterNr = siteList[tra_prev].clusterNr;
                  siteList[siteList[tra].clusterNr].clusterMul++;
                  if (siteList[tra].num < siteList[siteList[tra_prev].clusterNr].clusterNum) siteList[siteList[tra_prev].clusterNr].clusterNum = siteList[tra].num;
                  if (siteList[tra].value2 > siteList[siteList[tra_prev].clusterNr].value3) siteList[siteList[tra_prev].clusterNr].value3 = siteList[tra].value2;
                  goto next_tra;
                }
              } //cells
        } //symm
      }
      actual_num++;
      siteList[tra].topsite = true; // no hits found,this site must be unique
      siteList[tra].clusterNr = tra; //unique,so gets its own cluster number;
      siteList[tra].clusterNum = siteList[tra].num; //unique,so the cluster position so far is the position in the FTF
      siteList[tra].clusterMul = 1; //first member of its own cluster
      siteList[tra].value3 = siteList[tra].value2;
      siteList[tra].distance = 10000.0;
    }
    next_tra: continue; //performs the next iteration of the loop.
  }

// now work out distances from the topsite to the clustered peaks
  //first [tra] must be topsite and reference
  if (siteList.size())
  {
    siteList[0].distance = 0.0;
    dvect3 fracXyz(siteList[0].getFracXYZ(*this));
    orthRef = doFrac2Orth(fracXyz);
  }
  for (unsigned tra = 1; tra < siteList.size(); tra++) //all tra
  {
    siteList[tra].distance = 100000.0;
    if (siteList[tra].topsite)
    {
      for (unsigned isym = 0; isym < NSYMP; isym++)
      {
        fracPrev = doSymXYZ(isym,siteList[tra].getFracXYZ(*this));
        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
            for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
              {
                fracPrevCell = fracPrev + cellTrans;
                orthPrev = doFrac2Orth(fracPrevCell);
                floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
                floatType sqrtDelta(std::sqrt(std::fabs(delta)));
                siteList[tra].distance = std::min(siteList[tra].distance,sqrtDelta);
              }
      }
    }
  }

  if (LEVEL == 1) return; //default is 0, only take top of cluster

  for (unsigned tra = 0; tra < siteList.size(); tra++) //all tra
  {
    if (siteList[tra].topsite)
    {
      dvect3 orthRef = siteList[tra].getOrthXYZ();
      for (int tra_next = tra+1; tra_next < siteList.size(); tra_next++)
      if (siteList[tra_next].clusterNr == siteList[tra].clusterNr)
      {
        dvect3 fracNext = siteList[tra_next].getFracXYZ(*this);
        dvect3 fracNextCell = fracNext;
        dvect3 orthNext = doFrac2Orth(fracNextCell);
        double delta = (orthNext-orthRef)*(orthNext-orthRef);
          if (   (LEVEL == 0)
              || (LEVEL == 2 && delta <= 0.5*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 3 && delta <= 1.0*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 4 && delta <= 1.0*DIST2 && delta> 0.5*DIST2)
             )
        {
          siteList[tra_next].topsite = true;
          goto next_tra2;
        }

        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
            for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
            {
              dvect3 fracNextCell = fracNext+cellTrans;
              dvect3 orthNext = doFrac2Orth(fracNextCell);
              double delta = (orthNext-orthRef)*(orthNext-orthRef);
              if ( (LEVEL == 0)
              || (LEVEL == 2 && delta <= 0.5*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 3 && delta <= 1.0*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 4 && delta <= 1.0*DIST2 && delta> 0.5*DIST2)
              )
              {
                siteList[tra_next].topsite = true;
                goto next_tra2;
              }
            }
      }
      //Now consider symmetry related translations
      for (int tra_next = tra+1; tra_next < siteList.size(); tra_next++)
      if (siteList[tra_next].clusterNr == siteList[tra].clusterNr)
      {
        for (unsigned isym = 1; isym < NSYMM; isym++)
        {
          dvect3 fracNext = doSymXYZ(isym,siteList[tra_next].getFracXYZ(*this));

          //no whole cell translations first
          dvect3 fracNextCell = fracNext;
          dvect3 orthNext = doFrac2Orth(fracNextCell);
          double delta = (orthNext-orthRef)*(orthNext-orthRef);
          if ( (LEVEL == 0)
              || (LEVEL == 2 && delta <= 0.5*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 3 && delta <= 1.0*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 4 && delta <= 1.0*DIST2 && delta> 0.5*DIST2)
             )
          {
            siteList[tra].topsite = true;
            goto next_tra2;
          }

          for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
            for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
              for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
              {
                fracNextCell = fracNext+cellTrans;
                orthNext = doFrac2Orth(fracNextCell);
                delta = (orthNext-orthRef)*(orthNext-orthRef);
                if ( (LEVEL == 0)
              || (LEVEL == 2 && delta <= 0.5*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 3 && delta <= 1.0*DIST2 && delta> 0.0*DIST2)
              || (LEVEL == 4 && delta <= 1.0*DIST2 && delta> 0.5*DIST2)
                )
                {
                  siteList[tra].topsite = true;
                  goto next_tra2;
                }
              } //cells
        } //symm
      }
    }
    next_tra2: continue; //performs the next iteration of the loop.
  }
}

void SiteListXyz::logRawTop(outStream where,Output& output)
{
  for (int i = 0; i < max_nPrinted && i < siteList.size(); i++)
  {
    floatType maxValue = siteList[i].value;
    for (int t = i; t < siteList.size(); t++)
    {
      if (siteList[t].value >= maxValue)
      {
        maxValue = siteList[t].value;
        xyzsite tmp = siteList[i];
                siteList[i] = siteList[t];
                siteList[t] = tmp;
      }
    }
    siteList[i].num = i+1;
  }
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s\n","#","Frac X","Frac Y","Frac Z",FAST?"   FSS":"   LLG","Z-score");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    dvect3 frac = siteList[t].getFracXYZ(*this);
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    output.logTabPrintf(1,where,"%-5i % 6.3f % 6.3f % 6.3f % 10.*f %5.*f\n",
      siteList[t].num,
      frac[0],frac[1],frac[2],
      n,siteList[t].value,(siteList[t].zscore>=100?1:2),siteList[t].zscore);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#Sites = " + itos(num_selected) + ": output truncated to " +itos(max_nPrinted) + " sites");
  if (num_selected) output.logBlank(where);
}

void SiteListXyz::graphRawTop(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    for (int i = 0; i < max_nGraphed && i < siteList.size(); i++)
    {
      floatType maxValue = siteList[i].value;
      for (int t = i; t < siteList.size(); t++)
      {
        if (siteList[t].value >= maxValue)
        {
          maxValue = siteList[t].value;
          xyzsite tmp = siteList[i];
                  siteList[i] = siteList[t];
                  siteList[t] = tmp;
        }
      }
    }
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top raw peaks :");
      output.logTab(0,where,"$GRAPHS :Graph1 raw:AUTO:1,2:");
      output.logTab(0,where,"$$");
      output.logTab(0,where,"Peak_number  Score  $$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nGraphed; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f \n",num++,siteList[t].value);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

void SiteListXyz::logClusteredTop(outStream where,Output& output)
{
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-5s %-6s\n",
     "#","Frac X","Frac Y","Frac Z",FAST?"   FSS":"   LLG","Z-score","Split","#Group");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    dvect3 frac = siteList[t].getFracXYZ(*this);
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    output.logTabPrintf(1,where,"%-5i % 6.3f % 6.3f % 6.3f % 10.*f %5.*f % 5.*f %6.d\n",
      nPrinted,
      frac[0],frac[1],frac[2],
      n,siteList[t].value,
      (siteList[t].zscore >=100?1:2),siteList[t].zscore,
      (siteList[t].distance >=1000?0:1),siteList[t].distance,
      siteList[t].clusterMul);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");
  if (num_selected) output.logBlank(where);
}

void SiteListXyz::graphClusteredTop(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top clustered peaks:");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered:AUTO:1,2:");
      output.logTab(0,where,"        :Graph2 number:AUTO:1,3: $$");
      output.logTab(0,where,"Peak_number  Score  Number_in_Cluster$$ loggraph $$");    int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nPrinted; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f %d\n",num++,siteList[t].value,siteList[t].clusterMul);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

void SiteListXyz::logRawTopRescored(outStream where,Output& output)
{
  for (int i = 0; i < max_nPrinted && i < siteList.size(); i++)
  {
    floatType maxValue = siteList[i].value;
    for (int t = i; t < siteList.size(); t++)
    {
      if (siteList[t].value >= maxValue)
      {
        maxValue = siteList[t].value;
        xyzsite tmp = siteList[i];
                siteList[i] = siteList[t];
                siteList[t] = tmp;
      }
    }
    siteList[i].num = i+1;
  }
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
     output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-9s\n","#","Frac X","Frac Y","Frac Z","   LLG","Z-score","   FSS");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    dvect3 frac = siteList[t].getFracXYZ(*this);
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    int d = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value2)))),0.);
    output.logTabPrintf(1,where,"%-5i % 6.3f % 6.3f % 6.3f % 10.*f %5.*f % 10.*f\n",
      nPrinted,
      frac[0],frac[1],frac[2],
      n,siteList[t].value,
      (siteList[t].zscore>=100?1:2),siteList[t].zscore,
      d,siteList[t].value2);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");

  bool compute_CC(false);
  if (compute_CC && Size() > 2)
  {
    floatType sumxy(0),sumx(0),sumy(0),sumx2(0),sumy2(0);
    for (unsigned t = 0; t < Size(); t++)
    {
      sumx += siteList[t].value;
      sumy += siteList[t].value2;
    }
    floatType xmean(sumx/Size()),ymean(sumy/Size()),dx,dy;
    for (unsigned t = 0; t < Size(); t++)
    {
      dx = siteList[t].value-xmean;
      dy = siteList[t].value2-ymean;
      sumxy += dx*dy;
      sumx2 += dx*dx;
      sumy2 += dy*dy;
    }
    if (sumxy*sumx2*sumy2 != 0)
    {
      floatType correlation = sumxy/std::sqrt(sumx2*sumy2);
      floatType slope = sumxy/sumx2;
      floatType intercept = (sumy - slope*sumx)/Size();
      output.logBlank(VERBOSE);
      output.logTab(1,VERBOSE,"Correlation Coefficient between FTF and LLG = " + dtos(correlation) + " for " + itos(Size()) + " points");
      output.logTab(2,VERBOSE,"Slope and intercept of line: " + dtos(slope) + " " + dtos(intercept));
    }
  }

  //Break the mould and use cout to dump output
if (getenv("PHASER_MATHEMATICA_FILE") != NULL)
{
  std::ofstream outstream;
  outstream.open(getenv("PHASER_MATHEMATICA_FILE"));
  for (unsigned t = 0; t < Size(); t++)
    outstream << siteList[t].value << " " << siteList[t].value2 << std::endl;
  outstream.close();
}

  if (num_selected) output.logBlank(where);
}

void SiteListXyz::graphRawTopRescored(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    for (int i = 0; i < max_nGraphed && i < siteList.size(); i++)
    {
      floatType maxValue = siteList[i].value;
      for (int t = i; t < siteList.size(); t++)
      {
        if (siteList[t].value >= maxValue)
        {
          maxValue = siteList[t].value;
          xyzsite tmp = siteList[i];
                  siteList[i] = siteList[t];
                  siteList[t] = tmp;
        }
      }
    }
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top raw peaks rescored :");
      output.logTab(0,where,"$GRAPHS :Graph1 raw rescored:AUTO:1,2:");
      output.logTab(0,where,"$$");
      output.logTab(0,where,"Peak_number  Score  $$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nGraphed; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f \n",num++,siteList[t].value);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}


void SiteListXyz::logClusteredTopRescored(outStream where,Output& output)
{
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-5s %-6s %21s\n",
    "#","Frac X","Frac Y","Frac Z","   LLG","Z-score","Split","#Group","FSS-this-xyz/FSS-top");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    dvect3 frac = siteList[t].getFracXYZ(*this);
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    int m = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value2)))),0.);
    output.logTabPrintf(1,where,"%-5i % 6.3f % 6.3f % 6.3f % 10.*f %5.*f %5.*f %6d %10.*f/%10.*f\n",
        nPrinted,
        frac[0],frac[1],frac[2],
        n,siteList[t].value,
        (siteList[t].zscore>=100?1:2), siteList[t].zscore,
        (siteList[t].distance >=1000?0:1),siteList[t].distance,
        siteList[t].clusterMul,
        m,siteList[t].value2,
        m,siteList[t].value3);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");
  if (num_selected) output.logBlank(where);
}

void SiteListXyz::graphClusteredTopRescored(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top clustered peaks rescored:");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered rescored:AUTO:1,2:");
      output.logTab(0,where,"        :Graph2 number:AUTO:1,3: $$");
      output.logTab(0,where,"Peak_number  Score  Number_in_Cluster$$ loggraph $$");    int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nPrinted; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f %d\n",num++,siteList[t].value,siteList[t].clusterMul);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

} //end namespace phaser
