//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __Phaser__ListEditing__Class__
#define __Phaser__ListEditing__Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/ep_objects/ep_solution.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/io/Output.h>

namespace phaser {

class ListEditing
{
  public:
    ListEditing(bool b=false) { do_default_template_match=b; }
    //boolean flag allows the default move to origin to be turned off for GIMBLE refinement
    bool do_default_template_match;

  public:
    dvect31D add_known_orientations_to_rlist(map_str_pdb&,std::string,af::double6,floatType,mr_set&);
    pair_int_int  calculate_duplicates(map_str_pdb&,af::double6,floatType,mr_solution&,mr_solution,Output *output=0);
    void          move_to_template(map_str_pdb&,af::double6,floatType,mr_set&,mr_set&);
    int1D  purge_duplicates(double,std::string,af::double6,ep_solution&,Output *output=0);
    void   find_centrosymmetry(double,std::string,af::double6,ep_solution&,Output *output=0);
};

}
#endif
