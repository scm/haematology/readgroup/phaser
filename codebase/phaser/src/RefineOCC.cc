//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/src/RefineOCC.h>
#include <phaser/lib/round.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/solTerm.h>
#include <cmtzlib.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/xray/scatterer.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <cctbx/xray/fast_gradients.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/maptbx/statistics.h>
#include <scitbx/math/weighted_covariance.h>
#include <scitbx/random.h>
#include <cctbx/xray/sampled_model_density.h>
#include <mmtbx/masks/atom_mask.h>
#include <phaser/lib/factor.h>

namespace phaser {

RefineOCC::RefineOCC(data_occupancy& OCCUPANCY_,
                     data_solpar& SOLPAR_,
                     data_formfactors& FORMFACTOR_,
                     map_str_pdb* PDB_,
                     DataB& data,
                     Output& output) :
    RefineBase2(),
    DataB(data),
    OCCUPANCY(OCCUPANCY_),
    FORMFACTOR(FORMFACTOR_),
    PDB(PDB_),
    SOLPAR(SOLPAR_)
{
  initGfunction(false);
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);

  //make sure fft resolution runs to end of bin
  //so that the normalization has a full set of reflections in the bin
  fftHIRES = HiRes();
  for (int ibin = 0; ibin < bin.numbins(); ibin++)
  {
    if (bin.HiRes(ibin) < fftHIRES)
    {
      fftHIRES = bin.HiRes(ibin);
      break;
    }
  }

  selected_to_bin_end.resize(NREFL);
  selected_to_bin_end_miller.resize(0);
  for (int r = 0; r < NREFL; r++)
  {
    selected_to_bin_end[r] = selected[r] || (reso(r) > fftHIRES);
    if (selected_to_bin_end[r]) selected_to_bin_end_miller.push_back(MILLER[r]);
  }
  PHASER_ASSERT(selected_to_bin_end_miller.size());
  //comment out below and correct grads!!!
  //selected_to_bin_end = af_bool(NREFL,true);
  //selected_to_bin_end_miller = MILLER.deep_copy();

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  double d_min = cctbx::uctbx::d_star_sq_as_d(cctbxUC.max_d_star_sq(selected_to_bin_end_miller.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  double Shannon(3);
  double resolution_factor(1.0/Shannon);
  double quality_factor(100);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
    cctbxUC,d_min,resolution_factor,sym_flags,SgInfo,mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  cfft = scitbx::fftpack::complex_to_complex_3d<double>(gridding);
  u_base = cctbx::xray::calc_u_base(d_min,resolution_factor, quality_factor);

  nwindows = 0;
  do_fft_not_sum = true;
}

void RefineOCC::initKnown(double TOTAL_SCAT,mr_set& MRSET_)
{
  MRSET = MRSET_;

  fracScat.resize(MRSET.KNOWN.size()); // Save for later
  model_drms.resize(MRSET.KNOWN.size()); // Save for later
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  { //slow lookups to fast lookups
    std::string modlid = MRSET.KNOWN[s].MODLID;
    double SCATTERING = MRSET.BOOLOCC.size() ?
      PDB->find(modlid)->second.Coords().getScat(&MRSET.BOOLOCC[s]):
      PDB->find(modlid)->second.Coords().getScat(-999);
    fracScat[s] = SCATTERING/TOTAL_SCAT;
    model_drms[s] = MRSET.DRMS[MRSET.KNOWN[s].MODLID];
  } //end loop s

  //calc variances
  totvar_known.resize(MRSET.KNOWN.size());
  terms_on_sqrt_epsnSigmaP.resize(MRSET.KNOWN.size());
  Ecalc.resize(MRSET.KNOWN.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    totvar_known[s].resize(NREFL);
    terms_on_sqrt_epsnSigmaP[s].resize(NREFL);
    Ecalc[s].resize(NREFL);
    double VRMS = (*PDB)[MRSET.KNOWN[s].MODLID].DV[0].fixed;
    for (unsigned r = 0; r < NREFL; r++)
    {
      //double rssqr = ssqr(r);
      double rssqr = 1.0/fn::pow2(bin.MidRes(rbin(r)));
      double buf_vrms = model_drms[s] ? std::exp(-two_pi_sq_on_three*rssqr*model_drms[s]) : 1;
      double Vterm = fn::pow2(buf_vrms);
      double DLuzSqr = fn::pow2(solTerm(rssqr,SOLPAR)*DLuzzati(rssqr,VRMS));
      double thisV = DLuzSqr;
             thisV *= fn::pow2(DFAC[r]);
             thisV *= Vterm;
             thisV *= fracScat[s];
             thisV *= exp(-2.0*MRSET.KNOWN[s].BFAC*rssqr/4.0);
             //no ofac terms
             //no mult terms, coordinates must have M=1
      totvar_known[s][r] = thisV;
    }
  }

  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  xray_scatterers.resize(MRSET.KNOWN.size());
  scattering_type_registry.resize(MRSET.KNOWN.size());
  site_sym_table.resize(MRSET.KNOWN.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    xray_scatterers[s] = af::shared<xray::scatterer<double> >();
    site_sym_table[s] = cctbx::sgtbx::site_symmetry_table();
    scattering_type_registry[s] = cctbx::xray::scattering_type_registry();
    std::string MREF = MRSET.KNOWN[s].MODLID;
    dvect3 fracSolT(MRSET.KNOWN[s].TRA);
    dvect3 orthSolT(UnitCell::doFrac2Orth(fracSolT));
    dmat33 solR(MRSET.KNOWN[s].R);
    PHASER_ASSERT(PDB->find(MREF) != PDB->end());
    Molecule Coords = PDB->find(MREF)->second.Coords();
    for (int a = 0; a < Coords.nAtoms(); a++)
    {
      dvect3 orthXyz = solR*Coords.card(a).X + orthSolT;
      dvect3 fracXyz = UnitCell::doOrth2Frac(orthXyz);
      std::string atom_type = FORMFACTOR.label(Coords.card(a).Element);
      double occupancy = Coords.card(a).O;
             occupancy = std::max(DEF_MR_OMIN,occupancy);
      cctbx::xray::scatterer<> scatterer(
        itos(Coords.card(a).ResNum)+" "+Coords.card(a).Chain, // label
        fracXyz, // site
        cctbx::adptbx::b_as_u(Coords.card(a).B+MRSET.KNOWN[s].BFAC), // u_iso
        occupancy, // occupancy with limits applied
        atom_type,
          0, // fp
          0); // fdp
      site_sym_table[s].process(scatterer.apply_symmetry(cctbxUC,cctbxSG));
      scatterer.flags.set_grad_site(false);
      scatterer.flags.set_grad_u_iso(false);
      scatterer.flags.set_grad_u_aniso(false);
      scatterer.flags.set_grad_occupancy(true);
      scatterer.flags.set_grad_fp(false);
      scatterer.flags.set_grad_fdp(false);
      scatterer.flags.set_tan_u_iso(false);
      scatterer.apply_symmetry(cctbxUC, cctbxSG);
      xray_scatterers[s].push_back(scatterer);
    }
    scattering_type_registry[s].process(xray_scatterers[s].const_ref());
    scattering_type_registry[s].assign_from_table(FORMFACTOR.TABLE());
  }

  //if (getenv("PHASER_TEST_FFT") == 0)
  //  { std::cout << "setenv PHASER_TEST_FFT 1\n" ; std::exit(1); }
  do_fft_not_sum = true;// fixed, not (xray_scatterers.size() > int(DEF_FFTS_MAX));
  calcAtomic(true);

//#define AJM_DEBUG_SUM_FFT_SFCALC_MR
#ifdef AJM_DEBUG_SUM_FFT_SFCALC_MR
  calcAtomicFFT();
  cmplx2D fft_Ecalc = Ecalc;
  calcAtomicSum();
  for (int s = 0; s < Ecalc.size(); s++)
  for (int r = 0; r < NREFL && r < 10; r++)
  if (selected[r])
  {
    std::cout << "xxx s=" << s << " r= " << r << " fft vs sum:: fft=" << fft_Ecalc[s][r] << " sum=" << Ecalc[s][r] << std::endl;
  }
  std::exit(1);
#endif
  dLL_by_dPar.resize(MRSET.KNOWN.size());
  d2LL_by_dPar2.resize(MRSET.KNOWN.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    dLL_by_dPar[s].resize(xray_scatterers[s].size());
    d2LL_by_dPar2[s].resize(xray_scatterers[s].size());
  }
}

void RefineOCC::setup_constraints(int RESOFFSET)
{
  OFFSET = RESOFFSET;
//#define PHASER_AJM_DEBUG_CONSTRAINTS
#ifdef PHASER_AJM_DEBUG_CONSTRAINTS
  int2D atomocc(MRSET.KNOWN.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
     atomocc[s].resize(xray_scatterers[s].size(),0);
  for (OFFSET = 0; OFFSET < OCCUPANCY.WINDOW_NRES; OFFSET++)
#endif
  {
  constraint_list.resize(MRSET.KNOWN.size());
  PHASER_ASSERT(RESOFFSET < OCCUPANCY.WINDOW_NRES);
  std::string label = xray_scatterers[0][0].label;
  int         last_resnum = atoi(std::string(label.substr(0, label.find_first_of(' '))).c_str());
  std::string last_chain = label.substr(label.find_first_of(' ')+1,label.size());
  dvect3      last_orth = doFrac2Orth(xray_scatterers[0][0].site);
  int         last_s = 0;
  int         resnum(0);
  std::string chain;
  dvect3      orth;
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    constraint_list[s].clear();
    constraint_info midres_range(0,0);
    int first_a(0),nresnum(OFFSET),window_nres(0); //RESOFFSET
    double distSqr = 0;
    for (int a = 0; a <= xray_scatterers[s].size(); a++)
    {
      if (a < xray_scatterers[s].size())
      {
        std::string label = xray_scatterers[s][a].label;
        resnum = atoi(std::string(label.substr(0, label.find_first_of(' '))).c_str());
        chain = label.substr(label.find_first_of(' ')+1,label.size());
        orth = doFrac2Orth(xray_scatterers[s][a].site);
      }
      bool new_residue(s != last_s ||
                       chain != last_chain ||
                       resnum != last_resnum);
      if (new_residue) distSqr = (orth-last_orth)*(orth-last_orth);
      if (a == xray_scatterers[s].size() ||
          distSqr > fn::pow2(5.0) || //chain break - outside range of peptide bond distance
          new_residue)
      { window_nres++; nresnum++; } //previous increment
      if (a == xray_scatterers[s].size() ||
          distSqr > fn::pow2(5.0) || //chain break - outside range of peptide bond distance
          (s != last_s) || //new solution
          (chain != last_chain) || //new chain
          ((resnum != last_resnum) && (nresnum == OCCUPANCY.WINDOW_NRES))) //new window
      {
        midres_range.full_window = (window_nres == OCCUPANCY.WINDOW_NRES);
        int1D constraints(0);
        for (int aa = first_a; aa < a; aa++)
          constraints.push_back(aa);
#ifdef PHASER_AJM_DEBUG_CONSTRAINTS
  std::cout << "debug constraints: " ;
  if (a == xray_scatterers[s].size()) std::cout << "end chain, " ;
  if (distSqr > fn::pow2(5.0)) std::cout << "chain break, " ;
  if ((s != last_s)) std::cout << "new solution, " ;
  if ((chain != last_chain)) std::cout << "new chain, " ;
  if ((resnum != last_resnum) && (nresnum == OCCUPANCY.WINDOW_NRES)) std::cout << "new window, " ;
  std::cout << " offset=" << OFFSET << " res=" << ivtos(constraints) << " range=" << midres_range.amin << " " << midres_range.amax << " fw=" << btos(midres_range.full_window) << std::endl;
#endif
        constraint_list[s].push_back(std::pair<int1D,constraint_info>(constraints,midres_range));
        first_a = a;
        window_nres = 0;
        nresnum = (s != last_s) || (chain != last_chain) || distSqr > fn::pow2(5.0) ? OFFSET : 0;
        //nresnum = 0;
      }
      if (a == xray_scatterers[s].size()) break;
      bool odd(OCCUPANCY.WINDOW_NRES%2);
      int  nresmin(odd ? OCCUPANCY.WINDOW_NRES/2 : (OCCUPANCY.WINDOW_NRES-1)/2);
      int  nresmax(odd ? OCCUPANCY.WINDOW_NRES/2 : (OCCUPANCY.WINDOW_NRES-1)/2+1);
      if (distSqr > fn::pow2(5.0) ||
           s != last_s ||
           chain != last_chain ||
           resnum != last_resnum)
      {
        last_s = s;
        last_chain = chain;
        last_resnum = resnum;
        last_orth = doFrac2Orth(xray_scatterers[s][a].site);
        distSqr = (orth-last_orth)*(orth-last_orth);
        if (nresnum <= nresmin) //the middle of the window, or the start
        {
          midres_range.amin = a;
        }
      }
      else if (nresnum <= nresmax)
      { //the middle of the window, or the start
        midres_range.amax = a;
      }
    }
  }
  nwindows = 0;
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    for (int c = 0; c < constraint_list[s].size(); c++)
      if (constraint_list[s][c].second.full_window)
        nwindows++;

  //check all atoms are in a constraint group, and only once
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    int1D atomcount(xray_scatterers[s].size(),0);
    for (int c = 0; c < constraint_list[s].size(); c++)
      for (int a = 0; a < constraint_list[s][c].first.size(); a++)
        atomcount[constraint_list[s][c].first[a]]++;
    for (int a = 0; a < atomcount.size(); a++)
    {
    //if (atomcount[a] != 1) std::cout << "debug constraints: atomcount " << RESOFFSET << " " << a << " " << atomcount[a] <<std::endl;
      PHASER_ASSERT(atomcount[a] == 1);
    }
  }
#ifdef PHASER_AJM_DEBUG_CONSTRAINTS
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int c = 0; c < constraint_list[s].size(); c++)
    if (constraint_list[s][c].second.full_window)
      for (int aa = constraint_list[s][c].second.amin; aa <= constraint_list[s][c].second.amax; aa++)
      {
        atomocc[s][aa]++;
      }
#endif
  }
#ifdef PHASER_AJM_DEBUG_CONSTRAINTS
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < atomocc[s].size(); a++)
  {
    if (atomocc[s][a] != 1) std::cout << "debug constraints: atomocc " << s << " " << a << " " << atomocc[s][a] <<std::endl;
  }
#endif
}

void RefineOCC::calcAtomic(bool calcSigmaP)
{
  do_fft_not_sum ? calcAtomicFFT(calcSigmaP) : calcAtomicSum(calcSigmaP);
}

bool1D RefineOCC::getRefineMask(protocolPtr protocol)
{
  bool REFINE_ON(true);
  bool1D refineMask(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    refineMask.push_back(REFINE_ON);
  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

TNT::Vector<double> RefineOCC::getLargeShifts()
{
  TNT::Vector<double> largeShifts(npars_ref);
  for (int i = 0; i < npars_ref; i++)
    largeShifts[i] = 0.1;
  return largeShifts;
}

void RefineOCC::applyShift(TNT::Vector<double>& newx)
{
  int sa(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    xray_scatterers[s][a].occupancy = newx[sa++];
  calcAtomic();
}

void RefineOCC::logInit(outStream where,Output& output)
{
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    output.logTab(1,where,MRSET.KNOWN[s].logfile(0),false);
  output.logBlank(where);
  do_fft_not_sum ?
    output.logTab(1,VERBOSE,"Calculate structure factors by FFT"):
    output.logTab(1,VERBOSE,"Calculate structure factors by Direct Summation");
  logStats(VERBOSE,output);
  output.logTab(1,VERBOSE,"Initial Histogram of Occupancies");
  logHistogram(VERBOSE,output);
}

void RefineOCC::logInitial(outStream where,Output& output)
{}

void RefineOCC::logFinal(outStream where,Output& output)
{
  logStats(where,output);
  output.logTab(1,DEBUG,"Final Histogram of Occupancies");
  logHistogram(DEBUG,output);
}

void RefineOCC::logCurrent(outStream where,Output& output)
{
  output.logTab(1,TESTING,"Current Statistics");
  logStats(TESTING,output);
}

void RefineOCC::logStats(outStream where,Output& output)
{

  int sa(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    std::string MREF = MRSET.KNOWN[s].MODLID;
    output.logTab(1,where,"Ensemble: " + MREF);
    float1D  occs(0);
    Molecule Coords = PDB->find(MREF)->second.Coords();
    for (int a = 0; a < Coords.nAtoms(); a++)
    if (Coords.isTraceAtom(0,a)) //sampling, so as to keep occs short
    {
      if (Coords.isTraceAtom(0,a))
        occs.push_back(xray_scatterers[s][a].occupancy);
      sa++;
    }
    output.logTab(2,where,"Minimum/Maximum/Mean Occupancy = " + dtos(min(occs),4) + "/" + dtos(max(occs),4) + "/" + dtos(mean(occs),4));
  }
  output.logBlank(where);
}

void RefineOCC::logHistogram(outStream where,Output& output)
{
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    std::string MREF = MRSET.KNOWN[s].MODLID;
    output.logTab(1,where,"Ensemble: " + MREF);
    double  max_occ(0),mean_occ(0);
    int div = std::ceil(OCCUPANCY.MAX*10)+1;
    unsigned1D histogram(div,0);
    unsigned   maxStars(0);
    double  ntrace(0);
    Molecule Coords = PDB->find(MREF)->second.Coords();
    for (int a = 0; a < Coords.nAtoms(); a++)
    if (Coords.isTraceAtom(0,a)) //sampling, so as to keep occs short
    {
      double occupancy = xray_scatterers[s][a].occupancy;
             max_occ = std::max(occupancy,max_occ);
                         size_t iZ = std::max(0.0,round(occupancy*10));
             iZ = std::min(iZ,histogram.size()-1);
      if (Coords.isTraceAtom(0,a))
      {
        mean_occ += occupancy;
        histogram[iZ]++;
        ntrace++;
      }
      maxStars = std::max(maxStars,histogram[iZ]);
    }
    double columns = 50;
    double divStarFactor = maxStars/columns;
    int grad = divStarFactor*10; // maxStars/5
    output.logTabPrintf(1,where,"Mid Occupancy Bin Value\n");
    output.logTabPrintf(1,where," |    %-10d%-10d%-10d%-10d%-10d%-10d\n",0,grad,grad*2,grad*3,grad*4,grad*5);
    output.logTabPrintf(1,where," V    %-10s%-10s%-10s%-10s%-10s%-10s\n","|","|","|","|","|","|");
    for (unsigned h = 0; h < div; h++)
    {
      std::string stars;
      stars += dtos(h/10.,1) + "  ";
      int nstars = std::ceil(histogram[h]/divStarFactor);
      for (int star = 0; star < nstars; star++)
        stars += "*";
      if (histogram[h]) stars += " (" + itos(histogram[h]) + ")";
      output.logTab(1,where,stars);
      if (h/10. >= max_occ) break;
    }
    output.logBlank(where);
  }
}

std::string RefineOCC::whatAmI(int& ipar)
{
  int sa(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    if (sa++ == ipar) return "Occupancy for known #" + itos(s+1) + " atom #" + itos(a+1);
  return "Undefined Parameter";
}

TNT::Vector<double> RefineOCC::getRefinePars()
{
  TNT::Vector<double> pars(npars_ref);
  int sa(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    pars[sa++] = xray_scatterers[s][a].occupancy;
  return pars;
}

std::vector<reparams> RefineOCC::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int sa(0);
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
      repar[sa++].off();
  return repar;
}

std::vector<bounds> RefineOCC::getLowerBounds()
{
  if (getenv("PHASER_STUDY_PARAMS") != NULL)
  {
    std::vector<bounds> Lower(npars_ref);
    for (int i = 0; i < npars_ref; i++)
      Lower[i].on(OCCUPANCY.MIN);
    return Lower;
  }
  std::vector<bounds> Lower(1);
  Lower[0].on(OCCUPANCY.MIN);
  return Lower;
}

std::vector<bounds> RefineOCC::getUpperBounds()
{
  if (getenv("PHASER_STUDY_PARAMS") != NULL)
  {
    std::vector<bounds> Upper(npars_ref);
    for (int i = 0; i < npars_ref; i++)
      Upper[i].on(OCCUPANCY.MAX);
    return Upper;
  }
  std::vector<bounds> Upper(1);
  Upper[0].on(OCCUPANCY.MAX);
  return Upper;
}

void RefineOCC::setProtocol(protocolPtr protocol,Output& output)
{
  RefineBase::setProtocol(protocol,output);
}

float2D RefineOCC::getOcc()
{
  float2D tmp(MRSET.KNOWN.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
      tmp[s].push_back(xray_scatterers[s][a].occupancy);
  return tmp;
}

void RefineOCC::setOcc(float2D& OCC,bool calcSigmaP)
{
  PHASER_ASSERT(MRSET.KNOWN.size() == OCC.size());
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    PHASER_ASSERT(xray_scatterers[s].size() == OCC[s].size());
    for (int a = 0; a < xray_scatterers[s].size(); a++)
    {
      xray_scatterers[s][a].occupancy = OCC[s][a];
    }
  }
  calcAtomic(calcSigmaP);
}

void RefineOCC::mergeOcc(float3D OCC,int IOFFSET,bool calcSigmaP)
{
  for (int OFFSET = 0; OFFSET < OCCUPANCY.WINDOW_NRES; OFFSET+=IOFFSET)
  {
    setup_constraints(OFFSET);
    for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    {
      for (int a = 0; a < xray_scatterers[s].size(); a++)
      {
        for (int c = 0; c < constraint_list[s].size(); c++)
        {
          constraint_info midres_range = constraint_list[s][c].second;
          if (midres_range.full_window && a >= midres_range.amin && a <= midres_range.amax)
          {
            xray_scatterers[s][a].occupancy = OCC[OFFSET][s][a];
            break;
          }
        }
      }
    }
  }
  calcAtomic(calcSigmaP);
}

bool1D RefineOCC::getBoolOcc(int s)
{
  bool1D OCC;
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    OCC.push_back(bool(phaser::round(xray_scatterers[s][a].occupancy)));
  return OCC;
}

void RefineOCC::configureOcc(float1D OCC)
{
  PHASER_ASSERT(OCC.size() == npars_ref);
  int sa(0);
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    xray_scatterers[s][a].occupancy = OCC[sa++];
  calcAtomic(true);
}

double RefineOCC::likelihoodFn()
{
  double totalLL(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    totalLL += Rice_refl(r);
  }
  PHASER_ASSERT(totalLL-LLwilson);
  return totalLL-LLwilson;
}

double RefineOCC::Rice_refl(int r)
{
  std::complex<double> Etot(0);
  for (int s = 0; s < Ecalc.size(); s++)
    Etot += Ecalc[s][r];
  double ECsqr = Etot.real()*Etot.real()+Etot.imag()*Etot.imag();
  double V(1);
  //V = PTNCS.EPSFAC[r];
  for (int s = 0; s < totvar_known.size(); s++)
    V -= totvar_known[s][r];
  bool rcent = cent(r);
  double E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
  double Esqr = fn::pow2(E);
  //PHASER_ASSERT(ECsqr >= 0);
  double EC = std::sqrt(ECsqr);
  double X(0),reflLL(0);
//#define PHASER_NEGVAR_DEBUG_OCC
#ifdef PHASER_NEGVAR_DEBUG_OCC
  if (V <= 0) goto debug_code;
#else
  PHASER_ASSERT(V > 0);
#endif
  X = 2.0*E*EC/V;
  reflLL = -(std::log(V)+(Esqr + ECsqr)/V);
  //PHASER_ASSERT(X >= 0);
  if (rcent)
  {
    X /= 2.0;
    reflLL = reflLL/2.0 + m_alogch.getalogch(X);
  }
  else reflLL += m_alogchI0.getalogI0(X); //acentric
  return reflLL;

//debugging
#ifdef PHASER_NEGVAR_DEBUG_OCC
debug_code:
  {
    std::cout << "test " << "reflection # " << r << std::endl;
    for (int s = 0; s < MRSET.KNOWN.size(); s++)
    {
      std::cout << "test known #" << k <<   std::endl;
      std::cout << "  test bfac=" << MRSET.KNOWN[s].BFAC << std::endl;
      std::cout << "  test EM_known " << Ecalc[s][r] << std::endl;
      std::cout << "  test totvar_known " << -totvar_known[s][r] <<std::endl;
    }
    std::cout << "test " << "reflLL " <<  reflLL << std::endl;
    std::cout << "test " << "reso " <<  reso(r) << std::endl;
    int s = rbin(r);
    std::cout << "test " << "bin #" <<  s << " of " << bin.numbins() <<  std::endl;
    std::cout << "test " << "epsn " <<  epsn(r) << std::endl;
    std::cout << "test " << "ECsqr " << ECsqr << std::endl;
    std::cout << "test " << "E " << E << " Esqr " << Esqr << std::endl;
    std::cout << "test " << "V =" << V << std::endl;
    std::cout << std::endl;
    PHASER_ASSERT(V > 0);
  }//debug
#endif
  return reflLL;
}

double RefineOCC::targetFn()
{
// returns the -log(likelihood) of the current setup
  double l = likelihoodFn();
  double f = -l ;
  return f;
}

double RefineOCC::gradientFn(TNT::Vector<double>& Gradient)
{
  double totalLL = do_fft_not_sum ? calcGradFFT(true) : calcGradSum(true);
  Gradient.newsize(npars_ref);
  if (getenv("PHASER_STUDY_PARAMS") == NULL)
  {
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  for (int c = 0; c < constraint_list[s].size(); c++)
  {
    float1D atomic_grads(0);
    for (int a = 0; a < constraint_list[s][c].first.size(); a++)
      atomic_grads.push_back(dLL_by_dPar[s][constraint_list[s][c].first[a]]);
    double sum_atomic_grads = sum(atomic_grads);
    for (int a = 0; a < constraint_list[s][c].first.size(); a++)
      dLL_by_dPar[s][constraint_list[s][c].first[a]] = sum_atomic_grads;
  }
  }
  int sa(0);
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  for (int a = 0; a < xray_scatterers[s].size(); a++)
    Gradient[sa++] = dLL_by_dPar[s][a];
  double l = (totalLL - LLwilson);
  double f = -l;
  return f;
}

double RefineOCC::hessianFn(TNT::Fortran_Matrix<double>& Hessian,bool& is_diagonal)
{
  is_diagonal = true;
  Hessian.newsize(npars_ref,npars_ref);
  for (int ii = 0; ii < npars_ref; ii++)
  for (int jj = 0; jj < npars_ref; jj++)
  Hessian(ii+1,jj+1) =  0;
  double l(0);
  if (!do_fft_not_sum)
  {
    double totalLL = calcGradSum(false);
    l = (totalLL - LLwilson);
    int sa(0);
    for (int s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
    {
      Hessian(sa+1,sa+1) = -d2LL_by_dPar2[s][a];
      sa++;
    }
  }
  else // Start with unit matrix
  {
    l = likelihoodFn();
    int sa(0);
    for (int s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
    {
      Hessian(sa+1,sa+1) = 1.;
      sa++;
    }
  }
  double f = -l;
  return f;
}

void RefineOCC::calcAtomicSum(bool calcSigmaP)
{
  //sf calculation
  cctbx::eltbx::xray_scattering::gaussian ff_C = FORMFACTOR.fetch("C");
  cctbx::eltbx::xray_scattering::gaussian ff_N = FORMFACTOR.fetch("N");
  cctbx::eltbx::xray_scattering::gaussian ff_O = FORMFACTOR.fetch("O");
  cctbx::eltbx::xray_scattering::gaussian ff_S = FORMFACTOR.fetch("S");
  cctbx::eltbx::xray_scattering::gaussian ff_H = FORMFACTOR.fetch("H");
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    for (unsigned r = 0; r < NREFL; r++)
      Ecalc[s][r] = 0;
    float1D linearTerm(xray_scatterers[s].size()),debye_waller_u_iso(xray_scatterers[s].size(),0.);
    for (int a = 0; a < xray_scatterers[s].size(); a++)
    {
      double symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
      int M = SpaceGroup::NSYMM/site_sym_table[s].get(a).multiplicity();
      linearTerm[a] = symFacPCIF/M;
      if (!xray_scatterers[s][a].flags.use_u_aniso_only())
        debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(xray_scatterers[s][a].u_iso) * 0.25;
    }
    af_cmplx Fcalc(NREFL,std::complex<double>(0.0));
    for (unsigned r = 0; r < NREFL; r++)
    if (selected_to_bin_end[r])
    {
      double rssqr = ssqr(r);
      double fo_C = ff_C.at_d_star_sq(rssqr);
      double fo_N = ff_N.at_d_star_sq(rssqr);
      double fo_O = ff_O.at_d_star_sq(rssqr);
      double fo_S = ff_S.at_d_star_sq(rssqr);
      double fo_H = ff_H.at_d_star_sq(rssqr);
      std::vector<miller::index<int> >  rhkl = rotMiller(r);
      float1D   thkl = traMiller(r);
      for (int a = 0; a < xray_scatterers[s].size(); a++)
      {
        double fo_plus_fp(0);
        if (xray_scatterers[s][a].scattering_type == "C")
          fo_plus_fp = fo_C;
        else if (xray_scatterers[s][a].scattering_type == "N")
          fo_plus_fp = fo_N;
        else if (xray_scatterers[s][a].scattering_type == "O")
          fo_plus_fp = fo_O;
        else if (xray_scatterers[s][a].scattering_type == "S")
          fo_plus_fp = fo_S;
        else if (xray_scatterers[s][a].scattering_type == "H")
          fo_plus_fp = fo_H;
        else
          fo_plus_fp = FORMFACTOR.fetch(xray_scatterers[s][a].scattering_type).at_d_star_sq(rssqr);
        double isoB = xray_scatterers[s][a].flags.use_u_aniso_only() ? 1 : std::exp(-rssqr*debye_waller_u_iso[a]);
        double symmBfac(linearTerm[a]*isoB);
        double scat(fo_plus_fp*symmBfac);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
        {
          double anisoScat(scat);
          if (xray_scatterers[s][a].flags.use_u_aniso_only())
            anisoScat *= cctbx::adptbx::debye_waller_factor_u_star(rhkl[isym],xray_scatterers[s][a].u_star);
          double theta = rhkl[isym][0]*xray_scatterers[s][a].site[0] +
                            rhkl[isym][1]*xray_scatterers[s][a].site[1] +
                            rhkl[isym][2]*xray_scatterers[s][a].site[2] +
                            thkl[isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
          std::complex<double> sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
          double costheta = std::real(sincostheta);
          double sintheta = std::imag(sincostheta);
#else
          theta *= scitbx::constants::two_pi;
          double costheta = std::cos(theta);
          double sintheta = std::sin(theta);
#endif
          std::complex<double> noOccFcalc = anisoScat*std::complex<double>(costheta,sintheta);
          Fcalc[r] += xray_scatterers[s][a].occupancy*noOccFcalc;
        }//end sym
      } //end  loop over atoms
    } //end loop r
    // --- normalization
    if (calcSigmaP)
    {
      float1D numInBin(bin.numbins(),0);
      float1D SigmaP(bin.numbins(),0);
      for (int r = 0; r < NREFL; r++)
      if (selected_to_bin_end[r])
      {
        int ibin = rbin(r);
        double ECsqr = std::norm(Fcalc[r]);
        PHASER_ASSERT(ECsqr > 0);
        SigmaP[ibin] += ECsqr;
        numInBin[ibin]++;
      }
      double VRMS = (*PDB)[MRSET.KNOWN[s].MODLID].DV[0].fixed;
      for (int r = 0; r < NREFL; r++)
      if (selected_to_bin_end[r])
      {
        int ibin = rbin(r);
        //double rssqr = 1.0/fn::pow2(bin.MidRes(rbin(r)));
        double rssqr = ssqr(r);
        double sqrt_epsnSigmaP = std::sqrt(epsn(r)*SigmaP[ibin]/numInBin[ibin]);
        double DLuz(solTerm(rssqr,SOLPAR)*DLuzzati(rssqr,VRMS));
        double sqrt_scatFactor(0);
        if (fracScat[s] > 0) sqrt_scatFactor = std::sqrt(fracScat[s]);
        double terms = sqrt_scatFactor*DLuz;
        //Vterm
        double buf_vrms = model_drms[s] ?  std::exp(-two_pi_sq_on_three*rssqr*model_drms[s]) : 1;
        terms_on_sqrt_epsnSigmaP[s][r] = buf_vrms*terms/sqrt_epsnSigmaP;
      }
    }
    for (int r = 0; r < NREFL; r++)
    if (selected_to_bin_end[r])
    {
      Ecalc[s][r] += Fcalc[r]*terms_on_sqrt_epsnSigmaP[s][r];
    }
  } //end loop s
} //calcAtomicSum

double RefineOCC::calcGradSum(bool return_gradient)
{
  double totalLL(0);
  if (return_gradient)
  {
    for (int s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
      dLL_by_dPar[s][a] = 0;
  }
  else
  {
    for (int s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < xray_scatterers[s].size(); a++)
      d2LL_by_dPar2[s][a] = 0;
  }
  //sf calculation
  cctbx::eltbx::xray_scattering::gaussian ff_C = FORMFACTOR.fetch("C");
  cctbx::eltbx::xray_scattering::gaussian ff_N = FORMFACTOR.fetch("N");
  cctbx::eltbx::xray_scattering::gaussian ff_O = FORMFACTOR.fetch("O");
  cctbx::eltbx::xray_scattering::gaussian ff_S = FORMFACTOR.fetch("S");
  cctbx::eltbx::xray_scattering::gaussian ff_H = FORMFACTOR.fetch("H");
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected_to_bin_end[r])
    {
      std::complex<double> EC(0);
      for (int s = 0; s < MRSET.KNOWN.size(); s++)
        EC += Ecalc[s][r];
      double ECsqr = std::norm(EC);
      double V(1);
      //V = PTNCS.EPSFAC[r];
      for (int s = 0; s < MRSET.KNOWN.size(); s++)
        V -= totvar_known[s][r];
      bool rcent = cent(r);
      double E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
      double Esqr = fn::pow2(E);
      PHASER_ASSERT(ECsqr >= 0);
      double FC = std::sqrt(ECsqr);
      double X(0);
      PHASER_ASSERT(V > 0);
      X = 2.0*E*FC/V;
      double reflLL = -(std::log(V)+(Esqr + ECsqr)/V);
      PHASER_ASSERT(X >= 0);
      if (rcent)
      {
        X /= 2.0;
        reflLL = reflLL/2.0 + m_alogch.getalogch(X);
      }
      else reflLL += m_alogchI0.getalogI0(X); //acentric
      if (selected[r] && s==0) totalLL += reflLL;
      //derivatives
      double bessTerm = rcent ? std::tanh(X) : scitbx::math::bessel::i1_over_i0(X);
      double bessTerm2 = rcent ? fn::pow2(1/std::cosh(X)) : fn::pow2(bessTerm);
      double FA = EC.real();
      double FB = EC.imag();
      double dLL_by_dFC = rcent ? (1/V)*(E*bessTerm - FC) : (2/V)*(E*bessTerm - FC);
      double dFC_by_dFA = (FC > 0) ? FA/FC: 0;
      double dFC_by_dFB = (FC > 0) ? FB/FC: 0;
      //double dLL_by_dFA = -dLL_by_dFC*dFC_by_dFA*terms_on_sqrt_epsnSigmaP[s][r];
     // double dLL_by_dFB = -dLL_by_dFC*dFC_by_dFB*terms_on_sqrt_epsnSigmaP[s][r];
      double dLL_by_dFA = -dLL_by_dFC*dFC_by_dFA;
      double dLL_by_dFB = -dLL_by_dFC*dFC_by_dFB;
      double d2LL_by_dFA2(0),d2LL_by_dFB2(0),d2LL_by_dFA_dFB(0);
      if (!return_gradient && fn::pow4(FC) > 0)
      {
        double V2 = fn::pow2(V);
        double fc2 = fn::pow2(FC);
        double fc4 = fn::pow2(fc2);
        double denom = fc4*V2;
        d2LL_by_dFA2 = rcent ?
          bessTerm2*FA*FA*Esqr/fc2/V2 - bessTerm*FA*FA*FC*E/fc4/V + bessTerm*FC*E/fc2/V - 1/V :
          (-2*fc2*(fc2-bessTerm*FC*E)*V-4*FA*FA*E*((-1 + bessTerm2)*fc2*E + bessTerm*FC*V))/denom;
        d2LL_by_dFB2 = rcent ?
          bessTerm2*FB*FB*Esqr/fc2/V2 - bessTerm*FB*FB*FC*E/fc4/V + bessTerm*FC*E/fc2/V - 1/V :
          (-2*fc2*(fc2-bessTerm*FC*E)*V-4*FB*FB*E*((-1 + bessTerm2)*fc2*E + bessTerm*FC*V))/denom;
        d2LL_by_dFA_dFB = rcent ?
          bessTerm2*FA*FB*Esqr/fc2/V2 - bessTerm*FA*FB*FC*E/fc4/V :
          -4*FA*FB*E*((-1 + bessTerm2)*fc2*E+bessTerm*FC*V)/denom;
      }
      double rssqr = ssqr(r);
      double fo_C = ff_C.at_d_star_sq(rssqr);
      double fo_N = ff_N.at_d_star_sq(rssqr);
      double fo_O = ff_O.at_d_star_sq(rssqr);
      double fo_S = ff_S.at_d_star_sq(rssqr);
      double fo_H = ff_H.at_d_star_sq(rssqr);
      std::vector<miller::index<int> >  rhkl = rotMiller(r);
      float1D   thkl = traMiller(r);
      std::complex<double> thisE;
      for (int a = 0; a < xray_scatterers[s].size(); a++)
      {
        double fo_plus_fp(0);
        if (xray_scatterers[s][a].scattering_type == "C")
          fo_plus_fp = fo_C;
        else if (xray_scatterers[s][a].scattering_type == "N")
          fo_plus_fp = fo_N;
        else if (xray_scatterers[s][a].scattering_type == "O")
          fo_plus_fp = fo_O;
        else if (xray_scatterers[s][a].scattering_type == "S")
          fo_plus_fp = fo_S;
        else if (xray_scatterers[s][a].scattering_type == "H")
          fo_plus_fp = fo_H;
        else
          fo_plus_fp = FORMFACTOR.fetch(xray_scatterers[s][a].scattering_type).at_d_star_sq(rssqr);
        double symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
        int M = SpaceGroup::NSYMM/site_sym_table[s].get(a).multiplicity();
        double linearTerm = symFacPCIF/M;
        double debye_waller_u_iso(0);
        if (!xray_scatterers[s][a].flags.use_u_aniso_only())
          debye_waller_u_iso = cctbx::adptbx::u_as_b(xray_scatterers[s][a].u_iso) * 0.25;
        double isoB = xray_scatterers[s][a].flags.use_u_aniso_only() ? 1 : std::exp(-rssqr*debye_waller_u_iso);
        double symmBfac(linearTerm*isoB);
        double scat(fo_plus_fp*symmBfac);
        std::complex<double> Ecalc_atom(0);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
        {
          double anisoScat(scat);
          if (xray_scatterers[s][a].flags.use_u_aniso_only())
            anisoScat *= cctbx::adptbx::debye_waller_factor_u_star(rhkl[isym],xray_scatterers[s][a].u_star);
          double theta = rhkl[isym][0]*xray_scatterers[s][a].site[0] +
                         rhkl[isym][1]*xray_scatterers[s][a].site[1] +
                         rhkl[isym][2]*xray_scatterers[s][a].site[2] +
                         thkl[isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
          std::complex<double> sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
          double costheta = std::real(sincostheta);
          double sintheta = std::imag(sincostheta);
#else
          theta *= scitbx::constants::two_pi;
          double costheta = std::cos(theta);
          double sintheta = std::sin(theta);
#endif
          Ecalc_atom += anisoScat*std::complex<double>(costheta,sintheta);
        }//end sym
        Ecalc_atom *= terms_on_sqrt_epsnSigmaP[s][r];
        double dFA_by_dO = Ecalc_atom.real();
        double dFB_by_dO = Ecalc_atom.imag();
        double dLL_by_dO = dLL_by_dFA*dFA_by_dO+dLL_by_dFB*dFB_by_dO;
        if (return_gradient)
          dLL_by_dPar[s][a] += dLL_by_dO;
        else
          d2LL_by_dPar2[s][a] += d2LL_by_dFA2*fn::pow2(dFA_by_dO) +
                                 d2LL_by_dFB2*fn::pow2(dFB_by_dO) +
                                 2*d2LL_by_dFA_dFB*dFA_by_dO*dFB_by_dO;
        thisE += Ecalc_atom*xray_scatterers[s][a].occupancy;
      } //end  loop over atoms
    } //end loop r
  } //s
  PHASER_ASSERT(totalLL);
  return totalLL;
} // end calcGradSum

void RefineOCC::calcAtomicFFT(bool calcSigmaP)
{
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  {
    // --- spacegroup and unitcell
    cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
    const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
    cctbx::xray::sampled_model_density<double> sampled_density(
      cctbxUC,
      xray_scatterers[s].const_ref(),
      scattering_type_registry[s],
      cfft.n(),
      cfft.n(),
      u_base,
      1.e-8, // wing_cutoff 1.e-8
      -100, // exp_table_one_over_step_size
      true); // force_complex, otherwise all fdp = 0 gives a real map
    // --- fiddle to get correct form to pass to the fft calculation
    cctbx::xray::sampled_model_density<double>::complex_map_type density_map = sampled_density.complex_map();
    PHASER_ASSERT(density_map.size());
    af::ref<std::complex<double>, af::c_grid<3> > density_map_ref( &*density_map.begin(), af::c_grid<3>(cfft.n()));
    // --- do the fft
    cfft.backward(density_map_ref);
    // --- extract the complex structure factors
    af::const_ref<std::complex<double>, af::c_grid_padded<3> > density_map_cref(
        reinterpret_cast<std::complex<double>*>(&*density_map.begin()), af::c_grid_padded<3>(cfft.n()));
    bool conjugate_flag(false); //extracts h,k,l
    cctbx::maptbx::structure_factors::from_map<double> from_map(
       cctbxSG,
       sampled_density.anomalous_flag(),
       selected_to_bin_end_miller.const_ref(),
       density_map_cref,
       conjugate_flag);
    af_cmplx Fcalc = from_map.data();
    sampled_density.eliminate_u_extra_and_normalize(selected_to_bin_end_miller.const_ref(),Fcalc.ref());

  /*
    //-------------------------------------------------------------------------------------
    af::shared<dvect3>    fracxyz;
    af::shared<floatType> atoms_radii;
    for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
    {
      std::string MREF = MRSET.KNOWN[s].MODLID;
      dvect3 fracSolT(MRSET.KNOWN[s].TRA);
      dvect3 orthSolT(UnitCell::doFrac2Orth(fracSolT));
      dmat33 solR(MRSET.KNOWN[s].R);
      Molecule& Coords = PDB->find(MREF)->second.Coords();
      for (int a = 0; a < Coords.nAtoms(); a++)
      {
        dvect3 orthXyz = Coords.getOrthXYZ(a,solR,orthSolT);
        dvect3 fracXyz = UnitCell::doOrth2Frac(orthXyz);
        std::string atom_type = FORMFACTOR.label(Coords.getElement(a));
        fracxyz.push_back(fracXyz);
        atoms_radii.push_back(atoms_vdw_table.lookup(atom_type));
      }
    }

    // --- add bulk solvent correction
    floatType SHARAT(1.5);
    ivect3 limits = UnitCell::getMapLimits(HIRES,SHARAT);
    int max_prime_fac = 5;
    ivect3 symm_fac(2,2,2); //symmetry factors for P1
    bool next_highest = true;
    limits[0] = factor(limits[0],max_prime_fac,symm_fac[0],next_highest);
    limits[1] = factor(limits[1],max_prime_fac,symm_fac[1],next_highest);
    limits[2] = factor(limits[2],max_prime_fac,symm_fac[2],next_highest);
    scitbx::fftpack::real_to_complex_3d<floatType> rfft(limits);

    af_float k_mask(MILLER.size());
    for (int r = 0; r < MILLER.size(); r++)
    {
      double ss = cctbxUC.d_star_sq(MILLER[r])/4.;
      k_mask[r] = SOLPAR.BULK_FSOL*std::exp(-SOLPAR.BULK_BSOL*ss);
    }
    mmtbx::masks::atom_mask atommask(
      cctbxUC,
      cctbxSG,
      rfft.n_real(),
      1,1);
    atommask.compute(fracxyz,atoms_radii);
    af_cmplx Fmask = atommask.structure_factors(MILLER.const_ref());

    if (SOLPAR.BULK_USE)
      for (int r = 0; r < MILLER.size(); r++)
        Fcalc[r] += k_mask[r]*Fmask[r];
    //-------------------------------------------------------------------------------------
  */

    // --- normalization
    if (calcSigmaP)
    {
      float1D numInBin(bin.numbins());
      int rr(0);
      float1D SigmaP(bin.numbins(),0);
      for (int r = 0; r < NREFL; r++)
      if (selected_to_bin_end[r])
      {
        int ibin = rbin(r);
        double FCsqr = std::norm(Fcalc[rr++]);
        PHASER_ASSERT(FCsqr >= 0);
        SigmaP[ibin] += FCsqr;
        numInBin[ibin]++;
      }
      double VRMS = (*PDB)[MRSET.KNOWN[s].MODLID].DV[0].fixed;
      for (int r = 0; r < NREFL; r++)
      if (selected_to_bin_end[r])
      {
        int ibin = rbin(r);
        double rssqr = ssqr(r);
        PHASER_ASSERT(numInBin[ibin]);
        PHASER_ASSERT(SigmaP[ibin]);
        double sqrt_epsnSigmaP = std::sqrt(epsn(r)*SigmaP[ibin]/numInBin[ibin]);
        double DLuz(solTerm(rssqr,SOLPAR)*DLuzzati(rssqr,VRMS));
        double sqrt_scatFactor(0);
        if (fracScat[s] > 0) sqrt_scatFactor = std::sqrt(fracScat[s]);
        double terms = sqrt_scatFactor*DLuz;
        PHASER_ASSERT(sqrt_epsnSigmaP > 0);
        double buf_vrms = model_drms[s] ?  std::exp(-two_pi_sq_on_three*rssqr*model_drms[s]) : 1;
        double Eterm = buf_vrms;
        terms_on_sqrt_epsnSigmaP[s][r] = DFAC[r]*Eterm*terms/sqrt_epsnSigmaP;
      }
    }
    int rr(0);
    for (int r = 0; r < NREFL; r++)
    if (selected_to_bin_end[r])
    {
      Ecalc[s][r] = Fcalc[rr++]*terms_on_sqrt_epsnSigmaP[s][r];
    }
  }

}//calcAtomicFFT

double RefineOCC::calcGradFFT(bool return_gradient)
{
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
  const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();

  double totalLL(0);
  for (int s = 0; s < MRSET.KNOWN.size(); s++)
    for (int a = 0; a < dLL_by_dPar[s].size(); a++)
       dLL_by_dPar[s][a] = 0;

  af::shared<miller::index<int> > expanded_miller;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected_to_bin_end[r])
  {
    expanded_miller.push_back(MILLER[r]);
    expanded_miller.push_back(-MILLER[r]);
  }

  for (int s = 0; s < MRSET.KNOWN.size(); s++)
  {
    totalLL = 0;
    af_cmplx expanded_gradient;
    for (unsigned r = 0; r < NREFL; r++)
    if (selected_to_bin_end[r])
    {
      std::complex<double> EC(0);
      for (int s = 0; s < MRSET.KNOWN.size(); s++)
        EC += Ecalc[s][r];
      //std::complex<double> EC(Ecalc[s][r]);
      double ECsqr = std::norm(EC);
      double V(1);
      //V = PTNCS.EPSFAC[r];
      for (int s = 0; s < MRSET.KNOWN.size(); s++)
        V -= totvar_known[s][r];
      bool rcent = cent(r);
      double E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
      double Esqr = fn::pow2(E);
      PHASER_ASSERT(ECsqr >= 0);
      double FC = std::sqrt(ECsqr);
      double X(0);
      PHASER_ASSERT(V > 0);
      X = 2.0*E*FC/V;
      double reflLL = -(std::log(V)+(Esqr + ECsqr)/V);
      PHASER_ASSERT(X >= 0);
      if (rcent)
      {
        X /= 2.0;
        reflLL = reflLL/2.0 + m_alogch.getalogch(X);
      }
      else reflLL += m_alogchI0.getalogI0(X); //acentric
      if (selected[r]) totalLL += reflLL;

      //derivatives
      double bessTerm = rcent ? std::tanh(X) : scitbx::math::bessel::i1_over_i0(X);
      double FA = EC.real();
      double FB = EC.imag();
      double dLL_by_dFC = rcent ? (1/V)*(E*bessTerm - FC) : (2/V)*(E*bessTerm - FC);
      double dFC_by_dFA_s = (FC > 0) ? FA/FC: 0;
      double dFC_by_dFB_s = (FC > 0) ? FB/FC: 0;
      double dLL_by_dFA_s = -dLL_by_dFC*dFC_by_dFA_s*terms_on_sqrt_epsnSigmaP[s][r];
      double dLL_by_dFB_s = -dLL_by_dFC*dFC_by_dFB_s*terms_on_sqrt_epsnSigmaP[s][r];

      expanded_gradient.push_back(std::complex<double>(dLL_by_dFA_s,dLL_by_dFB_s));
      expanded_gradient.push_back(std::conj(std::complex<double>(dLL_by_dFA_s,dLL_by_dFB_s)));
    }

    cctbx::xray::fast_gradients<double> fast_gradients(
      cctbxUC,
      xray_scatterers[s].const_ref(),
      scattering_type_registry[s],
      u_base,
      1.e-8 // wing_cutoff 1.e-3
      );
    double  multiplier = cctbxUC.volume() / ivect3(cfft.n()).product() * cctbxSG.n_ltr();
    cctbx::xray::apply_u_extra(
      cctbxUC,
      fast_gradients.u_extra(),
      expanded_miller.const_ref(),
      expanded_gradient.ref(),
      multiplier);

    bool anomalous_flag(true);
    bool conjugate_flag(true);
    bool treat_restricted(true);
    af::c_grid_padded<3> map_grid(cfft.n());
    cctbx::maptbx::structure_factors::to_map<double> gradmap(
      cctbxSG,
      anomalous_flag,
      expanded_miller.const_ref(),
      expanded_gradient.const_ref(),
      cfft.n(),
      map_grid,
      conjugate_flag,
      treat_restricted);

    // --- do the fft
    af::ref<std::complex<double>, af::c_grid<3> > gradmap_ref( gradmap.complex_map().begin(), af::c_grid<3>(cfft.n()));
    cfft.backward(gradmap_ref);
    af::const_ref<std::complex<double>, scitbx::af::c_grid_padded_periodic<3> > gradmap_const_ref(
      gradmap_ref.begin(), scitbx::af::c_grid_padded_periodic<3>(cfft.n(), cfft.n()));
    bool sampled_density_must_be_positive(true);

    fast_gradients.sampling(
      xray_scatterers[s].const_ref(),
      af::const_ref<double>(0,0),//if sqrt_u_iso false in grad_flags, this is ignored
      scattering_type_registry[s],
      site_sym_table[s],
      gradmap_const_ref,
      0,//n_parameters, unpacked if zero, packed otherwise
      sampled_density_must_be_positive);

    af_float dtdo = fast_gradients.d_target_d_occupancy();

    for (int a = 0; a < xray_scatterers[s].size(); a++)
    {
      dtdo[a] /= 2.0;
      dLL_by_dPar[s][a] += dtdo[a];
    }
  }
  PHASER_ASSERT(totalLL);
  return totalLL;
}// end calcGradFFT

void RefineOCC::setUp()
{
  calcAtomic(true);
}

}//phaser
