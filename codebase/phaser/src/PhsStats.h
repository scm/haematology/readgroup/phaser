//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PhsStatsClass__
#define __PhsStatsClass__
#include <phaser/main/Phaser.h>

namespace phaser {

class integration;

class PhsStats
{
  public:
    PhsStats() { totLogLike = 0; }
    void addRefl(miller::index<int>,integration);
    void addRefl(miller::index<int>);
    ~PhsStats() {}

    std::vector<miller::index<int> > MILLER;
    float1D FOM,PHIB,HLA,HLB,HLC,HLD;
    floatType totLogLike;
};

} //end namespace phaser

#endif
