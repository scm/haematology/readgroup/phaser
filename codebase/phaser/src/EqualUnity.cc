//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <math.h>
#include <scitbx/constants.h>
#include <phaser/src/EqualUnity.h>

namespace phaser {

EqualUnity::EqualUnity(int n,floatType start_ang)
{
  nStep = Weight = 0;
  resize(n,start_ang);
}

void EqualUnity::resize(int n,floatType start_ang)
{
  nStep = n;
  Ang.clear();
  CosAng.clear();
  SinAng.clear();
  Cos2Ang.clear();
  Sin2Ang.clear();
  Weight = scitbx::constants::two_pi/nStep;
  for (int p = 0; p < n; p++)
  {
    Ang.push_back(start_ang + p*Weight);
    CosAng.push_back(cos(Ang[p]));
    SinAng.push_back(sin(Ang[p]));
    Cos2Ang.push_back(cos(2.0*Ang[p]));
    Sin2Ang.push_back(sin(2.0*Ang[p]));
  }
}

}//phaser
