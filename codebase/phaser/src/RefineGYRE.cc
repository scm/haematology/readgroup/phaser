//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineGYRE.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/between.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/bessel.h>

namespace phaser {

RefineGYRE::RefineGYRE(DataMR& data) : RefineBase2(), DataMR(data)
{
}

double RefineGYRE::Wilson()
{
// returns the -log(likelihood) of the current setup
  double totalLL(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    double reflLL(0);
    cmplxType EM(EM_known[r]+EM_search[r]);
    double EM_real = EM.real();
    double EM_imag = EM.imag();
    double phasedEsqr = EM_real*EM_real+EM_imag*EM_imag;

    double V(0);
    V = PTNCS.EPSFAC[r];
    V -= totvar_known[r];
    V -= totvar_search[r];
    V += phasedEsqr + sum_Esqr_search[r];

    double E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
    double Esqr = fn::pow2(E);
    PHASER_ASSERT(V > 0);
    reflLL = -(std::log(V)+(Esqr)/V);
    if (cent(r)) { reflLL/=2.0; }
    totalLL += reflLL;
  }
  return totalLL;
}

floatType RefineGYRE::targetFn()
{
// returns the -log(likelihood) of the current setup
  floatType totalLL = Wilson();
  floatType f = -(totalLL - LLwilson);
            f += restraint_term();
  return f;
}

floatType RefineGYRE::restraint_term()
{
  int m(0),i(0);
  floatType restraints(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      if (use_rotref_restraint[p])
      {
        restraints += fn::pow2(GYRE[p].perturbRot[rot]/sigma_rotref[p])/2.;
      }
      i++;
    }
    for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++])
    {
      if (use_traref_restraint[p])
      {
        restraints += fn::pow2(GYRE[p].perturbTrans[tra]/sigma_traref[p])/2.;
      }
      i++;
    }
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) i++; //VRMS
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  //localMLL += restraints;
  return restraints;
}

floatType RefineGYRE::gradientFn(TNT::Vector<floatType>& Gradient)
{
//#define PHASER_TIMINGS
#ifdef PHASER_TIMINGS
  static int countr(0);
  Output output;
  output.logTab(1,LOGFILE,"Gradient Start # " + itos(countr++));
  floatType start_clock = std::clock();
#endif
  //initialization
  Gradient.newsize(npars_ref);
  bool return_gradient(true);
  floatType localMLL = gradient_hessian(Gradient,return_gradient); //-(totalLL - LLwilson) + restraint

  int m(0),i(0);
  int1D Bindex(models_known.size());
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      if (use_rotref_restraint[p])
      {
        Gradient[i] += GYRE[p].perturbRot[rot]/sigma_rotref[p];
      }
      i++;
    }
    for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++])
    {
      if (use_traref_restraint[p])
      {
        Gradient[i] += GYRE[p].perturbTrans[tra]/sigma_traref[p];
      }
      i++;
    }
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) i++; //VRMS
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  return localMLL;
}

floatType RefineGYRE::hessianFn(TNT::Fortran_Matrix<floatType>& Hessian,bool& is_diagonal)
{
#ifdef PHASER_TIMINGS
  static int countr(0);
  Output output;
  output.logTab(1,LOGFILE,"Hessian Start # " + itos(countr++));
  floatType start_clock = std::clock();
#endif

  //initialization
  TNT::Vector<floatType> diag_hessian(npars_ref);
  bool return_gradient(false);
  floatType localMLL = gradient_hessian(diag_hessian,return_gradient); //-(totalLL - LLwilson) + restraint
  is_diagonal = true;

  Hessian.newsize(npars_ref,npars_ref);
  PHASER_ASSERT(Hessian.num_cols() == npars_ref);
  PHASER_ASSERT(Hessian.num_rows() == npars_ref);
  for (int ii = 0; ii < npars_ref; ii++) //not i, warning on visual-C, int i defined below
    for (int jj = 0; jj < npars_ref; jj++)
      Hessian(ii+1,jj+1) =  (ii==jj) ? diag_hessian[jj] : 0;

  int m(0),i(0);
  int1D Bindex(models_known.size());
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      if (use_rotref_restraint[p])
      {
        Hessian(i+1,i+1) += 1/fn::pow2(sigma_rotref[p]);
      }
      i++;
    }
    for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++])
    {
      if (use_traref_restraint[p])
      {
        Hessian(i+1,i+1) += 1/fn::pow2(sigma_traref[p]);
      }
      i++;
    }
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) i++; //VRMS
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return localMLL;
}

floatType RefineGYRE::gradient_hessian(TNT::Vector<floatType>& Array,bool return_gradient)
{
  const cmplxType cmplxZERO(0.,0.),cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
  dvect31D dLL_by_drot(GYRE.size(),dvect3(0,0,0));
  dvect31D dLL_by_dtra(GYRE.size(),dvect3(0,0,0));
  dvect31D d2LL_by_drot2(GYRE.size(),dvect3(0,0,0));
  dvect31D d2LL_by_dtra2(GYRE.size(),dvect3(0,0,0));
  map_str_float dLL_by_dvrms;
  map_str_float d2LL_by_dvrms2;
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    dLL_by_dvrms[iter->first] = 0;
    d2LL_by_dvrms2[iter->first] = 0;
  }
  floatType totalLL(0);

  dmat331D Q1tr(GYRE.size());
  dmat331D PR_Rtr_Dtr(GYRE.size());
  dvect31D TRA(GYRE.size());

  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
    //recover original centre of mass, rotated centre of mass
    dmat33 PRtr = ens_modlid->PR.transpose();
    dvect3 CMori = -PRtr*ens_modlid->PT;
    dmat33 ROT = GYRE[p].R;
    dvect3 CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    dmat33 Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,GYRE[p].perturbRot[dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    dvect3 CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented (*ensemble))
    ROT = ROT*PRtr;

    TRA[p] = GYRE[p].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA[p]);
    //apply xyz translation perturbation (for refinement)
    iOrthT += GYRE[p].perturbTrans;
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of (*ensemble))
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA[p] = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    Q1tr[p] = Q1.transpose();
    dmat33 Rtr = GYRE[p].R.transpose();
    PR_Rtr_Dtr[p] = ens_modlid->PR * Rtr * UnitCell::Orth2Frac().transpose();
  }

  bool return_hessian = !return_gradient;
  int countr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    countr++;
    floatType rssqr = ssqr(r);
    floatType& sqrt_repsn = sqrt_epsn[r];
    floatType sqrtrssqr = std::sqrt(rssqr);
    rotderivative FCderiv(GYRE.size(),SpaceGroup::NSYMP);
    { //memory
    std::vector<miller::index<int> > rhkl = rotMiller(r);
    for (unsigned p = 0; p < GYRE.size(); p++)
    {
      Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      floatType sqrt_scatFactor = std::sqrt(ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        FCderiv.duplicate[isym] = true;
        if (!duplicate(isym,rhkl))
        {
          FCderiv.duplicate[isym] = false;

          dvect3    RotSymHKL = Q1tr[p]*rhkl[isym];
          cvect3    dthisE(0,0,0);
          cmat33    hthisE(0,0,0,0,0,0,0,0,0);
          cmplxType interpE = ens_modlid->func_grad_hess_InterpE(RotSymHKL,DFAC[r],dthisE,return_hessian,hthisE);
          floatType Bfac = 0;
          cmplxType scalefac = sqrt_repsn * std::exp(-Bfac*rssqr/4.0) * dphi(r,isym,rhkl[isym],TRA[p]);
                    scalefac *= ens_modlid->Eterm_const();
                    scalefac *= sqrt_scatFactor;
                    scalefac /= 1;//MULT
          cmplxType thisE  = scalefac * interpE;
                    dthisE = (scalefac*DFAC[r]) * dthisE;
                    hthisE = (scalefac*DFAC[r]) * hthisE;
          FCderiv.FC[p][isym] = thisE;
          dvect3 dQ1tr_h_by_drot_p(0,0,0);
          dmat33 dQ1tr_by_drot(0,0,0,0,0,0,0,0,0);
          for (int rot = 0; rot < 3; rot++)
          {
            dQ1tr_by_drot = PR_Rtr_Dtr[p];
            for (int dir = 2; dir >= 0; dir--)
            {
              dQ1tr_by_drot = xyzRotMatDeg(dir,GYRE[p].perturbRot[dir],dir==rot).transpose()*dQ1tr_by_drot;
            }
            dQ1tr_by_drot = ens_modlid->Frac2Orth().transpose() * dQ1tr_by_drot;
            dQ1tr_h_by_drot_p = dQ1tr_by_drot*rhkl[isym];
            FCderiv.dFC_by_drot[p][isym][rot] = dthisE*dQ1tr_h_by_drot_p;
            if (return_hessian) // Could include curvature term d2Q1tr_by_drot2 as in RefineMR.cc, but effect is small
            {
              FCderiv.d2FC_by_drot2[p][isym][rot] = dQ1tr_h_by_drot_p*(hthisE*dQ1tr_h_by_drot_p);
            }
          }
          for (int tra = 0; tra < 3; tra++)
          {
            //dphi = rhkl.TRA + traMiller
            dvect3 dTRA_by_du(tra==0 ? 1 : 0, tra==1 ? 1 : 0, tra==2 ? 1 : 0);
            dvect3 dhkl(rhkl[isym][0],rhkl[isym][1],rhkl[isym][2]);
            cmplxType C = scitbx::constants::two_pi*cmplxType(0,1)*(dhkl*(UnitCell::Orth2Frac()*dTRA_by_du));
            FCderiv.dFC_by_dtra[p][isym][tra] = C*thisE;
            cmplxType C2 = fn::pow2(C);
            if (return_hessian)
            {
              FCderiv.d2FC_by_dtra2[p][isym][tra] = C2*thisE;
            }
          }
        }
      }
    }
    }
    floatType reflLL(0);
    cmplxType EM(EM_known[r]+EM_search[r]);
    floatType EM_real = EM.real();
    floatType EM_imag = EM.imag();
    floatType phasedEsqr = EM_real*EM_real+EM_imag*EM_imag;
    floatType sumEsqr = phasedEsqr + sum_Esqr_search[r];
    floatType maxEsqr = 0;//std::max(phasedEsqr,max_Esqr_search[r]); //max_Esqr = 0, Wilson approx

    //Now do likelihood calculation
    floatType Vterm = 0;
    Vterm = PTNCS.EPSFAC[r];
    Vterm -= totvar_known[r];
    Vterm -= totvar_search[r];
    Vterm += sumEsqr;
    Vterm -= maxEsqr;
    floatType V = Vterm;
    PHASER_ASSERT(V > 0);

    bool rcent = cent(r);
    floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
    floatType Esqr = fn::pow2(E);

    //floatType fc = std::sqrt(maxEsqr); //sqrt(FA*FA+FB*FB)
    floatType FCsqr = maxEsqr;
    floatType FC = std::sqrt(FCsqr);
    floatType X = 2.0*E*FC/V;
    reflLL = -(std::log(V)+(Esqr + FCsqr)/V);
    if (rcent)
    {
      X /= 2.0;
      reflLL = reflLL/2.0 + m_alogch.getalogch(X);
    }
    else
    {
      reflLL += m_alogchI0.getalogI0(X); //acentric
    }
    totalLL += reflLL;

    floatType bessTerm = rcent ? std::tanh(X) : scitbx::math::bessel::i1_over_i0(X);
    floatType bessTerm2 = rcent ? fn::pow2(1/std::cosh(X)) : fn::pow2(bessTerm);
    // NB: sech = 1/cosh
    floatType V2 = fn::pow2(V);
    floatType V3 = fn::pow3(V);
    floatType V4 = fn::pow4(V);
   // floatType fc2 = fn::pow2(fc);

    floatType dLL_by_dV(0);
    floatType d2LL_by_dV2(0);
    {
      dLL_by_dV = rcent ?
         (FCsqr - 2*bessTerm*FC*E + Esqr - V)/2/V2 :
         (1/V/V)*((Esqr + FCsqr - V) - 2*E*FC*bessTerm);

      if (!return_gradient && V3 > 0 && V4 > 0)
      {
        d2LL_by_dV2 = rcent ?
           (2*bessTerm2*FCsqr*Esqr + V*(-2*FCsqr + 4*bessTerm*FC*E - 2*Esqr + V))/(2*V4) :
           (4*Esqr*FCsqr/V4 - 2*(FCsqr+Esqr)/V3 + 1/V2 + 2*FC*E*bessTerm/V3 - 4*Esqr*FCsqr*bessTerm2/V4);
      }
    }

    for (int isym = 0; isym < NSYMP; isym++)
    if (!FCderiv.duplicate[isym])
    {
      floatType FA_s = std::real(FCderiv.Fcalc(isym));
      floatType FB_s = std::imag(FCderiv.Fcalc(isym));
      for (unsigned p = 0; p < GYRE.size(); p++)
      {
        if (!protocol.FIX_ROT)
        {
          for (int rot = 0; rot < 3; rot++)
          {
            floatType dFA_by_drot(FCderiv.dFC_by_drot[p][isym][rot].real());
            floatType dFB_by_drot(FCderiv.dFC_by_drot[p][isym][rot].imag());
            floatType dV_by_drot= 2*FA_s*dFA_by_drot+ 2*FB_s*dFB_by_drot;
            dLL_by_drot[p][rot] += dLL_by_dV*dV_by_drot; //over all reflections and isym and rot
            if (!return_gradient)
            {
              floatType d2FA_by_drot2(FCderiv.d2FC_by_drot2[p][isym][rot].real());
              floatType d2FB_by_drot2(FCderiv.d2FC_by_drot2[p][isym][rot].imag());
              floatType d2V_by_drot2= (2*fn::pow2(dFA_by_drot) + 2*fn::pow2(dFA_by_drot) +
                                            2*FA_s*d2FA_by_drot2+ 2*FB_s*d2FB_by_drot2);
              d2LL_by_drot2[p][rot] += d2LL_by_dV2*fn::pow2(dV_by_drot) + dLL_by_dV*d2V_by_drot2;
            }
          }
        }
        if (!protocol.FIX_TRA)
        {
          for (int tra = 0; tra < 3; tra++)
          {
            floatType dFA_by_dtra(FCderiv.dFC_by_dtra[p][isym][tra].real());
            floatType dFB_by_dtra(FCderiv.dFC_by_dtra[p][isym][tra].imag());
            floatType dV_by_dtra= 2*FA_s*dFA_by_dtra+ 2*FB_s*dFB_by_dtra;
            dLL_by_dtra[p][tra] += dLL_by_dV*dV_by_dtra; //over all reflections and isym and tra
            if (!return_gradient)
            {
              floatType d2FA_by_dtra2(FCderiv.d2FC_by_dtra2[p][isym][tra].real());
              floatType d2FB_by_dtra2(FCderiv.d2FC_by_dtra2[p][isym][tra].imag());
              floatType d2V_by_dtra2= (2*fn::pow2(dFA_by_dtra) + 2*fn::pow2(dFA_by_dtra) +
                                       2*FA_s*d2FA_by_dtra2+ 2*FB_s*d2FB_by_dtra2);
              d2LL_by_dtra2[p][tra] += d2LL_by_dV2*fn::pow2(dV_by_dtra) + dLL_by_dV*d2V_by_dtra2;
            }
          }
        }
      }
    }
    if (!protocol.FIX_VRMS)
    {
      for (unsigned p = 0; p < GYRE.size(); p++)
      {
        Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
        ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr); //necessary for dvrms_delta_const
        double Vconst = ens_modlid->dvrms_delta_const();
        double thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
               thisV *= ens_modlid->SCATTERING/TOTAL_SCAT;
        //     thisV *= exp(-2.0*models_known[s].BFAC*rssqr/4.0);
        if (PTNCS.use_and_present())
                  thisV *= G_DRMS[r]*G_Vterm[r];
        double totvar_known_s = thisV;
        double dV_by_dvrms = -2*Vconst*totvar_known_s; //EPSFAC - thisV term
        double d2V_by_dvrms2 = -2*Vconst*2*Vconst*totvar_known_s; //EPSFAC - thisV term
        for (int isym = 0; isym < NSYMP; isym++)
        if (!FCderiv.duplicate[isym])
        {
          double FA = std::real(FCderiv.FC[p][isym]);
          double FB = std::imag(FCderiv.FC[p][isym]);
          double FA_s = std::real(FCderiv.Fcalc(isym));
          double FB_s = std::imag(FCderiv.Fcalc(isym));
          dV_by_dvrms += 2*Vconst*FA*FA_s;//sumEsqr term
          dV_by_dvrms += 2*Vconst*FB*FB_s;//sumEsqr term
          if (!return_gradient)
          {
            d2V_by_dvrms2 += 2*Vconst*2*Vconst*FA*FA_s;//sumEsqr term
            d2V_by_dvrms2 += 2*Vconst*2*Vconst*FB*FB_s;//sumEsqr term
          }
        }
        dLL_by_dvrms[GYRE[p].MODLID] += dLL_by_dV*dV_by_dvrms;
        d2LL_by_dvrms2[GYRE[p].MODLID] += d2LL_by_dV2*fn::pow2(dV_by_dvrms) + dLL_by_dV*d2V_by_dvrms2;
      }
    }
  }

  floatType f = -(totalLL - LLwilson);
            f += restraint_term();

  int m(0),i(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Array[i++] = return_gradient ? -dLL_by_drot[p][rot] : -d2LL_by_drot2[p][rot];
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Array[i++] = return_gradient ? -dLL_by_dtra[p][tra] : -d2LL_by_dtra2[p][tra];
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) Array[i++] = return_gradient ? -dLL_by_dvrms[iter->first] : -d2LL_by_dvrms2[iter->first];
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  return f;
}

TNT::Vector<floatType> RefineGYRE::getLargeShifts()
{
  //relative sizes of these are important in finite diff grad calculation
  TNT::Vector<floatType> largeShifts(npars_ref);
  floatType dmin(HiRes());
  int m(0),i(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      floatType maxperp(0);
      if (rot == 0)
        maxperp = std::max(ens_modlid->B(),ens_modlid->C());
      else if (rot == 1)
        maxperp = std::max(ens_modlid->C(),ens_modlid->A());
      else
        maxperp = std::max(ens_modlid->A(),ens_modlid->B());
      largeShifts[i++] = 4*dmin/scitbx::deg_as_rad(maxperp);
    }
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) largeShifts[i++] = dmin/4.0;
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) largeShifts[i++] = dmin/6.0;//VRMS
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return largeShifts;
}

void  RefineGYRE::applyShift(TNT::Vector<floatType>& newx)
{
  int i(0),m(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
      if (refinePar[m++]) GYRE[p].perturbRot[rot] = newx[i++];
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) GYRE[p].perturbTrans[tra] = newx[i++];
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) models_drms[iter->first] = newx[i++]; //VRMS
  if (!protocol.FIX_ROT || !protocol.FIX_TRA)
  {
    calcRotRef();
  }
  if (!protocol.FIX_VRMS)
  {
    ensemble->setup_vrms(models_drms);
    calcRotRefVAR();
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
}

void RefineGYRE::logStats(outStream where,Output& output)
{
  size_t len(14);
  for (unsigned p = 0; p < GYRE.size(); p++)
    len = std::max(len,GYRE[p].MODLID.size());
  len = std::min(len,size_t(25));
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    std::string MODLID = GYRE[p].MODLID.substr(0,len);
    output.logTabPrintf(1,where,
       "%-*s EULER %+7.3f %+7.3f %+7.3f FRAC %6.3f %6.3f %6.3f \n",
        len,MODLID.c_str(),
        GYRE[p].euler_ang(0),GYRE[p].euler_ang(1),GYRE[p].euler_ang(2),
        GYRE[p].TRA[0],GYRE[p].TRA[1],GYRE[p].TRA[2]);
    output.logTabPrintf(2,where,
        "%-26s  %+5.3f %+5.3f %+5.3f (%5.3f degrees)\n",
        "xyz-angle perturbation:",
        GYRE[p].angle(0),GYRE[p].angle(1),GYRE[p].angle(2),
        GYRE[p].angle());
    output.logTabPrintf(2,where,
        "%-26s  %+5.3f %+5.3f %+5.3f (%5.3f Angstroms)\n",
        "xyz-distance perturbation:",
        GYRE[p].distance(0),GYRE[p].distance(1),GYRE[p].distance(2),
        GYRE[p].distance());
    output.logTab(2,where,"vrms delta factor: " + dtos(models_drms[GYRE[p].MODLID],6,4));
  }
  output.logBlank(where);
}

void RefineGYRE::logCurrent(outStream where,Output& output)
{
  output.logTab(1,VERBOSE,"Rotations and relative shifts (Degrees and Angstroms)");
  logStats(VERBOSE,output);
}

void RefineGYRE::logInitial(outStream where,Output& output)
{
  output.logTab(1,VERBOSE,"Initial Parameters:");
  logStats(VERBOSE,output);
}

void RefineGYRE::logFinal(outStream where,Output& output)
{
  output.logTab(1,VERBOSE,"Final Parameters:");
  logStats(VERBOSE,output);
}

std::string RefineGYRE::whatAmI(int& parameter)
{
  int i(0),m(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    std::string card = "Ensemble " + GYRE[p].MODLID;
    if (refinePar[m++]) if (i++ == parameter) return card + " RotX";
    if (refinePar[m++]) if (i++ == parameter) return card + " RotY";
    if (refinePar[m++]) if (i++ == parameter) return card + " RotZ";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraX";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraY";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraZ";
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) if (i++ == parameter) return "ensemble " + iter->first + " VRMS";
  PHASER_ASSERT(i == npars_ref);
  return "Undefined parameter";
}

bool1D RefineGYRE::getRefineMask(protocolPtr p)
{
  protocol.FIX_ROT = p->getFIX(macg_rot);
  protocol.FIX_TRA = p->getFIX(macg_tra);
  protocol.FIX_VRMS = p->getFIX(macg_vrms);
  protocol.ANCHOR = p->getFIX(macg_anch);
  use_rotref_restraint.resize(GYRE.size(),p->getNUM(macg_sigr) != 0);
  use_traref_restraint.resize(GYRE.size(),p->getNUM(macg_sigt) != 0);
  protocol.CHAIN_SIGR = p->getMAP(macg_sigr);
  protocol.CHAIN_SIGT = p->getMAP(macg_sigt);
  sigma_rotref.resize(GYRE.size());
  sigma_traref.resize(GYRE.size());
  for (unsigned g = 0; g < GYRE.size(); g++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[g].MODLID)->second;
    std::string CHAIN = ens_modlid->ENSEMBLE[0].CHAIN;
    CHAIN.erase(remove_if(CHAIN.begin(), CHAIN.end(), isspace), CHAIN.end());
    PHASER_ASSERT(protocol.CHAIN_SIGR.find(CHAIN) != protocol.CHAIN_SIGR.end());
    PHASER_ASSERT(protocol.CHAIN_SIGT.find(CHAIN) != protocol.CHAIN_SIGT.end());
    sigma_rotref[g] = protocol.CHAIN_SIGR[CHAIN];
    sigma_traref[g] = protocol.CHAIN_SIGT[CHAIN];
  }
  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
  double maxmw(0),maxp(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    double mw(ensemble->find(GYRE[p].MODLID)->second.MW.total_mw());
    if (mw > maxmw) { maxp = p; maxmw = mw;}
  }
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    std::string modlid = GYRE[p].MODLID;
    for (unsigned rot = 0; rot < 3; rot++)
      protocol.FIX_ROT ? refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    for (unsigned tra = 0; tra < 3; tra++)
     ((protocol.ANCHOR && p == maxp) || //fix heaviest molecule if anchor is true
       protocol.FIX_TRA) ? //fix all
       refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    (protocol.FIX_VRMS) ? //VRMS
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

TNT::Vector<floatType> RefineGYRE::getRefinePars()
{
  TNT::Vector<floatType> pars(npars_ref);
  int i(0),m(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) pars[i++] = GYRE[p].perturbRot[rot];
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) pars[i++] = GYRE[p].perturbTrans[tra];
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) pars[i++] = models_drms[iter->first]; //VRMS
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return pars;
}

std::vector<reparams> RefineGYRE::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) repar[i++].off();
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) repar[i++].off();
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (refinePar[m++]) repar[i++].off(); //VRMS
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return repar;
}

std::vector<bounds>  RefineGYRE::getUpperBounds()
{
  std::vector<bounds> Upper(npars_ref);
  int m(0),i(0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Upper[i++].off();
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Upper[i++].off();
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    Ensemble* ens_modlid = &ensemble->find(iter->first)->second;
    if (refinePar[m++]) Upper[i++].on(ens_modlid->DV.DRMS.upper); //VRMS
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

std::vector<bounds>  RefineGYRE::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  int m(0),i(0);
  //limit so that max factor from each term cannot take frac over 1
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Lower[i++].off();
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Lower[i++].off();
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    Ensemble* ens_modlid = &ensemble->find(iter->first)->second;
    if (refinePar[m++]) Lower[i++].on(ens_modlid->DV.DRMS.lower); //VRMS
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

void RefineGYRE::cleanUp(outStream where,Output& output)
{
  dmat33 ROT,PRtr,Rperturb;
  dvect3 TRA,CMori,CMrot,CMperturb;
  dvect3 zero3(0,0,0);
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
    //recover original centre of mass, rotated centre of mass
    PRtr = ens_modlid->PR.transpose();
    CMori = -PRtr*ens_modlid->PT;
    ROT = GYRE[p].R;
    CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform orientation
    Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,GYRE[p].perturbRot[dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    GYRE[p].R = ROT;

    CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented ensemble)
    //ROT = ROT*PRtr;
    TRA = GYRE[p].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation
    iOrthT += GYRE[p].perturbTrans;
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //keep coordinates in same unit cell
    dvect3 deltaTRA = UnitCell::doOrth2Frac(iOrthT) - TRA;
    for (unsigned tra = 0; tra < 3; tra++)
    {
      while (deltaTRA[tra] <= -1) deltaTRA[tra]++;
      while (deltaTRA[tra] >  +1) deltaTRA[tra]--;
    }
    TRA += deltaTRA;
    GYRE[p].TRA = TRA;
  }
  //Recover original centre of mass, difference in rotated centres of mass
  //output.logTab(1,where,"Reset rotation and translation perturbations to zero, update solutions");
  //output.logBlank(where);
}

void RefineGYRE::setUp()
{
  //for numerical stability
  TNT::Vector<floatType> old_x =  getRefinePars();
  applyShift(old_x); //calling search setup fast_last_applyShift internally
#if 1
  for (unsigned p = 0; p < GYRE.size(); p++)
  { //instead of in cleanUp
    GYRE[p].perturbRot = dvect3(0,0,0);
    GYRE[p].perturbTrans = dvect3(0,0,0);
  }
#endif
}

void RefineGYRE::calcRotRef()
{
  dmat33 ROT,PRtr,Rperturb;
  dvect3 CMori,CMrot,CMperturb;
  //Use Wilson target
  dmat331D Q1tr(GYRE.size());
  dvect31D TRA(GYRE.size());
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
    //recover original centre of mass, rotated centre of mass
    dmat33 ROT = GYRE[p].R;
    PRtr = ens_modlid->PR.transpose();
    CMori = -PRtr*ens_modlid->PT;
    CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,GYRE[p].perturbRot[dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented ensemble)
    ROT = ROT*PRtr;

    TRA[p] = GYRE[p].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA[p]);
    //apply xyz translation perturbation (for refinement)
    iOrthT += GYRE[p].perturbTrans;
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of (*ensemble))
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA[p] = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    Q1tr[p] = Q1.transpose();
  }

  sum_Esqr_search.resize(NREFL);
  for (int r = 0; r < NREFL; r++)
  {
   // max_Esqr_search[r] = 0;//Wilson approximation
    sum_Esqr_search[r] = 0;//init
  }

  const cmplxType TWOPII(0.,scitbx::constants::two_pi);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
   // floatType repsn = epsn(r);
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    floatType sqrt_repsn = sqrt_epsn[r];
    std::vector<miller::index<int> > rhkl = rotMiller(r);
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      if (!duplicate(isym,rhkl))
      {
        cmplxType totalE(0,0);
        for (unsigned p = 0; p < GYRE.size(); p++) //fixed phase relative to each other
        {
          Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
          ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
          dvect3    RotSymHKL = Q1tr[p]*rhkl[isym];
          cmplxType thisE =  ens_modlid->InterpE(RotSymHKL,DFAC[r]);
                    thisE *= std::sqrt(ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
                    thisE *= sqrt_repsn;
                    thisE *= dphi(r,isym,rhkl[isym],TRA[p]);
                    //no b-factor
          totalE += thisE;
          //no G function correction (gfun), models in separate (real) rotations
        }
        sum_Esqr_search[r] += std::norm(totalE);
      }
    }
  }
}

void RefineGYRE::calcRotRefVAR()
{
  for (unsigned r = 0; r < NREFL; r++)
    totvar_search[r] = 0;
  for (unsigned p = 0; p < GYRE.size(); p++)
  {
    Ensemble* ens_modlid = &ensemble->find(GYRE[p].MODLID)->second;
    floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT;
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r])); // D^2 for ensemble
                thisV *= scatFactor;                      // becomes sigmaA^2
                //search does not have MULT factor
      if (PTNCS.use_and_present())
      {
        // halfR=true (look for average orientation of tNCS pair)
        // when called from FRF or BRF, or from BTF or FTF before rotref,
        // false otherwise (BTF or FTF after rotref, RNP)
        if (PTNCS.NMOL > 2)
          thisV *= PTNCS.NMOL*G_Vterm[r];
        else //halfR
        {
          gfun.calcReflTerms(r);
          thisV *= PTNCS.NMOL*gfun.refl_Gsqr_Vterm;
        }
        thisV *= G_DRMS[r];
      }
      totvar_search[r] += thisV;
    }
  }
}

}//phaser
