//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __BrickClass__
#define __BrickClass__
#include <phaser/main/Phaser.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

class Brick
{
  private:
    dvect3 fmin,fmax;
    dvect3 omin,omax;

  public:
    Brick() { fmin = fmax = omin = omax = dvect3(0,0,0); }
    Brick(bool,dvect3,dvect3,UnitCell&);

    dvect3 orthMin();
    dvect3 orthMax();
    dvect3 fracMin();
    dvect3 fracMax();
    floatType orthMin(int);
    floatType orthMax(int);

    bool isPlaneX(floatType);
    bool isPlaneY(floatType);
    bool isPlaneZ(floatType);
    bool isLineX(floatType);
    bool isLineY(floatType);
    bool isLineZ(floatType);
    bool isPoint(floatType);
};

} //phaser

#endif
