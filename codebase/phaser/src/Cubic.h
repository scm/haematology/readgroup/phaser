//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CubicClass__
#define __CubicClass__
#include <phaser/main/Phaser.h>

namespace phaser {

class Cubic
{
  private:
    bool IS_VALID,IS_DEFAULT;
    dvect3 CUBIC;

  public:
    Cubic();
    Cubic(double,double,double);

    double FnSShell(double,double,int);
    double InvFnS(double);
    double FnS(double);

    bool is_valid() { return IS_VALID; }
    bool is_default() { return IS_DEFAULT; }
    double a() { return CUBIC[0]; }
    double b() { return CUBIC[1]; }
    double c() { return CUBIC[2]; }
};

}
#endif
