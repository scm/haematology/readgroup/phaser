//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/DataSAD.h>
#include <phaser/src/Molecule.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/round.h>
#include <phaser/lib/solTerm.h>
#include <phaser/include/data_solpar.h>
#include <cctbx/miller/asu.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <limits>

namespace phaser {

DataSAD::DataSAD() : UnitCell(), HandEP() { } //default constructor

//C++ constructor
DataSAD::DataSAD(SpaceGroup SG_HALL,
                 UnitCell UNIT_CELL,
                 af::shared<miller::index<int> > MILLER,
                 data_spots POS_,
                 data_spots NEG_,
                 data_tncs PTNCS_,
                 data_bins DATA_BINS,
                 data_outl OUTLIER_,
                 data_part PARTIAL_,
                 std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES,
                 Output& output
) :  UnitCell(UNIT_CELL)
{
  if (DATA_BINS.not_setup()) DATA_BINS.set_default_data();
#if defined CCTBX_MATH_COS_SIN_TABLE_H
  fast_cos_sin = cctbx::math::cos_sin_lin_interp_table<floatType>(4000);
#endif
  setSpaceGroup(SG_HALL);
  OUTLIER = OUTLIER_;
  PARTIAL = PARTIAL_;
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  //reference counted arrays
  //WARNING changes the input arrays
  miller = MILLER;
  NREFL = MILLER.size();
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
  if (!miller::is_unique_set_under_symmetry(SgInfo,false,miller.const_ref()))
  throw PhaserError(FATAL,"Reflections are not a unique set by symmetry");
  POS = POS_;
  NEG = NEG_;
  cctbx::sgtbx::reciprocal_space::asu asu(SgInfo);
  cctbx::sgtbx::space_group const& sg = SgInfo.group();
  for (int r = 0; r < NREFL; r++)
  {
    cctbx::miller::asym_index ai(sg, asu, miller[r]);
    cctbx::miller::index_table_layout_adaptor ila = ai.one_column(false);
    miller[r] = ila.h();
    POS.map_to_asu(r,ila);
    NEG.map_to_asu(r,ila);
  }
  POS.INPUT_INTENSITIES = POS.I.size();
  NEG.INPUT_INTENSITIES = NEG.I.size();
  POS.set_default_dfactor(NREFL);
  NEG.set_default_dfactor(NREFL);
  cent.resize(NREFL);
  both.resize(NREFL);
  plus.resize(NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  {
    cent[r] = cctbxSG.is_centric(miller[r]);
    both[r] = !cent[r] && POS_.PRESENT[r] && NEG_.PRESENT[r];
    plus[r] = !cent[r] && POS_.PRESENT[r] && !NEG_.PRESENT[r];
  }
  //correct badly flagged reflections i.e. reflections that do not have the PRESENT
  //flag set correctly, but can be rescued by changing from both to singleton
  for (unsigned r = 0; r < NREFL; r++)
  if (both[r] && (POS.F[r] == 0 || NEG.F[r] == 0))
  {
    if (POS.F[r] != 0 && NEG.F[r] == 0) { both[r] = false; plus[r] = true; }
    if (POS.F[r] == 0 && NEG.F[r] != 0) { both[r] = false; plus[r] = false; }
    //leave the (POS.F[r] == 0 && NEG.F[r] == 0) case for deletion below
  }

  //delete F=0 reflections
  int nerror(0),nerrorf(0),nerrorsigf(0),max_print(20);
  outStream where(VERBOSE);
  selected.resize(NREFL);
  bad_data.resize(NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  {
     selected[r] = true;
     bad_data[r] = false;
  }
  for (unsigned r = 0; r < NREFL; r++)
  if ((both[r] && (POS.F[r] == 0 && NEG.F[r] == 0)) ||
      (cent[r] && POS.F[r] == 0) ||
      (!cent[r] && !both[r] && plus[r] && POS.F[r] == 0) ||
      (!cent[r] && !both[r] && !plus[r] && NEG.F[r] == 0) ||
      (!POS_.PRESENT[r] && !NEG_.PRESENT[r])
      )
  {
    bad_data[r] = true;
    selected[r] = false;
    if (nerror == 0)
    {
      output.logUnderLine(where,"BAD DATA");
      output.logTab(1,where,"Data includes F's that are zero. These data are deleted.");
    }
    if (nerror < max_print)
    {
      if (both[r] && (POS.F[r] == 0 && NEG.F[r] == 0))
        output.logTabPrintf(1,where,"%4i %4i %4i  %s\n",miller[r][0],miller[r][1],miller[r][2],"acentric POS.F=0 & NEG.F=0");
      else if (cent[r] && POS.F[r] == 0)
        output.logTabPrintf(1,where,"%4i %4i %4i  %s\n",miller[r][0],miller[r][1],miller[r][2],"centric FO=0");
      else if (!cent[r] && !both[r] && plus[r] && POS.F[r] == 0)
        output.logTabPrintf(1,where,"%4i %4i %4i  %s\n",miller[r][0],miller[r][1],miller[r][2],"singleton POS.F=0");
      else if (!cent[r] && !both[r] && !plus[r] && NEG.F[r] == 0)
        output.logTabPrintf(1,where,"%4i %4i %4i  %s\n",miller[r][0],miller[r][1],miller[r][2],"singleton NEG.F=0");
    }
    nerror++;
  }
  if (nerror >= max_print) output.logTab(1,where,"etc... ");
  output.logTab(1,where,itos (nerror) + " problem but correctable reflections out of " + itos(NREFL));

  nerror = 0;
  for (unsigned r = 0; r < NREFL; r++)
  if (cctbxSG.is_sys_absent(miller[r]))
  {
    bad_data[r] = true;
    selected[r] = false;
    if (nerror == 0)
    {
      output.logUnderLine(where,"SYSTEMATIC ABSENCES");
      output.logTab(1,where,"Data includes F's that are systematically absent. These data are deleted.");
    }
    if (nerror < max_print)
      output.logTabPrintf(1,where,"%4i %4i %4i  %s\n",miller[r][0],miller[r][1],miller[r][2],"systematically absent");
    nerror++;
  }
  if (nerror >= max_print) output.logTab(1,where,"etc... ");
  output.logTab(1,where,itos (nerror) + " systematically absent reflections out of " + itos(NREFL));

  nerror = 0;
  for (int r = 0; r < NREFL; r++)
    if (bad_data[r])
      nerror++;
  output.logTab(1,where,itos (nerror) + " unique problem reflections deleted leaving " + itos(NREFL-nerror));

  if (NREFL == nerror) throw PhaserError(FATAL,"There are no reflections");

  nerror = 0;
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    bool fpos_zero((both[r] || plus[r] || cent[r]) && POS.F[r] < 0);
    bool sigfpos_zero((both[r] || plus[r] || cent[r]) && POS.SIGF[r] == 0);
    bool fneg_zero((both[r] || !plus[r]) && !cent[r] && NEG.F[r] < 0);
    bool sigfneg_zero((both[r] || !plus[r]) && !cent[r] && NEG.SIGF[r] == 0);
    if (fpos_zero || fneg_zero || sigfpos_zero || sigfneg_zero)
    {
      if (nerrorf == 0 && (fpos_zero || fneg_zero))
      {
        output.logTab(1,where,"Data includes F's that are negative");
        output.logTabPrintf(1,where,"%-10s%4s%4s%4s  %4s %7s %4s %7s\n","#","H","K","L","F(+)","SIGF(+)","F(-)","SIGF(-)");
      }
      if (nerrorsigf == 0 && (sigfpos_zero || sigfneg_zero))
      {
        output.logTab(1,where,"Data includes SIGF's that are zero");
        output.logTabPrintf(1,where,"%-10s%4s%4s%4s  %4s %7s %4s %7s\n","#","H","K","L","F(+)","SIGF(+)","F(-)","SIGF(-)");
      }
      if (fpos_zero || fneg_zero) nerrorf++;
      if (sigfpos_zero || sigfneg_zero) nerrorsigf++;
      if (nerror < max_print)
      {
        output.logTabPrintf(1,where,"%-10d%4d%4d%4d  %4s %7s %4s %7s\n",
                            r,miller[r][0],miller[r][1],miller[r][2],
                            fpos_zero ? "<0" : "OK",sigfpos_zero ? "=0" : "OK",
                            fneg_zero ? "<0" : "OK",sigfneg_zero ? "=0" : "OK");
        output.logTabPrintf(1,DEBUG,"%s %s %s %5.4e %5.4e %5.4e %5.4e\n",
                            btos(cent[r]).c_str(),btos(both[r]).c_str(),btos(plus[r]).c_str(),
                            POS.F[r],POS.SIGF[r],NEG.F[r],NEG.SIGF[r]);
      }
      nerror++;
    }
  }
  if (nerror >= max_print) output.logTab(1,where,"etc... ");
  if (nerrorsigf) output.logTab(1,where,itos (nerror) + " reflections with SIGF = 0 out of " + itos(NREFL));
  if (nerrorf) throw PhaserError(FATAL,itos (nerror) + " reflections with F < 0 out of " + itos(NREFL));
  //if (nerrorsigf) throw PhaserError(FATAL,itos (nerror) + " reflections with SIGF = 0 out of " + itos(NREFL));

  // --- Binning ---
  bin = Bin(DATA_BINS);
  bin.setup(HiRes(),LoRes(),MILLER.size());

  //This checks to see if any of the bins have no reflections
  //and makes sure there is at least one in every bin
  //This will override the NUM input option
  int1D NumInBin;
  int startbins(bin.numbins());
  recount:
  NumInBin.resize(bin.numbins());
  if (bin.numbins() != 1) //if == 1 then do nothing - assume 1 reflection!
  {
    //count numbers in each bin
    for (unsigned s = 0; s < bin.numbins(); s++) NumInBin[s] =  0;
    for (unsigned r = 0; r < NREFL; r++)
      if (!bad_data[r])
        NumInBin[bin.get_bin(S(miller[r]))]++;
    for (unsigned s = 0; s < bin.numbins(); s++)
      if (NumInBin[s] == 0)
      {
        //reduce the number of bins by 1 and see if this helps
        int reduceBins = bin.numbins()-1;
        bin.set_numbins(reduceBins);
        goto recount;
      }
  }
  if (bin.numbins() < startbins)
  {
    output.logWarning(SUMMARY,"Resolution gaps in data");
    output.logWarning(SUMMARY,"# of bins decreased from " + itos(startbins)
        + " to " + itos(bin.numbins()) + " to avoid empty bins");
  }

  // --- store reflection arrays
  for (unsigned r = 0; r < NREFL; r++)
  {
    ssqr.push_back(UnitCell::Ssqr(miller[r]));
    rbin.push_back(bin.get_bin(S(miller[r])));
  }
  calcRotMiller();
  calcTraMiller();
  phsr.resize(NREFL);
  epsn.resize(NREFL);
  PTNCS = PTNCS_;
  if (!PTNCS.EPSFAC.size()) PTNCS.EPSFAC.resize(NREFL,1);
  PHASER_ASSERT(PTNCS.EPSFAC.size() == NREFL);
  bool deg(false);
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (!PTNCS.use_and_present()) PTNCS.EPSFAC[r] = 1;
    epsn[r] = PTNCS.EPSFAC[r]*cctbxSG.epsilon(MILLER[r]);
    phsr[r] = cctbxSG.phase_restriction(miller[r]).nearest_valid_phase(0,deg);
  }

  //store the SN parameters for the outlier rejection and the large shifts
  SN_bin = float1D(bin.numbins(),0);
  OBSVAR_bin = float1D(bin.numbins(),0);
  DANOVAR_bin = float1D(bin.numbins(),0);
  {
    float1D SumWeight(bin.numbins(),0);
    int1D BothInBin(bin.numbins(),0);
    for (unsigned s = 0; s < bin.numbins(); s++) NumInBin[s] =  0;
    for (unsigned r = 0; r < NREFL; r++)
    if (!bad_data[r])
    {
      unsigned s = rbin[r]; // alias
      floatType weight = cent[r] ? 1 : 2;
      floatType epsilon = cctbxSG.epsilon(MILLER[r]); // Use symmetry not tNCS value here because of truncate artefacts
      SumWeight[s] += weight;
      NumInBin[s]++;
      if (cent[r] || plus[r])
      {
        SN_bin[s] += weight*fn::pow2(POS.F[r])/epsilon;
        double SigmaFactor = cent[r] ? 0.5: 1;
        OBSVAR_bin[s] += SigmaFactor*fn::pow2(POS.SIGF[r]); //stored in pos AND neg
      }
      else if (both[r])
      {
        SN_bin[s] += weight*fn::pow2((POS.F[r]+NEG.F[r])/2)/epsilon;
        OBSVAR_bin[s] += 2*(fn::pow2(POS.SIGF[r])+fn::pow2(NEG.SIGF[r]));
        DANOVAR_bin[s] += fn::pow2(POS.F[r]-NEG.F[r]);
        BothInBin[s]++;
      }
      else if (!plus[r])
      {
        SN_bin[s] += weight*fn::pow2(NEG.F[r])/epsilon;
        OBSVAR_bin[s] += fn::pow2(NEG.SIGF[r]);
      }
    }
    for (unsigned s = 0; s < bin.numbins(); s++)
    {
      if (SumWeight[s] != 0) SN_bin[s] /= SumWeight[s];
      if (NumInBin[s] != 0) OBSVAR_bin[s] /= NumInBin[s];
      if (BothInBin[s] != 0) DANOVAR_bin[s] /= BothInBin[s];
    }
  }
  calcAnomPartialStructure(ATOMTYPES,output);
}

void DataSAD::calcRotMiller()
{
  rotMiller.resize(miller.size());
  for (unsigned r = 0; r < miller.size(); r++)
  {
    rotMiller[r].resize(SpaceGroup::NSYMP);
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      rotMiller[r][isym] = SpaceGroup::Rotsym(isym)*miller[r];
  }
}

void DataSAD::calcTraMiller()
{
  traMiller.resize(miller.size());
  for (unsigned r = 0; r < miller.size(); r++)
  {
    traMiller[r].resize(SpaceGroup::NSYMP);
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      traMiller[r][isym] = SpaceGroup::Trasym(isym)*miller[r];
  }
}

double DataSAD::anisoTerm(unsigned& r,dmat6& anisoBeta)
{
  floatType AnisoExp(0);
  AnisoExp -= anisoBeta[0]*miller[r][0]*miller[r][0];
  AnisoExp -= anisoBeta[1]*miller[r][1]*miller[r][1];
  AnisoExp -= anisoBeta[2]*miller[r][2]*miller[r][2];
  AnisoExp -= anisoBeta[3]*miller[r][0]*miller[r][1];
  AnisoExp -= anisoBeta[4]*miller[r][0]*miller[r][2];
  AnisoExp -= anisoBeta[5]*miller[r][1]*miller[r][2];
  return  floatType(std::exp(AnisoExp));
}

floatType DataSAD::HiRes()
{
  floatType hires(DEF_LORES);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
    hires = std::min(hires,UnitCell::reso(miller[r]));
  return hires;
}

floatType DataSAD::maxSsqr()
{
  floatType maxSsqr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
    maxSsqr = std::max(maxSsqr,UnitCell::Ssqr(miller[r]));
  return maxSsqr;
}

floatType DataSAD::LoRes()
{
  floatType lores(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
    lores = std::max(lores,UnitCell::reso(miller[r]));
  return lores;
}

void DataSAD::calcAnomPartialStructure(std::map<std::string,cctbx::eltbx::fp_fdp>& ATOMTYPES,Output& output)
{
  if (!PARTIAL.ANOMALOUS) return; //use RefineSADf calcPartialStructure
  if (PARTIAL.filename() == "") output.logTab(1,DEBUG,"No partial structure");
  if (PARTIAL.filename() == "") return;
  if (!PARTIAL.is_string())
    output.logUnderLine(LOGFILE,"Partial Structure: " + PARTIAL.basename());
  FPpos.resize(NREFL,0);
  FPneg.resize(NREFL,0);
  if (!PARTIAL.map_format())
  {
    Molecule pdb(PARTIAL,true);
    if (pdb.read_errors() != "") throw PhaserError(FATAL,pdb.read_errors());

    cctbx::af::shared<cctbx::xray::scatterer<double> > partial_atoms;
    cctbx::sgtbx::site_symmetry_table partial_sym_table;
    //now make the partial structure
    for (int p = 0; p < pdb.nAtoms(); p++)
    {
      dvect3 orthXyz = pdb.card(p).X;
      dvect3 fracXyz = UnitCell::doOrth2Frac(orthXyz); // site
      cctbx::xray::scatterer<> new_atom(
          "partial structure", // label
          fracXyz, // site
          cctbx::adptbx::b_as_u(pdb.card(p).B), // u_iso
          pdb.card(p).O, // occupancy
          cctbx::eltbx::xray_scattering::wk1995(pdb.card(p).Element).label(), // scattering_type
            0, // fp
            0); // fdp
      partial_atoms.push_back(new_atom);
      partial_sym_table.process(new_atom.apply_symmetry(getCctbxUC(),getCctbxSG()));
    }

    if (!partial_atoms.size())
    throw PhaserError(FATAL,"Partial structure file does not have any atoms");
    int natoms = pdb.nAtoms();
    output.logTab(1,LOGFILE,itos(natoms) + " atom" + std::string(natoms==1?"":"s") + " in partial structure");

    //initialize
    for (unsigned r = 0; r < NREFL; r++) FPpos[r] = FPneg[r] = 0;

    float1D linearTerm(partial_atoms.size()),debye_waller_u_iso(partial_atoms.size(),0.);
    for (unsigned a = 0; a < partial_atoms.size(); a++)
    {
      floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
      int M = SpaceGroup::NSYMM/partial_sym_table.get(a).multiplicity();
      linearTerm[a] = symFacPCIF*partial_atoms[a].occupancy/M;
      if (!partial_atoms[a].flags.use_u_aniso_only())
        debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(partial_atoms[a].u_iso) * 0.25;
    }

    //sf calculation
    output.logEllipsisStart(LOGFILE,"Start structure factor calculation for partial structure");
    for (unsigned r = 0; r < NREFL; r++)
    {
      for (unsigned a = 0; a < partial_atoms.size(); a++)
      {
        floatType isoB = partial_atoms[a].flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
        floatType symmOccBfac(linearTerm[a]*isoB);
        cctbx::eltbx::xray_scattering::wk1995  ff(partial_atoms[a].scattering_type);
        double fp = (ATOMTYPES.find(partial_atoms[a].scattering_type) == ATOMTYPES.end()) ?
                  0 : ATOMTYPES[partial_atoms[a].scattering_type].fp();
        double fo_plus_fp = ff.fetch().at_d_star_sq(ssqr[r]) + fp;
        double fdp = (ATOMTYPES.find(partial_atoms[a].scattering_type) == ATOMTYPES.end()) ?
                  0 : ATOMTYPES[partial_atoms[a].scattering_type].fdp();
               fo_plus_fp *= symmOccBfac;
               fdp *= symmOccBfac;
        for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
        {
          if (partial_atoms[a].flags.use_u_aniso_only())
          {
            double anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],partial_atoms[a].u_star);
            fo_plus_fp *= anisoB;
            fdp *= anisoB;
          }
          floatType theta = rotMiller[r][isym][0]*partial_atoms[a].site[0] +
                            rotMiller[r][isym][1]*partial_atoms[a].site[1] +
                            rotMiller[r][isym][2]*partial_atoms[a].site[2] +
                            traMiller[r][isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
          cmplxType sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
          floatType costheta = std::real(sincostheta);
          floatType sintheta = std::imag(sincostheta);
#else
          theta *= scitbx::constants::two_pi;
          floatType costheta = std::cos(theta);
          floatType sintheta = std::sin(theta);
#endif
          FPpos[r] += cmplxType(fo_plus_fp*costheta - fdp*sintheta, fo_plus_fp*sintheta + fdp*costheta);
          //store the complex conjugate of FH-
          FPneg[r] += cmplxType(fo_plus_fp*costheta + fdp*sintheta, fo_plus_fp*sintheta - fdp*costheta);
        }//end sym
      } //end  loop over partial atoms
    } //end loop r
    output.logEllipsisEnd(LOGFILE);
  }

  output.logEllipsisStart(DEBUG,"Calculate weights");
  data_solpar SOLPAR;
         SOLPAR.SIGA_FSOL = 0.725;
         SOLPAR.SIGA_BSOL = 204.4;
  PHASER_ASSERT(FPpos.size() == NREFL);
  PHASER_ASSERT(FPneg.size() == NREFL);
  PHASER_ASSERT(PARTIAL.is_rms()); //map
  for (unsigned r = 0; r < miller.size(); r++)
  {
    floatType ssqr = UnitCell::Ssqr(miller[r]);
    floatType DLuz = solTerm(ssqr,SOLPAR)*DLuzzati(ssqr,PARTIAL.RMSID);
    FPpos[r] *= DLuz;
    FPneg[r] *= DLuz;
  }
  output.logEllipsisEnd(DEBUG);
} // end calcPartialStructure

} //phaser
