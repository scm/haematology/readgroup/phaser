//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineGYREClass__
#define __RefineGYREClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolGYRE.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/src/DataMR.h>
#include <phaser/src/RefineBase2.h>
#include <phaser/pod/rotderivative.h>

namespace phaser {

class RefineGYRE : public RefineBase2, public DataMR
{
  private:
    float1D      sigma_rotref,sigma_traref;
    bool1D       use_rotref_restraint,use_traref_restraint;
    ProtocolGYRE protocol;

  public:
    std::vector<mr_gyre> GYRE;

    RefineGYRE()
    {
    }
    RefineGYRE(DataMR&);
    ~RefineGYRE() {}

   //concrete member functions
    floatType likelihoodFnWilson() { return Wilson()-LLwilson; }
    floatType targetFn();
    floatType Wilson();
    floatType restraint_term();
    floatType gradient_hessian(TNT::Vector<floatType>&,bool);
    floatType gradientFn(TNT::Vector<floatType>&);
    floatType hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    void   applyShift(TNT::Vector<floatType>&);
    std::vector<reparams>  getRepar();
    std::vector<bounds>  getUpperBounds();
    std::vector<bounds>  getLowerBounds();
    void   logCurrent(outStream,Output&);
    void   logInitial(outStream,Output&);
    void   logFinal(outStream,Output&);
    void   logStats(outStream,Output&);
    void   cleanUp(outStream,Output&);
    void   setUp();

    TNT::Vector<floatType>      getRefinePars();
    TNT::Vector<floatType>      getLargeShifts();
    bool1D       getRefineMask(protocolPtr);
    std::string  whatAmI(int&);

    void      calcRotRef();
    void      calcRotRefVAR();
    map_str_float    getDRMS() { return models_drms; }
};

} //phaser

#endif
