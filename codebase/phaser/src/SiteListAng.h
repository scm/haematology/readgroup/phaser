//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SiteListAngClass__
#define __SiteListAngClass__
#include <phaser/src/SiteList.h>
#include <phaser/src/Sites.h>
#include <phaser/io/Output.h>
#include <phaser/src/Ensemble.h>
#include <phaser/lib/four_point_interp.h>
#include <cctbx/maptbx/structure_factors.h>
#include <phaser/lib/sphericalY.h>
#include <phaser/lib/rotationgroup.h>
#include <boost/none.hpp>

namespace phaser {

class SiteListAng : public SiteList
{
  private:
    std::vector<angsite> siteList;
    dmat33 principal_axes_;
    dvect3 extents_;
    floatType CLUSTER_ANG;

  public:
    SiteListAng(SpaceGroup& sg_,UnitCell& uc_, dmat33 principalR, dvect3 extents)
        : SiteList(sg_,uc_), principal_axes_(principalR), extents_(extents) { CLUSTER_ANG = 0; siteList.clear(); }
    angsite&  operator()(int const& i) { return siteList[i]; }
    void set_cluster_ang(floatType c)  { CLUSTER_ANG = c; }

    ~SiteListAng() {}

    floatType getRmax();
    floatType getRmin();
    floatType getRmean();
    floatType getRsigma();
    void push_back(angsite&);
    void Resize(unsigned);
    void Reserve(unsigned);
    void Sort();
    void truncate(unsigned);

   //concrete
    bool      isTopSite(unsigned);
    bool      isDeep(unsigned);
    bool      isSelected(unsigned);
    float1D   getValues();
    int1D     getClusters();
    float1D   getSignals();
    bool1D    getDeep();
    floatType getValue(unsigned);
    dvect31D  getEuler();
    unsigned  getNum(unsigned);
    floatType getSignal(unsigned);
    void      setNum(unsigned,unsigned);
    void      setValue(unsigned,floatType);
    void      setTopSite(unsigned,bool);
    void      setDeep(unsigned,bool);
    void      setSelected(unsigned,bool);
    void      archiveValue(unsigned);
    unsigned  Size();
    void      Erase(int,int);
    void      Copy(int,int);
    unsigned  numSelected();

    void setupRegion(floatType,Output&);
    void setupAroundPoint(dvect3,floatType,floatType);
    void setupAllAngles(floatType);

    void moveToRotationalASU();
    void flagClusteredTop(int);
    void flagClusteredTopNew(int);
    void logRawTop(outStream,Output&);
    void graphRawTop(outStream,Output&);
    void logClusteredTop(outStream,Output&);
    void graphClusteredTop(outStream,Output&);
    void logRawTopRescored(outStream,Output&);
    void graphRawTopRescored(outStream,Output&);
    void logClusteredTopRescored(outStream,Output&);
    void graphClusteredTopRescored(outStream,Output&);
    void get_FRF(cmplx3D&,Output&,floatType);

private:
    scitbx::fftpack::real_to_complex_3d<floatType> DoRfftStuff(cmplx3D&,floatType,int,cctbx::maptbx::structure_factors::to_map<floatType>&,lnfactorial&);
    int1D allocate_memory(int,floatType,Output&);
};

} //end namespace phaser

#endif
