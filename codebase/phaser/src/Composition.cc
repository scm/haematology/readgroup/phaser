//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Composition.h>
#include <phaser/lib/round.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/fasta2str.h>
#include <cctbx/uctbx.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>

namespace phaser {

double Composition::total_scat(std::string SG_HALL,af::double6 UNIT_CELL,std::string TYPE,double HIRES)
{
  PHASER_ASSERT(TYPE == "" || TYPE == "PROTEIN" || TYPE == "NUCLEIC");
  int NSYMM = cctbx::sgtbx::space_group(SG_HALL,"A1983").order_z();
  double volume = cctbx::uctbx::unit_cell(UNIT_CELL).volume();
  double TOTAL_SCAT = 0;
  if (BY == "AVERAGE" && (!TYPE.size() || TYPE == "PROTEIN"))
  {
    double meanDa(volume/NSYMM/matthewsVM(HIRES,"PROTEIN")); //composition is per asu
    scattering_protein protein;
    TOTAL_SCAT = protein.ScatMw(meanDa).scat;
  }
  else if (BY == "SOLVENT" && (!TYPE.size() || TYPE == "PROTEIN"))
  {
    double thisVM = 1.23/(1-PERCENTAGE);
    double thisDa(volume/NSYMM/thisVM); //composition is per asu
    scattering_protein protein;
    TOTAL_SCAT = protein.ScatMw(thisDa).scat;
  }
  else if (BY == "ASU")
  {
    for (int c = 0; c < ASU.size(); c++)
      if (!TYPE.size() || ASU[c].is_type(TYPE))
        TOTAL_SCAT += ASU[c].scattering();
  }
  return TOTAL_SCAT*INCREASE;
}

bool Composition::is_all_protein()
{
  if (BY == "AVERAGE" || BY == "SOLVENT" )
    return true;
  for (int c = 0; c < ASU.size(); c++)
    if (!ASU[c].is_type("PROTEIN"))
      return false;
   return true;
}

bool Composition::is_all_nucleic()
{
  if (BY == "AVERAGE" || BY == "SOLVENT" )
    return false;
  for (int c = 0; c < ASU.size(); c++)
    if (!ASU[c].is_type("NUCLIEC"))
      return false;
   return true;
}

double Composition::total_mw(std::string SG_HALL,af::double6 UNIT_CELL,std::string TYPE,double HIRES)
{
  int NSYMM = cctbx::sgtbx::space_group(SG_HALL,"A1983").order_z();
  double volume = cctbx::uctbx::unit_cell(UNIT_CELL).volume();
  double MW = 0;
  if (BY == "AVERAGE" && (!TYPE.size() || TYPE == "PROTEIN"))
  {
    MW = (volume/NSYMM/matthewsVM(HIRES,"PROTEIN")); //composition is per asu
  }
  else if (BY == "SOLVENT" && (!TYPE.size() || TYPE == "PROTEIN"))
  {
    double thisVM = 1.23/(1-PERCENTAGE);
    MW = (volume/NSYMM/thisVM); //composition is per asu
  }
  else if (BY == "ASU")
  {
    for (int c = 0; c < ASU.size(); c++)
      if (!TYPE.size() || ASU[c].is_type(TYPE))
        MW += ASU[c].mw();
  }
  return MW*INCREASE;
}

void Composition::set_modlid_scattering(map_str_pdb& PDB)
{
  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
  {
    std::string m = iter->first;
    model_scattering[m] = PDB[m].SCATTERING;
  }
}

void Composition::increase_total_scat_if_necessary(std::string SG_HALL,af::double6 UNIT_CELL,mr_solution MRSET,data_tncs PTNCS,data_bofac SEARCH_FACTORS,search_array AUTO_SEARCH)
{
  double TOTAL_SCAT = total_scat(SG_HALL,UNIT_CELL)/INCREASE; //actual scattering
  PHASER_ASSERT(model_scattering.size());
  int NMOL = 1;
  if (PTNCS.use_and_present()) NMOL = std::max(1,PTNCS.NMOL);

 //SEARCH does not have NMOL present explicitly
  double search_scat(0); // Accumulate fraction scattering at rest
  for (int c = 0; c < AUTO_SEARCH.size(); c++)
  if (!AUTO_SEARCH[c].found())
  {
    double maxScat(0); //maximum scattering of all the alternative models for each component
    for (int alt = 0; alt < AUTO_SEARCH[c].MODLID.size(); alt++)
    {
      std::string m = AUTO_SEARCH[c].MODLID[alt];
      maxScat = std::max(maxScat,SEARCH_FACTORS.OFAC*model_scattering[m]*NMOL);
    }
    search_scat += maxScat;
  }

 //MRSET has NMOL present explicitly
  double maxScatSolu(0);
  for (int k = 0; k < MRSET.size(); k++)
  {
    double thisScatSolu(0);
    //background of search first
    for (int l = 0; l < MRSET[k].KNOWN.size(); l++)
    {
      std::string m = MRSET[k].KNOWN[l].MODLID;
      thisScatSolu += double(DEF_OFAC_SCALE_MAX)*model_scattering[m];
    }
    maxScatSolu = std::max(maxScatSolu,thisScatSolu);
    //add the maximum from the RLIST
    double maxScatSearch(0);
    for (int l = 0; l < MRSET[k].RLIST.size(); l++)
    {
      std::string m = MRSET[k].RLIST[l].MODLID;
      maxScatSearch = std::max(maxScatSearch,SEARCH_FACTORS.OFAC*model_scattering[m]*NMOL);
    }
    thisScatSolu += maxScatSearch;
    maxScatSolu = std::max(maxScatSolu,thisScatSolu);
  }
  search_scat += maxScatSolu;

  if (search_scat/MAX_FRAC_SCAT > TOTAL_SCAT)
  {
    INCREASE = (search_scat/MAX_FRAC_SCAT)/TOTAL_SCAT;
  }
}

float1D Composition::get_search_bfactors(std::string SG_HALL,af::double6 UNIT_CELL,double fullhires,mr_solution MRSET)
{
  double upperB(DEF_MR_BMAX);
  float1D search_bfactors(MRSET.size());
  double TOTAL_SCAT = total_scat(SG_HALL,UNIT_CELL);
  PHASER_ASSERT(model_scattering.size());
  double kssqr = -2*fn::pow2(1./fullhires)/4;

  for (int k = 0; k < MRSET.size(); k++)
  {
    double fracmodeled(0.);   // Accumulate background fraction scattering at rest
    double fracmodeled_B(0.); // Accumulate B-weighted background fraction scattering at resolution limit
    for (int l = 0; l < MRSET[k].KNOWN.size(); l++)
    {
      std::string m = MRSET[k].KNOWN[l].MODLID;
      double this_scat(model_scattering[m]);
      double this_models_bfac(MRSET[k].KNOWN[l].BFAC);

      this_scat /= TOTAL_SCAT; // Now fractional
      fracmodeled   += this_scat;
      fracmodeled_B += this_scat*std::exp(kssqr*this_models_bfac);
    }
    // Work out B-factor that would have to apply to remaining structure to fit within total
    // scattering at resolution limit
    double fracleft(1.-fracmodeled);
    PHASER_ASSERT(fracleft > 0.);
    double fracleftmin = fracleft*std::exp(kssqr*upperB);
    double fracleft_B(1.-fracmodeled_B);
    if (fracleft_B < fracleftmin) // Too negative B supplied by user or numerical precision of refinement
    {
      double corrB = std::log(fracmodeled_B/(1.-fracleftmin))/kssqr;
      for (int l = 0; l < MRSET[k].KNOWN.size(); l++)
      {
        double this_models_bfac(MRSET[k].KNOWN[l].BFAC);
        MRSET[k].KNOWN[l].BFAC = (this_models_bfac+corrB);
      }
      search_bfactors[k] = upperB;
    }
    else if (fracleft_B < fracleft) // B-factor for what is left must be higher than average
      search_bfactors[k] = std::log(fracleft_B/fracleft)/kssqr;
    else
      search_bfactors[k] = 0.;
  }

  return search_bfactors;
}

void Composition::table(outStream where,double TOTAL_SCAT,map_str_pdb& pdb,data_bofac SEARCH_FACTORS,Output& output)
{
  output.logUnderLine(where,"Composition Table");
  output.logTab(1,where,"Total Scattering = " + itos(round(TOTAL_SCAT)));
  output.logTab(1,where,"Search occupancy factor = " + dtos(SEARCH_FACTORS.OFAC) +
                   std::string((SEARCH_FACTORS.OFAC == 1)?" (default)":""));
  int n(30);
  output.logTabPrintf(1,where,"%-30s %-10s %-20s\n","Ensemble","Frac.Scat.","(Search Frac.Scat.)");
  PHASER_ASSERT(TOTAL_SCAT);
  for (map_str_float::iterator iter = model_scattering.begin(); iter != model_scattering.end(); iter++)
  {
    std::string modlid = iter->first;
    output.logTabPrintf(1,where,"%-30s %9.2f%c %18.2f%c\n",modlid.substr(0,n).c_str(),
                      (100*pdb[modlid].SCATTERING/TOTAL_SCAT),'%',
                      (SEARCH_FACTORS.OFAC*100*pdb[modlid].SCATTERING/TOTAL_SCAT),'%');
  }
  output.logBlank(where);
}

void Composition::table(outStream where,double TOTAL_SCAT,map_str_pdb PDB,mr_set& SET,Output& output)
{
  output.logUnderLine(where,"Composition Table");
  output.logTab(1,where,"Total Scattering after Occupancy Refinement = " + itos(round(TOTAL_SCAT)));
  int n(30);
  output.logTabPrintf(1,where,"%-30s %-10s %-20s\n","Ensemble","Frac.Scat.","(Frac.Orig.Scat.)");
  PHASER_ASSERT(TOTAL_SCAT);
  for (int k = 0; k < SET.KNOWN.size(); k++)
  {
    std::string modlid = SET.KNOWN[k].MODLID;
    float1D allScat;
    for (int M = 0; M < PDB[modlid].nMols(); M++)
    {
      Molecule MOLECULE = PDB[modlid].Coords(M);
      for (int m = 0; m < MOLECULE.nMols(); m++)
      {
        float1D OCC = SET.bool_to_real_occupancies(k);
        if (OCC.size())  //mtrace only
        allScat.push_back(MOLECULE.getScat(&OCC));
        else
        allScat.push_back(MOLECULE.getScat(m));
      }
    }
    double this_scat = mean(allScat)/TOTAL_SCAT;
    output.logTabPrintf(1,where,"%-30s %9.2f% %18.2f%\n",modlid.substr(0,n).c_str(),
                    (100*this_scat),
                    (100*this_scat/PDB[modlid].SCATTERING/TOTAL_SCAT));
  }
  output.logBlank(where);
}

} //phaser
