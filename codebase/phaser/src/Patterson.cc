//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/src/Patterson.h>
#include <phaser/lib/jiffy.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <phaser/cctbx_project/iotbx/ccp4_map.h>
#include <phaser/io/ResultFile.h>

namespace phaser {

  Patterson::Patterson(
     UnitCell& uc,
     SpaceGroup& sg,
     af::shared<miller::index<int> > miller,
     af_cmplx  Iobs,
     bool PATT_MAPS,
     Output& output
     ) :
        SpaceGroup(sg),
        UnitCell(uc)
  {
    cctbx::uctbx::unit_cell cctbxUC(getCctbxUC());
    cctbx::sgtbx::space_group cctbxSG(getCctbxSG());
    cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
    cctbx::sgtbx::space_group cctbxPattSG(cctbxSG.build_derived_patterson_group());
    cctbx::sgtbx::space_group_type PattInfo(cctbxPattSG);
    SpaceGroup PATT(PattInfo.hall_symbol());
    output.logTab(1,LOGFILE,"Space Group :       " + spcgrpname());
    output.logTab(1,LOGFILE,"Patterson Symmetry: " + PATT.spcgrpname());

    floatType lores = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller.const_ref())[0]);
    floatType hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller.const_ref())[1]);
    output.logTabPrintf(1,LOGFILE,"Resolution of Patterson (Number):     %6.2f %6.2f (%d)\n",hires,lores,miller.size());

    // --- prepare fft grid
    cctbx::sgtbx::search_symmetry_flags sym_flags(true);
    af::int3 mandatory_factors(2,2,2);
    int max_prime(5);
    bool assert_shannon_sampling(true);
    floatType resolution_factor = 1.0/(2.0*1.5);
    af::int3 gridding = cctbx::maptbx::determine_gridding(
      cctbxUC,hires,resolution_factor,sym_flags,SgInfo,mandatory_factors,max_prime,assert_shannon_sampling);
    af::c_grid<3> grid_target(gridding);
    try { //bugfix for 3eke
      //where the memory is huge using deposited intensity data, which are bogus
      //errors are not being thrown from rfft allocation
      //make a std::vector same size as rfft will be, and then kill it
      std::vector<double>(gridding[0]*gridding[1]*gridding[2]);
    }
    catch(std::bad_alloc& ba)
    {
      throw PhaserError(MEMORY,"Patterson FFT array allocation");
    }
    cctbx::maptbx::grid_tags<long> tags(grid_target);
    scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

    bool anomalous_flag(false);
    bool conjugate_flag(true);
    af::c_grid_padded<3> map_grid(rfft.n_complex());
    bool treat_restricted = true;
    cctbx::maptbx::structure_factors::to_map<floatType> Patterson(
      cctbxPattSG,
      anomalous_flag,
      miller.const_ref(),
      Iobs.const_ref(),
      rfft.n_real(),
      map_grid,
      conjugate_flag,
      treat_restricted);
    af::ref<cmplxType, af::c_grid<3> > Patterson_fft_ref(
      Patterson.complex_map().begin(),
      af::c_grid<3>(rfft.n_complex()));

    // --- do the fft
    rfft.backward(Patterson_fft_ref);

    //convert padded to unpadded
    af::ref<floatType, af::c_grid_padded<3> > real_map_padded(
      reinterpret_cast<floatType*>(
        Patterson.complex_map().begin()),
        af::c_grid_padded<3>(rfft.m_real(), rfft.n_real()));

    af::versa<floatType, af::c_grid<3> > real_map_unpadded((
      af::c_grid<3>(rfft.n_real())));
    cctbx::maptbx::copy(real_map_padded, real_map_unpadded.ref());

    // --- work out mean and sigma for total search
    cctbx::maptbx::statistics<floatType> stats(
      af::const_ref<floatType, af::flex_grid<> >(
        real_map_unpadded.begin(),
        real_map_unpadded.accessor().as_flex_grid()));
    Patterson_mean = stats.mean();
    Patterson_sigma = stats.sigma();

    // --- find symmetry equivalents
    // convert unpadded to padded
    //!!No real_map_const_ref= af::const_ref<floatType, af::c_grid_padded<3> >(
    af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));
    tags.build(PattInfo, sym_flags);
    af::ref<long, af::c_grid<3> > tag_array_ref(
      tags.tag_array().begin(),
      af::c_grid<3>(tags.tag_array().accessor()));
    if (!treat_restricted)
    PHASER_ASSERT(tags.verify(real_map_const_ref));

    // --- peak search
    int peak_search_level(3);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
      real_map_const_ref,
      tag_array_ref,
      peak_search_level,
      max_peaks,
      interpolate);
    peak_heights = peak_list.heights().deep_copy();
    peak_sites = peak_list.sites().deep_copy();

    if (PATT_MAPS)
    {
      af::const_ref<std::string> labels(0,0);
      std::string HKLOUT = output.Fileroot()+".patterson.map";
      af::int3 gridding_first(0,0,0);
      af::int3 gridding_last = gridding;
      af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
         real_map_unpadded.begin(), //af::versa<floatType, af::c_grid<3> > tsmap;
           af::c_grid_padded_periodic<3>(real_map_unpadded.accessor()));
      cctbx::sgtbx::space_group SgOpsP1("P1");
      iotbx::ccp4_map::write_ccp4_map_p1_cell(
        HKLOUT,
        cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels);
      output.logTab(2,LOGFILE,"Patterson Map written to: " + HKLOUT);
      output.logBlank(LOGFILE);

      std::string PDBOUT = output.Fileroot()+".patterson.pdb";
      ResultFile opened(PDBOUT);
      fprintf(opened.outFile,"REMARK PATTERSON VECTORS\n");
      fprintf(opened.outFile,
      "ATOM%7s %4s%1s%3s%2s%4s%1s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s\n",
          itos(1).c_str()," CA ","","ORI","A",itos(1).c_str(),"",0.0,0.0,0.0,1.0,0.0,"","C");
      for (unsigned i = 0; i < peak_list.size(); i++)
      {
        dvect3 frac_i(peak_list.sites()[i]);
        dvect3 X = doFrac2Orth(frac_i);
        fprintf(opened.outFile,
          "ATOM%7s %4s%1s%3s%2s%4s%1s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s\n",
            itos(i+2).c_str()," CA ","","PPK","A",itos(i+2).c_str(),"",X[0],X[1],X[2],1.0,0.0,"","C");
      }
      fclose(opened.outFile);
    }

    Patterson_max = peak_list.heights()[0];
    for (std::size_t i = 1; i < peak_list.size(); i++)
      Patterson_max = std::max(Patterson_max,peak_list.heights()[i]);
    output.logUnderLine(VERBOSE,"Patterson Statistics");
    output.logTab(2,VERBOSE,"Number of peaks = " + itos(peak_list.size()));
    output.logTab(2,VERBOSE,"Maximum = " + dtos(Patterson_max));
    output.logTab(2,VERBOSE,"Mean   = "+dtos(Patterson_mean));
    output.logTab(2,VERBOSE,"Sigma  = "+dtos(Patterson_sigma));
    output.logBlank(VERBOSE);
  }

} //end namespace phaser
