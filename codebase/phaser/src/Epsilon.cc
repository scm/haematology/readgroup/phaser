//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/TncsVar.h>
#include <phaser/src/RefineNCS.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Hexagonal.h>
#include <phaser/lib/round.h>
#include <phaser/lib/factor.h>
#include <phaser/lib/between.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <phaser/lib/sg_prior.h>
#include <phaser/include/data_tncs.h>
#include <scitbx/math/r3_rotation.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <phaser/cctbx_project/iotbx/ccp4_map.h>
#include <phaser/io/ResultFile.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

Epsilon::Epsilon() : DataB()
{
  best_likelihood = best_num = 0;
  patt_vector = dvect3(0,0,0);
  refinedL.clear();
  refinedR.clear();
  refinedT.clear();
  MOMENTS = data_moments();
}

Epsilon::Epsilon(data_refl& REFLECTIONS_,
                 data_resharp& RESHARP_,
                 data_norm& SIGMAN_,
                 data_outl& OUTLIER_,
                 data_tncs& PTNCS_,
                 data_bins& DATABINS_,
                 data_composition& COMPOSITION_,
                 double HIRES,double LORES)
           : DataB(REFLECTIONS_,RESHARP_,SIGMAN_,OUTLIER_,PTNCS_,DATABINS_,COMPOSITION_,HIRES,LORES)
{
  best_likelihood = best_num = 0; patt_vector = dvect3(0,0,0);
  //need to take out the translational symmetry or the frequency analysis misses reflections
  //due to the systematic absences
  cctbx::sgtbx::space_group sg = getCctbxSG();
  sg = sg.build_derived_group(false,false);
  setSpaceGroup(sg.type().hall_symbol());
}

void Epsilon::initROT(floatType HIRES,Output& output)
{
  outStream where(VERBOSE);
  dvect3 POINT(0,0,0);
  if (PTNCS.ROT.angle_is_set())
  {
    output.logTab(1,where,"No sampling of tncs rotational differences");
    output.logTab(2,where,"Rotation angle input = " + dvtos(PTNCS.ROT.ANGLE,5,2));
    PTNCS.ROT.SAMP_ANGLE.push_back(PTNCS.ROT.ANGLE);
  }
  else if ((PTNCS.ROT.SAMPLING == 0) ||
           (PTNCS.ROT.RANGE == 0) ||
           (PTNCS.NMOL > 2))
  {
    output.logTab(1,where,"No sampling of of tncs rotational differences");
    (PTNCS.NMOL > 2) ?
    output.logTab(2,where,"Sampling not valid for NMOL > 2"):
    output.logTab(2,where,"Range or sampling input zero");
    PTNCS.ROT.SAMP_ANGLE.push_back(POINT);
  }
  else
  {
    if (!(PTNCS.ROT.SAMPLING >= 0)) //flag not set
    {
      output.logTab(1,where,"Use default sampling of tncs rotational differences");
      cctbx::uctbx::unit_cell cctbxUC(getCctbxUC());
      floatType mtz_hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.max_d_star_sq(MILLER.const_ref()));
      floatType hires = std::max(HIRES,mtz_hires);
      floatType sampling = 2.0*std::atan(hires/(4.0*PTNCS.GFUNCTION_RADIUS));
      PTNCS.ROT.SAMPLING = scitbx::rad_as_deg(sampling);
      PTNCS.ROT.SAMPLING /= 2; //fine sampling
    }
    else
      output.logTab(1,where,"Use input sampling of tncs rotational differences");
    output.logTab(2,where,"Sampling = " + dtos(PTNCS.ROT.SAMPLING));
    // Rotations around xyz are nearly orthogonal, so use machinery for
    // TRANSLATE AROUND feature to generate hexagonal set of rotations.
    UnitCell box(dvect3(1.,1.,1.));
    SpaceGroup P1("P 1");
    Hexagonal rotlist(box,P1);
    floatType def_range = 1.1*PTNCS.ROT.SAMPLING;
    (PTNCS.ROT.RANGE >=0) ?
    output.logTab(1,where,"Use input range"):
    output.logTab(1,where,"Use default range");
    PTNCS.ROT.RANGE = PTNCS.ROT.RANGE>=0 ? PTNCS.ROT.RANGE : def_range;
    rotlist.setupAroundPoint(POINT,PTNCS.ROT.SAMPLING,PTNCS.ROT.RANGE);
    for (int a = 0; a < rotlist.count_sites(); a++)
    if (rotlist.next_site_frac(a)[0] >= 0)
      PTNCS.ROT.SAMP_ANGLE.push_back(rotlist.next_site_frac(a));
    output.logTab(2,where,"Range = " + dtos(PTNCS.ROT.RANGE));
    PTNCS.ROT.SAMP_ANGLE.size() == 1 ?
    output.logTab(1,where,"There is 1 tncs rotational sampling perturbation angle"):
    output.logTab(1,where,"There are " + itos(PTNCS.ROT.SAMP_ANGLE.size()) + " tncs rotational perturbation angles");
    output.logBlank(where);
  }
}

void Epsilon::findTRA(Output& output)
{
  double Patterson_top(0);
  output.logSectionHeader(LOGFILE,"TRANSLATIONAL NCS");
/*
  if ((A() == 25. && B() == 25. && C() == 25. && Alpha() == 90. && Beta() == 100. && Gamma() == 90.) && spcgrpnumber() == 3) //P2,not P21
  {
    output.logWarning(LOGFILE,"TEST DATA DETECTED: NMOL HARDWIRED TO 1");
    PTNCS.REFINED = true; PTNCS.NMOL = 1;
  }
*/
  if (PTNCS.TRA.VECTOR_SET) //previously refined or set on input
  {
    output.logTab(1,LOGFILE,"tNCS vector = " + dvtos(PTNCS.TRA.VECTOR));
    if (!PTNCS.NMOL)
    {
      output.logTab(1,LOGFILE,"NMOL set to (default) value of 2");
      PTNCS.NMOL = 2;
    }
    output.logTab(1,LOGFILE,"tNCS NMOL   = " + itos(PTNCS.NMOL));
    output.logBlank(LOGFILE);
  }
  else if (PTNCS.REFINED && PTNCS.NMOL == 1) //previously refined or set on input
  {
    output.logTab(1,LOGFILE,"tNCS not corrected");
    output.logBlank(LOGFILE);
  }
  else if (PTNCS.REFINED) //file has been read for refinement previously
  {
    output.logTab(1,LOGFILE,"tNCS not present");
    output.logBlank(LOGFILE);
  }
  else if (PTNCS.FILENAME.size() && PTNCS.READ) //!REFINED but there is a file to read
  {
    std::string msg = PTNCS.read_file(MILLER);
    if (msg.size()) throw PhaserError(FATAL,"Error reading tNCS binary file: incompatible with data\n" + msg);
  }
  else //if (!PTNCS.REFINED)
  {
    output.logTab(1,LOGFILE,"tNCS vector not set");
    cctbx::uctbx::unit_cell cctbxUC(getCctbxUC());
    cctbx::sgtbx::space_group cctbxSG(getCctbxSG());
    cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
    cctbx::sgtbx::space_group cctbxPattSG(cctbxSG.build_derived_patterson_group());
    cctbx::sgtbx::space_group_type PattInfo(cctbxPattSG);
    SpaceGroup PATT(PattInfo.hall_symbol());
    output.logTab(1,LOGFILE,"Space Group (without translational symmetry):" + spcgrpname());
    output.logTab(1,LOGFILE,"Patterson Symmetry: " + PATT.spcgrpname());

    floatType mtz_lores = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(MILLER.const_ref())[0]);
    floatType mtz_hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(MILLER.const_ref())[1]);
    output.logTabPrintf(1,LOGFILE,"Resolution of All Data (Number):      %6.2f %6.2f (%d)\n",mtz_hires,mtz_lores,NREFL);

    af::shared<miller::index<int> > miller_res_limits;
    af_cmplx  Iobs;

    // Change default Patterson limits if only low-resolution data
    floatType patt_hires(std::max(PTNCS.TRA.PATT_HIRES,mtz_hires));
    floatType patt_lores(std::min(PTNCS.TRA.PATT_LORES,mtz_lores));

    af_float Idat = INPUT_INTENSITIES ?  I : IfromF;
    for (int r = 0; r < NREFL; r++)
    if (selected[r] &&
        cctbx::uctbx::d_star_sq_as_d(cctbxUC.d_star_sq(MILLER[r])) >= patt_hires &&
        cctbx::uctbx::d_star_sq_as_d(cctbxUC.d_star_sq(MILLER[r])) <= patt_lores)
    {
      miller_res_limits.push_back(MILLER[r]);
      floatType phase(0);
      Iobs.push_back(cmplxType(Idat[r],phase));
    }
    if (!miller_res_limits.size())
    {
      //get desperate, use all the reflections that are present
      for (int r = 0; r < NREFL; r++)
      if (selected[r])
      {
        miller_res_limits.push_back(MILLER[r]);
        floatType phase(0);
        Iobs.push_back(cmplxType(Idat[r],phase));
      }
    }
    floatType lores = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller_res_limits.const_ref())[0]);
    floatType hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller_res_limits.const_ref())[1]);
    output.logTabPrintf(1,LOGFILE,"Resolution of Patterson (Number):     %6.2f %6.2f (%d)\n",hires,lores,miller_res_limits.size());
    output.logBlank(LOGFILE);

    if (hires > 10.0)
    {
      output.logWarning(SUMMARY,"Very low resolution for NMOL analysis");
      PTNCS_NMOL.WarningReflections  = true;
    }
    else if (miller_res_limits.size() < 100)
    {
      output.logWarning(SUMMARY,"Very few reflections for NMOL analysis");
      PTNCS_NMOL.WarningReflections  = true;
    }

    // --- prepare fft grid
    cctbx::sgtbx::search_symmetry_flags sym_flags(true);
    af::int3 mandatory_factors(2,2,2);
    int max_prime(5);
    bool assert_shannon_sampling(true);
    floatType resolution_factor = 1.0/(2.0*1.5);
    af::int3 gridding = cctbx::maptbx::determine_gridding(
      cctbxUC,hires,resolution_factor,sym_flags,SgInfo,mandatory_factors,max_prime,assert_shannon_sampling);
    af::c_grid<3> grid_target(gridding);
    try { //bugfix for 3eke
      //where the memory is huge using deposited intensity data, which are bogus
      //errors are not being thrown from rfft allocation
      //make a std::vector same size as rfft will be, and then kill it
      std::vector<double>(gridding[0]*gridding[1]*gridding[2]);
    }
    catch(std::bad_alloc& ba)
    {
      throw PhaserError(MEMORY,"Patterson FFT array allocation");
    }
    cctbx::maptbx::grid_tags<long> tags(grid_target);
    scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

    bool anomalous_flag(false);
    bool conjugate_flag(true);
    af::c_grid_padded<3> map_grid(rfft.n_complex());
    bool treat_restricted = true;
    cctbx::maptbx::structure_factors::to_map<floatType> Patterson(
      cctbxPattSG,
      anomalous_flag,
      miller_res_limits.const_ref(),
      Iobs.const_ref(),
      rfft.n_real(),
      map_grid,
      conjugate_flag,
      treat_restricted);
    af::ref<cmplxType, af::c_grid<3> > Patterson_fft_ref(
      Patterson.complex_map().begin(),
      af::c_grid<3>(rfft.n_complex()));

    // --- do the fft
    rfft.backward(Patterson_fft_ref);

    af::ref<floatType, af::c_grid_padded<3> > real_map_padded(
      reinterpret_cast<floatType*>(
        Patterson.complex_map().begin()),
        af::c_grid_padded<3>(rfft.m_real(), rfft.n_real()));

    floatType map_cutoff_height(0);
    floatType Patterson_max(0);

    af::versa<floatType, af::c_grid<3> > real_map_unpadded((
      af::c_grid<3>(rfft.n_real())));
    cctbx::maptbx::copy(real_map_padded, real_map_unpadded.ref());

    // --- work out mean and sigma for total search
    cctbx::maptbx::statistics<floatType> stats(
      af::const_ref<floatType, af::flex_grid<> >(
        real_map_unpadded.begin(),
        real_map_unpadded.accessor().as_flex_grid()));
    floatType Patterson_mean = stats.mean();
    floatType Patterson_sigma = stats.sigma();

    // --- find symmetry equivalents
    af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));
    tags.build(PattInfo, sym_flags);
    af::ref<long, af::c_grid<3> > tag_array_ref(
      tags.tag_array().begin(),
      af::c_grid<3>(tags.tag_array().accessor()));
    if (!treat_restricted)
    PHASER_ASSERT(tags.verify(real_map_const_ref));

    // --- peak search
    int peak_search_level(3);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
      real_map_const_ref,
      tag_array_ref,
      peak_search_level,
      max_peaks,
      interpolate);

    af::shared<double> peak_heights = peak_list.heights().deep_copy();
    af::shared<scitbx::vec3<double> > peak_sites = peak_list.sites().deep_copy();

    Patterson_max = peak_heights[0];
    for (std::size_t i = 1; i < peak_heights.size(); i++)
      Patterson_max = std::max(Patterson_max,peak_heights[i]);
    Patterson_top = 100*peak_heights[1]/Patterson_max;
    output.logUnderLine(VERBOSE,"Patterson Statistics");
    output.logTab(2,VERBOSE,"Number of peaks = " + itos(peak_list.size()));
    output.logTab(2,VERBOSE,"Maximum = " + dtos(Patterson_max));
    output.logTab(2,VERBOSE,"Mean   = "+dtos(Patterson_mean));
    output.logTab(2,VERBOSE,"Sigma  = "+dtos(Patterson_sigma));
    output.logBlank(VERBOSE);
    map_cutoff_height = std::min(PTNCS.COMM.PATT,PTNCS.TRA.PATT_PERCENT)*Patterson_max;
    //map_cutoff_height should probably be same as commensurate test, although could be PATT_PERCENT
    //this is relevant to the complete test below, where we assume peak_heights are interesting ie. over PTNCS.COMM.PATT

    //scale the peak to percents
    for (int i = peak_list.size()-1; i >= 0; i--)
      peak_heights[i] *= 100/Patterson_max;

    //after peaks over cutoff selected
    if (PTNCS.TRA.PATT_MAPS)
    {
      output.logBlank(LOGFILE);
      output.logUnderLine(LOGFILE,"Patterson Output");
      af::const_ref<std::string> labels(0,0);
      std::string HKLOUT = output.Fileroot()+".patterson.map";
      af::int3 gridding_first(0,0,0);
      af::int3 gridding_last = gridding;
      af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
         real_map_unpadded.begin(), //af::versa<floatType, af::c_grid<3> > tsmap;
           af::c_grid_padded_periodic<3>(real_map_unpadded.accessor()));
      cctbx::sgtbx::space_group SgOpsP1("P1");
      iotbx::ccp4_map::write_ccp4_map_p1_cell(
        HKLOUT,
        cctbxUC,
        SgOpsP1,
        gridding_first,
        gridding_last,
        map_data,
        labels);
      output.logTab(2,LOGFILE,"Patterson Map written to: " + HKLOUT);

      std::string PDBOUT = output.Fileroot()+".patterson.pdb";
      ResultFile opened(PDBOUT);
      fprintf(opened.outFile,"REMARK PATTERSON VECTORS\n");
      for (unsigned i = 0; i < peak_sites.size(); i++)
      if (peak_heights[i] >= PTNCS.TRA.PATT_PERCENT*100) //soft selection
      {
        dvect3 frac_i(peak_sites[i]);
        dvect3 X = doFrac2Orth(frac_i);
        fprintf(opened.outFile,
          "ATOM%7s %4s%1s%3s%2s%4s%1s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s\n",
            itos(i+1).c_str()," CA ","","PPK","A",itos(i+1).c_str(),"",X[0],X[1],X[2],peak_heights[i]/100,0.0,"","C");
      }
      output.logTab(2,LOGFILE,"Patterson Pdb written to: " + PDBOUT);
      output.logBlank(LOGFILE);
      fclose(opened.outFile);
    }

    output.logUnderLine(LOGFILE,"Raw Patterson Peaks Table");
    output.logTab(1,LOGFILE,"Sorted by Height");
    output.logTab(1,LOGFILE,"Height Vector");
    for (unsigned i = 0; i < peak_sites.size(); i++)
    if (peak_heights[i] >= PTNCS.TRA.PATT_PERCENT*100) //soft selection
    {
      dvect3 frac_i(peak_sites[i]);
      dvect3 orth_i = doFrac2Orth(frac_i);
      output.logTabPrintf(1,LOGFILE,"%5.1f%c:   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)\n",
          peak_heights[i],'%',
          frac_i[0],frac_i[1],frac_i[2],
          orth_i[0],orth_i[1],orth_i[2]);
    }
    output.logBlank(LOGFILE);

    output.logTab(1,LOGFILE,"Patterson Top (All) = "+dtos(Patterson_top,2) + "%");
    output.logTab(2,LOGFILE,"There " + std::string(peak_heights.size() == 1 ? "was " : "were ") + itos(peak_heights.size()) + " peak" + std::string(peak_heights.size() == 1 ? "" : "s"));
    output.logBlank(LOGFILE);
    PTNCS_NMOL.PattersonTopAll = Patterson_top;

    //Remove non-origin peaks, same as xtriage
    int NSYM = PATT.NSYMM;
    bool1D origin(peak_sites.size(),false); //keep as flag, because we will cluster to these
    std::vector<std::vector<data_nmol> > expanded_non_origin_peaks(peak_sites.size());
    if (peak_sites.size())
    {
      std::vector<data_tncs> origin_peaks;
      float1D origin_dist(peak_sites.size(),10000);
      for (unsigned i = 0; i < peak_sites.size(); i++)
      {
        dvect3 peak_site_i(peak_sites[i]);
        for (unsigned isym = 0; isym < NSYM; isym++)
        {
          dvect3 frac_i = PATT.doSymXYZ(isym,peak_site_i);
          dvect3 cellTrans;
          for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
            for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
              for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
              {
                dvect3 fracCell_i = frac_i + cellTrans;
                dvect3 orth_i = doFrac2Orth(fracCell_i);
                floatType dist_to_origin(std::sqrt(std::fabs((orth_i)*(orth_i))));
                if (dist_to_origin < origin_dist[i])
                {
                  origin_dist[i] = dist_to_origin;
                  peak_sites[i] = fracCell_i; //replace with closest to origin
                }
                if (!origin[i] && dist_to_origin < PTNCS.TRA.PATT_DIST)
                {
                  origin[i] = true;
                  //goto end_peak; //find closest
                }
              }
        }
       // end_peak: continue;
      }
      int norigin(0);
      PTNCS_NMOL.PattersonTopNonOrigin = 0;
      for (unsigned i = 0; i < peak_sites.size(); i++)
      {
        dvect3 frac_j = peak_sites[i];
        for (int n = 0; n < 3; n++) { while (frac_j[n] < 0) frac_j[n]++; while (frac_j[n] >= 1) frac_j[n]--; }
        expanded_non_origin_peaks[i].push_back(data_nmol(2,frac_j,peak_heights[i],origin_dist[i]));
        if (!origin[i]) norigin++;
        if (!origin[i] && !PTNCS_NMOL.PattersonTopNonOrigin) PTNCS_NMOL.PattersonTopNonOrigin = peak_heights[i];
      }
      output.logTab(1,LOGFILE,"Patterson Top (Non-origin) = "+dtos(PTNCS_NMOL.PattersonTopNonOrigin,2) + "%");
      output.logTab(2,LOGFILE,"Patterson Origin Vector Distance = "+ dtos(PTNCS.TRA.PATT_DIST));
      output.logTab(2,LOGFILE,"There " + std::string(norigin == 1 ? "was " : "were ") + itos(norigin) + " non-origin peak" + std::string(norigin == 1 ? "" : "s"));
      output.logBlank(LOGFILE);
    }

    //now erase the low ones
    for (int i = peak_list.size()-1; i >= 0; i--)
    if (peak_heights[i] < PTNCS.TRA.PATT_PERCENT*100)
    {
      peak_heights.erase(peak_heights.begin()+i);
      peak_sites.erase(peak_sites.begin()+i);
      expanded_non_origin_peaks.erase(expanded_non_origin_peaks.begin()+i);
      origin.erase(origin.begin()+i);
    }
    PTNCS_NMOL.PattersonTopCutoff = 0;
    int norigin(0);
    for (unsigned i = 0; i < peak_sites.size(); i++)
    {
      if (!origin[i]) norigin++;
      if (!origin[i] && !PTNCS_NMOL.PattersonTopCutoff) PTNCS_NMOL.PattersonTopCutoff = peak_heights[i];
    }
    output.logTab(1,LOGFILE,"Patterson Top (Cutoff) = "+dtos(PTNCS_NMOL.PattersonTopCutoff,2) + "%");
    output.logTab(2,LOGFILE,"Patterson cutoff = " + dtos(PTNCS.TRA.PATT_PERCENT*100) + "%");
    output.logTab(2,LOGFILE,"There " + std::string(norigin == 1 ? "was " : "were ") + itos(norigin) + " non-origin peak" + std::string(norigin == 1 ? "" : "s") + " over cutoff");
    output.logBlank(LOGFILE);

    //erase the ones that are obviously peptides or otherwise odd, very short cells
    bool flag(false);
    if (UnitCell::A() < PTNCS.TRA.PATT_DIST ||
        UnitCell::B() < PTNCS.TRA.PATT_DIST ||
        UnitCell::C() < PTNCS.TRA.PATT_DIST)
    {
      for (unsigned i = 0; i < origin.size(); i++)
        origin[i] = true;
      flag = true;
    }
    norigin = 0;
    PTNCS_NMOL.PattersonTopLargeCell = 0;
    for (unsigned i = 0; i < peak_sites.size(); i++)
    {
      if (!origin[i]) norigin++;
      if (!origin[i] && !PTNCS_NMOL.PattersonTopLargeCell) PTNCS_NMOL.PattersonTopLargeCell = peak_heights[i];
    }
    output.logTab(1,LOGFILE,"Patterson Top (Large Cell) = "+ dtos(PTNCS_NMOL.PattersonTopLargeCell,2) + "%");
    output.logTab(2,LOGFILE,"Unit Cell dimension was" + std::string(flag? "" : " not") + " smaller than origin Patterson vector distance");
    output.logTab(2,LOGFILE,"There " + std::string(norigin == 1 ? "was " : "were ") + itos(norigin) + " non-origin and large cell peak" + std::string(norigin == 1 ? "" : "s") + " over cutoff");
    output.logBlank(LOGFILE);

    int nstore(expanded_non_origin_peaks.size());
    if (expanded_non_origin_peaks.size())
    {
      //already sorted by height, highest to lowest, because peak_heights are sorted
      bool1D cluster_site(expanded_non_origin_peaks.size(),true);
      for (unsigned i = 0; i < expanded_non_origin_peaks.size(); i++)
      {
        if (origin[i]) cluster_site[i] = false;
        dvect3 orthRef = doFrac2Orth(expanded_non_origin_peaks[i][0].vector_frac);
        for (int i_prev = 0; i_prev < i; i_prev++)
        {
          for (unsigned isym = 0; isym < NSYM; isym++)
          {
            dvect3 fracPrev = PATT.doSymXYZ(isym,expanded_non_origin_peaks[i_prev][0].vector_frac);
            dvect3 cellTrans;
            for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
              for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
                for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
                {
                  dvect3 fracPrevCell = fracPrev+cellTrans;
                  dvect3 orthPrev = doFrac2Orth(fracPrevCell);
                  floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
                  if (delta < fn::pow2(PTNCS.TRA.PATT_DIST))
                  {
                    cluster_site[i] = false;
                    cluster_site[i_prev] = false; //get rid of them both! very wide peak
                    goto next_i;
                  }
                } //cells
          } //symm
        }
        next_i: continue; //performs the next iteration of the loop.
      }
      //just take the top sites
      PTNCS_NMOL.PattersonTopAnalysis = 0;
      for (unsigned i = 0; i < peak_sites.size(); i++)
        if (cluster_site[i] && !PTNCS_NMOL.PattersonTopAnalysis) PTNCS_NMOL.PattersonTopAnalysis = peak_heights[i];
      for (int i = expanded_non_origin_peaks.size()-1; i >= 0; i--)
      if (!cluster_site[i])
        expanded_non_origin_peaks.erase(expanded_non_origin_peaks.begin()+i);

      output.logTab(1,LOGFILE,"Patterson Top (Analysis) = "+ dtos(PTNCS_NMOL.PattersonTopAnalysis,2) + "%");
      output.logTab(2,LOGFILE,"Peaks within minimum Patterson vector distance of one another were deleted");
      output.logTab(2,LOGFILE,"There " + std::string(expanded_non_origin_peaks.size() == 1 ? "was " : "were ") + itos(expanded_non_origin_peaks.size()) + " widely separated non-origin peak"  + (expanded_non_origin_peaks.size() == 1 ? "" : "s") +" over cutoff");
      output.logBlank(LOGFILE);
      int ndel(nstore - expanded_non_origin_peaks.size());
      if (ndel-1) //origin peak always removed
      {
        output.logWarning(SUMMARY,"Patterson Pathology in NMOL analysis, possible coiled coil or other regular structure, view Patterson (TNCS PATT MAPS ON)");
        PTNCS_NMOL.WarningPathology = true;
        output.logBlank(LOGFILE);
      }

      if (expanded_non_origin_peaks.size() == 0)
      {
        output.logTab(1,LOGFILE,"There were no interesting non-origin Patterson peaks");
        output.logBlank(LOGFILE);
      }
      else
      {
        int ncs_peaks = expanded_non_origin_peaks.size();
        output.logBlank(LOGFILE);
        output.logTab(1,LOGFILE,"There " + std::string(ncs_peaks == 1 ? "was " : "were ") + itos(ncs_peaks) + " non-origin distinct peak" + std::string(ncs_peaks == 1 ? "" : "s") + " (i.e. over " + dtos(PTNCS.TRA.PATT_PERCENT*100) + "% of origin peak and more than " + dtos(PTNCS.TRA.PATT_DIST) + " angstroms from the origin)");
        output.logBlank(LOGFILE);
        //first table on height
        output.logUnderLine(LOGFILE,"Patterson Peaks Table");
        output.logTab(1,LOGFILE,"Sorted by Height");
        output.logTab(1,LOGFILE,"Height Distance   Vector");
        for (unsigned i = 0; i < expanded_non_origin_peaks.size(); i++)
        {
          dvect3 frac_i(expanded_non_origin_peaks[i][0].vector_frac);
          dvect3 orth_i = doFrac2Orth(frac_i);
          output.logTabPrintf(1,LOGFILE,"%5.1f%c %6.1f :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)\n",
              expanded_non_origin_peaks[i][0].vector_height,'%',
              expanded_non_origin_peaks[i][0].vector_distance,
              frac_i[0],frac_i[1],frac_i[2],
              orth_i[0],orth_i[1],orth_i[2]);
        }
        output.logBlank(LOGFILE);
        //extend with symmetry
        double tol=1.0e-06;
        for (unsigned i = 0; i < ncs_peaks; i++)
        {
          std::vector<dvect3> symset;
          for (unsigned isym = 0; isym < NSYM; isym++)  //not identity
          {
            dvect3 frac_i = PATT.doSymXYZ(isym,expanded_non_origin_peaks[i][0].vector_frac);
            for (int n = 0; n < 3; n++) {  while (frac_i[n] < 0) frac_i[n]++; while (frac_i[n] >= 1) frac_i[n]--; }
            bool duplicate(false);
            for (int s = 0; s < symset.size(); s++)
            {
              dvect3 frac_j = symset[s];
              for (int n = 0; n < 3; n++) { while (frac_j[n] < 0) frac_j[n]++; while (frac_j[n] >= 1) frac_j[n]--; }
              if (between(frac_i[0],frac_j[0],tol) &&
                  between(frac_i[1],frac_j[1],tol) &&
                  between(frac_i[2],frac_j[2],tol))
              { duplicate = true; }
            }
            if (!duplicate)
             symset.push_back(frac_i);
          }
          for (int s = 0; s < symset.size(); s++)
          {
            double min_origin_dist(std::numeric_limits<double>::max());
            dvect3 min_frac_i;
            dvect3 cellTrans;
            for (cellTrans[0] = -0; cellTrans[0] <= 0; cellTrans[0]++)
            for (cellTrans[1] = -0; cellTrans[1] <= 0; cellTrans[1]++)
            for (cellTrans[2] = -0; cellTrans[2] <= 0; cellTrans[2]++)
            {
              dvect3 frac_i = symset[s]+cellTrans;
              dvect3 orth_i = doFrac2Orth(frac_i);
              double origin_dist(std::sqrt((std::fabs((orth_i)*(orth_i)))));
              if (origin_dist < min_origin_dist)
              {
                min_origin_dist = origin_dist;
                min_frac_i = frac_i;
              }
            }
            expanded_non_origin_peaks[i].push_back(data_nmol(2,min_frac_i,expanded_non_origin_peaks[i][0].vector_height,min_origin_dist));
          }
        }
        //second table on distance
        outStream where(LOGFILE);
        logUnitCell(where,output);
        output.logUnderLine(where,"Patterson Peaks Expanded by Symmetry");
        output.logTab(1,where,"Sorted by distance from origin in unit cell [(0,0,0),(1,1,1)]");
        output.logTab(1,where,"Height Distance   Vector");
        for (unsigned i = 0; i < expanded_non_origin_peaks.size(); i++)
        {
          std::sort(expanded_non_origin_peaks[i].begin(),expanded_non_origin_peaks[i].end(),sort_nmol_distance);
          std::reverse(expanded_non_origin_peaks[i].begin(),expanded_non_origin_peaks[i].end());
          dvect3 frac_i(expanded_non_origin_peaks[i][0].vector_frac);
          dvect3 orth_i = Frac2Orth()*(frac_i);
          output.logTabPrintf(1,where,"%5.1f%c %5.1f  :   FRAC %+6.4f %+6.4f %+6.4f   (ORTH %6.1f %6.1f %6.1f)\n",
              expanded_non_origin_peaks[i][0].vector_height,'%',
              expanded_non_origin_peaks[i][0].vector_distance,
              frac_i[0],frac_i[1],frac_i[2],
              orth_i[0],orth_i[1],orth_i[2]);
         }
        output.logBlank(where);
      }
    }

    if (!expanded_non_origin_peaks.size())
    {
      output.logBlank(LOGFILE);
      output.logTab(1,LOGFILE,"No tNCS found in Patterson");
      output.logBlank(LOGFILE);
      PTNCS.NMOL = 0; //reset default, because old .eff file in phenix will set NMOL to old default (2)
      PTNCS.ANALYSIS.resize(1); //for python
      PTNCS.ANALYSIS[0] = data_nmol(0); //default nmol 0
    }
    else if (expanded_non_origin_peaks.size())
    {
      cctbx::sgtbx::site_symmetry this_site_sym(cctbxUC,cctbxPattSG,PTNCS.TRA.VECTOR);
     // int M = PATT.NSYMM/this_site_sym.multiplicity();
     // if (M > 1) PTNCS.TRA.VECTOR = this_site_sym.exact_site(); // Put on exact site before perturbing

    //edit the map so that only the peaks are present, delete all the noise
      for (int i = 0; i < real_map_padded.size(); i++)
        real_map_padded[i] = real_map_padded[i] < map_cutoff_height ? 0 : map_cutoff_height;

      rfft.forward(real_map_padded);

      {//memory
      af::const_ref<cmplxType, af::c_grid_padded<3> > complex_map(
          reinterpret_cast<cmplxType*>(&*real_map_padded.begin()),
          af::c_grid_padded<3>(rfft.n_complex()));

      bool discard_indices_affected_by_aliasing = true;
      bool complex_conj = true;
      bool anomalous_flag = false;
      cctbx::maptbx::structure_factors::from_map<floatType> from_map(
         cctbxUC,
         SgInfo,
         anomalous_flag,
         PTNCS.TRA.PATT_HIRES,
         complex_map,
         complex_conj,
         discard_indices_affected_by_aliasing
        );

      //don't need to revert to real space, real_map_const_ref unchanged
      //-------------------------------------------------------------------------------------
      std::vector<data_freq> frequency_peaks(from_map.miller_indices().size());
      for (unsigned i = 0; i < from_map.miller_indices().size(); i++)
      {
        frequency_peaks[i].site[0] = from_map.miller_indices()[i][0];
        frequency_peaks[i].site[1] = from_map.miller_indices()[i][1];
        frequency_peaks[i].site[2] = from_map.miller_indices()[i][2];
        frequency_peaks[i].height = std::abs(from_map.data()[i]);
        double ssqr(cctbxUC.d_star_sq(from_map.miller_indices()[i]));
        frequency_peaks[i].distance = cctbx::uctbx::d_star_sq_as_d(ssqr);
      }

      //sort before
      std::sort(frequency_peaks.begin(),frequency_peaks.end(),sort_freq_height);
      std::reverse(frequency_peaks.begin(),frequency_peaks.end());

      floatType FREQ_CUTOFF(0.50);
      int maxi(0);
      for (maxi = 0; maxi < frequency_peaks.size(); maxi++)
      if (frequency_peaks[maxi].height < FREQ_CUTOFF*frequency_peaks[0].height) break;
      frequency_peaks.erase(frequency_peaks.begin()+maxi,frequency_peaks.end());

      //miller index duplicate search
      {
      bool1D top_site(frequency_peaks.size(),true);
      for (unsigned i = 0; i < frequency_peaks.size(); i++)
      {
        for (int i_prev = 0; i_prev < i; i_prev++)
        {
          ivect3 diff_miller = frequency_peaks[i_prev].site-frequency_peaks[i].site;
          if (!diff_miller[0] && !diff_miller[1] && !diff_miller[2])
            top_site[i] = false;
        }
      }
      for (int i = frequency_peaks.size()-1; i >= 0; i--)
      {
        if (!top_site[i])
        frequency_peaks.erase(frequency_peaks.begin()+i);
      }
      }

      if (false) //don't apply, not same as Patt vector, can be sym
      for (int i = frequency_peaks.size()-1; i >= 0; i--)
      {
        if (frequency_peaks[i].distance < PTNCS.TRA.PATT_DIST)
          frequency_peaks.erase(frequency_peaks.begin()+i);
      }

      if (true) //delete the frequencies that are mixed values or boring (0 or 1)
      for (int i = frequency_peaks.size()-1; i >= 0; i--)
      {
        frequency_peaks[i].height /= frequency_peaks[0].height;
        frequency_peaks[i].height *= 100; //stored as percent, same as data_nmol height
        bool match(true);
        for (int s = 0; s < 3; s++)
        {
          int nmol = frequency_peaks[i].site[s];
          if (nmol)
          {
            if (frequency_peaks[i].nmol == 0) frequency_peaks[i].nmol = nmol;
            else match = (match && (frequency_peaks[i].nmol == nmol));
          }
        }
        if (!match || frequency_peaks[i].nmol <= 1)
          frequency_peaks.erase(frequency_peaks.begin()+i);

      }
      for (int i = 0; i < frequency_peaks.size(); i++)
      {
        output.logTab(1,VERBOSE,"Frequency = " + ivtos(frequency_peaks[i].site) + " height=" + dtos(frequency_peaks[i].height));
      }
      output.logBlank(VERBOSE);
      if (frequency_peaks.size())
      {
        output.logTab(1,LOGFILE,"Frequency Top = " + ivtos(frequency_peaks[0].site) + " height=" + dtos(frequency_peaks[0].height));
        PTNCS_NMOL.FrequencyTopSite = frequency_peaks[0].site;
        PTNCS_NMOL.FrequencyTopPeak = frequency_peaks[0].height;
      }

      if (frequency_peaks.size())
      {
        PTNCS.ANALYSIS.clear();
        //need to find the relationship between the frequency peaks and the vectors in the Patterson
        //frequency peaks will relate to commensurate modulations
        //left overs will be designated NMOL 2
        for (int p = 0; p < expanded_non_origin_peaks.size(); p++)
        {
          PHASER_ASSERT(expanded_non_origin_peaks[p].size());
          output.logTab(1,TESTING,"Origin peak #" + itos(p));
          data_nmol best_analysis(expanded_non_origin_peaks[p][0]);
          dvect3 orthvec = UnitCell::doFrac2Orth(expanded_non_origin_peaks[p][0].vector_frac);
          best_analysis.shortdist = std::sqrt(orthvec*orthvec);
          output.logTab(1,TESTING,"Initial best");
          output.logTab(0,TESTING,best_analysis.log(PTNCS.TRA.PATT_DIST,PTNCS.COMM));
          for (int q = 0; q < expanded_non_origin_peaks[p].size(); q++)
          {
            output.logTab(1,TESTING,"Origin peak expanded #" + itos(q));
            for (int i = 0; i < frequency_peaks.size(); i++)
            {
              std::vector<int> factors = factors_of(frequency_peaks[i].nmol); //includes maxNMOL
              output.logTab(1,TESTING,"Freq peak #" + itos(i) + "=" + ivtos(frequency_peaks[i].site) + " factors = " + ivtos(factors));
              //std::vector<int> factors(1,frequency_peaks[0].nmol); //includes maxNMOL
              for (int f = 0; f < factors.size(); f++)
              {
                data_nmol this_analysis = expanded_non_origin_peaks[p][q]; //inits to max, 1
                this_analysis.NMOL = phaser::round((1./factors[f])*frequency_peaks[i].nmol);
                for (int s = 0; s < 3; s++)
                  this_analysis.freq.site[s] = (1./factors[f])*frequency_peaks[i].site[s];
                this_analysis.freq.height = frequency_peaks[i].height;
                this_analysis.freq.distance = frequency_peaks[i].distance;
                dvect3 frac(0,0,0);
                for (int s = 0; s < 3; s++)
                {
                  frac[s] = this_analysis.NMOL*this_analysis.vector_frac[s];
                  //frac[s] = this_analysis.freq[s]*this_analysis.vector_frac[s];
                  frac[s] = std::fabs(std::fabs(frac[s] - round(frac[s])));
                }
                bool good(true);//make sure all non-zero elements are multiplied
                output.logTab(3,TESTING,"factor #" + itos(f) + " freq = " + ivtos(this_analysis.freq.site));
                output.logTab(3,TESTING,"factor #" + itos(f) + " vector = " + dvtos(this_analysis.vector_frac));
                for (int s = 0; s < 3; s++)
                if ((this_analysis.freq.site[s] && !this_analysis.vector_frac[s]) ||
                     (this_analysis.vector_frac[s] && !this_analysis.freq.site[s]))
                {
                  good = false;
                }
                output.logTab(2,TESTING,"factor #" + itos(f) + " good = " + btos(good));
                if (!good) break;
                this_analysis.tolfrac = std::sqrt(frac*frac);
                dvect3 orth = UnitCell::doFrac2Orth(frac);
                this_analysis.tolorth = std::sqrt(orth*orth);
                //now store the extra values that will be used for commensurate tests
                //correct vector so that is an exact cell divisor, ie tol is zero
                PHASER_ASSERT(this_analysis.NMOL > 1);
                for (int repeat = 1; repeat < this_analysis.NMOL; repeat++)
                {
                  dvect3 frac = double(repeat)*this_analysis.exact_vector();
                  for (int s = 0; s < 3; s++)
                  { while (frac[s] < 0) frac[s]++; while (frac[s] >= 1) frac[s]--; }
                  dvect3 orth = UnitCell::doFrac2Orth(frac);
                  double dist = std::sqrt(orth*orth);
                  this_analysis.shortdist = std::min(dist,this_analysis.shortdist); //initialized to max
                  double mapval = cctbx::maptbx::eight_point_interpolation<double,double>(real_map_const_ref,frac)/Patterson_max;
                  //see of there is a peak within cooee of the vector multiples
                  for (unsigned j = 0; j < peak_sites.size(); j++) //truncated to interesting
                  {
                    double height_j = peak_heights[j];
                    dvect3 site_j = peak_sites[j];
                    if (height_j > mapval) //interpolation may be better already
                    {
                      for (unsigned isym = 0; isym < NSYM; isym++)
                      {
                        dvect3 frac_j = PATT.doSymXYZ(isym,site_j);
                        //first try  without cell translations first
                        dvect3 fracCell_j = frac_j;
                        dvect3 orth_j = doFrac2Orth(fracCell_j);
                        double dist_to_orth(std::sqrt(std::fabs((orth_j-orth)*(orth_j-orth))));
                        if (dist_to_orth < PTNCS.COMM.TOLO && height_j > mapval)
                        {
                          mapval = height_j;
                          goto end_loop; //because peaks are sorted highest first, this will be the best on offer
                        }
                        dvect3 cellTrans;
                        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
                        for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
                        for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
                        if (cellTrans != dvect3(0,0,0))//above
                        {
                          dvect3 fracCell_j = frac_j + cellTrans;
                          dvect3 orth_j = doFrac2Orth(fracCell_j);
                          double dist_to_orth(std::sqrt(std::fabs((orth_j-orth)*(orth_j-orth))));
                          if (dist_to_orth < PTNCS.COMM.TOLO && height_j > mapval)
                          {
                            mapval = height_j;
                            goto end_loop; //because peaks are sorted highest first
                          }
                        }
                      }
                    }
                  }
                  end_loop:
                  this_analysis.add_vector(frac,mapval);
                }
                output.logTab(1,TESTING,"This analysis");
                output.logTab(0,TESTING,this_analysis.log(PTNCS.TRA.PATT_DIST,PTNCS.COMM));
                sort_data_nmol lt(PTNCS.COMM); //rather than operator<, for passing to std::sort
                output.logTab(1,TESTING,"values: best="  + btos(lt(best_analysis,this_analysis)) + " nmol-limit=" + btos(!PTNCS.MAXNMOL || this_analysis.NMOL <= PTNCS.MAXNMOL) + " commensurate=" + btos(this_analysis.commensurate(PTNCS.TRA.PATT_DIST,PTNCS.COMM)));
                if (lt(best_analysis,this_analysis) &&
                    (!PTNCS.MAXNMOL || this_analysis.NMOL <= PTNCS.MAXNMOL) &&
                    this_analysis.commensurate(PTNCS.TRA.PATT_DIST,PTNCS.COMM))
                {
                  best_analysis = this_analysis;
                  output.logTab(1,TESTING,"Best analysis");
                  output.logTab(0,TESTING,best_analysis.log(PTNCS.TRA.PATT_DIST,PTNCS.COMM));
                }
              }
            }
          }
          output.logTab(1,TESTING,"Add analysis to PTNCS array");
          output.logTab(0,TESTING,best_analysis.log(PTNCS.TRA.PATT_DIST,PTNCS.COMM));
          PTNCS.ANALYSIS.push_back(best_analysis);
        }

        std::sort(PTNCS.ANALYSIS.begin(),PTNCS.ANALYSIS.end(),sort_data_nmol(PTNCS.COMM));
        std::reverse(PTNCS.ANALYSIS.begin(),PTNCS.ANALYSIS.end());
        for (int n = 0; n < PTNCS.ANALYSIS.size(); n++)
        PHASER_ASSERT(PTNCS.ANALYSIS[n].NMOL);

        {
          outStream where = DEBUG;
          output.logBlank(where);
          output.logUnderLine(where,"Pre-filtering Translational NCS Analysis Table");
          output.logTab(1,where,"C*=Commensurate Modulation");
          output.logTabPrintf(1,where,"%2s %4s  %5s %-23s  %5s %11s  %-8s %-8s %-10s\n",
            "C*","NMOL","%Patt","tNCS-vector","%Freq","Frequencies","tol-orth","tol-frac","repeat-dist");
          for (int n = 0; n < PTNCS.ANALYSIS.size(); n++)
          {
            if (PTNCS.ANALYSIS[n].commensurate(PTNCS.TRA.PATT_DIST,PTNCS.COMM))
            output.logTabPrintf(1,where,"%2s %4i  %5.1f %+6.4f %+6.4f %+6.4f  %5.1f %3i %3i %3i  %8.2f %8.6f %8.1fA\n",
              "T ",
              PTNCS.ANALYSIS[n].NMOL,
              PTNCS.ANALYSIS[n].vector_height,
              PTNCS.ANALYSIS[n].vector_frac[0], PTNCS.ANALYSIS[n].vector_frac[1], PTNCS.ANALYSIS[n].vector_frac[2],
              PTNCS.ANALYSIS[n].freq.height,
              PTNCS.ANALYSIS[n].freq.site[0], PTNCS.ANALYSIS[n].freq.site[1], PTNCS.ANALYSIS[n].freq.site[2],
              PTNCS.ANALYSIS[n].tolorth,
              PTNCS.ANALYSIS[n].tolfrac,
              PTNCS.ANALYSIS[n].shortdist
              );
            else
            output.logTabPrintf(1,where,"%2s %4i  %5.1f %+6.4f %+6.4f %+6.4f  %5s %3s %3s %3s  %8s %8s %8.1fA\n",
              "F ",
              PTNCS.ANALYSIS[n].NMOL,
              PTNCS.ANALYSIS[n].vector_height,
              PTNCS.ANALYSIS[n].vector_frac[0], PTNCS.ANALYSIS[n].vector_frac[1], PTNCS.ANALYSIS[n].vector_frac[2],
              "---", "---", "---", "---", "---", "---",
              PTNCS.ANALYSIS[n].shortdist
              );
          }
          output.logBlank(where);
        }

        //erase duplicate NMOLs in analysis if they are commensurate
        //NMOL = 2 is for non-commensurate
        //  Don't do this!! see 3teq, different commensurate modulations
        /*
        for (int n = 0; n < PTNCS.ANALYSIS.size(); n++)
        if (PTNCS.ANALYSIS[n].commensurate())
        {
          for (int m = PTNCS.ANALYSIS.size()-1; m > n; m--)
          if (PTNCS.ANALYSIS[m].commensurate())
          {
            if (PTNCS.ANALYSIS[n].tolorth == PTNCS.ANALYSIS[m].tolorth ||
                PTNCS.ANALYSIS[n].NMOL == PTNCS.ANALYSIS[m].NMOL)
              PTNCS.ANALYSIS.erase(PTNCS.ANALYSIS.begin()+m);
          }
        }
        */

        outStream where(LOGFILE);
        if (PTNCS.ANALYSIS.size())
        {
          output.logSectionHeader(where,"NMOL ANALYSIS");
          output.logBlank(where);
          output.logTab(1,where,"Estimate NMOL from Patterson spatial frequency analysis");
          {
          output.logTab(1,where,"Four measures of reliability of the NMOL estimate are given");
          output.logTab(1,where,"(1) %Top:");
          output.logTab(2,where,"Strength of the frequency analysis signal");
          output.logTab(3,where,"Indicator is strong when %Top > " + itos(int(PTNCS.COMM.PEAK*100)));
          output.logTab(1,where,"(2) tolfrac:");
          output.logTab(2,where,"The maximum fractional difference (in x, y, or z) between the freq*tNCS-vector and an exact unit cell repeat");
          output.logTab(3,where,"Indicator is strong when tolfrac < " + dtos(PTNCS.COMM.TOLF));
          output.logTab(1,where,"(3) tolorth:");
          output.logTab(2,where,"The length of the vector difference between freq*tncs-vector and a unit cell repeat in orthogonal Angstroms");
          output.logTab(3,where,"Indicator is strong when tolorth < " + dtos(PTNCS.COMM.TOLO));
          output.logTab(1,where,"(4) complete:");
          output.logTab(2,where,"Most tNCS vectors in tNCS must be present");
          }
          output.logBlank(where);
          output.logUnderLine(where,"Translational NCS Analysis Table");
          output.logTab(1,where,"C*=Commensurate Modulation");
          output.logTabPrintf(1,where,"%2s %4s  %5s %-23s  %5s %11s  %-8s %-8s %-10s\n",
            "C*","NMOL","%Patt","tNCS-vector","%Freq","Frequencies","tol-orth","tol-frac","repeat-dist");
          for (int n = 0; n < PTNCS.ANALYSIS.size(); n++)
          {
          //  dvect3 orth = UnitCell::doFrac2Orth(PTNCS.ANALYSIS[n].vector_frac);
          //  double shortdist = std::sqrt(orth*orth);
            if (PTNCS.ANALYSIS[n].commensurate(PTNCS.TRA.PATT_DIST,PTNCS.COMM))
            output.logTabPrintf(1,where,"%2s %4i  %5.1f %+6.4f %+6.4f %+6.4f  %5.1f %3i %3i %3i  %8.2f %8.6f %8.1fA\n",
              "T ",
              PTNCS.ANALYSIS[n].NMOL,
              PTNCS.ANALYSIS[n].vector_height,
              PTNCS.ANALYSIS[n].vector_frac[0], PTNCS.ANALYSIS[n].vector_frac[1], PTNCS.ANALYSIS[n].vector_frac[2],
              PTNCS.ANALYSIS[n].freq.height,
              PTNCS.ANALYSIS[n].freq.site[0], PTNCS.ANALYSIS[n].freq.site[1], PTNCS.ANALYSIS[n].freq.site[2],
              PTNCS.ANALYSIS[n].tolorth,
              PTNCS.ANALYSIS[n].tolfrac,
              PTNCS.ANALYSIS[n].shortdist
              );
            else
            output.logTabPrintf(1,where,"%2s %4i  %5.1f %+6.4f %+6.4f %+6.4f  %5s %3s %3s %3s  %8s %8s %8.1fA\n",
              "F ",
              PTNCS.ANALYSIS[n].NMOL,
              PTNCS.ANALYSIS[n].vector_height,
              PTNCS.ANALYSIS[n].vector_frac[0], PTNCS.ANALYSIS[n].vector_frac[1], PTNCS.ANALYSIS[n].vector_frac[2],
              "---", "---", "---", "---", "---", "---",
              PTNCS.ANALYSIS[n].shortdist
              );
          }
          output.logBlank(where);
          output.logSectionHeader(where,"RESULTS");
    //decision time for NMOL
          if (PTNCS.ANALYSIS.size() > 1)
          {
            output.logBlank(where);
            output.logTab(1,where,"Large non-origin Patterson peaks indicate that multiple tNCS vectors are present");
            output.logTab(1,where,"Top tNCS correction will be applied");
            output.logTab(2,where,"To change tNCS correction, enter a single TNCS vector and NMOL manually");
            for (int n = 0; n < PTNCS.ANALYSIS.size(); n++)
            {
              output.logTab(3,where,itos(n+1) + ": TNCS TRA VECTOR " + dvtos(PTNCS.ANALYSIS[n].vector_frac));
              output.logTab(3,where,"   TNCS NMOL " + itos(PTNCS.ANALYSIS[n].NMOL));
            }
            output.logBlank(where);
            //use below if tncs is turned off with multiple present
            //PTNCS.USE = false;
            //PTNCS.TRA.VECTOR_SET = true; //turn off z-score checks
          }
          if (PTNCS.ANALYSIS.size() >= 1)//strong modulation, or top and only top is commensurate
          {
            output.logTab(1,where,"Strong modulation present");
            if (PTNCS.USE)
            {
              PTNCS.TRA.VECTOR_SET = true; //so that it finds ROT next
              if (PTNCS.NMOL)
              {
                output.logTab(1,where,"Input NMOL: " + itos(PTNCS.NMOL));
                if (PTNCS.NMOL == 1)
                {
                  PTNCS.TRA.VECTOR_SET = false;
                  output.logWarning(SUMMARY,"tNCS is present but correction factors NOT applied");
                }
                else if (PTNCS.NMOL == PTNCS.ANALYSIS[0].NMOL)
                {
                  output.logTab(1,where,"Input NMOL matches NMOL from analysis");
                  PTNCS.TRA.VECTOR = PTNCS.ANALYSIS[0].vector_frac;
                  output.logTab(2,where,"tNCS vector = " + dvtos(PTNCS.TRA.VECTOR));
                  output.logTab(2,where,"tNCS NMOL = " + itos(PTNCS.NMOL));
                  output.logBlank(where);
                }
                else
                {
                  output.logTab(1,where,"Input NMOL does not match NMOL from analysis");
                  output.logTab(1,where,"Please check:");
                  output.logTab(2,where,"tNCS tolerance indicators");
                  output.logTab(2,where,"Cell Content Analysis");
                  output.logBlank(where);
                  PTNCS.TRA.VECTOR = PTNCS.ANALYSIS[0].vector_frac; //still need to set the vector!
                  output.logWarning(SUMMARY,"Input NMOL (" + itos(PTNCS.NMOL) + ") does not match NMOL (" + itos(PTNCS.ANALYSIS[0].NMOL) + ") from analysis: Please check tncs analysis and cell content analysis indicators to confirm NMOL");
                }
              }
              else //strong commensurate modulation
              {
                output.logTab(1,where,"NMOL not input");
                PTNCS.NMOL = PTNCS.ANALYSIS[0].NMOL;
                PTNCS.TRA.VECTOR = PTNCS.ANALYSIS[0].vector_frac;
                output.logTab(1,where,"NMOL and VECTOR set to values from analysis");
                output.logTab(2,where,"tNCS vector = " + dvtos(PTNCS.TRA.VECTOR));
                output.logTab(2,where,"tNCS NMOL = " + itos(PTNCS.NMOL));
                output.logBlank(where);
                output.logAdvisory(SUMMARY,"NMOL (" + itos(PTNCS.NMOL) + ") set by analysis. If structure solution fails consider other tNCS");
              }
            }
          }
        }
        else
        {
          output.logTab(1,where,"No peaks in analysis");
          output.logBlank(where);
        }
      }//freq peaks
      }//memory
    }//non origin peaks
  }
  if (PTNCS.TRA.VECTOR_SET && !PTNCS.USE)
  {
    output.logWarning(SUMMARY,"tNCS is present but correction factors NOT applied\n" + std::string(9,' ') + "Enter tNCS vector manually to activate tNCS correction");
  }
}

void Epsilon::initGFN(Output& output)
{
  if (!PTNCS.GFUNCTION_RADIUS)
  {
    cctbx::uctbx::unit_cell cctbxUC(getCctbxUC());
    floatType mol_volume(cctbxUC.volume()/(2*PTNCS.NMOL)/NSYMM); //25% of asu volume for NMOL=2
    PTNCS.GFUNCTION_RADIUS = std::pow(3*mol_volume/4/scitbx::constants::pi,1./3.);
    output.logTab(1,LOGFILE,"Molecular Radius for G-function = " + dtos(PTNCS.GFUNCTION_RADIUS) + " Angstroms");
  }
}

void Epsilon::findROT(af::shared<protocolPtr> MACRO_TNCS,Output& output)
{
  if (PTNCS.REFINED || !PTNCS.USE) return; //read file is REFINED
  output.logSectionHeader(LOGFILE,"TRANSLATIONAL NCS REFINEMENT");
  cctbx::uctbx::unit_cell cctbxUC(getCctbxUC());
  cctbx::sgtbx::space_group cctbxSG(getCctbxSG());
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
  cctbx::sgtbx::space_group cctbxPattSG(cctbxSG.build_derived_patterson_group());
  cctbx::sgtbx::space_group_type PattInfo(cctbxPattSG);
  SpaceGroup PATT(PattInfo.hall_symbol());
  cctbx::sgtbx::site_symmetry this_site_sym(cctbxUC,cctbxPattSG,PTNCS.TRA.VECTOR);
  int M = PATT.NSYMM/this_site_sym.multiplicity();
  if (M > 1 &&
      PTNCS.TRA.PERTURB)
    //  && PTNCS.NMOL == 2) //leave on the special position if there are more than two molecules, assume pseudo-centring operation
  {
    dvect3 before = PTNCS.TRA.VECTOR;
    PTNCS.TRA.VECTOR = this_site_sym.exact_site(); // Put on exact site before perturbing
    output.logUnderLine(LOGFILE,"Special Position Perturbation");
    output.logTab(1,DEBUG,"NCS translation before placing on special position = " + dvtos(before));
    output.logTab(1,LOGFILE,"NCS translation on special position with M = " + itos(M));
    output.logTab(2,LOGFILE,"NCS translation vector = " + dvtos(PTNCS.TRA.VECTOR));
    output.logTab(1,LOGFILE,"Starting point for refinement will be shifted from special position.");
    scitbx::vec3<std::string> direction("X","Y","Z");
    floatType mtz_lores = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(MILLER.const_ref())[0]);
    floatType mtz_hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(MILLER.const_ref())[1]);
    floatType hires = std::max(HIRES,mtz_hires);
    floatType lores = std::min(LORES,mtz_lores);
    int countr(0);
    for (int r = 0; r < NREFL; r++)
    if (selected[r] &&
        cctbx::uctbx::d_star_sq_as_d(cctbxUC.d_star_sq(MILLER[r])) >= hires &&
        cctbx::uctbx::d_star_sq_as_d(cctbxUC.d_star_sq(MILLER[r])) <= lores)
    {
      countr++;
    }
    output.logTabPrintf(1,LOGFILE,"Resolution of All Data (Number):      %6.2f %6.2f (%d)\n",mtz_hires,mtz_lores,NREFL);
    output.logTabPrintf(1,LOGFILE,"Resolution of Data (Number):         %6.2f %6.2f (%d)\n",hires,lores,countr);
    dvect3 perturb(hires/6/A(),hires/6/B(),hires/6/C());
    PTNCS.TRA.VECTOR -= perturb;
    for (int tra = 0; tra < 3; tra++)
      output.logTab(2,LOGFILE,"NCS translation has been perturbed in " + direction[tra]);
    output.logTab(2,LOGFILE,"NCS translation perturbed by (orthogonal angstroms): " + dvtos(doFrac2Orth(perturb),5,3));
    output.logTab(1,LOGFILE,"NCS translation has been set to " + dvtos(PTNCS.TRA.VECTOR));
    output.logBlank(LOGFILE);
  }
  patt_vector = PTNCS.TRA.VECTOR;
  initGFN(output);
  initROT(HIRES,output);
  unsigned nthreads = 1;
#pragma omp parallel
  {
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
#endif
  }
  refinedL.resize(PTNCS.ROT.SAMP_ANGLE.size());
  refinedR.resize(PTNCS.ROT.SAMP_ANGLE.size());
  refinedT.resize(PTNCS.ROT.SAMP_ANGLE.size());
  float2D refinedV(PTNCS.ROT.SAMP_ANGLE.size());
  float2D refinedE(PTNCS.ROT.SAMP_ANGLE.size());
  Bin refinedB;
// create nthreads output objects to collect text output from each thread
  std::vector<Output> omp_output;
  omp_output.resize(nthreads);
  for (int n = 0; n < nthreads; ++n)
  {
    // concurrent threads must not write to stdout
    omp_output[n].setLevel(output.getLevel());
    omp_output[n].setMute(!(nthreads == 1 && output.level(DEBUG)));
  }
  if (PTNCS.NMOL > 2)
  {
    //change default refinement to fix rotation
    for (unsigned p = 0 ; p < MACRO_TNCS.size(); p++)
      MACRO_TNCS[p]->setFIX(mact_rot,true);
  }
  //else use default refinement protocol
  output.logBlank(LOGFILE);
  for (unsigned p = 0 ; p < MACRO_TNCS.size(); p++)
  {
    output.logTab(1,LOGFILE,"Protocol cycle #"+ itos(p+1) + " of " + itos(MACRO_TNCS.size()));
    output.logTabPrintf(0,LOGFILE,"%s",MACRO_TNCS[p]->logfile().c_str());
    output.logBlank(LOGFILE);
  }
  PTNCS.ROT.SAMP_ANGLE.size() == 1 ?
    output.logTab(1,LOGFILE,"There is 1 tncs parameter set to refine"):
    output.logTab(1,LOGFILE,"There are " + itos(PTNCS.ROT.SAMP_ANGLE.size()) + " tncs parameter sets to refine");
  if (nthreads > 1)
    output.logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
  output.logProgressBarStart(LOGFILE,"Refining tNCS parameters",PTNCS.ROT.SAMP_ANGLE.size()/nthreads);
  int max_a = PTNCS.ROT.SAMP_ANGLE.size();
#pragma omp parallel for
  for (int a = 0; a < max_a; a++)
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
#endif
    omp_output[nthread].logUnderLine(LOGFILE,"TNCS REFINEMENT #" + itos(a+1) + " OF " + itos(max_a));
    RefineNCS refn(*this,PTNCS.ROT.SAMP_ANGLE[a]);
    omp_output[nthread].logUnderLine(VERBOSE,"Initial Parameters");
    refn.logNcsEpsn(VERBOSE,omp_output[nthread]);
    Minimizer minimizer;
    minimizer.run(refn,MACRO_TNCS,omp_output[nthread]);
    refinedL[a] = refn.getLikelihood();
    refinedR[a] = refn.getAngle();
    refinedT[a] = refn.getVector();
    refinedV[a] = refn.getVariances();
    refinedE[a] = refn.getNcsEpsfac();
    if (a == 0) refinedB = refn.getBins(); //does not alter with job
    if (nthread == 0) output.logProgressBarNext(LOGFILE);
  } //end openmp
  output.logProgressBarEnd(LOGFILE);

//firstprivate makes PTNCS local to thread
 // PTNCS.VAR.setRESO(refinedB.LoRes_array()); //does not alter with job


  for (int nthread = 0; nthread < nthreads; nthread++)
  {
    output += omp_output[nthread];
    if (!(nthreads == 1 && output.level(DEBUG))) output << omp_output[nthread];
  }
  //append and optionally print silenced results from the various threads to the master thread

  for (int a = 0; a < PTNCS.ROT.SAMP_ANGLE.size(); a++)
  if (refinedL[a] < best_likelihood || a == 0)
  {
    best_num = a;
    best_likelihood = refinedL[a];
    PTNCS.ROT.ANGLE = (refinedR[best_num]);
    PTNCS.TRA.VECTOR = (refinedT[best_num]);
    PTNCS.VAR.BINS = refinedV[a];
    PTNCS.EPSFAC = refinedE[a];
  }
  logTable(output);
  TncsVar tncsvar(PTNCS);
  output.logUnderLine(SUMMARY,"Translational NCS Refinement");
  output.logTab(1,SUMMARY,"Test rotation #" + itos(best_num+1));
  output.logTab(2,SUMMARY,"Final angle  : " + dvtos(refinedR[best_num],7,4));
  output.logTab(2,SUMMARY,"Final vector : " + dvtos(refinedT[best_num],7,4));
  output.logTab(2,SUMMARY,"tNCS D-values by resolution (low-high) : " +
                dtos(tncsvar.D(0),6,4) + " " + dtos(tncsvar.D(),6,4));
  output.logBlank(SUMMARY);
  RefineNCS refn(*this,PTNCS.ROT.ANGLE);
  bool print_loggraph(true);
  refn.logMoments(" after tNCS (best) refinement",LOGFILE,print_loggraph,output);
  MOMENTS = refn.getMoments();
  {
    int maxr = std::min(9,int(PTNCS.EPSFAC.size()));
    output.logBlank(DEBUG);
    output.logTab(1,DEBUG,"Epsfac for first " + itos(maxr) + " reflections");
    for (int r = 0; r < maxr; r++)
      output.logTab(1,DEBUG,"Reflection # " + itos(r+1) + " Epsfac= " + dtos(PTNCS.EPSFAC[r]));
    output.logBlank(DEBUG);
  }
  logEpsn(output);
}

void Epsilon::logTable(Output& output)
{
  if (PTNCS.ROT.SAMP_ANGLE.size() > 1)
  {
  output.logUnderLine(LOGFILE,"Table of Results of Refinements from Perturbed Starting Rotations");
  output.logTabPrintf(1,LOGFILE,"#  %-18s %-18s %-7s %-21s %-11s\n","Initial Rot\'n","Refined Rot\'n","(Angle)","Refined Translation","LLgain");
  RefineNCS refn(*this,dvect3(0,0,0));
  double minL = refn.targetFn();
  for (int a = 0; a < PTNCS.ROT.SAMP_ANGLE.size(); a++)
  {
    dmat33 ncsRmat(1,0,0,0,1,0,0,0,1);
    for (int dir = 0; dir < 3; dir++)
      ncsRmat = xyzRotMatDeg(dir,refinedR[a][dir])*ncsRmat;
    //convert to an angle about an axis
    floatType  ANGL = scitbx::rad_as_deg(scitbx::math::r3_rotation::axis_and_angle_from_matrix<floatType>(ncsRmat).angle_rad);
    output.logTabPrintf(1,LOGFILE,"%-2i  %+5.2f %+5.2f %+5.2f  %+5.2f %+5.2f %+5.2f (%5.2f) %+6.3f %+6.3f %+6.3f %10.3f %c\n",
                            a+1,
                            PTNCS.ROT.SAMP_ANGLE[a][0],PTNCS.ROT.SAMP_ANGLE[a][1],PTNCS.ROT.SAMP_ANGLE[a][2],
                            refinedR[a][0],refinedR[a][1],refinedR[a][2],
                            ANGL,
                            refinedT[a][0],refinedT[a][1],refinedT[a][2],
                            -refinedL[a]+minL,
                            a == best_num ? '*':' ');
  }
  output.logTab(1,LOGFILE,"Angle Perturbation Sampling = " + dtos(PTNCS.ROT.SAMPLING));
  output.logTab(1,LOGFILE,"Angle Perturbation Range = " + dtos(PTNCS.ROT.RANGE));
  output.logBlank(LOGFILE);
  }
}

void Epsilon::logEpsn(Output& output)
{
  if (!PTNCS.EPSFAC.size()) return;
  outStream where(LOGFILE);
  output.logUnderLine(where,"Histogram of Epsilon Factors");
  cctbx::sgtbx::space_group cctbxSG = getCctbxSG();
  int div(PTNCS.NMOL*10+1);
  unsigned1D histogram(div,0);
  unsigned   maxStars(0);
  floatType  min_epsfac(0),max_epsfac(0),mean_epsfac(0);
  for (int r = 0; r < PTNCS.EPSFAC.size(); r++)
  {
    min_epsfac = r ? std::min(PTNCS.EPSFAC[r],min_epsfac) : PTNCS.EPSFAC[r];
    max_epsfac = r ? std::max(PTNCS.EPSFAC[r],max_epsfac) : PTNCS.EPSFAC[r];
    mean_epsfac += PTNCS.EPSFAC[r];
    size_t iZ = std::max(0.0,round(PTNCS.EPSFAC[r]*10));
           iZ = std::min(iZ,histogram.size()-1);
    histogram[iZ]++;
    maxStars = std::max(maxStars,histogram[iZ]);
  }
  floatType columns = 50;
  floatType divStarFactor = maxStars/columns;
  int grad = divStarFactor*10; // maxStars/5
  output.logTab(2,where,"Minimum EpsFac = " + dtos(min_epsfac,4));
  output.logTab(2,where,"Maximum EpsFac = " + dtos(max_epsfac,4));
  output.logTab(2,where,"Mean    EpsFac = " + dtos(mean_epsfac/PTNCS.EPSFAC.size(),4) + " for "
      + itos(PTNCS.EPSFAC.size()) + " reflections");
  output.logBlank(where);
  output.logTabPrintf(1,where,"Mid EpsFac Bin Value\n");
  output.logTabPrintf(1,where," |    %-10d%-10d%-10d%-10d%-10d%-10d\n",0,grad,grad*2,grad*3,grad*4,grad*5);
  output.logTabPrintf(1,where," V    %-10s%-10s%-10s%-10s%-10s%-10s\n","|","|","|","|","|","|");
  for (unsigned h = 0; h < div; h++)
  {
    std::string stars;
    stars += dtos(h/10.0,1) + "  ";
    for (int star = 0; star < histogram[h]/divStarFactor; star++)
      stars += "*";
    if (histogram[h]) stars += " (" + itos(histogram[h]) + ")";
    output.logTab(1,where,stars);
  }
  output.logBlank(where);
}

af_float Epsilon::getEpsFac() const
{
  af_float tmp;
  for (int r = 0; r < PTNCS.EPSFAC.size(); r++)
    tmp.push_back(PTNCS.EPSFAC[r]);
  return tmp;
}

} //end namespace phaser
