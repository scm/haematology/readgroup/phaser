//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LlgcMapSitesClass__
#define __LlgcMapSitesClass__
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/af_atom.h>
#include <phaser/ep_objects/data_wave.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <set>

namespace phaser {

class restored_data
{
  public:
    restored_data() { atomtype = ""; zscore = occupancy = 0; }
    restored_data(std::string t,floatType z,floatType o) { atomtype = t; zscore = z; occupancy = o; }
    std::string atomtype;
    floatType zscore,occupancy;
};

class LlgcMapSites
{
  public:
    LlgcMapSites()
    { atoms.clear(); restored.clear(); anisotropic.clear(); }
    af_atom atoms;
    std::set<int> anisotropic;
    std::multimap<int,restored_data> restored;
    std::map<int,int> ptncs_pairs;
    void merge(LlgcMapSites& right)
    {
      atoms.insert(atoms.end(),right.atoms.begin(),right.atoms.end());
      restored.insert(right.restored.begin(),right.restored.end());
      anisotropic.insert(right.anisotropic.begin(),right.anisotropic.end());
      for (std::map<int,int>::iterator iter = right.ptncs_pairs.begin(); iter != right.ptncs_pairs.end(); iter++)
        ptncs_pairs[iter->first] = iter->second;
    }
    int nanisotropic()
    { return anisotropic.size(); }
    int nrestored()
    {
      std::set<int> unique_restored;
      typedef std::multimap<int,restored_data>::iterator I;
      for (I iter = restored.begin(); iter != restored.end(); iter++)
        unique_restored.insert(iter->first);
      return unique_restored.size();
    }
    int natoms()
    { return atoms.size(); }
    bool nchanged()
    { return (natoms() + nrestored() + nanisotropic()) > 0; }
    void selectTopSites(outStream,SpaceGroup,UnitCell,floatType,int,bool,Output&);
};

}//phaser

#endif
