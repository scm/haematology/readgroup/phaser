//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_COMPOSITION_Class__
#define __PHASER_COMPOSITION_Class__
#include <phaser/include/data_composition.h>
#include <phaser/include/data_bofac.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/io/Output.h>

namespace phaser {

class Composition : public data_composition
{
  public:
    Composition(data_composition COMPOSITION=data_composition()) : data_composition(COMPOSITION)
    { MAX_FRAC_SCAT = 0.99; }

  private:
    map_str_float    model_scattering;
    floatType        MAX_FRAC_SCAT;

  public:
    double total_scat(std::string,af::double6,std::string="",double=2.5);
    double total_mw(std::string,af::double6,std::string="",double=2.5);
    void set_modlid_scattering(map_str_pdb&);
    void increase_total_scat_if_necessary(std::string,af::double6,mr_solution,data_tncs,data_bofac,search_array=search_array());
    float1D get_search_bfactors(std::string,af::double6,floatType,mr_solution);
    void table(outStream,double,map_str_pdb&,data_bofac,Output&);
    void table(outStream,double,map_str_pdb,mr_set&,Output&);

    bool increased()      { return INCREASE > 1; }
    std::string warning() { return "Total search request exceeds scattering specified in composition. Composition increased to resolve the discrepancy."; }

    bool is_all_protein();
    bool is_all_nucleic();
};

} //phaser

#endif
