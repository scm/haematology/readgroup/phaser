//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Bin.h>
#include <phaser/src/Ensemble.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/between.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/lib/solTerm.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/index_span.h>
#include <phaser/mr_objects/rms_estimate.h>

namespace phaser {

//---------------------------
// Constructor from hklin F's
// --------------------------
Ensemble& Ensemble::setMAP(floatType HIRES,data_solpar& SOLPAR,Output& output)
{
  ENS_FROM_PDB = false;
  ENS_BIN = Bin(BIN);
  NREFL = 0;
  //-------------------------------------------------------------------------------------
  relative_wilsonB.resize(1,float1D(1,0));
  DV.init_vrms_single();
  output.logTab(1,LOGFILE,"MAP file: " + ENSEMBLE[0].basename());
  output.logTab(1,LOGFILE,"Map LABIN F = " + ENSEMBLE[0].LABI_F + " PHI = " + ENSEMBLE[0].LABI_P);
  output.logTab(1,LOGFILE,"Ensemble configured to resolution " + dtos(HIRES,5,2));
  //-------------------------------------------------------------------------------------

  output.logTab(1,VERBOSE,"Principal Rotation and Translation");
  output.logTabPrintf(2,VERBOSE,"Rotation: [% 5.3f % 5.3f % 5.3f]\n",PR(0,0),PR(0,1),PR(0,2));
  output.logTabPrintf(2,VERBOSE,"          [% 5.3f % 5.3f % 5.3f]\n",PR(1,0),PR(1,1),PR(1,2));
  output.logTabPrintf(2,VERBOSE,"          [% 5.3f % 5.3f % 5.3f]\n",PR(2,0),PR(2,1),PR(2,2));
  output.logTabPrintf(2,VERBOSE,"Translation: (% 6.3f % 6.3f % 6.3f)\n",PT[0],PT[1],PT[2]);

  PHASER_ASSERT(ENSEMBLE[0].filename() != "");
  PHASER_ASSERT(ENSEMBLE[0].LABI_F != "");
  PHASER_ASSERT(ENSEMBLE[0].LABI_P != "");

  CMtz::MTZ *mtzfrom = safe_mtz_get(ENSEMBLE[0].filename(),1);
  if (static_cast<int>(mtzfrom->mtzsymm.spcgrp) != 1)
  throw PhaserError(FATAL,"Space Group of Ensemble must be P1 (not "  + std::string(mtzfrom->mtzsymm.spcgrpname) + ")");

  CMtz::MTZCOL *mtzH(0),*mtzK(0),*mtzL(0),*Acol(0),*Pcol(0);
  mtzH = safe_mtz_col_lookup(mtzfrom,"H");
  mtzK = safe_mtz_col_lookup(mtzfrom,"K");
  mtzL = safe_mtz_col_lookup(mtzfrom,"L");
  Acol = safe_mtz_col_lookup(mtzfrom,ENSEMBLE[0].LABI_F);
  Pcol = safe_mtz_col_lookup(mtzfrom,ENSEMBLE[0].LABI_P);
  output.logBlank(LOGFILE);

  CMtz::MTZXTAL* xtal = CMtz::MtzSetXtal( mtzfrom, CMtz::MtzColSet( mtzfrom, Acol));
  floatType a(xtal->cell[0]);
  floatType b(xtal->cell[1]);
  floatType c(xtal->cell[2]);
  floatType alpha(xtal->cell[3]);
  floatType beta(xtal->cell[4]);
  floatType gamma(xtal->cell[5]);
  if (!between(alpha,90,1.0e-04) || !between(beta,90,1.0e-04) || !between(gamma,90,1.0e-04))
  throw PhaserError(FATAL,"Unit Cell of Ensemble must be orthogonal");
  dvect3 box(a,b,c);
  //do all calculations in terms of original unit cell,
  //SCALE does not come into binning as it must not change
  box *= CELL_SCALE;
  input_CELL.setUnitCell(box);
  input_CELL.logUnitCell(VERBOSE,output,false);

  floatType H(0),K(0),L(0),resolution(0);
  bool1D readMtzRefl(mtzfrom->nref,false);
  //the incoming HIRES is the HIRES of the data/input
  //the map has to be extended with an additional shell for interpolation
  //including allowance for the maximum CELL_SCALE
  miller::index<int> UNIT(1,1,1);
  miller.clear(); //critical
  floatType requestres = (1/(1/HIRES + input_CELL.S(UNIT)));
  requestres /= DEF_CELL_SCALE_MAX;
  for (int mtzr = 0; mtzr < mtzfrom->nref ; mtzr++)
  {
    H = mtzH->ref[mtzr];
    K = mtzK->ref[mtzr];
    L = mtzL->ref[mtzr];
    miller::index<int> HKL(H,K,L);
    resolution = input_CELL.reso(HKL);
    if (resolution >= requestres &&
        !mtz_isnan(Acol->ref[mtzr]) &&
        !mtz_isnan(Pcol->ref[mtzr]))
    {
      miller.push_back(HKL);
      readMtzRefl[mtzr] = true;
    }
  }
  cmplx1D Fcalc;
  Fcalc.resize(miller.size());
  output.logTab(1,DEBUG,"Number of miller::index<int> indices " + itos(miller.size()));

  miller::index_span index_span(miller.const_ref());
//abs_range() == maximum of abs(min()) and abs(max()) + 1.
  int hmax = index_span.abs_range()[0];
  int kmax = index_span.abs_range()[1];
  int lmax = index_span.abs_range()[2];
  output.logTabPrintf(1,DEBUG,"H K L limit: %d %d %d\n",hmax,kmax,lmax);
  sizeh =  2*hmax+1;
  sizek =  2*kmax+1;
  sizel =  lmax+1; //no anomalous
  resizeMaps();
  output.logBlank(DEBUG);

  //-------------------------------------------------------------------------------------
  output.logTab(1,DEBUG,"Setup Bins");
  cctbx::uctbx::unit_cell cctbxUC = input_CELL.getCctbxUC();
  floatType hires(DEF_LORES),lores(0);
  for (unsigned r = 0; r < miller.size(); r++)
  if (!(miller[r][0] == 0 && miller[r][1] == 0 && miller[r][2] == 0))
  { //exclusion gets around optimization bug where this fails despite internal checks in uctbx
    hires = std::min(hires,cctbxUC.d(miller[r]));
    lores = std::max(lores,cctbxUC.d(miller[r]));
  }
  ENS_BIN.setup(hires,lores,miller.size());
  output.logTab(2,DEBUG,"Number of bins " + itos(ENS_BIN.numbins()));
  output.logTab(2,DEBUG,"Resolution Range " + dtos(hires) + " - " + dtos(lores));
  unsigned1D numInBin(ENS_BIN.numbins(),0);
  for (int r = 0; r < miller.size() ; r++)
    numInBin[ENS_BIN.get_bin(input_CELL.S(miller[r]))]++;

  //Checks to see if any of the bins have no reflections
  //and makes sure there is at least one in every bin
  recount:
  if (ENS_BIN.numbins() != 1) //if == 1 then do nothing - assume 1 reflection!
    for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
      if (numInBin[ibin] == 0)
      {
        //reduce the number of bins by 1 and see if this helps
        int oneLessBin = ENS_BIN.numbins()-1;
        ENS_BIN.set_numbins(oneLessBin);
        ENS_BIN.setup(hires,lores,miller.size());
        unsigned1D numInBin = unsigned1D(ENS_BIN.numbins(),0);
        for (int r = 0; r < miller.size() ; r++)
          numInBin[ENS_BIN.get_bin(input_CELL.S(miller[r]))]++;
        output.logTab(2,DEBUG,"Number of bins reduced by 1 to " + itos(ENS_BIN.numbins()));
        goto recount;
      }
  //Store the binning
  int1D  rbin(miller.size());
  for (int ir = 0; ir < miller.size() ; ir++)
  {
    rbin[ir] = ENS_BIN.get_bin(input_CELL.S(miller[ir]));
    PHASER_ASSERT(rbin[ir] < ENS_BIN.numbins());
  }
  output.logBlank(DEBUG);

  //-------------------------------------------------------------------------------------
  output.logTab(1,DEBUG,"Normalization");
  output.logTab(2,DEBUG,"Structure factors (Fs) will be normalized (converted to Es)");
  //The Fs to Es calculation uses the stored binning
  //This is to avoid numerical instability
  float1D SumFsqr(ENS_BIN.numbins(),0);
  { //scope for r under Windows
    int r(0);
    for (int mtzr = 0; mtzr < readMtzRefl.size() ; mtzr++)
    if (readMtzRefl[mtzr])
    {
       SumFsqr[rbin[r]] += std::norm(std::polar(floatType(Acol->ref[mtzr]), scitbx::deg_as_rad(Pcol->ref[mtzr])));
       r++;
    }
  }

  //SigmaP is the weighted mean value of F**2/epsilon
  //Here epsilon is 1.0 (P1)
  float1D SigmaP(ENS_BIN.numbins(),0);
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    SigmaP[ibin] = SumFsqr[ibin]/numInBin[ibin];

  output.logTab(2,DEBUG,"Model Fs converted to Es");
  floatType unlikely_prob(1.0e-06);
  int num_unlikely(0);
  NEWALD = 1;
  { //scope for r under Windows
    int r(0);
    for (int mtzr = 0; mtzr < readMtzRefl.size() ; mtzr++)
    if (readMtzRefl[mtzr])
    {
      int ibin = rbin[r];
      cmplxType E(std::polar(floatType(Acol->ref[mtzr]), scitbx::deg_as_rad(Pcol->ref[mtzr])));
      E /= std::sqrt(SigmaP[ibin]);
     // int iewald = 0;
      Fcalc[r] = E;
      if (exp(-std::norm(E)) < unlikely_prob)
      {
        output.logTab(2,DEBUG,"Ec unlikely " + ivtos(miller[r]) + " " + ctos(E));
        num_unlikely++;
      }
      r++;
    }
  }
  floatType perc_unlikely = 100.*num_unlikely/readMtzRefl.size();
  if (perc_unlikely > 10) output.logWarning(LOGFILE,"Percentage unlikely reflections in Ensemble MAP = " + dtos(perc_unlikely));
  output.logTab(2,DEBUG,"Percent Ec unlikely = " + dtos(perc_unlikely) + "%");
  output.logBlank(DEBUG);

  CMtz::MtzFree(mtzfrom);
  PHASER_ASSERT(Fcalc.size());
  PHASER_ASSERT(miller.size());
  output.logUnderLine(DEBUG,"Calculation of Statistical Weighting");
  std::vector< TNT::Vector<floatType> > ModelWt;
  std::vector< TNT::Vector<floatType> > DLuz;
  std::vector< TNT::Fortran_Matrix<floatType> >  CorMat;
  ModelWt.resize(ENS_BIN.numbins());
  DLuz.resize(ENS_BIN.numbins());
  CorMat.resize(ENS_BIN.numbins());
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    ModelWt[ibin].newsize(1); //only one set of "models" - the map
    DLuz[ibin].newsize(1);
    CorMat[ibin].newsize(1,1);
  }
  std::vector< TNT::Fortran_Matrix<floatType> >  pseudoCorMatInv(ENS_BIN.numbins());
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    pseudoCorMatInv[ibin].newsize(1,1);
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    pseudoCorMatInv[ibin](1,1) = 1; //only one map, and it is correlated with itself

  //-------------------------------------------------------------------------------------
  output.logEllipsisStart(DEBUG,"Calculate weights");
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    /*
      Four-parameter Sigma(A) curve given by
      Sigma(A) = sqrt( fp*(1-fs*exp(-Bs*stlsq)) ) *
         exp(-8*Pi**2/3 * Sigma(X)**2 * stlsq)
      where fp = fraction of ordered protein in model
            fs = fraction of cell occupied by solvent
            Bs = B-factor to smear disordered solvent (typically 50-200)
            rmsx = e.s.d. of atoms in model
      stlsq = Ssqr/4 (because Ssqr=1/d**2)
    */
    floatType Ssqr = 1/fn::pow2(ENS_BIN.MidRes(ibin));
    DLuz[ibin] = solTerm(Ssqr,SOLPAR)*DLuzzati(Ssqr,DV[0].fixed);
    if (fabs(DLuz[ibin](1)) < 1.0e-20)
    throw PhaserError(FATAL,"DLuz essentially zero");

 // Without accounting for case when same pdb entered more than once, the code is -
 // ModelWt = Lapack_LU_linear_solve(pseudoCorMat[ibin],DLuz[ibin]);
 // if (ModelWt.size() == 0) throw PhaserError(FATAL,"Singular Correlation Matrix");

    ModelWt[ibin] = pseudoCorMatInv[ibin]*DLuz[ibin];
    if (fabs(ModelWt[ibin](1)) < 1.0e-20)
    throw PhaserError(FATAL,"ModelWt essentially zero");
  }
  output.logEllipsisEnd(DEBUG);

  //-------------------------------------------------------------------------------------
  output.logEllipsisStart(DEBUG,"Make negative variances positive");
  float1D Covariance(ENS_BIN.numbins());
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    Covariance[ibin] = TNT::dot_prod(ModelWt[ibin],DLuz[ibin]);
  output.logEllipsisEnd(DEBUG);

//------------------------------------------------------------
// OK happy with the weights
  resizeMaps();

  floatType WeightedV(0),meanEsqr(0);
  cmplxType WeightedE,PhaseShift;
  floatType ZERO(0),TWO(2);
  cmplxType TWOPII(cmplxType(ZERO,TWO)*scitbx::constants::pi);
  int numTot(0);
  output.logEllipsisStart(DEBUG,"Apply phase shift to centre density and store");
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    for (unsigned r = 0; r < miller.size(); r++)
    if (rbin[r] == ibin)
    {
      cmplxType E = Fcalc[r];
      meanEsqr += std::norm(E);
      numTot++;
      //apply phase shift to centre density on origin for molecular transform interpolation
      PhaseShift = std::exp(TWOPII*(input_CELL.doOrth2Frac(PT)*miller[r]));
      WeightedE = ModelWt[ibin](1)*E*PhaseShift;
      setE(miller[r][0],miller[r][1],miller[r][2],WeightedE);
    }
    WeightedV = Covariance[ibin];
    setV(ibin,WeightedV); // V is now a covariance
  }
  output.logTab(2,DEBUG,"Overall mean value of E^2 " + dtos(meanEsqr/numTot));
  output.logEllipsisEnd(DEBUG);

  output.logBlank(DEBUG);

  //-------------------------------------------------------------------------------------
  output.logUnderLine(DEBUG,"Check mean values of weighted E^2s");
  float1D normalization(ENS_BIN.numbins());
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    floatType SumFsqr(0),numInBin(0);
    for (int r = 0; r < Fcalc.size() ; r++)
    if (rbin[r] == ibin)
    {
      SumFsqr += std::norm(Fcalc[r]);
      numInBin++;
    }
    normalization[ibin] = SumFsqr/numInBin;
  }
  unsigned WIDTH(20),wbin(0);
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    if (wbin == 0)
      output.logTabPrintf(1,DEBUG,"Bins %3d-%3d: ",ibin+1,std::min(ibin+WIDTH,ENS_BIN.numbins()));
    output.logTabPrintf(0,DEBUG," %3.1f",normalization[ibin]);
    if (wbin++ == WIDTH)
    {
      wbin = 0;
      output.logTabPrintf(0,DEBUG," \n");
    }
  }
  output.logTabPrintf(0,DEBUG," \n");
  output.logBlank(DEBUG);

  if (flatMap()) throw PhaserError(FATAL,"The input map in file \"" + ENSEMBLE[0].basename() + "\" is flat");
  return *this;
} // Constructor from hklin (map)

}//phaser
