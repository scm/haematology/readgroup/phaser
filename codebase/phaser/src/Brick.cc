//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Brick.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>

namespace phaser {

Brick::Brick(bool frac,dvect3 bmin,dvect3 bmax,UnitCell& uc)
{
  dvect31D corners(8);
  corners[0] = bmin;
  corners[1] = dvect3(bmin[0],bmin[1],bmax[2]);
  corners[2] = dvect3(bmin[0],bmax[1],bmin[2]);
  corners[3] = dvect3(bmax[0],bmin[1],bmin[2]);
  corners[4] = dvect3(bmin[0],bmax[1],bmax[2]);
  corners[5] = dvect3(bmax[0],bmin[1],bmax[2]);
  corners[6] = dvect3(bmax[0],bmax[1],bmin[2]);
  corners[7] = bmax;
  omin = omax = frac ? uc.doFrac2Orth(corners[0]) : corners[0];
  fmin = fmax = frac ? corners[0] : uc.doOrth2Frac(corners[0]);
  for (int c = 1; c < 8; c++)
    for (int i = 0; i < 3; i++)
    {
      omin[i] = std::min(omin[i],frac ? uc.doFrac2Orth(corners[c])[i] : corners[c][i]);
      omax[i] = std::max(omax[i],frac ? uc.doFrac2Orth(corners[c])[i] : corners[c][i]);
      fmin[i] = std::min(fmin[i],frac ? corners[c][i] :uc.doOrth2Frac(corners[c])[i]);
      fmax[i] = std::max(fmax[i],frac ? corners[c][i] :uc.doOrth2Frac(corners[c])[i]);
    }
}

dvect3 Brick::fracMin() { return fmin; }
dvect3 Brick::fracMax() { return fmax; }
dvect3 Brick::orthMin() { return omin; }
dvect3 Brick::orthMax() { return omax; }
floatType Brick::orthMin(int i) { return omin[i]; }
floatType Brick::orthMax(int i) { return omax[i]; }

bool Brick::isPlaneX(floatType orthDist)
{
  dvect3 orthSamp(orthDist,std::sqrt(3./4.)*orthDist,std::sqrt(2./3.)*orthDist);
  return (orthMax(0)-orthMin(0))/orthSamp[0] < 1.0;
}

bool Brick::isPlaneY(floatType orthDist)
{
  dvect3 orthSamp(orthDist,std::sqrt(3./4.)*orthDist,std::sqrt(2./3.)*orthDist);
  return (orthMax(1)-orthMin(1))/orthSamp[1] < 1.0;
}

bool Brick::isPlaneZ(floatType orthDist)
{
  dvect3 orthSamp(orthDist,std::sqrt(3./4.)*orthDist,std::sqrt(2./3.)*orthDist);
  return (orthMax(2)-orthMin(2))/orthSamp[2] < 1.0;
}

bool Brick::isLineX(floatType orthDist)
{ return (isPlaneY(orthDist) && isPlaneZ(orthDist)); }

bool Brick::isLineY(floatType orthDist)
{ return (isPlaneX(orthDist) && isPlaneZ(orthDist)); }

bool Brick::isLineZ(floatType orthDist)
{ return (isPlaneX(orthDist) && isPlaneY(orthDist)); }

bool Brick::isPoint(floatType orthDist)
{ return (isPlaneX(orthDist) && isPlaneY(orthDist) && isPlaneZ(orthDist)); }


} //phaser
