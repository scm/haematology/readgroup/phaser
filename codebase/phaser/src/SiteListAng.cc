//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/SiteListAng.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/round.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rotdist.h>
#include <phaser/io/Errors.h>

namespace phaser {

// --------
// concrete
// --------

int1D SiteListAng::getClusters()
{
  int1D values(siteList.size(),0);
  if (!PEAKS.CLUSTER) return values;
  for (int i = 0; i < siteList.size(); i++)
    values[i] =  siteList[i].clusterNr+1;  //must be non-zero
  return values;
}

unsigned SiteListAng::numSelected()
{
  unsigned num_selected(0);
  for (int i = 0; i < siteList.size(); i++)
    if (siteList[i].selected) num_selected++;
  return num_selected;
}
unsigned  SiteListAng::Size()
{ return siteList.size(); }

void  SiteListAng::Erase(int i,int j)
{
  siteList.erase(siteList.begin() + i,siteList.begin() + j);
}

void  SiteListAng::Copy(int i,int j)
{
  PHASER_ASSERT(j <= i); // copy up list only
  siteList[j] = siteList[i];
}

bool      SiteListAng::isSelected(unsigned i)
{ return siteList[i].selected; }

bool      SiteListAng::isTopSite(unsigned i)
{ return siteList[i].topsite; }

bool      SiteListAng::isDeep(unsigned i)
{ return siteList[i].deep; }

floatType SiteListAng::getValue(unsigned i)
{ return siteList[i].value; }

float1D SiteListAng::getValues()
{
  float1D values(siteList.size());
  for (int i = 0; i < siteList.size(); i++)
    values[i] =  siteList[i].value;
  return values;
}

float1D SiteListAng::getSignals()
{
  float1D zscore(siteList.size());
  for (int i = 0; i < siteList.size(); i++)
    zscore[i] =  siteList[i].zscore;
  return zscore;
}

bool1D SiteListAng::getDeep()
{
  bool1D deep(siteList.size());
  for (int i = 0; i < siteList.size(); i++)
  {
    deep[i] = siteList[i].deep;
  }
  return deep;
}

std::vector<dvect3> SiteListAng::getEuler()
{
  std::vector<dvect3> eulers(siteList.size());
  for (int i = 0; i < siteList.size(); i++)
    eulers[i] =  siteList[i].euler;
  return eulers;
}

unsigned  SiteListAng::getNum(unsigned i)
{ return siteList[i].num; }

floatType SiteListAng::getSignal(unsigned i)
{ return siteList[i].zscore; }

void      SiteListAng::setNum(unsigned i,unsigned n)
{ siteList[i].num = n; }

void      SiteListAng::setValue(unsigned i,floatType v)
{ siteList[i].value = v; }

void      SiteListAng::setTopSite(unsigned i,bool b)
{ siteList[i].topsite = b; }

void     SiteListAng::setDeep(unsigned i,bool b)
{ siteList[i].deep = b; }

void      SiteListAng::setSelected(unsigned i,bool b)
{ siteList[i].selected = b; }

void      SiteListAng::archiveValue(unsigned i)
{ siteList[i].archiveValue(); }

void SiteListAng::push_back(angsite& s)
{ siteList.push_back(s); }

void SiteListAng::Resize(unsigned maxsize)
{ siteList.resize(maxsize); }

void SiteListAng::Reserve(unsigned maxsize)
{ siteList.reserve(maxsize); }

void SiteListAng::Sort()
{ std::sort(siteList.begin(),siteList.end()); }

void SiteListAng::truncate(unsigned maxSiteList)
{ siteList.erase(siteList.begin()+maxSiteList+1,siteList.end()); }

//-----------------------

void SiteListAng::setupRegion(floatType SAMP,Output& output)
{
 // Returns a list of unique angles for the space group (DO_FULL)
  try {

  dmat331D  osmat(NSYMP);
  floatType dbeta(0),betmin(0),betmax(0),nbeta(0),tpmax(0),dtp(0),tmmax(0),dtm(0);
  floatType alpha(0),gamma(0),cosb2(0),sinb2(0),offm(0);
  angsite   nextAngsite;
  dvect3    search_euler(0,0,0),seuler(0,0,0);
  dmat33    rmat,drmat,rsmat;
  floatType epsilon = 0.0002;

  dbeta = SAMP*std::sqrt(2.0/3.0);
  nbeta = std::floor(180./dbeta);
  betmin = (180.0-nbeta*dbeta)/2.0;
  betmax = 180.0;
  if (betmin < epsilon) betmin = 0;

//Combine orthogonalisation and symmetry matrices for use
//in generating symmetry equivalents of rotation matrices

  for (unsigned isym = 0; isym < NSYMP; isym++)
  {
    dmat33 rotsym = Rotsym(isym);
    osmat[isym] = rotsym*Frac2Orth().transpose();
  }

  //always search the origin
  angsite origin;
  origin.euler = dvect3(0,0,0);
  siteList.push_back(origin);

  floatType offp = 5.0/6.0;
  for (floatType beta = betmin; beta < betmax; beta += dbeta)
  {
    offp = 1.0 - offp;
    search_euler[1] = scitbx::deg_as_rad(beta);
    cosb2 = fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
    sinb2 = fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
    if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
    {
      tpmax = 1.0;
      dtp = 1.0;
      dtm = SAMP;
      tmmax = 360.0;
    }
    else if (sinb2 < SAMP/360.0)
    {
       dtp = std::sqrt(3.0/4.0)*SAMP;
       tpmax = 360.0;
       tmmax = 1.0;
       dtm = 1.0;
    }
    else
    {
      dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
      dtm = SAMP/sinb2;
      tpmax = 720.0;
      tmmax = 360.0;
    }

    offm = 3.0/4.0;
    for (floatType thetp = offp*dtp; thetp < tpmax; thetp += dtp)
    {
      offm = 1.0 - offm;
      for (floatType thetm = offm*dtm; thetm < tmmax; thetm += dtm)
      {
        alpha = fmod_pos(720.0+(thetp+thetm)/2.0,360.0);
        search_euler[0] = scitbx::deg_as_rad(alpha);
        gamma = fmod_pos(720.0+(thetp-thetm)/2.0,360.0);
        search_euler[2] = scitbx::deg_as_rad(gamma);

// Check Euler angles chosen with same convention as symmetry related ones
// by doing a conversion/back-conversion

        rmat = euler2matrixRAD(search_euler);
        search_euler = matrix2eulerRAD(rmat);
        drmat = Orth2Frac()*rmat;
        drmat = drmat.transpose();

//    If a symmetry related orientation beats this orientation to
//    the asymmetric unit, skip this orientation because we will find
//    it's better again later in the search.

//    A full search without symmetry covers 0-720 in one theta,
//    0-360 in the other. The search points are found by generating
//    all these angles, but only keeping the orientation if, when
//    compared to all its symmetry related equivalents:
//      it has uniquely the lowest beta, or
//      it shares the lowest beta and has uniquely the lowest alpha, or
//      it shares the lowest alpha and beta and has the lowest gamma
//    This requires offsets for theta(+) and theta(-),
//    as well as different metrics in each direction to make the
//    nearest neighbour distances all equivalent.


        for (unsigned isym = 1; isym < NSYMP; isym++)
        {
          rsmat = drmat*osmat[isym];
          rsmat = rsmat.transpose();
          seuler = matrix2eulerRAD(rsmat);

          //If new beta is smaller, skip this orientation.
          if (seuler[1] < search_euler[1]-epsilon) goto next_angle;

          //If new beta is bigger, try another symmetry operator.
          if (seuler[1] > search_euler[1]+epsilon) continue;

          //New beta is the same within epsilon
          //Make decision on alpha
          // If new alpha is smaller, skip this orientation.
          if (seuler[0] < search_euler[0]-epsilon) goto next_angle;

          //If it's bigger, try another symmetry operator.
          if (seuler[0] > search_euler[0]+epsilon) continue;

          //New beta and alpha are the same within epsilon
          //Make decision on gamma
          //If new gamma is smaller, skip this orientation.
          if (seuler[2] < search_euler[2]-epsilon) goto next_angle;

          //Success! Accept angles
          //If all angles are (nearly) the same, the orientation
          //will be duplicated in the search, but this will arise
          //very rarely and will be fixed in the cluster analysis
        }
        //accept these euler angles
        nextAngsite.euler = dvect3(alpha,beta,gamma);
        siteList.push_back(nextAngsite);

        next_angle: continue;
      }
    }
  }

  } //try
  catch (std::exception const& err) {
  //this catches the error  St9bad_alloc where it runs out of memory
    throw PhaserError(MEMORY,"Memory exhausted while generating search points");
  }
}

void SiteListAng::setupAroundPoint(dvect3 POINT,floatType RANGE,floatType SAMP)
{
  //DO_AROUND
  angsite point;
  point.euler = POINT;
  siteList.push_back(point);

  floatType alpha(0),gamma(0),dbeta(0),betmin(0),betmax(0);
  floatType tpmin(0),tmmin(0),tpmax(0),dtp(0),tmmax(0),dtm(0),nbeta(0);
  floatType cosb2(0),sinb2(0),offm(0);

 // SAMP = min(SAMP,RANGE);

  floatType tpstart = POINT[0] + POINT[2];
  floatType tmstart = POINT[0] - POINT[2];
  betmin = POINT[1] - RANGE;
  betmax = POINT[1] + RANGE;
  dbeta = SAMP*std::sqrt(2./3.);
  nbeta = phaser::round((betmax-betmin)/dbeta);
  if ((nbeta*dbeta) && (betmax-betmin)/(nbeta*dbeta) >  1.05 )
    nbeta++;
  dbeta = nbeta > 0 ? (betmax-betmin)/nbeta : 0 ;
  betmax = betmax + 0.001*dbeta;
  dvect3 starteuler;
  for (int i = 0; i < 3; i++)
    starteuler[i] = scitbx::deg_as_rad(POINT[i]);

  floatType offp = 5.0/6.0;
  angsite nextAngsite;
  for (floatType beta = betmin; beta < betmax; beta += dbeta)
  {
    offp = 1.0 - offp;
    cosb2 = fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
    sinb2 = fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
    if (cosb2 < sqrt(3.0/4.0)*SAMP/720.0)
    {
      tpmin = 0.0;
      tpmax = 1.0;
      dtp = 1.0;
      dtm = SAMP;
      tmmin = tmstart - RANGE - dtm;
      tmmax = tmstart + RANGE;
    }
    else if (sinb2 < SAMP/360.0)
    {
      dtp = sqrt(3./4.)*SAMP;
      tpmin = tpstart - sqrt(3./4.)*RANGE - dtp;
      tpmax = tpstart + sqrt(3./4.)*RANGE;
      tmmin = 0.0;
      tmmax = 1.0;
      dtm = 1.0;
    }
    else
    {
      dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
      dtm = SAMP/sinb2;
      tpmin = tpstart - sqrt(3./4.)*RANGE/cosb2 - dtp;
      tpmax = tpstart + sqrt(3./4.)*RANGE/cosb2;
      if (tpmax-tpmin > 720.0)
      {
        tpmin = 0.0;
        tpmax = 720.0;
      }
      tmmin = tmstart - RANGE/sinb2 - dtm;
      tmmax = tmstart + RANGE/sinb2;
      if (tmmax-tmmin > 360.0)
      {
        tmmin = 0.0;
        tmmax = 360.0;
      }
    }

    offm = 3.0/4.0;
    for (floatType thetp = tpmin+(offp*dtp); thetp < tpmax; thetp += dtp)
    {
      offm = 1.0 - offm;
      for (floatType thetm = tmmin+(offm*dtm); thetm < tmmax; thetm += dtm)
      {
        alpha = std::fmod(720.0+(thetp+thetm)/2.0,360.0);
        gamma = std::fmod(720.0+(thetp-thetm)/2.0,360.0);
//std::cout << "euler " << alpha << " " << beta << " " << gamma << " " << std::endl;
        if (rotdistDEG(dvect3(alpha,beta,gamma), POINT) < RANGE &&
            rotdistDEG(dvect3(alpha,beta,gamma), POINT) > SAMP/2.0)
        {
          nextAngsite.euler = dvect3(alpha,beta,gamma);
          siteList.push_back(nextAngsite);
        }
      }
    }
  }
}

void SiteListAng::flagClusteredTop(int LEVEL)
{
  Sort();
  using namespace phaser::rotation;
  dmat33 reference_rmat,rmat,dmat,rsmat,drmat;
  floatType delta(0);
#ifdef _PHASER_ANISOTROPIC_CLUSTERING
  AnisotropicObject dist_calc = AnisotropicObject(
    principal_axes_,
    extents_[0],
    extents_[1],
    extents_[2]
    );
#else
  IsotropicObject dist_calc = IsotropicObject( extents_.max() );
#endif
  floatType clust_rms_dist = dist_calc.max_rmsd_for_angle( CLUSTER_ANG );

  dmat331D osmat(NSYMP);
  for (unsigned isym = 0; isym < NSYMP; isym++)
  {
    dmat33 rotsym = Rotsym(isym);
    osmat[isym] = doFrac2Orth(rotsym);
   // osmat[isym] = rotsym;
  }

  //assume all sites are NOT unique to start with
  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
    siteList[rot].topsite = false;

  unsigned actual_num(0);

  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
  //if (!isDeep(rot))
  {
    if (siteList[rot].selected) //sites have already been subjected to selection criteria
    {
      reference_rmat = siteList[rot].getMatrix();
      dist_calc.set_reference( reference_rmat );

      //Look backwards as you are most likely to find a
      //site to cluster with close by in the list, not
      //at the top of the list each time (list in order)
      for (int rot_prev = rot-1; rot_prev >= 0; rot_prev--)
      {
        //Look at the sites not considering symmetry first
        //as you are most likely not going to need symmetry to
        //find a neighbouring site
        rmat = siteList[rot_prev].getMatrix();
        //dvect3  prev_angle = matrix2eulerDEG(rmat);

        //If within the required distance, change group of
        //next P1 rot to the group of the previous P1 rot
        //delta = rotdistDEG(reference_angle,prev_angle);

        if (dist_calc.distance_from_reference( rmat ) < clust_rms_dist)
        { //cluster it
           siteList[rot].clusterNr = siteList[rot_prev].clusterNr;
           siteList[siteList[rot].clusterNr].clusterMul++;
           if (siteList[rot].num < siteList[siteList[rot_prev].clusterNr].clusterNum)
               siteList[siteList[rot_prev].clusterNr].clusterNum = siteList[rot].num;
           if (siteList[rot].value2 > siteList[siteList[rot_prev].clusterNr].value3)
               siteList[siteList[rot_prev].clusterNr].value3 = siteList[rot].value2;
           goto next_rot;
        }
      }
      for (int rot_prev = rot-1; rot_prev >= 0 ; rot_prev--)
      {
        dmat33 prev_rotmat = siteList[rot_prev].getMatrix();
      //Now consider symmetry related rotations
        for (unsigned isym = 1; isym < NSYMP; isym++)
        {
          dmat33 prev_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(prev_rotmat);
          dmat33 sym_prev_rotmat = doFrac2Orth(prev_rotmat2);
          //dvect3 sym_prev_angle = matrix2eulerDEG(sym_prev_rotmat);
          //If within the required distance, change group of
          //next P1 rot to the group of the previous P1 rot
          //delta = rotdistDEG(reference_angle,sym_prev_angle);

          if (dist_calc.distance_from_reference(sym_prev_rotmat) < clust_rms_dist)
          {
            siteList[rot].clusterNr = siteList[rot_prev].clusterNr;
            siteList[siteList[rot].clusterNr].clusterMul++;
            if (siteList[rot].num < siteList[siteList[rot_prev].clusterNr].clusterNum)
                siteList[siteList[rot_prev].clusterNr].clusterNum = siteList[rot].num;
            if (siteList[rot].value2 > siteList[siteList[rot_prev].clusterNr].value3)
                siteList[siteList[rot_prev].clusterNr].value3 = siteList[rot].value2;
            goto next_rot;
          }
        }
      }
      actual_num++;
      siteList[rot].topsite = true; // no hits found, this site must be unique
      siteList[rot].distance = 360.0;
      siteList[rot].clusterNr = rot; //unique, so gets its own cluster number;
      siteList[rot].clusterNum = siteList[rot].num; //unique, so the cluster position so far is the position in the FRF
      siteList[rot].clusterMul = 1; //first member of its own cluster
      siteList[rot].value3 = siteList[rot].value2;
      for (unsigned isym = 0; isym < NSYMP; isym++) {
        dmat33 top_rotmat = siteList[0].getMatrix();
        dmat33 top_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(top_rotmat);
        dmat33 sym_top_rotmat = doFrac2Orth(top_rotmat2);
        delta = rotmatdistDEG(reference_rmat, sym_top_rotmat);
        siteList[rot].distance = std::min(fabs(siteList[rot].distance),fabs(delta));
      }
    }
    next_rot: continue; //performs the next iteration of the loop.
  }

  if (LEVEL == 1)  return; //default is 0, only take top of cluster

  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
  {
    if (siteList[rot].topsite)
    {
      reference_rmat = siteList[rot].getMatrix();
      dist_calc.set_reference( reference_rmat );

      for (int rot_next = rot+1; rot_next < siteList.size(); rot_next++)
      if (siteList[rot_next].clusterNr == siteList[rot].clusterNr)
      {
        rmat = siteList[rot_next].getMatrix();
        double dist = dist_calc.distance_from_reference( rmat );
          if (  (LEVEL == 0)
              || (LEVEL ==  5 && dist <= 0.5*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL == 10 && dist <= 1.0*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL == 15 && dist <= 1.0*clust_rms_dist && dist> 0.5*clust_rms_dist)
             )
        {
          siteList[rot_next].topsite = true;
          goto level_rot;
        }
      }
      for (int rot_next = rot+1; rot_next < siteList.size(); rot_next++)
      if (siteList[rot_next].clusterNr == siteList[rot].clusterNr)
      {
        dmat33 next_rotmat = siteList[rot_next].getMatrix();
        //Now consider symmetry related rotations
        for (unsigned isym = 1; isym < NSYMP; isym++)
        {
          dmat33 next_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(next_rotmat);
          dmat33 sym_next_rotmat = doFrac2Orth(next_rotmat2);
          double dist = dist_calc.distance_from_reference( sym_next_rotmat );
          if (   (LEVEL == 0)
              || (LEVEL ==  2 && dist <= 0.5*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL ==  3 && dist <= 1.0*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL ==  4 && dist <= 1.0*clust_rms_dist && dist> 0.5*clust_rms_dist)
             )
          {
            siteList[rot].topsite = true;
            goto level_rot;
          }
        }
      }
    }
    level_rot: continue; //performs the next iteration of the loop.
  }
}

void SiteListAng::flagClusteredTopNew(int LEVEL)
{
  Sort();
  using namespace phaser::rotation;
  dmat33 reference_rmat,rmat,dmat,rsmat,drmat;
  floatType delta(0);
#ifdef _PHASER_ANISOTROPIC_CLUSTERING
  AnisotropicObject dist_calc = AnisotropicObject(
    principal_axes_,
    extents_[0],
    extents_[1],
    extents_[2]
    );
#else
  IsotropicObject dist_calc = IsotropicObject( extents_.max() );
#endif
  floatType clust_rms_dist = dist_calc.max_rmsd_for_angle( CLUSTER_ANG );

  dmat331D osmat(NSYMP);
  for (unsigned isym = 0; isym < NSYMP; isym++)
  {
    dmat33 rotsym = Rotsym(isym);
    osmat[isym] = doFrac2Orth(rotsym);
   // osmat[isym] = rotsym;
  }

  //assume all sites are NOT unique to start with
  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
    siteList[rot].topsite = false;

  unsigned actual_num(0);

  double COOEE(0.01);
  int  NEW_NSYMP(1);
//#define NEW_CLUSTERING_TOP_DOWN
#ifdef NEW_CLUSTERING_TOP_DOWN
  std::cout << "NEW CLUSTERING: TOP-DOWN SORT" << std::endl;
#else
  std::cout << "NEW CLUSTERING: BOTTOM-UP SORT" << std::endl;
#endif
  std::cout << "NEW CLUSTERING: MAXIMUM CLUSTERING DISTANCE SAMPLING*" << dtos(COOEE) << std::endl;
  if (NEW_NSYMP == 1)
    std::cout << "NEW CLUSTERING: NO SYMMETRY CLUSTERING" << std::endl;
  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
  {
    if (siteList[rot].selected) //sites have already been subjected to selection criteria
    {
      reference_rmat = siteList[rot].getMatrix();
      dist_calc.set_reference( reference_rmat );
      double cooee_cluster_distance = clust_rms_dist*COOEE;

      bool within_cooee_of_current_topsite = false;
#ifdef NEW_CLUSTERING_TOP_DOWN
      for (int rot_prev = 0; rot_prev < rot ; rot_prev++)
#else
      for (int rot_prev = rot-1; rot_prev >= 0; rot_prev--)
#endif
      if (siteList[rot].topsite)
      {
        dmat33 prev_rotmat = siteList[rot_prev].getMatrix();
      //Now consider symmetry related rotations
        for (unsigned isym = 0; isym < NEW_NSYMP; isym++)
        {
          dmat33 prev_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(prev_rotmat);
          dmat33 sym_prev_rotmat = doFrac2Orth(prev_rotmat2);
          //dvect3 sym_prev_angle = matrix2eulerDEG(sym_prev_rotmat);

          if (dist_calc.distance_from_reference(sym_prev_rotmat) < cooee_cluster_distance)
          {
            within_cooee_of_current_topsite = true;
            //now go looking for the site it is really closest to, it if exists
          }
        }
      }
      //force a new topsite if not within cooee of a topsite already
      if (within_cooee_of_current_topsite)
      {
        //If within the required distance, change group of
        //next P1 rot to the group of the previous P1 rot
        //delta = rotdistDEG(reference_angle,prev_angle);

#ifdef NEW_CLUSTERING_TOP_DOWN
      for (int rot_prev = 0; rot_prev < rot ; rot_prev++)
#else
      for (int rot_prev = rot-1; rot_prev >= 0; rot_prev--)
#endif
        {
          rmat = siteList[rot_prev].getMatrix();
          if (dist_calc.distance_from_reference( rmat ) < clust_rms_dist)
          { //cluster it
            siteList[rot].clusterNr = siteList[rot_prev].clusterNr;
            siteList[siteList[rot].clusterNr].clusterMul++;
            if (siteList[rot].num < siteList[siteList[rot_prev].clusterNr].clusterNum)
                siteList[siteList[rot_prev].clusterNr].clusterNum = siteList[rot].num;
            if (siteList[rot].value2 > siteList[siteList[rot_prev].clusterNr].value3)
                siteList[siteList[rot_prev].clusterNr].value3 = siteList[rot].value2;
             goto next_rot;
          }
        }

#ifdef NEW_CLUSTERING_TOP_DOWN
      for (int rot_prev = 0; rot_prev < rot ; rot_prev++)
#else
      for (int rot_prev = rot-1; rot_prev >= 0; rot_prev--)
#endif
        {
          dmat33 prev_rotmat = siteList[rot_prev].getMatrix();
        //Now consider symmetry related rotations
          for (unsigned isym = 1; isym < NEW_NSYMP; isym++)
          {
            dmat33 prev_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(prev_rotmat);
            dmat33 sym_prev_rotmat = doFrac2Orth(prev_rotmat2);
            //dvect3 sym_prev_angle = matrix2eulerDEG(sym_prev_rotmat);

            //If within the required distance, change group of
            //next P1 rot to the group of the previous P1 rot
            //delta = rotdistDEG(reference_angle,sym_prev_angle);

            if (dist_calc.distance_from_reference(sym_prev_rotmat) < clust_rms_dist)
            {
              siteList[rot].clusterNr = siteList[rot_prev].clusterNr;
              siteList[siteList[rot].clusterNr].clusterMul++;
              if (siteList[rot].num < siteList[siteList[rot_prev].clusterNr].clusterNum)
                  siteList[siteList[rot_prev].clusterNr].clusterNum = siteList[rot].num;
              if (siteList[rot].value2 > siteList[siteList[rot_prev].clusterNr].value3)
                  siteList[siteList[rot_prev].clusterNr].value3 = siteList[rot].value2;
              goto next_rot;
            }
          }
        }
      }
      actual_num++;
      siteList[rot].topsite = true; // no hits found, this site must be unique
      siteList[rot].distance = 360.0;
      siteList[rot].clusterNr = rot; //unique, so gets its own cluster number;
      siteList[rot].clusterNum = siteList[rot].num; //unique, so the cluster position so far is the position in the FRF
      siteList[rot].clusterMul = 1; //first member of its own cluster
      siteList[rot].value3 = siteList[rot].value2;
      for (unsigned isym = 0; isym < NSYMP; isym++) {
        dmat33 top_rotmat = siteList[0].getMatrix();
        dmat33 top_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(top_rotmat);
        dmat33 sym_top_rotmat = doFrac2Orth(top_rotmat2);
        delta = rotmatdistDEG(reference_rmat, sym_top_rotmat);
        siteList[rot].distance = std::min(fabs(siteList[rot].distance),fabs(delta));
      }
    }
    next_rot: continue; //performs the next iteration of the loop.
  }

  if (LEVEL == 1)  return; //default is 0, only take top of cluster

  for (unsigned rot = 0; rot < siteList.size(); rot++) //all rot
  {
    if (siteList[rot].topsite)
    {
      reference_rmat = siteList[rot].getMatrix();
      dist_calc.set_reference( reference_rmat );

      for (int rot_next = rot+1; rot_next < siteList.size(); rot_next++)
      if (siteList[rot_next].clusterNr == siteList[rot].clusterNr)
      {
        rmat = siteList[rot_next].getMatrix();
        double dist = dist_calc.distance_from_reference( rmat );
          if (  (LEVEL == 0)
              || (LEVEL ==  5 && dist <= 0.5*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL == 10 && dist <= 1.0*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL == 15 && dist <= 1.0*clust_rms_dist && dist> 0.5*clust_rms_dist)
             )
        {
          siteList[rot_next].topsite = true;
          goto level_rot;
        }
      }
      for (int rot_next = rot+1; rot_next < siteList.size(); rot_next++)
      if (siteList[rot_next].clusterNr == siteList[rot].clusterNr)
      {
        dmat33 next_rotmat = siteList[rot_next].getMatrix();
        //Now consider symmetry related rotations
        for (unsigned isym = 1; isym < NSYMP; isym++)
        {
          dmat33 next_rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(next_rotmat);
          dmat33 sym_next_rotmat = doFrac2Orth(next_rotmat2);
          double dist = dist_calc.distance_from_reference( sym_next_rotmat );
          if (   (LEVEL == 0)
              || (LEVEL ==  2 && dist <= 0.5*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL ==  3 && dist <= 1.0*clust_rms_dist && dist> 0.0*clust_rms_dist)
              || (LEVEL ==  4 && dist <= 1.0*clust_rms_dist && dist> 0.5*clust_rms_dist)
             )
          {
            siteList[rot].topsite = true;
            goto level_rot;
          }
        }
      }
    }
    level_rot: continue; //performs the next iteration of the loop.
  }
}

void SiteListAng::moveToRotationalASU()
{
 // Returns a list of unique angles for the space group (DO_FULL)
  dmat331D osmat(NSYMP);
  dmat33   rmat;

//Combine orthogonalisation and symmetry matrices for use
//in generating symmetry equivalents of rotation matrices

  for (unsigned isym = 0; isym < NSYMP; isym++)
  {
    dmat33 rotsym = Rotsym(isym);
    osmat[isym] = doFrac2Orth(rotsym);
  }

  dvect3 search_euler(0,0,0),seuler(0,0,0);
  dmat33 rsmat,drmat;
  floatType alpha(0),beta(0),gamma(0);

  const floatType epsilon = 0.0002;

  for (unsigned isite = 0; isite < siteList.size(); isite++ )
  {
    alpha =  siteList[isite].euler[0];
    search_euler[0] = scitbx::deg_as_rad(alpha);
    beta =  siteList[isite].euler[1];
    search_euler[1] = scitbx::deg_as_rad(beta);
    gamma =  siteList[isite].euler[2];
    search_euler[2] = scitbx::deg_as_rad(gamma);

// std::cout << "Testing " << alpha << " " << beta<< " " << gamma <<std::endl;
// Check Euler angles chosen with same convention as symmetry related ones
// by doing a conversion/back-conversion

    rmat = euler2matrixRAD(search_euler);
    search_euler = matrix2eulerRAD(rmat);

    drmat = doOrth2Frac(rmat);

//    If a symmetry related orientation beats this orientation to
//    the asymmetric unit, save symmetry related orientation and continue test

//    The final orientations have
//      it has uniquely the lowest beta, or
//      it shares the lowest beta and has uniquely the lowest alpha, or
//      it shares the lowest alpha and beta and has the lowest gamma

    for (unsigned isym = 1; isym < NSYMP; isym++)
    {
      dmat33 rotmat = siteList[isite].getMatrix();
      dmat33 rotmat2 = Rotsym(isym).transpose()*doOrth2Frac(rotmat);
      dmat33 sym_rotmat = doFrac2Orth(rotmat2);
      dvect3  seuler = matrix2eulerRAD(sym_rotmat);

//      rsmat = osmat[isym]*drmat;
//      seuler = matrix2eulerRAD(rsmat);

      //If new beta is smaller, save this orientation.
      if (seuler[1] < search_euler[1] - epsilon)
      {
        search_euler = seuler;
      }
      else if (between(seuler[1],search_euler[1],epsilon))
      {
        //New beta is the same
        //Make decision on alpha
        // If new alpha is smaller, save this orientation.
        if (seuler[0] < search_euler[0] - epsilon)
        {
           search_euler = seuler;
        }
        else if (between(seuler[0],search_euler[0],epsilon))
        {
          //New beta and alpha are the same
          //Make decision on gamma
          //If new gamma is smaller, save this orientation.
          if (seuler[2] < search_euler[2] - epsilon)
          {
            search_euler = seuler;
          }
        }
      }
    }
    //accept these euler angles
    siteList[isite].euler[0] = scitbx::rad_as_deg(search_euler[0]);
    siteList[isite].euler[1] = scitbx::rad_as_deg(search_euler[1]);
    siteList[isite].euler[2] = scitbx::rad_as_deg(search_euler[2]);
  }
}

void SiteListAng::setupAllAngles(floatType SAMP)
{
 // Returns a list of all angles

  floatType dbeta(0),betmin(0),betmax(0),tpmax(0),dtp(0),tmmax(0),dtm(0),nbeta(0);
  floatType alpha(0),gamma(0),cosb2(0),sinb2(0),offm(0);
  angsite nextAngsite;

  floatType epsilon = 0.0002;

  dbeta = SAMP*std::sqrt(2.0/3.0);
  nbeta = std::floor(180./dbeta);
  betmin = (180.0-nbeta*dbeta)/2.0;
  betmax = 180.0;
  if (betmin < epsilon) betmin = 0;

  //always search the origin
  angsite origin;
  origin.euler = dvect3(0,0,0);
  siteList.push_back(origin);

  floatType offp = 5.0/6.0;
  for (floatType beta = betmin; beta < betmax; beta += dbeta)
  {
    offp = 1.0 - offp;
    cosb2 = fabs(std::cos(scitbx::deg_as_rad(beta)/2.0));
    sinb2 = fabs(std::sin(scitbx::deg_as_rad(beta)/2.0));
    if (cosb2 < std::sqrt(3.0/4.0)*SAMP/720.0)
    {
      tpmax = 1.0;
      dtp = 1.0;
      dtm = SAMP;
      tmmax = 360.0;
    }
    else if (sinb2 < SAMP/360.0)
    {
       dtp = std::sqrt(3.0/4.0)*SAMP;
       tpmax = 360.0;
       tmmax = 1.0;
       dtm = 1.0;
    }
    else
    {
      dtp = std::sqrt(3.0/4.0)*SAMP/cosb2;
      dtm = SAMP/sinb2;
      tpmax = 720.0;
      tmmax = 360.0;
    }

    offm = 3.0/4.0;
    for (floatType thetp = offp*dtp; thetp < tpmax; thetp += dtp)
    {
      offm = 1.0 - offm;
      for (floatType thetm = offm*dtm; thetm < tmmax; thetm += dtm)
      {
        alpha = fmod_pos(720.0+(thetp+thetm)/2.0,360.0);
        gamma = fmod_pos(720.0+(thetp-thetm)/2.0,360.0);
        nextAngsite.euler = dvect3(alpha,beta,gamma);
        siteList.push_back(nextAngsite);
      }
    }
  }
}

void SiteListAng::logRawTop(outStream where,Output& output)
{
  for (int i = 0; i < max_nPrinted && i < siteList.size(); i++)
  {
    floatType maxValue = siteList[i].value;
    for (int t = i; t < siteList.size(); t++)
    if (siteList[t].selected)
    {
      if (siteList[t].value >= maxValue)
      {
        maxValue = siteList[t].value;
        angsite tmp = siteList[i];
                siteList[i] = siteList[t];
                siteList[t] = tmp;
      }
    }
    siteList[i].num = i+1;
  }
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s\n",
       "#","Euler1","Euler2","Euler3",FAST?"   FSS":"   LLG","Z-score");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    if (FAST) n = 3; //top is 100
    output.logTabPrintf(1,where,"%-5i % 6.1f % 6.1f % 6.1f % 10.*f %5.*f\n",
      siteList[t].num,
      siteList[t].euler[0],siteList[t].euler[1],siteList[t].euler[2],
      n,siteList[t].value,
      (siteList[t].zscore >=100?1:2),siteList[t].zscore);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#Sites = " + itos(num_selected) + ": output truncated to " +itos(max_nPrinted) + " sites");
  if (num_selected) output.logBlank(where);
}

void SiteListAng::graphRawTop(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    for (int i = 0; i < max_nPrinted && i < siteList.size(); i++)
    {
      floatType maxValue = siteList[i].value;
      for (int t = i; t < siteList.size(); t++)
      if (siteList[t].selected)
      {
        if (siteList[t].value >= maxValue)
        {
          maxValue = siteList[t].value;
          angsite tmp = siteList[i];
                  siteList[i] = siteList[t];
                  siteList[t] = tmp;
        }
      }
    }
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top raw peaks");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered:AUTO:1,2:");
      output.logTab(0,where,"$$");
      output.logTab(0,where,"Peak_number  Score  $$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nPrinted; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f \n",num++,siteList[t].value);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

void SiteListAng::logClusteredTop(outStream where, Output& output)
{
  //must be sorted for clustering
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-5s %-6s\n",
      "#","Euler1","Euler2","Euler3",FAST?"   FSS":"   LLG","Z-score","Split","#Group");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    if (FAST) n = 3; //top is 100
    output.logTabPrintf(1,where,"%-5i % 6.1f % 6.1f % 6.1f % 10.*f %5.*f %5.1f %6d\n",
      nPrinted,
      siteList[t].euler[0],siteList[t].euler[1],siteList[t].euler[2],
      n,siteList[t].value,
      (siteList[t].zscore >=100?1:2),siteList[t].zscore,
      siteList[t].distance,siteList[t].clusterMul);
    for (unsigned isym = 1; isym < NSYMP; isym++)
    {
      dvect3 tempAng = matrix2eulerDEG(Frac2Orth()*Rotsym(isym).transpose()*Orth2Frac()*siteList[t].getMatrix());
      output.logTabPrintf(1,where,"%-5s % 6.1f % 6.1f % 6.1f\n"," ",tempAng[0],tempAng[1],tempAng[2]);
    }
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");
  if (num_selected) output.logBlank(where);
}

void SiteListAng::graphClusteredTop(outStream where, Output& output)
{
  //must be sorted for clustering
  if (output.isPackageCCP4())
  {
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top clustered peaks:");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered:AUTO:1,2:");
      output.logTab(0,where,"        :Graph2 number:AUTO:1,3: $$");
      output.logTab(0,where,"Peak_number  Score  Number_in_Cluster$$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nGraphed; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f %d\n",num++,siteList[t].value,siteList[t].clusterMul);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

void SiteListAng::logRawTopRescored(outStream where,Output& output)
{
  for (int i = 0; i < max_nPrinted && i < siteList.size(); i++)
  {
    floatType maxValue = siteList[i].value;
    for (int t = i; t < siteList.size(); t++)
    if (siteList[t].selected)
    {
      if (siteList[t].value >= maxValue)
      {
        maxValue = siteList[t].value;
        angsite tmp = siteList[i];
                siteList[i] = siteList[t];
                siteList[t] = tmp;
      }
    }
  }
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-9s\n",
       "#","Euler1","Euler2","Euler3","   LLG","Z-score","   FSS ");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    int m = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value2)))),0.);
    output.logTabPrintf(1,where,"%-5i % 6.1f % 6.1f % 6.1f % 10.*f %5.*f % 10.*f\n",
      nPrinted,
      siteList[t].euler[0],siteList[t].euler[1],siteList[t].euler[2],
      n,siteList[t].value,
      siteList[t].zscore >= 100 ? 2:1,siteList[t].zscore,
      m,siteList[t].value2);
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");

  bool compute_CC(false);
  if (compute_CC && Size() > 2)
  {
    floatType sumxy(0),sumx(0),sumy(0),sumx2(0),sumy2(0);
    for (unsigned t = 0; t < Size(); t++)
    {
      sumx += siteList[t].value;
      sumy += siteList[t].value2;
    }
    floatType xmean(sumx/Size()),ymean(sumy/Size()),dx,dy;
    for (unsigned t = 0; t < Size(); t++)
    {
      dx = siteList[t].value-xmean;
      dy = siteList[t].value2-ymean;
      sumxy += dx*dy;
      sumx2 += dx*dx;
      sumy2 += dy*dy;
    }
    if (sumxy*sumx2*sumy2 != 0)
    {
      floatType correlation = sumxy/std::sqrt(sumx2*sumy2);
      floatType slope = sumxy/sumx2;
      floatType intercept = (sumy - slope*sumx)/Size();
      output.logBlank(VERBOSE);
      output.logTab(1,VERBOSE,"Correlation Coefficient between FRF and LLG = " + dtos(correlation) + " for " + itos(Size()) + " points");
      output.logTab(2,VERBOSE,"Slope and intercept of line: " + dtos(slope) + " " + dtos(intercept));
    }
  }

  //Break the mould and use cout to dump output
  if (getenv("PHASER_MATHEMATICA_FILE") != NULL)
  {
    std::ofstream outstream;
    outstream.open(getenv("PHASER_MATHEMATICA_FILE"));
    for (unsigned t = 0; t < Size(); t++)
      outstream << siteList[t].value << " " << siteList[t].value2 << std::endl;
    outstream.close();
  }

  if (num_selected) output.logBlank(where);
}

void SiteListAng::graphRawTopRescored(outStream where,Output& output)
{
  if (output.isPackageCCP4())
  {
    for (int i = 0; i < max_nGraphed && i < siteList.size(); i++)
    {
      floatType maxValue = siteList[i].value;
      for (int t = i; t < siteList.size(); t++)
      if (siteList[t].selected)
      {
        if (siteList[t].value >= maxValue)
        {
          maxValue = siteList[t].value;
          angsite tmp = siteList[i];
                  siteList[i] = siteList[t];
                  siteList[t] = tmp;
        }
      }
    }
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top raw peaks rescored");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered:AUTO:1,2:");
      output.logTab(0,where,"$$");
      output.logTab(0,where,"Peak_number  Score  $$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nGraphed; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f\n",num++,siteList[t].value);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
  }
}

void SiteListAng::logClusteredTopRescored(outStream where, Output& output)
{
  unsigned num_selected = numSelected();
  unsigned nPrinted(0);
  if (num_selected)
    output.logTabPrintf(1,where,"%-5s %-6s %-6s %-6s %-8s %-7s %-5s %-6s %21s\n",
    "#","Euler1","Euler2","Euler3","   LLG","Z-score","Split","#Group","FSS-this-ang/FSS-top");
  for (unsigned t = 0; t < Size() && nPrinted < max_nPrinted; t++)
  if (siteList[t].selected)
  {
    nPrinted++;
    int n = std::max(4. - std::floor(std::log10(std::max(1.,std::abs(siteList[0].value)))),0.);
    int m = 3;
    output.logTabPrintf(1,where,"%-5i % 6.1f % 6.1f % 6.1f % 10.*f %5.*f %5.1f %6d %10.*f/%10.*f\n",
        nPrinted,
        siteList[t].euler[0],siteList[t].euler[1],siteList[t].euler[2],
        n,siteList[t].value,
        (siteList[t].zscore >=100?1:2),siteList[t].zscore,
        siteList[t].distance,
        siteList[t].clusterMul,
        m,siteList[t].value2,
        m,siteList[t].value3);
    for (unsigned isym = 1; isym < NSYMP; isym++)
    {
      dvect3 tempAng = matrix2eulerDEG(Frac2Orth()*Rotsym(isym).transpose()*Orth2Frac()*siteList[t].getMatrix());
      output.logTabPrintf(1,where,"%5s % 6.1f % 6.1f % 6.1f\n"," ",tempAng[0],tempAng[1],tempAng[2]);
    }
  }
  if (num_selected > max_nPrinted)
    output.logTab(1,where,"#SITES = " + itos(num_selected) + ": OUTPUT TRUNCATED TO " +itos(max_nPrinted) + " SITES");
  if (num_selected) output.logBlank(where);
}

void SiteListAng::graphClusteredTopRescored(outStream where, Output& output)
{
  bool create_plot_like_in_the_paper = false;
  if (output.isPackageCCP4())
  {
    unsigned num_selected = numSelected();
    if (num_selected)
    {
      output.logTab(0,where,"$TABLE : Top clustered peaks rescored:");
      output.logTab(0,where,"$GRAPHS :Graph1 clustered:AUTO:1,2:");
      output.logTab(0,where,"        :Graph2 number:AUTO:1,3: $$");
      output.logTab(0,where,"Peak_number  Score  Number_in_Cluster$$ loggraph $$");
      int num(1);
      for (unsigned t = 0; t < Size() && num <= max_nGraphed; t++)
      if (siteList[t].selected)
        output.logTabPrintf(0,where,"%d %f %d\n",num++,siteList[t].value,siteList[t].clusterMul);
      output.logTab(0,where,"$$");
      output.logBlank(where);
    }
    if (create_plot_like_in_the_paper && num_selected > 2)
    {
      floatType xbar(0),ybar(0),x2(0),y2(0),xy(0);
      int n(0);

      output.logTab(0,DEBUG,"<applet width=\" 800\" height=\" 500\" code=\"JLogGraph.class\" codebase=\"/usr/lab/crystal/ccp4-4.2/bin\"><param name=\"table\" value=\"$TABLE: Likelihood/FRF correlation:$GRAPHS:Likelihood gain against FRF score (Cluster sorted):A:2,3:\n\n"
    "     :Likelihood gain against FRF score (LLG sorted):A:1,3:\n\n"
    "     :Likelihood gain against FRF distance (Cluster sorted):A:4,3:\n\n"
    "     :Likelihood gain against FRF distance (LLG sorted):A:5,3:\n\n"
    "     :Cluster size:A:3,6:\n\n"
    "     $$ FRF_score_% Cl_FRF_score_%  Likelihood PeakNr Cl_PeakNr Size$$ $$\n");

      for (unsigned t = 0; t < num_selected; t++) {
        output.logTab(0,DEBUG,"       " + dtos(siteList[t].value2) +
                "      " + dtos(siteList[t].value3) +
                "     " + dtos(siteList[t].value) +
                "     " + itos(siteList[t].num) +
                "     " + itos(siteList[t].clusterNum) +
                "      " + itos(siteList[t].clusterMul));
        xbar += siteList[t].value3;
        x2 += fn::pow2(siteList[t].value3);
        ybar += siteList[t].value;
        y2 += fn::pow2(siteList[t].value);
        xy += siteList[t].value*siteList[t].value3;
        n++;
      }
      output.logTab(0,DEBUG," $$ \"><b>For inline graphs use a Java browser</b></applet>");
      floatType denom = sqrt((x2-xbar*xbar/n)*(y2-ybar*ybar/n));
      if (denom > 0.)
      {
        floatType r = (xy-xbar*ybar/n)/denom;
        output.logTab(0,DEBUG,"        Correlation coefficient of printed peaks is " + dtos(r));
      }
    }
  }
}

floatType SiteListAng::getRmax()
{
  if (!siteList.size()) return 0;
  floatType max_val(siteList[0].value);
  for (unsigned i = 0; i < siteList.size(); i++)
    max_val = std::max(max_val,siteList[i].value);
  return max_val;
}

floatType SiteListAng::getRmin()
{
  if (!siteList.size()) return 0;
  floatType min_val(siteList[0].value);
  for (unsigned i = 0; i < siteList.size(); i++)
    min_val = std::min(min_val,siteList[i].value);
  return min_val;
}

floatType SiteListAng::getRmean()
{
  if (!siteList.size()) return 0;
  floatType min_val = getRmin();
  floatType max_val = getRmax();
  //distribute roughly about zero
  floatType sub_val = (max_val-min_val)/2.0;

  //Calculate mean and stddev of sites
  floatType total(0);
  double n = siteList.size();
  for (unsigned i = 0; i < siteList.size(); i++)
  {
    total += siteList[i].value-sub_val;
  }
  return n ? (total/n + sub_val) : 0 ;
}

floatType SiteListAng::getRsigma()
{
  if (!siteList.size()) return 0;

  //Calculate mean and stddev of sites
  floatType mean(0),sigma(0),totalsqr(0),total(0);
  double n = siteList.size();
  for (unsigned i = 0; i < siteList.size(); i++) //all rot
  {
    total += siteList[i].value;
    totalsqr += fn::pow2(siteList[i].value);
  }
  mean = total/n;
  totalsqr /= n;
  //use sigma n-1 as this is a sample not population
  sigma = (n-1) ? (n/(n-1))*std::sqrt(totalsqr-fn::pow2(mean)) : 0;
  return sigma;
}

}// phaser
