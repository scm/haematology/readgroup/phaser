//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <float.h>
#include <phaser/io/Errors.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/rad2phi.h>
#include <phaser/src/PhsStats.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/Integration.h>
#include <scitbx/constants.h>
#include <scitbx/math/erf.h>

// Log output

namespace phaser {

void RefineSAD::writeMtzdebug(std::string HKLOUT,std::string RefID,std::string HKLIN,Output& output)
{
  MtzHandler mtzOut((*this),(*this),miller,false,HKLIN);
  mtzOut.addData(miller,selected,"FWTA","F",FWTA,RefID);
  mtzOut.addData(miller,selected,"PHWTA","P",PHWTA,RefID);
  mtzOut.addData(miller,selected,"PHIB(+)","P",PHIBpos,RefID);
  mtzOut.addData(miller,selected,"FOM(+)","W",FOMpos,RefID);
  mtzOut.addData(miller,selected,"PHIB(-)","P",PHIBneg,RefID);
  mtzOut.addData(miller,selected,"FOM(-)","W",FOMneg,RefID);
  mtzOut.addData(miller,selected,"HL#(+)","A",getHL(1),RefID);
  mtzOut.addData(miller,selected,"HL#(-)","A",getHL(2),RefID);
  af_float tmp(FHpos.size(),0);
  for (unsigned r = 0; r < FHpos.size(); r++) tmp[r] = std::abs(FHpos[r]);
  mtzOut.addData(miller,selected,"FH(+)","G",tmp,RefID);
  for (unsigned r = 0; r < FHpos.size(); r++) tmp[r] = rad2phi(std::arg(FHpos[r]));
  mtzOut.addData(miller,selected,"PH(+)","L",tmp,RefID);
  for (unsigned r = 0; r < FHneg.size(); r++) tmp[r] = std::abs(FHneg[r]);
  mtzOut.addData(miller,selected,"FH(-)","G",tmp,RefID);
  for (unsigned r = 0; r < FHneg.size(); r++) tmp[r] = rad2phi(std::arg(FHneg[r]));
  mtzOut.addData(miller,selected,"PH(-)","L",tmp,RefID);
  mtzOut.writeMtz(HKLOUT);
  if (mtzOut.not_subset())
    output.logAdvisory(SUMMARY,"Output reflections not a subset of " + HKLIN);
}

} //namespace phaser
