//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __Phaser__Integration__Class__
#define __Phaser__Integration__Class__
#include <phaser/main/Phaser.h>
#include <cctbx/hendrickson_lattman.h>

namespace phaser {

class acentric
{
  public:
    acentric()
    { LogLike = WtPart = 0; phsProb.clear(); Ang.clear(); }
    acentric(int Size)
    { LogLike = WtPart = 0; phsProb.resize(Size,0); Ang.resize(Size,0); }
  public:
    float1D phsProb,Ang;
    floatType LogLike;
    floatType WtPart;
    floatType FOM();
    floatType PHIB();
    cctbx::hendrickson_lattman<double>  HL();
    void      setProb(int p,floatType P,floatType A) { phsProb[p] = P; Ang[p] = A; }
};

class centric
{
  public:
    centric()
    { LogLike = phsProb1 = phsProb2 = phsr = 0; }
  public:
    floatType LogLike;
  private:
    floatType phsProb1,phsProb2,phsr;
  public:
    void      setProb(floatType p1,floatType p2,floatType p) { phsProb1 = p1; phsProb2 = p2; phsr = p; }
    floatType FOM();
    floatType PHIB();
    cctbx::hendrickson_lattman<double>  HL();
};

class singleton
{
  public:
    singleton()
    { LogLike = X = phib = 0; }
  public:
    floatType LogLike;
  private:
    floatType X,phib;
  public:
    void      setProb(floatType x,floatType a) { X = x; phib = a; }
    floatType FOM();
    floatType PHIB();
    cctbx::hendrickson_lattman<double>  HL();
};

} //phaser
#endif
