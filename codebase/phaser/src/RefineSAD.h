//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineSADClass__
#define __RefineSADClass__
#include <phaser/src/DataSAD.h>
#include <phaser/ep_objects/data_atom.h>
#include <phaser/include/data_ffts.h>
#include <phaser/include/data_restraint.h>
#include <phaser/include/VarianceEP.h>
#include <phaser/include/ProtocolSAD.h>
#include <phaser/pod/Derivatives.h>
#include <phaser/pod/Curvatures.h>
#include <phaser/include/data_peak.h>
#include <phaser/include/data_find.h>
#include <phaser/include/data_llgc.h>
#include <phaser/ep_objects/ep_search.h>
#include <phaser/src/RefineBase2.h>
#include <phaser/src/EqualUnity.h>
#include <phaser/src/LlgcMapSites.h>
#include <phaser/src/Integration.h>
#include <phaser/src/Cluster.h>
#include <phaser/pod/probType.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>

namespace phaser {

typedef af::shared<cctbx::hendrickson_lattman<double> > af_hl;

class RefineSAD : public DataSAD, public RefineBase2
{
  public:
    //Single value
    floatType   ScaleK,ScaleU;
    std::map<std::string,int> atomtype2t; //lookup array for t
    bool use_working_set;
    data_ffts FFTvDIRECT;
    bool input_atoms,input_partial;
    bool1D input_fix_fdp;
    double wilson_llg;
    float1D wilson_llg_refl;

    //Data dependent on atom [a]
    std::vector<Cluster> CLUSTER;
    std::map<int,int> ptncs_pairs;

    //Data dependent on atomtype [t]
    float1D AtomFdpInit;

    //set
    std::set<int> pdb_deleted_set;

  public:
    void logScale(outStream,Output&);
    void logAtoms(outStream,Output&,bool=false);
    void logScat(outStream,Output&);
    void logSigmaA(outStream,Output&);
    void logStatInfo(Output&,outStream);
    void logFOM(outStream,Output&,bool=true);
    void logOutliers(outStream,Output&);
    void logCurrent(outStream,Output&);
    void logInitial(outStream,Output&);
    void logFinal(outStream,Output&);
    void logProtocolPars(outStream,Output&);

  public:
    RefineSAD();
    //constructor for C++/phaser
    RefineSAD(std::string, af::double6,
              af::shared<miller::index<int> >,
              data_spots, //POS
              data_spots, //NEG
              data_tncs, //tncs epsn
              af_atom,
              std::string, //cluster_pdb
              data_bins,
              std::map<std::string,cctbx::eltbx::fp_fdp>, //atomtype fp fdp
              data_outl, //outliers
              data_restraint, floatType, //wilson restraints
              data_restraint, //sphericity restraints
              data_restraint, //fdp restraints
              data_part,
              data_ffts,
              float1D, //wilson_llg_refl
              Output&
    );
    //constructor for python/phenix.refine
    RefineSAD(cctbx::sgtbx::space_group, cctbx::uctbx::unit_cell,
              af::shared<miller::index<int> >,
              af_bool, //working set
              data_spots, //POS
              data_spots, //NEG
              Output&, //print_output
              std::string // partial model
    );
    ~RefineSAD() {}

  private:
    //real contructor
    void init(af_bool, //working set
              data_restraint, floatType, //wilson restraints
              data_restraint, //sphericity restraints
              data_restraint, //fdp restraints
              data_ffts, //partial structure
              float1D, //wilson_llg_refl
              Output&
    );

    void initAtomic(af_atom,std::map<std::string,cctbx::eltbx::fp_fdp>,std::string,Output&);

  private:
    unsigned    nsigmaa_ref,nsigmaa_all,nscales_ref,nscales_all;
    bool        reversed;
    bool        use_fft;
    bool        sad_target_anom_only;
    floatType   WilsonB;
    data_restraint SPHERICITY,WILSON,FDP;
    ProtocolSAD protocol;
    bool        FIX_ATOMIC,FIX_SIGMAA,FIX_SCALES;
    cctbx::sgtbx::site_symmetry_table site_sym_table;

    // -- Data dependent on reflection [r]
    unsigned1D  ibin; // integration steps
    af_cmplx    FHpos; // calculated heavy atom sf
    af_cmplx    FHneg; // calculated heavy atom sf
    af_cmplx    FApos;
    af_cmplx    FAneg;
    af_cmplx    LLGRAD_SF; // LLG coefficient
    float1D     sigDsqr;
    float1D     sigPlus;
    float1D     DphiA;
    float1D     DphiB;
    af_bool     working; // LLG coefficient

  private:
    af_float    FWTA,PHWTA;
    af_float    FOMpos,PHIBpos,FOMneg,PHIBneg;
    af_hl       HLpos,HLneg;

    //Data dependent on reflection and atomtype [r][t]
    float2D  fo_plus_fp; // FormFactor value for reflection

    std::vector<EqualUnity>  acentPhsIntg;
    bool fixed_num_integration_points;
    scitbx::fftpack::complex_to_complex_3d<floatType> cfft;
    floatType u_base;

//dL_by_dAtomPar [a] atom
  void dL_by_dAtomicPar(unsigned);
  void d2L_by_dAtomicPar2(unsigned);
  void dL_by_dSigmaaPar(unsigned);
  void d2L_by_dSigmaaPar2(unsigned);
  void dL_by_dScalesPar(unsigned);
  void d2L_by_dScalesPar2(unsigned);
  float1D dL_by_dPar,d2L_by_dPar2;

//dL_by_dFunctionPar function for each reflection
  floatType dL_by_dReFHpos,dL_by_dImFHpos,dL_by_dReFHneg,dL_by_dImFHneg,dL_by_dSDsqr,dL_by_dSA,dL_by_dSB,dL_by_dSP;

//dFH_by_dAtomPar
//constants
  floatType dReFHpos_by_dK, dImFHpos_by_dK, dReFHneg_by_dK, dImFHneg_by_dK;
  floatType dReFHpos_by_dB, dImFHpos_by_dB, dReFHneg_by_dB, dImFHneg_by_dB;
  floatType dReFHpos_by_dPartK, dImFHpos_by_dPartK, dReFHneg_by_dPartK, dImFHneg_by_dPartK;
  floatType dReFHpos_by_dPartU, dImFHpos_by_dPartU, dReFHneg_by_dPartU, dImFHneg_by_dPartU;
  float1D dReFHpos_by_dX, dImFHpos_by_dX, dReFHneg_by_dX, dImFHneg_by_dX;
  float1D dReFHpos_by_dY, dImFHpos_by_dY, dReFHneg_by_dY, dImFHneg_by_dY;
  float1D dReFHpos_by_dZ, dImFHpos_by_dZ, dReFHneg_by_dZ, dImFHneg_by_dZ;
  float1D dReFHpos_by_dO, dImFHpos_by_dO, dReFHneg_by_dO, dImFHneg_by_dO;
  floatType dReFHpos_by_dO_px, dImFHpos_by_dO_px, dReFHneg_by_dO_px, dImFHneg_by_dO_px;
  float1D dReFHpos_by_dIB, dImFHpos_by_dIB, dReFHneg_by_dIB, dImFHneg_by_dIB;
  std::vector<dmat6 >
          dReFHpos_by_dAB,dImFHpos_by_dAB,dReFHneg_by_dAB,dImFHneg_by_dAB;
  float1D dReFHpos_by_dFdp,dImFHpos_by_dFdp,dReFHneg_by_dFdp,dImFHneg_by_dFdp;

  floatType d2ReFHpos_by_dK2, d2ImFHpos_by_dK2, d2ReFHneg_by_dK2, d2ImFHneg_by_dK2;
  floatType d2ReFHpos_by_dB2, d2ImFHpos_by_dB2, d2ReFHneg_by_dB2, d2ImFHneg_by_dB2;
  floatType d2ReFHpos_by_dPartK2, d2ImFHpos_by_dPartK2, d2ReFHneg_by_dPartK2, d2ImFHneg_by_dPartK2;
  floatType d2ReFHpos_by_dPartU2, d2ImFHpos_by_dPartU2, d2ReFHneg_by_dPartU2, d2ImFHneg_by_dPartU2;
  float1D d2ReFHpos_by_dX2, d2ImFHpos_by_dX2, d2ReFHneg_by_dX2, d2ImFHneg_by_dX2;
  float1D d2ReFHpos_by_dY2, d2ImFHpos_by_dY2, d2ReFHneg_by_dY2, d2ImFHneg_by_dY2;
  float1D d2ReFHpos_by_dZ2, d2ImFHpos_by_dZ2, d2ReFHneg_by_dZ2, d2ImFHneg_by_dZ2;
  float1D d2ReFHpos_by_dO2, d2ImFHpos_by_dO2, d2ReFHneg_by_dO2, d2ImFHneg_by_dO2;
  floatType d2ReFHpos_by_dO2_px, d2ImFHpos_by_dO2_px, d2ReFHneg_by_dO2_px, d2ImFHneg_by_dO2_px;
  float1D d2ReFHpos_by_dIB2, d2ImFHpos_by_dIB2, d2ReFHneg_by_dIB2, d2ImFHneg_by_dIB2;
  std::vector<dmat6 >
          d2ReFHpos_by_dAB2,d2ImFHpos_by_dAB2,d2ReFHneg_by_dAB2,d2ImFHneg_by_dAB2;
  float1D d2ReFHpos_by_dFdp2,d2ImFHpos_by_dFdp2,d2ReFHneg_by_dFdp2,d2ImFHneg_by_dFdp2;

  //reflection hessian
  floatType d2L_by_dSDsqr2,d2L_by_dSA2,d2L_by_dSB2,d2L_by_dSP2;
  floatType d2L_by_dReFHpos2,d2L_by_dReFHpos_dImFHpos,d2L_by_dReFHpos_dReFHneg,d2L_by_dReFHpos_dImFHneg;
  floatType d2L_by_dImFHpos2,d2L_by_dImFHpos_dReFHneg,d2L_by_dImFHpos_dImFHneg;
  floatType d2L_by_dReFHneg2,d2L_by_dReFHneg_dImFHneg;
  floatType d2L_by_dImFHneg2;

  public:
    void applyShift(TNT::Vector<floatType>&);
    void applyShift(af_float);
    void setProtocol(protocolPtr,Output&);
    void cleanUp(outStream,Output&);
    std::vector<reparams>  getRepar();
    bool1D getRefineMask(protocolPtr);
    floatType Rfactor();
    floatType targetFn();
    floatType wilson_restraint_likelihood();
    floatType sphericity_restraint_likelihood();
    floatType fdp_restraint_likelihood();
    floatType ptncs_link_restraint_likelihood();
    floatType gradientFn(TNT::Vector<floatType>&);
    floatType gradientFn(af_float);
    Derivatives dL_by_dFC();
    Curvatures d2L_by_dFC2();
    floatType hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    std::string whatAmI(int&);
    TNT::Vector<floatType> getRefinePars();
    TNT::Vector<floatType> getLargeShifts();
    std::vector<bounds> getLowerBounds();
    std::vector<bounds> getUpperBounds();

    //-- Log Functions
    void        logDataStats(outStream,unsigned,Output&);

  private:
    void        calcAtomicData();
    void        calcScalesData();
    floatType   lowerO();
    floatType   lowerU();
    floatType   upperU();
    floatType   tolU();
    floatType   largeUshift();
    floatType   SigmaH(unsigned&);
    void        prepareAtomicFFT();
    void        calcAtomicDataFFT();
    void        calcAtomicDataSum();
    void        resizeAtomArrays();
    void        calcGradFFT(af::shared<miller::index<int> >,af_cmplx);
    floatType   constrain_restrain_gradient(TNT::Vector<floatType>&);
    floatType   constrain_restrain_hessian(TNT::Fortran_Matrix<floatType>&);

    //acentric reflections, F+ and F- present
    acentric    acentricReflIntgSAD1(unsigned&,bool=false);
    acentric    acentricReflIntgSAD2(unsigned&);
    floatType   acentricReflProbSAD2(unsigned&,bool,floatType);
    floatType   acentricReflGradSAD2(unsigned&,bool=true,bool=true);
    floatType   acentricReflHessSAD2(unsigned&,bool=true,bool=true);

    //centric reflections
    centric     centricReflIntgSAD2(unsigned&);
    floatType   centricReflGradSAD2(unsigned&,bool=true,bool=true);
    floatType   centricReflHessSAD2(unsigned&,bool=true,bool=true);

    //acentric singleton reflections, only one of F+ and F- present (bool indicates which)
    singleton   singletonReflIntgSAD2(unsigned&,bool);
    floatType   singletonReflGradSAD2(unsigned&,bool,bool=true,bool=true);
    floatType   singletonReflHessSAD2(unsigned&,bool,bool=true,bool=true);

  public:
    void useWorkingSet();
    void useTestSet();
    void setFC(af_cmplx,af_cmplx);
    void calcSigmaaData();
    void calcIntegrationPoints();
    void calcOutliers();
    void rejectOutliers(outStream,Output&);
    void calcBinStats();
    floatType calcFOMsubset(af_float);
    void calcPhsStats(Output&);
    void calcVariances();
    void addAtoms(af_atom,std::map<int,int>,Output&);
    int  numAtoms() { return atoms.size(); }
    void restoreAtoms(outStream,int,std::multimap<int,restored_data>&,int,Output&);
    void anisoAtoms(outStream,int,std::set<int>&,int,Output&);
    void deleteAtoms(outStream,int,std::set<int>&,int,Output&);
    void changeAtoms(outStream,changed_list&,Output&);
    changed_list findChangedAtoms(stringset,std::string,bool,outStream,Output&);
    std::set<int> findDeletedAtoms(outStream,Output&,std::vector<std::pair<int,std::string> >);
    void setAtomsInput(af_atom);

    af_cmplx     calcGradCoefs(std::string,bool,af_float=af_float(),af_float=af_float());
    void         calcGradCoefMaps(stringset,bool);
    LlgcMapSites calcGradMaps(outStream,std::string,floatType,data_llgc,int,bool,Output&);

    void setupIntegrationPoints(int);
    void setTarget(std::string);
    void setFixFdp(std::map<std::string,bool> );
    floatType get_refined_scaleK() { return ScaleK; }
    floatType get_refined_scaleU() { return ScaleU; }
    void set_scaleK(floatType K) { ScaleK = K; }
    void set_scaleU(floatType U) { ScaleU = U; }
    af_bool  getSelected() { return selected; }
    af_bool  getCentric()  { return cent; }
    af_hl    getHL(int=0);

  public:
    floatType getMaxDistSpecial(TNT::Vector<floatType>&,TNT::Vector<floatType>&,bool1D&,TNT::Vector<floatType>&,floatType&);
    std::vector<std::pair<int,int> >  getAniso();
    void writeMtzdebug(std::string,std::string,std::string,Output&);
    void calcPartialStructure(Output&);
    void initVariancesFromFC();
    double wilsonFn(float1D&);

    //sub-structure determination
  public:
    ep_search  fast_search(data_find,data_peak,double,af::versa<double, af::c_grid<3> >,Output&);
    void phase_o_phrenia(outStream,Output&);
  private:
    void calcFpart(af::shared<miller::index<int> >,af::shared<cmplxType>);
    double LESSTF1(data_find,af::shared<cmplxType>,af_float);
    std::pair<double,double> LdL_by_dUsqr_at_0(unsigned, double, double);

  public: //python
    double stats_numbins() { return allStats.numbins(); }
    double stats_hires() { return allStats.HiRes(); }
    double stats_lores() { return allStats.LoRes(); }
    double stats_acentric_fom() { return acentStats.FOM(); }
    double stats_acentric_num() { return acentStats.Num(); }
    double stats_centric_fom() { return centStats.FOM(); }
    double stats_centric_num() { return centStats.Num(); }
    double stats_fom() { return allStats.FOM(); }
    double stats_num() { return allStats.Num(); }

};

} //phaser
#endif
