//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Hexagonal.h>
#include <phaser/src/Brick.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/Errors.h>

namespace phaser {

Hexagonal::Hexagonal(UnitCell u,SpaceGroup s) : UnitCell(u), SpaceGroup(s)
{
  hexagonal_type = not_set;
  t = max_t = 0;
  START = SLOPE = POINT = orthSamp = orthMin = orthMax = orthSite = dvect3(0,0,0);
  off0 = off1 = 0;
  L = U = V = 0;
  ONE = 1.0;
  check_points = false;
}

bool Hexagonal::at_end() const
{ return (t >= max_t); }

void Hexagonal::restart()
{ t = 0; }

std::size_t Hexagonal::count_sites()
{ return max_t; }

dvect3 Hexagonal::next_site_frac(int ext_t)
{
  switch (hexagonal_type)
  {
    case (alongLine):   return nextAlongLine(ext_t);
    case (aroundPoint): return nextAroundPoint(ext_t);
    case (inPlane):     return nextInPlane(ext_t);
    case (inRegion):    return nextInRegion(ext_t);
    case (not_set):     ;
  }
  return dvect3(0,0,0);
}

void Hexagonal::setupAlongLine(dvect3 S,dvect3 END,floatType SAMP,bool vol_check,bool isFirst)
{
  max_t = 0;
  START = S; SLOPE = END-START;
  if (vol_check && isFirst) VOL_FN = &SpaceGroup::inCheshire;
  else if (vol_check && !isFirst) VOL_FN = &SpaceGroup::inPrimitive;
  else VOL_FN = &SpaceGroup::alwaysTrue;
  MAXL = (std::sqrt(SLOPE*SLOPE)/SAMP) + 1;
  hexagonal_type = alongLine;
  for (int l = 0; l < MAXL; l++)
  {
    for (unsigned i = 0; i < 3; i++) orthSite[i] = SLOPE[i]*l/MAXL+START[i];
    dvect3 fracSite(UnitCell::doOrth2Frac(orthSite));
    if ((this->*Hexagonal::VOL_FN)(fracSite))
    {
      max_t++;
      if (check_points) comp.push_back(orthSite);
    }
  }
  L = 0;
  t = 0;
  for (unsigned i = 0; i < 3; i++) orthSite[i] = START[i];
}

dvect3 Hexagonal::nextAlongLine(int ext_t)
{
  while (t < ext_t)
  {
    do {
      L++;
      for (unsigned i = 0; i < 3; i++) orthSite[i] = SLOPE[i]*L/MAXL+START[i];
    }
    while (!(this->*Hexagonal::VOL_FN)(UnitCell::doOrth2Frac(orthSite)));
    t++;
  }
  return UnitCell::doOrth2Frac(orthSite);
}

void Hexagonal::setupAroundPoint(dvect3 P,floatType SAMP,floatType RANGE,bool vol_check,bool isFirst)
{
  max_t = 0;
  POINT = P;
  if (vol_check && isFirst) VOL_FN = &SpaceGroup::inCheshire;
  else if (vol_check && !isFirst) VOL_FN = &SpaceGroup::inPrimitive;
  else VOL_FN = &SpaceGroup::alwaysTrue;
  hexagonal_type = aroundPoint;
  max_t++;  //point
  if (check_points) comp.push_back(POINT);
  dvect3 orthRange(RANGE,RANGE,RANGE);
  orthMin = POINT - orthRange;
  orthMax = POINT + orthRange;
  RANGEsqr = fn::pow2(RANGE);
  orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
  off1 = 5.0/6.0;
  bool first_point(true);
  dvect3 first_orthSite(orthSite);
  floatType first_off1(off1);
  for (orthSite[2] = orthMin[2]; orthSite[2] < orthMax[2]; orthSite[2] += orthSamp[2])
  {
    off1 = ONE - off1;
    off0 = 3.0/4.0;
    for (orthSite[1] = orthMin[1] + off1 * orthSamp[1]; orthSite[1] < orthMax[1]; orthSite[1] += orthSamp[1])
    {
      off0 = ONE - off0;
      for (orthSite[0] = orthMin[0] + off0 * orthSamp[0]; orthSite[0] < orthMax[0]; orthSite[0] += orthSamp[0])
      {
        if ((orthSite-POINT)*(orthSite-POINT) < RANGEsqr &&
            (this->*Hexagonal::VOL_FN)(UnitCell::doOrth2Frac(orthSite)))
        {
          if (first_point) first_orthSite = orthSite;
          if (first_point) first_off1 = off1;
          first_point = false;
          if (check_points) comp.push_back(orthSite);
          max_t++;
        }
      }
    }
  }
  t = 0;
  orthSite = first_orthSite;
  off1 = first_off1;
}

dvect3 Hexagonal::nextAroundPoint(int ext_t)
{
  //calculations done in terms of orthogonal sampling, output as fractional
  if (t == ext_t && t == 0) return UnitCell::doOrth2Frac(POINT);
  while (t < ext_t)
  {
    do {
      orthSite[0] += orthSamp[0];
      if (orthSite[0] < orthMax[0]) { }
      else
      {
        orthSite[1] += orthSamp[1];
        if (orthSite[1] < orthMax[1])
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
        }
        else
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          orthSite[2] += orthSamp[2];
          if (orthSite[2] < orthMax[2])
          {
            off1 = ONE - off1;
            off0 = 3.0/4.0;
            orthSite[1] = orthMin[1] + off1 * orthSamp[1];
            off0 = ONE - off0;
            orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          }
          else { break; } //out of region
        }
        off0 = ONE - off0;
      }
    }
    while (!((orthSite-POINT)*(orthSite-POINT) < RANGEsqr) ||
           !(this->*Hexagonal::VOL_FN)(UnitCell::doOrth2Frac(orthSite)));
    t++;
  }
  return  UnitCell::doOrth2Frac(orthSite);
}

void Hexagonal::setupBrickRegion(Brick brick,floatType SAMP,bool vol_check,bool isFirst)
{
  max_t = 0;
  if (vol_check && isFirst) VOL_FN = &SpaceGroup::inCheshire;
  else if (vol_check && !isFirst) VOL_FN = &SpaceGroup::inPrimitive;
  else VOL_FN = &SpaceGroup::alwaysTrue;
  orthMin = brick.orthMin();
  orthMax = brick.orthMax();

  if (brick.isLineX(SAMP) || brick.isLineY(SAMP) || brick.isLineZ(SAMP))
  {
    setupAlongLine(orthMin,orthMax,SAMP,true,isFirst);
  }
  else if (brick.isPlaneX(SAMP) || brick.isPlaneY(SAMP) || brick.isPlaneZ(SAMP))
  {
    hexagonal_type = inPlane;
    if (brick.isPlaneX(SAMP)) { U=1; V=2; }
    else if (brick.isPlaneY(SAMP)) { U=0; V=2; }
    else if (brick.isPlaneZ(SAMP)) { U=0; V=1; }
    orthSamp[U] = std::sqrt(3.0/4.0)*SAMP;
    orthSamp[V] = SAMP;
    off0 = 1.0/4.0;
    for (orthSite[U] = orthMin[U]; orthSite[U] < orthMax[U]; orthSite[U] += orthSamp[U])
    {
      off0 = ONE - off0;
      for (orthSite[V] = orthMin[V] + off0 * orthSamp[V]; orthSite[V] < orthMax[V]; orthSite[V] += orthSamp[V])
      {
        max_t++;
        if (check_points) comp.push_back(orthSite);
      }
    }
    off0 = 1.0/4.0;
    orthSite[U] = orthMin[U];
    off0 = ONE - off0;
    orthSite[V] = orthMin[V] + off0 * orthSamp[V];
  }
  else
  {
    hexagonal_type = inRegion;
    orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
    off1 = 5.0/6.0;
    for (orthSite[2] = orthMin[2]; orthSite[2] < orthMax[2]; orthSite[2] += orthSamp[2])
    {
      off1 = ONE - off1;
      off0 = 3.0/4.0;
      for (orthSite[1] = orthMin[1] + off1 * orthSamp[1]; orthSite[1] < orthMax[1]; orthSite[1] += orthSamp[1])
      {
        off0 = ONE - off0;
        for (orthSite[0] = orthMin[0] + off0 * orthSamp[0]; orthSite[0] < orthMax[0]; orthSite[0] += orthSamp[0])
          if ((this->*Hexagonal::VOL_FN)(UnitCell::doOrth2Frac(orthSite)))
          {
            max_t++;
            if (check_points) comp.push_back(orthSite);
          }
      }
    }
    off1 = 5.0/6.0;
    orthSite[2] = orthMin[2];
    off1 = ONE - off1;
    floatType off0(3.0/4.0);
    orthSite[1] = orthMin[1] + off1 * orthSamp[1];
    off0 = ONE - off0;
    orthSite[0] = orthMin[0] + off0 * orthSamp[0];
  }
}

dvect3 Hexagonal::nextInPlane(int ext_t)
{
  int t=0;
  off0 = 1.0/4.0;
  for (orthSite[U] = orthMin[U]; orthSite[U] < orthMax[U]; orthSite[U] += orthSamp[U])
  {
    off0 = ONE - off0;
    for (orthSite[V] = orthMin[V] + off0 * orthSamp[V]; orthSite[V] < orthMax[V]; orthSite[V] += orthSamp[V])
    {
      if (t == ext_t) return UnitCell::doOrth2Frac(orthSite);
      else t++;
    }
  }
  return UnitCell::doOrth2Frac(orthSite);
}

dvect3 Hexagonal::nextInRegion(int ext_t)
{
  while (t < ext_t)
  {
    do {
      orthSite[0] += orthSamp[0];
      if (orthSite[V] < orthMax[V]) { }
      else
      {
        orthSite[1] += orthSamp[1];
        if (orthSite[1] < orthMax[1])
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
        }
        else
        {
          orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          orthSite[2] += orthSamp[2];
          if (orthSite[2] < orthMax[2])
          {
            off1 = ONE - off1;
            off0 = 3.0/4.0;
            orthSite[1] = orthMin[1] + off1 * orthSamp[1];
            off0 = ONE - off0;
            orthSite[0] = orthMin[0] + off0 * orthSamp[0];
          }
          else { break; } //out of region
        }
        off0 = ONE - off0;
      }
    } //calculate hexagonal grid until a point is found in Region
    while (!(this->*Hexagonal::VOL_FN)(UnitCell::doOrth2Frac(orthSite)));
    t++; //increment counter and store
  }
  return UnitCell::doOrth2Frac(orthSite);
}

void Hexagonal::setupAsuRegion(floatType SAMP,bool isFirst)
{
  if (isFirst)
  {
    Brick brick = SpaceGroup::brickCheshire((*this));
    setupBrickRegion(brick,SAMP,true,isFirst);
  }
  else
  {
    Brick brick = SpaceGroup::brickPrimitive((*this));
    setupBrickRegion(brick,SAMP,true,isFirst);
  }
}

dvect3 Hexagonal::getOrthMin() {return orthMin;}
dvect3 Hexagonal::getOrthMax() {return orthMax;}

} //end namespace phaser
