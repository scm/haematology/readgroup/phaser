//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Sites.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>

namespace phaser {

/*--------
site Class
--------*/
site::site()
{
  value = 0;
  zscore = 0;
  value2 = 0;
  value3 = 0;
  topsite = true;
  distance = 0;
  clusterMul = 0;
  clusterNr = 0;
  clusterNum = 0;
  num = 0;
  selected = true;
}

void site::setValue(floatType v)
{ value=v; }

void site::archiveValue()
{ value2=value; }

bool site::operator<(const site &right)  const
{return (right.value < value);}

const site& site::operator=(const site& right)
{
  if (&right != this)   // check for self-assignment
  {
    value = right.value;
    value2 = right.value2;
    value3 = right.value3;
    zscore = right.zscore;
    topsite = right.topsite;
    distance = right.distance;
    clusterMul = right.clusterMul;
    clusterNr = right.clusterNr;
    clusterNum = right.clusterNum;
    num = right.num;
    selected = right.selected;
  }
  return *this;
}

/*-----------
angsite Class
-----------*/
angsite::angsite() : site()
{
  euler = dvect3(0,0,0);
  deep = false;
}

angsite::angsite(const angsite& init) : site(init)
{
  euler = init.euler;
  deep = init.deep;
}

const angsite& angsite::operator=(const angsite& right)
{
  if (&right != this)   // check for self-assignment
  {
    site::operator=(right);
    euler = right.euler;
    deep = right.deep;
  }
  return *this;
}

dmat33 angsite::getMatrix()
{ return euler2matrixDEG(euler); }

const dvect3& angsite::getEuler() const
{
  return euler;
}

void angsite::setMatrix(dmat33 val)
{ euler = matrix2eulerDEG(val); }

std::string angsite::str()
{
  return  " " +  dtos(euler[0]) + " " + dtos(euler[1]) + " " + dtos(euler[2]);
}

bool angsite::operator<(const angsite &right)  const
{
  if (right.value == value)
  {
    if (right.euler[0] == euler[0])
      return (right.euler[1] == euler[1]) ? (right.euler[2] < euler[2]) : (right.euler[1] < euler[1]);
    else return (right.euler[0] < euler[0]);
  }
  return (right.value < value);
}


/*-----------
xyzsite Class
-----------*/
xyzsite::xyzsite()  : site()
{
  orth = dvect3(0,0,0);
}

xyzsite::xyzsite(const xyzsite& init) : site(init)
{
  orth = init.orth;
}

const xyzsite& xyzsite::operator=(const xyzsite& right)
{
  if (&right != this)   // check for self-assignment
  {
    site::operator=(right);
    orth = right.orth;
  }
  return *this;
}

dvect3 xyzsite::getOrthXYZ()
{ return orth; }

dvect3 xyzsite::getFracXYZ(UnitCell uc)
{ return uc.doOrth2Frac(orth); }

void xyzsite::setOrthXYZ(dvect3& v)
{ orth = v; }

void xyzsite::setFracXYZ(dvect3& v,UnitCell uc)
{ orth = uc.doFrac2Orth(v); }

bool xyzsite::operator<(const xyzsite &right)  const
{
  if (right.value == value)
  {
    if (right.orth[0] == orth[0])
      return (right.orth[1] == orth[1]) ? (right.orth[2] < orth[2]) : (right.orth[1] < orth[1]);
    else return (right.orth[0] < orth[0]);
  }
  return (right.value < value);
}

}//phaser
