//c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineSAD.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/jiffy.h>
#include <phaser/src/Integration.h>

// Gradient related code

namespace phaser {

floatType RefineSAD::gradientFn(af_float af_Gradient)
{
  //format change for python access
  af_Gradient.resize(npars_ref);
  TNT::Vector<floatType> TNT_Gradient(npars_ref);
  floatType totLogLike = gradientFn(TNT_Gradient); //gradient in TNT_Gradient
  for (int i = 0; i < npars_ref; i++) af_Gradient[i] = TNT_Gradient[i]; //copy to af::shared
  return totLogLike;
}

floatType RefineSAD::gradientFn(TNT::Vector<floatType>& Gradient)
{
  //first make sure new structure factors are calculated
  //Speed optimization - new sf calculation is done in the function call that immediately preceeds the
  //call of this gradient function
  //initialization
  floatType totLogLike(-wilson_llg);
  PHASER_ASSERT(Gradient.size() == npars_ref);
  for (unsigned i = 0; i < npars_ref; i++) Gradient[i]  = 0;

  af::shared<miller::index<int> > expanded_miller;
  af_cmplx expanded_gradient;
  expanded_miller.reserve(NREFL*2);
  expanded_gradient.reserve(NREFL*2);
  calcIntegrationPoints();
  //if using fft, collect terms for atomic fft deriv calculation
  //else do the full atomic deviatives by the chain rule by summation
  //Also, do the sigmaa derivs (which don't involve direct summation)
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    if (both[r]) // i.e. && (sad_target_anom_only || !sad_target_anom_only)
    {
      totLogLike += acentricReflGradSAD2(r,!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
      if (use_fft && !FIX_ATOMIC)
      {
        expanded_gradient.push_back(cmplxType(dL_by_dReFHpos,dL_by_dImFHpos));
        expanded_miller.push_back(miller[r]);
        expanded_gradient.push_back(std::conj(cmplxType(dL_by_dReFHneg,dL_by_dImFHneg)));
        expanded_miller.push_back(-miller[r]);
      }
      else if (!use_fft && !FIX_ATOMIC) dL_by_dAtomicPar(r); //results in dL_by_dPar
      if (!FIX_SCALES) dL_by_dScalesPar(r); //results in dL_by_dPar
      if (!FIX_SIGMAA) dL_by_dSigmaaPar(r); //results in dL_by_dPar
    }
    else if (!sad_target_anom_only && cent[r])
    {
      totLogLike += centricReflGradSAD2(r,!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
      if (use_fft && !FIX_ATOMIC)
      {
        expanded_gradient.push_back(cmplxType(dL_by_dReFHpos,dL_by_dImFHpos));
        expanded_miller.push_back(miller[r]);
      }
      else if (!use_fft && !FIX_ATOMIC) dL_by_dAtomicPar(r); //results in dL_by_dPar
      if (!FIX_SCALES) dL_by_dScalesPar(r); //results in dL_by_dPar
      if (!FIX_SIGMAA) dL_by_dSigmaaPar(r); //results in dL_by_dPar
    }
    else if (!sad_target_anom_only)
    {
      totLogLike += singletonReflGradSAD2(r,plus[r],!(FIX_ATOMIC && FIX_SCALES),!FIX_SIGMAA);
      if (use_fft && !FIX_ATOMIC)
      {
        expanded_gradient.push_back(plus[r] ? cmplxType(dL_by_dReFHpos,dL_by_dImFHpos) : std::conj(cmplxType(dL_by_dReFHneg,dL_by_dImFHneg)));
        expanded_miller.push_back(plus[r] ? miller::index<int>(miller[r]) : miller::index<int>(-miller[r]));
      }
      else if (!use_fft && !FIX_ATOMIC) dL_by_dAtomicPar(r); //results in dL_by_dPar
      if (!FIX_SCALES) dL_by_dScalesPar(r); //results in dL_by_dPar
      if (!FIX_SIGMAA) dL_by_dSigmaaPar(r); //results in dL_by_dPar
    }
    for (unsigned i = 0; i < npars_ref; i++) Gradient[i] += dL_by_dPar[i]; //sum all refls
  }
  if (use_fft && !FIX_ATOMIC)
  {
    calcGradFFT(expanded_miller,expanded_gradient); //results in dL_by_dPar
    for (unsigned i = nsigmaa_ref; i < npars_ref; i++) Gradient[i] = dL_by_dPar[i];
  }
  totLogLike += constrain_restrain_gradient(Gradient);

  return totLogLike;
}

Derivatives RefineSAD::dL_by_dFC()
{
  Derivatives derivatives;
  floatType totLogLike(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    // In dL_by_dReFHpos etc, FH includes refined ScaleK/U but FC does not
    // Remove this scale factor from dL_by_dFC
    floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
    floatType scale = ScaleK*std::exp(-ssqr[r]*debye_ScaleB);
    if (cent[r])
    {
      totLogLike += centricReflGradSAD2(r,true,false);
      derivatives.miller.push_back(miller[r]);
      derivatives.dL_by_dFC.push_back(cmplxType(dL_by_dReFHpos,dL_by_dImFHpos)*scale);
    }
    else if (both[r])
    {
      totLogLike += acentricReflGradSAD2(r,true,false);
      derivatives.miller.push_back(miller[r]);
      derivatives.dL_by_dFC.push_back(cmplxType(dL_by_dReFHpos,dL_by_dImFHpos)*scale);
      derivatives.miller.push_back(-miller[r]);
      derivatives.dL_by_dFC.push_back(std::conj(cmplxType(dL_by_dReFHneg,dL_by_dImFHneg)*scale));
    }
    else if (plus[r])
    {
      totLogLike += singletonReflGradSAD2(r,plus[r],true,false);
      derivatives.miller.push_back(miller[r]);
      derivatives.dL_by_dFC.push_back(cmplxType(dL_by_dReFHpos,dL_by_dImFHpos)*scale);
    }
    else if (!plus[r])
    {
      totLogLike += singletonReflGradSAD2(r,plus[r],true,false);
      derivatives.miller.push_back(-miller[r]);
      derivatives.dL_by_dFC.push_back(std::conj(cmplxType(dL_by_dReFHneg,dL_by_dImFHneg)*scale));
    }
  }
  return derivatives;
}

floatType RefineSAD::constrain_restrain_gradient(TNT::Vector<floatType>& Gradient)
{
  //Add special position constraints to gradient
  //Add sphericity restraint terms to gradient
  //Add fdp restraint terms to gradient
  std::map<int,int> ptncs_pairs_reverse_lookup;
  for (std::map<int,int>::iterator iter = ptncs_pairs.begin(); iter != ptncs_pairs.end(); iter++)
  {
    int a = iter->first; //alias
    int b = iter->second; //alias
    ptncs_pairs_reverse_lookup[b] = a;
  }

  int m(nscales_all),i(nscales_ref);
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    int b(0);
    if (ptncs_pairs.find(a) != ptncs_pairs.end())
      b = ptncs_pairs[a]; //alias
    else if (ptncs_pairs_reverse_lookup.find(a) != ptncs_pairs_reverse_lookup.end())
      b = ptncs_pairs_reverse_lookup[a]; //alias

    if (refinePar[m++]) i+=atoms[a].n_xyz;
    if (refinePar[m++])
    {
      if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
      {
        floatType averageOcc = atoms[a].SCAT.occupancy/2 + atoms[b].SCAT.occupancy/2;
        Gradient[i++] += (atoms[a].SCAT.occupancy-averageOcc)/fn::pow2(PTNCS.EP.LINK_SIGMA);
      }
      else i++;
    }
    if (refinePar[m++])
    {
      floatType iso_grad_add(0);
      dmat6 aniso_grad_add(0,0,0,0,0,0);
      if (SPHERICITY.RESTRAINT)
      {
        if (!atoms[a].SCAT.flags.use_u_aniso_only())
          iso_grad_add += 0; //always zero
        else
        {
          dmat6 sigmaSphericityBeta;
          sigmaSphericityBeta[0] = 0.25*SPHERICITY.SIGMA*aStar()*aStar();
          sigmaSphericityBeta[1] = 0.25*SPHERICITY.SIGMA*bStar()*bStar();
          sigmaSphericityBeta[2] = 0.25*SPHERICITY.SIGMA*cStar()*cStar();
          sigmaSphericityBeta[3] = 0.25*SPHERICITY.SIGMA*aStar()*bStar();
          sigmaSphericityBeta[4] = 0.25*SPHERICITY.SIGMA*aStar()*cStar();
          sigmaSphericityBeta[5] = 0.25*SPHERICITY.SIGMA*bStar()*cStar();
          dmat6 u_star_sphericity = cctbx::adptbx::beta_as_u_star(sigmaSphericityBeta);
          cctbx::adptbx::factor_u_star_u_iso<floatType> factor(getCctbxUC(), atoms[a].SCAT.u_star);
          dmat6 u_iso_coeffs = cctbx::adptbx::u_iso_as_u_star(getCctbxUC(),1.0);
          dmat6 u_star_coeffs;
          for (unsigned n = 0; n < 6; n++)
          {
            dmat6 pick(n==0?1:0,n==1?1:0,n==2?1:0,n==3?1:0,n==4?1:0,n==5?1:0);
            u_star_coeffs[n] = cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),pick); //pick out the coefficients
          }
          dmat6 sphericity_grad(0,0,0,0,0,0);
          for (unsigned ni = 0; ni < 6; ni++)
            for (unsigned n = 0; n < 6; n++)
            {
              floatType du_by_dustarI = u_star_coeffs[ni]*u_iso_coeffs[n];
              floatType dustar_by_dustarI = (n == ni) ? 1. : 0.;
              sphericity_grad[ni] += factor.u_star_minus_u_iso[n]*
                                    (dustar_by_dustarI-du_by_dustarI)/fn::pow2(u_star_sphericity[n]);
            }
          if (atoms[a].n_adp == 6)
            for (int n = 0; n < 6; n++) aniso_grad_add[n] += sphericity_grad[n];
          else
          {
            scitbx::af::small<floatType,6> sphericity_grad_constrained = site_sym_table.get(a).adp_constraints().independent_gradients(sphericity_grad);
            for (int n = 0; n < atoms[a].n_adp; n++) aniso_grad_add[n] += sphericity_grad_constrained[n];
          }
        }
      }//sphericity restraint
      if (WILSON.RESTRAINT)
      {
        floatType sigmaWilsonU = cctbx::adptbx::b_as_u(WILSON.SIGMA);
        floatType WilsonU = cctbx::adptbx::b_as_u(WilsonB);
        if (!atoms[a].SCAT.flags.use_u_aniso_only())
        {
          iso_grad_add += (atoms[a].SCAT.u_iso-WilsonU)/fn::pow2(sigmaWilsonU);
        }
        else
        {
          floatType u_iso = cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),atoms[a].SCAT.u_star);
          dmat6 wilson_grad;
          for (unsigned n = 0; n < 6; n++)
          {
            wilson_grad[n] = (u_iso-WilsonU)/fn::pow2(sigmaWilsonU);
            dmat6 pick(n==0?1:0,n==1?1:0,n==2?1:0,n==3?1:0,n==4?1:0,n==5?1:0);
            wilson_grad[n] *= cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),pick); //pick out the coefficients
          }
          if (atoms[a].n_adp == 6)
            for (int n = 0; n < 6; n++) aniso_grad_add[n] += wilson_grad[n];
          else
          {
            scitbx::af::small<floatType,6> wilson_grad_constrained = site_sym_table.get(a).adp_constraints().independent_gradients(wilson_grad);
            for (int n = 0; n < atoms[a].n_adp; n++) aniso_grad_add[n] += wilson_grad_constrained[n];
          }
        }
      } //wilson restraint
      if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
      {
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          for (int n = 0; n < atoms[a].n_adp; n++)
          {
            floatType average = atoms[a].SCAT.u_star[n]/2 + atoms[b].SCAT.u_star[n]/2;
            aniso_grad_add[n] += (atoms[a].SCAT.u_star[n]-average)/fn::pow2(PTNCS.EP.LINK_SIGMA/10000);
          }
        }
        else
        {
          floatType average = atoms[a].SCAT.u_iso/2 + atoms[b].SCAT.u_iso/2;
          iso_grad_add += (atoms[a].SCAT.u_iso-average)/fn::pow2(PTNCS.EP.LINK_SIGMA);
        }
      }
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) Gradient[i++] += iso_grad_add;
      else for (int n = 0; n < atoms[a].n_adp; n++) Gradient[i++] += aniso_grad_add[n];
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++])
    {
      floatType AtomSigmaFdp = FDP.SIGMA*AtomFdpInit[t];
      Gradient[i++] += FDP.RESTRAINT ? (AtomFdp[t]-AtomFdpInit[t])/fn::pow2(AtomSigmaFdp) : 0;
    }

  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  floatType totLogLike(0);
  if (SPHERICITY.RESTRAINT)
    totLogLike += sphericity_restraint_likelihood();
  if (WILSON.RESTRAINT)
    totLogLike += wilson_restraint_likelihood();
  if (FDP.RESTRAINT)
    totLogLike += fdp_restraint_likelihood();
  if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
    totLogLike += ptncs_link_restraint_likelihood();
  return totLogLike;
}

void RefineSAD::dL_by_dAtomicPar(unsigned r)
{
//#define __FDGRADATOMIC__
#ifdef __FDGRADATOMIC__
static int grad_refl = 0;
static int grad_refl_cent = 0;
static int grad_refl_acent = 0;
static int grad_refl_sing = 0;
  //need to set SADM ALL
  if (grad_refl == 0)
  for (int i = nscales_ref; i < npars_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  if (grad_refl == 0)
    { std::cout << "Selected parameter " << whatAmI(ipar) <<std::endl; }
  PHASER_ASSERT(ipar >= nscales_ref);
  PHASER_ASSERT(ipar < npars_ref);
  floatType *test;
  int itest(nscales_all);
//-- atomic --
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[0]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[1]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.site[2]);
    if (itest++ == ipar) test = &(atoms[a].SCAT.occupancy);
    if (!atoms[a].SCAT.flags.use_u_aniso_only())
    { if (itest++ == ipar) test = &(atoms[a].SCAT.u_iso); }
    else
    { for (int n = 0; n < atoms[a].n_adp; n++) if (itest++ == ipar) test= &(atoms[a].SCAT.u_star[n]); }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
    if (itest++ == ipar) test = &(AtomFdp[t]); //fp

  floatType &dL_by_dtest = dL_by_dPar[ipar];
  calcAtomicData();
  calcScalesData();
  floatType f_start(0);
  if (cent[r])      f_start = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_start = acentricReflIntgSAD2(r).LogLike;
  else              f_start = singletonReflIntgSAD2(r,plus[r]).LogLike;
  (*test) += shift;
  calcAtomicData();
  calcScalesData();
  floatType f_forward;
  if (cent[r])      f_forward = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_forward = acentricReflIntgSAD2(r).LogLike;
  else              f_forward = singletonReflIntgSAD2(r,plus[r]).LogLike;
  (*test) -= shift;
  (*test) -= shift;
  calcAtomicData();
  calcScalesData();
  floatType f_backward;
  if (cent[r])      f_backward = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_backward = acentricReflIntgSAD2(r).LogLike;
  else              f_backward = singletonReflIntgSAD2(r,plus[r]).LogLike;
  floatType FD_test_forward = (f_forward-f_start)/shift;
  floatType FD_test_backward = (f_start-f_backward)/shift;
  (*test) += shift;
  calcAtomicData();
  calcScalesData();
#endif

  //initialize dFH_by_dAtomicPar
  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    PHASER_ASSERT(a < dReFHpos_by_dX.size());
    dReFHpos_by_dX[a] = dImFHpos_by_dX[a] = 0;
    dReFHpos_by_dY[a] = dImFHpos_by_dY[a] = 0;
    dReFHpos_by_dZ[a] = dImFHpos_by_dZ[a] = 0;
    dReFHneg_by_dX[a] = dImFHneg_by_dX[a] = 0;
    dReFHneg_by_dY[a] = dImFHneg_by_dY[a] = 0;
    dReFHneg_by_dZ[a] = dImFHneg_by_dZ[a] = 0;
   //O and B are not incremented, just set with equality
    for (unsigned n = 0; n < 6; n++)
    {
      dReFHpos_by_dAB[a][n] = dImFHpos_by_dAB[a][n] = 0;
      dReFHneg_by_dAB[a][n] = dImFHneg_by_dAB[a][n] = 0;
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    dReFHpos_by_dFdp[t] = dImFHpos_by_dFdp[t] = 0;
    dReFHneg_by_dFdp[t] = dImFHneg_by_dFdp[t] = 0;
  }

  //calculate dFH_by_dAtomicPar
  float1D linearTerm(atoms.size()),debye_waller_u_iso(atoms.size(),0.);
  floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    linearTerm[a] = symFacPCIF*ScaleK*atoms[a].SCAT.occupancy/M;
    if (!atoms[a].SCAT.flags.use_u_aniso_only()) debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
  }
  floatType b_as_u_term(0.25*scitbx::constants::eight_pi_sq);

  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    int t = atomtype2t[atoms[a].SCAT.scattering_type];
    floatType isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
    if (debye_ScaleB) isoB *= std::exp(-ssqr[r]*debye_ScaleB);
    floatType symmOccBfac(linearTerm[a]*isoB);
    floatType scat(fo_plus_fp[r][t]*symmOccBfac);
              symmOccBfac*=CLUSTER[t].debye(ssqr[r]); //clusters atomtype XX, 1 otherwise
    floatType fpp(AtomFdp[t]*symmOccBfac);
    // Add A and B components for all symmetry elements
    floatType ReFHpos(0),ImFHpos(0),ReFHneg(0),ImFHneg(0);
    for (unsigned isym = 0; isym < NSYMP; isym++)
    {
      floatType anisoB(1);
      floatType anisoScat(scat);
      floatType anisoFpp(fpp);
      if (atoms[a].SCAT.flags.use_u_aniso_only()) //anisoB is 1 if AB is zero
      {
        anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],atoms[a].SCAT.u_star);
        anisoScat *= anisoB;
        anisoFpp *= anisoB;
      }
      floatType theta = rotMiller[r][isym][0]*atoms[a].SCAT.site[0] +
                        rotMiller[r][isym][1]*atoms[a].SCAT.site[1] +
                        rotMiller[r][isym][2]*atoms[a].SCAT.site[2] +
                        traMiller[r][isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
      cmplxType sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
      floatType costheta = std::real(sincostheta);
      floatType sintheta = std::imag(sincostheta);
#else
      theta *= scitbx::constants::two_pi;
      floatType costheta = std::cos(theta);
      floatType sintheta = std::sin(theta);
#endif
      floatType anisoScat_costheta(anisoScat*costheta);
      floatType anisoScat_sintheta(anisoScat*sintheta);
      floatType anisoFpp_costheta(anisoFpp*costheta);
      floatType anisoFpp_sintheta(anisoFpp*sintheta);
      floatType symReFHpos(anisoScat_costheta - anisoFpp_sintheta);
      floatType symImFHpos(anisoScat_sintheta + anisoFpp_costheta);
      ReFHpos += symReFHpos;
      ImFHpos += symImFHpos;
      //Store the complex conjugate
      floatType symReFHneg(anisoScat_costheta + anisoFpp_sintheta);
      floatType symImFHneg(anisoScat_sintheta - anisoFpp_costheta);
      ReFHneg += symReFHneg;
      ImFHneg += symImFHneg;

      if (!protocol.FIX_XYZ)
      {
        dvect3 TWOPI_rotMiller;
        for (int i = 0; i < 3; i++)
          TWOPI_rotMiller[i] = rotMiller[r][isym][i]*scitbx::constants::two_pi;

        dReFHpos_by_dX[a] -= TWOPI_rotMiller[0]*symImFHpos;
        dImFHpos_by_dX[a] += TWOPI_rotMiller[0]*symReFHpos;
        dReFHpos_by_dY[a] -= TWOPI_rotMiller[1]*symImFHpos;
        dImFHpos_by_dY[a] += TWOPI_rotMiller[1]*symReFHpos;
        dReFHpos_by_dZ[a] -= TWOPI_rotMiller[2]*symImFHpos;
        dImFHpos_by_dZ[a] += TWOPI_rotMiller[2]*symReFHpos;

        dReFHneg_by_dX[a] -= TWOPI_rotMiller[0]*symImFHneg;
        dImFHneg_by_dX[a] += TWOPI_rotMiller[0]*symReFHneg;
        dReFHneg_by_dY[a] -= TWOPI_rotMiller[1]*symImFHneg;
        dImFHneg_by_dY[a] += TWOPI_rotMiller[1]*symReFHneg;
        dReFHneg_by_dZ[a] -= TWOPI_rotMiller[2]*symImFHneg;
        dImFHneg_by_dZ[a] += TWOPI_rotMiller[2]*symReFHneg;
      }

      if (!(protocol.FIX_FDP || input_fix_fdp[t]))
      {
        dReFHpos_by_dFdp[t] -= sintheta*symmOccBfac*anisoB;
        dImFHpos_by_dFdp[t] += costheta*symmOccBfac*anisoB;
        dReFHneg_by_dFdp[t] += sintheta*symmOccBfac*anisoB;
        dImFHneg_by_dFdp[t] -= costheta*symmOccBfac*anisoB;
      }


      if (atoms[a].SCAT.flags.use_u_aniso_only() && !protocol.FIX_BFAC)
      {
        dmat6 millerSqr = cctbx::adptbx::debye_waller_factor_u_star_gradient_coefficients(rotMiller[r][isym],scitbx::type_holder<floatType>());
        for (unsigned n = 0; n < 6; n++)
        {
          millerSqr[n] *= scitbx::constants::two_pi_sq;
          dReFHpos_by_dAB[a][n] -= millerSqr[n]*symReFHpos;
          dImFHpos_by_dAB[a][n] -= millerSqr[n]*symImFHpos;
          dReFHneg_by_dAB[a][n] -= millerSqr[n]*symReFHneg;
          dImFHneg_by_dAB[a][n] -= millerSqr[n]*symImFHneg;
        }
      }
    }//end sym


    if (!protocol.FIX_OCC && atoms[a].SCAT.occupancy)
    {
      dReFHpos_by_dO[a] = ReFHpos/atoms[a].SCAT.occupancy;
      dImFHpos_by_dO[a] = ImFHpos/atoms[a].SCAT.occupancy;
      dReFHneg_by_dO[a] = ReFHneg/atoms[a].SCAT.occupancy;
      dImFHneg_by_dO[a] = ImFHneg/atoms[a].SCAT.occupancy;
    }

    if (!atoms[a].SCAT.flags.use_u_aniso_only() && !protocol.FIX_BFAC)
    {
      dReFHpos_by_dIB[a] = -ssqr[r]*ReFHpos*b_as_u_term;
      dImFHpos_by_dIB[a] = -ssqr[r]*ImFHpos*b_as_u_term;
      dReFHneg_by_dIB[a] = -ssqr[r]*ReFHneg*b_as_u_term;
      dImFHneg_by_dIB[a] = -ssqr[r]*ImFHneg*b_as_u_term;
    }

  } //end loop over atoms

  //add gradients for joint occupancy
  dReFHpos_by_dO_px = dImFHpos_by_dO_px = dReFHneg_by_dO_px = dImFHneg_by_dO_px = 0;
  int npx(0);
  for (unsigned a = 0 ; a < atoms.size(); a++)
  if (!atoms[a].REJECTED && atoms[a].SCAT.label == "px")
  {
    dReFHpos_by_dO_px += dReFHpos_by_dO[a];
    dImFHpos_by_dO_px += dImFHpos_by_dO[a];
    dReFHneg_by_dO_px += dReFHneg_by_dO[a];
    dImFHneg_by_dO_px += dImFHneg_by_dO[a];
    npx++;
  }
  if (npx)
  {
    dReFHpos_by_dO_px /= npx;
    dImFHpos_by_dO_px /= npx;
    dReFHneg_by_dO_px /= npx;
    dImFHneg_by_dO_px /= npx;
  }

  //initialize Atomic part of dL_by_dPar array
  for (unsigned j = nscales_ref; j < npars_ref; j++) dL_by_dPar[j] = 0;

  //accumulate derivatives using the chain rule
  int m(nscales_all),i(nscales_ref);

  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
      af_float x_grad(3);
      x_grad[0] = dL_by_dReFHpos*dReFHpos_by_dX[a] + dL_by_dImFHpos*dImFHpos_by_dX[a] +
                  dL_by_dReFHneg*dReFHneg_by_dX[a] + dL_by_dImFHneg*dImFHneg_by_dX[a];
      x_grad[1] = dL_by_dReFHpos*dReFHpos_by_dY[a] + dL_by_dImFHpos*dImFHpos_by_dY[a] +
                  dL_by_dReFHneg*dReFHneg_by_dY[a] + dL_by_dImFHneg*dImFHneg_by_dY[a];
      x_grad[2] = dL_by_dReFHpos*dReFHpos_by_dZ[a] + dL_by_dImFHpos*dImFHpos_by_dZ[a] +
                  dL_by_dReFHneg*dReFHneg_by_dZ[a] + dL_by_dImFHneg*dImFHneg_by_dZ[a];
      scitbx::af::small<floatType,3> x_indep = site_sym_table.get(a).site_constraints().independent_gradients(x_grad.const_ref());
      for (int x = 0; x < atoms[a].n_xyz; x++) dL_by_dPar[i++] += x_indep[x];
    }
    if (refinePar[m++])
    {
      if (atoms[a].SCAT.label != "px")
        dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dO[a] + dL_by_dImFHpos*dImFHpos_by_dO[a] +
                           dL_by_dReFHneg*dReFHneg_by_dO[a] + dL_by_dImFHneg*dImFHneg_by_dO[a] ;
      else //linked gradients for joint occupancy
        dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dO_px + dL_by_dImFHpos*dImFHpos_by_dO_px +
                           dL_by_dReFHneg*dReFHneg_by_dO_px + dL_by_dImFHneg*dImFHneg_by_dO_px ;
    }
    if (refinePar[m++])
    {
    if (!atoms[a].SCAT.flags.use_u_aniso_only())
      dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dIB[a] + dL_by_dImFHpos*dImFHpos_by_dIB[a] +
                         dL_by_dReFHneg*dReFHneg_by_dIB[a] + dL_by_dImFHneg*dImFHneg_by_dIB[a] ;
    else
    {
      dmat6 u_star_grad;
      for (unsigned n = 0; n < 6; n++)
        u_star_grad[n] = dL_by_dReFHpos*dReFHpos_by_dAB[a][n] + dL_by_dImFHpos*dImFHpos_by_dAB[a][n] +
                         dL_by_dReFHneg*dReFHneg_by_dAB[a][n] + dL_by_dImFHneg*dImFHneg_by_dAB[a][n] ;
      if (atoms[a].n_adp == 6)
        for (int n = 0; n < 6; n++) dL_by_dPar[i++] += u_star_grad[n];
      else
      {
        scitbx::af::small<floatType,6> u_star_grad_constrained = site_sym_table.get(a).adp_constraints().independent_gradients(u_star_grad);
        for (int n = 0; n < atoms[a].n_adp; n++) dL_by_dPar[i++] += u_star_grad_constrained[n];
      }
    }
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    if (refinePar[m++])
    dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dFdp[t] + dL_by_dImFHpos*dImFHpos_by_dFdp[t] +
                       dL_by_dReFHneg*dReFHneg_by_dFdp[t] + dL_by_dImFHneg*dImFHneg_by_dFdp[t];
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

#ifdef __FDGRADATOMIC__
  if (cent[r] && grad_refl_cent++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (centric r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (both[r] && !cent[r] && grad_refl_acent++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (acentric r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (!both[r] && !cent[r] && grad_refl_sing++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (singleton r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (grad_refl_cent > std::atof(getenv("PHASER_TEST_NREFL")))
  if (grad_refl_acent > std::atof(getenv("PHASER_TEST_NREFL")))
  if (grad_refl_sing > std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
  if (grad_refl++ >= NREFL) std::exit(1);
#endif
}

void RefineSAD::dL_by_dSigmaaPar(unsigned r)
{
//#define __FDGRADSIGMAA__
#ifdef __FDGRADSIGMAA__
static int grad_refl = 0;
  //need to set SADM ALL
  if (grad_refl == 0)
  for (int i = 0; i < nsigmaa_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //hint: set CRYS x DATA x BINS NUM 1
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  if (grad_refl == 0)
    { std::cout << "Selected parameter " << whatAmI(ipar) <<std::endl; }
  PHASER_ASSERT(ipar < nsigmaa_ref);
  floatType *test;
  int itest(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (itest++ == ipar) test = &(DphiA_bin[rbin[r]]);
    if (itest++ == ipar) test = &(DphiB_bin[rbin[r]]);
    if (itest++ == ipar) test = &(SP_bin[rbin[r]]);
    if (itest++ == ipar) test = &(SDsqr_bin[rbin[r]]);
  }
  floatType &dL_by_dtest = dL_by_dPar[ipar];
  calcAtomicData();
  calcScalesData();
  calcSigmaaData();
  floatType f_start = acentricReflIntgSAD2(r).LogLike;
  (*test) += shift;
  calcSigmaaData();
  floatType f_forward = acentricReflIntgSAD2(r).LogLike;
  (*test) -= shift;
  (*test) -= shift;
  calcSigmaaData();
  floatType f_backward = acentricReflIntgSAD2(r).LogLike;
  floatType FD_test_forward = (f_forward-f_start)/shift;
  floatType FD_test_backward = (f_start-f_backward)/shift;
  (*test) += shift;
  calcSigmaaData();
#endif

  //initialize Sigmaa part of dL_by_dPar array
  for (unsigned j = 0; j < nsigmaa_ref; j++) dL_by_dPar[j] = 0;

  int i(0),m(0);
  floatType ZERO(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    bool bin_match(s == rbin[r]);
    if (refinePar[m++]) dL_by_dPar[i++] = bin_match ? dL_by_dSA : ZERO;
    if (refinePar[m++]) dL_by_dPar[i++] = bin_match ? dL_by_dSB : ZERO;
    if (refinePar[m++]) dL_by_dPar[i++] = bin_match ? dL_by_dSP : ZERO;
    if (refinePar[m++]) dL_by_dPar[i++] = bin_match ? dL_by_dSDsqr : ZERO;
  }
  PHASER_ASSERT(m == nsigmaa_all);
  PHASER_ASSERT(i == nsigmaa_ref);

#ifdef __FDGRADSIGMAA__
  std::cout << "=== First Derivative Test (r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward <<
               " ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  if (grad_refl++ >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
#endif
}

void RefineSAD::dL_by_dScalesPar(unsigned r)
{
//#define __FDGRADSCALES__
#ifdef __FDGRADSCALES__
static int grad_refl = 0;
static int grad_refl_cent = 0;
static int grad_refl_acent = 0;
static int grad_refl_sing = 0;
  //need to set SADM ALL
  if (grad_refl == 0)
  for (int i = nsigmaa_ref; i < npars_ref; i++)
    std::cout << whatAmI(i) << std::endl;
  if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_IPAR") == 0)
    { std::cout << "setenv PHASER_TEST_IPAR\n" ; std::exit(1); }
  if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL\n" ; std::exit(1); }
  floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  //set PHASER_TEST_IPAR to the number printed at the start of the refinement list
  int ipar = std::atoi(getenv("PHASER_TEST_IPAR"))-1;
  if (grad_refl == 0)
    { std::cout << "Selected parameter " << whatAmI(ipar) <<std::endl; }
  PHASER_ASSERT(ipar >= nsigmaa_all);
  PHASER_ASSERT(ipar < npars_all);
  floatType *test;
  int itest(nsigmaa_all);
//-- scale --
  if (input_atoms)
  {
    if (itest++ == ipar) test = &(ScaleK);
    if (itest++ == ipar) test = &(ScaleU);
  }
  if (input_partial)
  {
    if (itest++ == ipar) test = &(PartK);
    if (itest++ == ipar) test = &(PartU);
  }

  floatType &dL_by_dtest = dL_by_dPar[ipar];
  calcScalesData();
  floatType f_start(0);
  if (cent[r])      f_start = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_start = acentricReflIntgSAD2(r).LogLike;
  else              f_start = singletonReflIntgSAD2(r,plus[r]).LogLike;
  (*test) += shift;
  calcScalesData();
  floatType f_forward;
  if (cent[r])      f_forward = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_forward = acentricReflIntgSAD2(r).LogLike;
  else              f_forward = singletonReflIntgSAD2(r,plus[r]).LogLike;
  (*test) -= shift;
  (*test) -= shift;
  calcScalesData();
  floatType f_backward;
  if (cent[r])      f_backward = centricReflIntgSAD2(r).LogLike;
  else if (both[r]) f_backward = acentricReflIntgSAD2(r).LogLike;
  else              f_backward = singletonReflIntgSAD2(r,plus[r]).LogLike;
  floatType FD_test_forward = (f_forward-f_start)/shift;
  floatType FD_test_backward = (f_start-f_backward)/shift;
  (*test) += shift;
  calcScalesData();
#endif

  //initialize dFH_by_dScalesPar
  dReFHpos_by_dK = dImFHpos_by_dK = dReFHneg_by_dK = dImFHneg_by_dK = 0;
  dReFHpos_by_dB = dImFHpos_by_dB = dReFHneg_by_dB = dImFHneg_by_dB = 0;
  dReFHpos_by_dPartK = dImFHpos_by_dPartK = dReFHneg_by_dPartK = dImFHneg_by_dPartK = 0;
  dReFHpos_by_dPartU = dImFHpos_by_dPartU = dReFHneg_by_dPartU = dImFHneg_by_dPartU = 0;

  //calculate dFH_by_dScalesPar
  if (!(protocol.FIX_K && protocol.FIX_B) && input_atoms)
  {
    floatType b_as_u_term(0.25*scitbx::constants::eight_pi_sq);
    floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
    floatType K = ScaleK*std::exp(-ssqr[r]*debye_ScaleB);
    cmplxType KFApos = K*FApos[r];
    cmplxType KFAneg = K*FAneg[r];
    if (!protocol.FIX_K && ScaleK)
    {
      dReFHpos_by_dK = std::real(KFApos)/ScaleK;
      dReFHneg_by_dK = std::real(KFAneg)/ScaleK;
      dImFHpos_by_dK = std::imag(KFApos)/ScaleK;
      dImFHneg_by_dK = std::imag(KFAneg)/ScaleK;
    }
    if (!protocol.FIX_B)
    {
      dReFHpos_by_dB = -ssqr[r]*b_as_u_term*std::real(KFApos);
      dReFHneg_by_dB = -ssqr[r]*b_as_u_term*std::real(KFAneg);
      dImFHpos_by_dB = -ssqr[r]*b_as_u_term*std::imag(KFApos);
      dImFHneg_by_dB = -ssqr[r]*b_as_u_term*std::imag(KFAneg);
    }
  }

  //add partial scattering, which is real
  if (!(protocol.FIX_PARTK && protocol.FIX_PARTU) && input_partial)
  {
    floatType b_as_u_term(0.25*scitbx::constants::eight_pi_sq);
    floatType debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
    double K = PartK*std::exp(-ssqr[r]*debye_PartB);
    cmplxType KFPpos = K*FPpos[r];
    cmplxType KFPneg = K*FPneg[r];
    if (!protocol.FIX_PARTK && PartK)
    {
      dReFHpos_by_dPartK = std::real(KFPpos)/PartK;
      dReFHneg_by_dPartK = std::real(KFPneg)/PartK;
      dImFHpos_by_dPartK = std::imag(KFPpos)/PartK;
      dImFHneg_by_dPartK = std::imag(KFPneg)/PartK;
    }
    if (!protocol.FIX_PARTU)
    {
      dReFHpos_by_dPartU = -ssqr[r]*b_as_u_term*std::real(KFPpos);
      dReFHneg_by_dPartU = -ssqr[r]*b_as_u_term*std::real(KFPneg);
      dImFHpos_by_dPartU = -ssqr[r]*b_as_u_term*std::imag(KFPpos);
      dImFHneg_by_dPartU = -ssqr[r]*b_as_u_term*std::imag(KFPneg);
    }
  }

  //initialize Scales part of dL_by_dPar array
  for (unsigned j = nsigmaa_ref; j < nscales_ref; j++) dL_by_dPar[j] = 0;

  //accumulate derivatives using the chain rule
  int m(nsigmaa_all),i(nsigmaa_ref);

  if (input_atoms && refinePar[m++]) //ScaleK
  dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dK + dL_by_dImFHpos*dImFHpos_by_dK +
                     dL_by_dReFHneg*dReFHneg_by_dK + dL_by_dImFHneg*dImFHneg_by_dK;

  if (input_atoms && refinePar[m++]) //ScaleU
  dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dB + dL_by_dImFHpos*dImFHpos_by_dB +
                     dL_by_dReFHneg*dReFHneg_by_dB + dL_by_dImFHneg*dImFHneg_by_dB;

  if (input_partial && refinePar[m++]) //PartK
  dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dPartK + dL_by_dImFHpos*dImFHpos_by_dPartK +
                     dL_by_dReFHneg*dReFHneg_by_dPartK + dL_by_dImFHneg*dImFHneg_by_dPartK;

  if (input_partial && refinePar[m++]) //PartU
  dL_by_dPar[i++] += dL_by_dReFHpos*dReFHpos_by_dPartU + dL_by_dImFHpos*dImFHpos_by_dPartU +
                     dL_by_dReFHneg*dReFHneg_by_dPartU + dL_by_dImFHneg*dImFHneg_by_dPartU;

  PHASER_ASSERT(m == nscales_all);
  PHASER_ASSERT(i == nscales_ref);

#ifdef __FDGRADSCALES__
  if (cent[r] && grad_refl_cent++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (centric r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (both[r] && !cent[r] && grad_refl_acent++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (acentric r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (!both[r] && !cent[r] && grad_refl_sing++ <= std::atof(getenv("PHASER_TEST_NREFL")))
  {
  std::cout << "=== First Derivative Test (singleton r=" << r << ") ===\n";
  std::cout << "Analytic " << dL_by_dtest <<
               " FD forward " << FD_test_forward <<
               " FD backward " << FD_test_backward << std::endl <<
               "Ratio forward "  << dL_by_dtest/FD_test_forward <<
               " backward " << dL_by_dtest/FD_test_backward << "\n";
  }
  if (grad_refl_cent > std::atof(getenv("PHASER_TEST_NREFL")))
  if (grad_refl_acent > std::atof(getenv("PHASER_TEST_NREFL")))
  if (grad_refl_sing > std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
  if (grad_refl++ >= NREFL) std::exit(1);
#endif
}
} //phaser
