//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineBase2Class__
#define __RefineBase2Class__
#include <phaser/main/Phaser.h>
#include <phaser/src/RefineBase.h>
#include <phaser/io/Output.h>
#include <phaser/lib/fmat.h>
#include <phaser/lib/vec.h>

namespace phaser {

class RefineBase2 : public RefineBase
{
  public:
    RefineBase2() {};
    virtual ~RefineBase2() {};

    double finiteDiffGradient(TNT::Vector<double>&,double);
    double finiteGDiffHessian(TNT::Fortran_Matrix<double>&,double);
    double finiteGDiffDiagHessian(TNT::Fortran_Matrix<double>&,double);
    double finiteFDiffHessian(TNT::Fortran_Matrix<double>&,double);
    double finiteFDiffDiagHessian(TNT::Fortran_Matrix<double>&,double);
    void   CalcDampedShift(floatType, TNT::Vector<floatType>&, TNT::Vector<floatType>&,
             TNT::Vector<floatType>&,TNT::Vector<floatType>&,TNT::Vector<floatType>&);
};

} //phaser
#endif
