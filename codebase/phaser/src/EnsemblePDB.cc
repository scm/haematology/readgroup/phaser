//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Bin.h>
#include <phaser/src/TncsVar.h>
#include <phaser/src/Ensemble.h>
#include <phaser/io/Errors.h>
#include <phaser/src/Molecule.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SigmaN.h>
#include <phaser/lib/consistentDLuz.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/rmdirs.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/factor.h>
#include <phaser/lib/between.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/solTerm.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <mmtbx/masks/atom_mask.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/index_span.h>
#include <boost/numeric/conversion/cast.hpp>
#include <phaser/lib/van_der_waals_radii.h>
#include <phaser/lib/scattering.h>
#include <phaser/mr_objects/rms_estimate.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

//--------------------------
// Constructor from pdbfiles
// -------------------------
Ensemble& Ensemble::setPDB(floatType HIRES,data_solpar& SOLPAR,data_formfactors& FORMFACTOR,floatType BOXSCALE,SigmaN DATA_BIN,Output& output,bool DECOMPOSITION,floatType SPHERE,float1D* OCC)
{
  outStream where(DECOMPOSITION?VERBOSE:LOGFILE);
  ENS_BIN = Bin(BIN);
  int PDB_nMols = nMols();
  if (DECOMPOSITION)
  output.logTab(1,where,"Ensemble configured for spherical harmonic decomposition");
  else
  output.logTab(1,LOGFILE,"Ensemble configured for structure factor interpolation");
  output.logTab(1,LOGFILE,"Ensemble configured to resolution " + dtos(HIRES,5,2));
  if (SOLPAR.BULK_USE)
  output.logTab(1,LOGFILE,"Ensemble configured with bulk solvent correction (" + dtos(SOLPAR.BULK_FSOL) + "," + dtos(SOLPAR.BULK_BSOL) + ")");
  if (PDB_nMols == 1 && ENSEMBLE[0].CHAIN.size())
    output.logTab(1,where,"Ensemble configured for chain [" + ENSEMBLE[0].CHAIN + "]");
  else
    for (int ipdb = 0; ipdb < PDB_nMols; ipdb++)
    if (ENSEMBLE[ipdb].CHAIN.size())
      output.logTab(1,where,"Model #" + itos(ipdb) + ": Ensemble configured for chain [" + ENSEMBLE[ipdb].CHAIN + "]");
  if (OCC != NULL)
  { //if the occupancy is defined, only use the trace molecule in the ensembling
    output.logTab(2,where,"Ensemble configured with occupancies");
    PHASER_ASSERT(PDB_nMols == 1);
    PHASER_ASSERT(ENSEMBLE[0].nmodels() == 1);
  }

  output.logTab(1,DEBUG,"Moments of Inertia and Principal Axes :");
  output.logTab(1,DEBUG,"PR: " + dmtos(PR,8,5)); //same as ENSE unparse
  output.logTab(1,DEBUG,"PT: " + dvtos(PT,8,5)); //same as ENSE unparse
  output.logTab(2,DEBUG,"EXTENT: " + dvtos(PRINCIPAL_EXTENT));
  output.logTab(2,DEBUG,"Mean Radius: " + dtos(mean_radius()));

  ENS_FROM_PDB = true;
  PHASER_ASSERT(HIRES > 0);
  NREFL = 0;
  NEWALD = DV.size();
  PHASER_ASSERT(NEWALD);

  setup_scattering(OCC);
  output.logTab(1,DEBUG,"Average scattering = " +dtos(SCATTERING));

  if (is_atom)
  {
    std::string atm(Coords().one_atom_element());
    output.logTab(1,LOGFILE,"Ensemble has one atom (element: " + atm + ")");
    setPtInterpEV(0);
    int nrefl = ENS_BIN.maxbins()*ENS_BIN.width(); //will give MAXBINS bins
    ENS_BIN.setup(HIRES,DEF_LORES,nrefl); //same as old code in MapHKL.cc, not optimal
    NEWALD = 1;
    resizeMapsAtom();
    output.logUnderLine(DEBUG,"Scattering ratio to average protein scatterer by resolution bin:");
    scattering_protein protein;
    //scattering_nucleic nucleic; //check for which!
    cctbx::eltbx::xray_scattering::gaussian fetchX = FORMFACTOR.fetch(atm);
    double atm_atomic_number = cctbx::eltbx::tiny_pse::table(atm).atomic_number();
    for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    {
      double Ssqr = 1.0/fn::pow2(ENS_BIN.MidRes(ibin));
      double scatX = fn::pow2(fetchX.at_d_star_sq(Ssqr)/atm_atomic_number);
      double fp = //is_all_nucleic ? scatX/nucleic.average_scattering_at_ssqr(Ssqr,FORMFACTOR):
                  scatX/protein.average_scattering_at_ssqr(Ssqr);
      double DLuz = solTerm(Ssqr,SOLPAR)*DLuzzati(Ssqr,DV[0].fixed);
      double Covariance =  fn::pow2(DLuz);
      double WeightedE = DLuz;
      setV(ibin,Covariance);
      setE(ibin,WeightedE);
      setScatRat(ibin,fp);
      ibin ?
        output.logTabPrintf(1,DEBUG,"Bin #%4i (%5.2f - %5.2f A) : atom/protein = %6.3f\n", ibin+1,ENS_BIN.LoRes(ibin),ENS_BIN.HiRes(ibin),fp):
        output.logTabPrintf(1,DEBUG,"Bin #%4i (  inf - %5.2f A) : atom/protein = %6.3f\n", ibin+1,ENS_BIN.HiRes(ibin),fp);
    }
    output.logBlank(DEBUG);
    relative_wilsonB.resize(1);
    relative_wilsonB[0].resize(1,0);
    return *this; // don't bother with the rest
  }
  //-------------------------------------------------------------------------------------
  //calculate the box size for the FT
  dvect3 box;
  dvect3 BOX = PRINCIPAL_EXTENT;
  if (DECOMPOSITION)
  {
    output.logTab(1,VERBOSE,"Integration Sphere");
    floatType sphereOuter = SPHERE ? SPHERE : 2.*mean_radius();
    PHASER_ASSERT(sphereOuter > 0);
    output.logTab(2,VERBOSE,"The integration sphere is: " + dtos(sphereOuter));
    output.logTab(2,VERBOSE,"The unit cell will be a box equal to the extent of the model + integration sphere + resolution");
    output.logTab(2,VERBOSE,"The resolution limit is: " + dtos(HIRES));
    //no scalar multiplication defined
    box[0] = BOX[0]+sphereOuter+HIRES;
    box[1] = BOX[1]+sphereOuter+HIRES;
    box[2] = BOX[2]+sphereOuter+HIRES;
    for (int i = 0; i < 3; i++)
      if (box[i] < 8*HIRES)
      {
        output.logTab(1,VERBOSE,"Increase cell edge to minimum of 8*dmin");
        box[i] = 8*HIRES;
      }
  }
  else
  {
    output.logTab(1,VERBOSE,"The unit cell for interpolation will be in a box equal to BOXSCALE times the maximum extent of all the model");
    output.logTab(2,VERBOSE,"The extent of the model is: " + dvtos(BOX));
    PHASER_ASSERT(BOXSCALE > 0);
    output.logTab(2,VERBOSE,"The boxscale is: " + dtos(BOXSCALE));
  //no scalar multiplication defined for dvect3
    box[0] = BOXSCALE*BOX[0];
    box[1] = BOXSCALE*BOX[1];
    box[2] = BOXSCALE*BOX[2];
    for (int i = 0; i < 3; i++)
      if (box[i] < 8*HIRES)
      {
        output.logTab(1,VERBOSE,"Increase cell edge to minimum of 8*dmin");
        box[i] = 8*HIRES;
      }
  }
  input_CELL.setUnitCell(box);
  input_CELL.logUnitCell(VERBOSE,output,false);

  if (!DECOMPOSITION)
  {
    output.logTab(1,VERBOSE, "Resolution of Electron Density");
    miller::index<int> UNIT(1,1,1);
    dvect3 min_box = floatType(DEF_CELL_SCALE_MIN)*box;
    if (getenv("PHASER_STUDY_PARAMS_CELL") != NULL) min_box = 0.3*min_box; //for CELL REFINEMENT
    double tol = 0.01; //extra tolerance for large cell scale refinement changes
    floatType savereso(1/(1/(HIRES-tol) + UnitCell(min_box).S(UNIT)));
    output.logTab(2,VERBOSE,"The resolution will be extended so that interpolation can occur at the limit");
    output.logTab(2,VERBOSE,"The resolution limit for saved Ewald maps is " + dtos(savereso));
    HIRES = savereso;
  }

  //-------------------------------------------------------------------------------------
  output.logTab(2,VERBOSE,"Coordinates will be centred on the origin and rotated to align principal axes with XYZ");
  output.logTab(3,VERBOSE,"X - longest dimension");
  output.logTab(3,VERBOSE,"Z - shortest dimension");
  output.logTabPrintf(2,VERBOSE,"Rotation: [% 5.3f % 5.3f % 5.3f]\n",PR(0,0),PR(0,1),PR(0,2));
  output.logTabPrintf(2,VERBOSE,"          [% 5.3f % 5.3f % 5.3f]\n",PR(1,0),PR(1,1),PR(1,2));
  output.logTabPrintf(2,VERBOSE,"          [% 5.3f % 5.3f % 5.3f]\n",PR(2,0),PR(2,1),PR(2,2));
  dvect3 euler(matrix2eulerDEG(PR));
  output.logTabPrintf(2,VERBOSE,"Rotation (Euler): (% 5.3f % 5.3f % 5.3f)\n",euler[0],euler[1],euler[2]);
  output.logTabPrintf(2,VERBOSE,"Translation: (% 6.3f % 6.3f % 6.3f)\n",PT[0],PT[1],PT[2]);

  //-------------------------------------------------------------------------------------
  //extra check on the coordinates to see that they are relatively sensible
  double percent(20);
  double maxdeviation((percent/100.00)*SCATTERING);
  output.logTab(1,TESTING,"Scattering " + dtos(SCATTERING));
  output.logTab(1,TESTING,"Maximum deviation of scattering " + dtos(maxdeviation));
  for (int ipdb = 0; ipdb < PDB_nMols; ipdb++)
  {
    Molecule pdb_mol = Coords(ipdb);
    for (unsigned m = 0; m < ENSEMBLE[ipdb].nmodels(); m++)
    {
      double thisScat = (OCC == NULL) ? pdb_mol.getScat(m) : pdb_mol.getScat(OCC);
      double deviation(std::fabs(thisScat - SCATTERING));
      bool scattering_deviates(deviation > maxdeviation);
      output.logTab(1,TESTING,"Scattering this molecule " + dtos(thisScat));
      output.logTab(1,TESTING,"Scattering deviation " + dtos(thisScat-SCATTERING));
      std::string msg(" deviates more than " + dtos(percent) + "% from the mean. This pdb file may contain domains or large loops not present in the other files. ");
      if (!DISABLE_CHECK)
      {
        if (scattering_deviates && pdb_mol.nMols() == 1)
          throw PhaserError(INPUT,"Molecular scattering of " + ENSEMBLE[ipdb].basename() + msg);
        else if (scattering_deviates  && pdb_mol.nMols() > 1)
          throw PhaserError(INPUT,"Molecular scattering of " + ENSEMBLE[ipdb].basename() + " MODEL number " + itos(m+1) + msg);
      }
    }
  }

  //-------------------------------------------------------------------------------------
  floatType SHARAT(1.5);
  output.logTab(1,VERBOSE,"FFT map shannon sampling: " + dtos(SHARAT));
  dvect3 max_box = floatType(DEF_CELL_SCALE_MAX)*box;
  if (getenv("PHASER_STUDY_PARAMS_CELL") != NULL) max_box = 4.0*max_box; //for CELL REFINEMENT
  ivect3 limits = UnitCell(max_box).getMapLimits(HIRES,SHARAT);
  int max_prime_fac = 5;
  ivect3 symm_fac(2,2,2); //symmetry factors for P1
  bool next_highest = true;
  limits[0] = factor(limits[0],max_prime_fac,symm_fac[0],next_highest);
  limits[1] = factor(limits[1],max_prime_fac,symm_fac[1],next_highest);
  limits[2] = factor(limits[2],max_prime_fac,symm_fac[2],next_highest);
  output.logTab(1,VERBOSE,"Map limits satisfying spacegroup and fft grid constraints: "+ivtos(limits));
  cctbx::uctbx::unit_cell cctbxUC = input_CELL.getCctbxUC();
  cctbx::sgtbx::space_group_symbols Symbols("P1","A1983");
  std::string hall_name = Symbols.hall();
  cctbx::sgtbx::space_group cctbxSG(hall_name);
  cctbx::sgtbx::space_group_type cctbxSG_type(cctbxSG);
  floatType grid_resolution_factor =1/(2.0*SHARAT);
  floatType quality_factor = 100;
  floatType u_base = cctbx::xray::calc_u_base(HIRES, grid_resolution_factor, quality_factor);
  bool discard_indices_affected_by_aliasing = true;
  bool complex_conj = true;


  //-------------------------------------------------------------------------------------
  int nthreads = 1;

#pragma omp parallel
  {
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
#endif
  }


  output.logBlank(where);
  if (nthreads > 1 && NEWALD > 1+nthreads)
    output.logTab(1,where,"Spreading calculation onto "+itos(nthreads)+ " threads.");
  output.logProgressBarStart(where,"Electron Density Calculation",(6+4*(NEWALD-1)/nthreads));
  af_float ss;
  af::shared<dvect3>    fracxyz;
  af::shared<floatType> atoms_radii;
  vdw atoms_vdw_table;
  std::vector<af_cmplx> Fcalc(NEWALD);
  bool bfactors_zero(true);
  {//first sf calc memory
    // --- collect the scatterers
    af::shared<xray::scatterer<> > xray_scatterers;
    Molecule pdb_mol = (OCC != NULL) ? Coords() : Coords(0);
    int mzero = (OCC != NULL) ? pdb_mol.mtrace : 0; //not mtrace, the default
    std::ofstream principalpdb;
    if (output.level(TESTING))
    {
      std::string boxstr("debug.align_principal." + ENSEMBLE[mzero].rootname() + ".pdb");
      principalpdb.open(boxstr.c_str());
    }
    for (unsigned a = 0; a < pdb_mol.nAtoms(mzero); a++) //m = 0, not the lowest rms (default)
    if (!pdb_mol.isUnknown(mzero,a))
    {
      dvect3 orthXyz = PR*pdb_mol.card(mzero,a).X+PT;
      if (output.level(TESTING))
      {
        pdb_record card = pdb_mol.card(mzero,a);
        card.X = orthXyz;
        principalpdb << card.Card();
      }
      std::string atom_type = FORMFACTOR.label(pdb_mol.card(mzero,a).Element); // scattering_type
      double occupancy = (OCC != NULL) ? (*OCC)[a] : pdb_mol.card(mzero,a).O;
      bfactors_zero  = bfactors_zero && (pdb_mol.card(mzero,a).B == 0);
      cctbx::xray::scatterer<> scatterer(
        "another atom", // label
        input_CELL.doOrth2Frac(orthXyz), // site
        cctbx::adptbx::b_as_u(pdb_mol.card(mzero,a).B), // u_iso
        occupancy, // occupancy
        atom_type,
        0, // fp
        0); // fdp
      scatterer.apply_symmetry(cctbxUC, cctbxSG);
      xray_scatterers.push_back(scatterer);
      fracxyz.push_back(input_CELL.doOrth2Frac(orthXyz));
      atoms_radii.push_back(atoms_vdw_table.lookup(atom_type));
    }
    if (output.level(TESTING))
      principalpdb.close();
    output.logEllipsisEnd(TESTING);
    // --- make a grid of the density from the scatterers
    cctbx::xray::sampled_model_density<> sampled_density;
    cctbx::af::c_interval_grid<3>::index_type rfft_nreal;
    { //memory sampled_density
    scitbx::fftpack::real_to_complex_3d<floatType> rfft(limits);
    rfft_nreal = rfft.n_real();
    { //memory scattering
    cctbx::xray::scattering_type_registry scattering_type_registry;
    scattering_type_registry.process(xray_scatterers.const_ref());
    scattering_type_registry.assign_from_table(FORMFACTOR.TABLE());
    sampled_density = cctbx::xray::sampled_model_density<>(
      cctbxUC,
      xray_scatterers.const_ref(),
      scattering_type_registry,
      rfft.n_real(),
      rfft.m_real(),
      u_base);
    }
    output.logProgressBarNext(where);

    // --- check that the density is not anomalous
    PHASER_ASSERT(!sampled_density.anomalous_flag());
    // --- check that the density is in P1
    PHASER_ASSERT(cctbxSG.order_z() == 1);
    // --- fiddle to get correct form to pass to the fft calculation
    cctbx::xray::sampled_model_density<>::real_map_type real_map = sampled_density.real_map();
    af::ref<floatType, af::c_grid<3> > real_map_fft_ref( &*real_map.begin(), af::c_grid<3>(rfft.m_real()));
    // --- do the fft
    rfft.forward(real_map_fft_ref);
    // --- extract the complex structure factors
    af::const_ref<std::complex<floatType>, af::c_grid_padded<3> > complex_map(
        reinterpret_cast<std::complex<floatType>*>(&*real_map.begin()),
        af::c_grid_padded<3>(rfft.n_complex()));
    output.logProgressBarNext(where);

    //first pdb, calculate millers and structure factors to required resolution limit
    { //memory from map
    cctbx::maptbx::structure_factors::from_map<floatType> from_map(
       cctbxUC,
       cctbxSG_type,
       sampled_density.anomalous_flag(),
       HIRES,
       complex_map,
       complex_conj,
       discard_indices_affected_by_aliasing
      );
    output.logProgressBarNext(where);

    //-------------------------------------------------------------------------------------
    miller = from_map.miller_indices();
    output.logProgressBarNext(where);
    Fcalc[0] = from_map.data();
    output.logProgressBarNext(where);
    } //memory from map

    sampled_density.eliminate_u_extra_and_normalize(miller.const_ref(),Fcalc[0].ref());
    } //memory sampled_density

    //add bulk solvent correction
    //bulk solvent addition after eliminate_u_extra_and_normalize.
    //Much higher correlation with fmodel FC's in test case
    if (SOLPAR.BULK_USE)
    {
      mmtbx::masks::atom_mask atommask(
        cctbxUC,
        cctbxSG,
        rfft_nreal,
        1,1);
      atommask.compute(fracxyz,atoms_radii);
      af_cmplx Fmask = atommask.structure_factors(miller.const_ref());
      for (int r = 0; r < miller.size(); r++)
      {
        double ss = cctbxUC.d_star_sq(miller[r])/4.;
        double k_mask = SOLPAR.BULK_FSOL*std::exp(-SOLPAR.BULK_BSOL*ss);
        Fcalc[0][r] += k_mask*Fmask[r];
      }
    }
    PHASER_ASSERT(miller.size());
    output.logProgressBarNext(where);
  }//memory

#pragma omp parallel
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
    //if (nthread != 0) output.usePhenixCallback(false);
#endif

#pragma omp for
    for (long iewald = 1; iewald < NEWALD; iewald++)
    {
      int ipdb = DV.reverse_lookup[iewald].first;
      int m = DV.reverse_lookup[iewald].second;
      Molecule pdb_mol = Coords(ipdb);
      // --- collect the scatterers
      af::shared<xray::scatterer<> > xray_scatterers;
      for (unsigned a = 0; a < pdb_mol.nAtoms(m); a++)
      if (!pdb_mol.isUnknown(m,a))
      {
        dvect3 orthXyz = PR*pdb_mol.card(m,a).X+PT;
        double occupancy = (OCC != NULL) ? double((*OCC)[a]?1:0) : pdb_mol.card(m,a).O;
        bfactors_zero  = bfactors_zero && (pdb_mol.card(m,a).B == 0);
        cctbx::xray::scatterer<> scatterer(
          "another atom", // label
          input_CELL.doOrth2Frac(orthXyz), // site
          cctbx::adptbx::b_as_u(pdb_mol.card(m,a).B), // u_iso
          occupancy, // occupancy
          FORMFACTOR.label(pdb_mol.card(m,a).Element), // scattering_type
          0, // fp
          0); // fdp
        scatterer.apply_symmetry(cctbxUC, cctbxSG);
        xray_scatterers.push_back(scatterer);
      }
      // --- make a grid of the density from the scatterers
      cctbx::xray::sampled_model_density<> sampled_density;
      cctbx::af::c_interval_grid<3>::index_type rfft_nreal;
      { //memory sampled_density
      scitbx::fftpack::real_to_complex_3d<floatType> rfft(limits);
      rfft_nreal = rfft.n_real();
      {//memory scattering
      cctbx::xray::scattering_type_registry scattering_type_registry;
      scattering_type_registry.process(xray_scatterers.const_ref());
      scattering_type_registry.assign_from_table(FORMFACTOR.TABLE());
      sampled_density = cctbx::xray::sampled_model_density<>(
        cctbxUC,
        xray_scatterers.const_ref(),
        scattering_type_registry,
        rfft.n_real(),
        rfft.m_real(),
        u_base);
      } //memory scattering
      if (!nthread) output.logProgressBarNext(where);

      // --- check that the density is not anomalous
      PHASER_ASSERT(!sampled_density.anomalous_flag());
      // --- check that the density is in P1
      PHASER_ASSERT(cctbxSG.order_z() == 1);
      // --- fiddle to get correct form to pass to the fft calculation
      cctbx::xray::sampled_model_density<>::real_map_type real_map = sampled_density.real_map();
      af::ref<floatType, af::c_grid<3> > real_map_fft_ref( &*real_map.begin(), af::c_grid<3>(rfft.m_real()));
      // --- do the fft
      rfft.forward(real_map_fft_ref);
      // --- extract the complex structure factors
      af::const_ref<std::complex<floatType>, af::c_grid_padded<3> > complex_map(
          reinterpret_cast<std::complex<floatType>*>(&*real_map.begin()),
          af::c_grid_padded<3>(rfft.n_complex()));
      if (!nthread) output.logProgressBarNext(where);

      {
      cctbx::maptbx::structure_factors::from_map<floatType> from_map(
            sampled_density.anomalous_flag(),
            miller.const_ref(),
            complex_map,
            complex_conj,
            discard_indices_affected_by_aliasing
          );
      if (!nthread) output.logProgressBarNext(where);
      Fcalc[iewald] = from_map.data();
      } //from map
      sampled_density.eliminate_u_extra_and_normalize(miller.const_ref(),Fcalc[iewald].ref());
      } //sampled density

      // --- add this list of structure factors
      //add bulk solvent correction
      if (SOLPAR.BULK_USE)
      {
        mmtbx::masks::atom_mask atommask(
          cctbxUC,
          cctbxSG,
          rfft_nreal,
          1,1);
        atommask.compute(fracxyz,atoms_radii);
        af_cmplx Fmask = atommask.structure_factors(miller.const_ref());
        for (int r = 0; r < miller.size(); r++)
        {
          double ss = cctbxUC.d_star_sq(miller[r])/4.;
          double k_mask = SOLPAR.BULK_FSOL*std::exp(-SOLPAR.BULK_BSOL*ss);
          Fcalc[iewald][r] += k_mask*Fmask[r];
        }
      }
      if (!nthread) output.logProgressBarNext(where);
    }
  }
  output.logProgressBarEnd(where);

  if (bfactors_zero)
    output.logWarning(LOGFILE,"All Bfactors of all models are zero");

  //-------------------------------------------------------------------------------------
  output.logTab(1,DEBUG,"Setup Bins");
  floatType hires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller.const_ref())[1]);
  floatType lores = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(miller.const_ref())[0]);
  output.logTab(2,DEBUG,"resolution limits " + dtos(hires) + " - " + dtos(lores));
  ENS_BIN.setup(hires,lores,miller.size()); //new code using real limits of data
 // ENS_BIN.resetReso(HIRES,10000); //same as old code in MapHKL.cc, not optimal
  output.logTab(2,DEBUG,"Number of bins " + itos(ENS_BIN.numbins()));

  //Checks to see if any of the bins have no reflections
  //and makes sure there is at least one in every bin
  unsigned1D numInBin(ENS_BIN.numbins(),0);
  recount:
  for (int s = 0; s < ENS_BIN.numbins(); s++) numInBin[s] = 0;
  for (int r = 0; r < miller.size(); r++) numInBin[ENS_BIN.get_bin(input_CELL.S(miller[r]))]++;
  if (ENS_BIN.numbins() != 1) //if == 1 then do nothing - assume 1 reflection!
    for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
      if (numInBin[ibin] == 0)
      {
        //reduce the number of bins by 1 and see if this helps
        int oneLessBin = ENS_BIN.numbins()-1;
        ENS_BIN.set_numbins(oneLessBin);
        output.logTab(2,DEBUG,"Number of bins reduced by 1 to " + itos(ENS_BIN.numbins()));
        goto recount;
      }

  //Store the binning
  int1D  rbin(miller.size());
  for (int r = 0; r < miller.size(); r++)
  {
    rbin[r] = ENS_BIN.get_bin(input_CELL.S(miller[r]));
    PHASER_ASSERT(rbin[r] < ENS_BIN.numbins());
  }
  numInBin = unsigned1D(ENS_BIN.numbins(),0);
  for (int r = 0; r < miller.size(); r++)
    numInBin[ENS_BIN.get_bin(input_CELL.S(miller[r]))]++;
  for (int ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  output.logTab(2,TESTING,"Bin #" + itos(ibin+1) + " has " + itos(numInBin[ibin]) + " refls");

  //resize the maps
  miller::index_span index_span(miller.const_ref());
  int hmax = index_span.abs_range()[0];
  int kmax = index_span.abs_range()[1];
  int lmax = index_span.abs_range()[2];
  sizeh =  2*hmax+1;
  sizek =  2*kmax+1;
  sizel =  lmax+1; //no anomalous

  //side trip to calculate the Relative Wilson B
  if (SCATTERING) //OCC could be set all to zero for one ensemble (KNOWN) in a set (e.g. a helix)
  set_relative_wilsonB(DATA_BIN,Fcalc,output);

  //The Fs to Es calculation uses the stored binning
  //This is to avoid numerical instability
  int pdb_bin_numbins = boost::numeric_cast<int>(ENS_BIN.numbins());
  if (SCATTERING) //OCC could be set all to zero for one ensemble (KNOWN) in a set (e.g. a helix)
  for (unsigned iewald = 0; iewald < NEWALD; iewald++)
  {
    float1D SumFsqr(pdb_bin_numbins,0);
    for (int r = 0; r < miller.size(); r++)
      SumFsqr[rbin[r]] += std::norm(Fcalc[iewald][r]);

    float1D sqrtSigmaP(pdb_bin_numbins,0);
    for (int ibin = 0; ibin < pdb_bin_numbins; ibin++)
    if (numInBin[ibin])
    {
      //SigmaP is the weighted mean value of F**2/epsilon
      //Here epsilon is 1.0 (P1)
      floatType SigmaP = SumFsqr[ibin]/numInBin[ibin];
      PHASER_ASSERT(SigmaP > 0);
      sqrtSigmaP[ibin] = std::sqrt(SigmaP);
    }

    //EC = FC/sqrt(epsilon*SigmaP)
    for (int r = 0; r < miller.size(); r++)
      Fcalc[iewald][r] /= sqrtSigmaP[rbin[r]];
  }

  setPtInterpEV(4);

  PHASER_ASSERT(Fcalc.size());
  PHASER_ASSERT(miller.size());
  //-------------------------------------------------------------------------------------
  bool debug(output.level(DEBUG));
  output.logBlank(TESTING);
  output.logTab(2,TESTING,"Calculation of Statistical Weighting");
  output.logTab(2,TESTING,"The weights are calculated using the relationship:");
  output.logTab(3,TESTING,"[           ]   [      ] = [       ]");
  output.logTab(3,TESTING,"[Correlation] * [Weight] = [Luzzati]");
  output.logTab(3,TESTING,"[  Matrix   ]   [Vector] = [   D   ]");
  output.logTab(3,TESTING,"[           ]   [      ] = [       ]");
  output.logTab(2,TESTING,"The calculation is done in resolution bins of data");
  output.logBlank(TESTING);
  output.logTab(2,TESTING,"[Correlation Matrix] is a matrix of correlations between");
  output.logTab(3,TESTING,"structure factors of the models in the ensemble");
  output.logBlank(TESTING);
  output.logTab(2,TESTING,"[Luzzati D] is calculated from a four-parameter Sigma(A) curve:");
  output.logTab(3,TESTING,"Sigma(A) = sqrt( fp*(1-fs*exp(-Bs/(4*d*d))) ) ");
  output.logTab(3,TESTING,"             * exp(-(8 Pi^2/3) * RMS*RMS /(4*d*d))");
  output.logTab(2,TESTING,"where");
  output.logTab(3,TESTING,"fp = fraction of ordered protein in model");
  output.logTab(3,TESTING,"fs = fractional effect of solvent (0.95)");
  output.logTab(3,TESTING,"Bs = B-factor to smear disordered solvent (300)");
  output.logTab(3,TESTING,"RMS = estimated RMS of atoms in model to true structure");
  output.logTab(2,TESTING,"The estimated RMS is derived from the sequence identity using");
  output.logTab(2,TESTING,"the relationship given by Chothia & Lesk (EMBO J. 5, 823-826,1986)");
  output.logTab(3,TESTING,"RMS = 0.40*exp(1.87*(1.0-SeqId))");
  output.logBlank(TESTING);
  output.logTab(2,TESTING,"The ensemble of (i) structure factors consists of");
  output.logTab(2,TESTING,"Weighted E      = Sum(i) [ Weight(i)*E(i) ]");
  output.logTab(2,TESTING,"Variance for E  = (1 - dotprod([ Weight ],[ DLuz ]");
  output.logBlank(TESTING);

  float1D Covariance(ENS_BIN.numbins());
  std::vector< TNT::Vector<floatType> > ModelWt(ENS_BIN.numbins());
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
    ModelWt[ibin].newsize(NEWALD);

  //precalculate change in input_VRMS not in parallel section
  bool not_aligned(false),flag_input_VRMS_changed(false);
  if (SCATTERING) //OCC could be set all to zero for one ensemble (KNOWN) in a set (e.g. a helix)
  {
    double SigmaA_lower_limit(0.1);
    double correlation_resolution_limit(10.0); //for Pietro's packing test case, 15 is too low
    int nai(0),naj(0);
    for (int ibin = 0; ibin < pdb_bin_numbins; ibin++)
    {
      TNT::Fortran_Matrix<floatType>  CorMat(NEWALD,NEWALD);
      for (int i = 1; i <= NEWALD; i++)
      for (int j = i; j <= NEWALD; j++)
      {
        // Compute SigmaA from phased E-values.  It is assumed that the
        // E-values are normalised so that the mean-square value is 1.
        int ci(i-1),cj(j-1); //switch from Fortran to C-style indexing
        floatType SumE2(0);
        for (int r = 0; r < miller.size(); r++)
        if (rbin[r] == ibin)
          SumE2 += std::real((Fcalc[ci][r])*std::conj(Fcalc[cj][r]));
        floatType SigmaA = SumE2/numInBin[ibin];
        CorMat(i,j) = CorMat(j,i) = SigmaA;
        if (ENS_BIN.HiRes(ibin) > correlation_resolution_limit && SigmaA < SigmaA_lower_limit)
        {
          not_aligned = true;
          nai = i;
          naj = j;
        }
      }
    }

    if (not_aligned)
    throw PhaserError(INPUT,"","Structure Factors of Models " + itos(nai) + " and " + itos(naj) +
    " have correlations worse than " + dtos(SigmaA_lower_limit) + " to " +  dtos(correlation_resolution_limit) + " Angstroms. Check that the models in this ensemble are *aligned* and are *homologous*");
    if (flag_input_VRMS_changed)
    {
      output.logAdvisory(SUMMARY,"RMS (ID) values changed from input. Low correlation between input RMS (ID) values was incompatible with high correlation between calculated structure factors of corresponding models in ensemble. RMS set to lower of the two input values.");
    }
  }


  if (nthreads > 1) output.logTab(1,TESTING,"Spreading calculation onto "+itos(nthreads)+ " threads.");
  output.logProgressBarStart(TESTING,"Weighting Structure Factors",ENS_BIN.numbins()/nthreads);

  float1D thread_lower_DRMS(nthreads,DV.DRMS.lower);

  output.logUnderLine(TESTING,"Correlation matrices and LuzzatiD by resolution bin:");

#pragma omp parallel
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
    //if (nthread != 0) output.usePhenixCallback(false);
#endif
    bool debug_thread(debug  && nthread == 0);

    int pdb_bin_numbins = boost::numeric_cast<int>(ENS_BIN.numbins());
#pragma omp for
    for (int ibin = 0; ibin < pdb_bin_numbins; ibin++)
    {
      TNT::Fortran_Matrix<floatType>  CorMat(NEWALD,NEWALD);
      for (int i = 1; i <= NEWALD; i++)
        for (int j = i; j <= NEWALD; j++)
        {
       // Compute SigmaA from phased E-values.  It is assumed that the
       // E-values are normalised so that the mean-square value is 1.
          int ci(i-1),cj(j-1); //switch from Fortran to C-style indexing
          floatType SumE2(0);
          for (int r = 0; r < miller.size(); r++)
          if (rbin[r] == ibin)
            SumE2 += std::real((Fcalc[ci][r])*std::conj(Fcalc[cj][r]));
          floatType SigmaA = SumE2/numInBin[ibin];
          CorMat(i,j) = CorMat(j,i) = SigmaA;
        }

      if (debug_thread) output.logEllipsisStart(TESTING,"Bin #" + itos(ibin+1) + ": " + "Calculate weights");
      /*
        Four-parameter Sigma(A) curve given by
        Sigma(A) = sqrt( fp*(1-fs*exp(-Bs*stlsq)) ) *
           exp(-8*Pi*Pi/3 * Sigma(rmsx)^2 * stlsq)
        where fp = fraction of ordered protein in model
              fs = fraction of cell occupied by solvent
              Bs = B-factor to smear disordered solvent (typically 50-200)
              rmsx = e.s.d. of atoms in model
        stlsq = Ssqr/4 (because Ssqr=1/d**2)
      */
      floatType Ssqr = 1/fn::pow2(ENS_BIN.MidRes(ibin));
      TNT::Vector<floatType> DLuz(NEWALD),DStart(NEWALD);
      for (unsigned iewald = 0; iewald < NEWALD; iewald++)
        DStart[iewald] = DLuz[iewald] = solTerm(Ssqr,SOLPAR) * DLuzzati(Ssqr,DV[iewald].fixed);

      if (NEWALD > 1)
      {
        TNT::Fortran_Matrix<floatType>  pseudoCorMatInv(NEWALD,NEWALD); // returned by consistentDLuz
        DLuz = consistentDLuz(DStart,CorMat,pseudoCorMatInv);
        ModelWt[ibin] = pseudoCorMatInv*DLuz;
      }
      else
        ModelWt[ibin] = DLuz[0];

      if (debug_thread)
      {
        output.logEllipsisStart(TESTING,"Bin #" + itos(ibin+1) + ": " + "Model Weights and Luzzati D by resolution bin:");
        output.logTab(1,TESTING,"Bin #" + itos(ibin+1) + ": " + dtos(ENS_BIN.LoRes(ibin)) + " - " + dtos(ENS_BIN.HiRes(ibin)) + "A (weighted average " +  dtos(ENS_BIN.MidRes(ibin)) + "A) #refl = " +itos(numInBin[ibin]));
        for (int i=1; i<=CorMat.num_rows(); i++)
        {
          std::string cormat;
          for (int j=1; j<=CorMat.num_cols(); j++)
            cormat += dtos(CorMat(i,j),5,3) + " ";
          cormat = cormat.substr(0, cormat.size()-1);
          output.logTabPrintf(1,TESTING,"[%s]  [% 5.3f]   [%5.3f]\n",cormat.c_str(),ModelWt[ibin](i),DLuz(i));
        }
        output.logBlank(TESTING);
      }

    //-------------------------------------------------------------------------------------
      if (debug_thread) output.logEllipsisStart(TESTING,"Bin #" + itos(ibin+1) + ": " + "Keep variances positive by setting RMS delta range (DRMS)");
      Covariance[ibin] = TNT::dot_prod(ModelWt[ibin],DLuz); //even if variance is negative!
      double minvar = 0.002; // Corresponds to nearly perfect model with sigmaA=0.999
      double expTerm = -2*two_pi_sq_on_three*Ssqr;
      if (1-std::exp(expTerm*DV.DRMS.lower)*Covariance[ibin] < minvar)
      {
        double this_lower_DRMS = std::log((1-minvar)/Covariance[ibin])/expTerm;
        thread_lower_DRMS[nthread] = std::max(this_lower_DRMS,thread_lower_DRMS[nthread]);
        PHASER_ASSERT(1-std::exp(expTerm*thread_lower_DRMS[nthread])*Covariance[ibin] > 0);
      }

      if (!nthread) output.logProgressBarNext(TESTING);
    } // end of pragma omp for
  } // end of pragma omp parallel
  output.logProgressBarEnd(TESTING);
  //bring all limits together
  for (int n = 0; n < nthreads; n++)
    DV.DRMS.lower = std::max(DV.DRMS.lower,thread_lower_DRMS[n]);

 //-------------------------------------------------------------------------------------
  output.logUnderLine(TESTING,"Variances and Model Weights by resolution bin:");
  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    std::string modelwt;
    for (unsigned iewald=1; iewald<=NEWALD; iewald++)
      modelwt += dtos(ModelWt[ibin](iewald),5,3) + " ";
    modelwt = modelwt.substr(0, modelwt.size()-1);
    if (!ibin)
    {
      output.logTabPrintf(1,TESTING,"Bin #%4i (  inf - %5.2f A) : V=%7.4f Wt=[%s]", ibin+1,ENS_BIN.HiRes(ibin),1-Covariance[ibin],modelwt.c_str());
    }
    else
    {
      output.logTabPrintf(1,TESTING,"Bin #%4i (%5.2f - %5.2f A) : V=%7.4f Wt=[%s]\n", ibin+1,ENS_BIN.LoRes(ibin),ENS_BIN.HiRes(ibin),1-Covariance[ibin],modelwt.c_str());
    }
  }

  //-------------------------------------------------------------------------------------
  // --- OK, finally happy with the weights

  resizeMaps(); //use sizeh,sizek,sizel

  for (unsigned ibin = 0; ibin < ENS_BIN.numbins(); ibin++)
  {
    for (int r = 0; r < miller.size(); r++)
    if (rbin[r] == ibin)
    {
      cmplxType WeightedE(0,0);
      for (unsigned iewald=0; iewald < NEWALD; iewald++)
        WeightedE += static_cast<floatType>(ModelWt[ibin][iewald])*
                     static_cast<cmplxType>(Fcalc[iewald][r]);
      //if (!SCATTERING) WeightedE = 0;  //will be zero
      setE(miller[r][0],miller[r][1],miller[r][2],WeightedE);
    }
    floatType WeightedV = Covariance[ibin];
    //if (!SCATTERING) WeightedV = 1;  //will be rms derived value>0, doesn't matter
    setV(ibin,WeightedV); // V is really a covariance now
  }

  if (flag_input_VRMS_changed)
  {
  DV.setup_vrms_limits();
  output.logTab(1,where,"VRMS delta lower/upper limit = " + dtos(DV.DRMS.lower,8,6) + " / " + dtos(DV.DRMS.upper,8,6));
  output.logTab(2,where,"RMSD input value(s): " + dvtos(DV.fixed_vrms_array(),5,3));
  output.logTab(2,where,"RMSD upper limit(s): " + dvtos(DV.upper_vrms_array(),5,3));
  output.logTab(2,where,"RMSD lower limit(s): " + dvtos(DV.lower_vrms_array(),5,3));
  output.logBlank(where);
  if (DV.DRMS.upper < DV.DRMS.lower)
  throw PhaserError(FATAL,"Ensemble incompatible with RMS limits");
  }
  return *this;

} // Constructor from pdbfiles

void Ensemble::set_relative_wilsonB(SigmaN& DATA_BIN,std::vector<af_cmplx>& Fcalc,Output& output)
{
  relative_wilsonB.clear(); //start again as this may be a different resolution
  int iewald(0);
  unsigned1D numInBin(DATA_BIN.numbins(),0);
  for (int r = 0; r < miller.size(); r++)
    numInBin[DATA_BIN.get_bin(input_CELL.S(miller[r]))]++;
  relative_wilsonB.resize(nMols());
  for (int ipdb = 0; ipdb < nMols(); ipdb++)
  {
    relative_wilsonB[ipdb].resize(ENSEMBLE[ipdb].nmodels());
    //the B-factor is refine wrt the B-factor of the search
    PHASER_ASSERT(ipdb < ENSEMBLE.size());
    for (unsigned m = 0; m < ENSEMBLE[ipdb].nmodels(); m++)
    {
      PHASER_ASSERT(iewald < Fcalc.size());
      float1D SigmaP(DATA_BIN.numbins(),0);
      for (int r = 0; r < miller.size(); r++)
      {
        int rbin = DATA_BIN.get_bin(input_CELL.S(miller[r]));
        SigmaP[rbin] += std::norm(Fcalc[iewald][r]);
      }
      for (int s = 0; s < DATA_BIN.numbins(); s++)
      {
        if (numInBin[s])
        {
          SigmaP[s] /= numInBin[s]; //Here epsilon is 1.0 (P1)
          PHASER_ASSERT(SigmaP[s]>0);
        }
      }

      floatType WilsonB(0);
      { //memory
        floatType sumw(0),sumwx(0),sumwy(0),sumy(0),sumwx2(0),sumwxy(0);
        int countr(0);
        for (unsigned s = 0; s < DATA_BIN.numbins(); s++)
        if (numInBin[s])
        {
          floatType ssqr_bin(1.0/fn::pow2(DATA_BIN.MidRes(s)));
          floatType log_bin = std::log(DATA_BIN.sigmaN[s]/SigmaP[s]);
          floatType weight = 1;
          if (ssqr_bin < 0.04) weight = weight * fn::pow2(ssqr_bin/0.04); // Down-weight data below 5A - doesn't fit BEST curve
          if (ssqr_bin < 0.009)
            weight = 0; // Downweight data below 10.5409 - no BEST curve, although test not used
          else countr++;
          sumw += weight;
          sumwx += weight*ssqr_bin;
          sumwx2 += weight*fn::pow2(ssqr_bin);
          sumwy += weight*log_bin;
          sumy += ssqr_bin;
          sumwxy += weight*ssqr_bin*log_bin;
        }
        floatType slope = 2 <=countr ?  (sumw*sumwxy-(sumwx*sumwy))/(sumw*sumwx2-fn::pow2(sumwx)): 0;
        floatType WilsonB_intensity = -4*slope;
        WilsonB = WilsonB_intensity/2.0;
      }
      PHASER_ASSERT(ipdb < relative_wilsonB.size());
      PHASER_ASSERT(m < relative_wilsonB[ipdb].size());
      relative_wilsonB[ipdb][m] = WilsonB; //local copy for output
      iewald++;
    }
  }
}

}//phaser
