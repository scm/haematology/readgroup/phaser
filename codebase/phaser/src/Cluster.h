//(i) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ClusterClass__
#define __ClusterClass__
#include <phaser/main/Phaser.h>
#include <phaser/src/Molecule.h>
#include <cctbx/eltbx/xray_scattering/gaussian.h>

namespace phaser {

class Cluster
{
  public:
    Cluster() { is_not_cluster = true; }

  private:
    bool        is_not_cluster;
    float1D     twopi_ra;
    string1D    scattering_type;
    std::map<std::string,cctbx::eltbx::xray_scattering::gaussian> ff;

    void        init(Molecule&);
    std::string ta6br12_data();

  public:
    void        init_ta6br12();
    void        init_pdb(std::string);
    floatType   debye(floatType);
    floatType   scattering(floatType);
};

}

#endif
