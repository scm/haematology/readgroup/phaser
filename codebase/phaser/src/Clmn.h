//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CLMN__
#define __CLMN__
#include <complex>

namespace phaser {

cmplx3D clmnx(cmplx3D&,cmplx3D&,int,int);

}

#endif
