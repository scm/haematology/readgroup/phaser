//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/lib/maths.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/rotationgroup.h>

namespace phaser {

cmplx3D clmnx(cmplx3D& elmn1,cmplx3D& elmn2,int LMIN, int LMAX)
{
  // Calculate the Clmm' from two Elmn lists by summing over the n values

  cmplx3D clm1m2(0);
  cmplxType temp(0);
  int l(0);
  int max_index = LMAX/2;

  PHASER_ASSERT(elmn1.size());
  PHASER_ASSERT(elmn1.size() == elmn2.size());

  clm1m2.resize(elmn1.size());
  for (int l_index = 0; l_index < max_index; l_index++) {
    l=2*l_index+2;
   // PHASER_ASSERT(l_index < clm1m2.size());
    clm1m2[l_index].resize(2*l+1);
    for (int m1 = 0; m1 < 2*l+1; m1++) {
    //  PHASER_ASSERT(m1 < clm1m2[l_index].size());
      clm1m2[l_index][m1].resize(2*l+1);
      for (int m2 = 0; m2 < 2*l+1; m2++) {
        temp = 0;
    //    PHASER_ASSERT(l_index < elmn1.size());
    //    PHASER_ASSERT(m1 < elmn1[l_index].size());
    //    PHASER_ASSERT(l_index < elmn2.size());
    //    PHASER_ASSERT(m2 < elmn2[l_index].size());
        for (unsigned int n = 1; n <std::min(elmn1[l_index][m1].size(),elmn2[l_index][m2].size()); n++) {
    //      PHASER_ASSERT(n < elmn1[l_index][m1].size());
    //      PHASER_ASSERT(n < elmn2[l_index][m2].size());
          temp += std::conj(elmn1[l_index][m1][n])*elmn2[l_index][m2][n];
        }
    //    PHASER_ASSERT(l_index < clm1m2.size());
    //    PHASER_ASSERT(m1 < clm1m2[l_index].size());
    //    PHASER_ASSERT(m2 < clm1m2[l_index][m1].size());
        clm1m2[l_index][m1][m2] = temp;
        if (l_index < (LMIN-2)/2 || l_index > (LMAX-2)/2) clm1m2[l_index][m1][m2] = 0.0;
      }
    }
  }

  PHASER_ASSERT(clm1m2.size());
  return clm1m2;
}

} //phaser
