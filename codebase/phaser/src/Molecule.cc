//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Molecule.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/mr_objects/rms_estimate.h>
#include <phaser/mr_objects/data_model.h>
#include <phaser/lib/round.h>
#include <phaser/lib/euler.h>
#include <scitbx/constants.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/adptbx.h>
#include <phaser/io/hybrid_36_c.h>
#include <iotbx/pdb/hierarchy.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>
#include <iotbx/pdb/input.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/eltbx/icsd_radii.h>
#include <phaser/lib/mean.h>
#include <scitbx/math/superpose.h>
#include <scitbx/math/r3_rotation.h>
#include <boost/assign.hpp>
#include <cctbx/sgtbx/site_symmetry.h>
#include <cctbx/sgtbx/site_symmetry_table.h>
#include <mmdb.h>
#ifdef PHASER_CONFIGURE_WITH_MMDB2
#include <ccp4io/mmdb/mmdb_manager.h>
#endif

namespace phaser {

Molecule::Molecule(data_model MODEL,bool USE_HETATM)
{
  protein_resid = boost::assign::list_of
  ("ALA")("ARG")("ASN")("ASP")("CYS")("GLN")("GLU")
  ("GLY")("HIS")("ILE")("LEU")("LYS")("MET")("MSE")
  ("PHE")("PRO")("SER")("THR")("TRP")("TYR")("VAL").
  convert_to_container<std::list<std::string> >();
  nucleic_resid = boost::assign::list_of
  ("A  ")("C  ")("G  ")("I  ")("T  ")("U  ")
  ("+A ")("+C ")("+G ")("+I ")("+T ")("+U ")
  ("  A")("  C")("  G")("  I")("  T")("  U")
  (" +A")(" +C")(" +G")(" +I")(" +T")(" +U")
  ("DA ")("DC ")("DG ")("DI ")("DT ")("DU ")
  (" DA")(" DC")(" DG")(" DI")(" DT")(" DU").
  convert_to_container<std::list<std::string> >();
  protein_trace = boost::assign::list_of
  (" CA ").convert_to_container<std::list<std::string> >();
  protein_backbone = boost::assign::list_of
  (" CA ")(" C  ")(" O  ")(" N  ")(" CB ").
  convert_to_container<std::list<std::string> >();
  nucleic_trace = boost::assign::list_of
  (" P  ")(" C3*")(" C4*")(" C3'")(" C4'").
  convert_to_container<std::list<std::string> >();
  input_as_atom = input_as_helix = false;
  pdb_errors = "";
  mtrace = 0;
  protein_nres = nucleic_nres = 0;
  model_rmsid.clear();
  REMARKS.clear();
  if (MODEL.is_default()) return;

  { //fill pdb
  if (MODEL.pdb_format())
  {
    if (MODEL.is_string())
    {
      std::istringstream iss( MODEL.filename(), std::istringstream::in );
      ReadStreamPDB( iss );
    }
    else
    {
      ReadPDB(MODEL.filename());
    }
    //delete hetero atoms, only relevant to pdb files
    if (pdb_errors == "" && !USE_HETATM)
    {
      map_str_str HETATM_RESNAME;
      //maps ResName to one letter amino acid code
      HETATM_RESNAME["MSE"] = "M"; //don't delete seleno-met etc
      HETATM_RESNAME["MSO"] = "M";
      HETATM_RESNAME["CSE"] = "C";
      HETATM_RESNAME["CSO"] = "C";
      HETATM_RESNAME["ALY"] = "K";
      HETATM_RESNAME["MLY"] = "K";
      HETATM_RESNAME["MLZ"] = "K";
    //HETATM_RESNAME["PTR"] = "Y";
    //HETATM_RESNAME["SEP"] = "S";
    //HETATM_RESNAME["TPO"] = "T";
    //if there are all hetatms assume that the user wants to use them all
      bool all_hetatm(true);
      for (unsigned m = 0; m < pdb.size(); m++)
        for (int a = 0; a < pdb[m].size(); a++)
          if (!pdb[m][a].Hetatm)
            all_hetatm = false;
      if (!all_hetatm)
      for (unsigned m = 0; m < pdb.size(); m++)
        for (int a = pdb[m].size()-1; a >= 0; a--) //has to be int not unsigned: last iter goes negative
          if (pdb[m][a].Hetatm &&
              HETATM_RESNAME.find(pdb[m][a].ResName) == HETATM_RESNAME.end())
            pdb[m][a].O = 0;
        //set the occupancy to zero so that there is no scattering, but carry through
        //  pdb[m].erase(pdb[m].begin()+a);
    }
  }
  else if (MODEL.cif_format())
  {
    PHASER_ASSERT(!MODEL.is_string());
    ReadCIF( MODEL.filename() );
  }
  else if (MODEL.ha_format())
  {
    PHASER_ASSERT(!MODEL.is_string());
    ReadHA( MODEL.filename() );
  }
  else if (MODEL.is_atom())
  {
    input_as_atom = true;
    pdb_record next_atom;
    next_atom.AtomName = MODEL.element();
    next_atom.ResName = "ATM";
    next_atom.Element = MODEL.element();
    pdb.resize(1);
    pdb[0].resize(1);
    pdb[0][0] = next_atom;
  }
  else if (MODEL.is_helix())
  {
    input_as_helix = true;
    pdb.resize(1);
    pdb[0].clear();
    int natom(1);
    std::vector<std::pair<std::string,dvect3> > ala(5);
    ala[0] = std::pair<std::string,dvect3>(" N  " , dvect3(-1.488, -0.324,  0.000));
    ala[1] = std::pair<std::string,dvect3>(" CA " , dvect3(-1.729, -1.484,  0.869));
    ala[2] = std::pair<std::string,dvect3>(" C  " , dvect3(-0.653, -1.558,  1.954));
    ala[3] = std::pair<std::string,dvect3>(" O  " , dvect3(-0.955, -1.744,  3.143));
    ala[4] = std::pair<std::string,dvect3>(" CB " , dvect3(-1.695, -2.748,  0.062));
    for (int nres = 0; nres < MODEL.helix_length(); nres++)
    {
      dmat33 helixrot = euler2matrixDEG(dvect3(nres*100,0,0));
      dvect3 helixtra(0,0,1.501*nres);
      pdb_record next_atom;
      next_atom.ResNum = nres+1;
      next_atom.ResName = "ALA";
      for (int a = 0; a < ala.size(); a++)
      {
        next_atom.AtomNum = natom++;
        next_atom.AtomName = ala[a].first;
        next_atom.X = helixrot*ala[a].second  + helixtra;
        next_atom.Element = ala[a].first[1];
        pdb[0].push_back(next_atom);
      }
    }
  }
  }
  //pdb_model_serial defaults if it has not been set on reading
  {
    if (!pdb_model_serial.size()) //currently only set for a PDB input
    {
      for (int m = 0; m < pdb.size(); m++)
        pdb_model_serial.push_back(m+1);  //model numbering from 1 sequentially
    }
    else if (MODEL.MODEL_SERIAL != -999)//logic not quite right here. Should be able to select from CIFs also
    {
      bool found_model(false);
      int m(0);
      for (m = 0; m < pdb_model_serial.size(); m++)
      if (pdb_model_serial[m] == MODEL.MODEL_SERIAL)
      {
        found_model = true;
        break; //m is value at match
      }
      if (!found_model) pdb_errors = "MODEL serial numbers do not contain selected MODEL number";
      std::vector<pdb_record> tmppdb = pdb[m];
      pdb.clear();
      pdb_model_serial.clear();
      pdb.push_back(tmppdb);
      pdb_model_serial.push_back(MODEL.MODEL_SERIAL);
      PHASER_ASSERT(pdb_model_serial.size() == pdb.size());
      if (model_rmsid.size())
      {
        data_vartype  tmpvar = model_rmsid[m];
        model_rmsid.clear();
        model_rmsid.push_back(tmpvar);
        PHASER_ASSERT(model_rmsid.size() == pdb.size());
      }
    }
  }
  if (MODEL.CHAIN.size())
  {
    for (int m = 0; m < pdb.size(); m++)
    {
      bool found_chain(false);
      for (int a = 0; a < pdb[m].size(); a++)
      {
        if (pdb[m][a].Chain == MODEL.CHAIN)
          found_chain = true;
        else
          pdb[m][a].O = 0;
      }
      if (!found_chain) pdb_errors = "MODEL does not contain CHAIN selection";
    }
  }
  PHASER_ASSERT(pdb_model_serial.size() == pdb.size());

  //delete the waters, they are wrong for any new structure
  if (MODEL.pdb_format() || MODEL.cif_format())
  {
    stringset water_atom;
    water_atom.insert("OW");
    water_atom.insert("OH2");
    water_atom.insert("WAT");
    stringset water_residue;
    water_residue.insert("HOH");
    water_residue.insert("H2O");
    water_residue.insert("OH2");
    water_residue.insert("MOH");
    water_residue.insert("WAT");
    water_residue.insert("WTR");
    water_residue.insert("TIP");
    water_residue.insert("OW");
    water_residue.insert("DOD");
    water_residue.insert("OD2");
    for (unsigned m = 0; m < pdb.size(); m++)
    {
      for (int a = pdb[m].size()-1; a >= 0; a--) //int not unsigned
      {
        if (water_atom.find(pdb[m][a].AtomName) != water_atom.end() ||
            water_residue.find(pdb[m][a].ResName) != water_residue.end())
          pdb[m].erase(pdb[m].begin()+a);
      }
    }
  }

  if (MODEL.BFAC_ZERO)
  {
    for (int m = 0; m < pdb.size(); m++)
    {
      for (int a = 0; a < pdb[m].size(); a++) //int not unsigned
      {
        pdb[m][a].B = 0;
      }
    }
  }

  if (!model_rmsid.size()) //not input from REMARK cards
    for (int m = 0; m < pdb.size(); m++)
      model_rmsid.push_back(MODEL); //copy input to all
  //Can have more REMARK model rms values if the pdb has been truncated by occupancy refinement
  //Doesn't matter if the REMARKS don't match the coords as long as the coords are a subset of the remarks
  //else if (MODEL.is_card() && model_rmsid.size() != pdb.size())
  //  pdb_errors = "REMARK records do not contain correct number of RMS entries";

  for (int m = 0; m < pdb.size(); m++)
  {
    if (nucleic_nres && model_rmsid[m].is_id())
    {
      pdb_errors = "Cannot input variance as sequence idenity for nucleic acid";
      return;
    }
  }

//if it is an xray pdb file, then there is only one model
//if it is an NMR model, then the first model makes the trace
//if it is an ensemble with CARD ON then the lowest rms model makes the trace
  mtrace = 0;
  if (model_rmsid.size()) //CARD ON
  {
    if (model_rmsid[0].is_rms())
    {
      double lowest_rms = model_rmsid[0].RMSID;
      for (int m = 1; m < pdb.size(); m++)
      if (model_rmsid[m].is_rms())
      { //if have switched from rms to id, don't include in test
        double rms = model_rmsid[m].RMSID;
        if (rms && rms < lowest_rms)
        {
          mtrace = m;
          lowest_rms = rms;
        }
      }
    }
    else
    {
      double highest_id = model_rmsid[0].RMSID;
      for (int m = 1; m < pdb.size(); m++)
      if (model_rmsid[m].is_id())
      { //if have switched from id to rms, don't include in test
        double id = model_rmsid[m].RMSID;
        if (id && id > highest_id)
        {
          mtrace = m;
          highest_id = id;
        }
      }
    }
  }
  PHASER_ASSERT(mtrace >= 0);
  PHASER_ASSERT(mtrace < pdb.size());

  if (MODEL.coordinates().size())
  {
    bool has_at_least_one_atom(false);
    for (unsigned m = 0; m < pdb.size(); m++)
    for (unsigned a = 0; a < pdb[m].size(); a++)
      if (pdb[m][a].O > 0.0)
        has_at_least_one_atom = true;
    bool no_scattering = (!has_at_least_one_atom);
    if (no_scattering)
      pdb_errors = "No scattering in coordinate file";
  }

  protein_nres = nucleic_nres = 0;
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
    if (!pdb[mtrace][a].Hetatm) //must be protein, not a general trace atom (dna)
    {
      if (std::find(protein_resid.begin(),protein_resid.end(),pdb[mtrace][a].ResName) != protein_resid.end() &&
          std::find(protein_trace.begin(),protein_trace.end(),pdb[mtrace][a].AtomName) != protein_trace.end() )
       protein_nres++;
      else if (std::find(nucleic_resid.begin(),nucleic_resid.end(),pdb[mtrace][a].ResName) != nucleic_resid.end() &&
          std::find(nucleic_trace.begin(),nucleic_trace.end(),pdb[mtrace][a].AtomName) != nucleic_trace.end() )
      nucleic_nres++;
    }
}

bool Molecule::is_atom() const
{
  if (input_as_atom) return true; //input
  if (input_as_helix) return false; //input
  if (pdb.size() > 1) return false; //input

  //calculate if it is a single atom
  bool this_is_one_atom = false;
  int natom(0);
  for (unsigned a = 0; a < pdb[0].size(); a++)
  if (pdb[0][a].O > 0.0)
  {
    natom++;
    if (natom >= 2) break; //molecule
  }
  if (natom == 1) this_is_one_atom = true;
  return this_is_one_atom;
}

bool Molecule::is_helix()
{
  if (input_as_atom) return false; //input
  if (input_as_helix) return true; //input
  if (pdb.size() > 1) return false; //input ???? xxxx could have ensemble of helices?

  //calculate if it is a helix
  bool this_is_a_helix = true;
  af::shared< scitbx::vec3< floatType > > polyala_segment,reference_segment;
  int ca(0),min_helix_length(7);
  floatType max_rmsd(0.3); //Glykos test cast rmsd=0.52 #Ca=23
  for (unsigned a = 0; a < pdb[0].size(); a++)
  {
    if (pdb[0][a].AtomName == " CA ") //must be protein, not a general trace atom (dna)
    {
      reference_segment.push_back(pdb[0][a].X);
      //scitbx::vec3< floatType > N (-1.488, -0.324,  0.000);
      scitbx::vec3< floatType > CA(-1.729, -1.484,  0.869);
      //scitbx::vec3< floatType > C (-0.653, -1.558,  1.954);
      //scitbx::vec3< floatType > O (-0.955, -1.744,  3.143);
      //scitbx::vec3< floatType > CB(-1.695, -2.748,  0.062);
      dmat33 helixrot = euler2matrixDEG(dvect3(ca*100,0,0));
      dvect3 helixtra(0,0,1.501*ca);
      CA = helixrot*CA + helixtra;
      polyala_segment.push_back(CA);
      scitbx::math::superpose::least_squares_fit<floatType> lsf(reference_segment,polyala_segment);
      floatType rmsd = lsf.other_sites_rmsd();
      ca++;
      if (ca > min_helix_length && rmsd > max_rmsd)
      {
        this_is_a_helix = false;
        break;
      }
      //only test 7 residues (two turns) so as to identify bent helices
      if (ca >= min_helix_length) //ready for next one
      {
        polyala_segment.erase(polyala_segment.begin());
        reference_segment.erase(reference_segment.begin());
      }
    }
  }
  if (ca <= min_helix_length) this_is_a_helix = false;
  return this_is_a_helix;
}

void Molecule::ReadPDB(std::string file)
{
  pdb_errors = "";
  pdb.clear(); pdb.resize(1); pdb[0].resize(0);
  std::ifstream infile(const_cast<char*>(file.c_str()));
  if (!infile)
  {
    pdb_errors = "File \"" + file + "\" could not be opened";
    return;
  }
  ReadStreamPDB(infile);
  infile.close();
}

void Molecule::ReadStreamPDB(std::istream& infile)
{
  pdb_errors = "";
  pdb.clear(); pdb.resize(1); pdb[0].resize(0);
  //flag for when there are no MODEL SERIAL cards, but the ID is entered on a REMARK card
  bool remark_card_model_zero(false);

  //want to find all the errors in this pdb file in one read of the pdb file
  //OtherMolwise you have to run phaser a million times to collect them all
  std::set<int> model_serial;

  stringset TITLE_SECTION;
  TITLE_SECTION.insert("REMARK");
  TITLE_SECTION.insert("HEADER");
  TITLE_SECTION.insert("SOURCE");
  TITLE_SECTION.insert("AUTHOR");
  TITLE_SECTION.insert("OBSLTE");
  TITLE_SECTION.insert("KEYWDS");
  TITLE_SECTION.insert("TITLE");
  TITLE_SECTION.insert("JRNL:");
  TITLE_SECTION.insert("COMPND");
  try {
    while (!infile.eof())
    {
      pdb_record next_atom;
      std::string buffer;
      std::getline(infile, buffer);
      //only allow ATOM cards at start
      for (stringset::iterator iter = TITLE_SECTION.begin(); iter != TITLE_SECTION.end(); iter++)
      if (!buffer.find(*iter)) //REMARK
      { //store the REMARKS for output
        REMARKS.push_back(buffer);
      }
      if (!buffer.find("ATOM") || !buffer.find("HETATM")) //ATOM and optionally HETATOM at start
      {
        int this_pdb_errors_size(pdb_errors.size());
        next_atom.Hetatm = !buffer.find("HETATM");
        next_atom.ATOMorHETATMandNUM = buffer.substr(0,11);
        next_atom.AtomNum = std::atoi(buffer.substr(6,5).c_str());
        next_atom.AtomName = buffer.substr(12,4);
        next_atom.AltLoc = buffer.substr(16,1);
        next_atom.ResName = buffer.substr(17,3);
        next_atom.Chain = buffer.substr(20,2);
        next_atom.ResNum = std::atoi(buffer.substr(22,4).c_str());
        next_atom.InsCode = buffer.substr(26,1);
        std::string Xstr = buffer.substr(30,8);
     // if (!isfloat(Xstr)) pdb_errors += "Atom has invalid X (" + Xstr + ")\n";
        next_atom.X[0] = std::atof(Xstr.c_str());
        std::string Ystr = buffer.substr(38,8);
     // if (!isfloat(Ystr)) pdb_errors += "Atom has invalid Y (" + Ystr + ")\n";
        next_atom.X[1] = std::atof(Ystr.c_str());
        std::string Zstr = buffer.substr(46,8);
     // if (!isfloat(Zstr)) pdb_errors += "Atom has invalid Z (" + Zstr + ")\n";
        next_atom.X[2] = std::atof(Zstr.c_str());
        std::string Ostr = buffer.substr(54,6);
     // if (!isfloat(Ostr)) pdb_errors += "Atom has invalid O (" + Ostr + ")\n";
        next_atom.O = std::atof(Ostr.c_str());
        if (next_atom.O < 0.0 && next_atom.AtomName != "UNK ") pdb_errors += "Atom has negative occupancy (" + dtos(next_atom.O) + ")\n";
        if (next_atom.X == dvect3(9999,9999,9999) && next_atom.O != 0) pdb_errors += "Atom has coordinates at \"infinity\" (" + dvtos(next_atom.X) + ")\n";
        std::string Bstr = buffer.substr(60,6);
     // if (!isfloat(Bstr)) pdb_errors += "Atom has invalid B (" + Bstr + ")\n";
        next_atom.B = std::atof(Bstr.c_str());
     // if (next_atom.B < 0.0 && next_atom.AtomName != "UNK ") pdb_errors += "Atom has negative B-factor (" + dtos(next_atom.B) + ")\n";
        next_atom.B = std::atof(buffer.substr(60,6).c_str());
        next_atom.FlagAnisoU = false;
        if (buffer.size() >= 76) next_atom.Segment = buffer.substr(72,4);
        bool valid_element_or_cluster(false);
        iotbx::pdb::hierarchy::atom guessAtomType;
        guessAtomType.set_name(next_atom.AtomName.c_str());
        std::string AtmElement("col 77-78 not set");
        if (next_atom.AtomName == "UNK ") //allowed by pdb
        {
          next_atom.Element = " X"; //will fail elsewhere
          valid_element_or_cluster = true;
        }
        else
        if (buffer.size() >= 78)
        {
          AtmElement = buffer.substr(76,2);
          guessAtomType.set_element(AtmElement.c_str());
          if (is_alpha(AtmElement[0]) &&
              AtmElement[1] == 'X') //AX RX TX or cluster on Element cols
          {
            next_atom.Element = AtmElement;
            valid_element_or_cluster = true;
          }
        }
        else if (next_atom.AtomName[0] == ' ' &&
                 is_alpha(next_atom.AtomName[1]) && //any letter of alphabet
                 next_atom.AtomName[2] == 'X' &&
                 next_atom.AtomName[3] == ' ') //AX RX TX or cluster on Name cols
        {
          next_atom.Element = next_atom.AtomName.substr(1,2); //OtherMol cluster shorten 2 chars
          valid_element_or_cluster = true;
        }
        //guessAtomType has name and element set - if there are extra columns
        if (!valid_element_or_cluster && guessAtomType.determine_chemical_element_simple())
        {
          next_atom.Element = guessAtomType.determine_chemical_element_simple().get();
          //left justify
          std::stringstream s;
          s << next_atom.Element;
          s >> next_atom.Element;
          valid_element_or_cluster = true;
        }
        else if (!valid_element_or_cluster)
        {
          pdb_errors += "Atom Name (" + next_atom.AtomName + ") Element (" + AtmElement + ") not recognised\n";
          valid_element_or_cluster = false;
        }
        if (valid_element_or_cluster)
        {
//atom only accepted if it has an element type
          //must increment input number of models before doing anything
          int m = pdb.size() - 1;
          pdb[m].push_back(next_atom); //if above throws error this record is ignored
        }
        if (pdb_errors.size() > this_pdb_errors_size) pdb_errors += buffer + "\n";
      }
      else if (!buffer.find("ANISOU"))
      {
        int m = pdb.size()-1; //associate ANISOU card with previous atom
        int a = pdb[m].size()-1; //associate ANISOU card with previous atom
        if (a >= 0) //paranoia, in case rogue ANISOU comes before an ATOM card
        {
          pdb[m][a].FlagAnisoU = true;
          pdb[m][a].AnisoU[0] = std::atof(buffer.substr(28,7).c_str());
          pdb[m][a].AnisoU[1] = std::atof(buffer.substr(35,7).c_str());
          pdb[m][a].AnisoU[2] = std::atof(buffer.substr(42,7).c_str());
          pdb[m][a].AnisoU[3] = std::atof(buffer.substr(49,7).c_str());
          pdb[m][a].AnisoU[4] = std::atof(buffer.substr(56,7).c_str());
          pdb[m][a].AnisoU[5] = std::atof(buffer.substr(63,7).c_str());
        }
      }
      else if (!buffer.find("MODEL"))
      {
        std::vector<pdb_record> tmp(0);
        if (!(pdb.size() == 1 && pdb[0].size() == 0)) //this is the first model
          pdb.push_back(tmp); //start new
        //parse the MODEL line to extract SERIAL for comparison with REMARK
        std::stringstream modelstream(stoup(buffer));
       // floatType RMSID(0);
        try {
          std::string MODEL;
          int SERIAL(0);
          modelstream >> MODEL;
          modelstream >> SERIAL;
          pdb_model_serial.push_back(SERIAL); //record serial for REMARK PHASER ENSEMBLE in order of [m]
          SERIAL--;
          model_serial.insert(SERIAL); //record serial for REMARK PHASER ENSEMBLE cards
        }
        catch (std::stringstream::failure& e) { } //do nothing if parsing fails and/or RMS is not present
      }
      else if (!buffer.find("REMARK")) //REMARK
      {
        std::stringstream modelstream(stoup(buffer));
        floatType RMSID(-999); //any arbitrary negative value
        int SERIAL(-999); //any arbitrary negative value
        bool is_phaser_rms_card(false),good_parsing(false);
        std::string REMARK,PHASER,ENSEMBLE,MODEL,VARTYPE("");
        try {
          modelstream >> REMARK;
          modelstream >> PHASER;
          modelstream >> ENSEMBLE;
          modelstream >> MODEL;
          //card identified with REMARK PHASER ENSEMBLE MODEL
          if (PHASER == "PHASER" && ENSEMBLE == "ENSEMBLE" && MODEL == "MODEL") is_phaser_rms_card = true;
          modelstream >> SERIAL;
          modelstream >> VARTYPE;
          modelstream >> RMSID;
          VARTYPE = stoup(VARTYPE);
          if (VARTYPE == "RMS")
          {
            if (RMSID < 0)
              pdb_errors += "\n\nREMARK PHASER ENSEMBLE MODEL " + itos(SERIAL) + " ID is negative";
          }
          else if (VARTYPE == "ID" || VARTYPE == "IDENTITY")
          {
            if (RMSID > 1) RMSID /= 100;
            if (RMSID > 1) RMSID = 1;
            if (RMSID < 0)
              pdb_errors += "\n\nREMARK PHASER ENSEMBLE MODEL " + itos(SERIAL) + " ID is negative";
          }
          if (SERIAL >= 0 && RMSID > 0) good_parsing = true;
        }
        catch (std::stringstream::failure& e) { } //do nothing if parsing fails and/or RMS is not present
        if (is_phaser_rms_card && good_parsing)
        {
          SERIAL--; //C++ array numbering
          if (SERIAL == -1)  //REMARK PHASER MODEL 0 [ID/RMS] <RMSID>
          {
            model_rmsid.resize(1);
            model_rmsid[0].set_vartype(VARTYPE); //no model cards, -999 if not set
            model_rmsid[0].set_rmsid(RMSID); //no model cards, -999 if not set
            remark_card_model_zero = true;
          }
          else if (SERIAL < model_rmsid.size())
          {
            model_rmsid[SERIAL].set_vartype(VARTYPE); //no model cards, -999 if not set
            model_rmsid[SERIAL].set_rmsid(RMSID);
          }
          else
          { //add some spaces equal to flags until there is an appropriate index
            for (int i = model_rmsid.size()-1; i < SERIAL; i++)
            {
              model_rmsid.push_back(data_vartype()); //any arbitrary negative value
            }
            model_rmsid[SERIAL].set_vartype(VARTYPE);
            model_rmsid[SERIAL].set_rmsid(RMSID);
          }
        }
        else if (is_phaser_rms_card)
          pdb_errors += "\n\nParse error on REMARK PHASER card";
      }
     // else if (!buffer.find("ENDMDL")) //no action required
     // else if (!buffer.find("END")) break; //don't read any more
     // pymol outputs END then ENDMDL
    }
  }
  catch (std::exception const& err) {
  //this catches the error  basic_string::_M_check where the read runs out of memory if there is
  //something weird/missing at the end of a file
  }

  if (pdb_errors != "") return;

  std::string pdb_error_str = "\'MODEL SERIAL\' & \'REMARK PHASER ENSEMBLE MODEL SERIAL\' disagree";

  if (remark_card_model_zero && pdb.size() > 1)
  {
    pdb_errors = "\'MODEL SERIAL\' & \'REMARK PHASER ENSEMBLE MODEL 0\' both present";
    return;
  }

  if (model_rmsid.size() && pdb.size() != model_rmsid.size())
  {
    pdb_errors = pdb_error_str;
    return;
  }

  if (model_rmsid.size())
  for (std::set<int>::iterator iter = model_serial.begin(); iter != model_serial.end(); iter++)
  {
    int SERIAL = *iter+1;
    if (SERIAL > model_rmsid.size())
    {
      pdb_errors = pdb_error_str;
      return;
    }
    if (remark_card_model_zero && SERIAL == 1)
    {
      pdb_errors = pdb_error_str;
      return;
    }
  }

  for (int f = 0; f < model_rmsid.size(); f++) //input from REMARK cards
  {
    if (model_serial.find(f) != model_serial.end()) //input from MODEL cards
      if (!model_rmsid[f].is_set())
      {
        pdb_errors = pdb_error_str;
        return;
      }
  }
}

void Molecule::ReadHA(std::string hafile)
{
  pdb_errors = "";
  pdb.clear(); pdb.resize(1); pdb[0].resize(0);
  std::ifstream infile(const_cast<char*>(hafile.c_str()));
  if (!infile)
  {
    pdb_errors = "File \"" + hafile + "\" could not be opened";
    return;
  }
  pdb_record next_atom;
  //std::size_t init_pdb_errors_size(pdb_errors.size());
  std::string separators(" \t");
  try {
    while (!infile.eof())
    {
      std::string buffer;
      std::getline(infile, buffer);
      std::vector<std::string> value;
      std::size_t start = buffer.find_first_not_of(separators);
      while (start < buffer.size()) {
        size_t stop = buffer.find_first_of(separators,start);
        if (stop > buffer.size()) stop = buffer.size();
        value.push_back(buffer.substr(start,stop-start));
        start = buffer.find_first_not_of(separators,stop+1);
      }
      if (!buffer.find("ATOM") && value.size() < 8)
        pdb_errors += "Too few values\n" + buffer +"\n";
      if (!buffer.find("ATOM") && value.size() >= 8) //ATOM at start
      {
// free format
// ATOM atom_name x y z occupancy {anomalous_occupancy} BFAC B-factor
// Ignore anomalous_occupancy if present
//
        if (value[6] != "BFAC" && value[7] != "BFAC")
          pdb_errors += "No BFAC keyword\n" + buffer +"\n";
        int pbfac = (value[6] == "BFAC") ? 6:7;
        if (value.size() < pbfac+1)
          pdb_errors += "No B-factor after BFAC\n" + buffer +"\n";
     // if (!isfloat(value[pbfac+1])) pdb_errors += "Number not found for B-factor\n" + buffer +"\n";
        next_atom.Element = stoup(value[1]);
     // for (int n = 2; n < 6; n++) if (!isfloat(value[n])) pdb_errors += "Number not found at position " + itos(n+1) + "\n" + buffer +"\n";
        next_atom.X[0] =  std::atof(value[2].c_str()); //return 0 if conversion not possible
        next_atom.X[1] = std::atof(value[3].c_str());
        next_atom.X[2] = std::atof(value[4].c_str());
        next_atom.O    = std::atof(value[5].c_str());
        if (next_atom.O < 0.0) pdb_errors += "Atom has negative occupancy\n" + buffer +"\n";
        next_atom.B = std::atof(value[pbfac+1].c_str());
        if (next_atom.B < 0.0 && next_atom.AtomName != "UNK ") pdb_errors += "Atom has negative B-factor\n" + buffer +"\n";
        int m = pdb.size()-1;
        pdb[m].push_back(next_atom);
      }
    }
  }
  catch (std::exception const& err) {
  //this catches the error  basic_string::_M_check where the read runs out of memory if there is
  //something weird/missing at the end of a file
  }
}

std::string Molecule::ReadCIF(std::string filename)
{
  std::string pdb_file_content;
#ifdef PHASER_CONFIGURE_WITH_MMDB2
  mmdb::Manager* mmdb_manager = new mmdb::Manager();
  mmdb_manager->SetFlag(
               mmdb::MMDBF_PrintCIFWarnings |
               mmdb::MMDBF_FixSpaceGroup |
               mmdb::MMDBF_IgnoreDuplSeqNum |
               mmdb::MMDBF_IgnoreHash |
               mmdb::MMDBF_IgnoreNonCoorPDBErrors |
               mmdb::MMDBF_DoNotProcessSpaceGroup);

  mmdb::ERROR_CODE  RC = mmdb_manager->ReadCoorFile(filename.c_str());
  if (RC == 15)
  {
    pdb_errors = "File \"" + filename + "\" could not be opened";
  }
  else if (RC)
  {
    int lcount;
    char S[500];
    pdb_errors += ( "Read " + std::string(mmdb::GetErrorDescription(RC) ));
    mmdb_manager->GetInputBuffer ( S, lcount );
    if (lcount>=0)
      pdb_errors += ( "line #" + itos(lcount) + " " + std::string(S));
    else if (lcount==-1)
      pdb_errors += ( "cif item " + std::string(S));
  }
/*
  else if (true)
  {
    PPCAtom* mmdbpdb = new PPCAtom[mmdb_manager->GetNumberOfModels()];
    int*  mmdbpdb_n = new int[mmdb_manager->GetNumberOfModels()];
    int selHnd = mmdb_manager->NewSelection();
    for (int m = 0; m < mmdb_manager->GetNumberOfModels(); m++)
    {
      mmdb_manager->SelectModel ( selHnd,mmdb_manager->GetModel(m+1),
                                  STYPE_ATOM,SKEY_NEW,true );
      mmdb_manager->GetSelIndex ( selHnd,mmdbpdb[m],mmdbpdb_n[m] );
    }
    for (int m = 0; m < mmdb_manager->GetNumberOfModels(); m++)
      for (int a = 0; a < mmdbpdb_n[m]; a++)
        std::cout << "mmdbpdb test: " << m << " " << a << " " << mmdbpdb[m][a]->name << std::endl;
  }
*/
  else
  {
    mmdb::io::File   f;
    mmdb::pstr    S;       // string (char *) to receive the content
    mmdb::word    sSize;   // int to receive content's length
    int initial_mem(0);
    f.assign ( initial_mem,    // initial size for memory buffer
               2000, // increment for buffer size if not sufficient
               NULL  // no initial buffer
              );
    f.rewrite();  // like with a normal file
    mmdb_manager->WritePDBASCII(f);   // outputs into internal buffer
    f.shut();  // like with a normal file
    f.GetFilePool ( S,sSize );  // retrieve content
    S[sSize] = '\0'; //or else there are memory problems
    pdb_file_content = std::string(S);
    if (!pdb_file_content.size()) pdb_errors = "File \"" + filename + "\" empty on CIF read";
  }
  delete mmdb_manager;
#else
  throw PhaserError(INPUT,"Phaser is not configured for CIF input. Please recompile with MMDB v2.0.");
#endif
  return pdb_file_content;
}

xyz_weight Molecule::getCW(bool ignore_occupancy,int m) const
{
  xyz_weight cw;
  if (m < 0) m = mtrace;
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (pdb[m][a].AtomName != "UNK ")
  {
    xyzw this_xyzw;
    this_xyzw.xyz = pdb[m][a].X;
    this_xyzw.weight = 0;
    if (pdb[m][a].AtomName == std::string(DEF_MAP_ATMNAME))
    { //from maps
      this_xyzw.weight = 1;
    }
    else
    {
      try {
      cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
      this_xyzw.weight = cctbxAtom.weight();
      if (!ignore_occupancy) this_xyzw.weight *= pdb[m][a].O;
      }
      catch (std::exception const& err)  {}
    }
    if (this_xyzw.weight) //zero occupancy not returned
      cw.push_back(this_xyzw);
  }
  return cw;
}

std::string Molecule::one_atom_element() const
{
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
  if (pdb[mtrace][a].O > 0.0)
    return pdb[mtrace][a].Element;
  return "";
}

dvect3 Molecule::helix_axis(dmat33 R)
{
  if (!is_helix()) return dvect3(0,0,0);
 // dmat33 twofold(-1,0,0,0,1,0,0,0,-1);
 // R = twofold*R;
  af::shared< scitbx::vec3< floatType > > polyala,reference;
  int ca(0);
  //floatType max_rmsd(0.3); //Glykos test cast rmsd=0.52 #Ca=23
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
  {
    if (pdb[mtrace][a].AtomName == " CA ") //must be protein, not a general trace atom (dna)
    {
      reference.push_back(R*pdb[mtrace][a].X);
      //scitbx::vec3< floatType > N (-1.488, -0.324,  0.000);
      scitbx::vec3< floatType > CA(-1.729, -1.484,  0.869);
      //scitbx::vec3< floatType > C (-0.653, -1.558,  1.954);
      //scitbx::vec3< floatType > O (-0.955, -1.744,  3.143);
      //scitbx::vec3< floatType > CB(-1.695, -2.748,  0.062);
      dmat33 helixrot = euler2matrixDEG(dvect3(ca*100,0,0));
      dvect3 helixtra(0,0,1.501*ca);
      CA = helixrot*CA + helixtra;
      polyala.push_back(CA);
      ca++;
    }
  }
  //now superimpose the whole helix to get the overall axis
  scitbx::math::superpose::least_squares_fit<floatType> lsf(reference,polyala);
  //axis only valid if helix
  dvect3 helix_rotation = matrix2eulerDEG(lsf.get_rotation());
         helix_rotation[2] = 0; //remove z rotation
  dmat33 helix_matrix = euler2matrixDEG(helix_rotation);
  return scitbx::math::r3_rotation::axis_and_angle_from_matrix<floatType>(helix_matrix).axis;
}

float1D Molecule::allRms(std::string ESTIMATOR)
{
  int nRes(0);
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
  if ((pdb[mtrace][a].O > 0) &&
      (std::find(protein_trace.begin(),protein_trace.end(),pdb[mtrace][a].AtomName) != protein_trace.end() &&
       std::find(protein_resid.begin(),protein_resid.end(),pdb[mtrace][a].ResName) != protein_resid.end() ))
  {
    nRes++;
  }
  float1D all_rms(0);
  for (int m = 0; m < model_rmsid.size(); m++)
    model_rmsid[m].is_id() ? //flag
    all_rms.push_back(rms_estimate(ESTIMATOR).rms(model_rmsid[m].RMSID,nRes)):
    all_rms.push_back(model_rmsid[m].RMSID);
  return all_rms;
}

data_mw Molecule::getTotalMW(int m,std::string chain) const
{
  if (m < 0) m = mtrace;
  data_mw totalMW;
  map_str_float HYDRO_MW;
  cctbx::eltbx::tiny_pse::table hydrogen("H");
  floatType HydrogenWeight = hydrogen.weight();
  HYDRO_MW["ALA"]=5*HydrogenWeight;
  HYDRO_MW["ARG"]=13*HydrogenWeight;
  HYDRO_MW["ASN"]=6*HydrogenWeight;
  HYDRO_MW["ASP"]=4*HydrogenWeight;
  HYDRO_MW["CYS"]=5*HydrogenWeight;
  HYDRO_MW["GLN"]=8*HydrogenWeight;
  HYDRO_MW["GLU"]=6*HydrogenWeight;
  HYDRO_MW["GLY"]=3*HydrogenWeight;
  HYDRO_MW["HIS"]=8*HydrogenWeight;
  HYDRO_MW["ILE"]=11*HydrogenWeight;
  HYDRO_MW["LEU"]=11*HydrogenWeight;
  HYDRO_MW["LYS"]=13*HydrogenWeight;
  HYDRO_MW["MET"]=9*HydrogenWeight;
  HYDRO_MW["MSE"]=9*HydrogenWeight;
  HYDRO_MW["PHE"]=9*HydrogenWeight;
  HYDRO_MW["PRO"]=7*HydrogenWeight;
  HYDRO_MW["SER"]=5*HydrogenWeight;
  HYDRO_MW["THR"]=7*HydrogenWeight;
  HYDRO_MW["TRP"]=10*HydrogenWeight;
  HYDRO_MW["TYR"]=9*HydrogenWeight;
  HYDRO_MW["VAL"]=9*HydrogenWeight;
  HYDRO_MW["  A"]=11*HydrogenWeight; // Adenosine
  HYDRO_MW["  C"]=11*HydrogenWeight; // Cytidine
  HYDRO_MW["  G"]=11*HydrogenWeight; // Guanosine
  HYDRO_MW["  I"]= 9*HydrogenWeight; // Inosine
  HYDRO_MW["  T"]=12*HydrogenWeight; // Thymidine
  HYDRO_MW["  U"]=10*HydrogenWeight; // Uridine
  HYDRO_MW[" DA"]=11*HydrogenWeight; // Adenosine
  HYDRO_MW[" DC"]=11*HydrogenWeight; // Cytidine
  HYDRO_MW[" DG"]=11*HydrogenWeight; // Guanosine
  HYDRO_MW[" DI"]= 9*HydrogenWeight; // Inosine
  HYDRO_MW[" DT"]=12*HydrogenWeight; // Thymidine
  HYDRO_MW[" DU"]=10*HydrogenWeight; // Uridine

  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (!chain.size() || (m == mtrace && pdb[mtrace][a].Chain == chain))
  if (pdb[m][a].O > 0.0 && pdb[m][a].AtomName != "UNK ")
  {
    PHASER_ASSERT(pdb[m][a].AtomName != std::string(DEF_MAP_ATMNAME));
    double occ = pdb[m][a].O;
    floatType weight(0);
    try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
    weight = cctbxAtom.weight();
    }
    catch (std::exception const& err)  {}
//gives the same molecular weight as pdbset
//ignore the hydrogens on the file
    if (std::find(protein_resid.begin(),protein_resid.end(),pdb[m][a].ResName) != protein_resid.end())
    {
      if (pdb[m][a].Element != "H")
        totalMW.PROTEIN += weight*occ;
      if (pdb[m][a].AtomName == " CA ")
        totalMW.PROTEIN += HYDRO_MW[pdb[m][a].ResName]*occ;
    }
    else if (std::find(nucleic_resid.begin(),nucleic_resid.end(),pdb[m][a].ResName) != nucleic_resid.end())
    {
      if (pdb[m][a].Element != "H")
        totalMW.NUCLEIC += weight*occ;
      if (pdb[m][a].AtomName == " P  ")
        totalMW.NUCLEIC += HYDRO_MW[pdb[m][a].ResName]*occ;
    }
    else
    {
      totalMW.HETATOM += weight*occ;
    }
  }
  return totalMW;
}

floatType Molecule::getScat(int m) const
{
  // HYDROGEN SCATTERING NOT ADDED
  if (m < 0) m = mtrace;
  floatType totalScat(0);
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (pdb[m][a].O > 0.0 && //never ignore occupancy set on input
      pdb[m][a].AtomName != "UNK ")
  {
    floatType atomic_number(0);
    try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
    atomic_number = cctbxAtom.atomic_number();
    }
    catch (std::exception const& err)  {}
    totalScat += scitbx::fn::pow2(atomic_number)*pdb[m][a].O;
  }
  return totalScat;
}

floatType Molecule::getScat(float1D* OCC) const
{ // only one model
  // HYDROGEN SCATTERING NOT ADDED
  int m = mtrace;
  if (OCC != NULL) PHASER_ASSERT(pdb[m].size() == OCC->size());
  floatType totalScat(0);
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (pdb[m][a].O > 0.0 && //never ignore occupancy set on input
      pdb[m][a].AtomName != "UNK ")
  {
    floatType atomic_number(0);
    try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
    atomic_number = cctbxAtom.atomic_number();
    }
    catch (std::exception const& err)  {}
    double occ = (OCC != NULL) ? (*OCC)[a] : pdb[m][a].O;
    totalScat += scitbx::fn::pow2(atomic_number)*occ;
  }
  return totalScat;
}

floatType Molecule::getScat(bool1D* OCC) const
{
  // HYDROGEN SCATTERING NOT ADDED
  int m = mtrace;
  if (OCC != NULL) PHASER_ASSERT(pdb[m].size() == OCC->size());
  floatType totalScat(0);
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (pdb[m][a].O > 0.0 && //never ignore occupancy set on input
      pdb[m][a].AtomName != "UNK ")
  {
    floatType atomic_number(0);
    try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
    atomic_number = cctbxAtom.atomic_number();
    }
    catch (std::exception const& err)  {}
    double occ = (OCC != NULL) ? double((*OCC)[a]) : pdb[m][a].O;
    totalScat += scitbx::fn::pow2(atomic_number)*occ;
  }
  return totalScat;
}

floatType Molecule::getScat(std::string chain) const
{
  // HYDROGEN SCATTERING NOT ADDED
  int m = mtrace; //always mtrace for chain selection
  floatType totalScat(0);
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (pdb[m][a].Chain == chain &&
      pdb[m][a].O > 0.0 && //never ignore occupancy set on input
      pdb[m][a].AtomName != "UNK ")
  {
    floatType atomic_number(0);
    try {
    cctbx::eltbx::tiny_pse::table cctbxAtom(pdb[m][a].Element);
    atomic_number = cctbxAtom.atomic_number();
    }
    catch (std::exception const& err)  {}
    totalScat += scitbx::fn::pow2(atomic_number)*pdb[m][a].O;
  }
  return totalScat;
}

int1D Molecule::trace_to_atom_list(int nt) const
{
  int ResNumTrace(0); //not length of is_trace array
  int ca(0);
  //the trace atom may not be the first atom in the residue
  //find the resnum of the residue
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
  if (isTraceAtom(mtrace,a))
  {
    if (ca == nt)
    {
      ResNumTrace = pdb[mtrace][a].ResNum;
      break;
    }
    ca++;
  }
  int1D atom_list(0);
  ca = 0; //trace atom
  for (int a = 0; a < pdb[mtrace].size(); a++)
  {
    if ((ca == nt-1 || ca == nt || ca == nt+1) && pdb[mtrace][a].ResNum == ResNumTrace)
      atom_list.push_back(a);
    //if nt has been found, break
    if (ca > nt+1) break;
    if (isTraceAtom(mtrace,a))
      ca++;
  }
  return atom_list;
}

void Molecule::atomicHessian(floatType NMA_RADIUS,floatType NMA_FORCE,TNT::Fortran_Matrix<float>& HessianA,Output& output) const
{
  floatType cutoffDistSqr(fn::pow2(NMA_RADIUS));
  const int mzero(0);
  int natom = nAtoms();

  if (!natom) throw PhaserError(FATAL,"No atoms for interaction matrix");
  else output.logTab(1,LOGFILE,itos(natom) +" atoms for interaction matrix");
  HessianA.newsize(natom*3,natom*3);
  for (int i = 1; i <= HessianA.num_rows(); i++)
    for (int j = 1; j <= HessianA.num_cols(); j++)
       HessianA(i,j) = 0;

  int ha(1); //for Fortran_Matrix
  for (unsigned a = 0; a < pdb[mzero].size(); a++)
  {
    int hb(ha+3);
    for (unsigned b = a+1; b < pdb[mzero].size(); b++)
    {
      dvect3 vecDiff(card(mzero,a).X-card(mzero,b).X);
      floatType distSqr(vecDiff*vecDiff);
      if (distSqr <= cutoffDistSqr && distSqr > 0)
      {
        for (int i = 0; i < 3; i++)
          for (int j = i; j < 3; j++)
          {
            floatType Hessij(vecDiff[i]*vecDiff[j]*NMA_FORCE/distSqr);
            if (i == j) Hessij /= 2.0;
            HessianA(ha+i,ha+j) += Hessij;
            HessianA(ha+i,hb+j) -= Hessij;
            HessianA(ha+j,ha+i) += Hessij;
            HessianA(ha+j,hb+i) -= Hessij;
            HessianA(hb+i,hb+j) += Hessij;
            HessianA(hb+j,hb+i) += Hessij;
            HessianA(hb+i,ha+j) -= Hessij;
            HessianA(hb+j,ha+i) -= Hessij;
          }
      }
      hb+=3;
    }
    ha+=3;
  }

  { //memory
  output.logTab(1,DEBUG,"Hessian Matrix: " + itos(HessianA.num_rows()) + " x "+itos(HessianA.num_cols()));
  int printSizeI(std::min(50,HessianA.num_rows())),printSizeJ(std::min(20,HessianA.num_cols()));
  output.logTabPrintf(0,DEBUG,"  %4s  "," ");
  for (int j = 1; j <= printSizeJ; j++)
    output.logTabPrintf(0,DEBUG,"(%4i) ",j);
  output.logBlank(DEBUG);
  for (int i = 1; i <= printSizeI; i++)
  {
    output.logTabPrintf(0,DEBUG,"(%4i) ",i);
    for (int j = 1; j <= printSizeJ; j++)
      HessianA(i,j) ? output.logTabPrintf(0,DEBUG,"%6.1f ",HessianA(i,j)) :
                      output.logTabPrintf(0,DEBUG,"%6s ","  ----");
    output.logBlank(DEBUG);
  }
  output.logBlank(DEBUG);
  } //memory

  output.logTab(1,LOGFILE,"Atomic Matrix Statistics:");
  output.logTab(2,LOGFILE,"Matrix: " + itos(HessianA.num_rows()) + " x "+itos(HessianA.num_cols()));
  output.logBlank(LOGFILE);
}

af_atom Molecule::getAtomArrays()
{
  af_atom atom_array;
  data_atom new_atom;
  new_atom.ORIG = true;
  new_atom.SCAT.label = "";
  for (unsigned m = 0; m < pdb.size(); m++)
  for (unsigned a = 0; a < pdb[m].size(); a++)
  {
    new_atom.FRAC = false;
    new_atom.SCAT.scattering_type = stoup(pdb[m][a].Element);
    new_atom.SCAT.site = card(m,a).X;
    new_atom.SCAT.occupancy = card(m,a).O;
    if (!pdb[m][a].FlagAnisoU)
    {
      new_atom.SCAT.u_iso = cctbx::adptbx::b_as_u(pdb[m][a].B);
      new_atom.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
    }
    else
    {
      floatType iso_u = cctbx::adptbx::b_as_u(pdb[m][a].B);
      floatType ano_u = cctbx::adptbx::u_cart_as_u_iso(pdb[m][a].AnisoU/u_cart_pdb_factor);
      floatType tol(cctbx::adptbx::b_as_u(1.0e-02));
      //if the isotropic component of AnisoU is zero and not equal to the B, add together
      if (std::fabs(ano_u) < tol && std::fabs(iso_u) > tol)  //not standard pdb format
        new_atom.SCAT.u_star = cctbx::adptbx::u_iso_as_u_cart(iso_u)*u_cart_pdb_factor + pdb[m][a].AnisoU;
      else //standard pdb format,  or something we can't recognise
        new_atom.SCAT.u_star = pdb[m][a].AnisoU;
      new_atom.SCAT.set_use_u(/* iso */ false, /* aniso */ true);
      new_atom.USTAR = false; //not the default, cartesian
    }
   atom_array.push_back(new_atom);
 }
 return atom_array;
}

stringset Molecule::get_overlap_chains(mr_ndim& NDIM,SpaceGroup& sg, UnitCell& uc,int mx)
{
  stringset chains;

  int m = mx < 0 ? mtrace : mx;
  float1D original_occupancies(pdb[m].size());
  for (unsigned a = 0; a < pdb[m].size(); a++)
  {
    if (pdb[m][a].O > 0.0) chains.insert(pdb[m][a].Chain);
    original_occupancies[a] = pdb[m][a].O;
  }

  string1D chain1D; //convert from std::set to std::vector for looping, need c+1 index
  for (stringset::iterator iter = chains.begin(); iter != chains.end(); iter++)
    chain1D.push_back(*iter);

  dvect31D orthCentre_chain(chains.size());
  for (unsigned c = 0; c < chain1D.size(); c++)
  {
    for (unsigned a = 0; a < pdb[m].size(); a++)
      if (pdb[m][a].Chain != chain1D[c]) pdb[m][a].O = 0;
    bool ignore_occupancy(false);
    xyz_weight cw = getCW(ignore_occupancy,m);
    for (unsigned a = 0; a < pdb[m].size(); a++)
      pdb[m][a].O = original_occupancies[a];
    cw.calculate();
    orthCentre_chain[c] = NDIM.R*cw.CENTRE+uc.doFrac2Orth(NDIM.TRA);
    dvect3 fracCentre_chain = uc.doOrth2Frac(orthCentre_chain[c]);
    for (int i = 0; i < 3; i++)
    {
      while (fracCentre_chain[i] < 0) fracCentre_chain[i]++;
      while (fracCentre_chain[i] >= 1) fracCentre_chain[i]--;
    } // move close to origin for testing
    orthCentre_chain[c] = uc.doFrac2Orth(fracCentre_chain);
  }

  floatType DIST = fn::pow2(DEF_PACK_CONS_DIST);
  stringset DELCHAINS;
  for (unsigned isym = 1; isym < sg.NSYMM; isym++)
  {
    dvect3 cellTrans(0,0,0),cellTrans_min(-2,-2,-2),cellTrans_max(2,2,2);
    for (cellTrans[0] = cellTrans_min[0]; cellTrans[0] <= cellTrans_max[0]; cellTrans[0]++)
    {
      for (cellTrans[1] = cellTrans_min[1]; cellTrans[1] <= cellTrans_max[1]; cellTrans[1]++)
      {
        for (cellTrans[2] = cellTrans_min[2]; cellTrans[2] <= cellTrans_max[2]; cellTrans[2]++)
        {
          stringset this_overlapped_chains;
          for (int c = 0; c < chain1D.size(); c++)
          {
            for (int d = c+1; d < chain1D.size(); d++)
            {
              dvect3 orth_ref = orthCentre_chain[c];
              dvect3 orth_chk = orthCentre_chain[d];
              dvect3 frac_chk = uc.doOrth2Frac(orth_chk);
              dvect3 frac_chk_sym = sg.doSymXYZ(isym,frac_chk);
              dvect3 frac_chk_sym_cell = frac_chk_sym + cellTrans;
              dvect3 orth_chk_sym_cell = uc.doFrac2Orth(frac_chk_sym_cell);
              floatType delta = (orth_chk_sym_cell-orth_ref)*(orth_chk_sym_cell-orth_ref);
              if (delta < DIST)
              {
                this_overlapped_chains.insert(chain1D[d]);
              }
            }
          }
          if (this_overlapped_chains.size() > DELCHAINS.size())
             { DELCHAINS = this_overlapped_chains; }
        } //cell
      } //cell
    } //cell
  }//sym
  return DELCHAINS;
}

string1D Molecule::getChains(bool chainocc) const
{
  string1D chains;
  for (unsigned m = 0; m < pdb.size(); m++)
  for (unsigned a = 0; a < pdb[m].size(); a++)
  if (!chainocc || (chainocc && pdb[m][a].O))
  if (std::find(chains.begin(),chains.end(),pdb[m][a].Chain) == chains.end())
  chains.push_back(pdb[m][a].Chain);
  return chains;
}

int Molecule::nAtoms(int m) const
{
  if (!pdb.size()) return 0;
  if (m < 0) return pdb[mtrace].size();
  PHASER_ASSERT(m < pdb.size());
  return pdb[m].size();
}

stringset Molecule::get_chainIDs() const
{
  stringset chains;
  for (unsigned a = 0; a < pdb[mtrace].size(); a++)
    chains.insert(pdb[mtrace][a].Chain);
  return chains;
}

stringset Molecule::getAnomTypes() const
{
  stringset atomtypes;
  for (unsigned m = 0; m < pdb.size(); m++)
  for (unsigned a = 0; a < pdb[m].size(); a++)
  {
    if (card(m,a).Element != "C")
    if (card(m,a).Element != "N")
    if (card(m,a).Element != "O")
    if (card(m,a).Element != "H")
    atomtypes.insert(card(m,a).Element);
  }
  return atomtypes;
}

void Molecule::erase(int start,int natom_in_block)
{
  pdb[0].erase( //only one model allowed
              pdb[0].begin()+start,
              pdb[0].begin()+start+natom_in_block);
}

bool Molecule::isTraceAtom(int m,int a) const
{
  return
   (std::find(protein_trace.begin(),protein_trace.end(),pdb[m][a].AtomName) != protein_trace.end() &&
    std::find(protein_resid.begin(),protein_resid.end(),pdb[m][a].ResName) != protein_resid.end() )
   ||
   (std::find(nucleic_trace.begin(),nucleic_trace.end(),pdb[m][a].AtomName) != nucleic_trace.end() &&
    std::find(nucleic_resid.begin(),nucleic_resid.end(),pdb[m][a].ResName) != nucleic_resid.end() );
}

bool Molecule::isBackboneAtom(int m,int a) const
{
  bool bb(false); //must be protein
  //this is used for trimming to polyalanine, protein only
  if (std::find(protein_resid.begin(),protein_resid.end(),pdb[m][a].ResName) != protein_resid.end() )
    bb = (std::find(protein_backbone.begin(),protein_backbone.end(),pdb[m][a].AtomName) != protein_backbone.end());
  return bb; //must be protein
}

bool Molecule::set_occupancy(bool1D* OCC)
{
  if (pdb.size() != 1) return true;
  if (pdb[0].size() != OCC->size()) return true;
  for (unsigned a = 0; a < pdb[0].size(); a++)
  if (pdb[0][a].O) //ignore no occupancy on input
    pdb[0][a].O = (*OCC)[a] ? 1 : 0;
  return false;
}

}//phaser
