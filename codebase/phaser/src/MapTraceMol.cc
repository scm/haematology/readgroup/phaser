//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/MapTraceMol.h>

namespace phaser {

bool MapTraceMol::configure(stringset currentEns,bool map_only,bool occupancy_warning,map_str_pdb& PDB,Output& output)
{
  bool changed_configuration(false);
  if (!currentEns.size()) return changed_configuration;
  for (trcIter iter = tracemol.begin(); iter != tracemol.end(); iter++)
   // if ((currentEns.find(iter->first) == currentEns.end()))
    {
      output.logTab(1,TESTING,"Currently configured with " + iter->first);
   //   tracemol.erase(tracemol.find(iter->first));
    }

  //initialize tracemol
  for (stringset::iterator iter = currentEns.begin(); iter != currentEns.end(); iter++)
  {
    std::string modlid = (*iter);
    output.logTab(1,TESTING,"Configure " + modlid);
    PHASER_ASSERT(PDB.find(modlid) != PDB.end());
    if (tracemol.find(modlid) == tracemol.end())
         // ??? why was this here? || tracemol[modlid].SAMP_USED != PDB[modlid].TRACE.SAMP_USE)
    {
      if (PDB[modlid].ENSEMBLE[0].map_format()) //always
      {
        output.logUnderLine(LOGFILE,"Trace Generation: " + modlid);
        output.logTab(2,LOGFILE, "This trace is from a map");
        tracemol[modlid].setMAP(PDB[modlid],output);
        changed_configuration = true;
      }
      else if (!map_only ||
               (occupancy_warning && PDB[modlid].TRACE.SAMP_USE == "HEXGRID") //change to c-alpha
              )
      {
        output.logUnderLine(LOGFILE,"Trace Generation: " + modlid);
        output.logTab(2,LOGFILE, "This trace is from coordinates");
        tracemol[modlid].setPDB(occupancy_warning,PDB[modlid],output);
        changed_configuration = true;
      }
      //don't call tracemol[modlid] here as it inits
    }
  }
  // all the trace molecules have the same sampling
  // unless the CLASSIC mode is run, where the sampling can be anything
  // use backwards compatible code
  // which simply takes the biggest distance to generate the clash distance
  return changed_configuration;
}

void MapTraceMol::configure(mr_set& MRSET)
{
  for (unsigned s = 0; s < MRSET.KNOWN.size(); s++)
  {
    std::string modlid = MRSET.KNOWN[s].MODLID;
    PHASER_ASSERT(MRSET.BOOLOCC.size());
    tracemol[modlid].setOCC(&MRSET.BOOLOCC[s]);
  }
}

void MapTraceMol::trace_table(outStream where,Output& output)
{
  if (!tracemol.size()) return;
  output.logUnderLine(LOGFILE,"Trace Generation");
  int len = 15;
  output.logTabPrintf(1,LOGFILE,"%-*s %-11s %6s %13s %13s\n",len,"Ensemble","Trace-type","Length","Sampling","Close-contact");
  for (trcIter iter = tracemol.begin(); iter != tracemol.end(); iter++)
  {
    output.logTabPrintf(1,where,"%-*s %-11s %6d %13.2f %13.2f\n",len,
           iter->first.substr(0, len).c_str(),
           iter->second.TYPE.c_str(),
           iter->second.size(),
           iter->second.sampling_distance(),
           iter->second.close_contact_distance()
           );
  }
  output.logBlank(LOGFILE);
}

}//phaser
