//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SpaceGroupClass__
#define __SpaceGroupClass__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>
#include <phaser/include/space_group_name.h>

namespace phaser {

class Brick;
class UnitCell;

class SpaceGroup
{
  public:
    SpaceGroup(std::string);
    SpaceGroup(cctbx::sgtbx::space_group const&);
    SpaceGroup() { spcgrpnum = NSYMM = NSYMP = SYMFAC = 0; }
    virtual ~SpaceGroup(){}

    void setSpaceGroup(const SpaceGroup & init)
    {
      rotsym = init.rotsym;
      trasym = init.trasym;
      cctbxSG = cctbx::sgtbx::space_group(init.cctbxSG);
      spcgrpnum = init.spcgrpnum;
      NSYMM = init.NSYMM;
      NSYMP = init.NSYMP;
      SYMFAC = init.SYMFAC;
    }

    SpaceGroup(const SpaceGroup & init)
    { setSpaceGroup(init); }

    const SpaceGroup& operator=(const SpaceGroup& right)
    {
      if (&right != this) setSpaceGroup(right);
      return *this;
    }

    const SpaceGroup& getSpaceGroup() const
    { return *this; }

  private:
    cctbx::sgtbx::space_group cctbxSG;
    unsigned spcgrpnum;
  public:
    std::vector<dmat33> rotsym;
    std::vector<dvect3> trasym;
    unsigned NSYMM,NSYMP,SYMFAC;

  public:
    std::string spcgrpname();
    std::string pgname();
    int         spcgrpnumber();
    char        spcgrpcentring();

    bool      alwaysTrue(dvect3 x) { return true; }
    dmat33&   Rotsym(const int& isym) { return rotsym[isym]; }
    dvect3&   Trasym(const int& isym) { return trasym[isym]; }
    dvect3    doSymXYZ(const int&, const dvect3&); //Apply ith Symmetry to vector
    cctbx::sgtbx::space_group& getCctbxSG() { return cctbxSG; }
    const cctbx::sgtbx::space_group& getCctbxSG() const { return cctbxSG; }

    void   setSpaceGroupHall(std::string);
    Brick  brickCheshire(UnitCell);
    Brick  brickPrimitive(UnitCell);
    bool   inCheshire(dvect3);
    bool   inPrimitive(dvect3);
    int    orderX();        //order of symmetry about x-axis
    int    orderY();        //order of symmetry about y-axis
    int    orderZ();        //order of symmetry about z-axis
    int    highOrderAxis(); //highest symmetry axis

    void logSymm(outStream,Output&);
    void logSymmExtra(outStream,Output&);

    std::string  getHall() const;
    std::string  getSpaceGroupHall() const { return getHall(); }
    std::string  getSpaceGroupName() { return spcgrpname(); }
    int          getSpaceGroupNum() { return spcgrpnumber(); }
    int          getSpaceGroupNSYMM() { return NSYMM; }
    int          getSpaceGroupNSYMP() { return NSYMP; }
    floatType    getSpaceGroupR(int s,int i,int j) { return (s < NSYMM && i < 3 && j < 3) ? rotsym[s](i,j) : 0; }
    floatType    getSpaceGroupT(int s,int i) { return (s < NSYMM && i < 3) ? trasym[s][i] : 0; }

    bool         is_centric(const miller::index<int>& miller) { return cctbxSG.is_centric(miller); }
    int          epsilon(const miller::index<int>& miller)    { return cctbxSG.epsilon(miller); }
    bool         is_sys_absent(const miller::index<int>& miller) { return cctbxSG.is_sys_absent(miller); }
    bool         is_polar();
};

} //phaser

#endif
