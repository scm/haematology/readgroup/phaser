//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EnsembleClass__
#define __EnsembleClass__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/include/data_solpar.h>
#include <phaser/include/data_bofac.h>
#include <phaser/include/data_tncs.h>
#include <phaser/include/data_formfactors.h>
#include <phaser/lib/mean.h>
#include <phaser/pod/MillerDeepCopy.h>
#include <phaser/pod/EVS.h>
#include <phaser/pod/ssqrclass.h>
#include <phaser/lib/solTerm.h>
#include <phaser/src/Bin.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

typedef float mapFloat;

class SigmaN;

class Ensemble : public UnitCell, public MillerDeepCopy, public data_pdb
{
  public:
    //the parameters at the end of the first constructor are decomposition flags - by default no decomposition
    Ensemble& setPDB(floatType,data_solpar&,data_formfactors&,floatType,SigmaN,Output&,bool=false,floatType=0,float1D* OCC=NULL);
    Ensemble& setMAP(floatType,data_solpar&,Output&);
    Ensemble() : UnitCell(),MillerDeepCopy(),data_pdb()
    {
      pInterpE = &Ensemble::FourPtInterpE;
      pInterpV = &Ensemble::OnePtInterpV;
      pdInterpE = &Ensemble::dFourPtInterpE;
      NREFL = sizeh = sizek = sizel = 0;
      binV.clear(); mapE.clear(); mapV.clear(); ScatRat.clear();
      ENS_FROM_PDB  =  true;
      NEWALD = 0;
      ENS_BIN.set_default_ensembles();
      nearH = nearK = nearL = floorH = floorK = floorL = 0;
      fracH = fracK = fracL = 0;
      buf_cmplxType = 0;
      buf_floatType = 0;
      buf_int = 0;
      midres = ssqrclass(0,0);
      binned.clear();
      relative_wilsonB.clear();
    }

  private:
    int       NREFL,sizeh,sizek,sizel;
    int1D     binV;
    std::vector<std::complex<mapFloat> > mapE;
    std::vector<mapFloat> mapV;
    std::vector<mapFloat> ScatRat;
    //for recalculation
    bool      ENS_FROM_PDB;
    UnitCell  input_CELL;
    int       NEWALD;
    //af::shared<miller::index<int> > miller; is in MillerDeepCopy class
    Bin       ENS_BIN;
    //for speed
    int       nearH,nearK,nearL,floorH,floorK,floorL;
    floatType fracH,fracK,fracL;
    cmplxType buf_cmplxType;
    floatType buf_floatType;
    int       buf_int;
    ssqrclass midres;
    std::vector<EVS> binned;

  public:
    float2D   relative_wilsonB;

  private:
    cmplxType (Ensemble::*pdInterpE)(dvect3&,cvect3&,bool&,cmat33&);
    cmplxType (Ensemble::*pInterpE)(dvect3&);
    floatType (Ensemble::*pInterpV)();

  public:
    void      empty()    { mapE.resize(0); mapV.resize(0); ScatRat.resize(0); binV.resize(0); }
    bool      from_pdb() { return ENS_FROM_PDB; }
    void      set_sqrt_ssqr(floatType&,floatType&);

  private:

    floatType   OneAtomV();
    floatType   OnePtInterpV();

    cmplxType   OneAtomE(dvect3&);
    cmplxType   OnePtInterpE(dvect3&);
    cmplxType   FourPtInterpE(dvect3&);
    cmplxType   EightPtInterpE(dvect3&);

    cmplxType   dOneAtomE(dvect3&,cvect3&,bool&,cmat33&);
    cmplxType   dOnePtInterpE(dvect3&,cvect3&,bool&,cmat33&);
    cmplxType   dFourPtInterpE(dvect3&,cvect3&,bool&,cmat33&);
    cmplxType   dEightPtInterpE(dvect3&,cvect3&,bool&,cmat33&);

    floatType&  getV();
    void        setV(int,floatType);
    void        setE(int,floatType);
    void        setScatRat(int,floatType);
    ivect3      getHKL(int,int,int);

  public:
    cmplxType&  getE(int,int,int);
    void        setE(int,int,int,cmplxType);
    void        resizeMaps(int,int,int);

  public:
    floatType   dvrms_delta_const();
    floatType   Eterm_const();
    cmplxType   InterpE(dvect3,floatType);
    floatType   InterpV(floatType);
    floatType   AtomScatRatio();
    floatType   maxAtomScatRatio();
    void        setPtInterpEV(int);
    void        setup_vrms(floatType);
    void        setup_cell(floatType);
    void        set_relative_wilsonB(SigmaN&,std::vector<af_cmplx>&,Output&);

    cmplxType   func_grad_hess_InterpE(dvect3,floatType,cvect3&,bool&,cmat33&);

    void        writeMtz(std::string,Output&);
    bool        flatMap();
    void        resizeMaps();
    void        resizeMapsAtom();
    void        logMemory(outStream,Output&);

    cmplx3D     getELMNxR2(std::string,floatType,Output&,int,data_bofac,double);

    bool   is_empty() { return !mapE.size(); }
};

}//phaser
#endif
