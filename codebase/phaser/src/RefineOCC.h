//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineOCCClass__
#define __RefineOCCClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolOCC.h>
#include <phaser/include/data_formfactors.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/include/data_occupancy.h>
#include <phaser/src/DataB.h>
#include <phaser/src/RefineBase2.h>
#include <cctbx/math/cos_sin_table.h>
#include <phaser/lib/van_der_waals_radii.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <cctbx/sgtbx/site_symmetry_table.h>
#include <cctbx/xray/scatterer.h>
#include <cctbx/xray/scattering_type_registry.h>
#include <scitbx/fftpack/complex_to_complex_3d.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <phaser/pod/constraint_info.h>

namespace phaser {

class OccDeepCopy
{
  public:
    std::vector<af::shared<xray::scatterer<double> > > xray_scatterers;
  public:
    OccDeepCopy() {}
    void setOccDeepCopy(const OccDeepCopy & init)
    { //array over known
      for (int k = 0; k < xray_scatterers.size(); k++)
        xray_scatterers[k] = init.xray_scatterers[k].deep_copy();
    }
    OccDeepCopy(const OccDeepCopy & init)
    { setOccDeepCopy(init); }
    const OccDeepCopy& operator=(const OccDeepCopy& right)
    { if (&right != this) setOccDeepCopy(right); return *this; }
};

enum tgh { tgh_target,tgh_gradient,tgh_hessian };

class RefineOCC : public RefineBase2, public DataB, public OccDeepCopy
{
  private:
    data_occupancy OCCUPANCY;
    data_formfactors FORMFACTOR;
    map_str_pdb*   PDB;
    bool           do_fft_not_sum;
    double         fftHIRES,u_base,TotalScat;
    data_solpar    SOLPAR;
    vdw            atoms_vdw_table;
    Alogch         m_alogch;
    AlogchI0       m_alogchI0;
    int            OFFSET;
    int            nwindows;

    scitbx::fftpack::complex_to_complex_3d<double> cfft;
    std::vector<cctbx::sgtbx::site_symmetry_table> site_sym_table;
    std::vector<cctbx::xray::scattering_type_registry> scattering_type_registry;

    std::vector<std::vector<std::pair<int1D,constraint_info> > > constraint_list;

    //index ona[s][r]
    float2D      totvar_known,terms_on_sqrt_epsnSigmaP;
    cmplx2D      Ecalc;

    //index on [r]
    af::shared<miller::index<int> >    selected_to_bin_end_miller;
    af_bool      selected_to_bin_end;

    //index on [s]
    mr_set  MRSET;
    float1D fracScat;
    float1D model_drms;

    //index on [a]
    float2D      dLL_by_dPar,d2LL_by_dPar2;

  public:
    RefineOCC()
    {
      do_fft_not_sum = true;
      TotalScat = fftHIRES = u_base = OFFSET = 0;
      PDB = NULL;
    }
    RefineOCC(data_occupancy&,data_solpar&,data_formfactors&,map_str_pdb*,DataB&,Output&);
    ~RefineOCC() {}

   //concrete member functions
    double targetFn();
    double gradientFn(TNT::Vector<double>&);
    double hessianFn(TNT::Fortran_Matrix<double>&,bool&);
    void applyShift(TNT::Vector<double>&);
    void logCurrent(outStream,Output&);
    void logStats(outStream,Output&);
    void logHistogram(outStream,Output&);
    void logInit(outStream,Output&);
    void logInitial(outStream,Output&);
    void logFinal(outStream,Output&);
    void setProtocol(protocolPtr,Output&);
    void setUp();
    std::vector<reparams> getRepar();
    std::vector<bounds> getUpperBounds();
    std::vector<bounds> getLowerBounds();
    TNT::Vector<double> getRefinePars();
    TNT::Vector<double> getLargeShifts();
    bool1D  getRefineMask(protocolPtr);
    std::string whatAmI(int&);

  public:
    double Rice_refl(int);
    double likelihoodFn();
    void   initKnown(double,mr_set&);
    void   initKnownOld(mr_set&);
    void   setup_constraints(int);
    int    number_of_windows() { return nwindows; }
    void   setOcc(float2D&,bool);
    float2D getOcc();
    void   mergeOcc(float3D,int,bool);
    void   configureOcc(float1D);
    bool1D getBoolOcc(int);
    std::vector<mr_ndim>  getKnown() { return MRSET.KNOWN; }

  private:
    void calcAtomic(bool=false);
    void calcAtomicFFT(bool=false);
    void calcAtomicSum(bool=false);
    double calcGradSum(bool);
    double calcGradFFT(bool);
    std::complex<double> thisFcalc(int,int);
    void  studyParams(Output&);
};

} //phaser

#endif
