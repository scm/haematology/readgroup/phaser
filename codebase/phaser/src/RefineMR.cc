//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineMR.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/between.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <scitbx/math/bessel.h>

#define AVERAGE_GRADIENT_HESSIAN

namespace phaser {

RefineMR::RefineMR(data_restraint& BFAC_REFI_,
                   data_restraint& DRMS_REFI_,
                   DataMR& data,Output& output) : RefineBase2(), DataMR(data)
{
  BFAC_REFI = BFAC_REFI_;
  DRMS_REFI = DRMS_REFI_;
  use_rotref_restraint = sigma_rotref = 0;
  use_traref_restraint = sigma_traref = 0;
}

floatType RefineMR::targetFn()
{
// returns the -log(likelihood) of the current setup
  floatType totalLL = Target();
  floatType f = -(totalLL - LLwilson);
            f += restraint_term();
  return f;
}

floatType RefineMR::restraint_term()
{
  int m(0),i(0);
  floatType restraints(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      if (use_rotref_restraint)
      {
        restraints += fn::pow2(models_perturbRot[s][rot]/sigma_rotref)/2.;
      }
      i++;
    }
    for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++])
    {
      if (use_traref_restraint)
      {
        restraints += fn::pow2(models_perturbTrans[s][tra]/sigma_traref)/2.;
      }
      i++;
    }
    if (refinePar[m++])
    {
      if (BFAC_REFI.RESTRAINT)
      {
        restraints += fn::pow2(models_known[s].BFAC/BFAC_REFI.SIGMA)/2.;
      }
      i++;//BFAC
    }
    if (refinePar[m++]) i++;//OFAC
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++])
    {
      if (DRMS_REFI.RESTRAINT)
      {
        restraints += fn::pow2(DRMS_REFI.SIGMA)/2.;
      }
      i++;//BFAC
    }
    if (refinePar[m++]) i++; //CELL
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  //localMLL += restraints;
  return restraints;
}

floatType RefineMR::gradientFn(TNT::Vector<floatType>& Gradient)
{
//#define PHASER_TIMINGS
#ifdef PHASER_TIMINGS
  static int countr(0);
  Output output;
  output.logTab(1,LOGFILE,"Gradient Start # " + itos(countr++));
  floatType start_clock = std::clock();
#endif
  //initialization
  Gradient.newsize(npars_ref);
  bool return_gradient(true);
  floatType localMLL = gradient_hessian(Gradient,return_gradient); //-(totalLL - LLwilson) + restraint

  int m(0),i(0);
  int1D Bindex(models_known.size());
  int1D Oindex(models_known.size());
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
    if (refinePar[m++])
    {
      if (use_rotref_restraint)
      {
        Gradient[i] += models_perturbRot[s][rot]/sigma_rotref;
      }
      i++;
    }
    for (unsigned tra = 0; tra < 3; tra++)
    if (refinePar[m++])
    {
      if (use_traref_restraint)
      {
        Gradient[i] += models_perturbTrans[s][tra]/sigma_traref;
      }
      i++;
    }
    if (refinePar[m++])
    {
      Bindex[s] = i;
      if (BFAC_REFI.RESTRAINT)
      {
        Gradient[i] += models_known[s].BFAC/fn::pow2(BFAC_REFI.SIGMA);
      }
      i++; //BFAC
    }
    if (refinePar[m++])
    {
      Oindex[s] = i;
      i++; //OFAC
    }
  }
  int1D icell(0);
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++])
    {
      if (DRMS_REFI.RESTRAINT)
      {
        Gradient[i] += models_drms[iter->first]/fn::pow2(DRMS_REFI.SIGMA);
      }
      i++; //VRMS
    }
    if (refinePar[m++]) { icell.push_back(i); i++; } //CELL
  }
  if (getenv("PHASER_STUDY_PARAMS") == NULL) //constrain gradients in the case of pdb files as models
  {
  if (refine_datacell()) //and therefore need to do cell restraint, because it is for data only
  {
    float1D grads(0);
    for (int c = 0; c < icell.size(); c++)
      grads.push_back(Gradient[icell[c]]);
    double sum_grads = sum(grads);
           sum_grads /= icell.size();
    for (int c = 0; c < icell.size(); c++)
      Gradient[icell[c]] = sum_grads/icell.size();
  }
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

#ifdef AVERAGE_GRADIENT_HESSIAN
  //average Gradient
  if (PTNCS.use_and_present())
  {
    float1D avBfac(Bindex.size(),0);
    int n(-1);
    for (unsigned s = 0; s < Bindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      avBfac[n] += Gradient[Bindex[s]]/PTNCS.NMOL;
    }
    n = -1;
    for (unsigned s = 0; s < Bindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      Gradient[Bindex[s]] = avBfac[n];
    }
    float1D avOfac(Oindex.size(),0);
    n = -1;
    for (unsigned s = 0; s < Oindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      avOfac[n] += Gradient[Oindex[s]]/PTNCS.NMOL;
    }
    n = -1;
    for (unsigned s = 0; s < Oindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      Gradient[Oindex[s]] = avOfac[n];
    }
  }
#endif

#ifdef PHASER_TIMINGS
  std::clock_t now_clock = std::clock();
  floatType time_for_sum = (now_clock-start_clock)/double(CLOCKS_PER_SEC);
  output.logTab(1,LOGFILE,"Gradient Time = " + dtos(time_for_sum) + " secs");
#endif
  return localMLL;
}

floatType RefineMR::hessianFn(TNT::Fortran_Matrix<floatType>& Hessian,bool& is_diagonal)
{
#ifdef PHASER_TIMINGS
  static int countr(0);
  Output output;
  output.logTab(1,LOGFILE,"Hessian Start # " + itos(countr++));
  floatType start_clock = std::clock();
#endif

  //initialization
  TNT::Vector<floatType> diag_hessian(npars_ref);
  bool return_gradient(false);
  floatType localMLL = gradient_hessian(diag_hessian,return_gradient); //-(totalLL - LLwilson) + restraint
  is_diagonal = true;

  Hessian.newsize(npars_ref,npars_ref);
  PHASER_ASSERT(Hessian.num_cols() == npars_ref);
  PHASER_ASSERT(Hessian.num_rows() == npars_ref);
  for (int ii = 0; ii < npars_ref; ii++) //not i, warning on visual-C, int i defined below
    for (int jj = 0; jj < npars_ref; jj++)
      Hessian(ii+1,jj+1) =  (ii==jj) ? diag_hessian[jj] : 0;

  int m(0),i(0);
  int1D Bindex(models_known.size());
  int1D Oindex(models_known.size());
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (unsigned rot = 0; rot < 3; rot++) if (refinePar[m++]) i++;
    for (unsigned tra = 0; tra < 3; tra++) if (refinePar[m++]) i++;
    if (refinePar[m++])
    {
      Bindex[s] = i;
      if (BFAC_REFI.RESTRAINT)
      {
        Hessian(i+1,i+1) += 1/fn::pow2(BFAC_REFI.SIGMA);
      }
      i++; //BFAC
    }
    if (refinePar[m++])
    {
      Oindex[s] = i;
      i++; //OFAC
    }
  }
  int1D icell(0);
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++])
    {
      if (DRMS_REFI.RESTRAINT)
      {
        Hessian(i+1,i+1) += 1/fn::pow2(DRMS_REFI.SIGMA);
      }
      i++; //VRMS
    }
    if (refinePar[m++]) { icell.push_back(i); i++; } //CELL
  }
  if (getenv("PHASER_STUDY_PARAMS") == NULL) //constrain hessian in the case of pdb files as models
  {
  if (refine_datacell()) //and therefore need to do cell restraint, because it is for data only
  {
    float1D grads(0);
    for (int c = 0; c < icell.size(); c++)
      grads.push_back(Hessian(icell[c]+1,icell[c]+1));
    double sum_grads = sum(grads);
           sum_grads /= icell.size();
    for (int c = 0; c < icell.size(); c++)
      Hessian(icell[c]+1,icell[c]+1) = sum_grads/icell.size();
  }
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

#ifdef AVERAGE_GRADIENT_HESSIAN
  //average Hessian
  if (PTNCS.use_and_present())
  {
    float1D avBfac(Bindex.size(),0);
    int n(-1);
    for (unsigned s = 0; s < Bindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      avBfac[n] += Hessian(Bindex[s]+1,Bindex[s]+1)/PTNCS.NMOL;
    }
    n = -1;
    for (unsigned s = 0; s < Bindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      Hessian(Bindex[s]+1,Bindex[s]+1) = avBfac[n];
    }
    float1D avOfac(Oindex.size(),0);
    n = -1;
    for (unsigned s = 0; s < Oindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      avOfac[n] += Hessian(Oindex[s]+1,Oindex[s]+1)/PTNCS.NMOL;
    }
    n = -1;
    for (unsigned s = 0; s < Oindex.size(); s++)
    {
      if (s % PTNCS.NMOL == 0) n++;
      Hessian(Oindex[s]+1,Oindex[s]+1) = avOfac[n];
    }
  }
#endif

#ifdef PHASER_TIMINGS
  std::clock_t now_clock = std::clock();
  floatType time_for_sum = (now_clock-start_clock)/double(CLOCKS_PER_SEC);
  output.logTab(1,LOGFILE,"Hessian Time = " + dtos(time_for_sum) + " secs");
#endif
  return localMLL;
}

floatType RefineMR::gradient_hessian(TNT::Vector<floatType>& Array,bool return_gradient)
{
  // tNCS case: curvatures (particularly vrms) should take account of correlations between
  // tNCS-related copies.  For now, rely on BFGS algorithm to compensate, but it would be better
  // to deal with this explicitly.
  bool return_hessian = !return_gradient;
  mrllgrads dLL(models_known.size(),ensemble->set_of_modlids());
  mrllgrads d2LL(models_known.size(),ensemble->set_of_modlids());
  floatType totalLL(0);
  mrfcgrads dFC(models_known.size()*NREFL);
  mrfcgrads d2FC(models_known.size()*NREFL);
  cmplx1D   Fcalc(models_known.size()*NREFL,cmplxType(0,0));

  if (!protocol.FIX_ROT || !protocol.FIX_TRA || !protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_CELL || !protocol.FIX_OFAC)
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    if (FAST_LAST && modlid != LAST_MODLID) continue;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    //recover original centre of mass, rotated centre of mass
    dmat33 PRtr = ens_modlid->PR.transpose();
    dvect3 CMori = -PRtr*ens_modlid->PT;
    dmat33 ROT = models_known[s].R;
    dvect3 CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    dmat33 Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
    {
      Rperturb = xyzRotMatDeg(dir,models_perturbRot[s][dir])*Rperturb;
    }
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    dvect3 CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented (*ensemble))
    ROT = ROT*PRtr;

    dvect3 TRA = models_known[s].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation (for refinement)
    iOrthT += models_perturbTrans[s];
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of (*ensemble))
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    dmat33 Q1tr = Q1.transpose();

    dmat33 Rtr = models_known[s].R.transpose();
    dmat33 PR_Rtr_Dtr = ens_modlid->PR * Rtr * UnitCell::Orth2Frac().transpose();

    floatType Bfac = models_known[s].BFAC;
    floatType Ofac = std::sqrt(models_known[s].OFAC);

    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      int sr = s*NREFL + r;
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      floatType sqrt_scatFactor = std::sqrt(ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
      floatType rscale = sqrt_epsn[r]
                       * std::exp(-Bfac*rssqr/4.0)
                       * ens_modlid->Eterm_const()
                       * sqrt_scatFactor
                       / models_known[s].MULT;
      if (PTNCS.use_and_present())
      {
        rscale *= std::sqrt(G_DRMS[r]);
      }
      std::vector<miller::index<int> > rhkl = rotMiller(r);

      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3    RotSymHKL = Q1tr*rhkl[isym];
          cvect3    dthisE(0,0,0);
          cmat33    hthisE(0,0,0,0,0,0,0,0,0);
          cmplxType interpE = ens_modlid->func_grad_hess_InterpE(RotSymHKL,DFAC[r],dthisE,return_hessian,hthisE);
          // If duplicate symm ops were included, interpE would be added epsn times.
          // Divide by sqrt_epsn to normalise, giving overall factor of sqrt_epsn
          cmplxType scalefac = rscale * dphi(r,isym,rhkl[isym],TRA);
          cmplxType thisE  = scalefac * interpE;
          Fcalc[sr] += thisE;
          scalefac *= Ofac; //exclude OFAC from Fcalc
          thisE *= Ofac; //exclude OFAC from Fcalc
          dthisE = (scalefac*DFAC[r]) * dthisE;
          hthisE = (scalefac*DFAC[r]) * hthisE;
          dmat33 dQ1tr_by_drot, d2Q1tr_by_drot2;
          dvect3 dQ1tr_h_by_drot_s, d2Q1tr_h_by_drot2_s;
          for (int rot = 0; rot < 3; rot++)
          {
            dQ1tr_by_drot = PR_Rtr_Dtr;
            for (int dir = 2; dir >= 0; dir--)
              dQ1tr_by_drot   = xyzRotMatDeg(dir,models_perturbRot[s][dir],dir==rot).transpose()*dQ1tr_by_drot;

            dQ1tr_by_drot   = ens_modlid->Frac2Orth().transpose() * dQ1tr_by_drot;
            dQ1tr_h_by_drot_s   = dQ1tr_by_drot  *rhkl[isym];
            dFC.by_drot[sr][rot] += dthisE*dQ1tr_h_by_drot_s;
            if (return_hessian)
            {
              d2Q1tr_by_drot2 = PR_Rtr_Dtr;
              for (int dir = 2; dir >= 0; dir--)
                d2Q1tr_by_drot2 = xyzRotMatDeg(dir,models_perturbRot[s][dir],false,dir==rot).transpose()*d2Q1tr_by_drot2;

              d2Q1tr_by_drot2 = ens_modlid->Frac2Orth().transpose() * d2Q1tr_by_drot2;
              d2Q1tr_h_by_drot2_s = d2Q1tr_by_drot2*rhkl[isym];
              d2FC.by_drot[sr][rot] += dQ1tr_h_by_drot_s*(hthisE*dQ1tr_h_by_drot_s)
                                     +  dthisE*d2Q1tr_h_by_drot2_s;
            }
          }
          floatType cell = models_cell[modlid];
          dmat33 dQ1tr_by_dcell;
          for (int q = 0; q < Q1tr.size(); q++) dQ1tr_by_dcell[q] = Q1tr[q]/cell;
          dvect3 dQ1tr_h_by_dcell_s = dQ1tr_by_dcell*rhkl[isym];
          dFC.by_dcell[sr] += dthisE*dQ1tr_h_by_dcell_s; //scalar = vector.vector
          if (return_hessian)
            d2FC.by_dcell[sr] += dQ1tr_h_by_dcell_s*(hthisE*dQ1tr_h_by_dcell_s);
          for (int tra = 0; tra < 3; tra++)
          {
            //dphi = rhkl.TRA + traMiller
            dvect3 dTRA_by_du(tra==0 ? 1 : 0, tra==1 ? 1 : 0, tra==2 ? 1 : 0);
            dvect3 dhkl(rhkl[isym][0],rhkl[isym][1],rhkl[isym][2]);
            cmplxType C = scitbx::constants::two_pi*cmplxType(0,1)*(dhkl*(UnitCell::Orth2Frac()*dTRA_by_du));
            dFC.by_dtra[sr][tra] += C*thisE;
            cmplxType C2 = fn::pow2(C);
            if (return_hessian)
            {
               d2FC.by_dtra[sr][tra] += C2*thisE;
            }
          }
        }
      }
    }
  }

  if (FTFMAP.FMAP.size() && sum_Esqr_search.empty())
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) totalLL += Rice_grad_hess(r,return_hessian,Fcalc,dFC,d2FC,dLL,d2LL);
  }
  else
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) totalLL += MLHL_grad_hess(r,return_hessian,Fcalc,dFC,d2FC,dLL,d2LL);
  }

  floatType f = -(totalLL - LLwilson);
            f += restraint_term();

  int m(0),i(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (unsigned rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_drot[s][rot] : -d2LL.by_drot[s][rot];
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_dtra[s][tra] : -d2LL.by_dtra[s][tra];
    if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_dB[s] : -d2LL.by_dB[s];
    if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_dO[s] : -d2LL.by_dO[s];
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_dvrms[iter->first] : -d2LL.by_dvrms[iter->first];
    if (refinePar[m++]) Array[i++] = return_gradient ? -dLL.by_dcell[iter->first] : -d2LL.by_dcell[iter->first];
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  return f;
}

double RefineMR::MLHL_grad_hess(int r,bool return_hessian,cmplx1D& Fcalc,mrfcgrads& dFC,mrfcgrads& d2FC,mrllgrads& dLL,mrllgrads& d2LL)
{
  return Rice_grad_hess(r,return_hessian,Fcalc,dFC,d2FC,dLL,d2LL);
}

double RefineMR::Rice_grad_hess(int r,bool return_hessian,cmplx1D& Fcalc,mrfcgrads& dFC,mrfcgrads& d2FC,mrllgrads& dLL,mrllgrads& d2LL)
{
  floatType reflLL(0);
  cmplxType EM(EM_known[r]+EM_search[r]);
  floatType EM_real = EM.real();
  floatType EM_imag = EM.imag();
  floatType phasedEsqr = EM_real*EM_real+EM_imag*EM_imag;
  floatType sumEsqr_m_maxEsqr(0);
  if (sum_Esqr_search.size())//assume && max_Esqr_search.size())
  {
    sumEsqr_m_maxEsqr += phasedEsqr + sum_Esqr_search[r];
    sumEsqr_m_maxEsqr -= std::max(phasedEsqr,max_Esqr_search[r]);
  }

  //Now do likelihood calculation
  floatType V = PTNCS.EPSFAC[r];
  V -= totvar_known[r];
  V -= totvar_search[r];
  V += sumEsqr_m_maxEsqr;
  PHASER_ASSERT(V > 0);

  bool rcent = cent(r);
  floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
  floatType Esqr = fn::pow2(E);

  floatType FCsqr = max_Esqr_search.size() ? std::max(phasedEsqr,max_Esqr_search[r]) : phasedEsqr;
  floatType FC = std::sqrt(FCsqr);
  floatType X = 2.0*E*FC/V;
  reflLL = -(std::log(V)+(Esqr + FCsqr)/V);
  if (rcent)
  {
    X /= 2.0;
    reflLL = reflLL/2.0 + m_alogch.getalogch(X);
  }
  else
  {
    reflLL += m_alogchI0.getalogI0(X); //acentric
  }

  if (!protocol.FIX_ROT || !protocol.FIX_TRA || !protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_CELL || !protocol.FIX_OFAC)
  {
    dvect3 dLL_by_drot_s(0,0,0),d2LL_by_drot2_s(0,0,0);
    dvect3 dLL_by_dtra_s(0,0,0),d2LL_by_dtra2_s(0,0,0);
    floatType dLL_by_dB_s(0),d2LL_by_dB2_s(0);
    floatType dLL_by_dO_s(0),d2LL_by_dO2_s(0);
    floatType dLL_by_dvrms_s(0),d2LL_by_dvrms2_s(0);
    floatType dLL_by_dcell_s(0),d2LL_by_dcell2_s(0);

//#define PHASER_TEST_MR_HESS
#ifdef PHASER_TEST_MR_HESS
// This finite difference test only works currently for refinement of a single model
// As a result, it doesn't work in the presence of tNCS.
bool FDrcent(false);
static int FDcountr(0);
if (getenv("PHASER_TEST_MATHEMATICA") != 0)
{
if (std::string(getenv("PHASER_TEST_MATHEMATICA")) == "centric") FDrcent = true;
if (std::string(getenv("PHASER_TEST_MATHEMATICA")) == "acentric") FDrcent = false;
}
 // Shift has to be larger for second derivative tests, although second
 // derivative tests work poorly for rotation (because of interpolation artefacts)
 if (getenv("PHASER_TEST_SHIFT") == 0)
    { std::cout << "setenv PHASER_TEST_SHIFT 0.0001\n" ; std::exit(1); }
 if (getenv("PHASER_TEST_NREFL") == 0)
    { std::cout << "setenv PHASER_TEST_NREFL 1\n" ; std::exit(1); }
  floatType shift_a = std::atof(getenv("PHASER_TEST_SHIFT"));
  floatType FD_test_on_f = 0;
  floatType FD_test_forward = 0;
  floatType FD_test_backward = 0;
//#define PHASER_TEST_MR_HESS_BFAC
#define PHASER_TEST_MR_HESS_ROTTRA
//#define PHASER_TEST_MR_HESS_VRMS
#ifdef PHASER_TEST_MR_HESS_BFAC
  floatType orig_a = models_known[0].BFAC;
  floatType &dLL_by_dtest = dLL_by_dB_s;
  floatType &d2LL_by_dtest2 = d2LL_by_dB2_s;
#endif
#ifdef PHASER_TEST_MR_HESS_ROTTRA
  //floatType *test_a = &(models_perturbRot[0][0]);
  //floatType &dLL_by_dtest = dLL_by_drot_s[0];
  //floatType &d2LL_by_dtest2 = d2LL_by_drot2_s[0];
  floatType *test_a = &(models_perturbTrans[0][0]);
  floatType &dLL_by_dtest = dLL_by_dtra_s[0];
  floatType &d2LL_by_dtest2 = d2LL_by_dtra2_s[0];
#endif
#ifdef PHASER_TEST_MR_HESS_VRMS
  ensIter iter1 = ensemble->begin();
  floatType *test_a = &(models_drms[iter1->first]);
  ensemble->setup_vrms(models_drms);
  floatType &dLL_by_dtest = dLL_by_dvrms_s;
  floatType &d2LL_by_dtest2 = d2LL_by_dvrms2_s;
#endif
if (return_hessian)
{
  calcKnown();
  calcKnownVAR(); //totvar_known
  floatType f_start = Rice_refl(r);
#ifdef PHASER_TEST_MR_HESS_BFAC
  models_known[0].BFAC=(orig_a+shift_a);
#else
  (*test_a) += shift_a;
#ifdef PHASER_TEST_MR_HESS_VRMS
  ensemble->setup_vrms(models_drms);
#endif
#endif
  calcKnown();
  calcKnownVAR(); //totvar_known
  floatType f_forward = Rice_refl(r);
  FD_test_forward = (f_forward-f_start)/shift_a;
#ifdef PHASER_TEST_MR_HESS_BFAC
  models_known[0].BFAC=(orig_a-shift_a);
#else
  (*test_a) -= shift_a;
  (*test_a) -= shift_a;
#ifdef PHASER_TEST_MR_HESS_VRMS
  ensemble->setup_vrms(models_drms);
#endif
#endif
  calcKnown();
  calcKnownVAR(); //totvar_known
  floatType f_backward = Rice_refl(r);
  FD_test_backward = (f_start-f_backward)/shift_a;
#ifdef PHASER_TEST_MR_HESS_BFAC
  models_known[0].BFAC=(orig_a);
#else
  (*test_a) += shift_a;
#ifdef PHASER_TEST_MR_HESS_VRMS
  ensemble->setup_vrms(models_drms);
#endif
#endif
  calcKnown();
  calcKnownVAR(); //totvar_known
  FD_test_on_f = (f_forward-2*f_start+f_backward)/shift_a/shift_a;
}
#endif

    floatType bessTerm = rcent ? std::tanh(X) : scitbx::math::bessel::i1_over_i0(X);
    floatType bessTerm2 = rcent ? fn::pow2(1/std::cosh(X)) : fn::pow2(bessTerm);
    // NB: sech = 1/cosh
    floatType FA = EM_known[r].real() + EM_search[r].real();
    floatType FB = EM_known[r].imag() + EM_search[r].imag();
    floatType V2 = fn::pow2(V);
    floatType V3 = fn::pow3(V);
    floatType V4 = fn::pow4(V);
    floatType fc2 = fn::pow2(FC);
    floatType fc4 = fn::pow2(fc2);

    floatType dLL_by_dFA(0),dLL_by_dFB(0);
    floatType d2LL_by_dFA2(0),d2LL_by_dFB2(0),d2LL_by_dFA_dFB(0);
 //   floatType d2LL_by_dFC2(0);

    floatType dLL_by_dFC = rcent ? (1/V)*(E*bessTerm - FC) : (2/V)*(E*bessTerm - FC);

    floatType dFC_by_dFA = 0;
    floatType dFC_by_dFB = 0;
    if (FC > 0)
    {
      dFC_by_dFA = FA/FC;
      dFC_by_dFB = FB/FC;
    }
    dLL_by_dFA = dLL_by_dFC*dFC_by_dFA;
    dLL_by_dFB = dLL_by_dFC*dFC_by_dFB;

    floatType denom = fc4*V2;
    if (return_hessian && fc2 > 0 && fc4 > 0 && denom > 0)
    {
      d2LL_by_dFA2 = rcent ?
        bessTerm2*FA*FA*Esqr/fc2/V2 - bessTerm*FA*FA*FC*E/fc4/V + bessTerm*FC*E/fc2/V - 1/V :
        (-2*fc2*(fc2-bessTerm*FC*E)*V-4*FA*FA*E*((-1 + bessTerm2)*fc2*E + bessTerm*FC*V))/denom;
      d2LL_by_dFB2 = rcent ?
        bessTerm2*FB*FB*Esqr/fc2/V2 - bessTerm*FB*FB*FC*E/fc4/V + bessTerm*FC*E/fc2/V - 1/V :
        (-2*fc2*(fc2-bessTerm*FC*E)*V-4*FB*FB*E*((-1 + bessTerm2)*fc2*E + bessTerm*FC*V))/denom;
      d2LL_by_dFA_dFB = rcent ?
        bessTerm2*FA*FB*Esqr/fc2/V2 - bessTerm*FA*FB*FC*E/fc4/V :
        -4*FA*FB*E*((-1 + bessTerm2)*fc2*E+bessTerm*FC*V)/denom;
 //     d2LL_by_dFC2 = rcent ?
 //       bessTerm2*Esqr/V2 - 1/V :
 //       (4*Esqr/V2)*(1.-bessTerm2) - (2/V)*(1. + bessTerm*E/FC);
    }

    floatType dLL_by_dV(0);
    floatType d2LL_by_dV2(0);
    floatType d2LL_by_dFA_dV(0);
    floatType d2LL_by_dFB_dV(0);
    if (!protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_OFAC)
    {
      dLL_by_dV = rcent ?
         (FCsqr - 2*bessTerm*FC*E + Esqr - V)/(2*V2) :
         ((Esqr + FCsqr - V) - 2*E*FC*bessTerm)/V2;

      if (return_hessian && V3 > 0 && V4 > 0)
      {
        d2LL_by_dV2 = rcent ?
           (2*bessTerm2*FCsqr*Esqr + V*(-2*FCsqr + 4*bessTerm*FC*E - 2*Esqr + V))/(2*V4) :
           (4*Esqr*FCsqr/V4 - 2*(FCsqr+Esqr)/V3 + 1/V2 + 2*FC*E*bessTerm/V3 - 4*Esqr*FCsqr*bessTerm2/V4);
        if (fc2 > 0)
        {
        d2LL_by_dFA_dV =  rcent ?
           FA/V2 - FA*Esqr*bessTerm2/V3 - FA*FC*E*bessTerm/fc2/V2  :
          -4*FA*Esqr/V3 + 2*FA/V2 + 4*FA*Esqr*bessTerm2/V3;
        d2LL_by_dFB_dV =  rcent ?
           FB/V2 - FB*Esqr*bessTerm2/V3 - FB*FC*E*bessTerm/fc2/V2  :
          -4*FB*Esqr/V3 + 2*FB/V2 + 4*FB*Esqr*bessTerm2/V3;
        }
      }
    }

    for (int s = 0; s < models_known.size(); s++)
    {
      std::string modlid = models_known[s].MODLID;
      if (FAST_LAST && modlid != LAST_MODLID) continue;
      Ensemble* ens_modlid = &ensemble->find(modlid)->second;
      int sr = s*NREFL + r;
      if (!protocol.FIX_ROT)
      {
        dvect3 dFA_by_drot_s(dFC.by_drot[sr][0].real(),dFC.by_drot[sr][1].real(),dFC.by_drot[sr][2].real());
        dvect3 dFB_by_drot_s(dFC.by_drot[sr][0].imag(),dFC.by_drot[sr][1].imag(),dFC.by_drot[sr][2].imag());
               dLL_by_drot_s = dLL_by_dFA*dFA_by_drot_s + dLL_by_dFB*dFB_by_drot_s;
        dLL.by_drot[s] += dLL_by_drot_s;

        if (return_hessian)
        {
          for (int rot = 0; rot < 3; rot++)
          {
            floatType d2FA_by_drot2_s = (d2FC.by_drot[sr][rot].real());
            floatType d2FB_by_drot2_s = (d2FC.by_drot[sr][rot].imag());
            d2LL_by_drot2_s[rot] = d2LL_by_dFA2*fn::pow2(dFA_by_drot_s[rot]) +
                                   d2LL_by_dFB2*fn::pow2(dFB_by_drot_s[rot]) +
                                   d2FA_by_drot2_s*dLL_by_dFA +
                                   d2FB_by_drot2_s*dLL_by_dFB +
                                   2*d2LL_by_dFA_dFB*dFA_by_drot_s[rot]*dFB_by_drot_s[rot];
          }
          d2LL.by_drot[s] += d2LL_by_drot2_s;
        }
      }
      if (!protocol.FIX_TRA)
      {
        dvect3 dFA_by_dtra_s(dFC.by_dtra[sr][0].real(),dFC.by_dtra[sr][1].real(),dFC.by_dtra[sr][2].real());
        dvect3 dFB_by_dtra_s(dFC.by_dtra[sr][0].imag(),dFC.by_dtra[sr][1].imag(),dFC.by_dtra[sr][2].imag());
               dLL_by_dtra_s = dLL_by_dFA*dFA_by_dtra_s + dLL_by_dFB*dFB_by_dtra_s;
        dLL.by_dtra[s] += dLL_by_dtra_s;

        if (return_hessian)
        {
          dvect3 d2FA_by_dtra2_s(d2FC.by_dtra[sr][0].real(),d2FC.by_dtra[sr][1].real(),d2FC.by_dtra[sr][2].real());
          dvect3 d2FB_by_dtra2_s(d2FC.by_dtra[sr][0].imag(),d2FC.by_dtra[sr][1].imag(),d2FC.by_dtra[sr][2].imag());
          for (int tra = 0; tra < 3; tra++)
            d2LL_by_dtra2_s[tra] = d2LL_by_dFA2*fn::pow2(dFA_by_dtra_s[tra]) +
                                   d2LL_by_dFB2*fn::pow2(dFB_by_dtra_s[tra]) +
                                   dLL_by_dFA*d2FA_by_dtra2_s[tra] +
                                   dLL_by_dFB*d2FB_by_dtra2_s[tra] +
                                   2*d2LL_by_dFA_dFB*dFA_by_dtra_s[tra]*dFB_by_dtra_s[tra];
          d2LL.by_dtra[s] += d2LL_by_dtra2_s;
        }
      }

      if (!protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_OFAC)
      {
        floatType rssqr = ssqr(r);
        floatType sqrtrssqr = std::sqrt(rssqr);
        ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
        floatType Fconst = -rssqr/4.0;
        floatType Bconst = -rssqr/2.0;
        floatType Vconst = ens_modlid->dvrms_delta_const();
        floatType Ofac = std::sqrt(models_known[s].OFAC);
        floatType FA_s = Ofac*Fcalc[sr].real();
        floatType FB_s = Ofac*Fcalc[sr].imag();

        floatType dFA_by_dB(0),dFB_by_dB(0),dV_by_dB(0);
        floatType dFA_by_dO(0),dFB_by_dO(0),dV_by_dO(0);
        floatType dFA_by_dvrms(0),dFB_by_dvrms(0),dV_by_dvrms(0);
        floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
                  thisV *= ens_modlid->SCATTERING/TOTAL_SCAT;
                  thisV *= exp(-2.0*models_known[s].BFAC*rssqr/4.0);
                 // thisV *= models_known[k].OFAC;
                  thisV /= models_known[s].MULT;
        if (PTNCS.use_and_present())
                  thisV *= G_DRMS[r]*G_Vterm[r];
        floatType totvar_known_s = thisV; //exclude OFAC

        if (!protocol.FIX_BFAC)
        {
                    dFA_by_dB = Fconst*FA_s;
                    dFB_by_dB = Fconst*FB_s;
          floatType dtotvar_by_dB = Bconst*totvar_known_s*models_known[s].OFAC;

                    dV_by_dB = - dtotvar_by_dB;
                    dLL_by_dB_s = dLL_by_dFA*dFA_by_dB + dLL_by_dFB*dFB_by_dB + dLL_by_dV*dV_by_dB ;
          dLL.by_dB[s] += dLL_by_dB_s;

          if (return_hessian)
          {
            floatType d2FA_by_dB2 = Fconst*dFA_by_dB;
            floatType d2FB_by_dB2 = Fconst*dFB_by_dB;
            floatType d2V_by_dB2  = Bconst*dV_by_dB;

                      d2LL_by_dB2_s = d2FA_by_dB2*dLL_by_dFA +
                                      fn::pow2(dFA_by_dB)*d2LL_by_dFA2 +
                                      d2FB_by_dB2*dLL_by_dFB +
                                      2*dFA_by_dB*dFB_by_dB*d2LL_by_dFA_dFB +
                                      fn::pow2(dFB_by_dB)*d2LL_by_dFB2 +
                                      d2V_by_dB2*dLL_by_dV +
                                      2*dFA_by_dB*dV_by_dB*d2LL_by_dFA_dV +
                                      2*dFB_by_dB*dV_by_dB*d2LL_by_dFB_dV +
                                      fn::pow2(dV_by_dB)*d2LL_by_dV2;
            d2LL.by_dB[s] += d2LL_by_dB2_s;
          }
        }

        if (!protocol.FIX_VRMS)
        {
          dFA_by_dvrms = Vconst*FA_s;
          dFB_by_dvrms = Vconst*FB_s;
          dV_by_dvrms = -2*Vconst*totvar_known_s*models_known[s].OFAC; //EPSFAC - thisV term
          dLL_by_dvrms_s = dLL_by_dFA*dFA_by_dvrms + dLL_by_dFB*dFB_by_dvrms + dLL_by_dV*dV_by_dvrms;
          dLL.by_dvrms[modlid] += dLL_by_dvrms_s;

          if (return_hessian)
          {
            floatType d2FA_by_dvrms2 = Vconst*dFA_by_dvrms;
            floatType d2FB_by_dvrms2 = Vconst*dFB_by_dvrms;
            floatType d2V_by_dvrms2 =  2*Vconst*dV_by_dvrms;
                   d2LL_by_dvrms2_s = d2FA_by_dvrms2*dLL_by_dFA +
                                      fn::pow2(dFA_by_dvrms)*d2LL_by_dFA2 +
                                      d2FB_by_dvrms2*dLL_by_dFB +
                                      2*dFA_by_dvrms*dFB_by_dvrms*d2LL_by_dFA_dFB +
                                      fn::pow2(dFB_by_dvrms)*d2LL_by_dFB2 +
                                      d2V_by_dvrms2*dLL_by_dV +
                                      2*dFA_by_dvrms*dV_by_dvrms*d2LL_by_dFA_dV +
                                      2*dFB_by_dvrms*dV_by_dvrms*d2LL_by_dFB_dV +
                                      fn::pow2(dV_by_dvrms)*d2LL_by_dV2;
            d2LL.by_dvrms[modlid] += d2LL_by_dvrms2_s;
          }
        }

        if (!protocol.FIX_OFAC)
        {
          floatType Oterm = 1.0/2.0/std::sqrt(models_known[s].OFAC);
                    dFA_by_dO = Oterm*Fcalc[sr].real();
                    dFB_by_dO = Oterm*Fcalc[sr].imag();
          floatType dtotvar_by_dO = totvar_known_s;

                    dV_by_dO = -dtotvar_by_dO;
                    dLL_by_dO_s = dLL_by_dFA*dFA_by_dO + dLL_by_dFB*dFB_by_dO + dLL_by_dV*dV_by_dO ;
          dLL.by_dO[s] += dLL_by_dO_s;

          if (return_hessian)
          {
            floatType Oterm2 = 1.0/2.0/Oterm/models_known[s].OFAC;
            floatType d2FA_by_dO2 = Oterm2*Fcalc[sr].real();
            floatType d2FB_by_dO2 = Oterm2*Fcalc[sr].imag();
            floatType d2V_by_dO2  = 0;

                      d2LL_by_dO2_s = d2FA_by_dO2*dLL_by_dFA +
                                      fn::pow2(dFA_by_dO)*d2LL_by_dFA2 +
                                      d2FB_by_dO2*dLL_by_dFB +
                                      2*dFA_by_dO*dFB_by_dO*d2LL_by_dFA_dFB +
                                      fn::pow2(dFB_by_dO)*d2LL_by_dFB2 +
                                      d2V_by_dO2*dLL_by_dV +
                                      2*dFA_by_dO*dV_by_dO*d2LL_by_dFA_dV +
                                      2*dFB_by_dO*dV_by_dO*d2LL_by_dFB_dV +
                                      fn::pow2(dV_by_dO)*d2LL_by_dV2;
            d2LL.by_dO[s] += d2LL_by_dO2_s;
          }
        }
      } //loop over known

      if (!protocol.FIX_CELL)
      {
        floatType dFA_by_dcell_s = dFC.by_dcell[sr].real();
        floatType dFB_by_dcell_s = dFC.by_dcell[sr].imag();
        dLL_by_dcell_s = dLL_by_dFA*dFA_by_dcell_s + dLL_by_dFB*dFB_by_dcell_s;
        dLL.by_dcell[modlid] += dLL_by_dcell_s;

        if (return_hessian)
        {
          floatType d2FA_by_dcell2_s = d2FC.by_dcell[sr].real();
          floatType d2FB_by_dcell2_s = d2FC.by_dcell[sr].imag();
                    d2FA_by_dcell2_s = d2FA_by_dcell2_s/2;
                    d2FB_by_dcell2_s = d2FB_by_dcell2_s/2;
                 d2LL_by_dcell2_s = d2FA_by_dcell2_s*dLL_by_dFA +
                                    fn::pow2(dFA_by_dcell_s)*d2LL_by_dFA2 +
                                    d2FB_by_dcell2_s*dLL_by_dFB +
                                    2*dFA_by_dcell_s*dFB_by_dcell_s*d2LL_by_dFA_dFB +
                                    fn::pow2(dFB_by_dcell_s)*d2LL_by_dFB2;
          d2LL.by_dcell[modlid] += d2LL_by_dcell2_s/2;
        }
      }

    }

#ifdef PHASER_TEST_MR_HESS
if (return_hessian && rcent==FDrcent)
{
  FDcountr++;
  std::cout << "=== First Derivative test (r=" << r << ") ===\n";
  std::cout << "on function " << dLL_by_dtest  << " fd forward "  << FD_test_forward << " fd backward " << FD_test_backward << "\n";
  PHASER_ASSERT(FD_test_forward);
  PHASER_ASSERT(FD_test_backward);
  std::cout << "Ratio forward "  << dLL_by_dtest/FD_test_forward <<
                    " backward " << dLL_by_dtest/FD_test_backward << "\n";
  std::cout << "=== Second Derivative test (r=" << r << ") ===\n";
  PHASER_ASSERT(FD_test_on_f);
  std::cout <<"on function " << d2LL_by_dtest2 << " fd " << FD_test_on_f << "\nRatio " << d2LL_by_dtest2/FD_test_on_f << "\n";
  if (FDcountr >= std::atof(getenv("PHASER_TEST_NREFL"))) std::exit(1);
}
#endif
  }
  return reflLL;
}

TNT::Vector<floatType> RefineMR::getLargeShifts()
{
  //relative sizes of these are important in finite diff grad calculation
  TNT::Vector<floatType> largeShifts(npars_ref);
  floatType dmin(HiRes());
  int m(0),i(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    for (unsigned rot = 0; rot < 3; rot++)
      if (refinePar[m++])
      {
        std::string modlid = models_known[s].MODLID;
        floatType maxperp(0);
        if (rot == 0)
          maxperp = std::max(ens_modlid->B(),ens_modlid->C());
        else if (rot == 1)
          maxperp = std::max(ens_modlid->C(),ens_modlid->A());
        else
          maxperp = std::max(ens_modlid->A(),ens_modlid->B());
        largeShifts[i++] = 4*dmin/scitbx::deg_as_rad(maxperp);
      }
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) largeShifts[i++] = dmin/4.0;
    if (refinePar[m++]) largeShifts[i++] = 4.; //BFAC - or higher
    if (refinePar[m++]) largeShifts[i++] = 0.1; //OFAC
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) largeShifts[i++] = dmin/6.0; //VRMS
    if (refinePar[m++]) largeShifts[i++] = 0.1; //CELL
  }
  //if smaller than 0.5 toxd will not refine ROT and TRA of 2nd peak to unique solution
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return largeShifts;
}

void  RefineMR::applyShift(TNT::Vector<floatType>& newx)
{
  int i(0),m(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) models_perturbRot[s][rot] = newx[i++];
    for (unsigned tra = 0; tra < 3; tra++)
      if (refinePar[m++]) models_perturbTrans[s][tra] = newx[i++];
    if (refinePar[m++]) models_known[s].BFAC=(newx[i++]);
    if (refinePar[m++]) models_known[s].OFAC=(newx[i++]);
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) models_drms[iter->first] = newx[i++]; //VRMS
    if (refinePar[m++]) models_cell[iter->first] = newx[i++]; //CELL
  }
  //LAST_ONLY implies refine VRMS of last only if protocol.FIX_VRMS is false (VRMS refined)
  FAST_LAST = (!protocol.FIX_ROT || !protocol.FIX_TRA || !protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_OFAC) //refine something for models_known
               && protocol.LAST_ONLY && protocol.FIX_CELL;

  if (FAST_LAST)
  {
    //only set the vrms for the last ensemble
    if (!protocol.FIX_VRMS) ensemble->find(LAST_MODLID)->second.setup_vrms(models_drms[LAST_MODLID]);
    fast_last_applyShift(); //sets search arrays, init for known in setUp
  }
  else
  {
    if (!protocol.FIX_VRMS) ensemble->setup_vrms(models_drms);
    if (!protocol.FIX_CELL) ensemble->setup_cell(models_cell);
    calcKnown();
    if (!protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_CELL || !protocol.FIX_OFAC) calcKnownVAR(); //totvar_known
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
}

void RefineMR::logCurrent(outStream where,Output& output)
{
  size_t len(0);
  for (unsigned s = 0; s < models_known.size(); s++)
    len = std::max(len,models_known[s].MODLID.size());

  where = VERBOSE; //override for MR
  output.logTab(1,where,"Perturbation of MR solutions (Degrees & Angstroms), B-factor & O-factor shift");
  if (!models_known.size()) output.logTab(1,where,"No solutions");
  else
  {
    for (unsigned s = 0; s < models_known.size(); s++)
    {
      output.logTabPrintf(1,where,
        "#%-2i %-*s %+6.4f %+6.4f %+6.4f    %+6.4f %+6.4f %+6.4f  %+6.2f %+5.3f\n",
        (s+1),len,models_known[s].MODLID.c_str(),
        models_perturbRot[s][0],models_perturbRot[s][1],models_perturbRot[s][2],
        models_perturbTrans[s][0],models_perturbTrans[s][1],models_perturbTrans[s][2],
        models_known[s].BFAC-models_initBfac[s],models_known[s].OFAC-models_initOfac[s]);
    }
    if (!protocol.FIX_VRMS)
    for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
      output.logTab(1,where,"Ensemble " + iter->first + " VRMS delta factor: " + dtos(models_drms[iter->first],6,4));
    if (!protocol.FIX_CELL)
    for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
      output.logTab(1,where,"Ensemble " + iter->first + " Cell scale factor: " + dtos(models_cell[iter->first],6,4));
  }
  output.logBlank(where);
}

void RefineMR::logInitial(outStream where,Output& output)
{
  where = LOGFILE; //override
  output.logTab(1,where,"Initial Parameters:");
  if (models_known.size())
  {
    mr_set tmp;
    tmp.KNOWN = models_known;
    for (map_str_float::iterator iter = models_drms.begin(); iter != models_drms.end(); iter++)
      tmp.DRMS[iter->first] = iter->second;
    tmp.setup_newvrms(ensemble->get_drms_vrms());
    for (map_str_float::iterator iter = models_cell.begin(); iter != models_cell.end(); iter++)
      tmp.CELL[iter->first] = iter->second;
    output.logTab(0,where,tmp.logfile(1,true,true),false);
  }
  output.logBlank(where);
}

void RefineMR::logFinal(outStream where,Output& output)
{
  output.logTab(1,where,"Final Parameters:");
  if (models_known.size())
  {
    mr_set tmp;
    tmp.KNOWN = models_known;
    for (map_str_float::iterator iter = models_drms.begin(); iter != models_drms.end(); iter++)
      tmp.DRMS[iter->first] = iter->second;
    tmp.setup_newvrms(ensemble->get_drms_vrms());
    for (map_str_float::iterator iter = models_cell.begin(); iter != models_cell.end(); iter++)
      tmp.CELL[iter->first] = iter->second;
    output.logTab(0,where,tmp.logfile(1,true,true),false);
  }
  output.logBlank(where);
}

std::string RefineMR::whatAmI(int& parameter)
{
  int i(0),m(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    std::string card = "ensemble " + modlid + " model #" + itos(s+1) + " ";
    if (refinePar[m++]) if (i++ == parameter) return card + " RotX";
    if (refinePar[m++]) if (i++ == parameter) return card + " RotY";
    if (refinePar[m++]) if (i++ == parameter) return card + " RotZ";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraX";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraY";
    if (refinePar[m++]) if (i++ == parameter) return card + " TraZ";
    if (refinePar[m++]) if (i++ == parameter) return card + " Bfac";
    if (refinePar[m++]) if (i++ == parameter) return card + " Ofac";
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) if (i++ == parameter) return "ensemble " + iter->first + " VRMS";
    if (refinePar[m++]) if (i++ == parameter) return "ensemble " + iter->first + " CELL";
  }
  PHASER_ASSERT(i == npars_ref);
  return "Undefined parameter";
}

bool1D RefineMR::getRefineMask(protocolPtr p)
{
  protocol.FIX_ROT = p->getFIX(macm_rot);
  protocol.FIX_TRA = p->getFIX(macm_tra);
  protocol.FIX_BFAC = p->getFIX(macm_bfac);
  protocol.FIX_VRMS = p->getFIX(macm_vrms);
  protocol.FIX_CELL = p->getFIX(macm_cell);
  protocol.FIX_OFAC = p->getFIX(macm_ofac);
  protocol.LAST_ONLY = p->getFIX(macm_last);
  LAST_MODLID = (models_known.size()) ? models_known[models_known.size()-1].MODLID : "";
  use_rotref_restraint = p->getNUM(macm_sigr) != 0;
  sigma_rotref = p->getNUM(macm_sigr);
  use_traref_restraint = p->getNUM(macm_sigt) != 0;
  sigma_traref = p->getNUM(macm_sigt);
  //parameter refinement for rotref: hardwired, no user control
  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
  cctbx::sgtbx::space_group_type SgInfo(DataB::getCctbxSG());
  cctbx::sgtbx::structure_seminvariants semis(DataB::getCctbxSG());
  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
  af::tiny< bool, 3 > isshift(false,false,false);
  if (ssym.continuous_shifts_are_principal())
    isshift = ssym.continuous_shift_flags();

  int last = PTNCS.use_and_present() ? PTNCS.NMOL : 1;
  int known_minus_nmol(models_known.size()-last);
  LAST_KNOWN = std::max(0,known_minus_nmol);
  for (int s = 0; s < LAST_KNOWN; s++)
  {
    std::string modlid = models_known[s].MODLID;
    for (unsigned rot = 0; rot < 3; rot++)
      (protocol.LAST_ONLY || protocol.FIX_ROT || models_known[s].FIXR || (*ensemble)[modlid].is_atom) ?
        refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    for (unsigned tra = 0; tra < 3; tra++)
      (protocol.LAST_ONLY || protocol.FIX_TRA || models_known[s].FIXT ||
          (models_known.size()==1 && isshift[tra])) ?  // fix continuous shifts for only molecule
        refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    (protocol.LAST_ONLY || protocol.FIX_BFAC || models_known[s].FIXB ) ?
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    (protocol.LAST_ONLY || protocol.FIX_OFAC ) ?
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  for (int s = LAST_KNOWN; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    for (unsigned rot = 0; rot < 3; rot++)
      (protocol.FIX_ROT || models_known[s].FIXR || (*ensemble)[modlid].is_atom) ?
        refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    for (unsigned tra = 0; tra < 3; tra++)
      (protocol.FIX_TRA || models_known[s].FIXT ||
          (models_known.size()==1 && isshift[tra])) ?  // fix continuous shifts for only molecule
        refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    (protocol.FIX_BFAC || models_known[s].FIXB) ?
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
    (protocol.FIX_OFAC) ?
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    (protocol.FIX_VRMS || ensemble->find(iter->first)->second.is_atom ||
      (!protocol.FIX_VRMS && protocol.LAST_ONLY && iter->first != LAST_MODLID)) ? //VRMS
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
   // (protocol.FIX_CELL || ensemble->find(iter->first)->second.from_pdb()) ? //no longer fix, use contraints
    (protocol.FIX_CELL) ? //CELL
      refineMask.push_back(REFINE_OFF) : refineMask.push_back(REFINE_ON);
  }
  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

TNT::Vector<floatType> RefineMR::getRefinePars()
{
  TNT::Vector<floatType> pars(npars_ref);
  int i(0),m(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) pars[i++] = models_perturbRot[s][rot];
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) pars[i++] = models_perturbTrans[s][tra];
    if (refinePar[m++]) pars[i++] = models_known[s].BFAC; //BFAC
    if (refinePar[m++]) pars[i++] = models_known[s].OFAC; //OFAC
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) pars[i++] = models_drms[iter->first]; //VRMS
    if (refinePar[m++]) pars[i++] = models_cell[iter->first]; //CELL
  }
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return pars;
}

std::vector<reparams> RefineMR::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) repar[i++].off();
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) repar[i++].off();
    if (refinePar[m++]) repar[i++].off(); //BFAC
    if (refinePar[m++]) repar[i++].off(); //OFAC
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) repar[i++].off(); //VRMS
    if (refinePar[m++]) repar[i++].off(); //CELL
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return repar;
}

std::vector<bounds>  RefineMR::getUpperBounds()
{
  std::vector<bounds> Upper(npars_ref);
  floatType dmax(LoRes());
  int m(0),i(0);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Upper[i++].on(90.); // Any sensible perturbation will be much smaller
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Upper[i++].on(dmax/2); // Any sensible perturbation will be much smaller
    // Set overall limit for B values here, but also use correlated limit in getMaxDistSpecial
    if (refinePar[m++]) Upper[i++].on(upperB()); //BFAC
    if (refinePar[m++]) Upper[i++].on(DEF_OFAC_SCALE_MAX); //OFAC
  }
  //if too large, molecule will get washed out and won't come back from limits
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    Ensemble* ens_modlid = &ensemble->find(iter->first)->second;
    if (refinePar[m++]) Upper[i++].on(ens_modlid->DV.DRMS.upper); //VRMS
    if (refinePar[m++]) Upper[i++].on(DEF_CELL_SCALE_MAX); //CELL
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

std::vector<bounds>  RefineMR::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  floatType dmax(LoRes());
  int m(0),i(0);
  //limit so that max factor from each term cannot take frac over 1
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) Lower[i++].on(-90.);
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) Lower[i++].on(-dmax/2);
   // Set overall limit for B values here, but also use correlated limit in getMaxDistSpecial
    if (refinePar[m++]) Lower[i++].on(lowerB()); //BFAC
    if (refinePar[m++]) Lower[i++].on(DEF_OFAC_SCALE_MIN); //OFAC
  }
  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    Ensemble* ens_modlid = &ensemble->find(iter->first)->second;
    if (refinePar[m++]) Lower[i++].on(ens_modlid->DV.DRMS.lower); //VRMS
    if (refinePar[m++]) Lower[i++].on(DEF_CELL_SCALE_MIN); //CELL
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

void RefineMR::cleanUp(outStream where,Output& output)
{
  dmat33 ROT,PRtr,Rperturb;
  dvect3 TRA,CMori,CMrot,CMperturb;
  dvect3 zero3(0,0,0);
  GYRE.resize(models_known.size());

  for (unsigned s = 0; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;

    GYRE[s].MODLID = models_known[s].MODLID;
    GYRE[s].perturbRot = models_perturbRot[s];
    GYRE[s].perturbTrans = models_perturbTrans[s];

    //recover original centre of mass, rotated centre of mass
    PRtr = ens_modlid->PR.transpose();
    CMori = -PRtr*ens_modlid->PT;
    ROT = models_known[s].R;
    CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform orientation
    Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,models_perturbRot[s][dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    models_known[s].R=ROT;
    models_perturbRot[s] = zero3;

    CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented ensemble)
    //ROT = ROT*PRtr;
    TRA = models_known[s].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation
    iOrthT += models_perturbTrans[s];
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;

    //keep coordinates in same unit cell
    dvect3 deltaTRA = UnitCell::doOrth2Frac(iOrthT) - TRA;
    for (unsigned tra = 0; tra < 3; tra++)
    {
      while (deltaTRA[tra] <= -1) deltaTRA[tra]++;
      while (deltaTRA[tra] >  +1) deltaTRA[tra]--;
    }
    TRA += deltaTRA;

    models_known[s].setFracT(TRA);
    models_perturbTrans[s] = zero3;
  }
  output.logTab(1,where,"Reset rotation and translation perturbations to zero, update solutions");
  output.logBlank(where);

  if (getenv("PHASER_STUDY_PARAMS") == NULL) //constrain values in the case of pdb files as models
  {
  if (refine_datacell()) //and therefore need to do cell restraint, because it is for data only
  {
    for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    if (models_cell[iter->first] != models_cell[ensemble->begin()->first])
    { //make sure all the cells are numerically identical, for identification of constraint in mr_set
      models_cell[iter->first] = models_cell[ensemble->begin()->first]; //CELL
    }
  }
  }
}

floatType RefineMR::getMaxDistSpecial(TNT::Vector<floatType>& x, TNT::Vector<floatType>& g, bool1D& bounded, TNT::Vector<floatType>& dist,floatType& start_distance)
// Return maximum multiple of gradient that can be shifted before hitting *furthest possible* bound
// for "special" parameters, i.e. B-factors influencing total scattering.
// Calculations done as if OFAC is 1, which is conservative when OFAC is being refined.
// However, this would have to be changed if upper bound for OFAC were > 1.
// Set dist for any parameters that are found to have new distance limits
{
  if (protocol.FIX_BFAC) return 0;
  TNT::Vector<floatType>  localg = g;

  int m(0),i(0);
  // deltai and index keep track of which parameters are potentially affected by correlations
  // Value of 0 for deltai indicates parameter that is pushing up against its bounds
  float1D deltai;
  int1D   index;
  floatType fracmodeled(0.); // Fraction modeled of scattering at rest
  float1D fracScat(models_known.size()); // Save for later
  bool bdecreasing(false);
  for (unsigned s = 0; s < models_known.size(); s++)
  {
    std::string modlid = models_known[s].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    fracScat[s] = ens_modlid->maxAtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT;
    fracmodeled += fracScat[s];
    for (int rot = 0; rot < 3; rot++)
      if (refinePar[m++]) i++;
    for (int tra = 0; tra < 3; tra++)
      if (refinePar[m++]) i++;
    if (refinePar[m++])
    {
      //check the ensemble
      if ((g[i] <= 0 && x[i] >= upperB()) || // x - d*g i.e. has run up against upper limit
          (g[i] >= 0 && x[i] <= lowerB()))   // x - d*g i.e. has run up against lower limit
      {
        deltai.push_back(0);
        localg[i] = 0;
      }
      else deltai.push_back(g[i]);
      index.push_back(i);
      if (localg[i] > 0) bdecreasing = true;
      i++; //BFAC
    }
    if (refinePar[m++]) i++; //OFAC
  }
  if (!bdecreasing) return 0; // No danger if no B-factors are decreasing

  floatType fracleft(1.-fracmodeled); // Fraction of unmodeled scattering at rest
  floatType kssqr = -2*fn::pow2(1/fullHiRes())/4;
  floatType fracleftmin = fracleft*std::exp(kssqr*upperB());

  for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
  {
    if (refinePar[m++]) i++; // VRMS
    if (refinePar[m++]) i++; // CELL
  }
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);

  const floatType MAX_SHIFT(std::numeric_limits<floatType>::max()/2.);
  floatType max_step(MAX_SHIFT);
  for (unsigned s = 0; s < deltai.size(); s++)
  if (deltai[s] != 0) //i.e. 0 == flag for running up against bounds
  { //don't include the ones that are up against limits in test for max_step
    int ii = index[s];
    max_step = std::min(max_step,dist[ii]);
    //limits for dist have already been determined
    //this limit will be less than this
  }
  PHASER_ASSERT(max_step >= 0);

  //B-factor special restraint
  floatType distance(0),step(max_step);
  //limits now between distance and distance+step
  //doesn't matter if this goes outside minB and maxB, bound limit applied separately
  TNT::Vector<floatType> newx = x;
  int nloop(0);

  floatType lastgooddist(0.);
  TNT::Vector<floatType> largeShifts = getLargeShifts();

  if (step) //otherwise distance=0 and use 0 as the limit (max_step=0 above)
  for (;;)
  {
    distance += step;
    if (distance != MAX_SHIFT)
    {
      CalcDampedShift(distance,newx,x,localg,dist,largeShifts);
    }
    else
    {
      for (int ii = 0; ii < x.size(); ii++)
      {
        if (localg[ii] == 0)
          newx[ii] = x[ii]; //don't shift ones against the bounds
        else if (std::fabs(localg[ii]) < 1)
          newx[ii] = x[ii] - distance*localg[ii]; //very large but not infinite shift
        else if (localg[ii] < 0)
          newx[ii] = MAX_SHIFT; //shift to positive bound
        else if (localg[ii] > 0)
          newx[ii] = -MAX_SHIFT; //shift to negative bound
      }
    }
    floatType fracmodeled_B(0.);
    { //memory
    i = m = 0;
    for (unsigned s = 0; s < models_known.size(); s++)
    {
      for (int rot = 0; rot < 3; rot++)
        if (refinePar[m++]) i++;
      for (unsigned tra = 0; tra < 3; tra++)
        if (refinePar[m++]) i++;
      floatType this_models_bfac;
      if (refinePar[m++])
      {
        this_models_bfac = newx[i++]; //BFAC
        this_models_bfac = std::max(this_models_bfac,lowerB());
        this_models_bfac = std::min(this_models_bfac,upperB());
      }
      else
        this_models_bfac = models_known[s].BFAC;
      fracmodeled_B += fracScat[s]*std::exp(kssqr*this_models_bfac);
      if (refinePar[m++]) i++; //OFAC
    }
    for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
    {
      if (refinePar[m++]) i++; //VRMS
      if (refinePar[m++]) i++; //CELL
    }
    PHASER_ASSERT(i == npars_ref);
    PHASER_ASSERT(m == npars_all);
    } //end memory
    floatType fracleft_B(1.-fracmodeled_B);
    if ( (fracleft_B < fracleftmin) ) // Too much scattering at high res
      distance -= step;
    else
    {
      lastgooddist = distance;
      if (distance >= max_step) break; // No problems at limit imposed by uncorrelated limits
    }
    if (nloop++ > 1000)
    {
      distance = 0; // Paranoia: don't touch correlated parameters if limit could not be determined
      break;
    }
    step /= 2.;
    if (step < 1.e-8*distance) // Is step still big enough to make a difference?
    {
      break;
    }
  }
  distance = lastgooddist;

  for (unsigned s = 0; s < deltai.size(); s++)
  if (deltai[s] != 0) // 0 == flag up against limits
  {
    int ii = index[s];
    distance = std::min(distance,dist[ii]); //in case limit is lower
  }
  // Set distance for all B-factors moving to smaller values to the distance determined
  // (B-factors that are increasing won't create variance problems)
  for (unsigned s = 0; s < deltai.size(); s++)
  if (deltai[s] != 0) // 0 == flag
  {
    int ii = index[s];
    if (g[ii] > 0)
    {
      dist[ii] = distance;
      if (distance < MAX_SHIFT) bounded[ii] = true;
    }
  }
  return std::max(0.0,distance);
}

void RefineMR::setUp()
{
  //for numerical stability
  if (!npars_ref) return;
  TNT::Vector<floatType> old_x =  getRefinePars();
  //if (!old_x.size()) return;
  applyShift(old_x); //calling search setup fast_last_applyShift internally
  if (FAST_LAST)
  { //fill up the known arrays with the fixed components and the search arrays
    if (!protocol.FIX_VRMS) ensemble->setup_vrms(models_drms);
    fast_last_known_setUp();
  }
}

void RefineMR::fast_last_known_setUp()
{
  ///always required
  sum_Esqr_search.clear();
  max_Esqr_search.clear();
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    EM_known[r] = cmplxType(0,0);
  }

  std::vector<int> klooplist;
  for (int kk = 0; kk < LAST_KNOWN; kk++)
    if (protocol.FIX_VRMS || (!protocol.FIX_VRMS && models_known[kk].MODLID != LAST_MODLID))
      klooplist.push_back(kk);

  for (unsigned kk = 0; kk < klooplist.size(); kk++)
  {
    int k = klooplist[kk];
    std::string modlid = models_known[k].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    //recover original centre of mass, rotated centre of mass
    dmat33 PRtr = ens_modlid->PR.transpose();
    dvect3 CMori = -PRtr*ens_modlid->PT;
    dmat33 ROT = models_known[k].R;
    dvect3 CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    dmat33 Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,models_perturbRot[k][dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    dvect3 CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented (*ensemble))
    ROT = ROT*PRtr;

    dvect3 TRA = models_known[k].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation (for refinement)
    iOrthT += models_perturbTrans[k];
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of (*ensemble))
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    dmat33 Q1tr = Q1.transpose();

    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      floatType sqrt_scatFactor = std::sqrt(ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
      std::vector<miller::index<int> > rhkl = rotMiller(r);
      floatType terms  = sqrt_scatFactor;
                terms *= sqrt_epsn[r];
                terms *= std::exp(-models_known[k].BFAC*rssqr/4.0);
                terms *= std::sqrt(models_known[k].OFAC);
                terms /= models_known[k].MULT;
      if (PTNCS.use_and_present())
                terms *= std::sqrt(G_DRMS[r]);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3    RotSymHKL = Q1tr*rhkl[isym];
          cmplxType thisE =  ens_modlid->InterpE(RotSymHKL,DFAC[r]);
                    thisE *= terms;
                    thisE *= dphi(r,isym,rhkl[isym],TRA);
          //no G function correction (gfun), models in separate (real) rotations
          EM_known[r] += thisE;
        }
      }
    }
  }

  //calcKnownVAR
  if (!protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_CELL || !protocol.FIX_OFAC)
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      totvar_known[r] = 0;
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      //save this interpolation, so not doing multiple times for multiple copies
      map_str_float Vmap;
    //stored  if (PTNCS.use_and_present()) gfun.calcReflTerms(r);
      for (unsigned kk = 0; kk < klooplist.size(); kk++)
      {
        int k = klooplist[kk];
        std::string modlid = models_known[k].MODLID;
        Ensemble* ens_modlid = &ensemble->find(modlid)->second;
        ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
        floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT;
        if (Vmap.find(modlid) == Vmap.end())
        {
          floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
                    thisV *= scatFactor;
          Vmap[modlid] = thisV;
        }
        floatType expterm = exp(-2.0*models_known[k].BFAC*rssqr/4.0);
        floatType thisV = Vmap[modlid];
                  thisV *= expterm;
                  thisV *= models_known[k].OFAC;
                  thisV /= models_known[k].MULT;
        if (PTNCS.use_and_present())
                  thisV *= G_DRMS[r]*G_Vterm[r]; //not correct, B-factor differences
        totvar_known[r] += thisV;
      }
    }
  }
}

void RefineMR::fast_last_applyShift()
{
  PHASER_ASSERT(FAST_LAST); //a subset changes
  for (unsigned r = 0; r < NREFL; r++) //initialize only required
    if (selected[r])
      EM_search[r] = cmplxType(0,0);

  std::vector<int> klooplist;
  for (int kk = 0; kk < LAST_KNOWN; kk++)
    if (!protocol.FIX_VRMS && models_known[kk].MODLID == LAST_MODLID)
      klooplist.push_back(kk);
  for (int kk = LAST_KNOWN; kk < models_known.size(); kk++)
    klooplist.push_back(kk);

  Ensemble* ens_modlid = &ensemble->find(LAST_MODLID)->second;
  //calcKnown
  for (unsigned kk = 0; kk < klooplist.size(); kk++)
  {
    int k = klooplist[kk];
    std::string modlid = models_known[k].MODLID;
    PHASER_ASSERT(modlid == LAST_MODLID);
    //recover original centre of mass, rotated centre of mass
    dmat33 PRtr = ens_modlid->PR.transpose();
    dvect3 CMori = -PRtr*ens_modlid->PT;
    dmat33 ROT = models_known[k].R;
    dvect3 CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    dmat33 Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,models_perturbRot[k][dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    dvect3 CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented (*ensemble))
    ROT = ROT*PRtr;

    dvect3 TRA = models_known[k].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation (for refinement)
    iOrthT += models_perturbTrans[k];
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of (*ensemble))
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    dmat33 Q1tr = Q1.transpose();

    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      floatType sqrt_scatFactor = std::sqrt(ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
      std::vector<miller::index<int> > rhkl = rotMiller(r);
      floatType terms  = sqrt_scatFactor;
                terms *= sqrt_epsn[r];
                terms *= std::exp(-models_known[k].BFAC*rssqr/4.0);
                terms *= std::sqrt(models_known[k].OFAC);
                terms /= models_known[k].MULT;
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3    RotSymHKL = Q1tr*rhkl[isym];
          cmplxType thisE =  ens_modlid->InterpE(RotSymHKL,DFAC[r]);
                    thisE *= terms;
                    thisE *= dphi(r,isym,rhkl[isym],TRA);
          //no G function correction (gfun), models in separate (real) rotations
          if (PTNCS.use_and_present())
                    thisE *= std::sqrt(G_DRMS[r]);
          EM_search[r] += thisE;
        }
      }
    }
  }
  //calcKnownVAR
  if (!protocol.FIX_BFAC || !protocol.FIX_VRMS || !protocol.FIX_CELL || !protocol.FIX_OFAC)
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      totvar_search[r] = 0;
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      //save this interpolation, so not doing multiple times for multiple copies
      map_str_float Vmap;
    //stored  if (PTNCS.use_and_present()) gfun.calcReflTerms(r);
      for (unsigned kk = 0; kk < klooplist.size(); kk++)
      {
        int k = klooplist[kk];
        std::string modlid = models_known[k].MODLID;
        PHASER_ASSERT(modlid == LAST_MODLID); //ens_modlid is the right one
        ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
        floatType scatFactor = (ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT);
        if (Vmap.find(modlid) == Vmap.end())
        {
          floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
                    thisV *= scatFactor;
          Vmap[modlid] = thisV;
        }
        floatType expterm = exp(-2.0*models_known[k].BFAC*rssqr/4.0);
        floatType thisV = Vmap[modlid];
                  thisV *= expterm;
                  thisV *= models_known[k].OFAC;
                  thisV /= models_known[k].MULT;
        if (PTNCS.use_and_present())
                  thisV *= G_DRMS[r]*G_Vterm[r]; //not correct, B-factor differences
        totvar_search[r] += thisV;
      }
    }
  }
}

bool RefineMR::refine_datacell()
{
  if (protocol.FIX_CELL) return false;
  return ensemble->has_pdb();
}

}//phaser
