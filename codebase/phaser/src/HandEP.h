//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __HandEP__Class__
#define __HandEP__Class__
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/af_atom.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/Bin.h>
#include <phaser/src/BinStats.h>
#include <phaser/pod/llgc_coefs.h>
#include <phaser/include/VarianceEP.h>
#include <cctbx/eltbx/fp_fdp.h>
#include <cctbx/hendrickson_lattman.h>

namespace phaser {

class HandEP : public VarianceEP, public SpaceGroup
{
  public:
    HandEP() { }
    virtual ~HandEP() {}

    void setHandEP(const HandEP & init)
    {
      selected = init.selected;
      acentStats = init.acentStats;
      centStats = init.centStats;
      allStats = init.allStats;
      FWT = init.FWT.deep_copy();
      PHWT = init.PHWT.deep_copy();
      PHIB = init.PHIB.deep_copy();
      FOM = init.FOM.deep_copy();
      FPFOM = init.FPFOM.deep_copy();
      HL = init.HL.deep_copy();
      LLGCMAPS = init.LLGCMAPS.deep_copy();
      PDBfile = init.PDBfile;
      MTZfile = init.MTZfile;
      MAPfile = init.MAPfile;
      SOLfile = init.SOLfile;
      atoms = init.atoms;
      singleStats = init.singleStats;
      HL_ANOM = init.HL_ANOM.deep_copy();
      AtomFp = init.AtomFp;
      AtomFdp = init.AtomFdp;
      t2atomtype = init.t2atomtype;
      setVarianceEP(init.getVarianceEP());
      setSpaceGroup(init.getSpaceGroup());
    }

    HandEP(const HandEP & init)
    { setHandEP(init); }

    const HandEP& operator=(const HandEP& right)
    {
      if (&right != this) setHandEP(right);
      return *this;
    }

    const HandEP& getHandEP() const
    { return *this; }

  public:
    af_bool       selected;
    BinStats      acentStats,centStats,allStats;
    af_float      FWT,PHWT,PHIB,FOM,FPFOM;
    af_llgc_coefs LLGCMAPS;
    std::string   PDBfile,MAPfile,MTZfile,SOLfile;
    BinStats      singleStats;
    af::shared<cctbx::hendrickson_lattman<double> >         HL_ANOM;
    af::shared<cctbx::hendrickson_lattman<double> >         HL;
    float1D       AtomFp,AtomFdp;
    map_int_str   t2atomtype; //lookup array for atomtypes
    af_atom       atoms;      //these are the accepted and rejected atoms and all associated flags

    bool          mtz_is_set()       { return selected.size() && FWT.size(); }
    bool          atoms_is_set()     { return atoms.size(); }
    map_str_int   getAtomNum(); //lookup array for atomtypes
    af::shared<xray::scatterer<double> > getAtoms(); //!REJECTED atoms

    std::map<std::string,cctbx::eltbx::fp_fdp> getFpFdp();
    bool          scat_is_set()  { return getFpFdp().size(); }
    floatType     getLogLikelihood() const    { return atoms.LLG; }
};

}
#endif
