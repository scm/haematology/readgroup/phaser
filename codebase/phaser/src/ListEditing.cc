//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/ListEditing.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <phaser/lib/rotdist.h>
#include <phaser/lib/between.h>
#include <phaser/lib/round.h>
#include <cctbx/sgtbx/search_symmetry.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

dvect31D ListEditing::add_known_orientations_to_rlist(map_str_pdb& PDB,
                      std::string modlid,af::double6 CELL,floatType dmin,mr_set& MRSET)
{
  dvect31D nadd(0);
  SpaceGroup sg(MRSET.HALL);
  UnitCell   uc(CELL);
  cctbx::sgtbx::space_group SgOps = sg.getCctbxSG();
  cctbx::sgtbx::structure_seminvariants semis(SgOps);
  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
  SpaceGroup sgEuclid(ssym.subgroup().type().hall_symbol());

  // Check that orientations are sufficiently similar
  // Determine sensible distance thresholds from dmin and model mean radius
  // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
  floatType rotMax = PDB[modlid].mean_radius() ? 2.0*std::atan(dmin/(4.*PDB[modlid].mean_radius())) : scitbx::constants::two_pi;

  for (unsigned k = 0; k < MRSET.KNOWN.size(); k++)
  if (modlid == MRSET.KNOWN[k].MODLID)
  {
    bool in_list(false);
    for (unsigned isym = 0; isym < sgEuclid.NSYMM; isym++)
    {
      dmat33 Rotsym_chk = sgEuclid.Rotsym(isym);
      dmat33 solR_chk_sym = uc.Frac2Orth()*Rotsym_chk.transpose()*uc.Orth2Frac()*MRSET.KNOWN[k].R;
      for (unsigned ensym = 0; ensym < PDB[modlid].PG.multiplicity(); ensym++)
      {
        dmat33 ensymR(euler2matrixDEG(PDB[modlid].PG.get_euler(ensym)));
        dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
        for (unsigned chk = 0; chk < MRSET.RLIST.size(); chk++) //check all, may already be picked
        {
          floatType rotdist = rotmatdistRAD(euler2matrixDEG(MRSET.RLIST[chk].EULER),solR_chk_sym_ensym);
          if (rotdist < rotMax) in_list = true;
        }
      }
    }
    if (!in_list)
    {
      dvect3 euler = matrix2eulerDEG(MRSET.KNOWN[k].R);
      mr_rlist this_rlist(modlid,euler);
      MRSET.RLIST.push_back(this_rlist);
      nadd.push_back(euler);
    }
  }
  return nadd;
}

pair_int_int ListEditing::calculate_duplicates(map_str_pdb& PDB,
                          af::double6 CELL,floatType dmin,mr_solution& MRSET,mr_solution MRTEMPLATE,Output* output)
{
  int nmove(0);
  MRSET.sort_LLG();
  UnitCell uc(CELL);
  bool template_input(MRTEMPLATE.size());

  //here is a fudge for getting a solution close to original coordinates
  //for simple cases will be OK - non-isomorphism requiring MR
  //slacken the critera for matching the templates over merging solutions
  //similarity match will be more forgiving than the R-factor test used to pick up
  //absolutely identical solutions
  if (do_default_template_match && !MRTEMPLATE.size() && MRSET.size())
  {
    MRTEMPLATE.push_back(MRSET[0]);
    for (int k = 0; k < MRTEMPLATE[0].KNOWN.size(); k++)
    { //this obviously has overlapping molecules if the modlids are the same!
      //sensible for simplest of solutions only
      MRTEMPLATE[0].KNOWN[k].R =   dmat33(1,0,0,0,1,0,0,0,1);
      MRTEMPLATE[0].KNOWN[k].TRA = dvect3(0,0,0);
    }
  }
  //compare with the template solution (if present) by adding the template solution
  //to the real solution list (temporarily) and including it in the analysis below
  int tsize = MRSET.size();
  for (int t = 0; t < MRTEMPLATE.size(); t++)
    MRSET.push_back(MRTEMPLATE[t]);

  int tmplt_tsize = MRSET.size(); //includes template at the end
  for (unsigned k = 0; k < tsize; k++)
  {
    MRSET[k].KEEP = true;
    MRSET[k].EQUIV = 0;
    MRSET[k].TMPLT.clear();
  }
  for (unsigned k = 0; k < tsize; k++) // Make sure all translations stored as fractional
  {
    for (unsigned s = 0; s < MRSET[k].KNOWN.size(); s++)
    {
      if (!MRSET[k].KNOWN[s].FRAC)
      {
        dvect3 tmp = MRSET[k].KNOWN[s].TRA;
        dvect3 tmp2 = uc.doOrth2Frac(tmp);
        MRSET[k].KNOWN[s].setFracT(tmp2);
      }
    }
  }

  // Compare solutions to find duplicates further down list
  int nthreads = 1;
#pragma omp parallel
  {
#ifdef _OPENMP
  nthreads = omp_get_num_threads();
#endif
  }
  if (nthreads > 1 && output)
    output->logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");
  if (output) output->logProgressBarStart(LOGFILE,"Calculating Duplicates for " + itos(tsize) + " solutions",tsize/nthreads);

#ifdef __APPLE__
  #pragma omp for
#else
  #pragma omp parallel for
#endif
  for (int k = 0; k < tsize; k++) //not interested in finding duplicates of templates
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
#endif
    bool Space_Group_of_Solution_Set(MRSET[k].HALL != "");
    PHASER_ASSERT(Space_Group_of_Solution_Set);
    cctbx::sgtbx::space_group SgOps(MRSET[k].HALL,"A1983");
    cctbx::sgtbx::structure_seminvariants semis(SgOps);
    cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
    cctbx::sgtbx::space_group_type SgInfo(SgOps);
    cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
    SpaceGroup sgEuclid(ssym.subgroup().type().hall_symbol());
    // Repeat setup work for reference setting.
    cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
    cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
    cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
    cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
    dmat33 cb_op_r_to_reference_setting(SgInfo.cb_op().c().r().as_double());
    dmat33 cb_op_r_from_reference_setting(SgInfo.cb_op().c_inv().r().as_double());

    std::map<std::string,int> comp1;
    int nmodels = MRSET[k].KNOWN.size();
    for (unsigned s = 0; s < nmodels; s++)
      comp1[MRSET[k].KNOWN[s].MODLID]++;
    for (unsigned k_chk = k+1; k_chk < tmplt_tsize; k_chk++) //check those further down the list, including templates
    {
      bool template_chk = (k_chk >= tsize);
      if (!MRSET[k_chk].KEEP) continue;
      // skip comparison if not the same number of models in trial solutions
      if (MRSET[k_chk].KNOWN.size() != nmodels) continue;
      // skip comparison if different space groups
      if (MRSET[k].HALL.size() && MRSET[k_chk].HALL.size() && MRSET[k_chk].HALL != MRSET[k].HALL) continue;
      // skip comparison if different composition
      std::map<std::string,int> comp2;
      for (unsigned s = 0; s < nmodels; s++)
        comp2[MRSET[k_chk].KNOWN[s].MODLID]++;
      if (comp1.size() != comp2.size()) break;
      bool matched(true);
      for (std::map<std::string,int>::iterator iter=comp1.begin(); iter != comp1.end(); iter++)
        if (comp2.find(iter->first) == comp2.end() ||
            comp2[iter->first] != iter->second) matched = false;
      if (!matched) continue;
      bool1D used(nmodels,false);
      bool allmatched(true),originDefined(true); //not important initialization
      dvect3 zero3(0,0,0), originShift(0,0,0);
      floatType transMax(dmin/5.);
      int isymEuclid(-1);
      // Store some information about first model in reference solution
      std::string firstmodlid = MRSET[k].KNOWN[0].MODLID;
      dmat33 solR_first = MRSET[k].KNOWN[0].R;
      dvect3 fracSolC_first;
      {
        dvect3 fracT(MRSET[k].KNOWN[0].TRA);
        dvect3 orthSolC(solR_first*PDB[MRSET[k].KNOWN[0].MODLID].COORD_CENTRE + uc.doFrac2Orth(fracT));
        fracSolC_first = uc.doOrth2Frac(orthSolC);
      } // scope
      // Find possible choices of origin shift of checked solution that make a match with first reference model
      for (unsigned s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
      {
        allmatched = false;
        originDefined = false;
        originShift = zero3;

        if (MRSET[k_chk].KNOWN[s_chk_ori].MODLID != firstmodlid) continue;
        dmat33 solR_chk = MRSET[k_chk].KNOWN[s_chk_ori].R;
        dvect3 fracT_chk_ori(MRSET[k_chk].KNOWN[s_chk_ori].TRA);
        dvect3 orthSolC_chk = solR_chk*PDB[MRSET[k_chk].KNOWN[s_chk_ori].MODLID].COORD_CENTRE + uc.doFrac2Orth(fracT_chk_ori);
        dvect3 fracSolC_chk(uc.doOrth2Frac(orthSolC_chk));

        floatType ONE(1);
        for (unsigned isym = 0; isym < sgEuclid.NSYMM; isym++)
        {
          // Check that orientations are sufficiently similar
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          floatType rotMax = PDB[MRSET[k].KNOWN[0].MODLID].mean_radius() ?
            2.0*std::atan(dmin/(4.*PDB[MRSET[k].KNOWN[0].MODLID].mean_radius())) : scitbx::constants::two_pi;
          //when looking for a template match, be more forgiving
          //*** need to change this factor in more than one place!!
          //factor is less than half symm rot
          if (template_chk) rotMax = DEF_TMPL_SAME_ROT;
          dmat33 Rotsym_chk = sgEuclid.Rotsym(isym);
          dmat33 solR_chk_sym = uc.Frac2Orth()*Rotsym_chk.transpose()*uc.Orth2Frac()*solR_chk;
          std::string modlid = MRSET[k_chk].KNOWN[s_chk_ori].MODLID;
          for (unsigned ensym = 0; ensym < PDB[modlid].PG.multiplicity(); ensym++)
          {
            dmat33 ensymR(euler2matrixDEG(PDB[modlid].PG.get_euler(ensym)));
            dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
            floatType rotdist = rotmatdistRAD(solR_first,solR_chk_sym_ensym);
            if (rotdist > rotMax) continue;

            // Check that centres of mass are sufficiently similar
            // Centre of mass of ensemble won't change with application
            // of point group symmetry.
            dvect3 fracSolC_chk_sym = sgEuclid.doSymXYZ(isym,fracSolC_chk);
            dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
              while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
            }
            // Account for any continuous shifts as origin shift
            dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
            dvect3 originShift_reference(0,0,0);
            af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
            for (unsigned j = 0; j < 3; j++) {
              if (isshift[j]) {
                originShift_reference[j] = -fracdelta_reference[j];
                fracdelta_reference[j] = 0;
              }
            }
            fracdelta=cb_op_r_from_reference_setting*fracdelta_reference;
            originShift=cb_op_r_from_reference_setting*originShift_reference;
            dvect3 delta = uc.doFrac2Orth(fracdelta);
            floatType tdist = delta.length();
            //when looking for a template match, be more forgiving
            //*** need to change this in more than one place
            if (template_chk) transMax = PDB[modlid].min_extent()/2.0;
            if (tdist <= transMax)
            {
              isymEuclid = isym;
              originDefined = true;
              break;
            }
          } // ensym
          if (originDefined) break;
        } // isym
        if (!originDefined) continue; // Test different origins

        // Now use choice of relative origin (if defined) to test equivalence
        allmatched = true; //So far at least one match.  Negate if one fails.
        for (unsigned s = 0; s < nmodels; s++)
        { // For each model in the reference, check whether any matching models are the same
          // Determine sensible distance thresholds from dmin and model mean radius
          floatType rotMax = PDB[MRSET[k].KNOWN[0].MODLID].mean_radius() ?
            2.0*std::atan(dmin/(4.*PDB[MRSET[k].KNOWN[0].MODLID].mean_radius())) : scitbx::constants::two_pi;
          //when looking for a template match, be more forgiving
          //*** need to change this factor in more than one place!!
          if (template_chk) rotMax = DEF_TMPL_SAME_ROT;
          dmat33 solR = MRSET[k].KNOWN[s].R;
          dvect3 fracSolC;
          dvect3 fracT(MRSET[k].KNOWN[s].TRA);
          dvect3 orthSolC = solR*PDB[MRSET[k].KNOWN[s].MODLID].COORD_CENTRE + uc.doFrac2Orth(fracT);
          fracSolC = uc.doOrth2Frac(orthSolC);
          std::string modlid = MRSET[k].KNOWN[s].MODLID;
          matched = false;
          for (unsigned s_chk = 0; s_chk < nmodels; s_chk++)
          {
            if (used[s_chk] || MRSET[k_chk].KNOWN[s_chk].MODLID != modlid) continue;
            dmat33 solR_chk = MRSET[k_chk].KNOWN[s_chk].R;
            dvect3 fracSolC_chk;
            dvect3 fracT(MRSET[k_chk].KNOWN[s_chk].TRA);
            dvect3 orthSolC_chk = solR_chk*PDB[MRSET[k_chk].KNOWN[s_chk].MODLID].COORD_CENTRE + uc.doFrac2Orth(fracT);
            fracSolC_chk = uc.doOrth2Frac(orthSolC_chk);

            if (originDefined)
            {
              //  Apply operation selected for first molecule from space group expanded to include allowed origin shifts
              solR_chk = uc.Frac2Orth()*sgEuclid.Rotsym(isymEuclid).transpose()*uc.Orth2Frac()*solR_chk;
              fracSolC_chk = sgEuclid.doSymXYZ(isymEuclid,fracSolC_chk);
            }

            SpaceGroup sg = !originDefined ? sgEuclid : SpaceGroup(MRSET[k].HALL);
            floatType ONE(1);
            for (unsigned isym = 0; isym < sg.NSYMM; isym++)
            {
              // Check that orientations are sufficiently similar
              dmat33 Rotsym_chk = sg.Rotsym(isym);
              dmat33 solR_chk_sym = uc.Frac2Orth()*Rotsym_chk.transpose()*uc.Orth2Frac()*solR_chk;
              std::string modlid = MRSET[k_chk].KNOWN[s_chk].MODLID;
              for (unsigned ensym = 0; ensym < PDB[modlid].PG.multiplicity(); ensym++)
              {
                dmat33 ensymR(euler2matrixDEG(PDB[modlid].PG.get_euler(ensym)));
                dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
                floatType rotdist = rotmatdistRAD(solR,solR_chk_sym_ensym);
                if (rotdist > rotMax) continue;

                // Check that centres of mass are
                // sufficiently similar. Centre of mass of ensemble won't
                // change with application of point group symmetry.
                dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
                dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
                for (unsigned j = 0; j < 3; j++)
                {
                  while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
                  while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
                }
                dvect3 delta = uc.doFrac2Orth(fracdelta);
                floatType tdist = delta.length();
                if (template_chk) transMax = PDB[modlid].min_extent()/2.0;
                if (tdist > transMax) continue;
                matched = true;
                used[s_chk] = true;
                if (matched) break;
              } // ensym
              if (matched) break;
            } // isym
            if (matched) break;
          } // s_chk
          if (!matched) allmatched = false;
        } // s
        if (allmatched) break;
        // Failed to match everything.  Try again with another possible origin shift.
        for (unsigned m = 0; m < nmodels; m++)
          used[m] = false;
      } // s_chk_ori
      allmatched = allmatched && (MRSET[k].PACKS == MRSET[k_chk].PACKS);
      if (allmatched)
      {
#pragma omp critical
        {
        if (k_chk < tsize) //don't exclude template, needed for non-unique peaks as well
          MRSET[k_chk].KEEP = 0;
        MRSET[k_chk].EQUIV = k;
        //replace the kept TFZ with the TFZ highest from those found to be equivalent
        MRSET[k].TFZ = std::max(MRSET[k_chk].TFZ,MRSET[k].TFZ);
        if (k_chk >= tsize)
        {
          if (template_input) //i.e. not created by default
          {
          floatType tnum = k_chk - tsize + 1;
          MRSET[k].TMPLT.push_back(tnum);
          MRSET[k].ANNOTATION += " *T=" + itos(tnum);
          nmove++;
          }
    //there is a bug in this code below, don't move to the template, just flag
    //      move_to_template(PDB,CELL,dmin,MRSET[k],MRSET[k_chk]);
        }
        }
      }
    } //k_chk
    if (nthread == 0 && output) output->logProgressBarNext(LOGFILE);
  } // end #pragma omp parallel
  if (output) output->logProgressBarEnd(LOGFILE);
  if (output) output->logBlank(VERBOSE);

  //after paralled section, when all KEEP is flagged
  for (int k = 0; k < MRSET.size(); k++)
    MRSET[k].NUM = -999;
  //first pass for known
  int na(1);
  for (int k = 0; k < MRSET.size(); k++)
    if (MRSET[k].KEEP)
      MRSET[k].NUM = na++; //output number equivalent
  bool all_numbered(true);
  int count_loop(0);
  do
  {
    all_numbered = true;
    for (int k = 0; k < MRSET.size(); k++)
      if (!MRSET[k].KEEP) //if this is equivalent to something
          MRSET[k].NUM = MRSET[MRSET[k].EQUIV].NUM; //may be negative
    for (int k = 0; k < MRSET.size(); k++)
      if (MRSET[k].NUM < 0)
        all_numbered = false; //flag that we are not yet done
    PHASER_ASSERT(count_loop++ < 100); //infinite loop check
  }
  while (!all_numbered);

  int ndel(0);
  for (int t = 0; t < MRTEMPLATE.size(); t++)
  {
    MRSET.pop_back(); // deletes the template
  }
  for (int k = 0; k < MRSET.size(); k++)
    if (!MRSET[k].KEEP) ndel++;
  return pair_int_int(ndel,nmove);
}

void ListEditing::move_to_template(map_str_pdb& PDB,
                          af::double6 CELL,floatType dmin,mr_set& MRSET,mr_set& MRTEMPLATE)
{
  UnitCell uc(CELL);
  cctbx::sgtbx::space_group SgOps(MRSET.HALL,"A1983");
  cctbx::sgtbx::structure_seminvariants semis(SgOps);
  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
  SpaceGroup sgEuclid(ssym.subgroup().type().hall_symbol());
  // Repeat setup work for reference setting.
  cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
  cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
  cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
  cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
  dmat33 cb_op_r_to_reference_setting(SgInfo.cb_op().c().r().as_double());
  dmat33 cb_op_r_from_reference_setting(SgInfo.cb_op().c_inv().r().as_double());
  PHASER_ASSERT(MRSET.KNOWN.size() == MRTEMPLATE.KNOWN.size());
  int nmodels = MRSET.KNOWN.size();
  int isymEuclid(-1);
  double rotMax = DEF_TMPL_SAME_ROT;
  dvect3 originShift(0,0,0);
  { // memory
  dmat33 solR_first = MRTEMPLATE.KNOWN[0].R;
  std::string modlid = MRTEMPLATE.KNOWN[0].MODLID;
  dvect3 fracSolC_first = uc.Orth2Frac()*(solR_first*PDB[modlid].COORD_CENTRE + uc.Frac2Orth()*MRTEMPLATE.KNOWN[0].TRA);
  bool originDefined(false);
  for (unsigned s_chk_ori = 0; s_chk_ori < nmodels && !originDefined; s_chk_ori++)
  {
    if (MRSET.KNOWN[s_chk_ori].MODLID != modlid) continue;
    dmat33 solR_chk = MRSET.KNOWN[s_chk_ori].R;
    dvect3 fracSolC_chk = uc.Orth2Frac()*(solR_chk*PDB[modlid].COORD_CENTRE + uc.Frac2Orth()*MRSET.KNOWN[s_chk_ori].TRA);
    for (unsigned isym = 0; isym < sgEuclid.NSYMM && !originDefined; isym++)
    {
      // Check that orientations are sufficiently similar
      // Determine sensible distance thresholds from dmin and model mean radius
      // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
      dmat33 Rotsym_chk = sgEuclid.Rotsym(isym);
      dmat33 solR_chk_sym = uc.Frac2Orth()*Rotsym_chk.transpose()*uc.Orth2Frac()*solR_chk;
      for (unsigned ensym = 0; ensym < PDB[modlid].PG.multiplicity(); ensym++)
      {
        dmat33 ensymR(euler2matrixDEG(PDB[modlid].PG.get_euler(ensym)));
        dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
        floatType rotdist = rotmatdistRAD(solR_first,solR_chk_sym_ensym);
        if (rotdist > rotMax) continue;

        // Check that centres of mass are sufficiently similar
        // Centre of mass of ensemble won't change with application
        // of point group symmetry.
        dvect3 fracSolC_chk_sym = sgEuclid.doSymXYZ(isym,fracSolC_chk);
        dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
        for (unsigned j = 0; j < 3; j++)
        {
          while (fracdelta[j] < -0.5) fracdelta[j] += 1;
          while (fracdelta[j] >= 0.5) fracdelta[j] -= 1;
        }
        // Account for any continuous shifts as origin shift
        dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
        dvect3 originShift_reference(0,0,0);
        af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
        for (unsigned j = 0; j < 3; j++) {
          if (isshift[j]) {
            originShift_reference[j] = -fracdelta_reference[j];
            fracdelta_reference[j] = 0;
          }
        }
        fracdelta = cb_op_r_from_reference_setting*fracdelta_reference;
        originShift = cb_op_r_from_reference_setting*originShift_reference; //left with final value
        dvect3 delta = uc.doFrac2Orth(fracdelta);
        floatType tdist = delta.length();
        //when looking for a template match, be more forgiving
        //*** need to change this in more than one place
        double transMax = PDB[modlid].min_extent()/2.0;
        if (tdist <= transMax)
        {
          isymEuclid = isym;
          originDefined = true;
          break;
        }
      } // ensym
     } // isym
  } // s_chk_ori
  } // memory

  // Now use choice of relative origin (if defined) to test equivalence
  for (unsigned s_ref = 0; s_ref < nmodels; s_ref++)
  {
    bool matched(false);
    for (unsigned s_chk = 0; s_chk < nmodels && !matched; s_chk++)
    { // For each model in the reference, check whether any matching models are the same
      // Determine sensible distance thresholds from dmin and model mean radius
      if (MRSET.KNOWN[s_chk].MODLID != MRTEMPLATE.KNOWN[s_ref].MODLID) continue;
      std::string modlid = MRSET.KNOWN[s_chk].MODLID;
      floatType rotMax = PDB[modlid].mean_radius() ?  2.0*std::atan(dmin/(4.*PDB[modlid].mean_radius())) : scitbx::constants::two_pi;
      dmat33 solR_ref = MRTEMPLATE.KNOWN[s_ref].R;
      dvect3 fracSolC_ref = uc.Orth2Frac()*(solR_ref*PDB[MRTEMPLATE.KNOWN[s_ref].MODLID].COORD_CENTRE + uc.Frac2Orth()*MRTEMPLATE.KNOWN[s_ref].TRA);
      dmat33 solR_chk = MRSET.KNOWN[s_chk].R;
      dvect3 fracSolC_chk = uc.Orth2Frac()*(solR_chk*PDB[modlid].COORD_CENTRE + uc.Frac2Orth()*MRSET.KNOWN[s_chk].TRA);
      SpaceGroup sg = SpaceGroup(MRSET.HALL);
      //apply origin shift
      if (isymEuclid >= 0)
      {
        solR_chk = uc.Frac2Orth()*sgEuclid.Rotsym(isymEuclid).transpose()*uc.Orth2Frac()*solR_chk;
        fracSolC_chk = sgEuclid.doSymXYZ(isymEuclid,fracSolC_chk);
        sg = sgEuclid;
      }
      for (unsigned isym = 0; isym < sg.NSYMM && !matched; isym++)
      {
        // Check that orientations are sufficiently similar
        dmat33 Rotsym_chk = sg.Rotsym(isym);
        dmat33 solR_chk_sym = uc.Frac2Orth()*Rotsym_chk.transpose()*uc.Orth2Frac()*solR_chk;
        for (unsigned ensym = 0; ensym < PDB[modlid].PG.multiplicity() && !matched; ensym++)
        {
          dmat33 ensymR(euler2matrixDEG(PDB[modlid].PG.get_euler(ensym)));
          dmat33 solR_chk_sym_ensym = solR_chk_sym*ensymR;
          floatType rotdist = rotmatdistRAD(MRTEMPLATE.KNOWN[s_ref].R,solR_chk_sym_ensym);
          if (rotdist > rotMax) continue;

          // Check that centres of mass are
          // sufficiently similar. Centre of mass of ensemble won't
          // change with application of point group symmetry.
          dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
          dvect3 fracdelta = fracSolC_ref - fracSolC_chk_sym + originShift;
          for (unsigned j = 0; j < 3; j++)
          {
            while (fracdelta[j] < -0.5) fracdelta[j] += 1;
            while (fracdelta[j] >= 0.5) fracdelta[j] -= 1;
          }
          dvect3 delta = uc.doFrac2Orth(fracdelta);
          floatType tdist = delta.length();
          //when looking for a template match, be more forgiving
          //*** need to change this in more than one place
          double transMax = PDB[modlid].min_extent()/2.0;
          if (tdist < transMax)
          {
            matched = true;
            dvect3 fracSolC = uc.Orth2Frac()*(-solR_chk_sym_ensym*PDB[modlid].COORD_CENTRE + uc.Frac2Orth()*(fracSolC_chk_sym));
            dvect3 shift = fracSolC - MRTEMPLATE.KNOWN[s_ref].TRA;
            for (unsigned j = 0; j < 3; j++) shift[j] = round(shift[j]);
            shift += originShift;
//std::cout << "xxxxxxxxxx " << dvtos(matrix2eulerDEG(MRSET.KNOWN[s_chk].R)) << " new " << dvtos(matrix2eulerDEG(solR_chk_sym_ensym)) << std::endl;
            MRSET.KNOWN[s_chk].R = (solR_chk_sym_ensym);
            MRSET.KNOWN[s_chk].TRA = (fracSolC-shift);
          }
        } // ensym
      } // isym
    }// s_chk
  } // s_ref
}

int1D ListEditing::purge_duplicates(double transMax,std::string SG_HALL,af::double6 CELL,ep_solution& EPSET, Output* output)
{
  EPSET.sort_LLG();
  UnitCell uc(CELL);
  int1D EQUIV(EPSET.size(),-999); //flag not equivalent (keep)
  int tsize = EPSET.size();

  cctbx::sgtbx::space_group SgOps(SG_HALL,"A1983");
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  //add centre of symmetry so that other hand identified as duplicate
  if (!SgInfo.is_enantiomorphic()) SgOps.expand_inv(cctbx::sgtbx::tr_vec(0,0,0));
  SgInfo = cctbx::sgtbx::space_group_type(SgOps);
  cctbx::sgtbx::structure_seminvariants semis(SgOps);
  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
  SpaceGroup sgEuclid(ssym.subgroup().type().hall_symbol());
  // Repeat setup work for reference setting.
  cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
  cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
  cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
  cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
  dmat33 cb_op_r_to_reference_setting(SgInfo.cb_op().c().r().as_double());
  dmat33 cb_op_r_from_reference_setting(SgInfo.cb_op().c_inv().r().as_double());

  // Compare solutions to find duplicates further down list
  int nthreads = 1;
//#define AJM_BREAK_THIS_WITH_OMP //!!!!
#ifdef AJM_BREAK_THIS_WITH_OMP
#pragma omp parallel
  {
#ifdef _OPENMP
  nthreads = omp_get_num_threads();
#endif
  }
#endif
  if (nthreads > 1 && output)
    output->logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");
  if (output) output->logProgressBarStart(LOGFILE,"Calculating Duplicates for " + itos(tsize) + " solutions",tsize/nthreads);

#if AJM_BREAK_THIS_WITH_OMP
  std::vector<ep_solution> omp_EPSET(nthreads);
  for (int n = 0; n < nthreads; n++)
    omp_EPSET[n] = EPSET;

  int2D omp_EQUIV(tsize);

#ifdef __APPLE__
  #pragma omp for
#else
  #pragma omp parallel for
#endif
#endif
  for (int k = 0; k < tsize; k++)
  {
    int nthread = 0;
#if AJM_BREAK_THIS_WITH_OMP
#ifdef _OPENMP
    nthread = omp_get_thread_num();
#endif
#endif
    int nmodels = EPSET[k].size();
    for (unsigned k_chk = k+1; k_chk < tsize; k_chk++) //check those further down the list, including templates
    {
      if (EQUIV[k_chk]>=0) continue;
      // skip comparison if not the same number of models in trial solutions
      if (EPSET[k_chk].size() != nmodels) continue;
      bool matched(true);
      //bool1D used(nmodels,false);
      bool allmatched(true),originDefined(true); //not important initialization
      dvect3 zero3(0,0,0), originShift(0,0,0);
      int isymEuclid(-1);
      // Store some information about first model in reference solution
      dvect3 fracSolC_first = EPSET[k][0].SCAT.site;
      // Find possible choices of origin shift of checked solution that make a match with first reference model
      for (unsigned s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
      {
        allmatched = false;
        originDefined = false;
        originShift = zero3;

        dvect3 fracSolC_chk(EPSET[k_chk][s_chk_ori].SCAT.site);

        floatType ONE(1);
        for (unsigned isym = 0; isym < sgEuclid.NSYMM; isym++)
        {
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          {
            // Check that centres of mass are sufficiently similar
            // Centre of mass of ensemble won't change with application
            // of point group symmetry.
            dvect3 fracSolC_chk_sym = sgEuclid.doSymXYZ(isym,fracSolC_chk);
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
              while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
            }
            dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
              while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
            }
            // Account for any continuous shifts as origin shift
            dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
            dvect3 originShift_reference(0,0,0);
            af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
            for (unsigned j = 0; j < 3; j++) {
              if (isshift[j]) {
                originShift_reference[j] = -fracdelta_reference[j];
                fracdelta_reference[j] = 0;
              }
            }
            fracdelta=cb_op_r_from_reference_setting*fracdelta_reference;
            originShift=cb_op_r_from_reference_setting*originShift_reference;
            dvect3 delta = uc.doFrac2Orth(fracdelta);
            floatType tdist = delta.length();
            if (tdist <= transMax)
            {
              isymEuclid = isym;
              originDefined = true;
              break;
            }
          } // ensym
          if (originDefined) break;
        } // isym
        if (!originDefined) continue; // Test different origins

        PHASER_ASSERT(originDefined);

        // Now use choice of relative origin (if defined) to test equivalence
        allmatched = true; //So far at least one match.  Negate if one fails.
        std::vector<dvect3> matched_site(nmodels);
        bool1D used(nmodels,false);
        for (unsigned s = 0; s < nmodels; s++)
        { // For each model in the reference, check whether any matching models are the same
          // Determine sensible distance thresholds from dmin and model mean radius
          dvect3 fracSolC = EPSET[k][s].SCAT.site;
          matched = false;
          for (unsigned s_chk = 0; s_chk < nmodels; s_chk++)
          {
          dvect3 fracSolC_chk = EPSET[k_chk][s_chk].SCAT.site;
          if (originDefined)
            fracSolC_chk = sgEuclid.doSymXYZ(isymEuclid,fracSolC_chk);
          for (unsigned j = 0; j < 3; j++)
          {
            while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
            while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
          }
          if (used[s_chk]) continue;
          if (s_chk == s_chk_ori)
          {
            matched = true;
            used[s_chk] = true;
            matched_site[s_chk] = fracSolC_chk;
          }
          else
          {
            SpaceGroup sg = !originDefined ? sgEuclid : SgOps;
            floatType ONE(1);
            for (unsigned isym = 0; isym < sg.NSYMM; isym++)
            {
              {
                // Check that centres of mass are
                // sufficiently similar. Centre of mass of ensemble won't
                // change with application of point group symmetry.
                dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
                dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
                dvect3 shiftdelta(0,0,0);
                for (unsigned j = 0; j < 3; j++)
                {
                  while (fracdelta[j] < -0.5) { fracdelta[j] += ONE; shiftdelta[j]++; }
                  while (fracdelta[j] >= 0.5) { fracdelta[j] -= ONE; shiftdelta[j]--; }
                }
                dvect3 delta = uc.doFrac2Orth(fracdelta);
                floatType tdist = delta.length();
                if (tdist > transMax) continue;
                matched = true;
                used[s_chk] = true;
                matched_site[s_chk] = fracSolC_chk_sym-shiftdelta;
                if (matched) break;
              } // ensym
              if (matched) break;
            } // isym
            if (matched) break;
          } // s_chk
          } // s_chk
          if (!matched) allmatched = false;
        } // s
        if (allmatched)
        {
          PHASER_ASSERT( EPSET[k_chk].size() == matched_site.size());
          for (unsigned s = 0; s < EPSET[k_chk].size(); s++)
            EPSET[k_chk][s].SCAT.site = matched_site[s];
          break;
        }
        // Failed to match everything.  Try again with another possible origin shift.
        for (unsigned m = 0; m < nmodels; m++)
          used[m] = false;
      } // s_chk_ori
      if (allmatched)
      {
        EQUIV[k_chk] = k;
      }
    } //k_chk
    if (nthread == 0 && output) output->logProgressBarNext(LOGFILE);
  } // end #pragma omp parallel
  if (output) output->logProgressBarEnd(LOGFILE);
  if (output) output->logBlank(VERBOSE);
  return EQUIV;
}

void ListEditing::find_centrosymmetry(double transMax,std::string SG_HALL,af::double6 CELL,ep_solution& EPSET,Output* output)
{
  SpaceGroup sg(SG_HALL);
  UnitCell uc(CELL);
  int tsize = EPSET.size();

  cctbx::sgtbx::space_group SgOps(SG_HALL,"A1983");
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
 //add centre of symmetry so that other hand identified as duplicate
 //not for centrosymmetry test!
  for (int k = 0; k < tsize; k++)
    EPSET[k].CENTROSYMMETRIC = false;
  if (SgInfo.is_enantiomorphic()) return;
  SgInfo = cctbx::sgtbx::space_group_type(SgOps);
  cctbx::sgtbx::structure_seminvariants semis(SgOps);
  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
  SpaceGroup sgEuclid(ssym.subgroup().type().hall_symbol());
  // Repeat setup work for reference setting.
  cctbx::sgtbx::space_group reference_space_group(SgInfo.group().change_basis(SgInfo.cb_op()));
  cctbx::sgtbx::space_group_type reference_space_group_type(reference_space_group);
  cctbx::sgtbx::structure_seminvariants reference_semis(reference_space_group);
  cctbx::sgtbx::search_symmetry reference_ssym( flags, reference_space_group_type, reference_semis);
  dmat33 cb_op_r_to_reference_setting(SgInfo.cb_op().c().r().as_double());
  dmat33 cb_op_r_from_reference_setting(SgInfo.cb_op().c_inv().r().as_double());

  // Compare solutions to find duplicates further down list

  for (int k = 0; k < tsize; k++)
  {
    output->logTab(1,TESTING,"===== SET #" + itos(k+1)+ " of " + itos(tsize));
    int nmodels = EPSET[k].size();
    af_atom HAND = EPSET[k].other_hand(SG_HALL,CELL);
    {//k_chk (hand)
      bool matched(true);
      bool allmatched(true),originDefined(false); //-Wall init
      dvect3 zero3(0,0,0), originShift(0,0,0);
      int isymEuclid(-1);
      // Store some information about first model in reference solution
      dvect3 fracSolC_first = EPSET[k][0].SCAT.site;
      std::vector<dvect3> matched_site(nmodels);
      std::vector<double> matched_dist(nmodels,-999);
      std::vector<int> matched_symop(nmodels);
      std::vector<int> matched_s(nmodels);
      // Find possible choices of origin shift of checked solution that make a match with first reference model
      for (unsigned s_chk_ori = 0; s_chk_ori < nmodels; s_chk_ori++)
      {
        allmatched = false;
        originDefined = false;
        originShift = zero3;

        dvect3 fracSolC_chk(HAND[s_chk_ori].SCAT.site);

        floatType ONE(1);
        for (unsigned isym = 0; isym < sgEuclid.NSYMM; isym++)
        {
          // Determine sensible distance thresholds from dmin and model mean radius
          // Avoid divide-by-zero for single-atom model, where rotation doesn't matter
          {
            // Check that centres of mass are sufficiently similar
            // Centre of mass of ensemble won't change with application
            // of point group symmetry.
            dvect3 fracSolC_chk_sym = sgEuclid.doSymXYZ(isym,fracSolC_chk);
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
              while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
            }
            dvect3 fracdelta = fracSolC_first - fracSolC_chk_sym;
            for (unsigned j = 0; j < 3; j++)
            {
              while (fracdelta[j] < -0.5) fracdelta[j] += ONE;
              while (fracdelta[j] >= 0.5) fracdelta[j] -= ONE;
            }
            // Account for any continuous shifts as origin shift
            dvect3 fracdelta_reference = cb_op_r_to_reference_setting * fracdelta;
            dvect3 originShift_reference(0,0,0);
            af::tiny<bool, 3> isshift = reference_ssym.continuous_shift_flags();
            for (unsigned j = 0; j < 3; j++) {
              if (isshift[j]) {
                originShift_reference[j] = -fracdelta_reference[j];
                fracdelta_reference[j] = 0;
              }
            }
            fracdelta=cb_op_r_from_reference_setting*fracdelta_reference;
            originShift=cb_op_r_from_reference_setting*originShift_reference;
            dvect3 delta = uc.doFrac2Orth(fracdelta);
            floatType tdist = delta.length();
            if (tdist <= transMax)
            {
              isymEuclid = isym;
              originDefined = true;
              break;
            }
          } // ensym
          if (originDefined) break;
        } // isym
        if (!originDefined) continue; // Test different origins

        PHASER_ASSERT(originDefined);

        // Now use choice of relative origin (if defined) to test equivalence
        allmatched = true; //So far at least one match.  Negate if one fails.
        bool1D used(nmodels,false);
        for (unsigned s = 0; s < nmodels; s++)
        { // For each model in the reference, check whether any matching models are the same
          // Determine sensible distance thresholds from dmin and model mean radius
          dvect3 fracSolC = EPSET[k][s].SCAT.site;
          matched = false;
          for (unsigned s_chk = 0; s_chk < nmodels; s_chk++)
          {
          dvect3 fracSolC_chk = HAND[s_chk].SCAT.site;
          if (originDefined)
            fracSolC_chk = sgEuclid.doSymXYZ(isymEuclid,fracSolC_chk);
          for (unsigned j = 0; j < 3; j++)
          {
            while (fracSolC_chk[j] < -0.5) { fracSolC_chk[j] += ONE; }
            while (fracSolC_chk[j] >= 0.5) { fracSolC_chk[j] -= ONE; }
          }
          if (used[s_chk]) continue;
          if (s_chk == s_chk_ori)
          {
            matched = true;
            used[s_chk] = true;
            matched_site[s_chk] = fracSolC_chk;
            matched_symop[s_chk] = -999;
            matched_s[s] = s_chk;
          }
          else
          {
            SpaceGroup sg = !originDefined ? sgEuclid : SgOps;
            floatType ONE(1);
            for (unsigned isym = 0; isym < sg.NSYMM; isym++)
            {
              {
                // Check that centres of mass are
                // sufficiently similar. Centre of mass of ensemble won't
                // change with application of point group symmetry.
                dvect3 fracSolC_chk_sym = sg.doSymXYZ(isym,fracSolC_chk);
                dvect3 fracdelta = fracSolC - fracSolC_chk_sym + originShift;
                dvect3 shiftdelta(0,0,0);
                for (unsigned j = 0; j < 3; j++)
                {
                  while (fracdelta[j] < -0.5) { fracdelta[j] += ONE; shiftdelta[j]++; }
                  while (fracdelta[j] >= 0.5) { fracdelta[j] -= ONE; shiftdelta[j]--; }
                }
                dvect3 delta = uc.doFrac2Orth(fracdelta);
                floatType tdist = delta.length();
                if (tdist > transMax) continue;
//std::cout << "-------- ori=" << s_chk_ori << " s=" << s << " s_chk=" << s_chk << " isym=" << isym << " hand=" << dvtos(HAND[s_chk].SCAT.site,6,3) << " before=" << dvtos(fracSolC_chk,6,3) << " (symop=" << dvtos(fracSolC_chk_sym,6,3) << "==  orig=" << dvtos(EPSET[k][s].SCAT.site,6,3) << ") delta=" << dvtos(fracdelta,6,3) << " distance=" << tdist << std::endl;
                matched = true;
                used[s_chk] = true;
                matched_site[s] = fracSolC_chk_sym-shiftdelta;
                matched_dist[s] = tdist;
                matched_symop[s] = isym;
                matched_s[s] = s_chk;
                if (matched) break;
              } // ensym
              if (matched) break;
            } // isym
            if (matched) break;
          } // s_chk
          } // s_chk
          if (!matched) allmatched = false;
        } // s
        if (allmatched)
        {
         // for (unsigned s = 0; s < HAND.size(); s++)
         //   HAND[s].SCAT.site = matched_site[s];
          break;
        }
        // Failed to match everything.  Try again with another possible origin shift.
        for (unsigned m = 0; m < nmodels; m++)
          used[m] = false;
      } // s_chk_ori
      if (allmatched)
      {
        EPSET[k].CENTROSYMMETRIC = true;
        if (output->level(TESTING))
        {
          output->logTab(0,TESTING,"Match: shift=" + dvtos(originShift,5,3));
          for (unsigned s = 0; s < HAND.size(); s++)
          {
            output->logTab(0,TESTING,"Original: " + itos(s+1) + " " +
                                   dvtos(EPSET[k][s].SCAT.site,6,3) + "  |  " + dvtos(HAND[s].SCAT.site,6,3));

          }
          for (unsigned s = 0; s < HAND.size(); s++)
          {
            output->logTab(0,TESTING,"Match: " + itos(s+1) + "==" + itos(matched_s[s]+1) + " input:" + dvtos(EPSET[k][s].SCAT.site,6,3) + "  ==  hand of matched site" + dvtos(HAND[matched_s[s]].SCAT.site,6,3) + "-> actulal match" + dvtos(matched_site[s],6,3) + " dist=" + std::string(matched_dist[s]>0?dtos(matched_dist[s],3): "origin match"));
          }
        }
      }
    } //k_chk
    if (output) output->logProgressBarNext(LOGFILE);
  } // end #pragma omp parallel
  if (output) output->logBlank(VERBOSE);
}

}
