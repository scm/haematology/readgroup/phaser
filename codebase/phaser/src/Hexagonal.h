//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __HexagonalClass__
#define __HexagonalClass__
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>

namespace phaser {

enum HEXAGONAL_TYPE {not_set,alongLine,aroundPoint,inPlane,inRegion};

class Hexagonal : public UnitCell, public SpaceGroup
{
  private:
    HEXAGONAL_TYPE hexagonal_type;
    dvect3 START,SLOPE,POINT,orthSamp,orthMin,orthMax,orthSite;
    floatType ONE,RANGEsqr,off0,off1;
    int U,V,L,MAXL,t,max_t;
    bool (SpaceGroup::* VOL_FN) (dvect3);
    std::vector<dvect3> comp;
    bool check_points;

  public:
    bool                at_end() const;
    dvect3              next_site_frac(int);
    void                restart();
    std::vector<dvect3> all_sites_frac();
    std::size_t         count_sites();

    Hexagonal(UnitCell,SpaceGroup);
    ~Hexagonal() {}

    void setupAlongLine(dvect3,dvect3,floatType,bool=false,bool=false);
    void setupAroundPoint(dvect3,floatType,floatType,bool=false,bool=false);
    void setupBrickRegion(Brick,floatType,bool=false,bool=false);
    void setupAsuRegion(floatType,bool=false);

    dvect3 nextAlongLine(int);
    dvect3 nextAroundPoint(int);
    dvect3 nextInPlane(int);
    dvect3 nextInRegion(int);

    dvect3 getOrthMin();
    dvect3 getOrthMax();
};

} //end namespace phaser

#endif
