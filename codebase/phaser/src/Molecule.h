//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MoleculeClass__
#define __MoleculeClass__
#include <list>
#include <phaser/main/Phaser.h>
#include <phaser/ep_objects/af_atom.h>
#include <phaser/mr_objects/mr_ndim.h>
#include <phaser/mr_objects/data_map.h>
#include <phaser/mr_objects/data_vartype.h>
#include <phaser/mr_objects/data_model.h>
#include <phaser/mr_objects/overlap.h>
#include <phaser/pod/pdb_record.h>
#include <phaser/pod/rmsid.h>
#include <phaser/include/Boolean.h>
#include <phaser/io/Output.h>
#include <phaser/lib/fmat.h>
#include <phaser/lib/xyz_weight.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

class Molecule
{
  friend class TraceMol;

  public:
    Molecule(data_model=data_model(),bool=false);

  private:
    std::vector<std::vector<pdb_record> > pdb;
    std::list<std::string> protein_resid,nucleic_resid;
    std::list<std::string> protein_trace,nucleic_trace;
    std::list<std::string> protein_backbone;

  public:
    void        ReadPDB(std::string);
    std::string ReadCIF(std::string);
    void        ReadHA(std::string);
    void        ReadStreamPDB(std::istream&);

  public:
    const pdb_record& card(int m,int a) const { return pdb[m][a]; }
    const pdb_record& card(int a) const       { return pdb[mtrace][a]; }
    int   nMols() const                       { return pdb.size(); }
    int   nAtoms(int m = -999) const;

  public:
    bool        input_as_atom,input_as_helix;
    int         mtrace;
    int1D       pdb_model_serial;
    int         protein_nres,nucleic_nres;
    std::vector<data_vartype>  model_rmsid;
    std::string pdb_errors;
    string1D    REMARKS;

  public:
    void        atomicHessian(floatType,floatType,TNT::Fortran_Matrix<float>&,Output&) const;
    bool        is_atom() const;
    std::string one_atom_element() const;
    stringset   get_overlap_chains(mr_ndim&,SpaceGroup&,UnitCell&,int=-999);
    void        erase(int,int);
    af_atom     getAtomArrays();
    floatType   getScat(int) const;
    floatType   getScat(float1D*) const;
    floatType   getScat(bool1D*) const;
    floatType   getScat(std::string) const;
    string1D    getChains(bool=false) const; //in file order
    float1D     allRms(std::string);
    data_mw     getTotalMW(int m = -999,std::string="") const;
    int1D       trace_to_atom_list(int) const;
    int         mtrace_model_serial()            { return pdb_model_serial[mtrace]; }
    std::string read_errors() const              { return pdb_errors; }
    bool        isTraceAtom(int,int) const;
    bool        isBackboneAtom(int,int) const;
    bool        is_helix();
    bool        isUnknown(int m, int a) const    { return pdb[m][a].AtomName == "UNK "; }
    stringset   getAnomTypes() const;
    xyz_weight  getCW(bool,int) const;
    dvect3      helix_axis(dmat33);
    stringset   get_chainIDs() const;
    bool        set_occupancy(bool1D*);
};

}

#endif
