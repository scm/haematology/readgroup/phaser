//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PATTERSON_CLASS__
#define __PATTERSON_CLASS__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <cctbx/maptbx/peak_search.h>

namespace phaser {

class Patterson: public SpaceGroup, public UnitCell
{
  public:
    Patterson(UnitCell&,SpaceGroup&,af::shared<miller::index<int> >,af_cmplx,bool,Output&);
    //cannot store or pass map, is destroyed in memeory
    af::shared<double> peak_heights;
    af::shared<scitbx::vec3<double> > peak_sites;

  private:
    double Patterson_max;
    double Patterson_mean;
    double Patterson_sigma;

  public:
    double  mean() { return Patterson_mean; }
    double  max() { return Patterson_max; }
    double  sigma() { return Patterson_sigma; }
    af::shared<double> heights() { return peak_heights; }
    af::shared<scitbx::vec3<double> > sites() { return peak_sites; }
};

} //end namespace phaser

#endif
