//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <cctbx/adptbx.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/miller/index_span.h>
#include <cctbx/translation_search/fast_nv1995.h>
#include <cctbx/translation_search/fast_terms.h>
#include <cctbx/translation_search/symmetry_flags.h>
#include <cctbx/xray/sampled_model_density.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/lib/signal.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/rad2phi.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <phaser/src/Gfunction.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/ep_objects/ep_search.h>
#include <phaser/ep_objects/ep_solution.h>
#include <scitbx/fftpack/real_to_complex_3d.h>

namespace phaser {

void RefineSAD::calcFpart(af::shared<miller::index<int> > Fmiller,af::shared<cmplxType> Fpart)
{
  // Calculate Fpart in terms of fractional occupancy and B-factor.
  // Scattering factor will be taken account of later.
  Fmiller.resize(0);
  if (atoms.size()) for (unsigned r = 0; r < NREFL; r++) FApos[r] = FAneg[r] = 0;

  float1D linearTerm(atoms.size()),debye_waller_u_iso(atoms.size(),0.);
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    linearTerm[a] = symFacPCIF*atoms[a].SCAT.occupancy/M;
    if (!atoms[a].SCAT.flags.use_u_aniso_only()) debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
  }

  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
  {
    Fmiller.push_back(miller[r]);
  }
  Fpart.resize(Fmiller.size(),0);

  if (atoms.size())
  {
  int rr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
  {
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED)
    {
      // int t = atomtype2t[atoms[a].SCAT.scattering_type]; // Could be used for clusters?
      floatType isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
      floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
      if (debye_ScaleB) isoB *= std::exp(-ssqr[r]*debye_ScaleB);
      floatType scat(linearTerm[a]*isoB); // Scattering from unit atom with B and occ
           //   symmOccBfac*=CLUSTER[t].debye(ssqr[r]); // Maybe later?
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        floatType anisoB(1);
        floatType anisoScat(scat);
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],atoms[a].SCAT.u_star);
          anisoScat = scat*anisoB;
        }
        floatType theta = rotMiller[r][isym][0]*atoms[a].SCAT.site[0] +
                          rotMiller[r][isym][1]*atoms[a].SCAT.site[1] +
                          rotMiller[r][isym][2]*atoms[a].SCAT.site[2] +
                          traMiller[r][isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
        cmplxType sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
        floatType costheta = std::real(sincostheta);
        floatType sintheta = std::imag(sincostheta);
#else
        theta *= scitbx::constants::two_pi;
        floatType costheta = std::cos(theta);
        floatType sintheta = std::sin(theta);
#endif
        floatType anisoScat_costheta(anisoScat*costheta);
        floatType anisoScat_sintheta(anisoScat*sintheta);
        Fpart[rr] += cmplxType(anisoScat_costheta, anisoScat_sintheta);
      }//end sym
    } //end  loop over atoms
    rr++;
  } //end loop r
  }
}

double RefineSAD::LESSTF1(data_find FINDSITES, af::shared<cmplxType> Fpart, af_float mI)
{
  double LLconst(0),LLoffset(0);

  cmplxType TWOPII(0.,scitbx::constants::two_pi);
  int rr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
  {
    double rmI(0.);
    double bterm(std::exp(-WilsonB*ssqr[r]/4.));
    int t = atomtype2t[FINDSITES.TYPE];
    double fp =  (FINDSITES.TYPE == "AX") ? 0 : fo_plus_fp[r][t]*bterm;
    double fdp = (FINDSITES.TYPE == "AX") ? 1 : AtomFdp[t]*bterm;
    //Linear approximation centered on partial structure factor, if any
    double eU(std::abs(Fpart[rr]/bterm));
    if (eU == 0) // No partial substructure
    {
      FHpos[r] = FHneg[r] = 0.;
      std::pair<double,double> LdL = LdL_by_dUsqr_at_0(r,fp,fdp);
      LLconst += LdL.first;
      rmI = LdL.second;
      //std::cout << "dLcoeffs: " << miller[r][0] << " " << miller[r][1] << " " << miller[r][2] << " " << LdL.second << std::endl;
    }
    else
    {
      FHpos[r] = cmplxType(fp,fdp)*eU;
      FHneg[r] = cmplxType(fp,-fdp)*eU;
      if (both[r])
      {
        LLconst -= acentricReflGradSAD2(r,true,false);
        rmI = -(fp*(dL_by_dReFHpos+dL_by_dReFHneg)+fdp*(dL_by_dImFHpos-dL_by_dImFHneg));
      }
      else if (!sad_target_anom_only && cent[r])
      {
        LLconst -= centricReflGradSAD2(r,true,false);
        rmI = -(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos);
      }
      else if (!sad_target_anom_only)
      {
        LLconst -= singletonReflGradSAD2(r,plus[r],true,false);
        rmI = plus[r] ? -(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos): -(fp*dL_by_dReFHneg-fdp*dL_by_dImFHneg);
      }
      double unitary_derivative = 2*eU;
      rmI /= unitary_derivative;
      LLoffset -= fn::pow2(eU)*rmI;
    }
    mI.push_back(rmI);
    rr++;
  }
  LLconst += wilson_llg;
  LLoffset += LLconst; // Correct for null hypothesis, not just mean of search
  return LLoffset;
}

std::pair<double,double> RefineSAD::LdL_by_dUsqr_at_0(unsigned r, double fp, double fdp)
{
  double LL_at_0(0.),dL_by_dUsqr(0.);
  if (both[r])
  {
    double FOneg(NEG.F[r]),FOpos(POS.F[r]);
    double FOnegSqr(fn::pow2(FOneg));
    double FOposSqr(fn::pow2(FOpos));
    double VarFOneg(fn::pow2(NEG.SIGF[r]));
    double VarFOpos(fn::pow2(POS.SIGF[r]));
    double SigmaNeg(sigDsqr[r] + VarFOneg);
    PHASER_ASSERT(SigmaNeg > 0);
    double SigmaPos(sigPlus[r] + VarFOpos + VarFOneg);
    PHASER_ASSERT(SigmaPos > 0);
    double fpSqr(fn::pow2(fp));
    double fdpSqr(fn::pow2(fdp));
    double dFOnegSqr(FOnegSqr-SigmaNeg);
    double X(2*FOneg*FOpos/SigmaPos);
    LL_at_0 = -(FOnegSqr/SigmaNeg+fn::pow2(FOneg-FOpos)/SigmaPos) +
      std::log(4*FOpos*FOneg*eBesselI0(X)/(SigmaNeg*SigmaPos));
    dL_by_dUsqr =
      (4*fdpSqr*SigmaNeg*((FOnegSqr+FOposSqr)*SigmaNeg+dFOnegSqr*SigmaPos) +
      (fpSqr+fdpSqr)*dFOnegSqr*fn::pow2(SigmaPos) -
      4*FOneg*FOpos*fdpSqr*SigmaNeg*(2*SigmaNeg+SigmaPos)*sim(X)) /
      fn::pow2(SigmaNeg*SigmaPos);
  }
  else if (!sad_target_anom_only && cent[r])
  {
    double FOsqr = fn::pow2(POS.F[r]);
    double V(sigDsqr[r] + 0.5*fn::pow2(POS.SIGF[r]));
    LL_at_0 = -FOsqr/(2.*V) - std::log(scitbx::constants::pi*V/2.)/2.;
    dL_by_dUsqr = (fn::pow2(fp)+fn::pow2(fdp))*(FOsqr-V) /
      (2*fn::pow2(V));
  }
  else if (!sad_target_anom_only)
  {
    double FO = plus[r] ? POS.F[r] : NEG.F[r];
    double FOsqr = fn::pow2(FO);
    double VarFO = fn::pow2(plus[r] ? POS.SIGF[r] : NEG.SIGF[r]);
    double V(sigDsqr[r] + VarFO);
    LL_at_0 = -FOsqr/V + std::log(2.*FO/V);
    dL_by_dUsqr = (fn::pow2(fp)+fn::pow2(fdp))*(FOsqr-V) /
      fn::pow2(V);
  }
  return std::pair<double,double>(LL_at_0,dL_by_dUsqr);
}

ep_search RefineSAD::fast_search(
          data_find FINDSITES,data_peak PEAKS,double SAMPLING,
          af::versa<double, af::c_grid<3> > M,
          Output& output)
{
  ep_search final_atom_sets;

  double HIRES(DEF_LORES);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r] && selected[r])
    HIRES = std::min(HIRES,UnitCell::reso(miller[r]));
  //fft thing
  cctbx::uctbx::unit_cell UCell = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group SgOps = getCctbxSG();
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  af::int3 one_one_one(1,1,1);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  double resolution_factor(SAMPLING/HIRES);
  //cctbx assert resolution_factor <= 0.5
  resolution_factor = std::min(0.5,resolution_factor);

  bool isIsotropicSearchModel(false); //true doesn't run
  bool haveFpart = atoms.size();
  cctbx::translation_search::symmetry_flags sym_flags(isIsotropicSearchModel,haveFpart);

  af::int3 gridding = cctbx::maptbx::determine_gridding(
    UCell, HIRES, resolution_factor,sym_flags,SgInfo,
    one_one_one,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  tags.build(SgInfo,sym_flags);

  af::shared<cmplxType>  Fpart;
  af::shared<miller::index<int> > Fmiller,FmillerP1;
  calcFpart(Fmiller,Fpart); // Structure factors for fixed atoms with unit scattering factor
  PHASER_ASSERT(Fmiller.size() == Fpart.size());
  bool FriedelFlag(true);
  FmillerP1 = miller::expand_to_p1_indices(SgOps,FriedelFlag,Fmiller.const_ref());
  af_float mI;
  double LLconst = LESSTF1(FINDSITES,Fpart,mI);
  output.logTab(1,VERBOSE,"Translation-independent constant to add to FFT result: " + dtos(LLconst));
  af::shared<cmplxType> FcalcP1;
  // Search object can be one point atom or a constellation of point atoms
  af::shared<dvect3> constellation;
  constellation.push_back(dvect3(0,0,0));
  if (!haveFpart) // Only in first cycle of substructure determination
  {
    /* Top 5 sites in 2pn1 for hardwiring multi-atom search test
      0.809  0.423  0.077   0.9951   26.5
      0.774  0.431 -0.140   0.8494   25.4
      1.017  0.564  0.404   0.8854   23.0
      0.801  0.366 -0.053   0.7668   23.9
      0.138  0.685  0.584   0.8836   24.4  */
    //constellation.push_back(dvect3(-0.0298,0.0,-0.2115)); // from dL_by_dUsqr "Patterson"
    //constellation.push_back(dvect3(-0.0298,0.008,-0.2115)); // from dL_by_dUsqr "Patterson", shifted off Harker
    //constellation.push_back(dvect3(-0.035,0.008,-0.217));
    //constellation.push_back(dvect3(0.208,0.141,0.327));
    //constellation.push_back(dvect3(-0.008,-0.057,-0.13));
    //constellation.push_back(dvect3(0.329,0.262,0.507));
  }
  for (unsigned idat = 0; idat < FmillerP1.size(); idat++)
  {
    cmplxType Fsearch(0.,0.);
    for (unsigned natom = 0; natom < constellation.size(); natom++)
    {
      double dphi = FmillerP1[idat]*constellation[natom];
      #if defined CCTBX_MATH_COS_SIN_TABLE_H
        Fsearch += fast_cos_sin.get(dphi); //multiplies by 2*pi internally
      #else
        dphi *= scitbx::constants::two_pi;
        Fsearch += cmplxType(std::cos(dphi),std::sin(dphi));
      #endif
    }
    Fsearch *= FINDSITES.OCCUPANCY; // Default occupancy for new sites
    FcalcP1.push_back(Fsearch);
  }
  cctbx::translation_search::fast_terms<double> fast_terms(
          gridding,
          FriedelFlag,
          FmillerP1.const_ref(),
          FcalcP1.const_ref());

  bool squared_flag(false); //true for second order mIsqr
  af::versa<double, af::c_grid<3> > ssdmap = fast_terms.summation(
    SgOps,
    Fmiller.const_ref(),
    mI.const_ref(),
    Fpart.const_ref(),
    squared_flag).fft().accu_real_copy();

  for (int i = 0; i < ssdmap.size(); i++) ssdmap[i] = ssdmap[i] + LLconst; // multiplicity correction before constant correction

  //optical resolution 0.715*resolution
  floatType max_bond_dist(10.0),optical_res(0.715*HiRes());
  floatType separation_dist = std::min(max_bond_dist,optical_res);

/*
  if (!M.size())
  { //memory
  M = ssdmap.deep_copy(); //for size
  int count(0);
  double nx = ssdmap.accessor()[0];
  double ny = ssdmap.accessor()[1];
  double nz = ssdmap.accessor()[2];
  for (int lx = 0; lx < nx; lx++) {
  for (int ly = 0; ly < ny; ly++) {
  for (int lz = 0; lz < nz; lz++) {
     dvect3 site(lx/nx,ly/ny,lz/nz);
     cctbx::sgtbx::site_symmetry peak_sym(UCell,SgOps,site,separation_dist);
     M(lx,ly,lz) = SpaceGroup::NSYMM/peak_sym.multiplicity();
     count++;
  }}}
  PHASER_ASSERT(count == ssdmap.size());
  }
  PHASER_ASSERT(M.size() == ssdmap.size());
  for (int i = 0; i < M.size(); i++)
    ssdmap[i] /= fn::pow2(M[i]);
*/

  std::pair<dvect3,double> deepest_hole;
  int holeM;
  for (int holes = 1; holes >= 0; holes--) //do holes first
  {
    if (holes) for (int i = 0; i < ssdmap.size(); i++) ssdmap[i] *= -1;
    if (!holes) for (int i = 0; i < ssdmap.size(); i++) ssdmap[i] *= -1; //switch back again

    // --- work out mean and sigma for total search first time through
    cctbx::maptbx::statistics<double> stats;
    if (holes)
    {
      stats =   af::const_ref<double, af::flex_grid<> >(
                ssdmap.begin(),
                ssdmap.accessor().as_flex_grid());
      final_atom_sets.mean = stats.mean();
      final_atom_sets.sigma = stats.sigma();
      PHASER_ASSERT(final_atom_sets.sigma > 0);
    }
    else
      final_atom_sets.mean *= -1;

    // --- find symmetry equivalents
    af::const_ref<double, af::c_grid_padded<3> > ssdmap_const_ref(
        ssdmap.begin(),
        af::c_grid_padded<3>(ssdmap.accessor()));
    af::ref<long, af::c_grid<3> > tag_array_ref(
      tags.tag_array().begin(),
      af::c_grid<3>(tags.tag_array().accessor()));
    PHASER_ASSERT(tags.verify(ssdmap_const_ref));

    // --- peak search
    int peak_search_level(3);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
      ssdmap_const_ref,
      tag_array_ref,
      peak_search_level,
      max_peaks,
      interpolate);

    final_atom_sets.heights = peak_list.heights();
    final_atom_sets.sites = peak_list.sites();

    //collect the high peaks and store their Z-scores
    //print a histogram of the Z-scores
    unsigned1D peaks_over_cutoff(0);
    float1D peaks_Zscores(0);
    if (final_atom_sets.heights.size() && final_atom_sets.sigma) //i.e not a flat map
    {
      outStream where = LOGFILE;
      outStream where2 = holes ? VERBOSE : LOGFILE; // Skip some output for holes
      holes ? output.logUnderLine(where,"Histogram of Z-scores of HOLES (" + FINDSITES.TYPE + ")") :
              output.logUnderLine(where,"Histogram of Z-scores of PEAKS (" + FINDSITES.TYPE + ")");
      double maxZ = -std::numeric_limits<double>::max();
      final_atom_sets.top = -std::numeric_limits<double>::max();
      int peakM(0);
      for (std::size_t i = 0; i < final_atom_sets.heights.size(); i++)
      {
        cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),final_atom_sets.sites[i],separation_dist);
        double Z = (final_atom_sets.heights[i]-final_atom_sets.mean)/final_atom_sets.sigma;
        if (Z > maxZ)
        {
          maxZ = Z;
          peakM = SpaceGroup::NSYMM/peak_sym.multiplicity();
        }
        //use interpolated top, not stats top
        final_atom_sets.top = std::max(final_atom_sets.top,final_atom_sets.heights[i]);
      }
      holes ? output.logTab(2,where,"Number of holes = " + itos(final_atom_sets.heights.size())) :
              output.logTab(2,where,"Number of peaks = " + itos(final_atom_sets.heights.size()));
      output.logTab(2,where,"Maximum Z-score = " + dtos(maxZ));
      output.logTab(2,where2,"Mean   = "+dtos(final_atom_sets.mean));
      output.logTab(2,where2,"Sigma  = "+dtos(final_atom_sets.sigma));
      output.logTab(2,where2,"Maximum  = "+dtos(final_atom_sets.top));
      output.logTab(2,where,"Multiplicity at max = "+itos(peakM));
      if (holes) holeM = peakM;
      { //find top peak/hole
        double pk(-std::numeric_limits<double>::max());
        int topi(0); //-Wall init
        for (unsigned i = 0; i < final_atom_sets.heights.size(); i++)
          if (pk < final_atom_sets.heights[i]) { pk = final_atom_sets.heights[i]; topi = i; }
        dvect3 frac(final_atom_sets.sites[topi]);
        if (holes) deepest_hole.first = frac;
        if (holes) deepest_hole.second = maxZ;
        std::string ph = holes ? "Deepest hole: " : "Highest peak: ";
        output.logTab(2,where,ph + dvtos(frac,true) + " (Z-score " + dtos(maxZ) + ")");
        if (!holes && maxZ < deepest_hole.second) //set previous loop
        {
          output.logTab(1,where,"Deepest hole at " + dvtos(deepest_hole.first,true) + " (Z-score=" + dtos(deepest_hole.second) + ") is deeper than highest peak at " + dvtos(frac,true) + " (Z-score=" + dtos(maxZ) + ")");
          output.logTab(1,where,"Multiplicity of deepest hole = " + itos(holeM));
          output.logBlank(where);
          output.logWarning(LOGFILE,"Deepest hole in Phassade map is deeper than highest peak");
        }
        output.logBlank(where);
      }

      int imaxZ(std::ceil(maxZ));
      unsigned1D histogram(imaxZ);
      unsigned maxStars(0);
      for (std::size_t i = 0; i < final_atom_sets.heights.size(); i++)
      {
        cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),final_atom_sets.sites[i],separation_dist);
        int peakM = SpaceGroup::NSYMM/peak_sym.multiplicity();
        double Z = signal(final_atom_sets.heights[i],final_atom_sets.mean,final_atom_sets.sigma);
        if (Z >= PEAKS.get_cutoff() &&
            (FINDSITES.SPECIAL_POSITION || (!FINDSITES.SPECIAL_POSITION && peakM > 1)))
        {
          peaks_over_cutoff.push_back(i);
          peaks_Zscores.push_back(Z);
        }
        int iZ = std::floor(Z);
        if (iZ >= 0)
        {
          histogram[iZ]++;
          maxStars = std::max(maxStars,histogram[iZ]);
        }
      }
      // the maximum across is 50
      double columns = 50;
      //Can not have less than one star per value, divStarFactor must be at least 1
      double min_divStarFactor = 1.0;
      double divStarFactor = std::max(min_divStarFactor,static_cast<double>(maxStars)/columns);
      output.logBlank(where);
      int grad = static_cast<int>(divStarFactor*10);
      output.logTabPrintf(1,where,"    %-10d%-10d%-10d%-10d%-10d%-10d\n",0,grad,grad*2,grad*3,grad*4,grad*5);
      output.logTabPrintf(1,where,"    %-10s%-10s%-10s%-10s%-10s%-10s\n","|","|","|","|","|","|");
      for (unsigned h = 0; h < imaxZ; h++)
      {
        std::string stars = h >= 10 ? itos(h) + "   " : " " + itos(h) + "   ";
        for (int star = 0; star < static_cast<double>(histogram[h])/divStarFactor; star++)
          stars += "*";
        if (histogram[h]) stars += " (" + itos(histogram[h]) + ")";
        output.logTab(1,where,stars);
      }
      output.logBlank(where);
    }
  }
  return final_atom_sets; //last loop, peaks
}

}
