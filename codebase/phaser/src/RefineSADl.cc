//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/round.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/BinStats.h>
#include <cctbx/adptbx.h>

namespace phaser {

void RefineSAD::logScale(outStream where,Output& output)
{
  if (input_atoms)
  {
    output.logTab(1,where,"Scales for Atomic Structure:");
    output.logTab(2,where,"Overall Scale: " + dtos(ScaleK));
    output.logTab(2,where,"Overall B-factor: " + dtos(cctbx::adptbx::u_as_b(ScaleU)));
    output.logBlank(where);
  }
  if (input_partial)
  {
    output.logTab(1,where,"Scales for Partial Structure:");
    output.logTab(2,where,"Overall Partial Scale: " + dtos(PartK));
    output.logTab(2,where,"Overall Partial B-factor: " + dtos(cctbx::adptbx::u_as_b(PartU)));
    output.logBlank(where);
  }
}

void RefineSAD::logAtoms(outStream where,Output& output,bool rejected)
{
  PHASER_ASSERT(atoms.size() == atoms.size());
  int natoms(0);
  for (unsigned a = 0; a < atoms.size(); a++)
  if ((rejected && atoms[a].REJECTED) || (!rejected && !atoms[a].REJECTED))
    natoms++;
  output.logTab(1,where,"Atom Parameters: "+itos(natoms)+" atom"+std::string((natoms==1)?"":"s")+" in list");
  if (atoms.size())
  {
    output.logTab(1,VERBOSE,"ANISU, BETA and USTAR are the anisotropic B-factor values including the isotropic B-factor. ANISU is printed as for pdb output. EIGEN are the eigenvalues of the anisotropy tensor including the isotropic B-factor.");
    output.logTabPrintf(1,where,"       %-7s %-7s %-7s %-7s   %-2s   (%-6s) %1c %-s\n",
            "X","Y","Z","O","B","AnisoB",'M',"Atomtype");
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if ((rejected && atoms[a].REJECTED) || (!rejected && !atoms[a].REJECTED))
  {
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    if (!atoms[a].SCAT.flags.use_u_aniso_only())
      output.logTabPrintf(1,where,"#%-3d % 7.3f % 7.3f % 7.3f % 7.3f % 7.1f (%7s) %1d %-s\n",
          a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,
          cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso)," ---- ",
          M,atoms[a].SCAT.scattering_type.c_str());
    else
    {
      cctbx::adptbx::factor_u_star_u_iso<floatType> factor(UnitCell::getCctbxUC(),atoms[a].SCAT.u_star);
      dvect3 eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),factor.u_star_minus_u_iso));
      floatType minU(std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2])));
      floatType maxU(std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2])));
      if (atoms[a].SCAT.occupancy < 10)
      output.logTabPrintf(1,where,"#%-3d % 7.3f % 7.3f % 7.3f % 7.3f % 7.1f (%+7.2f) %1d %-s\n",
            a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,
            cctbx::adptbx::u_as_b(factor.u_iso),cctbx::adptbx::u_as_b(maxU-minU),
            M,atoms[a].SCAT.scattering_type.c_str());
      else
      output.logTabPrintf(1,where,"#%-3d % 7.3f % 7.3f % 7.3f % 7.2f % 7.1f (%+7.2f) %1d %-s\n",
            a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,
            cctbx::adptbx::u_as_b(factor.u_iso),cctbx::adptbx::u_as_b(maxU-minU),
            M,atoms[a].SCAT.scattering_type.c_str());
      eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),atoms[a].SCAT.u_star));
      output.logTabPrintf(1,VERBOSE,"EIGEN  %6.3f %6.3f %6.3f\n",
        cctbx::adptbx::u_as_b(eigenUvals[0]),cctbx::adptbx::u_as_b(eigenUvals[1]),cctbx::adptbx::u_as_b(eigenUvals[2]));
      dmat6 u_cart = cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),atoms[a].SCAT.u_star);
      u_cart *= u_cart_pdb_factor;
      output.logTabPrintf(1,VERBOSE,"ANISU  %6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n",
        u_cart[0],u_cart[1],u_cart[2],u_cart[3],u_cart[4],u_cart[5]);
      dmat6 beta = cctbx::adptbx::u_star_as_beta(atoms[a].SCAT.u_star);
      output.logTabPrintf(1,VERBOSE,"BETA   %9.8f %9.8f %9.8f %9.8f %9.8f %9.8f\n",
        beta[0],beta[1],beta[2],beta[3],beta[4],beta[5]);
      output.logTabPrintf(1,VERBOSE,"USTAR  %9.8f %9.8f %9.8f %9.8f %9.8f %9.8f\n",
        atoms[a].SCAT.u_star[0],atoms[a].SCAT.u_star[1],atoms[a].SCAT.u_star[2],atoms[a].SCAT.u_star[3],atoms[a].SCAT.u_star[4],atoms[a].SCAT.u_star[5]);
    }
  }
  if (!atoms.size())
    output.logTab(1,where,"There were no atoms");
  output.logBlank(where);
}

void RefineSAD::logSigmaA(outStream where,Output& output)
{
  output.logTab(1,where,"SigmaA Parameters:");
  int smin(0),smax(bin.numbins()-1);
  output.logTab(2,where,"SDsqr: (lores-hires) " + dtos(SDsqr_bin[smin],12,0) + " to " + dtos(SDsqr_bin[smax],12,0));
  output.logTab(2,where,"Dphi : (lores-hires) " + dtos(DphiA_bin[smin],10,8)+","+dtos(DphiB_bin[smin],10,8) + ") to (" + dtos(DphiA_bin[smax],10,8)+","+dtos(DphiB_bin[smax],10,8) + ")");
  output.logTab(2,where,"Splus: (lores-hires) " + dtos(SP_bin[smin],12,8) + " to " + dtos(SP_bin[smax],12,8));
  output.logBlank(where);
  output.logTabPrintf(1,VERBOSE,"%3s   %-12s    %-23s %-12s\n","Bin","SDsqr","s12 (real,imag)","Sigma+");
  for (int s = 0; s < bin.numbins(); s++)
    output.logTabPrintf(1,VERBOSE,"%3d: %12.0f    (% 10.8f,% 10.8f) %12.8f\n", s+1,SDsqr_bin[s],DphiA_bin[s],DphiB_bin[s],SP_bin[s]);
  output.logBlank(VERBOSE);
}

void RefineSAD::logScat(outStream where,Output& output)
{
  output.logTab(1,where,"Scattering Parameters:");
  output.logTabPrintf(1,where,"%5s %14s %14s\n", "Atom","f\"","(f\')");
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    PHASER_ASSERT(t < t2atomtype.size());
    output.logTabPrintf(1,where,"%5s %14.4f %14.4f\n", t2atomtype[t].c_str(),AtomFdp[t],AtomFp[t]);
  }
  output.logBlank(where);
}

void RefineSAD::logStatInfo(Output& output,outStream where)
{
  output.logTab(1,where,"Unweighted");
  output.logTab(1,where,"Statistics are calculated with \'bestphase\' PhiB");
  output.logTab(2,where,"Phase difference =  < |PhiB - (Phi of FH)|> ");
  output.logTab(1,where,"Weighted");
  output.logTab(1,where,"Statistics are calculated as probablity weighted sum over Phi (0-360)");
  output.logTab(2,where,"Wt Phase difference =  < SumPhi Prob(Phi)*|Phi - (Phi of FH)|> ");
  output.logTab(1,where,"where <f(x)> is the mean value of f(x) for the reflections in the bin");
  output.logTab(1,where,"Arithmetic averages are used instead of root mean square (rms) averages,");
  output.logTab(1,where,"because although the latter are theoretically correct, they are very");
  output.logTab(1,where,"sensitive to outliers");
  output.logTab(1,where,"The mean absolute phase difference should be 90deg (within about 10deg)");
  output.logTab(1,where,"The standard deviation should be 52deg for acentrics and 90deg for centrics,");
  output.logTab(1,where,"although these values will probably not be attained in SIR or when there");
  output.logTab(1,where,"is significant correlation between the phased and derivative structures");
  output.logTab(1,where,"i.e. the heavy atoms replace atoms in the phased structure");
  output.logBlank(where);
}

void RefineSAD::logFOM(outStream where,Output& output,bool print_loggraph)
{
  calcBinStats();
  output.logUnderLine(where,"Figures of Merit");
  output.logTabPrintf(1,where,"%3s %-12s %-12s %-12s %-12s %-12s\n",
          "Bin","Resolution","Acentric","Centric","Single","Total");
  output.logTabPrintf(1,where,"%3s %-12s %-6s %-5s %-6s %-5s %-6s %-5s %-6s %-5s\n",
          " "," ","Number","FOM","Number","FOM","Number","FOM","Number","FOM");
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    output.logTabPrintf(1,VERBOSE,"%3d %6.2f-%5.2f %6d %5.3f %6d %5.3f %6d %5.3f %6d %5.3f\n",
      s+1, bin.LoRes(s), bin.HiRes(s),
       acentStats.Num(s), acentStats.FOM(s),
       centStats.Num(s), centStats.FOM(s),
       singleStats.Num(s), singleStats.FOM(s),
       allStats.Num(s), allStats.FOM(s));
  }
  output.logTabPrintf(1,where,"ALL %6.2f-%5.2f %6d %5.3f %6d %5.3f %6d %5.3f %6d %5.3f\n",
     bin.LoRes(0),bin.HiRes(bin.numbins()-1),
       acentStats.Num(), acentStats.FOM(),
       centStats.Num(), centStats.FOM(),
       singleStats.Num(), singleStats.FOM(),
       allStats.Num(), allStats.FOM());
  output.logBlank(where);

  if (PTNCS.use_and_present())
  {
    output.logBlank(where);
    float1D FOM(3,0);
    int1D   Num(3,0);
    PHASER_ASSERT(PTNCS.EPSFAC.size() == NREFL);
    for (int c = 0; c < 3; c++) //lo,mid,hi
    {
      af_float subset(NREFL,false);
      for (int r = 0; r < NREFL; r++)
      if (selected[r])
      {
        if (c == 0 && PTNCS.EPSFAC[r] < 0.5) { Num[c]++; subset[r] = true; }
        if (c == 1 && PTNCS.EPSFAC[r] >= 0.5 && PTNCS.EPSFAC[r] <= 1.5) { Num[c]++; subset[r] = true; }
        if (c == 2 && PTNCS.EPSFAC[r] > 1.5) { Num[c]++; subset[r] = true; }
      }
      FOM[c] = calcFOMsubset(subset);
    }
    output.logTabPrintf(1,where,"%3s %-12s %-12s %-12s %-12s %-12s\n",
            "","Resolution","Low Epsn","Mid Epsn","High Epsn","Total");
    output.logTabPrintf(1,where,"%3s %-12s %-6s %-5s %-6s %-5s %-6s %-5s %-6s %-5s\n",
            " "," ","Number","FOM","Number","FOM","Number","FOM","Number","FOM");
    output.logTabPrintf(1,where,"ALL %6.2f-%5.2f %6d %5.3f %6d %5.3f %6d %5.3f %6d %5.3f\n",
       bin.LoRes(0),bin.HiRes(bin.numbins()-1),
         Num[0], FOM[0],
         Num[1], FOM[1],
         Num[2], FOM[2],
         allStats.Num(), allStats.FOM());
    output.logBlank(where);
  }

//logGraph
  std::string loggraph;
  for (unsigned s = 0; s < bin.numbins(); s++)
    loggraph += dtos(1.0/fn::pow2(bin.MidRes(s)),5,4) + " " +
        dtos(acentStats.FOM(s),5,3) + " " +
        dtos(centStats.FOM(s),5,3) + " "  +
        dtos(singleStats.FOM(s),5,3) + " "  +
        dtos(allStats.FOM(s)) + "\n";
  string1D graph(1);
           graph[0] = "FOM vs Resolution:AUTO:1,2,3,4,5";
  if (print_loggraph)
  output.logGraph(LOGFILE,"Figures of Merit",graph,"1/d^2 Acentrics Centrics Singletons All",loggraph);
}

void RefineSAD::logDataStats(outStream where,unsigned nrefl,Output& output)
{
  output.logUnderLine(where,"Reflection statistics");
  int nboth(0),npos(0),nneg(0),ncent(0);
  int1D NumInBin(bin.numbins(),0),NumInBinC(bin.numbins(),0),NumInBinA(bin.numbins(),0);
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    if (cent[r]) ncent++;
    else if (both[r]) nboth++;
    else if (plus[r]) npos++;
    else nneg++;
    cent[r] ? NumInBinC[rbin[r]]++ : NumInBinA[rbin[r]]++;
    NumInBin[rbin[r]]++;
  }
  output.logTab(1,where,"There were " +itos(ncent) + " centric reflections");
  output.logTab(1,where,"There were " +itos(nboth) + " acentric reflections with F+ and F-");
  output.logTab(1,where,"There were " +itos(npos) + " acentric reflections with F+ only");
  output.logTab(1,where,"There were " +itos(nneg) + " acentric reflections with F- only");
  output.logBlank(where);
  output.logTabPrintf(1,where,"%3s    %10s    %5s %9s %9s\n","Bin","Resolution","#All","#Centric","#Acentric");
  for (unsigned s = 0; s < bin.numbins(); s++)
    output.logTabPrintf(1,where,"%3d   %5.2f - %5.2f  %5d %9d %9d\n",
        s+1, bin.LoRes(s), bin.HiRes(s), NumInBin[s],NumInBinC[s],NumInBinA[s]);

  output.logBlank(where);

  if (ncent)
  {
    output.logTab(1,where,"CENTRIC reflections:");
    output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso");
    output.logTabPrintf(0,where,"%12s %10s\n","F","SIGF");
    unsigned rcent(0);
    for (int r = 0; r < NREFL && rcent < nrefl; r++)
    if (!bad_data[r] && cent[r])
    {
      output.logTabPrintf(1,where,"%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f\n",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r]);
      rcent++;
    }
    output.logBlank(where);
  }
  if (nboth)
  {
    output.logTab(1,where,"ACENTRIC reflections:");
    output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso");
    output.logTabPrintf(0,where,"%12s %10s %12s %10s\n","F(+)","SIGF(+)","F(-)","SIGF(-)");
    unsigned rboth(0);
    for (int r = 0; r < NREFL && rboth < nrefl; r++)
    if (!bad_data[r] && both[r])
    {
      output.logTabPrintf(1,where,"%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f %12.3f %10.3f\n",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r],NEG.F[r],NEG.SIGF[r]);
      rboth++;
    }
    output.logBlank(where);
  }
  if (npos)
  {
    output.logTab(1,where,"SINGLETON(+) reflections:");
    output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso");
    output.logTabPrintf(0,where,"%12s %10s %12s %10s\n","F(+)","SIGF(+)","F(-)","SIGF(-)");
    unsigned rpos(0);
    for (int r = 0; r < NREFL && rpos < nrefl; r++)
    if (!bad_data[r] && !cent[r] && !both[r] && plus[r])
    {
      output.logTabPrintf(1,where,"%-3d %-3d %-3d (%5d) %4.3f %5.2f %12.3f %10.3f %12s %10s\n",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),POS.F[r],POS.SIGF[r],"absent","absent");
      rpos++;
    }
    output.logBlank(where);
  }
  if (nneg)
  {
    output.logTab(1,where,"SINGLETON(-) reflections:");
    output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s ","H","K","L","(refl#)","epsn","reso");
    output.logTabPrintf(0,where,"%12s %10s %12s %10s\n","F(+)","SIGF(+)","F(-)","SIGF(-)");
    unsigned rneg(0);
    for (int r = 0; r < NREFL && rneg < nrefl; r++)
    if (!bad_data[r] && !cent[r] && !both[r] && !plus[r])
    {
        output.logTabPrintf(1,where,"%-3d %-3d %-3d (%5d) %4.3f %5.2f %12s %10s %12.3f %10.3f\n",
           miller[r][0],miller[r][1],miller[r][2],r+1,
           epsn[r],UnitCell::reso(miller[r]),"absent","absent",NEG.F[r],NEG.SIGF[r]);
      rneg++;
    }
    output.logBlank(where);
  }
}

void RefineSAD::logCurrent(outStream where,Output& output)
{
  output.logUnderLine(where,"Current parameters");
  logScale(where,output);
  if (!FIX_ATOMIC)
  {
    logAtoms(where,output);
    logScat(where,output);
  }
  if (!FIX_SIGMAA)
  {
    logSigmaA(where,output);
  }
  logFOM(where,output,false);
  output.logTab(1,where,"Log-likelihood = " + dtos(-atoms.LLG));
  output.logBlank(where);
}

void RefineSAD::logInitial(outStream where,Output& output)
{ }

void RefineSAD::logFinal(outStream where,Output& output)
{ }

void RefineSAD::logProtocolPars(outStream where,Output& output)
{
  output.logTab(1,where,"Parameters to refine:");
  char YES('+'),NO('-');
//--sigmaa--
  int m(0);
  char SAYN = refinePar[m++] ? YES : NO;
  char SBYN = refinePar[m++] ? YES : NO;
  char SPYN = refinePar[m++] ? YES : NO;
  char SDYN = refinePar[m++] ? YES : NO;
  output.logTabPrintf(2,where,"Variances SA SB SP SD: %c %c %c %c (%d Bins)\n",SAYN,SBYN,SPYN,SDYN,bin.numbins());
  for (unsigned s = 1; s < bin.numbins(); s++) m += 4;

//-- scales --
  if (input_atoms)
  {
    output.logTabPrintf(2,where,"Overall Scale:         %c\n",refinePar[m++] ? YES : NO); //ScaleK
    output.logTabPrintf(2,where,"Overall B-factor:      %c\n",refinePar[m++] ? YES : NO); //ScaleU
  }
  if (input_partial)
  {
    output.logTabPrintf(2,where,"Partial Scale:         %c\n",refinePar[m++] ? YES : NO); //PartK
    output.logTabPrintf(2,where,"Partial B-factor:      %c\n",refinePar[m++] ? YES : NO); //PartU
  }

//-- atomic --
  int last_diff_a(0),last_a(0);
  char lastXYN(' '),lastOYN(' '),lastBYN(' '),lastAYN(' ');
  int natoms(0);
  for (unsigned a = 0; a < atoms.size(); a++) if (!atoms[a].REJECTED) natoms++;
  bool first_time(true);
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    char XYN = refinePar[m++] ? YES : NO;
    char OYN = refinePar[m++] ? YES : NO;
    char BYN = !atoms[a].SCAT.flags.use_u_aniso_only() ? (refinePar[m++] ? YES : NO) : NO;
    char AYN = atoms[a].SCAT.flags.use_u_aniso_only() ? (refinePar[m++] ? YES : NO) : NO;
    if (first_time)
    {
      output.logTabPrintf(2,where,"Atomic X Y Z O B AB:\n");
      lastXYN = XYN; lastOYN = OYN; lastBYN = BYN; lastAYN = AYN;
      first_time = false;
    }
    if (lastXYN != XYN || lastOYN != OYN || lastBYN != BYN || lastAYN != AYN)
    {
      if (last_diff_a < last_a)
        output.logTabPrintf(3,where,"Atoms #%-3i to #%-3i   %c %c %c %c %c %c\n",
           last_diff_a+1,last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN);
      else
        output.logTabPrintf(3,where,"Atom  #%-3i           %c %c %c %c %c %c\n",
           last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN);
      last_diff_a = a;
      { lastXYN = XYN; lastOYN = OYN; lastBYN = BYN; lastAYN = AYN; }
    }
    last_a = a;
  }
  if (last_diff_a < last_a && natoms)
    output.logTabPrintf(3,where,"Atoms #%-3i to #%-3i   %c %c %c %c %c %c\n",
       last_diff_a+1,last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN);
  else if (natoms)
    output.logTabPrintf(3,where,"Atom  #%-3i           %c %c %c %c %c %c\n",
       last_a+1,lastXYN,lastXYN,lastXYN,lastOYN,lastBYN,lastAYN);
  else if (!input_atoms) //phenix, setFC
    output.logTab(2,where,"Atomic X Y Z O B AB:   None (partial structure only)");

//Fdp
  for (int t = 0; t < AtomFdp.size(); t++)
    output.logTabPrintf(2,where,"Atom Type %2s f\":       %c\n",t2atomtype[t].c_str(),refinePar[m++] ? YES : NO);
  output.logBlank(where);

  PHASER_ASSERT(m == refinePar.size());
  use_fft ? output.logTab(1,where,"FFT method will be used for structure factor and gradient calculations"):
            output.logTab(1,where,"Direct summation will be used for structure factor and gradient calculations");
  output.logBlank(where);
}

void RefineSAD::logOutliers(outStream where,Output& output)
{
  output.logUnderLine(where,"Outlier Rejection");
  if (!OUTLIER.REJECT)
  {
    output.logTab(1,where,"Outliers will not be rejected");
    output.logBlank(where);
    return;
  }
  if (!OUTLIER.ANO.size() && !OUTLIER.SAD.size())
  {
    output.logTab(1,where,"There were 0 reflections rejected as outliers");
    output.logBlank(where);
    return;
  }
  if (OUTLIER.ANO.size())
  {
    floatType percent = 100.*double(OUTLIER.ANO.size())/NREFL;
    OUTLIER.ANO.size() == 1 ?
    output.logTab(1,where,"There was 1 reflection of " + itos(NREFL) + " (" + dtos(percent,4) + "%) rejected as Wilson outlier"):
    output.logTab(1,where,"There were " + itos(OUTLIER.ANO.size()) + " reflections of " + itos(NREFL) + " (" + dtos(percent,4) + "%) rejected as Wilson outliers");
    output.logTab(1,where,"Measurements with a probability less than " + dtos(OUTLIER.PROB) + " are rejected");
    output.logTab(1,where,"Measurements with fewer bits of expected information than " + dtos(OUTLIER.INFO) + " are rejected:");
    output.logTab(2,VERBOSE,"threshold sigma(Eobs^2) for centrics/acentrics is " + dtos(OUTLIER.SIGESQ_CENT) + " / " + dtos(OUTLIER.SIGESQ_ACENT));
    output.logBlank(where);
    int maxprint(20);
    output.logTabPrintf(1,where,"%4s %4s %4s %6s  %10s  %10s %11s %7s %8s\n","H","K","L","reso","Eo^2","sigma","probability","wilson","low-info");
    for (int o = 0; o < OUTLIER.ANO.size(); o++)
    {
      int& r = OUTLIER.ANO[o].refl;
      int H(phaser::round(miller[r][0]));
      int K(phaser::round(miller[r][1]));
      int L(phaser::round(miller[r][2]));
      double reso = UnitCell::reso(miller[r]);
      if (o == maxprint) output.logTab(1,where,"More than " + itos(maxprint) + " outliers (see VERBOSE output)");
      if (std::fabs(OUTLIER.ANO[o].eosqr) < 1.0e+7 && std::fabs(OUTLIER.ANO[o].sigesqr) < 1.0e+7)
        output.logTabPrintf(1,o>=maxprint?VERBOSE:where,"%4i %4i %4i %6.2f % 11.3f % 11.3f %11.3e %-7s %-8s\n", H,K,L,reso,OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,btos(OUTLIER.ANO[o].wilson).c_str(),btos(OUTLIER.ANO[o].lowInfo).c_str());
      else
        output.logTabPrintf(1,o>=maxprint?VERBOSE:where,"%4i %4i %4i %6.2f % 11.6e % 11.6e %11.3e %-7s %-8s\n", H,K,L,reso,OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,btos(OUTLIER.ANO[o].wilson).c_str(),btos(OUTLIER.ANO[o].lowInfo).c_str());
    }
    output.logBlank(where);
  }
  if (OUTLIER.SAD.size())
  {
    floatType percent = 100.*double(OUTLIER.SAD.size())/NREFL;
    OUTLIER.SAD.size() == 1 ?
    output.logTab(1,where,"There was 1 reflection of " + itos(NREFL) + " (" + dtos(percent,4) + "%) rejected as SAD outlier"):
    output.logTab(1,where,"There were " + itos(OUTLIER.SAD.size()) + " reflections of " + itos(NREFL) + " (" + dtos((OUTLIER.SAD.size()*100)/floatType(NREFL),4) + "%) rejected as SAD outliers");
    std::sort(OUTLIER.SAD.begin(),OUTLIER.SAD.end());
    output.logTab(1,where,"Reflections listed with most improbable first");
    output.logTab(1,where,"Criterion on which reflection rejected indicated with *");
    output.logBlank(where);
    int num_output = 10;
    bool first_print(true);
    int counto(0);
    for (int o = 0; o < OUTLIER.SAD.size(); o++)
    {
      if (first_print)
      output.logTabPrintf(1,where,"%3s %3s %3s %8s %6s %9s %-9s %8s %8s %8s\n",
        "H","K","L","","reso","F+","F-","P(F+)","P(F-)","P(F+;F-)");
      first_print = false;
      int r = OUTLIER.SAD[o].refl;
      std::string ac = cent[r] ? "centric " : "acentric";
      if ((cent[r]) || (!both[r] &&  plus[r]))
      output.logTabPrintf(1,counto < num_output ? where : VERBOSE,"%3i %3i %3i %s %6.1f %9.1f %9s %3.1e*\n",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),
        UnitCell::reso(miller[r]),
        POS.F[r],"",OUTLIER.SAD[o].probPos);
      else if (!both[r] && !plus[r])
      output.logTabPrintf(1,counto < num_output ? where : VERBOSE,"%3i %3i %3i %s %6.1f %9s %-9.1f %8s %3.1e*\n",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),
        UnitCell::reso(miller[r]),
        "",NEG.F[r],"",OUTLIER.SAD[o].probNeg);
      else
      output.logTabPrintf(1,counto < num_output ? where : VERBOSE,"%3i %3i %3i %s %6.2f %9.1f %-9.1f %3.1e%s %3.1e%s %3.1e%s\n",
        int(miller[r][0]),int(miller[r][1]),int(miller[r][2]),ac.c_str(),UnitCell::reso(miller[r]),
        POS.F[r],NEG.F[r],
        OUTLIER.SAD[o].probPos, OUTLIER.SAD[o].probPos < OUTLIER.SAD[o].probLow() ? "*" : " ",
        OUTLIER.SAD[o].probNeg, OUTLIER.SAD[o].probNeg == OUTLIER.SAD[o].probLow() ? "*" : " ",
        OUTLIER.SAD[o].probCon, OUTLIER.SAD[o].probCon == OUTLIER.SAD[o].probLow() ? "*" : " ");
      counto++;
      if (counto == num_output) output.logTab(1,where," <-------  Top " + itos(num_output) + " (use VERBOSE to print all) ------->");
    }
    if (!first_print) output.logBlank(where);
  }
}

} //namespace phaser
