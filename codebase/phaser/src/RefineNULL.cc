//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/src/RefineNULL.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

RefineNULL::RefineNULL(std::string SG_HALL,
                       af::double6 UNIT_CELL,
                       af::shared<miller::index<int> > MILLER,
                       data_spots POS_,data_spots NEG_,
                       data_tncs PTNCS_,
                       data_bins DATA_BINS,
                       std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES,
                       data_outl OUTLIER,
                       data_part PARTIAL,
                       Output& output
) :  RefineBase2(),
     DataSAD(SpaceGroup(SG_HALL),UnitCell(UNIT_CELL),MILLER,POS_,NEG_,data_tncs(),DATA_BINS,data_outl(),PARTIAL,ATOMTYPES,output)
{
  CCC.resize(bin.numbins());
}

void RefineNULL::rejectOutliers(outStream where,Output& output)
{
}

double RefineNULL::targetFn()
{
  double target = 0.0;
  return target;
}

double RefineNULL::gradientFn(TNT::Vector<double>& Gradient)
{
  double minusLL(0);
  return minusLL;
}

double RefineNULL::hessianFn(TNT::Fortran_Matrix<double>& Hessian, bool& is_diagonal)
{
  double minusLL(0);
  return minusLL;
}

bool1D RefineNULL::getRefineMask(protocolPtr p)
{
  ProtocolNULL protocol;
  protocol.FIX_CCC = p->getFIX(macz_ccc);

  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
  for (int s = 0; s < bin.numbins(); s++)
    protocol.FIX_CCC ? refineMask.push_back(REFINE_OFF): refineMask.push_back(REFINE_ON);
  return refineMask;
}

TNT::Vector<double> RefineNULL::getLargeShifts()
{
  int m(0),i(0);
  TNT::Vector<double> largeShifts(npars_ref);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++])
      largeShifts[i++] = 1;
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return largeShifts;
}

void RefineNULL::applyShift(TNT::Vector<double>& newx)
{
  int m(0),i(0);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++])
      CCC[i++] = 1;
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
}

void RefineNULL::logCurrent(outStream where,Output& output)
{
  output.logUnderLine(SUMMARY,"Refined Null Hypthesis Parameters");
  output.logTabPrintf(1,where,"%3s %-12s %-12s\n",
          "Bin","Resolution","CCC");
  for (unsigned s = 0; s < bin.numbins(); s++)
    output.logTabPrintf(1,VERBOSE,"%3d %6.2f-%5.2f %6.4f\n",
      s+1, bin.LoRes(s), bin.HiRes(s), CCC[s]);
  double avCCC(0);
  output.logTabPrintf(1,where,"ALL %6.2f-%5.2f %6.4f\n",
     bin.LoRes(0),bin.HiRes(bin.numbins()-1), avCCC);
  output.logBlank(where);
}

void RefineNULL::logInitial(outStream where,Output& output)
{
  logCurrent(where,output);
}

void RefineNULL::logFinal(outStream where,Output& output)
{
  logCurrent(where,output);
}

std::string RefineNULL::whatAmI(int& ipar)
{
  int i(0),m(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++])
      if (i++ == ipar)
        return "Complex Correlation Coefficient Bin#" + itos(s+1);
  PHASER_ASSERT(i == npars_ref);
  return "Undefined Parameter";
}

TNT::Vector<double> RefineNULL::getRefinePars()
{
  int m(0),i(0);
  TNT::Vector<double> pars(npars_ref);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++])
      pars[i++] = CCC[s];
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return pars;
}

std::vector<reparams> RefineNULL::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) repar[i++].off();
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return repar;
}

std::vector<bounds> RefineNULL::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  int i(0),m(0);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Lower[i++].on(0.);
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

std::vector<bounds > RefineNULL::getUpperBounds()
{
  std::vector<bounds > Upper(npars_ref);
  int i(0),m(0);
  for (int s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Upper[i++].off();
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

void RefineNULL::cleanUp(outStream where,Output& output)
{
}

}//phaser
