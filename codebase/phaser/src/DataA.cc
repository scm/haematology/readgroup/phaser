//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/DataA.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/solTerm.h>
#include <scitbx/constants.h>
#include <cctbx/sgtbx/reciprocal_space_asu.h>
#include <cctbx/miller/asu.h>
#include <cctbx/adptbx.h>
#include <scitbx/math/erf.h>
#include <cstring>
#include <cctbx/miller/index_generator.h>

namespace phaser {

DataA::DataA(
        data_refl& REFLECTIONS_,
        data_resharp& RESHARP_,
        data_norm& SIGMAN_,
        data_outl& OUTLIER_,
        data_tncs& PTNCS_)
      : SpaceGroup(REFLECTIONS_.SG_HALL),
        UnitCell(REFLECTIONS_.UNIT_CELL),
        data_refl(REFLECTIONS_),
        SIGMAN(SIGMAN_),
        OUTLIER(OUTLIER_),
        RESHARP(RESHARP_),
        PTNCS(PTNCS_)
{
  // --- Read HKL of reflections ---
  NREFL = MILLER.size();
  INPUT_INTENSITIES = I.size();
  set_default_dfactor();
  selected.resize(NREFL,true);
  debugging = false;
} //end constructor

DataA::DataA(
        af::double6 UNIT_CELL_,
        data_refl& REFLECTIONS_,
        data_resharp& RESHARP_,
        data_norm& SIGMAN_,
        data_outl& OUTLIER_)
      : SpaceGroup(REFLECTIONS_.SG_HALL),
        UnitCell(UNIT_CELL_),
        data_refl(REFLECTIONS_),
        SIGMAN(SIGMAN_),
        OUTLIER(OUTLIER_),
        RESHARP(RESHARP_),
        PTNCS(data_tncs())
{
  // --- Read HKL of reflections ---
  NREFL = MILLER.size();
  INPUT_INTENSITIES = I.size();
  set_default_dfactor();
  selected.resize(NREFL,true);
  debugging = false;
} //end constructor

floatType DataA::getResharpB()
{
  //dmat6 anisoBeta = AnisoBetaRemoveIsoB(SIGMAN.ANISO,
  //          aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar(),
  //          A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  //i.e. Remove Wilson IsoB component
  //sharpening factor is obtained from most negative eigenvalue of
  //anisotropic part of SIGMAN.ANISO
  dvect3 eigenBvals = getEigenBs();
  floatType minB(std::min(eigenBvals[0],std::min(eigenBvals[1],eigenBvals[2])));
  floatType RESHARPB = minB*RESHARP.FRAC; //class variable
  return RESHARPB;
}

floatType DataA::anisoTerm(const unsigned& r,dmat6& anisoBeta)
{
  floatType AnisoExp(0);
  AnisoExp -= anisoBeta[0]*MILLER[r][0]*MILLER[r][0];
  AnisoExp -= anisoBeta[1]*MILLER[r][1]*MILLER[r][1];
  AnisoExp -= anisoBeta[2]*MILLER[r][2]*MILLER[r][2];
  AnisoExp -= anisoBeta[3]*MILLER[r][0]*MILLER[r][1];
  AnisoExp -= anisoBeta[4]*MILLER[r][0]*MILLER[r][2];
  AnisoExp -= anisoBeta[5]*MILLER[r][1]*MILLER[r][2];
  return  floatType(std::exp(AnisoExp));
}

af_float DataA::getCorrection(bool resharpen)
{
  dmat6 anisoBeta = AnisoBetaRemoveIsoB(SIGMAN.ANISO,
            aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar(),
            A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  //i.e. Remove Wilson IsoB component

  //optional resharpening factor
  if (resharpen)
  {
    double resharpB = 2.*getResharpB(); // Need value for intensities, not amplitudes
    dmat6 deltaAniso = IsoB2AnisoBeta(resharpB,
                                      aStar(),bStar(),cStar(),
                                      cosAlphaStar(),cosBetaStar(),cosGammaStar());
    for (unsigned n = 0; n < 6; n++)
      anisoBeta[n] -= deltaAniso[n]; //put in sharpening factor
  }

  af_float correction(NREFL);
  for (unsigned r = 0; r < NREFL; r++)
   //not just selected, must match incoming array size
    correction[r] =  1.0/std::sqrt(anisoTerm(r,anisoBeta)); //multiplicative factor
  return correction;
}

af_float DataA::getCorrectedF(bool resharpen)
{
  af_float corrected = getCorrection(resharpen);
  af_float Fdat = INPUT_INTENSITIES ? FfromI : F;
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= Fdat[r];
  return corrected;
}

af_float DataA::getCorrectedSIGF(bool resharpen)
{
  af_float corrected = getCorrection(resharpen);
  af_float SIGFdat = INPUT_INTENSITIES ? SIGFfromI : SIGF;
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= SIGFdat[r];
  return corrected;
}

af_float DataA::getCorrectedI(bool resharpen)
{
  af_float corrected = getCorrection(resharpen);
  af_float Idat = INPUT_INTENSITIES ? I : IfromF;
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= corrected[r];
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= Idat[r];
  return corrected;
}

af_float DataA::getCorrectedSIGI(bool resharpen)
{
  af_float corrected = getCorrection(resharpen);
  af_float SIGIdat = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= corrected[r];
  for (unsigned r = 0; r < NREFL; r++) corrected[r] *= SIGIdat[r];
  return corrected;
}

af::shared<miller::index<int> > DataA::getSelectedMiller()
{
  af::shared<miller::index<int> > sel_miller;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    sel_miller.push_back(MILLER[r]);
  return sel_miller;
}

std::vector<ssqr_dfac_solt> DataA::getSelected(data_solpar& SOLPAR)
{ //memory
  std::vector<ssqr_dfac_solt> ssqr_dfac;
  for (int r = 0; r < NREFL; r++)
  if (selected[r])
  {
    double ssqr = getCctbxUC().d_star_sq(MILLER[r]);
    double dfac_solt = DFAC[r]*solTerm(ssqr,SOLPAR);
    ssqr_dfac.push_back(ssqr_dfac_solt(ssqr,dfac_solt));
  }
  std::sort(ssqr_dfac.begin(),ssqr_dfac.end()); //important for ellg calculation!!
  return ssqr_dfac;
}

std::vector<ssqr_dfac_solt> DataA::getPerfect(data_solpar& SOLPAR,double resolution_d_min)
{ //memory
  std::vector<ssqr_dfac_solt> ssqr_dfac;
  bool anomalous_flag(false);
 // cctbx::uctbx::unit_cell unit_cell(af::double6(UNIT_CELL[0],UNIT_CELL[1],UNIT_CELL[2],90.0,90.0,90.0));
  cctbx::uctbx::unit_cell unit_cell(getCctbxUC());
  cctbx::sgtbx::space_group_type sg_type(getCctbxSG());
  cctbx::miller::index_generator generator(unit_cell,sg_type,anomalous_flag,resolution_d_min);
  cctbx::miller::index<int> hkl = generator.next();
  double ssqr_max(1.0/fn::pow2(resolution_d_min));
  while (hkl != cctbx::miller::index<int>(0,0,0)) //flags end
  {
    double ssqr(unit_cell.d_star_sq(hkl));
    if (ssqr < ssqr_max)
    {
      //double ssqr = getCctbxUC().d_star_sq(hkl);
      double DFAC(1);//perfect data
      double dfac_solt = DFAC*solTerm(ssqr,SOLPAR);
      ssqr_dfac.push_back(ssqr_dfac_solt(ssqr,dfac_solt));
    }
    hkl = generator.next();
  };
  std::sort(ssqr_dfac.begin(),ssqr_dfac.end()); //important for ellg calculation!!
  return ssqr_dfac;
}

af_float DataA::getBINS()
{
  af_float bins(SIGMAN.BINS.size());
  for (unsigned s = 0; s < SIGMAN.BINS.size(); s++)
    bins[s] = SIGMAN.BINS[s];
  return bins;
}

floatType DataA::HiRes()
{
  floatType hires(DEF_LORES);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    hires = std::min(hires,UnitCell::reso(MILLER[r]));
  return hires;
}

floatType DataA::LoRes()
{
  floatType lores(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    lores = std::max(lores,UnitCell::reso(MILLER[r]));
  return lores;
}

floatType DataA::fullHiRes()
{
  floatType hires(DEF_LORES);
  for (unsigned r = 0; r < MILLER.size(); r++)
    hires = std::min(UnitCell::reso(MILLER[r]),hires);
  return hires;
}

floatType DataA::fullLoRes()
{
  floatType lores(0);
  for (unsigned r = 0; r < MILLER.size(); r++)
    lores = std::max(UnitCell::reso(MILLER[r]),lores);
  return lores;
}

floatType DataA::getIsoB()
{
  //dmat6 anisoU = AnisoBeta2AnisoU(SIGMAN.ANISO,aStar(),bStar(),cStar());
 // dmat6 anisoB = AnisoBeta2AnisoB(SIGMAN.ANISO,aStar(),bStar(),cStar());
//  Determine isotropic component of anisoB
  floatType isotropicB_I = AnisoBeta2IsoB(SIGMAN.ANISO,A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  return isotropicB_I/2.0; //value on F's
}

cctbx::adptbx::eigensystem<floatType> DataA::getEigenSystem()
{
// Get principal components of thermal motion tensor
  dmat33 betaAsMatrix(
      SIGMAN.ANISO[0],  SIGMAN.ANISO[3]/2,SIGMAN.ANISO[4]/2,
      SIGMAN.ANISO[3]/2,SIGMAN.ANISO[1],  SIGMAN.ANISO[5]/2,
      SIGMAN.ANISO[4]/2,SIGMAN.ANISO[5]/2,SIGMAN.ANISO[2]);
// See Int Tab IV, p315.  Factor of 4 for 1/4 term in B-factor expression
  scitbx::mat3<floatType> orthTensor = 4.0*Frac2Orth()*betaAsMatrix*Frac2Orth().transpose();
  dmat6 sym_tensor(orthTensor,0.0001);
  cctbx::adptbx::eigensystem<floatType> eigenBs(sym_tensor);
  return eigenBs;
}

dvect3 DataA::getEigenBs()
{
  floatType isotropicB_I = getIsoB()*2;
  floatType tol(1.0e-06);
  dvect3 eigenBvals = getEigenSystem().values();
  for (unsigned n = 0; n < 3; n++)
  {
    eigenBvals[n] = (eigenBvals[n] - isotropicB_I)/2;
    if (std::fabs(eigenBvals[n]) < tol) eigenBvals[n] = 0;
  }
  return eigenBvals;
}

floatType DataA::getAnisoDeltaB()
{
  dvect3 eigenBvals = getEigenBs();
  floatType minB(std::min(eigenBvals[0],std::min(eigenBvals[1],eigenBvals[2])));
  floatType maxB(std::max(eigenBvals[0],std::max(eigenBvals[1],eigenBvals[2])));
  return maxB-minB;
}


unsigned DataA::NumInBin(const unsigned& s)
{
  int numInBin(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r] && rbin(r) == s) numInBin++;
  return numInBin;
}

unsigned  DataA::rbin(const int& r) {  return  bin.get_bin(S(MILLER[r])); }

} //phaser
