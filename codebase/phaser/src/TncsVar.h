//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_PHASER_TNCSVAR_Class__
#define __DATA_PHASER_TNCSVAR_Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_tncs.h>
#include <phaser/include/data_solpar.h>

namespace phaser {

class TncsVar: public data_tncs
{
  public:
    TncsVar(data_tncs);

    floatType lowerVAR(int);
    floatType upperVAR(int);
    floatType D(int s=-999);
    floatType lower_var_limit();
};

} //phaser

#endif
