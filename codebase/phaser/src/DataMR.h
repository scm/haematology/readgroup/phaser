//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DataMRClass__
#define __DataMRClass__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/pod/MapCoefs.h>
#include <phaser/include/data_btf.h>
#include <phaser/include/data_ftfmap.h>
#include <phaser/include/data_bofac.h>
#include <phaser/include/data_composition.h>
#include <phaser/src/DataB.h>
#include <phaser/src/MapEnsemble.h>
#include <phaser/src/Hexagonal.h>
#include <phaser/src/Brick.h>
#include <phaser/lib/maths.h>

namespace phaser {

class DataMR : public DataB, public MapEnsPtr
{
  public:
    //Constructor and Destructor
    DataMR(data_refl&,
           data_resharp&,
           data_norm&,
           data_outl&,
           data_tncs&,
           data_bins&,
           double,double,
           data_composition&,
           MapEnsPtr ensPtr);
    DataMR() { TOTAL_SCAT = 1; } //ensemble = NULL; }

    DataMR(const DataMR & init) : DataB((DataB&)init) // copy constructor necessary because of fast_cos_sin
    { setDataMR(init); }

    const DataMR& operator=(const DataMR& right)
    {
      if (&right != this)
      {
        setDataMR(right);
        (DataB&)*this = right;
      }
      return *this;
    }

    void setDataMR(const DataMR & init)
    {
       TOTAL_SCAT = init.TOTAL_SCAT;
       m_alogch = init.m_alogch;
       m_alogchI0 = init.m_alogchI0;

       models_known = init.models_known;
       models_perturbRot = init.models_perturbRot;
       models_perturbTrans = init.models_perturbTrans;
       models_initBfac = init.models_initBfac;
       models_initOfac = init.models_initOfac;
       models_drms = init.models_drms;
       models_cell = init.models_cell;

       EM_known = init.EM_known;
       totvar_known = init.totvar_known;
       EM_search = init.EM_search;
       sum_Esqr_search = init.sum_Esqr_search;
       max_Esqr_search = init.max_Esqr_search;
       totvar_search = init.totvar_search;

       initCos_Sin(); // dynamically allocated table created here

       MapEnsPtr ensPtr(new MapEnsemble(*init.ensemble));
       ensemble = ensPtr; //deep copy on init
    }

  protected:
    //single value
    MapEnsPtr ensemble;
    double TOTAL_SCAT;

    //arrays
    Alogch   m_alogch;
    AlogchI0 m_alogchI0;

    //Data dependent on solution [a]
    std::vector<mr_ndim> models_known;
    std::vector<dvect3>  models_perturbRot;
    std::vector<dvect3>  models_perturbTrans;
    float1D              models_initBfac;
    float1D              models_initOfac;
    map_str_float        models_drms;
    map_str_float        models_cell;

    //Data dependent on reflection [r]
    cmplx1D EM_known,EM_search; // calculated model sf
    float1D sum_Esqr_search,max_Esqr_search;
    float1D totvar_known,totvar_search;

  private:
    pair_flt_flt ScaleFC();
    floatType logRelRice(floatType,floatType,floatType);
    floatType logRelWoolfson(floatType,floatType,floatType);

  public:
    void      calcKnownB(map_str_float,std::string,data_bofac=data_bofac());
    void      initKnownMR(mr_set);
    void      deleteLastKnown();
    void      initSearchMR();
    void      init();
    void      calcKnown();
    void      calcKnownVAR();
    void      calcSearchVAR(std::string,bool,data_bofac=data_bofac());
    void      calcSearchROT(dmat33,std::string,data_bofac=data_bofac());
    void      calcSearchTRA1(dmat33,std::string);
    void      calcSearchTRA2(dvect3,bool,data_bofac,cmplx1D*);

    floatType likelihoodFn();

    floatType Target();
    floatType Rice_refl(int);
    floatType MLHL_refl(int);
    floatType Rfactor();
    cmplx1D   getInterpE(dmat33,std::string);
    bool      haveFpart();
    float1D   getInterpV();
    cmplx1D   getInterpE();
    void      setInterpV(float1D&);
    void      setInterpE(cmplx1D&);
    void      addInterpV(float1D&);
    void      addInterpE(cmplx1D&);

    cmplx3D    getELMNxR2(std::string,Output&,int,double);
    floatType  m_LETF1(dmat33,std::string,bool,af_float&,bool,data_bofac);
    floatType  m_LETF2(dmat33,std::string,bool,af_float&,af_float&,bool,data_bofac);

    af_cmplx getFcalc(dmat33,std::string,af::shared<miller::index<int> >,bool,data_bofac,double&);
    af_cmplx getFcalcPhsTF(dmat33,std::string,data_bofac);
    af_cmplx getFobsPhsTF();
    af_float getEnat();
    af_cmplx getEpart();
    std::vector<mr_ndim>  getModls();
    MapCoefs              getMapCoefs();

    floatType lowerB();
    floatType upperB();
    floatType tolB();
    Hexagonal calc_search_points(data_btf&,floatType,bool,Output&);

    MapEnsPtr& ensPtr() { PHASER_ASSERT(ensemble); return ensemble; }
    Ensemble* ensPtr(std::string m) { return &ensemble->find(m)->second; }
};

}
#endif
