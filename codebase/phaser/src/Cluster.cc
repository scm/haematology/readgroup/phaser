//(i) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Cluster.h>
#include <phaser/lib/jiffy.h>
#include <scitbx/constants.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/adptbx.h>
#include <sstream>

namespace phaser {

void Cluster::init_ta6br12()
{
  Molecule pdb;
  std::istringstream ss(ta6br12_data());
  pdb.ReadStreamPDB(ss); //true = use HETATOM records as well as ATOM records
  init(pdb);
}

void Cluster::init_pdb(std::string pdbfile)
{
  Molecule pdb;
  if (!pdbfile.size()) return;
  pdb.ReadPDB(pdbfile); //raw read of pdb file, no editing
  if (pdb.read_errors().size())
  throw PhaserError(FATAL,"Cluster pdb file (" + pdbfile + ") Read Errors\n" + pdb.read_errors());
  init(pdb);
}

void Cluster::init(Molecule& pdb)
{
  for (int a = 0; a < pdb.nAtoms(); a++)
    scattering_type.push_back(stoup(pdb.card(a).Element));

  bool ignore_occupancies(false);
  xyz_weight cw = pdb.getCW(ignore_occupancies,-999);
  cw.calculate();
  dvect3 centre = cw.CENTRE;
  twopi_ra.resize(cw.size());
  for (unsigned a = 0; a < cw.size(); a++)
    twopi_ra[a] = scitbx::constants::two_pi*
                std::sqrt( fn::pow2(cw[a].xyz[0]-centre[0]) +
                           fn::pow2(cw[a].xyz[1]-centre[1]) +
                           fn::pow2(cw[a].xyz[2]-centre[2]) );
  ff.clear();
  for (unsigned a = 0; a < scattering_type.size(); a++)
    ff[scattering_type[a]] = cctbx::eltbx::xray_scattering::wk1995(scattering_type[a]).fetch();
  is_not_cluster = false;
}

floatType Cluster::debye(floatType ssqr)
{
  //This returns the sinx/x factor from Debye's formula
  //This is used to correct f' and f" at the resolution of r
  //For f' and f" this is a crude approximation that assumes that all the atoms
  //anomalously scatter the same so there is only one fi or fpp for the lot.
  //Makes it the same as a heavy atom.

  if (is_not_cluster) return 1;
  floatType sqrtssqr = sqrt(ssqr);
  floatType debye(0);
  for (unsigned a = 0; a < twopi_ra.size(); a++)
  {
    floatType sqrtssqr_twopi_ra(sqrtssqr*twopi_ra[a]);
    debye += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? 1 : //limit for approx from Mathematica
              sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
  }
  return debye;
}

floatType Cluster::scattering(floatType ssqr)
{
  //Calculates the spherically averaged scattering from a cluster
  //using Debye's formula

  if (is_not_cluster) return 0;
  floatType sqrtssqr = sqrt(ssqr);
  floatType scatter(0);
  for (unsigned a = 0; a < twopi_ra.size(); a++)
  {
    floatType fo = ff[scattering_type[a]].at_d_star_sq(ssqr);
    floatType sqrtssqr_twopi_ra(sqrtssqr*twopi_ra[a]);
    scatter += (std::fabs(sqrtssqr_twopi_ra) < 0.001) ? fo :  //limit for approx from Mathematica
                fo * sin(sqrtssqr_twopi_ra)/(sqrtssqr_twopi_ra);
  }
  return scatter;
}

std::string Cluster::ta6br12_data()
{
/*
COMPND TBR HEXATANTALUM DODECABROMIDE
REMARK TBR Part of HIC-Up: http://xray.bmc.uu.se/hicup
REMARK TBR Extracted from PDB file pdb2bvl.ent
REMARK TBR Formula BR12 TA6
REMARK TBR Nr of non-hydrogen atoms  18
REMARK TBR Eigen-values covariance X/Y/Z       63.6      62.0      60.6
REMARK TBR Residue type TBR
REMARK TBR Residue name   7131
REMARK TBR Original residue name (for O) $A1548
REMARK TBR  RESOLUTION. 2.20 ANGSTROMS.
REMARK TBR occurs in        3 other PDB entries
REMARK TBR Also in 2.0-2.5A  :  1DD4
REMARK TBR Resolution (A)    :  2.40
REMARK TBR Also in 2.5-3.0A  :  1HKX 1Z7L
REMARK TBR Resolution (A)    :  2.65 2.80
REMARK TBR
*/
  std::string tabr;
  tabr += "HETATM    1 TA1  TBR  7131       7.285  -7.899  32.642  1.00 20.00          TA+0\n";
  tabr += "HETATM    2 TA2  TBR  7131       5.977 -10.513  32.571  1.00 20.00          TA+0\n";
  tabr += "HETATM    3 TA3  TBR  7131       8.878 -10.352  32.967  1.00 20.00          TA+0\n";
  tabr += "HETATM    4 TA4  TBR  7131       7.122 -11.158  35.180  1.00 20.00          TA+0\n";
  tabr += "HETATM    5 TA5  TBR  7131       8.432  -8.546  35.250  1.00 20.00          TA+0\n";
  tabr += "HETATM    6 TA6  TBR  7131       5.530  -8.705  34.853  1.00 20.00          TA+0\n";
  tabr += "HETATM    7 BR1  TBR  7131       5.804  -8.747  30.668  1.00 20.00          BR+0\n";
  tabr += "HETATM    8 BR2  TBR  7131       9.338  -8.507  31.160  1.00 20.00          BR+0\n";
  tabr += "HETATM    9 BR3  TBR  7131       8.827  -6.310  33.981  1.00 20.00          BR+0\n";
  tabr += "HETATM   10 BR4  TBR  7131       5.265  -6.496  33.515  1.00 20.00          BR+0\n";
  tabr += "HETATM   11 BR5  TBR  7131       7.709 -11.761  31.102  1.00 20.00          BR+0\n";
  tabr += "HETATM   12 BR6  TBR  7131      10.758  -9.286  34.415  1.00 20.00          BR+0\n";
  tabr += "HETATM   13 BR7  TBR  7131       8.604 -10.311  37.152  1.00 20.00          BR+0\n";
  tabr += "HETATM   14 BR8  TBR  7131       5.070 -10.550  36.660  1.00 20.00          BR+0\n";
  tabr += "HETATM   15 BR9  TBR  7131       5.581 -12.749  33.837  1.00 20.00          BR+0\n";
  tabr += "HETATM   16 BRA  TBR  7131       9.143 -12.562  34.306  1.00 20.00          BR+0\n";
  tabr += "HETATM   17 BRB  TBR  7131       6.697  -7.297  36.720  1.00 20.00          BR+0\n";
  tabr += "HETATM   18 BRC  TBR  7131       3.650  -9.771  33.405  1.00 20.00          BR+0\n";
//REMARK TBR ENDHET
  return tabr;
}

} //phaser
