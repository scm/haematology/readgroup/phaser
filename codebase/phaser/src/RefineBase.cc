//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/RefineBase.h>

namespace phaser {

//------------------
// VIRTUAL FUNCTIONS
//------------------
void        RefineBase::cleanUp(outStream s,Output& o) {}
void        RefineBase::finalize(outStream s,Output& o) {}
void        RefineBase::rejectOutliers(outStream s,Output& o) {}
std::vector<reparams>    RefineBase::getRepar() { return std::vector<reparams>(0); }
double      RefineBase::getMaxDistSpecial(TNT::Vector<double>&, TNT::Vector<double>&, bool1D&, TNT::Vector<double>&,floatType&) { return 0.; }
std::vector<bounds>    RefineBase::getLowerBounds() { return std::vector<bounds>(numRefinePars()); }
std::vector<bounds>    RefineBase::getUpperBounds() { return std::vector<bounds>(numRefinePars()); }
void        RefineBase::logProtocolPars(outStream s,Output& o) { }
void        RefineBase::CalcDampedShift(floatType, TNT::Vector<floatType>&, TNT::Vector<floatType>&,
              TNT::Vector<floatType>&,TNT::Vector<floatType>&,TNT::Vector<floatType>&) { }

void RefineBase::setProtocol(protocolPtr protocol,Output& output)
{
  refinePar = getRefineMask(protocol);
  npars_ref = 0;
  for (int i = 0; i < refinePar.size(); i++) if (refinePar[i]) npars_ref++;
  npars_all = refinePar.size();
}

void RefineBase::setUp()
{
  //for numerical stability
  //make sure that the targetFn and gradientFn return the same LL
  //target function can be very sensitive to VRMS and Bfac values
  //lack of agreement means minimizer can go backwards (produce lower llg values)
  TNT::Vector<floatType> old_x =  getRefinePars();
  applyShift(old_x);
}

//----------
// FUNCTIONS
//----------
int RefineBase::numRefinePars() { return npars_ref; }

// Return maximum multiple of gradient that can be shifted
// before hitting bounds.  Used to define limits for line search.
// Also return vector of allowed shifts for each parameter, which
// can be used to decide which parameters can't be moved along the
// search direction
double      RefineBase::getMaxDist(TNT::Vector<double>& x, TNT::Vector<double>& g, TNT::Vector<double>& dist)
{
  floatType maxDist(0.),ZERO(0.);
  std::vector<bounds> Lower = getLowerBounds();
  reparBounds(Lower);
  std::vector<bounds> Upper = getUpperBounds();
  reparBounds(Upper);
  bool1D bounded((Upper.size() == numRefinePars()) ? numRefinePars() : 1);

  for (int i=0; i < numRefinePars(); i++)
  {
    int j = (Upper.size() == numRefinePars()) ? i : 0;
    bounded[j] = false;
    dist[i] = -1.; // Unbounded (or not moving) until found to be otherwise
    if (Upper[j].bounded)
    {
      if (g[i] <= ZERO)  // Not moving in unbounded direction
      {
        bounded[j] = true; // Not contributing to change in function in unbounded search, even if not moving
        if (g[i] < ZERO) // Moving towards upper bound
        {
          dist[i] = std::max(ZERO,(x[i]-Upper[j].limit)/g[i]);
          maxDist = std::max(maxDist,dist[i]);
        }
      }
    }
    if (Lower[j].bounded)
    {
      if (g[i] >= ZERO)  // Not moving in unbounded direction
      {
        bounded[j] = true;
        if (g[i] > ZERO) // Moving towards lower bound
        {
          dist[i] = std::max(ZERO,(x[i]-Lower[j].limit)/g[i]);
          maxDist = std::max(maxDist,dist[i]);
        }
      }
    }
  }

  // Correlated parameters are checked here, updating bounded and dist if relevant.
  // If correlated parameters are also limited by overall Upper and Lower bounds, those can
  // be set as well for the normal limit checks above.
  if (Upper.size() == 1 && bounded[0]) //there can't be any correlations
    return maxDist;

  maxDist = std::max(maxDist,getMaxDistSpecial(x,g,bounded,dist,maxDist));

  // If any parameters are unbounded, search can carry on essentially indefinitely
  // Large multiple of initial shift estimate should be safe maximum
  for (int i=0; i < numRefinePars(); i++)
  {
    if (!bounded[i]) {maxDist = 1000000.; break;}
  }
  return maxDist;
}

void RefineBase::logVector(outStream where,std::string what, Output& output,TNT::Vector<floatType>& vec)
{
  output.logTab(1,where,what);
  for (int i = 0; i < numRefinePars(); i++)
    output.logTabPrintf(1,where,"%3d [% 5.3e] : %s\n", i+1,vec[i],whatAmI(i).c_str());
  output.logBlank(where);
}

void RefineBase::logHessian(outStream where,Output& output,TNT::Fortran_Matrix<floatType>& Hessian)
{
  int max_dim(11);
  output.logTab(1,where,"Hessian for refined parameters - Matrix");
  if (Hessian.num_rows() >= max_dim)
    output.logTab(1,where,"Hessian is too large to write to log: size = "+itos(Hessian.num_rows()));
  for (int i=1; i<=Hessian.num_rows() && i < max_dim; i++)
  {
    std::string line;
    for (int j=1; j<=Hessian.num_cols() && j < max_dim; j++)
      line += (Hessian(i,j)) ? etos(Hessian(i,j),1,0,true) + " " : " --0-- " ;
    line = line.substr(0,line.length()-1);
    (Hessian.num_rows() >= max_dim) ?
      output.logTabPrintf(1,where,"[%s etc...]\n",line.c_str()) :
      output.logTabPrintf(0,where,"[%s]\n",line.c_str());
  }
  if (Hessian.num_rows() >= max_dim) output.logTabPrintf(0,where," etc...\n");
  output.logBlank(where);
  output.logTab(1,where,"Hessian for refined parameters - Diagonals");
  for (int i=1; i<=Hessian.num_rows() && i < max_dim; i++)
  {
    int j=i-1;
    output.logTabPrintf(1,where,"%3d [% 5.3e] : %s\n", i,Hessian(i,i),whatAmI(j).c_str());
  }
  output.logBlank(where);
}

bool RefineBase::fixHessian(TNT::Fortran_Matrix<floatType>& Hessian)
// Eliminate non-positive curvatures.
// Return true if any curvatures were non-positive.
//
// Determine mean factor by which curvatures differ from 1/largeShift^2
// from parameters with positive curvatures.  This gives the average
// curvature that would be obtained if all the parameters were scaled
// so that their largeShift values were one.
// Expected curvatures (consistent with relative largeShift values) are
// computed from this and used to replace non-positive curvatures.
// Corresponding off-diagonal terms are set to zero.
// RJR Note 31/5/06: tried setting a minimum fraction of the expected
// curvature, but this degraded convergence and depended too much on
// accurate estimation of largeShift.
{
  floatType meanfac(0),ZERO(0.);
  bool needNewHessian(false);
  int npos(0);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  reparLargeShifts(largeShifts);
  for (int i = 0; i < numRefinePars(); i++)
  {
    if (Hessian(i+1,i+1) > ZERO)
    {
      npos++;
      meanfac += Hessian(i+1,i+1)*fn::pow2(largeShifts[i]);
    }
  }
  if (npos)
  {
    meanfac /= npos;
    for (int i = 0; i < numRefinePars(); i++)
    {
      if (Hessian(i+1,i+1) <= ZERO)
      {
        Hessian(i+1,i+1) = meanfac/fn::pow2(largeShifts[i]);
        for (int j = 0; j < numRefinePars(); j++)
          if (i != j) Hessian(i+1,j+1) = Hessian(j+1,i+1) = ZERO;
      }
    }
  }
  else // Fall back on diagonal matrix based on largeShifts shift values
  {
    for (int i = 0; i < numRefinePars(); i++)
    {
      Hessian(i+1,i+1) = 100./fn::pow2(largeShifts[i]);
      for (int j = 0; j < numRefinePars(); j++)
        if (i != j) Hessian(i+1,j+1) = Hessian(j+1,i+1) = ZERO;
    }
  }
  if (numRefinePars()-npos) needNewHessian = true;
  return needNewHessian;
}

void RefineBase::studyParams(Output& output)
// Vary refined parameters and print out function, gradient and curvature
{
  output.setMute(false);
  std::string filename("studyParams");
  TNT::Vector<floatType> x(numRefinePars()),oldx(numRefinePars()),g(numRefinePars());
  TNT::Vector<floatType> unrepar_x(numRefinePars()),unrepar_oldx(numRefinePars()),unrepar_g(numRefinePars());
  TNT::Fortran_Matrix<floatType> Hessian(numRefinePars(),numRefinePars());
  std::vector<bounds> Lower = getLowerBounds();
  reparBounds(Lower);
  std::vector<bounds> Upper = getUpperBounds();
  reparBounds(Upper);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  reparLargeShifts(largeShifts);

  output.logBlank(LOGFILE);
  output.logTab(0,LOGFILE,"Study behaviour of parameters near current values\n");
  unrepar_x = getRefinePars();
  oldx = reparRefinePars(unrepar_x);
  x = oldx;
  applyShift(unrepar_x);
  floatType fmin(targetFn());

  // First check that function values from targetFn, gradientFn and hessianFn agree
  floatType gradLogLike = gradientFn(unrepar_g);
  g = reparGradient(unrepar_x,unrepar_g);
  if (std::abs(fmin-gradLogLike) >= 0.1)
  {
    output.logTab(1,LOGFILE,"Likelihoods from targetFn and gradientFn disagree");
    output.logTab(2,LOGFILE,"Likelihood from targetFn: " + dtos(-fmin));
    output.logTab(2,LOGFILE,"Likelihood from   gradientFn: " + dtos(-gradLogLike));
  }
  bool is_diagonal;
  floatType hessLogLike = hessianFn(Hessian,is_diagonal);
  reparHessian(unrepar_x,unrepar_g,Hessian);
  if (std::abs(fmin-hessLogLike) >= 0.1)
  {
    output.logTab(1,LOGFILE,"Likelihoods from targetFn and hessianFn disagree");
    output.logTab(2,LOGFILE,"Likelihood from  targetFn: " + dtos(-fmin));
    output.logTab(2,LOGFILE,"Likelihood from hessianFn: " + dtos(-hessLogLike));
  }
  PHASER_ASSERT(std::abs(fmin-gradLogLike) < 0.1);
  PHASER_ASSERT(std::abs(fmin-hessLogLike) < 0.1);
  fmin = gradLogLike; // Use function value from gradient for consistency below

  int ndiv(10);
  for (int i = 0; i < numRefinePars(); i++)
  {
    std::ofstream outstream;
    if (i+1 < 10)
    outstream.open(std::string(filename+".0"+itos(i+1)).c_str());
    else
    outstream.open(std::string(filename+"."+itos(i+1)).c_str());
    output.logBlank(LOGFILE);
    output.logTabPrintf(1,LOGFILE,"Refined Parameter #%d (%s)\n",i+1,whatAmI(i).c_str());
    output.logTabPrintf(1,LOGFILE,"Centered on %g\n",oldx[i]);
    floatType xmin = oldx[i] - 2.*largeShifts[i];
    if (Lower[i].bounded)
    {
      xmin = std::max(xmin,Lower[i].limit);
      output.logTabPrintf(1,LOGFILE,"Lower limit: %g\n",Lower[i].limit);
    }
    else // "Unbounded" parameters may have limits depending on correlations
    {
      TNT::Vector<floatType> g_fake(numRefinePars(),0.);
      TNT::Vector<floatType> d_fake(numRefinePars());
      g_fake[i] = largeShifts[i]/10.; // Put in range of plausible shift
      floatType MaxDist = getMaxDist(oldx,g_fake,d_fake);
      floatType fmax = std::numeric_limits<floatType>::max();
      floatType ftol = std::numeric_limits<floatType>::epsilon();
      floatType specialLimit = (MaxDist < fmax-ftol) ? oldx[i]-MaxDist*g_fake[i] : -fmax;
      if (specialLimit > xmin)
      {
        xmin = specialLimit;
        output.logTabPrintf(1,LOGFILE,"Special lower limit: %g\n",specialLimit);
      }
    }
    floatType xmax = oldx[i] + 2.*largeShifts[i];
    if (Upper[i].bounded)
    {
      xmax = std::min(xmax,Upper[i].limit);
      output.logTabPrintf(1,LOGFILE,"Upper limit: %g\n",Upper[i].limit);
    }
    else
    {
      TNT::Vector<floatType> g_fake(numRefinePars(),0.);
      TNT::Vector<floatType> d_fake(numRefinePars());
      g_fake[i] = -largeShifts[i]/10.;
      floatType MaxDist = getMaxDist(oldx,g_fake,d_fake);
      floatType fmax = std::numeric_limits<floatType>::max();
      floatType ftol = std::numeric_limits<floatType>::epsilon();
      floatType specialLimit = (MaxDist < fmax-ftol) ? oldx[i]-MaxDist*g_fake[i] : fmax;
      if (specialLimit < xmax)
      {
        xmax = specialLimit;
        output.logTabPrintf(1,LOGFILE,"Special upper limit: %g\n",specialLimit);
      }
    }
    output.logTabPrintf(1,LOGFILE,"Large shift: %g\n",largeShifts[i]);
    output.logTab(1,LOGFILE," parameter   func-min    gradient   curvature  crv*lrg^2   FD grad     FD curv\n\n");
    float1D fval(ndiv+1);
    floatType dx((xmax-xmin)/ndiv),df(0),lastdf(0),lastg(0);
    floatType lasth = Hessian(1,1); //-Wall: this value not used, escaped with j > 1 condition
    for (int j = 0; j <= ndiv; j++)
    {
      x[i] = xmin + j*dx;
      TNT::Vector<double> unrepar_x = reparRefineParsInv(x);
      applyShift(unrepar_x);
      floatType gradLogLike = gradientFn(unrepar_g);
      g = reparGradient(unrepar_x,unrepar_g);
      df = gradLogLike - fmin;
      fval[j] = df;
      bool is_diagonal;
      hessianFn(Hessian,is_diagonal);
      reparHessian(unrepar_x,unrepar_g,Hessian);
      if (j > 1)
      {
        floatType fdgrad((fval[j]-fval[j-2])/(2.*dx));
        floatType fdhess((fval[j]+fval[j-2]-2.*fval[j-1])/fn::pow2(dx));
        output.logTabPrintf(1,LOGFILE,"%+.4e %+.3e %+.4e %+.4e %+.2e %+.4e %+.4e\n",x[i]-dx,lastdf,lastg,lasth,lasth*fn::pow2(largeShifts[i]),fdgrad,fdhess);
      }
      if (j == 0 || j == ndiv)
        output.logTabPrintf(1,LOGFILE,"%+.4e %+.3e %+.4e %+.4e %+.2e\n",x[i],df,g[i],Hessian(i+1,i+1),Hessian(i+1,i+1)*fn::pow2(largeShifts[i]));
      outstream << x[i] << " " << df << " " << g[i] << " " << Hessian(i+1,i+1) << " " << std::endl;
      lastdf = df;
      lastg = g[i];
      lasth = Hessian(i+1,i+1);
    }
    x[i] = oldx[i];
    outstream.close();
  }
  output.logBlank(LOGFILE);
  unrepar_oldx = reparRefineParsInv(oldx);
  applyShift(unrepar_oldx);
}

TNT::Vector<double> RefineBase::reparRefinePars(TNT::Vector<floatType> pars)
{
  std::vector<reparams> repar = getRepar();
  for (int i = 0; i < numRefinePars() && repar.size(); i++)
    if (repar[i].reparamed)
      pars[i] = std::log(pars[i] + repar[i].offset);
  return pars;
}

TNT::Vector<double> RefineBase::reparRefineParsInv(TNT::Vector<floatType> pars)
{
  std::vector<reparams> repar = getRepar();
  for (int i = 0; i < numRefinePars() && repar.size(); i++)
    if (repar[i].reparamed)
      pars[i] = std::exp(pars[i]) - repar[i].offset;
  return pars;
}

void RefineBase::reparBounds(std::vector<bounds>& bounds)
{
  TNT::Vector<floatType> x(bounds.size());
  for (int i = 0; i < bounds.size(); i++) x[i] = bounds[i].limit;
  x = reparRefinePars(x);
  for (int i = 0; i < bounds.size(); i++)  bounds[i].limit = x[i];
}

TNT::Vector<floatType> RefineBase::reparGradient(TNT::Vector<double>& pars,TNT::Vector<floatType> gradient)
{
  //This is only valid for y=ln(x+c) transformation.
  //f(y(x))
  //df    df   dx   df
  //-- =  -- * -- = -- (x+c)
  //dy    dx   dy   dx

  std::vector<reparams> repar = getRepar();
  for (int i = 0; i < numRefinePars() && repar.size(); i++)
    if (repar[i].reparamed)
      gradient[i] *= (pars[i]+repar[i].offset);
  return gradient;
}

void RefineBase::reparHessian(TNT::Vector<floatType>& pars,TNT::Vector<floatType>& gradient,TNT::Fortran_Matrix<floatType>& Hessian)
{
  //This is only valid for y=ln(x+c) transformation.
  std::vector<reparams> repar = getRepar();
  for (int i = 0; i < numRefinePars() && repar.size(); i++)
    for (int j = 0; j < numRefinePars(); j++)
      if (repar[i].reparamed || repar[j].reparamed)
      {
        floatType dxidyi(1),dxjdyj(1);
        if (repar[i].reparamed) dxidyi = pars[i]+repar[i].offset;
        if (repar[j].reparamed) dxjdyj = pars[j]+repar[j].offset;
        Hessian(i+1,j+1) *= dxidyi*dxjdyj;
        if (i==j) Hessian(i+1,i+1) += gradient[i]*dxidyi; //dxidyi=d2xidyi2
      }
}

void RefineBase::reparLargeShifts(TNT::Vector<floatType>& largeShifts)
{
  std::vector<bounds> oriLower = getLowerBounds();
  std::vector<bounds> Lower = getLowerBounds();
  reparBounds(Lower);
  std::vector<reparams> repar = getRepar();
  for (int i = 0; i < numRefinePars() && repar.size(); i++)
    if (repar[i].reparamed)
    {
      PHASER_ASSERT(Lower[i].bounded);
      largeShifts[i] = std::log(oriLower[i].limit + largeShifts[i] + repar[i].offset) - Lower[i].limit;
    }
}

} //phaser
