//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TraceMolClass__
#define __TraceMolClass__
#include <list>
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/pod/pdb_record.h>

namespace phaser {

class TraceMol
{
  public:
    TraceMol() { npdb = SAMP = DIST = 0; pdb_errors = SAMP_USED = TYPE = ""; }
    void setMAP(data_pdb&,Output&);
    void setPDB(bool,data_pdb&,Output&);
    void setOCC(bool1D*);

  public:
    std::string             TYPE;
    std::string             SAMP_USED;

  private:
    std::vector<pdb_record> trace;
    std::vector<int>        calpha_lookup;
    double                  SAMP,DIST;
    std::string             pdb_errors;
    int                     npdb;

  public:
    std::vector<pdb_record> trace_RT(dmat33&,dvect3&);
    int         size() const { return trace.size(); }
    pdb_record  card(int a) const { return trace[a]; }
    std::string read_errors() const { return pdb_errors; }
    double      sampling_distance() const { return SAMP; }
    double      close_contact_distance() const { return DIST; }
    double      hex_factor() const { return (2./3.*(0.5*std::sqrt(3.0))); }

    void trim_asa(std::string,Molecule&,data_pdb&,Output&);
    void hexgrid(std::string,Molecule&,data_pdb&,Output&);
};

}

#endif
