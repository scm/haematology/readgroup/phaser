//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Solvent.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/scattering.h>
#include <phaser/io/Errors.h>
#include <cctbx/uctbx.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>

namespace phaser {

Solvent::Solvent(std::string SG_HALL,af::double6 UNIT_CELL,data_composition& COMPOSITION)
{
  //constants
  minVM = (1.23); //solvent content 0 for specific gravity of 0.74 cm^3/gm
  maxVM = (10);

  scattering_protein protein;
  floatType assemblyMw = std::floor(Composition(COMPOSITION).total_scat(SG_HALL,UNIT_CELL)/protein.avScat());
  floatType MW(1*assemblyMw);
  cctbx::sgtbx::space_group sg(SG_HALL,"A1983");
  int NSYMM = sg.order_z();
  cctbx::uctbx::unit_cell uc(UNIT_CELL);
  floatType volume = uc.volume();
  thisVM = volume/(NSYMM*MW);
  sc = (100-std::ceil(1/thisVM*100));
}

std::string Solvent::throwError()
{
  std::string warning("");
  if (sc <  0)
    throw PhaserError(FATAL,"The composition you have entered has a protein/nucleic acid content of " +
      dtos(100-sc) + "%, which is impossible. DECREASE your composition.");
  if (low() || high())
    warning = ("The composition you have entered gives a solvent content of " +
     dtos(sc) + "%, which is extremely unlikely. You should decrease the composition.");
  return warning;
}

} //phaser
