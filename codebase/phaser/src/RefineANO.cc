//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/aniso.h>
#include <cctbx/sgtbx/site_symmetry.h>

namespace phaser {

RefineANO::RefineANO(
            data_refl& REFLECTIONS_,
            data_resharp& RESHARP_,
            data_norm& SIGMAN_,
            data_outl& OUTLIER_,
            data_tncs& PTNCS_,
            data_bins& DATABINS_,
            data_composition& COMPOSITION_,
            double res1,double res2)
        :RefineBase2(),
         DataB(REFLECTIONS_,RESHARP_,SIGMAN_,OUTLIER_,PTNCS_,DATABINS_,COMPOSITION_,res1,res2)
{
  floatType sigmaSphericity(10.*fn::pow2(HiRes()/2.5)); //Restrict relative correction at resolution limit
  floatType QUARTER(1./4.);
  TOTAL_SCAT = 1;
  if (!SIGMAN.REFINED && TOTAL_SCAT != 1) //numerical instability in sqrt(1)!
    SIGMAN.WILSON_K = 1.0/std::sqrt(TOTAL_SCAT); //initial estimate from current TOTAL_SCAT, subject to change
  //Convert isotropic equivalent as in largeShifts
  sigmaSphericityBeta[0] = QUARTER*sigmaSphericity*aStar()*aStar();
  sigmaSphericityBeta[1] = QUARTER*sigmaSphericity*bStar()*bStar();
  sigmaSphericityBeta[2] = QUARTER*sigmaSphericity*cStar()*cStar();
  sigmaSphericityBeta[3] = QUARTER*sigmaSphericity*aStar()*bStar();
  sigmaSphericityBeta[4] = QUARTER*sigmaSphericity*aStar()*cStar();
  sigmaSphericityBeta[5] = QUARTER*sigmaSphericity*bStar()*cStar();
}

void RefineANO::rejectOutliers(outStream where, Output& output)
{
  calcOutliers();
  logOutliers(where,output);
  calcSelected();
}

floatType RefineANO::targetFn()
{
  // returns the -log(likelihood) of the current anisotropic parameters
  static double EPS(std::numeric_limits<double>::min());
  floatType minusLL(0);
  af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
  af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    unsigned s = rbin(r);
    floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    bool do_grad(false),do_hess(false);
    floatType grad,hess;
    // For French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2, ignore error
    floatType    thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                           Idat[r];
    floatType thisSIGI = is_FWamplitudes ? 0. : SIGIdat[r];
    floatType prob = (cent(r)) ?
        pIobsCen_grad_hess_by_dSN( thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess) :
        pIobsAcen_grad_hess_by_dSN(thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess);
    if (prob < EPS) // Cope with search reaching extremes
      prob = EPS;
    minusLL -= std::log(prob);
  }
  // Add sphericity restraint terms
  dmat6 anisoRemoveIso = AnisoBetaRemoveIsoB(SIGMAN.ANISO,
                       aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar(),
                       A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  for (unsigned n = 0; n < 6; n++)
  {
    if (sigmaSphericityBeta[n]) minusLL += fn::pow2(anisoRemoveIso[n]/sigmaSphericityBeta[n])/2.;
  }
  // Add best correction factor restraint term
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    // Restrain log of SIGMAN.BINS (shape correction for BEST curve) to zero
    // Sigma for restraint is inspired by relative Wilson plot statistics, but
    // only very weak restraint below 6A resolution to avoid restraint
    // dominating in sparsely populated shells where the BEST curve is imprecise.
    floatType sigmascale(0.15+0.001*fn::pow3(bin.MidRes(s)));
    floatType logbin(std::log(SIGMAN.BINS[s]));
    minusLL += fn::pow2(logbin/sigmascale)/2.;
  }
  return minusLL;
}

floatType RefineANO::gradientFn(TNT::Vector<floatType>& Gradient)
{
  floatType minusLL(0);
  af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
  af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  PHASER_ASSERT(Gradient.size() == npars_ref);

  int wilki = bin.numbins()+0;
  int solk = bin.numbins()+1;
  int solb = bin.numbins()+2;
  int1D ano(SIGMAN.ANISO.size(),0);
  for (int n = 0; n < SIGMAN.ANISO.size(); n++)
    ano[n] = bin.numbins()+3+n;

  //work out constraints and partial derivative matrix of beta(i) vs beta(j)
  float2D dBeta_by_dBeta;
  dBeta_by_dBeta.resize(6);
  for (int n1 = 0; n1 < 6; n1++)
    dBeta_by_dBeta[n1].resize(6);
  dmat6 aniso_diag(3,5,7,11,13,17);
  cctbx::sgtbx::space_group SgOps = getCctbxSG();
  cctbx::sgtbx::site_symmetry site_sym(UnitCell::getCctbxUC(), SgOps.build_derived_point_group(),cctbx::fractional<>(0,0,0));
  dmat6 constrained(site_sym.average_u_star(aniso_diag));
  for (int i = 3; i < 6; i++)
    constrained[i] *= 2; //Factor of two for off-diagonal elements
  int2D constrain_equal(npars_all);
  bool1D interesting;
  interesting = getRefineAnisoMask();
  for (int s = 0; s < bin.numbins(); s++)
    constrain_equal[s].push_back(s);
  constrain_equal[wilki].push_back(wilki);
  constrain_equal[solk].push_back(solk);
  constrain_equal[solb].push_back(solb);
  floatType tol = 1.0e-06;
  for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
    for (int n2 = n1; n2 < SIGMAN.ANISO.size(); n2++)
    {
      if (interesting[n1] && interesting[n2] && std::abs(constrained[n1]-constrained[n2]) < tol)
      {
        constrain_equal[ano[n1]].push_back(ano[n2]);
        interesting[n2] = (n1==n2); // Only set flag for first time constraint is encountered
        dBeta_by_dBeta[n1][n2] = dBeta_by_dBeta[n2][n1] = 1.;
      }
      else
        dBeta_by_dBeta[n1][n2] = dBeta_by_dBeta[n2][n1] = 0.;
    }

  //Loop over reflections, accumulating partial derivatives
  static double EPS(std::numeric_limits<double>::min());
  float1D Grad(npars_all,0.);
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    unsigned s = rbin(r);
    floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    floatType epsnSigmaN_nosol,ZERO(0.);
    epsnSigmaN_nosol = binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,ZERO,SIGMAN.SOLB,WilsonK_I);
    bool do_grad(true),do_hess(false);
    floatType grad,hess;
    floatType    thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                           Idat[r];
    floatType thisSIGI = is_FWamplitudes ? 0. : SIGIdat[r];
    floatType prob = (cent(r)) ?
        pIobsCen_grad_hess_by_dSN( thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess) :
        pIobsAcen_grad_hess_by_dSN(thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess);
    floatType dLL_by_dEpsnSigmaN;
    if (prob < EPS) // Cope with search reaching extremes
    {
      prob = EPS;
      dLL_by_dEpsnSigmaN = 0.;
    }
    else
      dLL_by_dEpsnSigmaN = grad/prob;
    minusLL -= std::log(prob);
    float1D millerSqr(6);
    millerSqr[0] = -MILLER[r][0]*MILLER[r][0];
    millerSqr[1] = -MILLER[r][1]*MILLER[r][1];
    millerSqr[2] = -MILLER[r][2]*MILLER[r][2];
    millerSqr[3] = -MILLER[r][0]*MILLER[r][1];
    millerSqr[4] = -MILLER[r][0]*MILLER[r][2];
    millerSqr[5] = -MILLER[r][1]*MILLER[r][2];
    floatType dEpsnSigmaN_by_dBin = epsnSigmaN/SIGMAN.BINS[s];
    floatType dEpsnSigmaN_by_dWilsonKI = epsnSigmaN/WilsonK_I;
    float1D dEpsnSigmaN_by_dAno(6);
    for (int n = 0; n < SIGMAN.ANISO.size(); n++)
      dEpsnSigmaN_by_dAno[n] = millerSqr[n]*epsnSigmaN;
    floatType expSB = std::exp(-SIGMAN.SOLB*ssqr(r)/4.0);
    floatType dEpsnSigmaN_by_dsolK = -expSB*epsnSigmaN_nosol;
    floatType dEpsnSigmaN_by_dsolB = (SIGMAN.SOLK*ssqr(r)/4.0)*expSB*epsnSigmaN_nosol;

    //need to return grad of -LL
    Grad[s] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dBin;
    Grad[wilki] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dWilsonKI;
    Grad[solk] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dsolK;
    Grad[solb] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dsolB;
    for (int n = 0; n < SIGMAN.ANISO.size(); n++)
      Grad[ano[n]] -= dLL_by_dEpsnSigmaN * dEpsnSigmaN_by_dAno[n];
  }

  // Add sphericity restraint terms
  floatType one(1.);
  dmat6 dBetaIso_by_dBIso = IsoB2AnisoBeta(one,
                       aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar());
  dmat6 anisoRemoveIso = AnisoBetaRemoveIsoB(SIGMAN.ANISO,
                       aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar(),
                       A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  dmat6 dBIso_by_dBetaAno =
             AnisoBeta2IsoBCoeffs(A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  floatType dBetaIso_by_dBetaAnoI(0),dBetaAno_by_dBetaAnoI(0);
  for (unsigned ni = 0; ni < 6; ni++)
  {
    if (sigmaSphericityBeta[ni]) minusLL += fn::pow2(anisoRemoveIso[ni]/sigmaSphericityBeta[ni])/2.;
      for (unsigned n = 0; n < 6; n++)
      {
        dBetaIso_by_dBetaAnoI = dBetaIso_by_dBIso[n]*dBIso_by_dBetaAno[ni];
        dBetaAno_by_dBetaAnoI = (n == ni) ? 1. : 0.;
        if (sigmaSphericityBeta[n]) Grad[ano[ni]] += anisoRemoveIso[n]*(dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
        fn::pow2(sigmaSphericityBeta[n]);
      }
  }

  // Add best correction factor restraint term
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    floatType sigmascale(0.15+0.001*fn::pow3(bin.MidRes(s)));
    floatType binfac(SIGMAN.BINS[s]);
    floatType logbin(std::log(binfac));
    minusLL += fn::pow2(logbin/sigmascale)/2.;
    Grad[s] += logbin/(binfac*fn::pow2(sigmascale));
  }

  //Sum partial derivatives to get total derivatives for constrained parameters
  int esn1(0);
  for (int n1 = 0; n1 < npars_all; n1++)
  {
    if (constrain_equal[n1].size()-1) // only sum if there are constraints
    {
      floatType sum(0.);
      for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
      {
        esn1 = constrain_equal[n1][sn1];
        sum += Grad[esn1];
      }
      for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
      {
        esn1 = constrain_equal[n1][sn1];
        Grad[esn1] = sum;
      }
    }
  }

  //filter for refined parameters
  { //scope for windows (i)
  int i = 0;
  for (int m = 0; m < npars_all; m++)
    if (refinePar[m])
      Gradient[i++] = Grad[m];
  PHASER_ASSERT(i == npars_ref);
  }

#if 0
//  Call to test finite difference Gradient
TNT::Vector<floatType> diff(Gradient.size());
//floatType shift = std::atof(getenv("PHASER_TEST_SHIFT"));
  floatType shift(0.0001);
  finiteDiffGradient(diff,shift);
  floatType maxFracError(0.0001);
  std::cout << "\nFlag gradient elements differing by fractional error > " << maxFracError << "\n";
  for (int m = 0; m < npars_ref; m++)
  {
    floatType meanG((Gradient[m]+diff[m])/2.);
    if (meanG)
    {
      floatType fracError(std::abs(Gradient[m]-diff[m])/meanG);
      if (fracError > maxFracError)
        std::cout << "  Element " << m+1 <<  ": " << Gradient[m] << " vs. " << diff[m] <<
                      " : frac error = " << fracError << "\n";
    }
  }
  std::exit(1);
#endif

  return minusLL;
}

floatType RefineANO::hessianFn(TNT::Fortran_Matrix<floatType>& Hessian, bool& is_diagonal)
{
  //initialization
  floatType minusLL(0);
  af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
  af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  Hessian.newsize(npars_ref,npars_ref);
  PHASER_ASSERT(Hessian.num_cols() == npars_ref);
  PHASER_ASSERT(Hessian.num_rows() == npars_ref);
  for (int i_ = 1; i_ <= npars_ref; i_++)
    for (int j = 1; j <= npars_ref; j++)
      Hessian(i_,j) = 0;

  float2D Hess,rHess;
  rHess.resize(npars_all);
  Hess.resize(npars_all);
  for (int m1 = 0; m1 < npars_all; m1++)
    rHess[m1].resize(npars_all), Hess[m1].resize(npars_all);
  for (int p = 0; p < npars_all; p++)
    for (int q = 0; q < npars_all; q++)
      Hess[p][q]= 0;

  int wilki = bin.numbins()+0;
  int solk = bin.numbins()+1;
  int solb = bin.numbins()+2;
  int1D ano(SIGMAN.ANISO.size(),0.0);
  for (int n_ = 0; n_ < SIGMAN.ANISO.size(); n_++)
    ano[n_] = bin.numbins()+3+n_;

  //work out constraints
  float2D dBeta_by_dBeta;
  dBeta_by_dBeta.resize(6);
  for (int n1 = 0; n1 < 6; n1++)
    dBeta_by_dBeta[n1].resize(6);
  dmat6 aniso_diag(3,5,7,11,13,17);
  cctbx::sgtbx::space_group SgOps = getCctbxSG();
  cctbx::sgtbx::site_symmetry site_sym(UnitCell::getCctbxUC(), SgOps.build_derived_point_group(),cctbx::fractional<>(0,0,0));
  dmat6 constrained(site_sym.average_u_star(aniso_diag));
  for (int i = 3; i < 6; i++)
    constrained[i] *= 2; //Factor of two for off-diagonal elements
  int2D constrain_equal(npars_all);
  bool1D interesting;
  interesting = getRefineAnisoMask();
  for (int s = 0; s < bin.numbins(); s++)
    constrain_equal[s].push_back(s);
  constrain_equal[wilki].push_back(wilki);
  constrain_equal[solk].push_back(solk);
  constrain_equal[solb].push_back(solb);
  floatType tol = 1.0e-06;
  for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
    for (int n2 = n1; n2 < SIGMAN.ANISO.size(); n2++)
    {
      if (interesting[n1] && interesting[n2] && std::abs(constrained[n1]-constrained[n2]) < tol)
      {
        constrain_equal[ano[n1]].push_back(ano[n2]);
        interesting[n2] = (n1==n2); // Only set flag for first time constraint is encountered
        dBeta_by_dBeta[n1][n2] = dBeta_by_dBeta[n2][n1] = 1.;
      }
      else
        dBeta_by_dBeta[n1][n2] = dBeta_by_dBeta[n2][n1] = 0.;
    }

  //Loop over reflections, accumulating partial derivatives
  static double EPS(std::numeric_limits<double>::min());
  float1D Grad(npars_all,0.);
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    for (int p = 0; p < npars_all; p++)
      for (int q = 0; q < npars_all; q++)
        rHess[p][q]= 0;

    unsigned s = rbin(r);
    floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    floatType epsnSigmaN_nosol,ZERO(0.);
    epsnSigmaN_nosol = binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,ZERO,SIGMAN.SOLB,WilsonK_I);
    bool do_grad(true),do_hess(true);
    floatType grad,hess;
    floatType    thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                           Idat[r];
    floatType thisSIGI = is_FWamplitudes ? 0. : SIGIdat[r];
    floatType prob = (cent(r)) ?
        pIobsCen_grad_hess_by_dSN( thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess) :
        pIobsAcen_grad_hess_by_dSN(thisI,epsnSigmaN,thisSIGI,do_grad,grad,do_hess,hess);
    floatType dLL_by_dEpsnSigmaN,d2LL_by_dEpsnSigmaN2;
    if (prob < EPS) // Cope with search reaching extremes
    {
      prob = EPS;
      dLL_by_dEpsnSigmaN = d2LL_by_dEpsnSigmaN2 = 0.;
    }
    else
    {
      dLL_by_dEpsnSigmaN = grad/prob;
      d2LL_by_dEpsnSigmaN2 = hess/prob - fn::pow2(grad/prob);
    }
    minusLL -= std::log(prob);
    float1D millerSqr(6);
    millerSqr[0] = -MILLER[r][0]*MILLER[r][0];
    millerSqr[1] = -MILLER[r][1]*MILLER[r][1];
    millerSqr[2] = -MILLER[r][2]*MILLER[r][2];
    millerSqr[3] = -MILLER[r][0]*MILLER[r][1];
    millerSqr[4] = -MILLER[r][0]*MILLER[r][2];
    millerSqr[5] = -MILLER[r][1]*MILLER[r][2];
    floatType dEpsnSigmaN_by_dBin = epsnSigmaN/SIGMAN.BINS[s];
    floatType dEpsnSigmaN_by_dWilsonKI = epsnSigmaN/WilsonK_I;
    float1D dEpsnSigmaN_by_dAno(6);
    for (int n = 0; n < SIGMAN.ANISO.size(); n++)
      dEpsnSigmaN_by_dAno[n] = millerSqr[n]*epsnSigmaN;
    floatType expSB = std::exp(-SIGMAN.SOLB*ssqr(r)/4.0);
    floatType dEpsnSigmaN_by_dsolK = -expSB*epsnSigmaN_nosol;
    floatType dEpsnSigmaN_by_dsolB = (SIGMAN.SOLK*ssqr(r)/4.0)*expSB*epsnSigmaN_nosol;

    //Work out Hessian in terms of LL, then convert to -LL at end
    //start with bin [s]
    rHess[s][s] = d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dBin);
    rHess[s][wilki] = rHess[wilki][s] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dWilsonKI +
       dLL_by_dEpsnSigmaN*epsnSigmaN/(SIGMAN.BINS[s]*WilsonK_I);
    rHess[s][solk] = rHess[solk][s] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dsolK +
       dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dsolK/SIGMAN.BINS[s];
    rHess[s][solb] = rHess[solb][s] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dsolB +
       dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dsolB/SIGMAN.BINS[s];
    for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
      rHess[s][ano[n1]] = rHess[ano[n1]][s] =
           d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dBin*dEpsnSigmaN_by_dAno[n1] +
            dLL_by_dEpsnSigmaN*millerSqr[n1]*dEpsnSigmaN_by_dBin;

    //start with Wilson_K_I
    rHess[wilki][wilki] =
      d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dWilsonKI);
    rHess[wilki][solk] = rHess[solk][wilki] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dWilsonKI*dEpsnSigmaN_by_dsolK +
       dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dsolK/WilsonK_I;
    rHess[wilki][solb] = rHess[solb][wilki] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dWilsonKI*dEpsnSigmaN_by_dsolB +
       dLL_by_dEpsnSigmaN*dEpsnSigmaN_by_dsolB/WilsonK_I;
    for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
      rHess[wilki][ano[n1]] = rHess[ano[n1]][wilki] =
           d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dWilsonKI*dEpsnSigmaN_by_dAno[n1] +
            dLL_by_dEpsnSigmaN*millerSqr[n1]*dEpsnSigmaN_by_dWilsonKI;

    //start with solk
    rHess[solk][solk] =
      d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dsolK);
    rHess[solk][solb] = rHess[solb][solk] =
      d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dsolK*dEpsnSigmaN_by_dsolB +
       dLL_by_dEpsnSigmaN*(ssqr(r)/4.0)*expSB*epsnSigmaN_nosol;
    for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
      rHess[ano[n1]][solk] = rHess[solk][ano[n1]] =
        d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dAno[n1]*dEpsnSigmaN_by_dsolK +
         dLL_by_dEpsnSigmaN*millerSqr[n1]*dEpsnSigmaN_by_dsolK;

    //start with solb
    rHess[solb][solb] =
      d2LL_by_dEpsnSigmaN2*fn::pow2(dEpsnSigmaN_by_dsolB) +
       dLL_by_dEpsnSigmaN*(-SIGMAN.SOLK*fn::pow2(ssqr(r))/16.0)*expSB*epsnSigmaN_nosol;
    for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
      rHess[ano[n1]][solb] = rHess[solb][ano[n1]] =
        d2LL_by_dEpsnSigmaN2*dEpsnSigmaN_by_dAno[n1]*dEpsnSigmaN_by_dsolB +
        dLL_by_dEpsnSigmaN*millerSqr[n1]*SIGMAN.SOLK*(ssqr(r)/4.0)*expSB*epsnSigmaN_nosol;

    //start with ano
    for (int n1 = 0; n1 < SIGMAN.ANISO.size(); n1++)
      for (int n2 = n1; n2 < SIGMAN.ANISO.size(); n2++)
        rHess[ano[n1]][ano[n2]] = rHess[ano[n2]][ano[n1]] =
          d2LL_by_dEpsnSigmaN2*millerSqr[n1]*millerSqr[n2]*fn::pow2(epsnSigmaN) +
          dLL_by_dEpsnSigmaN*millerSqr[n1]*millerSqr[n2]*epsnSigmaN;

    for (int p = 0; p < npars_all; p++)
      for (int q = 0; q < npars_all; q++)
        Hess[p][q] += rHess[p][q];
  }

  // Add sphericity restraint terms
  floatType one(1.);
  dmat6 dBetaIso_by_dBIso = IsoB2AnisoBeta(one,
           aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar());
  dmat6 anisoRemoveIso = AnisoBetaRemoveIsoB(SIGMAN.ANISO,
           aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar(),
           A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  dmat6 dBIso_by_dBetaAno =
             AnisoBeta2IsoBCoeffs(A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  floatType dBetaIso_by_dBetaAnoI(0),dBetaAno_by_dBetaAnoI(0);
  floatType dBetaIso_by_dBetaAnoJ(0),dBetaAno_by_dBetaAnoJ(0);
  for (unsigned ni = 0; ni < 6; ni++)
  {
    if (sigmaSphericityBeta[ni]) minusLL += fn::pow2(anisoRemoveIso[ni]/sigmaSphericityBeta[ni])/2.;
    for (unsigned nj = 0; nj < 6; nj++)
    {
      for (unsigned n = 0; n < 6; n++)
      {
        dBetaIso_by_dBetaAnoI = dBetaIso_by_dBIso[n]*dBIso_by_dBetaAno[ni];
        dBetaAno_by_dBetaAnoI = (n == ni) ? 1. : 0.;
        dBetaIso_by_dBetaAnoJ = dBetaIso_by_dBIso[n]*dBIso_by_dBetaAno[nj];
        dBetaAno_by_dBetaAnoJ = (n == nj) ? 1. : 0.;
        // Hessian at this point refers to LL, not minusLL, so subtract
        if (sigmaSphericityBeta[n]) Hess[ano[ni]][ano[nj]] -= (dBetaAno_by_dBetaAnoJ-dBetaIso_by_dBetaAnoJ) *
                                  (dBetaAno_by_dBetaAnoI-dBetaIso_by_dBetaAnoI) /
                                        fn::pow2(sigmaSphericityBeta[n]);
      }
    }
  }

  // Add best correction factor restraint term
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    floatType sigmascale(0.15+0.001*fn::pow3(bin.MidRes(s)));
    floatType binfac(SIGMAN.BINS[s]);
    floatType logbin(std::log(binfac));
    minusLL += fn::pow2(logbin/sigmascale)/2.;
    Hess[s][s] -= (1.-logbin)/fn::pow2(binfac*sigmascale); // wrt LL, not yet -LL

  }

  //Sum partial derivative terms to get total derivative
  for (int n1 = 0; n1 < npars_all; n1++)
    for (int n2 = 0; n2 < npars_all; n2++)
    {
      if (constrain_equal[n1].size()*constrain_equal[n2].size()-1) // Only sum if there are constraints
      {
        floatType sum(0.);
        for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
          for (int sn2 = 0; sn2 < constrain_equal[n2].size(); sn2++)
          {
            int esn1 = constrain_equal[n1][sn1];
            int esn2 = constrain_equal[n2][sn2];
            sum += Hess[esn1][esn2];
          }
        for (int sn1 = 0; sn1 < constrain_equal[n1].size(); sn1++)
          for (int sn2 = 0; sn2 < constrain_equal[n2].size(); sn2++)
          {
            int esn1 = constrain_equal[n1][sn1];
            int esn2 = constrain_equal[n2][sn2];
            Hess[esn1][esn2] = sum;
          }
      }
    }

  //store refined values in Hessian
  { //scope for windows (i)
  int i = 1;
  for (int m1 = 0; m1 < npars_all; m1++)
    if (refinePar[m1])
    {
      int j = 1;
      for (int m2 = 0; m2 < npars_all; m2++)
        if (refinePar[m2])
          Hessian(i,j++) = -Hess[m1][m2];
      PHASER_ASSERT(j == npars_ref+1);
      i++;
    }
  PHASER_ASSERT(i == npars_ref+1);
  }

#if 0
  //Call to compare with finite difference (function or gradient) Hessian
  TNT::Fortran_Matrix<floatType> fdHessian;
  fdHessian.newsize(npars_ref,npars_ref);
  //finiteFDiffHessian(fdHessian,0.0001);
  finiteGDiffHessian(fdHessian,0.0001);
  floatType maxFracError(0.0001);
  std::cout << "\nFlag Hessian elements differing by fractional error > " << maxFracError << "\n";
  for (int i=1; i<= npars_ref; i++)
    for (int j=i; j<=npars_ref; j++)
    {
      floatType meanH((Hessian(i,j)+fdHessian(i,j))/2.);
      if (meanH)
      {
        floatType fracError(std::abs(Hessian(i,j)-fdHessian(i,j))/meanH);
        if (fracError > maxFracError)
          std::cout << "  Element " << i << " " << j << ": " << Hessian(i,j) << " vs. " << fdHessian(i,j) <<
                " : frac error = " << fracError << "\n";
      }
    }
#endif

  is_diagonal = false;
  return minusLL;
}

bool1D RefineANO::getRefineMask(protocolPtr p)
{
  ProtocolANO protocol;
  protocol.FIX_BINS =  p->getFIX(maca_bins);
  protocol.FIX_ANISO = p->getFIX(maca_aniso);
  protocol.FIX_SOLK =  p->getFIX(maca_solk);
  protocol.FIX_SOLB =  p->getFIX(maca_solb);

  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(0);
  for (int s = 0; s < bin.numbins(); s++) // BEST curve shape correction scales
    if (protocol.FIX_BINS) refineMask.push_back(REFINE_OFF);
    else refineMask.push_back(REFINE_ON);
  if (protocol.FIX_BINS) refineMask.push_back(REFINE_OFF); // Overall WilsonK_I goes with BEST curve
  else refineMask.push_back(REFINE_ON);
  if (protocol.FIX_SOLK) refineMask.push_back(REFINE_OFF);
  else refineMask.push_back(REFINE_ON);
  if (protocol.FIX_SOLB) refineMask.push_back(REFINE_OFF);
  else refineMask.push_back(REFINE_ON);
  bool1D anisoRefineMask;
  anisoRefineMask = getRefineAnisoMask();
  for (unsigned n = 0; n < SIGMAN.ANISO.size(); n++)
    if (protocol.FIX_ANISO) refineMask.push_back(REFINE_OFF);
    else refineMask.push_back(anisoRefineMask[n]);

  //this is where npars_all and npars_ref are DEFINED
  return refineMask;
}

TNT::Vector<floatType> RefineANO::getLargeShifts()
{
  //Get anisotropic shifts that will have similar effect to isotropic B
  floatType largeIsoBShift(4.);
  floatType QUARTER(1./4.);
  dmat6 anisoLargeShifts;
  anisoLargeShifts[0] = QUARTER*largeIsoBShift*aStar()*aStar();
  anisoLargeShifts[1] = QUARTER*largeIsoBShift*bStar()*bStar();
  anisoLargeShifts[2] = QUARTER*largeIsoBShift*cStar()*cStar();
  anisoLargeShifts[3] = QUARTER*largeIsoBShift*aStar()*bStar();
  anisoLargeShifts[4] = QUARTER*largeIsoBShift*aStar()*cStar();
  anisoLargeShifts[5] = QUARTER*largeIsoBShift*bStar()*cStar();
  int m(0),i(0);
  TNT::Vector<floatType> largeShifts(npars_ref);
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) largeShifts[i++] = 0.05*SIGMAN.BINS[s];
  //if (refinePar[m++]) largeShifts[i++] = 0.02/fn::pow2(SIGMAN.WILSON_K)/TOTAL_SCAT;  //Overall WilsonK_I
  if (refinePar[m++]) largeShifts[i++] = 0.02/fn::pow2(SIGMAN.WILSON_K);  //Overall WilsonK_I
  if (refinePar[m++]) largeShifts[i++] = 0.05; //solK
  if (refinePar[m++]) largeShifts[i++] = largeIsoBShift; //solB
  for (unsigned n = 0; n < SIGMAN.ANISO.size(); n++)
    if (refinePar[m++]) largeShifts[i++] = anisoLargeShifts[n];
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return largeShifts;
}

void RefineANO::applyShift(TNT::Vector<floatType>& newx)
{
  int m(0),i(0);
//-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) SIGMAN.BINS[s] = newx[i++];
  if (refinePar[m++]) SIGMAN.WILSON_K = 1./std::sqrt(newx[i++]);
  if (refinePar[m++]) SIGMAN.SOLK = newx[i++];
  if (refinePar[m++]) SIGMAN.SOLB = newx[i++];
  for (unsigned n = 0; n < SIGMAN.ANISO.size(); n++)
    if (refinePar[m++]) SIGMAN.ANISO[n] = newx[i++];
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
}

void RefineANO::logCurrent(outStream where,Output& output)
{
  logSigmaN(where,output);
}

void RefineANO::logInitial(outStream where,Output& output)
{
  logSigmaN(DEBUG,output);
}

void RefineANO::logFinal(outStream where,Output& output)
{
  output.logUnderLine(SUMMARY,"Refined Anisotropy Parameters");
  logSigmaN(SUMMARY,output,true);
}

std::string RefineANO::whatAmI(int& ipar)
{
  int i(0),m(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) if (i++ == ipar) return "SigmaN Bin #" + itos(s+1);
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN WilsonK";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN SolK";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN SolB";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BHH";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BKK";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BLL";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BHK ";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BHL ";
  if (refinePar[m++]) if (i++ == ipar) return "SigmaN Aniso BKL ";
  PHASER_ASSERT(i == npars_ref);
  return "Undefined Parameter";
}

TNT::Vector<floatType> RefineANO::getRefinePars()
{
  int m(0),i(0);
  TNT::Vector<floatType> pars(npars_ref);
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) pars[i++] = SIGMAN.BINS[s];
  if (refinePar[m++]) pars[i++] = 1./fn::pow2(SIGMAN.WILSON_K);
  if (refinePar[m++]) pars[i++] = SIGMAN.SOLK;
  if (refinePar[m++]) pars[i++] = SIGMAN.SOLB;
  for (unsigned n = 0; n < SIGMAN.ANISO.size(); n++)
    if (refinePar[m++]) pars[i++] = SIGMAN.ANISO[n];
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return pars;
}

std::vector<reparams> RefineANO::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) repar[i++].on(0.1); //Bin factor
  if (refinePar[m++]) repar[i++].on(0.1); //Wilson_k_I
  if (refinePar[m++]) repar[i++].off(); //solk
  if (refinePar[m++]) repar[i++].off(); //solB
  for (unsigned n = 0; n < SIGMAN.ANISO.size(); n++)
    if (refinePar[m++]) repar[i++].off(); //beta
  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);
  return repar;
}

std::vector<bounds> RefineANO::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  int i(0),m(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Lower[i++].on(1.e-6); //Bin factor
  if (refinePar[m++]) Lower[i++].on(1.e-15); //Wilson_K_I
  if (refinePar[m++]) Lower[i++].on(0.); //solk
  if (refinePar[m++]) Lower[i++].on(0.); //solB
  //anisotropy
  for (unsigned n = 0; n < 6; n++)
    if (refinePar[m++]) Lower[i++].off(); //beta
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

std::vector<bounds > RefineANO::getUpperBounds()
{
  std::vector<bounds > Upper(npars_ref);
  int i(0),m(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (refinePar[m++]) Upper[i++].off(); //Bin factor
  if (refinePar[m++]) Upper[i++].off();  //Wilson_K_I
  if (refinePar[m++]) Upper[i++].on(1.-1.e-7); //solk
  if (refinePar[m++]) Upper[i++].off(); //solB
  //anisotropy
  for (unsigned n = 0; n < 6; n++)
    if (refinePar[m++]) Upper[i++].off(); //beta
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

bool1D RefineANO::getRefineAnisoMask()
{
  bool REFINE_ON(true),REFINE_OFF(false);
  bool1D refineMask(6,0.0);
  dmat6 aniso;
  for (int n = 0; n < 6; n++)
    aniso[n] = 10+n*10; //any old junk, but values unique

  cctbx::sgtbx::space_group SgOps = SpaceGroup::getCctbxSG();
  cctbx::sgtbx::site_symmetry site_sym(UnitCell::getCctbxUC(), SgOps.build_derived_point_group(),cctbx::fractional<>(0,0,0));
  dmat6 constrained(site_sym.average_u_star(aniso));
  floatType tol = 1.0e-06;
  for (int n = 0; n < 6; n++)
    if (constrained[n] < tol && constrained[n] > -tol)
       refineMask[n] = REFINE_OFF;
    else
       refineMask[n] = REFINE_ON;

  return refineMask;
}

void RefineANO::cleanUp(outStream where,Output& output)
{
  calculate_sigman_parameters();
}

void RefineANO::finalize(outStream where, Output& output)
{
  calcOutliers();
  logOutliers(where,output);
  calcSelected();
}

}//phaser
