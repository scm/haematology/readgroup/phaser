//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MapTraceMolClass__
#define __MapTraceMolClass__
#include <phaser/src/TraceMol.h>
#include <phaser/io/Output.h>
#include <phaser/mr_objects/mr_set.h>

namespace phaser {

typedef std::map<std::string,TraceMol>::iterator trcIter;

class MapTraceMol
{
  public:
  std::map<std::string,TraceMol> tracemol;

  public:
    MapTraceMol() { }

    MapTraceMol(MapTraceMol* init)
    { //deep copy with pointer initialization
      tracemol = init->tracemol;
    }

    ~MapTraceMol() { }

  bool  configure(stringset,bool,bool,map_str_pdb&,Output&);
  void  configure(mr_set&);
  void  trace_table(outStream,Output&);

  typedef std::map<std::string,TraceMol>::size_type size_type;
  typedef std::map<std::string,TraceMol>::iterator iterator;
  typedef std::map<std::string,TraceMol>::const_iterator const_iterator;
  TraceMol& operator[](std::string& t) { return tracemol[t]; }
  iterator begin() { return tracemol.begin(); }
  iterator end() { return tracemol.end(); }
  const_iterator begin() const { return tracemol.begin(); }
  const_iterator end() const { return tracemol.end(); }
  size_type size() const { return tracemol.size(); }
  iterator find (const std::string& k) { return tracemol.find(k); }
  const_iterator find (const std::string& k) const { return tracemol.find(k); }
};

}//phaser
#endif
