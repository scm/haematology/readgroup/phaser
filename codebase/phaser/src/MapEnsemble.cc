//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/MapEnsemble.h>

namespace phaser {

void MapEnsemble::configure(stringset currentEns,
                            map_str_pdb& PDB,
                            data_solpar& SOLPAR,
                            floatType BOXSCALE,
                            floatType HIRES_,
                            SigmaN DATA_BIN,
                            data_formfactors& FORMFACTOR,
                            stringset KEEPENS,
                            Output& output,
                            bool decomposition,
                            double sphere,
                            map_str_float1D* OCC,
                            bool force_reset)
{
  PHASER_ASSERT(currentEns.size());
  stringset eraseEns;
  //if these are not present or not same resolution
  //bool resolution_condition = !between(HIRES_,HIRES,1.0e-06);
  //if these are not present or not present to high enough resolution
  bool resolution_condition = HIRES_ < MAPHIRES;
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    if (force_reset || (currentEns.find(iter->first) == currentEns.end()) || resolution_condition || iter->second.is_empty())
      eraseEns.insert(iter->first);
  //erase all if there are occupancies to change
  if (OCC != NULL)
    for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
      eraseEns.insert(iter->first);

  for (stringset::iterator iter = eraseEns.begin(); iter != eraseEns.end(); iter++)
  {
    std::string m = (*iter);
    ensemble[m].empty(); //paranoia
    ensemble.erase(ensemble.find(m));
  }
  if (resolution_condition) MAPHIRES = HIRES_; //prevents chinese whispers

  for (stringset::iterator iter = currentEns.begin(); iter != currentEns.end(); iter++)
  {
    std::string modlid = (*iter); //index for PDB
    //the modlids for occupancy refinement are per-solution
    //flagged with [#component] on the end of the modlid
    if (ensemble.find(modlid) == ensemble.end())
    {
      decomposition  ? output.logUnderLine(VERBOSE,"Ensemble Generation for Spherical Harmonic Decomposition: " + modlid): output.logUnderLine(LOGFILE,"Ensemble Generation: " + modlid);
      PHASER_ASSERT(PDB.find(modlid) != PDB.end());
      PHASER_ASSERT(PDB[modlid].ENSEMBLE.size());
      if (PDB[modlid].ENSEMBLE[0].map_format())
      {
        ensemble[modlid].set_data_pdb(PDB[modlid]);
        ensemble[modlid].setMAP(MAPHIRES,SOLPAR,output);
      }
      else if ((OCC == NULL) && PDB.find(modlid) != PDB.end())
      {
        ensemble[modlid].set_data_pdb(PDB[modlid]);
        ensemble[modlid].setPDB(MAPHIRES,SOLPAR,FORMFACTOR,BOXSCALE,DATA_BIN,output,decomposition,sphere);
      }
      else if (PDB.find(modlid) != PDB.end())
      {
        PHASER_ASSERT(OCC->find(modlid) != OCC->end());
        ensemble[modlid].set_data_pdb(PDB[modlid]);
        ensemble[modlid].setPDB(MAPHIRES,SOLPAR,FORMFACTOR,BOXSCALE,DATA_BIN,output,decomposition,sphere,&(*OCC)[modlid]);
      }
      else throw PhaserError(FATAL,"No input for ensemble " + modlid);
    }
    //setup variances
    ensemble[modlid].setup_vrms(ensemble[modlid].DV.DRMS.initial());
    ensemble[modlid].setup_cell(1);
    if (KEEPENS.find(*iter) == KEEPENS.end()) { ensemble[modlid].empty(); }
  }
}

void MapEnsemble::ensembling_table(outStream where,floatType TOTAL_SCAT,Output& output)
{
  output.logUnderLine(LOGFILE,"Ensemble Generation");
  output.logTab(1,LOGFILE,"Resolution of Ensembles: " + dtos(HiRes()));
  output.logTabPrintf(1,LOGFILE,"%-5s %6s %6s %5s %6s %6s %6s %-35s\n","Scat%","Radius","Model#","Rel-B","RMS","DRMS","VRMS","Ensemble");
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
  for (int ipdb = 0; ipdb < iter->second.nMols(); ipdb++)
  for (int m = 0; m < iter->second.ENSEMBLE[ipdb].nmodels(); m++)
  {
    PHASER_ASSERT((fn::pow2(iter->second.DV.VRMS[ipdb][m].fixed) + iter->second.DV.DRMS.initial()) >= 0);
    double VRMS =  std::sqrt(fn::pow2(iter->second.DV.VRMS[ipdb][m].fixed) + iter->second.DV.DRMS.initial());
    PHASER_ASSERT(ipdb < iter->second.relative_wilsonB.size());
    PHASER_ASSERT(m < iter->second.relative_wilsonB[ipdb].size());
    PHASER_ASSERT(ipdb < iter->second.DV.VRMS.size());
    PHASER_ASSERT(m < iter->second.DV.VRMS[ipdb].size());
    if (ipdb == 0 && m == 0)
    {
      output.logTabPrintf(1,where,"%5.1f %6.1f %6d %5.1f %6.3f %6.3f %6.3f %-35s\n",
           100*iter->second.SCATTERING/TOTAL_SCAT,
           iter->second.mean_radius(),
           1,
           iter->second.relative_wilsonB[0][0],
           iter->second.DV.VRMS[0][0].fixed,
           iter->second.DV.DRMS.initial(),
           VRMS,
           iter->first.substr(0, 15).c_str());
    }
    else
    {
      output.logTabPrintf(1,where,"%5s %6s %6d %5.1f %6.3f %6s %6.3f\n",
           "",
           "",
           iter->second.DV.forward_lookup[ipdb][m]+1,
           iter->second.relative_wilsonB[ipdb][m],
           iter->second.DV.VRMS[ipdb][m].fixed,
           "",
          VRMS);
    }
  }
  output.logBlank(LOGFILE);
}

void MapEnsemble::setPtInterpEV(int ptsE)
{
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
  {
    std::string m = iter->first;
    if (ensemble[m].is_atom) ensemble[m].setPtInterpEV(0);
    else ensemble[m].setPtInterpEV(ptsE);
  }
}

void MapEnsemble::setup_vrms(map_str_float DRMS)
{
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    iter->second.setup_vrms(DRMS[iter->first]);
}

void MapEnsemble::setup_cell(map_str_float CELL)
{
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    iter->second.setup_cell(CELL[iter->first]);
}

void MapEnsemble::set_ssqr(floatType rssqr)
{
  floatType sqrtrssqr = std::sqrt(rssqr);
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    iter->second.set_sqrt_ssqr(sqrtrssqr,rssqr);
}

std::map<std::string,drms_vrms> MapEnsemble::get_drms_vrms()
{
  std::map<std::string,drms_vrms> ens;
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
  {
    PHASER_ASSERT(iter->second.DV.size());
    ens[iter->first] = iter->second.DV;
  }
  return ens;
}

std::map<std::string,double> MapEnsemble::get_cell_scale()
{
  std::map<std::string,double> ens;
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    ens[iter->first] = double(iter->second.CELL_SCALE);
  return ens;
}

map_str_float2D MapEnsemble::get_map_relative_wilsonB()
{
  std::map<std::string,float2D> ens;
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    ens[iter->first] = iter->second.relative_wilsonB;
  return ens;
}

std::set<std::string> MapEnsemble::set_of_modlids()
{
  std::set<std::string> ens;
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    ens.insert(iter->first);
  return ens;
}

bool MapEnsemble::has_pdb()
{
  bool has_pdb(false);
  for (ensIter iter = ensemble.begin(); iter != ensemble.end(); iter++)
    has_pdb = has_pdb || ensemble.find(iter->first)->second.from_pdb();
  return has_pdb;
}

}//phaser
