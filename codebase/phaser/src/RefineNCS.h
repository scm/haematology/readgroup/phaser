//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineNCSClass__
#define __RefineNCSClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolNCS.h>
#include <phaser/src/DataB.h>
#include <phaser/include/data_tncs.h>
#include <phaser/src/RefineBase2.h>

namespace phaser {

class RefineNCS : public RefineBase2, public DataB
{
  public:
    RefineNCS()
    {
      likelihood = tncs_gfunction_radius = 0;
      tncs_angle = tncs_vector = dvect3(0,0,0);
    }

    RefineNCS(DataB,dvect3);
    ~RefineNCS() {}

   //concrete member functions
    floatType    targetFn();  //this is where main body goes
    floatType    gradientFn(TNT::Vector<floatType>&);
    floatType    hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    void         applyShift(TNT::Vector<floatType>&);
    void         logCurrent(outStream,Output&);
    void         logInitial(outStream,Output&);
    void         logFinal(outStream,Output&);
    bool1D       getRefineMask(protocolPtr);
    void         cleanUp(outStream,Output&);
    std::string  whatAmI(int&);
    std::vector<reparams>  getRepar();
    std::vector<bounds>    getLowerBounds();
    std::vector<bounds>    getUpperBounds();
    TNT::Vector<floatType> getRefinePars();
    TNT::Vector<floatType> getLargeShifts();

  private:
    floatType likelihood;
    floatType tncs_gfunction_radius;
    dvect3    tncs_angle,tncs_vector;
    float1D   tncs_varbins,tncs_epsfac;
    float1D   SigmaN;

  private:
    void      calcNcsEpsn();
    floatType target_gradient_hessian(bool,bool,TNT::Vector<floatType>&,bool,TNT::Fortran_Matrix<floatType>&, bool&);

  public:
    floatType getLikelihood() { return likelihood; }
    dvect3    getAngle() { return tncs_angle; }
    dvect3    getVector() { return tncs_vector; }
    float1D   getVariances() { return tncs_varbins; }
    float1D   getNcsEpsfac();
    void      logNcsEpsn(outStream,Output&);
};

} //phaser

#endif
