//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SiteListClass__
#define __SiteListClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_zscore.h>
#include <phaser/src/SiteList.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/lib/maths.h>
#include <phaser/include/data_peak.h>

namespace phaser {

// The class carries out the functions that are common to SiteListXyz and
// SiteListAng, which will both be derived from this class
// The virtual functions have the functionality that is required to extract
// the information from the list of xyzsites (in SiteListXyz) or the list of
// angsites (in SiteListAng), and will have concrete members in these classes

class SiteList : public SpaceGroup,public UnitCell
{
  public:
  SiteList(SpaceGroup& sg_,UnitCell& uc_):SpaceGroup(sg_),UnitCell(uc_)
  {
    max_nPrinted = 3;
    max_nGraphed = 50;
    MEAN = TOP = 0;
    FAST = RESCORE = false;
    ZSCORE = false;
  }

  protected:
   floatType MEAN,TOP;
   data_peak PEAKS;
   bool      FAST,RESCORE;
   unsigned  max_nPrinted,max_nGraphed;
   data_zscore ZSCORE;

  public:
   void set_stats(floatType m,floatType t,data_peak p,data_zscore z)
        { MEAN = m; TOP = t; PEAKS = p; ZSCORE = z; }
   void set_peaks(data_peak p)
        {  PEAKS = p; }
   void set_rescore(bool b) { RESCORE = b; }
   void set_fast(bool b) { FAST = b; }
   void select_and_log(outStream,Output&,bool=false);

  private:
    virtual bool      isSelected(unsigned);
    virtual bool      isTopSite(unsigned);
    virtual bool      isDeep(unsigned);
    virtual floatType getValue(unsigned);
    virtual unsigned  getNum(unsigned);
    virtual floatType getSignal(unsigned);
    virtual void      setNum(unsigned,unsigned);
    virtual void      setValue(unsigned,floatType);
    virtual void      setTopSite(unsigned,bool);
    virtual void      setDeep(unsigned,bool);
    virtual void      Sort();
    virtual void      setSelected(unsigned,bool);
    virtual void      archiveValue(unsigned);
    virtual unsigned  Size();
    virtual void      Copy(int,int);
    virtual void      Erase(int,int);
    virtual unsigned  numSelected();

    virtual void      flagClusteredTop(int);
    virtual void      flagClusteredTopNew(int);
    virtual void      logRawTop(outStream,Output&);
    virtual void      logClusteredTop(outStream,Output&);
    virtual void      logRawTopRescored(outStream,Output&);
    virtual void      logClusteredTopRescored(outStream,Output&);
    virtual void      graphRawTop(outStream,Output&);
    virtual void      graphClusteredTop(outStream,Output&);
    virtual void      graphRawTopRescored(outStream,Output&);
    virtual void      graphClusteredTopRescored(outStream,Output&);

  public:
    void      archiveValues();
    void      renumber();
    floatType getTop();
    floatType getTail();
    int       Over(double);
    floatType getTopTFZ();
    void      purgeUnselected();
    void      selectRawTop();
    void      selectRawTopByNumber(floatType);
    void      logRawTopInfo(outStream,Output&);
    void      selectClusteredTop();
    void      logClusteredTopInfo(outStream,Output&);
};

inline bool      SiteList::isSelected(unsigned a) { return false; }
inline bool      SiteList::isTopSite(unsigned a) { return false; }
inline bool      SiteList::isDeep(unsigned a) { return false; }
inline floatType SiteList::getValue(unsigned a) { return 0; }
inline unsigned  SiteList::getNum(unsigned a) { return 0; }
inline floatType SiteList::getSignal(unsigned a) { return 0; }
inline void      SiteList::setNum(unsigned a,unsigned b)  {}
inline void      SiteList::setValue(unsigned a,floatType b) {}
inline void      SiteList::setTopSite(unsigned a,bool b) {}
inline void      SiteList::setDeep(unsigned a,bool b) {}
inline void      SiteList::Sort() {}
inline void      SiteList::setSelected(unsigned a,bool b) {}
inline void      SiteList::archiveValue(unsigned a) {}
inline unsigned  SiteList::Size() { return 0; }
inline void      SiteList::Erase(int,int) { }
inline void      SiteList::Copy(int,int) { }
inline unsigned  SiteList::numSelected() { return 0; }

inline void  SiteList::logClusteredTop(outStream s,Output& o) {}
inline void  SiteList::flagClusteredTop(int) {}
inline void  SiteList::flagClusteredTopNew(int) {}
inline void  SiteList::logRawTopRescored(outStream a,Output& b) {}
inline void  SiteList::logRawTop(outStream a,Output& b) {}
inline void  SiteList::logClusteredTopRescored(outStream a,Output& b) {}
inline void  SiteList::graphRawTop(outStream,Output&) {}
inline void  SiteList::graphClusteredTop(outStream a,Output& b) {}
inline void  SiteList::graphRawTopRescored(outStream a,Output& b) {}
inline void  SiteList::graphClusteredTopRescored(outStream a,Output& b) {}


} //end namespace phaser

#endif
