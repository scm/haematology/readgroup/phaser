//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EqualUnityClass__
#define __EqualUnityClass__
#include <phaser/main/Phaser.h>
namespace phaser {

//points equally distributed, weights unity  i.e. no quadrature

class EqualUnity
{
  public:
    EqualUnity(int = 15,floatType = 0);

    unsigned nStep;
    float1D Ang;
    float1D CosAng,Cos2Ang;
    float1D SinAng,Sin2Ang;
    floatType Weight;

  public:
    void resize(int n,floatType = 0);
    floatType size() { return nStep; }
    floatType ang(int p) { return Ang[p]; }
    floatType cosAng(int p) { return CosAng[p]; }
    floatType sinAng(int p) { return SinAng[p]; }
    floatType cos2Ang(int p) { return Cos2Ang[p]; }
    floatType sin2Ang(int p) { return Sin2Ang[p]; }
};

}//phaser

#endif
