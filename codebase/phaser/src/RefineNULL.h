//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineNULLClass__
#define __RefineNULLClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolNULL.h>
#include <phaser/src/RefineBase2.h>
#include <phaser/src/DataSAD.h>

namespace phaser {

class RefineNULL : public RefineBase2, public DataSAD
{
  private:
    af_float CCC;

  public:
    RefineNULL() { }
    RefineNULL(std::string,
               af::double6,
               af::shared<miller::index<int> >,
               data_spots,data_spots,
               data_tncs,
               data_bins,
               std::map<std::string,cctbx::eltbx::fp_fdp>,
               data_outl,
               data_part,
               Output&);
    ~RefineNULL() { }

   //concrete member functions
    floatType    targetFn();
    floatType    gradientFn(TNT::Vector<floatType>&);
    floatType    hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    void         applyShift(TNT::Vector<floatType>&);
    void         rejectOutliers(outStream,Output&);
    void         logCurrent(outStream,Output&);
    void         logInitial(outStream,Output&);
    void         logFinal(outStream,Output&);
    bool1D       getRefineMask(protocolPtr);
    void         cleanUp(outStream,Output&);
    std::string  whatAmI(int&);
    std::vector<reparams>   getRepar();
    std::vector<bounds>     getLowerBounds();
    std::vector<bounds>     getUpperBounds();
    TNT::Vector<floatType>  getRefinePars();
    TNT::Vector<floatType>  getLargeShifts();
};

} //phaser

#endif
