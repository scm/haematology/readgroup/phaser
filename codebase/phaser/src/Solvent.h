//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TNCS_SOLVENT__
#define __TNCS_SOLVENT__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_composition.h>

namespace phaser {

class Solvent
{
  public:
    Solvent () { sc = thisVM = minVM = maxVM = 0; }
    Solvent(std::string,af::double6,data_composition&);
    std::string throwError();

  private:
    floatType sc,thisVM,minVM,maxVM;

  public:
    floatType getMinVM() { return minVM; }
    floatType getMaxVM() { return maxVM; }
    floatType VM()       { return thisVM; }
    bool      high()     { return (thisVM >= maxVM); }
    bool      low()      { return (thisVM <= minVM); }
};

} //phaser

#endif
