//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <cstring>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rad2phi.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/Molecule.h>
#include <phaser/lib/sphericalY.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <scitbx/math/r3_rotation.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

void Ensemble::set_sqrt_ssqr(floatType& sqrtrssqr,floatType& rssqr)
{
  midres.ibin = ENS_BIN.get_bin(sqrtrssqr);
  midres.midssqr = binned[midres.ibin].MidSsqr;
  midres.ssqr = rssqr;
}

floatType Ensemble::dvrms_delta_const()
{ return -two_pi_sq_on_three*midres.Ssqr(); }

floatType Ensemble::Eterm_const()
{ return binned[midres.ibin].Eterm; }

void Ensemble::setup_vrms(floatType delta_VRMS)
{
  delta_VRMS = std::min(DV.DRMS.upper,delta_VRMS);
  delta_VRMS = std::max(DV.DRMS.lower,delta_VRMS);
  for (int ibin = 0; ibin < mapV.size(); ibin++)
  {
    floatType buf_vrms = delta_VRMS ? std::exp(-two_pi_sq_on_three*binned[ibin].MidSsqr*delta_VRMS) : 1;
    binned[ibin].Eterm = buf_vrms;
    binned[ibin].Vterm = fn::pow2(buf_vrms);
  }
}

void Ensemble::setup_cell(floatType scale_CELL)
{
  scale_CELL = std::min(DEF_CELL_SCALE_MAX,scale_CELL);
  scale_CELL = std::max(DEF_CELL_SCALE_MIN,scale_CELL);
  dvect3 box;
  box[0] = scale_CELL*input_CELL.A();
  box[1] = scale_CELL*input_CELL.B();
  box[2] = scale_CELL*input_CELL.C();
  UnitCell::setUnitCell(box);
}

cmplxType Ensemble::InterpE(dvect3 v,floatType dfac)
{ return dfac*(this->*pInterpE)(v)*binned[midres.ibin].Eterm; }

cmplxType Ensemble::func_grad_hess_InterpE(dvect3 v,floatType dfac,cvect3& gradient,bool& return_hessian,cmat33& hessian)
{ return dfac*(this->*pdInterpE)(v,gradient,return_hessian,hessian); }

void Ensemble::setPtInterpEV(int ptsE)
{
  PHASER_ASSERT(ptsE == 0 || ptsE == 1 || ptsE == 4 || ptsE == 8);
  if (ptsE == 0)      pInterpE = &Ensemble::OneAtomE;
  else if (ptsE == 1) pInterpE = &Ensemble::OnePtInterpE;
  else if (ptsE == 4) pInterpE = &Ensemble::FourPtInterpE;
  else if (ptsE == 8) pInterpE = &Ensemble::EightPtInterpE;
  if (ptsE == 0)      pdInterpE = &Ensemble::dOneAtomE;
  else if (ptsE == 1) pdInterpE = &Ensemble::dOnePtInterpE;
  else if (ptsE == 4) pdInterpE = &Ensemble::dFourPtInterpE;
  else if (ptsE == 8) pdInterpE = &Ensemble::dEightPtInterpE;
  if (ptsE == 0)      pInterpV = &Ensemble::OneAtomV;
  else                pInterpV = &Ensemble::OnePtInterpV;
}

inline int floor_double2int(const floatType& d)
{
  return ( d >= 0 ? int( d ) : int( d ) - 1 );
}

cmplxType Ensemble::OneAtomE(dvect3& offgridHKL)
{
  return mapE[midres.ibin];
}

cmplxType Ensemble::dOneAtomE(dvect3& offgridHKL,cvect3& gradient,bool& return_hessian,cmat33& hessian)
{
  //gradient=cvect3(0,0,0); external initiation
  //hessian=cmat33(0,0,0,0,0,0,0,0,0); external initiation
  return OneAtomE(offgridHKL);
}

cmplxType Ensemble::OnePtInterpE(dvect3& offgridHKL)
{
  //rounding operation round = floor(A=0.5);
  nearH = static_cast<int>(floor(offgridHKL[0]+ 0.5));
  nearK = static_cast<int>(floor(offgridHKL[1]+ 0.5));
  nearL = static_cast<int>(floor(offgridHKL[2]+ 0.5));
  return getE(nearH,nearK,nearL);
}

cmplxType Ensemble::dOnePtInterpE(dvect3& offgridHKL,cvect3& gradient,bool& return_hessian,cmat33& hessian)
{
  //Shouldn't call this function because it doesn't compute gradient or Hessian!

  //gradient=cvect3(0,0,0); external initiation
  //hessian=cmat33(0,0,0,0,0,0,0,0,0); external initiation
  //rounding operation round = floor(A+0.5);
  PHASER_ASSERT(!return_hessian); // Full Hessian calculation requires evaluation of all 8 points
  nearH = static_cast<int>(floor(offgridHKL[0] + 0.5));
  nearK = static_cast<int>(floor(offgridHKL[1] + 0.5));
  nearL = static_cast<int>(floor(offgridHKL[2] + 0.5));
  cmplxType near(getE(nearH,nearK,nearL));
  return near;
}

cmplxType Ensemble::FourPtInterpE(dvect3& offgridHKL)
{
  //rounding operation round = floor(A+0.5);
  nearH = static_cast<int>(floor(offgridHKL[0] + 0.5));
  nearK = static_cast<int>(floor(offgridHKL[1] + 0.5));
  nearL = static_cast<int>(floor(offgridHKL[2] + 0.5));
  fracH = offgridHKL[0]-nearH;
  fracK = offgridHKL[1]-nearK;
  fracL = offgridHKL[2]-nearL;
  cmplxType near(getE(nearH,nearK,nearL));
  cmplxType interpolated(near);
  interpolated += (fracH >= 0) ?  fracH * (getE(nearH+1,nearK,nearL) - near) : fracH * (near - getE(nearH-1,nearK,nearL)) ;
  interpolated += (fracK >= 0) ?  fracK * (getE(nearH,nearK+1,nearL) - near) : fracK * (near - getE(nearH,nearK-1,nearL)) ;
  interpolated += (fracL >= 0) ?  fracL * (getE(nearH,nearK,nearL+1) - near) : fracL * (near - getE(nearH,nearK,nearL-1)) ;
  return interpolated;
}

cmplxType Ensemble::dFourPtInterpE(dvect3& offgridHKL,cvect3& gradient,bool& return_hessian,cmat33& hessian)
{
  PHASER_ASSERT(!return_hessian); // Full Hessian calculation requires evaluation of all 8 points

  //gradient=cvect3(0,0,0); external initiation
  //hessian=cmat33(0,0,0,0,0,0,0,0,0); external initiation

  //rounding operation round = floor(A+0.5);
  nearH = static_cast<int>(floor(offgridHKL[0] + 0.5));
  nearK = static_cast<int>(floor(offgridHKL[1] + 0.5));
  nearL = static_cast<int>(floor(offgridHKL[2] + 0.5));
  fracH = offgridHKL[0]-nearH;
  fracK = offgridHKL[1]-nearK;
  fracL = offgridHKL[2]-nearL;
  cmplxType near = getE(nearH,nearK,nearL);
  cmplxType interpolated(near);
  cmplxType V = (fracH >= 0) ? (getE(nearH+1,nearK,nearL) - near) : (near - getE(nearH-1,nearK,nearL));
  interpolated += fracH * V;
  gradient[0] = V;
            V = (fracK >= 0) ? (getE(nearH,nearK+1,nearL) - near) : (near - getE(nearH,nearK-1,nearL));
  interpolated += fracK * V;
  gradient[1] = V;
            V = (fracL >= 0) ? (getE(nearH,nearK,nearL+1) - near) : (near - getE(nearH,nearK,nearL-1));
  interpolated += fracL * V;
  gradient[2] = V;

  return interpolated;
}

cmplxType Ensemble::EightPtInterpE(dvect3& offgridHKL)
{
  //truncate all coordinates to lower floor of box
  floorH = floor_double2int( offgridHKL[0] );
  floorK = floor_double2int( offgridHKL[1] );
  floorL = floor_double2int( offgridHKL[2] );

  //fractions in each direction
  fracH = offgridHKL[0] - floorH;
  fracK = offgridHKL[1] - floorK;
  fracL = offgridHKL[2] - floorL;
  floatType invH = 1 - fracH;

  return ( 1.0 - fracL ) * (
      ( 1 - fracK ) * ( invH  * getE( floorH,     floorK,     floorL     )
                      + fracH * getE( floorH + 1, floorK,     floorL     ) )
    + fracK         * ( invH  * getE( floorH,     floorK + 1, floorL     )
                      + fracH * getE( floorH + 1, floorK + 1, floorL     ) )
    )
    + fracL * (
      ( 1 - fracK ) * ( invH  * getE( floorH,     floorK,     floorL + 1 )
                      + fracH * getE( floorH + 1, floorK,     floorL + 1 ) )
    + fracK         * ( invH  * getE( floorH,     floorK + 1, floorL + 1 )
                      + fracH * getE( floorH + 1, floorK + 1, floorL + 1 ) )
    );
   //   (1-fracL)*(1-fracK)*(1-fracH)*(1)          (7)--------(8)
   // + (1-fracL)*(1-fracK)*(  fracH)*(2)          /          /|
   // + (1-fracL)*(  fracK)*(1-fracH)*(3)        (5)--------(6)|
   // + (1-fracL)*(  fracK)*(  fracH)*(4)         | |        | |
   // + (  fracL)*(1-fracK)*(1-fracH)*(5)         L | *      | |
   // + (  fracL)*(1-fracK)*(  fracH)*(6)         |(3)--------(4)
   // + (  fracL)*(  fracK)*(1-fracH)*(7)         |K  +      |/
   // + (  fracL)*(  fracK)*(  fracH)*(8)        (1)--H-----(2)
}

cmplxType Ensemble::dEightPtInterpE(dvect3& offgridHKL,cvect3& gradient,bool& return_hessian,cmat33& hessian)
{
  //gradient=cvect3(0,0,0); external initiation
  //hessian=cmat33(0,0,0,0,0,0,0,0,0); external initiation
  //truncate all coordinates to lower floor of box
  floorH = floor_double2int( offgridHKL[0] );
  floorK = floor_double2int( offgridHKL[1] );
  floorL = floor_double2int( offgridHKL[2] );

  //fractions in each direction
  fracH = offgridHKL[0] - floorH;
  fracK = offgridHKL[1] - floorK;
  fracL = offgridHKL[2] - floorL;
  floatType invsH = 1 - fracH;
  floatType invsK = 1 - fracK;
  floatType invsL = 1 - fracL;

  cmplx1D cube(8);
  cube[0] = getE( floorH,     floorK,     floorL     );
  cube[1] = getE( floorH + 1, floorK,     floorL     );
  cube[2] = getE( floorH,     floorK + 1, floorL     );
  cube[3] = getE( floorH + 1, floorK + 1, floorL     );
  cube[4] = getE( floorH,     floorK,     floorL + 1 );
  cube[5] = getE( floorH + 1, floorK,     floorL + 1 );
  cube[6] = getE( floorH,     floorK + 1, floorL + 1 );
  cube[7] = getE( floorH + 1, floorK + 1, floorL + 1 );

  cmplxType target =  invsL * (invsK * (invsH * cube[0] + fracH * cube[1] ) + fracK * (invsH * cube[2] + fracH * cube[3]) ) +
                      fracL * (invsK * (invsH * cube[4] + fracH * cube[5] ) + fracK * (invsH * cube[6] + fracH * cube[7]) );

  gradient[0] =  invsL * (invsK * (-cube[0] + cube[1]) + fracK * (-cube[2] + cube[3]) ) +
              fracL * (invsK * (-cube[4] + cube[5]) + fracK * (-cube[6] + cube[7]) );

  gradient[1] =  invsL * (-1.0 * (invsH * cube[0] + fracH * cube[1]) + (invsH * cube[2] + fracH * cube[3]) ) +
              fracL * (-1.0 * (invsH * cube[4] + fracH * cube[5]) + (invsH * cube[6] + fracH * cube[7]) );

  gradient[2] =  -1.0 * (invsK * (invsH * cube[0] + fracH * cube[1]) + fracK * (invsH * cube[2] + fracH * cube[3]) ) +
                     (invsK * (invsH * cube[4] + fracH * cube[5]) + fracK * (invsH * cube[6] + fracH * cube[7]) );

  if (return_hessian)
  {
    // Extend outside box (towards lower resolution, i.e. within resolution of molecular transform) to get
    // diagonal curvature terms from finite differences
    cmplxType foutside,cmplx2(2.,0.);
    if (floorH >= 0)
    {
      foutside = getE(floorH-1,floorK,floorL);
      hessian(0,0) = (foutside - cmplx2*cube[0] + cube[1]);
    }
    else
    {
      foutside = getE(floorH+2,floorK,floorL);
      hessian(0,0) = (cube[0] - cmplx2*cube[1] + foutside);
    }
    if (floorK >= 0)
    {
      foutside = getE(floorH,floorK-1,floorL);
      hessian(1,1) = (foutside - cmplx2*cube[0] + cube[2]);
    }
    else
    {
      foutside = getE(floorH,floorK+2,floorL);
      hessian(1,1) = (cube[0] - cmplx2*cube[2] + foutside);
    }
    if (floorL >= 0)
    {
      foutside = getE(floorH,floorK,floorL-1);
      hessian(2,2) = (foutside - cmplx2*cube[0] + cube[4]);
    }
    else
    {
      foutside = getE(floorH,floorK,floorL+2);
      hessian(2,2) = (cube[0] - cmplx2*cube[4] + foutside);
    }

    // Off-diagonal Hessian terms are analytic derivatives of gradients above
    hessian(0,1)=  invsL * (cube[0] - cube[1] - cube[2] + cube[3]) +
                   fracL * (cube[4] - cube[5] - cube[6] + cube[7]);
    hessian(0,2)=  invsL * (cube[0] - cube[1] - cube[4] + cube[5]) +
                   fracL * (cube[2] - cube[3] - cube[6] + cube[7]);
    hessian(1,2)=  invsL * (cube[0] - cube[2] - cube[4] + cube[6]) +
                   fracL * (cube[1] - cube[3] - cube[5] + cube[7]);
    hessian(1,0) = hessian(0,1);
    hessian(2,0) = hessian(0,2);
    hessian(2,1) = hessian(1,2);
  }
  return target;
}

floatType Ensemble::InterpV(floatType dfacsqr)
{ return dfacsqr*(this->*pInterpV)()*binned[midres.ibin].Vterm; }

floatType Ensemble::AtomScatRatio()
{ return ScatRat[midres.ibin] ; }

floatType Ensemble::maxAtomScatRatio()
{ return max(ScatRat) ; }

floatType Ensemble::OneAtomV()
{ return getV(); }

floatType Ensemble::OnePtInterpV()
{ return getV(); }

void Ensemble::writeMtz(std::string HKLOUT,Output& output)
{
  int nxtal(1),nset[1];
  nset[0] = 1;
  CMtz::MTZ *mtz = CMtz::MtzMalloc(nxtal,nset);
  /* initialise main header */
  output.logTab(1,DEBUG,itos(NREFL) + " reflections for mtz file");
  std::strcpy(mtz->title,"Ensemble from Phaser");
  mtz->nxtal = nxtal;
  mtz->mtzsymm.nsym = 1;
  mtz->mtzsymm.nsymp = 1;
  mtz->mtzsymm.spcgrp = 1;
  mtz->mtzsymm.symtyp = 'P';
  std::strcpy(mtz->mtzsymm.spcgrpname,"P 1");
  std::strcpy(mtz->mtzsymm.pgname,"PG1");
  for (int i = 0; i < 3; i++)
  {
    mtz->mtzsymm.sym[0][i][3] = 0;
    for (int j = 0; j < 3; j++)
      mtz->mtzsymm.sym[0][i][j] = (i==j) ? 1 : 0;
  }
 // const char history[][MTZRECORDLENGTH] dd
 // MtzAddHistory(mtz,

  //nref must be set before columns are added as this defines their size
  mtz->nref = NREFL-1;
  CMtz::MTZCOL* mtzH  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"H","H");
  CMtz::MTZCOL* mtzK  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"K","H");
  CMtz::MTZCOL* mtzL  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"L","H");
  CMtz::MTZCOL* mtzE  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"E","F");
  CMtz::MTZCOL* mtzP  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"P","P");
  CMtz::MTZCOL* mtzV  = CMtz::MtzAddColumn(mtz,mtz->xtal[0]->set[0],"V","V");
  PHASER_ASSERT(mtzH != 0);
  PHASER_ASSERT(mtzK != 0);
  PHASER_ASSERT(mtzL != 0);
  PHASER_ASSERT(mtzE != 0);
  PHASER_ASSERT(mtzP != 0);
  PHASER_ASSERT(mtzV != 0);

  int mtzr(0);
  for (int h = 0; h < sizeh; h++)
    for (int k = 0; k < sizek; k++)
      for (int l = 0; l < sizel; l++)
        if (std::abs(mapE[((h*sizek)+k)*sizel+l]) != 0)
        {
          ivect3 hkl(getHKL(h,k,l));
          miller::index<int> millerhkl(hkl);
          floatType rssqr = UnitCell::Ssqr(millerhkl);
          floatType sqrtrssqr = std::sqrt(rssqr);
          set_sqrt_ssqr(sqrtrssqr,rssqr);
          mtzH->ref[mtzr] = hkl[0];
          mtzK->ref[mtzr] = hkl[1];
          mtzL->ref[mtzr] = hkl[2];
          cmplxType E(getE(hkl[0],hkl[1],hkl[2]));
                    E *= binned[midres.ibin].Eterm;
          floatType thisV(getV());
                    thisV *= binned[midres.ibin].Vterm;
          mtzE->ref[mtzr] = std::abs(E);
          mtzP->ref[mtzr] = scitbx::rad_as_deg(std::arg(E));
          mtzV->ref[mtzr] = thisV;
          mtzr++;
        }

  //store the cell with the scale factor applied
  mtz->xtal[0]->cell[0] = UnitCell::A();
  mtz->xtal[0]->cell[1] = UnitCell::B();
  mtz->xtal[0]->cell[2] = UnitCell::C();
  mtz->xtal[0]->cell[3] = UnitCell::Alpha();
  mtz->xtal[0]->cell[4] = UnitCell::Beta();
  mtz->xtal[0]->cell[5] = UnitCell::Gamma();

  CMtz::MtzPut(mtz, const_cast<char*>(HKLOUT.c_str()));

  CMtz::MtzFree(mtz);
}

cmplxType& Ensemble::getE(int h,int k,int l)
{
#if 0
  int hh(h),kk(k),ll(l);
#endif
  bool pos_l(true);
  if (l < 0 || (l == 0 && k < 0) || (l == 0 && k == 0 && h < 0)) {pos_l = false; h *= -1; k *= -1; l *= -1; }
  if (h < 0) h += sizeh;
  if (k < 0) k += sizek;
  buf_int = ((h*sizek)+k)*sizel+l;
  //int i = ((h*sizek)+k)*sizel+l;
  //buf_int = index[i] > 0 ? index[i] : -index[i];
#if 0
  if (buf_int < 0 || buf_int >= mapE.size())
  {
    miller::index<int> hkl(hh,kk,ll);
    floatType reso = UnitCell::reso(hkl);
    std::cout << "debug: hkl=(" << hh << "," << kk << "," << ll << ") reso=" << reso << std::endl;
    std::cout << "debug: limits h=" << h << " " << sizeh << std::endl;
    std::cout << "debug: limits k=" << k << " " << sizek << std::endl;
    std::cout << "debug: limits l=" << l << " " << sizel << std::endl;
  }
#endif
  PHASER_ASSERT(buf_int >= 0);
  PHASER_ASSERT(buf_int < mapE.size());
  buf_cmplxType = mapE[buf_int];
  if(!pos_l) buf_cmplxType = std::conj(buf_cmplxType);
  return buf_cmplxType;
//the time here is taken loading the relevant section of the map into memory
}

floatType& Ensemble::getV()
{
  buf_floatType = mapV[midres.ibin];
  return buf_floatType;
}

ivect3 Ensemble::getHKL(int h,int k,int l)
{
  if (h > sizeh/2) h -=sizeh;
  if (k > sizek/2) k -=sizek;
#if 0
  bool pos_l(true);
#endif
  if (l < 0 || (l == 0 && k < 0) || (l == 0 && k == 0 && h < 0))
  {
#if 0
    pos_l = false;
#endif
    h *= -1; k *= -1; l *= -1; 
  }
  return ivect3(h,k,l);
}

void Ensemble::setE(int h,int k,int l,cmplxType e)
{
  bool pos_l(true);
  if (l < 0 || (l == 0 && k < 0) || (l == 0 && k == 0 && h < 0))
    {pos_l = false; h *= -1; k *= -1; l *= -1; }
  if (h < 0) h += sizeh;
  if (k < 0) k += sizek;
  //PHASER_ASSERT((((h*sizek)+k)*sizel+l) < index.size());
  //index[((h*sizek)+k)*sizel+l] = 0; //should be 0 already from resizeMaps
 // PHASER_ASSERT(index[((h*sizek)+k)*sizel+l] == 0); //one user ended up getting this - instability?

  //store whether Friedel mate was stored
  PHASER_ASSERT((((h*sizek)+k)*sizel+l) < mapE.size());
  mapE[((h*sizek)+k)*sizel+l] = pos_l ? e : std::conj(e);
  //if (!pos_l) e = std::conj(e);
  //mapE.push_back(std::complex<mapFloat>(e));
  NREFL++;
}

void Ensemble::setV(int ibin,floatType v)
{ mapV[ibin] = v; } // Note this is really a covariance proportional to sigmaA^2

void Ensemble::setE(int ibin,floatType v)
{ mapE[ibin] = v; }

void Ensemble::setScatRat(int ibin,floatType v)
{ ScatRat[ibin] = v; }

void Ensemble::resizeMaps(int a, int b, int c)
{
  sizeh = a;
  sizek = b;
  sizel = c;
  resizeMaps();
}

void Ensemble::resizeMaps()
{
  NREFL = 0;
  mapE.resize(sizeh*sizek*sizel);
  for (int i = 0; i < mapE.size(); i++) mapE[i] = std::complex<mapFloat>(0,0); //explicit initialization
  mapE[0] = FLT_MIN; //prevents divide-by-zero
  NREFL++; //so first reflection is non-zero;
  mapV.clear();
  mapV.resize(ENS_BIN.numbins(),1);
  binned.resize(mapV.size());
  for (int ibin = 0; ibin < mapV.size(); ibin++)
    binned[ibin].MidSsqr = 1/fn::pow2(ENS_BIN.MidRes(ibin)); //ssqr mid res
  ScatRat.clear();
  ScatRat.resize(ENS_BIN.numbins(),1);
}

bool Ensemble::flatMap()
{
  if (!mapE.size()) return true;
  for (int x = 0; x < mapE.size(); x++)
    if (mapE[x] != mapE[0]) return false;
  return true;
}

void Ensemble::resizeMapsAtom()
{
  mapE.clear();
  mapE.resize(ENS_BIN.numbins(),1);
  mapV.clear();
  mapV.resize(ENS_BIN.numbins(),1);
  binned.resize(mapV.size());
  for (int ibin = 0; ibin < mapV.size(); ibin++)
    binned[ibin].MidSsqr = 1/fn::pow2(ENS_BIN.MidRes(ibin)); //ssqr mid res
  ScatRat.clear();
  ScatRat.resize(ENS_BIN.numbins(),1);
}

void Ensemble::logMemory(outStream where,Output& output)
{
  output.logTab(1,where,"Memory:");
  output.logTab(1,where,"Size of mapE " + itos(mapE.size()*sizeof(std::complex<mapFloat>)));
  output.logTab(1,where,"Size of mapV " + itos(mapV.size()*sizeof(mapFloat)));
  output.logTab(1,where,"Size of binV " + itos(binV.size()*sizeof(int)));
  floatType new_memory = mapE.size()*sizeof(std::complex<mapFloat>) +
               mapV.size()*sizeof(mapFloat) +
               binV.size()*sizeof(int);
  output.logTab(1,where,"Total Memory = " + dtos(new_memory));
  output.logBlank(where);
}

cmplx3D Ensemble::getELMNxR2(std::string TARGET,floatType TOTAL_SCAT,Output& output,int LMAX,data_bofac SEARCH_FACTORS,double LMAX_RESO)
{
  bool isCROWTHER(TARGET == "CROWTHER");
  bool isLERF1(TARGET == "LERF1");
  if (!(isCROWTHER || isLERF1)) throw PhaserError(FATAL,"Unknown FRF target: " + TARGET);

  cmplx3D elmn;
  const floatType ZERO(0),ONE(1),TWO(2);

  output.logBlank(VERBOSE);
  output.logTab(1,LOGFILE,"Elmn for Search Ensemble");

  HKL_clustered HKL_list;
 // bool do_gradient(false),do_hessian(false);
  floatType max_resolution = std::numeric_limits<floatType>::max();
  for (int h = 0; h < sizeh; h++)
  for (int k = 0; k < sizek; k++)
  for (int l = 0; l < sizel; l++)
  if (std::abs(mapE[((h*sizek)+k)*sizel+l]) != 0)
  {
    ivect3 hkl(getHKL(h,k,l));
    miller::index<int> MILLER(hkl[0],hkl[1],hkl[2]);
    floatType resolution = UnitCell::reso(MILLER);
    if (resolution >= LMAX_RESO)
    {
      floatType rssqr = UnitCell::Ssqr(MILLER);
      floatType sqrtrssqr = std::sqrt(rssqr);
      max_resolution = std::min(resolution,max_resolution);
      set_sqrt_ssqr(sqrtrssqr,rssqr);
      cmplxType E(getE(hkl[0],hkl[1],hkl[2])); //Weighted by D but not fracMove
                E *= binned[midres.ibin].Eterm;
      floatType thisV(getV()); //getV returns D^2
                thisV *= binned[midres.ibin].Vterm;
      //Avoid some extra work if B-factor and occupancy for search are left at default of 0 and 1.
      if (!SEARCH_FACTORS.defaults())
      {
        E *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
        E *= std::sqrt(SEARCH_FACTORS.OFAC);
        thisV *= exp(-TWO*SEARCH_FACTORS.BFAC*rssqr/4.0);
        thisV *= SEARCH_FACTORS.OFAC;
      }

      floatType Esqr = std::norm(E);
              //  Esqr *= scatFactor; //scale factor unnecessary
              //  thisV *= scatFactor; //scale factor unnecessary
      HKL HKL_temp;
      HKL_temp.flipped = (MILLER[2]!=0); //l!=0
      dvect3 hklscaled = UnitCell::HKLtoVecS(MILLER[0],MILLER[1],MILLER[2]);
      if (hklscaled[0]==0 && hklscaled[1]==0 && hklscaled[2]==0)
        continue;

      HKL_temp.hkl.x = hklscaled[0];
      HKL_temp.hkl.y = hklscaled[1];
      HKL_temp.hkl.z = hklscaled[2];
      HKL_temp.htp = toPolar(HKL_temp.hkl);
      /* Model Patterson coefficient for LERF1/2 is obtained from equation (20)
         of FRF paper by dividing by SigmaN (left out of observed Patterson
         coefficient in DataMR.cc), giving sigmaA^2*(Ecalc^2-1).
         Factor of fracMove/SymP would convert D^2 to sigmaA^2 corresponding to
         the fraction of the total scattering given by one symm copy, but the
         FRF result is normalised later anyway, so this is left out.
      */
      if (isCROWTHER)
        HKL_temp.intensity = Esqr;
      else if (isLERF1)
        HKL_temp.intensity = Esqr - thisV;

      HKL_list.add(HKL_temp);
    }
  }
  PHASER_ASSERT(HKL_list.clustered.size());
//randomize to even up the number on each thread
  HKL_list.shuffle();

  const int jmax(LMAX); //change notation

  output.logTab(1,VERBOSE, "Maximum degree l of spherical harmonics, Y[l,m](theta, phi), is " + itos(jmax));
  output.logTab(1,VERBOSE,"Number of HKL is "+itos(HKL_list.size()));
  output.logBlank(VERBOSE);

  elmn.resize(jmax/2); // j=2 => [0], j=jmax => [lmax/2-1]
  for (int j = 2; j <= jmax; j += 2)
  {
    int j_index = j/2-1;
    int nmax = (jmax-j+2)/2;

    elmn[j_index].resize(2*j+1);
    for (int m = 0; m <= j; m++)
    {
      elmn[j_index][j+m].resize(nmax+1);
      elmn[j_index][j-m].resize(nmax+1);

      for (int n = 1; n <= nmax; n++)
      {
        elmn[j_index][j+m][n] = 0;
        elmn[j_index][j-m][n] = 0;
      }
    }
  }

  float1D sqrt_table(3*jmax);
  for (int i=0; i<3*jmax; i++) sqrt_table[i]=sqrt(TWO*i+1);

// OpenMP reduction() clause cannot be used for parallelising the summing of
// the elmn variable as it hasn't got operator+ overloaded. So omp_elmn
// serves as an auxiliary variable to that end
  cmplx1D omp_elmn;
  cmplx1D omp_elmn_second;
  int nthreads = 1;
  int num_elmn(0);

#pragma omp parallel
  {
    int nthread = 0;
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
    nthread = omp_get_thread_num();
    //if (nthread != 0) output.usePhenixCallback(false);
#endif
    p3Dp htp;
    lnfactorial lnfac;

#pragma omp single
    {
      if (nthreads > 1)
        output.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
      output.logProgressBarStart(LOGFILE,"Elmn Calculation for Search Ensemble",HKL_list.clustered.size()/nthreads);

      for (int m = 0; m <= jmax; m++)
      {
        for (int l = m; l <= jmax ; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          int nmax = (jmax-l+2)/2;
       //   int l_index = l/2-1;
          for (int n = 1; n <= nmax; n++)
          {
            num_elmn++;
          } // n loop
        } // l loop
      } // m loop
      omp_elmn.resize(nthreads*num_elmn,0);
    }

#pragma omp for
    for (int c = 0; c < HKL_list.clustered.size(); c++)
    {
      int lmax = jmax;
      float1D spharmfn;
      PHASER_ASSERT(HKL_list.clustered[c].size());
      // floatType x = std::cos(HKL_list.clustered[c][0].htp.theta);
      // floatType sqRoot = std::sqrt(std::max(0.0,1.0-x*x));
      cmplxType cos_sin = std::exp(cmplxType(0.,HKL_list.clustered[c][0].htp.theta));
      floatType x = std::real(cos_sin);
      floatType sintheta = std::imag(cos_sin); // Previously sqRoot
     // floatType Pmj(0),Pmm(0),Pmm1(0);
      floatType Pmj(0),Pmm1(0);
      for (int m = 0; m <= lmax; m++)
      {
        floatType Pmm_j(0),Pmm1_j(0);
        floatType Pmm(1);
        if (m>0) {
          int odd_fact=1;
          for (int i=1; i<=m; i++) {
            // Pmm *= -sqRoot*odd_fact;
            Pmm *= -sintheta*odd_fact;
            odd_fact += 2;
          }
        }
        for (int l = m; l <= lmax ; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          floatType normalization = std::sqrt((2*l+1)/scitbx::constants::four_pi);
          floatType scale = normalization * exp(0.5*(lnfac.get(l-scitbx::fn::absolute(m))-lnfac.get(l+scitbx::fn::absolute(m))));
          if (Pmm_j)
          {
            Pmj = (x*(2*l-3)*Pmm1_j-(l+m-2)*Pmm_j)/(l-1-m);
            Pmm = Pmm1_j;
            Pmm1 = Pmj;
            Pmj = (x*(2*l-1)*Pmm1-(l+m-1)*Pmm)/(l-m);
            Pmm_j = Pmm1;
            Pmm1_j = Pmj;
          }
          else if (l==m)
          {
            Pmj = Pmm;
          }
          else if (l==m+1)
          {
            Pmm1=x*(2*m+1)*Pmm;
            Pmj = Pmm1;
          }
          else
          {
            Pmm1=x*(2*m+1)*Pmm;
            for (int j=m+2; j<=l; j++) {
              Pmj = (x*(2*j-1)*Pmm1-(j+m-1)*Pmm)/(j-m);
              Pmm = Pmm1;
              Pmm1 = Pmj;
            }
            Pmm_j = Pmm;
            Pmm1_j = Pmm1;
          }
          spharmfn.push_back(scale*Pmj);
        } // l loop
      } // m loop

      for (int r = 0; r <  HKL_list.clustered[c].size(); r++)
      {
        int i(0),n_elmn(0);
        htp = HKL_list.clustered[c][r].htp;
        floatType pintensity = HKL_list.clustered[c][r].intensity;
        floatType sign = (HKL_list.clustered[c][r].flipped) ?  TWO : ONE;
        pintensity *= sign;

        floatType H(LMAX*max_resolution*htp.r);
        float1D besselx(jmax+2);
        for (int u = 3; u<jmax+2; u+=2)
          besselx[u] = sqrt_table[u]*sphbessel(u,H)/H;

        cmplxType phi_phase_prev = ONE;
        cmplxType phi_phase_1 = std::exp(cmplxType(ZERO,ONE)*htp.phi);
        for (int m = 0; m <= lmax; m++)
        {
          cmplxType phi_phase_m = m ? phi_phase_prev * phi_phase_1 : phi_phase_prev;
          phi_phase_prev = phi_phase_m;
          for (int l = m; l <= lmax ; l += 2)
          {
            if (l%2 == 1) l++;
            if (l < 2) continue;
            cmplxType Ylm = spharmfn[i++];
                      Ylm *= phi_phase_m;
                      Ylm *= pintensity;
            int nmax = (lmax-l+2)/2;
         //   int l_index = l/2-1;
            for (int n = 1; n <= nmax; n++)
            {
              omp_elmn[nthread*num_elmn+n_elmn] += besselx[l+2*n-1]*Ylm; //note way of storing this!
              n_elmn++;
            }
          } // l loop
        } // m loop
      } // r loop
      if (nthread == 0) output.logProgressBarNext(LOGFILE);
    } // c loop
  }  // #pragma omp parallel

  output.logProgressBarEnd(LOGFILE);
  output.logBlank(VERBOSE);

// sum up values of auxiliary variable
  for (int nthread=0; nthread<nthreads; nthread++)
  {
    int n_elmn(0);
    for (int m = 0; m <= jmax; m++)
    {
      for (int l = m; l <= jmax ; l += 2)
      {
        if (l%2 == 1) l++;
        if (l < 2) continue;
        int nmax = (jmax-l+2)/2;
        int l_index = l/2-1;
        for (int n = 1; n <= nmax; n++)
        {
          elmn[l_index][l+m][n] += omp_elmn[nthread*num_elmn+n_elmn];
          n_elmn++;
        } // n loop
      } // l loop
    } // m loop
  }

  floatType sign = ONE;

  output.logElapsedTime(DEBUG);
  output.logBlank(DEBUG);
  for (unsigned l_index = 0; l_index < elmn.size(); l_index++)
  {
    unsigned j = 2*l_index+2;
    for (int m_index = 0; m_index <= j; m_index++)
    {
      sign = ONE;
      if (m_index%2==1) sign = -ONE;
      for (unsigned n_index = 1; n_index < elmn[l_index][m_index].size(); n_index++)
      {
        elmn[l_index][j-m_index][n_index] = sign*std::conj(elmn[l_index][j+m_index][n_index]);
      }
    }
  }
  output.logBlank(VERBOSE);

  return elmn;
}

}//phaser
