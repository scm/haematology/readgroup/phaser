//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Minimizer.h>
#include <phaser/lib/symm_eigen.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <cmath>

namespace phaser {


void Minimizer::run(RefineSAD& target,ProtocolSAD protocol,Output& output)
{
  protocolPtr cPtr(new ProtocolSAD(protocol));
  af::shared<protocolPtr> p;
  p.push_back(cPtr);
  run(target,p,output);
}


void Minimizer::run(RefineBase& target,af::shared<protocolPtr> protocol,Output& output,bool clean_up,bool study_params)
{
  //this check is also done on Input
  if (!protocol.size()) PhaserError(FATAL,"No protocol for refinement");

  output.logTab(1,TESTING,"Values of parameters will be reported at end of each cycle");
  output.logBlank(TESTING);

  // Main loop
  bool calcNewHessian(false),needNewHessian(false);
  int ncyc_since(0);

  // Allocate memory for variables needed by the local minimizers
  floatType f(0),firstLogLike(0),oldLogLike(0),requiredGain(0);

  bool some_refinement(false);
  for (unsigned big_cyc = 0 ; big_cyc < protocol.size(); big_cyc++)
    if (!protocol[big_cyc]->is_off()) some_refinement = true;
  if (!some_refinement)
  {
    output.logTab(1,LOGFILE,"No refinement of parameters");
    output.logBlank(LOGFILE);
  }

  //for bfgs
  TNT::Vector<floatType> x_old;
  TNT::Vector<floatType> g_old;
  TNT::Fortran_Matrix<floatType> h_old;

  if (some_refinement)
  {
  target.logInitial(VERBOSE,output); //initial parameters, numerical stability targetFn==gradientFn
  for (unsigned big_cyc = 0 ; big_cyc < protocol.size(); big_cyc++)
  {
    output.logUnderLine(TESTING,"MACROCYCLE #" + itos(big_cyc+1) + " OF " + itos(protocol.size()));
    target.setProtocol(protocol[big_cyc],output);
    target.setUp();
    if (!protocol[big_cyc]->getNCYC())
    {
      output.logTab(1,LOGFILE,"No cycles of refinement this macrocycle");
      output.logBlank(LOGFILE);
      continue; //skip rest of code in loop
    }
    else if (!target.numRefinePars())
    {
      output.logTab(1,LOGFILE,"No parameters to refine this macrocycle");
      output.logBlank(LOGFILE);
      continue; //skip rest of code in loop
    }
    else
    {
      target.logProtocolPars(LOGFILE,output);
    }

    target.rejectOutliers(LOGFILE,output);

    output.logEllipsisStart(LOGFILE,"Performing Optimization");
    bool iteration_limit(false);
    int  last_iteration(0);
    for (unsigned small_cyc = 0; small_cyc < protocol[big_cyc]->getNCYC(); small_cyc++)
    {
      output.logUnderLine(TESTING,"MACROCYCLE #" + itos(big_cyc+1) + ": CYCLE #" + itos(small_cyc+1));
      last_iteration++;
      //store old value
      oldLogLike = f;
      //calculate new value
      f = target.targetFn();
      //Store value of refined parameters from start of this loop
      TNT::Vector<floatType> xbefore(target.numRefinePars());
      xbefore = target.getRefinePars();
      xbefore = target.reparRefinePars(xbefore);

      //if this is first call of the function, initialize reference function values
      if (!small_cyc) firstLogLike = oldLogLike = f;
      if (!small_cyc)
      {
        output.logBlank(VERBOSE);
        output.logUnderLine(VERBOSE,"Parameters before starting refinement");
        target.logCurrent(VERBOSE,output);
        output.logTab(1,VERBOSE,"Optimization statistics macrocycle #" + itos(big_cyc+1));
        output.logTabPrintf(1,VERBOSE,"Cycle  %-15s %-18s %-18s\n","end-this-cycle","change-from-start","change-from-last");
        if (std::fabs(firstLogLike) > 10e-3)
          output.logTabPrintf(1,VERBOSE,"start  %13.3f %13.3s %18.3s\n",-firstLogLike,"","");
        else
          output.logTabPrintf(1,VERBOSE,"start  %13.3e %13.3s %18.3s\n",-firstLogLike,"","");
      }
      //determine gain required not to converge in this cycle
      const floatType EPS(1.0e-10);
      const floatType FTOL(1.0e-6);
      const floatType TWO(2.0);
      // Convergence test based on numerical precision
      requiredGain = FTOL*(fabs(f)+fabs(oldLogLike)+EPS)/TWO;

      // Call the minimizer, return the new function value, parameter shifts implicit

      bool tooSmallShift(false);
      if (protocol[big_cyc]->getMINIMIZER() == "NEWTON")
      {
        f = newton(target,output,requiredGain,tooSmallShift);
      }
      else if (protocol[big_cyc]->getMINIMIZER() == "BFGS")
      {
        if (!small_cyc) calcNewHessian = true;
        f = bfgs(target,output,requiredGain,calcNewHessian,needNewHessian,tooSmallShift,x_old,g_old,h_old);
        if (calcNewHessian)
        {
          ncyc_since = 0;
          calcNewHessian = false;
        }
      }
      else if (protocol[big_cyc]->getMINIMIZER() == "DESCENT")
      {
        f = descent(target,output,requiredGain,tooSmallShift);
      }

      target.logCurrent(TESTING,output); //current parameters

  //  Print out information detailing the minimization progress
      if (std::fabs(firstLogLike) > 10e-3)
      output.logTabPrintf(1,VERBOSE,"#%-5d %13.3f %13.3f %18.3f\n",small_cyc+1,-f,firstLogLike-f,oldLogLike-f);
      else
      output.logTabPrintf(1,VERBOSE,"#%-5d %13.3e %13.3e %18.3e\n",small_cyc+1,-f,firstLogLike-f,oldLogLike-f);
      // See whether parameters have moved
      TNT::Vector<floatType> xafter(target.numRefinePars());
      xafter = target.getRefinePars();
      xafter = target.reparRefinePars(xafter);

      if ( tooSmallShift || (oldLogLike-f < requiredGain) )
      {
        if (needNewHessian && ncyc_since)
        {
          output.logTab(1,TESTING,"Recalculate Hessian and carry on");
          calcNewHessian = true;
          needNewHessian = false;
        }
        /* else if (protocol[big_cyc]->getMINIMIZER() == "NEWTON" &&
                 oldLogLike-f >= requiredGain)
        {
          output.logTab(1,TESTING,"Carry on with Newton until gain is small");
        } */
        else
        {
          output.logBlank(VERBOSE);
          output.logTab(1,VERBOSE,"---CONVERGENCE OF MACROCYCLE---");
          output.logBlank(VERBOSE);
          break;
        }
      }
      if (small_cyc == protocol[big_cyc]->getNCYC()-1)
      {
        output.logBlank(VERBOSE);
        output.logTab(1,VERBOSE,"---ITERATION LIMIT OF MACROCYCLE---");
        output.logBlank(VERBOSE);
        iteration_limit=true;
        break;
      }
      ncyc_since++;
      output.logBlank(TESTING);
    } //end cycle loop
    output.logEllipsisEnd(LOGFILE);
    iteration_limit ? output.logTab(1,LOGFILE,"--- Iteration limit reached at cycle " + itos(protocol[big_cyc]->getNCYC()) + " ---"):
                      output.logTab(1,LOGFILE,"--- Convergence before iteration limit (" + itos(protocol[big_cyc]->getNCYC()) +") at cycle " + itos(last_iteration) + " ---");
    output.logTab(1,LOGFILE,"Start-this-macrocycle End-this-macrocycle Change-this-macrocycle");
    output.logTabPrintf(1,LOGFILE,"%13.3f %21.3f %21.3f\n",-firstLogLike,-f,firstLogLike-f);
    output.logBlank(LOGFILE);
    target.logCurrent(LOGFILE,output); //current parameters

    // study behaviour of function, gradient and curvature as parameters varied around minimum
    // using mathematica file studyParams_short.nb
    if (study_params || (getenv("PHASER_STUDY_PARAMS") != NULL)) target.studyParams(output);

    if (clean_up) target.cleanUp(VERBOSE,output); //clean up ready for next macrocycle

  } //end macrocycle loop
  if (clean_up) target.finalize(LOGFILE,output); //clean up ready for finish
  target.logFinal(LOGFILE,output); //final parameters
  }
}

floatType Minimizer::descent(RefineBase& target,Output& output,floatType requiredGain, bool& tooSmallShift)
{
  floatType f(0.);
  const floatType ZERO(0.), MinDXoverESD(0.1);
  output.logTab(1,TESTING,"== STEEPEST DESCENT ==");

  //get initial parameter values  x
  TNT::Vector<floatType> unrepar_x = target.getRefinePars();
  TNT::Vector<floatType> x = target.reparRefinePars(unrepar_x);

  // Compute gradient.
  // If it is zero (pathological case), return current likelihood.
  output.logTab(1,TESTING,"Descent: Gradient");
  output.logEllipsisStart(TESTING,"Descent: Calculating the gradient");
  TNT::Vector<floatType> unrepar_g(target.numRefinePars());
  floatType gradLogLike = target.gradientFn(unrepar_g);
  bool zero_gradient(true);
  for (int i = 0; i<target.numRefinePars(); i++)
    if (unrepar_g[i] != ZERO) zero_gradient = false;
  if (zero_gradient)
  {
    output.logTab(1,TESTING,"Descent: Zero gradient: exiting");
    return gradLogLike;
  }

  TNT::Vector<floatType> g = target.reparGradient(unrepar_x,unrepar_g);
  output.logEllipsisEnd(TESTING);
  output.logBlank(TESTING);
  target.logVector(TESTING,"Descent: Gradient",output,g);
  output.logBlank(TESTING);

  // Scale the gradient by factor proportional to large shifts squared.
  // (Hessian is inversely proportional to scale of parameter squared.)
  // Additional factor of 1/100 approximates gradient over curvature,
  // with the largeShift values chosen by behaviour of function around
  // minimum.
  TNT::Vector<floatType> largeShifts = target.getLargeShifts();
  target.reparLargeShifts(largeShifts);
  TNT::Vector<floatType> gs(target.numRefinePars());
  for (int i =0; i < g.size(); i++) gs[i] = g[i]*0.01*fn::pow2(largeShifts[i]);

  output.logBlank(TESTING);
  target.logVector(TESTING,"Descent: Scaled Gradient",output,g);
  output.logBlank(TESTING);

  // Figure out how far linesearch has to go to shift at least one parameter
  // by minimum shift/esd, using largeShift/10 as rough estimate of esd
  floatType requiredShift(0.);
  for (int i = 0; i < g.size(); i++)
    requiredShift = std::max(requiredShift,fabs(gs[i])/(largeShifts[i]/10.));
  requiredShift = MinDXoverESD/requiredShift;

  f = LineSearch(target,output,x,gradLogLike,g,gs,1.,requiredGain,requiredShift,tooSmallShift);

  return f;
}

floatType Minimizer::bfgs(RefineBase& target,Output& output,floatType requiredGain,
           bool& calcNewHessian,bool& needNewHessian,bool& tooSmallShift,TNT::Vector<floatType>& x_old,
           TNT::Vector<floatType>& g_old,TNT::Fortran_Matrix<floatType>& h_old)
{
  floatType f(0);
  const floatType ZERO(0.), MinDXoverESD(0.1);
  output.logTab(1,TESTING,"== BFGS ==");

  if (calcNewHessian && (x_old.size() != target.numRefinePars()))
  {
    // Resize if the number of parameters has changed
    x_old.newsize(target.numRefinePars());
    g_old.newsize(target.numRefinePars());
    h_old.newsize(target.numRefinePars(),target.numRefinePars());
  }

  //get initial parameter values x
  TNT::Vector<floatType> unrepar_x = target.getRefinePars();
  TNT::Vector<floatType> x = target.reparRefinePars(unrepar_x);

  // Compute gradient.
  // If gradient is zero (pathological case), return current likelihood.
  output.logTab(1,TESTING,"BFGS: Gradient");
  output.logEllipsisStart(TESTING,"BFGS: Calculating the gradient");
  TNT::Vector<floatType> unrepar_g(target.numRefinePars());
  floatType gradLogLike = target.gradientFn(unrepar_g);
  bool zero_gradient(true);
  for (int i = 0; i<target.numRefinePars(); i++)
    if (unrepar_g[i] != ZERO) zero_gradient = false;
  if (zero_gradient)
  {
    output.logTab(1,TESTING,"BFGS: Zero gradient: exiting");
    calcNewHessian = false;
    needNewHessian = false;
    return gradLogLike;
  }

  TNT::Vector<floatType> g = target.reparGradient(unrepar_x,unrepar_g);
  output.logEllipsisEnd(TESTING);
  output.logBlank(TESTING);
  target.logVector(TESTING,"BFGS: Gradient",output,g);
  output.logBlank(TESTING);

  if (calcNewHessian)
  {
    output.logTab(1,TESTING,"== Newton Step ==");
    output.logBlank(TESTING);

    bool is_diagonal;
    target.hessianFn(h_old,is_diagonal);
    target.reparHessian(unrepar_x,unrepar_g,h_old);
    target.logHessian(TESTING,output,h_old);
    needNewHessian = target.fixHessian(h_old);
    if (needNewHessian)
    {
      output.logTab(1,TESTING,"BFGS::Newton: Fixed curvature(s) below expected minimum in Hessian");
      target.logHessian(TESTING,output,h_old);
    }

    // Set up to repeat perturbed Newton step if insufficient progress
    int ntry(0);
    floatType hiiRMS(ZERO);
    TNT::Vector<floatType> hii(target.numRefinePars());
    for (int i = 0; i < g.size(); i++)
    {
      hii[i] = h_old(i+1,i+1);
      hiiRMS += fn::pow2(hii[i]);
    }
    if (g.size()) hiiRMS = std::sqrt(hiiRMS/g.size());
    int filtered_last(0);
    f = gradLogLike;
    x_old = x;
    while (ntry < 3 && gradLogLike-f < requiredGain && filtered_last < target.numRefinePars())
    {
      ntry++;
      if (is_diagonal)
      {
        for (int i = 0; i < g.size(); i++)
          h_old(i+1,i+1) = 1/(hii[i]+(ntry-1)*hiiRMS); // downweight low curvature later
      }
      else
      {
        //  Scale Hessian by pre- and post-multiplication by diagonal matrix to
        //  have unit diagonal prior to computing pseudoinverse. Scale resulting
        //  pseudoinverse again to put back on original scale.
        TNT::Fortran_Matrix<floatType> h(target.numRefinePars(),target.numRefinePars());
        TNT::Vector<floatType> hscale(target.numRefinePars());
        for (int i = 0; i < g.size(); i++)
          hscale[i] = 1/sqrt(h_old(i+1,i+1));
        for (int i = 0; i < g.size(); i++)
          for (int j = 0; j < g.size(); j++)
            h(i+1,j+1) = hscale[i]*h_old(i+1,j+1)*hscale[j];
        int min_to_filter(0);
        if (ntry > 1) // First attempt did not minimize.  Perturb Hessian and try again.
        {
          min_to_filter = filtered_last + std::max(1,target.numRefinePars()/10);
          if (min_to_filter >= target.numRefinePars())
            min_to_filter = filtered_last + 1;
//Phil Evan's bugfix below: If just 2 parameters, and it decides that filtering out just one isn't good enough, it then tries to filter out 2/2 parameters.
          if (min_to_filter >= target.numRefinePars())
            min_to_filter = target.numRefinePars() - 1; // force less than number of parameters
        }
        int filtered(0);
        h_old = SymmetricPseudoinverse<floatType>(h,filtered,false,min_to_filter).getInv();
        filtered_last = filtered;
        if (filtered) output.logTabPrintf(1,TESTING,"Filtered %i small eigenvectors\n",filtered);
        for (int i = 0; i < g.size(); i++)
          for (int j = 0; j < g.size(); j++)
            h_old(i+1,j+1) = hscale[i]*h_old(i+1,j+1)*hscale[j];
        output.logTab(1,TESTING,"Pseudoinverse Hessian");
        target.logHessian(TESTING,output,h_old);
      }

      TNT::Vector<floatType> gs = h_old*g;

      target.logVector(TESTING,"BFGS::Newton: Scaled Gradient",output,gs);
      output.logBlank(TESTING);

      // Figure out how far linesearch has to go to shift at least one parameter
      // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
      floatType requiredShift(0.);
      for (int i = 0; i < g.size(); i++)
      {
        if (h_old(i+1,i+1) > 0)
          requiredShift = std::max(requiredShift,fabs(gs[i])/std::sqrt(h_old(i+1,i+1)));
      }
      if (requiredShift > 0)
        requiredShift = MinDXoverESD/requiredShift;

      f = LineSearch(target,output,x,gradLogLike,g,gs,1.,requiredGain,requiredShift,tooSmallShift);

      //LineSearch sets scaled gradient to zero on boundaries.
      //Do same to gradient before BFGS update.
      for (int i = 0; i < g.size(); i++) if (gs[i] == 0) g[i] = 0;
      g_old = g;
    }
  }
  else
  {
    output.logTab(1,TESTING,"== BFGS Step ==");
    TNT::Vector<floatType> dx(target.numRefinePars());
    TNT::Vector<floatType> dg(target.numRefinePars());
    TNT::Vector<floatType> Hdg(target.numRefinePars());
    TNT::Vector<floatType> u(target.numRefinePars());
    dg  = g-g_old;
    dx  = x-x_old;
    Hdg = h_old*dg;
    target.logVector(TESTING,"BFGS: dg",output,dg);
    target.logVector(TESTING,"BFGS: dx",output,dx);
    target.logVector(TESTING,"BFGS: Hdg",output,Hdg);
    floatType dx_dot_dg(0);
    floatType dg_dot_Hdg(0);
    for (int i = 0; i < g.size(); i++) dx_dot_dg += dx[i]*dg[i];
    for (int i = 0; i < g.size(); i++) dg_dot_Hdg += dg[i]*Hdg[i];
    output.logTab(1,TESTING,"TESTING: dx_dot_dg, dg_dot_Hdg: " + dtos(dx_dot_dg) + "  " + dtos(dg_dot_Hdg));
    if (dx_dot_dg <= 0)  // Shouldn't happen, but we're cutting corners on line search convergence
    {                    // i.e. Wolfe condition considering new gradient
      output.logTab(1,TESTING,"BFGS: dx_dot_dg <= 0: restart with new Hessian");
      needNewHessian = true;
      return gradLogLike;
    }
    PHASER_ASSERT(dg_dot_Hdg);
    for (int i = 0; i < g.size(); i++)
      u[i] = dx[i]/dx_dot_dg - Hdg[i]/dg_dot_Hdg;
    for (int i = 0; i < g.size(); i++)
      for (int j = 0; j < g.size(); j++)
        h_old(i+1,j+1) += dx[i]*dx[j]/dx_dot_dg - Hdg[i]*Hdg[j]/dg_dot_Hdg + dg_dot_Hdg*u[i]*u[j];

    output.logBlank(TESTING);
    target.logHessian(TESTING,output,h_old);
    output.logBlank(TESTING);

    g_old = g;
    x_old = x;
    TNT::Vector<floatType> gs = h_old*g;

    target.logVector(TESTING,"BFGS: Scaled Gradient",output,gs);
    output.logBlank(TESTING);

    // Figure out how far linesearch has to go to shift at least one parameter
    // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
    floatType requiredShift(0.);
    for (int i = 0; i < g.size(); i++)
    {
      if (h_old(i+1,i+1) > 0)
        requiredShift = std::max(requiredShift,fabs(gs[i])/std::sqrt(h_old(i+1,i+1)));
    }
    if (requiredShift > 0)
      requiredShift = MinDXoverESD/requiredShift;

    f = LineSearch(target,output,x,gradLogLike,g,gs,1,requiredGain,requiredShift,tooSmallShift);

    // Check for positive grad
    floatType grad(-dot_prod(g,gs));
    if (grad >= 0.) needNewHessian = true; // Try again with new Hessian

  }
  return f;
}

floatType Minimizer::newton(RefineBase& target,Output& output,floatType requiredGain,bool& tooSmallShift)
{
  floatType f(0);
  const floatType ZERO(0.), MinDXoverESD(0.1);
  output.logTab(1,TESTING,"==NEWTON==");

  //get initial parameter values x
  TNT::Vector<floatType> unrepar_x = target.getRefinePars();
  TNT::Vector<floatType> x = target.reparRefinePars(unrepar_x);

  // Compute gradient.
  // If gradient is zero (pathological case), return current likelihood.
  output.logTab(1,TESTING,"Newton: Gradient");
  output.logEllipsisStart(TESTING,"Newton: Calculating the gradient");
  TNT::Vector<floatType> unrepar_g(target.numRefinePars());
  floatType gradLogLike = target.gradientFn(unrepar_g);
  bool zero_gradient(true);
  for (int i = 0; i<target.numRefinePars(); i++)
    if (unrepar_g[i] != ZERO) zero_gradient = false;
  if (zero_gradient)
  {
    output.logTab(1,TESTING,"Newton: Zero gradient: exiting");
    return gradLogLike;
  }

  TNT::Vector<floatType> g = target.reparGradient(unrepar_x,unrepar_g);
  output.logEllipsisEnd(TESTING);
  output.logBlank(TESTING);
  target.logVector(TESTING,"Newton: Gradient",output,g);
  output.logBlank(TESTING);

  // compute hessian
  bool is_diagonal;
  TNT::Fortran_Matrix<floatType> h;
  target.hessianFn(h,is_diagonal);
  target.reparHessian(unrepar_x,unrepar_g,h);
  target.fixHessian(h);
  output.logTab(1,TESTING,"Newton: BEFORE INVERSE");
  target.logHessian(TESTING,output,h);

  // invert hessian
  if (is_diagonal)
  {
    for (int i = 1; i <= g.size(); i++)
      h(i,i) = 1/h(i,i);
  }
  else
  {
// Scale Hessian by pre- and post-multiplication by diagonal matrix to
// have unit diagonal prior to computing pseudoinverse.  Scale resulting
// pseudoinverse again to put back on original scale.
    TNT::Vector<floatType> hscale(target.numRefinePars());
    for (int i = 0; i < g.size(); i++)
      hscale[i] = 1/sqrt(h(i+1,i+1));
    for (int i = 0; i < g.size(); i++)
      for (int j = 0; j < g.size(); j++)
        h(i+1,j+1) = hscale[i]*h(i+1,j+1)*hscale[j];
    int filtered(0);
    h = SymmetricPseudoinverse<floatType>(h,filtered).getInv();
    if (filtered) output.logTabPrintf(1,TESTING,"Newton: Filtered %i small eigenvectors\n",filtered);
    for (int i = 0; i < g.size(); i++)
      for (int j = 0; j < g.size(); j++)
        h(i+1,j+1) = hscale[i]*h(i+1,j+1)*hscale[j];
    output.logTab(1,TESTING,"Newton: AFTER COMPUTING PSEUDOINVERSE");
    target.logHessian(TESTING,output,h);
  }

  // this is the scaled gradient (this SHOULD be exact near the minimum)
  TNT::Vector<floatType> gs = h*g;

  target.logVector(TESTING,"Newton: Parameters",output,x);
  target.logVector(TESTING,"Newton: Scaled Gradient",output,gs);
  output.logBlank(TESTING);

  // Figure out how far linesearch has to go to shift at least one parameter
  // by minimum shift/esd, using inverse Hessian to estimate covariance matrix
  floatType requiredShift(0.);
  for (int i = 0; i < g.size(); i++)
  {
    if (h(i+1,i+1) > 0)
      requiredShift = std::max(requiredShift,fabs(gs[i])/std::sqrt(h(i+1,i+1)));
  }
  if (requiredShift > 0)
    requiredShift = MinDXoverESD/requiredShift;

  f = LineSearch(target,output,x,gradLogLike,g,gs,1.,requiredGain,requiredShift,tooSmallShift);
  return f;
}

void Minimizer::ShiftX(floatType a, RefineBase& target, TNT::Vector<floatType> &x,
      TNT::Vector<floatType> &oldx, TNT::Vector<floatType> &gs, TNT::Vector<floatType> &dist,
      TNT::Vector<floatType> &largeShifts)
{
  target.CalcDampedShift(a,x,oldx,gs,dist,largeShifts);
  TNT::Vector<floatType> unrepar_x = target.reparRefineParsInv(x);
  target.applyShift(unrepar_x);
}

floatType Minimizer::ShiftScore(floatType a, RefineBase& target, TNT::Vector<floatType> &x,
     TNT::Vector<floatType> &oldx, TNT::Vector<floatType> &gs,TNT::Vector<floatType> &dist,
     TNT::Vector<floatType> &largeShifts, Output* poutput, bool bcount)
{
  ShiftX(a,target,x,oldx,gs,dist,largeShifts);
  floatType f = target.targetFn();
  if (bcount)
    mincount++;

  if (poutput)
    poutput->logTabPrintf(1,TESTING,"f(%9.6f) = %10.6f\n", a, f);

  return f;
}


floatType Minimizer::LineSearch(RefineBase& target,Output& output,TNT::Vector<floatType> oldx,
   floatType f, TNT::Vector<floatType> g, TNT::Vector<floatType>& gs, floatType starting_distance,
   floatType requiredGain, floatType requiredShift, bool& tooSmallShift)
{
  tooSmallShift = false;
  mincount = 0;

  TNT::Vector<floatType> x(target.numRefinePars());
  TNT::Vector<floatType> unrepar_x(target.numRefinePars());

  const floatType ZERO(0.0),HALF(0.5),ONE(1.0),TWO(2.0),FIVE(5.0);
  const floatType GOLDEN((std::sqrt(FIVE)+ONE)/TWO);
  const floatType GOLDFRAC((GOLDEN-ONE)/GOLDEN);
  const floatType WOLFEC1(1.E-4); // 1.E-4 suggested in Nocedal & Wright
  const floatType DTOL(1.e-5);

  // Check grad of function in direction of line search
  floatType grad(-dot_prod(g,gs));
  if (grad >= ZERO)
  {
    output.logTab(1,TESTING,"Grad of target in search direction >= 0: grad = " +dtos(grad));
    tooSmallShift = true;
    return f;
  }

  output.logTab(1,TESTING,"Determining Stepsize");
  output.logTabPrintf(1,TESTING,"f(  Start  ) = %12.6f\n", f);
  output.logTabPrintf(1,TESTING,"|");

  // Put distance each parameter can move before it hits a bound in dist array. If all parameters are
  // bounded in search direction, need to know maxDist we can move before hitting ultimate bound.
  TNT::Vector<floatType> dist(target.numRefinePars());
  floatType maxDist = target.getMaxDist(oldx,gs,dist);
  if (starting_distance > maxDist)
  {
    if (maxDist > ZERO)
    {
      output.logTab(1,TESTING,"To avoid exceeding bounds for all parameters, starting distance reduced from "
           + dtos(starting_distance) + " to " + dtos(maxDist));
      starting_distance = maxDist;
    }
    else
    {
      output.logTab(1,TESTING,"All parameters have hit a bound -- end search");
      tooSmallShift = true;
      return f;
    }
  }

  // Get largeShifts for damping in ShiftX
  TNT::Vector<floatType> largeShifts = target.getLargeShifts();
  target.reparLargeShifts(largeShifts);

  floatType fk(0),flo(0),fhi(0),dk(0),dlo(0),dhi(0);
  PHASER_ASSERT(gs.size() == oldx.size());

  //Sample first test point
  output.logTab(1,TESTING,"Unit distance = " + dtos(starting_distance));

  dk  = starting_distance;
  fk = ShiftScore(dk, target, x, oldx, gs, dist, largeShifts, &output);

  const floatType WOLFEFRAC(HALF); // 0.5 slightly better in tests of 0,0.25,0.5,1
  if ((dk >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
      && (fk < f+WOLFEC1*dk*grad)) // Wolfe condition for first step
  {
    output.logTab(1,TESTING,"Satisfied Wolfe condition for first step");
    output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -fk, dk);
    output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");
    if (dk < requiredShift) tooSmallShift = true;
    return fk;
  }

  dlo = ZERO;
  flo = f;

  if (f <= fk) // First step is too big
  {
    while (f <= fk)
    { // Shrink shift until we find a value lower than starting point
      // Fit quadratic to grad and f at 0 and dk, choose its minimum (but not too small)
      if (dk < starting_distance/1.E10) // Give up if shift too small
      {
        unrepar_x = target.reparRefineParsInv(oldx);
        target.applyShift(unrepar_x);  // Reset x before returning
        tooSmallShift = true;
        output.logTab(1,TESTING,"LineSearch: Shift too small");
        f = fk;
        return f;
      }
      fhi = fk;
      dhi = dk;
      floatType denom(TWO*(grad*dk+(f-fk)));
      if (denom < 0)
        dk *= std::min(0.99,std::max(0.1,grad*dk/denom));
      else // unlikely but possible for vanishingly small gradient
        dk *= 0.1;
      fk = ShiftScore(dk, target, x, oldx, gs, dist, largeShifts, &output);
    }
    if ((fk < f+WOLFEC1*dk*grad))
    {
      output.logTab(1,TESTING,"Satisfied Wolfe condition after backtracking");
      output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -fk, dk);
      output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");
      if (dk < requiredShift) tooSmallShift = true;
      return fk;
    }
  }
  else // First step goes down.
  {
    if (dk >= maxDist) // First step already hit ultimate bound
    {
      dhi = maxDist;
      fhi = fk;
    }
    else
    {
      dhi = std::min(maxDist,(ONE+GOLDEN)*dk);
      fhi = ShiftScore(dhi, target, x, oldx, gs, dist, largeShifts, &output);
      if ((fhi <= fk)
          && (dhi >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
          && (fhi < f+WOLFEC1*dhi*grad)) // Wolfe condition for first step
      {
        output.logTab(1,TESTING,"Satisfied Wolfe condition for extended step");
        output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -fhi, dhi);
        output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");
        if (dhi < requiredShift) tooSmallShift = true;
        return fhi;
      }
    }
    if (flo > fk && fk >= fhi) // Still not coming up at second step
    {
      while (fk >= fhi && dhi < maxDist)
      {
        flo = fk;
        dlo = dk;

        fk = fhi;
        dk = dhi;

        dhi = std::min(maxDist,dk + GOLDEN*(dk-dlo));
        fhi = ShiftScore(dhi, target, x, oldx, gs, dist, largeShifts, &output);
        if ((fhi <= fk)
            && (dhi >= WOLFEFRAC) // Significant fraction of (quasi-)Newton shift
            && (fhi < f+WOLFEC1*dhi*grad)) // Wolfe condition for first step
        {
          output.logTab(1,TESTING,"Satisfied Wolfe condition for further extended step");
          output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -fhi, dhi);
          output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");
          if (dhi < requiredShift) tooSmallShift = true;
          return fhi;
        }
      }
      if (dhi >= maxDist && fk >= fhi)
      {
        // Reached boundary without defining bracket.
        // Use finite differences to test if still going down.
        // If so, stop this line search.  Otherwise, we've verified bracket.
        dk = (1.-DTOL)*dhi;
        fk = ShiftScore(dk, target, x, oldx, gs, dist, largeShifts, &output);
        if (fk >= fhi)
        {
          ShiftX(dhi,target,x,oldx,gs,dist,largeShifts);
          output.logTab(1,TESTING,"Reached boundary without defining bracket");
          if (dhi < requiredShift) tooSmallShift = true;
          return fhi;
        }
      }
    }
    if ((dk >= WOLFEFRAC && fk < f+WOLFEC1*dk*grad))
    {
      output.logTab(1,TESTING,"Satisfied Wolfe condition for bigger than initial step");
      output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -fk, dk);
      output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");

      ShiftX(dk,target,x,oldx,gs,dist,largeShifts);  // Set to current best before returning
      if (dk < requiredShift) tooSmallShift = true;
      return fk;
    }
  }

  //===================================  End of bracketing

  output.logTab(1,TESTING,"Bracketing took " + itos(mincount) + " function evaluations");
  output.logTab(1,TESTING,"xlow= " + dtos(dlo) + ", xhigh= " + dtos(dhi));
  floatType dmid(dk);
  floatType fmid(fk);

  // BISECTING/INTERPOLATION STARTS HERE (we now know that the value is between dlo and dhi)

  // let tolerance be small but not too close to zero as to ensure our algorithm tries a new value
  // distinctly different from existing one
  const floatType XTOL(0.01);

  floatType a(0), b(0), c(0);
  floatType stepsize = std::min(dhi-dmid,dmid-dlo);
  floatType lastlaststepsize(0), laststepsize(0);
  laststepsize = stepsize*TWO; // permit first step to actually happen
  floatType d1(0),f1(0),d2(0),f2(0);

  output.logTabPrintf(1,TESTING,"Line Search |");
  for (int i=0; i<20; i++) // fall-back upper limit on evaluations in interpolation stage
  {
    if (GetQuadraticCoefficients(flo, fmid, fhi, dlo, dmid, dhi, a, b, c)) // Make sure there will be quadratic with a minimum
    {
      output.logTab(1, TESTING, "Quadratic interpolation used" );
      dk = -b/(TWO*c); // where the derivative of parabolic curve is zero
      dk = std::max(dk,dlo); // remain within high and low interval
      dk = std::min(dk,dhi); // (Paranoia: fmid < an end should be enough)
    }
    else
    {
      output.logTab(1, TESTING, "No quadratic interpolation possible, taking interval midpoint..." );
      c = ZERO;
      dk = dmid;
    }

    lastlaststepsize = laststepsize;
    laststepsize = stepsize;
    stepsize = fabs(dk - dmid);

    output.logTabPrintf(0,TESTING," stepsize= %12.6f, laststepsize= %12.6f,  ",stepsize, laststepsize);
    if (c > ZERO  // i.e. the interval has a local minimum, not a local maximum so proceed.
        && dk-dlo > XTOL*(dhi-dlo) // New minimum is sufficiently far from previous points
        && dhi-dk > XTOL*(dhi-dlo) // to make quadratic convergence probable.
        && fabs(dk-dmid) > XTOL*(dhi-dlo)
        && stepsize <= HALF*lastlaststepsize // Steps are decreasing sufficiently in size
        )
    {
      output.logTab(1,TESTING,"Use quadratic step");
      fk = ShiftScore(dk, target, x, oldx, gs, dist, largeShifts, &output);
    }
    else
    {    // Golden search instead
      dk = (dhi-dmid >= dmid-dlo) ? dmid + GOLDFRAC*(dhi-dmid) : dmid - GOLDFRAC*(dmid-dlo);
      output.logTab(1,TESTING,"Use golden search step");
      fk = ShiftScore(dk, target, x, oldx, gs, dist, largeShifts, &output);
    }
    if (dk < dmid) // Get data sorted by size of shift
    {
      d1 = dk;
      f1 = fk;
      d2 = dmid;
      f2 = fmid;
    }
    else
    {
      d1 = dmid;
      f1 = fmid;
      d2 = dk;
      f2 = fk;
    }
    if (fk < fmid) // Keep current best at dmid
    {
      dmid = dk;
      fmid = fk;
    }

    output.logTabPrintf(0,TESTING,"=");

    output.logTabPrintf(0,TESTING,"[%12.6f,%12.6f,%12.6f,%12.6f]\n",dlo,d1,d2,dhi);
    output.logTabPrintf(0,TESTING,"[    ----    ,%12.6f,%12.6f,    ----    ]\n",f1,f2);
    if (f1 < f2)
      output.logTabPrintf(0,TESTING,"          <        ^^^^        >                     \n");
    else
      output.logTabPrintf(0,TESTING,"                       <       ^^^^       >          \n");

    // convergence tests
    if (fmid < f - std::max(requiredGain,-WOLFEC1*dmid*grad))
    {
      output.logTab(1,TESTING,"Stop linesearch: good enough for next cycle");
      break;
    }
    else if ((d2-d1)/d2 < 0.01)
    {
      output.logTab(1,TESTING,"Stop linesearch: fractional change in stepsize < 0.01");
      break;
    }
    // Not converged, so prepare for next loop
    if (f1 < f2)
    {
      dhi = d2;
      fhi = f2;
    }
    else
    {
      dlo = d1;
      flo = f1;
    }
  }
  // converged or limit in steps

  if (f < fmid) // Shouldn't happen, but catch possibility that function didn't improve
  {
    unrepar_x = target.reparRefineParsInv(oldx);
    target.applyShift(unrepar_x);
  }
  else
  {
    ShiftX(dmid,target,x,oldx,gs,dist,largeShifts);  // Make sure best shift has been applied.
    f = fmid;
  }

  output.logBlank(TESTING);
  output.logTab(1,TESTING,"This line search took " + itos(mincount) + " function evaluations");
  output.logBlank(TESTING);
  output.logTabPrintf(1,TESTING,"Final LLG: %12.6f distance: %12.6f \n", -f, dmid);
  output.logTab(1,TESTING,"End distance = " +dtos(dmid));
  output.logTab(1,TESTING,"Prediction ratio = " +dtos(dmid/starting_distance));
  output.logBlank(TESTING);
  if (dmid < requiredShift) tooSmallShift = true;
  return f;
}


// Find minimum from parabolic interpolation. Return true as long as minimum is between xmin and xmax
bool Minimizer::GetQuadraticMinimum(floatType &xmin, floatType x1, floatType y1, floatType x2,
                             floatType y2, floatType x3, floatType y3, floatType xlow, floatType xhigh)
{
  floatType a(0),b(0),c(0);
  GetQuadraticCoefficients(y1, y2, y3, x1, x2, x3, a, b, c);
  xmin = -b/(2.0*c); // where the derivative of parabolic curve is zero

  if (xmin <xlow)
  {
    xmin = xlow;
    return false;
  }

  if (xmin >xhigh)
  {
    xmin = xhigh;
    return false;
  }

  return true;
}

// Given three points in the x-y plane get the coefficients corresponding to
// the parabolic equation y(x) = a + b*x + c*x^2
bool Minimizer::GetQuadraticCoefficients(floatType y1, floatType y2, floatType y3, floatType x1,
                                    floatType x2, floatType x3, floatType& a, floatType& b, floatType& c)
{
  a = b = c = 0.0;

  floatType det = x2*x3*x3 - x3*x2*x2 - x1*(x3*x3 - x2*x2) + x1*x1*(x3 - x2);
  if (fabs(det) < std::numeric_limits<floatType>::epsilon()*3) // too close to zero within machine precision
    return false;

  floatType invdet = 1.0/det;

  //   a = (y1*(x2*x3*x3 - x2*x2*x3) + y2*(x1*x3*x3 - x3*x1*x1) + y3*(x1*x2*x2 - x2*x1*x1))*invdet; // correct but unstable
  b = (y1*(x2*x2 - x3*x3) + y2*(x3*x3 - x1*x1) + y3*(x1*x1 - x2*x2))*invdet;
  c = (y1*(x3 -x2) + y2*(x1 - x3) + y3*(x2 -x1))*invdet;
  a = y1 - (b*x1 +c*x1*x1);

  if (c == 0) return false;
  else return true;
}


}//phaser
