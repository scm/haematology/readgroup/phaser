//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TNCS_EPSILON__
#define __TNCS_EPSILON__
#include <phaser/io/Output.h>
#include <phaser/include/data_moments.h>
#include <phaser/include/ProtocolBase.h>
#include <phaser/src/DataB.h>
#include <phaser/pod/epspod.h>

namespace phaser {

class Epsilon : public DataB
{
  public:
    Epsilon();
    Epsilon(data_refl&,
            data_resharp&,
            data_norm&,
            data_outl&,
            data_tncs&,
            data_bins&,
            data_composition&,
            double=0,double=DEF_LORES);

  private:
    floatType best_likelihood;
    int       best_num;
    dvect3    patt_vector;
    af_float  refinedL;
    af_dvect3 refinedR,refinedT;
    data_moments MOMENTS;

  public:
    epspod PTNCS_NMOL;

  public:
    void findTRA(Output&);
    void findROT(af::shared<protocolPtr>,Output&);
    void logTable(Output&);
    void logEpsn(Output&);
    //get
    data_tncs    getPtNcs() const     { return PTNCS; }
    data_moments getMoments() const   { return MOMENTS; }
    af_float     getEpsFac() const;

  private:
    void initROT(floatType,Output&);
    void initGFN(Output&);
};

} //phaser

#endif
