//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DFACTOR_CALC__
#define __DFACTOR_CALC__
#include <phaser/io/Output.h>
#include <phaser/src/DataB.h>

namespace phaser {

class Dfactor : public DataB
{
  public:
    Dfactor(data_refl&,
            data_resharp&,
            data_norm&,
            data_outl&,
            data_tncs&,
            data_bins&,
            data_composition&,
            double=0,double=DEF_LORES);

  public:
    void      calcDfactor(Output&);
};

} //phaser

#endif
