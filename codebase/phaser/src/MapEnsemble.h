//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MapEnsembleClass__
#define __MapEnsembleClass__
#include <phaser/src/Ensemble.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/src/SigmaN.h>
#include <phaser/io/Output.h>

namespace phaser {

typedef std::map<std::string,Ensemble>::iterator ensIter;

class MapEnsemble
{
  public:
  std::map<std::string,Ensemble> ensemble;
  floatType    MAPHIRES;
  std::string  gyre_modlid;
  bool         debugging;

  public:
    MapEnsemble()
    {
      MAPHIRES = std::numeric_limits<floatType>::max();
      gyre_modlid = "";
      debugging = false;
    }

    MapEnsemble(MapEnsemble* init)
    { //deep copy with pointer initialization
      MAPHIRES = init->MAPHIRES;
      gyre_modlid = init->gyre_modlid;
      ensemble = init->ensemble;
      debugging = false;
    }

    ~MapEnsemble() { }

  void  configure(stringset,
                  map_str_pdb&,
                  data_solpar&,
                  floatType,
                  floatType,
                  SigmaN,
                  data_formfactors&,
                  stringset,
                  Output&,
                  bool=false,
                  double=0,
                  map_str_float1D* OCC=NULL,
                  bool=false);
  void  setPtInterpEV(int);

  void            ensembling_table(outStream,floatType,Output&);
  void            setup_vrms(map_str_float);
  void            setup_cell(map_str_float);
  void            set_ssqr(floatType);
  void            set_gyre_modlid(std::string modlid) { gyre_modlid = modlid; }
  floatType       HiRes() { return MAPHIRES; }
  map_str_float2D get_map_relative_wilsonB();
  std::map<std::string,drms_vrms> get_drms_vrms();
  std::map<std::string,double> get_cell_scale();
  std::set<std::string> set_of_modlids();

  typedef std::map<std::string,Ensemble>::size_type size_type;
  typedef std::map<std::string,Ensemble>::iterator iterator;
  typedef std::map<std::string,Ensemble>::const_iterator const_iterator;
  Ensemble& operator[](std::string& t) { return ensemble[t]; }
  iterator begin() { return ensemble.begin(); }
  iterator end() { return ensemble.end(); }
  const_iterator begin() const { return ensemble.begin(); }
  const_iterator end() const { return ensemble.end(); }
  size_type size() const { return ensemble.size(); }
  iterator find (const std::string& k) { return ensemble.find(k); }
  const_iterator find (const std::string& k) const { return ensemble.find(k); }

  bool has_pdb();
};

}//phaser
#endif
