//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SigmaNClass__
#define __SigmaNClass__
#include <phaser/src/Bin.h>

namespace phaser {

class SigmaN : public Bin
{
  public:
    SigmaN()  { sigmaN.clear(); }
    SigmaN(Bin b,float1D s): Bin(b),sigmaN(s) { }
    ~SigmaN() { }

  float1D sigmaN;
};

} //phaser

#endif
