//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/LlgcMapSites.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

void LlgcMapSites::selectTopSites(outStream where,SpaceGroup sg,UnitCell uc,floatType DIST,int numAtoms,bool TOP,Output& output)
{
  output.logUnderLine(where,"Add Sites Identified from Log-Likelihood Gradient Map(s)");
  std::vector<std::pair<floatType,int> > sorted_list;
  for (int i = 0; i < atoms.size(); i++)
    sorted_list.push_back(std::pair<floatType,int>(atoms[i].ZSCORE,i));
  std::sort(sorted_list.begin(),sorted_list.end());
  std::reverse(sorted_list.begin(),sorted_list.end());
  //now square it so don't have to keep taking sqrt(distance) for comparison
  floatType DISTsqr = fn::pow2(DIST);
  bool1D top_site(sorted_list.size(),false);
  std::multimap<int,int> cluster;
  int natoms(0);
  for (unsigned i = 0; i < sorted_list.size(); i++) //all i
  {
    int j = sorted_list[i].second;
    dvect3 orthRef = uc.doFrac2Orth(atoms[j].SCAT.site);
    for (int i_prev = 0; i_prev < i; i_prev++)
    {
      int j_prev = sorted_list[i_prev].second;
      for (unsigned isym = 0; isym < sg.NSYMM; isym++)
      {
        dvect3 fracPrev = sg.doSymXYZ(isym,atoms[j_prev].SCAT.site);
        dvect3 cellTrans;
        for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
          for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
            for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
            {
              dvect3 fracPrevCell = fracPrev+cellTrans;
              dvect3 orthPrev = uc.doFrac2Orth(fracPrevCell);
              floatType delta = (orthPrev-orthRef)*(orthPrev-orthRef);
              if (delta < DISTsqr)
              {
                cluster.insert(std::pair<int,int>(j_prev,j));
                goto next_i;
              }
            } //cells
      } //symm
    }
    // no hits found,this site must be unique
    top_site[j] = true;
    natoms++;
    next_i: continue; //performs the next iteration of the loop.
  }
  if (TOP)//make sure only one is selected
  {
    bool first_found(false);
    for (unsigned j = 0; j < top_site.size(); j++) //all i
      if (first_found) top_site[j] = false;
      else first_found = top_site[j];
    //paranoia recount
    natoms = 0;
    for (unsigned j = 0; j < top_site.size(); j++)
      if (top_site[j]) natoms++;
  }
  output.logTab(1,where,itos(natoms) + " atom" + std::string(natoms==1?"":"s") + " after pruning");
  if (atoms.size())
  {
    output.logTabPrintf(1,where,"%-3s %6s %6s %6s %12s %4s %7s\n", "#", "X","Y","Z","occupancy","Atom","Z-score");
    for (int a = 0; a < atoms.size(); a++)
    if (top_site[a])
    {
      output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %12.4f %4s %7.1f\n",
      ++numAtoms,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str(),atoms[a].ZSCORE);
      typedef std::multimap<int,int>::iterator I;
      std::pair<I,I> c = cluster.equal_range(a);
      for (I iter=c.first; iter != c.second; iter++)
      {
        int j = iter->second;
        output.logTabPrintf(1,where,"%3s %6.3f %6.3f %6.3f %12.4f %4s %7.1f\n",
        "=",atoms[j].SCAT.site[0],atoms[j].SCAT.site[1],atoms[j].SCAT.site[2],atoms[j].SCAT.occupancy,atoms[j].SCAT.scattering_type.c_str(),atoms[j].ZSCORE);
      }
    }
    output.logBlank(where);
  }
  //erase from end to preserve numbering
  for (int j = atoms.size()-1; j >= 0; j--)
  if (!top_site[j])
  {
    atoms.erase(atoms.begin()+j,atoms.begin()+j+1);
  }
}

}//phaser
