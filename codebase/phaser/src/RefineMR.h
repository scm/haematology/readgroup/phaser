//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineMRClass__
#define __RefineMRClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/ProtocolMR.h>
#include <phaser/include/data_restraint.h>
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/src/DataMR.h>
#include <phaser/src/RefineBase2.h>
#include <phaser/pod/mrgrads.h>

namespace phaser {

class RefineMR : public RefineBase2, public DataMR
{
  private:
    ProtocolMR  protocol;
    bool        FAST_LAST;
    std::string LAST_MODLID;
    int LAST_KNOWN;
    data_restraint  BFAC_REFI;
    data_restraint  DRMS_REFI;
    std::vector<mr_gyre>     GYRE; //for output history, not for refinement
    double     sigma_rotref,sigma_traref;
    bool       use_rotref_restraint,use_traref_restraint;

  public:
    RefineMR()
    {
      FAST_LAST = false;
      BFAC_REFI = data_restraint(false,0);
      DRMS_REFI = data_restraint(false,0);
      LAST_KNOWN = 0;
      LAST_MODLID = "";
    }
    RefineMR(data_restraint&,data_restraint&,DataMR&,Output&);
    ~RefineMR() {}

   //concrete member functions
    floatType targetFn();
    floatType restraint_term();
    floatType gradient_hessian(TNT::Vector<floatType>&,bool);
    floatType gradientFn(TNT::Vector<floatType>&);
    floatType hessianFn(TNT::Fortran_Matrix<floatType>&,bool&);
    void   applyShift(TNT::Vector<floatType>&);
    std::vector<reparams>  getRepar();
    std::vector<bounds>  getUpperBounds();
    std::vector<bounds>  getLowerBounds();
    void   logCurrent(outStream,Output&);
    void   logInitial(outStream,Output&);
    void   logFinal(outStream,Output&);
    void   cleanUp(outStream,Output&);
    void   setUp();

    TNT::Vector<floatType>      getRefinePars();
    TNT::Vector<floatType>      getLargeShifts();
    bool1D       getRefineMask(protocolPtr);
    std::string  whatAmI(int&);

    map_str_float    getDRMS() { return models_drms; }
    map_str_float    getCELL() { return ensemble->has_pdb() ? map_str_float() : models_cell; }
    double           getDATACELL() { return ensemble->has_pdb() ? 1.0/models_cell.begin()->second : 1; }
    bool             refine_datacell();
    std::vector<mr_gyre>&  getGyre() { return GYRE; }

    floatType getMaxDistSpecial(TNT::Vector<floatType>&,TNT::Vector<floatType>&,bool1D&,TNT::Vector<floatType>&,floatType&);

    void   fast_last_known_setUp();
    void   fast_last_applyShift();

    double Rice_grad_hess(int,bool,cmplx1D&,mrfcgrads&,mrfcgrads&,mrllgrads&,mrllgrads&);
    double MLHL_grad_hess(int,bool,cmplx1D&,mrfcgrads&,mrfcgrads&,mrllgrads&,mrllgrads&);

};

} //phaser

#endif
