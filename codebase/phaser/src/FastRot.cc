//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/SiteListAng.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/sphericalY.h>
#include <phaser/lib/rotationgroup.h>
#include <phaser/io/Errors.h>
#include <scitbx/array_family/accessors/c_grid_padded_periodic.h>
#include <ctime>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

// A bug in GCC 4.2 compels us to introduce this helper function, the contents of which all used to reside in get_FRF()
scitbx::fftpack::real_to_complex_3d<floatType>
SiteListAng::DoRfftStuff(cmplx3D& clmn, floatType bgrid_sampling, int bmax,
                                        cctbx::maptbx::structure_factors::to_map<floatType> &to_map,lnfactorial& lnfac)
{
  floatType betaDEG = static_cast<floatType>(bgrid_sampling);
  floatType betaRAD =  scitbx::deg_as_rad(betaDEG);
  floatType cosbeta = cos(betaRAD/2.);
 // floatType sinbeta = sin(betaRAD/2.);
  af::shared<miller::index<int> > pseudo_miller;
  af_cmplx  pseudo_sf;

 // Calculate the rotated Smm'(beta) from the Clmm' and Dlmm'(beta) by summing over the l values
 // and storing it in the FFT array.

  cmplxType rot(0);
  int l(0);
  floatType beta = 2*acos(cosbeta);
  cmplxType zero(0,0);
  int max_index = static_cast<int>(clmn.size()); //this is lmax/2

  for (int l_index = 0; l_index < max_index; l_index++) {
    l = 2*l_index+2;
    float2D table = djmn_recursive_table(l,beta,lnfac);
    for (int m1_index = 0; m1_index < 2*l+1; m1_index++) {
      for (int m2_index = 0; m2_index < 2*l+1; m2_index++) {
        if ((l_index < clmn.size()) &&
          (m1_index < clmn[l_index].size()) &&
          (m2_index < clmn[l_index][m1_index].size()) &&
          clmn[l_index][m1_index][m2_index] != zero) {
           int m1l = m1_index-l;
           int m2l = m2_index-l;
          rot = clmn[l_index][m1_index][m2_index]*table[m1_index][m2_index];
           // This MUST be exactly one asymmetric unit!
           if (m2l > 0 || (m2l == 0 && m1l >= 0)) {
             pseudo_miller.push_back(miller::index<>(0,m1l,m2l));
             pseudo_sf.push_back(rot);
          }
        }
      }
    }
  }
  PHASER_ASSERT(pseudo_sf.size());
  cctbx::sgtbx::space_group cctbxSG("P 1"); // Hall symbol
  int lmax = 2*static_cast<int>(clmn.size())+2;
  int min_grid = 2* std::max(bmax, lmax); //number of steps
  bool anomalous_flag = false;int max_prime = 5;
  int mandatory_factor = 1;
  int amax = scitbx::fftpack::adjust_gridding(
    min_grid,
    max_prime,
    mandatory_factor);
  int cmax = amax;
  scitbx::fftpack::real_to_complex_3d<floatType> rfft(ivect3(1,amax,cmax));

  cctbx::af::flex_grid<> map_grid(scitbx::af::adapt(rfft.n_complex()));
  bool conjugate_flag = true;
  to_map = cctbx::maptbx::structure_factors::to_map<floatType>(
    cctbxSG,
    anomalous_flag,
    pseudo_miller.const_ref(),
    pseudo_sf.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag);

  rfft.backward(scitbx::af::ref<std::complex<floatType>,
                scitbx::af::c_grid<3> >(to_map.complex_map().begin(),
                                        scitbx::af::c_grid<3>(to_map.complex_map().accessor().all())));
  return rfft;
}

void SiteListAng::get_FRF(cmplx3D& clmn, Output& output, floatType grid_sampling)
{
  PHASER_ASSERT(grid_sampling);
  int bmax = static_cast<int>(ceil(180.0/grid_sampling));
  PHASER_ASSERT(bmax);
  output.logTab(1,DEBUG,"Evaluating FRF for beta between 0 and " + dtos(bmax*grid_sampling) + " degrees");
  output.logTab(1,DEBUG,"Angle resolution requires "+itos(bmax)+" steps");

// claim necessary memory in advance to avoid vector's greedy memory grabber
  int1D start_b = allocate_memory(bmax,grid_sampling,output);
  int nthreads = 1;

//the b angles have different number of associated alpha,gamma samplings, which means that the
//parallel for loops do not have equal numbers of angles on each thread. Randomize to even it out.
  int1D random_b(bmax);
  for (int b = 0; b < bmax; b++) random_b[b] = b;
  random_shuffle(random_b.begin(),random_b.end());

#pragma omp parallel
  {
    int nthread = 0;
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
    nthread = omp_get_thread_num();
    //if (nthread != 0) output.usePhenixCallback(false);
#endif

#pragma omp single
    {
      if (nthreads > 1)
        output.logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");
      output.logProgressBarStart(LOGFILE,"Clmn Calculation",bmax/nthreads);
    }
    lnfactorial lnfac;

#pragma omp for
    for (int rb = 0; rb < bmax; rb++)
    {
      int b = random_b[rb];
      floatType betaDEG = static_cast<floatType>(grid_sampling*b);
      //floatType betaRAD =  scitbx::deg_as_rad(betaDEG);
      //floatType cosbeta = cos(betaRAD/2.);
      //floatType sinbeta = sin(betaRAD/2.);

      cctbx::maptbx::structure_factors::to_map<floatType> to_map;
      scitbx::fftpack::real_to_complex_3d<floatType> rfft = DoRfftStuff(clmn,grid_sampling*b,bmax,to_map,lnfac);
      scitbx::af::const_ref< floatType, scitbx::af::c_grid_padded_periodic<3> >
      beta_section(
        reinterpret_cast<floatType*>(to_map.complex_map().begin()),
        scitbx::af::c_grid_padded_periodic<3>(
        rfft.m_real(),
        rfft.n_real()));

     // int pmax = static_cast<int>(720.0/grid_sampling*cosbeta);
     // int qmax = static_cast<int>(360.0/grid_sampling*sinbeta);

      four_point_interpolation<floatType> interpolate(beta_section);

      for (int b_ang = start_b[b]; b_ang < start_b[b+1]; b_ang++)
      {
        floatType alpha = siteList[b_ang].euler[0];
        floatType gamma = siteList[b_ang].euler[2];
        siteList[b_ang].value = interpolate(alpha, gamma);
        siteList[b_ang].euler = dvect3(-360.0*alpha,betaDEG,-360.0*gamma);
      }
      if (nthread == 0) output.logProgressBarNext(LOGFILE);
    } // for

  } // pragma omp parallel

  output.logProgressBarEnd(LOGFILE);
  output.logTab(1,VERBOSE,itos(siteList.size())+" grid points sampled.");
  output.logBlank(VERBOSE);

  // Phaser's definition of the Euler angles assumes R_z(gamma)R_y(beta)R_z(alpha)
  // Sakurai uses R_z(alpha)R_y(beta)R_z(gamma)

}

int1D SiteListAng::allocate_memory(int bmax,floatType grid_sampling,Output& output)
{
  output.logTab(1,VERBOSE,"Allocating memory");
  //int requestRotlistSize(0);
  int1D num_beta_section(bmax+1,0);
  //count the numbers in the beta sections, but in the index of the following section,
  //so that the index of the section contains the *starting* index for siteList
  //don't do this in parallel: need to count requestRotlistSize in section order
  siteList.clear();
  for (int b = 0; b < bmax; b++)
  {
    int b_start = b+1;
    floatType betaDEG = static_cast<floatType>(grid_sampling*b);
    floatType betaRAD =  scitbx::deg_as_rad(betaDEG);
    floatType cosbeta = cos(betaRAD/2.);
    floatType sinbeta = sin(betaRAD/2.);

    int pmax = static_cast<int>(720.0/grid_sampling*cosbeta);
    int qmax = static_cast<int>(360.0/grid_sampling*sinbeta);

    if (b==0)
    {
      for (int p=0; p<pmax; p++)
      {
        floatType alpha = (static_cast<floatType>(p)/(pmax));
        floatType gamma = (static_cast<floatType>(p)/(pmax));
        if (alpha>=0.5) continue;
        try {
          angsite alpha_gamma;
          alpha_gamma.euler = dvect3(alpha,betaDEG,gamma);
          siteList.reserve(siteList.size()); //so that there isn't "exponential" grabbing
          siteList.push_back(alpha_gamma);
        }
        catch (std::exception const& err) {
          //this catches the error  St9bad_alloc where it runs out of memory
          throw phaser::PhaserError(phaser::MEMORY,"Memory exhausted during fast rotation function");
        }
      }
    }
    else
    {
      for (int p=0; p<pmax; p++) {
        floatType p_float = static_cast< floatType >( p );
        floatType p_ratio =  p_float / pmax;
        for (int q=0; q<qmax; q++) {
          floatType q_float = static_cast< floatType >( q );
          floatType q_ratio = q_float / qmax;
          floatType alpha = fmod( p_ratio + q_ratio, 1 );
          floatType gamma = ( p_ratio >= q_ratio
                          ? fmod( p_ratio-q_ratio, 1 )
                          : 1.0 - fmod( q_ratio - p_ratio, 1 ) );

       // there are cases where alpha and gamma are repeated because
       //the sum and difference of p/pmax and q/qmax are the same.
          bool repeated_angle(false);
          if (p >= pmax/2) {
            floatType p2_float = static_cast< floatType >( p-pmax/2 ); //repeats when p is +0.5
            floatType p2_ratio =  p2_float / pmax;
            for (int q2 = 0; q2 < qmax; q2++) {
              floatType q2_float = static_cast< floatType >( q2 );
              floatType q2_ratio = q2_float / qmax;
              floatType alpha2 = fmod( p2_ratio+q2_ratio, 1);
              floatType gamma2 = ( p2_ratio >= q2_ratio
                               ? fmod( p2_ratio-q2_ratio, 1 )
                               : 1.0 - fmod( q2_ratio - p2_ratio, 1 ) );
              floatType epsilon(1.0e-06);
              if ((alpha < alpha2+epsilon) && (alpha > alpha2-epsilon) &&
                 (gamma < gamma2+epsilon) &&  (gamma > gamma2-epsilon))
              {
                repeated_angle = true;
                break;
              }
            }
          }
          if (!repeated_angle) {
            try {
              angsite alpha_gamma;
              alpha_gamma.euler = dvect3(alpha,betaDEG,gamma);
              siteList.reserve(siteList.size()); //so that there isn't "exponential" grabbing
              siteList.push_back(alpha_gamma);
            }
            catch (std::exception const& err) {
            //this catches the error  St9bad_alloc where it runs out of memory
              throw phaser::PhaserError(phaser::MEMORY,"Memory exhaused during fast rotation function");
            }
          } //repeated angle
        }
      }
    }
    num_beta_section[b_start] = siteList.size();
  }
  output.logTab(1,DEBUG,"Request for list size " + itos(siteList.size()) + " succeeded");
  return num_beta_section;
}

} //phaser
