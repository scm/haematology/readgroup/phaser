//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/io/Errors.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/rad2phi.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <cctbx/xray/sampled_model_density.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/index_span.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/adptbx.h>
#include <cctbx/miller/expand_to_p1.h>
#include <phaser/src/Gfunction.h>
#include <phaser/lib/xyzRotMatDeg.h>

//-- Things to do with gradient maps

namespace phaser {
void RefineSAD::calcGradCoefMaps(stringset ATOMTYPE,bool ptncs_dbl_sites)
{
  LLGCMAPS.clear();
  for (stringset::iterator iter = ATOMTYPE.begin(); iter != ATOMTYPE.end(); iter++)
  {
    std::string atomtype = *iter;
    af_float FLLG,PHLLG;
    calcGradCoefs(atomtype,ptncs_dbl_sites,FLLG,PHLLG);
    LLGCMAPS.push_back(llgc_coefs(atomtype,FLLG,PHLLG));
  }
}

af_cmplx RefineSAD::calcGradCoefs(std::string LLGC_ATOMTYPE,bool ptncs_dbl_sites,af_float FLLG,af_float PHLLG)
{
  LLGRAD_SF.clear();
  FLLG.clear();
  PHLLG.clear();
  //hack to get code to work for phenix maps
  int t = (atomtype2t.find(LLGC_ATOMTYPE) == atomtype2t.end()) ? 0 : atomtype2t[LLGC_ATOMTYPE];
  if (atomtype2t.find(LLGC_ATOMTYPE) == atomtype2t.end()) PHASER_ASSERT(LLGC_ATOMTYPE == "AX");
  cctbx::sgtbx::space_group SgOps(SpaceGroup::getCctbxSG());

  Gfunction gfun(*this,*this,PTNCS.NMOL);
  if (PTNCS.use_and_present())
  {
    dmat33 ncsRmat(1,0,0,0,1,0,0,0,1);
    for (int dir = 0; dir < 3; dir++)
      ncsRmat = xyzRotMatDeg(dir,PTNCS.ROT.ANGLE[dir])*ncsRmat;
    gfun.initArrays(PTNCS.TRA.VECTOR,ncsRmat,PTNCS.GFUNCTION_RADIUS,miller);
  }

  const cmplxType cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (PTNCS.use_and_present())
    {
      gfun.calcReflTerms(r);
    }
    miller::sym_equiv_indices s_e_i(SgOps,miller[r]);
    int multiplicity = s_e_i.multiplicity(false);
    //if the search is to be done with dbled sites, then the map must be P1
    int max_isym = ptncs_dbl_sites ? multiplicity : std::min(1,multiplicity);
    for (std::size_t isym = 0; isym < max_isym; isym++)
    {
      miller::sym_equiv_index h_eq = s_e_i(isym);
      miller::index<int> h = ptncs_dbl_sites ? h_eq.h() : miller[r];
      if (ptncs_dbl_sites ? cctbx::sgtbx::reciprocal_space::is_in_reference_asu_1b(h_eq.h()) : true)
      {
        cmplxType rLLGRAD_SF = 0;
        floatType rFLLG = 0;
        floatType rPHLLG = rad2phi(phsr[r]);
        if (!bad_data[r] && selected[r])
        {
          floatType sharpen = std::exp(-WilsonB*ssqr[r]/4.);
          //hack to get code to work for phenix maps
          floatType fp =  (LLGC_ATOMTYPE == "AX") ? 0 : fo_plus_fp[r][t]*sharpen;
          floatType fdp = (LLGC_ATOMTYPE == "AX") ? 1 : AtomFdp[t]*sharpen;
          cmplxType fp_plus_fdp(fp,fdp);
          if (PTNCS.use_and_present() && ptncs_dbl_sites)
          {
            floatType theta  = h[0]*PTNCS.TRA.VECTOR[0] +
                               h[1]*PTNCS.TRA.VECTOR[1] +
                               h[2]*PTNCS.TRA.VECTOR[2];
            cmplxType ptncs_scat = cmplxONE + std::exp(TWOPII*theta);
            fp_plus_fdp *= ptncs_scat;
            int jsym(0);// if not found equal to refl_G.size()
            for (jsym = 0; jsym < gfun.refl_G.size(); jsym++)
            {
              miller::index<int> rotMiller_jsym = SpaceGroup::Rotsym(jsym)*miller[r];
              if (rotMiller_jsym == h_eq.h()) break;
              if (-rotMiller_jsym == h_eq.h()) break;
            }
            PHASER_ASSERT(jsym < gfun.refl_G.size());
            fp_plus_fdp *= gfun.refl_G[jsym];
          }
          fp = std::real(fp_plus_fdp);
          fdp = std::imag(fp_plus_fdp);
          if (both[r])
          {
            acentricReflGradSAD2(r); // Note that FHneg is complex conjugate of H-, and dL refers to minus log-likelihood
            rLLGRAD_SF = -cmplxType(fp*(dL_by_dReFHpos+dL_by_dReFHneg)+fdp*(dL_by_dImFHpos-dL_by_dImFHneg),
                                   fdp*(-dL_by_dReFHpos+dL_by_dReFHneg)+fp*(dL_by_dImFHpos+dL_by_dImFHneg));
          }
          else if (!sad_target_anom_only && cent[r])
          {
            centricReflGradSAD2(r);
            rLLGRAD_SF = -cmplxType(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos,
                                   -fdp*dL_by_dReFHpos+fp*dL_by_dImFHpos);
            rFLLG = std::abs(rLLGRAD_SF);
            floatType phllg_rad = (std::cos(std::arg(rLLGRAD_SF) - phsr[r]) > 0) ? phsr[r] : phsr[r]+scitbx::constants::pi;
            rPHLLG = rad2phi(phllg_rad);
            rLLGRAD_SF = std::polar(rFLLG,phllg_rad); // enforce phase restriction
          }
          else if (!sad_target_anom_only) // singleton acentric
          {
            singletonReflGradSAD2(r,plus[r]);
            rLLGRAD_SF = plus[r] ? -cmplxType(fp*dL_by_dReFHpos+fdp*dL_by_dImFHpos,-fdp*dL_by_dReFHpos+fp*dL_by_dImFHpos):
                                   -cmplxType(fp*dL_by_dReFHneg-fdp*dL_by_dImFHneg, fdp*dL_by_dReFHneg+fp*dL_by_dImFHneg);
          }
          rLLGRAD_SF = ptncs_dbl_sites ? h_eq.complex_eq(rLLGRAD_SF) : rLLGRAD_SF;
          rFLLG = std::abs(rLLGRAD_SF);
          rPHLLG = rad2phi(std::arg(rLLGRAD_SF));
        } //selected data
        LLGRAD_SF.push_back(rLLGRAD_SF);
        FLLG.push_back(rFLLG);
        PHLLG.push_back(rPHLLG);
      } //isym asu
    } //isym
  }
  return LLGRAD_SF.deep_copy();
}

LlgcMapSites RefineSAD::calcGradMaps(outStream where,std::string LLGC_ATOMTYPE,floatType LLGC_CLASH,data_llgc LLGCOMPLETE,int ncyc,bool ptncs_dbl_sites,Output& output)
{
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell UCell = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group      SgOps(SpaceGroup::getCctbxSG());
  cctbx::sgtbx::space_group_type SgInfo(SgOps);
  cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
  cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);

  af::shared<miller::index<int> > miller_cp = miller.deep_copy();
  af_cmplx  LLGRAD_SF_cp = LLGRAD_SF.deep_copy();
  //miller_cp = miller::expand_to_p1_indices(SgOps,!FriedelFlag,miller.const_ref());
  //LLGRAD_SF_cp = miller::expand_to_p1_complex<floatType>(SgOps,!FriedelFlag,miller.const_ref(),LLGRAD_SF.const_ref()).data;

  if (ptncs_dbl_sites) //map in P1, search for TNCS VECTOR related pair
  {
    miller_cp.resize(0);
   // LLGRAD_SF_cp.resize(0);
    for (int r = 0; r < NREFL; r++)
    {
      miller::sym_equiv_indices s_e_i(SgOps,miller[r]);
      for (std::size_t isym = 0; isym < s_e_i.multiplicity(false); isym++)
      {
        miller::sym_equiv_index h_eq = s_e_i(isym);
        if (cctbx::sgtbx::reciprocal_space::is_in_reference_asu_1b(h_eq.h()))
        {
          miller_cp.push_back(h_eq.h());
       //   LLGRAD_SF_cp.push_back(h_eq.complex_eq(LLGRAD_SF[r]));
        }
      }
    }
  }
  PHASER_ASSERT(miller_cp.size() == LLGRAD_SF_cp.size());

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  floatType d_min = cctbx::uctbx::d_star_sq_as_d(UCell.max_d_star_sq(miller_cp.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  floatType resolution_factor = 1.0/(2.0*1.5);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
      UCell,d_min,resolution_factor,sym_flags,
      ptncs_dbl_sites ? SgInfoP1 : SgInfo, //PtNcs requires P1
      mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

  bool anomalous_flag(false);
  bool conjugate_flag(true);
  bool treat_restricted(false);
  af::c_grid_padded<3> map_grid(rfft.n_complex());
  //multiplier into LLGRAD ????
  cctbx::maptbx::structure_factors::to_map<floatType> gradmap(
    ptncs_dbl_sites ? SgOpsP1 : SgOps, //PtNcs requires P1
    anomalous_flag,
    miller_cp.const_ref(),
    LLGRAD_SF_cp.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag,
    treat_restricted);
  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref(
    gradmap.complex_map().begin(),
    af::c_grid<3>(rfft.n_complex()));

  // --- do the fft
  rfft.backward(gradmap_fft_ref);

  af::versa<floatType, af::flex_grid<> > real_map(
    gradmap.complex_map().handle(),
    af::flex_grid<>(af::adapt((rfft.m_real())))
          .set_focus(af::adapt(rfft.n_real())));
  cctbx::maptbx::unpad_in_place(real_map);
  af::versa<floatType, af::c_grid<3> > real_map_unpadded(
    real_map, af::c_grid<3>(rfft.n_real()));

  //-- calculate average occupancy of current atoms
  // 0.9 is "expected occupancy" for target of scattering type assignment
  floatType av_occ(0.9),av_u_iso(cctbx::adptbx::b_as_u(WilsonB));
  if (atoms.size())
  {
    floatType sum_occ(0),sum_u_iso(0);
    floatType natoms(0);
    for (int a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED)
    {
      sum_occ += atoms[a].SCAT.occupancy;
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) sum_u_iso += atoms[a].SCAT.u_iso;
      else
      {
        cctbx::adptbx::factor_u_star_u_iso<floatType> factor(getCctbxUC(),atoms[a].SCAT.u_star);
        sum_u_iso += factor.u_iso;
      }
      natoms++;
    }
    av_occ = sum_occ/natoms;
    av_u_iso = sum_u_iso/natoms;
  }
  output.logTab(1,where,"New atoms will be added with occupancy = " + dtos(av_occ,3) +
                             " and B-factor = " + dtos(cctbx::adptbx::u_as_b(av_u_iso),1));
  output.logBlank(where);

  LlgcMapSites new_sites;

  bool header = true;
  bool1D atoms_bswap(atoms.size(),false); //outside loop because set for holes and peaks
  bool1D resurrect_old_atom(atoms.size(),false);
  floatType separation_dist = LLGC_CLASH;
  floatType maxZM_holes(0);
 //do holes first, they are only used to turn known sites anisotropic
 //arrays above will contain peak information at end
  for (int holes = 1; holes >= 0; holes--)
  {
  //  where = holes ? VERBOSE : LOGFILE;
    if (holes) for (int i = 0; i < real_map_unpadded.size(); i++) real_map_unpadded[i] *= -1;
    if (!holes) for (int i = 0; i < real_map_unpadded.size(); i++) real_map_unpadded[i] *= -1; //switch back again

    // --- work out mean and sigma for total search
    cctbx::maptbx::statistics<floatType> stats(
      af::const_ref<floatType, af::flex_grid<> >(
        real_map_unpadded.begin(),
        real_map_unpadded.accessor().as_flex_grid()));
    floatType gradmap_mean = stats.mean();
    floatType gradmap_sigma = stats.sigma();

    // --- find symmetry equivalents
    af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
        real_map_unpadded.begin(),
        af::c_grid_padded<3>(real_map_unpadded.accessor()));
    tags.build(ptncs_dbl_sites ? SgInfoP1 : SgInfo, sym_flags); //PtNcs requires P1
    af::ref<long, af::c_grid<3> > tag_array_ref(
      tags.tag_array().begin(),
      af::c_grid<3>(tags.tag_array().accessor()));
    PHASER_ASSERT(tags.verify(real_map_const_ref));

    // --- peak search
    int peak_search_level(1);
    std::size_t max_peaks(0);
    bool interpolate(true);
    cctbx::maptbx::peak_list<> peak_list(
      real_map_const_ref,
      tag_array_ref,
      peak_search_level,
      max_peaks,
      interpolate);

    af::shared<floatType> peak_heights = peak_list.heights();
    af::shared<dvect3>    peak_sites = peak_list.sites();

    if (PTNCS.use_and_present()) //if there is translational NCS
    {
      output.logEllipsisStart(where,"Doubling sites for translational NCS");
      af::shared<floatType> tmp_heights;
      af::shared<dvect3>    tmp_sites;
      int npeaks = peak_sites.size();
      for (unsigned j = 0; j < npeaks; j++)
      { //double the list of sites by adding equivalent height peaks at the translation position
        //heights in same order
        tmp_heights.push_back(peak_heights[j]);
        //make the second one slightly lower, for cluster analysis
        tmp_heights.push_back(peak_heights[j] - std::numeric_limits<floatType>::min());
        tmp_sites.push_back(peak_sites[j]);
        tmp_sites.push_back(peak_sites[j] + PTNCS.TRA.VECTOR);
      }
      peak_heights = tmp_heights.deep_copy();
      peak_sites = tmp_sites.deep_copy();
      output.logEllipsisEnd(where);
      output.logBlank(where);
    }

    //collect the high peaks and store their Z-scores
    //print a histogram of the Z-scores
    unsigned1D peaks_over_cutoff(0);
    float1D peaks_Zscores(0);
    if (peak_heights.size() && gradmap_sigma) //i.e not a flat map
    {
      holes ? output.logUnderLine(where,"Histogram of Z-scores of HOLES (" + LLGC_ATOMTYPE + ")") :
              output.logUnderLine(where,"Histogram of Z-scores of PEAKS (" + LLGC_ATOMTYPE + ")");
      floatType maxZ = -std::numeric_limits<floatType>::max();
      floatType maxZM = -std::numeric_limits<floatType>::max();
      for (std::size_t i = 0; i < peak_heights.size(); i++)
      {
        //handle special positions
        cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),peak_sites[i]);
        int M = SpaceGroup::NSYMM/peak_sym.multiplicity();
        floatType Z = (peak_heights[i]-gradmap_mean)/gradmap_sigma;
        maxZ = std::max(maxZ,Z);
        floatType ZM = Z/std::sqrt(static_cast<floatType>(M));
        maxZM = std::max(maxZM,ZM);
      }
      if (holes && LLGCOMPLETE.HOLES) maxZM_holes = maxZM;
      holes ? output.logTab(2,where,"Number of holes = " + itos(peak_heights.size())) :
              output.logTab(2,where,"Number of peaks = " + itos(peak_heights.size()));
      output.logTab(2,where,"Maximum Z-score = " + dtos(maxZ));
      output.logTab(2,where,"Maximum M-corrected Z-score = " + dtos(maxZM));
      output.logTab(2,DEBUG,"Mean   = "+dtos(gradmap_mean));
      output.logTab(2,DEBUG,"Sigma  = "+dtos(gradmap_sigma));
      int imaxZ(std::ceil(maxZ));
      unsigned1D histogram(imaxZ);
      unsigned maxStars(0);
      double maxheight = -std::numeric_limits<double>::max();
      int maxi(0);
      for (std::size_t i = 0; i < peak_heights.size(); i++)
      {
        floatType Z((peak_heights[i]-gradmap_mean)/gradmap_sigma);
        if (!LLGCOMPLETE.TOP && Z >= LLGCOMPLETE.SIGMA)
        {
          peaks_over_cutoff.push_back(i);
          peaks_Zscores.push_back(Z);
        }
        if (peak_heights[i] > maxheight)
        {
          maxheight = peak_heights[i];
          maxi = i;
        }
        int iZ = std::floor(Z);
        if (iZ >= 0)
        {
          histogram[iZ]++;
          maxStars = std::max(maxStars,histogram[iZ]);
        }
      }
      if (LLGCOMPLETE.TOP)
      {
        double Z((peak_heights[maxi]-gradmap_mean)/gradmap_sigma);
        peaks_over_cutoff.push_back(maxi);
        peaks_Zscores.push_back(Z);
      }
      // the maximum across is 50
      floatType columns = 50;
      //Can not have less than one star per value, divStarFactor must be at least 1
      floatType min_divStarFactor = 1.0;
      floatType divStarFactor = std::max(min_divStarFactor,static_cast<floatType>(maxStars)/columns);
      output.logBlank(where);
      int grad = static_cast<int>(divStarFactor*10);
      output.logTabPrintf(1,where,"    %-10d%-10d%-10d%-10d%-10d%-10d\n",0,grad,grad*2,grad*3,grad*4,grad*5);
      output.logTabPrintf(1,where,"    %-10s%-10s%-10s%-10s%-10s%-10s\n","|","|","|","|","|","|");
      for (unsigned h = 0; h < imaxZ; h++)
      {
        std::string stars = h >= 10 ? itos(h) + "   " : " " + itos(h) + "   ";
        for (int star = 0; star < static_cast<floatType>(histogram[h])/divStarFactor; star++)
          stars += "*";
        if (histogram[h]) stars += " (" + itos(histogram[h]) + ")";
        output.logTab(1,where,stars);
      }
      output.logBlank(where);
    }

    if (peaks_over_cutoff.size())
    {

      //cluster the peaks - i.e. find the top peaks within separation_dist
      //all the flagged peaks are guaranteed to be separation_dist apart
      bool1D cluster_top(peaks_over_cutoff.size(),true);
      header = true;
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      {
        int cluster_top_num(0);
        floatType cluster_top_dist(0);
        dvect3 peak_site_j(peak_sites[j]);
        dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
        floatType min_sqrtDelta = std::numeric_limits<floatType>::max();
        for (unsigned i = 0; i < j; i++) //check peaks higher up the list
        {
          dvect3 peak_site_i(peak_sites[i]);
          for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
          {
            dvect3 frac_i = SpaceGroup::doSymXYZ(isym,peak_site_i);
            dvect3 cellTrans;
            for (cellTrans[0] = -2; cellTrans[0] <= 2; cellTrans[0]++)
              for (cellTrans[1] = -2; cellTrans[1] <= 2; cellTrans[1]++)
                for (cellTrans[2] = -2; cellTrans[2] <= 2; cellTrans[2]++)
                {
                  dvect3 fracCell_i = frac_i + cellTrans;
                  dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
                  floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
                  if (sqrtDelta < min_sqrtDelta)
                  {
                    min_sqrtDelta = sqrtDelta; //find closest one higer up
                    cluster_top_num = i;
                    cluster_top_dist = sqrtDelta;
                    if (sqrtDelta < separation_dist) cluster_top[j] = false;
                  }
                }
          }
        }
        outStream where(VERBOSE);
        if (header)
        {
          output.logUnderLine(where,"Cluster Analysis (" + LLGC_ATOMTYPE + ")");
          output.logTabPrintf(1,where,"%3s %-7s   %6s %6s %6s %3s %10s %8s\n",
             "#","Z-score","X","Y","Z","Top","Cluster To #","Distance");
          header = false;
        }
        output.logTabPrintf(1,where,"%3i %7.1f   %6.3f %6.3f %6.3f %3s %10s %8s\n",
           j+1,peaks_Zscores[j],peak_site_j[0],peak_site_j[1],peak_site_j[2],
                       cluster_top[j] ? " Y " : " N ",
                       std::string(j ? itos(cluster_top_num+1) : "---").c_str(),
                       std::string(j ? dtos(cluster_top_dist,7,2)  : "---").c_str());
      }
      if (!header) output.logBlank(where);
      //now check clashes with atoms
      bool1D close_to_old(peaks_over_cutoff.size(),false);
      int1D  close_to_old_num(peaks_over_cutoff.size(),-1); //flag initialization with -1
      float1D close_to_old_dist(peaks_over_cutoff.size(),0);
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      //calculate not just cluster_top peaks, as others may affect B-swapping
      {
        floatType min_sqrtDelta = std::numeric_limits<floatType>::max();
        dvect3 peak_site_j(peak_sites[j]);
        dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
        for (unsigned a = 0; a < atoms.size(); a++)
        {
          for (unsigned isym = 0; isym < SpaceGroup::NSYMM; isym++)
          {
            dvect3 frac_i = SpaceGroup::doSymXYZ(isym,atoms[a].SCAT.site);
            dvect3 cellTrans;
            for (cellTrans[0] = -1; cellTrans[0] <= 1; cellTrans[0]++)
              for (cellTrans[1] = -1; cellTrans[1] <= 1; cellTrans[1]++)
                for (cellTrans[2] = -1; cellTrans[2] <= 1; cellTrans[2]++)
                {
                  dvect3 fracCell_i = frac_i + cellTrans;
                  dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
                  floatType sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
                  if (sqrtDelta < min_sqrtDelta)
                  {
                    min_sqrtDelta = sqrtDelta;
                    close_to_old_num[j] = a;
                    close_to_old_dist[j] = sqrtDelta;
                    if (sqrtDelta < separation_dist) close_to_old[j] = true;
                  }
                }
          }
        }
      }

      bool1D accept_new_atom(peaks_over_cutoff.size(),false);
      header = true;
      bool first_Zlimit(true);
      for (unsigned j = 0; j < peaks_over_cutoff.size(); j++)
      {
        int a = close_to_old_num[j];
        int i = peaks_over_cutoff[j]; //index into original array for heights and xyz

        //handle special positions
        //divide the Z-score by sqrt(M) before check on acceptance
        cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),peak_sites[i]);
        int M = SpaceGroup::NSYMM/peak_sym.multiplicity();
        floatType ZM = peaks_Zscores[j]/std::sqrt(static_cast<floatType>(M));

        bool this_atoms_bswap(false),this_resurrect_old_atom(false);
        //bswap test for peaks and holes
        //always take top if top is requested
        PHASER_ASSERT(peaks_Zscores.size());//paranoia
        floatType Zlimit = !LLGCOMPLETE.TOP ? std::max(maxZM_holes,LLGCOMPLETE.SIGMA) : std::max(peaks_Zscores[0],LLGCOMPLETE.SIGMA);
        floatType tol(cctbx::adptbx::b_as_u(0.01));
        if (a >=0)
        {
          this_atoms_bswap = close_to_old[j] &&
                             (ZM > LLGCOMPLETE.SIGMA) &&
                          !atoms[a].REJECTED &&
                          !atoms[a].SCAT.flags.use_u_aniso_only() && //is currently isotropic
                           atoms[a].SCAT.u_iso > lowerU()+tol && atoms[a].SCAT.u_iso < upperU()-tol; //within limits for refinement
          atoms_bswap[a] = atoms_bswap[a] || this_atoms_bswap;
        }
        if (!holes) //additional tests
        {
          if (a >= 0) //there are old atoms
          {
            //resurrection
            this_resurrect_old_atom = close_to_old[j] &&
                                      (ZM > LLGCOMPLETE.SIGMA) &&
                                      atoms[a].REJECTED;
            resurrect_old_atom[a] = resurrect_old_atom[a] || this_resurrect_old_atom;
          }
          //new site
          accept_new_atom[j] = !close_to_old[j] && //not separation_dist from old atom
                              cluster_top[j] && //must be separation_dist apart from any others
                             (ZM >= Zlimit) && //bigger than largest hole, or top etc
                              !this_resurrect_old_atom && !this_atoms_bswap; //except if the peak is being used to bswap or resurrect
        }
        if (holes)
        {
          if (header)
          {
            output.logUnderLine(where,"B-swap Analysis HOLES (" + LLGC_ATOMTYPE + ")");
            output.logTab(1,where,"Holes within " + dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
            output.logBlank(where);
            output.logTabPrintf(1,where,"%5s %2s %6s %6s %6s %6s to %3s %5s %5s %4s %6s\n",
                 "Z-scr","M","X","Y","Z","Dist.","#","Rej\'d","Res\'d","AnoB","B-Swap");
            header = false;
          }
          output.logTabPrintf(1,where,"%5.1f %2i %6.3f %6.3f %6.3f %6s to %3s %5s %5s %4s %6s\n",
              peaks_Zscores[j],M,peak_sites[i][0],peak_sites[i][1],peak_sites[i][2],
                       std::string(a < 0 ? "---" : dtos(close_to_old_dist[j],6,2) ).c_str(),
                       std::string(a < 0 ? "---" : itos(close_to_old_num[j]+1)).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].REJECTED ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].RESTORED ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].SCAT.flags.use_u_aniso_only() ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (this_atoms_bswap ? "Y" : "N")).c_str());
          //and store the numbers of the edited sites
          if (a >= 0)
          {
            if (atoms_bswap[a]) new_sites.anisotropic.insert(a);
          }
        }
        else //peaks
        {
          if (header)
          {
            output.logUnderLine(where,"New atoms, B-swap and Resurrection Analysis PEAKS (" + LLGC_ATOMTYPE + ")");
            output.logTab(1,where,"Peaks within:");
            output.logTab(2,where,dtos(separation_dist,2) + "A of an old atom will turn it anisotropic");
            output.logTab(2,where,dtos(separation_dist,2) + "A of a rejected old atom will resurrect the atom");
            output.logTab(1,where,"Peaks not within:");
            output.logTab(2,where,dtos(separation_dist,2) + "A of a higher peak will qualify as new atoms");
            output.logTab(2,where,dtos(separation_dist,2) + "A of an old atom will qualify as new atoms");
            output.logTab(1,where,"Peak heights:");
            output.logTab(2,where,"Z-score/sqrt(M) > " + dtos(maxZM_holes,1) + " (the depth of the biggest hole)");
            output.logTab(2,where,"Z-score/sqrt(M) > " + dtos(LLGCOMPLETE.SIGMA,1) + " minimum Z-score");
            output.logTab(2,where,"The depth of the biggest hole was " + std::string(LLGCOMPLETE.HOLES?"":"NOT ") + "used for peak selection");
            output.logBlank(where);
            output.logTabPrintf(1,where,"%5s %2s %6s %6s %6s %6s to %3s %5s %5s %4s %6s %5s %3s %3s\n",
             "Z-scr","M","X","Y","Z","Dist.","#","Rej\'d","Res\'d","AnoB","B-Swap","Renew","Top","New");
            header = false;
          }
          if (first_Zlimit && peaks_Zscores[j] < Zlimit)
          {
            output.logTab(1,where,"<------ Z-score cutoff for new atoms ------>");
            first_Zlimit = false;
          }
          output.logTabPrintf(1,where,"%5.1f %2i %6.3f %6.3f %6.3f %6s to %3s %5s %5s %4s %6s %5s %3s %3s\n",
              peaks_Zscores[j],M,peak_sites[i][0],peak_sites[i][1],peak_sites[i][2],
                       std::string(a < 0 ? "---" : dtos(close_to_old_dist[j],6,2) ).c_str(),
                       std::string(a < 0 ? "---" : itos(close_to_old_num[j]+1)).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].REJECTED ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].RESTORED ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (atoms[a].SCAT.flags.use_u_aniso_only() ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (this_atoms_bswap ? "Y" : "N")).c_str(),
                       std::string(a < 0 ? "---" : (this_resurrect_old_atom ? "Y" : "N")).c_str(),
                       std::string(cluster_top[j] ? "Y" : "N").c_str(),
                       std::string(accept_new_atom[j] ? "Y" : "N").c_str());

          //now store the added sites
          if (accept_new_atom[j])
          {
            int i = peaks_over_cutoff[j]; //index into original array for heights
            int t = atomtype2t[LLGC_ATOMTYPE];
            data_atom new_atom;
            new_atom.SCAT = cctbx::xray::scatterer<double>(
              "Added NCYC=" + itos(ncyc) + " Z=" + dtos(peaks_Zscores[j],1), // label
              peak_sites[i], // site
              av_u_iso, // u_iso
              av_occ, // occupancy
              LLGC_ATOMTYPE, // scattering_type
              AtomFp[t], // fp
              AtomFdp[t]); // fdp
            new_atom.set_extra_defaults();  //all defaults
            new_atom.ZSCORE = peaks_Zscores[j];
            new_sites.atoms.push_back(new_atom);
            if (ptncs_dbl_sites &&
                (j%2) && j>0 && //odd
                accept_new_atom[j-1]) //previous was accepted too (pairs even,odd)
            {
              int a = new_sites.atoms.size()-1;
              new_sites.ptncs_pairs[a] = a-1;
            }
          }
          //and store the numbers of the edited sites
          if (a >= 0)
          {
            if (atoms_bswap[a]) new_sites.anisotropic.insert(a);
            restored_data this_restored_data(LLGC_ATOMTYPE,peaks_Zscores[j],av_occ);
            if (resurrect_old_atom[a]) new_sites.restored.insert(std::pair<int,restored_data>(a,this_restored_data));
          }
        }
      }
      output.logBlank(where);
    }
    else
    {
      if (!holes) output.logTab(1,where,"No peaks for new atoms above cut-off");
      output.logBlank(where);
    }
  }//peaks and holes


  where = SUMMARY;
  output.logUnderLine(where,"Identify New Atoms for Addition (" + LLGC_ATOMTYPE + ")");
  output.logBlank(where);
  header = true;
  for (unsigned j = 0; j < new_sites.atoms.size(); j++)
  {
    if (header) //header
    {
      output.logTab(1,where,"Peaks over Z-score cutoff");
      output.logTabPrintf(1,where,"%-3s %6s %6s %6s %5s %2s\n","#","X","Y","Z","Z-scr","M");
      header = false;
    }
    cctbx::sgtbx::site_symmetry peak_sym(getCctbxUC(),getCctbxSG(),new_sites.atoms[j].SCAT.site);
    int M = SpaceGroup::NSYMM/peak_sym.multiplicity();
    output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %5.1f %2i\n",
                   j+1,
                   new_sites.atoms[j].SCAT.site[0],new_sites.atoms[j].SCAT.site[1],new_sites.atoms[j].SCAT.site[2],
                   new_sites.atoms[j].ZSCORE,M);
  }
  if (!header) output.logBlank(where);
  if (!new_sites.atoms.size()) output.logTab(1,where,"No new atoms identified");

  output.logUnderLine(where,"Identify Old Atoms for Editing (" + LLGC_ATOMTYPE + ")");
  output.logTab(1,where,"Old atoms edited if:");
  output.logTab(2,where,"peak < " + dtos(separation_dist,2) + "A from a previously deleted atom will resurrect the old atom");
  output.logTab(2,where,"peak or hole < " + dtos(separation_dist,2) + "A from an old atom will turn it anisotropic");
  output.logBlank(where);
  int nedited = new_sites.nanisotropic() + new_sites.nrestored();
  if (nedited)
  {
    output.logTab(1,where,"Edited atoms list:");
    output.logTabPrintf(1,where,"%-3s %6s %6s %6s %12s %8s\n", "#", "X","Y","Z","occupancy","Atom");
  }
  for (std::set<int>::iterator iter = new_sites.anisotropic.begin(); iter != new_sites.anisotropic.end(); iter++)
  {
    int a = (*iter);
    output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %12.4f %8s  Anisotropic\n",
      a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str());
  }
  std::set<int> unique_a;
  for (std::multimap<int,restored_data>::iterator iter = new_sites.restored.begin(); iter != new_sites.restored.end(); iter++)
  {
    int a = iter->first;
    if (unique_a.find(a) == unique_a.end())
    {
      unique_a.insert(a);
      output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %12.4f %8s  Restored \n",
        a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],av_occ,atoms[a].SCAT.scattering_type.c_str());
    }
  }
  if (!nedited) output.logTab(1,where,"No atoms for editing");
  output.logBlank(where);

  output.logUnderLine(where,"Summary (" + LLGC_ATOMTYPE + ")");
  output.logTab(1,where,"Number of new atoms identified this cycle = " + itos(new_sites.atoms.size()));
  output.logTab(1,where,"Number of old isotropic atoms identified as anisotropic = " + itos(new_sites.nanisotropic()));
  output.logTab(1,where,"Number of old atoms previously identified for restoration = " + itos(new_sites.nrestored()));
  if (PTNCS.use_and_present())
  {
    output.logTab(1,where,"Translational NCS correction factors used this cycle");
    (ptncs_dbl_sites) ? output.logTab(2,where,"Sites added in pairs related by tncs vector"):
                        output.logTab(2,where,"Sites added individually (not in pairs related by tncs vector)");
  }
  output.logBlank(where);
  return new_sites;
}

std::set<int> RefineSAD::findDeletedAtoms(outStream where,Output& output,std::vector<std::pair<int,std::string> > changed)
{
  output.logUnderLine(where,"Identify Old Atoms for Deletion");
  output.logTab(1,where,"Low occupancy sites identified for deletion.");
  output.logTab(1,where,"Minimum occupancy depends on scattering type f\"");
  output.logTab(1,where,"Atoms previously restored are not deleted, to prevent cycling");
  output.logBlank(where);
  std::set<int> atoms_deleted;
  unsigned nfail(0),ndeleted(0),nrestored(0);
  float1D maxOcc(AtomFdp.size(),0),minOcc(AtomFdp.size(),0),rmOcc(AtomFdp.size());
  std::set<int> changed_set;
  for (int c = 0; c < changed.size(); c++)
    changed_set.insert(changed[c].first);
  if (atoms.size())
  {
    bool1D first_call(AtomFdp.size(),true);
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED && (changed_set.find(a) == changed_set.end()))
    {
      floatType rmFactor(0.1);
      int t = atomtype2t[atoms[a].SCAT.scattering_type];
      if (first_call[t])
      {
        maxOcc[t] = 0;
        minOcc[t] = std::numeric_limits<floatType>::max();
        rmOcc[t] = 0;
      }
      first_call[t] = false;
      maxOcc[t] = std::max(maxOcc[t],atoms[a].SCAT.occupancy);
      minOcc[t] = std::min(minOcc[t],atoms[a].SCAT.occupancy);
      if (atoms[a].SCAT.scattering_type != "AX" && atoms[a].SCAT.scattering_type != "RX")
        rmOcc[t] = maxOcc[t]*rmFactor; // Purely real or imaginary are special
    }
    output.logTabPrintf(1,where,"%5s %12s %12s %12s %12s\n", "Atom","f\"","max occ","min occ","cutoff occ");
    for (int t = 0; t < AtomFdp.size(); t++)
      output.logTabPrintf(1,where,"%5s %12.4f %12.4f %12.4f %12.4f\n", t2atomtype[t].c_str(),AtomFdp[t],maxOcc[t],minOcc[t],rmOcc[t]);
    output.logBlank(where);
    bool header = true;
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED && (changed_set.find(a) == changed_set.end()))
    {
      int t = atomtype2t[atoms[a].SCAT.scattering_type];
      if (atoms[a].SCAT.occupancy < rmOcc[t])
      {
        if (header) //header
        {
          output.logTab(1,where,"Deleted atoms list:");
          output.logTabPrintf(1,where,"%-3s %6s %6s %6s %12s %8s\n", "#", "X","Y","Z","occupancy","Atom");
          header = false;
        }
        nfail++;
        atoms[a].RESTORED ? nrestored++ : ndeleted++;
        output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %12.4f %8s  %s\n",
          a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],atoms[a].SCAT.occupancy,atoms[a].SCAT.scattering_type.c_str(),
          (atoms[a].RESTORED ? "(Previously restored)" : "Rejected" ));
        if (!atoms[a].RESTORED) atoms_deleted.insert(a);
      }
    }
    if (!header) output.logBlank(where);
  }
  output.logTab(1,where,"Number of old atoms with occupancy less than cut-off = " + itos(nfail));
  output.logTab(2,where,"Number of these previously restored (therefore not deleted) = " + itos(nrestored));
  output.logTab(1,where,"Number of old atoms identified for deletion = " + itos(ndeleted));
  output.logBlank(where);
  return atoms_deleted;
}

std::vector<std::pair<int,std::string> > RefineSAD::findChangedAtoms(stringset LLGC_ATOMTYPE,std::string LLGC_METHOD,bool CHANGE_ORIG,outStream where,Output& output)
{
  std::vector<std::pair<int,std::string> > changed;
  if (atoms.size() && (AtomFdp.size() > 1 || LLGC_METHOD == "IMAGINARY"))
  {
    output.logUnderLine(where,"Identify Atoms for Change of Atomtype");
    output.logTab(1,where,"Change atomtype to put expected occupancy (based on f\") closest to target");
    if (!CHANGE_ORIG)
      output.logTab(2,where,"Only changes atoms that have been added during completion");
    floatType targetOcc(0.9);
    output.logTab(1,where,"Target occupancy = " + dtos(targetOcc));
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED
         && (!atoms[a].ORIG || CHANGE_ORIG)
         && atoms[a].SCAT.scattering_type != "RX"
         && ((atoms[a].SCAT.scattering_type == "AX" && LLGC_METHOD == "IMAGINARY") || atoms[a].SCAT.scattering_type != "AX"))
    {
      floatType minDiffOcc = std::numeric_limits<floatType>::max();
      std::string new_scattering_type = atoms[a].SCAT.scattering_type;
      for (int t = 0; t < t2atomtype.size(); t++)
      {
        std::string atomtype = t2atomtype[t];
        if (LLGC_ATOMTYPE.find(atomtype) != LLGC_ATOMTYPE.end())
        {
          int old_t = atomtype2t[atoms[a].SCAT.scattering_type];
          floatType new_occupancy =  AtomFdp[t] ? atoms[a].SCAT.occupancy*AtomFdp[old_t]/AtomFdp[t] :
                                                  atoms[a].SCAT.occupancy;
          // Use heuristic difference score that penalises very low occupancies
          floatType diffOcc = (new_occupancy >= targetOcc) ?
              new_occupancy-targetOcc : std::log(targetOcc/new_occupancy);
          if (diffOcc < minDiffOcc)
          {
            minDiffOcc = diffOcc;
            new_scattering_type = atomtype;
          }
        }
      }
      if (atoms[a].SCAT.scattering_type != new_scattering_type)
        changed.push_back(std::pair<int,std::string>(a,new_scattering_type));
    }
    output.logTab(1,where,"Number of old atoms identified for change of atomtype = " + itos(changed.size()));
    output.logBlank(where);
  }
  return changed;
}

void RefineSAD::changeAtoms(outStream where,std::vector<std::pair<int,std::string> >& changed,Output& output)
{
  if (changed.size())
  {
    output.logUnderLine(where,"Change Atomtype");
    output.logTabPrintf(1,where,"%-3s %6s %6s %6s %-8s %-9s %-8s %-9s\n","","","","","Old","Old","New","New");
    output.logTabPrintf(1,where,"%-3s %6s %6s %6s %8s %9s %8s %9s\n", "#", "X","Y","Z","Atomtype","Occupancy","Atomtype","Occupancy");
    for (unsigned i = 0; i < changed.size(); i++)
    {
      int a = changed[i].first;
      std::string atomtype = changed[i].second;
      int old_t = atomtype2t[atoms[a].SCAT.scattering_type];
      int new_t = atomtype2t[atomtype];
      floatType new_occupancy = atoms[a].SCAT.occupancy*AtomFdp[old_t]/AtomFdp[new_t];
      output.logTabPrintf(1,where,"%-3d %6.3f %6.3f %6.3f %8s %9.4f %8s %9.4f\n",
          a+1,atoms[a].SCAT.site[0],atoms[a].SCAT.site[1],atoms[a].SCAT.site[2],
             atoms[a].SCAT.scattering_type.c_str(),atoms[a].SCAT.occupancy,
             atomtype.c_str(),new_occupancy);
      atoms[a].SCAT.scattering_type = atomtype;
      atoms[a].SCAT.fp = AtomFp[new_t];
      atoms[a].SCAT.fdp = AtomFdp[new_t];
      atoms[a].SCAT.occupancy = new_occupancy;
    }
    output.logBlank(where);
    output.logTab(1,where,"Number of old atoms that have changed atomtype = " + itos(changed.size()));
    output.logBlank(where);
  }
}

void RefineSAD::phase_o_phrenia(outStream where,Output& output)
{
  // --- spacegroup and unitcell
  cctbx::uctbx::unit_cell UCell = UnitCell::getCctbxUC();
  cctbx::sgtbx::space_group      SgOps(SpaceGroup::getCctbxSG());
  cctbx::sgtbx::space_group_type SgInfo(SgOps);

  //difference with phase-o-phrenia is that this does not generate all reflections to 3.0
  //it just uses the reflections that are present to 3.0
  //atomic data are already calculated
  //So of the data are to lower resolution or not complete then the result will not be the same
  double maxreso = 3.0;
  af_cmplx PHISUB;
  af::shared<miller::index<int> > hires_miller;
  for (int r = 0; r < NREFL; r++)
  if (reso(miller[r]) > maxreso)
  {
    PHISUB.push_back(std::polar<double>(1.0,2*std::arg(FApos[r]+FAneg[r])));
    hires_miller.push_back(miller[r]);
  }

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  floatType d_min = cctbx::uctbx::d_star_sq_as_d(UCell.max_d_star_sq(hires_miller.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  floatType resolution_factor = 1.0/(2.0*1.5);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
      UCell,d_min,resolution_factor,sym_flags,
      SgInfo,
      mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  cctbx::maptbx::grid_tags<long> tags(grid_target);
  scitbx::fftpack::real_to_complex_3d<floatType> rfft(gridding);

  bool anomalous_flag(false);
  bool conjugate_flag(true);
  bool treat_restricted(false);
  af::c_grid_padded<3> map_grid(rfft.n_complex());
  cctbx::maptbx::structure_factors::to_map<floatType> gradmap(
    SgOps, //PtNcs requires P1
    anomalous_flag,
    hires_miller.const_ref(),
    PHISUB.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag,
    treat_restricted);
  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref(
    gradmap.complex_map().begin(),
    af::c_grid<3>(rfft.n_complex()));

  // --- do the fft
  rfft.backward(gradmap_fft_ref);

  af::versa<floatType, af::flex_grid<> > real_map(
    gradmap.complex_map().handle(),
    af::flex_grid<>(af::adapt((rfft.m_real())))
          .set_focus(af::adapt(rfft.n_real())));
  cctbx::maptbx::unpad_in_place(real_map);
  af::versa<floatType, af::c_grid<3> > real_map_unpadded(
    real_map, af::c_grid<3>(rfft.n_real()));

  // --- find symmetry equivalents
  af::const_ref<floatType, af::c_grid_padded<3> > real_map_const_ref(
      real_map_unpadded.begin(),
      af::c_grid_padded<3>(real_map_unpadded.accessor()));
  tags.build(SgInfo, sym_flags); //PtNcs requires P1
  af::ref<long, af::c_grid<3> > tag_array_ref(
    tags.tag_array().begin(),
    af::c_grid<3>(tags.tag_array().accessor()));
  PHASER_ASSERT(tags.verify(real_map_const_ref));

  // --- peak search
  int peak_search_level(1);
  std::size_t max_peaks(0);
  bool interpolate(true);
  cctbx::maptbx::peak_list<> peak_list(
    real_map_const_ref,
    tag_array_ref,
    peak_search_level,
    max_peaks,
    interpolate);

  af::shared<double> peak_heights = peak_list.heights();
  af::shared<dvect3> peak_sites = peak_list.sites();

  bool1D cluster_top(peak_sites.size(),true);
  double min_sqrtDelta = 4.0; //phase-o-phrenia default
  bool do_cluster(true);
  if (do_cluster)
  if (peak_heights.size())
  {
    //cluster the peaks - i.e. find the top peaks within separation_dist
    //all the flagged peaks are guaranteed to be separation_dist apart
    for (unsigned j = 0; j < peak_sites.size(); j++)
    {
      dvect3 peak_site_j(peak_sites[j]);
      dvect3 orth_j = UnitCell::doFrac2Orth(peak_site_j);
      bool flag = true;
      for (unsigned i = 0; i < j; i++) //check peaks higher up the list
      {
        dvect3 peak_site_i(peak_sites[i]);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMM && flag; isym++)
        {
          dvect3 frac_i = SpaceGroup::doSymXYZ(isym,peak_site_i);
          dvect3 cellTrans;
          for (cellTrans[0] = -2; cellTrans[0] <= 2 && flag; cellTrans[0]++)
          for (cellTrans[1] = -2; cellTrans[1] <= 2 && flag; cellTrans[1]++)
          for (cellTrans[2] = -2; cellTrans[2] <= 2 && flag; cellTrans[2]++)
          {
            dvect3 fracCell_i = frac_i + cellTrans;
            dvect3 orth_i = UnitCell::doFrac2Orth(fracCell_i);
            double sqrtDelta = std::sqrt(std::fabs((orth_i-orth_j)*(orth_i-orth_j)));
            if (sqrtDelta < min_sqrtDelta)
            {
              cluster_top[j] = false;
              flag = false;
            }
          }
        }
      }
    }
  }

  int maxh(20);
  double maxval = peak_heights[0];
  for (int i = peak_sites.size()-1; i >=0; i--)
  if (!cluster_top[i])
  {
    peak_sites.erase(peak_sites.begin()+i);
    peak_heights.erase(peak_heights.begin()+i);
  }
  else
  {
    peak_heights[i] = (peak_heights[i]*100./maxval); //percent
  }

  if (peak_heights.size()) //i.e not a flat map
  {
    int npks = std::min(peak_heights.size(),size_t(60));
    output.logUnderLine(where,"Phase-o-Phrenia Histogram");
    output.logTab(1,where,"Total Number of peaks = " + itos(peak_heights.size()));
    output.logTab(1,where,"Number of peaks plotted = " + itos(npks));
    output.logTab(1,where,"Clustering " + std::string(do_cluster ? "on" : "off"));
    if (do_cluster) output.logTab(1,where,"Cluster distance = " + dtos(min_sqrtDelta,2));
    output.logTab(1,where,"Maximum resolution = " + dtos(maxreso,2));
    for (int h = maxh-1; h >= 0; h--)
    {
      std::string hstr = "|";
      for (std::size_t i = 0; i < npks; i++)
        hstr += std::string(std::floor(peak_heights[i])*(maxh/100.) > h ? "*" : " ");
      output.logTab(1,where,hstr);
    }
    int div(10);
    output.logTabPrintf(1,where,"%-s\n","------------------------------------------------------------");
    output.logTabPrintf(1,where,"%-10s%-10s%-10s%-10s%-10s%-10s%-10s\n","|","|","|","|","|","|","|");
    output.logTabPrintf(1,where,"%-10d%-10d%-10d%-10d%-10d%-10d%-10d\n",0,div,div*2,div*3,div*4,div*5,div*6);
    output.logBlank(where);
    if (false)
    {
    output.logTab(1,where,"Peak List:");
    output.logTab(1,where,"Relative height  Fractional coordinates");
    for (std::size_t i = 0; i < npks; i++)
      output.logTab(1,where,itos(i+1,3) + ": " + dtos(peak_heights[i],7,2) + "     " + dvtos(peak_sites[i],8,5));
    output.logBlank(where);
    }
  }
}

} //phaser
