//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <algorithm>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <cctbx/adptbx.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/maptbx/copy.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/structure_factors.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/miller/index_span.h>
#include <cctbx/xray/sampled_model_density.h>
#include <fstream>
#include <iotbx/pdb/hierarchy.h>
#include <iotbx/pdb/input.h>
#include <phaser/cctbx_project/maptbx/utils_extra.h>
#include <mmtbx/geometry/calculator.hpp>
#include <phaser/io/Errors.h>
#include <phaser/io/Output.h>
#include <phaser/io/hybrid_36_c.h>
#include <phaser/lib/between.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/lib/van_der_waals_radii.h>
#include <phaser/mr_objects/data_pdb.h>
#include <phaser/src/Molecule.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/TraceMol.h>
#include <phaser/src/UnitCell.h>
#include <scitbx/constants.h>
#include <scitbx/fftpack/real_to_complex_3d.h>
#include <scitbx/misc/split_lines.h>
#include <set>

namespace phaser {

  struct pdb_getter
  {
    public:
     typedef dvect3 value_type;
     value_type operator ()(pdb_record const& record) const // const added
     { return record.X; }
  };

  struct vec3_getter
  {
    public:
     typedef dvect3 value_type;
     value_type operator ()(dvect3 const& record) const // const added
     { return record; }
  };


void TraceMol::setMAP(data_pdb& PDB,Output& output)
{
  SAMP_USED = "HEXGRID";
  data_model& MODEL = PDB.ENSEMBLE[0];
  data_trace& TRACE = PDB.TRACE;
  data_map& MAP = PDB;
  npdb = 0;
  SAMP = TRACE.SAMP_DISTANCE ? TRACE.SAMP_DISTANCE : PDB.calculate_sampling_from_extent(TRACE);
  pdb_errors = "";
  PHASER_ASSERT(SAMP);
  PHASER_ASSERT(MODEL.map_format());
  //double SAMPsqr = fn::pow2(SAMP);
  double requestres = 5.0; //5A map, for memory and to always have similar looking desity
  pdb_errors = "";
  trace.clear();
  std::ifstream infile(const_cast<char*>(MODEL.filename().c_str()));
  if (!infile)
  {
    pdb_errors = "File \"" + MODEL.filename() + "\" could not be opened";
    return;
  }

  af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref;
  double d_min;
  double resolution_factor;
  cctbx::maptbx::grid_tags<long> tags;
  cctbx::maptbx::structure_factors::to_map<double> gradmap;
  UnitCell  UC;
  cctbx::uctbx::unit_cell UCell;

  af::shared<dvect3> wang_mask; //keep until correct number of points
  {//memory
  af::versa<double, af::c_grid<3> > real_map_unpadded;
  {
  scitbx::fftpack::real_to_complex_3d<double> rfft;
  {
  af::shared<miller::index<int> > miller;
  af_cmplx  Fmap;
  {
  CMtz::MTZ *mtzfrom = safe_mtz_get(MODEL.filename(),1);
  CMtz::MTZCOL *mtzH(0),*mtzK(0),*mtzL(0),*Acol(0),*Pcol(0);
  mtzH = safe_mtz_col_lookup(mtzfrom,"H");
  mtzK = safe_mtz_col_lookup(mtzfrom,"K");
  mtzL = safe_mtz_col_lookup(mtzfrom,"L");
  Acol = safe_mtz_col_lookup(mtzfrom,MODEL.LABI_F);
  Pcol = safe_mtz_col_lookup(mtzfrom,MODEL.LABI_P);

  output.logEllipsisStart(TESTING,"Reading MTZ file");
  CMtz::MTZXTAL* xtal = CMtz::MtzSetXtal( mtzfrom, CMtz::MtzColSet( mtzfrom, Acol));
  double a(xtal->cell[0]);
  double b(xtal->cell[1]);
  double c(xtal->cell[2]);
  double alpha(xtal->cell[3]);
  double beta(xtal->cell[4]);
  double gamma(xtal->cell[5]);
  if (!between(alpha,90,1.0e-04) || !between(beta,90,1.0e-04) || !between(gamma,90,1.0e-04))
  throw PhaserError(FATAL,"Unit Cell of Ensemble must be orthogonal");
  // --- spacegroup and unitcell
  af::double6                    Cell(a,b,c,alpha,beta,gamma);
  UCell = cctbx::uctbx::unit_cell(Cell);
  UC = UnitCell(Cell);
  cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
  cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
  double H(0),K(0),L(0),resolution(0);
  for (int mtzr = 0; mtzr < mtzfrom->nref ; mtzr++)
  {
    H = mtzH->ref[mtzr];
    K = mtzK->ref[mtzr];
    L = mtzL->ref[mtzr];
    cmplxType F(std::polar(double(Acol->ref[mtzr]), scitbx::deg_as_rad(Pcol->ref[mtzr])));
    miller::index<int> HKL(H,K,L);
    resolution = UC.reso(HKL);
    if (resolution >= requestres &&
        !mtz_isnan(Acol->ref[mtzr]) &&
        !mtz_isnan(Pcol->ref[mtzr]))
    {
      miller.push_back(HKL);
      Fmap.push_back(F);
    }
  }
  CMtz::MtzFree(mtzfrom);
  }
  output.logEllipsisEnd(TESTING);
  output.logEllipsisStart(LOGFILE,"Performing Map FFT");

  // --- prepare fft grid
  cctbx::sgtbx::search_symmetry_flags sym_flags(true);
  d_min = cctbx::uctbx::d_star_sq_as_d(UCell.max_d_star_sq(miller.const_ref()));
  af::int3 mandatory_factors(2,2,2);
  int max_prime(5);
  bool assert_shannon_sampling(true);
  resolution_factor = 1.0/(2.0*1.5);
  cctbx::sgtbx::space_group      SgOpsP1("P 1","A1983");
  cctbx::sgtbx::space_group_type SgInfoP1(SgOpsP1);
  af::int3 gridding = cctbx::maptbx::determine_gridding(
      UCell,d_min,resolution_factor,sym_flags,
      SgInfoP1,
      mandatory_factors,max_prime,assert_shannon_sampling);
  af::c_grid<3> grid_target(gridding);
  tags = cctbx::maptbx::grid_tags<long>(grid_target);
  rfft = scitbx::fftpack::real_to_complex_3d<double> (gridding);

  bool anomalous_flag(false);
  bool conjugate_flag(true);
  bool treat_restricted(false);
  af::c_grid_padded<3> map_grid(rfft.n_complex());
  gradmap = cctbx::maptbx::structure_factors::to_map<double> (
    SgOpsP1,
    anomalous_flag,
    miller.const_ref(),
    Fmap.const_ref(),
    rfft.n_real(),
    map_grid,
    conjugate_flag,
    treat_restricted);
  gradmap_fft_ref = af::ref<cmplxType, af::c_grid<3> > (
    gradmap.complex_map().begin(),
    af::c_grid<3>(rfft.n_complex()));
  }

  // --- do the fft
  rfft.backward(gradmap_fft_ref);

  af::versa<double, af::flex_grid<> > real_map(
    gradmap.complex_map().handle(),
    af::flex_grid<>(af::adapt((rfft.m_real())))
          .set_focus(af::adapt(rfft.n_real())));
  cctbx::maptbx::unpad_in_place(real_map);
  real_map_unpadded = af::versa<double, af::c_grid<3> > (
    real_map, af::c_grid<3>(rfft.n_real()));
  }

  // --- work out mean and sigma for total search
  {//memory
  cctbx::maptbx::statistics<double> statistics(
      af::const_ref<double, af::flex_grid<> >(
        real_map_unpadded.begin(),
        real_map_unpadded.accessor().as_flex_grid()));
    double gradmap_sigma = statistics.sigma();
    PHASER_ASSERT(gradmap_sigma);
  }//memory


  // --- convert to mask
  double map_volume = UC.A()*UC.B()*UC.C();
  //allow for 25% wrong, tight edges
  //wang is a scale factor, more or less
  double protein_volume = MAP.volume()*0.75*TRACE.SAMP_WANG;
  double solvent_fraction = 1.0-(protein_volume/map_volume);
  cctbx::maptbx::map_to_wang_mask(real_map_unpadded,UCell,solvent_fraction);

  output.logEllipsisEnd(LOGFILE);
  output.logTab(1,TESTING,"Wang distance: " + dtos(d_min*resolution_factor));
  output.logEllipsisStart(TESTING,"Peak Search");

  //convert mask points to frac coordinates
  double nx = real_map_unpadded.accessor()[0];
  double ny = real_map_unpadded.accessor()[1];
  double nz = real_map_unpadded.accessor()[2];
  for (int lx = 0; lx < nx; lx++) {
    for (int ly = 0; ly < ny; ly++) {
      for (int lz = 0; lz < nz; lz++) {
        if (real_map_unpadded(lx,ly,lz) > 0.5) // mask is 0 or 1
          wang_mask.push_back(dvect3(lx/nx,ly/ny,lz/nz));
  }}}
  PHASER_ASSERT(wang_mask.size());
  } //memory real_map_unpadded

  output.logEllipsisEnd(TESTING);

  for (unsigned i = 0; i < wang_mask.size(); i++)
  { //permanently change
    wang_mask[i][0] *= UC.A();
    wang_mask[i][1] *= UC.B();
    wang_mask[i][2] *= UC.C();
  }


  output.logEllipsisStart(TESTING,"Moving wang peaks to being proximal to centre");

//move wang_mask to centre
//box is region of interest
//map may be corners of grid
//MAP.PRINCIPAL_CENTRE == MAP.CENTRE as PR,PT identity
  double efactor(1.9);//1.2, 2
  dvect3 box_min = MAP.PRINCIPAL_CENTRE - MAP.PRINCIPAL_EXTENT/efactor;
  dvect3 box_max = MAP.PRINCIPAL_CENTRE + MAP.PRINCIPAL_EXTENT/efactor;
  //how many times does the extent fit into the map box?
  int afrac = int(UC.A()/MAP.PRINCIPAL_EXTENT[0])+1; //round up
  int bfrac = int(UC.B()/MAP.PRINCIPAL_EXTENT[1])+1; //round up
  int cfrac = int(UC.C()/MAP.PRINCIPAL_EXTENT[2])+1; //round up
  dvect3 frac_first(afrac,bfrac,cfrac);
  dvect3 cellTrans_min(-frac_first);
  dvect3 cellTrans_max(frac_first);
  dvect3 cellTrans(0,0,0);
  bool1D checked(wang_mask.size(),false);
  { //most likely first, centred in correct place
    for (unsigned i = 0; i < wang_mask.size(); i++)
    {
      dvect3& orth = wang_mask[i];
      if ((orth[0] >= box_min[0]) && (orth[0] <= box_max[0]) &&
          (orth[1] >= box_min[1]) && (orth[1] <= box_max[1]) &&
          (orth[2] >= box_min[2]) && (orth[2] <= box_max[2]))
      {
        checked[i] = true;
      }
    }
  }
  for (cellTrans[0] = cellTrans_min[0]; cellTrans[0] <= cellTrans_max[0]; cellTrans[0]++)
  for (cellTrans[1] = cellTrans_min[1]; cellTrans[1] <= cellTrans_max[1]; cellTrans[1]++)
  for (cellTrans[2] = cellTrans_min[2]; cellTrans[2] <= cellTrans_max[2]; cellTrans[2]++)
  if (cellTrans != dvect3(0,0,0))
  {
    dvect3 orthCellTrans;
    orthCellTrans[0] = cellTrans[0]*UC.A();
    orthCellTrans[1] = cellTrans[1]*UC.B();
    orthCellTrans[2] = cellTrans[2]*UC.C();
    for (unsigned i = 0; i < wang_mask.size(); i++)
    if (!checked[i])
    {
      dvect3 orth = wang_mask[i]+orthCellTrans;
      if ((orth[0] >= box_min[0]) && (orth[0] <= box_max[0]) &&
          (orth[1] >= box_min[1]) && (orth[1] <= box_max[1]) &&
          (orth[2] >= box_min[2]) && (orth[2] <= box_max[2]))
      {
        wang_mask[i] = orth;
        checked[i] = true;
      }
    }
  }
  output.logEllipsisEnd(TESTING);

  if (output.level(TESTING))
  {
    std::string wangstr("debug." + ntos(++npdb) + ".wang." + MODEL.rootname() + ".pdb");
    output.logTab(1,TESTING,"Write " + wangstr + " (" + itos(wang_mask.size()) + ")" );
    std::ofstream wangpdb(wangstr.c_str());
    for (unsigned i = 0; i < wang_mask.size(); i++)
    {
      pdb_record card;
      card.X = wang_mask[i]; //after conversion
      wangpdb << card.Card(i,i);
    }
    wangpdb.close();
  }

  int ncyc(0);

  output.logEllipsisStart(LOGFILE,"Calculate Hexagonal Grid");
  do {
  trace.clear();
  output.logTab(2,LOGFILE,"Sampling #" + itos(ncyc+1) + ": " + dtos(SAMP) + " Angstroms");
  double min_dist = d_min*resolution_factor; //wang map sampling grid
  output.logTab(1,TESTING,"Wang sampling distance: " + dtos(min_dist));
  std::ofstream boxpdb;
  int box_a(1);
  if (output.level(TESTING))
  {
    std::string boxstr("debug."+ ntos(++npdb) + ".box." + MODEL.rootname() + ".pdb");
    output.logTab(1,TESTING,"Write box points to " + boxstr);
    boxpdb.open(boxstr.c_str());
  }

  pdb_record next_atom;
  { //insert centre always at the start
    PHASER_ASSERT(!trace.size());
    int a(0);
    char hybrid36_AtmNum[6]; //leave room for \0
    hy36encode(5,a,hybrid36_AtmNum);
    next_atom.ATOMorHETATMandNUM = "ATOM  " + std::string(hybrid36_AtmNum,5);
    next_atom.ResNum = a;
    next_atom.X = MAP.PRINCIPAL_CENTRE;
    trace.push_back(next_atom);
  }

//now select only the ones that are buried
//TEMP before format change for constructor
  //distance mapped out by each point is the cube diagonal divided by 2
  double wangradius = min_dist/sqrt(3.)/2.;

  mmtbx::geometry::asa::calculator::ConstRadiusCalculator<
    mmtbx::geometry::asa::calculator::utility::TransformedArray< af::shared<dvect3>, vec3_getter >,
    double // arrays can be used directly without adaptors
   >
   wangcalc(
     mmtbx::geometry::asa::calculator::utility::TransformedArray< af::shared<dvect3>, vec3_getter >( wang_mask ),
     wangradius
     );

  double hex_distance = SAMP*hex_factor();
  PHASER_ASSERT(hex_distance);
  //volume mapped out by each hex grid point is the close contact distance
  dvect3 orthSite(0,0,0);
  dvect3 orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
  double off1 = 5.0/6.0;
  //add buffer to box, so that not right up against extent
  box_min[0] -= SAMP; box_min[1] -= SAMP; box_min[2] -= SAMP;
  box_max[0] += SAMP; box_max[1] += SAMP; box_max[2] += SAMP;
  int ih(1);
  for (orthSite[2] = box_min[2]; orthSite[2] < box_max[2]; orthSite[2] += orthSamp[2])
  {
    off1 = 1. - off1;
    double off0 = 3.0/4.0;
    for (orthSite[1] = box_min[1] + off1 * orthSamp[1]; orthSite[1] < box_max[1]; orthSite[1] += orthSamp[1])
    {
      off0 = 1. - off0;
      for (orthSite[0] = box_min[0] + off0 * orthSamp[0]; orthSite[0] < box_max[0]; orthSite[0] += orthSamp[0])
      {
        if (output.level(TESTING))
        {
          pdb_record card;
          card.X = orthSite;
          boxpdb << card.Card(box_a,box_a);
          box_a++;
        }
        if (wangcalc.is_overlapping_sphere(orthSite,hex_distance))
        {
          std::string atomcard = "ATOM  ";
          char hybrid36_AtmNum[6]; //leave room for \0
          hy36encode(5,ih++,hybrid36_AtmNum);
          next_atom.ATOMorHETATMandNUM = atomcard + std::string(hybrid36_AtmNum,5);
          next_atom.ResNum = trace.size();
          next_atom.X = orthSite;
          trace.push_back(next_atom);
        }
      }
    }
  }
  if (output.level(TESTING))
  {
    boxpdb.close();
  }
  output.logEllipsisEnd(TESTING);

  //wang_mask

//now select only the ones that are buried
  std::vector<double> hexradii(trace.size(),hex_distance);
  int    sampling_point_count = 960;
  double probe = 0;

  mmtbx::geometry::asa::calculator::SimpleCalculator<
    mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >,
    std::vector<double> // arrays can be used directly without adaptors
   >
   mycalc(
     mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >( trace ),
     hexradii,
     probe, // probe radius
     sampling_point_count, // sampling point count
     2*hex_distance+probe //this is required
     );

  int1D counts(101,0);

  for (int a = 0; a < trace.size(); a++)
  {
    double accessible_surface_area = mycalc.accessible_points(a)/double(sampling_point_count);
    counts[int(accessible_surface_area*100)]++;
    trace[a].B = accessible_surface_area*100; //for coot output
    if (accessible_surface_area > TRACE.SAMP_ASA)
      trace[a].O = 0;
  }

  if (output.level(TESTING)) //write here so that asa is in coordinates
  {
    std::string unprunedstr("debug." + ntos(++npdb) + ".unpruned." + MODEL.rootname() + ".pdb");
    output.logTab(1,TESTING,"Write " + unprunedstr + " (" + itos(trace.size()) + ")");
    std::ofstream unprunedpdb(unprunedstr.c_str());
    for (unsigned a = 0; a < trace.size(); a++)
      unprunedpdb << trace[a].Card();
    unprunedpdb.close();
  }

  for (int a = trace.size()-1; a >= 0; a--) //a==0 is the centre
  if (!trace[a].O) trace.erase(trace.begin()+a);

  output.logTab(1,TESTING,"Asa minimum: " + dtos(TRACE.SAMP_ASA));
  for (unsigned a = 0; a < counts.size(); a++)
  if (counts[a])
    output.logTab(1,TESTING,"histogram asa=" + dtos(a/100.) + " num=" + itos(counts[a]) +
                   std::string(a/100. > TRACE.SAMP_ASA ? " erased" :""));
  output.logTab(1,TESTING,"(other histogram bins empty)" );
  if (output.level(TESTING))
  {
    std::string buriedstr("debug." + ntos(++npdb) + ".buried." + MODEL.rootname() + ".pdb");
    output.logTab(1,TESTING,"Write " + buriedstr + " (" + itos(trace.size()) + ")");
    std::ofstream buriedpdb(buriedstr.c_str());
    for (unsigned a = 0; a < trace.size(); a++)
      buriedpdb << trace[a].Card();
    buriedpdb.close();
  }

  //renumber
  for (int a = 0; a < trace.size(); a++)
  {
    char hybrid36_AtmNum[6]; //leave room for \0
    hy36encode(5,a+1,hybrid36_AtmNum);
    trace[a].ATOMorHETATMandNUM = "ATOM  " + std::string(hybrid36_AtmNum,5);
    trace[a].ResNum = a+1;
  }

  if (trace.size() > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE) ||
      trace.size() < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE) )
  {
    double factor = trace.size() ?  double(trace.size())/double(TRACE.SAMP_TARGET) : 0.25;
    factor = std::pow(factor,1./3.);
    SAMP *= factor;
    output.logTab(1,TESTING,"Trace length " + itos(trace.size()) + " Target=" + itos(TRACE.SAMP_TARGET) + " Range=" + itos(TRACE.SAMP_RANGE));
    output.logTab(1,TESTING,"Sampling multiplied by " + dtos(factor));
    output.logBlank(TESTING);
  }
  ncyc++;
  }
  while ((trace.size() > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE) ||
          trace.size() < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE)) &&
          ncyc < TRACE.SAMP_NCYC); //infinite loop trap
  output.logEllipsisEnd(LOGFILE);
  DIST = SAMP*hex_factor();
  TYPE = "map-grid";
}

void TraceMol::setPDB(bool occupancy_warning,data_pdb& PDB,Output& output)
{
  SAMP_USED = "ALL";
  data_trace& TRACE = PDB.TRACE;
  npdb = 0;
  trace.clear();
  SAMP = TRACE.SAMP_DISTANCE ? TRACE.SAMP_DISTANCE : PDB.calculate_sampling_from_extent(TRACE);
  output.logTab(1,TESTING,"Sampling: " + dtos(SAMP));
  pdb_errors = "";
  PHASER_ASSERT(SAMP);
  int MTRACE = PDB.MTRACE;
  std::vector<data_model> ENSEMBLE = PDB.ENSEMBLE;
  Molecule Coords(ENSEMBLE[MTRACE],PDB.USE_HETATM);
  if (!PDB.TRACE.MODEL.is_default())
  { //overwrite
    //if the TRACE molecule is set on input
    ENSEMBLE = std::vector<data_model>(1,PDB.TRACE.MODEL);
    MTRACE = 0;
    Coords = Molecule(PDB.TRACE.MODEL,PDB.USE_HETATM);
  }
  if (Coords.is_atom())
  {
    for (unsigned a = 0; a < Coords.pdb[0].size(); a++)
      if (Coords.pdb[0][a].O != 0.0)
        trace.push_back( Coords.pdb[0][a] );
    TYPE = "Single-atom";
    DIST = SAMP; //this is the close contact distance, used in packing
    SAMP = 0; //special case, not used in packing anyway, but makes the table look good
    return;
  }

  if (output.level(TESTING))
  {
    std::string origstr("debug."+ ntos(++npdb) + ".orig." + ENSEMBLE[MTRACE].rootname() + ".pdb");
    output.logTab(1,TESTING,"Write " + origstr + " (" + itos(Coords.nAtoms()) + ")");
    std::ofstream backpdb(origstr.c_str());
    for (int a = 0; a < Coords.nAtoms(); a++)
      backpdb << Coords.card(a).Card();
    backpdb.close();
  }

  //select conserved atoms
  if (Coords.nMols() > 1 || ENSEMBLE.size() > 1)
  {
    double maxDistSqr = fn::pow2(DEF_PACK_CONS_DIST); //within 1.5 angstroms
    double CONS(DEF_PACK_CONS_DIST); //within 1.5 angstroms
    { //memory first clear up same molecule
    int mtrace = Coords.mtrace;
    std::vector<pdb_record>& pdbt = Coords.pdb[mtrace];
    bool1D is_conserved(pdbt.size(),false);
    for (unsigned a = 0; a < pdbt.size(); a++)
    {
      if (pdbt[a].O != 0.0)// && Coords.isTraceAtom(mtrace,a))
      {
        is_conserved[a] = true;
        bool1D found_proximal(Coords.pdb.size(),false);
        for (unsigned m = 0; m < Coords.pdb.size(); m++)
        {
          std::vector<pdb_record>& pdbm = Coords.pdb[m];
          if (m == mtrace)
          {
            found_proximal[m] = true; //the trace atom is proximal!
          }
          else
          {
            for (unsigned b = 0; b < pdbm.size() && !found_proximal[m]; b++)
            if (pdbm[b].O != 0.0)
            { //if a close atom is found, bail
            if ((pdbt[a].X[0] > pdbm[b].X[0]-CONS) && (pdbt[a].X[0] < pdbm[b].X[0]+CONS))
            if ((pdbt[a].X[1] > pdbm[b].X[1]-CONS) && (pdbt[a].X[1] < pdbm[b].X[1]+CONS))
            if ((pdbt[a].X[2] > pdbm[b].X[2]-CONS) && (pdbt[a].X[2] < pdbm[b].X[2]+CONS))
            {
              double distSqr = (pdbt[a].X-pdbm[b].X)*(pdbt[a].X-pdbm[b].X);
              bool proximal(distSqr < maxDistSqr);
              if (proximal)
              {
                found_proximal[m] = true;
                break;
              }
            }
            }
            //found_proximal will stay false;
          }
        }
        for (int m = 0; m < found_proximal.size(); m++) //all must have proximal to be conserved
          is_conserved[a] = found_proximal[m] && is_conserved[a];
      }
      else
      {
        is_conserved[a] = false;
      }
    }
    // delete all but conserved model

    std::vector<pdb_record> tmp;
    for (unsigned a = 0; a < Coords.pdb[mtrace].size(); a++)
      if (is_conserved[a])
        tmp.push_back(Coords.pdb[mtrace][a]);
    Coords.pdb.resize(1);
    Coords.pdb[0] = tmp; //Molecule becomes backbone
    }
    //then delete from other
    for (int M = 0; M < PDB.ENSEMBLE.size(); M++)
    if (M != MTRACE)
    {
      Molecule Coords_ref(PDB.ENSEMBLE[M],PDB.USE_HETATM);
      std::vector<pdb_record>& pdbt = Coords.pdb[0];
      bool1D is_conserved(pdbt.size(),true);
      for (unsigned a = 0; a < pdbt.size(); a++)
      if (pdbt[a].O != 0.0)
      {
        bool1D found_proximal(Coords_ref.pdb.size(),false);
        for (unsigned m = 0; m < Coords_ref.pdb.size(); m++)
        {
          std::vector<pdb_record>& pdbm = Coords_ref.pdb[m];
          for (unsigned b = 0; b < pdbm.size(); b++)
          if (pdbm[b].O != 0.0)
          if ((pdbt[a].X[0] > pdbm[b].X[0]-CONS) && (pdbt[a].X[0] < pdbm[b].X[0]+CONS))
          if ((pdbt[a].X[1] > pdbm[b].X[1]-CONS) && (pdbt[a].X[1] < pdbm[b].X[1]+CONS))
          if ((pdbt[a].X[2] > pdbm[b].X[2]-CONS) && (pdbt[a].X[2] < pdbm[b].X[2]+CONS))
          {
            double distSqr = (pdbt[a].X-pdbm[b].X)*(pdbt[a].X-pdbm[b].X);
            bool proximal(distSqr < maxDistSqr);
            if (proximal)
            {
              found_proximal[m] = true;
              break;
            }
          }
          //else found_proximal will stay false;
        }
        for (int m = 0; m < found_proximal.size(); m++) //all must have proximal to be conserved
          is_conserved[a] = found_proximal[m] && is_conserved[a];
      }
      std::vector<pdb_record> tmp;
      for (unsigned a = 0; a < Coords.pdb[0].size(); a++)
        if (is_conserved[a])
          tmp.push_back(Coords.pdb[0][a]);
      Coords.pdb[0] = tmp; //Molecule becomes conserved only
    }
  }

  if (output.level(TESTING))
  {
    std::string conservedstr("debug." + ntos(++npdb) + ".conserved." + ENSEMBLE[MTRACE].rootname() + ".pdb");
    output.logTab(1,TESTING,"Write " + conservedstr + " (" + itos(Coords.pdb[0].size()) + ")");
    std::ofstream tracepdb(conservedstr.c_str());
    for (int a = 0; a < Coords.pdb[0].size(); a++)
      tracepdb << Coords.pdb[0][a].Card();
    tracepdb.close();
  }

  //now there is only one model in Coords, the conserved one
  //delete zero occupancy atoms e.g. HETATM
  std::vector<pdb_record>& pdbm = Coords.pdb[0];
  for (int a = pdbm.size()-1; a >= 0; a--) //a==0 is the centre for hexgrid only
    if (pdbm[a].O == 0)
      pdbm.erase(pdbm.begin()+a);

  int num_calpha(0);
  for (unsigned a = 0; a < pdbm.size(); a++)
    if (Coords.isTraceAtom(0,a))
      num_calpha++;

  output.logTab(1,TESTING,"Selection method: " + TRACE.SAMP_USE);
  output.logTab(1,TESTING,"Selection pdb=" + itos(pdbm.size()) + " num_calpha=" + itos(num_calpha) + " target=" + itos(TRACE.SAMP_TARGET));
  if ((TRACE.SAMP_USE == "AUTO" && pdbm.size() <= TRACE.SAMP_TARGET) ||
       TRACE.SAMP_USE == "ALL")
  {
    output.logTab(1,TESTING,"Use ALL");
    TYPE = "all-atom";
    //if the structure is small use all atoms, better volume fill
    for (unsigned a = 0; a < pdbm.size(); a++)
      trace.push_back(pdbm[a]);
    //SAMP = 0; //flag for not using hexagonal grid
    SAMP = DEF_TRAC_BOND_DIST; //average bond distance
    for (int a = 0; a < trace.size(); a++)
    {
      trace[a].AtomNum = a;
      trace[a].ResNum = a;
    }
    double clash_factor(1.5);
    DIST = SAMP/clash_factor;
    SAMP_USED = "ALL";
  }
  else if ((TRACE.SAMP_USE == "AUTO" && num_calpha <= TRACE.SAMP_TARGET) ||
            TRACE.SAMP_USE == "CALPHA" ||
            occupancy_warning)
  { //if the occupancy is refined, the structure MUST be sampled at the same "resolution" as
    //the occupancy refinement
    //This is done in blocks of residues, so we have to use the trace (CA) atoms
    //i.e reverts to old code
    output.logTab(1,TESTING,"Use CALPHA");
    TYPE = "C-alpha";
    for (unsigned a = 0; a < pdbm.size(); a++)
    {
      if (pdbm[a].O != 0 && Coords.isTraceAtom(0,a))
      {
        pdb_record card;
        card.X = pdbm[a].X;
        trace.push_back(card);
        calpha_lookup.push_back(a); //index trace->atom
      }
    }
    SAMP = DEF_PACK_DIST; //old default
    for (int a = 0; a < trace.size(); a++)
    {
      trace[a].AtomNum = a;
      trace[a].ResNum = a;
    }
    double clash_factor(1.5);
    DIST = SAMP/clash_factor;
    SAMP_USED = "CALPHA";
  }
  else if ((TRACE.SAMP_USE == "AUTO" && num_calpha > TRACE.SAMP_TARGET) ||
            TRACE.SAMP_USE == "HEXGRID")
  {
    output.logTab(1,TESTING,"Use HEXGRID");
    output.logTab(1,TESTING,"Target=" + itos(TRACE.SAMP_TARGET) + " Range(+/-)=" + itos(TRACE.SAMP_RANGE));
    SAMP_USED = "HEXGRID";
    int ncyc(0);
    trim_asa(PDB.ENSEMBLE[MTRACE].rootname(),Coords,PDB,output);
    output.logBlank(TESTING);
    std::map<double,int> sampling;
    output.logEllipsisStart(LOGFILE,"Calculate Hexagonal Grid");
    do {
      trace.clear();
      output.logTab(2,LOGFILE,"Sampling #" + itos(ncyc+1) + ": " + dtos(SAMP) + " Angstroms");
      hexgrid(PDB.ENSEMBLE[MTRACE].rootname(),Coords,PDB,output);
      sampling[SAMP] = trace.size();
      output.logTab(1,TESTING,"Sampling: " + dtos(SAMP) + " n=" + itos(sampling.size()));
      output.logTab(1,TESTING,"Trace= " + itos(trace.size()) + " Target=" + itos(TRACE.SAMP_TARGET) + " Range=" + itos(TRACE.SAMP_RANGE));
      output.logTab(2,TESTING,"Cycle#" + itos(ncyc+1) + " sampling=" + dtos(SAMP) +  " trace length=" + itos(trace.size()));
      if (trace.size() > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE) ||
          trace.size() < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE) )
      {
        if (sampling.size() == 1)
        {
          if (trace.size() > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE))
          {
            SAMP *= 2.;
            output.logTab(1,TESTING,"Sampling doubled");
          }
          else if (trace.size() < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE))
          {
            double tol = 1.0e-03;
            if (SAMP < TRACE.SAMP_MIN + tol)
            {
              ncyc = TRACE.SAMP_NCYC; //to force while to fail
              output.logTab(1,TESTING,"Sampling aborted, at minimum");
            }
            else
            {
              SAMP /= 2.;
              output.logTab(1,TESTING,"Sampling halved");
            }
          }
        }
        else
        {
          std::map<double,int>::iterator iterlo = sampling.begin();
          std::map<double,int>::iterator iterhi = sampling.end(); iterhi--; //first
          output.logTab(1,TESTING,"init samp  lo " + dtos(iterlo->first) + " hi " + dtos(iterhi->first));
          output.logTab(1,TESTING,"init trace lo " + itos(iterlo->second) + " hi " + itos(iterhi->second));
          while (iterlo->second > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE))
          {
            output.logTab(1,TESTING,"iterlo++");
            if (iterlo == sampling.end()) break;
            iterlo++;
          }
          while (iterhi->second < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE))
          {
            output.logTab(1,TESTING,"iterhi--");
            if (iterhi == sampling.begin()) break;
            iterhi--;
          }
          output.logTab(1,TESTING,"trace lo " + dtos(iterlo->first) + " hi " + dtos(iterhi->first));
          output.logTab(1,TESTING,"trace lo " + itos(iterlo->second) + " hi " + itos(iterhi->second));
          if (iterhi == iterlo && iterhi == sampling.begin())
          {
            double tol = 1.0e-03;
            if (SAMP < TRACE.SAMP_MIN + tol)
            {
              ncyc = TRACE.SAMP_NCYC; //to force while to fail
              output.logTab(1,TESTING,"Sampling aborted, at minimum");
            }
            else
            {
              SAMP /= 2.;
              SAMP = std::min(SAMP,TRACE.SAMP_MIN);
              output.logTab(1,TESTING,"Sampling doubled");
            }
          }
          else if (iterhi == iterlo && iterhi == sampling.end())
          {
            SAMP *= 2.;
            output.logTab(1,TESTING,"Sampling halved");
          }
          else
          {
            SAMP = (iterlo->first + iterhi->first)/2.0;
            output.logTab(1,TESTING,"Sampling reset to half way");
          }
        }
        output.logBlank(TESTING);
      }
      ncyc++;
    }
    while ((trace.size() > (TRACE.SAMP_TARGET + TRACE.SAMP_RANGE) ||
            trace.size() < (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE)) &&
            ncyc < TRACE.SAMP_NCYC); //infinite loop trap
    output.logEllipsisEnd(LOGFILE);
    DIST = SAMP*hex_factor();
  }
  PHASER_ASSERT(DIST);
}

void TraceMol::setOCC(bool1D* OCCptr)
{
  if (OCCptr != NULL)
  {
    PHASER_ASSERT(SAMP_USED == "CALPHA" || SAMP_USED == "ALL");
    if (SAMP_USED == "CALPHA")
      for (unsigned a = 0; a < trace.size(); a++)
        trace[a].O = (*OCCptr)[calpha_lookup[a]];
    else
      for (unsigned a = 0; a < trace.size(); a++)
        trace[a].O = (*OCCptr)[a];
  }
  else //reset otherwise
  {
    for (unsigned a = 0; a < trace.size(); a++)
      trace[a].O = 1;
  }
}

//use asa in test
void TraceMol::trim_asa(std::string rootname,Molecule& Coords,data_pdb& PDB,Output& output)
{
  //data_trace& TRACE = PDB.TRACE;
  //data_map& MAP = PDB;
  std::vector<pdb_record>& pdbm = Coords.pdb[0];

  for (int a = pdbm.size()-1; a > 0; a--) //a==0 is the centre
  {
    if (!(pdbm[a].AltLoc == " " || pdbm[a].AltLoc == "A"))
      pdbm[a].O = 0;
  }

  if (output.level(TESTING))
  {
    std::string altlocstr("debug." + ntos(++npdb) + ".altloc." + rootname + ".pdb");
    output.logTab(1,TESTING,"Write " + altlocstr + " (" + itos(pdbm.size()) + ")");
    std::ofstream altpdb(altlocstr.c_str());
    for (int a = 0; a < pdbm.size(); a++)
    if (pdbm[a].O)
      altpdb << pdbm[a].Card();
    altpdb.close();
  }

  vdw atoms_vdw_table;
  int    sampling_point_count = 960;
  double probe = 1.4;
  std::vector<double> myradii(pdbm.size());
  for (int a = 0; a < pdbm.size(); a++)
    myradii[a] = pdbm[a].O ? atoms_vdw_table.lookup(pdbm[a].Element) : -999; //flag -999 to ignore atoms totally

  mmtbx::geometry::asa::calculator::SimpleCalculator<
    mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >,
    std::vector<double> // arrays can be used directly without adaptors
   >
   mycalc(
     mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >( pdbm ),
     myradii,
     probe, // probe radius
     sampling_point_count // sampling point count
     );

  int1D counts(101,0);

  double structure_asa(0.2);
  int ntrace(0);
  for (int a = 0; a < pdbm.size(); a++)
  if (pdbm[a].O)
  {
    double accessible_surface_area = mycalc.accessible_points(a)/double(sampling_point_count);
    counts[int(accessible_surface_area*100)]++;
    if (accessible_surface_area > structure_asa)// && !Coords.isBackboneAtom(0,a))
      pdbm[a].O = 0;//flag is faster than erase
    else ntrace++;
  }

  for (unsigned a = 0; a < counts.size(); a++)
    if (counts[a])
      output.logTab(1,TESTING,"histogram asa=" + dtos(a/100.) + " num=" + itos(counts[a]) +
                   std::string(a/100. > structure_asa ? " erased" :""));
  output.logTab(1,TESTING,"(other histogram bins empty)" );
  if (output.level(TESTING))
  {
    std::string asastr("debug." + ntos(++npdb) + ".asa." + rootname + ".pdb");
    output.logTab(1,TESTING,"Write " + asastr + " (" + itos(ntrace) + ")");
    std::ofstream trimpdb(asastr.c_str());
    for (int a = 0; a < pdbm.size(); a++)
    if (pdbm[a].O)
      trimpdb << pdbm[a].Card(a,a);
    trimpdb.close();
  }

}

void TraceMol::hexgrid(std::string rootname,Molecule& Coords,data_pdb& PDB,Output& output)
{
  data_trace& TRACE = PDB.TRACE;
  data_map& MAP = PDB;
  std::vector<pdb_record>& pdbm = Coords.pdb[0];
  std::vector<double> myradii(pdbm.size());
  vdw atoms_vdw_table;
  for (int a = 0; a < pdbm.size(); a++)
    myradii[a] = pdbm[a].O ? atoms_vdw_table.lookup(pdbm[a].Element) : -999; //flag -999 to ignore atoms totally

  mmtbx::geometry::asa::calculator::SimpleCalculator<
    mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >,
    std::vector<double> // arrays can be used directly without adaptors
   >
   hexcalc(
     mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >( pdbm ),
     myradii
     );

  int box_a(1);
  std::ofstream boxpdb;
  if (output.level(TESTING))
  {
    std::string boxstr("debug."+ ntos(++npdb) + ".box." + rootname + ".pdb");
    output.logTab(1,TESTING,"Write box points to " + boxstr);
    boxpdb.open(boxstr.c_str());
  }

  double hex_distance = SAMP*hex_factor();
  //concatenate hexgrid points onto structure
  //see if they are buried
  //MAP.PRINCIPAL_CENTRE != MAP.CENTRE
  //double molecule_extent = MAP.max_extent();
  dvect3 box_min = MAP.TRACE_CENTRE - MAP.TRACE_EXTENT/1.9;
  dvect3 box_max = MAP.TRACE_CENTRE + MAP.TRACE_EXTENT/1.9;
  dvect3 orthSite(0,0,0);
  dvect3 orthSamp = dvect3(SAMP,std::sqrt(3./4.)*SAMP,std::sqrt(2./3.)*SAMP);
  double off1 = 5.0/6.0;

  int ih(1);
  for (orthSite[2] = box_min[2]; orthSite[2] < box_max[2]; orthSite[2] += orthSamp[2])
  {
    off1 = 1. - off1;
    double off0 = 3.0/4.0;
    for (orthSite[1] = box_min[1] + off1 * orthSamp[1]; orthSite[1] < box_max[1]; orthSite[1] += orthSamp[1])
    {
      off0 = 1. - off0;
      for (orthSite[0] = box_min[0] + off0 * orthSamp[0]; orthSite[0] < box_max[0]; orthSite[0] += orthSamp[0])
      {
        if (output.level(TESTING))
        {
          pdb_record card;
          card.X = orthSite;
          boxpdb << card.Card(box_a,box_a);
          box_a++;
        }
        if (hexcalc.is_overlapping_sphere(orthSite,hex_distance))
        {
          std::string atomcard = "ATOM  ";
          char hybrid36_AtmNum[6]; //leave room for \0
          hy36encode(5,ih++,hybrid36_AtmNum);
          pdb_record next_atom;
          next_atom.ATOMorHETATMandNUM = atomcard + std::string(hybrid36_AtmNum,5);
          next_atom.ResNum = trace.size();
          next_atom.X = orthSite;
          trace.push_back(next_atom);
        }
      }
    }
  }
  if (output.level(TESTING))
  {
    boxpdb.close();
  }
  output.logEllipsisEnd(TESTING);

  if ((trace.size() > pdbm.size()) && (pdbm.size() > (TRACE.SAMP_TARGET - TRACE.SAMP_RANGE)))
  {
    output.logTab(1,TESTING,"Asa calculation aborted: unpruned trace > natoms");
    return; //can't be any good
  }

//now select only the ones that are buried
  double radius = SAMP*hex_factor(); //SAMP/1.9;
  double probe = radius/20.;
  int    sampling_point_count = 960;
  PHASER_ASSERT(radius);

  mmtbx::geometry::asa::calculator::ConstRadiusCalculator<
    mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >,
    double // arrays can be used directly without adaptors
   >
   mycalc(
     mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, pdb_getter >( trace ),
     radius,
     probe, // probe radius
     sampling_point_count,
     2*radius+probe //this is required
     );

  int1D counts(101,0);

  for (int a = 0; a < trace.size(); a++)
  {
    double accessible_surface_area = mycalc.accessible_points(a)/double(sampling_point_count);
    counts[int(accessible_surface_area*100)]++;
    trace[a].B = accessible_surface_area*100; //for coot output
    if (accessible_surface_area > TRACE.SAMP_ASA)
      trace[a].O = 0;
  }

  if (output.level(TESTING)) //write here so that asa is in coordinates
  {
    std::string unprunedstr("debug." + ntos(++npdb) + ".unpruned." + rootname + ".pdb");
    output.logTab(1,TESTING,"Write " + unprunedstr + " (" + itos(trace.size()) + ")");
    std::ofstream unprunedpdb(unprunedstr.c_str());
    for (unsigned a = 0; a < trace.size(); a++)
      unprunedpdb << trace[a].Card();
    unprunedpdb.close();
  }

  for (int a = trace.size()-1; a >= 0; a--)
  if (!trace[a].O) trace.erase(trace.begin()+a);

  output.logTab(1,TESTING,"Asa minimum: " + dtos(TRACE.SAMP_ASA));
  for (unsigned a = 0; a < counts.size(); a++)
  if (counts[a])
    output.logTab(1,TESTING,"histogram asa=" + dtos(a/100.) + " num=" + itos(counts[a]) +
                   std::string(a/100. > TRACE.SAMP_ASA ? " erased" :""));
  output.logTab(1,TESTING,"(other histogram bins empty)" );
  if (output.level(TESTING))
  {
    std::string buriedstr("debug." + ntos(++npdb) + ".buried." + rootname + ".pdb");
    output.logTab(1,TESTING,"Write " + buriedstr + " (" + itos(trace.size()) + ")");
    std::ofstream buriedpdb(buriedstr.c_str());
    for (unsigned a = 0; a < trace.size(); a++)
      buriedpdb << trace[a].Card();
    buriedpdb.close();
  }

  //renumber
  for (int a = 0; a < trace.size(); a++)
  {
    char hybrid36_AtmNum[6]; //leave room for \0
    hy36encode(5,a+1,hybrid36_AtmNum);
    trace[a].ATOMorHETATMandNUM = "ATOM  " + std::string(hybrid36_AtmNum,5);
    trace[a].ResNum = a+1;
  }

}

std::vector<pdb_record> TraceMol::trace_RT(dmat33& solR_ref,dvect3& orthSolT_ref)
{
  std::vector<pdb_record> tmp;
  for (int a = 0; a < trace.size(); a++)
  if (trace[a].O)
  {
    pdb_record tatom = trace[a];
    tatom.X = solR_ref*trace[a].X + orthSolT_ref;
    tmp.push_back(tatom);
  }
  return tmp;
}

} //phaser
