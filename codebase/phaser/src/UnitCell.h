//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __UnitCellClass__
#define __UnitCellClass__
#include <phaser/main/Phaser.h>
#include <phaser/io/Output.h>
#include <cctbx/uctbx.h>

namespace phaser {

class UnitCell
{
  public:
    UnitCell();
    UnitCell(cctbx::uctbx::unit_cell);
    UnitCell(af::double6);
    UnitCell(dvect3);
    virtual ~UnitCell(){}
    UnitCell(const UnitCell & init)
    { cctbxUC = cctbx::uctbx::unit_cell(init.cctbxUC); }
    const UnitCell& operator=(const UnitCell& right)
    {
      if (&right != this)   // check for self-assignment
        cctbxUC = cctbx::uctbx::unit_cell(right.cctbxUC);
      return *this;
    }

  private:
    cctbx::uctbx::unit_cell cctbxUC;

  public:
    //reset
    void setUnitCell(af::double6);
    void setUnitCellDouble6(af::double6 cell) { setUnitCell( cell ); } //boost python
    void setUnitCell(dvect3);
    void setUnitCell(const UnitCell& init)
    { cctbxUC = cctbx::uctbx::unit_cell(init.cctbxUC); }

    //parameters
    floatType A(),B(),C(),Alpha(),Beta(),Gamma(),cosAlpha(),cosBeta(),cosGamma();
    floatType aStar(),bStar(),cStar(),cosAlphaStar(),cosBetaStar(),cosGammaStar();
    floatType Param(int);
    af::double6 getCell6() const;
    const UnitCell& getUnitCell() const { return *this; }
    cctbx::uctbx::unit_cell  getCctbxUC();

    //frac/orth
    dmat33 Orth2Frac();
    dmat33 Frac2Orth();
    dvect3 doOrth2Frac(dvect3&);
    dvect3 doFrac2Orth(dvect3&);
    dmat33 doOrth2Frac(dmat33&);
    dmat33 doFrac2Orth(dmat33&);

    //ssqr/reso
    floatType Ssqr(dvect3&) const;
    floatType Ssqr(miller::index<int>&) const;
    floatType S(miller::index<int>&);
    floatType reso(miller::index<int>&);

    //other
    dvect3      HKLtoVecS(int&, int&, int&);
    ivect3      getMapLimits(floatType,floatType);
    void        logUnitCell(outStream,Output&,bool=true);
    std::string CrystCard(std::string,int);
};

}

#endif
