//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RefineBaseClass__
#define __RefineBaseClass__
#include <phaser/include/ProtocolBase.h>
#include <phaser/pod/bounds.h>
#include <phaser/pod/reparams.h>
#include <phaser/io/Output.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/fmat.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

//abstract - reminder! not objects of an abstract
//base class can be instantiated
class RefineBase  //abstract
{
  public:
    RefineBase() { npars_ref = npars_all = 0; refinePar.clear(); }
    virtual ~RefineBase() {}

    //pure virtual functions
    virtual double      targetFn() = 0;
    virtual double      gradientFn(TNT::Vector<double>&) = 0;
    virtual double      hessianFn(TNT::Fortran_Matrix<double>&,bool&) = 0;
    virtual void        applyShift(TNT::Vector<double>&) = 0;
    virtual bool1D      getRefineMask(protocolPtr) = 0;
    virtual std::string whatAmI(int&) = 0;
    virtual void        logCurrent(outStream,Output&) = 0;
    virtual void        logInitial(outStream,Output&) = 0;
    virtual void        logFinal(outStream,Output&) = 0;
    virtual TNT::Vector<double>  getRefinePars() = 0;
    virtual TNT::Vector<double>  getLargeShifts() = 0;

    //virtual functions
    virtual void        setUp();
    virtual void        cleanUp(outStream,Output&);
    virtual void        finalize(outStream,Output&);
    virtual void        rejectOutliers(outStream,Output&);
    virtual std::vector<reparams>    getRepar();
    virtual double      getMaxDistSpecial(TNT::Vector<double>&, TNT::Vector<double>&, bool1D&, TNT::Vector<double>&,floatType&);
    virtual void        logProtocolPars(outStream,Output&);
    virtual std::vector<bounds>    getLowerBounds();
    virtual std::vector<bounds>    getUpperBounds();
    virtual void        setProtocol(protocolPtr,Output&);
    virtual void        CalcDampedShift(floatType, TNT::Vector<floatType>&, TNT::Vector<floatType>&,
                        TNT::Vector<floatType>&,TNT::Vector<floatType>&,TNT::Vector<floatType>&);

    //functions
    int    numRefinePars();
    double getMaxDist(TNT::Vector<double>&, TNT::Vector<double>&, TNT::Vector<double>&);
    bool   fixHessian(TNT::Fortran_Matrix<double>&);
    void   studyParams(Output&);
    void   logVector(outStream,std::string,Output&,TNT::Vector<double>&);
    void   logHessian(outStream,Output&,TNT::Fortran_Matrix<double>&);
    void   reparBounds(std::vector<bounds>&);
    TNT::Vector<double> reparRefinePars(TNT::Vector<double>);
    TNT::Vector<double> reparRefineParsInv(TNT::Vector<double>);
    TNT::Vector<double> reparGradient(TNT::Vector<double>&,TNT::Vector<double>);
    void   reparHessian(TNT::Vector<double>&,TNT::Vector<double>&,TNT::Fortran_Matrix<double>&);
    void   reparLargeShifts(TNT::Vector<double>&);

  protected:
    int    npars_ref;
    int    npars_all;
    bool1D refinePar;
};

} //phaser

#endif
