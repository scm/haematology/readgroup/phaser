//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <limits>
#include <phaser/src/RefineBase2.h>
#include <phaser/io/Errors.h>
#include <phaser/io/Output.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/sgtbx/site_symmetry.h>

namespace phaser {

floatType RefineBase2::finiteDiffGradient(TNT::Vector<floatType>& Gradient, floatType fracLarge)
// Fraction of large shift to use for each type of function should be
// investigated by numerical tests, varying by, say, factors of two.
// Gradient is with respect to original parameters, so no reparameterisation is done
{
  floatType f(0),fplus(0),sz(0);
  TNT::Vector<floatType> x(npars_ref),old_x(npars_ref);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  old_x = getRefinePars();
  x = old_x;
  applyShift(x);
  f = targetFn();
  for (int i = 0; i < npars_ref; i++)
  {
    sz = fracLarge*largeShifts[i];
    x[i] = old_x[i] + sz;
    applyShift(x);
    fplus = targetFn();
    x[i] = old_x[i];
    Gradient[i] = (fplus - f)/sz;
  }
  applyShift(old_x);
  return f;
}

floatType RefineBase2::finiteGDiffHessian(TNT::Fortran_Matrix<floatType>& Hessian, floatType fracLarge)
// Fraction of large shift to use for each type of function should be
// investigated by numerical tests, varying by, say, factors of two.
{
  bool do_repar(false);
  floatType f(0),sz(0);
  Hessian.newsize(npars_ref,npars_ref);
  TNT::Vector<floatType> g(npars_ref),gplus(npars_ref),unrepar_g(npars_ref);
  TNT::Vector<floatType> x(npars_ref),old_x(npars_ref),unrepar_oldx(npars_ref);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  if (do_repar) reparLargeShifts(largeShifts);
  for (int i = 1; i <= npars_ref; i++)
  for (int j = 1; j <= npars_ref; j++)
    Hessian(i,j) = 0.;
  unrepar_oldx = getRefinePars();
  old_x = do_repar ? reparRefinePars(unrepar_oldx) : unrepar_oldx;
  x = old_x;
  f = gradientFn(unrepar_g);
  g = do_repar ? reparGradient(unrepar_oldx,unrepar_g) : unrepar_g;
  for (int i = 0; i < npars_ref; i++)
  {
    sz = fracLarge*largeShifts[i];
    x[i] = old_x[i] + sz;
    TNT::Vector<floatType> unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    gradientFn(unrepar_g);
    gplus = do_repar ? reparGradient(unrepar_x,unrepar_g) : unrepar_g;
    x[i] = old_x[i];
    for (int j = 0; j < npars_ref; j++)
    {
      if (i == j)
      {
        Hessian(i+1,i+1) = (gplus[i] - g[i])/sz;
      }
      else
      {
        Hessian(i+1,j+1) += (gplus[j] - g[j])/(2*sz);
        Hessian(j+1,i+1) += (gplus[j] - g[j])/(2*sz);
      }
    }
  }
  unrepar_oldx = do_repar ? reparRefineParsInv(old_x) : old_x;
  applyShift(unrepar_oldx);
  return f;
}

floatType RefineBase2::finiteGDiffDiagHessian(TNT::Fortran_Matrix<floatType>& Hessian, floatType fracLarge)
// Fraction of large shift to use for each type of function should be
// investigated by numerical tests, varying by, say, factors of two.
// This routine is inefficient, costing as much as finiteGDiffHessian
// to compute full Hessian, but may be useful for testing purposes.
{
  bool do_repar(false);
  Hessian.newsize(npars_ref,npars_ref);
  TNT::Vector<floatType> g(npars_ref),gplus(npars_ref),unrepar_g(npars_ref);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  if (do_repar) reparLargeShifts(largeShifts);
  for (int i = 1; i <= npars_ref; i++)
    for (int j = 1; j <= npars_ref; j++)
      Hessian(i,j) = 0.;
  TNT::Vector<floatType> unrepar_oldx = getRefinePars();
  TNT::Vector<floatType> old_x = do_repar ? reparRefinePars(unrepar_oldx) : unrepar_oldx;
  TNT::Vector<floatType> x = old_x;
  floatType f = gradientFn(unrepar_g);
  g = do_repar ? reparGradient(unrepar_oldx,unrepar_g) : unrepar_g;
  for (int i = 0; i < npars_ref; i++)
  {
    floatType sz = fracLarge*largeShifts[i];
    x[i] = old_x[i] + sz;
    TNT::Vector<floatType> unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    gradientFn(unrepar_g);
    gplus = do_repar ? reparGradient(unrepar_x,unrepar_g) : unrepar_g;
    gradientFn(gplus);
    x[i] = old_x[i];
    Hessian(i+1,i+1) = (gplus[i] - g[i])/sz;
  }
  unrepar_oldx = do_repar ? reparRefineParsInv(old_x) : old_x;
  applyShift(unrepar_oldx);
  return f;
}

floatType RefineBase2::finiteFDiffHessian(TNT::Fortran_Matrix<floatType>& Hessian, floatType fracLarge)
// Fraction of large shift to use for each type of function should be
// investigated by numerical tests, varying by, say, factors of two.
{
  bool do_repar(false);
  floatType f(0),fplusi(0),fminusi(0),fplusij(0),fplusj(0),szi(0),szj(0);
  Hessian.newsize(npars_ref,npars_ref);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  if (do_repar) reparLargeShifts(largeShifts);
  for (int i = 1; i <= npars_ref; i++)
  for (int j = 1; j <= npars_ref; j++)
    Hessian(i,j) = 0.;
  f = targetFn();
  TNT::Vector<floatType> unrepar_oldx = getRefinePars();
  TNT::Vector<floatType> old_x = do_repar ? reparRefinePars(unrepar_oldx) : unrepar_oldx;
  TNT::Vector<floatType> x = old_x;
  for (int i = 0; i < npars_ref; i++)
  {
    szi = fracLarge*largeShifts[i];
    x[i] = old_x[i] - szi;
    TNT::Vector<floatType> unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    fminusi = targetFn();
    x[i] = old_x[i] + szi;
    unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    fplusi = targetFn();
    x[i] = old_x[i];
    Hessian(i+1,i+1) = (fplusi - 2*f + fminusi)/(szi*szi);
    for (int j = i+1; j < npars_ref; j++)
    {
      x[i] = old_x[i] + szi;
      szj = fracLarge*largeShifts[j];
      x[j] = old_x[j] + szj;
      unrepar_x = do_repar ? reparRefineParsInv(x) : x;
      applyShift(unrepar_x);
      fplusij = targetFn();
      x[i] = old_x[i];
      unrepar_x = do_repar ? reparRefineParsInv(x) : x;
      applyShift(unrepar_x);
      fplusj = targetFn();
      x[j] = old_x[j];
      Hessian(i+1,j+1) = Hessian(j+1,i+1) = (fplusij - fplusi - fplusj + f)/(szi*szj);
    }
  }
  unrepar_oldx = do_repar ? reparRefineParsInv(old_x) : old_x;
  applyShift(unrepar_oldx);
  return f;
}

floatType RefineBase2::finiteFDiffDiagHessian(TNT::Fortran_Matrix<floatType>& Hessian, floatType fracLarge)
// Fraction of large shift to use for each type of function should be
// investigated by numerical tests, varying by, say, factors of two.
{
  bool do_repar(false);
  floatType f(0),fplus(0),fminus(0),sz(0);
  Hessian.newsize(npars_ref,npars_ref);
  TNT::Vector<floatType> x(npars_ref),old_x(npars_ref);
  TNT::Vector<floatType> largeShifts = getLargeShifts();
  if (do_repar) reparLargeShifts(largeShifts);
  for (int i = 1; i <= npars_ref; i++)
    for (int j = 1; j <= npars_ref; j++)
      Hessian(i,j) = 0.;
  f = targetFn();
  TNT::Vector<floatType> unrepar_oldx = getRefinePars();
  old_x = do_repar ? reparRefinePars(unrepar_oldx) : unrepar_oldx;
  x = old_x;
  for (int i = 0; i < npars_ref; i++)
  {
    sz = fracLarge*largeShifts[i];
    x[i] = old_x[i] + sz;
    TNT::Vector<floatType> unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    fplus = targetFn();
    x[i] = old_x[i] - sz;
    unrepar_x = do_repar ? reparRefineParsInv(x) : x;
    applyShift(unrepar_x);
    fminus = targetFn();
    x[i] = old_x[i];
    Hessian(i+1,i+1) = (fplus - 2*f + fminus)/(sz*sz);
  }
  unrepar_oldx = do_repar ? reparRefineParsInv(old_x) : old_x;
  applyShift(unrepar_oldx);
  return f;
}

void RefineBase2::CalcDampedShift(floatType a, TNT::Vector<floatType> &x,
      TNT::Vector<floatType> &oldx, TNT::Vector<floatType> &gs, TNT::Vector<floatType> &dist,
      TNT::Vector<floatType> &largeShifts)
{
  if (gs.size()) PHASER_ASSERT(dist.size() == gs.size());
  if (gs.size()) PHASER_ASSERT(largeShifts.size() == gs.size());
  if (gs.size()) PHASER_ASSERT(x.size() == gs.size());
  if (gs.size()) PHASER_ASSERT(oldx.size() == gs.size());
  for (int i = 0; i < gs.size(); i++)
  {
    floatType thisdist( (dist[i]<0.) ? a : std::min(a,dist[i]) ); // neg dist: unconstrained
    floatType rawshift(thisdist*gs[i]); // Undamped shift
    floatType relshift(std::max(std::fabs(rawshift),largeShifts[i])/largeShifts[i]);
    floatType dampfac = (relshift <= 1.) ? 1. : (1.+std::log(relshift))/relshift;
    floatType shift = dampfac*rawshift;
    x[i] = oldx[i] - shift; // Shift parameter only up to its bound, damped if > largeShift
  }
}

} //phaser
