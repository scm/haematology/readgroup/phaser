//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/TncsVar.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/solTerm.h>

namespace phaser {

TncsVar::TncsVar(data_tncs init) : data_tncs(init)
{
  PHASER_ASSERT(VAR.BINS.size());
  PHASER_ASSERT(VAR.BINS.size() == VAR.RESO.size());
}

floatType TncsVar::lowerVAR(int s)
{
  PHASER_ASSERT(VAR.BINS.size() == VAR.RESO.size());
  PHASER_ASSERT(s < VAR.RESO.size());
  floatType min_rmsd(0.1);
  floatType Drms(std::exp(-2*fn::pow2(scitbx::constants::pi*min_rmsd/VAR.RESO[s])/3.));
  return 1. - VAR.FRAC*fn::pow2(Drms);
}

floatType TncsVar::upperVAR(int s)
{
  PHASER_ASSERT(VAR.BINS.size() == VAR.RESO.size());
  PHASER_ASSERT(s < VAR.RESO.size());
  //floatType max_rmsd(2.0);
  //floatType Drms(std::exp(-2*fn::pow2(scitbx::constants::pi*max_rmsd/VAR.RESO[s])/3.));
  //return std::min(0.999999,1. - VAR.FRAC*fn::pow2(Drms));
  floatType nearly1(0.999999);
  return nearly1; // Allow only part of content to obey tNCS, so essentially no upper limit
}

floatType TncsVar::D(int s)
{
  PHASER_ASSERT(VAR.BINS.size());
  if (s < 0) return std::sqrt(1.0 - VAR.BINS[VAR.BINS.size()-1]);
  return std::sqrt(1.0 - VAR.BINS[s]);
}

floatType TncsVar::lower_var_limit()
{
  PHASER_ASSERT(VAR.BINS.size());
  floatType maxVAR(0);
  for (int s = 0; s < VAR.BINS.size(); s++)
    maxVAR = std::max(maxVAR,VAR.BINS[s]);
  return 1./std::sqrt(2.)*maxVAR;
}


} //phaser
