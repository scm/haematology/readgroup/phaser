//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DataAClass__
#define __DataAClass__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_refl.h>
#include <phaser/include/data_outl.h>
#include <phaser/include/data_norm.h>
#include <phaser/include/data_tncs.h>
#include <phaser/include/data_resharp.h>
#include <phaser/src/Bin.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/pod/ssqr_dfac_solt.h>
#include <scitbx/sym_mat3.h>
#include <cctbx/adptbx.h>

namespace phaser {
class DataA : public SpaceGroup, public UnitCell, public data_refl
{
  public:
    //Constructor and Destructor
    DataA(data_refl&,
          data_resharp&,
          data_norm&,
          data_outl&,
          data_tncs&);
    //unpickle constructor, backwards compabibility for phenix gui
    DataA(af::double6,
          data_refl&,
          data_resharp&,
          data_norm&,
          data_outl&);
    DataA() { NREFL = 0; }
    virtual ~DataA() { }

    //all base classes have deep copy constructors, for threads
    void setDataA(const DataA & init)
    {
      set_data_refl(init); // deep copy of af::shared for threads
      bin = init.bin;
      RESHARP = init.RESHARP;
      PTNCS = init.PTNCS;
      NREFL = init.NREFL;
      SIGMAN = init.SIGMAN;
      OUTLIER = init.OUTLIER;
      selected = init.selected.deep_copy();
      setUnitCell( init.getUnitCell() );
      setSpaceGroup( init.getSpaceGroup() );
      debugging = false;
    }
    DataA(const DataA & init):SpaceGroup(init),UnitCell(init),data_refl(init)
    { setDataA(init); }
   const DataA& operator=(const DataA& right) {
      if (&right != this) {
        setDataA(right);
        ((UnitCell &) *this) = right;
        ((SpaceGroup &) *this) = right;
        ((data_refl &) *this) = right;
      }
      return *this;
   }

    bool       debugging;
    data_norm  SIGMAN;
    data_outl  OUTLIER;

  protected:
    //Single value
    Bin        bin;  // Bins for each dataset
    data_resharp  RESHARP;
    data_tncs     PTNCS;
    unsigned   NREFL; //number of reflections

    // -- Data dependent on reflection [r]
    af_bool   selected;

  protected:
    floatType anisoTerm(const unsigned&,dmat6&);

  public:
    af::shared<miller::index<int> >   getMiller()     { return MILLER; }
    af_float    getF()          { return F; }
    af_float    getSIGF()       { return SIGF; }
    af_float    getI()          { return I; }
    af_float    getSIGI()       { return SIGI; }
    af_float    getDFAC()       { return DFAC; }
    std::string getLaboutF()    { if (F_ID == "") F_ID = "F"; return F_ID+"_ISO"; }
    std::string getLaboutSIGF() { if (SIGF_ID == "") SIGF_ID = "SIGF"; return SIGF_ID+"_ISO"; }
    std::string getLaboutI()    { if (I_ID == "") I_ID = "I"; return I_ID+"_ISO"; }
    std::string getLaboutSIGI() { if (SIGI_ID == "") SIGI_ID = "SIGI"; return SIGI_ID+"_ISO"; }
    af_bool     getSelected()   { return selected; }
    floatType   getWilsonK()    { return SIGMAN.WILSON_K; }
    floatType   getWilsonB()    { return SIGMAN.WILSON_B; }
    floatType   getIsoB();
    dmat6       getAniso()      { return SIGMAN.ANISO; }
    Bin         getBins()       { return bin; }
    data_norm   getSigmaN()     { return SIGMAN; }
    data_outl   getOutlier()    { return OUTLIER; }
    float1D     LoRes_array()   { return bin.LoRes_array(); }
    unsigned    NumInBin(const unsigned&);
    unsigned    rbin(const int&);  // bin number for reflection

    af::shared<miller::index<int> >   getSelectedMiller();
    af_float    getCorrection(bool=true);
    af_float    getCorrectedF(bool=true);
    af_float    getCorrectedSIGF(bool=true);
    af_float    getCorrectedI(bool=true);
    af_float    getCorrectedSIGI(bool=true);
    af_float    getBINS();
    floatType   getResharpB();
    floatType   HiRes();
    floatType   LoRes();
    floatType   fullHiRes();
    floatType   fullLoRes();
    dvect3      getEigenBs();
    floatType   getAnisoDeltaB();
    cctbx::adptbx::eigensystem<floatType> getEigenSystem();

    void        setAniso(const dmat6& value)    { SIGMAN.ANISO = value; }
    void        setWilsonK(const double& value) { SIGMAN.WILSON_K = value; }
    void        setWilsonB(const double& value) { SIGMAN.WILSON_B = value; }

    std::vector<ssqr_dfac_solt> getSelected(data_solpar&);
    std::vector<ssqr_dfac_solt> getPerfect(data_solpar&,double);
};

} //phaser

#endif
