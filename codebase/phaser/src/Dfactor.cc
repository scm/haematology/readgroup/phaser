//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Dfactor.h>
#include <cctbx/xray/targets/correlation.h>
#include <phaser/lib/matthewsProb.h>

namespace phaser {

Dfactor::Dfactor(data_refl& REFLECTIONS_,
                 data_resharp& RESHARP_,
                 data_norm& SIGMAN_,
                 data_outl& OUTLIER_,
                 data_tncs& PTNCS_,
                 data_bins& DATABINS_,
                 data_composition& COMPOSITION_,
                 double res1,double res2)
          : DataB(REFLECTIONS_,RESHARP_,SIGMAN_,OUTLIER_,PTNCS_,DATABINS_,COMPOSITION_,res1,res2)
{
  //for (unsigned r = 0; r < std::min(unsigned(10),NREFL); r++)
  //  std::cout << unparse(r) << std::endl;
}

void Dfactor::calcDfactor(Output& output)
{
  output.logTab(1,LOGFILE,"Calculate Luzzati D factors accounting for observational error...");
  output.logBlank(LOGFILE);
  //the DFAC array has been resized and set to 1 in the constructor of the DataB base class
  PHASER_ASSERT(DFAC.size() == NREFL);

  //don't do it code...
  if (Feff.size())
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (DFAC[r] != 1) return; //DFAC terms have already been set
  }

  bool is_FWamplitudes(false),is_FWintensities(false);
  af_float Idat,SIGIdat;
  if (INPUT_INTENSITIES)
  {
    Idat = I;
    SIGIdat = SIGI;
    is_FWintensities = is_FrenchWilsonI(I,SIGI,is_centric);
    if (is_FWintensities)
    {
      output.logTab(1,SUMMARY,"Data have been provided as French-Wilson intensities");
      output.logTab(1,SUMMARY,"Please rerun providing raw (unmassaged) intensities");
      throw PhaserError(INPUT,"","French-Wilson intensities are not allowed as input data");
    }
    else
      output.logTab(1,SUMMARY,"Data have been provided as raw intensities");
  }
  else
  {
    Idat = IfromF;
    SIGIdat = SIGIfromF;
    is_FWamplitudes = is_FrenchWilsonF(F,SIGF,is_centric);
    if (is_FWamplitudes)
      output.logTab(1,SUMMARY,"Data have been provided as French-Wilson amplitudes");
    else
      output.logTab(1,SUMMARY,"Data have been provided as raw amplitudes");
  }
  output.logBlank(SUMMARY);
  af_float eEobs;
  eEobs.resize(Idat.size());
  Feff.resize(Idat.size());

  for (unsigned r = 0; r < NREFL; r++)
  {
    floatType eEFW,eEsqFW;
    floatType normfac = 1./(SIGMAN.sqrt_epsnSN[r]*std::sqrt(PTNCS.EPSFAC[r]));
    if (!is_FWamplitudes) // Either intensities or unmassaged amplitudes (turned back into intensities)
    {
      floatType thisEsqr = Idat[r]*fn::pow2(normfac);
      floatType thisSigesq = SIGIdat[r]*fn::pow2(normfac);
      eEFW   = expectEFW(thisEsqr,thisSigesq,is_centric[r]);
      eEsqFW = expectEsqFW(thisEsqr,thisSigesq,is_centric[r]);
    }
    else // Deduce expected values from French-Wilson F and SIGF
    {
      eEFW = F[r]*normfac;
      floatType sige = SIGF[r]*normfac;
      eEsqFW = fn::pow2(eEFW) + fn::pow2(sige);
    }
    // Once DFAC < 0.05, DFAC/eEobs pairs give almost identical distributions
    // Keeping DFAC>0.05 gives more sensible looking eEobs values
    DFAC[r] = std::max(0.05,getDfactor(eEFW,eEsqFW,is_centric[r]));
    eEobs[r] = getEffectiveEobs(eEsqFW,DFAC[r]);
    if ((eEobs[r] > 10.) && (eEsqFW > 1.)) // Secondary condition: restrict to 10 and compute corresponding DFAC from equation (23) of paper
    {
      eEobs[r] = 10.;
      DFAC[r] = std::sqrt((eEsqFW-1.)/(fn::pow2(eEobs[r])-1.));
    }
    Feff[r] = eEobs[r]/normfac; // Put back on original scale
  }
  # if 0
  bool calc_fisher_info(false);
  if (getenv("PHASER_FISHERINFO_NATOMS") != NULL)
  {
    // Set up for Fisher information calculations
    double fracscat;
    data_solpar SOLPAR;
      SOLPAR.SIGA_FSOL = 0.07; // About 20% R-factor at 15A from solvent model deficiencies
      SOLPAR.SIGA_BSOL = 204.4;
    double MTZ_HIRES =
      cctbx::uctbx::d_star_sq_as_d(cctbx::uctbx::unit_cell(UNIT_CELL).min_max_d_star_sq(MILLER.const_ref())[1]);
    double rms = MTZ_HIRES/10.; // Optimistic value for RMSD at end of refinement for good data
    double rms_old = 0.;
    calc_fisher_info = true;
    double natoms = std::atof(getenv("PHASER_FISHERINFO_NATOMS"));
    if (natoms <= 0.) // Use default composition
    {
      double volume = cctbx::uctbx::unit_cell(UNIT_CELL).volume();
      double meanDa(volume/NSYMM/matthewsVM(HIRES,"PROTEIN")); //composition is per asu
      // Taking average protein composition in weight percent and converting to elemental composition
      // each Da contributes on average 0.0456 C, 0.0112 N, 0.0133 O and 0.0005 S, working out
      // to about one heavy (non-H) atom per 14.2 Da
      natoms = meanDa/14.2;
    }
    fracscat = 1./(SpaceGroup::NSYMP*natoms);
    double pisq(fn::pow2(scitbx::constants::pi));
    double FisherInfoMean;
    output.logSectionHeader(LOGFILE,"EXPECTED FISHER INFORMATION");
    output.logTab(1,LOGFILE,"Estimated number of atoms in a.u.: " + dtos(natoms));
    output.logTab(1,LOGFILE,"Starting rmsd for Fisher info: " + dtos(rms));
    output.logBlank(LOGFILE);
    int niter(0);
    while (niter < 100 && std::abs(rms_old-rms)/rms > 0.001)
    {
      niter++;
      dmat33 FisherInfoTensor;
      for (unsigned i = 0; i < 3; i++)
        for (unsigned j = 0; j < 3; j++)
          FisherInfoTensor(i,j) = 0.;
      for (unsigned r = 0; r < NREFL; r++)
      {
        double ssqr = UnitCell::Ssqr(MILLER[r]);
        double sigmaA = solTerm(ssqr,SOLPAR)*DLuzzati(ssqr,rms);
        double FisherInfoWt;
        if (is_centric[r])
          FisherInfoWt = expectedFisherWtCen(eEobs[r],DFAC[r],sigmaA);
        else
          FisherInfoWt = expectedFisherWtAcen(eEobs[r],DFAC[r],sigmaA);
        FisherInfoWt *= fracscat; // Account for fraction scattering for typical atom
        std::vector<miller::index<int> >  rhkl = rotMiller(r);
        for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
        {
          dvect3 HKLoverABC(rhkl[isym][0]/A(),rhkl[isym][1]/B(),rhkl[isym][2]/C());
          for (unsigned i = 0; i < 3; i++)
            for (unsigned j = 0; j < 3; j++)
              FisherInfoTensor(i,j) += FisherInfoWt * 4.*HKLoverABC[i]*HKLoverABC[j]*pisq;
        }
      }
      output.logTab(1,VERBOSE,"Cycle " + itos(niter));
      output.logTabPrintf(1,VERBOSE,"%15s %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n",
        "FisherInfo tensor: ",FisherInfoTensor(0,0),FisherInfoTensor(0,1),FisherInfoTensor(0,2),
        FisherInfoTensor(1,1),FisherInfoTensor(1,2),FisherInfoTensor(2,2));
      FisherInfoMean = (FisherInfoTensor(0,0)+FisherInfoTensor(1,1)+FisherInfoTensor(2,2))/3.;
      output.logTabPrintf(1,VERBOSE,"%17s %10.1f\n","FisherInfo mean: ",FisherInfoMean);
      rms_old = rms;
      double rms_diag = std::sqrt(1./FisherInfoTensor(0,0)+1./FisherInfoTensor(1,1)+1./FisherInfoTensor(2,2));
      output.logTab(1,VERBOSE,"RMS from FisherInfo diagonal: " + dtos(rms_diag));
      dmat33 FisherInverse = FisherInfoTensor.inverse();
      output.logTabPrintf(1,VERBOSE,"%15s %8.5f %8.5f %8.5f %8.5f %8.5f %8.5f\n",
        "FisherInfo inverse: ",FisherInverse(0,0),FisherInverse(0,1),FisherInverse(0,2),
        FisherInverse(1,1),FisherInverse(1,2),FisherInverse(2,2));
      // Probably should account for non-orthogonality here?
      // More importantly, should use this to make anisotropic sigmaA.
      rms = std::sqrt(FisherInverse(0,0)+FisherInverse(1,1)+FisherInverse(2,2));
      output.logTab(1,VERBOSE,"RMS from diagonal of FisherInfo inverse: " + dtos(rms));
      output.logBlank(VERBOSE);
    }
    output.logTab(1,LOGFILE,"Fisher information converged after " + itos(niter) + " cycles");
    output.logTabPrintf(1,LOGFILE,"%17s %10.1f\n","FisherInfo mean: ",FisherInfoMean);
    output.logTab(1,LOGFILE,"RMS from diagonal of FisherInfo inverse: " + dtos(rms));
    output.logBlank(LOGFILE);
  }
  # endif
}

} //end namespace phaser
