//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/DataB.h>
#include <phaser/src/Composition.h>
#include <phaser/src/TncsVar.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/round.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/best.h>
#include <scitbx/constants.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <scitbx/math/r3_rotation.h>
#include <cctbx/sgtbx/reciprocal_space_asu.h>
#include <cctbx/miller/asu.h>
#include <cstring>

namespace phaser {

DataB::DataB(data_refl& REFLECTIONS_,
             data_resharp& RESHARP_,
             data_norm& SIGMAN_,
             data_outl& OUTLIER_,
             data_tncs& PTNCS_,
             data_bins& DATABINS_,
             data_composition& COMPOSITION_,
             double res1,double res2)
      :DataA(REFLECTIONS_,RESHARP_,SIGMAN_,OUTLIER_,PTNCS_)
{
  checkData();
  TOTAL_SCAT_B = Composition(COMPOSITION_).total_scat(REFLECTIONS_.SG_HALL,REFLECTIONS_.UNIT_CELL);
  HIRES = std::min(res1,res2);
  LORES = std::max(res1,res2);
  sqrt_epsn.resize(NREFL);
  is_centric.resize(NREFL);
  LLwilson = 0;
  if (!PTNCS.EPSFAC.size()) PTNCS.EPSFAC.resize(NREFL,1);
  PHASER_ASSERT(PTNCS.EPSFAC.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (!PTNCS.use_and_present()) PTNCS.EPSFAC[r] = 1;
    sqrt_epsn[r] = std::sqrt(static_cast<floatType>(SpaceGroup::epsilon(MILLER[r])));
    is_centric[r] = SpaceGroup::is_centric(MILLER[r]);
  }
  if (!PTNCS.GFUNCTION_RADIUS)
  {
    cctbx::uctbx::unit_cell cctbxUC = UnitCell::getCctbxUC();
    floatType mol_volume(cctbxUC.volume()/4/SpaceGroup::NSYMM); //25% of asu volume
    PTNCS.GFUNCTION_RADIUS = std::pow(3*mol_volume/4/scitbx::constants::pi,1./3.);
  }

  // --- Binning ---
  bin = Bin(DATABINS_);
  bin.setup(HiRes(),LoRes(),NREFL);

  // --- Binning ---
  //This checks to see if any of the bins have no reflections
  //and makes sure there is at least one in every bin
  //This will override the NUM input option
  int1D NumInBin(bin.numbins());
  recount:
  if (bin.numbins() != 1) //if == 1 then do nothing - assume 1 reflection!
  {
    //count numbers in each bin
    for (unsigned s = 0; s < bin.numbins(); s++) NumInBin[s] =  0;
    for (unsigned r = 0; r < NREFL; r++)
    if (INPUT_INTENSITIES || (F[r] != 0)) // Input intensities or non-zero amplitude
    {
   // don't select or full resolution fails with empty bins
       NumInBin[bin.get_bin(S(MILLER[r]))]++;
    }
    for (unsigned s = 0; s < bin.numbins(); s++)
      if (NumInBin[s] == 0)
      {
        //reduce the number of bins by 1 and see if this helps
        int reduceBins = bin.numbins()-1;
        bin.set_numbins(reduceBins);
        goto recount;
      }
  }
  // --- store the binning
  PTNCS.set_variances(bin.LoRes_array());

  // --- Sigma N ---
  if (SIGMAN_.BINS.size() == bin.numbins() && SIGMAN_.sqrt_epsnSN.size() == NREFL)
  {
    SIGMAN = SIGMAN_;
  }
  else if (SIGMAN_.FILENAME.size() && SIGMAN_.READ)
  {
    SIGMAN = SIGMAN_;
    std::string msg = SIGMAN.read_file(MILLER);
    if (msg.size()) throw PhaserError(FATAL,"Error reading normalization binary file: incompatible with data\n" + msg);
  }
  else
  {
    SIGMAN = SIGMAN_;
    initialize_sigman();
    calculate_sigman_parameters();
  }
  calcOutliers(); //fixed after epsnSigmaN calculated
  calcSelected();
  initCos_Sin();
} //end constructor

void DataB::changeSpaceGroup(std::string spac,floatType HIRES,floatType LORES)
{
  if (spac.size() && spac != SpaceGroup::getHall())
    SpaceGroup::setSpaceGroupHall(spac);
  selectReso(HIRES,LORES); //include calcSelected, which includes systematic absences
}

floatType DataB::binAnisoFactor(const unsigned& r,floatType& bin,dmat6& anisoBeta,floatType& solK,floatType& solB,floatType WilsonK_I)
{
  if (!solK) return best(r)*bin*anisoTerm(r,anisoBeta)*WilsonK_I;
  return best(r)*bin*(1.0-solK*std::exp(-solB*ssqr(r)/4.0))*anisoTerm(r,anisoBeta)*WilsonK_I;
}

floatType DataB::binIsoFactor(const double& s_sqr,floatType& bin,double& isoB,floatType& solK,floatType& solB,floatType WilsonK_I)
{
  double isoTerm = std::exp(-isoB*s_sqr/2.);
  if (!solK) return best(s_sqr)*bin*isoTerm*WilsonK_I;
  return best(s_sqr)*bin*(1.0-solK*std::exp(-solB*s_sqr/4.0))*isoTerm*WilsonK_I;
}

unsigned DataB::NumInBinAC(const unsigned& s)
{
  int numInBin(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r] && !cent(r) && rbin(r) == s) numInBin++;
  return numInBin;
}

unsigned DataB::NumInBinC(const unsigned& s)
{
  int numInBin(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r] && cent(r) && rbin(r) == s) numInBin++;
  return numInBin;
}

unsigned DataB::NumAC()
{
  int numAC(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r] && !cent(r)) numAC++;
  return numAC;
}

unsigned DataB::NumC()
{
  int numC(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r] && cent(r)) numC++;
  return numC;
}

floatType DataB::maxSsqr()
{
  floatType maxSsqr(0);
  for (unsigned r = 0; r < NREFL; r++) if (selected[r]) maxSsqr = std::max(maxSsqr,Ssqr(MILLER[r]));
  return maxSsqr;
}

void DataB::calcOutliers()
{
  // Put general test for presence of French-Wilson intensities here for lack of a better place now
  static bool is_FWintensities = INPUT_INTENSITIES ? is_FrenchWilsonI(I,SIGI,is_centric) : false;
  if (is_FWintensities)
    throw PhaserError(INPUT,"","French-Wilson intensities are not allowed as input data\nPlease rerun providing raw (unmassaged) intensities");

  OUTLIER.ANO.clear();
  if (OUTLIER.REJECT)
  {
    af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
    af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
    bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                           is_FrenchWilsonF(F,SIGF,is_centric);
/*  // Code that can be uncommented to test the option of culling brightest intensities
  floatType Imax;
  if (getenv("PHASER_CULL_BRIGHT") != NULL)
  {
    std::vector<floatType> Icopy;
    for (unsigned r = 0; r < NREFL; r++)
      Icopy.push_back(Idat[r]);
    std::sort(Icopy.begin(),Icopy.end());
    Imax = Icopy[int(NREFL*(1-std::atof(getenv("PHASER_CULL_BRIGHT"))))];
  }
*/
    for (unsigned r = 0; r < NREFL; r++)
    {
      probTypeANO outlier(r);
      PHASER_ASSERT((SIGMAN.sqrt_epsnSN[r] > 0) && (PTNCS.EPSFAC[r] > 0));
      // For French-Wilson, estimate I as <F^2> = <F>^2 + SIGF^2
      // Safer to reject F-W outliers without allowance for measurement error
      floatType thisI  = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                           Idat[r];
      outlier.eosqr   = thisI/fn::pow2(SIGMAN.sqrt_epsnSN[r])/PTNCS.EPSFAC[r];
      outlier.sigesqr = is_FWamplitudes ? 0. :
                         SIGIdat[r]/fn::pow2(SIGMAN.sqrt_epsnSN[r])/PTNCS.EPSFAC[r];
      if (outlier.eosqr<0. && outlier.sigesqr<=0.)
      throw PhaserError(FATAL,"Negative observed intensities must have positive sigmas");
      if (!INPUT_INTENSITIES && (F[r] == 0)) // Reject input amplitudes of zero
      {
        outlier.prob = 0.;
      }
      else if (INPUT_INTENSITIES && (SIGI[r] <= 0)) // Reject input intensities with non-pos sigma
        {
          outlier.prob = 0.;
        }
      else
      {
        if (cent(r))
        {
          if (outlier.eosqr < 0.) // Check for too negative
            outlier.prob = pSmallerEosqCen(outlier.eosqr,outlier.sigesqr);
          else if (outlier.eosqr >= 0.) // Check for too large
            outlier.prob = pLargerEosqCen(outlier.eosqr,outlier.sigesqr);
          if (outlier.sigesqr > OUTLIER.SIGESQ_CENT) // Omit reflections with expected information below target
          {
            outlier.lowInfo = true;
          }
        }
        else // acentric case
        {
          if (outlier.eosqr < 0.) // Too negative?
            outlier.prob = pSmallerEosqAcen(outlier.eosqr,outlier.sigesqr);
          else if (outlier.eosqr >= 0.) // Too large?
            outlier.prob = pLargerEosqAcen(outlier.eosqr,outlier.sigesqr);
          if (outlier.sigesqr > OUTLIER.SIGESQ_ACENT) // Different threshold for acentrics
          {
            outlier.lowInfo = true;
          }
        }
      }
      //if (prob <= OUTLIER.PROB || Idat[r] > Imax) // code for cull bright
      outlier.wilson = (outlier.prob <= OUTLIER.PROB);
      if (outlier.wilson || outlier.lowInfo)
      {
        OUTLIER.ANO.push_back(outlier);
      }
    }
  }
}

void DataB::logInfoBits(outStream where,bool print_loggraph, Output& output)
{
  output.logSectionHeader(where,"DATA INFORMATION CONTENT");
  if (!INPUT_INTENSITIES)
  {
    output.logTab(1,where,"Information can only be estimated for intensity data");
    return;
  }
  float1D bininfo(bin.numbins(),0.);
  int1D ninbin(bin.numbins(),0),nselbin(bin.numbins(),0);
  double infobits(0.);
  int nsel(0),n0p01(0),n0p05(0),n0p1(0),n0p5(0),n1(0),n2(0);
  for (unsigned r = 0; r < NREFL; r++)
  {
    int s = rbin(r);
    ninbin[s]++;
    if (selected[r])
    {
      double eosqr   = I[r]/   fn::pow2(SIGMAN.sqrt_epsnSN[r])/PTNCS.EPSFAC[r];
      double sigesqr = SIGI[r]/fn::pow2(SIGMAN.sqrt_epsnSN[r])/PTNCS.EPSFAC[r];
      double thisInfo = (cent(r)) ?
        EosqInfoBitsCen(eosqr,sigesqr) :
        EosqInfoBitsAcen(eosqr,sigesqr);
      infobits += thisInfo;
      nsel++;
      if (thisInfo > 0.01)
      {
        n0p01++;
        if (thisInfo > 0.05)
        {
          n0p05++;
          if (thisInfo > 0.1)
          {
            n0p1++;
            if (thisInfo > 0.5)
            {
              n0p5++;
              if (thisInfo > 1.)
              {
                n1++;
                if (thisInfo > 2.)
                  n2++;
              }
            }
          }
        }
      }
      bininfo[s] += thisInfo;
      nselbin[s]++;
    }
  }
  output.logTab(1,where,"Estimate of information depends on:");
  output.logTab(2,where,"accuracy of experimental sigmas");
  output.logTab(2,where,"accuracy of anisotropy and tNCS parameters");
  output.logTab(2,where,"status of twinning (which reduces information)");
  output.logBlank(where);
  output.logTab(1,where,"Estimated total information = " +
         dtos(infobits) + " bits, or " + dtos(infobits*std::log(2.)) +
         " nats in " + itos(nsel) + " reflections");
  if (nsel)
  {
    output.logTab(2,where,"average of " + dtos(infobits/nsel) +
         " bits per reflection");
    output.logTab(1,where,"Number of reflections exceeding information threshold:");
    output.logTab(2,where,"0.01 bit:  " + itos(n0p01));
    output.logTab(2,where,"0.05 bit:  " + itos(n0p05));
    output.logTab(2,where,"0.1  bit:  " + itos(n0p1));
    output.logTab(2,where,"0.5  bit:  " + itos(n0p5));
    output.logTab(2,where,"1    bit:  " + itos(n1));
    output.logTab(2,where,"2    bits: " + itos(n2));
  }
  output.logBlank(where);

  if (!print_loggraph) return;

  std::string loggraph;
  for (unsigned s = 0; s < bin.numbins(); s++)
  if (nselbin[s])
  {
    loggraph += dtos(1.0/fn::pow2(bin.MidRes(s)),5,4) + " " +
                dtos(bininfo[s]/nselbin[s]) + " " +
                dtos(bininfo[s]/ninbin[s]) +
                "\n";
  }
  string1D graphs(1);
  graphs[0] = "Info (bits) per reflection vs resolution:AUTO:1,2,3";
  output.logGraph(where,"Information content of data",graphs,"1/d^2 Selected All",loggraph);
}

void DataB::logOutliers(outStream where,Output& output)
{
  output.logUnderLine(where,"Outlier Rejection");
  if (OUTLIER.ANO.size())
  {
    floatType percent = 100.*floatType(OUTLIER.ANO.size())/NREFL;
    OUTLIER.ANO.size() == 1 ?
    output.logTab(1,where,"There was 1 reflection of " + itos(NREFL) + " (" + dtos(percent,4) + "%) rejected as an outlier"):
    output.logTab(1,where,"There were " + itos(OUTLIER.ANO.size()) + " reflections of " + itos(NREFL) + " (" + dtos(percent,4) + "%) rejected as outliers");
    output.logTab(2,where,"- outliers with a Wilson probability less than " + dtos(OUTLIER.PROB));
    output.logTab(2,where,"- measurements expected to contain fewer than " + dtos(OUTLIER.INFO) + " bits of information");
    output.logTab(3,VERBOSE,"threshold sigma(Eobs^2) for centrics/acentrics is " + dtos(OUTLIER.SIGESQ_CENT) + " / " + dtos(OUTLIER.SIGESQ_ACENT));
    output.logBlank(where);
    int maxprint(20);
    output.logTabPrintf(1,where,"%4s %4s %4s %6s  %10s  %10s %11s  %6s %8s\n","H","K","L","reso","Eo^2","sigma","probability","wilson","low-info");
    for (int o = 0; o < OUTLIER.ANO.size(); o++)
    {
      int& r = OUTLIER.ANO[o].refl;
      int H(phaser::round(MILLER[r][0]));
      int K(phaser::round(MILLER[r][1]));
      int L(phaser::round(MILLER[r][2]));
      if (o == maxprint) output.logTab(1,where,"More than " + itos(maxprint) + " outliers (see VERBOSE output)");
      if (std::fabs(OUTLIER.ANO[o].eosqr) < 1.0e+7 && std::fabs(OUTLIER.ANO[o].sigesqr) < 1.0e+7)
        output.logTabPrintf(1,o>=maxprint?VERBOSE:where,"%4i %4i %4i %6.2f % 11.3f % 11.3f %11.3e  %-6s %-8s\n", H,K,L,reso(r),OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,btos(OUTLIER.ANO[o].wilson).c_str(),btos(OUTLIER.ANO[o].lowInfo).c_str());
      else
        output.logTabPrintf(1,o>=maxprint?VERBOSE:where,"%4i %4i %4i %6.2f % 11.6e % 11.6e %11.3e  %-6s %-8s\n", H,K,L,reso(r),OUTLIER.ANO[o].eosqr,OUTLIER.ANO[o].sigesqr,OUTLIER.ANO[o].prob,btos(OUTLIER.ANO[o].wilson).c_str(),btos(OUTLIER.ANO[o].lowInfo).c_str());
    }
  }
  else output.logTab(1,where,"No reflections are outliers");
  output.logBlank(where);
}

void DataB::logDataStats(outStream where,Output& output)
{
  floatType mtz_hires(DEF_LORES),mtz_lores(0);
  for (unsigned r = 0; r < NREFL; r++)
    mtz_hires = std::min(mtz_hires,UnitCell::reso(MILLER[r])),
    mtz_lores = std::max(mtz_lores,UnitCell::reso(MILLER[r]));
  af::shared<miller::index<int> > sel = getSelectedMiller();
  output.logTabPrintf(1,where,"Resolution of All Data (Number):      %6.2f %6.2f (%d)\n",mtz_hires,mtz_lores,NREFL);
  output.logTabPrintf(1,where,"Resolution of Selected Data (Number): %6.2f %6.2f (%d)\n",HiRes(),LoRes(),sel.size());
  TncsVar tncsvar(PTNCS);
  if (PTNCS.use_and_present())
  {
    output.logTab(1,where,"Translational NCS:");
    output.logTab(2,where,"Vector = " + dvtos(PTNCS.TRA.VECTOR,7,4));
    output.logTab(2,where,"Rotation = " + dvtos(PTNCS.ROT.ANGLE,7,4));
    int m = PTNCS.VAR.BINS.size()-1;
    output.logTab(2,where,"D values (lores-hires)= " + dtos(tncsvar.D(0),7,4) + "-" + dtos(tncsvar.D(m),7,4));
    output.logBlank(where);
  }
  output.logBlank(where);
}

void DataB::logDataStatsExtra(outStream where,unsigned nrefl,Output& output)
{
  output.logUnderLine(where,"Extra reflection statistics");
  int ncent(0),nacent(0),ncentsel(0),nacentsel(0);
  unsigned r(0),rcount(0);
  float1D meanE2(bin.numbins(),0.);
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  for (r = 0; r < NREFL; r++)
  {
    cent(r) ? ncent++ : nacent++;
    if (selected[r])
    {
      cent(r) ? ncentsel++ : nacentsel++;
      unsigned s = rbin(r);
      floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
      if (PTNCS.use_and_present()) epsnSigmaN *= PTNCS.EPSFAC[r];
      floatType thisI;
      if (INPUT_INTENSITIES)
        thisI = I[r];
      else
      {
        thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                  IfromF[r];
      }
      floatType Esqr = thisI/epsnSigmaN;
      meanE2[s] += Esqr;
    }
  }
  output.logTab(1,where,"Number of centric reflections selected:  " + itos(ncentsel) + " of " + itos(ncent));
  output.logTab(1,where,"Number of acentric reflections selected: " + itos(nacentsel) + " of " + itos(nacent));
  output.logTabPrintf(1,where,"%3s    %10s      %8s   %5s\n","Bin","Resolution","Refl/bin","<E^2>");
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (NumInBin(s)) meanE2[s] /= NumInBin(s);
    if (bin.LoRes(s) > HiRes())
      output.logTabPrintf(1,where,"%3d  %6.2f - %5.2f %10d  %7.3f\n",
                      s+1, bin.LoRes(s), bin.HiRes(s), NumInBin(s), meanE2[s]);
  }
  output.logBlank(where);
  output.logTab(1,where,"First " + itos(nrefl) + " selected CENTRIC reflections:");
  output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s %12s %12s \n","H","K","L","(refl#)","epsn","reso",INPUT_INTENSITIES?"I":"F",INPUT_INTENSITIES ? "SIGI":"SIGF");
  r = rcount = 0;
  while (rcount < nrefl && r < NREFL)
  {
    if (cent(r) && selected[r])
    {
      int H(phaser::round(MILLER[r][0]));
      int K(phaser::round(MILLER[r][1]));
      int L(phaser::round(MILLER[r][2]));
      output.logTabPrintf(1,where,"% 3d % 3d % 3d (%5d) %4.0f  %5.2f %12.3f %12.3f\n",H,K,L,r+1,epsn(r),reso(r),INPUT_INTENSITIES?I[r]:F[r],INPUT_INTENSITIES?SIGI[r]:SIGF[r]);
      rcount++;
    }
    r++;
  }
  if (rcount == 0)
    output.logTab(1,where,"No centric reflections");
  output.logBlank(where);

  output.logTab(1,where,"First " + itos(nrefl) + " selected ACENTRIC reflections:");
  output.logTabPrintf(1,where,"%-3s %-3s %-3s %-7s %-4s %5s %12s %12s \n","H","K","L","(refl#)","epsn","reso",INPUT_INTENSITIES?"I":"F",INPUT_INTENSITIES ? "SIGI":"SIGF");
  r = rcount = 0;
  while (rcount < nrefl && r < NREFL)
  {
    if (!cent(r) && selected[r])
    {
      int H(phaser::round(MILLER[r][0]));
      int K(phaser::round(MILLER[r][1]));
      int L(phaser::round(MILLER[r][2]));
      output.logTabPrintf(1,where,"% 3d % 3d % 3d (%5d) %4.0f  %5.2f %12.3f %12.3f\n",H,K,L,r+1,epsn(r),reso(r),INPUT_INTENSITIES?I[r]:F[r],INPUT_INTENSITIES?SIGI[r]:SIGF[r]);
      rcount++;
    }
    r++;
  }
  if (rcount == 0)
    output.logTab(1,where,"No acentric reflections");
  output.logBlank(where);
}

void DataB::set_twin_bin_reso()
{
  // Don't do anything special here for French-Wilson data
  MOMENTS.max_s = bin.numbins();
  MOMENTS.hires = bin.HiRes(bin.numbins());
  bool use_xtriage_data_selection = true;
  if (use_xtriage_data_selection)
  {
    af_float    Idat = INPUT_INTENSITIES ?    I :    IfromF;
    af_float SIGIdat = INPUT_INTENSITIES ? SIGI : SIGIfromF;
    float1D IonSIGI(bin.numbins(),0);
    float1D NumInBin(bin.numbins(),0);
    double maxSIGI(0.);
    for (int r = 0; r < NREFL; r++)
    {
      if (selected[r])
      {
        maxSIGI = std::max(maxSIGI,SIGIdat[r]);
        if (SIGIdat[r])
        {
          if (Idat[r]/SIGIdat[r] > 3) IonSIGI[rbin(r)]++;
          NumInBin[rbin(r)]++;
        }
      }
    }
    //bail out if all the SIGIs are zero
    if (maxSIGI == 0) return;
    for (MOMENTS.max_s = 0; MOMENTS.max_s < bin.numbins(); MOMENTS.max_s++)
    {
      if (NumInBin[MOMENTS.max_s])
      {
        if (IonSIGI[MOMENTS.max_s]/NumInBin[MOMENTS.max_s] < 0.85 && bin.HiRes(MOMENTS.max_s) < 3.5) break; //xtriage limits
        MOMENTS.hires = bin.HiRes(MOMENTS.max_s); //the bin before break
      }
    }
  }
}

void DataB::calcMoments(Output& output)
{
  // For overall statistics, restrict data to resolution range where measurement
  // error will not affect the moments significantly, exclude individual reflections
  // where error would dominate, and ignore error.
  // For bin-wise statistics, account for expected effect of measurement error.
  MOMENTS.zero();
  set_twin_bin_reso();
  int ncentsel(0),nacentsel(0);
  floatType sumwt2_cent(0.),sumwt2_acent(0.),sumwt4_cent(0.),sumwt4_acent(0.);
  int1D ncentsel_bin(bin.numbins(),0);
  int1D nacentsel_bin(bin.numbins(),0);
  float1D sumwt2_cent_bin(bin.numbins(),0.), sumwt4_cent_bin(bin.numbins(),0.);
  float1D sumwt2_acent_bin(bin.numbins(),0.),sumwt4_acent_bin(bin.numbins(),0.);
  unsigned r(0);
  af_float    Idat     = INPUT_INTENSITIES ?    I :    IfromF;
  af_float SIGIdat     = INPUT_INTENSITIES ? SIGI : SIGIfromF;
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  MOMENTS.meanE2_cent_bin.resize(bin.numbins());
  MOMENTS.meanE4_cent_bin.resize(bin.numbins());
  MOMENTS.expectE4_cent_bin.resize(bin.numbins());
  MOMENTS.meanE2_acent_bin.resize(bin.numbins());
  MOMENTS.meanE4_acent_bin.resize(bin.numbins());
  MOMENTS.expectE4_acent_bin.resize(bin.numbins());
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (r = 0; r < NREFL; r++)
  {
    if (selected[r])
    {
      int s = rbin(r);
      floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
      //if (PTNCS.use_and_present())
      epsnSigmaN *= PTNCS.EPSFAC[r];
      // For French-Wilson, turn <F> and variance into <F^2> to estimate I
      // but keep SIGIdat as surrogate for true intensity sigma
      floatType    thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                             Idat[r];
      floatType    Esqr(thisI/epsnSigmaN);
      floatType SigEsqr(SIGIdat[r]/epsnSigmaN);
      floatType VarEsqr(fn::pow2(SigEsqr));
      floatType E4(fn::pow2(Esqr));
      if (cent(r))
      {
        floatType wt2(1./(2.+VarEsqr));
        floatType wt4(1./(96.+12*VarEsqr+2*fn::pow2(VarEsqr)));
        if (rbin(r) < MOMENTS.max_s)
        {// Overall moments for data not dominated by errors
          ncentsel++;
          sumwt2_cent += wt2;
          sumwt4_cent += wt4;
          MOMENTS.meanE2_cent += wt2*Esqr;
          MOMENTS.meanE4_cent += wt4*E4;
          MOMENTS.expectE4_cent += wt4*(3. + VarEsqr);
        }
        ncentsel_bin[s]++; // Binwise stats account for errors
        sumwt2_cent_bin[s] += wt2;
        sumwt4_cent_bin[s] += wt4;
        MOMENTS.meanE2_cent_bin[s] += wt2*Esqr;
        MOMENTS.meanE4_cent_bin[s] += wt4*E4;
        MOMENTS.expectE4_cent_bin[s] += wt4*(3. + VarEsqr);
      }
      else
      {
        floatType wt2(1./(1.+VarEsqr));
        floatType wt4(1./(20.+8*VarEsqr+2*fn::pow2(VarEsqr)));
        if (rbin(r) < MOMENTS.max_s)
        {// Overall moments for data not dominated by errors
          nacentsel++;
          sumwt2_acent += wt2;
          sumwt4_acent += wt4;
          MOMENTS.meanE2_acent += wt2*Esqr;
          MOMENTS.meanE4_acent += wt4*E4;
          MOMENTS.expectE4_acent += wt4*(2. + VarEsqr);
        }
        nacentsel_bin[s]++; // Binwise stats account for errors
        sumwt2_acent_bin[s] += wt2;
        sumwt4_acent_bin[s] += wt4;
        MOMENTS.meanE2_acent_bin[s] += wt2*Esqr;
        MOMENTS.meanE4_acent_bin[s] += wt4*E4;
        MOMENTS.expectE4_acent_bin[s] += wt4*(2. + VarEsqr);
      }
    }
  }
  if (ncentsel)
  {
    MOMENTS.meanE2_cent /= sumwt2_cent;
    MOMENTS.meanE4_cent /= sumwt4_cent;
    MOMENTS.expectE4_cent /= sumwt4_cent;
    MOMENTS.sigmaE4_cent = std::sqrt(1./sumwt4_cent);
    for (int s = 0; s < bin.numbins(); s++)
    if (ncentsel_bin[s])
    {
      MOMENTS.meanE2_cent_bin[s]   /= sumwt2_cent_bin[s];
      MOMENTS.meanE4_cent_bin[s]   /= sumwt4_cent_bin[s];
      MOMENTS.expectE4_cent_bin[s] /= sumwt4_cent_bin[s];
    }
    else MOMENTS.expectE4_cent_bin[s] = 3.; // Something sensible for empty bins
  }
  else
    MOMENTS.expectE4_cent = 3.;

  if (nacentsel)
  {
    MOMENTS.meanE2_acent /= sumwt2_acent;
    MOMENTS.meanE4_acent /= sumwt4_acent;
    MOMENTS.expectE4_acent /= sumwt4_acent;
    MOMENTS.sigmaE4_acent = std::sqrt(1./sumwt4_acent);
    for (int s = 0; s < bin.numbins(); s++)
    if (nacentsel_bin[s])
    {
      MOMENTS.meanE2_acent_bin[s]   /= sumwt2_acent_bin[s];
      MOMENTS.meanE4_acent_bin[s]   /= sumwt4_acent_bin[s];
      MOMENTS.expectE4_acent_bin[s] /= sumwt4_acent_bin[s];
    }
    else MOMENTS.expectE4_acent_bin[s] = 2.;
  }
  else
    MOMENTS.expectE4_acent = 2.;

  output.logTab(1,DEBUG,"Statistics by resolution include all data and account for measurement error");
  output.logTab(1,DEBUG,"2nd Moment Centric (expected) / Acentric (expected):");
  for (int s = 0; s < bin.numbins(); s++)
  {
    output.logTab(2,DEBUG,"Bin # " + std::string(s+1<10?" ":"") + itos(s+1) + " " +
              "(" + dtos(bin.LoRes(s),5,2) + "-" + dtos(bin.HiRes(s),5,2) + ") " +
              dtos(MOMENTS.meanE4_cent_bin[s],6,2)  + " (" + dtos(MOMENTS.expectE4_cent_bin[s],5,2) + ") / " +
              dtos(MOMENTS.meanE4_acent_bin[s],6,2) + " (" + dtos(MOMENTS.expectE4_acent_bin[s],5,2) + ")" +
              " #(" + itos(ncentsel_bin[s]) + "/" + itos(nacentsel_bin[s]) + ") ");
  }
  output.logBlank(DEBUG);
}

void DataB::logMoments(std::string description,outStream where,bool print_loggraph,Output& output)
{
  output.logUnderLine(where,"Weighted Intensity Moments" + description);
  calcMoments(output);
  output.logTab(1,where,"Inverse variance-weighted 2nd Moment = <wE^4>/<wE^2>^2 == <wJ^2>/<wJ>^2");
  output.logTab(1,where,"                     Untwinned   Perfect Twin");
  output.logTab(1,where,"2nd Moment  Centric:    3.0          2.0");
  output.logTab(1,where,"2nd Moment Acentric:    2.0          1.5");
  output.logTab(1,where,"                            Measured");
  if (MOMENTS.meanE2_cent) //will be zero if no centric reflection
  {
    output.logTab(1,DEBUG,"<E^2> for  Centric: " + dtos(MOMENTS.meanE2_cent,7,4) + " (check=1)");
    output.logTab(1,where,"2nd Moment  Centric:          " + dtos(MOMENTS.meanE4_cent,7,2));
  }
  if (MOMENTS.meanE2_acent) //will be zero if no acentric reflections
  {
    output.logTab(1,DEBUG,"<E^2> for Acentric " + dtos(MOMENTS.meanE2_acent,7,4) + " (check=1)");
    output.logTab(1,where,"2nd Moment Acentric:          " + dtos(MOMENTS.meanE4_acent,7,2));
  }
  output.logBlank(where);
  output.logTab(1,where,"Resolution for Twin Analysis (85\% I/SIGI > 3): " + dtos(MOMENTS.hires,5,2) + "A (HiRes=" + dtos(HiRes(),5,2) + "A)");
  output.logBlank(where);

  if (!print_loggraph) return;

  int ncentsel(0),nacentsel(0);
  unsigned r(0);
  float1D cumulative_centric(51,0),cumulative_acentric(51,0);
  double deltax=0.04;
  af_float Idat = INPUT_INTENSITIES ? I : IfromF;
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  bool is_FWamplitudes = INPUT_INTENSITIES ? false :
                         is_FrenchWilsonF(F,SIGF,is_centric);
  for (r = 0; r < NREFL; r++)
  {
    if (selected[r] && rbin(r) < MOMENTS.max_s)
    {
      int s = rbin(r);
      floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
      //if (PTNCS.use_and_present())
      epsnSigmaN *= PTNCS.EPSFAC[r];
      floatType thisI = is_FWamplitudes ? fn::pow2(F[r]) + fn::pow2(SIGF[r]) :
                                          Idat[r];
      floatType Esqr = thisI/epsnSigmaN;
      //floatType E4(fn::pow2(Esqr));
      if (cent(r))
      {
        ncentsel++;
        for (int s = 50; s >= std::ceil(std::max(0.,Esqr)/deltax); s--)
          cumulative_centric[s]++;
      }
      else
      {
        nacentsel++;
        for (int s = 50; s >= std::ceil(std::max(0.,Esqr)/deltax); s--)
          cumulative_acentric[s]++;
      }
    }
  }
//logGraph
//theoretical values taken from ctruncate
  std::string loggraph;
  double acen[51] = {0.0,
  0.0392106, 0.0768837, 0.1130796, 0.1478562, 0.1812692, 0.2133721, 0.2442163, 0.2738510, 0.3023237, 0.3296800,
  0.3559636, 0.3812166, 0.4054795, 0.4287909, 0.4511884, 0.4727076, 0.4933830, 0.5132477, 0.5323336, 0.5506710,
  0.5682895, 0.5852171, 0.6014810, 0.6171071, 0.6321206, 0.6465453, 0.6604045, 0.6737202, 0.6865138, 0.6988058,
  0.7106158, 0.7219627, 0.7328647, 0.7433392, 0.7534030, 0.7630722, 0.7723623, 0.7812881, 0.7898639, 0.7981035,
  0.8060200, 0.8136260, 0.8209339, 0.8279551, 0.8347011, 0.8411826, 0.8474099, 0.8533930, 0.8591416, 0.8646647};

  double cen[51] = {0.0,
  0.1585194, 0.2227026, 0.2709655, 0.3108435, 0.3452792, 0.3757939, 0.4032988, 0.4283924, 0.4514938, 0.4729107,
  0.4928775, 0.5115777, 0.5291583, 0.5457398, 0.5614220, 0.5762892, 0.5904133, 0.6038561, 0.6166715, 0.6289066,
  0.6406032, 0.6517983, 0.6625250, 0.6728131, 0.6826895, 0.6921785, 0.7013024, 0.7100815, 0.7185345, 0.7266783,
  0.7345289, 0.7421010, 0.7494079, 0.7564625, 0.7632764, 0.7698607, 0.7762255, 0.7823805, 0.7883348, 0.7940968,
  0.7996745, 0.8050755, 0.8103070, 0.8153755, 0.8202875, 0.8250491, 0.8296659, 0.8341433, 0.8384867, 0.8427008};

  double acen_twin[51] = {0.0,
  0.0030343, 0.0115132, 0.0245815, 0.0414833, 0.0615519, 0.0842006, 0.1089139, 0.1352404, 0.1627861, 0.1912079,
  0.2202081, 0.2495299, 0.2789524, 0.3082868, 0.3373727, 0.3660750, 0.3942806, 0.4218963, 0.4488460, 0.4750691,
  0.5005177, 0.5251562, 0.5489585, 0.5719077, 0.5939942, 0.6152149, 0.6355726, 0.6550744, 0.6737317, 0.6915590,
  0.7085736, 0.7247951, 0.7402450, 0.7549459, 0.7689218, 0.7821971, 0.7947971, 0.8067470, 0.8180725, 0.8287987,
  0.8389511, 0.8485543, 0.8576328, 0.8662106, 0.8743109, 0.8819565, 0.8891694, 0.8959710, 0.9023818, 0.9084218};

  if (ncentsel && nacentsel)
  {
    for (unsigned s = 0; s < 51; s++)
    {
      loggraph += dtos(deltax*s) + " " +
      dtos(acen[s]) + " " +
      dtos(acen_twin[s]) + " " +
      dtos(cumulative_acentric[s]/nacentsel) + " " +
      dtos(cen[s]) + " " +
      dtos(cumulative_centric[s]/ncentsel) + " " +
      "\n";
    }
    string1D graph(1);
    graph[0] = "Cumulative intensity distribution (acentric and centric):N:1,2,3,4,5,6";
    output.logGraph(LOGFILE,"Intensity distribution" + description,graph,"Z ACent_theor ACent_twin ACent_obser Cent_theor Cent_obser",loggraph);
  }
  else if (ncentsel)
  {
    for (unsigned s = 0; s < 51; s++)
    {
      loggraph += dtos(deltax*s) + " " +
      dtos(cen[s]) + " " +
      dtos(cumulative_centric[s]/ncentsel) + " " +
      "\n";
    }
    string1D graph(1);
    graph[0] = "Cumulative intensity distribution (acentric and centric):N:1,2,3";
    output.logGraph(LOGFILE,"Intensity distribution" + description,graph,"Z Cent_theor Cent_obser",loggraph);
  }
  else if (nacentsel)
  {
    for (unsigned s = 0; s < 51; s++)
    {
      loggraph += dtos(deltax*s) + " " +
      dtos(acen[s]) + " " +
      dtos(acen_twin[s]) + " " +
      dtos(cumulative_acentric[s]/nacentsel) + " " +
      "\n";
    }
    string1D graph(1);
    graph[0] = "Cumulative intensity distribution (acentric only):N:1,2,3,4";
    output.logGraph(LOGFILE,"Intensity distribution" + description,graph,"Z ACent_theor ACent_twin ACent_obser",loggraph);
  }
  if (nacentsel) // 2nd moment vs resolution: only acentrics
  {
    loggraph = "";
    for (unsigned s = 0; s < bin.numbins(); s++)
    {
      loggraph += dtos(1.0/fn::pow2(bin.MidRes(s)),5,4) + " " +
      dtos(MOMENTS.expectE4_acent_bin[s]) + " " +
      dtos(MOMENTS.expectE4_acent_bin[s]-0.5) + " " +
      dtos(MOMENTS.meanE4_acent_bin[s]) + " " +
      "\n";
    }
    string1D graph(1);
    graph[0] = "Weighted second moments vs resolution (acentric only):AUTO:1,2,3,4";
    output.logGraph(LOGFILE,"Weighted second moments" + description,graph,"1/d^2 ACent_theor ACent_twin ACent_obser",loggraph);
  }
}

void DataB::logSigmaN(outStream where,Output& output,bool print_info)
{
  if (where == VERBOSE) where = DEBUG; //override
  output.logUnderLine(VERBOSE,"SigmaN Parameters");
  if (print_info)
  {
  output.logTab(1,VERBOSE,"Anisotropy parameters are applied to SigmaN bins as");
  output.logTab(1,VERBOSE,"exp(-(betaHH.HH + betaKK.KK + betaLL.LL + betaHK.HK + betaHL.HL + betaKL.KL))");
  output.logTab(1,VERBOSE,"exp(-2*pi*pi*(UHH.HH.a*a* + UKK.KK.b*b* + ULL.LL.c*c* + 2UHK.HK.a*b* + 2UHL.HL.a*c* + 2UKL.KL.b*c*))");
  output.logTab(1,VERBOSE,"exp(-(1/4)*(BHH.HH.a*a* + BKK.KK.b*b* + BLL.LL.c*c* + 2BHK.HK.a*b* + 2BHL.HL.a*c* + 2BKL.KL.b*c*))");
  output.logTab(1,VERBOSE,"Refmac5 Bij applies to amplitudes not intensities, factor of 2");
  output.logTab(1,VERBOSE,"exp(-2*(B11.HH.a*a* + B22.KK.b*b* + B33.LL.c*c* + 2B12.HK.a*b* + 2B13.HL.a*c* + 2B23.KL.b*c*))");
  }
  output.logTabPrintf(1,VERBOSE,"%-24s %6.3f\n","Overall Isotropic B-factor:",SIGMAN.WILSON_B);
  output.logTabPrintf(1,VERBOSE,"%-24s %6.3f\n","Solvent Scale:",SIGMAN.SOLK);
  output.logTabPrintf(1,VERBOSE,"%-24s %6.3f\n","Solvent B-factor:",SIGMAN.SOLB);
  output.logTabPrintf(1,VERBOSE,"%-24s\n","Anisotropy parameters:");
  output.logTabPrintf(1,VERBOSE,"   %6s    %6s    %6s    %6s    %6s    %6s\n",
      "betaHH","betaKK","betaLL","betaHK","betaHL","betaKL");
  output.logTabPrintf(1,VERBOSE,"  %9.6f %9.6f %9.6f %9.6f %9.6f %9.6f\n",
      SIGMAN.ANISO[0],SIGMAN.ANISO[1],SIGMAN.ANISO[2],
      SIGMAN.ANISO[3],SIGMAN.ANISO[4],SIGMAN.ANISO[5]);
  dmat6 anisoU = AnisoBeta2AnisoU(SIGMAN.ANISO,aStar(),bStar(),cStar());
  dmat6 anisoB = AnisoBeta2AnisoB(SIGMAN.ANISO,aStar(),bStar(),cStar());
  output.logTabPrintf(1,VERBOSE,"%-24s\n","Alternative conventions");
  output.logTabPrintf(1,VERBOSE,"%5s %-8s (%-9s) (%-9s) (%-9s)\n","Aniso","beta","U","B","Refmac5 B");
  for (int n = 0; n < SIGMAN.ANISO.size(); n++)
    output.logTabPrintf(1,VERBOSE,"%3s %9.6f (%9.6f) (%9.5f) (%9.5f)\n",
      itoaniso(n).c_str(),SIGMAN.ANISO[n],anisoU[n],anisoB[n],anisoB[n]/8.0);
  output.logTab(1,where,
      "Principal components of anisotropic part of B affecting observed amplitudes:");
  output.logTab(1,where,"  eigenB (A^2)     direction cosines (orthogonal coordinates)");
  dvect3 eigenBvals = getEigenBs();  // Eigenvalues for anisotropic part of SigmaN.ANISO applying to F
  cctbx::adptbx::eigensystem<floatType> SIGMAN_eigenBs = getEigenSystem();
  for (int j = 0; j < 3; j++)
    output.logTabPrintf(1,where,"    %7.3f             %7.4f  %7.4f  %7.4f\n",
      eigenBvals[j],
      SIGMAN_eigenBs.vectors(j)[0],SIGMAN_eigenBs.vectors(j)[1],SIGMAN_eigenBs.vectors(j)[2]);
  output.logTabPrintf(1,where,"Anisotropic deltaB (i.e. range of principal components): %7.3f\n",
        getAnisoDeltaB());
  floatType resharpB(RESHARP.FRAC*std::min(eigenBvals[0],std::min(eigenBvals[1],eigenBvals[2])));
  output.logTabPrintf(1,VERBOSE,
      "Resharpening B (to restore strong direction of diffraction): %7.3f\n",resharpB);
  output.logTabPrintf(1,DEBUG,"%-20s\n","Bin Parameters:");
  for (int s = 0; s < bin.numbins(); s++)
    output.logTabPrintf(1,DEBUG,"#%-3d  %5.2f -%5.2fA %16.4f\n",s+1,bin.LoRes(s),bin.HiRes(s),SIGMAN.BINS[s]);
  output.logTabPrintf(1,VERBOSE,"Wilson scale applied to get observed intensities: %5.4e\n",1./fn::pow2(SIGMAN.WILSON_K));
  output.logBlank(where);
}

void DataB::logScale(outStream where,Output& output)
{
  output.logTab(1,where,"Scale factor to put input Fs on absolute scale");
  output.logTab(1,where,"Wilson Scale:    " + dtos(SIGMAN.WILSON_K));
  output.logTab(1,where,"Wilson B-factor: " + dtos(SIGMAN.WILSON_B));
  output.logBlank(where);
}

void DataB::selectReso(floatType res1, floatType res2)
{
  HIRES = std::min(res1,res2);
  LORES = std::max(res1,res2);
  calcSelected();
}

void DataB::calcSelected()
{
  //initialize true/false as absent or not
  for (unsigned r = 0; r < NREFL; r++)
    selected[r] = !SpaceGroup::is_sys_absent(MILLER[r]);
  //then flag OUTLIER.ANO as false
  for (unsigned o = 0; o < OUTLIER.ANO.size(); o++)
  {
    int r = OUTLIER.ANO[o].refl;
    selected[r] = false;
  }
  //now flag those outside range as false
  for (unsigned r = 0; r < NREFL; r++)
  {
    floatType res = reso(r);
    if (res < HIRES || res > LORES)
      selected[r] = false;
    //and reflections with input amplitudes of zero
    if (!INPUT_INTENSITIES && (F[r] == 0)) selected[r] = false;
  }
  { //check
  int nsel(0);
  for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) nsel++;
  if (!nsel)
  throw PhaserError(FATAL,"No reflections within resolution range");
  }
  {
  int nsel(0);
  for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) nsel++;
  if (!nsel)
  throw PhaserError(FATAL,"No reflections selected");
  }
  //check relections unique
  const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
  if (!miller::is_unique_set_under_symmetry(SgInfo,false,MILLER.const_ref()))
  throw PhaserError(FATAL,"Reflections are not a unique set by symmetry");
}

floatType DataB::epsn(const int& r) {  return fn::pow2(sqrt_epsn[r]); } //SpaceGroup::epsilon(MILLER[r]);
bool      DataB::cent(const int& r) {  return is_centric[r]; } // SpaceGroup::is_centric(MILLER[r]);
floatType DataB::reso(const int& r) {  return UnitCell::reso(MILLER[r]); }
floatType DataB::ssqr(const int& r) {  return UnitCell::Ssqr(MILLER[r]); }
//Composition will be multiplied by number of symmetry operators.
//Extra factor of NSYMM/NSYMP (i.e. # of centering operations) compensates
//for lack of centering in epsilon calculation
floatType DataB::best(const unsigned& r)    { return TOTAL_SCAT_B*SpaceGroup::SYMFAC*BEST(ssqr(r)); }
floatType DataB::best(const double& s_sqr) { return TOTAL_SCAT_B*SpaceGroup::SYMFAC*BEST(s_sqr); }

miller::index<int>    DataB::rotMiller(const int& r,const int& isym) { return SpaceGroup::rotsym[isym]*MILLER[r]; }
floatType DataB::traMiller(const int& r,const int& isym) { return SpaceGroup::trasym[isym]*MILLER[r]; }

std::vector<miller::index<int> >  DataB::rotMiller(const int& r)
{
  std::vector<miller::index<int> > rhkl(SpaceGroup::NSYMP);
  for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++) rhkl[isym] = rotMiller(r,isym);
  return rhkl;
}
float1D  DataB::traMiller(const int& r)
{
  float1D thkl(SpaceGroup::NSYMP);
  for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++) thkl[isym] = SpaceGroup::trasym[isym]*MILLER[r];
  return thkl;
}

bool DataB::duplicate(const int& isym,const std::vector<miller::index<int> >& rotMiller)
{
  for (unsigned jsym = 0; jsym < isym; jsym++)
    if (rotMiller[isym] == rotMiller[jsym]) return true;
  return false;
}

void DataB::checkData() const
{
  if (NREFL == 0) throw PhaserError(FATAL,"There are no reflections within the resolution ranges requested");
  const cctbx::sgtbx::space_group cctbxSG = SpaceGroup::getCctbxSG();
  cctbx::sgtbx::space_group_type SgInfo(cctbxSG);
  if (!miller::is_unique_set_under_symmetry(SgInfo,false,MILLER.const_ref()))
  throw PhaserError(FATAL,"Reflections are not a unique set by symmetry");

  std::string negFs("");
  for (unsigned r = 0; r < NREFL; r++)
  {
    if (INPUT_INTENSITIES)
    {
      if (SIGI[r] < 0) negFs += "\nSIGI is negative (" + dtos(SIGI[r]) + ") for reflection with index ("+ ivtos(MILLER[r]) + ")";
    }
    else
    {
      if (F[r] < 0) negFs +=  "\nF is negative (" + dtos(F[r]) + ") for reflection with index ("+ ivtos(MILLER[r]) + ")";
      if (SIGF[r] < 0) negFs += "\nSIGF is negative (" + dtos(SIGF[r]) + ") for reflection with index ("+ ivtos(MILLER[r]) + ")";
    }
    //if they are equal to zero, rejected in rejectOutliers
  }
  if (negFs.size()) throw PhaserError(FATAL,negFs);
}

SigmaN DataB::getBinSigmaN()
{
  // Get SigmaN values corresponding to observed data after correcting for anisotropy
  // (without resharpening), for use in determining relative scale/B of FC in ensembles.
  float1D  SIGMAN_BINS(bin.numbins(),0);
  double WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    double ssqr_bin(1.0/fn::pow2(bin.MidRes(s)));
    SIGMAN_BINS[s] = binIsoFactor(ssqr_bin,SIGMAN.BINS[s],SIGMAN.WILSON_B,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
  }
  return SigmaN(bin,SIGMAN_BINS);
}

void DataB::logWilson(outStream where,floatType LL,Output& output)
{
  output.logTab(1,where,"Parameters set for Wilson log-likelihood calculation");
  output.logTab(1,where,"E = 0 and variance 1 for each reflection");
  int NC = NumC();
  int NAC = NumAC();
  output.logTab(1,where,"Without correction for SigF to the variances,");
  output.logTab(2,where,"Wilson log(likelihood) = - number of acentrics (" + itos(NAC) + ")");
  output.logTab(2,where,"                         - half number of centrics (" + itos(NC) + "/2)" );
  output.logTab(2,where,"                       = " + itos(-NAC-NC/2));
  output.logTab(1,where,"With correction for SigF,");
  output.logTab(2,where,"Wilson log(likelihood) = " + dtos(LL));
  output.logBlank(where);
}

floatType DataB::WilsonLL()
{
  floatType totalLL(0);
  PHASER_ASSERT(Feff.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    bool      rcent = cent(r);
    floatType V = PTNCS.EPSFAC[r];
    floatType Esqr = fn::pow2(Feff[r]/SIGMAN.sqrt_epsnSN[r]);
    PHASER_ASSERT(V > 0);
    floatType reflLL = -(std::log(V)+Esqr/V);
    totalLL += rcent ? reflLL/2.0 : reflLL;
  } // r loop
  return totalLL;
}

void DataB::initCos_Sin()
{
#ifdef CCTBX_MATH_COS_SIN_TABLE_H
  fast_cos_sin = cctbx::math::cos_sin_lin_interp_table<floatType>(4000);
#endif
}

cmplxType DataB::dphi(unsigned& r,unsigned& isym,dvect3& TRA)
{
  floatType dphi_arg = rotMiller(r,isym)*TRA + traMiller(r,isym);
#if defined CCTBX_MATH_COS_SIN_TABLE_H
  return fast_cos_sin.get(dphi_arg); //multiplies by 2*pi internally
#else
  dphi_arg *= scitbx::constants::two_pi;
  return cmplxType(std::cos(dphi_arg),std::sin(dphi_arg));
#endif
}

cmplxType DataB::dphi(unsigned& r,unsigned& isym,miller::index<int>& rotMiller_isym,dvect3& TRA)
{
  floatType dphi_arg = rotMiller_isym*TRA + traMiller(r,isym);
#if defined CCTBX_MATH_COS_SIN_TABLE_H
  return fast_cos_sin.get(dphi_arg); //multiplies by 2*pi internally
#else
  dphi_arg *= scitbx::constants::two_pi;
  return cmplxType(std::cos(dphi_arg),std::sin(dphi_arg));
#endif
}

void DataB::initialize_sigman()
{
  af_float    Idat = INPUT_INTENSITIES ?    I :    IfromF;
  floatType ONE(1),TWO(2);
  float1D  SumWeight(bin.numbins());
  SIGMAN.BINS.resize(bin.numbins());
  for (unsigned s = 0; s < bin.numbins(); s++)
    SIGMAN.BINS[s] = SumWeight[s] = 0;
  floatType weight(0);
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  for (unsigned r = 0; r < NREFL; r++)
  if (INPUT_INTENSITIES || (F[r] != 0))
  {
    unsigned s = rbin(r); // alias
    bool rcent = cent(r);
    floatType repsn = epsn(r);
    weight = rcent ? ONE : TWO;
    PHASER_ASSERT(repsn != 0);
    // binAnisoFactor incorporates BEST curve contribution for composition
    floatType anoFactor(binAnisoFactor(r,ONE,SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I));
    SIGMAN.BINS[s] += weight*Idat[r]/repsn/anoFactor;
    SumWeight[s] += weight;
  }
  for (unsigned s = 0; s < bin.numbins(); s++)
    if (SumWeight[s] != 0) SIGMAN.BINS[s] /= SumWeight[s];

  // Use Wilson plot on SIGMAN.BINS (scale correction factors for scaled BEST curve) to determine the
  // overall scale and the part of the isotropic B that is not in SIGMAN.ANISO
  // Then remove this variation with resolution from the BINS and put it into the ANISO parameters
  floatType sumw(0),sumwx(0),sumwy(0),sumy(0),sumwx2(0),sumwxy(0);
  int countr(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (SIGMAN.BINS[s] > 0.) // Skip empty bins or ones with negative mean intensity
    {
      floatType ssqr_bin(1.0/fn::pow2(bin.MidRes(s)));
      floatType logSN_bin = std::log(SIGMAN.BINS[s]);
      floatType weight = SumWeight[s];
      if (ssqr_bin < 0.04) weight = weight * fn::pow2(ssqr_bin/0.04); // Down-weight data below 5A - doesn't fit BEST curve
      if (ssqr_bin < 0.009)
        weight = 0; // Ignore data below 10.5409 - no BEST curve
      else countr++;
      sumw += weight;
      sumwx += weight*ssqr_bin;
      sumwx2 += weight*fn::pow2(ssqr_bin);
      sumwy += weight*logSN_bin;
      sumy += ssqr_bin;
      sumwxy += weight*ssqr_bin*logSN_bin;
    }
  }
  floatType slope =  2 <= countr?  (sumw*sumwxy-(sumwx*sumwy))/(sumw*sumwx2-fn::pow2(sumwx)): 0;
  floatType intercept = 2 <= countr ? (sumwy - slope*sumwx)/sumw : 0;
  floatType WilsonK_intensity = exp(intercept); // Scale applied to absolute intensities to get observed
  double  WilsonK = 1.0/std::sqrt(WilsonK_intensity); // Scale to correct observed amplitudes to get absolute
  floatType WilsonB_intensity = -4*slope;
  dmat6 deltaAniso = IsoB2AnisoBeta(WilsonB_intensity,aStar(),bStar(),cStar(),
                                                       cosAlphaStar(),cosBetaStar(),cosGammaStar());
  for (unsigned n = 0; n < 6; n++)
  {
    SIGMAN.ANISO[n] += deltaAniso[n];
  }
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    floatType ssqr_bin(1.0/fn::pow2(bin.MidRes(s)));
    if (SIGMAN.BINS[s] > 0.)
    {
      SIGMAN.BINS[s] *= std::exp(WilsonB_intensity*ssqr_bin/4.) / WilsonK_intensity;
      // Don't allow initial values to deviate more than factor of 2 from BEST
      SIGMAN.BINS[s] = std::max(std::min(SIGMAN.BINS[s],2.),0.5);
    }
    else
      SIGMAN.BINS[s] = 1.; // Negative mean intensity: just use BEST value
  }
  SIGMAN.WILSON_K = WilsonK;
}

void DataB::calculate_sigman_parameters()
{
  // Calculate parameters that are not refined directly.
  //dmat6 anisoU = AnisoBeta2AnisoU(SIGMAN.ANISO,aStar(),bStar(),cStar());
 // dmat6 anisoB = AnisoBeta2AnisoB(SIGMAN.ANISO,aStar(),bStar(),cStar());
  // Determine isotropic component of anisoB
  floatType isotropicB_I = AnisoBeta2IsoB(SIGMAN.ANISO,A(),B(),C(),cosAlpha(),cosBeta(),cosGamma());
  SIGMAN.WILSON_B = isotropicB_I/2.0; //value on F's
  floatType WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));

  //calculate SIGMAN per reflection, and store
  SIGMAN.sqrt_epsnSN.resize(NREFL,0);
  for (unsigned r = 0; r < NREFL; r++)
  {
    int s = rbin(r);
    floatType epsnSigmaN = epsn(r)*binAnisoFactor(r,SIGMAN.BINS[s],SIGMAN.ANISO,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    PHASER_ASSERT(epsnSigmaN > 0);
    SIGMAN.sqrt_epsnSN[r] = std::sqrt(epsnSigmaN);
  }
}

void DataB::initGfunction(bool halfR)
{
  if (PTNCS.use_and_present())
  {
    gfun.init(*this,*this,PTNCS.NMOL);
    dmat33 ncsRmat(1,0,0,0,1,0,0,0,1);
    for (int dir = 0; dir < 3; dir++)
      ncsRmat = xyzRotMatDeg(dir,PTNCS.ROT.ANGLE[dir])*ncsRmat;
    if (halfR)
    {
      //convert to an angle about an axis
      dvect3     AXIS = scitbx::math::r3_rotation::axis_and_angle_from_matrix<floatType>(ncsRmat).axis;
      floatType  ANGL = scitbx::math::r3_rotation::axis_and_angle_from_matrix<floatType>(ncsRmat).angle_rad;
      //convert back to matrix, with half the rotation
      ncsRmat = scitbx::math::r3_rotation::axis_and_angle_as_matrix(AXIS,ANGL/2);
    }
    gfun.initArrays(PTNCS.TRA.VECTOR,ncsRmat,PTNCS.GFUNCTION_RADIUS,MILLER);
    G_DRMS.resize(NREFL);
    G_Vterm.resize(NREFL);
    for (int r = 0; r < NREFL; r++)
    {
      int s = rbin(r);
      G_DRMS[r] = std::sqrt(1. - PTNCS.VAR.BINS[s]);
      gfun.calcReflTerms(r);
      G_Vterm[r] = gfun.refl_G_Vterm; //not correct, B-factor differences
    }
  }
}

std::pair<int,int> DataB::number_selected()
{
  std::pair<int,int> count(0,0);
  bool1D outlier(NREFL,false);
  for (unsigned o = 0; o < OUTLIER.ANO.size(); o++)
    outlier[OUTLIER.ANO[o].refl] = true;
  for (unsigned r = 0; r < NREFL; r++)
  {
    floatType res = reso(r);
    if (!(res < HIRES || res > LORES) && !SpaceGroup::is_sys_absent(MILLER[r]))
    {
      count.second++;
      if (!outlier[r] && !(!INPUT_INTENSITIES && (F[r] == 0))) count.first++;
    }
  }
  return count;
}


} //phaser
