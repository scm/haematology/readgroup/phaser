//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TNCS_GFUNCTION_CLASS__
#define __TNCS_GFUNCTION_CLASS__
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <cctbx/math/cos_sin_table.h>

namespace phaser {

class Gfunction : SpaceGroup, UnitCell
{
  public:
    Gfunction()
    {
      NREFL = dE_by_dD = refl_epsfac = refl_G_Vterm = refl_Gsqr_Vterm = 0;
      max_ncsT = 2;
      dE_by_dT = d2E_by_dT2 = dvect3(0,0,0);
#if defined CCTBX_MATH_COS_SIN_TABLE_H
      fast_cos_sin = cctbx::math::cos_sin_lin_interp_table<floatType>(4000);
#endif
    }
    Gfunction(SpaceGroup,UnitCell,int);
    virtual ~Gfunction() { }
    void init(SpaceGroup,UnitCell,int);

  private:
    int max_ncsT;
#if defined CCTBX_MATH_COS_SIN_TABLE_H
    cctbx::math::cos_sin_lin_interp_table<floatType> fast_cos_sin;
#endif

  public:
    double61D   GfunTensorArray;
    dvect31D    ncsDeltaT;
    float1D     cosTerm,sinTerm,G;
    int         NREFL;
    //reflection terms
    floatType   dE_by_dD,refl_epsfac,refl_G_Vterm,refl_Gsqr_Vterm;
    dvect3      dE_by_dT,d2E_by_dT2;
    float1D     refl_G,refl_Gsqr;

  public:
    void initArrays(dvect3,dmat33,floatType,af::shared<miller::index<int> >);
    void calcReflTerms(unsigned);
    void calcArrays(dvect3,dmat33,floatType);
    void calcRefineTerms(bool&,bool&,floatType,miller::index<int>&);
};

} //phaser

#endif
