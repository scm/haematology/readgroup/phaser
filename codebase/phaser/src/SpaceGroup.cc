//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <cctype>
#include <phaser/src/SpaceGroup.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/src/Brick.h>
#include <cctbx/sgtbx/seminvariant.h>

namespace phaser {

SpaceGroup::SpaceGroup(std::string SG_HALL)
{ setSpaceGroupHall(SG_HALL); }

SpaceGroup::SpaceGroup(cctbx::sgtbx::space_group const& CctbxSG)
{ setSpaceGroup(CctbxSG.type().hall_symbol()); }

void SpaceGroup::setSpaceGroupHall(std::string SG_HALL)
{
  cctbxSG = cctbx::sgtbx::space_group(SG_HALL,"A1983");
  NSYMM = cctbxSG.order_z();
  NSYMP = cctbxSG.order_p();
  SYMFAC = NSYMM*(NSYMM/NSYMP);
  trasym.resize(NSYMM);
  rotsym.resize(NSYMM);
  for (unsigned isym = 0; isym < cctbxSG.order_z(); isym++)
  {
    for (int i = 0; i < 3; i++)
    {
      trasym[isym][i] = static_cast<floatType>(cctbxSG(isym).t()[i])/cctbxSG(isym).t().den();
      for (int j = 0; j < 3; j++)
        rotsym[isym](i,j) = static_cast<floatType>(cctbxSG(isym).r()(i,j))/cctbxSG(isym).r().den();
    }
    rotsym[isym] = rotsym[isym].transpose();
  }
 //should store the number as this is used for volumes
  spcgrpnum = cctbxSG.type().number();
}

dvect3 SpaceGroup::doSymXYZ(const int& isym, const dvect3& v)
{ return rotsym[isym].transpose()*v + trasym[isym]; }

int SpaceGroup::orderX()
{
  int epsn(0);
  dvect3 xaxis(1,0,0);
  for (unsigned isym = 0; isym < NSYMP; isym++)
    if (xaxis ==  rotsym[isym]*xaxis)
      epsn++;
  return epsn;
}

int SpaceGroup::orderY()
{
  int epsn(0);
  dvect3 yaxis(0,1,0);
  for (unsigned isym = 0; isym < NSYMP; isym++)
    if (yaxis == rotsym[isym]*yaxis)
      epsn++ ;
  return epsn;
}

int SpaceGroup::orderZ()
{
  int epsn(0);
  dvect3 zaxis(0,0,1);
  for (unsigned isym = 0; isym < NSYMP; isym++)
    if (zaxis == rotsym[isym]*zaxis)
      epsn++ ;
  return epsn;
}

int SpaceGroup::highOrderAxis()
{
  int axis=3;
  if (orderY() > orderZ()) axis=2;
  if (orderX() > orderY() && orderX() > orderZ()) axis=1;
  return axis;
}

Brick SpaceGroup::brickPrimitive(UnitCell uc)
{
  dvect3 primitiveMin(0,0,0);
  dvect3 primitiveMax(0,0,0);
  bool unknown_primitive_volume(false);
  if (NSYMM == NSYMP) primitiveMax = dvect3(1,1,1);
  else if (NSYMM == 2*NSYMP) primitiveMax = dvect3(1,1,0.5);
  else if (NSYMM == 4*NSYMP) primitiveMax = dvect3(1,0.5,0.5);
  else if (NSYMM == 3*NSYMP) primitiveMax = dvect3(1,1,1);
  else PHASER_ASSERT(unknown_primitive_volume);
  return Brick(true,primitiveMin,primitiveMax,uc);
}

bool SpaceGroup::inPrimitive(dvect3 point)
{ return true; }

Brick SpaceGroup::brickCheshire(UnitCell uc)
{
  dvect3 cheshireMin(0,0,0);
  dvect3 cheshireMax(0,0,0);

  //Set the values for the limit of the Cheshire cell
  //The more complex Cheshire cells  must also satisfy the
  //criteria in the function inCheshire
  const floatType ZERO(0.0),ONE(1.0);
  const floatType HALF(0.5),QUARTER(0.25);

  switch (spcgrpnum)
  {
    case 1:  //P1
      cheshireMax = dvect3(ZERO,ZERO,ZERO);
    break;

    //pg2
    case 3: case 4: case 5: //pg2
      cheshireMax = dvect3(HALF,ZERO,HALF);
    break;

    //pg222
    case 16: case 17: case 18: case 19: case 20: case 21: case 23: case 24:
      cheshireMax = dvect3(HALF,HALF,HALF);
    break;

    //Exception F222
    case 22:
      cheshireMax = dvect3(QUARTER,HALF,HALF);
    break;

    //pg4
    case 75: case 76: case 77: case 78: case 79: case 80:
      cheshireMax = dvect3(HALF,ONE,ZERO);
    break;

    //pg422
    case 89: case 90: case 91: case 92: case 95: case 96:
    case 93: case 94: case 97: case 98:
      cheshireMax = dvect3(HALF,ONE,HALF);
    break;

    //pg3
    case 143: case 144: case 145: case 146:  //R3 rhombohedral
      cheshireMax = dvect3(ONE,ONE,ZERO);
      //This is three asymmetric units
    break;

    //pg312
    case 149: case 151: case 153:
      cheshireMax = dvect3(ONE,ONE,HALF);
      //This is three asymmetric units
    break;

    //pg321
    case 150: case 152: case 154:
    case 155: //R32 rhombohedral
    //pg622
    case 177: case 178: case 179: case 180: case 181: case 182:
      cheshireMax = dvect3(ONE,ONE,HALF);
    break;

    //pg6
    case 168: case 169: case 170: case 171: case 172: case 173:
      cheshireMax = dvect3(ONE,ONE,ZERO);
    break;

    //pg23
    case 195: case 197: case 198: case 199:
    //pg432
    case 207: case 208: case 209: case 210: case 211: case 212: case 213: case 214:
      cheshireMax = dvect3(HALF,ONE,ONE);
    break;

    //Exception F23
    case 196:
      cheshireMax = dvect3(ONE,ONE,QUARTER);
    break;

    default:
    {
      bool unknown_spacegroup_number_maxCheshire(false);
      PHASER_ASSERT(unknown_spacegroup_number_maxCheshire);
    }
  }
  return Brick(true,cheshireMin,cheshireMax,uc);
}

bool SpaceGroup::inCheshire(dvect3 point)
{
  floatType x(point[0]);
  floatType y(point[1]);
  floatType z(point[2]);
  const floatType ZERO(0.0),ONE(1.0);
  const floatType HALF(0.5),QUARTER(0.25),THIRD(1.0/3.0);

  switch (spcgrpnum)
  {
    case 1:  //P1
      return (x == 0 && y == 0 && z == 0);
      break;

    //pg2
    case 3: case 4: case 5: //pg2
      return ( x>=0 && y>=0 && z>=0 && x<=HALF && y<=ZERO && z<=HALF);
      break;

    //pg222
    case 16: case 17: case 18: case 19: case 20: case 21:
    case 23: case 24:
      return ( x>=0 && y>=0 && z>=0 && x<=HALF && y<=HALF && z<=HALF);
      break;

    //Exception F222
    case 22:
      return ( x>=0 && y>=0 && z>=0 && x<=QUARTER && y<=HALF && z<=HALF);
      break;

    //pg4
    case 75: case 76: case 77: case 78: case 79: case 80:
      return ( x>=0 && y>=0 && z>=0 && x<=HALF && y<=ONE && z<=ZERO);
      break;

    //pg422
    case 89: case 90: case 91: case 92: case 95: case 96:
    case 93: case 94: case 97: case 98:
      return ( x>=0 && y>=0 && z>=0 && x<=HALF && y<=ONE && z<=HALF);
      break;

    //pg3
    case 143: case 144: case 145:
    case 146:  //R3 rhombohedral
      return ( x>=0 && y>=0 && z>=0 && x<=ONE && y<=ONE && z<=ZERO &&
               x<=2*THIRD && y<=2*THIRD && x<=(1+y)/2 && y<=std::min(1-x,(1+x)/2) );
      break;

    //pg312
    case 149: case 151: case 153:
      return ( x>=0 && y>=0 && z>=0 && x<=ONE && y<=ONE && z<=HALF &&
               x<=2*THIRD && y<=2*THIRD && x<=(1+y)/2 && y<=std::min(1-x,(1+x)/2) );
      break;

    //pg321
    case 150: case 152: case 154:
    case 155: //R32 rhombohedral
    //pg622
    case 177: case 178: case 179: case 180: case 181: case 182:
      return ( x>=0 && y>=0 && z>=0 && x<=ONE && y<=ONE && z<=HALF);
      break;

    //pg6
    case 168: case 169: case 170: case 171: case 172: case 173:
      return ( x>=0 && y>=0 && z>=0 && x<=ONE && y<=ONE && z<=ZERO);
      break;

    //pg23
    case 195: case 197: case 198: case 199:
    //pg432
    case 207: case 208: case 209: case 210: case 211: case 212: case 213: case 214:
      return ( x>=0 && y>=0 && z>=0 && x<=HALF && y<=ONE && z<=ONE);
      break;

    //Exception F23
    case 196:
      return ( x>=0 && y>=0 && z>=0 && x<=ONE && y<=ONE && z<=QUARTER);
      break;

    default:
    {
      bool unknown_spacegroup_number_inCheshire(false);
      PHASER_ASSERT(unknown_spacegroup_number_inCheshire);
    }
  }
  return 0;
}

void SpaceGroup::logSymm(outStream where, Output& output)
{
  output.logTabPrintf(1,where,"Space Group Name (Number): %s (%d)\n",spcgrpname().c_str(),spcgrpnumber());
  output.logBlank(where);
}

void SpaceGroup::logSymmExtra(outStream where, Output& output)
{
  for (unsigned isym = 0; isym < NSYMM; isym++)
  {
    output.logTabPrintf(1,where,"Symmetry # %d\n",isym+1);
    output.logTabPrintf(1,where,"Rotation:   [% 5.3f % 5.3f % 5.3f]\n",rotsym[isym](0,0),rotsym[isym](0,1),rotsym[isym](0,2));
    output.logTabPrintf(1,where,"            [% 5.3f % 5.3f % 5.3f]\n",rotsym[isym](1,0),rotsym[isym](1,1),rotsym[isym](1,2));
    output.logTabPrintf(1,where,"            [% 5.3f % 5.3f % 5.3f]\n",rotsym[isym](2,0),rotsym[isym](2,1),rotsym[isym](2,2));
    output.logTabPrintf(1,where,"Translation: % 7.6f % 7.6f % 7.6f \n",trasym[isym][0],trasym[isym][1],trasym[isym][2]);
    output.logBlank(where);
  }
  output.logBlank(where);
}

std::string SpaceGroup::pgname()
{
  cctbx::sgtbx::space_group PG(cctbxSG.build_derived_point_group());
  std::string Point_Group("PG");
  for (int i = 1; i < PG.type().lookup_symbol().size(); i++)
    if (!(std::isspace)(PG.type().lookup_symbol()[i]))
      Point_Group += (std::toupper)(PG.type().lookup_symbol()[i]);
  return Point_Group;
}

std::string SpaceGroup::spcgrpname()
{
  space_group_name sg(cctbxSG.type().hall_symbol(),true);
  return sg.CCP4;
}

int  SpaceGroup::spcgrpnumber()
{ return spcgrpnum; }

char SpaceGroup::spcgrpcentring()
{ //fudge for cctbx
  std::string Extended_Hermann_Mauguin = cctbxSG.type().universal_hermann_mauguin_symbol();
  if (Extended_Hermann_Mauguin.find("R 3 :H (") != std::string::npos) return 'R';
  if (Extended_Hermann_Mauguin.find("R 3 2 :H (") != std::string::npos) return 'R';
  if (Extended_Hermann_Mauguin.find("R 3 :H") != std::string::npos) return 'H';
  if (Extended_Hermann_Mauguin.find("R 3 2 :H") != std::string::npos) return 'H';
  return cctbxSG.conventional_centring_type_symbol();
}

std::string SpaceGroup::getHall() const
{ return cctbxSG.type().hall_symbol(); }

bool SpaceGroup::is_polar()
{
  cctbx::sgtbx::structure_seminvariants seminvariants(cctbxSG);
  af::small<cctbx::sgtbx::ss_vec_mod, 3> const &vm = seminvariants.vectors_and_moduli();
  for (int i=0; i<vm.size(); ++i) {
    if (vm[i].m != 0) continue;
    // allowed continuous origin shift:
    return true;
  }
  return false;
}

}//phaser
