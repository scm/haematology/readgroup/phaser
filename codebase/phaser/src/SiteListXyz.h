//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SiteListXyzClass__
#define __SiteListXyzClass__
#include <phaser/src/SiteList.h>
#include <phaser/src/Sites.h>
#include <phaser/io/Output.h>

namespace phaser {

class SiteListXyz : public SiteList
{
  private:
    std::vector<xyzsite> siteList;
    xyzsite  nextPoint();
    floatType CLUSTER_DIST;
    bool check_centrosymmetry;

  public:
    bool                at_end() const;
    dvect3              next_site_frac();
    void                restart();
    std::vector<dvect3> all_sites_frac();

    SiteListXyz(SpaceGroup& sg_,UnitCell& uc_,bool b): SiteList(sg_,uc_),check_centrosymmetry(b) { CLUSTER_DIST = 0; }
    xyzsite&  operator()(int const& i) { return siteList[i]; }
    ~SiteListXyz() {}
    void set_cluster_dist(floatType d) { CLUSTER_DIST = d; }

    void push_back(xyzsite&);
    void Resize(unsigned);
    void Reserve(unsigned);
    void Sort();
    void SortTop(double limit);
    void truncate(unsigned);

   //concrete
    bool      isSelected(unsigned);
    bool      isTopSite(unsigned);
    bool      isDeep(unsigned) { return true; }
    floatType getValue(unsigned);
    float1D   getValues(size_t=0);
    int1D     getClusters(size_t=0);
    float1D   getSignals(size_t=0);
    dvect31D  getFracsXYZ(size_t=0);
    unsigned  getNum(unsigned);
    dvect3    getFracXYZ(unsigned);
    dvect3    getOrthXYZ(unsigned);
    floatType getSignal(unsigned);
    void      setNum(unsigned,unsigned);
    void      setValue(unsigned,floatType);
    void      setTopSite(unsigned,bool);
    void      setDeep(unsigned i,bool b) {}
    void      setSelected(unsigned,bool);
    void      archiveValue(unsigned);
    unsigned  Size();
    void      Erase(int,int);
    void      Copy(int,int);
    unsigned  numSelected();

    void flagClusteredTop(int);
    void flagClusteredTopNew(int) {}
    void logRawTop(outStream,Output&);
    void logClusteredTop(outStream,Output&);
    void logRawTopRescored(outStream,Output&);
    void logClusteredTopRescored(outStream,Output&);
    void graphRawTop(outStream,Output&);
    void graphClusteredTop(outStream,Output&);
    void graphRawTopRescored(outStream,Output&);
    void graphClusteredTopRescored(outStream,Output&);
};

} //end namespace phaser

#endif
