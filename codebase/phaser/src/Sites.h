//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SiteClass__
#define __SiteClass__
#include <phaser/main/Phaser.h>
#include <phaser/src/UnitCell.h>

namespace phaser {

class site
{
  public:
    site();

    floatType value;
    floatType zscore;
    floatType value2;
    floatType value3;
    bool topsite;
    floatType distance;
    int  clusterMul;
    int  clusterNr;
    int  clusterNum;
    int  num;
    bool selected;

    void archiveValue();
    void setValue(floatType);

// need the operator < for sorting
// define wrong way round for top to bottom sorting
    bool operator<(const site &right)  const;
    const site& operator=(const site&);
};

class angsite : public site
{
  public:
    dvect3 euler; //euler angles in degrees
    bool   deep;

    angsite();
    angsite(const angsite&);

    dmat33 getMatrix() ;
    const dvect3& getEuler() const;
    void setMatrix(dmat33);
    std::string str();
    const angsite& operator=(const angsite&);
    bool operator<(const angsite &right)  const;
};

class xyzsite : public site
{
  private:
    dvect3 orth;

  public:
    xyzsite();
    xyzsite(const xyzsite&);

    dvect3 getOrthXYZ() ;
    dvect3 getFracXYZ(UnitCell uc) ;
    void setOrthXYZ(dvect3&);
    void setFracXYZ(dvect3&,UnitCell uc);
    const xyzsite& operator=(const xyzsite&);
    bool operator<(const xyzsite &right)  const;
};

} //phaser

#endif
