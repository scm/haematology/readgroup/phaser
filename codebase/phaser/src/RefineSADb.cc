//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/include/ProtocolSAD.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <cctbx/adptbx.h>

//RefineBase2 functions

namespace phaser {

void RefineSAD::applyShift(af_float af_newx)
{
  //format change for python access
  TNT::Vector<floatType> TNT_newx(af_newx.size());
  for (int i = 0; i < af_newx.size(); i++) TNT_newx[i] = af_newx[i];
  applyShift(TNT_newx);
}

void RefineSAD::applyShift(TNT::Vector<floatType>& newx)
{
  int i(0),m(0);
//-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) DphiA_bin[s] = newx[i++];
    if (refinePar[m++]) DphiB_bin[s] = newx[i++];
    if (refinePar[m++]) SP_bin[s] = newx[i++];
    if (refinePar[m++]) SDsqr_bin[s] = newx[i++];
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) ScaleK = newx[i++];
    if (refinePar[m++]) ScaleU = newx[i++];
  }
  if (input_partial)
  {
    if (refinePar[m++]) PartK = newx[i++];
    if (refinePar[m++]) PartU = newx[i++];
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++])
    {
      int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
      if (M == 1)
      {
        for (int j = 0; j < 3; j++)
        {
          floatType shift = newx[i++] - atoms[a].SCAT.site[j];
          while (shift <= -1) shift++;
          while (shift >= +1) shift--;
          atoms[a].SCAT.site[j] += shift;
        }
      }
      else
      {
        scitbx::af::small<floatType,3> x_indep(atoms[a].n_xyz);
        for (int x = 0; x < atoms[a].n_xyz; x++) x_indep[x] = newx[i++];
        cctbx::fractional<floatType> new_xyz = site_sym_table.get(a).site_constraints().all_params(x_indep);
     //move the atom onto a special position if it is close to one
     //accounts for rounding errors
        atoms[a].SCAT.site = site_sym_table.get(a).special_op() * new_xyz;
      }
    }
    if (refinePar[m++]) atoms[a].SCAT.occupancy = newx[i++];
    if (refinePar[m++])
    {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) atoms[a].SCAT.u_iso = newx[i++];
      else
      {
        if (atoms[a].n_adp == 6)
          for (int n = 0; n < 6; n++) atoms[a].SCAT.u_star[n] = newx[i++];
        else
        {
          scitbx::af::small<floatType,6> u_indep(atoms[a].n_adp);
          for (int n = 0; n < atoms[a].n_adp; n++) u_indep[n] = newx[i++];
          atoms[a].SCAT.u_star = site_sym_table.get(a).adp_constraints().all_params(u_indep);
        }
        cctbx::adptbx::factor_u_star_u_iso<double> factor_u(getCctbxUC(),atoms[a].SCAT.u_star);
        PHASER_ASSERT(factor_u.u_iso >= lowerU());
      }
    }
  }

  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++]) AtomFdp[t] = newx[i++] ;

  PHASER_ASSERT(m == npars_all);
  PHASER_ASSERT(i == npars_ref);

  //copy fp and fdp to scatterer array
  for (int a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    int t = atomtype2t[atoms[a].SCAT.scattering_type];
    atoms[a].SCAT.fp = AtomFp[t];
    atoms[a].SCAT.fdp = AtomFdp[t];
  }

  if (!FIX_ATOMIC) calcAtomicData();
  /*always*/ calcScalesData();
  if (!FIX_SIGMAA) calcSigmaaData();
}

TNT::Vector<floatType> RefineSAD::getLargeShifts()
{
  TNT::Vector<floatType> largeShifts(npars_ref);
  floatType largeXshift = HiRes()/10.0; //in Angstroms
  cctbx::fractional<floatType> largeXYZshift;
  largeXYZshift[0] = largeXshift/UnitCell::A(); //X, fractional
  largeXYZshift[1] = largeXshift/UnitCell::B(); //Y, fractional
  largeXYZshift[2] = largeXshift/UnitCell::C(); //Z, fractional
  floatType largeBshift = cctbx::adptbx::u_as_b(largeUshift());
  dmat6 largeABshift;
  largeABshift[0] = largeBshift/fn::pow2(UnitCell::A());
  largeABshift[1] = largeBshift/fn::pow2(UnitCell::B());
  largeABshift[2] = largeBshift/fn::pow2(UnitCell::C());
  largeABshift[3] = largeBshift/(UnitCell::A()*UnitCell::B());
  largeABshift[4] = largeBshift/(UnitCell::A()*UnitCell::C());
  largeABshift[5] = largeBshift/(UnitCell::B()*UnitCell::C());
  dmat6 largeUSshift = cctbx::adptbx::beta_as_u_star(largeABshift);
//--sigmaa--
  int m(0),i(0);
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) largeShifts[i++] = 0.03; //SA
    if (refinePar[m++]) largeShifts[i++] = 0.1; //SB
    PHASER_ASSERT(OBSVAR_bin[s]); //log(0) is nan
    if (refinePar[m++]) largeShifts[i++] = OBSVAR_bin[s]/2.; //sigma+
    PHASER_ASSERT(SN_bin[s]); //log(0) is nan
    if (refinePar[m++]) largeShifts[i++] = 0.01*SN_bin[s]; //sigma(delta)^2
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) largeShifts[i++] = 0.05; //ScaleK
    if (refinePar[m++]) largeShifts[i++] = largeUshift(); //ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) largeShifts[i++] = 0.05; //PartK
    if (refinePar[m++]) largeShifts[i++] = largeUshift(); //PartU
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
      if (atoms[a].n_xyz == 3)
        for (int x = 0; x < 3; x++) largeShifts[i++] = largeXYZshift[x]; //XYZ
      else
      {
        scitbx::af::small<floatType,3> x_shift = site_sym_table.get(a).site_constraints().independent_params(largeXYZshift);
        for (int x = 0; x < atoms[a].n_xyz; x++) largeShifts[i++] = x_shift[x];
      }
    }
    if (refinePar[m++]) largeShifts[i++] = std::max(0.075,0.15*atoms[a].SCAT.occupancy); // occupancy
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        largeShifts[i++] = largeUshift(); //B (i.e. u_iso)
      } else if (atoms[a].n_adp == 6) {
        for (int n = 0; n < 6; n++) largeShifts[i++] = largeUSshift[n]; //AnisoB (i.e. u_star)
      } else
      {
        scitbx::af::small<floatType,6> u_shift = site_sym_table.get(a).adp_constraints().independent_params(largeUSshift);
        for (int n = 0; n < atoms[a].n_adp; n++) largeShifts[i++] = u_shift[n];
      }
    }
  }
  for (int t = 0; t < AtomFdp.size(); t++)
     if (refinePar[m++]) largeShifts[i++] = std::max(0.07*AtomFdp[t],0.01);
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return largeShifts;
}

std::vector<bounds>  RefineSAD::getLowerBounds()
{
  std::vector<bounds> Lower(npars_ref);
  int m(0),i(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
//in principle zero, but can't imagine being lower than about 0.9 in reality
    if (refinePar[m++]) Lower[i++].on(0.7);//Real part of Dphi
 //Deviation from zero likely to be much smaller
    if (refinePar[m++]) Lower[i++].on(0.0);//Imag part of Dphi
    if (refinePar[m++]) Lower[i++].on(0.0);//Sigma+
    if (refinePar[m++]) Lower[i++].on(0.01*SN_bin[s]); //Sigma(delta): works for nearly complete model
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) Lower[i++].on(0.001);//ScaleK
    if (refinePar[m++]) Lower[i++].on(-upperU());//ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) Lower[i++].on(0.05);//PartK
    if (refinePar[m++]) Lower[i++].on(-upperU());//PartU
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
      for (int x = 0; x < atoms[a].n_xyz; x++) Lower[i++].off(); //XYZ
    }
    if (refinePar[m++]) Lower[i++].on(lowerO()); //O
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        Lower[i++].on(lowerU()); //B (i.e. u_iso)
      } else {
        for (int n = 0; n < atoms[a].n_adp; n++) Lower[i++].off(); //AnisoB (i.e. u_star)
      }
    }
  }
  // Allow to be negative for inverse hand
  for (int t = 0; t < AtomFdp.size(); t++)
     if (refinePar[m++]) Lower[i++].on(0.0001);//f''
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Lower;
}

//make functions of these as they are required in setProtocol too,
//where the atomic parameters are reset to be within the upper and lower limits before starting refinement
floatType RefineSAD::lowerO() { return 0.001; }
floatType RefineSAD::lowerU() { return cctbx::adptbx::b_as_u(DEF_EP_BMIN); }
floatType RefineSAD::upperU() { return cctbx::adptbx::b_as_u(DEF_EP_BMAX); }
floatType RefineSAD::tolU() { return cctbx::adptbx::b_as_u(1.e-03); }
floatType RefineSAD::largeUshift() { return cctbx::adptbx::b_as_u(2.0); }

std::vector<bounds>  RefineSAD::getUpperBounds()
{
  std::vector<bounds> Upper(npars_ref);
  int m(0),i(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) Upper[i++].on(1.0);//Real part of Dphi
    if (refinePar[m++]) Upper[i++].on(0.7);//Imag part of Dphi
    if (refinePar[m++]) Upper[i++].off();//Sigma+
    if (refinePar[m++]) Upper[i++].on(1.5*SN_bin[s]); //Sigma(delta)
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) Upper[i++].off();//ScaleK
    if (refinePar[m++]) Upper[i++].on(upperU());//ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) Upper[i++].off();//PartK
    if (refinePar[m++]) Upper[i++].on(upperU());//PartU
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
      for (int x = 0; x < atoms[a].n_xyz; x++) Upper[i++].off(); //XYZ
    }
    if (refinePar[m++]) Upper[i++].off(); //O
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        Upper[i++].on(upperU()); //B (i.e. u_iso)
      } else {
        for (int n = 0; n < atoms[a].n_adp; n++) Upper[i++].off(); //AnisoB (i.e. u_star)
      }
    }
  }
  // Allow to be negative for inverse hand
  for (int t = 0; t < AtomFdp.size(); t++)
     if (refinePar[m++]) Upper[i++].off(); //f''
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return Upper;
}

std::vector<reparams> RefineSAD::getRepar()
{
  std::vector<reparams> repar(npars_ref);
  int m(0),i(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) repar[i++].off(); //Real part of Dphi
    if (refinePar[m++]) repar[i++].off(); //Imag part of Dphi
    if (refinePar[m++]) repar[i++].on(OBSVAR_bin[s]); //Sigma+
    if (refinePar[m++]) repar[i++].on(OBSVAR_bin[s]/2); //Sigma(delta)
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) repar[i++].on(0.1); //ScaleK
    if (refinePar[m++]) repar[i++].off(); //ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) repar[i++].off(); //PartK
    if (refinePar[m++]) repar[i++].off(); //PartU
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) {
      for (int x = 0; x < atoms[a].n_xyz; x++) repar[i++].off(); //XYZ
    }
    if (refinePar[m++]) repar[i++].off(); //O
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        repar[i++].on(cctbx::adptbx::b_as_u(5.0)); //B (i.e. u_iso)
      } else {
        for (int n = 0; n < atoms[a].n_adp; n++) repar[i++].off(); //AnisoB (i.e. u_star)
      }
    }
  }
  // Allow to be negative for inverse hand
  for (int t = 0; t < AtomFdp.size(); t++)
    if (refinePar[m++]) repar[i++].off();//f''
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return repar;
}

void RefineSAD::cleanUp(outStream where,Output& output)
{
  calcIntegrationPoints(); // Update ready for next cycle
  calcOutliers();
}

std::vector<std::pair<int,int> >  RefineSAD::getAniso()
{
  std::vector<std::pair<int,int> > aniso(0);
  int m(0),i(0);
  //-- sigmaa --
  for (unsigned s = 0; s < bin.numbins(); s++)
  {
    if (refinePar[m++]) i++; //Real part of Dphi
    if (refinePar[m++]) i++; //Imag part of Dphi
    if (refinePar[m++]) i++; //Sigma+
    if (refinePar[m++]) i++; //Sigma(delta)
  }
//-- atomic --
  if (input_atoms)
  {
    if (refinePar[m++]) i++; //ScaleK
    if (refinePar[m++]) i++; //ScaleU
  }
  if (input_partial)
  {
    if (refinePar[m++]) i++; //PartK
    if (refinePar[m++]) i++; //PartU
  }
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    if (refinePar[m++]) i+=atoms[a].n_xyz; //XYZ
    if (refinePar[m++]) i++; //O
    if (refinePar[m++]) {
      if (!atoms[a].SCAT.flags.use_u_aniso_only()) {
        i++; //B (i.e. u_iso)
      } else {
        aniso.push_back(std::pair<int,int>(a,i));
        i+=atoms[a].n_adp;
      } //AnisoB (i.e. u_star)
    }
  }
  // Allow to be negative for inverse hand
  for (int t = 0; t < AtomFdp.size(); t++) if (refinePar[m++]) i++;//f''
  PHASER_ASSERT(i == npars_ref);
  PHASER_ASSERT(m == npars_all);
  return aniso;
}

floatType RefineSAD::getMaxDistSpecial(TNT::Vector<floatType>& x, TNT::Vector<floatType>& g, bool1D& bounded, TNT::Vector<floatType>& dist,floatType& start_distance)
// Return maximum multiple of gradient that can be shifted
// before hitting bounds for "special" parameters, i.e. aniso B-factors.
{
  floatType maxDist(0.),ZERO(0.);
  std::vector<std::pair<int,int> > aniso = getAniso();
  if (!aniso.size()) return ZERO; // Only aniso atoms have special bounds

  for (int s = 0; s < aniso.size(); s++)
  {
    floatType thisdist(std::numeric_limits<floatType>::max());
    int a = aniso[s].first;
    int i = aniso[s].second;
    dmat6 u_star,u_star_grad;
    if (atoms[a].n_adp == 6)
    {
      for (int n = 0; n < 6; n++) u_star[n] = x[i+n];
      for (int n = 0; n < 6; n++) u_star_grad[n] = g[i+n];
    }
    else
    {
      {
      scitbx::af::small<floatType,6> u_indep(atoms[a].n_adp);
      for (int n = 0; n < atoms[a].n_adp; n++) u_indep[n] = x[i+n];
      u_star = site_sym_table.get(a).adp_constraints().all_params(u_indep);
      }
      {
      scitbx::af::small<floatType,6> g_indep(atoms[a].n_adp);
      for (int n = 0; n < atoms[a].n_adp; n++) g_indep[n] = g[i+n];
      u_star_grad = site_sym_table.get(a).adp_constraints().all_params(g_indep);
      }
    }
    bool zero_grad(true);
    for (int n = 0; n < 6; n++) if (u_star_grad[n] != ZERO) zero_grad = false;
    if (zero_grad)
      thisdist = -1.0; // Flag for parameter that isn't moving
    else
    {
      cctbx::adptbx::factor_u_star_u_iso<floatType> factor_x(getCctbxUC(),u_star);
      cctbx::adptbx::factor_u_star_u_iso<floatType> factor_g(getCctbxUC(),u_star_grad);

      // Test upper bound for negative gradient (positive shift),
      // lower bound for positive gradient
      if (factor_g.u_iso < ZERO)
      {
        thisdist = std::max(ZERO,(factor_x.u_iso-upperU())/factor_g.u_iso);
      }
      else if (factor_g.u_iso > ZERO)
      {
        thisdist = std::max(ZERO,(factor_x.u_iso-lowerU())/factor_g.u_iso);
      }
      //First use finite differences to make sure we're not already on boundary heading out
      dvect3 eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),u_star));
      floatType minU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2]));
      floatType maxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2]));
      dmat6 newx;
      floatType delta(1.e-6); // Assume shift normally around one times gradient
      for (int n = 0; n < 6; n++) newx[n] = u_star[n] - delta*u_star_grad[n];
      eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),newx));
      floatType dminU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2])) - minU;
      floatType dmaxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2])) - maxU;
      if ( (dminU < ZERO && minU < lowerU()+tolU())
        || (dmaxU > ZERO && maxU > upperU()-tolU()) ) thisdist = ZERO;
      if (thisdist > ZERO)
      {
        //line search to find value of distance that puts the anisotropy within tolU of
        //lowerU or upperU, whichever limit is the first to be reached
        floatType distance(ZERO),step(1);
        newx = u_star;
        eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),newx));
        floatType test_minU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2]));
        floatType test_maxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2]));
        //Through numerical lack of precision, may come in slightly outside of
        //bounds, so just make sure the violation doesn't get worse
        floatType thisLowerU = std::min(lowerU(),test_minU);
        floatType thisUpperU = std::max(upperU(),test_maxU);
        //Now bracket by finding distance that breaches a limit
        for (;;)
        {
          distance += step;
          for (int n = 0; n < 6; n++) newx[n] = u_star[n] - distance*u_star_grad[n];
          eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),newx));
          test_minU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2]));
          test_maxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2]));
          if (test_minU < thisLowerU || test_maxU > thisUpperU)
            break;
        }
        distance -= step; //go back to last accepted position
        //limits now between distance and distance+step
        //We may get here with U between limit and tolerance, so have to know whether it will get better or worse
        while ( (dminU >= ZERO || (dminU < ZERO && minU > thisLowerU+tolU()) )
             && (dmaxU <= ZERO || (dmaxU > ZERO && maxU < thisUpperU-tolU()) )
             && (step > distance*1.e-8) ) // Step still big enough to make a difference
        {
          dmat6 newx;
          for (int n = 0; n < 6; n++) newx[n] = u_star[n] - distance*u_star_grad[n];
          eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),newx));
          floatType test_minU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2]));
          floatType test_maxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2]));
          if (test_minU < thisLowerU || test_maxU > thisUpperU)
            distance -= step;
          else //accept test as allowed step
          { //do finite difference test to find direction of change
            minU = test_minU, maxU = test_maxU;
            delta = std::min(1.e-6,step/100.);
            for (int n = 0; n < 6; n++) newx[n] = u_star[n] - (distance+delta)*u_star_grad[n];
            eigenUvals = cctbx::adptbx::eigenvalues(cctbx::adptbx::u_star_as_u_cart(UnitCell::getCctbxUC(),newx));
            dminU = std::min(eigenUvals[0],std::min(eigenUvals[1],eigenUvals[2])) - minU;
            dmaxU = std::max(eigenUvals[0],std::max(eigenUvals[1],eigenUvals[2])) - maxU;
          }
          step /= 2.;
          distance += step;
        }
        distance -= step;
        thisdist = std::min(thisdist,std::max(ZERO,distance));
      }
    }
    for (int n = 0; n < atoms[a].n_adp; n++)
    {
      dist[i+n] = thisdist;
      bounded[i+n] = true;
    }
    if (thisdist >= ZERO)
      maxDist = std::max(maxDist,thisdist);
  }
  return maxDist;
}

} //phaser
