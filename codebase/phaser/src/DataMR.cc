//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/src/Bin.h>
#include <phaser/src/DataMR.h>
#include <phaser/io/Errors.h>
#include <scitbx/constants.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/UnitCell.h>
#include <phaser/src/Composition.h>
#include <phaser/io/Output.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <phaser/lib/sphericalY.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/mean.h>
#include <scitbx/math/erf.h>
#include <cmtzlib.h>
#include <scitbx/math/r3_rotation.h>

#ifdef _OPENMP
#include <omp.h>
#endif


namespace phaser {

DataMR::DataMR(data_refl& REFLECTIONS_,
               data_resharp& RESHARP_,
               data_norm& SIGMAN_,
               data_outl& OUTLIER_,
               data_tncs& PTNCS_,
               data_bins& DATABINS_,
               double res1,double res2,
               data_composition& COMPOSITION_,
               MapEnsPtr ensPtr)
          : DataB(REFLECTIONS_,RESHARP_,SIGMAN_,OUTLIER_,PTNCS_,DATABINS_,COMPOSITION_,res1,res2)
{
  TOTAL_SCAT = Composition(COMPOSITION_).total_scat(REFLECTIONS_.SG_HALL,REFLECTIONS_.UNIT_CELL);
  if (ensPtr)
  {
    ensemble = ensPtr;
  }
  else
  {
    MapEnsPtr new_ensPtr(new MapEnsemble());
    ensemble = new_ensPtr; //deep copy on init
  }
  EM_known.resize(NREFL,cmplxType(0,0));
  EM_search.resize(NREFL,cmplxType(0,0));
  totvar_known.resize(NREFL,0);
  totvar_search.resize(NREFL,0);
  sum_Esqr_search.clear();
  max_Esqr_search.clear();
}

void DataMR::initSearchMR()
{
  sum_Esqr_search.clear();
  max_Esqr_search.clear();
  for (unsigned r = 0; r < NREFL; r++)
  {
    totvar_search[r] = 0;
    EM_search[r] = cmplxType(0,0);
  }
}

void DataMR::init()
{
  models_known.resize(0);
  models_perturbRot.resize(0);
  models_perturbTrans.resize(0);
  models_initBfac.resize(0);
  models_initOfac.resize(0);
  models_drms.clear();
  models_cell.clear();
  sum_Esqr_search.clear();
  max_Esqr_search.clear();
  for (unsigned r = 0; r < NREFL; r++)
  {
    totvar_search[r] = 0;
    totvar_known[r] = 0;
    EM_known[r] = cmplxType(0,0);
    EM_search[r] = cmplxType(0,0);
  }
}

void DataMR::calcSearchVAR(std::string modlid,bool halfR,data_bofac SEARCH_FACTORS)
{
  // This function is used for the interpolation of the variance
  // at the identity rotation. It assumes that the variances
  // in the model are spherically symmetric and therefore do not
  // need to be calculated for each rotation separately
  // Numerical differences in the interpolation mean that there
  // will be slightly different values even if the variances are
  // spherically symmetric
  //rotation only
  Ensemble* ens_modlid = &ensemble->find(modlid)->second;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT;
    floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r])); // D^2 for ensemble
              thisV *= scatFactor;                      // becomes sigmaA^2
              //search does not have MULT factor
    if (!SEARCH_FACTORS.defaults())
    {
      thisV *= std::exp(-2.0*SEARCH_FACTORS.BFAC*ssqr(r)/4.0);
      thisV *= SEARCH_FACTORS.OFAC;
    }
    if (PTNCS.use_and_present())
    {
      // halfR=true (look for average orientation of tNCS pair)
      // when called from FRF or BRF, or from BTF or FTF before rotref,
      // false otherwise (BTF or FTF after rotref, RNP)
      if (PTNCS.NMOL > 2 || !halfR)
        thisV *= PTNCS.NMOL*G_Vterm[r];
      else //halfR
      {
        gfun.calcReflTerms(r);
        thisV *= PTNCS.NMOL*gfun.refl_Gsqr_Vterm;
      }
      thisV *= G_DRMS[r];
    }
    totvar_search[r] = thisV;
  }
}

void DataMR::calcSearchROT(dmat33 ROT,std::string modlid,data_bofac SEARCH_FACTORS) //halfR default false
{
  Ensemble* ens_modlid = &ensemble->find(modlid)->second;
  const cmplxType cmplxZERO(0.,0.),cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
  dmat33 R = ROT*ens_modlid->Frac2Orth();
  dmat33 Q1 = UnitCell::doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();
  if (sum_Esqr_search.empty()) sum_Esqr_search.resize(NREFL,0), max_Esqr_search.resize(NREFL,0);
  //rotation only
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType repsn = epsn(r);
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP;
    sum_Esqr_search[r] = 0;
    max_Esqr_search[r] = 0;
    EM_search[r] = cmplxZERO;
    std::vector<miller::index<int> >  rhkl = rotMiller(r);
    // Scale includes multiplying Esqr by epsilon^2 for missing symops, dividing by epsilon
    // for normalisation
    floatType terms = repsn*scatFactor;
    if (!SEARCH_FACTORS.defaults())
    {
      terms *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/2.0); // for Esqr
      terms *= SEARCH_FACTORS.OFAC;
    }
    if (PTNCS.use_and_present())
    {
      gfun.calcReflTerms(r);
      terms *= G_DRMS[r];
    }

    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
      {
        dvect3    RotSymHKL = Q1tr*rhkl[isym];
        cmplxType thisE = ens_modlid->InterpE(RotSymHKL,DFAC[r]);
        //search does not have MULT factor
        if (PTNCS.use_and_present())
        {
          floatType theta  = rhkl[isym][0]*PTNCS.TRA.VECTOR[0] +
                             rhkl[isym][1]*PTNCS.TRA.VECTOR[1] +
                             rhkl[isym][2]*PTNCS.TRA.VECTOR[2];
          cmplxType ptncs_scat = cmplxONE; //iMOL=0
          for (floatType iMOL = 1; iMOL < PTNCS.NMOL; iMOL++)
            ptncs_scat += std::exp(TWOPII*iMOL*theta);
          ptncs_scat *= gfun.refl_G[isym]; //FRF,BRF rotation unknown
          thisE *= ptncs_scat;
        }
        floatType thisEsqr = thisE.real()*thisE.real() + thisE.imag()*thisE.imag();
                  thisEsqr *= terms;
        sum_Esqr_search[r] += thisEsqr;
        max_Esqr_search[r]  = std::max(max_Esqr_search[r],thisEsqr);
      }
    }
  }
}

// Calculate the non-varying parameters for a translation function
// Variance values interpolated from a molecular transform are
// precalculated and passed to the function. The solvent content is
// calculated from the search model.
void DataMR::calcSearchTRA1(dmat33 ROT,std::string modlid)
{
  sum_Esqr_search.clear();
  max_Esqr_search.clear();
}

// Calculate the varying parameters for a translation function
// E values interpolated from a molecular transform are
// precalculated and passed to the function
void DataMR::calcSearchTRA2(dvect3 TRA,bool halfR,data_bofac SEARCH_FACTORS,cmplx1D* interpE)
{
  const cmplxType cmplxZERO(0.,0.),cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);

  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType rssqr = ssqr(r);
    EM_search[r] = cmplxZERO;
    std::vector<miller::index<int> > rhkl = rotMiller(r);
    floatType terms = sqrt_epsn[r];
    if (PTNCS.use_and_present())
    {
      gfun.calcReflTerms(r);
      terms *= std::sqrt(G_DRMS[r]);
    }
    if (!SEARCH_FACTORS.defaults())
    {
      terms *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
      terms *= std::sqrt(SEARCH_FACTORS.OFAC);
    }
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
      {
        cmplxType thisE = (*interpE)[r*SpaceGroup::NSYMP+isym];
                  thisE *= terms;
                  thisE *= dphi(r,isym,rhkl[isym],TRA);
        if (PTNCS.use_and_present())
        {
          floatType theta  = rhkl[isym][0]*PTNCS.TRA.VECTOR[0] +
                             rhkl[isym][1]*PTNCS.TRA.VECTOR[1] +
                             rhkl[isym][2]*PTNCS.TRA.VECTOR[2];
          if (PTNCS.NMOL > 2)
          {
            cmplxType ptncs_scat = cmplxONE; //iMOL=0
            for (floatType iMOL = 1; iMOL < PTNCS.NMOL; iMOL++)
              ptncs_scat += std::exp(TWOPII*iMOL*theta);
            thisE *= ptncs_scat;
            //Gfunction term unity since no rotation
          }
          else if (halfR) //second molecule comes from doubling at same rot
          {
            thisE *= cmplxONE + std::exp(TWOPII*theta);
            thisE *= gfun.refl_G[isym];
          }
        }
        EM_search[r] += thisE;
      }
    }
  }
}

void DataMR::calcKnownVAR()
{
  // This function is used for the interpolation of the variance
  // at the identity rotation. It assumes that the variances
  // in the model are spherically symmetric and therefore do not
  // need to be calculated for each rotation separately
  // Numerical differences in the interpolation mean that there will
  // be slightly different values even if the variances are
  // spherically symmetric
  for (unsigned r = 0; r < NREFL; r++)
    totvar_known[r] = 0;

  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType rssqr = ssqr(r);
    //save this interpolation, so not doing multiple times for multiple copies
    map_str_float Vmap;
  //stored  if (PTNCS.use_and_present()) gfun.calcReflTerms(r);
    for (unsigned k = 0; k < models_known.size(); k++)
    {
      std::string modlid = models_known[k].MODLID;
      floatType thisV(0);
      if (Vmap.find(modlid) != Vmap.end())
      {
        thisV = Vmap[modlid];
      }
      else
      {
        Ensemble* ens_modlid = &ensemble->find(modlid)->second;
        floatType sqrtrssqr = std::sqrt(rssqr);
        ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
        thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
        floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT;
        thisV *= scatFactor;
        Vmap[modlid] = thisV;
      }
      thisV *= models_known[k].OFAC;
      thisV *= std::exp(-2.0*models_known[k].BFAC*rssqr/4.0);
      thisV /= models_known[k].MULT;
      if (PTNCS.use_and_present())
        thisV *= G_DRMS[r]*G_Vterm[r]; //not correct, B-factor differences
      totvar_known[r] += thisV;
    }
  }
}

void DataMR::addInterpV(float1D& interpV)
{
  PHASER_ASSERT(interpV.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    totvar_known[r] += interpV[r];
}

void DataMR::addInterpE(cmplx1D& interpE)
{
  PHASER_ASSERT(interpE.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    EM_known[r] += interpE[r];
}

void DataMR::setInterpV(float1D& interpV)
{
  PHASER_ASSERT(interpV.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    totvar_known[r] = interpV[r];
}

void DataMR::setInterpE(cmplx1D& interpE)
{
  PHASER_ASSERT(interpE.size() == NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    EM_known[r] = interpE[r];
}

cmplx1D DataMR::getInterpE()
{
  return EM_known;
}

float1D DataMR::getInterpV()
{
  return totvar_known;
}

// Calculate the parameters for MR
// E values are interpolated from a molecular transform of the
// ensemble of structures.
// If shift (perturbation) parameters are changed, also have to change
// RefineMR::applyShift and RefineMR::getRefinePars
void DataMR::calcKnown()
{
  sum_Esqr_search.clear(); //because there is no sum_Esqr_known
  max_Esqr_search.clear();
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    EM_known[r] = cmplxType(0,0);
  }

  dmat33 ROT,PRtr,Rperturb;
  dvect3 TRA,CMori,CMrot,CMperturb;

  for (unsigned k = 0; k < models_known.size(); k++)
  {
    std::string modlid = models_known[k].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    //recover original centre of mass, rotated centre of mass
    PRtr = ens_modlid->PR.transpose();
    CMori = -PRtr*ens_modlid->PT;
    dmat33 ROT = models_known[k].R;
    CMrot = ROT*CMori;
    //apply xyz rotation perturbation to molecular transform
    //orientation (for refinement)
    Rperturb = ens_modlid->PR;
    for (int dir = 0; dir < 3; dir++)
      Rperturb = xyzRotMatDeg(dir,models_perturbRot[k][dir])*Rperturb;
    Rperturb = PRtr*Rperturb;
    ROT = ROT*Rperturb;
    CMperturb = ROT*CMori;
    //correct for PR (refer rotation to reoriented ensemble)
    ROT = ROT*PRtr;

    TRA = models_known[k].TRA;
    dvect3 iOrthT = UnitCell::doFrac2Orth(TRA);
    //apply xyz translation perturbation (for refinement)
    iOrthT += models_perturbTrans[k];
    //correct for rotation of centre of mass
    iOrthT += CMrot - CMperturb;
    //correct for PT (translation to origin of ensemble)
    dvect3 eOrthT = iOrthT - ROT*ens_modlid->PT;
    TRA = UnitCell::doOrth2Frac(eOrthT);

    dmat33 R = ROT*ens_modlid->Frac2Orth();
    dmat33 Q1 = UnitCell::doOrth2Frac(R);
    dmat33 Q1tr = Q1.transpose();

    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      floatType sqrt_repsn = sqrt_epsn[r];
      floatType rssqr = ssqr(r);
      floatType sqrtrssqr = std::sqrt(rssqr);
      ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
      std::vector<miller::index<int> > rhkl = rotMiller(r);
      floatType sqrt_scatFactor = std::sqrt(ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
      floatType terms  = sqrt_scatFactor;
                terms *= sqrt_repsn;
                terms *= std::sqrt(models_known[k].OFAC);
                terms *= std::exp(-models_known[k].BFAC*rssqr/4.0);
                terms /= models_known[k].MULT;
      if (PTNCS.use_and_present())
                terms *= std::sqrt(G_DRMS[r]);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3    RotSymHKL = Q1tr*rhkl[isym];
          cmplxType thisE =  ens_modlid->InterpE(RotSymHKL,DFAC[r]);
                    thisE *= terms;
                    thisE *= dphi(r,isym,rhkl[isym],TRA);
          //no G function correction (gfun), models in separate (real) rotations
          EM_known[r] += thisE;
        }
      }
    }
  }
}

double DataMR::likelihoodFn() { return Target()-LLwilson; }

double DataMR::Target()
{
//  Compute likelihood score for all x and r.  If there are any
//  relative phase ambiguities,use Wilson or Rice function approximation.
//  For the Rice function approximation,the largest single contribution
//  is taken as a partial structure factor and the variance is incremented
//  by the remaining contributions
//
//  The likelihood over all the data sets is the product of the likelihood for
//  the individual data sets (sum of the logs)
//  This does not take into account the correlation between them

  double totalLL(0);

  if (FTFMAP.FMAP.size() && //data entered as map
      sum_Esqr_search.empty()) //not a rotation function
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) totalLL += MLHL_refl(r);
  }
  else
  {
    for (unsigned r = 0; r < NREFL; r++)
    if (selected[r]) totalLL += Rice_refl(r);
  }
  return totalLL;
}

floatType DataMR::MLHL_refl(int r)
{ return Rice_refl(r); }

floatType DataMR::Rice_refl(int r)
{
  floatType reflLL(0);
  cmplxType EM(EM_known[r]+EM_search[r]);
  floatType EM_real = EM.real();
  floatType EM_imag = EM.imag();
  floatType phasedEsqr = EM_real*EM_real+EM_imag*EM_imag;
  floatType sumEsqr_m_maxEsqr(0);
  if (sum_Esqr_search.size())//assume && max_Esqr_search.size())
  {
    sumEsqr_m_maxEsqr += phasedEsqr + sum_Esqr_search[r];
    sumEsqr_m_maxEsqr -= std::max(phasedEsqr,max_Esqr_search[r]);
  }
  // Note that, after interpolation of molecular transform, EM is systematically underestimated
  // by a few percent

  floatType V(0);
  V = PTNCS.EPSFAC[r];
  V -= totvar_known[r];
  V -= totvar_search[r];
  V += sumEsqr_m_maxEsqr;

  bool rcent = cent(r);
  floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
  floatType Esqr = fn::pow2(E);

  floatType ECsqr = max_Esqr_search.size() ? std::max(phasedEsqr,max_Esqr_search[r]) : phasedEsqr;
  PHASER_ASSERT(ECsqr >= 0);
  floatType EC = std::sqrt(ECsqr);
  floatType X(0);
//#define PHASER_NEGVAR_DEBUG
#ifdef PHASER_NEGVAR_DEBUG
  bool debugging(false);
  if (V <= 0) goto debug_code;
  if (debugging && r==0) goto debug_code;
#else
  PHASER_ASSERT(V > 0);
#endif
  X = 2.0*E*EC/V;
  reflLL = -(std::log(V)+(Esqr + ECsqr)/V);
  PHASER_ASSERT(X >= 0);
  if (rcent)
  {
    X /= 2.0;
    reflLL = reflLL/2.0 + m_alogch.getalogch(X);
  }
  else reflLL += m_alogchI0.getalogI0(X); //acentric
  return reflLL;

//debugging
#ifdef PHASER_NEGVAR_DEBUG
debug_code:
  {
    std::cout << "debugging reflection # " << r << std::endl;
    std::cout << "debugging scattering " << TOTAL_SCAT << std::endl;
    for (int k = 0; k < models_known.size(); k++)
    {
      std::cout << "debugging known #" << k+1 <<  " bfac=" << models_known[k].BFAC << std::endl;
      std::cout << "debugging known #" << k+1 <<  " ofac=" << models_known[k].OFAC << std::endl;
    }
    std::cout << "debugging " << "reso " <<  reso(r) << std::endl;
    int s = rbin(r);
    std::cout << "debugging " << "bin #" <<  s+1 << " of " << bin.numbins() <<  std::endl;
    if (PTNCS.use_and_present())
    {
      std::cout << "debugging calc PTNCS " << std::endl;
      bool b(false);
      unsigned rr = r;
      gfun.calcReflTerms(rr);
      std::cout << "   debugging G Vterm: " << gfun.refl_G_Vterm << std::endl;
      std::cout << "   debugging stored G Vterm: " << G_Vterm[r] << std::endl;
      std::cout << "   debugging Gsqr Vterm: " << gfun.refl_Gsqr_Vterm << std::endl;
      std::cout << "   debugging G DRMS: " << G_DRMS[r] << std::endl;
      for (unsigned isym = 0; isym < gfun.refl_G.size(); isym++)
        std::cout << "   debugging G isym:" << isym+1 << " " << gfun.refl_G[isym] << std::endl;
      std::cout << "debugging nmol: " << PTNCS.NMOL << std::endl;
    }
    std::cout << "debugging epsfac: " << PTNCS.EPSFAC[r] << std::endl;
    std::cout << "debugging " << "epsn " <<  epsn(r) << std::endl;
    std::cout << "debugging " << "EM_known " << EM_known[r] << std::endl;
    std::cout << "debugging " << "EM_search " << EM_search[r] << std::endl;
    std::cout << "debugging " << "EM_known+EM_search " << EM_known[r] + EM_search[r] << std::endl;
    std::cout << "debugging " << "EC " << EC << " ECsqr " << ECsqr << std::endl;
    std::cout << "debugging " << "E " << E << " Esqr " << Esqr << std::endl;
    std::cout << "debugging " << "phasedEsqr " << phasedEsqr << std::endl;
    if (sum_Esqr_search.size())//assume && max_Esqr_search.size())
    {
    std::cout << "debugging " << "max_Esqr_search " <<max_Esqr_search[r] <<  std::endl;
    std::cout << "debugging " << "sum_Esqr_search " <<sum_Esqr_search[r] <<  std::endl;
    std::cout << "debugging " << "sumEsqr_m_maxEsqr " << sumEsqr_m_maxEsqr << std::endl;
    }
    std::cout << "debugging " << "-totvar_known " << -totvar_known[r] <<std::endl;
    std::cout << "debugging " << "-totvar_search " << -totvar_search[r] <<  std::endl;
    std::cout << "debugging " << "-totvar_known-totvar_search " << -totvar_known[r]-totvar_search[r] <<  std::endl;
    std::cout << "debugging " << "V =" << V << std::endl;
    std::cout << std::endl;
    PHASER_ASSERT(ECsqr >= 0);
    PHASER_ASSERT(V > 0);
  }//debug
#endif
  return reflLL;
}

pair_flt_flt DataMR::ScaleFC()
{
  // Compute overall scale and B that approximates least-squares minimum of Fo vs Fc
  // for isotropically-scaled Fo.  Allow for single bin when resolution limited for R-factor calc.
  af_float F_ISO = getCorrectedF(false); // Don't resharpen data here
  PHASER_ASSERT(F_ISO.size() == NREFL);

  double WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  int nbins(SIGMAN.BINS.size());
  float1D sumFoFc(nbins,0.),sumFcSqr(nbins,0.);
  int1D NinBin(nbins,0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    // Put calculated E-values on absolute scale using isotropic component of SigmaN
    floatType sigmaA_Ecalc(std::abs(EM_known[r]+EM_search[r]));
    unsigned s(rbin(r));
    double epsnSN_iso = epsn(r) *
           binIsoFactor(ssqr(r),SIGMAN.BINS[s],SIGMAN.WILSON_B,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    floatType FC(sigmaA_Ecalc*std::sqrt(epsnSN_iso));
    if (FC > 0 && F_ISO[r] > 0)
    {
      NinBin[s]++;
      sumFoFc[s] += F_ISO[r]*FC;
      sumFcSqr[s] += fn::pow2(FC);
    }
  }
  // Determine overall scale and B that approximates overall least-squares optimum by
  // fitting log of optimal scale in each bin vs resolution
  floatType sum(0),sumx(0),sumx2(0),sumy(0),sumxy(0);
  int nbinsused(0);
  for (unsigned s = 0; s<nbins; s++)
  if (NinBin[s])
  {
    double logkbin(std::log(sumFoFc[s]/sumFcSqr[s])); // Log of least-squares optimal scale for bin
    double ssqr_bin(1.0/fn::pow2(bin.MidRes(s)));
    double W(NinBin[s]); // Weight by number of reflections in bin
    sum += W;
    sumx += W*ssqr_bin;
    sumx2 += W*fn::pow2(ssqr_bin);
    sumy += W*logkbin;
    sumxy += W*ssqr_bin*logkbin;
    nbinsused++;
  }

  double slope,intercept,scaleK(1.),scaleB(0.);
  if (nbinsused > 1)
  {
    PHASER_ASSERT(sum*sumx2-fn::pow2(sumx));
    slope = (sum*sumxy-(sumx*sumy))/(sum*sumx2-fn::pow2(sumx));
    intercept = (sumy - slope*sumx)/sum;
    scaleK = std::exp(intercept);
    scaleB = -4*slope;
  }
  else if (nbinsused == 1)
    scaleK = std::exp(sumy/sum);
  return std::pair<floatType,floatType>(scaleK,scaleB);
}

floatType DataMR::Rfactor()
{
  // Compute R-factor for isotropically-corrected Fobs
  af_float F_ISO = getCorrectedF(false); // Don't resharpen data here

  std::pair<floatType,floatType> scale = ScaleFC();
  floatType scaleK(scale.first);
  floatType scaleB(scale.second);
  double WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  long double totalR_numerator(0),totalR_denominator(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    // Put calculated E-values on absolute scale using isotropic component of SigmaN
    floatType sigmaA_Ecalc(std::abs(EM_known[r]+EM_search[r]));
    unsigned s(rbin(r));
    double epsnSN_iso = epsn(r) *
           binIsoFactor(ssqr(r),SIGMAN.BINS[s],SIGMAN.WILSON_B,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I);
    floatType FC(sigmaA_Ecalc*std::sqrt(epsnSN_iso));
    totalR_numerator += std::fabs(F_ISO[r]-FC*scaleK*std::exp(-scaleB*ssqr(r)/4));
    totalR_denominator += F_ISO[r];
  } // r loop
  return (totalR_denominator > 0.) ? 100*totalR_numerator/totalR_denominator : -1.0 ;
}

// Linear algebra for the interpolation routines:
// ----------------------------------------------
//     PR          is the rotation matrix applied to molecule from PDB to orient
//                 along principal axes before computing molecular transform (MT)
//     PRtr        transpose (inverse) of P (=MTrT)
//     PT          is the translation that centres PDB molecule on origin
//                 after rotating by PR
//     Xpdb        are the original Angstrom coordinates from PDB file(s).
//     Xmol        are the orthogonal Angstrom coordinates of molecule in orientation
//                 of the MT
//
//     Xmol = PR*Xpdb + PT,or
//     Xpdb = PRtr*Xmol - PRtr*PT,
//
//     xmol        are the fractional coordinates in MT cell of Xmol
//     Frac2OrthMT is the orthogonalising matrix for the MT cell
//
//     Xmol = Frac2OrthMT * xmol
//
//     R           the applied test rotation
//     T           the applied test translation in Angstroms
//     t           the applied test translation in fractional coordinates
//                 (this is the translation given,not T)
//     Orth2FracX  is the fractionalising matrix for the crystal cell
//
//     t = Orth2FracX*T
//
//     Xx          are orthogonal Angstrom coordinates in crystal,after rotating
//                 and translating PDB coordinates by R and T
//
//     Xx = R*Xpdb + T
//
//     xx          are fractional coordinates in crystal
//
//     To interpolate molecular transform,we need to relate xx to xmol
//
//     xx = Orth2FracX*Xx
//        = Orth2FracX*(R*Xpdb+T)
//        = Orth2FracX*R*Xpdb + Orth2FracX*T
//        = Orth2FracX*R*Xpdb + t
//        = Orth2FracX*R*(PRtr*Xmol-PRtr*PT) + t
//        = Orth2FracX*R*PRtr*Xmol - Orth2FracX*R*PRtr*PT + t
//        = Orth2FracX*R*PRtr*Frac2OrthMT*xmol - Orth2FracX*R*PRtr*PT + t
//
//     Interpolation
//     indexes into MT by multiplying crystal HKL by transpose of Q1
//       Q1 = Orth2FracX*R*PRtr*Frac2OrthMT
//
//     corrects phase by adding 2*Pi*(HKL.v1)
//       v1 = t - Orth2FracX*R*PRtr*PT
//

void DataMR::initKnownMR(mr_set MRSET)
{
// this loads input set into the models arrays
  models_known.clear();
  models_perturbRot.clear();
  models_perturbTrans.clear();
  models_initBfac.clear();
  models_initOfac.clear();
  for (unsigned k = 0 ; k < MRSET.KNOWN.size(); k++)
  {
    models_known.push_back(MRSET.KNOWN[k]);
    models_perturbRot.push_back(dvect3(0,0,0));
    models_perturbTrans.push_back(dvect3(0,0,0));
  }
//now convert the non-fractional coordinates to fractional
  for (unsigned k = 0; k < models_known.size(); k++)
    if (!models_known[k].FRAC ) //order important
    {
      dvect3 tmp = models_known[k].TRA;
      dvect3 tmp2 = UnitCell::doOrth2Frac(tmp);
      models_known[k].setFracT(tmp2);
    }
  //start by putting values in range
  for (unsigned k = 0; k < models_known.size(); k++)
  {
    if (models_known[k].BFAC < lowerB()+tolB())
      models_known[k].BFAC = (lowerB()+tolB());
    if (models_known[k].BFAC > upperB()-tolB())
      models_known[k].BFAC = (upperB()-tolB());
    if (models_known[k].OFAC < double(DEF_OFAC_SCALE_MIN))
      models_known[k].OFAC = (DEF_OFAC_SCALE_MIN);
    if (models_known[k].OFAC > double(DEF_OFAC_SCALE_MAX))
      models_known[k].OFAC = (DEF_OFAC_SCALE_MAX);
  }
  float1D mapEnsScatKnown(models_known.size());
  for (unsigned k = 0; k < models_known.size(); k++)
  {
    std::string modlid = models_known[k].MODLID;
    PHASER_ASSERT(ensemble->find(modlid) != ensemble->end()); //paranoia
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    mapEnsScatKnown[k] = ens_modlid->maxAtomScatRatio()*double(DEF_OFAC_SCALE_MIN)*ens_modlid->SCATTERING/TOTAL_SCAT;
  }
//----------- Bfactors
  // Make sure that B-factor-corrected scattering at resolution limit does not exceed total
  // In refinement this should be kept under control by getMaxDistSpecial, but solutions
  // provided manually as input or assembled in amalgamation step may violate this constraint.
  // If so, increase B-factors of all placed components.
  floatType fracmodeled(0);
  double testreso = fullHiRes(); //only local resolution
  floatType kssqr = -2*fn::pow2(1/testreso)/4;
  for (unsigned k = 0; k < models_known.size(); k++)
    fracmodeled += mapEnsScatKnown[k];
  floatType fracleft(1.-fracmodeled);
  bool overshot;
  PHASER_ASSERT(fracleft > 0.);
  do
  {
    overshot = false;
    floatType fracmodeled_B(0);
    for (unsigned k = 0; k < models_known.size(); k++)
      fracmodeled_B += mapEnsScatKnown[k]*std::exp(kssqr*models_known[k].BFAC);
    floatType fracleftmin = fracleft*std::exp(kssqr*(upperB()-tolB())); // Minimum B-weighted scattering left
    floatType fracleft_B = 1.-fracmodeled_B;
    if (fracleft_B < fracleftmin)
    { // What deltaB is needed to leave enough scattering at high resolution?
      floatType deltaB = 2.*fn::pow2(testreso)*std::log(fracmodeled_B/(1.-fracleftmin));
      for (unsigned k = 0; k < models_known.size(); k++)
      {
        floatType Bfac(models_known[k].BFAC);
        models_known[k].BFAC = (Bfac+deltaB);
      }
      //put upper B back in range if it overshot
      for (unsigned k = 0; k < models_known.size(); k++)
        if (models_known[k].BFAC > upperB()-0.9*tolB()) // Keep reasonably well within limits
        {
          models_known[k].BFAC = (upperB()-tolB());
          overshot = true;
        }
    }
  } while (overshot); // Run through again if upperB limit restricted correction
  for (unsigned k = 0 ; k < MRSET.KNOWN.size(); k++)
  {
    models_initBfac.push_back(models_known[k].BFAC);
    models_initOfac.push_back(models_known[k].OFAC);
  }

  for (unsigned k = 0; k < models_known.size(); k++)
  {
    std::string modlid = models_known[k].MODLID;
    Ensemble* ens_modlid = &ensemble->find(modlid)->second;
    double VRMS_DELTA = (MRSET.DRMS.find(modlid) == MRSET.DRMS.end()) ? 0.0 : MRSET.DRMS[modlid];
    models_drms[modlid] = VRMS_DELTA;
    ens_modlid->setup_vrms(VRMS_DELTA);
    double CELL_SCALE = (MRSET.CELL.find(modlid) == MRSET.CELL.end()) ? 1.0 : MRSET.CELL[modlid];
    models_cell[modlid] = ensemble->has_pdb() ? 1.0/MRSET.DATACELL : CELL_SCALE;
    if (getenv("PHASER_STUDY_PARAMS") == NULL) //constrain values in the case of pdb files as models
    {
    if (ensemble->has_pdb()) //and therefore need to do cell restraint, because it is for data only
    {
      for (ensIter iter = ensemble->begin(); iter != ensemble->end(); iter++)
      if (models_cell.find(iter->first) != models_cell.end())
      if (models_cell.find(ensemble->begin()->first) != models_cell.end())
      if (models_cell[iter->first] != models_cell[ensemble->begin()->first])
      { //make sure all the cells are numerically identical to start with, set to first (for no good reason)
        models_cell[iter->first] = models_cell[ensemble->begin()->first]; //CELL
      }
      if (models_cell.find(modlid) != models_cell.end())
      ens_modlid->setup_cell(models_cell[modlid]);
    }
    }
  }
}

void DataMR::deleteLastKnown()
{
  models_known.pop_back();
  models_perturbRot.pop_back();
  models_perturbTrans.pop_back();
  models_initBfac.pop_back();
  models_initOfac.pop_back();
}

cmplx1D  DataMR::getInterpE(dmat33 ROT,std::string modlid)
{
  Ensemble* ens_modlid = &ensemble->find(modlid)->second;
  cmplx1D interpE;
  dmat33 R = ROT*ens_modlid->Frac2Orth();
  dmat33 Q1 = UnitCell::doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();
  interpE.resize(SpaceGroup::NSYMP*NREFL);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType sqrt_scatFactor = std::sqrt(ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP);
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      cmplxType thisE = ens_modlid->InterpE(Q1tr*rotMiller(r,isym),DFAC[r]);
                thisE *= sqrt_scatFactor;
      //store InterpE*fraction_scattering
      interpE[r*SpaceGroup::NSYMP+isym] = thisE;
    }
  }
  return interpE;
}

cmplx3D DataMR::getELMNxR2(std::string TARGET,Output& output,int LMAX,double LMAX_RESO)
{
  output.logBlank(VERBOSE);
  output.logTab(1,LOGFILE,"Elmn for Data");
  if (TARGET != "CROWTHER" && TARGET != "LERF1")
  throw PhaserError(FATAL,"Unknown target " + TARGET);

  //function return
  cmplx3D elmn;

  int ZSYMM(0);

  int axis(highOrderAxis());
  if (axis==1) ZSYMM=orderX();
  if (axis==2) ZSYMM=orderY();
  if (axis==3) ZSYMM=orderZ();

  output.logTab(1,VERBOSE,"Highest symmetry of multiplicity "+itos(ZSYMM)+" around axis number "+itos(axis));

  p3Dc hkl;
  floatType intensity(0.);
  int countr = 0;
  const floatType ZERO(0.),ONE(1.),TWO(2.);

  floatType HIRES(10000);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    HIRES = std::min(static_cast<floatType>(reso(r)),HIRES);
    countr++;
  }
  output.logTab(1,VERBOSE,"Number of reflections within resolution range is "+itos(countr* SpaceGroup::NSYMP));
  countr = 0;

  const int lmax(LMAX); //change notation
//  besselx.resize(lmax+2);

  output.logTab(1, VERBOSE, "Maximum degree l of spherical harmonics, Y[l,m](theta, phi), is " + itos(lmax));
  output.logTab(1,VERBOSE,"Number of Symmetries is  "+itos(SpaceGroup::NSYMP));
  output.logBlank(VERBOSE);

  elmn.resize(lmax/2); // l=2 => [0],  l=lmax => [lmax/2-1]
  for (int l = 2; l <= lmax; l += 2)
  {
    int l_index = l/2-1;
    int nmax = (lmax-l+2)/2;

    elmn[l_index].resize(2*l+1);
    for (int m = 0; m <= l; m++)
    {
      elmn[l_index][l+m].resize(nmax+1);
      elmn[l_index][l-m].resize(nmax+1);

      for (int n = 1; n <= nmax; n++)
      {
        elmn[l_index][l+m][n] = 0;
        elmn[l_index][l-m][n] = 0;
      }
    }
  }

  output.startClock();
  floatType V(0),cweight(0);

  HKL_clustered HKL_list;

  bool TARGET_CROWTHER(TARGET == "CROWTHER");
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    if (reso(r) > HIRES)
    {
      floatType Esqr = fn::pow2(Feff[r]/SIGMAN.sqrt_epsnSN[r]);
      std::vector<miller::index<int> >  rhkl = rotMiller(r);
      /* EMfixSqr is (sigmaA*Ecalc)^2 for sum of known components,
         i.e. this accounts for fraction of scattering and coordinate error.
         sum_Esqr_known will be zero unless SOLU 3DIM is resurrected.
         variance (V) is SigmaN'/SigmaN from FRF paper:
         SigmaN'/SigmaN = 1 + (sigmaA*Ecalc)^2 - sigmaA^2
         Note that totvar_known is basically sigmaA^2 (possibly corrected by G-function)
         Comparing what is here with equation (18) of the FRF paper, the
         observed coefficients have been multiplied by SigmaN but then the
         calculated coefficients (It term) are computed in terms of E-values, which
         is where the other factor of SigmaN goes.  For the fixed model terms in
         V, any factors of sigmaA include the DFAC (= Dobs) factor for the LLGI
         target to account for the effect of measurement error.
         For the moving component, the factor of DFAC (= Dobs) squared is applied
         to the normalised intensity coming from Feff, instead of being combined
         with sigmaA weighting of the E values for the model. This is because the
         hkl values do not remain constant for the rotating model. In equation (17)
         of the FRF paper, it's equivalent to apply the factor of Dobs^2 to the
         It (data and fixed model) term rather than trying to associate it with the
         rotating model terms in Is.
      */
      cweight = cent(r) ? ONE : TWO;
      floatType EMfixSqr(EM_known[r].real()*EM_known[r].real()+EM_known[r].imag()*EM_known[r].imag());
      V = PTNCS.EPSFAC[r] - totvar_known[r] + EMfixSqr;
      intensity = TARGET_CROWTHER ? Esqr : cweight*(Esqr-V)/fn::pow2(V);
      intensity *= fn::pow2(DFAC[r]);
      if (PTNCS.use_and_present()) gfun.calcReflTerms(r);

      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Generate only unique set of P1 indices
        {
          int indexh = rhkl[isym][0];
          int indexk = rhkl[isym][1];
          int indexl = rhkl[isym][2];
          dvect3 hklscaled = UnitCell::HKLtoVecS(indexh,indexk,indexl);
          if (axis==3)
          {
            hkl.x = hklscaled[0];
            hkl.y = hklscaled[1];
            hkl.z = hklscaled[2];
          }
          else if (axis==2)
          {
            hkl.y = hklscaled[0];
            hkl.z = hklscaled[1];
            hkl.x = hklscaled[2];
          }
          else
          {
            hkl.z = hklscaled[0];
            hkl.x = hklscaled[1];
            hkl.y = hklscaled[2];
          }
          HKL HKL_temp;
          HKL_temp.intensity = intensity;
          if (PTNCS.use_and_present()) HKL_temp.intensity *= G_DRMS[r]*gfun.refl_Gsqr[isym];
          HKL_temp.htp = toPolar(hkl);
          if (reso(r) > LMAX_RESO) HKL_list.add(HKL_temp);
        }
      }
    }
  }
  //randomize the list to even up the number on each processor
  HKL_list.shuffle();

  float1D sqrt_table(3*lmax);
  for (int i=0; i<3*lmax; i++) sqrt_table[i]=sqrt(TWO*i+1);

// OpenMP reduction() clause cannot be used for parallelising the summing of
// the elmn variable as it hasn't got operator+ overloaded. So omp_elmn
// serves as an omp extra variable to that end
  cmplx1D omp_elmn_first;
  int nthreads = 1;
  int num_elmn(0);

#pragma omp parallel
  {
    int nthread = 0;
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
    nthread = omp_get_thread_num();
    //if (nthread != 0) output.usePhenixCallback(false);
#endif

#pragma omp single
    {
      if (nthreads > 1)
        output.logTab(1,LOGFILE,"Spreading calculation onto "+itos(nthreads)+ " threads.");
      output.logProgressBarStart(LOGFILE,"Elmn Calculation for Data",HKL_list.clustered.size()/nthreads);

      for (int m = 0; m <= lmax; m++)
      {
        if ((m/ZSYMM)*ZSYMM !=m) continue;
        for (int l = m; l <= lmax ; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          int nmax = (lmax-l+2)/2;
         // int l_index = l/2-1;
          for (int n = 1; n <= nmax; n++)
          {
            num_elmn++;
          } // n loop
        } // l loop
      } // m loop
      omp_elmn_first.resize(nthreads*num_elmn,0);
    }

    //p3Dp htp;
    lnfactorial lnfac;

#pragma omp for
    for ( int c=0 ; c < HKL_list.clustered.size(); c++)
    {
      float1D spharmfn;
      PHASER_ASSERT(HKL_list.clustered[c].size());
      // floatType x = std::cos(HKL_list.clustered[c][0].htp.theta);
      // floatType sqRoot = std::sqrt(std::max(0.0,1.0-x*x));
      cmplxType cos_sin = std::exp(cmplxType(0.,HKL_list.clustered[c][0].htp.theta));
      floatType x = std::real(cos_sin);
      floatType sintheta = std::imag(cos_sin); // Previously sqRoot
      //floatType Pmj(0),Pmm(0),Pmm1(0);
      floatType Pmj(0),Pmm1(0);
      for (int m = 0; m <= lmax; m++)
      {
        if ((m/ZSYMM)*ZSYMM !=m) continue;
        floatType Pmm_j(0),Pmm1_j(0);
        floatType Pmm(1);
        if (m>0) {
          int odd_fact=1;
          for (int i=1; i<=m; i++) {
            // Pmm*= -sqRoot*odd_fact;
            Pmm*= -sintheta*odd_fact;
            odd_fact += 2;
          }
        }
        for (int l = m; l <= lmax ; l += 2)
        {
          if (l%2 == 1) l++;
          if (l < 2) continue;
          floatType normalization = std::sqrt((2*l+1)/scitbx::constants::four_pi);
          floatType scale = normalization * std::exp(0.5*(lnfac.get(l-scitbx::fn::absolute(m))-lnfac.get(l+scitbx::fn::absolute(m))));
          if (Pmm_j)
          {
            Pmj = (x*(2*l-3)*Pmm1_j-(l+m-2)*Pmm_j)/(l-1-m);
            Pmm = Pmm1_j;
            Pmm1 = Pmj;
            Pmj = (x*(2*l-1)*Pmm1-(l+m-1)*Pmm)/(l-m);
            Pmm_j = Pmm1;
            Pmm1_j = Pmj;
          }
          else if (l==m)
          {
            Pmj = Pmm;
          }
          else if (l==m+1)
          {
            Pmm1=x*(2*m+1)*Pmm;
            Pmj = Pmm1;
          }
          else
          {
            Pmm1=x*(2*m+1)*Pmm;
            for (int j=m+2; j<=l; j++) {
              Pmj = (x*(2*j-1)*Pmm1-(j+m-1)*Pmm)/(j-m);
              Pmm = Pmm1;
              Pmm1 = Pmj;
            }
            Pmm_j = Pmm;
            Pmm1_j = Pmm1;
          }
          spharmfn.push_back(scale*Pmj);
        } // l loop
      } // m loop

      for (int r=0; r < HKL_list.clustered[c].size(); r++)
      {
        int i(0),n_elmn(0);
        floatType pintensity = HKL_list.clustered[c][r].intensity;
        float1D besselx(lmax+2);
        floatType h = lmax*HKL_list.clustered[c][r].htp.r*HIRES; //TWOPI*htp.r*b;
        for (int u = 3; u<lmax+2; u+=2)
          besselx[u] = sqrt_table[u]*sphbessel(u,h)/h;

        cmplxType phi_phase_prev = ONE;
        cmplxType phi_phase_1 = std::exp(cmplxType(ZERO,ONE)*HKL_list.clustered[c][r].htp.phi);
        for (int m = 0; m <= lmax; m++)
        {
          cmplxType phi_phase_m = m ? phi_phase_prev * phi_phase_1 : phi_phase_prev;
          phi_phase_prev = phi_phase_m;
          if ((m/ZSYMM)*ZSYMM !=m) continue;
          for (int l = m; l <= lmax ; l += 2)
          {
            if (l%2 == 1) l++;
            if (l < 2) continue;
            cmplxType Ylm = spharmfn[i++];
                      Ylm *= phi_phase_m;
                      Ylm *= pintensity;
            int nmax = (lmax-l+2)/2;
          //  int l_index = l/2-1;
            for (int n = 1; n <= nmax; n++)
            {
              omp_elmn_first[nthread*num_elmn+n_elmn] += besselx[l+2*n-1]*Ylm; //note way of storing this!
              n_elmn++;
            }
          } // l loop
        } // m loop
      } // r loop for clustered seta
      if (nthread == 0) output.logProgressBarNext(LOGFILE);
    } // c loop and #pragma omp for
  }  // #pragma omp parallel
// now we're unparallelised again
  output.logProgressBarEnd(LOGFILE);
  output.logBlank(VERBOSE);

// sum up values of omp extra variable
  for (int nthread=0; nthread<nthreads; nthread++)
  {
    int n_elmn(0);
    for (int m = 0; m <= lmax; m++)
    {
      if ((m/ZSYMM)*ZSYMM !=m) continue;
      for (int l = m; l <= lmax ; l += 2)
      {
        if (l%2 == 1) l++;
        if (l < 2) continue;
        int nmax = (lmax-l+2)/2;
        int l_index = l/2-1;
        for (int n = 1; n <= nmax; n++)
        {
          elmn[l_index][l+m][n] += omp_elmn_first[nthread*num_elmn+n_elmn];
          n_elmn++;
        } // n loop
      } // l loop
    } // m loop
  }

  output.logElapsedTime(DEBUG);
  output.logBlank(DEBUG);
  for (unsigned l_index = 0; l_index < elmn.size(); l_index++)
  {
    unsigned l=2*l_index+2;
    for (int m_index = 0; m_index <= l; m_index++)
      for (unsigned n_index = 1; n_index < elmn[l_index][m_index].size(); n_index++)
      {
        floatType oddFac = (m_index & 1) ? -ONE : ONE; //bitwise AND operation - looks at last bit, very fast
        elmn[l_index][l-m_index][n_index]= oddFac*std::conj(elmn[l_index][l+m_index][n_index]);
      }
  }
  return elmn;
}

af_cmplx DataMR::getFcalc(dmat33 ROT,std::string modlid,af::shared<miller::index<int> > P1,bool halfR,data_bofac SEARCH_FACTORS,double& sum_Fm_sqr)
{
  Ensemble* MAP = &ensemble->find(modlid)->second;
  int P1_size = P1.size();
  af_cmplx Fcalc(P1_size);

  dmat33 R = ROT*MAP->Frac2Orth();
  dmat33 Q1 = doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();

  int rP1(0);
  sum_Fm_sqr = 0;
  const cmplxType cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    std::vector<miller::index<int> > rhkl = rotMiller(r);
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    MAP->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType terms = std::sqrt(MAP->AtomScatRatio()*MAP->SCATTERING/TOTAL_SCAT/NSYMP);
    if (!SEARCH_FACTORS.defaults())
    {
      terms *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
      terms *= std::sqrt(SEARCH_FACTORS.OFAC);
    }
    if (PTNCS.use_and_present())
    {
      gfun.calcReflTerms(r);
      terms *= std::sqrt(G_DRMS[r]);
    }
    for (unsigned isym = 0; isym < NSYMP; isym++)
    {
      if (!duplicate(isym,rhkl)) // Generate only unique set of P1 structure factors
      {
        PHASER_ASSERT(rP1 < P1_size); //paranoia
        dvect3 RotSymHKL = Q1tr*rhkl[isym];
        cmplxType thisE = MAP->InterpE(RotSymHKL,DFAC[r]);
                  thisE *= terms;
        if (PTNCS.use_and_present())
        {
          floatType theta  = rhkl[isym][0]*PTNCS.TRA.VECTOR[0] +
                             rhkl[isym][1]*PTNCS.TRA.VECTOR[1] +
                             rhkl[isym][2]*PTNCS.TRA.VECTOR[2];
          if (PTNCS.NMOL > 2)
          {
            cmplxType ptncs_scat = cmplxONE; //iMOL=0
            for (floatType iMOL = 1; iMOL < PTNCS.NMOL; iMOL++)
              ptncs_scat += std::exp(TWOPII*iMOL*theta);
            thisE *= ptncs_scat;
          }
          else if (halfR) //second molecule comes from doubling at the one rotation
          {
            thisE *= cmplxONE + std::exp(TWOPII*theta);
            thisE *= gfun.refl_G[isym];
          }
        }
        sum_Fm_sqr += std::norm(thisE);
        P1[rP1] = rhkl[isym]; //in place change
        Fcalc[rP1] = thisE;
        rP1++;
      }
    }
  }
  PHASER_ASSERT(P1_size == rP1); //paranoia
  return Fcalc;
}

af_cmplx DataMR::getFcalcPhsTF(dmat33 ROT,std::string modlid,data_bofac SEARCH_FACTORS)
{
  Ensemble* MAP = &ensemble->find(modlid)->second;
  af_cmplx Fcalc;
  dmat33 R = ROT*MAP->Frac2Orth();
  dmat33 Q1 = doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType rssqr = ssqr(r);
    floatType sqrt_repsn = sqrt_epsn[r];
    floatType sqrtrssqr = std::sqrt(rssqr);
    MAP->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType terms = std::sqrt(MAP->AtomScatRatio()*MAP->SCATTERING/TOTAL_SCAT/NSYMP);
    terms *= sqrt_repsn;
    if (!SEARCH_FACTORS.defaults())
    {
      terms *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
      terms *= std::sqrt(SEARCH_FACTORS.OFAC);
    }
    std::vector<miller::index<int> >  rhkl = rotMiller(r);
    for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
    {
      if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
      {
        cmplxType thisE = MAP->InterpE(Q1tr*rhkl[isym],DFAC[r]);
                  thisE *= terms;
        Fcalc.push_back(thisE);
      }
    }
  }
  return Fcalc;
}

af_cmplx DataMR::getFobsPhsTF()
{
  af_cmplx Fobs;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    Fobs.push_back(FTFMAP.FOM[r]*std::polar(FTFMAP.FMAP[r]/SIGMAN.sqrt_epsnSN[r],scitbx::deg_as_rad(FTFMAP.PHMAP[r])));
  return Fobs;
}

std::vector<mr_ndim> DataMR::getModls()
{
  return models_known;
}

af_cmplx DataMR::getEpart()
{
  af_cmplx Epart;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    Epart.push_back(EM_known[r]);
  return Epart;
}

bool DataMR::haveFpart()
{
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
    if (EM_known[r] != cmplxType(0,0))
      return true;
  return false;
}

af_float DataMR::getEnat()
{
  af_float Enat;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
    Enat.push_back(E);
  }
  return Enat;
}

floatType DataMR::m_LETF1(dmat33 ROT,std::string modlid,bool use_rot,af_float& mI,bool halfR,data_bofac SEARCH_FACTORS)
{
  Ensemble* ens_modlid = &ensemble->find(modlid)->second;
  mI.clear();
  dmat33 R = ROT*ens_modlid->Frac2Orth();
  dmat33 Q1 = UnitCell::doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();
  floatType C1(0.);
  int countr(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    bool rcent = cent(r);
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType scatFactor = ens_modlid->AtomScatRatio()*ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP;
    floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
    floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
              thisV *= scatFactor*NSYMP;
    if (!SEARCH_FACTORS.defaults())
    {
      thisV *= std::exp(-2.0*SEARCH_FACTORS.BFAC*ssqr(r)/4.0);
      thisV *= SEARCH_FACTORS.OFAC;
    }
    if (PTNCS.use_and_present())
    {
      if (PTNCS.NMOL > 2 || !halfR)
        thisV *= PTNCS.NMOL*G_Vterm[r];
      else
      {
        gfun.calcReflTerms(r);
        thisV *= PTNCS.NMOL*gfun.refl_Gsqr_Vterm;
      }
      thisV *= G_DRMS[r];
    }
    totvar_search[r] = thisV;
    floatType phasedEsqr(EM_known[r].real()*EM_known[r].real()+EM_known[r].imag()*EM_known[r].imag());
    floatType eImove(0);
    const cmplxType cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
    if (use_rot)
    {
      floatType sum_Esqr_search(0);
      floatType repsn = epsn(r);
      std::vector<miller::index<int> > rhkl = rotMiller(r);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3 RotSymHKL = Q1tr*rhkl[isym];
          cmplxType thisE = ens_modlid->InterpE(RotSymHKL,DFAC[r]);
          if (!SEARCH_FACTORS.defaults())
          {
            thisE *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
            thisE *= std::sqrt(SEARCH_FACTORS.OFAC);
          }
          if (PTNCS.use_and_present())
          {
            floatType theta  = rhkl[isym][0]*PTNCS.TRA.VECTOR[0] +
                               rhkl[isym][1]*PTNCS.TRA.VECTOR[1] +
                               rhkl[isym][2]*PTNCS.TRA.VECTOR[2];
            cmplxType ptncs_scat = cmplxONE;
            for (floatType iMOL = 1; iMOL < PTNCS.NMOL; iMOL++)
            {
              ptncs_scat += std::exp(TWOPII*iMOL*theta);
            }
            if (halfR && PTNCS.NMOL == 2)
              ptncs_scat *= gfun.refl_G[isym];
            thisE *= ptncs_scat;
          }
          floatType thisEsqr = thisE.real()*thisE.real() + thisE.imag()*thisE.imag();
                    thisEsqr *= repsn;
                    thisEsqr *= scatFactor;
          if (PTNCS.use_and_present())
                    thisEsqr *= G_DRMS[r];
          sum_Esqr_search += thisEsqr;
        }
      }
      eImove = phasedEsqr + sum_Esqr_search; // <Imove> knowing molecular transforms
    }
    else
    {
      eImove = phasedEsqr + totvar_search[r]; // <Imove> from scattering content
    }
    PHASER_ASSERT(eImove >= 0);
    floatType C(PTNCS.EPSFAC[r] - totvar_known[r] - totvar_search[r]);
    floatType sqrt_eImove = std::sqrt(eImove);
    PHASER_ASSERT(C > 0);
    floatType X = 2.*E*sqrt_eImove/C;
    floatType weight = rcent ? 2.0 : 1.0;
    X = X/weight;
    floatType rh = rcent ? std::tanh(X) : sim(X);
    floatType B1;
    if (sqrt_eImove > 0)
      B1 = (rh*E/sqrt_eImove - 1)/(weight*C);
    else
      B1 = (E*E/C - 1)/(weight*C); // Limit as sqrt_eImove -> 0
    mI.push_back(B1);
    //Compute LL for eImove, leaving out constants that are left out of Rice()
    floatType eLL = rcent ? logRelWoolfson(E,sqrt_eImove,C) : logRelRice(E,sqrt_eImove,C);
    C1 += eLL - B1*eImove;
    countr++;
  }
  return C1;
}

floatType DataMR::m_LETF2(dmat33 ROT,std::string modlid,bool use_rot,af_float& mI,af_float& mIsqr,bool halfR,data_bofac SEARCH_FACTORS)
{
  Ensemble* ens_modlid = &ensemble->find(modlid)->second;
  mI.clear();
  mIsqr.clear();
  dmat33 R = ROT*ens_modlid->Frac2Orth();
  dmat33 Q1 = UnitCell::doOrth2Frac(R);
  dmat33 Q1tr = Q1.transpose();
  floatType C2(0.);
  floatType scatFactor = ens_modlid->SCATTERING/TOTAL_SCAT/NSYMP;
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    bool rcent = cent(r);
    floatType rssqr = ssqr(r);
    floatType sqrtrssqr = std::sqrt(rssqr);
    ens_modlid->set_sqrt_ssqr(sqrtrssqr,rssqr);
    floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r];
    floatType thisV = ens_modlid->InterpV(fn::pow2(DFAC[r]));
              thisV *= scatFactor*NSYMP*ens_modlid->AtomScatRatio();
    if (!SEARCH_FACTORS.defaults())
    {
      thisV *= std::exp(-2.0*SEARCH_FACTORS.BFAC*ssqr(r)/4.0);
      thisV *= SEARCH_FACTORS.OFAC;
    }
    if (PTNCS.use_and_present())
    {
      if (PTNCS.NMOL > 2 || !halfR)
        thisV *= PTNCS.NMOL*G_Vterm[r];
      else
      {
        gfun.calcReflTerms(r);
        thisV *= PTNCS.NMOL*gfun.refl_Gsqr_Vterm;
      }
      thisV *= G_DRMS[r];
    }
    totvar_search[r] = thisV;
    floatType phasedEsqr(EM_known[r].real()*EM_known[r].real()+EM_known[r].imag()*EM_known[r].imag());
    floatType I(E*E);
    floatType eImove(0);
    const cmplxType cmplxONE(1.,0.),TWOPII(0.,scitbx::constants::two_pi);
    if (use_rot)
    {
      floatType sum_Esqr_search(0);
      floatType repsn = epsn(r);
      floatType rssqr = ssqr(r);
      std::vector<miller::index<int> > rhkl =   rotMiller(r);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        if (!duplicate(isym,rhkl)) // Compensate for skipping symops with epsilon below
        {
          dvect3    RotSymHKL = Q1tr*rhkl[isym];
          cmplxType thisE = ens_modlid->InterpE(RotSymHKL,DFAC[r]);
          if (!SEARCH_FACTORS.defaults())
          {
            thisE *= std::exp(-SEARCH_FACTORS.BFAC*rssqr/4.0);
            thisE *= std::sqrt(SEARCH_FACTORS.OFAC);
          }
          if (PTNCS.use_and_present())
          {
            floatType theta  = rhkl[isym][0]*PTNCS.TRA.VECTOR[0] +
                               rhkl[isym][1]*PTNCS.TRA.VECTOR[1] +
                               rhkl[isym][2]*PTNCS.TRA.VECTOR[2];
            cmplxType ptncs_scat = cmplxONE;
            for (floatType iMOL = 1; iMOL < PTNCS.NMOL; iMOL++)
              ptncs_scat += std::exp(TWOPII*iMOL*theta);
            if (halfR && PTNCS.NMOL == 2)
              ptncs_scat *= gfun.refl_G[isym];
            thisE *= ptncs_scat;
          }
          floatType thisEsqr = thisE.real()*thisE.real() + thisE.imag()*thisE.imag();
                    thisEsqr *= repsn;
                    thisEsqr *= scatFactor;
          if (PTNCS.use_and_present())
                    thisEsqr *= G_DRMS[r];
          sum_Esqr_search += thisEsqr;
        }
      }
      eImove = phasedEsqr + sum_Esqr_search;
    }
    else
    {
      eImove = phasedEsqr + totvar_search[r];
    }
    floatType C(PTNCS.EPSFAC[r] - totvar_known[r] - totvar_search[r]);
    floatType sqrt_eImove = std::sqrt(eImove);
    floatType X = 2.*E*sqrt_eImove/C;
    floatType weight = rcent ? 2.0 : 1.0;
    X = X/weight;
    floatType rh = rcent ? std::tanh(X) : sim(X);
    floatType LLp((rh*E/sqrt_eImove - 1)/(weight*C));
    floatType weightSqr = rcent ? 4.0 : 1.0;
    floatType LLdp((1/C/eImove/weightSqr)*((1-rh*rh)*I/(C) - rh*E/sqrt_eImove));
    mI.push_back(LLp-LLdp*eImove), mIsqr.push_back(LLdp/2.0);
    //Compute LL for eImove, leaving out constants that are left out of Rice()
    floatType eLL = rcent ? logRelWoolfson(E,sqrt_eImove,C) : logRelRice(E,sqrt_eImove,C);
    C2 += eLL - LLp*eImove + LLdp*eImove*eImove/2.;
  }
  return C2;
}

MapCoefs DataMR::getMapCoefs()
{
  MapCoefs MAPCOEFS;
  MAPCOEFS.resize(NREFL); //first call only
  std::pair<floatType,floatType> scale = ScaleFC();
  floatType scaleK(scale.first);
  floatType scaleB(scale.second);
  double WilsonK_I(1./fn::pow2(SIGMAN.WILSON_K));
  double resharpB(getResharpB());

  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r]) //case for not selected below
  {
    bool rcent = cent(r);
    floatType E = Feff[r]/SIGMAN.sqrt_epsnSN[r]; // Remove anisotropy but not tNCS
    // Map coeffs only calculated when relative phase known for all components.
    // However, can be classified as fixed or moving, depending on context.
    floatType sigmaA_Ecalc(std::abs(EM_known[r]+EM_search[r]));
    floatType phase = scitbx::rad_as_deg(std::arg(EM_known[r]+EM_search[r]));
    floatType V = PTNCS.EPSFAC[r] - totvar_known[r] - totvar_search[r];
    PHASER_ASSERT(V > 0);
    floatType X(2*E*sigmaA_Ecalc/V);
    floatType fom = rcent ? std::tanh(X/2.) : sim(X);
    floatType fwt = rcent ? fom*E : 2*fom*E-sigmaA_Ecalc;
    // Get scale to put calculated E-values on absolute scale using isotropic component of SigmaN
    unsigned s(rbin(r));
    double sqrt_epsnSN_iso = std::sqrt(epsn(r) *
           binIsoFactor(ssqr(r),SIGMAN.BINS[s],SIGMAN.WILSON_B,SIGMAN.SOLK,SIGMAN.SOLB,WilsonK_I));
    // Include resharpening factor
    double mapScaleFac(sqrt_epsnSN_iso*std::exp(-resharpB*ssqr(r)/4.));
    fwt *= mapScaleFac;
    if (fwt >= 0)
    {
      MAPCOEFS.FWT[r] = fwt;
      MAPCOEFS.PHWT[r] = phase;
    }
    else
    {
      MAPCOEFS.FWT[r] = -fwt;
      MAPCOEFS.PHWT[r] = phase+180;
    }
    floatType delfwt(mapScaleFac*(fom*E-sigmaA_Ecalc));
    if (delfwt >= 0)
    {
      MAPCOEFS.DELFWT[r] = delfwt;
      MAPCOEFS.PHDELWT[r] = phase;
    }
    else
    {
      MAPCOEFS.DELFWT[r] = -delfwt;
      MAPCOEFS.PHDELWT[r] = phase+180;
    }
    MAPCOEFS.FOM[r] = fom;
    floatType FC(sigmaA_Ecalc*scaleK*std::exp(-scaleB*ssqr(r)/4)*sqrt_epsnSN_iso);
    MAPCOEFS.FC[r] = FC;
    MAPCOEFS.PHIC[r] = phase;
    floatType radphase(scitbx::deg_as_rad(phase));
    MAPCOEFS.HLA[r] = X*cos(radphase);
    MAPCOEFS.HLB[r] = X*sin(radphase);
    if (rcent)
    {
      MAPCOEFS.HLA[r] /= 2.;
      MAPCOEFS.HLB[r] /= 2.;
    }
    MAPCOEFS.HLC[r] = 0;
    MAPCOEFS.HLD[r] = 0;
  }
  else //not just selected, as list must be in sync with the miller list for output
  {
    MAPCOEFS.FWT[r] = MAPCOEFS.PHWT[r] = CCP4::ccp4_nan().f;
    MAPCOEFS.DELFWT[r] = MAPCOEFS.PHDELWT[r] = CCP4::ccp4_nan().f;
    MAPCOEFS.FC[r] = MAPCOEFS.PHIC[r] = MAPCOEFS.FOM[r] = CCP4::ccp4_nan().f;
    MAPCOEFS.HLA[r] = MAPCOEFS.HLB[r] = MAPCOEFS.HLC[r] = MAPCOEFS.HLD[r] = CCP4::ccp4_nan().f;
  }
  return MAPCOEFS;
}

floatType DataMR::logRelRice(floatType F1, floatType DF2, floatType V)
{
/* Calculate log(Rice), omitting the constant terms omitted from WilsonRice.
   Suitable for LLG calculations, where only relative values needed.
   Assuming F1 is constant observed F, this form works for both
   amplitude and intensity-based likelihoods.
*/
  PHASER_ASSERT(F1 >= 0. && DF2 >= 0. && V > 0.);
  floatType X(2*F1*DF2/V);
  return m_alogchI0.getalogI0(X)-(std::log(V)+(fn::pow2(F1)+fn::pow2(DF2))/V);
}

floatType DataMR::logRelWoolfson(floatType F1, floatType DF2, floatType V)
{
// Centric equivalent to logRelRice
  PHASER_ASSERT(F1 >= 0. && DF2 >= 0. && V > 0.);
  floatType X(F1*DF2/V),TWO(2);
  return m_alogch.getalogch(X)-(std::log(V)+(fn::pow2(F1)+fn::pow2(DF2))/V)/TWO;
}


//xxxx floatType DataMR::lowerB() { return -SIGMAN.WILSON_B + tolB(); }
floatType DataMR::lowerB() { return -getIsoB() + tolB(); }
floatType DataMR::upperB() { return DEF_MR_BMAX; }
floatType DataMR::tolB()   { return DEF_MR_BTOL; }

Hexagonal DataMR::calc_search_points(data_btf& BTF,floatType SAMPLING,bool isFirst,Output& output)
{
  Hexagonal search_points(*this,*this);
  if (BTF.VOLUME.full())
  {
    if (isFirst) output.logTab(1,LOGFILE,"Full Search for first ensemble");
    else output.logTab(1,LOGFILE,"Full Search for second and subsequent ensemble");
    search_points.setupAsuRegion(SAMPLING,isFirst);
    output.logTab(2,VERBOSE,"Orthogonal x, y and z ranges of search:");
    output.logTab(3,VERBOSE,"Min: " + dvtos(search_points.getOrthMin(),5,2));
    output.logTab(3,VERBOSE,"Max: " + dvtos(search_points.getOrthMax(),5,2));
  }
  else if (BTF.VOLUME.region())
  {
    Brick brick(BTF.FRAC,BTF.START,BTF.END,*this);
    search_points.setupBrickRegion(brick,SAMPLING);
    output.logTab(2,LOGFILE,"Search in Region");
    output.logTab(2,VERBOSE,"Orthogonal x, y and z ranges of search:");
    output.logTab(3,VERBOSE,"Min: " + dvtos(search_points.getOrthMin(),5,2));
    output.logTab(3,VERBOSE,"Max: " + dvtos(search_points.getOrthMax(),5,2));
  }
  else if (BTF.VOLUME.around())
  {
    dvect3 fracPoint = BTF.FRAC ? BTF.START : doOrth2Frac(BTF.START);
    dvect3 orthPoint = BTF.FRAC ? doFrac2Orth(BTF.START) : BTF.START;
    search_points.setupAroundPoint(orthPoint,SAMPLING,BTF.RANGE);
    output.logTab(2,LOGFILE,"Search around Point");
    output.logTab(3,LOGFILE,"Fractional Coordinates: " + dvtos(fracPoint,5,2));
    output.logTab(3,LOGFILE,"Orthogonal Coordinates: " + dvtos(orthPoint));
    output.logTab(3,LOGFILE,"Coordinate Range:       " + dtos(BTF.RANGE) + "A");
  }
  else if (BTF.VOLUME.line())
  {
    output.logTab(1,LOGFILE,"Search along Line");
    dvect3 tran_start = BTF.FRAC ? doFrac2Orth(BTF.START) : BTF.START;
    dvect3 tran_end   = BTF.FRAC ? doFrac2Orth(BTF.END) : BTF.END;
    search_points.setupAlongLine(tran_start,tran_end,SAMPLING);
    output.logTab(2,VERBOSE,"Orthogonal x, y and z ranges of search:");
    output.logTab(3,VERBOSE,"Min: " + dvtos(tran_start,5,2));
    output.logTab(3,VERBOSE,"Max: " + dvtos(tran_end,5,2));
  }
  return search_points;
}

} //phaser
