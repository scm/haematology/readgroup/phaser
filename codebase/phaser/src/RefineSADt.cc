//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <float.h>
#include <phaser/include/ProtocolSAD.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/aniso.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/Integration.h>
#include <phaser/src/PhsStats.h>
#include <phaser/src/Integration.h>
#include <phaser/src/Cluster.h>
#include <phaser/lib/rad2phi.h>
#include <scitbx/constants.h>
#include <scitbx/math/erf.h>
#include <phaser/src/Minimizer.h>
#include <cctbx/xray/structure_factors_direct.h>

// Target related code

namespace phaser {

floatType RefineSAD::targetFn()
{
  double REFLLG = -wilson_llg;
  for (unsigned r = 0; r < NREFL; r++)
    if (selected[r])
    {
      if (both[r])
        REFLLG += acentricReflIntgSAD2(r).LogLike;
      else if (!sad_target_anom_only && cent[r])
        REFLLG += centricReflIntgSAD2(r).LogLike;
      else if (!sad_target_anom_only)
        REFLLG += singletonReflIntgSAD2(r,plus[r]).LogLike;
    }
  atoms.LLG = REFLLG; //store value without restraints
  if (SPHERICITY.RESTRAINT)
    REFLLG += sphericity_restraint_likelihood();
  if (WILSON.RESTRAINT)
    REFLLG += wilson_restraint_likelihood();
  if (FDP.RESTRAINT)
    REFLLG += fdp_restraint_likelihood();
  if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
    REFLLG += ptncs_link_restraint_likelihood();
  return REFLLG;
}

floatType RefineSAD::Rfactor()
{
  long double totalR_numerator(0),totalR_denominator(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    totalR_numerator += std::fabs(POS.F[r]-std::abs(FHpos[r]));
    totalR_denominator += POS.F[r];
  } // r loop
  atoms.RFAC = (totalR_denominator > 0.) ? 100*totalR_numerator/totalR_denominator : -1.0 ;
  return atoms.RFAC;
}

floatType RefineSAD::sphericity_restraint_likelihood()
{
//Convert isotropic equivalent as in largeShifts
  floatType sphericity_likelihood(0);
  dmat6 sigmaSphericityBeta;
  sigmaSphericityBeta[0] = 0.25*SPHERICITY.SIGMA*aStar()*aStar();
  sigmaSphericityBeta[1] = 0.25*SPHERICITY.SIGMA*bStar()*bStar();
  sigmaSphericityBeta[2] = 0.25*SPHERICITY.SIGMA*cStar()*cStar();
  sigmaSphericityBeta[3] = 0.25*SPHERICITY.SIGMA*aStar()*bStar();
  sigmaSphericityBeta[4] = 0.25*SPHERICITY.SIGMA*aStar()*cStar();
  sigmaSphericityBeta[5] = 0.25*SPHERICITY.SIGMA*bStar()*cStar();
  dmat6 u_star_sphericity = cctbx::adptbx::beta_as_u_star(sigmaSphericityBeta);
  for (int a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED && atoms[a].SCAT.flags.use_u_aniso_only())
  {
    cctbx::adptbx::factor_u_star_u_iso<floatType> factor(getCctbxUC(),atoms[a].SCAT.u_star);
    for (unsigned n = 0; n < 6; n++)
      sphericity_likelihood += fn::pow2(factor.u_star_minus_u_iso[n]/u_star_sphericity[n])/2.;
  }
  return sphericity_likelihood;
}

floatType RefineSAD::wilson_restraint_likelihood()
{
  floatType wilson_likelihood(0);
  floatType sigmaWilsonU = cctbx::adptbx::b_as_u(WILSON.SIGMA);
  floatType WilsonU = cctbx::adptbx::b_as_u(WilsonB);
  for (int a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    floatType u_iso = atoms[a].SCAT.flags.use_u_aniso_only() ? cctbx::adptbx::u_star_as_u_iso(getCctbxUC(),atoms[a].SCAT.u_star) : atoms[a].SCAT.u_iso;
    wilson_likelihood += fn::pow2((u_iso-WilsonU)/sigmaWilsonU)/2.;
  }
  return wilson_likelihood;
}

floatType RefineSAD::fdp_restraint_likelihood()
{
  floatType fdp_likelihood(0);
  for (int t = 0; t < AtomFdp.size(); t++)
  {
    floatType AtomSigmaFdp = FDP.SIGMA*AtomFdpInit[t];
    if (AtomSigmaFdp) fdp_likelihood += fn::pow2((AtomFdp[t]-AtomFdpInit[t])/AtomSigmaFdp)/2.;
  }
  return fdp_likelihood;
}

floatType RefineSAD::ptncs_link_restraint_likelihood()
{
  floatType ptncs_link_likelihood(0);
  for (std::map<int,int>::iterator iter = ptncs_pairs.begin(); iter != ptncs_pairs.end(); iter++)
  {
    int a = iter->first; //alias
    int b = iter->second; //alias
    floatType average = atoms[a].SCAT.occupancy/2 + atoms[b].SCAT.occupancy/2;
    ptncs_link_likelihood += fn::pow2((atoms[a].SCAT.occupancy-average)/PTNCS.EP.LINK_SIGMA)/2.;
    ptncs_link_likelihood += fn::pow2((atoms[b].SCAT.occupancy-average)/PTNCS.EP.LINK_SIGMA)/2.;
    if (atoms[a].SCAT.flags.use_u_aniso_only())
    {
      for (int n = 0; n < atoms[a].n_adp; n++)
      {
        average = atoms[a].SCAT.u_star[n]/2 + atoms[b].SCAT.u_star[n]/2;
        ptncs_link_likelihood += fn::pow2((atoms[a].SCAT.u_star[n]-average)/PTNCS.EP.LINK_SIGMA/10000)/2.;
        ptncs_link_likelihood += fn::pow2((atoms[b].SCAT.u_star[n]-average)/PTNCS.EP.LINK_SIGMA/10000)/2.;
      }
    }
    else
    {
      average = atoms[a].SCAT.u_iso/2 + atoms[b].SCAT.u_iso/2;
      ptncs_link_likelihood += fn::pow2((atoms[a].SCAT.u_iso-average)/PTNCS.EP.LINK_SIGMA)/2.;
      ptncs_link_likelihood += fn::pow2((atoms[b].SCAT.u_iso-average)/PTNCS.EP.LINK_SIGMA)/2.;
    }
  }
  return ptncs_link_likelihood;
}

void RefineSAD::calcAtomicData()
{
  //use fastest way to do sf calculation
  use_fft ? calcAtomicDataFFT() : calcAtomicDataSum();

//#define AJM_DEBUG_SUM_FFT_SFCALC
#ifdef AJM_DEBUG_SUM_FFT_SFCALC
std::cout << "number of atoms =  " + itos(atoms.size()) << std::endl;
std::cout << "calculating by fft..." << std::endl;
  prepareAtomicFFT();
  calcAtomicDataFFT();
std::cout << "copy sf..." << std::endl;
  af_cmplx FApos_fft = FApos.deep_copy();
  af_cmplx FAneg_fft = FAneg.deep_copy();
std::cout << "calculating by sum..." << std::endl;
  calcAtomicDataSum();
std::cout << "comparison..." << std::endl;
  int r(0),ar(0);
  do {
    floatType tol(1.0e-06);
    if (epsn[r] > 1+tol || epsn[r] < 1-tol)  //!= 1
    {
      std::cout << "r= " << r << " miller=" << ivtos(miller[r]) << " epsn=" << epsn[r] << " cent=" << cent[r] << std::endl;
      std::cout << "\tpos  fft " << FApos_fft[r] << " sum " << FApos[r] << std::endl;
      std::cout << "\tneg  fft " << FAneg_fft[r] << " sum " << FAneg[r] << std::endl;
      ar++;
    }
    r++;
  }
  while (ar < 5 && r < NREFL);
std::exit(1);
#endif
}

void RefineSAD::calcAtomicDataSum()
{
// Calculate structure factors for heavy atoms.
// Structure factors are calculated by direct summation.

  //initialize
  if (atoms.size()) for (unsigned r = 0; r < NREFL; r++) FApos[r] = FAneg[r] = 0;

  float1D linearTerm(atoms.size()),debye_waller_u_iso(atoms.size(),0.);
  for (unsigned a = 0; a < atoms.size(); a++)
  if (!atoms[a].REJECTED)
  {
    floatType symFacPCIF(SpaceGroup::NSYMM/SpaceGroup::NSYMP);
    int M = SpaceGroup::NSYMM/site_sym_table.get(a).multiplicity();
    linearTerm[a] = symFacPCIF*ScaleK*atoms[a].SCAT.occupancy/M;
    if (!atoms[a].SCAT.flags.use_u_aniso_only()) debye_waller_u_iso[a] = cctbx::adptbx::u_as_b(atoms[a].SCAT.u_iso) * 0.25;
  }

  //sf calculation
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    for (unsigned a = 0; a < atoms.size(); a++)
    if (!atoms[a].REJECTED)
    {
      int t = atomtype2t[atoms[a].SCAT.scattering_type];
      floatType isoB = atoms[a].SCAT.flags.use_u_aniso_only() ? 1 : std::exp(-ssqr[r]*debye_waller_u_iso[a]);
      floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
      if (debye_ScaleB) isoB *= std::exp(-ssqr[r]*debye_ScaleB);
      floatType symmOccBfac(linearTerm[a]*isoB);
      floatType scat(fo_plus_fp[r][t]*symmOccBfac);
                symmOccBfac*=CLUSTER[t].debye(ssqr[r]);
      floatType fpp(AtomFdp[t]*symmOccBfac);
      for (unsigned isym = 0; isym < SpaceGroup::NSYMP; isym++)
      {
        floatType anisoB(1);
        floatType anisoScat(scat);
        floatType anisoFpp(fpp);
        if (atoms[a].SCAT.flags.use_u_aniso_only())
        {
          anisoB = cctbx::adptbx::debye_waller_factor_u_star(rotMiller[r][isym],atoms[a].SCAT.u_star);
          anisoScat = scat*anisoB;
          anisoFpp = fpp*anisoB;
        }
        floatType theta = rotMiller[r][isym][0]*atoms[a].SCAT.site[0] +
                          rotMiller[r][isym][1]*atoms[a].SCAT.site[1] +
                          rotMiller[r][isym][2]*atoms[a].SCAT.site[2] +
                          traMiller[r][isym];
#if defined CCTBX_MATH_COS_SIN_TABLE_H
        cmplxType sincostheta = fast_cos_sin.get(theta); //multiplies by 2*pi internally
        floatType costheta = std::real(sincostheta);
        floatType sintheta = std::imag(sincostheta);
#else
        theta *= scitbx::constants::two_pi;
        floatType costheta = std::cos(theta);
        floatType sintheta = std::sin(theta);
#endif
        floatType anisoScat_costheta(anisoScat*costheta);
        floatType anisoScat_sintheta(anisoScat*sintheta);
        floatType anisoFpp_costheta(anisoFpp*costheta);
        floatType anisoFpp_sintheta(anisoFpp*sintheta);
        FApos[r] += cmplxType(anisoScat_costheta - anisoFpp_sintheta, anisoScat_sintheta + anisoFpp_costheta);
        //store the complex conjugate of FH-
        FAneg[r] += cmplxType(anisoScat_costheta + anisoFpp_sintheta, anisoScat_sintheta - anisoFpp_costheta);
//         |
//         |* FHpos
//         |  /  * FHneg stored
//         | /
//         |/ FH
//       --o--------
//         |
//
      }//end sym
    } //end  loop over atoms
  } //end loop r
} // end calcAtomicData

//for python
void RefineSAD::setFC(af_cmplx FCpos,af_cmplx FCneg)
{
  PHASER_ASSERT(FCpos.size() == NREFL);
  PHASER_ASSERT(FCneg.size() == NREFL);
  FApos = FCpos;
  //store the complex conjugate of FH-
  FAneg = FCneg;
  input_atoms = true;
  calcScalesData();
//         |
//         |* FHpos
//         |  /  * FHneg stored
//         | /
//         |/ FH
//       --o--------
//         |
//
  //don't want to calcOutliers, calcVariances, or calcSigmaaData here
  //because the values will change each target function evaluation
} // end setFC

void RefineSAD::calcSigmaaData()
{
  //setup parameters to pass to likelihood function
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    unsigned s = rbin[r];
    DphiA[r] = DphiA_bin[s];
    DphiB[r] = DphiB_bin[s];
    sigPlus[r] = epsn[r]*SP_bin[s];
    sigDsqr[r] = epsn[r]*SDsqr_bin[s];
  }
}

void RefineSAD::calcBinStats()
{
  acentStats.reset(bin);
  centStats.reset(bin);
  singleStats.reset(bin);
  allStats.reset(bin);
  double REFLLG = -wilson_llg;

  int num_phs_reversals(1); //1 means don't, 2 means do, perform both integrations
  for (unsigned r = 0; r < NREFL; r++)
  if (selected[r])
  {
    if (both[r])
    {
      for (unsigned reverse = 0; reverse < num_phs_reversals; reverse++)
      {
        acentric rIntg = acentricReflIntgSAD2(r);
        REFLLG += reverse ? 0 : rIntg.LogLike;
        floatType rFOM = rIntg.FOM();
        acentStats.addFOM(rbin[r],rFOM,num_phs_reversals);
        allStats.addFOM(rbin[r],rFOM,num_phs_reversals);
        if (num_phs_reversals == 2) //switch around pos and neg ready for next loop
        {
          //switch around pos and neg ready for next loop
          { floatType tmp = POS.F[r]; POS.F[r] = NEG.F[r]; NEG.F[r] = tmp; }
          { floatType tmp = POS.SIGF[r]; POS.SIGF[r] = NEG.SIGF[r]; NEG.SIGF[r] = tmp; }
          { cmplxType tmp = FHpos[r]; FHpos[r] = FHneg[r]; FHneg[r] = tmp; }
          { DphiB[r] *= -1.0; }
        }
      }
    }
    else if (cent[r])
    {
      centric rIntg = centricReflIntgSAD2(r);
      REFLLG += rIntg.LogLike;
      floatType rFOM = rIntg.FOM();
      centStats.addFOM(rbin[r],rFOM);
      allStats.addFOM(rbin[r],rFOM);
    }
    else
    {
      singleton rIntg = singletonReflIntgSAD2(r,plus[r]);
      REFLLG += rIntg.LogLike;
      floatType rFOM =  rIntg.FOM();
      singleStats.addFOM(rbin[r],rFOM);
      allStats.addFOM(rbin[r],rFOM);
    }
  }
  atoms.LLG = REFLLG;
  if (SPHERICITY.RESTRAINT)
    REFLLG += sphericity_restraint_likelihood();
  if (WILSON.RESTRAINT)
    REFLLG += wilson_restraint_likelihood();
  if (FDP.RESTRAINT)
    REFLLG += fdp_restraint_likelihood();
  if (PTNCS.use_and_present() && PTNCS.EP.LINK_RESTRAINT)
    REFLLG += ptncs_link_restraint_likelihood();
}

floatType RefineSAD::calcFOMsubset(af_float subset)
{
  PHASER_ASSERT(subset.size() == NREFL);
  floatType subset_FOM(0);
  floatType Num(0);
  for (unsigned r = 0; r < NREFL; r++)
  if (subset[r]) //includes selected, to avoid outliers
  {
    Num++;
    if (both[r])
    {
      acentric rIntg = acentricReflIntgSAD2(r);
      subset_FOM += rIntg.FOM();
    }
    else if (cent[r])
    {
      centric rIntg = centricReflIntgSAD2(r);
      subset_FOM += rIntg.FOM();
    }
    else
    {
      singleton rIntg = singletonReflIntgSAD2(r,plus[r]);
      subset_FOM += rIntg.FOM();
    }
  }
  if (!Num) return 0;
  subset_FOM /= Num;
  return subset_FOM;
}

void RefineSAD::calcPhsStats(Output& output)
{
  //resize the arrays
  FWT.resize(NREFL);
  PHWT.resize(NREFL);
  PHIB.resize(NREFL);
  FOM.resize(NREFL);
  FPFOM.resize(NREFL);
  HL.resize(NREFL);
  HL_ANOM.resize(NREFL);
  if (output.level(VERBOSE))
  {
    FWTA.resize(NREFL);
    PHWTA.resize(NREFL);
    FOMpos.resize(NREFL);
    FOMneg.resize(NREFL);
    PHIBpos.resize(NREFL);
    PHIBneg.resize(NREFL);
    HLpos.resize(NREFL);
    HLneg.resize(NREFL);
  }

  //local variables
  int num_phs_reversals(2); // perform both integrations
  cmplxType HPII(-cmplxType(0.,1.)*floatType(scitbx::constants::pi)/2.);

  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
  if (cent[r])
  {
    if (selected[r])
    {
      centric rIntg = centricReflIntgSAD2(r);
      FOM[r] = rIntg.FOM();
      FPFOM[r] = rIntg.FOM();
      HL[r] = rIntg.HL();
      HL_ANOM[r] = cctbx::hendrickson_lattman<double>(0,0,0,0); // No phase information without partial contribution
      FWT[r] = FOM[r]*POS.F[r];
      PHWT[r] = rad2phi(rIntg.PHIB());
      PHIB[r] = PHWT[r];
    }
    else
    {
      FOM[r] = FPFOM[r] = FWT[r] = 0.;
      HL[r] = HL_ANOM[r] = cctbx::hendrickson_lattman<double>(0,0,0,0);
      PHIB[r] = PHWT[r] = rad2phi(phsr[r]); //arbitrary legal phase
    }
    if (output.level(VERBOSE))
    {
      FWTA[r] = 0; // No anomalous for centrics
      PHWTA[r] = rad2phi(phsr[r]); // Arbitrary, but legal, phase
      PHIBpos[r] = PHIBneg[r] = PHWT[r];
      FOMpos[r] = FOMneg[r] = FOM[r];
      HLpos[r] = HLneg[r] = HL[r];
    }
  }
  else if (both[r])
  {
    float1D rFOM(2),rPHIB(2);
    std::vector<cctbx::hendrickson_lattman<double> > rHL(2),rHL_ANOM(2);
    if (selected[r])
    {
    floatType wtPart(1.); // Set to one for simple bias correction
    for (unsigned pos_or_neg = 0; pos_or_neg < num_phs_reversals; pos_or_neg++)
    {
      acentric rIntg = acentricReflIntgSAD1(r,false); // Include partial structure in phasing
      acentric rIntg_ANOM = acentricReflIntgSAD1(r,true); // Omit partial structure from phasing
      //wtPart = rIntg.WtPart; // Complicated bias correction
      rFOM[pos_or_neg] = rIntg.FOM();
      rPHIB[pos_or_neg] = rIntg.PHIB();
      rHL[pos_or_neg] = rIntg.HL();
      rHL_ANOM[pos_or_neg] = rIntg_ANOM.HL();

      //switch around pos and neg ready for next loop
      { floatType tmp = POS.F[r]; POS.F[r] = NEG.F[r]; NEG.F[r] = tmp; }
      { floatType tmp = POS.SIGF[r]; POS.SIGF[r] = NEG.SIGF[r]; NEG.SIGF[r] = tmp; }
      { cmplxType tmp = FHpos[r]; FHpos[r] = FHneg[r]; FHneg[r] = tmp; }
      { DphiB[r] *= -1.0; }
    } //loop over pos and neg

    //store the values
    unsigned pos(0),neg(1);
    cmplxType Freal = (std::polar(rFOM[pos]*POS.F[r],rPHIB[pos]) +
                       std::polar(rFOM[neg]*NEG.F[r],rPHIB[neg]))/2.;
    PHIB[r] = rad2phi(std::arg(Freal));
    cmplxType Fimag = HPII*(std::polar(rFOM[pos]*POS.F[r],rPHIB[pos]) -
                            std::polar(rFOM[neg]*NEG.F[r],rPHIB[neg]))/2.;

    //Apply heuristic correction for model bias, in proportion to amount by
    //which partial structure determines the phase.
    cmplxType Fpart((FHpos[r]+FHneg[r])/2.);
    Freal = (2.*Freal - wtPart*Fpart)/(2.-wtPart);
    FWT[r] = std::abs(Freal);
    PHWT[r] = rad2phi(std::arg(Freal));
    floatType FP = (POS.F[r] + NEG.F[r])/2.;
    FPFOM[r] = FWT[r]/FP;
    FOM[r] = (rFOM[pos]+rFOM[neg])/num_phs_reversals;
    HL[r] = (rHL[pos]+rHL[neg])/num_phs_reversals;
    HL_ANOM[r] = (rHL_ANOM[pos]+rHL_ANOM[neg])/num_phs_reversals;
    if (output.level(VERBOSE))
    {
      FWTA[r] = std::abs(Fimag);
      PHWTA[r] = rad2phi(std::arg(Fimag));
      FOMpos[r] = rFOM[pos];
      FOMneg[r] = rFOM[neg];
      PHIBpos[r] = rad2phi(rPHIB[pos]);
      PHIBneg[r] = rad2phi(rPHIB[neg]);
      HLpos[r] = rHL[pos];
      HLneg[r] = rHL[neg];
    }
    }//selected, working, not outlier
    else
    {
      FOM[r] = FPFOM[r] = FWT[r] = PHIB[r] = PHWT[r] = 0.;
      HL[r] = HL_ANOM[r] = cctbx::hendrickson_lattman<double>(0,0,0,0);
      if (output.level(VERBOSE))
      {
        FWTA[r] = PHWTA[r] = 0.;
        FOMpos[r] = FOMneg[r] = PHIBpos[r] = PHIBneg[r] = 0.;
        HLpos[r] = HLneg[r] = cctbx::hendrickson_lattman<double>(0,0,0,0);
      }
    }
  }
  else
  {
    if (selected[r])
    {
      singleton rIntg = singletonReflIntgSAD2(r,plus[r]);
      FOM[r] = rIntg.FOM();
      HL[r] = rIntg.HL();
      HL_ANOM[r] = cctbx::hendrickson_lattman<double>(0,0,0,0); // No phase information without partial contribution
      PHIB[r] = rad2phi(rIntg.PHIB());
      //Map coeff is 2Fo-DFc minus half of calculated anomalous difference
      floatType FO = plus[r] ? POS.F[r] : NEG.F[r];
      cmplxType FH = plus[r] ? FHpos[r] : FHneg[r];
      cmplxType Fmap(std::polar(2*FOM[r]*FO,std::arg(FH))-FH);
      cmplxType dFH(FHpos[r]-FHneg[r]);
      if (!plus[r]) dFH *= -1.;
      Fmap -= dFH/2.;
      FWT[r] = std::abs(Fmap);
      FPFOM[r] = FWT[r]/FO;
      PHWT[r] = rad2phi(std::arg(Fmap));
    }
    else
    {
      FOM[r] = FPFOM[r] = FWT[r] = PHIB[r] = PHWT[r] = 0.;
      HL[r] = HL_ANOM[r] = cctbx::hendrickson_lattman<double>(0,0,0,0);
    }
    if (output.level(VERBOSE))
    {
      FWTA[r] = PHWTA[r] = 0; // No anomalous for singletons
      PHIBpos[r] = PHIBneg[r] = PHWT[r];
      FOMpos[r] = FOMneg[r] = FOM[r];
      HLpos[r] = HLneg[r] = HL[r];
    }
  }
  }
}

void RefineSAD::calcScalesData()
{
// Calculate structure factors for heavy atoms.
// case where the individual atomic xyzbof haven't changed
  //initialize
  for (unsigned r = 0; r < NREFL; r++) FHpos[r] = FHneg[r] = 0;

  floatType debye_PartB = cctbx::adptbx::u_as_b(PartU) * 0.25;
  floatType debye_ScaleB = cctbx::adptbx::u_as_b(ScaleU) * 0.25;
  //add partial scattering, which has no anomalous component
  if (input_partial)
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    cmplxType scale = PartK*std::exp(-ssqr[r]*debye_PartB);
    FHpos[r] += scale*FPpos[r];
    FHneg[r] += scale*FPneg[r];
  }
  //add atomic component
  if (input_atoms)
  for (unsigned r = 0; r < NREFL; r++)
  if (!bad_data[r])
  {
    floatType scale = ScaleK*std::exp(-ssqr[r]*debye_ScaleB);
    FHpos[r] += scale*FApos[r];
    FHneg[r] += scale*FAneg[r];
  }
} // end calcScalesData

af::shared<cctbx::hendrickson_lattman<double> > RefineSAD::getHL(int a)
{
  if (a == 0) return HL;
  else if (a == 1) return HLpos;
  else if (a == 2) return HLneg;
  return HL_ANOM;
}

} //namespace phaser
