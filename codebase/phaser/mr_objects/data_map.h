//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_MAP_Class__
#define __DATA_MAP_Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/data_mw.h>
#include <phaser/lib/scattering.h>
#include <phaser/mr_objects/data_trace.h>
#include <phaser/ncs/EnsembleSymmetry.h>

namespace phaser {

class data_map
{
  public:
    data_map();

    void set_data_map(const data_map& other)
    {
      PRINCIPAL_EXTENT = other.PRINCIPAL_EXTENT;
      PRINCIPAL_CENTRE = other.PRINCIPAL_CENTRE;
      COORD_EXTENT = other.COORD_EXTENT;
      COORD_CENTRE = other.COORD_CENTRE;
      TRACE_EXTENT = other.TRACE_EXTENT;
      TRACE_CENTRE = other.TRACE_CENTRE;
      MW = other.MW;
      SCATTERING = other.SCATTERING;
      CELL_SCALE = other.CELL_SCALE;
      PR = other.PR;
      PT = other.PT;
      PG = other.PG;
      HIRES = other.HIRES;
    }

  public:
  dvect3  PRINCIPAL_EXTENT;
  dvect3  PRINCIPAL_CENTRE;
  dvect3  COORD_EXTENT;
  dvect3  COORD_CENTRE;
  dvect3  TRACE_EXTENT;
  dvect3  TRACE_CENTRE;
  data_mw MW;
  double  SCATTERING;
  double  CELL_SCALE;
  dmat33  PR;
  dvect3  PT;

  ncs::PointGroup PG;
  double          HIRES;//HiRes limits for map

  void    set_centre(dvect3);
  void    set_extent(dvect3);
  double  mean_radius();
  double  max_extent();
  double  min_extent();
  double  volume();
  double  calculate_sampling_from_extent(data_trace&);
};

} //phaser

#endif
