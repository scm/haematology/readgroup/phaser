//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_GYRE_Class__
#define __PHASER_GYRE_Class__
#include <phaser/main/Phaser.h>
#include <phaser_defaults.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <cstdio>

namespace phaser {

class mr_gyre
{
  public:
    std::string MODLID;
    dmat33      R; //the rotation matrix
    dvect3      TRA; //FRAC only
    dvect3      perturbRot,perturbTrans;

  public:
    mr_gyre(
      std::string const& MODLID_="",
      dvect3 const& EULER_=dvect3(0,0,0),
      dvect3 const& TRA_=dvect3(0,0,0),
      dvect3 const& perturbRot_=dvect3(0,0,0),
      dvect3 const& perturbTrans_=dvect3(0,0,0)
      )
    :
      MODLID(MODLID_),
      TRA(TRA_),
      perturbRot(perturbRot_),
      perturbTrans(perturbTrans_)
    {
      R = euler2matrixDEG(EULER_);
    }

    std::string unparse()
    {
      std::string Card("#");
      std::string modlid = MODLID;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      Card += " ENSEMBLE " + modlid;
      Card += " DROT " + dvtos(perturbRot,5,2);
      Card += " ANGLE " + dtos(angle(),3);
      Card += " DTRA " + dvtos(perturbTrans,6,3);
      Card += " DISTANCE " + dtos(distance(),3); //not the first
      return Card;
    }

    std::string unparse_sol()
    {
      std::string modlid = MODLID;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      return "SOLU GYRE ENSEMBLE " + modlid + " EULER " + dvtos(euler(),6,3) +
             " # ANGLE " + dtos(angle()) + " DISTANCE " + dtos(distance()) + "\n";
    }

    std::string logfile(int depth)
    { //don't return anything for logfile, too confusing
      return std::string(DEF_TAB*depth,' ') + unparse() + "\n";
    }

    dvect3 euler() { return matrix2eulerDEG(R); }
    double euler_ang(int i) { return matrix2eulerDEG(R)[i]; }
    double angle() { return xyzAngle(perturbRot); }
    double angle(int i) { return perturbRot[i]; }
    double distance() { return std::sqrt(perturbTrans*perturbTrans); }
    double distance(int i) { return perturbTrans[i]; }
};

} //phaser

#endif
