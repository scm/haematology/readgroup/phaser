#include <phaser/mr_objects/data_pdb.h>
#include <phaser/mr_objects/rms_estimate.h>
#include <phaser/src/TncsVar.h>
#include <phaser/lib/solTerm.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/eLLG.h>
#include <phaser/lib/safe_mtz.h>
#include <iotbx/pdb/input.h>
#include <scitbx/misc/split_lines.h>
#include <boost/assign.hpp>
#include <cctbx/uctbx.h>
#include <cctbx/sgtbx/space_group_type.h>
#include <cctbx/eltbx/xray_scattering/gaussian.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <cctbx/miller/index_generator.h>
#include <cctbx/adptbx.h>
#include <cctbx/eltbx/tiny_pse.h>
#include <phaser/lib/consistentDLuz.h>
#include <phaser/lib/maths.h>

#include <stdexcept>

namespace phaser {

bool data_pdb::addModel(data_model MODEL)
{
  for (int f = 0; f < ENSEMBLE.size(); f++)
    if (ENSEMBLE[f].same_filestr(MODEL.coordinates())) return false; //read same pdb file twice
  ENSEMBLE.push_back(MODEL);
  return true;
}

std::string data_pdb::setup_molecules()
{
  if (setup_has_been_called) return "";
  PHASER_ASSERT(ENSEMBLE.size());
  mtrace_model_serial.resize(ENSEMBLE.size());
  //find the lowest rms model, to set MTRACE for all stats calculations
  //the lowest rms has already been found in the pdb file
  //avoid numerical instability in comparitor or ID conversion to RMS by using mtrace
  MTRACE = 0;
  LOWEST_RMS_INDEX = 0;

  if (ENSEMBLE[0].map_format())
  {
    mtrace_model_serial[0] = 1;
    //throw error
    CMtz::MTZ *mtzfrom = safe_mtz_get(ENSEMBLE[0].filename(),1);
    CMtz::MTZCOL *mtzH(0),*mtzK(0),*mtzL(0),*Acol(0),*Pcol(0);
    mtzH = safe_mtz_col_lookup(mtzfrom,"H");
    mtzK = safe_mtz_col_lookup(mtzfrom,"K");
    mtzL = safe_mtz_col_lookup(mtzfrom,"L");
    Acol = safe_mtz_col_lookup(mtzfrom,ENSEMBLE[0].LABI_F);
    Pcol = safe_mtz_col_lookup(mtzfrom,ENSEMBLE[0].LABI_P);
    CMtz::MTZXTAL* xtal = CMtz::MtzSetXtal( mtzfrom, CMtz::MtzColSet( mtzfrom, Acol));
    af::double6 uc(xtal->cell[0],xtal->cell[1],xtal->cell[2],xtal->cell[3],xtal->cell[4],xtal->cell[5]);
    cctbx::uctbx::unit_cell cctbxUC(uc);

    floatType max_resolution = 10000;
    for (int mtzr = 0; mtzr < mtzfrom->nref ; mtzr++)
    if (!mtz_isnan(Acol->ref[mtzr]) &&
        !mtz_isnan(Pcol->ref[mtzr]))
    {
      floatType H = mtzH->ref[mtzr];
      floatType K = mtzK->ref[mtzr];
      floatType L = mtzL->ref[mtzr];
      miller::index<int> HKL(H,K,L);
      floatType ssqr = cctbxUC.d_star_sq(HKL);
      floatType s    = std::sqrt(ssqr);
      if (s > 0) // Ignore 0,0,0 reflection if included in molecular transform
        max_resolution = std::min(max_resolution,1/s);
    }
    //max_resolution is the maximum *stored*
    //the effective resolution is lower than this by the shell that has been added for interpolation
    miller::index<int> UNIT(1,1,1);
    floatType Ssqr = cctbxUC.d_star_sq(UNIT);
    floatType S    = std::sqrt(Ssqr);
    //lowest resolution of all the maps/pdbs
    HIRES = std::max(HIRES,1/((1/max_resolution)-S));

    PHASER_ASSERT(ENSEMBLE[0].is_rms());
    ENSEMBLE[0].RMS.resize(1);
    ENSEMBLE[0].RMS[0] = ENSEMBLE[0].RMSID;
    LOWEST_RMS_INDEX = 0;

    //data_map set on input
    return "";
  }

  std::vector<Molecule> MOLECULE(ENSEMBLE.size());
  for (int f = 0; f < ENSEMBLE.size(); f++)
  {
    MOLECULE[f] = Molecule(ENSEMBLE[f],USE_HETATM);
    if (MOLECULE[f].read_errors() != "") return MOLECULE[f].read_errors();
    mtrace_model_serial[f] = MOLECULE[f].mtrace_model_serial(); //store so that it does not have to be recalculated from coords
    PHASER_ASSERT(MOLECULE[f].nMols());
  }
  if (ENSEMBLE.size() == 1)
  {
    is_helix = MOLECULE[0].is_helix(); //xxxxx AJM TODO ensemble of helices?
    is_atom = MOLECULE[0].is_atom();
  }
  std::vector<data_map> data_map_(ENSEMBLE.size());
  std::vector<double> SCATTERING_(ENSEMBLE.size());
  std::vector<data_mw> MW_(ENSEMBLE.size());
  xyz_weight cw;
  for (int f = 0; f < ENSEMBLE.size(); f++)
  {
    if (ENSEMBLE[f].is_rms()) //RMS contains RMS
    {
      ENSEMBLE[f].RMS = float1D(MOLECULE[f].nMols(),ENSEMBLE[f].RMSID); //expand
    }
    else if (ENSEMBLE[f].is_id()) //RMS contains ID
    {
      floatType rms = rms_estimate(ESTIMATOR).rms(ENSEMBLE[f].RMSID,MOLECULE[f].protein_nres);
      ENSEMBLE[f].RMS = float1D(MOLECULE[f].nMols(),rms);
    }
    else if (ENSEMBLE[f].is_card())
    {
      ENSEMBLE[f].RMS = MOLECULE[f].allRms(ESTIMATOR); //mtrace molecule already determined
    }
    bool ignore_occupancy(true);
    int mtrace(-999); //flag
    xyz_weight cwf = MOLECULE[f].getCW(ignore_occupancy,mtrace); //AJM TODO all
    cw += cwf;
    cwf.calculate();
    data_map_[f].PR = cwf.PR;
    data_map_[f].PT = cwf.PT;
    //CENTRE = -PR.transpose()*PT
    data_map_[f].COORD_CENTRE = cwf.CENTRE;
    data_map_[f].COORD_EXTENT = cwf.EXTENT;
    data_map_[f].PRINCIPAL_EXTENT = cwf.PRINCIPAL_EXTENT;
    data_map_[f].PRINCIPAL_CENTRE = cwf.PRINCIPAL_CENTRE;
    data_map_[f].SCATTERING = MOLECULE[f].getScat(mtrace); //AJM TODO all
    data_map_[f].MW = MOLECULE[f].getTotalMW();
    SCATTERING_[f] = data_map_[f].SCATTERING;
    PHASER_ASSERT(ENSEMBLE[f].RMS.size());
    ENSEMBLE[f].mtrace = MOLECULE[f].mtrace;
  }

  int count(0);
  double LOWEST_RMS = 10000;
  for (int f = 0; f < ENSEMBLE.size(); f++)
  {
    if (ENSEMBLE[f].RMS[ENSEMBLE[f].mtrace] < LOWEST_RMS)
    {
      count += ENSEMBLE[f].mtrace;
      MTRACE = f;
      LOWEST_RMS = ENSEMBLE[f].RMS[ENSEMBLE[f].mtrace];
      LOWEST_RMS_INDEX = count;
    }
    count += MOLECULE[f].nMols();
  }

  //all stats calculated from MTRACE
  {
  set_data_map(data_map_[MTRACE]);
  SCATTERING = mean(SCATTERING_); //backwards compatibility
  PHASER_ASSERT(SCATTERING);
  float1D prot(MW_.size());
  float1D nucl(MW_.size());
  float1D hatm(MW_.size());
  for (int f = 0; f < MW_.size(); f++)
  {
    prot[f] = data_map_[f].MW.PROTEIN; //backwards compatibility
    nucl[f] = data_map_[f].MW.NUCLEIC; //backwards compatibility
    hatm[f] = data_map_[f].MW.HETATOM; //backwards compatibility
  }
  MW = data_mw(mean(prot),mean(nucl),mean(hatm));
  cw.calculate();
  PR = cw.PR;
  PT = cw.PT;
  COORD_CENTRE = cw.CENTRE;
  COORD_EXTENT = cw.EXTENT;
  PRINCIPAL_EXTENT = cw.PRINCIPAL_EXTENT;
  PRINCIPAL_CENTRE = cw.PRINCIPAL_CENTRE;
  TRACE_CENTRE = cw.CENTRE;
  TRACE_EXTENT = cw.EXTENT;
  }
  //reset trace extent if there is a trace molecule
  if (!TRACE.MODEL.is_default())
  {
    bool ignore_occupancy(true);
    int mtrace(-999); //flag
    xyz_weight cwf = Molecule(TRACE.MODEL,USE_HETATM).getCW(ignore_occupancy,mtrace);
    cwf.calculate();
    TRACE_CENTRE = cwf.CENTRE;
    TRACE_EXTENT = cwf.EXTENT;
  }

  if (ENSEMBLE[MTRACE].pdb_format())//what about CIF, need cif reader
  {
    std::string pdb_file_content;
    std::string source_info;
    if (!ENSEMBLE[MTRACE].is_string())
    {
      std::ifstream t( ENSEMBLE[MTRACE].filename().c_str() );
      pdb_file_content = std::string(
        ( std::istreambuf_iterator<char>(t) ),  // Extra parentheses required
        std::istreambuf_iterator<char>()
        );
      t.close();
      source_info = ENSEMBLE[MTRACE].filename();
    }
    else
    {
      pdb_file_content = ENSEMBLE[MTRACE].coordinates();
      source_info = "memory";
    }

    try {
    iotbx::pdb::input ensemble_input(
      source_info.c_str(),
      scitbx::misc::split_lines(
        pdb_file_content.c_str(),
        pdb_file_content.size(),
        false,
        true
        ).const_ref()
      ); //throws UNHANDLED ERROR
    ncs::EnsembleSymmetry ensemble_symmetry;
    ensemble_symmetry.set_min_sequence_coverage(PTGRP.COVERAGE);
    ensemble_symmetry.set_min_sequence_identity(PTGRP.IDENTITY);
    ensemble_symmetry.set_max_rmsd(PTGRP.RMSD);
    ensemble_symmetry.set_angular_tolerance(PTGRP.TOLANG);
    ensemble_symmetry.set_spatial_tolerance(PTGRP.TOLSPA);
    ensemble_symmetry.set_root(ensemble_input.construct_hierarchy());
    PG = ensemble_symmetry.get_point_group();
    }
    catch (std::invalid_argument& err) {
      return std::string( "Error while interpreting " )
        + ENSEMBLE[MTRACE].filename()
        + std::string( ": " ) + std::string( err.what() );
    }
  }

  setup_has_been_called = true;
  return "";
}

size_t data_pdb::nMols()
{
  return ENSEMBLE.size();
}

Molecule data_pdb::Coords()
{
  PHASER_ASSERT(!ENSEMBLE[0].map_format());
  return Molecule(ENSEMBLE[MTRACE],USE_HETATM);
}

Molecule data_pdb::Coords(int p)
{
  PHASER_ASSERT(p < ENSEMBLE.size());
  PHASER_ASSERT(!ENSEMBLE[p].map_format());
  return Molecule(ENSEMBLE[p],USE_HETATM);
}

float1D data_pdb::allRms()
{
  float1D allRms(0);
  for (int f = 0; f < ENSEMBLE.size(); f++)
  for (int r = 0; r < ENSEMBLE[f].RMS.size(); r++)
  {
    allRms.push_back(ENSEMBLE[f].RMS[r]);
  }
  return allRms;
}

double data_pdb::avRms() //xxxx AJM TODO this will go when RefineOCC uses VRMS correctly
{ return mean(allRms()); }

void data_pdb::setup_scattering(float1D* OCC)
{
  if (OCC != NULL)
  {
    SCATTERING = Coords().getScat(OCC); //mtrace only
  }
  else if (!ENSEMBLE[0].map_format())
  {
    float1D allScat; // list of rms values including those for models within a pdb file
    for (int ipdb = 0; ipdb < nMols(); ipdb++)
      for (unsigned m = 0; m < ENSEMBLE[ipdb].nmodels(); m++)
        allScat.push_back(Coords(ipdb).getScat(m));
    SCATTERING = mean(allScat);
  }
  //else if map do nothing, use input SCATTERING
  //can be zero PHASER_ASSERT(SCATTERING);
}

void data_pdb::trace_only()
{
  //internally reconfigure the PDB molecule to only contain the trace molecule, for occupancy refinement
  int MODEL_SERIAL = mtrace_model_serial[MTRACE];
  vrms RMS = DV.VRMS[MTRACE][ENSEMBLE[MTRACE].mtrace];
  PHASER_ASSERT(RMS.fixed > 0);
  LOWEST_RMS_INDEX = 0;
  //now reset
  mtrace_model_serial.resize(1,0);
  ENSEMBLE = std::vector<data_model>(1,ENSEMBLE[MTRACE]);
  MTRACE = 0;
  ENSEMBLE[MTRACE].MODEL_SERIAL = MODEL_SERIAL;
  ENSEMBLE[MTRACE].RMS.resize(1,RMS.fixed);
  ENSEMBLE[MTRACE].RMSID = RMS.fixed;
  ENSEMBLE[MTRACE].mtrace = 0;
  PHASER_ASSERT(ENSEMBLE.size() == 1);
  PHASER_ASSERT(ENSEMBLE[0].RMS.size() == 1);
  PHASER_ASSERT(allRms().size() == 1);
  // Update RMSD values to agree with updated DLuz values
  DV.resize_vrms(1);
  DV.resize_vrms(0,1);
  DV.VRMS[0][0] = RMS;
  //SAMPLING is not changed
  //do not use TRACE, as we want it to come from occupancy refined coords by default
  TRACE.MODEL = data_model();
}

void data_pdb::fix_rms_correlations(double HIRES,Output& output)
{
  output.logEllipsisStart(LOGFILE,"Checking and fixing VRMS correlations");
  //sf calculation
  cctbx::eltbx::xray_scattering::gaussian ff_C = cctbx::eltbx::xray_scattering::wk1995("C").fetch();
  cctbx::eltbx::xray_scattering::gaussian ff_N = cctbx::eltbx::xray_scattering::wk1995("N").fetch();
  cctbx::eltbx::xray_scattering::gaussian ff_O = cctbx::eltbx::xray_scattering::wk1995("O").fetch();
  cctbx::eltbx::xray_scattering::gaussian ff_S = cctbx::eltbx::xray_scattering::wk1995("S").fetch();
  cctbx::eltbx::xray_scattering::gaussian ff_H = cctbx::eltbx::xray_scattering::wk1995("H").fetch();

  // Set cell just big enough to prevent overlap, so neighbouring reflections relatively uncorrelated
  dvect3 CELL = COORD_EXTENT + dvect3(5.,5.,5.);
  cctbx::uctbx::unit_cell unit_cell(af::double6(CELL[0],CELL[1],CELL[2],90.0,90.0,90.0));
  cctbx::sgtbx::space_group_type sg_type("P1");

  // Carry out correlation calculations at resolution limit for data, unless the
  // worst model's contribution will not be significant at that resolution.
  // Aim for 500-2000 reflections for statistics, unless model and cell too small
  double maxRMS(0.5); // Best quality model where ensembling could possibly matter
  float1D RMSlist(allRms());
  int nmodels(RMSlist.size()); // Proxy for number of coordinate sets, each of which has an RMS error assigned
  for (unsigned i = 0; i < nmodels; i++)
    maxRMS = std::max(maxRMS,RMSlist[i]);
  double resolution_d_min = std::max(HIRES,1.6*maxRMS) / 1.5; // Factor of 1.5 will be reintroduced for round 1

  // If initial RMS estimates are much too optimistic for internal agreement of models,
  // correlations at initial resolution choice may be essentially random, so back off if necessary
  // Don't go to lower than 15A, where correlations should be positive in any sensible calculation!
  double midSsqr(0);
  double minOffDiag(0.); // Minimum off-diagonal element in correlation matrix
  double targetOffDiag(0.005); // Minimum expected for two models as bad as worst is 0.00586
  TNT::Fortran_Matrix<floatType>  CorMat(nmodels,nmodels);
  while ((minOffDiag < targetOffDiag) && (resolution_d_min <= 10.))
  {
    resolution_d_min *= 1.5;
    minOffDiag = 1.;
    int ntot = scitbx::constants::two_pi*unit_cell.volume()/fn::pow3(resolution_d_min)/3.; // Approx unique reflections
    int ngoal = std::min(2000,std::max(500,ntot/10));
    ngoal = std::min(ngoal,ntot/2);
    double resolution_d_max = std::pow(double(ntot)/(ntot-ngoal),1./3.)*resolution_d_min;
    double ssqr_min(1.0/fn::pow2(resolution_d_max));

    // Generate reflection list
    midSsqr = 0.;
    bool anomalous_flag(false);
    cctbx::miller::index_generator generator(unit_cell,sg_type,anomalous_flag,resolution_d_min);
    scitbx::af::shared<cctbx::miller::index<int> >  miller;
    cctbx::miller::index<int> hkl = generator.next();
    while (hkl != cctbx::miller::index<int>(0,0,0)) //flags end
    {
      double ssqr(unit_cell.d_star_sq(hkl));
      if (ssqr > ssqr_min)
      {
        miller.push_back(hkl);
        midSsqr += ssqr;
      }
      hkl = generator.next();
    };
    PHASER_ASSERT(miller.size());
    midSsqr /= miller.size();

    // Compute structure factor sets for high resolution shell, using direct summation
    std::vector<std::vector<std::complex<double> > > Fcalc;
    Fcalc.resize(nmodels); //proxy for number of coordinate sets
    for (unsigned i = 0; i < Fcalc.size(); i++)
      Fcalc[i].resize(miller.size());

    cctbx::eltbx::xray_scattering::wk1995(element);
    int M(0);
    for (unsigned f = 0; f < ENSEMBLE.size(); f++)
    { //loop over ENSE cards on input
      Molecule pdb_mol = Coords(f);
      for (unsigned m = 0; m < pdb_mol.nMols(); m++)
      { //loop over MODELS in pdb files
        for (unsigned r = 0; r < miller.size(); r++)
        {
          double rssqr =  unit_cell.d_star_sq(miller[r]);
          double fo_C = ff_C.at_d_star_sq(rssqr);
          double fo_N = ff_N.at_d_star_sq(rssqr);
          double fo_O = ff_O.at_d_star_sq(rssqr);
          double fo_S = ff_S.at_d_star_sq(rssqr);
          double fo_H = ff_H.at_d_star_sq(rssqr);
          Fcalc[M][r] = 0.;
          for (unsigned a = 0; a < pdb_mol.nAtoms(m); a++)
          if (!pdb_mol.isUnknown(m,a))
          {
            cctbx::xray::scatterer<> scatterer(
              "another atom", // label
              unit_cell.fractionalization_matrix()*(pdb_mol.card(m,a).X), // site
              cctbx::adptbx::b_as_u(pdb_mol.card(m,a).B), // u_iso
              pdb_mol.card(m,a).O, // occupancy
              cctbx::eltbx::xray_scattering::wk1995(pdb_mol.card(m,a).Element).label(),
              0, // fp
              0); // fdp
            double fo_plus_fp(0);
            if (scatterer.scattering_type == "C")
              fo_plus_fp = fo_C;
            else if (scatterer.scattering_type == "N")
              fo_plus_fp = fo_N;
            else if (scatterer.scattering_type == "O")
              fo_plus_fp = fo_O;
            else if (scatterer.scattering_type == "S")
              fo_plus_fp = fo_S;
            else if (scatterer.scattering_type == "H")
              fo_plus_fp = fo_H;
            else
              fo_plus_fp = cctbx::eltbx::xray_scattering::wk1995(scatterer.scattering_type).fetch().at_d_star_sq(rssqr);
            double debye_waller_u_iso = cctbx::adptbx::u_as_b(scatterer.u_iso) * 0.25;
            double isoB = std::exp(-rssqr*debye_waller_u_iso);
            double scat(fo_plus_fp*isoB);
            double theta = miller[r][0]*scatterer.site[0] + miller[r][1]*scatterer.site[1] + miller[r][2]*scatterer.site[2];
            theta *= scitbx::constants::two_pi;
            double costheta = std::cos(theta);
            double sintheta = std::sin(theta);
            std::complex<double> noOccFcalc = scat*std::complex<double>(costheta,sintheta);
            Fcalc[M][r] += scatterer.occupancy*noOccFcalc;
          }
        }
        M++;
      }
    }

    PHASER_ASSERT(M == nmodels);

    // Compute correlation matrix
    float1D sqrtSigmaP(nmodels,0.); // Normalisation factors
    for (unsigned m=0; m < nmodels; m++)
    {
      for (unsigned r = 0; r < miller.size(); r++)
        sqrtSigmaP[m] += std::norm(Fcalc[m][r]);
      sqrtSigmaP[m] = std::sqrt(sqrtSigmaP[m]);
    }
    for (unsigned m = 0; m < nmodels; m++)
    {
      for (unsigned n = m; n < nmodels; n++)
      {
        if (n == m) CorMat(m+1,n+1) = 1.;
        else
        {
          double sumF1F2(0.);
          for (unsigned r = 0; r < miller.size(); r++)
            sumF1F2 += std::real((Fcalc[m][r])*std::conj(Fcalc[n][r]));
          CorMat(m+1,n+1) = CorMat(n+1,m+1) = sumF1F2/(sqrtSigmaP[m]*sqrtSigmaP[n]);
          minOffDiag = std::min(minOffDiag,CorMat(m+1,n+1));
        }
      }
    }
  } // end while loop

  // Massage DLuz values computed from input RMSD to be consistent with correlation matrix at chosen resolution
  TNT::Vector<floatType> DLuz(nmodels);
  for (unsigned m = 0; m < nmodels; m++)
    DLuz[m] = DLuzzati(midSsqr,RMSlist[m]); // Don't need solvent term to obtain internal consistency
  TNT::Fortran_Matrix<floatType> pseudoCorMatInv(nmodels,nmodels); // returned from consistentDLuz
  TNT::Vector<floatType> DLuz_updated = consistentDLuz(DLuz,CorMat,pseudoCorMatInv);

  // Update RMSD values to agree with updated DLuz values
  DV.resize_vrms(nMols());
  for (int ipdb = 0; ipdb < nMols(); ipdb++)
    DV.resize_vrms(ipdb,ENSEMBLE[ipdb].nmodels());
  int m(0);
  for (int f = 0; f < ENSEMBLE.size(); f++)
    for (int r = 0; r < ENSEMBLE[f].RMS.size(); r++)
    {
      floatType thisDVRMS = std::log(DLuz[m]/DLuz_updated[m])/(two_pi_sq_on_three*midSsqr);
      floatType thisVRMS = std::sqrt(fn::pow2(RMSlist[m]) + thisDVRMS);
      DV.VRMS[f][r].fixed = thisVRMS;
      m++;
    }
  PHASER_ASSERT(m == nmodels);
  output.logEllipsisEnd(LOGFILE);
  output.logBlank(LOGFILE);
}

void data_pdb::setup_and_fix_vrms(std::string modlid,double HIRES,data_tncs& PTNCS,data_solpar& SOLPAR,Output& output)
{
  if (fix_vrms_correlations_has_been_called) return;
  fix_vrms_correlations_has_been_called = true;

  output.logUnderLine(LOGFILE,"Ensemble: " + modlid);

  bool multi_ensemble = (ENSEMBLE.size() > 1) || (ENSEMBLE[0].nmodels() > 1);
  if (ENSEMBLE[0].map_format() || !multi_ensemble)
  {
    DV.init_vrms_single();
    DV.VRMS[0][0].fixed = ENSEMBLE[0].RMS[0];
    double meanRadius = mean_radius();
    std::pair<double,double> min_max_rms = rms_estimate(ESTIMATOR).min_max_rms(HIRES,meanRadius);
    DV.VRMS[0][0].lower_rms_limit = min_max_rms.first;
    DV.VRMS[0][0].upper_rms_limit = min_max_rms.second;
  }
  else if (is_atom)
  {
    DV.init_vrms_single();
    DV.VRMS[0][0].fixed = ENSEMBLE[0].RMS[0];
    DV.VRMS[0][0].lower_rms_limit = ENSEMBLE[0].RMS[0];
    DV.VRMS[0][0].upper_rms_limit = ENSEMBLE[0].RMS[0];
  }
  else
  {
    fix_rms_correlations(HIRES,output);
    for (int ipdb = 0; ipdb < nMols(); ipdb++)
    {
      for (unsigned m = 0; m < ENSEMBLE[ipdb].nmodels(); m++)
      {
        double meanRadius = mean_radius();
        PHASER_ASSERT(meanRadius > 0);
        std::pair<double,double> min_max_rms = rms_estimate(ESTIMATOR).min_max_rms(HIRES,meanRadius);
        floatType max_rms(0);
        if (PTNCS.use_and_present())
        {
          // To ensure that variances do not become negative on refinement of the
          // VAR.RMSD of the model, it is sufficient to ensure that the DLuzzati^2
          // corresponding to the VAR.RMSD is always less than the refined Drms for the
          // tNCS-related copies, deduced from the observed data.  This is
          // actually conservative, because the model is probably not complete
          // and there will be a contribution of measurement error to the variance.
          // Do the calculations for the low resolution ends of the resolution bins,
          // because this requires the largest VAR.RMSD to give a particular DLuzzati.
          // DLuzzati^2 = (exp(-2Pi^2/3*(VAR.RMSD/d)^2)*sc)^2,
          //   where d = d-spacing (reso) and sc is bulk solvent correction term.
          // DLuzzati^2 < Drms if log(DLuzzati^2) < log(Drms), which implies
          // VAR.RMSD^2 > 3 d^2/(4Pi^2)*(2log(sc) - log(Drms))
          // NB: D below is Drms.
          TncsVar tncsvar(PTNCS);
          for (int s = 0; s < tncsvar.VAR.BINS.size(); s++)
          {
            floatType ssqr(1./fn::pow2(tncsvar.VAR.RESO[s]));
            floatType solCorr(solTerm(ssqr,SOLPAR));
            floatType rms(0);
            if (solCorr > 0)
            {
              floatType deltalog = std::max(0.,2*std::log(solCorr) - log(tncsvar.D(s)));
              rms = std::sqrt(3/ssqr/scitbx::constants::four_pi_sq*deltalog);
              //rms = std::sqrt(std::max(0.,3/ssqr/scitbx::constants::four_pi_sq*(2*std::log(solCorr) - log(tncsvar.D(s)))));
            }
            max_rms = std::max(max_rms,rms);
          }
        }
        min_max_rms.first = std::max(min_max_rms.first,max_rms);
        floatType this_lower_rms_limit = std::min(min_max_rms.first,min_max_rms.second);
        floatType this_upper_rms_limit = std::max(min_max_rms.first,min_max_rms.second);
        DV.VRMS[ipdb][m].lower_rms_limit = this_lower_rms_limit;
        DV.VRMS[ipdb][m].upper_rms_limit = this_upper_rms_limit;
      }
    }
  }
  DV.setup_drms_limits(); //call first
  DV.setup_vrms_limits(); //call second

  //print results
  for (int ipdb = 0; ipdb < ENSEMBLE.size(); ipdb++)
  {
    if (ENSEMBLE[ipdb].is_string())
      output.logTab(1,LOGFILE,"Coordinates from string # " +itos(ipdb+1) + " " + ENSEMBLE[ipdb].string_name());
    else
      output.logTab(1,LOGFILE,ENSEMBLE[ipdb].format() + " file # " +itos(ipdb+1) +": " + ENSEMBLE[ipdb].basename());
    (ENSEMBLE[ipdb].nmodels() == 1) ?
      output.logTab(2,LOGFILE,"This file contains 1 model"):
      output.logTab(2,LOGFILE,"This file contains " + itos(ENSEMBLE.size()) + " models");
    for (unsigned m = 0; m < ENSEMBLE[ipdb].nmodels(); m++)
    {
      if (is_atom) // no refinement of DRMS for single atom
      {
        output.logTab(2,LOGFILE,"The RmsD of atom with respect to the real structure is " + dtos(ENSEMBLE[ipdb].RMS[m],5,3));
        floatType warning_rms(0.2);
        if (ENSEMBLE[ipdb].RMS[m] > warning_rms)
          output.logWarning(SUMMARY,"Single Atom MR: The RmsD of atom with respect to the real structure (" + dtos(ENSEMBLE[ipdb].RMS[m],5,3) + ") is greater than " + dtos(warning_rms));
      }
      else
      {
        output.logTab(2,LOGFILE,"The input RmsD of model #" + itos(m+1) + " with respect to the real structure is " + dtos(ENSEMBLE[ipdb].RMS[m],5,3));
        if (multi_ensemble)
          output.logTab(2,LOGFILE,"The correlation corrected RmsD of model #" + itos(m+1) + " is " + dtos(DV.VRMS[ipdb][m].fixed,5,3));
        output.logTab(3,VERBOSE,"Allowed range (resolution and radius dependent) is " + dtos(DV.VRMS[ipdb][m].lower_rms_limit,5,3) + " to " + dtos(DV.VRMS[ipdb][m].upper_rms_limit,5,3));
      }
    }
  }

  output.logTab(1,DEBUG,"Initial VRMS delta lower/upper limit = " + dtos(DV.DRMS.lower,8,6) + " / " + dtos(DV.DRMS.upper,8,6));
  output.logTab(2,DEBUG,"Initial RMSD fixed value(s): " + dvtos(DV.fixed_vrms_array(),5,3));
  output.logTab(2,DEBUG,"Initial RMSD upper limit(s): " + dvtos(DV.upper_vrms_array(),5,3));
  output.logTab(2,DEBUG,"Initial RMSD lower limit(s): " + dvtos(DV.lower_vrms_array(),5,3));
  output.logBlank(DEBUG);

  if (!DISABLE_GYRE && ENSEMBLE.size() && !ENSEMBLE[0].map_format())
  {
    Molecule MOLECULE(ENSEMBLE[0],USE_HETATM);
    bool ignore_occupancy(false);
    int mtrace(-999);
    xyz_weight cwf = MOLECULE.getCW(ignore_occupancy,mtrace);
    cwf.calculate();
    PRINCIPAL_EXTENT = cwf.PRINCIPAL_EXTENT;
    PRINCIPAL_CENTRE = cwf.PRINCIPAL_CENTRE;
  }
}

} //phaser
