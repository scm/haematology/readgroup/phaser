//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MR_SET_Class__
#define __MR_SET_Class__
#include <phaser/mr_objects/mr_rlist.h>
#include <phaser/mr_objects/mr_ndim.h>
#include <phaser/mr_objects/mr_gyre.h>
#include <phaser/mr_objects/drms_vrms.h>
#include <phaser/mr_objects/mr_clash.h>
#include <phaser/include/data_peak.h>
#include <phaser/pod/MapCoefs.h>
#include <phaser/io/Errors.h>
#include <iostream>
#include <iterator>
#include <sstream>
#include <algorithm>
#include <stdio.h>

namespace phaser {

class mr_set
{
  public:
  std::string           ANNOTATION,HALL,HISTORY;
  double                TF,TFZ,TFZeq,LLG,ORIG_LLG,PAK,R,ORIG_R,CONTRAST;
  int                   ORIG_NUM,NUM,EQUIV,NROT,NSET,NRF,NTF,NCLUS;
  bool                  KEEP,PURGE,CUMULATIVE_LLG_INCREASES,PACKS;
  std::vector<mr_ndim>  KNOWN;
  std::vector<mr_rlist> RLIST;
  std::vector<mr_gyre>  GYRE;
  std::vector<bool1D>   BOOLOCC;
  std::vector<float1D>  REALOCC;
  int1D                 TMPLT;
  MapCoefs              MAPCOEFS;
  map_str_float         CELL;
  double                DATACELL;
  map_str_float         DRMS;    //the shift in VRMS applied to all models

  //for results only
  map_str_float1D       VRMS;    //the input VRMS (RMSD) per model of the ensemble§
  map_str_float1D       NEWVRMS; //the DRMS-shifted VRMS per model of the ensemble

  //for packing
  mr_matrix_clash       CLASH;

  public:
  mr_set()
  {
    ANNOTATION = HALL = HISTORY = "";
    KEEP = 1; //true
    PURGE = 0; //false
    ORIG_NUM = NUM = EQUIV = NROT = NSET = NRF = NTF = NCLUS = 0;
    TF = TFZ = TFZeq = LLG = ORIG_LLG = PAK = R = ORIG_R = CONTRAST = 0;
    CUMULATIVE_LLG_INCREASES = false;
    PACKS = true;
    DATACELL = 1;
    BOOLOCC.clear();
    REALOCC.clear();
  }

  mr_set(
    std::string const& ANNOTATION_,
    std::vector<mr_ndim> const& KNOWN_,
    std::vector<mr_rlist> const& RLIST_,
    std::string const& HALL_
    )
  :
    ANNOTATION(ANNOTATION_),
    HALL(HALL_),
    KNOWN(KNOWN_),
    RLIST(RLIST_)
  {
    KEEP = 1; //true
    PURGE = 0; //false
    ORIG_NUM = NUM = EQUIV = NROT = NSET = NRF = NTF = NCLUS = 0;
    TF = TFZ = TFZeq = LLG = ORIG_LLG = PAK = R = ORIG_R = CONTRAST = 0;
    CUMULATIVE_LLG_INCREASES = false;
    PACKS = true;
    DATACELL = 1;
    HISTORY = "";
    BOOLOCC.clear();
    REALOCC.clear();
  }

  mr_set(
    std::string const& ANNOTATION_,
    std::vector<mr_ndim> const& KNOWN_,
    std::vector<mr_rlist> const& RLIST_,
    std::string const& HALL_,
    double const& TF_,
    double const& TFZ_,
    double const& LLG_,
    double const& R_,
    double const& PAK_,
    int const& ORIG_NUM_,
    map_str_float1D const& NEWVRMS_,
    map_str_float const& CELL_,
    MapCoefs const& MAPCOEFS_
    )
  :
    ANNOTATION(ANNOTATION_),
    HALL(HALL_),
    TF(TF_),
    TFZ(TFZ_),
    LLG(LLG_),
    PAK(PAK_),
    R(R_),
    ORIG_NUM(ORIG_NUM_),
    KNOWN(KNOWN_),
    RLIST(RLIST_),
    MAPCOEFS(MAPCOEFS_),
    CELL(CELL_),
    NEWVRMS(NEWVRMS_)
  {
    KEEP = 1; //true
    PURGE = 0; //false
    ORIG_NUM = NUM = EQUIV = NROT = NSET = NRF = NTF = NCLUS = 0;
    TFZeq = ORIG_LLG = ORIG_R = CONTRAST = 0;
    CUMULATIVE_LLG_INCREASES = false;
    PACKS = true;
    DATACELL = 1;
    HISTORY = "";
    BOOLOCC.clear();
    REALOCC.clear();
  }

  bool empty()              { return !(KNOWN.size() || RLIST.size() || GYRE.size()); }
  void add(mr_ndim known)   { KNOWN.push_back(known); }

  std::string unparse();
  std::string short_annotation(int,int=0);
  std::string logfile(int depth,bool=false,bool=true);
  std::string unparse_template();
  stringset   set_of_modlids();
  stringset   set_of_modlids_with_numbers();
  std::string getSpaceGroupName();
  std::string getSpaceGroupHall();
  int         Z();
  int         getNDEEP() const;
  bool        has_mult();
  map_str_float1D   modlid_occupancies();
  float1D     bool_to_real_occupancies(int);
  void        real_to_bool_occupancies(double);
  void        setup_newvrms(std::map<std::string,drms_vrms>);

  // Fake method required for python sets
  bool operator==(const mr_set& other) const
  {
    return
      ANNOTATION == other.ANNOTATION &&
      HISTORY == other.HISTORY &&
      HALL == other.HALL &&
      TF == other.TF &&
      TFZ == other.TFZ &&
      LLG == other.LLG &&
      R == other.R &&
      PAK == other.PAK &&
      ORIG_NUM == other.ORIG_NUM &&
      DRMS == other.DRMS &&
      CELL == other.CELL &&
      DATACELL == other.DATACELL;
  }

  //sequester the flag CUMULATIVE_LLG_INCREASES for packing high tfz in runMR_PAK
  void high_tfz(bool b) { CUMULATIVE_LLG_INCREASES = b; }
  bool high_tfz() { return CUMULATIVE_LLG_INCREASES; }
};

} //phaser

#endif
