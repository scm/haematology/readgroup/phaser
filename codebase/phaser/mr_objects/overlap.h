//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_OVERLAP_Class__
#define __PHASER_OVERLAP_Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_ndim.h>
#include <cctbx/sgtbx/space_group.h>
#include <cctbx/uctbx.h>

namespace phaser {

class overlap
{
  public:
  int       MULT;
  dvect3    COFM,EXACT_SITE;

  overlap(mr_ndim&,cctbx::sgtbx::space_group&,cctbx::uctbx::unit_cell&,dvect3&);
};

} //phaser

#endif
