#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/rms_estimate.h>
#include <algorithm>

//#define __PHASER_USE_CHOTHIA_AND_LESK__

namespace phaser {

  double rms_estimate::rms(double identity,int num_residues)
  {
    double val = (this->*estimator)(identity,num_residues);
    val *= (1 + vrms_perturb*sigma());
    double min_rms = floor();
    return std::max(min_rms,val); //floor the function
  }

  double rms_estimate::floor() { return estimator_floor; }
  double rms_estimate::sigma() { return estimator_sigma; }

  std::pair<double,double> rms_estimate::min_max_rms(double resolution,double radius)
  {
    double min_rms = std::min(resolution/20.,floor());
    double fixed_rms(0.5); //single atom
    double divfactor = ESTIMATOR == "TEST" ? 2.0 : 6.0;
    double max_rms = std::max(fixed_rms,radius/divfactor);
    std::pair<double,double> min_max;
    min_max.first = std::min(max_rms,min_rms); //paranoia
    min_max.second = std::max(max_rms,min_rms); //paranoia
    return min_max;
  }

  double rms_estimate::chothia_and_lesk(const double& identity, int num_residues)
  {
    return 0.40*exp(1.87*(1.0-identity));
  }

  double rms_estimate::oeffner(const double& identity, int num_residues)
  {
    // The use of min/max on the number of residues avoids extrapolating beyond range of well
    // populated data
    // Acta Cryst. (2013). D69, 2209-2215 page:2213
    // eVRMS = A(B + num_residues)^(1/3) * exp(C*H), H = 1-identity/100
    // A = 0.0569, B = 173, C = 1.52.
    return (A * std::pow(B + std::min(std::max(num_residues, 125), 1500), 1. / 3.))
      * exp(C * (1. - identity));
  }

}
