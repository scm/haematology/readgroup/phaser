//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_PDB_Class__
#define __DATA_PDB_Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/data_solpar.h>
#include <phaser/include/data_ellg.h>
#include <phaser/include/data_bins.h>
#include <phaser/include/data_ptgrp.h>
#include <phaser/include/data_tncs.h>
#include <phaser/src/Molecule.h>
#include <phaser/mr_objects/drms_vrms.h>
#include <phaser/lib/scattering.h>
#include <phaser/mr_objects/data_map.h>
#include <phaser/mr_objects/data_model.h>
#include <phaser/io/Output.h>
#include <phaser_defaults.h>

namespace phaser {

class data_pdb : public data_map
{
  public:
    data_pdb() : data_map()
    {
      BIN.set_default_ensembles();
      PTGRP = data_ptgrp();
      DISABLE_CHECK = bool(DEF_ENSE_DISA_CHEC);
      DISABLE_GYRE = bool(DEF_ENSE_DISA_GYRE);
      USE_HETATM = bool(DEF_ENSE_USE_HETATM);
      MTRACE = 0;
      mtrace_model_serial.clear();
      ENSEMBLE.clear();
      ESTIMATOR = std::string(DEF_ENSE_ESTI);
      BFAC_ZERO_INPUT = false;
      is_helix = false;
      is_atom = false;
      setup_has_been_called = false;
      fix_vrms_correlations_has_been_called = false;
      TRACE = data_trace();
    }

    void set_data_pdb(const data_pdb& other)
    {
      BIN = other.BIN;
      PTGRP = other.PTGRP;
      DISABLE_CHECK = other.DISABLE_CHECK;
      DISABLE_GYRE = other.DISABLE_GYRE;
      USE_HETATM = other.USE_HETATM;
      MTRACE = other.MTRACE;
      TRACE = other.TRACE;
      mtrace_model_serial = other.mtrace_model_serial;
      ENSEMBLE = other.ENSEMBLE;
      ESTIMATOR = other.ESTIMATOR;
      BFAC_ZERO_INPUT = other.BFAC_ZERO_INPUT;
      is_helix = other.is_helix;
      is_atom = other.is_atom;
      DV = other.DV;
      set_data_map(other);
      setup_has_been_called = other.setup_has_been_called;
      fix_vrms_correlations_has_been_called = other.fix_vrms_correlations_has_been_called;
      TRACE = other.TRACE;
    }

    const data_pdb& operator=(const data_pdb& right)
    {
      if (&right != this) set_data_pdb(right);
      return *this;
    }


  data_bins   BIN;
  data_ptgrp  PTGRP;
  bool        DISABLE_GYRE;
  bool        DISABLE_CHECK;
  bool        USE_HETATM;
  int         MTRACE;
  int         LOWEST_RMS_INDEX;
  int1D       mtrace_model_serial;
  bool        is_helix,is_atom;
  drms_vrms   DV;
  data_trace  TRACE;

  std::vector<data_model> ENSEMBLE;
  std::string ESTIMATOR;
  bool        BFAC_ZERO_INPUT;

  bool        addModel(data_model);
  std::string setup_molecules();
  void        setup_scattering(float1D*);
  void        fix_rms_correlations(double HIRES,Output& output);
  void        setup_and_fix_vrms(std::string,double,data_tncs&,data_solpar&,Output&);

  size_t    nMols();
  Molecule  Coords();
  Molecule  Coords(int);
  bool      map_format() const { return ENSEMBLE[0].map_format(); }

  float1D   allRms();
  double    avRms();
  void      trace_only();

  //python interface
  dvect3 centre() { return COORD_CENTRE; }
  double mean_rad_prpt() { return mean_radius(); }
  dvect31D symm_eulers() { return PG.get_euler_list(); }

  bool setup_has_been_called;
  bool fix_vrms_correlations_has_been_called;
};

} //phaser

#endif
