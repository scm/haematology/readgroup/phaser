#include <phaser/mr_objects/data_trace.h>

namespace phaser {

data_trace::data_trace()
{
  SAMP_DISTANCE = 0;
  SAMP_MIN = double(DEF_TRAC_SAMP_MIN);
  SAMP_TARGET = double(DEF_TRAC_SAMP_TARG);
  SAMP_RANGE = double(DEF_TRAC_SAMP_RANG);
  SAMP_USE = std::string(DEF_TRAC_SAMP_USE);
  SAMP_WANG = double(DEF_TRAC_SAMP_WANG);
  SAMP_ASA = double(DEF_TRAC_SAMP_ASA);
  SAMP_NCYC = double(DEF_TRAC_SAMP_NCYC);
  MODEL = data_model();
}

bool data_trace::set_use(std::string use)
{
  SAMP_USE = use;
  return (use == "ALL" || use == "CALPHA" || use == "HEXGRID" || use == "AUTO");
}

} //phaser
