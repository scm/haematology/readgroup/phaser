//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MRSET_COMP_TYPE_Class__
#define __MRSET_COMP_TYPE_Class__
#include <phaser/mr_objects/mr_set.h>

namespace phaser {

inline  bool mrset_llg_comp(const mr_set& m1, const mr_set& m2)
{
  if (m1.LLG == m2.LLG)
    return (m1.ORIG_NUM < m2.ORIG_NUM);
  return m1.LLG < m2.LLG;
}

inline  bool mrset_tf_comp(const mr_set& m1, const mr_set& m2)
{
  if (m1.TF == m2.TF)
  {
    if (m1.ORIG_NUM == m2.ORIG_NUM)
      return m1.NCLUS > m2.NCLUS; //yes, invert operator
    else
      return (m1.ORIG_NUM < m2.ORIG_NUM);
  }
  return m1.TF < m2.TF;
}

inline  bool mrset_orig_num_comp(const mr_set& m1, const mr_set& m2)
{
  return m1.ORIG_NUM < m2.ORIG_NUM;
}

} //phaser

#endif
