//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_MODEL_Class__
#define __DATA_MODEL_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/END.h>
#include <phaser/mr_objects/data_vartype.h>

namespace phaser {


class data_model : public data_vartype
{
  private:
    std::string FILESTR;
    bool        IS_STRING;
    std::string STRNAME;
    std::string FORMAT;

  public:
    const std::string& filename() const    { return FILESTR; }
    const std::string& element() const     { return FILESTR; }
    const std::string& coordinates() const { return FILESTR; }
    const std::string& format() const      { return FORMAT; }
    const std::string& string_name() const { return STRNAME; }
    const bool& is_string() const          { return IS_STRING; }

    float1D     RMS; //nmodels in pdb file
    std::string LABI_F,LABI_P;
    double      rms_outside_range;
    std::string CHAIN;
    int         MODEL_SERIAL;
    int         mtrace;

    bool BFAC_ZERO; //hack added for testing

    int nmodels() const { return RMS.size(); }

    data_model()
    {
      FILESTR = "";
      STRNAME = "";
      LABI_F = LABI_P = FORMAT = "";
      IS_STRING = true;
      RMS.clear();
      rms_outside_range = 0;
      CHAIN = "";
      MODEL_SERIAL = -999;
      BFAC_ZERO = false;
    }

    bool is_default()
    {
      return
         (FILESTR == "" &&
          LABI_F == "" &&
          LABI_P == "" &&
          FORMAT == "" &&
          IS_STRING == true &&
          !RMS.size() &&
          rms_outside_range == 0 );
    }

    void init_filename(std::string  STR_,
                       std::string  FORMAT_,
                       std::string  VARTYPE_,
                       double       RMSID_)
    {
      FILESTR = STR_;
      set_format(FORMAT_);
      set_vartype(VARTYPE_);
      set_rmsid(RMSID_);
      IS_STRING = false;
    }

    void init_trace(std::string  STR_,
                    std::string  FORMAT_)
    {
      FILESTR = STR_;
      set_format(FORMAT_);
      set_vartype("RMS"); //irrelevant
      set_rmsid(0);//irrelevant
      IS_STRING = false;
    }

    void init_card(std::string  STR_)
    {
      FILESTR = STR_;
      set_format("PDB");
      set_vartype("CARD");
      set_rmsid(0);//irrelevant
      IS_STRING = false;
    }

    void init_string(std::string  STR_,
                     std::string  STRNAME_,
                     std::string  FORMAT_,
                     std::string  VARTYPE_,
                     double       RMSID_)
    {
      FILESTR = STR_;
      STRNAME = STRNAME_;
      set_format(FORMAT_);
      set_vartype(VARTYPE_);
      set_rmsid(RMSID_);
      IS_STRING = true;
    }

    void init_helix(std::string  STR_,
                    double       RMSID_)
    {
      FILESTR = STR_;
      IS_STRING = true;
      STRNAME = "helix";
      set_format("HELIX");
      set_vartype("RMS");
      set_rmsid(RMSID_);
    }

    void init_atom(std::string  STR_,
                   double       RMSID_)
    {
      if (STR_[0] == '\"') STR_.erase(0,1);
      if (STR_[STR_.size()-1] == '\"') STR_.erase(STR_.size()-1,1);
      FILESTR = STR_;
      IS_STRING = true;
      STRNAME = "atom";
      set_format("ATOM");
      set_vartype("RMS");
      set_rmsid(RMSID_);
    }

    void init_map(std::string HKLIN_,
                  std::string LABI_F_,
                  std::string LABI_P_,
                  double      RMS_)
    {
      FILESTR = HKLIN_;
      LABI_F = LABI_F_;
      LABI_P = LABI_P_;
      set_format("MAP");
      set_vartype("RMS");
      set_rmsid(RMS_);
      IS_STRING = false;
    }

    bool pdb_format()  const  { return FORMAT == "PDB"; }
    bool ha_format()   const  { return FORMAT == "HA"; }
    bool cif_format()  const  { return FORMAT == "CIF"; }
    bool map_format()  const  { return FORMAT == "MAP"; }
    bool is_helix() const     { return FORMAT == "HELIX"; }
    bool is_atom() const      { return FORMAT == "ATOM"; }

    bool is_file()            { return (!IS_STRING && !(is_helix() || is_atom()));  }
    int  helix_length() const { return std::atoi(FILESTR.c_str()); }
    bool same_filestr(std::string FILESTR_) { return (FILESTR == FILESTR_); }
    std::string basename() const
    {
      if (is_helix()) return "Helix " + FILESTR;
      else if (is_atom()) return "Atom " + FILESTR;
      return FILESTR.substr(FILESTR.find_last_of("\\/")+1);
    }
    std::string rootname() const
    {
      std::string root = basename();
      return root.substr(0,FILESTR.find_first_of("."));
    }
    std::string strname() const   { return STRNAME; }

    void set_filestr(std::string FILESTR_) { FILESTR = FILESTR_; }
    void set_as_string(std::string name)   { IS_STRING = true; STRNAME = name; }
    void set_as_file()                     { IS_STRING = false; }

    void set_format(std::string FORMAT_)
    {
      FORMAT = FORMAT_;
      PHASER_ASSERT(FORMAT == "PDB" ||
                    FORMAT == "CIF" ||
                    FORMAT == "HA" ||
                    FORMAT == "ATOM" ||
                    FORMAT == "HELIX" ||
                    FORMAT == "MAP");
    }

    std::string unparse()
    {
      std::string Card;
      if (map_format())
      { //not used yet
        Card += " HKLIN \"" + filename() + "\"";
        Card += " F = " + LABI_F;
        Card += " PHI = " + LABI_P;
        Card += " RMS " + dtos(RMSID);
      }
      else if (FILESTR.size())
      {
        Card += FORMAT + " ";
        Card += is_helix() ?  FILESTR : "\"" + FILESTR + "\"";
        if (is_rms()) Card += " RMS " + dtos(RMSID);
        else if (is_id())  Card += " ID " + dtos(RMSID);
        else Card += " CARD ON";
        if (CHAIN.size()) Card += " CHAIN \"" + CHAIN + "\"";
        if (MODEL_SERIAL != -999) Card += " MODEL SERIAL " + itos(MODEL_SERIAL);
      }
      return Card;
    }

};

} //phaser

#endif
