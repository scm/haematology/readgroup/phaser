//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MR_SOLUTION_Class__
#define __MR_SOLUTION_Class__
#include <phaser/mr_objects/mr_set.h>
#include <phaser/include/data_zscore.h>
#include <phaser/include/data_ellg.h>
#include <phaser/include/data_bofac.h>
#include <phaser/include/data_tncs.h>
#include <phaser/src/SiteListXyz.h>
#include <phaser/io/Output.h>
#include <phaser/mr_objects/drms_vrms.h>
#include <phaser/pod/delel.h>
#include <phaser/pod/llg_cutoff.h>

namespace phaser {

class mr_solution
{
  private:
  std::vector<mr_set> SET; //don't use af::shared
  int NROT;
  bool FTF_RESCORED,DEEP_USED,CAN_LOWER_RESOLUTION;
  double PURGE_MEAN,HIRES;
  public:
  bool PTF;

  public:
  mr_solution(int nrot = 0) : NROT(nrot)
  { SET.clear(); FTF_RESCORED = DEEP_USED = CAN_LOWER_RESOLUTION = PTF = PURGE_MEAN = HIRES = 0; }
  mr_solution(std::vector<mr_set> const& SET_) : SET(SET_)
  { NROT = FTF_RESCORED = DEEP_USED = CAN_LOWER_RESOLUTION = PTF = PURGE_MEAN = HIRES = 0; }
  mr_solution(std::vector<mr_set> const& SET_, double const& HIRES_) : SET(SET_), HIRES(HIRES_)
  { NROT = FTF_RESCORED = DEEP_USED = CAN_LOWER_RESOLUTION = PTF = PURGE_MEAN = 0; }

  void set_mr_solution(const mr_solution & init)
  {
    SET.resize(init.SET.size());
    for (int i = 0; i < init.SET.size(); i++)
      SET[i] = init.SET[i];
    setNROT(init.num_rlist());
    copy_extras(init);
  }

  mr_solution(const mr_solution & init)
  { set_mr_solution(init); }

  void copy_extras(const mr_solution & init)
  {
    set_as_rescored(init.has_been_rescored());
    set_purge_mean(init.get_purge_mean());
    set_resolution(init.get_resolution());
    set_deep_used(init.get_deep_used());
    can_lower_resolution(init.can_lower_resolution());
    PTF = init.PTF;
  }

  const mr_solution& operator=(const mr_solution& right)
  {
    if (&right != this) set_mr_solution(right);
    return *this;
  }

  mr_solution& operator+=(const mr_solution &rhs)
  {
    for (int i = 0; i < rhs.SET.size(); i++)
      SET.push_back(rhs.SET[i]);
    return *this;
  }

  int operator!=(const mr_solution& right) const
  {
    if (size() != right.size()) return true;
    if (!size()) return true; // usage detail
    return (SET[0].KNOWN.size() != right[0].KNOWN.size());
  }

  // vector interface
  typedef std::vector<mr_set>::value_type value_type;
  typedef std::vector<mr_set>::size_type size_type;
  typedef std::vector<mr_set>::difference_type difference_type;
  typedef std::vector<mr_set>::reference reference;
  typedef std::vector<mr_set>::const_reference const_reference;
  typedef std::vector<mr_set>::iterator iterator;
  typedef std::vector<mr_set>::const_iterator const_iterator;

  template<typename InputIterator>
  mr_solution(InputIterator first, InputIterator last) : SET( first, last ) {}

  const_reference operator[](size_type t) const { return SET[t]; }
  reference operator[](size_type t) { return SET[t]; }
  iterator begin() { return SET.begin(); }
  iterator end() { return SET.end(); }
  const_iterator begin() const { return SET.begin(); }
  const_iterator end() const { return SET.end(); }
  size_type size() const { return SET.size(); }
  int num_packs() const;
  iterator insert(iterator pos, const value_type& mrset) { return SET.insert(pos, mrset); }
  template<typename InputIterator> void insert(iterator pos, InputIterator first, InputIterator last) { SET.insert(pos, first, last); }
  iterator erase(iterator pos) { return SET.erase(pos); }
  iterator erase(iterator first, iterator last) { return SET.erase(first, last); }
  void push_back(const value_type& mrset) { SET.push_back(mrset); }
  void pop_back() { SET.pop_back(); }
  void increment() { SET.push_back(mr_set()); }
  void clear() { SET.clear(); }
  void resize(int i) { SET.resize(i); }

  // Methods
  void sort_LLG();
  void calc_contrast();
  void sort_TF();
  void unsort();
  floatType top_TF() const;
  floatType top_TFZ() const;
  floatType top_TFZeq() const;
  floatType top_packs_TFZ() const;
  int num_TFZ(floatType zsolved) const;
  bool homogeneous() const;
  af_float  tfz() const;
  af_float  tf() const;
  floatType top_LLG();
  floatType top_packs_LLG();
  mr_rlist top_rlist();
  std::vector<mr_gyre>  top_gyre();
  std::string unparse();
  std::string unparse_template();
  std::string logfile(int,bool=false);
  void check_init();
  void add(mr_rlist);
  void add(mr_gyre);
  void add(mr_ndim);
  void add(std::string);
  void add_history(std::string);
  void add_cluster(int);
  void add_DRMS(std::string,floatType);
  void add_CELL(std::string,floatType);
  void add_hall(std::string);
  stringset set_of_modlids();
  std::map<std::string,int> map_of_modlids();
  void convert_vrms_cell_to_modlids_with_numbers();
  std::string last_modlid();
  void add(bool one_mol_in_P1,const mr_set&,unsigned,unsigned,mr_rlist&,dvect31D,float1D,float1D,int1D,data_bofac,data_tncs);
  void incNROT();
  bool is_rlist();
  llg_cutoff purge_llg(floatType,int,data_zscore);
  floatType purge_tf(floatType,int,data_zscore);
  const mr_solution& purge_tfz(data_zscore&);
  std::vector<delel>  purge_rf(floatType,floatType,int);
  bool has_vrms();
  bool has_solp();
  bool has_cell();
  void fix_all(bool);
  mr_solution set_as_sol(int);
  int num_rlist() const;
  int num_in_rlist(int) const;
  void setNROT(int);
  int num_tfz_rlist(int,floatType) const;
  mr_solution rlist_zscore(int,floatType);
  void apply_space_group_expansion(af_string);
  void if_not_set_apply_space_group(std::string);
  bool all_space_groups_the_same();
  bool all_space_groups_set();
  void pruneDuplicateSolutions();
  void set_as_rescored(bool);
  bool has_been_rescored() const;
  void set_purge_mean(floatType);
  floatType get_purge_mean() const;
  void set_resolution(floatType);
  floatType get_resolution() const;
  void can_lower_resolution(bool);
  bool can_lower_resolution() const;
  void set_deep_used(bool);
  bool get_deep_used() const;
  int  ndeep() const;
  int  nknown() const;
  int  not_ndeep() const;
  void num_over(floatType ,floatType ,int& ,int& ,int&);
  floatType max_cell_scale();
  void set_PACKS(bool);
  void set_CELL_SCALE(double);
  void set_all_packs(bool);
  bool has_reject();
  double resolution() { return HIRES; }
  bool check_vrms_all_the_same();
  std::string setup_vrms_delta(std::map<std::string,drms_vrms>);
  void        setup_newvrms(std::map<std::string,drms_vrms>);
  int      number_of_unique_ensembles();
  string1D unique_ensembles();
  int1D    number_of_copies_of_unique_ensembles();
  void prune_by_packing();
  void clear_cluster();
  void clear_clash();
  bool has_been_packed(double);
  bool has_been_background_packed(double);
  void setup_rlist(std::string,double,double,double);
  void setup_known(std::string,double,double,double,double,double,double);
  void clear_real_occupancies();
  std::string top_logfile();
};

} //phaser

#endif
