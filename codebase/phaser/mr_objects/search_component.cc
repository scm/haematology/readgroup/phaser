#include <phaser/mr_objects/search_component.h>

namespace phaser {

  void search_component::set_modlid(af_string modlid)
  {
    MODLID.clear();
    for (int m = 0; m < modlid.size(); m++) MODLID.push_back(modlid[m]);
  }

  af_string search_component::af_MODLID() const
  {
    af_string tmp;
    for (int m = 0; m < MODLID.size(); m++) tmp.push_back(MODLID[m]);
    return tmp;
  }

  std::string search_component::logModlid() const
  {
    std::string tmp;
    for (int m = 0; m < MODLID.size(); m++) tmp += MODLID[m] + " ";
    return tmp;
  }

}
