#ifndef __PHASER_ID2RMS__
#define __PHASER_ID2RMS__
#include <math.h>
#include <utility>
#include <phaser_defaults.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class rms_estimate
{
  std::string ESTIMATOR;
  public:
  rms_estimate(std::string ESTIMATOR_="") : ESTIMATOR(ESTIMATOR_)
  {
    error = false;
    vrms_perturb = estimator_floor = estimator_sigma = 0;
    estimator = &rms_estimate::oeffner;
    ESTIMATOR = stoup(ESTIMATOR);
    if (!ESTIMATOR.size()) ESTIMATOR = std::string(DEF_ENSE_ESTI);
    if (ESTIMATOR == "CHOTHIALESK")
    {
      estimator = &rms_estimate::chothia_and_lesk;
      estimator_floor = 0.8;
    }
    else if (!ESTIMATOR.find("OEFFNER") || !ESTIMATOR.find("TEST"))
    {
      estimator_floor = 0.362426;
      estimator_sigma = 0.1965;
      A = 0.0568808;
      B =  172.53;
      C = 1.52198;
      if (!ESTIMATOR.find("OEFFNERHI")) vrms_perturb = 1;
      if (!ESTIMATOR.find("OEFFNERLO")) vrms_perturb = -1;
    }
    else if (ESTIMATOR == "LLGSUM")
    {
      A = 0.05588;
      B = 154.9;
      C = 1.469;
    }
    else if (ESTIMATOR == "LLGLSQ")
    {
      A = 0.06683;
      B = 212.3;
      C = 1.0729;
    }
    else if (ESTIMATOR == "LLGWLSQ")
    {
      A = 0.1178;
      B = 164.9;
      C = 0.6185;
    }
    else error = true;
  }

  double rms(double,int);
  std::pair<double,double> min_max_rms(double,double);
  double floor();
  double sigma();

  double (rms_estimate::*estimator)(const double&,int);
  bool   error;

  private:
    double chothia_and_lesk(const double&, int);
    double oeffner(const double&, int);
    double llgsum(const double&, int);
    double llglsq(const double&, int);
    double llgwlsq(const double&, int);

    double estimator_floor;
    double estimator_sigma;
    double vrms_perturb;
    double A,B,C;
};

}

#endif
