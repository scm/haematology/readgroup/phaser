//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_TRACE_Class__
#define __DATA_TRACE_Class__
#include <phaser_defaults.h>
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/data_model.h>

namespace phaser {

class data_trace
{
  public:
    data_trace();

    void set_data_trace(const data_trace& other)
    {
      SAMP_DISTANCE = other.SAMP_DISTANCE;
      SAMP_TARGET = other.SAMP_TARGET;
      SAMP_RANGE = other.SAMP_RANGE;
      SAMP_MIN =  other.SAMP_MIN;
      SAMP_USE = other.SAMP_USE;
      SAMP_WANG = other.SAMP_WANG;
      SAMP_ASA = other.SAMP_ASA;
      SAMP_NCYC = other.SAMP_NCYC;
      MODEL = other.MODEL;
    }

  public:
  double      SAMP_DISTANCE;
  int         SAMP_TARGET;
  int         SAMP_RANGE;
  double      SAMP_MIN;
  std::string SAMP_USE; //"",CALPHA,HEXGRID
  double      SAMP_WANG;
  double      SAMP_ASA;
  int         SAMP_NCYC;
  data_model  MODEL;

  bool set_use(std::string);
};

} //phaser

#endif
