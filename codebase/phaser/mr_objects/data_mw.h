//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_Class__
#define __DATA_Class__
#include <phaser_defaults.h>
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class data_mw
{
  public:
    data_mw(
       double PROTEIN_ = 0,
       double NUCLEIC_ = 0,
       double HETATOM_ = 0
       ) :
     PROTEIN(PROTEIN_),
     NUCLEIC(NUCLEIC_),
     HETATOM(HETATOM_)
    { }

  double  PROTEIN;
  double  NUCLEIC;
  double  HETATOM;

  int protein_nres_estimate_from_mw()  { return std::floor(PROTEIN/double(DEF_MW_PER_RES)); }
  int nucleic_nbase_estimate_from_mw() { return std::floor(NUCLEIC/double(DEF_MW_PER_BASE)); }

  double total_mw()
  { return (PROTEIN + NUCLEIC + HETATOM); }

  void set_data_mw(const data_mw& init)
  {
    PROTEIN = init.PROTEIN;
    NUCLEIC = init.NUCLEIC;
    HETATOM = init.HETATOM;
  }

};

} //phaser

#endif
