//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/mr_objects/mr_clash.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

bool mr_matrix_clash::one_bad(bool1D included)
{
  if (!included.size()) included.resize(cmatrix.size(),true);
  if (included.size() != cmatrix.size())
  PHASER_ASSERT(included.size() == cmatrix.size());
  for (int s_ref = 0; s_ref < cmatrix.size(); s_ref++)
  for (int s_chk = 0; s_chk < cmatrix[s_ref].size(); s_chk++)
  if (included[s_ref] && included[s_chk])
  {
    double percent_bad(cmatrix[s_ref][s_chk]/trace_length[s_chk]);
    bool bad(percent_bad > trace_percent[s_ref]);
    if (bad) return true;
  }
  return false;
}

double mr_matrix_clash::worst_clash()
{
  double percent_bad_max = 0;
  //matrix not symmetric!
  for (int s_ref = 0; s_ref < cmatrix.size(); s_ref++)
  for (int s_chk = 0; s_chk < cmatrix[s_ref].size(); s_chk++)
  {
    double percent_bad(cmatrix[s_ref][s_chk]/trace_length[s_chk]);
    if (percent_bad > percent_bad_max)
    {
      percent_bad_max = percent_bad;
      ibad = s_ref;
      jbad = s_chk;
    }
  }
  //round to 0.01
  return percent_bad_max;
}

bool mr_matrix_clash::one_excessive()
{
  for (int s_ref = 0; s_ref < cmatrix.size(); s_ref++)
  for (int s_chk = 0; s_chk < cmatrix[s_ref].size(); s_chk++)
  {
    double percent_bad(cmatrix[s_ref][s_chk]/trace_length[s_chk]);
    if (percent_bad > 0.5)
    {
      ibad = s_ref;
      jbad = s_chk;
      return true;
    }
  }
  return false;
}

std::string mr_matrix_clash::unparse()
{
  std::string tmp;
  tmp += "sizes: matrix=" + itos(cmatrix.size()) + " trace length=" + itos(trace_length.size()) + " trace percent=" + itos(trace_percent.size()) + "\n";
  if (cmatrix.size())
  {
  tmp += "cmatrix:\n";
  for (int s_ref = 0; s_ref < cmatrix.size(); s_ref++)
    tmp += dvtos(cmatrix[s_ref]) + "\n";
  }
  if (trace_length.size()) tmp += "trace_length: " + dvtos(trace_length) + "\n";
  if (trace_percent.size()) tmp += "trace_percent: " + dvtos(trace_percent) + "\n";
  return tmp;
}

bool mr_matrix_clash::ok(int k)
{
  for (int s_ref = 0; s_ref < cmatrix.size(); s_ref++)
    if (cmatrix[s_ref].size() != cmatrix.size()) return false;
  PHASER_ASSERT(cmatrix.size() == k );
  PHASER_ASSERT(trace_length.size() == k );
  PHASER_ASSERT(trace_percent.size() == k );
  return (cmatrix.size() == k && trace_length.size() == k && trace_percent.size() == k);
}

bool mr_matrix_clash::has_been_packed(int knownsize,double perc)
{
  if (knownsize != trace_percent.size()) return false;
  for (int s_ref = 0; s_ref < trace_percent.size(); s_ref++)
    if (trace_percent[s_ref] > perc) return false;
  return true;
}

bool mr_matrix_clash::has_been_background_packed(int knownsize,double perc)
{
  if (trace_percent.size() < knownsize-1) return false;
  for (int s_ref = 0; s_ref < knownsize-1; s_ref++)
    if (trace_percent[s_ref] > perc) return false;
  return true;
}

void mr_matrix_clash::add_matrix(int n)
{
  PHASER_ASSERT(cmatrix.size() == n-1);
  for (unsigned s_ref = 0; s_ref < cmatrix.size(); s_ref++)
    cmatrix[s_ref].push_back(0);
  cmatrix.push_back(float1D(n,0));
  PHASER_ASSERT(cmatrix.size() == n);
  for (unsigned s_ref = 0; s_ref < cmatrix.size(); s_ref++)
    PHASER_ASSERT(cmatrix[s_ref].size() == n);
}

} //phaser
