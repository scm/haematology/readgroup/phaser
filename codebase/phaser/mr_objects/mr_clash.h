//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MRCLASH_Class__
#define __MRCLASH_Class__
#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class mr_closest
{
  public:
  double Sqr;
  dvect3 Trasym;
  dmat33 Rotsym;
  dvect3 CellTrans;
  bool   IsIdentity;

  public:
  mr_closest()
  {
    Sqr = std::numeric_limits<double>::max();
    Trasym = dvect3(0,0,0);
    Rotsym = dmat33(1,0,0,0,1,0,0,0,1);
    CellTrans = dvect3(0,0,0);
    IsIdentity = bool(false);
  }
};

class mr_matrix_clash
{
  private:
  float2D   cmatrix;
  float1D   trace_length;
  float1D   trace_percent;

  public:
  bool greater_than;
  int  ibad,jbad;

  public:
  mr_matrix_clash() { greater_than = false; ibad = jbad = 0; }
  void clear()
  {
    cmatrix.clear();
    trace_length.clear();
    trace_percent.clear();
    greater_than = false;
    ibad = jbad = 0;
  }
  void resize(int n)
  {
    cmatrix.resize(n);
    for (unsigned s_ref = 0; s_ref < n; s_ref++)
      cmatrix[s_ref].resize(n,0);
  }
  void add_trace_percent(double s)  { trace_percent.push_back(s); }
  void add_trace_length(int s)  { trace_length.push_back(s); }
  void inc(int s_chk,int s_ref) { cmatrix[s_chk][s_ref]++; }
  int  get(int s_chk,int s_ref) { return cmatrix[s_chk][s_ref]; }
  int  size()                   { if (cmatrix.size()) PHASER_ASSERT(cmatrix.size() == cmatrix[0].size()); return cmatrix.size(); }

  bool one_bad(bool1D=bool1D(0)); //arrays for selection, for amalgamation
  double worst_clash();
  bool one_excessive();
  std::string unparse();
  bool  ok(int);
  bool  has_been_packed(int,double);
  bool  has_been_background_packed(int,double);
  void  add_matrix(int);
};

} //phaser

#endif
