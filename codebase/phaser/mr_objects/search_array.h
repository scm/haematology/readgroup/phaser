//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __AUTO_SEARCH_Class__
#define __AUTO_SEARCH_Class__
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/search_component.h>
#include <phaser/pod/llg_hires.h>

namespace phaser {

inline  bool search_order_ellg_comp(const search_component& m1, const search_component& m2)
{ return (m1.eLLG > m2.eLLG); }

class search_array
{
  public:
  std::vector<search_component> SET; //the list of alternative MODLIDs ("OR" search)

  std::string unparse()
  {
    std::string Card;
    for (int i = 0; i < SET.size(); i++) Card += SET[i].unparse();
    return Card;
  }

  public:
  search_array() { SET.clear();  }
  search_array(af_string SEARCH_MODLID)
  {
    SET.resize(1);
    for (int r = 0; r < SEARCH_MODLID.size(); r++)
      SET[0].MODLID.push_back(SEARCH_MODLID[r]);
  }
  search_array(std::vector<search_component> const& SET_) : SET(SET_) {}

  std::vector<search_component>  set_of_components(int=0) ;
  std::map<search_component,int> map_of_components() const;
  std::map<std::string,int>      map_of_modlids() const;
  std::set<std::string>          set_of_modlids() const;
  int          num_unique(int c) const;
  bool         not_an_OR_search() const;
  int1D        nmols() const;
  search_array remaining(int) const;
  int          remaining(int,std::string) const;
  void         set_modlid(int,search_component);
  void         set_modlid_num(search_array,int);
  string2D     getModlidStar();
  void         sort_ellg()     { std::sort(SET.begin(),SET.end(),search_order_ellg_comp); }
  void         sort_ellg(std::map<search_component,llg_hires>,int=0);
  void         edit_tncs(int);

  //------------------------------
  typedef std::vector<search_component>::value_type value_type;
  typedef std::vector<search_component>::size_type size_type;
  typedef std::vector<search_component>::difference_type difference_type;
  typedef std::vector<search_component>::reference reference;
  typedef std::vector<search_component>::const_reference const_reference;
  typedef std::vector<search_component>::iterator iterator;
  typedef std::vector<search_component>::const_iterator const_iterator;

  template<typename InputIterator>
  search_array(InputIterator first, InputIterator last) : SET( first, last ) {}
  template<typename InputIterator>
  void insert(iterator pos, InputIterator first, InputIterator last) { SET.insert(pos, first, last); }

  const_reference operator[](size_type t) const { return SET[t]; }
  reference operator[](size_type t) { return SET[t]; }
  iterator begin() { return SET.begin(); }
  iterator end() { return SET.end(); }
  const_iterator begin() const { return SET.begin(); }
  const_iterator end() const { return SET.end(); }
  size_type size() const { return SET.size(); }
  iterator insert(iterator pos, const value_type& v) { return SET.insert(pos, v); }
  iterator erase(iterator pos) { return SET.erase(pos); }
  iterator erase(iterator first, iterator last) { return SET.erase(first, last); }
  void push_back(const value_type& v) { SET.push_back(v); }
  void pop_back() { SET.pop_back(); }
  void clear() { SET.clear(); }
  void resize(int s) { SET.resize(s);  }
  const value_type& back() { return SET.back(); }
};

} //phaser

#endif
