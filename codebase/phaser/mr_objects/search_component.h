//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __AUTO_SEARCH_COMPONENT_Class__
#define __AUTO_SEARCH_COMPONENT_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class search_component
{
  private:
  std::string star;

  public:
  string1D    MODLID;
  double      eLLG;

  std::string unparse()
  {
    std::string Card;
    Card += dtos(eLLG) + " " + star + " ";
    for (int i = 0; i < MODLID.size(); i++) Card += MODLID[i] + " " ;
    return Card;
  }

  public:
  search_component() { eLLG = 0; star = " "; }
  void        set_modlid(af_string);
  af_string   af_MODLID() const;
  std::string logModlid() const;
  bool        found() const { return (star == "+" || star == "&"); }
  void        set_star_found()  { star = "+"; }
  void        set_star_search() { star = "*"; }
  void        set_star_append() { star = "&"; }
  void        set_star_clear()  { star = ""; }
  std::string star_str() const  { return star; }

  //operators only use MODLID for comparisons
  bool operator<(const search_component &rhs) const
  {
    for (int m = 0; m < std::min(MODLID.size(),rhs.MODLID.size()); m++)
      if (MODLID[m] < rhs.MODLID[m]) return true;
    return false; //same
  }

  bool operator==(const search_component& rhs) const
  {
    if (MODLID.size() != rhs.MODLID.size()) return false;
    for (int m = 0; m < MODLID.size(); m++) if (MODLID[m] != rhs.MODLID[m]) return false;
    return true;
  }

  bool operator!=(const search_component& rhs) const
  {
    if (MODLID.size() != rhs.MODLID.size()) return true;
    for (int m = 0; m < MODLID.size(); m++) if (MODLID[m] != rhs.MODLID[m]) return true;
    return false;
  }
};

} //phaser

#endif
