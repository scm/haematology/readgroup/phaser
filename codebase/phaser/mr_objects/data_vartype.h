//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DATA_VARTYPE_Class__
#define __DATA_VARTYPE_Class__
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class data_vartype
{
  private:
    std::string VARTYPE;

  public:
    double      RMSID;

    data_vartype(std::string VARTYPE_="",double RMSID_=0)
    {
      set_vartype(VARTYPE_);
      set_rmsid(RMSID_);
    }

    bool   is_set() const   { return VARTYPE != ""; }
    bool   is_rms() const   { return VARTYPE == "RMS"; }
    bool   is_id() const    { return VARTYPE == "ID"; }
    bool   is_card() const  { return VARTYPE == "CARD"; }

    const std::string& vartype() const { return VARTYPE; }
    const double& rmsid() const        { return RMSID; }

    void set_vartype(std::string VARTYPE_)
    {
      VARTYPE = VARTYPE_;
      PHASER_ASSERT(VARTYPE == "" ||
                    VARTYPE == "RMS" ||
                    VARTYPE == "ID" ||
                    VARTYPE == "CARD");
    }

    void set_rmsid(double RMSID_)
    {
      RMSID = RMSID_;
      PHASER_ASSERT(RMSID >= 0);
    }

    std::string variance()
    {
      if (is_rms()) return " RMS " + dtos(RMSID);
      if (is_id())  return " ID " + dtos(RMSID);
      return " CARD ON";
    }

};

} //phaser

#endif
