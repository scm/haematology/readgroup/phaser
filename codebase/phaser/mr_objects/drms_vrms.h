//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DRMS_VRMS_Class__
#define __PHASER_DRMS_VRMS_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class drms
{
  public:
  double upper,lower;

  drms(double upper_=0,
       double lower_=0) :
    upper(upper_),
    lower(lower_)
  { }

  double initial()
  {
    floatType delta(0);
    delta= std::min(upper,delta);
    delta= std::max(lower,delta);
    return delta;
  }

};

class vrms
{
  public:
  double fixed,upper,lower;
  double lower_rms_limit,upper_rms_limit;

  vrms(double fixed_=0,
       double upper_=0,
       double lower_=0,
       double lower_rms_limit_=0,
       double upper_rms_limit_=0):
    fixed(fixed_),
    upper(upper_),
    lower(lower_),
    lower_rms_limit(lower_rms_limit_),
    upper_rms_limit(upper_rms_limit_)
  { }

};

class drms_vrms
{
  public:
  std::vector<std::vector<vrms> >  VRMS;
  std::vector<std::vector<int> >   forward_lookup;
  std::vector<std::pair<int,int> > reverse_lookup;

  drms DRMS;

  //always init with size [0][0]
  drms_vrms() { VRMS.clear(); }
  void init_vrms_single()
  {
    VRMS.resize(1); VRMS[0].resize(1);
    forward_lookup.resize(1); forward_lookup[0].resize(1,0);
    reverse_lookup.resize(1,std::pair<int,int>(0,0));
  }

  void resize_vrms(int nMols)
  {
    VRMS.clear();
    reverse_lookup.clear();
    forward_lookup.clear();
    VRMS.resize(nMols);
    forward_lookup.resize(nMols);
  }

  void resize_vrms(int ipdb,int nmodels)
  {
    VRMS[ipdb].resize(nmodels);
    forward_lookup[ipdb].resize(nmodels);
    for (unsigned m = 0; m < nmodels; m++)
    {
      forward_lookup[ipdb][m] = reverse_lookup.size();
      reverse_lookup.push_back(std::pair<int,int>(ipdb,m));
    }
  }

  typedef std::vector<vrms>::value_type value_type;
  typedef std::vector<vrms>::size_type size_type;
  typedef std::vector<vrms>::difference_type difference_type;
  typedef std::vector<vrms>::reference reference;
  typedef std::vector<vrms>::const_reference const_reference;
  typedef std::vector<vrms>::iterator iterator;
  typedef std::vector<vrms>::const_iterator const_iterator;

  size_type size() const { return reverse_lookup.size(); }

  const_reference operator[](size_type t) const
  { return VRMS[reverse_lookup[t].first][reverse_lookup[t].second]; }
  reference operator[](size_type t)
  { return VRMS[reverse_lookup[t].first][reverse_lookup[t].second]; }

  float1D fixed_vrms_array()
  {
    float1D tmp;
    for (int i = 0; i < size(); i++)
      tmp.push_back((*this)[i].fixed);
    return tmp;
  }
  float1D lower_vrms_array()
  {
    float1D tmp;
    for (int i = 0; i < size(); i++)
      tmp.push_back((*this)[i].lower);
    return tmp;
  }
  float1D upper_vrms_array()
  {
    float1D tmp;
    for (int i = 0; i < size(); i++)
      tmp.push_back((*this)[i].upper);
    return tmp;
  }
  double min_upper()
  {
    double tmp = std::numeric_limits<double>::max();
    for (int i = 0; i < size(); i++)
      tmp = std::min(tmp,(*this)[i].upper);
    return tmp;
  }
  double max_lower()
  {
    double tmp = -std::numeric_limits<double>::max();
    for (int i = 0; i < size(); i++)
      tmp = std::max(tmp,(*this)[i].lower);
    return tmp;
  }

  void setup_drms_limits();
  void setup_vrms_limits();
};


float1D newvrms(double,float1D);

} //phaser

#endif
