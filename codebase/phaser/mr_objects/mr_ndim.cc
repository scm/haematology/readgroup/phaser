//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/mr_objects/mr_ndim.h>
#include <phaser_defaults.h>

namespace phaser {

    std::string mr_ndim::getSOLU(int w1,int d1,int w2,int d2) const
    {
      std::string Card = "";
      dvect3 euler = matrix2eulerDEG(R);
      std::string modlid = MODLID;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      Card += "SOLU 6DIM ENSE " + modlid + " EULER " + dvtos(euler,w1,d1)
        + std::string(FRAC ? " FRAC " : " ORTH ") + dvtos(TRA,w2,d2) + " BFAC " + dtos(BFAC,w2,d2);
      if (MULT > 1) Card += " MULT " + itos(MULT);
      if (OFAC != 1) Card += " OFAC " + dtos(OFAC);
      if (TFZeq != 0) Card += " #TFZ==" + dtos(TFZeq,1);
      return Card;
    }

    std::string mr_ndim::short_logfile() const
    {
      dvect3 euler = matrix2eulerDEG(R);
      std::string modlid = MODLID;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      return "ENSEMBLE " + modlid + " EULER " + dvtos(euler,3) + " FRAC " + dvtos(TRA,3);
    }

    std::string mr_ndim::logfile(int depth) const
    {
      std::string tab(DEF_TAB*depth,' ');
      std::string text = getSOLU(6,1,5,2); // 6.1f,5.2f
      return tab + text + "\n";
    }

} //phaser
