//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MR_Class__
#define __PHASER_MR_Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>
#include <cstdio>

namespace phaser {

class mr_ndim
{
  public:
    std::string MODLID;
    dmat33      R; //the rotation matrix
    bool        FRAC; //whether or not the coordinates are fractional
    bool        inFRAC; //whether or not the coordinates are input as fractional
    dvect3      TRA; //this can be either frac or orth, only use accessors
    floatType   BFAC; //The B-factor
    bool        FIXR;
    bool        FIXT;
    bool        FIXB;
    int         MULT;
    double      TFZeq;
    double      OFAC; //The Occupancy-factor

  public:
    mr_ndim(
      std::string const& MODLID_="",
      dmat33 const& R_=dmat33(1,0,0,0,1,0,0,0,1),
      bool const& FRAC_=true,
      bool const& inFRAC_=true,
      dvect3 const& TRA_=dvect3(0,0,0),
      double const& BFAC_=0,
      bool const& FIXR_=false,
      bool const& FIXT_=false,
      bool const& FIXB_=false,
      int  const& MULT_=1,
      double const& OFAC_=1)
    :
      MODLID(MODLID_),
      R(R_),
      FRAC(FRAC_),
      inFRAC(inFRAC_),
      TRA(TRA_),
      BFAC(BFAC_),
      FIXR(FIXR_),
      FIXT(FIXT_),
      FIXB(FIXB_),
      MULT(MULT_),
      OFAC(OFAC_)
    {
      TFZeq = 0;
    }

    mr_ndim(
      std::string const& MODLID_,
      dvect3 const& EULER_,
      bool const& FRAC_=true,
      dvect3 const& TRA_=dvect3(0,0,0),
      double const& BFAC_=0,
      double const& OFAC_=1,
      bool const& FIXR_=false,
      bool const& FIXT_=false,
      bool const& FIXB_=false,
      int const& MULT_=1)
    :
      MODLID(MODLID_),
      FRAC(FRAC_),
      inFRAC(FRAC_),
      TRA(TRA_),
      BFAC(BFAC_),
      FIXR(FIXR_),
      FIXT(FIXT_),
      FIXB(FIXB_),
      MULT(MULT_),
      OFAC(OFAC_)
    {
      R = euler2matrixDEG(EULER_);
      TFZeq = 0;
    }

    std::string getSOLU(int,int,int,int) const;
    std::string logfile(int depth=0) const;
    std::string short_logfile() const;

    void          setFracT(dvect3 t) { FRAC = true; TRA = t; }
    void          addT(dvect3 tncsvec) { TRA += tncsvec; }

    void          fix_all(bool fix) { FIXR = FIXT = FIXB = fix; }
    dvect3        getEuler() const { return matrix2eulerDEG(R); }
    floatType     getEulerAng(int t) const { return (t == 0 || t == 1 || t == 2)? matrix2eulerDEG(R)[t] : 0; }
    floatType     getCoord(int t) const { return (t == 0 || t == 1 || t == 2)? TRA[t] : 0; }
    std::string   unparse() const { return getSOLU(8,3,7,5); } //8.3f,7.5f

    int operator==(const mr_ndim& right) const
    {
      if (MODLID != right.MODLID) return false;
      if (TRA != right.TRA) return false;
      if (FRAC != right.FRAC) return false;
      if (inFRAC != right.inFRAC) return false;
      if (R != right.R) return false;
      if (FIXR != right.FIXR) return false;
      if (FIXT != right.FIXT) return false;
      if (BFAC != right.BFAC) return false;
      if (OFAC != right.OFAC) return false;
      if (MULT != right.MULT) return false;
      return true;
    }

    int operator!=(const mr_ndim& right) const
    {
      if (TRA != right.TRA) return true;
      if (FRAC != right.FRAC) return true;
      if (inFRAC != right.inFRAC) return true;
      if (R != right.R) return true;
      if (FIXR != right.FIXR) return true;
      if (FIXT != right.FIXT) return true;
      if (MODLID != right.MODLID) return true;
      if (BFAC != right.BFAC) return true;
      if (OFAC != right.OFAC) return true;
      if (MULT != right.MULT) return true;
      return false;
    }
};

} //phaser

#endif
