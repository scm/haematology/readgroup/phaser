//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/drms_vrms.h>

namespace phaser {

float1D newvrms(double DRMS,float1D vrms)
{
  float1D tmp;
  for (int i = 0; i < vrms.size(); i++)
    tmp.push_back((DRMS == 0) ? vrms[i] : std::sqrt(fn::pow2(vrms[i]) + DRMS));
  return tmp;
}

  void drms_vrms::setup_drms_limits()
  {
    if (!VRMS.size() || !VRMS[0].size()) return;
    //set DRMS upper and lower from VRMS fixed and RMS upper and lower limits
    DRMS.upper = fn::pow2(VRMS[0][0].upper_rms_limit) - fn::pow2(VRMS[0][0].fixed);
    DRMS.lower = fn::pow2(VRMS[0][0].lower_rms_limit) - fn::pow2(VRMS[0][0].fixed);
    for (int ipdb = 0; ipdb < VRMS.size(); ipdb++)
    for (int m = 0; m < VRMS[ipdb].size(); m++)
    {
      DRMS.upper = std::min(DRMS.upper,fn::pow2(VRMS[ipdb][m].upper_rms_limit) - fn::pow2(VRMS[ipdb][m].fixed));
      DRMS.lower = std::max(DRMS.lower,fn::pow2(VRMS[ipdb][m].lower_rms_limit) - fn::pow2(VRMS[ipdb][m].fixed));
    }
  }

  void drms_vrms::setup_vrms_limits()
  {
    //set VRMS upper and lower from VRMS fixed and DRMS limits
    for (int ipdb = 0; ipdb < VRMS.size(); ipdb++)
    for (int m = 0; m < VRMS[ipdb].size(); m++)
    {
      VRMS[ipdb][m].upper =  std::sqrt(fn::pow2(VRMS[ipdb][m].fixed) + DRMS.upper);
      VRMS[ipdb][m].lower =  std::sqrt(fn::pow2(VRMS[ipdb][m].fixed) + DRMS.lower);
    }
  }

} //phaser
