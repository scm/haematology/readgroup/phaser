//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/mr_objects/mr_set.h>
#include <phaser/include/space_group_name.h>

namespace phaser {

std::string mr_set::unparse()
{
  std::string Card;
  if (HISTORY.size())
  Card += "SOLU HISTORY " + HISTORY + "\n";
  space_group_name sg(HALL,true);
  if (HALL.size())
    Card += !sg.error ? "SOLU SPAC " + sg.CCP4 + "\n":
                        "SOLU SPAC HALL " + HALL + "\n"; //unconventional
  if (KNOWN.size())
  {
    for (int k = 0; k < KNOWN.size()-1 ; k++)
      Card += KNOWN[k].unparse() + "\n";
    Card += KNOWN.back().unparse() + std::string(NCLUS ? " # CLUSTER " + itos(NCLUS): "") + "\n";
  }
  for (int k = 0; k < GYRE.size() ; k++)
    Card += GYRE[k].unparse_sol();
  for (int e = 0; e < RLIST.size() ; e++)
    Card += RLIST[e].unparse(); //DEEP recorded in comments at end of line
  stringset modlids = set_of_modlids();
  if (!GYRE.size()) //VRMS not relevant for GYRE refinement
  for (map_str_float_iter iter = DRMS.begin(); iter != DRMS.end() ; iter++)
  if (modlids.find(iter->first) != modlids.end()) //only if relevant
  {
    std::string modlid = iter->first;
    modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
    modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
    if (iter->second != 0.) //default vrms delta
      Card += "SOLU ENSEMBLE " + modlid + " VRMS DELTA " + dtos(iter->second,6,4,true) + " #RMSD " + dvtos(VRMS[iter->first],5,2) + " #VRMS " + dvtos(NEWVRMS[iter->first],5,2) + "\n";
  }
  if (DATACELL != 1)
  {
    Card += "SOLU CELL SCALE " + dtos(DATACELL,7,5) + "\n";
  }
  else
  {
    for (map_str_float_iter iter = CELL.begin(); iter != CELL.end(); iter++)
    if (modlids.find(iter->first) != modlids.end()) //only if relevant
    {
      std::string modlid = iter->first;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      if (iter->second != 1.) //default cell scale
        Card += "SOLU ENSEMBLE " + modlid + " CELL SCALE " + dtos(iter->second,7,5) + "\n";
    }
  }
  if (!PACKS) Card += "SOLU PACKS FALSE\n";
   //remove last char if it is eol
  if (Card.find_last_of("\n") != std::string::npos) Card.erase(Card.find_last_of("\n"));
  if (Card.size()) Card = "SOLU SET" + ANNOTATION + "\n" + Card;
  return Card;
}

std::string mr_set::short_annotation(int max_len,int depth)
{
  std::string tab(3*depth,' ');
  std::string ann = ANNOTATION;
  int ann_len = ann.size();
  while (ann[0] == ' ') { ann = ann.substr(1); }
  if (ann_len <= max_len) return (tab + ann);
  int b(0);
  for (int a = 0; a < ann.size(); a++) if (ann[a] == ' ' && ann_len-a <= max_len ) { b = a; break; }
  return (tab + "..." + ann.substr(b));
}

std::string mr_set::logfile(int depth,bool full_annotation,bool print_solutions)
{
  std::string tab(DEF_TAB*depth,' ');
  std::string Card;
  if (full_annotation)
  {
    std::string annotation("SOLU SET ");
    std::string text(ANNOTATION);
    int tab_max_line_width(DEF_WIDTH-tab.size());
    do
    {
      if (text.size() > tab_max_line_width-annotation.size())
      {
        std::string rawline(text,0,tab_max_line_width-annotation.size());
        if (rawline.rfind(" ") != std::string::npos)
        {
          Card += tab + annotation + std::string(rawline,0,rawline.rfind(" ")) + "\n";
          text.erase(0,rawline.rfind(" "));
        }
        else
        {
          Card += tab + annotation + rawline + "\n";
          text.clear();
        }
      }
      else
      {
        Card += tab + annotation + text + "\n";
        text.clear();
      }
      annotation = "";
    }
    while (text.size());
  }
  if (print_solutions)
  {
    space_group_name sg(HALL,true);
    if (HALL.size())
      Card += !sg.error ? tab + "SOLU SPAC " + sg.CCP4 + "\n":
                          tab + "SOLU SPAC HALL " + HALL + "\n"; //unconventional
    for (int k = 0; k < KNOWN.size(); k++)
      Card += KNOWN[k].logfile(depth);
    for (int k = 0; k < GYRE.size(); k++)
      Card += GYRE[k].logfile(depth);
    for (int e = 0; e < RLIST.size() ; e++)
      Card += RLIST[e].unparse(depth) + "\n";
    stringset modlids = set_of_modlids();
    if (!GYRE.size()) //VRMS not relevant for GYRE refinement
    for (map_str_float_iter iter = DRMS.begin(); iter != DRMS.end() ; iter++)
    if (modlids.find(iter->first) != modlids.end()) //only if relevant
    if (iter->second != 0.) //default vrms delta
    {
      std::string modlid = iter->first;
      modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
      modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
      std::string annotation("SOLU ENSEMBLE ");
      std::string text = modlid + " VRMS DELTA " + dtos(iter->second,6,4,true) + " #RMSD " + dvtos(VRMS[iter->first],5,2) + " #VRMS " + dvtos(NEWVRMS[iter->first],5,2);
      Card += tab + annotation + text + "\n";
    }
    if (DATACELL != 1)
    {
      Card += tab + "SOLU CELL SCALE " + dtos(DATACELL,6,4) + "\n";
    }
    else
    {
      for (map_str_float_iter iter = CELL.begin(); iter != CELL.end(); iter++)
      if (modlids.find(iter->first) != modlids.end()) //only if relevant
      {
        std::string modlid = iter->first;
        modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
        modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
        if (iter->second != 1.) //default cell scale
          Card += tab + "SOLU ENSEMBLE " + modlid + " CELL SCALE " + dtos(iter->second,6,4) + "\n";
      }
    }
    if (!PACKS) Card += tab + "SOLU PACKS FALSE\n";
  }
  //remove last char if it is eol
  while (Card.find_last_of("\n") == Card.size()-1) { Card.erase(Card.find_last_of("\n")); }
  if (Card.find("SOLU SET ") == Card.size()-9) return ""; //tabs on the front
  return Card;
}

std::string mr_set::unparse_template()
{
  std::string Card;
  Card += "SOLU TEMPLATE" + ANNOTATION + "\n";
  for (int k = 0; k < KNOWN.size() ; k++)
    Card += KNOWN[k].unparse() + "\n";
   //remove last char if it is eol
  if (Card.find_last_of("\n") != std::string::npos) Card.erase(Card.find_last_of("\n"));
  return Card;
}

stringset mr_set::set_of_modlids()
{
  stringset m;
  for (int k = 0; k < KNOWN.size(); k++) m.insert(KNOWN[k].MODLID);
  for (int e = 0; e < RLIST.size(); e++) m.insert(RLIST[e].MODLID);
  for (int p = 0; p < GYRE.size(); p++) m.insert(GYRE[p].MODLID);
  return m;
}

stringset mr_set::set_of_modlids_with_numbers()
{
  stringset m;
  for (int k = 0; k < KNOWN.size(); k++) m.insert(KNOWN[k].MODLID+brackets(k));
  return m;
}

std::string mr_set::getSpaceGroupName()
{
  space_group_name sg(HALL,true);
  return sg.CCP4;
}

std::string mr_set::getSpaceGroupHall()
{
  return HALL;
}

int mr_set::Z()
{
  map_str_int tmp;
  for (int k = 0; k < KNOWN.size(); k++) tmp[KNOWN[k].MODLID] = 0;
  for (int k = 0; k < KNOWN.size(); k++) tmp[KNOWN[k].MODLID]++;
  int max_num(0);
  for (map_str_int::iterator iter = tmp.begin(); iter != tmp.end(); iter++)
    max_num = std::max(iter->second,max_num);
  return max_num;
}

int mr_set::getNDEEP() const
{
  for (int e = 0; e < RLIST.size(); e++)
    if (RLIST[e].DEEP) return e; //return last one that
  return RLIST.size();
}

bool mr_set::has_mult()
{
  for (int k = 0; k < KNOWN.size() ; k++)
    if (KNOWN[k].MULT > 1) return true;
  return false;
}

map_str_float1D mr_set::modlid_occupancies()
{
  map_str_float1D thisOCC; //binary to real
  for (int s = 0; s < KNOWN.size(); s++)
    thisOCC[KNOWN[s].MODLID] = bool_to_real_occupancies(s);
  return thisOCC;
}

float1D mr_set::bool_to_real_occupancies(int s)
{
  if (s >= BOOLOCC.size()) return float1D(0);
  float1D thisOCC(BOOLOCC[s].size()); //binary to real
  for (int o = 0; o < BOOLOCC[s].size(); o++)
    thisOCC[o] = BOOLOCC[s][o] ? 1:0;
  return thisOCC;
}

void mr_set::real_to_bool_occupancies(double cutoff)
{
  BOOLOCC.resize(REALOCC.size());
  for (int s = 0; s < REALOCC.size(); s++)
  {
    BOOLOCC[s].resize(REALOCC[s].size());
    for (int a = 0; a < REALOCC[s].size(); a++)
    {
      BOOLOCC[s][a] = (REALOCC[s][a] >= cutoff);
    }
  }
}


void mr_set::setup_newvrms(std::map<std::string,drms_vrms> ensemble)
{ //store the input VRMS and NEWVRMS for python output
  for (std::map<std::string,drms_vrms>::iterator iter = ensemble.begin(); iter != ensemble.end(); iter++)
  if (DRMS.find(iter->first) != DRMS.end())
  {
    VRMS[iter->first] = iter->second.fixed_vrms_array();
    NEWVRMS[iter->first] = newvrms(DRMS[iter->first],iter->second.fixed_vrms_array());
  }
}

} //phaser
