//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/mr_comp.h>
#include <phaser/lib/between.h>
#include <phaser_defaults.h>

namespace phaser {

  void mr_solution::sort_LLG()
  {
    std::sort(SET.begin(),SET.end(),mrset_llg_comp);
    std::reverse(SET.begin(),SET.end());
  }

  void mr_solution::calc_contrast()
  {
    if (!PURGE_MEAN) return;
    double tol(0.1); //ignore values very close to mean, within error of LLG
    for (int t = 0; t < SET.size()-1; t++)
      if (SET[t+1].LLG > PURGE_MEAN+tol) //no negatives
        SET[t].CONTRAST = (SET[t].LLG-PURGE_MEAN)/(SET[t+1].LLG-PURGE_MEAN);
    SET.back().CONTRAST = 0;
  }

  void mr_solution::sort_TF()
  {
    std::sort(SET.begin(),SET.end(),mrset_tf_comp);
    std::reverse(SET.begin(),SET.end());
  }

  void mr_solution::unsort()
  {
    std::sort(SET.begin(),SET.end(),mrset_orig_num_comp);
  }

  floatType mr_solution::top_TF() const
  {
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++) top = std::max(SET[t].TF,top);
    return top;
  }

  floatType mr_solution::top_TFZ() const
  {
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++) top = std::max(SET[t].TFZ,top);
    return top;
  }

  std::string mr_solution::top_logfile()
  {
    std::string unparse = "";
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++)
    if (SET[t].TF > top)
    {
      top = SET[t].TF;
      unparse = SET[t].KNOWN.back().short_logfile() + " TF=" + dtos(top,1);
    }
    return unparse;
  }

  floatType mr_solution::top_packs_TFZ() const
  {
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++) if (SET[t].PACKS) top = std::max(SET[t].TFZ,top);
    return top;
  }


  floatType mr_solution::top_TFZeq() const
  {
    floatType top(-std::numeric_limits<floatType>::max());
    //top of TF and TFZeq, for phenix interface
    for (int t = 0; t < SET.size(); t++) top = std::max(SET[t].TFZ,top);
    for (int t = 0; t < SET.size(); t++) if (SET[t].TFZeq) top = std::max(SET[t].TFZeq,top);
    return top;
  }

  int mr_solution::num_TFZ(floatType zsolved) const
  {
    int count(0);
    for (int t = 0; t < SET.size(); t++) if (SET[t].TFZ > zsolved) count++;
    return count;
  }

  bool mr_solution::homogeneous() const
  {
    if (!SET.size()) return false;
    int l = SET[0].KNOWN.size()-1; //last entry
    //assume all the same size
    for (int t = 1; t < SET.size(); t++)
      if (SET[0].KNOWN[l].MODLID != SET[t].KNOWN[l].MODLID) return false;
    return true;
  }

  af_float mr_solution::tfz() const
  {
    af_float zscores(SET.size());
    for (int t = 0; t < SET.size(); t++)
      zscores[t] = SET[t].TFZ;
    return zscores;
  }

  af_float mr_solution::tf() const
  {
    af_float scores(SET.size());
    for (int t = 0; t < SET.size(); t++)
      scores[t] = SET[t].TF;
    return scores;
  }

  floatType mr_solution::top_LLG()
  {
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++) top = std::max(SET[t].LLG,top);
    return top;
  }

  floatType mr_solution::top_packs_LLG()
  {
    floatType top(-std::numeric_limits<floatType>::max());
    for (int t = 0; t < SET.size(); t++) if (SET[t].PACKS) top = std::max(SET[t].LLG,top);
    return top;
  }

  mr_rlist mr_solution::top_rlist()
  {
    double top(-std::numeric_limits<floatType>::max());
    mr_rlist toprlist;
    for (int t = 0; t < SET.size(); t++)
    for (unsigned e = 0; e < SET[t].RLIST.size(); e++)
    if (SET[t].RLIST[e].RF > top)
    {
      top = SET[t].RLIST[e].RF;
      toprlist = SET[t].RLIST[e];
    }
    return toprlist;
  }

  std::vector<mr_gyre> mr_solution::top_gyre()
  {
    double top(-std::numeric_limits<floatType>::max());
    std::vector<mr_gyre> topgyre;
    for (int t = 0; t < SET.size(); t++)
    if (SET[t].LLG > top)
    {
      top = SET[t].LLG;
      topgyre = SET[t].GYRE;
    }
    return topgyre;
  }

  std::string mr_solution::unparse()
  {
    std::string Card;
    //if this has come from a RF, then output the resolution of the RF for the TF step
    if (SET.size() && HIRES)
      Card += "SOLU RESOLUTION " + dtos(HIRES) + "\n";
    for (int t = 0; t < SET.size(); t++)
      Card += SET[t].unparse() + "\n";
     //remove last char if it is eol
    if (Card.find_last_of("\n") != std::string::npos) Card.erase(Card.find_last_of("\n"));
    return Card;
  }

  std::string mr_solution::unparse_template()
  {
    std::string Card;
    for (int t = 0; t < SET.size(); t++)
      Card += SET[t].unparse_template() + "\n";
     //remove last char if it is eol
    if (Card.find_last_of("\n") != std::string::npos) Card.erase(Card.find_last_of("\n"));
    return Card;
  }

  std::string mr_solution::logfile(int depth,bool full_annotation)
  {
    std::string Card;
    for (int t = 0; t < SET.size(); t++)
      Card += SET[t].logfile(depth,full_annotation) + "\n";
     //remove last char if it is eol
    if (Card.find_last_of("\n") != std::string::npos) Card.erase(Card.find_last_of("\n"));
    return Card;
  }

  void mr_solution::check_init()
  { if (!SET.size()) { mr_set tmp; SET.push_back(tmp); } }

  void mr_solution::add(mr_rlist rlist)
  { check_init(); SET.back().RLIST.push_back(rlist); }

  void mr_solution::add(mr_gyre gyre)
  { check_init(); SET.back().GYRE.push_back(gyre); }

  void mr_solution::add(mr_ndim known)
  { check_init(); SET.back().KNOWN.push_back(known); }

  void mr_solution::add(std::string annotation)
  { check_init(); SET.back().ANNOTATION = annotation; }

  void mr_solution::add_history(std::string history)
  { check_init(); SET.back().HISTORY = history; }

  void mr_solution::add_cluster(int nclus)
  { check_init(); SET.back().NCLUS = nclus; }

  void mr_solution::add_DRMS(std::string modlid,double DRMS)
  { check_init(); SET.back().DRMS[modlid] = DRMS; }

  void mr_solution::add_CELL(std::string modlid,floatType cell)
  { check_init(); SET.back().CELL[modlid] = cell; }

  void mr_solution::set_PACKS(bool packs)
  { check_init(); SET.back().PACKS = packs; }

  void mr_solution::set_CELL_SCALE(double scale)
  { check_init(); SET.back().DATACELL = scale; }

  void mr_solution::add_hall(std::string hall)
  { check_init(); SET.back().HALL = hall; }

  void mr_solution::set_all_packs(bool b)
  { for (int t = 0; t < SET.size(); t++) SET[t].PACKS=b; }

  bool mr_solution::has_reject()
  {
    for (int t = 0; t < SET.size(); t++)
      if (!SET[t].PACKS) return true;
    return false;
  }

  stringset mr_solution::set_of_modlids()
  {
    stringset m;
    for (int t = 0; t < SET.size(); t++)
    {
      for (int k = 0; k < SET[t].KNOWN.size(); k++) m.insert(SET[t].KNOWN[k].MODLID);
      for (int e = 0; e < SET[t].RLIST.size(); e++) m.insert(SET[t].RLIST[e].MODLID);
      for (int p = 0; p < SET[t].GYRE.size(); p++)  m.insert(SET[t].GYRE[p].MODLID);
    }
    return m;
  }

  std::map<std::string,int>  mr_solution::map_of_modlids()
  {
    std::map<std::string,int> m;
    for (int t = 0; t < SET.size(); t++)
    {
      for (int k = 0; k < SET[t].KNOWN.size(); k++) m[SET[t].KNOWN[k].MODLID] = 0;
      for (int e = 0; e < SET[t].RLIST.size(); e++) m[SET[t].RLIST[e].MODLID] = 0;
      for (int p = 0; p < SET[t].GYRE.size(); p++)  m[SET[t].GYRE[p].MODLID] = 0;
    }
    for (int t = 0; t < SET.size(); t++)
    {
      for (int k = 0; k < SET[t].KNOWN.size(); k++) m[SET[t].KNOWN[k].MODLID]++;
      for (int e = 0; e < SET[t].RLIST.size(); e++) m[SET[t].RLIST[e].MODLID]++;
      for (int p = 0; p < SET[t].GYRE.size(); p++)  m[SET[t].GYRE[p].MODLID]++;
    }
    return m;
  }

  void mr_solution::convert_vrms_cell_to_modlids_with_numbers()
  {
    for (int t = 0; t < SET.size(); t++)
    {
      map_str_float DRMS,CELL;
      for (int k = 0; k < SET[t].KNOWN.size(); k++)
      {
        std::string MODLID_brackets = SET[t].KNOWN[k].MODLID + brackets(k);
        if (SET[t].DRMS.find(SET[t].KNOWN[k].MODLID) != SET[t].DRMS.end())
          DRMS[MODLID_brackets] = SET[t].DRMS[SET[t].KNOWN[k].MODLID];
        if (SET[t].CELL.find(SET[t].KNOWN[k].MODLID) != SET[t].CELL.end())
          CELL[MODLID_brackets] = SET[t].CELL[SET[t].KNOWN[k].MODLID];
        SET[t].KNOWN[k].MODLID += brackets(k);
      }
      SET[t].DRMS = DRMS;
      SET[t].CELL = CELL;
    }
  }

  std::string mr_solution::last_modlid()
  {
    if (!SET.size()) return "";
    if (!SET[0].KNOWN.size()) return "";
    return SET[0].KNOWN[SET[0].KNOWN.size()-1].MODLID;
  }

  void mr_solution::add(bool one_mol_in_P1,const mr_set& mrset,unsigned k,unsigned e,mr_rlist& RLIST,dvect31D frac,float1D values,float1D zscores,int1D nclus,data_bofac SEARCH_FACTORS,data_tncs PTNCS)
 {
   PHASER_ASSERT(frac.size() == values.size());
   PHASER_ASSERT(frac.size() == zscores.size());
   for (int r = 0; r < frac.size(); r++)
   {
     //if RFZ zero, has been added because in known list, not from rotation function
     //std::string RFZstr = RLIST.RFZ ? std::string(" RF/RFZ=" + dtos(RLIST.RF,0) + "/" + dtos(RLIST.RFZ,1)) : " RF++";
     std::string RFZstr = RLIST.RFZ ? std::string(" RFZ=" + dtos(RLIST.RFZ,1)) : " RF++";
     if (RLIST.GRF) RFZstr = "";
     mr_set thisTrial = mrset;
     //std::string TFZstr = " TF/TFZ=" + std::string(one_mol_in_P1 ? "*" : dtos(values[r],0) + "/" +dtos(zscores[r],1));
     std::string TFZstr = " TFZ=" + std::string(one_mol_in_P1 ? "*" : dtos(zscores[r],1));
     thisTrial.ANNOTATION = mrset.ANNOTATION + RFZstr + TFZstr;
     thisTrial.TF = values[r];
     thisTrial.TFZ = (one_mol_in_P1 ? RLIST.RFZ : zscores[r]); //use RFZ as TFZ
     thisTrial.NSET = k+1;
     thisTrial.NRF = e+1;
     thisTrial.NTF = r+1;
     thisTrial.NCLUS = nclus[r];
     thisTrial.HISTORY = mrset.HISTORY;
     PHASER_ASSERT(!thisTrial.MAPCOEFS.size()); //takes time! clear them before!
     PHASER_ASSERT(!thisTrial.RLIST.size()); //takes time! clear them before!
     thisTrial.NROT = NROT-1;
     mr_ndim new_known(RLIST.MODLID,RLIST.EULER,true,frac[r],SEARCH_FACTORS.BFAC,SEARCH_FACTORS.OFAC);
     thisTrial.KNOWN.push_back(new_known);
     if (PTNCS.use_and_present())
     {
       thisTrial.ANNOTATION += " +TNCS";
       {
         int nmol_ptncs_if_present(PTNCS.NMOL-1);
         while (nmol_ptncs_if_present)
         {
           nmol_ptncs_if_present--;
           new_known.addT(PTNCS.TRA.VECTOR);
           thisTrial.KNOWN.push_back(new_known);
         }
       }
     }
     SET.push_back(thisTrial);
   }
   //sort_TF();
 }


  void mr_solution::incNROT() { NROT++; }

  bool mr_solution::is_rlist()
  {
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].RLIST.size()) return true;
    return false;
  }

  llg_cutoff mr_solution::purge_llg(floatType PURGE_PERCENT,int PURGE_NUMBER,data_zscore ZSCORE)
  {
    llg_cutoff cutoff;
    //sort_LLG();
   // floatType TOP = num_packs() ? top_packs_LLG() : top_LLG(); //ONLY USE PACKING SOLUTIONS FOR PURGE
    cutoff.num = std::numeric_limits<floatType>::max();
    for (int t = 0; t < SET.size(); t++) //look down the list
      cutoff.num = std::min(SET[t].LLG,cutoff.num);
    floatType tol(0.001);
    cutoff.num -= tol;
    cutoff.perc = cutoff.num;
    if (PURGE_NUMBER && SET.size())
    {
      //count how many KEEP solutions there are in the list
      int num_KEEP(0);
      for (int i = 0; i < SET.size(); i++)
        if (SET[i].KEEP) num_KEEP++;
      if (PURGE_NUMBER < num_KEEP)
      {
        int KEEP_PURGE_NUMBER = PURGE_NUMBER;
        for (int i = 0; i < KEEP_PURGE_NUMBER && i < SET.size(); i++)
        {
          floatType maxValue = SET[i].LLG;
          for (int t = i; t < SET.size(); t++) //look down the list
          {
            if (SET[t].LLG >= maxValue) //if value is greater than current max
            {
              maxValue = SET[t].LLG;
              cutoff.num = maxValue;
              mr_set tmp = SET[i]; //swap this value into i in the list
                     SET[i] = SET[t];
                     SET[t] = tmp;
            }
          }
          if (!SET[i].KEEP) KEEP_PURGE_NUMBER++;
        }
        //now flag these for deletion
        for (int i = KEEP_PURGE_NUMBER; i < SET.size(); i++)
        {
          SET[i].PURGE = true;
          cutoff.npurge++;
        }
      }
    }
    //limit to number, then also apply percent condition
    if (PURGE_MEAN && SET.size()) //TF reliable
    {
      floatType TOP = num_packs() ? top_packs_LLG() : top_LLG(); //ONLY USE PACKING SOLUTIONS FOR PURGE
      cutoff.perc = PURGE_PERCENT*(TOP-PURGE_MEAN) + PURGE_MEAN - tol;
      for (int t = 0; t < SET.size(); t++)
      {
        if (!SET[t].PURGE && SET[t].LLG < cutoff.perc && !ZSCORE.over_cutoff(SET[t].TFZ))
        {
          SET[t].PURGE = true;
          cutoff.npurge++; // Only count ones that haven't already been purged
        }
        if (SET[t].LLG <= cutoff.perc && ZSCORE.over_cutoff(SET[t].TFZ))
          cutoff.ntfz++;
      }
    }
    return cutoff;
  }

  floatType mr_solution::purge_tf(floatType PURGE_PERCENT,int PURGE_NUMBER,data_zscore ZSCORE)
  {
    //sort_TF();
    floatType cutoff(0);
    std::vector<mr_set> tmpSET;
    if (PURGE_NUMBER && SET.size() && PURGE_NUMBER >= SET.size())
    {
      cutoff = SET[0].TF;
      for (int i = 0; i < SET.size(); i++)
        if (SET[i].TF < cutoff)
         cutoff = SET[i].TF;
      floatType tol(1.0e-06);
      cutoff -= tol;
      tmpSET = SET;
    }
    else if (PURGE_NUMBER && SET.size())
    {
      for (int i = 0; i < PURGE_NUMBER; i++)
      {
        floatType maxValue = SET[i].TF;
        for (int t = i; t < SET.size(); t++)
        {
          if (SET[t].TF >= maxValue)
          {
            maxValue = SET[t].TF;
            cutoff = maxValue;
            mr_set tmp = SET[i];
                   SET[i] = SET[t];
                   SET[t] = tmp;
          }
        }
      }
      for (int i = 0; i < PURGE_NUMBER; i++)
        tmpSET.push_back(SET[i]);
    }
    else if (!PURGE_MEAN) return 0; //TF not reliable
    else
    {
      floatType TOP = top_TF();
      cutoff = PURGE_PERCENT*(TOP-PURGE_MEAN) + PURGE_MEAN;
      for (int t = 0; t < SET.size(); t++)
        if (SET[t].TF > cutoff || ZSCORE.over_cutoff(SET[t].TFZ))
          tmpSET.push_back(SET[t]);
    }
    SET = tmpSET;
    int tmpNROT(0);
    std::set<int> nrot;
    for (int k = 0; k < SET.size(); k++)
      nrot.insert(SET[k].NROT);
    for (std::set<int>::iterator iter = nrot.begin(); iter != nrot.end(); iter++)
    {
      for (int k = 0; k < SET.size(); k++)
      if (SET[k].NROT == *iter)
        SET[k].NROT=tmpNROT;
      tmpNROT++;
    }
    NROT = tmpNROT;
    return cutoff;
  }

  const mr_solution& mr_solution::purge_tfz(data_zscore& ZSCORE)
  {
    std::vector<mr_set> tmpSET;
    for (int t = 0; t < SET.size(); t++)
      if (ZSCORE.over_cutoff(SET[t].TFZ))
        tmpSET.push_back(SET[t]);
    SET = tmpSET;
    int tmpNROT(0);
    std::set<int> nrot;
    for (int k = 0; k < SET.size(); k++)
      nrot.insert(SET[k].NROT);
    for (std::set<int>::iterator iter = nrot.begin(); iter != nrot.end(); iter++)
    {
      for (int k = 0; k < SET.size(); k++)
      if (SET[k].NROT == *iter)
        SET[k].NROT = tmpNROT;
      tmpNROT++;
    }
    NROT = tmpNROT;
    return *this;
  }

  std::vector<delel>  mr_solution::purge_rf(floatType PURGE_PERCENT,floatType DOWN_PERCENT,int PURGE_NUMBER)
  {
    std::vector<delel> deletion_table;

    //isOneAtom
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].RLIST.size() == 1 && SET[t].RLIST[0].RFZ == 0) return deletion_table;

    std::multimap<floatType,std::pair<int,int> > rankRF;
    for (int t = 0; t < SET.size(); t++)
      for (int e = 0; e < SET[t].RLIST.size(); e++)
      {
        std::pair<int,int> te(t,e);
        std::pair<floatType,std::pair<int,int> > RFte(-SET[t].RLIST[e].RF,te);
        rankRF.insert(RFte);
      }

    bool2D selectRF(SET.size());
    for (int t = 0; t < SET.size(); t++)
      selectRF[t].resize(SET[t].RLIST.size());

    if (PURGE_NUMBER)
    {
      for (int t = 0; t < SET.size(); t++)
        for (int e = 0; e < SET[t].RLIST.size(); e++)
          selectRF[t][e] = false;
      int count(0);
      for (std::multimap<floatType,std::pair<int,int> >::iterator iter = rankRF.begin(); iter != rankRF.end(); iter++)
      {
         int t = iter->second.first;
         int e = iter->second.second;
         selectRF[t][e] = true;
         count++;
         if (count == PURGE_NUMBER) break;
      }
    }
    else
    {
      for (int t = 0; t < SET.size(); t++)
        for (int e = 0; e < SET[t].RLIST.size(); e++)
          selectRF[t][e] = true;
    }

    std::vector<mr_set> tmpSET(num_rlist());
    std::vector<mr_set> deepSET(num_rlist());
    floatType topRF = top_rlist().RF;
    floatType purge_mean = get_purge_mean();
    PHASER_ASSERT(purge_mean);
    floatType cutoff = (PURGE_PERCENT*(topRF-purge_mean)+purge_mean);
    floatType deep_cutoff = ((PURGE_PERCENT-DOWN_PERCENT)*(topRF-purge_mean)+purge_mean);
    for (int t = 0; t < SET.size(); t++)
    {
      //isOneAtom
      if (SET[t].RLIST.size() == 1 && SET[t].RLIST[0].RFZ == 0) return deletion_table;

      tmpSET.push_back(SET[t]);
      tmpSET[t].RLIST.clear();
      for (int e = 0; e < SET[t].RLIST.size(); e++)
      {
        if (SET[t].RLIST[e].RF > deep_cutoff && selectRF[t][e])
        {
          tmpSET[t].RLIST.push_back(SET[t].RLIST[e]);
          if (SET[t].RLIST[e].RF < cutoff && selectRF[t][e])
          {
            int last = tmpSET[t].RLIST.size()-1;
            tmpSET[t].RLIST[last].DEEP = true;
          }
        }
      }
    }

    deletion_table.resize(SET.size());
    for (int k = 0; k < SET.size(); k++)
    {
      deletion_table[k].first  = SET[k].RLIST.size();
      deletion_table[k].second = tmpSET[k].RLIST.size();
      deletion_table[k].deep_first = SET[k].getNDEEP();
      deletion_table[k].deep_second = tmpSET[k].getNDEEP();
    }

    //delete sets with empty RLIST
    for (int t = tmpSET.size()-1; t >= 0; t--) //erase backwards
      if (!tmpSET[t].RLIST.size())
        tmpSET.erase(tmpSET.begin()+t);

    SET = tmpSET;
    std::set<int> nrot;
    for (int k = 0; k < SET.size(); k++)
      nrot.insert(SET[k].NROT);
    int tmpNROT(0);
    for (std::set<int>::iterator iter = nrot.begin(); iter != nrot.end(); iter++)
    {
      for (int k = 0; k < SET.size(); k++)
      if (SET[k].NROT == *iter)
        SET[k].NROT = tmpNROT;
      tmpNROT++;
    }
    setNROT(tmpNROT);
    return deletion_table;
  }

  void mr_solution::num_over(floatType cutoff,floatType zscore,int& num_cutoff,int& num_zscore,int& num_both)
  {
    for (int t = 0; t < SET.size(); t++)
    {
      if (SET[t].TF > cutoff) num_cutoff++;
      if (SET[t].TFZ > zscore) num_zscore++;
      if (SET[t].TF > cutoff && SET[t].TFZ > zscore) num_both++;
    }
  }

  bool mr_solution::has_vrms()
  {
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].DRMS.size()) return true;
    return false;
  }

  bool mr_solution::has_cell()
  {
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].CELL.size()) return true;
    return false;
  }

  void mr_solution::fix_all(bool fix)
  {
    for (int t = 0; t < SET.size(); t++)
      for (int k = 0; k < SET[t].KNOWN.size(); k++)
        SET[t].KNOWN[k].fix_all(fix);
  }

  mr_solution mr_solution::set_as_sol(int t)
  {
    mr_solution tmp(NROT);
    tmp.copy_extras(*this);
    if (t < SET.size()) tmp.push_back(SET[t]);
    return tmp;
  }

  int mr_solution::num_rlist() const { return NROT; }
  void mr_solution::setNROT(int nrot) { NROT = nrot; }

  int mr_solution::num_in_rlist(int nrot) const
  {
    int tmp(0);
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].NROT == nrot) tmp++;
    return tmp;
  }

  int mr_solution::num_tfz_rlist(int nrot, floatType zscore) const
  {
    int tmp(0);
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].NROT == nrot && SET[t].TFZ > zscore) tmp++;
    return tmp;
  }

  mr_solution mr_solution::rlist_zscore(int nrot,floatType zscore_cutoff)
  {
    mr_solution tmp(0);
    tmp.copy_extras(*this);
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].NROT == nrot && SET[t].TFZ > zscore_cutoff) tmp.push_back(SET[t]);
    if (tmp.size()) tmp.NROT = 1;
    return tmp;
  }


  void mr_solution::apply_space_group_expansion(af_string SGALT)
  {
    PHASER_ASSERT(SGALT.size());
    PHASER_ASSERT(SET.size());
    int init_size = SET.size();
    for (int i = 0; i < init_size; i++)
      SET[i].HALL = SGALT[0];
    for (int s = 1; s < SGALT.size(); s++)
    {
      for (int i = 0; i < init_size; i++)
        SET.push_back(SET[i]);
      for (int i = 0; i < init_size; i++)
        SET[s*init_size+i].HALL = SGALT[s];
    }
  }

  void mr_solution::if_not_set_apply_space_group(std::string SG)
  {
    if (!SET.size()) return;
    if (!all_space_groups_set())
      for (int i = 0; i < SET.size(); i++)
        SET[i].HALL = SG;
  }

  bool mr_solution::all_space_groups_the_same()
  {
    for (int i = 1; i < SET.size(); i++)
      if (SET[i].HALL != SET[0].HALL) return false;
    return true;
  }

  bool mr_solution::all_space_groups_set()
  {
    if (!SET.size()) return true; //no known sets
    int nhall(0);
    for (int i = 0; i < SET.size(); i++)
      if (SET[i].HALL.size()) nhall++;
    return(nhall == SET.size());
  }

  void mr_solution::pruneDuplicateSolutions()
  {
    int tsize = SET.size();
    for (int k = tsize-1; 0 <= k; k--)
      if (!SET[k].KEEP || SET[k].PURGE) SET.erase(SET.begin()+k);
  }

  void mr_solution::set_as_rescored(bool b) { FTF_RESCORED = b; }
  bool mr_solution::has_been_rescored() const { return FTF_RESCORED; }

  void mr_solution::set_purge_mean(floatType m) { PURGE_MEAN = m; }
  floatType mr_solution::get_purge_mean() const { return PURGE_MEAN; }

  void mr_solution::set_resolution(floatType m) { HIRES = m; }
  floatType mr_solution::get_resolution() const { return HIRES; }

  void mr_solution::can_lower_resolution(bool b) { CAN_LOWER_RESOLUTION = b; }
  bool mr_solution::can_lower_resolution() const { return CAN_LOWER_RESOLUTION; }

  void mr_solution::set_deep_used(bool m)
  {
    DEEP_USED = false;
    if (!m) { return; }
    for (int k = 0; k < SET.size(); k++)
      if (SET[k].RLIST.size() > SET[k].getNDEEP()) //there are deep rotations
        { DEEP_USED = m; return; }
  }
  bool mr_solution::get_deep_used() const { return DEEP_USED; }

  int mr_solution::ndeep() const
  {
    int count(0);
    for (int k = 0; k < SET.size(); k++)
      count += SET[k].getNDEEP();
    return count;
  }

  int mr_solution::nknown() const
  {
    if (!SET.size()) return 0;
    return SET[0].KNOWN.size();
  }

  int mr_solution::not_ndeep() const
  {
    int count(0);
    for (int k = 0; k < SET.size(); k++)
      count += SET[k].RLIST.size() - SET[k].getNDEEP();
    return count;
  }

  floatType mr_solution::max_cell_scale()
  {
    floatType max_cell(0); //flag value 0
    for (int k = 0; k < SET.size(); k++)
      for (map_str_float_iter iter = SET[k].CELL.begin(); iter != SET[k].CELL.end(); iter++)
        max_cell = std::max(max_cell,iter->second);
    if (!max_cell) max_cell = 1;// flag no cells
    return max_cell; //zero if no CELLs present
  }

  std::string mr_solution::setup_vrms_delta(std::map<std::string,drms_vrms> ensemble)
  {
    stringset warning;
    for (int k = 0; k < SET.size(); k++)
    for (std::map<std::string,drms_vrms>::iterator iter = ensemble.begin(); iter != ensemble.end(); iter++)
    {
      //if the set drms is not present, initialize
      if (SET[k].DRMS.find(iter->first) == SET[k].DRMS.end())
      {
        SET[k].DRMS[iter->first] = iter->second.DRMS.initial();
      }
      else //if the vrms array is set on the mrset
      {
        if (SET[k].DRMS[iter->first] > iter->second.DRMS.upper ||
            SET[k].DRMS[iter->first] < iter->second.DRMS.lower)
        {
          warning.insert(iter->first);
          SET[k].DRMS[iter->first] = std::min(SET[k].DRMS[iter->first],iter->second.DRMS.upper);
          SET[k].DRMS[iter->first] = std::max(SET[k].DRMS[iter->first],iter->second.DRMS.lower);
        }
      }
    }
    std::string warningstr;
    for (stringset::iterator iter = warning.begin(); iter != warning.end(); iter++)
      warningstr += "   Solution VRMS DELTA shifts reset for ensemble \"" + *iter + "\"\n";
    return warningstr;
  }

  void mr_solution::setup_newvrms(std::map<std::string,drms_vrms> ensemble)
  { //store the input VRMS and NEWVRMS for python output
    for (int k = 0; k < SET.size(); k++)
      SET[k].setup_newvrms(ensemble);
  }

  int mr_solution::number_of_unique_ensembles()
  {
    if (!SET.size()) return 0;
    std::set<std::string> unique;
    for (int k = 0; k < SET[0].KNOWN.size(); k++)
      unique.insert(SET[0].KNOWN[k].MODLID);
    return unique.size();
  }
  string1D mr_solution::unique_ensembles()
  {
    if (!SET.size()) return string1D();
    string1D unique;
    for (int k = 0; k < SET[0].KNOWN.size(); k++)
      if (std::find(unique.begin(),unique.end(),SET[0].KNOWN[k].MODLID) == unique.end())
        unique.push_back(SET[0].KNOWN[k].MODLID);
    return unique;
  }
  int1D mr_solution::number_of_copies_of_unique_ensembles()
  {
    if (!SET.size()) return int1D();
    string1D unique = unique_ensembles();
    int1D count(unique.size(),0);
    for (int u = 0; u < unique.size(); u++)
      for (int k = 0; k < SET[0].KNOWN.size(); k++)
        if (unique[u] == SET[0].KNOWN[k].MODLID) count[u]++;
    return count;
  }

  int mr_solution::num_packs() const
  {
    size_type count(0);
    for (int t = 0; t < SET.size(); t++)
      if (SET[t].PACKS) count++;
    return count;
  }

  void  mr_solution::prune_by_packing()
  {
    for (int t = SET.size()-1; t >= 0; t--) //erase backwards
      if (!SET[t].PACKS)
        SET.erase(SET.begin()+t);
  }

  void  mr_solution::clear_cluster()
  {
    for (int t = 0; t < SET.size(); t++)
    {
      SET[t].NCLUS = 0;
      for (int e = 0; e < SET[t].RLIST.size(); e++)
        SET[t].RLIST[e].NCLUS = 0;
    }
  }

  void  mr_solution::clear_clash()
  {
    for (int t = 0; t < SET.size(); t++)
    {
      SET[t].CLASH.clear();
      SET[t].NSET = SET[t].NRF = SET[t].NTF = 0; //FROM_AUTO, FTF flag
    }
  }

  bool mr_solution::has_been_packed(double perc)
  {
    for (int t = 0; t < SET.size(); t++)
      if (!SET[t].CLASH.has_been_packed(SET[t].KNOWN.size(),perc)) return false;
    return true;
  }

  bool mr_solution::has_been_background_packed(double perc)
  {
    for (int t = 0; t < SET.size(); t++)
      if (!SET[t].CLASH.has_been_background_packed(SET[t].KNOWN.size(),perc)) return false;
    return true;
  }

  void mr_solution::setup_rlist(std::string modlid,double e1, double e2, double e3)
  {
    SET.resize(1);
    SET[0] = mr_set();
    SET[0].RLIST.push_back(mr_rlist(modlid,dvect3(e1,e2,e3)));
  }

  void mr_solution::setup_known(std::string modlid,double e1, double e2, double e3,double t1,double t2, double t3)
  {
    SET.resize(1);
    SET[0] = mr_set();
    SET[0].KNOWN.push_back(mr_ndim(modlid,dvect3(e1,e2,e3),true,dvect3(t1,t2,t3)));
  }

  void mr_solution::clear_real_occupancies()
  {
    for (int t = 0; t < SET.size(); t++)
    {
       float2D zero(0);
       SET[t].REALOCC.swap(zero);
    }
  }


} //phaser
