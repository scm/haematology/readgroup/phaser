#include <phaser/mr_objects/data_map.h>

namespace phaser {

data_map::data_map()
{
  PRINCIPAL_EXTENT = dvect3(0,0,0);
  PRINCIPAL_CENTRE = dvect3(0,0,0);
  COORD_EXTENT = dvect3(0,0,0);
  COORD_CENTRE = dvect3(0,0,0);
  MW = data_mw();
  SCATTERING = 0;
  CELL_SCALE = 1;
  PR = dmat33(1,0,0,0,1,0,0,0,1);
  PT = dvect3(0,0,0);
  std::vector< ncs::NCSOperator > p1(1,ncs::NCSOperator(PR,PT)); //because they are unitary
  PG = ncs::PointGroup(p1,double(DEF_ENSE_PTGR_TOLA),double(DEF_ENSE_PTGR_TOLS));
  HIRES = 0;
}

double data_map::mean_radius()
{
  double a = PRINCIPAL_EXTENT[0]/2.0;
  double b = PRINCIPAL_EXTENT[1]/2.0;
  double c = PRINCIPAL_EXTENT[2]/2.0;
  return (a+b+c)/3.;
}

double data_map::max_extent()
{ return std::max(PRINCIPAL_EXTENT[0],std::max(PRINCIPAL_EXTENT[1],PRINCIPAL_EXTENT[2])); }
double data_map::min_extent()
{ return std::min(PRINCIPAL_EXTENT[0],std::min(PRINCIPAL_EXTENT[1],PRINCIPAL_EXTENT[2])); }

void data_map::set_extent(dvect3 x) { COORD_EXTENT = PRINCIPAL_EXTENT = x; }
void data_map::set_centre(dvect3 x) { COORD_CENTRE = PRINCIPAL_CENTRE = x; }

//psv Rupp 2003 + RNA doi:10.1016/j.jmb.2004.11.072, note latter has different number for protein psv than Rupp
double data_map::volume() { return 1.22879889*MW.PROTEIN + 0.944846714*MW.NUCLEIC; }

double data_map::calculate_sampling_from_extent(data_trace& TRACE)
{
  double mapvolume = volume(); //fraction of volume,conservative
  mapvolume *= 0.74; //Kepler
  mapvolume /= TRACE.SAMP_TARGET;
  double SAMPLING = std::pow(mapvolume/(4./3.*scitbx::constants::pi),1./3.);
  SAMPLING *= 2; //diameter, between hexagonal points
  return std::max(TRACE.SAMP_MIN,SAMPLING);
}

} //phaser
