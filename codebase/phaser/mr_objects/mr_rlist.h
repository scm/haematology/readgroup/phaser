//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MR_RLIST_Class__
#define __MR_RLIST_Class__
#include <phaser/io/Errors.h>
#include <phaser/main/Phaser.h>
#include <phaser/mr_objects/mr_gyre.h>
#include <phaser/lib/jiffy.h>

namespace phaser {

class mr_rlist
{
  public:
  std::string MODLID;
  dvect3      EULER;
  floatType   RF,RFZ,GRF,NCLUS;
  bool        DEEP;
  std::vector<mr_gyre> GYRE;

  public:
  mr_rlist(
    std::string const& MODLID_="",
    dvect3 const& EULER_=dvect3(0,0,0),
    floatType const& RF_=0,
    floatType const& RFZ_=0,
    floatType const& GRF_=0,
    floatType const& DEEP_=0)
  :
    MODLID(MODLID_),
    EULER(EULER_),
    RF(RF_),
    RFZ(RFZ_),
    GRF(GRF_),
    DEEP(DEEP_?true:false)
  { NCLUS = 0; GYRE.clear(); }

  bool operator<(const mr_rlist &right)  const
  {
    if (NCLUS == right.NCLUS) return (RF < right.RF);
    return NCLUS > right.NCLUS; //yes, invert operator
  }

  std::string unparse(int depth=0)
  {
    std::string modlid = MODLID;
    modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
    modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
    std::string Card;
    for (int i=0; i<depth; i++) Card += "   ";
    Card += "SOLU TRIAL ENSEMBLE " + modlid + " EULER " + dvtos(EULER,8,3) +
            " RF " + dtos(RF,6,1) +  //NOT optional unparse, change in parsing for phaser-2.8
            " RFZ " + dtos(RFZ,5,2) +
            std::string(GRF? " GRF " + dtos(GRF,6,1) : "") +  //optional unparse
            std::string(NCLUS ? " # CLUSTER " + itos(NCLUS) : "") +
            std::string(DEEP ? " # DEEP" : "");
    Card += "\n";
    if (GYRE.size())
    {
      for (int k = 0; k < GYRE.size(); k++)
        Card += GYRE[k].unparse() + "\n";
    }
    return Card;
  }

  std::string logfile()
  {
    std::string modlid = MODLID;
    modlid.erase(std::remove(modlid.begin(),modlid.end(),' '),modlid.end());
    modlid = (modlid.find_first_of(" ")!=std::string::npos) ? "\"" + modlid + "\"" : modlid;
    return  "ENSEMBLE " + modlid + " EULER " + dvtos(EULER,3) +
            " RF=" + dtos(RF,1) + " RFZ=" + dtos(RFZ,2);
  }

};

} //phaser

#endif
