#include <phaser/mr_objects/search_array.h>
#include <phaser/lib/round.h>
#include <phaser/io/Errors.h>

namespace phaser {

  std::vector<search_component> search_array::set_of_components(int c)
  {
    std::vector<search_component> vec;
    for (int i = c; i < SET.size(); i++)
    { //returns search order by current sort order of SET
      bool found_modlid(false);
      for (int j = 0; j < vec.size(); j++)
        if (vec[j].logModlid() == SET[i].logModlid())
          found_modlid = true;
      if (!found_modlid) vec.push_back(SET[i]);
    }
    return vec;
  }

  std::map<search_component,int> search_array::map_of_components() const
  {
    //test for < on MODLID only
    std::map<search_component,int> tmp;
    for (int i = 0; i < SET.size(); i++) tmp[SET[i]] = 0;
    for (int i = 0; i < SET.size(); i++) tmp[SET[i]]++;
    return tmp;
  }

  std::map<std::string,int> search_array::map_of_modlids() const
  {
    std::map<std::string,int> tmp;
    std::map<search_component,int> moc = map_of_components();
    for (std::map<search_component,int>::iterator iter = moc.begin(); iter != moc.end(); iter++)
    {
      for (int i = 0; i < iter->first.af_MODLID().size(); i++)
        tmp[iter->first.af_MODLID()[i]] = iter->second;
    }
    return tmp;
  }

  std::set<std::string> search_array::set_of_modlids() const
  {
    std::set<std::string> tmp;
    std::map<search_component,int> moc = map_of_components();
    for (std::map<search_component,int>::iterator iter = moc.begin(); iter != moc.end(); iter++)
    {
      for (int i = 0; i < iter->first.af_MODLID().size(); i++)
        tmp.insert(iter->first.af_MODLID()[i]);
    }
    return tmp;
  }

  bool search_array::not_an_OR_search() const
  {
    for (int i = 0; i < SET.size(); i++)
      if (SET[i].af_MODLID().size() > 1) return false;
    return true;
  }


  int search_array::num_unique(int c) const
  {
    std::set<search_component> tmp;
    for (int i = c; i < SET.size(); i++) tmp.insert(SET[i]);
    return tmp.size();
  }

  int1D search_array::nmols() const
  {
    int1D tmp;
    std::map<search_component,int> zsearch = map_of_components();
    for (std::map<search_component,int>::iterator iter = zsearch.begin(); iter != zsearch.end(); iter++)
      tmp.push_back(iter->second);
    return tmp;
  }

  search_array search_array::remaining(int c) const
  {
    search_array tmp;
    for (int j = c; j < SET.size(); j++)
      tmp.push_back(SET[j]);
    return tmp;
  }

  int search_array::remaining(int c,std::string comp) const
  {
    int tmp(0);
    for (int j = c; j < SET.size(); j++)
      if (SET[j].MODLID[0] == comp) //first MODLID only
        tmp++;
    return tmp;
  }

  void search_array::set_modlid(int c,search_component modlid)
  {
    //permute the remaining models
    search_array tmp;
    for (int j = c; j < SET.size(); j++)
      if (SET[j].logModlid() == modlid.logModlid())
      {
        tmp.push_back(SET[j]);
        break;
      }
    bool found_first(false);
    for (int j = c; j < SET.size(); j++)
    {
      if (!found_first && SET[j].logModlid() == modlid.logModlid())
        found_first = true;
      else tmp.push_back(SET[j]);
    }
    for (int i = 0; i < tmp.size(); i++)
      SET[c+i] = tmp[i];
  }

  string2D search_array::getModlidStar()
  {
    string2D MODLID;
    for (int c = 0; c < SET.size(); c++)
    {
      string1D tmp;
      for (int m = 0; m < SET[c].MODLID.size(); m++)
        tmp.push_back(" " + SET[c].MODLID[m] + " " + SET[c].star_str());
      MODLID.push_back(tmp);
    }
    return MODLID;
  }

  void search_array::set_modlid_num(search_array SEARCH_ARRAY,int num)
  {
    SET.clear();
    for (int j = 0; j < num; j++) SET.push_back(SEARCH_ARRAY[j]);
  }

  void  search_array::sort_ellg(std::map<search_component,llg_hires> LLGmap,int c)
  {
    //sort the ones that are in LLGmap first
    //then add the others
    std::vector<search_component> tmpa,tmpb;
    for (int i = c; i < SET.size(); i++)
      if (LLGmap.find(SET[i]) != LLGmap.end()) //only uses the MODLID for comparison
      {
        tmpa.push_back(SET[i]);
        tmpa.back().eLLG = LLGmap.find(SET[i])->second.LLG;
      }
      else
        tmpb.push_back(SET[i]);
    PHASER_ASSERT(c+tmpa.size()+tmpb.size() == SET.size());
    std::sort(tmpa.begin(),tmpa.end(),search_order_ellg_comp);
    for (int i = 0; i < tmpa.size(); i++)
      SET[c+i] = tmpa[i];
    for (int i = 0; i < tmpb.size(); i++)
      SET[c+tmpa.size()+i] = tmpb[i];
  }

  void search_array::edit_tncs(int NMOL)
  {
    std::map<search_component,int> target = map_of_components();
    //number of copies is rounded to nearest nmol divisor
    //except that the minimum is 1
    //nmol=3 search= 1(1) 2(1) 3(1) 4(1) 5(2) 6(2) 7(2) 8(3) 9(3) etc
    for (std::map<search_component,int>::iterator iter = target.begin(); iter != target.end(); iter++)
      iter->second = std::max(1.0,phaser::round(double(iter->second)/double(NMOL)));
    std::map<search_component,int> count = map_of_components();
    for (std::map<search_component,int>::iterator iter = count.begin(); iter != count.end(); iter++)
      iter->second = 0;
    int c(0);
    while (c < SET.size())
    {
      count[SET[c]]++;
      if (count[SET[c]] > target[SET[c]])
        SET.erase(SET.begin()+c);
      else c++;
    }
  }
}
