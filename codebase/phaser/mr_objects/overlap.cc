#include <phaser/mr_objects/overlap.h>
#include <phaser/src/SpaceGroup.h>
#include <phaser/src/UnitCell.h>
#include <cctbx/sgtbx/site_symmetry.h>
#include <cctbx/sgtbx/site_symmetry_table.h>

namespace phaser {

  overlap::overlap(mr_ndim& NDIM,cctbx::sgtbx::space_group& sg,cctbx::uctbx::unit_cell& uc,dvect3& CENTRE)
  {
    dvect3 orthCentre_chk(NDIM.R*CENTRE+uc.orthogonalization_matrix()*(NDIM.TRA));
    COFM = uc.fractionalization_matrix()*(orthCentre_chk);
    double min_distance_sym_equiv = 1;//more generous than default of 0.5
    cctbx::sgtbx::site_symmetry this_site_sym(uc,sg,COFM,min_distance_sym_equiv);
    EXACT_SITE = this_site_sym.exact_site();
    MULT = sg.order_z()/this_site_sym.multiplicity();
  }

}
