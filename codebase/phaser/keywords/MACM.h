//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACMClass__
#define __MACMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolMR.h>
#include <phaser/include/data_protocol.h>
#include <phaser/include/Boolean.h>

namespace phaser {

class MACM : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr>  MACRO_MR;
    Boolean MACM_CHAINS;
    bool MACM_LAST;
    bool MACM_TFZEQ;
    double MACM_SIGR,MACM_SIGT;

    MACM();
    virtual ~MACM() {}

  public:
    data_protocol MACM_PROTOCOL;
    bool LAST_ONLY,FIX_BFAC;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACM(bool,bool,bool,bool,bool,bool,bool,int,std::string);
    void addMACM_FULL(bool,bool,bool,bool,bool,bool,bool,int,std::string,double,double);
    void setMACM_PROT(std::string);
    void setMACM_ATOM_PROTOCOL();
    void setMACM_PROTOCOL_VRMS_FIX(bool);
    void setMACM_PROTOCOL_LAST_ONLY();
    void setMACM_PROTOCOL_BFAC_FIX();
    void setMACM_CHAI(bool);
    void setMACM_LAST(bool);
    void setMACM_TFZE(bool);
    void setMACM_SIGR(double);
    void setMACM_SIGT(double);
};

} //phaser

#endif
