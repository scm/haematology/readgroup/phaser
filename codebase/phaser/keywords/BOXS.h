//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __BOXSClass__
#define __BOXSClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class BOXS : public InputBase, virtual public CCP4base
{
  public:
    floatType BOXSCALE;

    BOXS();
    virtual ~BOXS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setBOXS(floatType);
};

} //phaser

#endif
