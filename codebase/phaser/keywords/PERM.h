//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PERMClass__
#define __PERMClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class PERM : public InputBase, virtual public CCP4base
{
  public:
    bool DO_PERMUTE;

    PERM();
    virtual ~PERM() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPERM(bool);
};

} //phaser

#endif
