//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SCATClass__
#define __SCATClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_scat.h>

namespace phaser {

class SCAT : public InputBase, virtual public CCP4base
{
  public:
    data_scat SCATTERING;

    SCAT();
    virtual ~SCAT() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addSCAT(std::string,floatType,floatType,std::string);
    void setSCAT_REST(bool);
    void setSCAT_SIGM(floatType);
};

} //phaser

#endif
