//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/EIGE.h>
#include <phaser_defaults.h>

namespace phaser {

EIGE::EIGE() : CCP4base(), InputBase()
{
  Add_Key("EIGE");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  EIGE_READ = "";
  EIGE_WRITE = bool(DEF_EIGE_WRIT);
}

std::string EIGE::unparse()
{
  std::string Card("");
  if (EIGE_READ != "") Card += "EIGEN READ \"" + EIGE_READ + "\"\n";
  else if (EIGE_WRITE && EIGE_WRITE != bool(DEF_EIGE_WRIT))
    Card += "EIGEN WRITE "  + card(EIGE_WRITE) + "\n";
  return Card;
}

Token_value EIGE::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"READ","WRITE");
  if (keyIs("READ")) setEIGE_READ(getFileName(input_stream));
  if (keyIs("WRITE")) setEIGE_WRIT(getBoolean(input_stream));
  return skip_line(input_stream);
}

void EIGE::setEIGE_READ(std::string eigenfile)
{
  std::ifstream infile(const_cast<char*>(eigenfile.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,eigenfile));
  EIGE_READ = eigenfile; EIGE_WRITE = false;
}

void EIGE::setEIGE_WRIT(bool b)
{ EIGE_WRITE = b; }

void EIGE::analyse(void)
{
  if (EIGE_READ != "" && EIGE_WRITE)
  EIGE_WRITE = false; //don't overwrite input
}

} //phaser
