//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CLUSClass__
#define __CLUSClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class CLUS : public InputBase, virtual public CCP4base
{
  public:
    std::string CLUSTER_PDB;

    CLUS();
    virtual ~CLUS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setCLUS_PDB(std::string);
    void addCLUS_PDB(std::string id,std::string pdbfile) { setCLUS_PDB(pdbfile); }
};

} //phaser

#endif
