//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PURG.h>
#include <phaser_defaults.h>

namespace phaser {

PURG::PURG() : CCP4base(), InputBase()
{
  Add_Key("PURG");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string PURG::unparse()
{
  std::string Card("");
  Card += PURGE_ROT.unparse();
  Card += PURGE_TRA.unparse();
  Card += PURGE_RNP.unparse();
  Card += PURGE_GYRE.unparse();
  return Card;
}

Token_value PURG::parse(std::istringstream& input_stream)
{
  int key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  compulsoryKey(input_stream,4,"ROTATION","TRANSLATION","RNP","GYRE");
  if (keyIs("ROT"))
  {
    compulsoryKey(input_stream,3,"ENABLE","PERCENT","NUMBER");
    if (keyIs("ENABLE"))  setPURG_ROTA_ENAB(getBoolean(input_stream));
    if (keyIs("PERCENT")) setPURG_ROTA_PERC(get1num(input_stream));
    if (keyIs("NUMBER"))  setPURG_ROTA_NUMB(get1num(input_stream));
  }
  else if (keyIs("TRA"))
  {
    compulsoryKey(input_stream,3,"ENABLE","PERCENT","NUMBER");
    if (keyIs("ENABLE"))  setPURG_TRAN_ENAB(getBoolean(input_stream));
    if (keyIs("PERCENT")) setPURG_TRAN_PERC(get1num(input_stream));
    if (keyIs("NUMBER"))  setPURG_TRAN_NUMB(get1num(input_stream));
  }
  else if (keyIs("RNP"))
  {
    compulsoryKey(input_stream,3,"ENABLE","PERCENT","NUMBER");
    if (keyIs("ENABLE"))  setPURG_RNP_ENAB(getBoolean(input_stream));
    if (keyIs("PERCENT")) setPURG_RNP_PERC(get1num(input_stream));
    if (keyIs("NUMBER"))  setPURG_RNP_NUMB(get1num(input_stream));
  }
  else if (keyIs("GYRE"))
  {
    compulsoryKey(input_stream,3,"ENABLE","PERCENT","NUMBER");
    if (keyIs("ENABLE"))  setPURG_GYRE_ENAB(getBoolean(input_stream));
    if (keyIs("PERCENT")) setPURG_GYRE_PERC(get1num(input_stream));
    if (keyIs("NUMBER"))  setPURG_GYRE_NUMB(get1num(input_stream));
  }
  setKeyLength(key_len);
  return skip_line(input_stream);
}

void PURG::setPURG_ROTA_ENAB(bool on_off)    { PURGE_ROT.ENABLE = on_off; }
void PURG::setPURG_ROTA_PERC(floatType perc)
{
  PURGE_ROT.set_percent(perc);
}
void PURG::setPURG_ROTA_NUMB(floatType numb) { PURGE_ROT.NUMBER = iszeroposi(numb); }
void PURG::setPURG_TRAN_ENAB(bool on_off)    { PURGE_TRA.ENABLE = on_off; }
void PURG::setPURG_TRAN_PERC(floatType perc)
{
  PURGE_TRA.set_percent(perc);
}
void PURG::setPURG_TRAN_NUMB(floatType numb) { PURGE_TRA.NUMBER = iszeroposi(numb); }
void PURG::setPURG_RNP_ENAB(bool on_off)     { PURGE_RNP.ENABLE = on_off; }
void PURG::setPURG_RNP_PERC(floatType perc)
{ //no need for default inequality test, not compared to PEAKS value
  PURGE_RNP.set_percent(perc);
}
void PURG::setPURG_RNP_NUMB(floatType numb)  { PURGE_RNP.NUMBER = iszeroposi(numb); }

void PURG::setPURG_GYRE_ENAB(bool on_off)     { PURGE_GYRE.ENABLE = on_off; }
void PURG::setPURG_GYRE_PERC(floatType perc)
{ //no need for default inequality test, not compared to PEAKS value
  PURGE_GYRE.set_percent(perc);
}
void PURG::setPURG_GYRE_NUMB(floatType numb)  { PURGE_GYRE.NUMBER = isposi(numb); } //can't be zero, no output

void PURG::analyse(void)
{
  if (PURGE_TRA.ENABLE != bool(DEF_PURG_TRAN_ENAB) && PURGE_RNP.ENABLE == bool(DEF_PURG_RNP_ENAB))
    PURGE_RNP.ENABLE = PURGE_TRA.ENABLE;
  if (!PURGE_TRA.is_default_percent() && PURGE_RNP.is_default_percent())
    PURGE_RNP.set_percent(PURGE_TRA.get_percent());
  if (PURGE_TRA.NUMBER != int(DEF_PURG_TRAN_NUMB) && PURGE_RNP.NUMBER == int(DEF_PURG_RNP_NUMB))
    PURGE_RNP.NUMBER = PURGE_TRA.NUMBER;
}

}//phaser
