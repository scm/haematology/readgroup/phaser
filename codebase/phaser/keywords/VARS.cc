//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/VARS.h>
#include <cctbx/adptbx.h>

namespace phaser {

VARS::VARS() : CCP4base(), InputBase()
{
  Add_Key("VARS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string VARS::unparse()
{ return VAR_EP.unparse(); }

Token_value VARS::parse(std::istringstream& input_stream)
{
  //break the mould and set values directly in parser
  compulsoryKey(input_stream,8,"SA","SB","SP","SD","PK","PB","K","B");
  if (keyIs("K")) VAR_EP.ScaleK = get1num(input_stream);
  if (keyIs("B")) VAR_EP.ScaleU = get1num(input_stream);
  if (keyIs("PK")) VAR_EP.PartK = get1num(input_stream);
  if (keyIs("PB")) VAR_EP.PartU = get1num(input_stream);
  if (keyIs("SA"))
  {
    VAR_EP.DphiA_bin.clear();
    while (get_token(input_stream) == NUMBER)
      VAR_EP.DphiA_bin.push_back(number_value);
  }
  if (keyIs("SB"))
  {
    VAR_EP.DphiB_bin.clear();
    while (get_token(input_stream) == NUMBER)
      VAR_EP.DphiB_bin.push_back(number_value);
  }
  if (keyIs("SP"))
  {
    VAR_EP.SP_bin.clear();
    while (get_token(input_stream) == NUMBER)
      VAR_EP.SP_bin.push_back(number_value);
  }
  if (keyIs("SD"))
  {
    VAR_EP.SDsqr_bin.clear();
    while (get_token(input_stream) == NUMBER)
      VAR_EP.SDsqr_bin.push_back(number_value);
  }
  return skip_line(input_stream);
}

void VARS::setVARS(af_float sadvar)
{ VAR_EP.set_variance_array(sadvar); }

void VARS::analyse(void)
{
}

}//phaser
