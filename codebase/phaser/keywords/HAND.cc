//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/HAND.h>
#include <phaser/io/Errors.h>
#include <scitbx/constants.h>
#include <phaser_defaults.h>

namespace phaser {

HAND::HAND() : CCP4base(), InputBase()
{
  Add_Key("HAND");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  HAND_CHANGE.set(DEF_HAND);
}

std::string HAND::unparse()
{
  if (HAND_CHANGE.unparse() != std::string(DEF_HAND))
  return "HAND " + HAND_CHANGE.unparse() + "\n";
  return "";
}

Token_value HAND::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,3,"ON","OFF","BOTH");
  if (keyIs("ON"))   setHAND("ON");
  if (keyIs("OFF"))  setHAND("OFF");
  if (keyIs("BOTH")) setHAND("BOTH");
  return skip_line(input_stream);
}

void HAND::setHAND(std::string hand)
{
  bool error = HAND_CHANGE.set(hand);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Hand selection not recognised"));
}

void HAND::analyse(void)
{
}

}//phaser
