//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/BFAC.h>
#include <scitbx/constants.h>
#include <phaser_defaults.h>

namespace phaser {

BFAC::BFAC() : CCP4base(), InputBase()
{
  Add_Key("BFAC");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  BFAC_WILS.RESTRAINT = bool(DEF_BFAC_WILS_REST);
  BFAC_WILS.SIGMA = floatType(DEF_BFAC_WILS_SIGM);
  BFAC_SPHE.RESTRAINT = bool(DEF_BFAC_SPHE_REST);
  BFAC_SPHE.SIGMA = floatType(DEF_BFAC_SPHE_SIGM);
  BFAC_REFI.RESTRAINT = bool(DEF_BFAC_REFI_REST);
  BFAC_REFI.SIGMA = floatType(DEF_BFAC_REFI_SIGM);

  DRMS_REFI.RESTRAINT = bool(DEF_DRMS_REFI_REST);
  DRMS_REFI.SIGMA = floatType(DEF_DRMS_REFI_SIGM);
}

std::string BFAC::unparse()
{
  std::string Card("");
  if (BFAC_WILS.RESTRAINT != bool(DEF_BFAC_WILS_REST))
  Card += "BFACTOR WILSON RESTRAINT " + card(BFAC_WILS.RESTRAINT) + "\n";
  if (BFAC_SPHE.RESTRAINT != bool(DEF_BFAC_SPHE_REST))
  Card += "BFACTOR SPHERICITY RESTRAINT " + card(BFAC_SPHE.RESTRAINT) + "\n";
  if (BFAC_REFI.RESTRAINT != bool(DEF_BFAC_REFI_REST))
  Card += "BFACTOR REFINE RESTRAINT " + card(BFAC_REFI.RESTRAINT) + "\n";
  if (BFAC_WILS.RESTRAINT && BFAC_WILS.SIGMA != floatType(DEF_BFAC_WILS_SIGM))
  Card += "BFACTOR WILSON SIGMA " + dtos(BFAC_WILS.SIGMA) + "\n";
  if (BFAC_SPHE.RESTRAINT && BFAC_SPHE.SIGMA != floatType(DEF_BFAC_SPHE_SIGM))
  Card += "BFACTOR SPHERICITY SIGMA " + dtos(BFAC_SPHE.SIGMA) + "\n";
  if (BFAC_REFI.RESTRAINT && BFAC_REFI.SIGMA != floatType(DEF_BFAC_REFI_SIGM))
  Card += "BFACTOR REFINE SIGMA " + dtos(BFAC_REFI.SIGMA) + "\n";

  if (DRMS_REFI.RESTRAINT != bool(DEF_DRMS_REFI_REST))
  Card += "BFACTOR DRMS RESTRAINT " + card(DRMS_REFI.RESTRAINT) + "\n";
  if (DRMS_REFI.RESTRAINT && DRMS_REFI.SIGMA != floatType(DEF_DRMS_REFI_SIGM))
  Card += "BFACTOR DRMS SIGMA " + dtos(DRMS_REFI.SIGMA) + "\n";
  return Card;
}

Token_value BFAC::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,4,"WILSON","SPHERICITY","REFINE","DRMS");
  if (keyIs("WILSON"))
  {
    while (optionalKey(input_stream,2,"RESTRAINT","SIGMA"))
      if (keyIs("RESTRAINT"))  setBFAC_WILS_REST(getBoolean(input_stream));
      else if (keyIs("SIGMA")) setBFAC_WILS_SIGM(get1num(input_stream));
  }
  if (keyIs("SPHERICITY"))
  {
    while (optionalKey(input_stream,2,"RESTRAINT","SIGMA"))
      if (keyIs("RESTRAINT"))  setBFAC_SPHE_REST(getBoolean(input_stream));
      else if (keyIs("SIGMA")) setBFAC_SPHE_SIGM(get1num(input_stream));
  }
  if (keyIs("REFINE"))
  {
    while (optionalKey(input_stream,2,"RESTRAINT","SIGMA"))
      if (keyIs("RESTRAINT"))  setBFAC_REFI_REST(getBoolean(input_stream));
      else if (keyIs("SIGMA")) setBFAC_REFI_SIGM(get1num(input_stream));
  }
  if (keyIs("DRMS"))
  {
    while (optionalKey(input_stream,2,"RESTRAINT","SIGMA"))
      if (keyIs("RESTRAINT"))  setDRMS_REFI_REST(getBoolean(input_stream));
      else if (keyIs("SIGMA")) setDRMS_REFI_SIGM(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void BFAC::setBFAC_WILS_REST(bool on_off)
{ BFAC_WILS.RESTRAINT = on_off; }

void BFAC::setBFAC_SPHE_REST(bool on_off)
{ BFAC_SPHE.RESTRAINT = on_off;}

void BFAC::setBFAC_REFI_REST(bool on_off)
{ BFAC_REFI.RESTRAINT = on_off;}

void BFAC::setBFAC_WILS_SIGM(floatType sigma)
{ BFAC_WILS.SIGMA = ispos(sigma); }

void BFAC::setBFAC_SPHE_SIGM(floatType sigma)
{ BFAC_SPHE.SIGMA = ispos(sigma); }

void BFAC::setBFAC_REFI_SIGM(floatType sigma)
{ BFAC_REFI.SIGMA = ispos(sigma); }


void BFAC::setDRMS_REFI_REST(bool on_off)
{ DRMS_REFI.RESTRAINT = on_off;}

void BFAC::setDRMS_REFI_SIGM(floatType sigma)
{ DRMS_REFI.SIGMA = ispos(sigma); }

void BFAC::analyse(void)
{
}

}//phaser
