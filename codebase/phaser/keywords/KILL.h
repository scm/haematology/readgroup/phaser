//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __KILLClass__
#define __KILLClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class KILL : public InputBase, virtual public CCP4base
{
  public:
    std::string FILEKILL;
    floatType   TIMEKILL;

    KILL();
    virtual ~KILL() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setKILL_FILE(std::string);
    void setKILL_TIME(floatType);
};

} //phaser

#endif
