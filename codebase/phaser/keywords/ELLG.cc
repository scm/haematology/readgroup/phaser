//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ELLG.h>
#include <phaser_defaults.h>

namespace phaser {

ELLG::ELLG() : CCP4base(), InputBase()
{
  Add_Key("ELLG");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  ELLG_TARGET = floatType(DEF_ELLG_TARG);
  ELLG_HIRES = bool(true); //change unparse
  ELLG_DATA.clear();
  ELLG_RMSD_MULT = floatType(DEF_ELLG_RMSD_MULT);
}

std::string ELLG::unparse()
{
  std::string Card("");
  if (ELLG_TARGET != DEF_ELLG_TARG)
  Card += "ELLG TARGET " + dtos(ELLG_TARGET) + "\n";
  if (ELLG_HIRES != true)
  Card += "ELLG HIRES " + card(ELLG_HIRES) + "\n";
  for (std::set<double>::iterator iter = ELLG_POLYALANINE.begin(); iter != ELLG_POLYALANINE.end(); iter++)
    Card += "ELLG POLYALANINE RMSD " + dtos(*iter) + "\n";
  if (ELLG_RMSD_MULT != double(DEF_ELLG_RMSD_MULT))
  Card += "ELLG RMSD MULT " + dtos(ELLG_RMSD_MULT) + "\n";
  Card += "ELLG HIRES " + card(ELLG_HIRES) + "\n";
  return Card;
}

Token_value ELLG::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,4,"TARGET","HIRES","POLYALANINE","RMSD");
  if (keyIs("HIRES"))  setELLG_HIRE(getBoolean(input_stream));
  if (keyIs("TARGET")) setELLG_TARG(get1num(input_stream));
  if (keyIs("POLYALANINE"))
  {
    while (optionalKey(input_stream,1,"RMSD")) addELLG_POLY_RMSD(get1num(input_stream));
  }
  if (keyIs("RMSD"))
  {
    compulsoryKey(input_stream,1,"MULTIPIER");
    setELLG_RMSD_MULT(get1num(input_stream));
  }
  return skip_line(input_stream);
  return skip_line(input_stream);
}

void ELLG::setELLG_TARG(floatType t)      { ELLG_TARGET = ispos(t); }
void ELLG::setELLG_HIRE(bool b)           { ELLG_HIRES = b; }
void ELLG::addELLG_POLY_RMSD(double b)    { ELLG_POLYALANINE.insert(b); }
void ELLG::setELLG_RMSD_MULT(double b)    { ELLG_RMSD_MULT = ispos(b); }

void ELLG::analyse(void)
{
}

} //phaser
