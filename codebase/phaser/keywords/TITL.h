//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TITLClass__
#define __TITLClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class TITL : public InputBase, virtual public CCP4base
{
  public:
    std::string TITLE;

    TITL();
    virtual ~TITL() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setTITL(std::string);
};

} //phaser

#endif
