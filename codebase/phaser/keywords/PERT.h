//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PERTClass__
#define __PERTClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_perturb.h>

namespace phaser {

class PERT : public InputBase, virtual public CCP4base
{
  public:
    data_perturb    PERTURB_RMS;
    std::string     PERTURB_INCREMENT;
    af::shared<int> PERTURB_DQ;

    PERT();
    virtual ~PERT() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPERT_RMS_STEP(floatType);
    void setPERT_RMS_MAXI(floatType);
    void setPERT_RMS_DIRE(std::string);
    void addPERT_DQ(floatType);
    void setPERT_INCR(std::string);
};

} //phaser

#endif
