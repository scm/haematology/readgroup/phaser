//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/RFAC.h>
#include <phaser_defaults.h>

namespace phaser {

RFAC::RFAC() : CCP4base(), InputBase()
{
  Add_Key("RFAC");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  RFAC_USE = bool(DEF_RFAC_USE);
  RFAC_CUTOFF = floatType(DEF_RFAC_CUTO);
}

std::string RFAC::unparse()
{
  std::string Card;
  if (RFAC_USE != bool(DEF_RFAC_USE))
  Card += "RFACTOR USE " + card(RFAC_USE) + "\n";
  if (RFAC_USE  && RFAC_CUTOFF != double(DEF_RFAC_CUTO))
  Card += "RFACTOR CUTOFF " + dtos(RFAC_CUTOFF,5,2) + "\n";
  return Card;
}

Token_value RFAC::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,2,"USE","CUTOFF"))
  {
    if (keyIs("USE"))    setRFAC_USE(getBoolean(input_stream));
    if (keyIs("CUTOFF")) setRFAC_CUTO(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void RFAC::setRFAC_USE(bool b)        { RFAC_USE = b; }
void RFAC::setRFAC_CUTO(floatType  r) { RFAC_CUTOFF = ispos(r); }

void RFAC::analyse(void)
{
}

}//phaser
