//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/CRYS.h>
#include <cctbx/eltbx/tiny_pse.h>

namespace phaser {

CRYS::CRYS() : CCP4base(), InputBase()
{
  Add_Key("CRYS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  Add_Key("EXPE");
  //Add to CCP4base;
  inputPtr jPtr(this);
  possible_fns.push_back(jPtr);

  EXPERIMENT = "SAD";
}

std::string CRYS::unparse()
{
  std::string Card;
  if (EXPERIMENT != std::string("SAD"))
    Card += "EXPERIMENT " + EXPERIMENT + "\n";
  //don't attempt to set data this way!
  //the Preprocessor string will be very long
  Card += CRYS_DATA.unparse();
  return Card;
}

Token_value CRYS::parse(std::istringstream& input_stream)
{
  if (keyIs("EXPERIMENT")) // EXPE keyword parsed here
  {
    setEXPE(getString(input_stream));
    return skip_line(input_stream);
  }
  std::string xtalid = getString(input_stream);
  compulsoryKey(input_stream,1,"DATASET");
  if (keyIs("DATASET"))
  {
    std::string waveid = getString(input_stream);
    CRYS_DATA.init(xtalid,waveid);
    compulsoryKey(input_stream,2,"LABIN","WAVELENGTH");
    if (keyIs("LABIN"))
    {
      int default_key_len = getKeyLength();
      setKeyLength(7); //use all the unique chars for comparison
      compulsoryKey(input_stream,5,"F","F(+)","F+","FPOS","I");
      if (keyIs("F(+)") || keyIs("F+") || keyIs("FPOS"))
      {
        setCRYS_LABI_FPOS(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"SIGF(+)","SIGF+","SIGFPOS");
        setCRYS_LABI_SIGFPOS(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"F(-)","F-","FNEG");
        setCRYS_LABI_FNEG(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"SIGF(-)","SIGF-","SIGFNEG");
        setCRYS_LABI_SIGFNEG(xtalid,waveid,getAssignString(input_stream));
      }
      else if (keyIs("I(+)") || keyIs("I+") || keyIs("IPOS"))
      {
        setCRYS_LABI_IPOS(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"SIGI(+)","SIGI+","IPOS");
        setCRYS_LABI_SIGIPOS(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"I(-)","I-","INEG");
        setCRYS_LABI_INEG(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,3,"SIGI(-)","SIGI-","SIGINEG");
        setCRYS_LABI_SIGINEG(xtalid,waveid,getAssignString(input_stream));
      }
      else if (keyIs("F"))
      {
        setCRYS_LABI_F(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,1,"SIGF");
        setCRYS_LABI_SIGF(xtalid,waveid,getAssignString(input_stream));
      }
      else if (keyIs("I"))
      {
        setCRYS_LABI_I(xtalid,waveid,getAssignString(input_stream));
        compulsoryKey(input_stream,1,"SIGI");
        setCRYS_LABI_SIGI(xtalid,waveid,getAssignString(input_stream));
      }
      setKeyLength(default_key_len);  //reinstate default
    }
    if (keyIs("WAVELENGTH"))
      addCRYS_DATA_WAVE(xtalid,waveid,get1num(input_stream));
  }
  return skip_line(input_stream);
}

void CRYS::setEXPE(std::string experiment)
{
  if (experiment != "SAD")
  throw PhaserError(SYNTAX,keywords,"Experiment type not known");
  EXPERIMENT = experiment;
}

void CRYS::addCRYS_ANOM_LABI(std::string xtalid,std::string waveid,
                        std::string fpos,std::string sigfpos,
                        std::string fneg,std::string sigfneg)
{
  CRYS_DATA.init(xtalid,waveid);
  CRYS_DATA(xtalid,waveid).initAnom();
  setCRYS_LABI_FPOS(xtalid,waveid,fpos);
  setCRYS_LABI_SIGFPOS(xtalid,waveid,sigfpos);
  setCRYS_LABI_FNEG(xtalid,waveid,fneg);
  setCRYS_LABI_SIGFNEG(xtalid,waveid,sigfneg);
}

void CRYS::addCRYS_MEAN_LABI(std::string xtalid,std::string waveid,
                        std::string f,std::string sigf)
{
  CRYS_DATA.init(xtalid,waveid);
  CRYS_DATA(xtalid,waveid).initMean();
  setCRYS_LABI_FPOS(xtalid,waveid,f);
  setCRYS_LABI_SIGFPOS(xtalid,waveid,sigf);
}

void CRYS::addCRYS_ANOM_LABI_I(std::string xtalid,std::string waveid,
                        std::string fpos,std::string sigfpos,
                        std::string fneg,std::string sigfneg)
{
  setCRYS_LABI_IPOS(xtalid,waveid,fpos);
  setCRYS_LABI_SIGIPOS(xtalid,waveid,sigfpos);
  setCRYS_LABI_INEG(xtalid,waveid,fneg);
  setCRYS_LABI_SIGINEG(xtalid,waveid,sigfneg);
}

void CRYS::addCRYS_MEAN_LABI_I(std::string xtalid,std::string waveid,
                        std::string f,std::string sigf)
{
  setCRYS_LABI_I(xtalid,waveid,f);
  setCRYS_LABI_SIGI(xtalid,waveid,sigf);
}

void CRYS::setCRYS_DATA(data_ep crys_data)
{ CRYS_DATA = crys_data; }

void CRYS::setCRYS_MILLER(af::shared<miller::index<int> > m)
{ CRYS_DATA.MILLER = m; }

void CRYS::addCRYS_ANOM_DATA(std::string xtalid,std::string w,
                             af_float fpos,af_float sigfpos,af_bool ppos,
                             af_float fneg,af_float sigfneg,af_bool pneg)
{
  bool success = CRYS_DATA.setAnomData(xtalid,w,fpos,sigfpos,ppos,fneg,sigfneg,pneg);
  if (!success) CCP4base::store(PhaserError(INPUT,keywords,"Reflection array size mismatch"));
}

void CRYS::setCRYS_SAD_DATA(af::shared<miller::index<int> > m,
                            af_float fpos,af_float sigfpos,af_bool ppos,
                            af_float fneg,af_float sigfneg,af_bool pneg)
{
  CRYS_DATA.MILLER = m;
  bool success = CRYS_DATA.setAnomData("SAD","SAD",fpos,sigfpos,ppos,fneg,sigfneg,pneg);
  if (!success) CCP4base::store(PhaserError(INPUT,keywords,"Reflection array size mismatch"));
}

void CRYS::addCRYS_DATA_WAVE(std::string xtalid,std::string waveid,floatType wavelength)
{
  CRYS_DATA.init(xtalid,waveid);
  CRYS_DATA(xtalid,waveid).WAVELENGTH = wavelength;
}

void CRYS::setCRYS_LABI_FPOS(std::string x,std::string w, std::string c)    { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).POS.F_ID = c; }
void CRYS::setCRYS_LABI_FNEG(std::string x,std::string w, std::string c)    { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).NEG.F_ID = c; }
void CRYS::setCRYS_LABI_SIGFPOS(std::string x,std::string w, std::string c) { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).POS.SIGF_ID = c; }
void CRYS::setCRYS_LABI_SIGFNEG(std::string x,std::string w, std::string c) { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).NEG.SIGF_ID = c; }
void CRYS::setCRYS_LABI_IPOS(std::string x,std::string w, std::string c)    { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).POS.I_ID = c; }
void CRYS::setCRYS_LABI_INEG(std::string x,std::string w, std::string c)    { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).NEG.I_ID = c; }
void CRYS::setCRYS_LABI_SIGIPOS(std::string x,std::string w, std::string c) { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).POS.SIGI_ID = c; }
void CRYS::setCRYS_LABI_SIGINEG(std::string x,std::string w, std::string c) { CRYS_DATA.initAnom(x,w); CRYS_DATA(x,w).NEG.SIGI_ID = c; }
void CRYS::setCRYS_LABI_F(std::string x,std::string w, std::string c)       { CRYS_DATA.initMean(x,w); CRYS_DATA(x,w).POS.F_ID = c; }
void CRYS::setCRYS_LABI_SIGF(std::string x,std::string w, std::string c)    { CRYS_DATA.initMean(x,w); CRYS_DATA(x,w).POS.SIGF_ID = c; }
void CRYS::setCRYS_LABI_I(std::string x,std::string w, std::string c)       { CRYS_DATA.initMean(x,w); CRYS_DATA(x,w).POS.I_ID = c; }
void CRYS::setCRYS_LABI_SIGI(std::string x,std::string w, std::string c)    { CRYS_DATA.initMean(x,w); CRYS_DATA(x,w).POS.SIGI_ID = c; }

void CRYS::analyse(void)
{
  if (!CRYS_DATA.size() && !EXPERIMENT.size())
  {
    throw PhaserError(SYNTAX,keywords,"CRYSTAL/DATASET LABIN not set");
  }
  else if (!CRYS_DATA.size() && EXPERIMENT.size())
  {
    if (EXPERIMENT != "SAD") //only one allowed at the moment!
    throw PhaserError(SYNTAX,keywords,"Experiment \"" + EXPERIMENT + "\" not known");
  }
  else if (CRYS_DATA.size())
  {
  int error = CRYS_DATA().setup_error();
  if (error == 1)
  throw PhaserError(SYNTAX,keywords,"Both F and I set for Crystal/Dataset");
  if (error >= 2)
  throw PhaserError(SYNTAX,keywords,"Crystal/Dataset input incomplete");
  if (!CRYS_DATA.XTAL.size()) throw PhaserError(SYNTAX,keywords,"Crystal reflection data not set");
  int NREFL = CRYS_DATA.MILLER.size();
  for (xtalIter xiter = CRYS_DATA.XTAL.begin(); xiter != CRYS_DATA.XTAL.end(); xiter++)
  for (waveIter witer = CRYS_DATA.XTAL[xiter->first].WAVE.begin(); witer != CRYS_DATA.XTAL[xiter->first].WAVE.end(); witer++)
  {
    std::string x = xiter->first;
    std::string w = witer->first;
    if (CRYS_DATA(x,w).POS.F_ID.size() && CRYS_DATA(x,w).POS.F_ID == CRYS_DATA(x,w).NEG.F_ID)
      CCP4base::store(PhaserError(INPUT,keywords,"F+ and F- column labels identical"));
    if (CRYS_DATA(x,w).POS.SIGF_ID.size() && CRYS_DATA(x,w).POS.SIGF_ID == CRYS_DATA(x,w).NEG.SIGF_ID)
      CCP4base::store(PhaserError(INPUT,keywords,"SIGF+ and SIGF- column labels identical"));
    if (CRYS_DATA(x,w).POS.I_ID.size() && CRYS_DATA(x,w).POS.I_ID == CRYS_DATA(x,w).NEG.I_ID)
      CCP4base::store(PhaserError(INPUT,keywords,"I+ and I- column labels identical"));
    if (CRYS_DATA(x,w).POS.SIGI_ID.size() && CRYS_DATA(x,w).POS.SIGI_ID == CRYS_DATA(x,w).NEG.SIGI_ID)
      CCP4base::store(PhaserError(INPUT,keywords,"SIGI+ and SIGI- column labels identical"));
  }
  if (!CRYS_DATA().RefID.size())
    CRYS_DATA().RefID = CRYS_DATA().POS.INPUT_INTENSITIES ? CRYS_DATA().POS.I_ID : CRYS_DATA().POS.F_ID;
  CRYS_DATA().POS.INPUT_INTENSITIES = CRYS_DATA().POS.I.size() || CRYS_DATA().POS.I_ID.size();
  CRYS_DATA().NEG.INPUT_INTENSITIES = CRYS_DATA().NEG.I.size() || CRYS_DATA().NEG.I_ID.size();
  CRYS_DATA().POS.set_default_dfactor(NREFL);
  CRYS_DATA().NEG.set_default_dfactor(NREFL);
  }
}

} //phaser
