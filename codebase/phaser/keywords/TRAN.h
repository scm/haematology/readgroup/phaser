//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TRANClass__
#define __TRANClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_btf.h>

namespace phaser {

class TRAN : public InputBase, virtual public CCP4base
{
  public:
    data_btf    BTF;
    bool        TRAN_PACKING_USE;
    double      TRAN_PACKING_CUTOFF;
    int         TRAN_PACKING_NUMBER;
    bool        TRAN_MAPS;
    bool        TRAN_FAST_STATS;

    TRAN();
    virtual ~TRAN() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setTRAN_VOLU(std::string);
    void setTRAN_STAR(dvect3); //also alias as python setTRAN_AROU setTRAN_POIN
    void setTRAN_END(dvect3);
    void setTRAN_CENT(bool,double,double,double);
    void setTRAN_RANG(floatType);
    void setTRAN_FRAC(bool);
    void setTRAN_PACK_USE(bool);
    void setTRAN_PACK_CUTO(double);
    void setTRAN_PACK_NUMB(double);
    void setTRAN_MAPS(bool);
    void setTRAN_FAST_STAT(bool);
};

} //phaser

#endif
