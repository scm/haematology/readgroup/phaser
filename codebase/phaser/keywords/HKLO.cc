//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/HKLO.h>

namespace phaser {

HKLO::HKLO() : CCP4base(), InputBase()
{
  Add_Key("HKLO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_HKLOUT.Default(true);
}

std::string HKLO::unparse()
{
  if (DO_HKLOUT.unparse() != "") return "HKLOUT " + DO_HKLOUT.unparse() + "\n";
  return "";
}

Token_value HKLO::parse(std::istringstream& input_stream)
{
  setHKLO(getBoolean(input_stream));
  return skip_line(input_stream);
}

void HKLO::setHKLO(bool do_hklout) { DO_HKLOUT.Set(do_hklout); }

void HKLO::analyse(void)
{
}

} //phaser
