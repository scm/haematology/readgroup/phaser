//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/WAVE.h>

namespace phaser {

WAVE::WAVE() : CCP4base(), InputBase()
{
  Add_Key("WAVE");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  //WAVELENGTH = 1.5418; //default CuKa
  WAVELENGTH = 0; //no default
}

std::string WAVE::unparse()
{
  return "WAVELENGTH " + dtos(WAVELENGTH) + "\n";
}

Token_value WAVE::parse(std::istringstream& input_stream)
{
  setWAVE(get1num(input_stream));
  return skip_line(input_stream);
}

void WAVE::setWAVE(floatType wavelength)
{
  WAVELENGTH = ispos(wavelength);
}

void WAVE::analyse(void)
{
  if (!WAVELENGTH) throw PhaserError(INPUT,keywords,"Wavelength not set");
}

}//phaser
