//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/BINS.h>

namespace phaser {

BINS::BINS() : CCP4base(), InputBase()
{
  Add_Key("BINS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DATABINS.set_default_data();
}

std::string BINS::unparse()
{
  std::string Card;
  if (!DATABINS.is_default_data()) Card += DATABINS.unparse();
  return Card;
}

Token_value BINS::parse(std::istringstream& input_stream)
{
  int set_min(0),set_max(0),set_width(0);
  compulsoryKey(input_stream,1,"DATA");
  if (keyIs("DATA")) {
    while (optionalKey(input_stream,3,"MIN","MAX","WIDTH"))
    {
      if (keyIs("MIN")) {
        if (set_min++) {
          throw PhaserError(SYNTAX,keywords,"BINS DATA MIN already set");
        } else {
          setBINS_DATA_MINI(get1num(input_stream));
        }
      } else if (keyIs("MAX")) {
        if (set_max++) {
          throw PhaserError(SYNTAX,keywords,"BINS DATA MAX already set");
        } else {
          setBINS_DATA_MAXI(get1num(input_stream));
        }
      } else if (keyIs("WIDTH")) {
        if (set_width++) {
          throw PhaserError(SYNTAX,keywords,"BINS DATA WIDTH already set");
        } else {
          setBINS_DATA_WIDT(get1num(input_stream));
        }
      }
    }
  }
  return skip_line(input_stream);
}

void BINS::setBINS_DATA(data_bins databins)     { DATABINS = databins; }
void BINS::setBINS_DATA_MINI(floatType minbins) { DATABINS.MINBINS = isposi(minbins); }
void BINS::setBINS_DATA_MAXI(floatType maxbins) { DATABINS.MAXBINS = isposi(maxbins); }
void BINS::setBINS_DATA_WIDT(floatType width)   { DATABINS.WIDTH = isposi(width); }

void BINS::analyse(void)
{
}

} //phaser
