//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LABIClass__
#define __LABIClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_refl.h>

namespace phaser {

class LABI : public InputBase, virtual public CCP4base
{
  public:
    data_refl REFLECTIONS;

    LABI();
    virtual ~LABI() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setLABI_F(std::string);
    void setLABI_SIGF(std::string);
    void setLABI_I(std::string);
    void setLABI_SIGI(std::string);
    void setLABI_FMAP(std::string);
    void setLABI_PHMA(std::string);
    void setLABI_FOM(std::string);
    void setLABI_FWT(std::string);
    void setLABI_PHWT(std::string);

    void setREFL_DATA(data_refl&);
    void setREFL_F_SIGF(af::shared<miller::index<int> >,af_float,af_float);
    void setREFL_I_SIGI(af::shared<miller::index<int> >,af_float,af_float);
    void setREFL_FMAP_PHMAP_FOM(af_float,af_float,af_float);
    void setREFL_FWT_PHWT(af_float,af_float);

    void setLABI_F_SIGF(std::string,std::string);
    void setLABI_I_SIGI(std::string,std::string);
    void setLABI_FMAP_PHMAP_FOM(std::string,std::string,std::string);
    void setLABI_FWT_PHWT(std::string,std::string);

    //for analyse
    void setREFL_CELL(af::double6);
    void setREFL_HALL(std::string);
};

} //phaser

#endif
