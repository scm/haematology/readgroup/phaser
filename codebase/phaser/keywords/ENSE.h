//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ENSEClass__
#define __ENSEClass__
#include <phaser/io/CCP4base.h>
#include <phaser/mr_objects/data_pdb.h>

namespace phaser {

class ENSE : public InputBase, virtual public CCP4base
{
  public:
    map_str_pdb PDB;
    MapEnsPtr ensemble;
    MapTrcPtr tracemol;

    ENSE();
    virtual ~ENSE() {}

  private:
    void initENSE_PDB(std::string);

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addENSE_ATOM(std::string,std::string,double);
    void addENSE_HELI(std::string,double,double);
    void addENSE_STR_RMS(std::string,std::string,std::string,floatType);
    void addENSE_STR_ID(std::string,std::string,std::string,floatType);
    void addENSE_STR_RMS_PTGR(std::string,std::string,std::string,floatType,data_ptgrp);
    void addENSE_STR_ID_PTGR(std::string,std::string,std::string,floatType,data_ptgrp);
    void addENSE_MAP(std::string,std::string,std::string,std::string,dvect3,double,dvect3,double,double,double);
    void setENSE_OCC(std::string,floatType);
    void setENSE_BFAC_ZERO(std::string,bool);
    void setENSE_DISA_CHEC(std::string,bool);
    void setENSE_DISA_GYRE(std::string,bool);
    void setENSE_HETA(std::string,bool);

    void addENSE_CIF_RMS(std::string a,std::string b,floatType c)  { return addENSE_PDB_RMS(a,b,c,false); }
    void addENSE_CIF_ID(std::string a,std::string b,floatType c)   { return addENSE_PDB_ID(a,b,c,false); }

    void setENSE_PTGR_COVE(std::string,double);
    void setENSE_PTGR_IDEN(std::string,double);
    void setENSE_PTGR_RMSD(std::string,double);
    void setENSE_PTGR_TOLA(std::string,double);
    void setENSE_PTGR_TOLS(std::string,double);

    void setENSE_BINS(std::string,data_bins);
    void setENSE_BINS_MINI(std::string,double);
    void setENSE_BINS_MAXI(std::string,double);
    void setENSE_BINS_WIDT(std::string,double);

    void setENSE_ESTI(std::string,std::string);

    void addENSE_PDB_RMS_SELECT(std::string,std::string,floatType,bool=true,std::string="",int=-999);
    void addENSE_PDB_ID_SELECT(std::string,std::string,floatType,bool=true,std::string="",int=-999);
    void addENSE_PDB_CARD_SELECT(std::string,std::string,bool,std::string="",int=-999);

    //python backwards compatible
    void addENSE_PDB_RMS(std::string,std::string,floatType,bool=true);
    void addENSE_PDB_ID(std::string,std::string,floatType,bool=true);
    void addENSE_PDB_CARD(std::string,std::string,bool);

    void setENSE_TRAC_PDB(std::string,std::string);
    void setENSE_TRAC_CIF(std::string,std::string);

    void setENSE_TRAC_SAMP_DIST(std::string,double);
    void setENSE_TRAC_SAMP_MIN(std::string,double);
    void setENSE_TRAC_SAMP_TARG(std::string,double);
    void setENSE_TRAC_SAMP_RANG(std::string,double);
    void setENSE_TRAC_SAMP_USE(std::string,std::string);
    void setENSE_TRAC_SAMP_WANG(std::string,double);
    void setENSE_TRAC_SAMP_ASA(std::string,double);
    void setENSE_TRAC_SAMP_NCYC(std::string,double);

    void setENSE_MAP_PDB(map_str_pdb PDB_) { PDB = PDB_; }
};

} //phaser

#endif
