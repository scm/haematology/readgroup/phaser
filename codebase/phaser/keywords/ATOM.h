//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ATOMClass__
#define __ATOMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/ep_objects/ep_solution.h>
#include <iotbx/pdb/hierarchy.h>

namespace phaser {

class ATOM : public InputBase, virtual public CCP4base
{
  public:
    ep_solution  ATOM_SET;
    bool ATOM_CHANGE_BFAC_WILSON;
    bool ATOM_CHANGE_ORIG;
    std::string ATOM_XTALID;

    ATOM();
    virtual ~ATOM() {}

  private:
    bool ATOM_CHANGE_SCAT;
    std::string ATOM_SCAT;
    int T;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setATOM_IOTBX(std::string,iotbx::pdb::hierarchy::root);
    void setATOM_PDB(std::string,std::string);
    void setATOM_STR(std::string,string1D);
    void setATOM_HA(std::string,std::string);
    void incATOM_SET();
    void addATOM(std::string,std::string,floatType,floatType,floatType,floatType);
    void addATOM_FULL(std::string,std::string,bool,dvect3,floatType,bool,floatType,bool,dmat6,bool,bool,bool,bool,std::string);
    void addATOM_FAST(std::string,std::string, bool,double,double,double, double,double);
    void setATOM_CHAN_BFAC_WILS(bool);
    void setATOM_CHAN_SCAT(bool);
    void setATOM_CHAN_ORIG(bool);
    void setATOM_CHAN_SCAT_TYPE(std::string);

    void setATOM(ep_solution& set) { ATOM_SET = set; }
};

} //phaser

#endif
