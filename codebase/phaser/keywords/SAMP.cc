//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SAMP.h>

namespace phaser {

SAMP::SAMP() : CCP4base(), InputBase()
{
  Add_Key("SAMP");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  ROT_SAMPLING = TRA_SAMPLING = 0;
}

std::string SAMP::unparse()
{
  std::string Card("");
  if (ROT_SAMPLING) Card += "SAMPLING ROTATION " + dtos(ROT_SAMPLING) + "\n";
  if (TRA_SAMPLING) Card += "SAMPLING TRANSLATION " + dtos(TRA_SAMPLING) + "\n";
  return Card;
}

Token_value SAMP::parse(std::istringstream& input_stream)
{
  int key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  while (optionalKey(input_stream,2,"ROT","TRA"))
    if (keyIs("ROT")) setSAMP_ROTA(get1num(input_stream));
    else if (keyIs("TRA")) setSAMP_TRAN(get1num(input_stream));
  setKeyLength(key_len);
  return skip_line(input_stream);
}

void SAMP::setSAMP_ROTA(floatType sampling)
{
  ROT_SAMPLING = iszeropos(sampling);
  if (ROT_SAMPLING > 360) throw PhaserError(INPUT,keywords,"Sampling for rotation angle > 360 degrees");
}

void SAMP::setSAMP_TRAN(floatType sampling)
{ TRA_SAMPLING = iszeropos(sampling); }

void SAMP::analyse(void)
{
}

}//phaser
