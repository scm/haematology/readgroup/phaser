//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ENM.h>
#include <phaser_defaults.h>

namespace phaser {

ENM::ENM() : CCP4base(), InputBase()
{
  Add_Key("ENM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  ELNEMO.METHOD = std::string(DEF_ENM_OSCI);
  ELNEMO.RADIUS = floatType(DEF_ENM_RADI);
  ELNEMO.FORCE = floatType(DEF_ENM_FORC);
  ELNEMO.NRES = int(DEF_ENM_NRES);
  ELNEMO.MAXBLOCKS = int(DEF_ENM_MAXB);
                       //This *should* be the same as or less than the large_matrix number used
                       //to generate warnings, although the two are not strictly linked.
                       //If phaser automatically generates block sizes that give warnings,
                       //it is not good.
}

std::string ENM::unparse()
{
  std::string Card("");
  if (ELNEMO.METHOD != std::string(DEF_ENM_OSCI) ||
      ELNEMO.NRES != int(DEF_ENM_NRES) ||
      ELNEMO.MAXBLOCKS != int(DEF_ENM_MAXB))
  {
  Card += "ENMETHOD OSCILLATORS " + ELNEMO.METHOD;
  if (ELNEMO.METHOD == "RTB")
  {
    if (ELNEMO.NRES) Card += " NRES " + itos(ELNEMO.NRES);
    Card += " MAXBLOCKS " + itos(ELNEMO.MAXBLOCKS);
  }
  Card += "\n";
  }
  if (ELNEMO.RADIUS != floatType(DEF_ENM_RADI))
  Card += "ENMETHOD RADIUS " + dtos(ELNEMO.RADIUS) + "\n";
  if (ELNEMO.FORCE != floatType(DEF_ENM_FORC))
  Card += "ENMETHOD FORCE " + dtos(ELNEMO.FORCE) + "\n";
  return Card;
}

Token_value ENM::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,5,"OSCILLATORS","MAXBLOCKS","NRES","RADIUS","FORCE"))
  {
    if (keyIs("OSCILLATORS"))
    {
      compulsoryKey(input_stream,2,"RTB","ALL");
      if (keyIs("RTB")) setENM_OSCI("RTB");
      if (keyIs("ALL")) setENM_OSCI("ALL");
    }
    if (keyIs("MAXBLOCKS")) setENM_RTB_MAXB(get1num(input_stream));
    if (keyIs("NRES"))      setENM_RTB_NRES(get1num(input_stream));
    if (keyIs("RADIUS"))    setENM_RADI(get1num(input_stream));
    if (keyIs("FORCE"))     setENM_FORC(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void ENM::setENM_OSCI(std::string osci)   { ELNEMO.METHOD = stoup(osci); }
void ENM::setENM_RADI(floatType radius)   { ELNEMO.RADIUS = ispos(radius); }
void ENM::setENM_FORC(floatType force)    { ELNEMO.FORCE = ispos(force); }
void ENM::setENM_RTB_NRES(floatType nres) { ELNEMO.NRES = iszeroposi(nres); }
void ENM::setENM_RTB_MAXB(floatType maxb) { ELNEMO.MAXBLOCKS = isposi(maxb); }

void ENM::analyse(void)
{
  if (   ELNEMO.METHOD != "RTB"
      && ELNEMO.METHOD != "ALL"
      ) CCP4base::store(PhaserError(INPUT,keywords,"ENM oscillators not recognised"));
}

} //phaser
