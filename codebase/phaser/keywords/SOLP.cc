//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SOLP.h>
#include <phaser_defaults.h>

namespace phaser {

SOLP::SOLP() : CCP4base(), InputBase()
{
  Add_Key("SOLP");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string SOLP::unparse()
{
  return SOLPAR.unparse();
}

Token_value SOLP::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"SIGA","BULK");
  if (keyIs("SIGA"))
  {
    while (optionalKey(input_stream,3,"FSOL","BSOL","MIN"))
    {
      if (keyIs("FSOL")) setSOLP_SIGA_FSOL(get1num(input_stream));
      if (keyIs("BSOL")) setSOLP_SIGA_BSOL(get1num(input_stream));
      if (keyIs("MIN"))  setSOLP_SIGA_MIN(get1num(input_stream));
    }
  }
  else if (keyIs("BULK"))
  {
    while (optionalKey(input_stream,3,"FSOL","BSOL","USE"))
    {
      if (keyIs("FSOL")) setSOLP_BULK_FSOL(get1num(input_stream));
      if (keyIs("BSOL")) setSOLP_BULK_BSOL(get1num(input_stream));
      if (keyIs("USE"))  setSOLP_BULK_USE(getBoolean(input_stream));
    }
  }
  return skip_line(input_stream);
}

void SOLP::setSOLP_SIGA_FSOL(floatType fsol) { SOLPAR.SIGA_FSOL = ispos(fsol); }
void SOLP::setSOLP_SIGA_BSOL(floatType bsol) { SOLPAR.SIGA_BSOL = ispos(bsol); }
void SOLP::setSOLP_SIGA_MIN(floatType mmm)   { SOLPAR.SIGA_MIN = ispos(mmm); }
void SOLP::setSOLP_BULK_FSOL(floatType fsol) { SOLPAR.BULK_FSOL = ispos(fsol); }
void SOLP::setSOLP_BULK_BSOL(floatType bsol) { SOLPAR.BULK_BSOL = ispos(bsol); }
void SOLP::setSOLP_BULK_USE(bool b)          { SOLPAR.BULK_USE = b; }

void SOLP::analyse(void)
{
}

}//phaser
