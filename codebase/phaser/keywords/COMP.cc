//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/COMP.h>
#include <phaser/lib/fasta2str.h>
#include <phaser/lib/scattering.h>
#include <phaser/src/Composition.h>

namespace phaser {

COMP::COMP() : CCP4base(), InputBase()
{
  Add_Key("COMP");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  COMPOSITION.BY = "ASU";
}

std::string COMP::unparse()
{
  return COMPOSITION.unparse();
}

Token_value COMP::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,7,"BY","PERCENTAGE","PROTEIN","NUCLEIC","ATOM","INCREASE","MIN"))
  {
    if (keyIs("BY"))
    {
      compulsoryKey(input_stream,3,"AVERAGE","SOLVENT","ASU");
      if (keyIs("AVERAGE")) setCOMP_AVER();
      if (keyIs("SOLVENT")) setCOMP_SOLV();
      if (keyIs("ASU")) setCOMP_ASU();
    }
    if (keyIs("PERCENTAGE"))
      setCOMP_PERC(get1num(input_stream));
    if (keyIs("INCREASE"))
      setCOMP_INCR(get1num(input_stream));
    if (keyIs("PROTEIN") || keyIs("NUCLEIC"))
    {
      std::string TYPE;
      if (keyIs("PROTEIN")) TYPE = "PROTEIN";
      if (keyIs("NUCLEIC")) TYPE = "NUCLEIC";
      compulsoryKey(input_stream,4,"MW","SEQ","NRES","STR");
      if (keyIs("MW"))
      {
        double mw = get1num(input_stream);
        compulsoryKey(input_stream,1,"NUM");
        double num = get1num(input_stream);
        addCOMP_TYPE_MW_NUM(TYPE,mw,num);
      }
      else if (keyIs("SEQ"))
      {
        std::string seq = getFileName(input_stream);
        compulsoryKey(input_stream,1,"NUM");
        double num = get1num(input_stream);
        addCOMP_TYPE_SEQ_NUM(TYPE,seq,num);
      }
      else if (keyIs("NRES"))
      {
        double nres = get1num(input_stream);
        compulsoryKey(input_stream,1,"NUM");
        double num = get1num(input_stream);
        addCOMP_TYPE_NRES_NUM(TYPE,nres,num);
      }
      else if (keyIs("STR"))
      {
        std::string seq = getString(input_stream);
        compulsoryKey(input_stream,1,"NUM");
        double num = get1num(input_stream);
        addCOMP_TYPE_STR_NUM(TYPE,seq,num);
      }
    }
    if (keyIs("ATOM"))
    {
      std::string atomtype(getString(input_stream));
      compulsoryKey(input_stream,1,"NUM");
      addCOMP_ATOM_NUM(atomtype,get1num(input_stream));
    }
    if (keyIs("MIN"))
    {
      compulsoryKey(input_stream,1,"SOLVENT");
      setCOMP_MINI_SOLV(get1num(input_stream));
    }
  }
  return skip_line(input_stream);
}

void COMP::setCOMP_AVER() { COMPOSITION.BY = "AVERAGE"; }
void COMP::setCOMP_SOLV() { COMPOSITION.BY = "SOLVENT"; }
void COMP::setCOMP_ASU() { COMPOSITION.BY = "ASU"; }
void COMP::setCOMP_MINI_SOLV(double solvent) { COMPOSITION.MIN_SOLVENT = isperc(solvent); }
void COMP::setCOMP_BY(std::string by)
{
  if (stoup(by) == "AVERAGE") COMPOSITION.BY = "AVERAGE";
  else if (stoup(by) == "SOLVENT") COMPOSITION.BY = "SOLVENT";
  else if (stoup(by) == "ASU") COMPOSITION.BY = "ASU";
  else CCP4base::store(PhaserError(INPUT,keywords,"Composition type not recongnised"));
}

void COMP::addCOMP_TYPE_MW_NUM(std::string TYPE,double mw,double num)
{
  data_asu COMPONENT(TYPE,isposi(num));
  COMPONENT.MW = ispos(mw);
  COMPOSITION.ASU.push_back(COMPONENT);
}

void COMP::addCOMP_TYPE_SEQ_NUM(std::string TYPE,std::string seqfile,double num)
{
  std::ifstream infile(const_cast<char*>(seqfile.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,seqfile));
  data_asu COMPONENT(TYPE,isposi(num));
  COMPONENT.SEQ = seqfile;
  COMPOSITION.ASU.push_back(COMPONENT);
}

void COMP::addCOMP_TYPE_STR_NUM(std::string TYPE,std::string seq,double num)
{
  data_asu COMPONENT(TYPE,isposi(num));
  if (!seq.size())
  CCP4base::store(PhaserError(INPUT,keywords,"Composition sequence string empty"));
  COMPONENT.STR = seq;
  COMPOSITION.ASU.push_back(COMPONENT);
}

void COMP::addCOMP_TYPE_NRES_NUM(std::string TYPE,double nres,double num)
{
  data_asu COMPONENT(TYPE,isposi(num));
  COMPONENT.NRES = isposi(nres);
  COMPOSITION.ASU.push_back(COMPONENT);
}

void COMP::addCOMP_ATOM_NUM(std::string atomtype,double num)
{
  if (!isElement(atomtype))
  CCP4base::store(PhaserError(INPUT,keywords,"Atom type \"" + atomtype + "\" not recognised in Composition"));
  data_asu COMPONENT("ATOM",isposi(num));
  COMPONENT.STR = atomtype;
  COMPOSITION.ASU.push_back(COMPONENT);
}

void COMP::setCOMP_PERC(double solvent)
{
  COMPOSITION.PERCENTAGE = isperc(solvent);
}

void COMP::setCOMP_INCR(double factor)
{
  COMPOSITION.INCREASE = ispos(factor);
}

void COMP::analyse(void)
{
  if (COMPOSITION.BY == "ASU" && !COMPOSITION.ASU.size())
    COMPOSITION.BY = "AVERAGE"; //if there is no input scattering, switch default to AVERAGE
  for (int c = 0; c < COMPOSITION.ASU.size(); c++)
  {
    bool seq_error = COMPOSITION.ASU[c].check_seq();
    if (seq_error) CCP4base::store(PhaserError(INPUT,keywords,"Error interpreting composition sequence"));
  }
  bool fakescat = (Composition(COMPOSITION).total_scat("P 1",af::double6(100,100,10,90,90,90)));
  if (!fakescat) CCP4base::store(PhaserError(INPUT,keywords,"No scattering in composition"));
}

} //phaser
