//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACG.h>

namespace phaser {

MACG::MACG() : CCP4base(), InputBase()
{
  Add_Key("MACG");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_GYRE.clear();
  MACG_SIGR = -999;
  MACG_SIGT = -999;
  MACG_ANCH = bool(DEF_MACG_ANCHOR);
}

std::string MACG::unparse()
{
  std::string Card("");
  if (!MACG_PROTOCOL.is_default() && !MACG_PROTOCOL.is_custom())
    Card += "MACGR PROTOCOL " + MACG_PROTOCOL.unparse() + "\n";
  if (MACG_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_GYRE.size() != 1) //not default
    {
      Card += "MACGR PROTOCOL " + MACG_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_GYRE.size(); i++)
        Card += MACRO_GYRE[i]->unparse();
    }
    else if (!MACRO_GYRE[0]->is_default() ) //not default
    {
      Card += "MACGR PROTOCOL " + MACG_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_GYRE.size(); i++)
        Card += MACRO_GYRE[i]->unparse();
    }
  }
  return Card;
}

Token_value MACG::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,6,"PROTOCOL","ROT","SIGROT","SIGTRA","ANCHOR","CHAIN");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACG_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACG_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACG_PROT("OFF");
    if (keyIs("ALL"))     setMACG_PROT("ALL");
  }
  if (keyIs("ROT"))
  {
    bool ref_rot = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"TRA");
    bool ref_tra = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"VRMS");
    bool ref_vrms = getBoolean(input_stream);
    bool anchor = bool(DEF_MACG_ANCHOR);
    int ncyc(-999);
    double sigrot(-999),sigtra(-999);
    std::string minimizer("");
    while (optionalKey(input_stream,5,"ANCHOR","SIGROT","SIGTRA","NCYC","MINIMIZER"))
    {
      if (keyIs("ANCHOR")) anchor = getBoolean(input_stream);
      if (keyIs("SIGROT")) sigrot = get1num(input_stream);
      if (keyIs("SIGTRA")) sigtra = get1num(input_stream);
      if (keyIs("NCYC")) ncyc = get1num(input_stream);
      if (keyIs("MINIMIZER")) minimizer = getString(input_stream);
    }
    addMACG_FULL(ref_rot,ref_tra,ref_vrms,anchor,sigrot,sigtra,ncyc,minimizer);
  }
  if (keyIs("SIGROT"))
  {
    setMACG_SIGR(get1num(input_stream));
    while (optionalKey(input_stream,2,"SIGTRA","ANCHOR"))
    {
      if (keyIs("SIGTRA")) setMACG_SIGT(get1num(input_stream));
      if (keyIs("ANCHOR")) setMACG_ANCH(getBoolean(input_stream));
    }
  }
  if (keyIs("SIGTRA"))
  {
    setMACG_SIGT(get1num(input_stream));
    while (optionalKey(input_stream,2,"SIGROT","ANCHOR"))
    {
      if (keyIs("SIGROT")) setMACG_SIGR(get1num(input_stream));
      if (keyIs("ANCHOR")) setMACG_ANCH(getBoolean(input_stream));
    }
  }
  if (keyIs("ANCHOR"))
  {
    setMACG_ANCH(getBoolean(input_stream));
    while (optionalKey(input_stream,2,"SIGROT","SIGTRA"))
    {
      if (keyIs("SIGROT")) setMACG_SIGR(get1num(input_stream));
      if (keyIs("SIGTRA")) setMACG_SIGT(get1num(input_stream));
    }
  }
  if (keyIs("CHAIN"))
  {
    std::string chain = getFileName(input_stream);
    compulsoryKey(input_stream,2,"SIGROT","SIGTRA");
    {
      if (keyIs("SIGROT")) setMACG_CHAI_SIGR(chain,get1num(input_stream));
      if (keyIs("SIGTRA")) setMACG_CHAI_SIGT(chain,get1num(input_stream));
    }
  }
  return skip_line(input_stream);
}

void MACG::addMACG_FULL(bool ref_rota,bool ref_tran,bool ref_vrms,bool anchor,double sigrot,double sigtra,int ncyc,std::string minimizer)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  protocolPtr cPtr(new ProtocolGYRE(!ref_rota,!ref_tran,!ref_vrms,anchor,sigrot,sigtra,ncyc,minimizer));
  MACRO_GYRE.push_back(cPtr);
}

void MACG::addMACG(bool ref_rota,bool ref_tran,bool ref_vrms)
{
  protocolPtr cPtr(new ProtocolGYRE(!ref_rota,!ref_tran,!ref_vrms));
  MACRO_GYRE.push_back(cPtr);
}

void MACG::setMACG_PROT(std::string method)
{
  bool error = MACG_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACG::setMACG_SIGR(double rot)  { MACG_SIGR = iszeropos(rot); }
void MACG::setMACG_SIGT(double tra)  { MACG_SIGT = iszeropos(tra); }
void MACG::setMACG_CHAI_SIGR(std::string chain,double rot)  { MACG_CHAIN_SIGR[chain] = iszeropos(rot); }
void MACG::setMACG_CHAI_SIGT(std::string chain,double tra)  { MACG_CHAIN_SIGT[chain] = iszeropos(tra); }
void MACG::setMACG_ANCH(bool anchor) { MACG_ANCH = anchor; }

void MACG::analyse(void)
{
  if (MACG_PROTOCOL.is_default() && MACRO_GYRE.size())
  {
    MACG_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACG_PROTOCOL.is_default() || (MACG_PROTOCOL.is_custom() && !MACRO_GYRE.size()))
  {
    MACRO_GYRE.clear();
    ProtocolGYRE Macro; //default
    { //cycle
      protocolPtr cPtr(new ProtocolGYRE(Macro));
      MACRO_GYRE.push_back(cPtr);
    }
/*
    { //cycle defined as REFINE not FIX
      Macro.cycle(true,false,false,0,0);
      protocolPtr cPtr(new ProtocolGYRE(Macro));
      MACRO_GYRE.push_back(cPtr);
    }
*/
  }
  else if (MACG_PROTOCOL.is_off())
  {
    MACRO_GYRE.clear();
    ProtocolGYRE offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolGYRE(offMacro));
    MACRO_GYRE.push_back(cPtr);
  }
  else if (MACG_PROTOCOL.is_all())
  {
    MACRO_GYRE.clear();
    ProtocolGYRE offMacro;
    offMacro.all();
    protocolPtr cPtr(new ProtocolGYRE(offMacro));
    MACRO_GYRE.push_back(cPtr);
  }
  //if some protocols have been set, then set the defaults
  for (unsigned big_cyc = 0 ; big_cyc < MACRO_GYRE.size(); big_cyc++)
  {
    //if the restraints are not set on parsing, then set the defaults here
    MACRO_GYRE[big_cyc]->setNUM(macg_sigr,MACG_SIGR);
    MACRO_GYRE[big_cyc]->setNUM(macg_sigt,MACG_SIGT);
    MACRO_GYRE[big_cyc]->setFIX(macg_anch,MACG_ANCH);
  }
  for (unsigned big_cyc = 0 ; big_cyc < MACRO_GYRE.size(); big_cyc++)
  {
    //if the restraints are not set on parsing, then set the defaults here
    MACRO_GYRE[big_cyc]->setMAP(macg_sigr,MACG_CHAIN_SIGR);
    MACRO_GYRE[big_cyc]->setMAP(macg_sigt,MACG_CHAIN_SIGT);
  }
}

}//phaser
