//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SPACClass__
#define __SPACClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class SPAC : public InputBase, virtual public CCP4base
{
  public:
    bool CHANGE_SG;
    std::string SG_HALL;

    SPAC();
    virtual ~SPAC() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSPAC_NUM(floatType);
    void setSPAC_NAME(std::string);
    void setSPAC_HALL(std::string);
};

} //phaser

#endif
