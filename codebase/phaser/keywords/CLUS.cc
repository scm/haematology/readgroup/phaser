//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/CLUS.h>
#include <phaser/lib/scattering.h>

namespace phaser {

CLUS::CLUS() : CCP4base(), InputBase()
{
  Add_Key("CLUS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  CLUSTER_PDB = "";
}

std::string CLUS::unparse()
{
  std::string Card("");
  if (CLUSTER_PDB != "") Card += "CLUSTER PDB \"" + CLUSTER_PDB + "\"\n";
  return Card;
}

Token_value CLUS::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,1,"PDB");
  std::string pdbfile = getFileName(input_stream);
  setCLUS_PDB(pdbfile);
  return skip_line(input_stream);
}

void CLUS::setCLUS_PDB(std::string pdbfile)
{
  std::ifstream infile(const_cast<char*>(pdbfile.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,pdbfile));
  CLUSTER_PDB = pdbfile;
}

void CLUS::analyse(void)
{
}


} //phaser
