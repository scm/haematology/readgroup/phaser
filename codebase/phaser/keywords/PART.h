//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PARTClass__
#define __PARTClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_part.h>
#include <iotbx/pdb/hierarchy.h>

namespace phaser {

class PART : public InputBase, virtual public CCP4base
{
  public:
    data_part PARTIAL;

    PART();
    virtual ~PART() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPART_HKLI(std::string);
    void setPART_LABI_FC(std::string);
    void setPART_LABI_PHIC(std::string);
    void setPART_PDB(std::string);
    void setPART_CIF(std::string);
    void setPART_PDB_STR(std::string);
    void setPART_CIF_STR(std::string);
    void setPART_VARI(std::string);
    void setPART_DEVI(floatType);
    void setPART_IOTBX(iotbx::pdb::hierarchy::root);
    void setPART_ANOM(bool);
    void setPART_ESTI(std::string);
};

} //phaser

#endif
