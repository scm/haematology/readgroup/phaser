//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/TITL.h>
#include <phaser_defaults.h>

namespace phaser {

TITL::TITL() : CCP4base(), InputBase()
{
  Add_Key("TITL");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  TITLE = std::string(DEF_TITL);
}

std::string TITL::unparse()
{
  if (TITLE == std::string(DEF_TITL)) return "";
  return "TITLE " + TITLE + "\n";
}

Token_value TITL::parse(std::istringstream& input_stream)
{
//parsing of this one different as we want everything to end of line
  setTITL(getLine(input_stream));
  return ENDLINE;
}

void TITL::setTITL(std::string title)
{
  TITLE = title;
}

void TITL::analyse(void)
{
}


} //phaser
