//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/TNCS.h>

namespace phaser {

TNCS::TNCS() : CCP4base(), InputBase()
{
  Add_Key("TNCS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_TNCS_RLIST_ADD.Default(DEF_TNCS_RLIS_ADD);
}

std::string TNCS::unparse()
{
  std::string Card = PTNCS.unparse();
  if (DO_TNCS_RLIST_ADD.unparse() != "") Card += "TNCS RLIST ADD " + DO_TNCS_RLIST_ADD.unparse() + "\n";
  if (PTNCS.READ && PTNCS.FILENAME.size()) Card += "TNCS EPSFAC READ \"" + PTNCS.FILENAME + "\"\n";
  if (!PTNCS.READ && PTNCS.FILENAME.size()) Card += "TNCS EPSFAC WRITE \"" + PTNCS.FILENAME + "\"\n";
  return Card;
}

Token_value TNCS::parse(std::istringstream& input_stream)
{
  int key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  while (optionalKey(input_stream,13,"USE","ROT","TRA","VAR","GFUNCTION","NMOL","MAXNMOL","LINK","PAIR","PATT","RLIST","EPSFAC","COMMENSURATE"))
  {
  if (keyIs("USE")) setTNCS_USE(getBoolean(input_stream));
  else if (keyIs("ROT"))
  {
    compulsoryKey(input_stream,3,"ANGLE","RANGE","SAMPLING");
    if (keyIs("ANGLE")) setTNCS_ROTA_ANGL(get3nums(input_stream));
    if (keyIs("RANGE")) setTNCS_ROTA_RANG(get1num(input_stream));
    if (keyIs("SAMPLING")) setTNCS_ROTA_SAMP(get1num(input_stream));
  }
  else if (keyIs("TRA"))
  {
    compulsoryKey(input_stream,2,"VECTOR","PERTURB");
    if (keyIs("VECTOR"))  setTNCS_TRAN_VECT(get3nums(input_stream));
    if (keyIs("PERTURB")) setTNCS_TRAN_PERT(getBoolean(input_stream));
  }
  else if (keyIs("VAR"))
  {
    compulsoryKey(input_stream,4,"RMSD","FRAC","BINS","RESO");
    if (keyIs("RMSD")) setTNCS_VARI_RMSD(get1num(input_stream));
    if (keyIs("FRAC")) setTNCS_VARI_FRAC(get1num(input_stream));
    if (keyIs("BINS"))
    {
      float1D shells;
      while (get_token(input_stream) == NUMBER)
        shells.push_back(number_value);
      setTNCS_VARI_BINS(shells);
      setKeyLength(key_len); //ROT TRA NUM
      return skip_line(input_stream);
    }
    if (keyIs("RESO"))
    {
      float1D shells;
      while (get_token(input_stream) == NUMBER)
        shells.push_back(number_value);
      setTNCS_VARI_RESO(shells);
      setKeyLength(key_len); //ROT TRA NUM
      return skip_line(input_stream);
    }
  }
  else if (keyIs("GFUNCTION"))
  {
    compulsoryKey(input_stream,1,"RADIUS");
    setTNCS_GFUN_RADI(get1num(input_stream));
  }
  else if (keyIs("NMOL")) setTNCS_NMOL(get1num(input_stream));
  else if (keyIs("MAXNMOL")) setTNCS_MAXN(get1num(input_stream));
  else if (keyIs("LINK"))
  {
    compulsoryKey(input_stream,2,"RESTRAINT","SIGMA");
    if (keyIs("RESTRAINT")) setTNCS_LINK_REST(getBoolean(input_stream));
    if (keyIs("SIGMA"))     setTNCS_LINK_SIGM(get1num(input_stream));
  }
  else if (keyIs("PAIR"))
  {
    compulsoryKey(input_stream,1,"ONLY");
    if (keyIs("ONLY")) setTNCS_PAIR_ONLY(getBoolean(input_stream));
  }
  else if (keyIs("PATT"))
  {
    compulsoryKey(input_stream,5,"HIRES","LORES","PERCENT","DISTANCE","MAPS");
    if (keyIs("HIRES")) setTNCS_PATT_HIRE(get1num(input_stream));
    if (keyIs("LORES")) setTNCS_PATT_LORE(get1num(input_stream));
    if (keyIs("PERCENT")) setTNCS_PATT_PERC(get1num(input_stream));
    if (keyIs("DISTANCE")) setTNCS_PATT_DIST(get1num(input_stream));
    if (keyIs("MAPS")) setTNCS_PATT_MAPS(getBoolean(input_stream));
  }
  else if (keyIs("RLIST"))
  {
    compulsoryKey(input_stream,1,"ADD");
    if (keyIs("ADD")) setTNCS_RLIS_ADD(getBoolean(input_stream));
  }
  else if (keyIs("COMM"))
  {
    setKeyLength(4); //TOLF TOLO
    compulsoryKey(input_stream,5,"PEAK","TOLF","TOLO","PATT","PERC");
    if (keyIs("PEAK")) setTNCS_COMM_PEAK(get1num(input_stream));
    if (keyIs("TOLF")) setTNCS_COMM_TOLF(get1num(input_stream));
    if (keyIs("TOLO")) setTNCS_COMM_TOLO(get1num(input_stream));
    if (keyIs("PATT")) setTNCS_COMM_PATT(get1num(input_stream));
    if (keyIs("MISS")) setTNCS_COMM_MISS(get1num(input_stream));
  }
  else if (keyIs("EPSFAC"))
  {
  //use setter from python
  //this is for scripging only
  compulsoryKey(input_stream,2,"READ","WRITE");
  PTNCS.READ = keyIs("READ"); //indeterminate of not set
  PTNCS.FILENAME = getFileName(input_stream); //binary file setting
  }
  }
  setKeyLength(key_len); //ROT TRA NUM
  return skip_line(input_stream);
}

void TNCS::setTNCS_USE(bool use) { PTNCS.USE = use; }
void TNCS::setTNCS_GFUN_RADI(floatType radius) { PTNCS.GFUNCTION_RADIUS = iszeropos(radius); }
void TNCS::setTNCS_NMOL(floatType nmol) { PTNCS.NMOL = iszeroposi(nmol); }
void TNCS::setTNCS_MAXN(floatType nmol) { PTNCS.MAXNMOL = iszeroposi(nmol); }
void TNCS::setTNCS_LINK_REST(bool link) { PTNCS.EP.LINK_RESTRAINT = link; }
void TNCS::setTNCS_LINK_SIGM(floatType link) { PTNCS.EP.LINK_SIGMA = iszeropos(link); }
void TNCS::setTNCS_PAIR_ONLY(bool b) { PTNCS.EP.PAIR_ONLY = b; }
void TNCS::setTNCS_ROTA_RANG(floatType range) { PTNCS.ROT.RANGE = (range); }
void TNCS::setTNCS_ROTA_SAMP(floatType samp) { PTNCS.ROT.SAMPLING = (samp); }
void TNCS::setTNCS_VARI_RMSD(floatType rms) {PTNCS.VAR.RMSD = rms; }
void TNCS::setTNCS_VARI_FRAC(floatType frac) {PTNCS.VAR.FRAC = frac; }
void TNCS::setTNCS_VARI_BINS(float1D vrms) {PTNCS.VAR.BINS = vrms; }
void TNCS::setTNCS_VARI_RESO(float1D reso) {PTNCS.VAR.RESO = reso; }
void TNCS::setTNCS_PATT_HIRE(floatType hi) {PTNCS.TRA.PATT_HIRES = iszeropos(hi); }
void TNCS::setTNCS_PATT_MAPS(bool b) {PTNCS.TRA.PATT_MAPS = b; }
void TNCS::setTNCS_PATT_LORE(floatType lo) {PTNCS.TRA.PATT_LORES = iszeropos(lo); }
void TNCS::setTNCS_PATT_PERC(floatType perc) {PTNCS.TRA.PATT_PERCENT = isperc(perc); }
void TNCS::setTNCS_PATT_DIST(floatType minv) {PTNCS.TRA.PATT_DIST = ispos(minv); }
void TNCS::setTNCS_RLIS_ADD(bool add) {DO_TNCS_RLIST_ADD.Set(add); }
void TNCS::setTNCS_ROTA_ANGL(dvect3 rot)  {PTNCS.ROT.ANGLE=rot;}
void TNCS::setTNCS_TRAN_PERT(bool pert)   {PTNCS.TRA.PERTURB = pert;}

void TNCS::setTNCS_COMM_PEAK(double x) { PTNCS.COMM.PEAK = isperc(x); }
void TNCS::setTNCS_COMM_TOLF(double x) { PTNCS.COMM.TOLF = ispos(x); }
void TNCS::setTNCS_COMM_TOLO(double x) { PTNCS.COMM.TOLO = ispos(x); }
void TNCS::setTNCS_COMM_PATT(double x) { PTNCS.COMM.PATT = isperc(x); }
void TNCS::setTNCS_COMM_MISS(double x) { PTNCS.COMM.MISS = iszeropos(x); }

void TNCS::setTNCS_TRAN_VECT(dvect3 tra)
{
  if (tra != dvect3(DEF_TNCS_TRAN_VECT))
    PTNCS.TRA.VECTOR_SET = true;
  PTNCS.TRA.VECTOR = tra;
}

void TNCS::analyse(void)
{
  if (PTNCS.FILENAME.size() && PTNCS.READ)
  {
    CCP4base::store(PhaserError(NULL_ERROR,keywords,"tNCS parameters read from binary file \"" + PTNCS.FILENAME + "\""));
  }
}

} //phaser
