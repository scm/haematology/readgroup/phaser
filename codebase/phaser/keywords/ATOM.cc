//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ATOM.h>
#include <phaser/src/Molecule.h>
#include <phaser/lib/aniso.h>
#include <phaser/lib/scattering.h>
#include <phaser/mr_objects/data_model.h>
#include <cctbx/adptbx.h>
#include <phaser_defaults.h>

namespace phaser {

ATOM::ATOM() : CCP4base(), InputBase()
{
  Add_Key("ATOM");
  //Add to CCP4base
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  ATOM_CHANGE_BFAC_WILSON = bool(DEF_ATOM_CHAN_BFAC);
  ATOM_CHANGE_ORIG = bool(DEF_ATOM_CHAN_ORIG);
  ATOM_CHANGE_SCAT = bool(DEF_ATOM_CHAN_SCAT);
  ATOM_SCAT = std::string(DEF_ATOM_SCAT_TYPE);
  ATOM_XTALID = "";
  T = 0;
}

std::string ATOM::unparse()
{
  std::string Card("");
  Card += ATOM_SET.unparse(ATOM_XTALID);
  if (ATOM_CHANGE_BFAC_WILSON != bool(DEF_ATOM_CHAN_BFAC))
    Card += "ATOM CHANGE BFACTOR WILSON " + card(ATOM_CHANGE_BFAC_WILSON) + "\n";
  if (ATOM_CHANGE_ORIG != bool(DEF_ATOM_CHAN_ORIG))
    Card += "ATOM CHANGE ORIGINAL " + card(ATOM_CHANGE_ORIG) + "\n";
  if (ATOM_CHANGE_SCAT && ATOM_SCAT.size()) Card += "ATOM CHANGE SCATTERER " + ATOM_SCAT + "\n";
  return Card;
}

Token_value ATOM::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"CRYSTAL","CHANGE");
  if (keyIs("CRYSTAL"))
  {
    std::string xtalid = getString(input_stream);
    compulsoryKey(input_stream,6,"SET","PDB","HA","CLUSTER","ELEMENT","TYPE");
    if (keyIs("SET"))       incATOM_SET();
    if (keyIs("PDB"))       setATOM_PDB(xtalid,getFileName(input_stream));
    if (keyIs("HA"))        setATOM_HA(xtalid,getFileName(input_stream));
    if (keyIs("ELEMENT") || keyIs("CLUSTER") || keyIs("TYPE"))
    {
      std::string atomtype =  getString(input_stream);
      compulsoryKey(input_stream,2,"FRACTIONAL","ORTHOGONAL");
      bool frac = keyIs("FRACTIONAL");
      dvect3 xyz = get3nums(input_stream);
      compulsoryKey(input_stream,1,"OCC");
      floatType occ = get1num(input_stream);
      floatType Biso(0);
      dmat6 Uano(0,0,0,0,0,0);
      bool anisotropic_flag(false),u_star_flag(false);
      bool fixX(false),fixO(false),fixB(false),Bswap(false);
      std::string label("");
      while (optionalKey(input_stream,8,"ISOB","ANOU","USTAR","FIXX","FIXO","FIXB","BSWAP","LABEL"))
      {
        if (keyIs("ISOB")) Biso = get1num(input_stream);
        else if (keyIs("ANOU"))
        {
          Uano = get6nums(input_stream);
          anisotropic_flag = true;
        }
        else if (keyIs("USTAR"))
        {
          Uano = get6nums(input_stream);
          anisotropic_flag = true;
          u_star_flag = true;
        }
        if (keyIs("FIXX")) fixX = getBoolean(input_stream);
        if (keyIs("FIXO")) fixO = getBoolean(input_stream);
        if (keyIs("FIXB")) fixB = getBoolean(input_stream);
        if (keyIs("BSWAP")) Bswap = getBoolean(input_stream);
        if (keyIs("LABEL"))
        {
          label = getLine(input_stream);
          addATOM_FULL(xtalid,atomtype,frac,xyz,occ,anisotropic_flag,Biso,u_star_flag,Uano,fixX,fixO,fixB,Bswap,label);
          return ENDLINE; //must be the last thing on the line
        }
      }
      addATOM_FULL(xtalid,atomtype,frac,xyz,occ,anisotropic_flag,Biso,u_star_flag,Uano,fixX,fixO,fixB,Bswap,label);
    }
  }
  else if (keyIs("CHANGE"))
  {
    compulsoryKey(input_stream,3,"SCATTERER","BFACTOR","ORIGINAL");
    if (keyIs("SCATTERER"))
    {
      setATOM_CHAN_SCAT(true);
      setATOM_CHAN_SCAT_TYPE(getString(input_stream));
    }
    if (keyIs("BFACTOR"))
    {
      compulsoryKey(input_stream,1,"WILSON");
      setATOM_CHAN_BFAC_WILS(getBoolean(input_stream));
    }
    if (keyIs("ORIGINAL"))
    {
      setATOM_CHAN_ORIG(getBoolean(input_stream));
    }
  }
  return skip_line(input_stream);
}

void ATOM::addATOM_FULL(std::string xtalid,std::string atomtype,bool frac,dvect3 xyz,floatType occ,bool anisotropic_flag,floatType Biso,bool u_star_flag,dmat6 Uano,bool fixX,bool fixO,bool fixB,bool Bswap,std::string label)
{
  ATOM_XTALID = xtalid;
  //set if first atom
  data_atom new_atom;
  new_atom.FRAC = frac;
  new_atom.FIXX = fixX;
  new_atom.FIXO = fixO;
  new_atom.FIXB = fixB;
  new_atom.BSWAP = Bswap;
  //new_atom.USTAR = u_star_flag;
  new_atom.ORIG = true;

  //if isotropic, input B can be converted to stored u_iso without knowing unitcell
  //if anisotropic, want the parsing format to be the same as the pdb format i.e. u_cart*10000
  //therefore store this in u_star temporarily until we know the unitcell
  //then convert u_cart*10000 to u_star
  //Can also input ustar directly, for unparse compatibility
  new_atom.SCAT.scattering_type = stoup(atomtype);
  new_atom.SCAT.site = xyz;
  new_atom.SCAT.occupancy = occ;
  if (!anisotropic_flag)
  {
    new_atom.SCAT.u_iso = cctbx::adptbx::b_as_u(Biso);
    new_atom.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
  }
  else if (anisotropic_flag)
  {
    new_atom.SCAT.u_star = Uano; //temporary storage
    new_atom.SCAT.set_use_u(/* iso */ false, /* aniso */ true);
    new_atom.SCAT.u_iso = 0;
  }
  new_atom.SCAT.label = label;

  if (!ATOM_SET.size()) ATOM_SET.resize(1); //initialize
  ATOM_SET[T].push_back(new_atom);
}

void ATOM::addATOM_FAST(std::string xtalid,std::string atomtype,bool frac,double x,double y, double z,double occ,double Biso)
{
  ATOM_XTALID = xtalid;
  //set if first atom
  data_atom new_atom;
  new_atom.FRAC = frac;
  new_atom.FIXX = false;
  new_atom.FIXO = false;
  new_atom.FIXB = false;
  new_atom.BSWAP = false;
  //new_atom.USTAR = u_star_flag;
  new_atom.ORIG = true;
  new_atom.SCAT.scattering_type = stoup(atomtype);
  new_atom.SCAT.site = dvect3(x,y,z);
  new_atom.SCAT.occupancy = occ;
  new_atom.SCAT.u_iso = cctbx::adptbx::b_as_u(Biso);
  new_atom.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
  new_atom.SCAT.label = "";
  if (!ATOM_SET.size()) ATOM_SET.resize(1); //initialize
  ATOM_SET[T].push_back(new_atom);
}

void ATOM::incATOM_SET()
{
  if (ATOM_SET.size()) T++;
  af_atom new_atoms;
  ATOM_SET.push_back(new_atoms);
}

void ATOM::addATOM(std::string xtalid,std::string atom_type,floatType x,floatType y,floatType z,floatType occ)
{
  ATOM_XTALID = xtalid;
  //short format for python interface
  data_atom new_atom; //all defaults

  new_atom.SCAT = cctbx::xray::scatterer<double>(
    "", // label
    dvect3(x,y,z), // site
    0, // u_iso
    occ, // occupancy
    stoup(atom_type), // scattering_type
    0, // fp
    0); // fdp
  new_atom.ORIG = true;

  //map, creates own entry if it doesn't exist
  if (!ATOM_SET.size()) ATOM_SET.resize(1); //initialize
  ATOM_SET[T].push_back(new_atom);
}

void ATOM::setATOM_IOTBX(std::string xtalid,iotbx::pdb::hierarchy::root iotbx_input)
{
  ATOM_XTALID = xtalid;
  af_atom  new_atoms;
  for (int a = 0; a < iotbx_input.atoms_sequential_conf().size(); a++)
  {
    std::string element("  ");
    element[0] = iotbx_input.atoms_sequential_conf()[a].data->element.elems[0];
    element[1] = iotbx_input.atoms_sequential_conf()[a].data->element.elems[1];
    data_atom new_atom;
    new_atom.SCAT = cctbx::xray::scatterer<double>(
      "", // label
      iotbx_input.atoms_sequential_conf()[a].data->xyz, // site
      iotbx_input.atoms_sequential_conf()[a].data->uij, // u_iso
      iotbx_input.atoms_sequential_conf()[a].data->occ, // occupancy
      stoup(element), // scattering_type
      0, // fp
      0); // fdp
    new_atom.ORIG = true;
    //map, creates own entry if it doesn't exist
    new_atoms.push_back(new_atom);
  }
  ATOM_SET.push_back(new_atoms);
}

void ATOM::setATOM_PDB(std::string xtalid,std::string pdbfile)
{
  ATOM_XTALID = xtalid;
  std::ifstream infile(const_cast<char*>(pdbfile.c_str()));
  if (!infile)
  {
    data_model model;
    model.init_string(pdbfile,"atom","PDB","RMS",0);
    Molecule pdb(model,true);
    if (pdb.read_errors() == "No scattering in coordinate file")
      CCP4base::store(PhaserError(FILEOPEN,keywords,pdbfile));
    else if (pdb.read_errors() != "")
       CCP4base::store(PhaserError(INPUT,keywords,pdb.read_errors()));
    ATOM_SET.push_back(pdb.getAtomArrays());
    return;
  }
  //some things can not be set when using pdb format
  data_model model;
  model.init_filename(pdbfile,"PDB","RMS",0);
  Molecule pdb(model,true);
  if (pdb.read_errors() != "") CCP4base::store(PhaserError(INPUT,keywords,pdb.read_errors()));
  ATOM_SET.push_back(pdb.getAtomArrays());
}

void ATOM::setATOM_STR(std::string xtalid,string1D pdbfiles)
{
  ATOM_XTALID = xtalid;
  for (int t = 0; t < pdbfiles.size(); t++)
  {
    data_model model;
    model.init_string(pdbfiles[t],"atom","PDB","RMS",0);
    Molecule pdb(model,true);
    ATOM_SET.push_back(pdb.getAtomArrays());
  }
}

void ATOM::setATOM_HA(std::string xtalid,std::string hafile)
{
  ATOM_XTALID = xtalid;
  std::ifstream infile(const_cast<char*>(hafile.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,hafile));
  //some things can not be set when using ha format
  data_model model;
  model.init_filename(hafile,"HA","RMS",0);
  Molecule pdb(model,true);
  if (pdb.read_errors() != "") CCP4base::store(PhaserError(INPUT,keywords,pdb.read_errors()));
  ATOM_SET.push_back(pdb.getAtomArrays());
}

void ATOM::setATOM_CHAN_BFAC_WILS(bool b) { ATOM_CHANGE_BFAC_WILSON = b; }
void ATOM::setATOM_CHAN_SCAT(bool b) { ATOM_CHANGE_SCAT = b; }
void ATOM::setATOM_CHAN_ORIG(bool b) { ATOM_CHANGE_ORIG = b; }
void ATOM::setATOM_CHAN_SCAT_TYPE(std::string scatterer)
{
  if (scatterer.size()) ATOM_SCAT = stoup(scatterer);
  else ATOM_CHANGE_SCAT = false;
}

void ATOM::analyse(void)
{
  for (int t = 0; t < ATOM_SET.size(); t++)
  if (ATOM_CHANGE_SCAT)
  for (int t = 0; t < ATOM_SET.size(); t++)
    for (int a = 0; a < ATOM_SET[t].size(); a++) ATOM_SET[t][a].SCAT.scattering_type = ATOM_SCAT;
}

}//phaser
