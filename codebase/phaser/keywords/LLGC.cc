//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/LLGC.h>
#include <phaser/lib/scattering.h>

namespace phaser {

LLGC::LLGC() : CCP4base(), InputBase()
{
  Add_Key("LLGC");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string LLGC::unparse()
{
  return LLGCOMPLETE.unparse();
}

Token_value LLGC::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,13,"COMPLETE","TYPE","SCATTERER","ELEMENT","CLUSTER","SIGMA","TOP","NCYC","CLASH","ANOM","REAL","METHOD","HOLES"))
  {
    if (keyIs("COMPLETE"))  setLLGC_COMP(getBoolean(input_stream));
    if (keyIs("SCATTERER") || keyIs("ELEMENT") || keyIs("CLUSTER") || keyIs("TYPE"))
                            addLLGC_SCAT(getString(input_stream));
    if (keyIs("SIGMA"))     setLLGC_SIGM(get1num(input_stream));
    if (keyIs("TOP"))       setLLGC_TOP(getBoolean(input_stream));
    if (keyIs("NCYC"))      setLLGC_NCYC(get1num(input_stream));
    if (keyIs("CLASH"))     setLLGC_CLAS(get1num(input_stream));
    if (keyIs("REAL"))      addLLGC_REAL(getBoolean(input_stream));
    if (keyIs("ANOM"))      addLLGC_ANOM(getBoolean(input_stream));
    if (keyIs("METHOD"))    setLLGC_METH(getString(input_stream));
    if (keyIs("HOLES"))     setLLGC_HOLE(getBoolean(input_stream));
  }
  return skip_line(input_stream);
}

void LLGC::setLLGC_NCYC(floatType ncyc)       { LLGCOMPLETE.NCYC = iszeroposi(ncyc); } //can't be zero, do loop (test ncyc end)
void LLGC::setLLGC_CLAS(floatType clash)      { LLGCOMPLETE.CLASH = iszeropos(clash); }
void LLGC::setLLGC_SIGM(floatType sigma)      { LLGCOMPLETE.SIGMA = ispos(sigma); }
void LLGC::setLLGC_TOP(bool b)                { LLGCOMPLETE.TOP = b; }
void LLGC::setLLGC_HOLE(bool b)               { LLGCOMPLETE.HOLES = b; }
void LLGC::setLLGC_COMP(bool do_llgc)         { LLGCOMPLETE.COMPLETE = do_llgc; }

void LLGC::addLLGC_SCAT(std::string id)
{ //element or cluster or real or anom
  if (id.size() > 2)
  CCP4base::store(PhaserError(INPUT,keywords,"Scatterer ID (" + id + ") must be 1 or 2 characters"));
  LLGCOMPLETE.ATOMTYPE.insert(stoup(id)); //clusters and atoms stored as upper case
}

void LLGC::addLLGC_REAL(bool b) { if (b) LLGCOMPLETE.ATOMTYPE.insert("RX"); }
void LLGC::addLLGC_ANOM(bool b) { if (b) LLGCOMPLETE.ATOMTYPE.insert("AX"); }

void LLGC::setLLGC_METH(std::string peaks)
{
  if (stoup(peaks) == "IMAGINARY") LLGCOMPLETE.METHOD = "IMAGINARY";
  else if (stoup(peaks) == "ATOMTYPE") LLGCOMPLETE.METHOD = "ATOMTYPE";
  else CCP4base::store(PhaserError(INPUT,keywords,"LLG completion peak selection method not recognised"));
}

void LLGC::analyse(void)
{
  for (stringset::iterator iter = LLGCOMPLETE.ATOMTYPE.begin(); iter != LLGCOMPLETE.ATOMTYPE.end(); iter++)
  {
    std::string id = (*iter);
    if (id.size() > 2)
    CCP4base::store(PhaserError(INPUT,keywords,"Scatterer (" + id + ") must be 1 or 2 characters"));
  }
}

} //phaser
