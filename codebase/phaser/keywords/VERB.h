//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __VERBClass__
#define __VERBClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/outStream.h>

namespace phaser {

class VERB : public InputBase, virtual public CCP4base
{
  public:
    outStream OUTPUT_LEVEL;

    VERB();
    virtual ~VERB() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setVERB(bool);
    void setDEBU(bool);
    void setOUTP_LEVE(std::string);
};

} //phaser

#endif
