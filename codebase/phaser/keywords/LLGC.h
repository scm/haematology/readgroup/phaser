//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LLGCClass__
#define __LLGCClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_llgc.h>

namespace phaser {

class LLGC : public InputBase, virtual public CCP4base
{
  public:
    data_llgc LLGCOMPLETE;

    LLGC();
    virtual ~LLGC() {}

  private:
    void init(std::string);

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setLLGC_COMP(bool);
    void setLLGC_CLAS(floatType);
    void setLLGC_SIGM(floatType);
    void setLLGC_TOP(bool);
    void setLLGC_NCYC(floatType);
    void addLLGC_SCAT(std::string);
    void addLLGC_ANOM(bool);
    void addLLGC_REAL(bool);
    void setLLGC_HOLE(bool);
    void setLLGC_METH(std::string);
};

} //phaser

#endif
