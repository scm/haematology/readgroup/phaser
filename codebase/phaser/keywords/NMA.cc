//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/NMA.h>
#include <phaser_defaults.h>

namespace phaser {

NMA::NMA() : CCP4base(), InputBase()
{
  Add_Key("NMA");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  NMA_COMBINATION = int(DEF_NMA_COMB);
  NMA_MODE.clear(); //flag for default in analyse
  NMA_ORIG = true; //change unparse also
}

std::string NMA::unparse()
{
  std::string Card("");
  bool defaults(NMA_MODE.size() == DEF_NMA_MODE);
  for (int M = 0; M < DEF_NMA_MODE && defaults; M++)
    defaults = (NMA_MODE[M] == (7 + M));
  if (NMA_MODE.size() && !defaults)
  {
    Card += "NMA";
    for (int i = 0; i < NMA_MODE.size(); i++)
      Card += " MODE " + itos(NMA_MODE[i]);
    Card += "\n";
  }
  if (NMA_COMBINATION != int(DEF_NMA_COMB))
    Card += "NMA COMBINATION " + itos(NMA_COMBINATION) + "\n";
  if (NMA_ORIG != true)
    Card += "NMA ORIG " + card(NMA_ORIG) + "\n";
  return Card;
}

Token_value NMA::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,3,"MODE","COMBINATION","ORIGINAL");
  if (keyIs("MODE"))
  {
    addNMA_MODE(get1num(input_stream));
    while (optionalKey(input_stream,1,"MODE"))
      addNMA_MODE(get1num(input_stream));
  }
  else if (keyIs("COMBINATION")) setNMA_COMB(get1num(input_stream));
  else if (keyIs("ORIGINAL")) setNMA_ORIG(getBoolean(input_stream));
  return skip_line(input_stream);
}

void NMA::setNMA_COMB(floatType b)
{ NMA_COMBINATION = iszeroposi(b); }

void NMA::setNMA_ORIG(bool b)
{ NMA_ORIG = b; }

void NMA::addNMA_MODE(floatType mode)
{
  int imode = isposi(mode);
//remove duplicates, sort, by using temporary <set>
  std::set<int> tmp;
  for (int i = 0; i < NMA_MODE.size(); i++)
    tmp.insert(isposi(NMA_MODE[i]));
  if (imode >= 7) { tmp.insert(imode); }
  else { for (int M = 7; M < 7 + imode; M++) tmp.insert(M); }
  NMA_MODE.clear();
  for (std::set<int>::iterator iter = tmp.begin(); iter != tmp.end(); iter++)
    NMA_MODE.push_back((*iter));
}

void NMA::analyse(void)
{
  if (!NMA_MODE.size()) addNMA_MODE(DEF_NMA_MODE);
}

} //phaser
