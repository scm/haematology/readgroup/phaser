//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ENSE.h>
#include <phaser/lib/xyz_weight.h>
#include <phaser/mr_objects/rms_estimate.h>

namespace phaser {

ENSE::ENSE() : CCP4base(), InputBase()
{
  Add_Key("ENSE");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  PDB.clear();
}

std::string ENSE::unparse()
{
  std::string Card("");
  //ENSEMBLE
  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
  {
    PHASER_ASSERT(iter->second.ENSEMBLE.size());
    std::string modlid = iter->first;
    if (modlid.find_first_of(" ") != std::string::npos)
      modlid = "\"" + modlid + "\"";
    if (iter->second.ENSEMBLE[0].map_format())
    { //TODO AJM switch order of rms and extent and put extent etc on separate line
      Card += "ENSEMBLE " + modlid;
      Card += " HKLIN \"" + iter->second.ENSEMBLE[0].filename() + "\"";
      Card += " F = " + iter->second.ENSEMBLE[0].LABI_F;
      Card += " PHI = " + iter->second.ENSEMBLE[0].LABI_P;
      Card += " EXTENT " + dvtos(iter->second.COORD_EXTENT);
      Card += " RMS " + dtos(iter->second.ENSEMBLE[0].rmsid());
      Card += " CENTRE " + dvtos(iter->second.COORD_CENTRE);
      Card += " PROTEIN MW " + dtos(iter->second.MW.PROTEIN);
      Card += " NUCLEIC MW " + dtos(iter->second.MW.NUCLEIC);
      if (iter->second.CELL_SCALE != 1)
        Card += " CELL SCALE " + dtos(iter->second.CELL_SCALE);
      Card += "\n";
    }
    else
    {
      //iter->second.setup_molecules(); //core dump if this has not been run
      for (int i = 0; i < iter->second.ENSEMBLE.size(); i++)
      if (iter->second.ENSEMBLE[i].is_string())
      {
        Card += "#ENSEMBLE " + modlid + " STR " + iter->second.ENSEMBLE[i].strname() + "\n";
      }
      else
      {
        PHASER_ASSERT(iter->second.ENSEMBLE[i].unparse().size());
        Card += "ENSEMBLE " + modlid + " ";
        Card +=  iter->second.ENSEMBLE[i].unparse() + " ";
        Card += "\n";
      }
      if (iter->second.USE_HETATM != bool(DEF_ENSE_USE_HETATM))
        Card += "ENSEMBLE " + modlid + " HETATM " + card(iter->second.USE_HETATM) + "\n";
      if (iter->second.DISABLE_CHECK != bool(DEF_ENSE_DISA_CHEC))
        Card += "ENSEMBLE " + modlid + " DISABLE CHECK " + card(iter->second.DISABLE_CHECK) + "\n";
      if (iter->second.DISABLE_GYRE != bool(DEF_ENSE_DISA_GYRE))
        Card += "ENSEMBLE " + modlid + " DISABLE GYRE " + card(iter->second.DISABLE_GYRE) + "\n";
    }
    if (!iter->second.BIN.is_default_ensemble())
    Card += "ENSEMBLE " + modlid + " " + iter->second.BIN.unparse() + "\n";
    if (iter->second.ESTIMATOR != std::string(DEF_ENSE_ESTI))
    Card += "ENSEMBLE " + modlid + " ESTIMATOR " + iter->second.ESTIMATOR + "\n";
    if (iter->second.BFAC_ZERO_INPUT)
    Card += "ENSEMBLE " + modlid + " BFAC ZERO ON\n";

    //TRACE
    if (iter->second.TRACE.SAMP_DISTANCE)
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING DISTANCE " + dtos(iter->second.TRACE.SAMP_DISTANCE) + "\n";
    if (iter->second.TRACE.SAMP_MIN != double(DEF_TRAC_SAMP_MIN))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING MIN " + dtos(iter->second.TRACE.SAMP_MIN) + "\n";
    if (iter->second.TRACE.SAMP_TARGET != double(DEF_TRAC_SAMP_TARG))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING TARGET " + itos(iter->second.TRACE.SAMP_TARGET) + "\n";
    if (iter->second.TRACE.SAMP_RANGE != double(DEF_TRAC_SAMP_RANG))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING RANGE " + itos(iter->second.TRACE.SAMP_RANGE) + "\n";
    if (iter->second.TRACE.SAMP_USE != std::string(DEF_TRAC_SAMP_USE))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING USE " + iter->second.TRACE.SAMP_USE + "\n";
    if (iter->second.TRACE.SAMP_WANG != double(DEF_TRAC_SAMP_WANG))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING WANG " + dtos(iter->second.TRACE.SAMP_WANG) + "\n";
    if (iter->second.TRACE.SAMP_ASA != double(DEF_TRAC_SAMP_ASA))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING ASA " + dtos(iter->second.TRACE.SAMP_ASA) + "\n";
    if (iter->second.TRACE.SAMP_NCYC != int(DEF_TRAC_SAMP_NCYC))
      Card += "ENSEMBLE " + modlid + " TRACE SAMPLING NCYC " + itos(iter->second.TRACE.SAMP_NCYC) + "\n";
    if (!iter->second.TRACE.MODEL.is_default())
      Card += "ENSEMBLE " + modlid + " " + iter->second.TRACE.MODEL.unparse() + "\n";
  }
  return Card;
}

Token_value ENSE::parse(std::istringstream& input_stream)
{
  if (keyIs("ENSEMBLE"))
  {
    std::string modlid = getFileName(input_stream);
    compulsoryKey(input_stream,13,"ATOM","HELIX","MODEL","PDB","CIF","HKLIN","DISABLE","HETATM","ESTIMATOR","BFAC","PTGRP","BINS","TRACE");
    if (keyIs("ATOM"))
    {
      std::string atomid = getString(input_stream);
      double rmsd(DEF_ENSE_ATOM_RMSD);
      if (optionalKey(input_stream,1,"RMS"))
      {
        if (keyIs("RMS")) rmsd = get1num(input_stream);
      }
      addENSE_ATOM(modlid,atomid,rmsd);
    }
    if (keyIs("BFAC"))
    {
      compulsoryKey(input_stream,1,"ZERO");
      setENSE_BFAC_ZERO(modlid,getBoolean(input_stream));
    }
    if (keyIs("HELIX"))
    {
      double nres = get1num(input_stream);
      double rmsd(DEF_ENSE_HELI_RMSD);
      if (optionalKey(input_stream,1,"RMS"))
        rmsd = get1num(input_stream);
      addENSE_HELI(modlid,nres,rmsd);
    }
    else if (keyIs("PDB") || keyIs("MODEL") || keyIs("CIF"))
    {
      if (keyIs("MODEL")) compulsoryKey(input_stream,2,"PDB","CIF"); //for phil
      bool pdb_not_cif(keyIs("PDB"));
      std::string pdbfile = getFileName(input_stream);
      compulsoryKey(input_stream,3,"RMS","ID","CARD");
      bool key_was_RMS(keyIs("RMS")),key_was_ID(keyIs("ID")),key_was_CARD(keyIs("CARD"));
      double var(0); bool card(false); //-Wall init
      if (key_was_RMS || key_was_ID) var = get1num(input_stream);
      if (key_was_CARD) card = getBoolean(input_stream);
      std::string CHAIN;
      int MODEL_SERIAL(-999);//flag
      while (optionalKey(input_stream,3,"CHAIN","MODEL","PDB"))
      {
        if (keyIs("PDB"))
        {
          throw PhaserError(SYNTAX,keywords,"\nConcatenation of PDB input on ENSEMBLE cards is deprecated\nPlease use separate ENSEMBLE cards for each PDB input\nUse CHAIN or MODEL to select subset of atoms");
        }
        //chain selection is done at the structure factor calculation, so as to keep PRPT for all
        if (keyIs("CHAIN")) CHAIN = getFileName(input_stream);
        //model selection is done on pdb reading, not implemented for CIF yet
        if (keyIs("MODEL"))
        {
          compulsoryKey(input_stream,1,"SERIAL");
          MODEL_SERIAL = get1num(input_stream);
        }
      }
      if (key_was_RMS)  addENSE_PDB_RMS_SELECT(modlid,pdbfile,var,pdb_not_cif,CHAIN,MODEL_SERIAL);
      if (key_was_ID)   addENSE_PDB_ID_SELECT(modlid,pdbfile,var,pdb_not_cif,CHAIN,MODEL_SERIAL);
      if (key_was_CARD) addENSE_PDB_CARD_SELECT(modlid,pdbfile,card,CHAIN,MODEL_SERIAL);
    }
    else if (keyIs("HKLIN"))
    {
      std::string hklin = getFileName(input_stream);
      compulsoryKey(input_stream,1,"F");
      std::string sf(getAssignString(input_stream));
      compulsoryKey(input_stream,1,"PHI");
      std::string phase(getAssignString(input_stream));
      compulsoryKey(input_stream,1,"EXTENT");
      dvect3 extent(get3nums(input_stream));
      compulsoryKey(input_stream,1,"RMS");
      floatType rms(get1num(input_stream));
      compulsoryKey(input_stream,1,"CENTRE");
      dvect3 centre(get3nums(input_stream));
      compulsoryKey(input_stream,1,"PROTEIN");
      compulsoryKey(input_stream,1,"MW");
      floatType protmw(get1num(input_stream));
      compulsoryKey(input_stream,1,"NUCLEIC");
      compulsoryKey(input_stream,1,"MW");
      floatType nuclmw(get1num(input_stream));
      double cell_scale = 1;
      if (optionalKey(input_stream,1,"CELL")) //use separate function call for backwards compatibility
      {
        compulsoryKey(input_stream,1,"SCALE");
        cell_scale = get1num(input_stream);
      }
      addENSE_MAP(modlid,hklin,sf,phase,extent,rms,centre,protmw,nuclmw,cell_scale);
    }
    else if (keyIs("DISABLE"))
    {
      compulsoryKey(input_stream,2,"CHECK","GYRE");
      if (keyIs("CHECK")) setENSE_DISA_CHEC(modlid,getBoolean(input_stream));
      if (keyIs("GYRE"))  setENSE_DISA_GYRE(modlid,getBoolean(input_stream));
    }
    else if (keyIs("HETATM"))
    {
      setENSE_HETA(modlid,getBoolean(input_stream));
    }
    else if (keyIs("ESTIMATOR"))
    {
      setENSE_ESTI(modlid,getString(input_stream));
    }
    else if (keyIs("PTGRP"))
    {
      while (optionalKey(input_stream,5,"COVERAGE","IDENTITY","RMSD","TOLANG","TOLSPA"))
      {
        if (keyIs("COVERAGE")) setENSE_PTGR_COVE(modlid,get1num(input_stream));
        if (keyIs("IDENTIY"))  setENSE_PTGR_IDEN(modlid,get1num(input_stream));
        if (keyIs("RMSD"))     setENSE_PTGR_RMSD(modlid,get1num(input_stream));
        if (keyIs("TOLANG"))   setENSE_PTGR_TOLA(modlid,get1num(input_stream));
        if (keyIs("TOLSPA"))   setENSE_PTGR_TOLS(modlid,get1num(input_stream));
      }
    }
    else if (keyIs("BINS"))
    {
      while (optionalKey(input_stream,3,"MIN","MAX","WIDTH"))
      {
        if (keyIs("MIN"))   setENSE_BINS_MINI(modlid,get1num(input_stream));
        if (keyIs("MAX"))   setENSE_BINS_MAXI(modlid,get1num(input_stream));
        if (keyIs("WIDTH")) setENSE_BINS_WIDT(modlid,get1num(input_stream));
      }
    }
    else if (keyIs("TRACE"))
    {
      compulsoryKey(input_stream,3,"PDB","CIF","SAMPLING");
      if (keyIs("PDB"))
      {
        setENSE_TRAC_PDB(modlid,getFileName(input_stream));
      }
      else if (keyIs("CIF"))
      {
        setENSE_TRAC_CIF(modlid,getFileName(input_stream));
      }
      else if (keyIs("SAMPLING"))
      {
        compulsoryKey(input_stream,8,"MIN","TARGET","RANGE","DISTANCE","USE","WANG","ASA","NCYC");
        if (keyIs("MIN"))      setENSE_TRAC_SAMP_MIN(modlid,get1num(input_stream));
        if (keyIs("TARGET"))   setENSE_TRAC_SAMP_TARG(modlid,get1num(input_stream));
        if (keyIs("RANGE"))    setENSE_TRAC_SAMP_RANG(modlid,get1num(input_stream));
        if (keyIs("DISTANCE")) setENSE_TRAC_SAMP_DIST(modlid,get1num(input_stream));
        if (keyIs("WANG"))     setENSE_TRAC_SAMP_WANG(modlid,get1num(input_stream));
        if (keyIs("ASA"))      setENSE_TRAC_SAMP_ASA(modlid,get1num(input_stream));
        if (keyIs("NCYC"))     setENSE_TRAC_SAMP_NCYC(modlid,get1num(input_stream));
        if (keyIs("USE"))
        {
          compulsoryKey(input_stream,4,"AUTO","CALPHA","ALL","HEXGRID");
          std::string use;
          if (keyIs("AUTO")) use = "AUTO";
          if (keyIs("ALL")) use = "ALL";
          if (keyIs("CALPHA")) use = "CALPHA";
          if (keyIs("HEXGRID")) use = "HEXGRID";
          setENSE_TRAC_SAMP_USE(modlid,use);
        }
      }
    }
  }
  return skip_line(input_stream);
}

void ENSE::initENSE_PDB(std::string modlid)
{
  if (PDB.find(modlid) == PDB.end()) PDB[modlid] = data_pdb();
}

void ENSE::addENSE_PDB_RMS(std::string modlid,std::string pdbfile,floatType rmsd,bool pdb_not_cif)
{ addENSE_PDB_RMS_SELECT(modlid,pdbfile,rmsd,pdb_not_cif); }

void ENSE::addENSE_PDB_RMS_SELECT(std::string modlid,std::string pdbfile,floatType rmsd,bool pdb_not_cif,std::string CHAIN,int MODEL_SERIAL)
{
  if (ispos(rmsd) > DEF_RMSD_LIMIT)
  throw PhaserError(INPUT,keywords,"File \"" + pdbfile + "\" entered RMSD (" + dtos(rmsd) + ") over input limit (" + dtos(double(DEF_RMSD_LIMIT)) + ")");
  if (PDB.find(modlid) == PDB.end()) initENSE_PDB(modlid);
  data_model model;
  model.init_filename(pdbfile,std::string(pdb_not_cif?"PDB":"CIF"),"RMS",rmsd);
  model.CHAIN = (CHAIN.size() == 1) ? " " + CHAIN : CHAIN;
  model.MODEL_SERIAL = (MODEL_SERIAL == -999) ? MODEL_SERIAL : iszeroposi(MODEL_SERIAL);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"File \"" + pdbfile + "\" entered twice to ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_PDB_CARD(std::string modlid,std::string pdbfile,bool b)
{ addENSE_PDB_CARD_SELECT(modlid,pdbfile,b); }

void ENSE::addENSE_PDB_CARD_SELECT(std::string modlid,std::string pdbfile,bool b,std::string CHAIN,int MODEL_SERIAL)
{
  initENSE_PDB(modlid);
  data_model model;
  model.init_card(pdbfile);
  model.CHAIN = (CHAIN.size() == 1) ? " " + CHAIN : CHAIN;
  model.MODEL_SERIAL = (MODEL_SERIAL == -999) ? MODEL_SERIAL : iszeroposi(MODEL_SERIAL);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"File \"" + pdbfile + "\" entered twice to ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_PDB_ID(std::string modlid,std::string pdbfile,floatType identity,bool pdb_not_cif)
{ addENSE_PDB_ID_SELECT(modlid,pdbfile,identity,pdb_not_cif); }

void ENSE::addENSE_PDB_ID_SELECT(std::string modlid,std::string pdbfile,floatType identity,bool pdb_not_cif,std::string CHAIN,int MODEL_SERIAL)
{
  initENSE_PDB(modlid);
  identity = isperc(identity);
  data_model model;
  model.init_filename(pdbfile,pdb_not_cif ? "PDB" : "CIF","ID",identity);
  model.CHAIN = (CHAIN.size() == 1) ? " " + CHAIN : CHAIN;
  model.MODEL_SERIAL = (MODEL_SERIAL == -999) ? MODEL_SERIAL : iszeroposi(MODEL_SERIAL);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"File \"" + pdbfile + "\" entered twice to ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_STR_ID_PTGR(std::string modlid,std::string content,std::string name,floatType identity,data_ptgrp ptrtol)
{
  initENSE_PDB(modlid);
  identity = isperc(identity);
  data_model model;
  model.init_string(content,name,"PDB","ID",identity);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"File entered twice to ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_STR_ID(std::string modlid,std::string content,std::string name,floatType identity)
{ addENSE_STR_ID_PTGR(modlid,content,name,identity,data_ptgrp()); }

void ENSE::addENSE_STR_RMS(std::string modlid,std::string content,std::string name,floatType rms)
{ addENSE_STR_RMS_PTGR(modlid,content,name,rms,data_ptgrp()); }

void ENSE::addENSE_ATOM(std::string modlid,std::string scatterer,double RMSD)
{
  initENSE_PDB(modlid);
  data_model model;
  model.init_atom(scatterer,RMSD);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"Atom entered twice to ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_HELI(std::string modlid,double NRES,double RMSD)
{
  initENSE_PDB(modlid);
  if (NRES < DEF_MIN_HELIX_LEN)
  throw PhaserError(INPUT,keywords,"Helix length input (" + itos(NRES) + ") less than minimum (" + itos(DEF_MIN_HELIX_LEN) + ")");
  data_model model;
  model.init_helix(dtos(NRES),RMSD);
  bool success = PDB[modlid].addModel(model);
  if (!success) throw PhaserError(INPUT,keywords,"Helix generation failed for ensemble \"" + modlid + "\"");
}

void ENSE::addENSE_STR_RMS_PTGR(std::string modlid,std::string content,std::string name, floatType rmsd, data_ptgrp ptrtol)
{
  initENSE_PDB(modlid);
  if (ispos(rmsd) > DEF_RMSD_LIMIT)
  throw PhaserError(INPUT,keywords,"Entered RMSD (" + dtos(rmsd) + ") over input limit (" + dtos(double(DEF_RMSD_LIMIT)) + ")");
  data_model model;
  model.init_string(content,name,"PDB","RMS",rmsd);
  PDB[modlid].addModel(model);
}

void ENSE::addENSE_MAP(std::string modlid,std::string hklin,std::string sf,std::string phase,dvect3 extent,double rmsd,dvect3 centre,double protmw,double nuclmw,double cell_scale)
{
  if (ispos(rmsd) > DEF_RMSD_LIMIT)
  throw PhaserError(INPUT,keywords,"File \"" + hklin + "\" entered RMSD (" + dtos(rmsd) + ") over input limit (" + dtos(double(DEF_RMSD_LIMIT)) + ")");
  if (PDB.find(modlid) != PDB.end())
  CCP4base::store(PhaserError(INPUT,keywords,"ENSEMBLE \"" + modlid + "\": hklin already defined "));
  for (int i = 0; i < 3; i++) ispos(extent[i]);
  cell_scale = ispos(cell_scale);
  if (nuclmw <= 0 && protmw <= 0)
  CCP4base::store(PhaserError(INPUT,keywords,"PROTEIN MW or NUCLEIC MW must be positive"));
  data_model model;
  model.init_map(hklin,sf,phase,rmsd);
  PDB[modlid].addModel(model);
  data_map dat;
  dat.set_extent(extent);
  dat.set_centre(centre);
  scattering_protein protein;
  scattering_nucleic nucleic;
  dat.SCATTERING = protein.ScatMw(protmw).scat + nucleic.ScatMw(nuclmw).scat;
  dat.MW = data_mw(protmw,nuclmw);
  dat.CELL_SCALE = cell_scale;
  dat.PT = -centre;
  PDB[modlid].set_data_map(dat);
}

void ENSE::setENSE_DISA_CHEC(std::string modlid,bool b)
{ initENSE_PDB(modlid); PDB[modlid].DISABLE_CHECK = b; }

void ENSE::setENSE_DISA_GYRE(std::string modlid,bool b)
{ initENSE_PDB(modlid); PDB[modlid].DISABLE_GYRE = b; }

void ENSE::setENSE_HETA(std::string modlid,bool b)
{ initENSE_PDB(modlid); PDB[modlid].USE_HETATM = b; }

void ENSE::setENSE_PTGR_COVE(std::string modlid,double value)
{ initENSE_PDB(modlid); PDB[modlid].PTGRP.COVERAGE = value; }

void ENSE::setENSE_PTGR_IDEN(std::string modlid,double value)
{ initENSE_PDB(modlid); PDB[modlid].PTGRP.IDENTITY = value; }

void ENSE::setENSE_PTGR_RMSD(std::string modlid,double value)
{ initENSE_PDB(modlid); PDB[modlid].PTGRP.RMSD = value; }

void ENSE::setENSE_PTGR_TOLA(std::string modlid,double value)
{ initENSE_PDB(modlid); PDB[modlid].PTGRP.TOLANG = value; }

void ENSE::setENSE_PTGR_TOLS(std::string modlid,double value)
{ initENSE_PDB(modlid); PDB[modlid].PTGRP.TOLSPA = value; }

void ENSE::setENSE_BINS(std::string modlid, data_bins databins)
{ initENSE_PDB(modlid); PDB[modlid].BIN = databins; }

void ENSE::setENSE_BINS_MINI(std::string modlid, double minbins)
{ initENSE_PDB(modlid); PDB[modlid].BIN.MINBINS = isposi(minbins); }

void ENSE::setENSE_BINS_MAXI(std::string modlid, double maxbins)
{ initENSE_PDB(modlid); PDB[modlid].BIN.MAXBINS = isposi(maxbins); }

void ENSE::setENSE_BINS_WIDT(std::string modlid, double width)
{ initENSE_PDB(modlid); PDB[modlid].BIN.WIDTH = isposi(width); }

void ENSE::setENSE_ESTI(std::string modlid,std::string estimator)
{
  initENSE_PDB(modlid);
  rms_estimate tmp(estimator);
  if (tmp.error) CCP4base::store(PhaserError(INPUT,keywords,"ENSEMBLE ESTIMATOR " + estimator + ": not known"));
  if (PDB.find(modlid) != PDB.end()) PDB[modlid].ESTIMATOR = estimator;
  else CCP4base::store(PhaserError(INPUT,keywords,"ENSEMBLE \"" + modlid + "\": Not yet defined (card order?)"));
}

void ENSE::setENSE_TRAC_PDB(std::string modlid,std::string pdbfile)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.MODEL.init_trace(pdbfile,"PDB"); }

void ENSE::setENSE_TRAC_CIF(std::string modlid,std::string pdbfile)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.MODEL.init_trace(pdbfile,"CIF"); }

void ENSE::setENSE_TRAC_SAMP_DIST(std::string modlid,double dist)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_DISTANCE = iszeropos(dist); }

void ENSE::setENSE_TRAC_SAMP_MIN(std::string modlid,double samp)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_MIN = ispos(samp); }

void ENSE::setENSE_TRAC_SAMP_TARG(std::string modlid,double target)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_TARGET = isposi(target); }

void ENSE::setENSE_TRAC_SAMP_RANG(std::string modlid,double range)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_RANGE = isposi(range); }

void ENSE::setENSE_TRAC_SAMP_WANG(std::string modlid,double wang)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_WANG = isperc(wang); }

void ENSE::setENSE_TRAC_SAMP_ASA(std::string modlid,double asa)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_ASA = isperc(asa); }

void ENSE::setENSE_TRAC_SAMP_NCYC(std::string modlid,double ncyc)
{ initENSE_PDB(modlid); PDB[modlid].TRACE.SAMP_NCYC = isposi(ncyc); }

void ENSE::setENSE_BFAC_ZERO(std::string modlid,bool b)
{ initENSE_PDB(modlid); PDB[modlid].BFAC_ZERO_INPUT = b; }

void ENSE::setENSE_TRAC_SAMP_USE(std::string modlid,std::string use)
{
  initENSE_PDB(modlid);
  use = stoup(use);
  bool allowed = PDB[modlid].TRACE.set_use(use); //python
  if (!allowed) CCP4base::store(PhaserError(INPUT,keywords,"Trace sampling use \"" + use + "\" not recognised"));
}

void ENSE::analyse(void)
{
  for (pdbIter iter = PDB.begin(); iter != PDB.end(); iter++)
  {
    std::string modlid = iter->first;
    if (!PDB[modlid].ENSEMBLE.size())
      CCP4base::store(PhaserError(INPUT,keywords,"ENSEMBLE \"" + modlid + "\": has not been assigned any models"));
    else
    {
      //Don't have to check filetypes streams
      bool file_read_error(false);
      for (int f = 0; f < iter->second.ENSEMBLE.size(); f++)
      if (iter->second.ENSEMBLE[f].is_file())
      {
        std::ifstream infile(const_cast<char*>(iter->second.ENSEMBLE[f].filename().c_str()));
        if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,iter->second.ENSEMBLE[f].filename()));
        if (!infile) file_read_error = true;
        iter->second.ENSEMBLE[f].BFAC_ZERO = PDB[modlid].BFAC_ZERO_INPUT;
      }
      if (!file_read_error)
      {
        std::string read_error = iter->second.setup_molecules();
        if (read_error != "") CCP4base::store(PhaserError(INPUT,keywords,read_error));
      }
    }
  }
}

}//phaser
