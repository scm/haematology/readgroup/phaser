//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SCED.h>
#include <phaser_defaults.h>

namespace phaser {

SCED::SCED() : CCP4base(), InputBase()
{
  Add_Key("SCED");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SCEDS.set_defaults();
}

std::string SCED::unparse()
{
  return SCEDS.unparse();
}

Token_value SCED::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"NDOM","WEIGHT");
  if (keyIs("NDOM"))     setSCED_NDOM(get1num(input_stream));
  else if (keyIs("WEIGHT"))
  {
    while (optionalKey(input_stream,4,"EQUALITY","SPHERICITY","DENSITY","CONTINUITY"))
    {
      if (keyIs("EQUALITY"))   setSCED_WEIG_EQUA(get1num(input_stream));
      if (keyIs("SPHERICITY")) setSCED_WEIG_SPHE(get1num(input_stream));
      if (keyIs("DENSITY"))    setSCED_WEIG_DENS(get1num(input_stream));
      if (keyIs("CONTINUITY")) setSCED_WEIG_CONT(get1num(input_stream));
    }
  }
  return skip_line(input_stream);
}

void SCED::setSCED_NDOM(floatType v)      { SCEDS.NDOM = isposi(v); }
void SCED::setSCED_WEIG_EQUA(floatType b) { SCEDS.wEQUALITY = iszeropos(b); }
void SCED::setSCED_WEIG_SPHE(floatType b) { SCEDS.wSPHERICITY = iszeropos(b); }
void SCED::setSCED_WEIG_DENS(floatType b) { SCEDS.wDENSITY = iszeropos(b); }
void SCED::setSCED_WEIG_CONT(floatType b) { SCEDS.wCONTINUITY = iszeropos(b); }

void SCED::analyse(void)
{
  if (!SCEDS.wEQUALITY && !SCEDS.wSPHERICITY && !SCEDS.wDENSITY && !SCEDS.wCONTINUITY) CCP4base::store(PhaserError(INPUT,keywords,"All SCEDOMAIN weights zero"));
}

} //phaser
