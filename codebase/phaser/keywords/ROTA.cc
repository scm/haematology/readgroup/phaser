//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ROTA.h>

namespace phaser {

ROTA::ROTA() : CCP4base(), InputBase()
{
  Add_Key("ROTA");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_ROTATE_AROUND = (std::string(DEF_ROTA_VOLU) == "AROUND");
  ROTATE_EULER = dvect3(DEF_ROTA_EULE);
  ROTATE_RANGE = DEF_ROTA_RANG;
  USE_ROTATE_LMAX_RESO = bool(DEF_ROTA_LMAX_RESO);
  NEW_CLUSTERING=false;
}

std::string ROTA::unparse()
{
  std::string Card;
  if (DO_ROTATE_AROUND)
    Card += "ROTATE VOLUME AROUND EULER "
           + dtos(ROTATE_EULER[0]) + " " + dtos(ROTATE_EULER[1]) + " " + dtos(ROTATE_EULER[2]) +
           " RANGE " + dtos(ROTATE_RANGE) + "\n";
  if (USE_ROTATE_LMAX_RESO != DEF_ROTA_LMAX_RESO)
    Card += "ROTATE LMAX RESOLUTION " + std::string(USE_ROTATE_LMAX_RESO?"ON":"OFF") + "\n";
  return Card;
  //return "ROTATE VOLUME FULL\n";
}

Token_value ROTA::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"VOLUME","LMAX");
  if (keyIs("VOLUME"))
  {
    compulsoryKey(input_stream,2,"FULL","AROUND");
    if (keyIs("FULL")) setROTA_VOLU("FULL");
    if (keyIs("AROUND"))
    {
      setROTA_VOLU("AROUND");
      compulsoryKey(input_stream,1,"EULER");
      setROTA_EULE(get3nums(input_stream));
      compulsoryKey(input_stream,1,"RANGE");
      setROTA_RANG(get1num(input_stream));
    }
  }
  if (keyIs("LMAX"))
  {
    compulsoryKey(input_stream,1,"RESOLUTION");
    setROTA_LMAX_RESO(getBoolean(input_stream));
  }
  return skip_line(input_stream);
}

void ROTA::setROTA_VOLU(std::string vol)
{
  if (stoup(vol) == "FULL") DO_ROTATE_AROUND = false;
  else if (stoup(vol) == "AROUND") DO_ROTATE_AROUND = true;
  else CCP4base::store(PhaserError(INPUT,keywords,"Rotation volume not recognised"));
}
void ROTA::setROTA_EULE(dvect3 point) { ROTATE_EULER = point; }
void ROTA::setROTA_LMAX_RESO(bool b)  { USE_ROTATE_LMAX_RESO = b; }
void ROTA::setROTA_RANG(floatType range) { ROTATE_RANGE = iszeropos(range); }

void ROTA::analyse(void)
{
}

} //phaser
