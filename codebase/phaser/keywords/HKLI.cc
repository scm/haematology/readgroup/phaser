//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/HKLI.h>

namespace phaser {

HKLI::HKLI() : CCP4base(), InputBase()
{
  Add_Key("HKLI");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  HKLIN ="";
}

std::string HKLI::unparse()
{
  if (HKLIN.size()) return "HKLIN \"" + HKLIN + "\"\n";
  return "";
}

Token_value HKLI::parse(std::istringstream& input_stream)
{
  std::string hklin = getFileName(input_stream);
  setHKLI(hklin);
  return skip_line(input_stream);
}


void HKLI::setHKLI(std::string hklin)
{
//don't do FILEOPEN test as this is also used as an optional keyword for APPENDING data
  HKLIN = hklin;
}

void HKLI::analyse(void)
{
}


} //phaser
