//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SPAC.h>
#include <phaser/include/space_group_name.h>

namespace phaser {

SPAC::SPAC() : CCP4base(), InputBase()
{
  Add_Key("SPAC");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SG_HALL = "";
}

std::string SPAC::unparse()
{
  if (SG_HALL.size())
  {
    space_group_name sg(SG_HALL,true);
    if (!sg.error) return "SPACEGROUP " + sg.CCP4 + "\n";
    return "SPACEGROUP HALL " + SG_HALL + "\n"; //for unconventional settings
  }
  return "";
}

Token_value SPAC::parse(std::istringstream& input_stream)
{
  get_token(input_stream);
  if (curr_tok == NUMBER)
    setSPAC_NUM(number_value);
  else if (curr_tok == NAME)
  {
    //collect all strings to end of line
    bool is_hall(stoup(string_value) == "HALL");
    if (stoup(string_value) == "HALL") string_value = "";
    std::string sg_name(string_value);
    while (get_token(input_stream) == NAME || curr_tok == NUMBER)
      sg_name += " " + string_value;
    is_hall ? setSPAC_HALL(sg_name) : setSPAC_NAME(sg_name);
  }
  else
    throw PhaserError(SYNTAX,keywords,"Space Group not present or not valid");
  return skip_line(input_stream);
}

void SPAC::setSPAC_NAME(std::string sg_name)
{
  space_group_name this_sg(sg_name,false);
  if (this_sg.error)
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group Name not valid"));
  else
  {
    //check that the parser has not found two conflicting SPAC keywords
    if (SG_HALL.size() && (this_sg.HALL != SG_HALL))
    CCP4base::store(PhaserError(INPUT,keywords,"Conflicting Space Group choices"));
    SG_HALL = this_sg.HALL;
  }
}

void SPAC::setSPAC_HALL(std::string sg_name)
{
  space_group_name this_sg(sg_name,true);
  //don't check that this has been set before
  //this is the command called by scripting to change SG on the fly
  if (this_sg.hall_error)
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group Hall Symbol not valid: \"" + sg_name + "\""));
  else
    SG_HALL = this_sg.HALL;
}

void SPAC::setSPAC_NUM(floatType sg_num)
{
  //check that the parser has not found two conflicting SPAC keywords
  try {
    sg_num = isposi(sg_num);
    cctbx::sgtbx::space_group_symbols Symbols(sg_num,"","A1983");
    std::string hall = Symbols.hall();
    //check that the parser has not found two conflicting SPAC keywords
    if (SG_HALL.size() && (hall != SG_HALL))
    CCP4base::store(PhaserError(INPUT,keywords,"Conflicting Space Group choices"));
    SG_HALL = hall;
  }
  catch (...) {
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group Number not valid"));
  }
}

void SPAC::analyse(void)
{
}

} //phaser
