//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ROOT.h>
#include <phaser_defaults.h>

namespace phaser {

ROOT::ROOT() : CCP4base(), InputBase()
{
  Add_Key("ROOT");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  FILEROOT = std::string(DEF_ROOT);
}

std::string ROOT::unparse()
{
  if (FILEROOT == std::string(DEF_ROOT)) return "";
  return "ROOT \"" + FILEROOT + "\"\n";
}

Token_value ROOT::parse(std::istringstream& input_stream)
{
  setROOT(getFileName(input_stream));
  return skip_line(input_stream);
}

void ROOT::setROOT(std::string fileroot)
{
  if (FILEROOT != "" && FILEROOT != "\"\"") FILEROOT = fileroot;
}

void ROOT::analyse(void)
{
  //must have a fileroot for output without throwing an error message
}


} //phaser
