//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CRYSClass__
#define __CRYSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/ep_objects/data_ep.h>

namespace phaser {

class CRYS : public InputBase, virtual public CCP4base
{
  public:
    data_ep CRYS_DATA;
    std::string EXPERIMENT;

    CRYS();
    virtual ~CRYS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setCRYS_LABI_FPOS(std::string,std::string,std::string);
    void setCRYS_LABI_FNEG(std::string,std::string,std::string);
    void setCRYS_LABI_SIGFPOS(std::string,std::string,std::string);
    void setCRYS_LABI_SIGFNEG(std::string,std::string,std::string);
    void setCRYS_LABI_IPOS(std::string,std::string,std::string);
    void setCRYS_LABI_INEG(std::string,std::string,std::string);
    void setCRYS_LABI_SIGIPOS(std::string,std::string,std::string);
    void setCRYS_LABI_SIGINEG(std::string,std::string,std::string);
    void setCRYS_LABI_F(std::string,std::string,std::string);
    void setCRYS_LABI_SIGF(std::string,std::string,std::string);
    void setCRYS_LABI_I(std::string,std::string,std::string);
    void setCRYS_LABI_SIGI(std::string,std::string,std::string);

   void addCRYS_ANOM_LABI(std::string,std::string,std::string,std::string,std::string,std::string);
   void addCRYS_MEAN_LABI(std::string,std::string,std::string,std::string);
   void addCRYS_ANOM_LABI_I(std::string,std::string,std::string,std::string,std::string,std::string);
   void addCRYS_MEAN_LABI_I(std::string,std::string,std::string,std::string);

    void setCRYS_DATA(data_ep); //shortcut for input
    void setCRYS_MILLER(af::shared<miller::index<int> >);
    void addCRYS_ANOM_DATA(std::string,std::string,af_float,af_float,af_bool,af_float,af_float,af_bool);
    void setCRYS_SAD_DATA(af::shared<miller::index<int> >,af_float,af_float,af_bool,af_float,af_float,af_bool);
    void addCRYS_DATA_WAVE(std::string,std::string,floatType);

    void setEXPE(std::string);
};

} //phaser

#endif
