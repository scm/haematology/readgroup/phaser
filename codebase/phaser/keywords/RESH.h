//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RESHClass__
#define __RESHClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_resharp.h>

namespace phaser {

class RESH : public InputBase, virtual public CCP4base
{
  public:
    data_resharp RESHARP;

    RESH();
    virtual ~RESH() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setRESH_PERC(floatType);
};

} //phaser

#endif
