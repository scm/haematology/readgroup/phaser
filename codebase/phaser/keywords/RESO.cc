//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/RESO.h>
#include <float.h>

namespace phaser {

RESO::RESO() : CCP4base(), InputBase()
{
  Add_Key("RESO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  HIRES = -999; //flag
  AUTO_HIRES = -999; //flag
  DO_AUTO_HIRES = true;
  LORES = double(DEF_LORES); //not doublemax
}

std::string RESO::unparse()
{
  std::string Card("");
  if (HIRES != -999)
  Card += "RESOLUTION HIGH " + dtos(HIRES) + "\n";
  if (LORES != double(DEF_LORES))
  Card += "RESOLUTION LOW " + dtos(LORES) + "\n";
  if (!DO_AUTO_HIRES)
    Card += "RESOLUTION AUTO OFF\n";
  else if (AUTO_HIRES != -999)
    Card += "RESOLUTION AUTO HIGH " + dtos(AUTO_HIRES) + "\n";
  return Card;
}

Token_value RESO::parse(std::istringstream& input_stream)
{
  get_token(input_stream);
  if (curr_tok == NUMBER)
  {
    floatType reso1 = number_value;
    if (tokenIs(input_stream,1,NUMBER)) setRESO(reso1,number_value);
    else setRESO_HIGH(reso1);
  }
  else if (curr_tok == NAME)
  {
    while (optionalKey(3,"HIGH","LOW","AUTO"))
    {
      if (keyIs("HIGH"))
      {
        setRESO_HIGH(get1num(input_stream));
        get_token(input_stream);
      }
      else if (keyIs("LOW"))
      {
        setRESO_LOW(get1num(input_stream));
        get_token(input_stream);
      }
      else if (keyIs("AUTO"))
      {
        compulsoryKey(input_stream,2,"HIGH","OFF");
        if (keyIs("HIGH")) setRESO_AUTO_HIGH(get1num(input_stream));
        if (keyIs("OFF"))  setRESO_AUTO_OFF();
        get_token(input_stream);
      }
    }
  }
  return skip_line(input_stream);
}

void RESO::setRESO_HIGH(floatType reso) { HIRES = iszeropos(reso); }
void RESO::setRESO_LOW(floatType reso)  { LORES = iszeropos(reso); }
void RESO::setRESO_AUTO_HIGH(floatType reso) { AUTO_HIRES = iszeropos(reso); }
void RESO::setRESO_AUTO_OFF()           { DO_AUTO_HIRES = false; }

void RESO::setRESO(floatType reso1, floatType reso2)
{
  HIRES = iszeropos(std::min(reso1,reso2));
  LORES = iszeropos(std::max(reso1,reso2));
}

void RESO::analyse(void)
{
}


} //phaser
