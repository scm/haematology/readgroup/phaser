//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PACK.h>
#include <phaser_defaults.h>

namespace phaser {

PACK::PACK() : CCP4base(), InputBase()
{
  Add_Key("PACK");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string PACK::unparse()
{
  return PACKING.unparse();
}

Token_value PACK::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,5,"SELECT","CUTOFF","QUICK","COMPACT","KEEP"))
  {
    if (keyIs("SELECT"))
    {
      compulsoryKey(input_stream,2,"PERCENT","ALL");
      if (keyIs("ALLOW"))   throw PhaserError(SYNTAX,keywords,"PERCENT or ALL"); //first, special, obsoleted
      if (keyIs("PERCENT")) setPACK_SELE("PERCENT");
      if (keyIs("ALL"))     setPACK_SELE("ALL");
    }
    if (keyIs("CUTOFF"))   setPACK_CUTO(get1num(input_stream));
    if (keyIs("QUICK"))    setPACK_QUIC(getBoolean(input_stream));
    if (keyIs("COMPACT"))  setPACK_COMP(getBoolean(input_stream));
    if (keyIs("KEEP"))
    {
      compulsoryKey(input_stream,1,"HIGH");
      compulsoryKey(input_stream,1,"TFZ");
      setPACK_KEEP_HIGH_TFZ(getBoolean(input_stream));
    }
  }
  return skip_line(input_stream);
}

void PACK::setPACK_SELE(std::string select)
{
  bool error = PACKING.set_select(select);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Packing selection not recognised"));
}
void PACK::setPACK_CUTO(double clash) { PACKING.CUTOFF = isperc(clash); }
void PACK::setPACK_QUIC(bool q) { PACKING.QUICK = q; }
void PACK::setPACK_COMP(bool c) { PACKING.COMPACT = c; }
void PACK::setPACK_KEEP_HIGH_TFZ(bool b) { PACKING.KEEP_HIGH_TFZ = b; }

void PACK::analyse(void)
{
}

}//phaser
