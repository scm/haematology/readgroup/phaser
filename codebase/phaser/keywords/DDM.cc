//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/DDM.h>
#include <phaser_defaults.h>

namespace phaser {

DDM::DDM() : CCP4base(), InputBase()
{
  Add_Key("DDM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  NMADDM.set_defaults();
}

std::string DDM::unparse()
{
  return NMADDM.unparse();
}

Token_value DDM::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,6,"SLIDER","DISTANCE","SEQUENCE","SEPARATION","JOIN","CLUSTER");
  if (keyIs("SLIDER"))
  {
    setDDM_SLID(get1num(input_stream));
  }
  else if (keyIs("DISTANCE"))
  {
    while (optionalKey(input_stream,3,"STEP","MIN","MAX"))
    if (keyIs("STEP"))   setDDM_DIST_STEP(get1num(input_stream));
    else if (keyIs("MIN"))    setDDM_DIST_MIN(get1num(input_stream));
    else if (keyIs("MAX"))    setDDM_DIST_MAX(get1num(input_stream));
  }
  else if (keyIs("SEQUENCE"))
  {
    while (optionalKey(input_stream,2,"MIN","MAX"))
    {
      if (keyIs("MIN")) setDDM_SEQU_MIN(get1num(input_stream));
      else if (keyIs("MAX")) setDDM_SEQU_MAX(get1num(input_stream));
    }
  }
  else if (keyIs("SEPARATION"))
  {
    while (optionalKey(input_stream,2,"MIN","MAX"))
    {
      if (keyIs("MIN")) setDDM_SEPA_MIN(get1num(input_stream));
      else if (keyIs("MAX")) setDDM_SEPA_MAX(get1num(input_stream));
    }
  }
  else if (keyIs("JOIN"))
  {
    while (optionalKey(input_stream,2,"MIN","MAX"))
    {
      if (keyIs("MIN")) setDDM_JOIN_MIN(get1num(input_stream));
      else if (keyIs("MAX")) setDDM_JOIN_MAX(get1num(input_stream));
    }
  }
  else if (keyIs("CLUSTER"))
  {
    compulsoryKey(input_stream,3,"FIRST","LAST","MAX");
    setDDM_CLUS(string_value);
  }
  return skip_line(input_stream);
}

void DDM::setDDM_SLID(floatType v)      { NMADDM.SLIDER = iszeroposi(v); }
void DDM::setDDM_DIST_STEP(floatType v) { NMADDM.DISTANCE_STEP = iszeroposi(v); }
void DDM::setDDM_DIST_MIN(floatType v)  { NMADDM.DISTANCE_MIN = iszeroposi(v); }
void DDM::setDDM_DIST_MAX(floatType v)  { NMADDM.DISTANCE_MAX = iszeroposi(v); }
void DDM::setDDM_JOIN_MIN(floatType v)  { NMADDM.JOIN_MIN = isperc(v); }
void DDM::setDDM_JOIN_MAX(floatType v)  { NMADDM.JOIN_MAX = isperc(v); }
void DDM::setDDM_SEQU_MIN(floatType v)  { NMADDM.SEQ_MIN = iszeroposi(v); }
void DDM::setDDM_SEQU_MAX(floatType v)  { NMADDM.SEQ_MAX = iszeroposi(v); }
void DDM::setDDM_SEPA_MIN(floatType v)  { NMADDM.SEPARATION_MIN = iszeropos(v); }
void DDM::setDDM_SEPA_MAX(floatType v)  { NMADDM.SEPARATION_MAX = iszeropos(v); }
void DDM::setDDM_CLUS(std::string c)    { NMADDM.CLUSTER = stoup(c); }

void DDM::analyse(void)
{
  NMADDM.check_min_max();
}

} //phaser
