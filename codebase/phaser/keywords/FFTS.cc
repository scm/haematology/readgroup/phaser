//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/FFTS.h>
#include <phaser_defaults.h>

namespace phaser {

FFTS::FFTS() : CCP4base(), InputBase()
{
  Add_Key("FFTS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string FFTS::unparse()
{ return FFTvDIRECT.unparse(); }

Token_value FFTS::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,1,"MIN");
  setFFTS_MIN(get1num(input_stream));
  compulsoryKey(input_stream,1,"MAX");
  setFFTS_MAX(get1num(input_stream));
  return skip_line(input_stream);
}

void FFTS::setFFTS_MIN(double m) { FFTvDIRECT.MIN = iszeroposi(m); }
void FFTS::setFFTS_MAX(double m) { FFTvDIRECT.MAX = iszeroposi(m); }

void FFTS::analyse(void)
{
  if (FFTvDIRECT.MIN > FFTvDIRECT.MAX)
  CCP4base::store(PhaserError(INPUT,keywords,"FFT minimum number of atoms input is greater than maximum number input"));
}

}//phaser
