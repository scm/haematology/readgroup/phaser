//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/OUTL.h>
#include <phaser/lib/maths.h>

namespace phaser {

OUTL::OUTL() : CCP4base(), InputBase()
{
  Add_Key("OUTL");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string OUTL::unparse()
{
  return OUTLIER.unparse();
}

Token_value OUTL::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,3,"REJECT","PROBABILITY","INFORMATION"))
  {
    if (keyIs("REJECT")) setOUTL_REJE(getBoolean(input_stream));
    if (keyIs("PROBABILITY")) setOUTL_PROB(get1num(input_stream));
    if (keyIs("INFORMATION")) setOUTL_INFO(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void OUTL::setOUTL_REJE(bool do_outlier)
{ OUTLIER.REJECT = do_outlier; }

void OUTL::setOUTL_INFO(double info)
{
  OUTLIER.INFO = ispos(info);
  OUTLIER.SIGESQ_CENT = lookup_sigesq_cent(info);
  OUTLIER.SIGESQ_ACENT = lookup_sigesq_acent(info);
}

void OUTL::setOUTL_PROB(double outlier_prob)
{
  if (outlier_prob > OUTLIER.MAXPROB)
  CCP4base::store(PhaserError(INPUT,keywords,"Outlier probability must be < " + dtos(OUTLIER.MAXPROB)));
  OUTLIER.PROB = ispos(outlier_prob);
}

void OUTL::analyse(void)
{
}


} //phaser
