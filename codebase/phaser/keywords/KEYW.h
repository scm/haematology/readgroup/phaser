//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __KEYWClass__
#define __KEYWClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>

namespace phaser {

class KEYW : public InputBase, virtual public CCP4base
{
  public:
    Boolean DO_KEYWORDS;

    KEYW();
    virtual ~KEYW() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setKEYW(bool);
};

} //phaser

#endif
