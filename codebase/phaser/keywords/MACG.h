//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACGClass__
#define __MACGClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolGYRE.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACG : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr>  MACRO_GYRE;
    double MACG_SIGR,MACG_SIGT;
    map_str_float MACG_CHAIN_SIGR,MACG_CHAIN_SIGT;
    bool   MACG_ANCH;

    MACG();
    virtual ~MACG() {}

  public:
    data_protocol MACG_PROTOCOL;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACG_FULL(bool,bool,bool,bool,double,double,int,std::string);
    void addMACG(bool,bool,bool);
    void setMACG_ANCH(bool);
    void setMACG_SIGR(double);
    void setMACG_SIGT(double);
    void setMACG_CHAI_SIGR(std::string,double);
    void setMACG_CHAI_SIGT(std::string,double);
    void setMACG_PROT(std::string);
};

} //phaser

#endif
