//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MUTE.h>

namespace phaser {

MUTE::MUTE() : CCP4base(), InputBase()
{
  Add_Key("MUTE");
  // Add to CCP4base
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MUTE_ON = false;
}

std::string MUTE::unparse()
{
  return (MUTE_ON) ? "MUTE ON\n" : "";
}

Token_value MUTE::parse(std::istringstream& input_stream)
{
  setMUTE(getBoolean(input_stream));
  return skip_line(input_stream);
}

void MUTE::setMUTE(bool v)
{ MUTE_ON = v; }

void MUTE::analyse(void)
{
}

}//phaser
