//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACSClass__
#define __MACSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolSAD.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACS : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr> MACRO_SAD;

    MACS();
    virtual ~MACS() { }

    void setMACS_DEFAULT_PROTOCOL();
    void setMACS_FIX_FDP_PROTOCOL(bool=false);
    void setMACS_REFINE_FDP_PROTOCOL(bool=false);
    data_protocol MACS_PROTOCOL;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACS(bool,bool,bool,bool,bool,bool,bool,bool,bool,bool,int,std::string,std::string);
    void setMACS_PROT(std::string);
    void setMACS_NAT_PROTOCOL();
    void setMACS_NULL_PROTOCOL();
    void setMACS_OFF_PROTOCOL();
};

} //phaser

#endif
