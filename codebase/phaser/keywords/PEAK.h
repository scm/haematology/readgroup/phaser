//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PEAKClass__
#define __PEAKClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_peak.h>

namespace phaser {

class PEAK : public InputBase, virtual public CCP4base
{
  public:
    data_peak_rot PEAKS_ROT;
    data_peak_tra PEAKS_TRA;

    PEAK();
    virtual ~PEAK() {}

  private:
    void setPEAK_SELE(bool,std::string);
    void setPEAK_CLUS(bool,bool);
    void setPEAK_CUTO(bool,floatType);
    void setPEAK_LEVE(bool,floatType);

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPEAK_ROTA_SELE(std::string);
    void setPEAK_TRAN_SELE(std::string);
    void setPEAK_ROTA_CUTO(floatType);
    void setPEAK_TRAN_CUTO(floatType);
    void setPEAK_ROTA_CLUS(bool);
    void setPEAK_TRAN_CLUS(bool);
    void setPEAK_ROTA_DOWN(double);
    void setPEAK_ROTA_LEVE(double);
    void setPEAK_TRAN_LEVE(double);
};

} //phaser

#endif
