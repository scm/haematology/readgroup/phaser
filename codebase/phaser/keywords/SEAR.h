//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SEARClass__
#define __SEARClass__
#include <phaser/io/CCP4base.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/include/data_bofac.h>

namespace phaser {

class SEAR : public InputBase, virtual public CCP4base
{
  public:
    search_array AUTO_SEARCH;
    af_string SEARCH_MODLID; //the "current" search modlid set
    af_string SEARCH_ZERO; //for auto search determination, only one MODLID per zero search
    bool SEARCH_ORDER_AUTO;
    bool SEARCH_PRUNE;
    bool SEARCH_AMAL_USE;
    data_bofac SEARCH_FACTORS;
    std::string SEARCH_METHOD;

    bool SEARCH_FROM_AUTO; //internal flag

    SEAR();
    virtual ~SEAR() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addSEAR_ENSE_NUM(std::string,int);
    void addSEAR_ENSE_OR_ENSE_NUM(af_string,int);
    void setSEAR_ENSE_OR_ENSE(af_string);
    void setSEAR_ORDE_AUTO(bool);
    void setSEAR_HIRE(bool);
    void setSEAR_PRUN(bool);
    void setSEAR_BFAC(double);
    void setSEAR_OFAC(double);
    void setSEAR_AMAL_USE(bool);
    void setSEAR_METH(std::string);

    //keyword input only, do not boost, use deprecated where possible
    void setSEAR_DEEP(bool); //obsoleted, warning thrown
    void setSEAR_DOWN_PERC(double); //obsolete, warning thrown
};

} //phaser

#endif
