//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/ZSCO.h>
#include <phaser_defaults.h>

namespace phaser {

ZSCO::ZSCO() : CCP4base(), InputBase()
{
  Add_Key("ZSCO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  ZSCORE= data_zscore();
}

std::string ZSCO::unparse()
{
  return ZSCORE.unparse();
}

Token_value ZSCO::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,5,"SOLVED","POSSIBLY","HALF","USE","STOP"))
  {
    if (keyIs("SOLVED")) setZSCO_SOLV(get1num(input_stream));
    if (keyIs("HALF"))   setZSCO_HALF(getBoolean(input_stream));
    if (keyIs("USE"))    setZSCO_USE(getBoolean(input_stream));
    if (keyIs("STOP"))   setZSCO_STOP(getBoolean(input_stream));
    if (keyIs("POSSIBLY"))
    {
      compulsoryKey(input_stream,1,"SOLVED");
      setZSCO_POSS_SOLV(get1num(input_stream));
    }
  }
  return skip_line(input_stream);
}

void ZSCO::setZSCO_SOLV(floatType zscore)      { ZSCORE.SOLVED = iszeropos(zscore); }
void ZSCO::setZSCO_POSS_SOLV(floatType zscore) { ZSCORE.POSSIBLY_SOLVED = iszeropos(zscore); }
void ZSCO::setZSCO_HALF(bool b)                { ZSCORE.HALF = b; }
void ZSCO::setZSCO_USE(bool b)                 { ZSCORE.USE = b; }
void ZSCO::setZSCO_STOP(bool b)                { ZSCORE.STOP = b; }

void ZSCO::analyse(void)
{
}

}//phaser
