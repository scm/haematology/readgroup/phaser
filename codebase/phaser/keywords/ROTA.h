//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ROTAClass__
#define __ROTAClass__
#include <phaser/io/CCP4base.h>
#include <phaser_defaults.h>

namespace phaser {

class ROTA : public InputBase, virtual public CCP4base
{
  public:
    bool      DO_ROTATE_AROUND;
    dvect3    ROTATE_EULER;
    floatType ROTATE_RANGE;
    bool      USE_ROTATE_LMAX_RESO;
    bool      NEW_CLUSTERING;

    ROTA();
    virtual ~ROTA() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setROTA_VOLU(std::string);
    void setROTA_EULE(dvect3);
    void setROTA_RANG(floatType);
    void setROTA_LMAX_RESO(bool);
    void setROTA_ANGL(double e1,double e2,double e3) { setROTA_EULE(dvect3(e1,e2,e3)); }
    void set_new_clustering(bool b) { NEW_CLUSTERING = b; }
};

} //phaser

#endif
