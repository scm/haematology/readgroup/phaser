//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACA.h>

namespace phaser {

MACA::MACA() : CCP4base(), InputBase()
{
  Add_Key("MACA");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_ANO.clear();
}

std::string MACA::unparse()
{
  std::string Card("");
  if (!MACA_PROTOCOL.is_default() && !MACA_PROTOCOL.is_custom())
    Card += "MACANO PROTOCOL " + MACA_PROTOCOL.unparse() + "\n";
  if (MACA_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_ANO.size() != 3) //not default
    {
      Card += "MACANO PROTOCOL " + MACA_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_ANO.size(); i++)
        Card += MACRO_ANO[i]->unparse();
    }
    else if (!MACRO_ANO[0]->is_default() || //not default
             !MACRO_ANO[1]->is_default() ||
             !MACRO_ANO[2]->is_default())
    {
      Card += "MACANO PROTOCOL " + MACA_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_ANO.size(); i++)
        Card += MACRO_ANO[i]->unparse();
    }
  }
  return Card;
}

Token_value MACA::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"PROTOCOL","ANISOTROPIC");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACA_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACA_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACA_PROT("OFF");
    if (keyIs("ALL"))     setMACA_PROT("ALL");
  }
  if (keyIs("ANISOTROPIC"))
  {
    bool REF_ANISO = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"BINS");
    bool REF_BINS = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SOLK");
    bool REF_SOLK = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SOLB");
    bool REF_SOLB = getBoolean(input_stream);
    int NCYC(-999);
    std::string MINIMIZER("");
    while (optionalKey(input_stream,2,"NCYC","MINIMIZER"))
    {
      if (keyIs("NCYC")) NCYC = get1num(input_stream);
      if (keyIs("MINIMIZER")) MINIMIZER = getString(input_stream);
    }
    addMACA(REF_ANISO,REF_BINS,REF_SOLK,REF_SOLB,NCYC,MINIMIZER);
  }
  return skip_line(input_stream);
}

void MACA::addMACA(bool ref_anis,bool ref_bins,bool ref_solk,bool ref_solb,int ncyc,std::string minimizer)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  protocolPtr cPtr(new ProtocolANO(!ref_anis,!ref_bins,!ref_solk,!ref_solb,ncyc,minimizer));
  MACRO_ANO.push_back(cPtr);
}

void MACA::setMACA_PROT(std::string method)
{
  bool error = MACA_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACA::analyse(void)
{
  if (MACA_PROTOCOL.is_default() && MACRO_ANO.size())
  {
    MACA_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACA_PROTOCOL.is_default() || (MACA_PROTOCOL.is_custom() && !MACRO_ANO.size()))
  {
    MACRO_ANO.clear();
    protocolPtr cPtr(new ProtocolANO());
    MACRO_ANO.push_back(cPtr);
    MACRO_ANO.push_back(cPtr);
    MACRO_ANO.push_back(cPtr); //there are convergence issues
  }
  else if (MACA_PROTOCOL.is_off())
  {
    MACRO_ANO.clear();
    ProtocolANO offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolANO(offMacro));
    MACRO_ANO.push_back(cPtr);
  }
  else if (MACA_PROTOCOL.is_all())
  {
    MACRO_ANO.clear();
    ProtocolANO offMacro;
    offMacro.all();
    protocolPtr cPtr(new ProtocolANO(offMacro));
    MACRO_ANO.push_back(cPtr);
  }
}

}//phaser
