//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ROOTClass__
#define __ROOTClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class ROOT : public InputBase, virtual public CCP4base
{
  public:
    std::string FILEROOT;

    ROOT();
    virtual ~ROOT() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setROOT(std::string);
};

} //phaser

#endif
