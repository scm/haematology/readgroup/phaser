//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __NORMClass__
#define __NORMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_norm.h>
#include <boost/logic/tribool.hpp>

namespace phaser {

class NORM : public InputBase, virtual public CCP4base
{
  public:
    data_norm SIGMAN;

    NORM();
    virtual ~NORM() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setNORM_DATA(data_norm sigman) { SIGMAN = sigman; }
};

} //phaser

#endif
