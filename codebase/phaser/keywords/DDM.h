//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __DDMClass__
#define __DDMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_ddm.h>

namespace phaser {

class DDM : public InputBase, virtual public CCP4base
{
  public:
    data_ddm NMADDM;

    DDM();
    virtual ~DDM() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setDDM_SLID(floatType);
    void setDDM_DIST_STEP(floatType);
    void setDDM_DIST_MIN(floatType);
    void setDDM_DIST_MAX(floatType);
    void setDDM_JOIN_MIN(floatType);
    void setDDM_JOIN_MAX(floatType);
    void setDDM_SEQU_MIN(floatType);
    void setDDM_SEQU_MAX(floatType);
    void setDDM_SEPA_MIN(floatType);
    void setDDM_SEPA_MAX(floatType);
    void setDDM_CLUS(std::string);
};

} //phaser

#endif
