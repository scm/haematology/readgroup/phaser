//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RESOClass__
#define __RESOClass__
#include <phaser/io/CCP4base.h>
#include <phaser_defaults.h>

namespace phaser {

class RESO : public InputBase, virtual public CCP4base
{
  public:
    floatType HIRES,AUTO_HIRES;
    floatType LORES;
    bool      DO_AUTO_HIRES;

    RESO();
    virtual ~RESO() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setRESO(floatType,floatType);
    void setRESO_HIGH(floatType);
    void setRESO_LOW(floatType);
    void setRESO_AUTO_HIGH(floatType);
    void setRESO_AUTO_OFF();
};

} //phaser

#endif
