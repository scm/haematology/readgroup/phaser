//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __JOBSClass__
#define __JOBSClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class JOBS : public InputBase, virtual public CCP4base
{
  public:
    unsigned NJOBS;

    JOBS();
    virtual ~JOBS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setJOBS(floatType);
};

} //phaser

#endif
