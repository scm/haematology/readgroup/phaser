//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PURGClass__
#define __PURGClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_purge.h>

namespace phaser {

class PURG : public InputBase, virtual public CCP4base
{
  public:
    data_purge_rot  PURGE_ROT;
    data_purge_tra  PURGE_TRA;
    data_purge_rnp  PURGE_RNP;
    data_purge_gyre PURGE_GYRE;

    PURG();
    virtual ~PURG() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPURG_ROTA_ENAB(bool);
    void setPURG_ROTA_PERC(floatType);
    void setPURG_ROTA_NUMB(floatType);
    void setPURG_TRAN_ENAB(bool);
    void setPURG_TRAN_PERC(floatType);
    void setPURG_TRAN_NUMB(floatType);
    void setPURG_RNP_ENAB(bool);
    void setPURG_RNP_PERC(floatType);
    void setPURG_RNP_NUMB(floatType);
    void setPURG_GYRE_ENAB(bool);
    void setPURG_GYRE_PERC(floatType);
    void setPURG_GYRE_NUMB(floatType);
};

} //phaser

#endif
