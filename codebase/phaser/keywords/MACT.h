//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACTClass__
#define __MACTClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolNCS.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACT : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr>  MACRO_TNCS;

    MACT();
    virtual ~MACT() {}

  private:
    data_protocol MACT_PROTOCOL;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACT(bool,bool,bool,floatType,std::string);
    void setMACT_PROT(std::string);
};

} //phaser

#endif
