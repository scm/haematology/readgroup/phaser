//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SAMPClass__
#define __SAMPClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class SAMP : public InputBase, virtual public CCP4base
{
  public:
    floatType ROT_SAMPLING,TRA_SAMPLING;

    SAMP();
    virtual ~SAMP() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSAMP_ROTA(floatType);
    void setSAMP_TRAN(floatType);
};

} //phaser

#endif
