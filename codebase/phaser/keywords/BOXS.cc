//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/BOXS.h>
#include <phaser_defaults.h>

namespace phaser {

BOXS::BOXS() : CCP4base(), InputBase()
{
  Add_Key("BOXS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  BOXSCALE = floatType(DEF_BOXS);
}

std::string BOXS::unparse()
{
  std::string Card("");
  if (BOXSCALE != floatType(DEF_BOXS))
    Card += "BOXSCALE " + dtos(BOXSCALE) + "\n";
  return Card;
}

Token_value BOXS::parse(std::istringstream& input_stream)
{
  setBOXS(get1num(input_stream));
  return skip_line(input_stream);
}

void BOXS::setBOXS(floatType boxscale)
{
  if (boxscale < 2.4)
  CCP4base::store(PhaserError(INPUT,keywords,"Boxscale must be >2.4 (minimum sampling of molecular transform)"));
  BOXSCALE = ispos(boxscale);
}

void BOXS::analyse(void)
{
}

}//phaser
