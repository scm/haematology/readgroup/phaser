//(c) 2000-2018 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __INFOClass__
#define __INFOClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>

namespace phaser {

class INFO : public InputBase, virtual public CCP4base
{
  public:
    Boolean DO_INFORMATION;

    INFO();
    virtual ~INFO() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setINFO(bool);
};

} //phaser

#endif
