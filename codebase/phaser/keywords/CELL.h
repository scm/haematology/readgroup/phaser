//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __CELLClass__
#define __CELLClass__
#include <phaser/io/CCP4base.h>
#include <cctbx/uctbx.h>

namespace phaser {

class CELL : public InputBase, virtual public CCP4base
{
  public:
    af::double6 UNIT_CELL;

    CELL();
    virtual ~CELL() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setCELL(floatType,floatType,floatType,floatType,floatType,floatType);
    void setCELL6(af::double6);
};

} //phaser

#endif
