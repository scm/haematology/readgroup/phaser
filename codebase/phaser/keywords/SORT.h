//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SORTClass__
#define __SORTClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_solpar.h>

namespace phaser {

class SORT : public InputBase, virtual public CCP4base
{
  public:
    bool SORT_LLG;

    SORT();
    virtual ~SORT() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSORT(bool);
};

} //phaser

#endif
