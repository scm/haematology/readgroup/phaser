//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PEAK.h>

namespace phaser {

PEAK::PEAK() : CCP4base(), InputBase()
{
  Add_Key("PEAK");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string PEAK::unparse()
{
  std::string Card("");
  Card += PEAKS_ROT.unparse();
  Card += PEAKS_TRA.unparse();
  return Card;
}

Token_value PEAK::parse(std::istringstream& input_stream)
{
  int key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  bool rot(false);
  compulsoryKey(input_stream,2,"ROT","TRA");
  if (keyIs("ROT")) rot = true;
  while (optionalKey(input_stream,5,"SELECT","CLUSTER","CUTOFF","DOWN","LEVEL"))
  {
    if (keyIs("SELECT"))
    {
      compulsoryKey(input_stream,4,"SIG","NUM","PERCENT","ALL");
      if (keyIs("SIG"))     setPEAK_SELE(rot,"SIGMA");
      if (keyIs("NUM"))     setPEAK_SELE(rot,"NUMBER");
      if (keyIs("PERCENT")) setPEAK_SELE(rot,"PERCENT");
      if (keyIs("ALL"))     setPEAK_SELE(rot,"ALL");
    }
    else if (keyIs("CUTOFF"))  setPEAK_CUTO(rot,get1num(input_stream));
    else if (keyIs("CLUSTER")) setPEAK_CLUS(rot,getBoolean(input_stream));
    else if (keyIs("LEVEL")) setPEAK_LEVE(rot,get1num(input_stream));
    else if (keyIs("DOWN") && rot) setPEAK_ROTA_DOWN(get1num(input_stream));
  }
  setKeyLength(key_len);
  return skip_line(input_stream);
}

//expert
void PEAK::setPEAK_SELE(bool rot,std::string test)
{
  bool error =  rot ? PEAKS_ROT.set_select(test) : PEAKS_TRA.set_select(test);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Peaks selection not recognised"));
}
void PEAK::setPEAK_CUTO(bool rot,floatType num)
{
  if (rot)
  {
    if (PEAKS_ROT.by_percent()) PEAKS_ROT.set_cutoff(isperc(num));
    if (PEAKS_ROT.by_sigma())   PEAKS_ROT.set_cutoff(ispos(num));
    if (PEAKS_ROT.by_number())  PEAKS_ROT.set_cutoff(isposi(num));
  }
  else
  {
    if (PEAKS_TRA.by_percent()) PEAKS_TRA.set_cutoff(isperc(num));
    if (PEAKS_TRA.by_sigma())   PEAKS_TRA.set_cutoff(ispos(num));
    if (PEAKS_TRA.by_number())  PEAKS_TRA.set_cutoff(isposi(num));
  }
}

void PEAK::setPEAK_LEVE(bool rot,floatType num)
{
  rot ? PEAKS_ROT.LEVEL  = iszeroposi(num) : PEAKS_TRA.LEVEL = iszeroposi(num);
}

void PEAK::setPEAK_CLUS(bool rot,bool cluster)
{
  rot ? PEAKS_ROT.CLUSTER = cluster : PEAKS_TRA.CLUSTER = cluster;
}

//non_expert
void PEAK::setPEAK_ROTA_SELE(std::string select)  { setPEAK_SELE(true,select); }
void PEAK::setPEAK_TRAN_SELE(std::string select)  { setPEAK_SELE(false,select); }
void PEAK::setPEAK_ROTA_CLUS(bool b) { setPEAK_CLUS(true,b);}
void PEAK::setPEAK_TRAN_CLUS(bool b) { setPEAK_CLUS(false,b);}
void PEAK::setPEAK_ROTA_CUTO(floatType num)  { setPEAK_CUTO(true,num);}
void PEAK::setPEAK_TRAN_CUTO(floatType num)  { setPEAK_CUTO(false,num);}
void PEAK::setPEAK_ROTA_LEVE(floatType num)  { setPEAK_LEVE(true,num);}
void PEAK::setPEAK_TRAN_LEVE(floatType num)  { setPEAK_LEVE(false,num);}
void PEAK::setPEAK_ROTA_DOWN(double num){ PEAKS_ROT.DOWN = isperc(num);}

void PEAK::analyse(void)
{
}

}//phaser
