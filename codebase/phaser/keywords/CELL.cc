//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/CELL.h>
#include <phaser_defaults.h>

namespace phaser {

CELL::CELL() : CCP4base(), InputBase()
{
  Add_Key("CELL");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  UNIT_CELL = DEF_CELL;
}

std::string CELL::unparse()
{
  std::string Card("");
  bool DEFAULT_CELL = cctbx::uctbx::unit_cell(UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
  if (!DEFAULT_CELL)
  {
    Card = "CELL";
    for (int n = 0; n < 6; n++) Card +=  " " + dtos(UNIT_CELL[n]);
    Card += "\n";
  }
  return Card;
}

Token_value CELL::parse(std::istringstream& input_stream)
{
  dmat6 cell = get6nums(input_stream);
  setCELL(cell[0],cell[1],cell[2],cell[3],cell[4],cell[5]);
  return skip_line(input_stream);
}

void CELL::setCELL6(af::double6 cell)
{ setCELL(cell[0],cell[1],cell[2],cell[3],cell[4],cell[5]); }

void CELL::setCELL(floatType a,floatType b,floatType c,floatType alfa,floatType beta,floatType gama)
{
  af::double6 check_input;
  check_input[0] = ispos(a);
  check_input[1] = ispos(b);
  check_input[2] = ispos(c);
  check_input[3] = ispos(alfa);
  check_input[4] = ispos(beta);
  check_input[5] = ispos(gama);
  if (!cctbx::uctbx::unit_cell(check_input).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001))
  UNIT_CELL = check_input;
}

void CELL::analyse(void)
{
}

}//phaser
