//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __OCCUClass__
#define __OCCUClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_occupancy.h>

namespace phaser {

class OCCU : public InputBase, virtual public CCP4base
{
  public:
    data_occupancy OCCUPANCY;

    OCCU();
    virtual ~OCCU() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setOCCU_WIND_NRES(double);
    void setOCCU_WIND_ELLG(double);
    void setOCCU_WIND_MAX(double);
    void setOCCU_MIN(double);
    void setOCCU_MAX(double);
    void setOCCU_REFI_REJE(bool);
    void setOCCU_MERG(bool);
    void setOCCU_OFFS(double);
    void setOCCU_FRAC(double);
    void setOCCU_BIAS(double);
};

} //phaser

#endif
