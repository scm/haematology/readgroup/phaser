//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/TARG.h>
#include <phaser_defaults.h>

namespace phaser {

TARG::TARG() : CCP4base(), InputBase()
{
  Add_Key("TARG");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  TARGET_ROT = std::string(DEF_TARG_ROTA);
  TARGET_TRA = std::string(DEF_TARG_TRAN);
  TARGET_ROT_TYPE = "LERF1"; //hardwired
  TARGET_TRA_TYPE = "LETF1"; //hardwired
}

std::string TARG::unparse()
{
  std::string Card("");
  if (TARGET_ROT != std::string(DEF_TARG_ROTA))
  Card += "TARGET ROT " + TARGET_ROT + std::string(TARGET_ROT == "FAST" ? " TYPE " + TARGET_ROT_TYPE : "") + "\n";
  if (TARGET_TRA != std::string(DEF_TARG_TRAN))
  Card += "TARGET TRA " + TARGET_TRA + std::string(TARGET_TRA == "FAST" ? " TYPE " + TARGET_TRA_TYPE : "") + "\n";
  return Card;
}

Token_value TARG::parse( std::istringstream& input_stream)
{
  int default_key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  while (optionalKey(input_stream,2,"ROT","TRA"))
  {
    setKeyLength(8);
    if (keyIs("ROT"))
    {
      compulsoryKey(input_stream,2,"FAST","BRUTE");
      setTARG_ROTA(stoup(string_value)); //keylength=8
      if (keyIs("FAST"))
      {
        if (optionalKey(input_stream,1,"TYPE"))
        {
          compulsoryKey(input_stream,2,"CROWTHER","LERF1");
          setTARG_ROTA_TYPE(stoup(string_value)); //keylength=8
        }
      }
    }
    if (keyIs("TRA"))
    {
      setKeyLength(8);
      compulsoryKey(input_stream,3,"FAST","BRUTE","PHASED");
      setTARG_TRAN(stoup(string_value)); //keylength=8
      if (keyIs("FAST"))
      {
        if (optionalKey(input_stream,1,"TYPE"))
        {
          compulsoryKey(input_stream,9,"CORRELATION","LETF1","LETF2","LETF1A","LETF2A","LETFL","LETFQ","LETFLA","LETFQA");
          setTARG_TRAN_TYPE(stoup(string_value));
        }
      }
    }
  }
  setKeyLength(default_key_len); //use all the unique chars for comparison
  return skip_line(input_stream);
}

void TARG::setTARG_ROTA(std::string tname)
{
  tname = stoup(tname);
  if ( tname != "FAST"
    && tname != "BRUTE"
    ) CCP4base::store(PhaserError(INPUT,keywords,"ROT target not recognised"));
  TARGET_ROT = tname;
}

void TARG::setTARG_TRAN(std::string tname)
{
  tname = stoup(tname);
  if ( tname != "FAST"
    && tname != "BRUTE"
    && tname != "PHASED"
    ) CCP4base::store(PhaserError(INPUT,keywords,"TRA target not recognised"));
  TARGET_TRA = tname;
}

void TARG::setTARG_ROTA_TYPE(std::string tname)
{
  tname = stoup(tname);
  if (   tname != "LERF1"
      && tname != "CROWTHER" //undocumented,see TRA paper
     ) CCP4base::store(PhaserError(INPUT,keywords,"ROT fast target not recognised"));
  TARGET_ROT_TYPE = tname;
}

void TARG::setTARG_TRAN_TYPE(std::string tname)
{
  tname = stoup(tname);
  if (   tname == "LETFL" //undocumented,see TRA paper
      && tname == "LETFQ" //undocumented,see TRA paper
      && tname == "LETFLA" //undocumented,see TRA paper
      && tname == "LETFQA" //undocumented,see TRA paper
      ) CCP4base::store(PhaserError(INPUT,keywords,"Sorry, Fast TF Target \"" + tname + "\" has been deprecated"));
  else
  if (   tname != "LETF1"
      && tname != "LETF2" //undocumented,see TRA paper
      && tname != "LETF1A" //undocumented,see TRA paper
      && tname != "LETF2A" //undocumented,see TRA paper
      && tname != "CORRELATION" //undocumented,see TRA paper
      ) CCP4base::store(PhaserError(INPUT,keywords,"TRA fast target not recognised"));
  TARGET_TRA_TYPE = tname;
}

void TARG::analyse(void)
{
}

}//phaser
