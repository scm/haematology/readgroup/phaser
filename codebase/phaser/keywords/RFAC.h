//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RFACClass__
#define __RFACClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class RFAC : public InputBase, virtual public CCP4base
{
  public:
    floatType RFAC_CUTOFF;
    bool      RFAC_USE;

    RFAC();
    virtual ~RFAC() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setRFAC_USE(bool);
    void setRFAC_CUTO(floatType);
};

} //phaser

#endif
