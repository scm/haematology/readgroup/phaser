//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/RESC.h>
#include <phaser_defaults.h>

namespace phaser {

RESC::RESC() : CCP4base(), InputBase()
{
  Add_Key("RESC");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  //hardwired defaults, not set by user
  DO_RESCORE_ROT = NOT_SET;
  DO_RESCORE_TRA = NOT_SET;
}

std::string RESC::unparse()
{
  std::string Card("");
  //default is on, set in analyse
  if (DO_RESCORE_ROT == OFF) Card += "RESCORE ROT OFF\n";
  if (DO_RESCORE_TRA == OFF) Card += "RESCORE TRA OFF\n";
  //else NOT_SET
  return Card;
}

Token_value RESC::parse( std::istringstream& input_stream)
{
  int key_len = getKeyLength();
  setKeyLength(3); //ROT TRA NUM
  compulsoryKey(input_stream,2,"ROT","TRA");
  if (keyIs("ROT")) setRESC_ROTA(getBoolean(input_stream));
  if (keyIs("TRA")) setRESC_TRAN(getBoolean(input_stream));
  setKeyLength(key_len);
  return skip_line(input_stream);
}

void RESC::setRESC_ROTA(bool do_rescore) { DO_RESCORE_ROT = do_rescore ? ON : OFF; }
void RESC::setRESC_TRAN(bool do_rescore) { DO_RESCORE_TRA = do_rescore ? ON : OFF; }

void RESC::analyse(void)
{
  if (DO_RESCORE_ROT == NOT_SET) DO_RESCORE_ROT = ON;
  if (DO_RESCORE_TRA == NOT_SET) DO_RESCORE_TRA = ON;
}

}//phaser
