//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __EIGEClass__
#define __EIGEClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class EIGE : public InputBase, virtual public CCP4base
{
  public:
    bool        EIGE_WRITE;
    std::string EIGE_READ;

    EIGE();
    virtual ~EIGE() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setEIGE_READ(std::string);
    void setEIGE_WRIT(bool);
};

} //phaser

#endif
