//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SCEDClass__
#define __SCEDClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_sceds.h>

namespace phaser {

class SCED : public InputBase, virtual public CCP4base
{
  public:
    data_sceds SCEDS;

    SCED();
    virtual ~SCED() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSCED_NDOM(floatType);
    void setSCED_WEIG_SPHE(floatType);
    void setSCED_WEIG_DENS(floatType);
    void setSCED_WEIG_CONT(floatType);
    void setSCED_WEIG_EQUA(floatType);
};

} //phaser

#endif
