//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/JOBS.h>
#include <phaser_defaults.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

JOBS::JOBS() : CCP4base(), InputBase()
{
  Add_Key("JOBS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  NJOBS = int(DEF_JOBS);
}

std::string JOBS::unparse()
{
  if (NJOBS == int(DEF_JOBS)) return "";
  return "JOBS " + itos(NJOBS) + "\n";
}

Token_value JOBS::parse(std::istringstream& input_stream)
{
  setJOBS(get1num(input_stream));
  return skip_line(input_stream);
}

void JOBS::setJOBS(floatType njobs)
{
  NJOBS = isposi(njobs); //store literal input
}

void JOBS::analyse(void)
{
#if _OPENMP
  unsigned nthreads(1);
#pragma omp parallel
#pragma omp master
  {
    nthreads = omp_get_num_procs();
  }
  omp_set_num_threads(std::min(isposi(NJOBS),nthreads));
#endif
}

}//phaser
