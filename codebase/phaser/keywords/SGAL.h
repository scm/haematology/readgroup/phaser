//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SGALClass__
#define __SGALClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class SGAL : public InputBase, virtual public CCP4base
{
  public:
    af_string     SGALT_HALL;
    std::string   DO_SGALT;
    bool SGALT_IS_DEFAULT;
    std::string   SGALT_BASE;

    SGAL();
    virtual ~SGAL() {}

  private:
    void addSG(std::string);

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addSGAL_TEST(std::string);
    void setSGAL_SELE(std::string);
    void setSGAL_BASE(std::string);

  //internal control
    void setupSGAL(std::string);
};

} //phaser

#endif
