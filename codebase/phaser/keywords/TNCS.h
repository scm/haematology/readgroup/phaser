//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TNCSClass__
#define __TNCSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>
#include <phaser/include/data_tncs.h>

namespace phaser {

class TNCS : public InputBase, virtual public CCP4base
{
  public:
    data_tncs PTNCS;
    Boolean   DO_TNCS_RLIST_ADD;

    TNCS();
    virtual ~TNCS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setTNCS(data_tncs t) { PTNCS = t; }

    void setTNCS_ROTA_ANGL(dvect3);
    void setTNCS_ROTA_RANG(double);
    void setTNCS_ROTA_SAMP(double);
    void setTNCS_TRAN_PERT(bool);
    void setTNCS_TRAN_VECT(dvect3);
    void setTNCS_VARI_RMSD(double);
    void setTNCS_VARI_FRAC(double);
    void setTNCS_VARI_BINS(float1D);
    void setTNCS_VARI_RESO(float1D);
    void setTNCS_GFUN_RADI(double);
    void setTNCS_NMOL(double);
    void setTNCS_MAXN(double);
    void setTNCS_LINK_REST(bool);
    void setTNCS_LINK_SIGM(double);
    void setTNCS_PAIR_ONLY(bool);
    void setTNCS_USE(bool);
    void setTNCS_PATT_HIRE(double);
    void setTNCS_PATT_LORE(double);
    void setTNCS_PATT_PERC(double);
    void setTNCS_PATT_DIST(double);
    void setTNCS_PATT_MAPS(bool);
    void setTNCS_RLIS_ADD(bool);
    void setTNCS_COMM_PEAK(double);
    void setTNCS_COMM_TOLF(double);
    void setTNCS_COMM_TOLO(double);
    void setTNCS_COMM_PATT(double);
    void setTNCS_COMM_MISS(double);

};

} //phaser

#endif
