//(c) 2000-2018 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/INFO.h>

namespace phaser {

INFO::INFO() : CCP4base(), InputBase()
{
  Add_Key("INFO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_INFORMATION.Default(false);
}

std::string INFO::unparse()
{
  if (DO_INFORMATION.unparse() != "") return "INFORMATION " + DO_INFORMATION.unparse() + "\n";
  return "";
}

Token_value INFO::parse(std::istringstream& input_stream)
{
  setINFO(getBoolean(input_stream));
  return skip_line(input_stream);
}

void INFO::setINFO(bool do_information) { DO_INFORMATION.Set(do_information); }

void INFO::analyse(void)
{
}

} //phaser
