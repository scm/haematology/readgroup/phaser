//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/TOPF.h>
#include <phaser_defaults.h>

namespace phaser {

TOPF::TOPF() : CCP4base(), InputBase()
{
  Add_Key("TOPF");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  NUM_TOPFILES = int(DEF_TOPF);
}

std::string TOPF::unparse()
{
  if (NUM_TOPFILES == int(DEF_TOPF)) return "";
  return "TOPFILES " + itos(NUM_TOPFILES) + "\n";
}

Token_value TOPF::parse(std::istringstream& input_stream)
{
  setTOPF(get1num(input_stream));
  return skip_line(input_stream);
}

void TOPF::setTOPF(floatType num)
{
  NUM_TOPFILES = iszeroposi(num);
}

void TOPF::analyse(void)
{
}

}//phaser
