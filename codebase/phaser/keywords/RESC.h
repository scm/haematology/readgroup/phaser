//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __RESCClass__
#define __RESCClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>

namespace phaser {

class RESC : public InputBase, virtual public CCP4base
{
  public:
    on_off DO_RESCORE_ROT,DO_RESCORE_TRA;

    RESC();
    virtual ~RESC() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setRESC_ROTA(bool);
    void setRESC_TRAN(bool);
};

} //phaser

#endif
