//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/KILL.h>
#include <phaser_defaults.h>

namespace phaser {

KILL::KILL() : CCP4base(), InputBase()
{
  Add_Key("KILL");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  FILEKILL = std::string(DEF_KILL_FILE);
  TIMEKILL = 0;
}

std::string KILL::unparse()
{
  std::string Card("");
  if (FILEKILL != "") Card += "KILL FILE \"" + FILEKILL + "\"\n";
  if (TIMEKILL > 0) Card += "KILL TIME " + dtos(TIMEKILL) + "\n";
  return Card;
}

Token_value KILL::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"FILE","TIME");
  if (keyIs("FILE")) setKILL_FILE(getFileName(input_stream));
  if (keyIs("TIME")) setKILL_TIME(get1num(input_stream));
  return skip_line(input_stream);
}

void KILL::setKILL_FILE(std::string filename) { FILEKILL = filename; }
void KILL::setKILL_TIME(floatType t)          { TIMEKILL = iszeropos(t); }

void KILL::analyse(void)
{
}

} //phaser
