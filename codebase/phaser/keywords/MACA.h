//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACAClass__
#define __MACAClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolANO.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACA : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr> MACRO_ANO;

    MACA();
    virtual ~MACA() {}

  public:
    data_protocol MACA_PROTOCOL;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACA(bool,bool,bool,bool,int,std::string);
    void setMACA_PROT(std::string);
};

} //phaser

#endif
