//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/TRAN.h>
#include <phaser_defaults.h>

namespace phaser {

TRAN::TRAN() : CCP4base(), InputBase()
{
  Add_Key("TRAN");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  TRAN_PACKING_USE = bool(DEF_TRAN_PACK_USE);
  TRAN_PACKING_CUTOFF = double(DEF_TRAN_PACK_CUTO/100.0);//flag
  TRAN_PACKING_NUMBER = int(DEF_TRAN_PACK_NUMB);//flag
  TRAN_FAST_STATS = true;
  TRAN_MAPS = false;
}

std::string TRAN::unparse()
{
  std::string Card("");
  if (TRAN_PACKING_USE != bool(DEF_TRAN_PACK_USE))
  Card += "TRANSLATION PACKING USE " + card(TRAN_PACKING_USE) + "\n";
  if (TRAN_PACKING_CUTOFF != double(DEF_TRAN_PACK_CUTO/100.0)) //as above
  Card += "TRANSLATION PACKING CUTOFF " + dtos(TRAN_PACKING_CUTOFF) + "\n";
  if (TRAN_PACKING_NUMBER != int(DEF_TRAN_PACK_NUMB))
  Card += "TRANSLATION PACKING NUM " + itos(TRAN_PACKING_NUMBER) + "\n";
  if (!BTF.VOLUME.is_default())
  Card += "TRANSLATION VOLUME " + BTF.VOLUME.unparse() + "\n";
  if (BTF.VOLUME.full()) return Card;
  if (BTF.VOLUME.around())
    Card += "TRANSLATION " + std::string(BTF.FRAC ? "FRAC " : "ORTH ") +
            "START " + dtos(BTF.START[0]) + " " + dtos(BTF.START[1]) + " " + dtos(BTF.START[2]) +
            " RANGE " + dtos(BTF.RANGE) + "\n";
  else
    Card += "TRANSLATION " + std::string(BTF.FRAC ? "FRAC " : "ORTH ") +
            "START " + dtos(BTF.START[0]) + " " + dtos(BTF.START[1]) + " " + dtos(BTF.START[2]) +
            "END " + dtos(BTF.END[0]) + " " + dtos(BTF.END[1]) + " " + dtos(BTF.END[2]) + "\n";
  if (TRAN_MAPS)
  Card += "TRAN MAPS ON\n";
  if (!TRAN_FAST_STATS)
  Card += "TRAN FAST STATS OFF\n";
  return Card;
}

Token_value TRAN::parse(std::istringstream& input_stream)
{
  dvect3 point(0,0,0);
  while (optionalKey(input_stream,10,"VOLUME","START","END","RANGE","ORTH","FRAC","POINT","PACK","MAPS","FAST"))
  {
    if (keyIs("VOLUME"))
    {
      compulsoryKey(input_stream,4,"FULL","REGION","LINE","AROUND");
      if (keyIs("FULL")) setTRAN_VOLU("FULL");
      if (keyIs("REGION")) setTRAN_VOLU("REGION");
      if (keyIs("LINE")) setTRAN_VOLU("LINE");
      if (keyIs("AROUND")) setTRAN_VOLU("AROUND");
    }
    if (keyIs("START") || keyIs("POINT")) setTRAN_STAR(get3nums(input_stream));
    if (keyIs("END"))   setTRAN_END(get3nums(input_stream));
    if (keyIs("RANGE")) setTRAN_RANG(get1num(input_stream));
    if (keyIs("FRAC"))  setTRAN_FRAC(true);
    if (keyIs("ORTH"))  setTRAN_FRAC(false);
    if (keyIs("PACK"))
    {
      compulsoryKey(input_stream,3,"USE","CUTOFF","NUM");
      if (keyIs("USE"))    setTRAN_PACK_USE(getBoolean(input_stream));
      if (keyIs("CUTOFF")) setTRAN_PACK_CUTO(get1num(input_stream));
      if (keyIs("NUM"))    setTRAN_PACK_NUMB(get1num(input_stream));
    }
    if (keyIs("MAPS")) setTRAN_MAPS(getBoolean(input_stream));
    if (keyIs("FAST"))
    {
      compulsoryKey(input_stream,1,"STAT");
      setTRAN_FAST_STAT(getBoolean(input_stream));
    }
  }
  return skip_line(input_stream);
}

void TRAN::setTRAN_VOLU(std::string vol)
{
  bool error = BTF.VOLUME.set(vol);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Translation volume not recognised"));
}
void TRAN::setTRAN_RANG(floatType range) { BTF.RANGE = range; }
void TRAN::setTRAN_STAR(dvect3 v)        { BTF.START = v; }
void TRAN::setTRAN_END(dvect3 v)         { BTF.END = v; }
void TRAN::setTRAN_FRAC(bool b)          { BTF.FRAC = b; }
void TRAN::setTRAN_PACK_USE(bool b)      { TRAN_PACKING_USE = b; }
void TRAN::setTRAN_PACK_CUTO(double t)   { TRAN_PACKING_CUTOFF = isperc(t); }
void TRAN::setTRAN_PACK_NUMB(double t)   { TRAN_PACKING_NUMBER = t; }
void TRAN::setTRAN_MAPS(bool b)          { TRAN_MAPS = b; }
void TRAN::setTRAN_FAST_STAT(bool b)     { TRAN_FAST_STATS = b; }
void TRAN::setTRAN_CENT(bool frac,double t1, double t2, double t3)
{ BTF.FRAC = frac; BTF.START = dvect3(t1,t2,t3); }

void TRAN::analyse(void)
{
}

}//phaser
