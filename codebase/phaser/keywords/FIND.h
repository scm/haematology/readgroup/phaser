//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __FINDClass__
#define __FINDClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_find.h>
#include <phaser/include/data_purge.h>
#include <phaser/include/data_peak.h>

namespace phaser {

class FIND : public InputBase, virtual public CCP4base
{
  public:
    data_find  FINDSITES;
    data_peak_find  FIND_PEAKS;
    data_peak_purge FIND_PURGE;

    FIND();
    virtual ~FIND() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setFIND_SCAT(std::string);
    void setFIND_NUMB(double);
    void setFIND_CLUS(bool);
    void setFIND_PURG_SELE(std::string);
    void setFIND_PURG_CUTO(double);
    void setFIND_PURG_OCCU(double);
    void setFIND_PEAK_SELE(std::string);
    void setFIND_PEAK_CUTO(floatType);
    void setFIND_PEAK_CLUS(bool);
    void setFIND_SPEC_POSI(bool);
    void setFIND_TARG(std::string);
    void setFIND_DIST_CENT(double);
    void setFIND_DIST_DUPL(double);
    void setFIND_OCCU(double);
};

} //phaser

#endif
