//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SORT.h>
#include <phaser_defaults.h>

namespace phaser {

SORT::SORT() : CCP4base(), InputBase()
{
  Add_Key("SORT");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SORT_LLG = true; //change unparse if changed
}

std::string SORT::unparse()
{ return SORT_LLG ? "" : "SORT " + card(SORT_LLG) + "\n"; }

Token_value SORT::parse(std::istringstream& input_stream)
{
  setSORT(getBoolean(input_stream));
  return skip_line(input_stream);
}

void SORT::setSORT(bool b)  { SORT_LLG = b; }

void SORT::analyse(void)
{
}

}//phaser
