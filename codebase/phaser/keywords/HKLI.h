//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __HKLIClass__
#define __HKLIClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class HKLI : public InputBase, virtual public CCP4base
{
  public:
    std::string HKLIN;

    HKLI();
    virtual ~HKLI() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setHKLI(std::string);
};

} //phaser

#endif
