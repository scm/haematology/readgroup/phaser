//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ZSCOClass__
#define __ZSCOClass__
#include <phaser/io/CCP4base.h>
#include <cctbx/uctbx.h>
#include <phaser/include/data_zscore.h>

namespace phaser {

class ZSCO : public InputBase, virtual public CCP4base
{
  public:
    data_zscore ZSCORE;

    ZSCO();
    virtual ~ZSCO() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setZSCO_SOLV(floatType);
    void setZSCO_POSS_SOLV(floatType);
    void setZSCO_HALF(bool);
    void setZSCO_USE(bool);
    void setZSCO_STOP(bool);
    void setZSCO(data_zscore z) { ZSCORE = z; }
};

} //phaser

#endif
