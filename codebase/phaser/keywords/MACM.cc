//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACM.h>

namespace phaser {

MACM::MACM() : CCP4base(), InputBase()
{
  Add_Key("MACM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_MR.clear();
  LAST_ONLY = false;
  FIX_BFAC = false;
  MACM_CHAINS.Default(false);
  MACM_LAST = true; //change unparse below
  MACM_TFZEQ = true; //flag, not unparse
}

std::string MACM::unparse()
{
  std::string Card("");
  if (!MACM_PROTOCOL.is_default() && !MACM_PROTOCOL.is_custom())
    Card += "MACMR PROTOCOL " + MACM_PROTOCOL.unparse() + "\n";
  if (MACM_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_MR.size() != 1) //not default
    {
      Card += "MACMR PROTOCOL " + MACM_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_MR.size(); i++)
        Card += MACRO_MR[i]->unparse();
    }
    else if (!MACRO_MR[0]->is_default() ) //not default
    {
      Card += "MACMR PROTOCOL " + MACM_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_MR.size(); i++)
        Card += MACRO_MR[i]->unparse();
    }
  }
  if (MACM_CHAINS.unparse().size()) //change here
    Card = "MACMR CHAINS " + MACM_CHAINS.unparse() + "\n";
  if (MACM_LAST != true) //change here
    Card = "MACMR LAST " + card(MACM_LAST) + "\n";
  return Card;
}

Token_value MACM::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,6,"PROTOCOL","ROT","CHAINS","LAST","SIGROT","SIGTRA");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACM_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACM_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACM_PROT("OFF");
    if (keyIs("ALL"))     setMACM_PROT("ALL");
  }
  if (keyIs("ROT"))
  {
    bool ref_rot = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"TRA");
    bool ref_tra = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"BFAC");
    bool ref_bfac = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"VRMS");
    bool ref_vrms = getBoolean(input_stream);
    int ncyc(-999);
    double sigrot(-999),sigtra(-999);
    std::string minimizer("");
    bool ref_cell(DEF_MACM_REF_CELL);
    bool ref_ofac(DEF_MACM_REF_OFAC);
    bool last_only(DEF_MACM_LAST_ONLY);
    while (optionalKey(input_stream,7,"SIGROT","SIGTRA","CELL","OFAC","LAST","NCYC","MINIMIZER"))
    {
      if (keyIs("SIGROT")) sigrot = get1num(input_stream);
      if (keyIs("SIGTRA")) sigtra = get1num(input_stream);
      if (keyIs("NCYC")) ncyc = get1num(input_stream);
      if (keyIs("MINIMIZER")) minimizer = getString(input_stream);
      if (keyIs("CELL")) ref_cell = getBoolean(input_stream);
      if (keyIs("OFAC")) ref_ofac = getBoolean(input_stream);
      if (keyIs("LAST")) last_only = getBoolean(input_stream);
    }
    addMACM_FULL(ref_rot,ref_tra,ref_bfac,ref_vrms,ref_cell,ref_ofac,last_only,ncyc,minimizer,sigrot,sigtra);
  }
  if (keyIs("CHAINS"))
  {
    setMACM_CHAI(getBoolean(input_stream));
  }
  if (keyIs("LAST"))
  {
    setMACM_LAST(getBoolean(input_stream));
  }
  if (keyIs("SIGROT"))
  {
    setMACM_SIGR(get1num(input_stream));
    if (optionalKey(input_stream,1,"SIGTRA")) setMACM_SIGT(get1num(input_stream));
  }
  if (keyIs("SIGTRA"))
  {
    setMACM_SIGT(get1num(input_stream));
    if (optionalKey(input_stream,1,"SIGROT")) setMACM_SIGR(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void MACM::addMACM(bool ref_rota,bool ref_tran,bool ref_bfac,bool ref_vrms,bool ref_cell,bool ref_ofac,bool last_only,int ncyc,std::string minimizer)
{
  addMACM_FULL(ref_rota,ref_tran,ref_bfac,ref_vrms,ref_cell,ref_ofac,last_only,ncyc,minimizer,-999,-999);
}

void MACM::addMACM_FULL(bool ref_rota,bool ref_tran,bool ref_bfac,bool ref_vrms,bool ref_cell,bool ref_ofac,bool last_only,int ncyc,std::string minimizer,double sigrot,double sigtra)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  //ncyc = std::min(ncyc,DEF_MACM_NCYC);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  protocolPtr cPtr(new ProtocolMR(!ref_rota,!ref_tran,!ref_bfac,!ref_vrms,!ref_cell,!ref_ofac,last_only,ncyc,minimizer,sigrot,sigtra));
  MACRO_MR.push_back(cPtr);
  MACM_PROTOCOL.set("CUSTOM"); //must set on input, don't wait for analyse
}

void MACM::setMACM_PROT(std::string method)
{
  bool error = MACM_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACM::setMACM_ATOM_PROTOCOL(void)
{
  if (MACRO_MR.size()) return;
  ProtocolMR atomMacro;
  atomMacro.single_atom();
  protocolPtr cPtr(new ProtocolMR(atomMacro));
  MACRO_MR.push_back(cPtr);
  MACM_PROTOCOL.set("CUSTOM"); //don't change with analyse
}

void MACM::setMACM_PROTOCOL_VRMS_FIX(bool fix)
{
  MACRO_MR.clear();
  protocolPtr cPtr(new ProtocolMR());
  MACRO_MR.push_back(cPtr);
  MACRO_MR.back()->setFIX(macm_vrms,fix);
  MACM_PROTOCOL.set("CUSTOM"); //don't change with analyse
}

void MACM::setMACM_PROTOCOL_BFAC_FIX()
{
  FIX_BFAC = true;
}

void MACM::setMACM_PROTOCOL_LAST_ONLY()
{
  LAST_ONLY = true;
}

void MACM::analyse(void)
{
  if (MACM_PROTOCOL.is_default())
  {
    MACRO_MR.clear();
    protocolPtr cPtr(new ProtocolMR());
    MACRO_MR.push_back(cPtr);
  }
  else if (MACM_PROTOCOL.is_off())
  {
    MACRO_MR.clear();
    ProtocolMR offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolMR(offMacro));
    MACRO_MR.push_back(cPtr);
  }
  else if (MACM_PROTOCOL.is_all())
  {
    MACRO_MR.clear();
    ProtocolMR allMacro;
    allMacro.all();
    protocolPtr cPtr(new ProtocolMR(allMacro));
    MACRO_MR.push_back(cPtr);
  }
  if (LAST_ONLY)
  for (int i = 0; i < MACRO_MR.size(); i++)
    MACRO_MR[i]->setFIX(macm_last,true);
  if (FIX_BFAC)
  for (int i = 0; i < MACRO_MR.size(); i++)
    MACRO_MR[i]->setFIX(macm_bfac,true);
  //if some protocols have been set, then set the defaults
  for (unsigned big_cyc = 0 ; big_cyc < MACRO_MR.size(); big_cyc++)
  {
    //if the restraints are not set on parsing, then set the defaults here
    MACRO_MR[big_cyc]->setNUM(macm_sigr,MACM_SIGR);
    MACRO_MR[big_cyc]->setNUM(macm_sigt,MACM_SIGT);
  }
}

void MACM::setMACM_CHAI(bool ref) { MACM_CHAINS.Set(ref); }
void MACM::setMACM_LAST(bool ref) { MACM_LAST = ref; }
void MACM::setMACM_TFZE(bool ref) { MACM_TFZEQ = ref; }
void MACM::setMACM_SIGR(double rot)  { MACM_SIGR = iszeropos(rot); }
void MACM::setMACM_SIGT(double tra)  { MACM_SIGT = iszeropos(tra); }

}//phaser
