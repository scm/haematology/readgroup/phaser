//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACS.h>

namespace phaser {
MACS::MACS() : CCP4base(), InputBase()
{
  Add_Key("MACS");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_SAD.clear();
}

std::string MACS::unparse()
{
  std::string Card("");
  if (!MACS_PROTOCOL.is_default() && !MACS_PROTOCOL.is_custom())
    Card += "MACS PROTOCOL " + MACS_PROTOCOL.unparse() + "\n";
  if (MACS_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_SAD.size() != 1) //not default
    {
      Card += "MACS PROTOCOL " + MACS_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_SAD.size(); i++)
        Card += MACRO_SAD[i]->unparse();
    }
    else if (!MACRO_SAD[0]->is_default() ) //not default
    {
      Card += "MACS PROTOCOL " + MACS_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_SAD.size(); i++)
        Card += MACRO_SAD[i]->unparse();
    }
  }
  return Card;
}

Token_value MACS::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"PROTOCOL","XYZ");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACS_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACS_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACS_PROT("OFF");
    if (keyIs("ALL"))     setMACS_PROT("ALL");
  }
  if (keyIs("XYZ"))
  {
    bool REF_XYZ = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"OCC");
    bool REF_OCC = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"BFAC");
    bool REF_BFAC = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"FDP");
    bool REF_FDP = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SA");
    bool REF_SA = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SB");
    bool REF_SB = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SP");
    bool REF_SP = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SD");
    bool REF_SD = getBoolean(input_stream);
    int NCYC(-999);
    std::string MINIMIZER("");
    std::string TARGET(DEF_MACS_TARG);
    bool REF_PARTK(true),REF_PARTU(false);
    while (optionalKey(input_stream,5,"PK","PB","NCYC","MINIMIZER","TARGET"))
    {
      if (keyIs("PK")) REF_PARTK = getBoolean(input_stream);
      if (keyIs("PB")) REF_PARTU = getBoolean(input_stream);
      if (keyIs("NCYC")) NCYC = get1num(input_stream);
      if (keyIs("MINIMIZER")) MINIMIZER = getString(input_stream);
      if (keyIs("TARGET")) TARGET = getString(input_stream);
    }
    addMACS(REF_PARTK,REF_PARTU,REF_XYZ,REF_OCC,REF_BFAC,REF_FDP,REF_SA,REF_SB,REF_SP,REF_SD,NCYC,TARGET,MINIMIZER);
  }
  return skip_line(input_stream);
}

void MACS::addMACS(bool ref_pk,bool ref_pb,
                   bool ref_xyz,bool ref_occ,bool ref_bfac,bool ref_fdp,
                   bool ref_sa,bool ref_sb,bool ref_sp,bool ref_sd,
                   int ncyc,std::string target,std::string minimizer)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  target = stoup(target);
  if (target.size() && target != "SADM_ONLY" && target != "NOT_ANOM_ONLY")
    CCP4base::store(PhaserError(INPUT,keywords,"Unknown target " + target));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  ProtocolSAD thisMacro(!ref_xyz,!ref_occ,!ref_bfac,!ref_fdp,!ref_sa,!ref_sb,!ref_sp,!ref_sd,!ref_pk,!ref_pb,ncyc,target,minimizer);
  protocolPtr cPtr(new ProtocolSAD(thisMacro));
  MACRO_SAD.push_back(cPtr);
}

void MACS::setMACS_REFINE_FDP_PROTOCOL(bool fix_sp) //default
{
  MACRO_SAD.clear();
  { // First macrocycle just refines occupancy and fdp
    ProtocolSAD thisMacro;
    thisMacro.occ_fdp();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
  {
    ProtocolSAD thisMacro;
    thisMacro.xyz_occ_bfac_fdp_sd_sp(fix_sp);
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}

void MACS::setMACS_FIX_FDP_PROTOCOL(bool fix_sp)
{
  MACRO_SAD.clear();
  { // First macrocycle just refines occupancy
    ProtocolSAD thisMacro;
    thisMacro.occ();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
  {
    ProtocolSAD thisMacro;
    thisMacro.xyz_occ_bfac_sd_sp(fix_sp);
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}

void MACS::setMACS_NAT_PROTOCOL()
{
  MACS_PROTOCOL.set("CUSTOM");
  bool fix_sp(true); //no SP refinement
  MACRO_SAD.clear();
  { // First macrocycle just refines occupancy and fdp
    ProtocolSAD thisMacro;
    thisMacro.occ();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
  {
    ProtocolSAD thisMacro;
    thisMacro.xyz_occ_bfac_sd_sp(fix_sp);
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}

void MACS::setMACS_NULL_PROTOCOL()
{
  MACS_PROTOCOL.set("CUSTOM");
  MACRO_SAD.clear();
  { // First macrocycle just refines occupancy and fdp
    ProtocolSAD thisMacro;
    thisMacro.null();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}

void MACS::setMACS_OFF_PROTOCOL()
{
  MACS_PROTOCOL.set("CUSTOM");
  MACRO_SAD.clear();
  { // First macrocycle just refines occupancy and fdp
    ProtocolSAD thisMacro;
    thisMacro.off();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}


void MACS::setMACS_DEFAULT_PROTOCOL(void)
{
  setMACS_REFINE_FDP_PROTOCOL();
}

void MACS::setMACS_PROT(std::string method)
{
  bool error =  MACS_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACS::analyse(void)
{
  if (MACS_PROTOCOL.is_default() && MACRO_SAD.size())
  {
    MACS_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACS_PROTOCOL.is_default() || (MACS_PROTOCOL.is_custom() && !MACRO_SAD.size()))
  {
    setMACS_DEFAULT_PROTOCOL();
  }
  else if (MACS_PROTOCOL.is_off())
  {
    MACRO_SAD.clear();
    ProtocolSAD offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolSAD(offMacro));
    MACRO_SAD.push_back(cPtr);
  }
  else if (MACS_PROTOCOL.is_all())
  {
    MACRO_SAD.clear();
    ProtocolSAD thisMacro;
    thisMacro.all();
    protocolPtr cPtr(new ProtocolSAD(thisMacro));
    MACRO_SAD.push_back(cPtr);
  }
}

}//phaser
