//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PERT.h>
#include <phaser_defaults.h>

namespace phaser {

PERT::PERT() : CCP4base(), InputBase()
{
  Add_Key("PERT");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  PERTURB_DQ.clear();
  PERTURB_INCREMENT = std::string(DEF_PERT_INCR);
}

std::string PERT::unparse()
{
  std::string Card("");
  if (PERTURB_INCREMENT != std::string(DEF_PERT_INCR))
    Card += "PERTURB INCREMENT " + PERTURB_INCREMENT + "\n";
  if (PERTURB_INCREMENT == "RMS")
    Card += PERTURB_RMS.unparse(); //string returned only if not default
  else if (PERTURB_INCREMENT == "DQ" && PERTURB_DQ.size())
  {
    Card += "PERTURB";
    for (int i = 0; i < PERTURB_DQ.size(); i++)
      Card += " DQ " + itos(PERTURB_DQ[i]) + " ";
    Card += "\n";
  }
  return Card;
}

Token_value PERT::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,3,"INCREMENT","RMS","DQ"))
  {
    if (keyIs("INCREMENT"))
    {
      compulsoryKey(input_stream,2,"RMS","DQ");
      if (keyIs("RMS")) setPERT_INCR("RMS");
      if (keyIs("DQ"))  setPERT_INCR("DQ");
    }
    else if (keyIs("RMS"))
    {
      compulsoryKey(input_stream,3,"STEP","MAX","DIRECTION");
      if (keyIs("STEP"))    setPERT_RMS_STEP(get1num(input_stream));
      if (keyIs("MAX"))     setPERT_RMS_MAXI(get1num(input_stream));
      if (keyIs("DIRECTION"))
      {
        compulsoryKey(input_stream,3,"FORWARD","BACKWARD","TOFRO");
        if (keyIs("FORWARD"))  setPERT_RMS_DIRE("FORWARD");
        if (keyIs("BACKWARD")) setPERT_RMS_DIRE("BACKWARD");
        if (keyIs("TOFRO"))    setPERT_RMS_DIRE("TOFRO");
      }
    }
    else if (keyIs("DQ")) addPERT_DQ(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void PERT::setPERT_INCR(std::string pert)
{
  if (stoup(pert) == "RMS") PERTURB_INCREMENT = "RMS";
  else if (stoup(pert) == "DQ")  PERTURB_INCREMENT = "DQ";
  else CCP4base::store(PhaserError(INPUT,keywords,"PERTDB perturbation not recognised"));
}

void PERT::addPERT_DQ(floatType dq)
{
  int idq = (dq > 0) ? isposi(dq) : -isposi(-dq);
  if (dq == 0) idq = 0;
  PERTURB_DQ.push_back(idq);
}

void PERT::setPERT_RMS_DIRE(std::string dir)
{
  dir = stoup(dir);
  bool error(true);
  if (dir.find("FORW") == 0) { PERTURB_RMS.DIRECTION = "FORWARD";  error = false; }
  if (dir.find("BACK") == 0) { PERTURB_RMS.DIRECTION = "BACKWARD";  error = false; }
  if (dir.find("TOFR") == 0) { PERTURB_RMS.DIRECTION = "TOFRO";  error = false; }
  if (error)
     CCP4base::store(PhaserError(INPUT,keywords,"PERTDB direction not recognised"));
}

void PERT::setPERT_RMS_STEP(floatType step) { PERTURB_RMS.STEP = iszeropos(step); }
void PERT::setPERT_RMS_MAXI(floatType maxrms) { PERTURB_RMS.MAX = ispos(maxrms); }

void PERT::analyse(void)
{
}

} //phaser
