//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/FORM.h>
#include <phaser_defaults.h>

namespace phaser {

FORM::FORM() : CCP4base(), InputBase()
{
  Add_Key("FORM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string FORM::unparse()
{
  return FORMFACTOR.unparse();
}

Token_value FORM::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,3,"XRAY","ELECTRON","NEUTRON");
  if (keyIs("XRAY"))     setFORM("XRAY");
  if (keyIs("ELECTRON")) setFORM("ELECTRON");
  if (keyIs("NEUTRON"))  setFORM("NEUTRON");
  return skip_line(input_stream);
}

void FORM::setFORM(std::string ff)
{
  ff = stoup(ff);
  if (ff == "XRAY")     FORMFACTOR = data_formfactors(ff_xray);
  if (ff == "ELECTRON") FORMFACTOR = data_formfactors(ff_electron);
  if (ff == "NEUTRON")  FORMFACTOR = data_formfactors(ff_neutron);
}

void FORM::analyse(void)
{
}

}//phaser
