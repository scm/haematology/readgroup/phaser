//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/RESH.h>

namespace phaser {

RESH::RESH() : CCP4base(), InputBase()
{
  Add_Key("RESH");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string RESH::unparse()
{
  if (!RESHARP.IS_DEFAULT) return "RESHARPEN PERCENTAGE " + dtos(RESHARP.FRAC*100) + "\n";
  else return "";
}

Token_value RESH::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,1,"PERCENTAGE");
  setRESH_PERC(get1num(input_stream));
  return skip_line(input_stream);
}

void RESH::setRESH_PERC(floatType perc)
{ RESHARP.setFrac(isperc(perc)); }

void RESH::analyse(void)
{
}

}//phaser
