//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __BFACClass__
#define __BFACClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_restraint.h>

namespace phaser {

class BFAC : public InputBase, virtual public CCP4base
{
  public:
    data_restraint BFAC_WILS,BFAC_SPHE,BFAC_REFI;
    data_restraint DRMS_REFI;

    BFAC();
    virtual ~BFAC() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setBFAC_WILS_REST(bool);
    void setBFAC_SPHE_REST(bool);
    void setBFAC_REFI_REST(bool);
    void setBFAC_WILS_SIGM(floatType);
    void setBFAC_SPHE_SIGM(floatType);
    void setBFAC_REFI_SIGM(floatType);

    void setDRMS_REFI_REST(bool);
    void setDRMS_REFI_SIGM(floatType);
};

} //phaser

#endif
