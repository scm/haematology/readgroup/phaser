//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __FFTSClass__
#define __FFTSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_ffts.h>

namespace phaser {

class FFTS : public InputBase, virtual public CCP4base
{
  public:
    data_ffts FFTvDIRECT;

    FFTS();
    virtual ~FFTS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setFFTS_MIN(double);
    void setFFTS_MAX(double);

    void setFFTS_MIN_MAX(double m,double n) { setFFTS_MIN(m) ; setFFTS_MAX(n); }
};

} //phaser

#endif
