//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/LLGM.h>
#include <phaser/lib/scattering.h>

namespace phaser {

LLGM::LLGM() : CCP4base(), InputBase()
{
  Add_Key("LLGM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_LLGMAPS = bool(DEF_LLGM);
}

std::string LLGM::unparse()
{
  if (DO_LLGMAPS != bool(DEF_LLGM))
    return "LLGMAPS " + card(DO_LLGMAPS) + "\n";
  return "";
}

Token_value LLGM::parse(std::istringstream& input_stream)
{
  setLLGM(getBoolean(input_stream));
  return skip_line(input_stream);
}

void LLGM::setLLGM(bool b)  { DO_LLGMAPS = b; }

void LLGM::analyse(void)
{
}

} //phaser
