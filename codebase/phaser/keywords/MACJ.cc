//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACJ.h>

namespace phaser {
MACJ::MACJ() : CCP4base(), InputBase()
{
  Add_Key("MACJ");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_JOINT.clear();
  MACRO_JOINT_CHECK = true;
}

std::string MACJ::unparse()
{
  std::string Card("");
  if (!MACJ_PROTOCOL.is_default() && !MACJ_PROTOCOL.is_custom())
    Card += "MACJ PROTOCOL " + MACJ_PROTOCOL.unparse() + "\n";
  if (MACJ_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_JOINT.size() != 1) //not default
    {
      Card += "MACJ PROTOCOL " + MACJ_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_JOINT.size(); i++)
        Card += MACRO_JOINT[i]->unparse();
    }
    else if (!MACRO_JOINT[0]->is_default() ) //not default
    {
      Card += "MACJ PROTOCOL " + MACJ_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_JOINT.size(); i++)
        Card += MACRO_JOINT[i]->unparse();
    }
  }
  return Card;
}

Token_value MACJ::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"PROTOCOL","K");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACJ_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACJ_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACJ_PROT("OFF");
    if (keyIs("ALL"))     setMACJ_PROT("ALL");
  }
  if (keyIs("K"))
  {
    bool REF_K = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"B");
    bool REF_B = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SIGMA");
    bool REF_SIGMA = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"XYZ");
    bool REF_XYZ = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"OCC");
    bool REF_OCC = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"BFAC");
    bool REF_BFAC = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SN");
    bool REF_SN = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"SD");
    bool REF_SD = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"DPHI");
    bool REF_DPHI = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"DNN");
    bool REF_DNN = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"DLL");
    bool REF_DLL = getBoolean(input_stream);
    int NCYC(-999);
    std::string MINIMIZER("");
    std::string TARGET(DEF_MACJ_TARG);
    bool REF_PARTK(true),REF_PARTU(true);
    while (optionalKey(input_stream,5,"PK","PB","NCYC","MINIMIZER","TARGET"))
    {
      if (keyIs("PK")) REF_PARTK = getBoolean(input_stream);
      if (keyIs("PB")) REF_PARTU = getBoolean(input_stream);
      if (keyIs("NCYC")) NCYC = get1num(input_stream);
      if (keyIs("MINIMIZER")) MINIMIZER = getString(input_stream);
      if (keyIs("TARGET")) TARGET = getString(input_stream);
    }
    addMACJ(REF_K,REF_B,REF_SIGMA,REF_XYZ,REF_OCC,REF_BFAC,REF_SN,REF_SD,REF_DPHI,REF_DNN,REF_DLL,REF_PARTK,REF_PARTU,NCYC,TARGET,MINIMIZER);
  }
  return skip_line(input_stream);
}

void MACJ::addMACJ(bool ref_k,bool ref_b,bool ref_sigma,
                   bool ref_pk,bool ref_pb,
                   bool ref_xyz,bool ref_occ,bool ref_bfac,
                   bool ref_sn,bool ref_sd,
                   bool ref_dphi,
                   bool ref_dnn,bool ref_dll,
                   int ncyc,std::string target,std::string minimizer)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  target = stoup(target);
  if (target.size() && target != "SIRM_ONLY" && target != "NOT_ANOM_ONLY")
    CCP4base::store(PhaserError(INPUT,keywords,"Unknown target " + target));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  ProtocolSIR thisMacro(!ref_k,!ref_b,!ref_sigma,!ref_pk,!ref_pb,!ref_xyz,!ref_occ,!ref_bfac,!ref_sn,!ref_sd,!ref_dphi,!ref_dnn,!ref_dll,ncyc,target,minimizer);
  protocolPtr cPtr(new ProtocolSIR(thisMacro));
  MACRO_JOINT.push_back(cPtr);
}

void MACJ::setMACJ_DEFAULT_PROTOCOL(void)
{
  MACRO_JOINT.clear();
  { // First macrocycle just refines occupancy
    ProtocolSIR thisMacro;
    thisMacro.occ();
    protocolPtr cPtr(new ProtocolSIR(thisMacro));
    MACRO_JOINT.push_back(cPtr);
  }
  {
    ProtocolSIR thisMacro;
    thisMacro.variances();
    protocolPtr cPtr(new ProtocolSIR(thisMacro));
    MACRO_JOINT.push_back(cPtr);
  }
}

void MACJ::setMACJ_PROT(std::string method)
{
  bool error =  MACJ_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACJ::analyse(void)
{
  if (MACJ_PROTOCOL.is_default() && MACRO_JOINT.size())
  {
    MACJ_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACJ_PROTOCOL.is_default() || (MACJ_PROTOCOL.is_custom() && !MACRO_JOINT.size()))
  {
    setMACJ_DEFAULT_PROTOCOL();
  }
  else if (MACJ_PROTOCOL.is_off())
  {
    MACRO_JOINT.clear();
    ProtocolSIR offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolSIR(offMacro));
    MACRO_JOINT.push_back(cPtr);
  }
  else if (MACJ_PROTOCOL.is_all())
  {
    MACRO_JOINT_CHECK = false; //overrides the checking of joint refinements - for debugging
    MACRO_JOINT.clear();
    ProtocolSIR thisMacro;
    thisMacro.all();
    protocolPtr cPtr(new ProtocolSIR(thisMacro));
    MACRO_JOINT.push_back(cPtr);
  }
  //check for illegal combinations of refinement parameters
  for (int i = 0; i < MACRO_JOINT.size() && MACRO_JOINT_CHECK; i++)
  {
    if (!MACRO_JOINT[i]->getFIX(macj_k) && !MACRO_JOINT[i]->getFIX(macj_occ))
    CCP4base::store(PhaserError(INPUT,keywords,"Cannot refine data scale and atomic occupancy in same macrocycle"));
    if (!MACRO_JOINT[i]->getFIX(macj_b) && !MACRO_JOINT[i]->getFIX(macj_bfac))
    CCP4base::store(PhaserError(INPUT,keywords,"Cannot refine data B and atomic B in same macrocycle"));
  }
}

}//phaser
