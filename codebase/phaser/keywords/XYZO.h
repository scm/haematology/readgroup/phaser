//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __XYZOClass__
#define __XYZOClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>
#include <phaser/pod/xyzout.h>

namespace phaser {

class XYZO : public InputBase, virtual public CCP4base
{
  public:
    xyzout   DO_XYZOUT;

    XYZO();
    virtual ~XYZO() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setXYZO(bool);
    void setXYZO_ENSE(bool);
    void setXYZO_PACK(bool);
    void setXYZO_NMA_ALL(bool);
    void setXYZO_CHAI_COPY(bool);
    void setXYZO_PRIN(bool);
};

} //phaser

#endif
