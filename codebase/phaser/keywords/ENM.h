//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ENMClass__
#define __ENMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_elnemo.h>

namespace phaser {

class ENM : public InputBase, virtual public CCP4base
{
  public:
    data_elnemo ELNEMO;

    ENM();
    virtual ~ENM() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setENM_OSCI(std::string);
    void setENM_RTB_NRES(floatType);
    void setENM_RTB_MAXB(floatType);
    void setENM_RADI(floatType);
    void setENM_FORC(floatType);
};

} //phaser

#endif
