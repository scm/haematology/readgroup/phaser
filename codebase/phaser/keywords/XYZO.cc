//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/XYZO.h>
#include <phaser_defaults.h>

namespace phaser {

XYZO::XYZO() : CCP4base(), InputBase()
{
  Add_Key("XYZO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_XYZOUT.Default(true);
  DO_XYZOUT.ENSEMBLE = bool(DEF_XYZO_ENSE);
  DO_XYZOUT.PACKING = bool(DEF_XYZO_PACK);
  DO_XYZOUT.NMA_ALL = bool(DEF_XYZO_NMA_ALL);
  DO_XYZOUT.CHAIN_COPY = false; //change below
  DO_XYZOUT.PRINCIPAL = false; //change below
}

std::string XYZO::unparse()
{
  std::string Card;
  if (DO_XYZOUT.unparse() != "" ||
      DO_XYZOUT.ENSEMBLE != bool(DEF_XYZO_ENSE) ||
      DO_XYZOUT.NMA_ALL != bool(DEF_XYZO_NMA_ALL) ||
      DO_XYZOUT.PACKING != bool(DEF_XYZO_PACK) ||
      DO_XYZOUT.CHAIN_COPY != false  || //change above
      DO_XYZOUT.PRINCIPAL != false) //change above
  {
    Card = "XYZOUT " + DO_XYZOUT.unparse();
    if (DO_XYZOUT.ENSEMBLE != bool(DEF_XYZO_ENSE))
      Card += " ENSEMBLE " + card(DO_XYZOUT.ENSEMBLE);
    if (DO_XYZOUT.NMA_ALL != bool(DEF_XYZO_NMA_ALL))
       Card += " NMA ALL " + card(DO_XYZOUT.ENSEMBLE);
    if (DO_XYZOUT.PACKING != bool(DEF_XYZO_PACK))
      Card += " PACKING "  + card(DO_XYZOUT.PACKING);
    if (DO_XYZOUT.CHAIN_COPY != false) //change above
      Card += " CHAIN COPY " + card(DO_XYZOUT.CHAIN_COPY);
    if (DO_XYZOUT.PRINCIPAL != false) //change above
      Card += " PRINCIPAL " + card(DO_XYZOUT.PRINCIPAL);
  }
  if (Card.size()) Card += "\n";
  return Card;
}

Token_value XYZO::parse(std::istringstream& input_stream)
{
  setXYZO(getBoolean(input_stream));
  while (optionalKey(input_stream,5,"ENSEMBLE","PACKING","NMA","CHAIN","PRINCIPAL"))
  {
    if (keyIs("ENSEMBLE")) setXYZO_ENSE(getBoolean(input_stream));
    if (keyIs("PACKING"))  setXYZO_PACK(getBoolean(input_stream));
    if (keyIs("PRINCIPAL"))  setXYZO_PRIN(getBoolean(input_stream));
    if (keyIs("NMA"))
    {
      compulsoryKey(input_stream,1,"ALL");
      setXYZO_NMA_ALL(getBoolean(input_stream));
    }
    if (keyIs("CHAIN"))
    {
      compulsoryKey(input_stream,1,"COPY");
      setXYZO_CHAI_COPY(getBoolean(input_stream));
    }
  }
  return skip_line(input_stream);
}

void XYZO::setXYZO(bool do_xyzout) { DO_XYZOUT.Set(do_xyzout); }
void XYZO::setXYZO_ENSE(bool do_xyzout) { DO_XYZOUT.ENSEMBLE = do_xyzout; }
void XYZO::setXYZO_PACK(bool do_xyzout) { DO_XYZOUT.PACKING = do_xyzout; }
void XYZO::setXYZO_PRIN(bool do_xyzout) { DO_XYZOUT.PRINCIPAL = do_xyzout; }
void XYZO::setXYZO_NMA_ALL(bool do_xyzout) { DO_XYZOUT.NMA_ALL = do_xyzout; }
void XYZO::setXYZO_CHAI_COPY(bool do_xyzout) { DO_XYZOUT.CHAIN_COPY = do_xyzout; }

void XYZO::analyse(void)
{
}

} //phaser
