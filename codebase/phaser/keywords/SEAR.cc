//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SEAR.h>
#include <phaser_defaults.h>

namespace phaser {

SEAR::SEAR() : CCP4base(), InputBase()
{
  Add_Key("SEAR");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SEARCH_ORDER_AUTO = bool(DEF_SEAR_ORDE_AUTO);
  SEARCH_MODLID.clear();
  AUTO_SEARCH.clear();
  SEARCH_METHOD = std::string(DEF_SEAR_METH);
  SEARCH_PRUNE = bool(DEF_SEAR_PRUN);
  SEARCH_FACTORS = data_bofac(DEF_SEAR_BFAC,DEF_SEAR_OFAC);
  SEARCH_AMAL_USE = true; //change unparse below

  SEARCH_FROM_AUTO = false; //internal flag
}

std::string SEAR::unparse()
{
  std::string Card("");
  if (AUTO_SEARCH.size())
  {
    for (int a = 0; a < AUTO_SEARCH.size(); a++)
    {
      if (AUTO_SEARCH[a].MODLID.size()) //paranoia
        Card += "SEARCH ENSEMBLE " + AUTO_SEARCH[a].MODLID[0] + " ";
      for (int i = 1; i < AUTO_SEARCH[a].MODLID.size(); i++)
        Card += " OR ENSEMBLE " + AUTO_SEARCH[a].MODLID[i];
      if (AUTO_SEARCH[a].MODLID.size()) //paranoia
        Card += "\n";
    }
  }
  else if (SEARCH_ZERO.size())
  {
    for (int a = 0; a < SEARCH_ZERO.size(); a++)
      Card += "SEARCH ENSEMBLE " + SEARCH_ZERO[a] + " NUM 0\n";
  }
  if (SEARCH_ORDER_AUTO !=  bool(DEF_SEAR_ORDE_AUTO))
  Card += "SEARCH ORDER AUTO " + card(SEARCH_ORDER_AUTO) + "\n";
  if (SEARCH_PRUNE != bool(DEF_SEAR_PRUN))
  Card += "SEARCH PRUNE " + card(SEARCH_PRUNE) + "\n";
  if (SEARCH_METHOD != std::string(DEF_SEAR_METH))
  Card += "SEARCH METHOD " + SEARCH_METHOD + "\n";
  if (SEARCH_FACTORS.BFAC) Card += "SEARCH BFACTOR " + dtos(SEARCH_FACTORS.BFAC) + "\n";
  if (SEARCH_FACTORS.OFAC != 1) Card += "SEARCH OFACTOR " + dtos(SEARCH_FACTORS.OFAC) + "\n";
  if (SEARCH_AMAL_USE != true)
  Card += "SEARCH AMALGAMATE USE " + card(SEARCH_AMAL_USE) + "\n";
  //NUM done explicitly
  //SEARCH_MODLID is set as last AUTO_SEARCH
  return Card;
}

Token_value SEAR::parse(std::istringstream& input_stream)
{
  af_string modlid;
  compulsoryKey(input_stream,9,"ENSEMBLE","ORDER","METHOD","DEEP","DOWN","PRUNE","BFACTOR","OFACTOR","AMALGAMATE");
  if (keyIs("ENSEMBLE"))
  {
    modlid.push_back(getString(input_stream));
    int num(1);
    while (optionalKey(input_stream,3,"OR","NUM","AUTO"))
    {
      if (keyIs("OR"))
      {
        compulsoryKey(input_stream,1,"ENSEMBLE");
        modlid.push_back(getString(input_stream));
      }
      else if (keyIs("NUM"))
      {
        num = get1num(input_stream);
        break;
      }
      else if (keyIs("AUTO"))
      {
        num = 0;
        break;
      }
    }
    addSEAR_ENSE_OR_ENSE_NUM(modlid,num);
  }
  else if (keyIs("ORDER"))
  {
    compulsoryKey(input_stream,1,"AUTO");
    setSEAR_ORDE_AUTO(getBoolean(input_stream));
  }
  else if (keyIs("METHOD"))
  {
    compulsoryKey(input_stream,2,"FULL","FAST");
    setSEAR_METH(string_value);
  }
  else if (keyIs("DOWN"))
  {
    compulsoryKey(input_stream,1,"PERC");
    setSEAR_DOWN_PERC(get1num(input_stream));
  }
  else if (keyIs("DEEP"))      setSEAR_DEEP(getBoolean(input_stream));
  else if (keyIs("PRUNE"))     setSEAR_PRUN(getBoolean(input_stream));
  else if (keyIs("BFACTOR"))   setSEAR_BFAC(get1num(input_stream));
  else if (keyIs("OFACTOR"))   setSEAR_OFAC(get1num(input_stream));
  else if (keyIs("AMALGAMATE"))
  {
    compulsoryKey(input_stream,1,"USE");
    if (keyIs("USE"))
      setSEAR_AMAL_USE(getBoolean(input_stream));
  }
  return skip_line(input_stream);
}

void SEAR::setSEAR_ORDE_AUTO(bool b)   { SEARCH_ORDER_AUTO = b; }
void SEAR::setSEAR_PRUN(bool b)        { SEARCH_PRUNE = b; }
void SEAR::setSEAR_BFAC(double bfac)   { SEARCH_FACTORS.BFAC = bfac; }
void SEAR::setSEAR_OFAC(double ofac)   { SEARCH_FACTORS.OFAC = ofac; }
void SEAR::setSEAR_AMAL_USE(bool b)    { SEARCH_AMAL_USE = b; }

void SEAR::addSEAR_ENSE_NUM(std::string modlid,int num)
{
  af_string modlid_list(1,modlid);
  addSEAR_ENSE_OR_ENSE_NUM(modlid_list,num);
}

void SEAR::addSEAR_ENSE_OR_ENSE_NUM(af_string modlid,int num)
{
  //if a search with num=0 (AUTO) has been set already, then complain
  //if (!AUTO_SEARCH.size() && SEARCH_MODLID.size())
  //CCP4base::store(PhaserError(INPUT,keywords,"Search number AUTO can only have one search request"));
  //only add new value if it is unique
  //don't want OR search to have multiple entries the same
  af_string new_modlid;
  std::set<std::string> tmp;
  int last_size = tmp.size();
  for (int i = 0; i < modlid.size(); i++)
  {
    tmp.insert(modlid[i]);
    if (tmp.size() > last_size) new_modlid.push_back(modlid[i]);
    last_size = tmp.size();
  }
  modlid = new_modlid.deep_copy();
  for (int i = 0; i < num; i++)
  {
    search_component thisSearch;
    thisSearch.MODLID.resize(modlid.size());
    for (int m = 0; m < modlid.size(); m++) thisSearch.MODLID[m] = modlid[m];
    AUTO_SEARCH.push_back(thisSearch);
  }
  if (num == 0)
  {
    if (modlid.size() > 1)
    CCP4base::store(PhaserError(INPUT,keywords,"Search number 0 can only have one search component"));
    SEARCH_ZERO.push_back(modlid[0]);
  }
  SEARCH_MODLID = modlid;  //this is not necessary now that SEARCH_ZERO is implemented
}

void SEAR::setSEAR_ENSE_OR_ENSE(af_string modlid)
{
  //only add new value if it is unique
  //don't want OR search to have multiple entries the same
  af_string new_modlid;
  std::set<std::string> tmp;
  int last_size = tmp.size();
  for (int i = 0; i < modlid.size(); i++)
  {
    tmp.insert(modlid[i]);
    if (tmp.size() > last_size) new_modlid.push_back(modlid[i]);
    last_size = tmp.size();
  }
  modlid = new_modlid.deep_copy();
  search_component thisSearch;
  thisSearch.MODLID.resize(modlid.size());
  for (int m = 0; m < modlid.size(); m++) thisSearch.MODLID[m] = modlid[m];
  SEARCH_MODLID = modlid;
}

void SEAR::setSEAR_METH(std::string method)
{
  if (stoup(method) == "FULL")      SEARCH_METHOD = "FULL";
  else if (stoup(method) == "FAST") SEARCH_METHOD = "FAST";
  else CCP4base::store(PhaserError(INPUT,keywords,"Search method not recognised"));
}

void SEAR::setSEAR_DEEP(bool b)
{ CCP4base::store(PhaserError(NULL_ERROR,keywords,"Keyword SEARCH DEEP [ON/OFF] obsolete: Use PEAK ROT DOWN 0")); }

void SEAR::setSEAR_DOWN_PERC(double num)
{ CCP4base::store(PhaserError(NULL_ERROR,keywords,"Keyword SEARCH DOWN PERC <PERC> obsolete: Use PEAK ROT DOWN <PERC>")); }

void SEAR::analyse(void)
{
  if (!SEARCH_MODLID.size())
  CCP4base::store(PhaserError(INPUT,keywords,"No ensembles for search"));
  //if no NUM cards are give, then it has to be a simple search
  if (!AUTO_SEARCH.size() && SEARCH_MODLID.size() > 1)
  CCP4base::store(PhaserError(INPUT,keywords,"Number of ensembles for search not given"));
}

} //phaser
