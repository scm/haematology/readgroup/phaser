//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TOPFClass__
#define __TOPFClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class TOPF : public InputBase, virtual public CCP4base
{
  public:
    size_t NUM_TOPFILES;

    TOPF();
    virtual ~TOPF() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setTOPF(floatType);
};

} //phaser

#endif
