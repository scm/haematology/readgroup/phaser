//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __BINSClass__
#define __BINSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_bins.h>

namespace phaser {

class BINS : public InputBase, virtual public CCP4base
{
  public:
    data_bins DATABINS;

    BINS();
    virtual ~BINS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setBINS_DATA(data_bins);
    void setBINS_DATA_MINI(floatType);
    void setBINS_DATA_MAXI(floatType);
    void setBINS_DATA_WIDT(floatType);
};

} //phaser

#endif
