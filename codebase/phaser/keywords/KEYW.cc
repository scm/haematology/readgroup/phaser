//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/KEYW.h>

namespace phaser {

KEYW::KEYW() : CCP4base(), InputBase()
{
  Add_Key("KEYW");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_KEYWORDS.Default(true);
}

std::string KEYW::unparse()
{
  if (DO_KEYWORDS.unparse() != "") return "KEYWORDS " + DO_KEYWORDS.unparse() + "\n";
  return "";
}

Token_value KEYW::parse(std::istringstream& input_stream)
{
  setKEYW(getBoolean(input_stream));
  return skip_line(input_stream);
}

void KEYW::setKEYW(bool do_script) { DO_KEYWORDS.Set(do_script); }

void KEYW::analyse(void)
{
}

} //phaser
