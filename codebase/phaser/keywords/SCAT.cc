//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SCAT.h>
#include <phaser/lib/scattering.h>

namespace phaser {

SCAT::SCAT() : CCP4base(), InputBase()
{
  Add_Key("SCAT");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string SCAT::unparse()
{
  //don't attempt to set data this way!
  //the Preprocessor string will be very long
  std::string Card("");
  for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator iter = SCATTERING.ATOMTYPE.begin(); iter !=  SCATTERING.ATOMTYPE.end(); iter++)
  {
    std::string atomtype = iter->first;
    Card += "SCATTERING TYPE " + atomtype;
    Card += " FP=" + dtos(iter->second.fp()) +
            " FDP=" + dtos(iter->second.fdp()) +
            " FIX " + SCATTERING.getFixFdp(atomtype) + "\n";
  }
  if (SCATTERING.FDP.RESTRAINT != bool(DEF_SCAT_REST) || SCATTERING.FDP.SIGMA != floatType(DEF_SCAT_SIGM))
  {
  Card += "SCATTERING RESTRAINT " + card(SCATTERING.FDP.RESTRAINT);
  if (SCATTERING.FDP.RESTRAINT) Card += " SIGMA " + dtos(SCATTERING.FDP.SIGMA);
  }
  Card += "\n";
  return Card;
}

Token_value SCAT::parse(std::istringstream& input_stream)
{
  while (optionalKey(input_stream,3,"TYPE","RESTRAINT","SIGMA"))
  {
    if (keyIs("TYPE"))
    {
      std::string atomtype(getString(input_stream));
      compulsoryKey(input_stream,1,"FP");
      floatType fp(getAssignNumber(input_stream));
      compulsoryKey(input_stream,1,"FDP");
      floatType fdp(getAssignNumber(input_stream));
      compulsoryKey(input_stream,1,"FIX");
      std::string fixfdp = getString(input_stream);
      addSCAT(atomtype,fp,fdp,fixfdp);
    }
    else if (keyIs("RESTRAINT")) setSCAT_REST(getBoolean(input_stream));
    else if (keyIs("SIGMA")) setSCAT_SIGM(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void SCAT::setSCAT_REST(bool b) { SCATTERING.FDP.RESTRAINT = b; }
void SCAT::setSCAT_SIGM(floatType s) { SCATTERING.FDP.SIGMA = s; }
void SCAT::addSCAT(std::string type,floatType fp,floatType fdp,std::string fixfdp)
{
  SCATTERING.ATOMTYPE[stoup(type)] =  cctbx::eltbx::fp_fdp(fp,fdp);
  bool error = SCATTERING.addFixFdp(type,fixfdp);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Fdp Fix option not recognised"));
}

void SCAT::analyse(void)
{
}

} //phaser
