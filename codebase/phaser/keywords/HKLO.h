//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __HKLOClass__
#define __HKLOClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>

namespace phaser {

class HKLO : public InputBase, virtual public CCP4base
{
  public:
    Boolean DO_HKLOUT;

    HKLO();
    virtual ~HKLO() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setHKLO(bool);
};

} //phaser

#endif
