//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __FORMClass__
#define __FORMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_formfactors.h>

namespace phaser {

class FORM : public InputBase, virtual public CCP4base
{
  public:
    data_formfactors FORMFACTOR;

    FORM();
    virtual ~FORM() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setFORM(std::string);
};

} //phaser

#endif
