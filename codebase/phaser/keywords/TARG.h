//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __TARGClass__
#define __TARGClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class TARG : public InputBase, virtual public CCP4base
{
  public:
    std::string TARGET_ROT,TARGET_TRA;
    std::string TARGET_ROT_TYPE,TARGET_TRA_TYPE;

    TARG();
    virtual ~TARG() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setTARG_ROTA(std::string);
    void setTARG_TRAN(std::string);
    void setTARG_ROTA_TYPE(std::string);
    void setTARG_TRAN_TYPE(std::string);
};

} //phaser

#endif
