//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __COMPClass__
#define __COMPClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/Boolean.h>
#include <phaser/include/data_composition.h>

namespace phaser {

class COMP : public InputBase, virtual public CCP4base
{
  public:
    data_composition COMPOSITION;

    COMP();
    virtual ~COMP() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setCOMP_AVER();
    void setCOMP_SOLV();
    void setCOMP_ASU();
    void setCOMP_BY(std::string);
    void addCOMP_ATOM_NUM(std::string,double);
    void setCOMP_PERC(double);
    void setCOMP_INCR(double);
    void setCOMP_MINI_SOLV(double);
    void addCOMP_TYPE_MW_NUM(std::string,double,double);
    void addCOMP_TYPE_SEQ_NUM(std::string,std::string,double);
    void addCOMP_TYPE_NRES_NUM(std::string,double,double);
    void addCOMP_TYPE_STR_NUM(std::string,std::string,double);

    //python backwards compatible
    void addCOMP_PROT_MW_NUM(double x,double num)       { addCOMP_TYPE_MW_NUM("PROTEIN",x,num); }
    void addCOMP_PROT_SEQ_NUM(std::string x,double num) { addCOMP_TYPE_SEQ_NUM("PROTEIN",x,num); }
    void addCOMP_PROT_NRES_NUM(double x,double num)     { addCOMP_TYPE_NRES_NUM("PROTEIN",x,num); }
    void addCOMP_PROT_STR_NUM(std::string x,double num) { addCOMP_TYPE_STR_NUM("PROTEIN",x,num); }
    void addCOMP_NUCL_MW_NUM(double x,double num)       { addCOMP_TYPE_MW_NUM("NUCLEIC",x,num); }
    void addCOMP_NUCL_SEQ_NUM(std::string x,double num) { addCOMP_TYPE_SEQ_NUM("NUCLEIC",x,num); }
    void addCOMP_NUCL_NRES_NUM(double x,double num)     { addCOMP_TYPE_NRES_NUM("NUCLEIC",x,num); }
    void addCOMP_NUCL_STR_NUM(std::string x,double num) { addCOMP_TYPE_STR_NUM("NUCLEIC",x,num); }
};

} //phaser

#endif
