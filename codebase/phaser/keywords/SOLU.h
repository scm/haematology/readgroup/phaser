//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SOLUClass__
#define __SOLUClass__
#include <phaser/io/CCP4base.h>
#include <phaser/mr_objects/mr_solution.h>

namespace phaser {

class SOLU : public InputBase, virtual public CCP4base
{
  public:
    mr_solution MRSET;
    mr_solution MRTEMPLATE;

    SOLU();
    virtual ~SOLU() {}
    SOLU(std::string&); //for python

  private:
    bool  IS_TEMPLATE;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSOLU(mr_solution);
    void addSOLU_SET(std::string);
    void addSOLU_SPAC(std::string);
    void addSOLU_RESO(floatType);
    void addSOLU_SPAC_HALL(std::string);
    void addSOLU_6DIM_ENSE(std::string,dvect3,bool,dvect3,double,bool,bool,bool,double,double);
    void addSOLU_GYRE_ENSE(std::string,dvect3);
    void addSOLU_ENSE_ROT_TRA(std::string,double,double,double,double,double,double);
    void addSOLU_TRIAL_ENSE_EULER(std::string,dvect3,double,double);
    void addSOLU_TRIAL_ENSE_EULER_GYRE(std::string,dvect3,double,double,double);
    void addSOLU_ENSE_DRMS(std::string,floatType);
    void addSOLU_ENSE_CELL(std::string,floatType);
    void addSOLU_TEMPLATE(std::string);
    void addSOLU_ORIG_ENSE(std::string);
    void addSOLU_HIST(std::string);
    void setSOLU_PACK(bool);
    void setSOLU_CELL_SCAL(floatType);
};

} //phaser

#endif
