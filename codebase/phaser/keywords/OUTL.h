//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __OUTLClass__
#define __OUTLClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_outl.h>

namespace phaser {

class OUTL : public InputBase, virtual public CCP4base
{
  public:
    data_outl OUTLIER;

    OUTL();
    virtual ~OUTL() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setOUTL_REJE(bool);
    void setOUTL_PROB(double);
    void setOUTL_INFO(double);
};

} //phaser

#endif
