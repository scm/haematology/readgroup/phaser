//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACOClass__
#define __MACOClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolOCC.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACO : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr>  MACRO_OCC;

    MACO();
    virtual ~MACO() {}

  private:
    data_protocol MACO_PROTOCOL;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACO(double,std::string);
    void setMACO_PROT(std::string);
};

} //phaser

#endif
