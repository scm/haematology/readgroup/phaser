//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACT.h>

namespace phaser {

MACT::MACT() : CCP4base(), InputBase()
{
  Add_Key("MACT");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_TNCS.clear();
}

std::string MACT::unparse()
{
  std::string Card("");
  if (!MACT_PROTOCOL.is_default() && !MACT_PROTOCOL.is_custom())
    Card += "MACTNCS PROTOCOL " + MACT_PROTOCOL.unparse() + "\n";
  if (MACT_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_TNCS.size() != 3) //not default
    {
      Card += "MACTNCS PROTOCOL " + MACT_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_TNCS.size(); i++)
        Card += MACRO_TNCS[i]->unparse();
    }
    else if (!MACRO_TNCS[0]->is_default() || //not default
             !MACRO_TNCS[1]->is_default() || //not default
             !MACRO_TNCS[2]->is_default()) //not default
    {
      Card += "MACTNCS PROTOCOL " + MACT_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_TNCS.size(); i++)
        Card += MACRO_TNCS[i]->unparse();
    }
  }
  return Card;
}

Token_value MACT::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"PROTOCOL","ROT");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACT_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACT_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACT_PROT("OFF");
    if (keyIs("ALL"))     setMACT_PROT("ALL");
  }
  if (keyIs("ROT"))
  {
    bool ref_rota = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"TRA");
    bool ref_tran = getBoolean(input_stream);
    compulsoryKey(input_stream,1,"VRMS");
    bool ref_vrms = getBoolean(input_stream);
    floatType ncyc(-999);
    std::string minimizer("");
    while (optionalKey(input_stream,2,"NCYC","MINIMIZER"))
    {
      if (keyIs("NCYC")) ncyc = get1num(input_stream);
      if (keyIs("MINIMIZER")) minimizer = getString(input_stream);
    }
    addMACT(ref_rota,ref_tran,ref_vrms,ncyc,minimizer);
  }
  return skip_line(input_stream);
}

void MACT::addMACT(bool ref_rota,bool ref_tran,bool ref_vrms,floatType ncyc,std::string minimizer)
{
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  protocolPtr cPtr(new ProtocolNCS(!ref_rota,!ref_tran,!ref_vrms,ncyc,minimizer));
  MACRO_TNCS.push_back(cPtr);
}

void MACT::setMACT_PROT(std::string method)
{
  bool error = MACT_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACT::analyse(void)
{
  if (MACT_PROTOCOL.is_default() && MACRO_TNCS.size())
  {
    MACT_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACT_PROTOCOL.is_default() || (MACT_PROTOCOL.is_custom() && !MACRO_TNCS.size()))
  {
    MACRO_TNCS.clear();
    protocolPtr cPtr(new ProtocolNCS());
    MACRO_TNCS.push_back(cPtr);
    MACRO_TNCS.push_back(cPtr);
    MACRO_TNCS.push_back(cPtr); //there are convergence issues
  }
  else if (MACT_PROTOCOL.is_off())
  {
    MACRO_TNCS.clear();
    ProtocolNCS offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolNCS(offMacro));
    MACRO_TNCS.push_back(cPtr);
  }
  else if (MACT_PROTOCOL.is_all())
  {
    MACRO_TNCS.clear();
    ProtocolNCS offMacro;
    offMacro.all();
    protocolPtr cPtr(new ProtocolNCS(offMacro));
    MACRO_TNCS.push_back(cPtr);
  }
}

}//phaser
