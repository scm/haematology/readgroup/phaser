//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SGAL.h>
#include <phaser/lib/sg_prior.h>
#include <phaser/include/space_group_name.h>
#include <phaser_defaults.h>

namespace phaser {

SGAL::SGAL() : CCP4base(), InputBase()
{
  Add_Key("SGAL");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SGALT_HALL.clear();
  DO_SGALT = std::string(DEF_SGAL_SELE);
  SGALT_IS_DEFAULT = true;
}

std::string SGAL::unparse()
{
  std::string Card("");
  if (SGALT_BASE.size()) Card += "SGALT BASE " + SGALT_BASE + "\n";
  if (DO_SGALT == std::string(DEF_SGAL_SELE)) return Card;
  Card += "SGALT SELECT " + DO_SGALT + "\n";
  if (DO_SGALT == "LIST")
  {
    for (int i = 0; i < SGALT_HALL.size(); i++)
      Card += "SGALT TEST " + cctbx::sgtbx::space_group(SGALT_HALL[i],"A1983").match_tabulated_settings().universal_hermann_mauguin() + "\n" ;
  }
  return Card;
}

Token_value SGAL::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,3,"SELECT","TEST","BASE");
  if (keyIs("BASE"))
  {
    std::string sg_name("");
    while (get_token(input_stream) == NAME || curr_tok == NUMBER)
      sg_name += string_value + " "; //build up the spacegroup, including spaces
    setSGAL_BASE(sg_name);
  }
  else if (keyIs("SELECT"))
  {
    compulsoryKey(input_stream,4,"NONE","ALL","HAND","LIST");
    if (keyIs("NONE")) setSGAL_SELE("NONE");
    if (keyIs("ALL"))  setSGAL_SELE("ALL");
    if (keyIs("HAND")) setSGAL_SELE("HAND");
    if (keyIs("LIST")) setSGAL_SELE("LIST");
  }
  else if (keyIs("TEST"))
  {
    std::string sg_name("");
    while (get_token(input_stream) == NAME || curr_tok == NUMBER)
      sg_name += string_value + " "; //build up the spacegroup, including spaces
    addSGAL_TEST(sg_name);
  }
  return skip_line(input_stream);
}

void SGAL::setSGAL_SELE(std::string select)
{
  if (stoup(select) == "NONE")      DO_SGALT = "NONE";
  else if (stoup(select) == "ALL")  DO_SGALT = "ALL";
  else if (stoup(select) == "HAND") DO_SGALT = "HAND";
  else if (stoup(select) == "LIST") DO_SGALT = "LIST";
  else CCP4base::store(PhaserError(INPUT,keywords,"Space Group selection not recognised"));
  SGALT_IS_DEFAULT = false;
}

void SGAL::addSGAL_TEST(std::string sg_name)
{
  try {
    cctbx::sgtbx::space_group_symbols Symbols(sg_name,"A1983");
    addSG(Symbols.hall()); //use set to avoid checking for multiple entries
  }
  catch (...) {
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group \"" + sg_name + "\" not valid"));
  }
  SGALT_IS_DEFAULT = false;
}

void SGAL::setSGAL_BASE(std::string sg_name)
{
  try { //hall: this is what will be stored without explicit user input
    cctbx::sgtbx::space_group spacegroup(sg_name);
    SGALT_BASE = spacegroup.type().hall_symbol();
  }
  catch (...) {
    try { //name
      cctbx::sgtbx::space_group_symbols Symbols(sg_name,"A1983");
      SGALT_BASE = Symbols.hall();
    }
    catch (...) {
      CCP4base::store(PhaserError(INPUT,keywords,"Space Group \"" + sg_name + "\" not valid"));
    }
  }
}

void SGAL::analyse(void)
{
  if (DO_SGALT == "LIST")
  {
    if (!SGALT_HALL.size())
    {
      CCP4base::store(PhaserError(INPUT,keywords,"Alternative Space Group list is empty"));
    }
    else
    { //use sort order is on by default
      cctbx::sgtbx::space_group throwtest;
      try {
        std::vector<prob_sg> sort_SGALT_HALL;
        for (int sg = 0; sg < SGALT_HALL.size(); sg++)
          throwtest = cctbx::sgtbx::space_group(SGALT_HALL[sg]); //throw
        for (int sg = 0; sg < SGALT_HALL.size(); sg++)
          sort_SGALT_HALL.push_back(prob_sg(sg_prior(SGALT_HALL[sg]),SGALT_HALL[sg]));
        std::sort(sort_SGALT_HALL.begin(),sort_SGALT_HALL.end());
        std::reverse(sort_SGALT_HALL.begin(),sort_SGALT_HALL.end());
        for (int sg = 0; sg < SGALT_HALL.size(); sg++)
          SGALT_HALL[sg] = sort_SGALT_HALL[sg].SG;
      }
      catch (...) {
        CCP4base::store(PhaserError(INPUT,keywords,"Space Group Hall Symbol not valid"));
      }
    }
  }
}

void SGAL::setupSGAL(std::string sg_name)
{
  cctbx::sgtbx::space_group reference_sg;
  // The spacegroup (SPAC) is really set on the SGALT card
  // If the base class is set directly in SGALT_BASE, use this
  // otherwise use the spacegroup from the SPAC card (sg_name) for the base class
  // SPAC spacegroup can refer to the spacegroup of the data in the phenix interface
  // SGALT_BASE is the spacegroup of the MR run
  try {
    reference_sg = SGALT_BASE.size() ? cctbx::sgtbx::space_group(SGALT_BASE):
                                       cctbx::sgtbx::space_group(sg_name);
  }
  catch (...) {
    throw PhaserError(INPUT,keywords,"Space Group Hall Symbol not valid: " + std::string(SGALT_BASE.size() ? SGALT_BASE : sg_name));
  }

  SGALT_BASE = reference_sg.type().hall_symbol();
  if (DO_SGALT == "NONE")
  {
    DO_SGALT = "LIST";
    if (!SGALT_HALL.size())
      addSG(sg_name); //so that there is a spacegroup - the input spacegroup
  }
  else if (DO_SGALT == "ALL")
  {
    std::vector<cctbx::sgtbx::space_group> sglist = groups_sysabs(reference_sg);
    for (int s = 0; s < sglist.size(); s++)
      addSG(sglist[s].type().hall_symbol());
  }
  else if (DO_SGALT == "HAND")
  {
    addSG(reference_sg.type().hall_symbol());
    if (reference_sg.type().is_enantiomorphic())
    {
      cctbx::sgtbx::change_of_basis_op cb_op = reference_sg.type().change_of_hand_op();
      cctbx::sgtbx::space_group enantiomorph_sg = reference_sg.change_basis(cb_op);
      addSG(enantiomorph_sg.type().hall_symbol());
    }
  }
  else if (DO_SGALT == "LIST")
  {
    std::vector<cctbx::sgtbx::space_group> sglist = groups_sysabs(reference_sg);
    bool1D found_match_to_base(SGALT_HALL.size(),false);
    for (int a = 0; a < SGALT_HALL.size(); a++)
    {
      std::string ccp4alt = space_group_name(SGALT_HALL[a],true).CCP4;
      for (int s = 0; s < sglist.size(); s++)
      {
        std::string ccp4ls = space_group_name(sglist[s].type().hall_symbol(),true).CCP4;
        if (ccp4alt == ccp4ls)
          found_match_to_base[a] = true;
      }
    }
    for (int a = 0; a < SGALT_HALL.size(); a++)
      if (!found_match_to_base[a])
        throw PhaserError(INPUT,keywords,"Space Group does not match SGALT list");
  }

  bool DEF_SGAL_SORT(true);//hardwired default
  if (DEF_SGAL_SORT)
  {
    std::vector<prob_sg> sort_SGALT_HALL;
    for (int sg = 0; sg < SGALT_HALL.size(); sg++)
      sort_SGALT_HALL.push_back(prob_sg(sg_prior(SGALT_HALL[sg]),SGALT_HALL[sg]));
    std::sort(sort_SGALT_HALL.begin(),sort_SGALT_HALL.end());
    std::reverse(sort_SGALT_HALL.begin(),sort_SGALT_HALL.end());
    for (int sg = 0; sg < SGALT_HALL.size(); sg++)
      SGALT_HALL[sg] = sort_SGALT_HALL[sg].SG;
  }
}

//private
void SGAL::addSG(std::string new_sg)
{
  //only add the new spacegroup to the vector if it has not been seen before
  stringset set_SGALT_HALL;
  for (int sg = 0; sg < SGALT_HALL.size(); sg++)
    set_SGALT_HALL.insert(SGALT_HALL[sg]);
  set_SGALT_HALL.insert(new_sg);
  if (set_SGALT_HALL.size() > SGALT_HALL.size())
    SGALT_HALL.push_back(new_sg);
}

} //phaser
