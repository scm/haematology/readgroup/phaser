//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/SOLU.h>
#include <phaser/lib/euler.h>
#include <phaser/io/Errors.h>
#include <phaser/include/space_group_name.h>

namespace phaser {

SOLU::SOLU(std::string& input_str) : CCP4base(), InputBase()
{
  std::istringstream input_stream(input_str);
  parse(input_stream);
}

SOLU::SOLU() : CCP4base(), InputBase()
{
  Add_Key("SOLU");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MRSET.clear();
  IS_TEMPLATE = false;
}

std::string SOLU::unparse()
{
  std::string Cards;
  if (MRSET.unparse().size())
    Cards += MRSET.unparse() + "\n";
  if (MRTEMPLATE.unparse().size())
    Cards += MRTEMPLATE.unparse_template() + "\n";
  return Cards;
}

Token_value SOLU::parse(std::istringstream& input_stream)
{
  bool fixr(false),fixt(false),fixb(false);
  floatType bfac(0),mult(1),ofac(1);
  compulsoryKey(input_stream,12,"SET","HISTORY","SPAC","RESO","6DIM","GYRE","TRIAL","TEMPLATE","ORIGIN","ENSEMBLE","PACKS","CELL");
  if (keyIs("SET"))
  {
    addSOLU_SET(getLine(input_stream));
    return ENDLINE;
  }
  else if (keyIs("HISTORY"))
  {
    addSOLU_HIST(getLine(input_stream));
    return ENDLINE;
  }
  else if (keyIs("SPAC"))
  {
    if (get_token(input_stream) == NAME)
    {
      bool is_hall(string_value == "HALL");
      if (string_value == "HALL") string_value = "";
      std::string sg_name(string_value);
      while (get_token(input_stream) == NAME || curr_tok == NUMBER)
      sg_name += " " + string_value;
      is_hall ? addSOLU_SPAC_HALL(sg_name) : addSOLU_SPAC(sg_name);
    }
    return ENDLINE;
  }
  else if (keyIs("6DIM"))
  {
    compulsoryKey(input_stream,1,"ENSEMBLE");
    std::string modlid = getFileName(input_stream);
    compulsoryKey(input_stream,1,"EULER");
    dvect3 euler = get3nums(input_stream);
    compulsoryKey(input_stream,2,"FRAC","ORTH");
    bool frac = keyIs("FRAC");
    dvect3 trans = get3nums(input_stream);
    while (optionalKey(input_stream,6,"FIXR","FIXT","FIXB","BFAC","OFAC","MULT"))
    {
      if (keyIs("FIXR")) fixr = getBoolean(input_stream);
      if (keyIs("FIXT")) fixt = getBoolean(input_stream);
      if (keyIs("FIXB")) fixb = getBoolean(input_stream);
      if (keyIs("BFAC")) bfac = get1num(input_stream);
      if (keyIs("OFAC")) ofac = get1num(input_stream);
      if (keyIs("MULT")) mult = get1num(input_stream);
    }
    addSOLU_6DIM_ENSE(modlid,euler,frac,trans,bfac,fixr,fixt,fixb,mult,ofac);
  }
  else if (keyIs("GYRE"))
  {
    compulsoryKey(input_stream,1,"ENSEMBLE");
    std::string modlid = getString(input_stream);
    compulsoryKey(input_stream,1,"EULER");
    dvect3 euler = get3nums(input_stream);
    addSOLU_GYRE_ENSE(modlid,euler);
  }
  else if (keyIs("TRIAL"))
  {
    compulsoryKey(input_stream,1,"ENSEMBLE");
    std::string modlid = getFileName(input_stream);
    compulsoryKey(input_stream,1,"EULER");
    dvect3 euler = get3nums(input_stream);
    //compulsoryKey(input_stream,"RF");
    get_token(input_stream); if (stoup(string_value) != "RF") throw PhaserError(SYNTAX,keywords,"Use RF");
    floatType rf = get1num(input_stream);
    compulsoryKey(input_stream,1,"RFZ");
    floatType rfz = get1num(input_stream);
    (optionalKey(input_stream,2,"GYRE","GRF"))?
    addSOLU_TRIAL_ENSE_EULER_GYRE(modlid,euler,rf,rfz,get1num(input_stream)):
    addSOLU_TRIAL_ENSE_EULER(modlid,euler,rf,rfz);
  }
  else if (keyIs("TEMPLATE"))
  {
    addSOLU_TEMPLATE(getLine(input_stream));
    return ENDLINE;
  }
  else if (keyIs("ORIGIN"))
  {
    while (optionalKey(input_stream,1,"ENSEMBLE"))
      addSOLU_ORIG_ENSE(getString(input_stream));
  }
  else if (keyIs("ENSEMBLE"))
  {
    std::string modlid = getFileName(input_stream);
    compulsoryKey(input_stream,2,"VRMS","CELL");
    if (keyIs("VRMS"))
    {
      compulsoryKey(input_stream,1,"DELTA");
      addSOLU_ENSE_DRMS(modlid,get1num(input_stream));
    }
    if (keyIs("CELL"))
    {
      compulsoryKey(input_stream,1,"SCALE");
      addSOLU_ENSE_CELL(modlid,get1num(input_stream));
    }
  }
  else if (keyIs("RESO"))
  {
    addSOLU_RESO(get1num(input_stream));
  }
  else if (keyIs("PACKS"))
  {
    setSOLU_PACK(getBoolean(input_stream));
  }
  else if (keyIs("CELL"))
  {
    compulsoryKey(input_stream,1,"SCALE");
    setSOLU_CELL_SCAL(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void SOLU::addSOLU_SET(std::string annotation)
{
  IS_TEMPLATE = false;
  MRSET.increment();
  //check for windows annotation
   while (annotation[annotation.size()-1] == '\r')
     annotation.erase(annotation.size()-1,annotation.size());
  //check for blank annotation
  for (int i = 0; i < annotation.size(); i++)
    if (annotation[i] != ' ') MRSET.add(annotation);
}

void SOLU::addSOLU_HIST(std::string history)
{
  IS_TEMPLATE = false;
  //check for windows history
   while (history[history.size()-1] == '\r')
     history.erase(history.size()-1,history.size());
  //check for blank history
  for (int i = 0; i < history.size(); i++)
    if (history[i] != ' ') MRSET.add_history(history);
}

void SOLU::addSOLU_TEMPLATE(std::string annotation)
{
  IS_TEMPLATE = true;
  MRTEMPLATE.increment();
  for (int i = 0; i < annotation.size(); i++)
    if (annotation[i] != ' ') MRTEMPLATE.add(annotation);
}

void SOLU::setSOLU(mr_solution mrset)
{
  MRSET = mrset;
}

void SOLU::addSOLU_SPAC_HALL(std::string sg_name)
{
  space_group_name this_sg(sg_name,false);
  if (this_sg.hall_error)
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group Hall Symbol not valid"));
  else MRSET.add_hall(this_sg.HALL);
}

void SOLU::addSOLU_SPAC(std::string sg_name)
{
  space_group_name this_sg(sg_name,false);
  if (this_sg.error)
    CCP4base::store(PhaserError(INPUT,keywords,"Space Group Name not valid"));
  else MRSET.add_hall(this_sg.HALL);
}

void SOLU::addSOLU_TRIAL_ENSE_EULER(std::string modlid,dvect3 euler,double rf,double rfz)
{
  mr_rlist RLIST;
  RLIST.MODLID = modlid;
  RLIST.EULER = euler;
  RLIST.RF = rf;
  RLIST.RFZ = rfz;
  RLIST.GRF = 0;
  MRSET.add(RLIST);
}

void SOLU::addSOLU_TRIAL_ENSE_EULER_GYRE(std::string modlid,dvect3 euler,double rf,double rfz,double llg)
{
  mr_rlist RLIST;
  RLIST.MODLID = modlid;
  RLIST.EULER = euler;
  RLIST.RF = rf;
  RLIST.RFZ = rfz;
  RLIST.GRF = llg;
  MRSET.add(RLIST);
}

void SOLU::addSOLU_6DIM_ENSE(std::string modlid,dvect3 euler,bool frac,dvect3 T,double bfac,bool fixr,bool fixt,bool fixb,double mult,double ofac)
{
  int m = isposi(mult);
  ofac = ispos(ofac);
  mr_ndim new_mr(modlid,euler,frac,T,bfac,ofac,fixr,fixt,fixb,m);
  IS_TEMPLATE ? MRTEMPLATE.add(new_mr) : MRSET.add(new_mr);
}

void SOLU::addSOLU_ENSE_ROT_TRA(std::string modlid,double e1,double e2,double e3,double t1,double t2,double t3)
{
  mr_ndim new_mr(modlid,dvect3(e1,e2,e3),true,dvect3(t1,t2,t3));
  MRSET.add(new_mr);
}

void SOLU::addSOLU_ORIG_ENSE(std::string modlid)
{
  mr_ndim new_mr(modlid);
  MRSET.add(new_mr);
}

void SOLU::addSOLU_ENSE_DRMS(std::string modlid,floatType DRMS)
{ IS_TEMPLATE ? MRTEMPLATE.add_DRMS(modlid,DRMS) : MRSET.add_DRMS(modlid,DRMS); }

void SOLU::addSOLU_ENSE_CELL(std::string modlid,floatType cell)
{ IS_TEMPLATE ? MRTEMPLATE.add_CELL(modlid,cell) : MRSET.add_CELL(modlid,cell); }

void SOLU::addSOLU_RESO(floatType reso)
{ MRSET.set_resolution(iszeropos(reso)); }

void SOLU::setSOLU_PACK(bool packs)
{ MRSET.set_PACKS(packs); }

void SOLU::setSOLU_CELL_SCAL(double scale)
{ MRSET.set_CELL_SCALE(scale); }

void SOLU::addSOLU_GYRE_ENSE(std::string modlid,dvect3 euler)
{
  mr_gyre new_gyre(modlid,euler);
  IS_TEMPLATE ? MRTEMPLATE.add(new_gyre) : MRSET.add(new_gyre);
}

void SOLU::analyse(void)
{
  if (MRSET.size() == 1 && MRSET[0].empty()) MRSET.clear();
  //if some stuff was accidentally added to sol file before a SOLU SET command
  //if (MRSET.size() > 1 && (MRSET[0].KNOWN.size() < MRSET[1].KNOWN.size())) MRSET.erase(MRSET.begin());
}

} //end namespace phaser
