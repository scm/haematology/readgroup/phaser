//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MACJClass__
#define __MACJClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/ProtocolSIR.h>
#include <phaser/include/data_protocol.h>

namespace phaser {

class MACJ : public InputBase, virtual public CCP4base
{
  public:
    af::shared<protocolPtr> MACRO_JOINT;

    MACJ();
    virtual ~MACJ() { }

  private:
    void setMACJ_DEFAULT_PROTOCOL();
    data_protocol MACJ_PROTOCOL;
    bool MACRO_JOINT_CHECK;

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addMACJ(bool,bool,bool,bool,bool,bool,bool,bool,bool,bool,bool,bool,bool,int,std::string,std::string);
    void setMACJ_PROT(std::string);
};

} //phaser

#endif
