//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/VERB.h>
#include <phaser_defaults.h>

namespace phaser {

VERB::VERB() : CCP4base(), InputBase()
{
  Add_Key("VERB");
  // Add to CCP4base
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  //parse as OUTPUT for new keyword
  Add_Key("OUTP");
  inputPtr oPtr(this);
  possible_fns.push_back(oPtr);
  //parse as DEBUG also, for backwards compatibility
  Add_Key("DEBU");
  inputPtr dPtr(this);
  possible_fns.push_back(dPtr);

  OUTPUT_LEVEL = outStreamStr(DEF_OUTP_LEVE).enumerator(); //string default
}

std::string VERB::unparse()
{
  if (OUTPUT_LEVEL == outStreamStr(DEF_OUTP_LEVE).enumerator()) return "";
  return "OUTPUT LEVEL " + outStreamStr(OUTPUT_LEVEL).string() + "\n";
}

Token_value VERB::parse(std::istringstream& input_stream)
{
  //parse DEBUG keyword, before reading new input
  if (keyIs("DEBUG"))
  {
    setDEBU(getBoolean(input_stream));
    return skip_line(input_stream);
  }
  else if (keyIs("OUTPUT"))
  {
    compulsoryKey(input_stream,1,"LEVEL");
    int key_len = getKeyLength();
    setKeyLength(7); //check all chars
    compulsoryKey(input_stream,8,"SILENT","PROCESS","CONCISE","SUMMARY","LOGFILE","VERBOSE","DEBUG","TESTING");
    setOUTP_LEVE(string_value);
    setKeyLength(key_len);
    return skip_line(input_stream);
  }
  //else VERBOSE
  setVERB(getBoolean(input_stream));
  return skip_line(input_stream);
}

//if false, setting to the default is not ideal, but best we can do
void VERB::setVERB(bool b) { OUTPUT_LEVEL = b ? VERBOSE : outStreamStr(DEF_OUTP_LEVE).enumerator(); }
void VERB::setDEBU(bool b) { OUTPUT_LEVEL = b ? DEBUG : outStreamStr(DEF_OUTP_LEVE).enumerator(); }
void VERB::setOUTP_LEVE(std::string v)
{
  OUTPUT_LEVEL = outStreamStr(v).enumerator();
  if (OUTPUT_LEVEL == ERRATUM)
    CCP4base::store(PhaserError(INPUT,keywords,"Output level not recognised"));
}

void VERB::analyse(void)
{
}

}//phaser
