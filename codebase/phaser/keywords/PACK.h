//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PACKClass__
#define __PACKClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_pack.h>

namespace phaser {

class PACK : public InputBase, virtual public CCP4base
{
  public:
    data_pack PACKING;

    PACK();
    virtual ~PACK() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setPACK_SELE(std::string);
    void setPACK_CUTO(double);
    void setPACK_QUIC(bool);
    void setPACK_COMP(bool);
    void setPACK_KEEP_HIGH_TFZ(bool);
    void setPACK_PRUN(bool b) { PACKING.PRUNE = true; }
};

} //phaser

#endif
