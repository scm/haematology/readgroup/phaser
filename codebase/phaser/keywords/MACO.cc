//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/MACO.h>

namespace phaser {

MACO::MACO() : CCP4base(), InputBase()
{
  Add_Key("MACO");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  MACRO_OCC.clear();
}

std::string MACO::unparse()
{
  std::string Card("");
  if (!MACO_PROTOCOL.is_default() && !MACO_PROTOCOL.is_custom())
    Card += "MACOCC PROTOCOL " + MACO_PROTOCOL.unparse() + "\n";
  if (MACO_PROTOCOL.is_custom()) //default as CUSTOM
  {
    if (MACRO_OCC.size() != 1) //not default
    {
      Card += "MACOCC PROTOCOL " + MACO_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_OCC.size(); i++)
        Card += MACRO_OCC[i]->unparse();
    }
    else if (!MACRO_OCC[0]->is_default() ) //not default
    {
      Card += "MACOCC PROTOCOL " + MACO_PROTOCOL.unparse() + "\n";
      for (int i = 0; i < MACRO_OCC.size(); i++)
        Card += MACRO_OCC[i]->unparse();
    }
  }
  return Card;
}

Token_value MACO::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,2,"PROTOCOL","NCYC");
  if (keyIs("PROTOCOL"))
  {
    compulsoryKey(input_stream,4,"DEFAULT","CUSTOM","OFF","ALL");
    if (keyIs("DEFAULT")) setMACO_PROT("DEFAULT");
    if (keyIs("CUSTOM"))  setMACO_PROT("CUSTOM");
    if (keyIs("OFF"))     setMACO_PROT("OFF");
    if (keyIs("ALL"))     setMACO_PROT("ALL");
  }
  if (keyIs("NCYC"))
  {
    double ncyc = get1num(input_stream);
    std::string minimizer("");
    while (optionalKey(input_stream,1,"MINIMIZER"))
    {
      if (keyIs("MINIMIZER")) minimizer = getString(input_stream);
    }
    addMACO(ncyc,minimizer);
  }
  return skip_line(input_stream);
}

void MACO::addMACO(double ncyc,std::string minimizer)
{
  ncyc = iszeroposi(ncyc);
  bool error = data_minimizer().set(minimizer);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer method not recognized"));
  if (ncyc != -999) ncyc = iszeroposi(ncyc);
  if (data_minimizer().set(minimizer)) CCP4base::store(PhaserError(INPUT,keywords,"Minimizer not valid"));
  protocolPtr cPtr(new ProtocolOCC(ncyc,minimizer));
  MACRO_OCC.push_back(cPtr);
}

void MACO::setMACO_PROT(std::string method)
{
  bool error = MACO_PROTOCOL.set(method);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Protocol method not recognised"));
}

void MACO::analyse(void)
{
  if (MACO_PROTOCOL.is_default() && MACRO_OCC.size())
  {
    MACO_PROTOCOL.set("CUSTOM"); //backwards compatibility, don't need to specify CUSTOM
  }
  else if (MACO_PROTOCOL.is_default() || (MACO_PROTOCOL.is_custom() && !MACRO_OCC.size()))
  {
    //default two macrocycles first without restraints, second with restraints
    MACRO_OCC.clear();
    protocolPtr c1Ptr(new ProtocolOCC());
    MACRO_OCC.push_back(c1Ptr);
    MACRO_OCC.push_back(c1Ptr);
    MACRO_OCC.push_back(c1Ptr);
    MACRO_OCC.push_back(c1Ptr);
    MACRO_OCC.push_back(c1Ptr);
  }
  else if (MACO_PROTOCOL.is_off())
  {
    MACRO_OCC.clear();
    ProtocolOCC offMacro;
    offMacro.off();
    protocolPtr cPtr(new ProtocolOCC(offMacro));
    MACRO_OCC.push_back(cPtr);
  }
  else if (MACO_PROTOCOL.is_all())
  {
    MACRO_OCC.clear();
    ProtocolOCC offMacro;
    offMacro.all();
    protocolPtr cPtr(new ProtocolOCC(offMacro));
    MACRO_OCC.push_back(cPtr);
  }
}

}//phaser
