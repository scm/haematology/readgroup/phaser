//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PART.h>
#include <iotbx/pdb/write_utils.h>
#include <phaser/io/MtzHandler.h>
#include <phaser/mr_objects/rms_estimate.h>

namespace phaser {

PART::PART() : CCP4base(), InputBase()
{
  Add_Key("PART");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string PART::unparse()
{
  return  PARTIAL.unparse();
}

Token_value PART::parse(std::istringstream& input_stream)
{
//Two ways of parsing  HKLIN F/PHI columns
//First via HKLIN subkey is same as ENSEMBLE method (data_model unparse F/PH%I)
//Second via LABIN subkey is for partial only, and used for phenix python interface (FC/PHIC)
  compulsoryKey(input_stream,6,"PDB","CIF","HKLIN","LABIN","ANOMALOUS","ESTIMATOR");
  if (keyIs("PDB") || keyIs("CIF") || keyIs("HKLIN"))
  {
    if (keyIs("PDB"))   setPART_PDB(getFileName(input_stream));
    if (keyIs("CIF"))   setPART_CIF(getFileName(input_stream));
    if (keyIs("HKLIN")) setPART_HKLI(getFileName(input_stream));
    compulsoryKey(input_stream,3,"RMS","ID","F");
    if (keyIs("RMS")) setPART_VARI("RMS");
    if (keyIs("ID"))  setPART_VARI("ID");
    if (keyIs("F"))
    {
      setPART_LABI_FC(getAssignString(input_stream));
      compulsoryKey(input_stream,1,"PHI");
      setPART_LABI_PHIC(getAssignString(input_stream));
      compulsoryKey(input_stream,2,"RMS","ID");
      if (keyIs("RMS")) setPART_VARI("RMS");
      if (keyIs("ID"))  setPART_VARI("ID");
    }
    setPART_DEVI(get1num(input_stream));
  }
  else if (keyIs("LABIN"))
  {
    compulsoryKey(input_stream,1,"FC");
    setPART_LABI_FC(getAssignString(input_stream));
    compulsoryKey(input_stream,1,"PHIC");
    setPART_LABI_PHIC(getAssignString(input_stream));
  }
  else if (keyIs("ANOMALOUS"))  setPART_ANOM(getBoolean(input_stream));
  else if (keyIs("ESTIMATOR"))  setPART_ESTI(getString(input_stream));
  return skip_line(input_stream);
}

void PART::setPART_PDB(std::string filename)
{
  std::ifstream infile(const_cast<char*>(filename.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,filename));
  PARTIAL.set_filestr(filename);
  PARTIAL.set_format("PDB");
  PARTIAL.set_as_file();
}

void PART::setPART_CIF(std::string filename)
{
  std::ifstream infile(const_cast<char*>(filename.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,filename));
  PARTIAL.set_filestr(filename);
  PARTIAL.set_format("CIF");
  PARTIAL.set_as_file();
}

void PART::setPART_PDB_STR(std::string str)
{
  PARTIAL.set_filestr(str);
  PARTIAL.set_format("PDB");
  PARTIAL.set_as_string("partial");
}

void PART::setPART_CIF_STR(std::string str)
{
  PARTIAL.set_filestr(str);
  PARTIAL.set_format("CIF");
  PARTIAL.set_as_string("partial");
}

void PART::setPART_LABI_FC(std::string f)      { PARTIAL.LABI_F = f; }
void PART::setPART_LABI_PHIC(std::string p)    { PARTIAL.LABI_P = p; }
void PART::setPART_ANOM(bool b)                { PARTIAL.ANOMALOUS = b; }

void PART::setPART_ESTI(std::string estimator)
{
  estimator = stoup(estimator);
  rms_estimate tmp(estimator);
  if (tmp.error) CCP4base::store(PhaserError(INPUT,keywords,"PARTIAL ESTIMATOR " + estimator + ": not known"));
  PARTIAL.ESTIMATOR = estimator;
}

void PART::setPART_IOTBX(iotbx::pdb::hierarchy::root iotbx_input)
{
  iotbx::pdb::write_utils::sstream_open_close foc;
  iotbx::pdb::write_utils::sstream_write write(&foc.out);
  iotbx::pdb::hierarchy::models_as_pdb_string(write,iotbx_input.models(),false,false,true,false,false,false);
  PARTIAL.set_filestr(std::string(write.stream->str()));
  PARTIAL.set_format("PDB");
  PARTIAL.set_as_string("iotbx");
}

void PART::setPART_HKLI(std::string filename)
{
  std::ifstream infile(const_cast<char*>(filename.c_str()));
  if (!infile) CCP4base::store(PhaserError(FILEOPEN,keywords,filename));
  PARTIAL.set_filestr(filename);
  PARTIAL.set_format("MAP");
  PARTIAL.set_as_file();
}

void PART::setPART_VARI(std::string vari) { PARTIAL.set_vartype(stoup(vari)); }
void PART::setPART_DEVI(floatType dev)
{
  PARTIAL.set_rmsid(ispos(dev));
}

void PART::analyse(void)
{
  if (PARTIAL.map_format())
  {
    if (PARTIAL.ANOMALOUS)
      CCP4base::store(PhaserError(INPUT,keywords,"Partial structure MAP cannot be ANOMALOUS"));
    try {
      bool read_reflections(false);
      CMtz::MTZ *mtz = safe_mtz_get(PARTIAL.filename(),read_reflections);
      safe_mtz_col_lookup(mtz,PARTIAL.LABI_F);
      safe_mtz_col_lookup(mtz,PARTIAL.LABI_P);
    }
    catch (...)
    {
      CCP4base::store(PhaserError(INPUT,keywords,"Partial structure MAP does not have columns " + PARTIAL.LABI_F + "/"  + PARTIAL.LABI_P));
    }
  }
  if (PARTIAL.is_id()) //xxxxxxx AJM201 TODO
  {
    PARTIAL.set_rmsid(std::max(0.8,0.40*exp(1.87*(1.0-PARTIAL.rmsid())))); //minimum is 0.8
    PARTIAL.set_vartype("RMS");
  }
}


} //phaser
