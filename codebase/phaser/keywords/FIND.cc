//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/FIND.h>
#include <phaser_defaults.h>

namespace phaser {

FIND::FIND() : CCP4base(), InputBase()
{
  Add_Key("FIND");
  //Add to CCP4base
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string FIND::unparse()
{
  std::string Card("");
  Card += FINDSITES.unparse();
  Card += FIND_PEAKS.unparse();
  Card += FIND_PURGE.unparse();
  return Card;
}

Token_value FIND::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,9,"SCATTERER","NUMBER","CLUSTER","PURGE","PEAKS","TARGET","SPECIAL","DISTANCE","OCCUPANCY");
  if (keyIs("SCATTERER"))
  {
    setFIND_SCAT(getString(input_stream));
    //alternative parsing for NUMBER and CLUSTER
    if (optionalKey(input_stream,2,"NUMBER","CLUSTER"))
    {
      if (keyIs("NUMBER")) setFIND_NUMB(get1num(input_stream));
      if (keyIs("CLUSTER")) setFIND_CLUS(getBoolean(input_stream));
    }
  }
  else if (keyIs("NUMBER"))    setFIND_NUMB(get1num(input_stream));
  else if (keyIs("CLUSTER"))   setFIND_CLUS(getBoolean(input_stream));
  else if (keyIs("TARGET"))    setFIND_TARG(getString(input_stream));
  else if (keyIs("SPECIAL"))
  {
     compulsoryKey(input_stream,1,"POSITION");
     setFIND_SPEC_POSI(getBoolean(input_stream));
  }
  else if (keyIs("OCCUPANCY")) setFIND_OCCU(get1num(input_stream));
  else if (keyIs("PEAKS"))
  {
    while (optionalKey(input_stream,3,"SELECT","CLUSTER","CUTOFF"))
    {
      if (keyIs("SELECT"))
      {
        compulsoryKey(input_stream,4,"SIG","NUM","PERCENT","ALL");
        if (keyIs("SIG"))     setFIND_PEAK_SELE("SIGMA");
        if (keyIs("NUM"))     setFIND_PEAK_SELE("NUMBER");
        if (keyIs("PERCENT")) setFIND_PEAK_SELE("PERCENT");
        if (keyIs("ALL"))     setFIND_PEAK_SELE("ALL");
      }
      else if (keyIs("CUTOFF"))  setFIND_PEAK_CUTO(get1num(input_stream));
      else if (keyIs("CLUSTER")) setFIND_PEAK_CLUS(getBoolean(input_stream));
    }
  }
  else if (keyIs("PURGE"))
  {
    while (optionalKey(input_stream,3,"SELECT","CUTOFF","OCCUPANCY"))
    {
      if (keyIs("SELECT"))
      {
        compulsoryKey(input_stream,3,"SIG","NUM","ALL");
        if (keyIs("SIG"))     setFIND_PURG_SELE("SIGMA");
        if (keyIs("NUM"))     setFIND_PURG_SELE("NUMBER");
        if (keyIs("ALL"))     setFIND_PURG_SELE("ALL");
      }
      else if (keyIs("CUTOFF"))  setFIND_PURG_CUTO(get1num(input_stream));
      else if (keyIs("OCCUPANCY")) setFIND_PURG_OCCU(get1num(input_stream));
    }
  }
  else if (keyIs("DISTANCE"))
  {
    compulsoryKey(input_stream,2,"CENTROSYMMETRIC","DUPLICATE");
    if (keyIs("CENTROSYMMETRIC")) setFIND_DIST_CENT(get1num(input_stream));
    if (keyIs("DUPLICATE"))       setFIND_DIST_DUPL(get1num(input_stream));
  }
  return skip_line(input_stream);
}

void FIND::setFIND_SCAT(std::string t) { FINDSITES.TYPE = t; }
void FIND::setFIND_NUMB(double n)      { FINDSITES.NATOM = isposi(n); }
void FIND::setFIND_CLUS(bool b)        { FINDSITES.CLUSTER = b; }
void FIND::setFIND_TARG(std::string t) { FINDSITES.TARGET = t; }
void FIND::setFIND_DIST_CENT(double d) { FINDSITES.DIST_CENTRO = ispos(d); }
void FIND::setFIND_DIST_DUPL(double d) { FINDSITES.DIST_DUPL = ispos(d); }
void FIND::setFIND_SPEC_POSI(bool b)   { FINDSITES.SPECIAL_POSITION = b; }
void FIND::setFIND_OCCU(double d)      { FINDSITES.OCCUPANCY = ispos(std::min(1.0,d)); }

void FIND::setFIND_PEAK_CLUS(bool b)   { FIND_PEAKS.CLUSTER = b; }
void FIND::setFIND_PEAK_SELE(std::string test)
{
  bool error =  FIND_PEAKS.set_select(test);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Peaks selection not recognised"));
}
void FIND::setFIND_PEAK_CUTO(floatType num)
{
  if (FIND_PEAKS.by_percent()) FIND_PEAKS.set_cutoff( isperc(num) );
  if (FIND_PEAKS.by_sigma())   FIND_PEAKS.set_cutoff( ispos(num) );
  if (FIND_PEAKS.by_number())  FIND_PEAKS.set_cutoff( isposi(num) );
}

void FIND::setFIND_PURG_SELE(std::string test)
{
  bool error =  FIND_PURGE.set_select(test);
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Purge selection not recognised"));
}
void FIND::setFIND_PURG_CUTO(floatType num)
{
  if (FIND_PURGE.by_percent()) FIND_PURGE.set_cutoff( isperc(num) );
  if (FIND_PURGE.by_sigma())   FIND_PURGE.set_cutoff( ispos(num) );
  if (FIND_PURGE.by_number())  FIND_PURGE.set_cutoff( isposi(num) );
}
void FIND::setFIND_PURG_OCCU(double o) { FIND_PURGE.OCCUPANCY = iszeropos(o); }

void FIND::analyse(void)
{
  if (!FINDSITES.TYPE.size() && !FINDSITES.NATOM) throw PhaserError(SYNTAX,keywords,"Type of substructure and number of sites to find not set");
  if (!FINDSITES.TYPE.size()) throw PhaserError(SYNTAX,keywords,"Type of substructure to find not set");
  if (!FINDSITES.NATOM) throw PhaserError(SYNTAX,keywords,"Number of substructure sites to find not set");
}

}//phaser
