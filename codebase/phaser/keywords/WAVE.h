//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __WAVEClass__
#define __WAVEClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class WAVE : public InputBase, virtual public CCP4base
{
  public:
    floatType WAVELENGTH;

    WAVE();
    virtual ~WAVE() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setWAVE(floatType);
};

} //phaser

#endif
