//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __NMAClass__
#define __NMAClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class NMA : public InputBase, virtual public CCP4base
{
  public:
    int             NMA_COMBINATION;
    af::shared<int> NMA_MODE;
    bool            NMA_ORIG;

    NMA();
    virtual ~NMA() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void addNMA_MODE(floatType);
    void setNMA_COMB(floatType);
    void setNMA_ORIG(bool);
};

} //phaser

#endif
