//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/PERM.h>
#include <phaser_defaults.h>

namespace phaser {

PERM::PERM() : CCP4base(), InputBase()
{
  Add_Key("PERM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  DO_PERMUTE = bool(DEF_PERM);
}

std::string PERM::unparse()
{
  if (DO_PERMUTE == bool(DEF_PERM)) return "";
  return "PERMUTATIONS " + card(DO_PERMUTE) + "\n";
}

Token_value PERM::parse(std::istringstream& input_stream)
{
  setPERM(getBoolean(input_stream));
  return skip_line(input_stream);
}

void PERM::setPERM(bool do_permute)
{ DO_PERMUTE = do_permute; }

void PERM::analyse(void)
{
}


} //phaser
