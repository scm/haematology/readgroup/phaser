//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/NORM.h>
#include <fstream>

namespace phaser {

NORM::NORM() : CCP4base(), InputBase()
{
  Add_Key("NORM");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);

  SIGMAN.FILENAME = "";
  SIGMAN.READ = boost::indeterminate;
}

std::string NORM::unparse()
{
  std::string Card("");
  if (SIGMAN.READ && SIGMAN.FILENAME.size()) Card += "NORM EPSFAC READ \"" + SIGMAN.FILENAME + "\"\n";
  if (!SIGMAN.READ && SIGMAN.FILENAME.size()) Card += "NORM EPSFAC WRITE \"" + SIGMAN.FILENAME + "\"\n";
  return Card; //use getter
}

Token_value NORM::parse(std::istringstream& input_stream)
{
  //use setter from python
  //this is for scripting only
  compulsoryKey(input_stream,1,"EPSFAC");
  compulsoryKey(input_stream,2,"READ","WRITE");
  SIGMAN.READ = keyIs("READ"); //indeterminate of not set
  SIGMAN.FILENAME = getFileName(input_stream); //binary file setting
  return skip_line(input_stream);
}

void NORM::analyse(void)
{
  if (SIGMAN.FILENAME.size() && SIGMAN.READ)
  {
    CCP4base::store(PhaserError(NULL_ERROR,keywords,"Normalization parameters read from binary file \"" + SIGMAN.FILENAME + "\""));
  }
}

}//phaser
