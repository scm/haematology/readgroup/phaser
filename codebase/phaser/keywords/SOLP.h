//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __SOLPClass__
#define __SOLPClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_solpar.h>

namespace phaser {

class SOLP : public InputBase, virtual public CCP4base
{
  public:
    data_solpar SOLPAR;

    SOLP();
    virtual ~SOLP() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setSOLP_SIGA_FSOL(floatType);
    void setSOLP_SIGA_BSOL(floatType);
    void setSOLP_SIGA_MIN(floatType);
    void setSOLP_BULK_FSOL(floatType);
    void setSOLP_BULK_BSOL(floatType);
    void setSOLP_BULK_USE(bool);
};

} //phaser

#endif
