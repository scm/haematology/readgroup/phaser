//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/OCCU.h>
#include <scitbx/constants.h>
#include <phaser_defaults.h>

namespace phaser {

OCCU::OCCU() : CCP4base(), InputBase()
{
  Add_Key("OCCU");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string OCCU::unparse()
{
  return OCCUPANCY.unparse();
}

Token_value OCCU::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,7,"WINDOW","MIN","MAX","MERGE","OFFSET","FRAC","BIAS");
  if (keyIs("WINDOW"))
  {
    compulsoryKey(input_stream,3,"ELLG","NRES","MAX");
    if (keyIs("ELLG")) setOCCU_WIND_ELLG(get1num(input_stream));
    if (keyIs("NRES")) setOCCU_WIND_NRES(get1num(input_stream));
    if (keyIs("MAX"))  setOCCU_WIND_MAX(get1num(input_stream));
  }
  if (keyIs("MIN"))
  {
    setOCCU_MIN(get1num(input_stream));
    if (optionalKey(input_stream,1,"MAX"))
      setOCCU_MAX(get1num(input_stream));
  }
  if (keyIs("MAX"))
  {
    setOCCU_MAX(get1num(input_stream));
    if (optionalKey(input_stream,1,"MIN"))
      setOCCU_MIN(get1num(input_stream));
  }
  if (keyIs("MERGE"))   setOCCU_MERG(getBoolean(input_stream));
  if (keyIs("OFFSET"))  setOCCU_OFFS(get1num(input_stream));
  if (keyIs("FRAC"))    setOCCU_FRAC(get1num(input_stream));
  if (keyIs("BIAS"))    setOCCU_BIAS(get1num(input_stream));
  return skip_line(input_stream);
}

void OCCU::setOCCU_WIND_ELLG(double ellg)  { OCCUPANCY.WINDOW_ELLG = ispos(ellg); }
void OCCU::setOCCU_WIND_NRES(double natom) { OCCUPANCY.WINDOW_NRES = iszeroposi(natom); }
void OCCU::setOCCU_WIND_MAX(double m)      { OCCUPANCY.WINDOW_MAX = ispos(m); }
void OCCU::setOCCU_MIN(double m)           { OCCUPANCY.MIN = ispos(m); }
void OCCU::setOCCU_MAX(double m)           { OCCUPANCY.MAX = ispos(m); }
void OCCU::setOCCU_MERG(bool b)            { OCCUPANCY.MERGE = b; }
void OCCU::setOCCU_OFFS(double i)          { OCCUPANCY.OFFSET = iszeropos(i); }
void OCCU::setOCCU_FRAC(double b)          { OCCUPANCY.FRACSCAT = b; }
void OCCU::setOCCU_BIAS(double b)          { OCCUPANCY.BIAS = b; }

void OCCU::analyse(void)
{
  //asser min < max
  double tmp_max = std::max(OCCUPANCY.MIN,OCCUPANCY.MAX);
  double tmp_min = std::min(OCCUPANCY.MIN,OCCUPANCY.MAX);
  //set an absolute non-zero limit on occupancy
  OCCUPANCY.MIN = std::max(double(DEF_MR_OMIN),tmp_min);
  OCCUPANCY.MAX = std::max(double(DEF_MR_OMIN),tmp_max);
}

}//phaser
