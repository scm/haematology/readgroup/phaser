//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LLGMClass__
#define __LLGMClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_llgc.h>

namespace phaser {

class LLGM : public InputBase, virtual public CCP4base
{
  public:
    bool DO_LLGMAPS;

    LLGM();
    virtual ~LLGM() {}

  private:
    void init(std::string);

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setLLGM(bool);
};

} //phaser

#endif
