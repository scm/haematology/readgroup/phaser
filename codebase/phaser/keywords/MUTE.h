//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MUTEClass__
#define __MUTEClass__
#include <phaser/io/CCP4base.h>

namespace phaser {

class MUTE : public InputBase, virtual public CCP4base
{
  public:
    bool MUTE_ON;

    MUTE();
    virtual ~MUTE() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setMUTE(bool);
};

} //phaser

#endif
