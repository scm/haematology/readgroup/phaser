//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __VARSClass__
#define __VARSClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/VarianceEP.h>

namespace phaser {

class VARS : public InputBase, virtual public CCP4base
{
  public:
    VarianceEP VAR_EP;

    VARS();
    virtual ~VARS() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setVARS(af_float);
};

} //phaser

#endif
