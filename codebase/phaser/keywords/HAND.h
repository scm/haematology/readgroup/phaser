//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __HANDClass__
#define __HANDClass__
#include <phaser/include/data_hand.h>
#include <phaser/io/CCP4base.h>

namespace phaser {

class HAND : public InputBase, virtual public CCP4base
{
  public:
    data_hand HAND_CHANGE;

    HAND();
    virtual ~HAND() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setHAND(std::string);
};

} //phaser

#endif
