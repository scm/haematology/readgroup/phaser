//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/keywords/LABI.h>
#include <cctbx/uctbx.h>

namespace phaser {

LABI::LABI() : CCP4base(), InputBase()
{
  Add_Key("LABI");
  //Add to CCP4base;
  inputPtr iPtr(this);
  possible_fns.push_back(iPtr);
}

std::string LABI::unparse()
{
  return REFLECTIONS.unparse("LABIN");
  //does not unparse reflection data by HKL
}

Token_value LABI::parse(std::istringstream& input_stream)
{
  compulsoryKey(input_stream,4,"F","I","FWT","FMAP");
  if (keyIs("FMAP")) //must be before F
  {
    setLABI_FMAP(getAssignString(input_stream));
    compulsoryKey(input_stream,1,"PHMAP");
    setLABI_PHMA(getAssignString(input_stream));
    if (optionalKey(input_stream,1,"FOM"))
      setLABI_FOM(getAssignString(input_stream));
  }
  else if (keyIs("FWT")) //must be before F
  {
    setLABI_FWT(getAssignString(input_stream));
    compulsoryKey(input_stream,1,"PHWT");
    setLABI_PHWT(getAssignString(input_stream));
  }
  else if (keyIs("F"))
  {
    setLABI_F(getAssignString(input_stream));
    if (optionalKey(input_stream,1,"SIGF"))
      setLABI_SIGF(getAssignString(input_stream));
  }
  else if (keyIs("I"))
  {
    setLABI_I(getAssignString(input_stream));
    if (optionalKey(input_stream,1,"SIGI"))
      setLABI_SIGI(getAssignString(input_stream));
  }
  //do not parse reflection data by HKL
  return skip_line(input_stream);
}

void LABI::setLABI_F(std::string F)         { REFLECTIONS.F_ID = F; }
void LABI::setLABI_SIGF(std::string SIGF)   { REFLECTIONS.SIGF_ID = SIGF; }
void LABI::setLABI_I(std::string I)         { REFLECTIONS.I_ID = I; }
void LABI::setLABI_SIGI(std::string SIGI)   { REFLECTIONS.SIGI_ID = SIGI; }
void LABI::setLABI_FMAP(std::string FMAP)   { REFLECTIONS.FTFMAP.FMAP_ID = FMAP; }
void LABI::setLABI_PHMA(std::string PHMAP)  { REFLECTIONS.FTFMAP.PHMAP_ID = PHMAP; }
void LABI::setLABI_FOM(std::string FOM)     { REFLECTIONS.FTFMAP.FOM_ID = FOM; }
void LABI::setLABI_FWT(std::string FMAP)    { REFLECTIONS.FTFMAP.FMAP_ID = FMAP; }
void LABI::setLABI_PHWT(std::string PHMAP)  { REFLECTIONS.FTFMAP.PHMAP_ID = PHMAP; }

void LABI::setREFL_F_SIGF(af::shared<miller::index<int> > HKL,af_float fmean,af_float sigfmean)
{
  if (HKL.size() != fmean.size())
    CCP4base::store(PhaserError(INPUT,keywords,"F array not equal to miller size"));
  if (fmean.size() != sigfmean.size())
    CCP4base::store(PhaserError(INPUT,keywords,"F and SIGF arrays different sizes"));
  REFLECTIONS.MILLER = HKL.deep_copy();
  REFLECTIONS.F = fmean.deep_copy();
  REFLECTIONS.SIGF = sigfmean.deep_copy();
  //REFLECTIONS.SPACEGROUP set with SPAC Card
  //REFLECTIONS.UNITCELL set with CELL Card
}

void LABI::setREFL_I_SIGI(af::shared<miller::index<int> > HKL,af_float imean,af_float sigimean)
{
  if (HKL.size() != imean.size())
    CCP4base::store(PhaserError(INPUT,keywords,"I array not equal to miller size"));
  if (imean.size() != sigimean.size())
    CCP4base::store(PhaserError(INPUT,keywords,"I and SIGI arrays different sizes"));
  REFLECTIONS.MILLER = HKL.deep_copy();
  REFLECTIONS.I = imean.deep_copy();
  REFLECTIONS.SIGI = sigimean.deep_copy();
  //REFLECTIONS.SPACEGROUP set with SPAC Card
  //REFLECTIONS.UNITCELL set with CELL Card
}

void LABI::setREFL_FMAP_PHMAP_FOM(af_float fmap,af_float phase,af_float fom)
{
  //don't clear
  if (REFLECTIONS.MILLER.size() != phase.size())
    CCP4base::store(PhaserError(INPUT,keywords,"FMAP array not equal to miller size"));
  if (REFLECTIONS.MILLER.size() != phase.size())
    CCP4base::store(PhaserError(INPUT,keywords,"PHMAP array not equal to miller size"));
  if (REFLECTIONS.MILLER.size() != fom.size())
    CCP4base::store(PhaserError(INPUT,keywords,"FOM array not equal to miller size"));
  REFLECTIONS.FTFMAP.FMAP = fmap.deep_copy();
  REFLECTIONS.FTFMAP.PHMAP = phase.deep_copy();
  REFLECTIONS.FTFMAP.FOM = fom.deep_copy();
}

void LABI::setREFL_FWT_PHWT(af_float fmap,af_float phase)
{
  //don't clear
  if (REFLECTIONS.MILLER.size() != phase.size())
    CCP4base::store(PhaserError(INPUT,keywords,"FMAP array not equal to miller size"));
  if (REFLECTIONS.MILLER.size() != phase.size())
    CCP4base::store(PhaserError(INPUT,keywords,"PHMAP array not equal to miller size"));
  REFLECTIONS.FTFMAP.FMAP = fmap.deep_copy();
  REFLECTIONS.FTFMAP.PHMAP = phase.deep_copy();
  REFLECTIONS.FTFMAP.FOM.clear();
}

void LABI::setLABI_F_SIGF(std::string F,std::string SIGF)
{
  REFLECTIONS.F_ID = F;
  REFLECTIONS.SIGF_ID = SIGF;
}

void LABI::setLABI_I_SIGI(std::string I,std::string SIGI)
{
  REFLECTIONS.I_ID = I;
  REFLECTIONS.SIGI_ID = SIGI;
}

void LABI::setLABI_FMAP_PHMAP_FOM(std::string fmap_id, std::string phmap_id,std::string fom_id)
{
  REFLECTIONS.FTFMAP.FMAP_ID = fmap_id;
  REFLECTIONS.FTFMAP.PHMAP_ID = phmap_id;
  REFLECTIONS.FTFMAP.FOM_ID = fom_id;
}

void LABI::setLABI_FWT_PHWT(std::string fmap_id, std::string phmap_id)
{
  REFLECTIONS.FTFMAP.FMAP_ID = fmap_id;
  REFLECTIONS.FTFMAP.PHMAP_ID = phmap_id;
  REFLECTIONS.FTFMAP.FOM_ID = "";
}

void LABI::setREFL_DATA(data_refl& init)
{ REFLECTIONS = init; } //copy by reference

void LABI::setREFL_HALL(std::string hall)
{ if (hall.size()) REFLECTIONS.SG_HALL = hall; }

void LABI::setREFL_CELL(af::double6 cell)
{
  bool DEFAULT_CELL = cctbx::uctbx::unit_cell(cell).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
  if (!DEFAULT_CELL) REFLECTIONS.UNIT_CELL = cell;
}

void LABI::analyse(void)
{
  for (int r = 0; r < REFLECTIONS.MILLER.size(); r++)
  {
    bool zerozerozero(REFLECTIONS.MILLER[r][0] == 0 && REFLECTIONS.MILLER[r][1] == 0 && REFLECTIONS.MILLER[r][2] == 0);
    if (zerozerozero)
    {
      REFLECTIONS.erase(r);
      break;
    }
  }
  bool error = REFLECTIONS.set_default_id("");
  if (error) CCP4base::store(PhaserError(INPUT,keywords,"Duplicate data labels assigned"));
  REFLECTIONS.INPUT_INTENSITIES = REFLECTIONS.I.size();
  REFLECTIONS.set_default_dfactor();
 // if (!REFLECTIONS.MILLER.size() && REFLECTIONS.F_ID == "" && REFLECTIONS.I_ID == "")
 // CCP4base::store(PhaserError(INPUT,keywords,"LABIN F/I not assigned"));
}

} //phaser
