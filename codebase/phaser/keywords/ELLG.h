//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ELLGClass__
#define __ELLGClass__
#include <phaser/io/CCP4base.h>
#include <phaser/include/data_ellg.h>

namespace phaser {

class ELLG : public InputBase, virtual public CCP4base
{
  public:
    bool      ELLG_HIRES; //this is for internal control, not user input
    std::map<std::string,data_ellg> ELLG_DATA; //this is for internal control, not user input
    floatType ELLG_TARGET;
    std::set<double> ELLG_POLYALANINE;
    double    ELLG_RMSD_MULT;

    ELLG();
    virtual ~ELLG() {}

  protected:
    Token_value parse(std::istringstream&);
    std::string unparse(void);
    void analyse(void);

  public:
    void setELLG_TARG(floatType);
    void setELLG_HIRE(bool);
    void addELLG_POLY_RMSD(double);
    void setELLG_RMSD_MULT(double);
};

} //phaser

#endif
