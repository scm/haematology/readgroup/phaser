//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/io/InputNMA.h>
#include <phaser/io/ResultNMA.h>

namespace phaser {

ResultNMA runSCEDS(InputNMA& input,Output output)
{
  bool RUN_SCEDS = true;
  return runNMA(input,RUN_SCEDS,output);
}

//for python
ResultNMA runSCEDS(InputNMA& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runSCEDS(input,output);
}

} //end namespace phaser
