//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/io/InputNMA.h>
#include <phaser/io/ResultNMA.h>

namespace phaser {

ResultNMA runNMAXYZ(InputNMA& input,Output output)
{
  bool RUN_SCEDS = false;
  return runNMA(input,RUN_SCEDS,output);
}

//for python
ResultNMA runNMAXYZ(InputNMA& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runNMAXYZ(input,output);
}

} //end namespace phaser
