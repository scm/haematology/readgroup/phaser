//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/lib/sg_prior.h>
#include <cmtzlib.h>
#include <mtzdata.h>
#include <ccp4_errno.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <phaser_defaults.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR_DAT runMR_DAT(InputMR_DAT& input,Output output)
{
  ResultMR_DAT results(output);
  bool incoming_bookend = results.getBookend();
  try
  {
    CCP4::ccp4_liberr_verbosity(0);
    results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
    results.setLevel(input.OUTPUT_LEVEL);
    results.setMute(input.MUTE_ON);
    results.logHeader(LOGFILE,header(MR_DAT));
    input.Analyse(results);

    if (input.HKLIN == "") throw PhaserError(FATAL,"Mtz file name not set");

    CMtz::MTZCOL *mtzH(0),*mtzK(0),*mtzL(0);
    CMtz::MTZCOL *Fcol(0),*SIGFcol(0),*Icol(0),*SIGIcol(0),*FMAPcol(0),*PHMAPcol(0),*FOMcol(0),*DFACcol(0);
    int read_reflections(0);
    CMtz::MTZ *mtz = safe_mtz_get(input.HKLIN,read_reflections);

    if (input.REFLECTIONS.I_ID == "" && input.REFLECTIONS.F_ID == "" &&
        input.REFLECTIONS.FTFMAP.FMAP_ID == "")
    {
      int Fcount(0),Icount(0);
      std::string Flabel,Ilabel,SIGFlabel,SIGIlabel;;
      for (int i = 0; i < mtz->nxtal; ++i)
      for (int j = 0; j < mtz->xtal[i]->nset; ++j)
      for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
      {
        if (mtz->xtal[i]->set[j]->col[k]->type[0] == 'F') //amplitudes
        {
          bool isF(true);
          std::string label(mtz->xtal[i]->set[j]->col[k]->label); //string conversion
          if (label == "FOM") //bug in phenix.fetch_pdb --mtz conversion
            isF=false;// this F is a FOM
          if (k < mtz->xtal[i]->set[j]->ncol-1 && mtz->xtal[i]->set[j]->col[k+1]->type[0] == 'P')
            isF=false;// this F is an Fcalc
          if (isF)
          {
            Fcount++;
            Flabel = mtz->xtal[i]->set[j]->col[k]->label;
            if (k < mtz->xtal[i]->set[j]->ncol-1 && mtz->xtal[i]->set[j]->col[k+1]->type[0] == 'Q')
              SIGFlabel = mtz->xtal[i]->set[j]->col[k+1]->label;
          }
        }
        else if (mtz->xtal[i]->set[j]->col[k]->type[0] == 'J') //intensity
        {
          Icount++;
          Ilabel = mtz->xtal[i]->set[j]->col[k]->label;
          if (k < mtz->xtal[i]->set[j]->ncol-1 && mtz->xtal[i]->set[j]->col[k+1]->type[0] == 'Q')
            SIGIlabel = mtz->xtal[i]->set[j]->col[k+1]->label;
        }
      }
      if (input.REFLECTIONS.I_ID == "" && Icount == 1)
      {
        input.REFLECTIONS.I_ID = Ilabel;
        if (SIGIlabel.size()) input.REFLECTIONS.SIGI_ID = SIGIlabel;
      }
      else if (input.REFLECTIONS.F_ID == "" && Fcount == 1)
      {
        input.REFLECTIONS.F_ID = Flabel;
        if (SIGFlabel.size()) input.REFLECTIONS.SIGF_ID = SIGFlabel;
      }
    }
    if (input.REFLECTIONS.F_ID == "" && input.REFLECTIONS.I_ID == "")
    {
      if (input.REFLECTIONS.FTFMAP.FMAP_ID == "")
      {
        int Fcount(0);
        std::string Flabel,Phslabel,FOMlabel;
        for (int i = 0; i < mtz->nxtal; ++i)
        for (int j = 0; j < mtz->xtal[i]->nset; ++j)
        for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
        {
          if (mtz->xtal[i]->set[j]->col[k]->type[0] == 'F') //amplitudes
          {
            bool isF(true);
            std::string label(mtz->xtal[i]->set[j]->col[k]->label); //string conversion
            if (label == "FOM") //bug in phenix.fetch_pdb --mtz conversion
              isF=false;// this F is a FOM
            if (k < mtz->xtal[i]->set[j]->ncol-1 && mtz->xtal[i]->set[j]->col[k+1]->type[0] != 'P')
              isF=false;// this F is an Fcalc
           // FOM is not compulsory
           // if (k < mtz->xtal[i]->set[j]->ncol-2 && mtz->xtal[i]->set[j]->col[k+2]->type[0] != 'W')
           //   isF=false;// this F is an Fcalc
            if (isF)
            {
              Fcount++;
              Flabel = mtz->xtal[i]->set[j]->col[k]->label;
              if (k < mtz->xtal[i]->set[j]->ncol-1 && mtz->xtal[i]->set[j]->col[k+1]->type[0] == 'P')
                Phslabel = mtz->xtal[i]->set[j]->col[k+1]->label;
              //read fom if it is there
              if (k < mtz->xtal[i]->set[j]->ncol-2 && mtz->xtal[i]->set[j]->col[k+2]->type[0] == 'W')
                FOMlabel = mtz->xtal[i]->set[j]->col[k+1]->label;
            }
          }
        }
        if (Fcount == 1)
        {
          input.REFLECTIONS.FTFMAP.FMAP_ID = Flabel;
          input.REFLECTIONS.FTFMAP.PHMAP_ID = Phslabel;
          input.REFLECTIONS.FTFMAP.FOM_ID = FOMlabel;
        }
      }
    }
    if (input.REFLECTIONS.F_ID == "" && input.REFLECTIONS.I_ID == "" && input.REFLECTIONS.FTFMAP.FMAP_ID == "")
    {
      throw PhaserError(FATAL,"Data structure factors F/I/FMAP not set");
    }

    mtzH = safe_mtz_col_lookup(mtz,"H");
    mtzK = safe_mtz_col_lookup(mtz,"K");
    mtzL = safe_mtz_col_lookup(mtz,"L");
    if (input.REFLECTIONS.F_ID != "")
    {
      Fcol = safe_mtz_col_lookup(mtz, input.REFLECTIONS.F_ID);
      if ((*Fcol->type) != 'F')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.F_ID + " not \'F\'");
    }
    if (input.REFLECTIONS.SIGF_ID != "")
    {
      SIGFcol = safe_mtz_col_lookup(mtz, input.REFLECTIONS.SIGF_ID);
      if ((*SIGFcol->type) != 'Q')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.SIGF_ID + " not \'Q\'");
    }
    if (input.REFLECTIONS.I_ID != "")
    {
      Icol = safe_mtz_col_lookup(mtz, input.REFLECTIONS.I_ID);
      if ((*Icol->type) != 'J')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.I_ID + " not \'J\'");
    }
    if (input.REFLECTIONS.SIGI_ID != "")
    {
      SIGIcol =  safe_mtz_col_lookup(mtz, input.REFLECTIONS.SIGI_ID);
      if ((*SIGIcol->type) != 'Q')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.SIGI_ID + " not \'Q\'");
    }
    if (input.REFLECTIONS.FTFMAP.FMAP_ID != "")
    {
      FMAPcol =  safe_mtz_col_lookup(mtz, input.REFLECTIONS.FTFMAP.FMAP_ID);
      if ((*FMAPcol->type) != 'F')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.FTFMAP.FMAP_ID + " not \'F\'");
    }
    if (input.REFLECTIONS.FTFMAP.PHMAP_ID != "")
    {
      PHMAPcol = safe_mtz_col_lookup(mtz, input.REFLECTIONS.FTFMAP.PHMAP_ID);
      if ((*PHMAPcol->type) != 'P')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.FTFMAP.PHMAP_ID + " not \'P\'");
    }
    if (input.REFLECTIONS.FTFMAP.FOM_ID != "")
    {
      FOMcol = safe_mtz_col_lookup(mtz, input.REFLECTIONS.FTFMAP.FOM_ID);
      if ((*FOMcol->type) != 'W')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.FTFMAP.FOM_ID + " not \'W\'");
    }
    if (input.REFLECTIONS.DFAC_ID != "")
    {
      DFACcol =  safe_mtz_col_lookup(mtz, input.REFLECTIONS.DFAC_ID);
      if ((*DFACcol->type) != 'R')
      throw PhaserError(FATAL,"Mtz column type for " + input.REFLECTIONS.DFAC_ID + " not \'R\'");
    }

/* UNITCELL */
    PHASER_ASSERT(input.HKLIN != "");
    CMtz::MTZXTAL* xtal(NULL);
    if (input.REFLECTIONS.F_ID != "")
      xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, Fcol));
    else if (input.REFLECTIONS.I_ID != "")
      xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, Icol));
    else if (input.REFLECTIONS.FTFMAP.FMAP_ID != "")
      xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, FMAPcol));
    PHASER_ASSERT(xtal != 0);
    floatType a     = static_cast<floatType>(xtal->cell[0]);
    floatType b     = static_cast<floatType>(xtal->cell[1]);
    floatType c     = static_cast<floatType>(xtal->cell[2]);
    floatType alpha = static_cast<floatType>(xtal->cell[3]);
    floatType beta  = static_cast<floatType>(xtal->cell[4]);
    floatType gamma = static_cast<floatType>(xtal->cell[5]);
    input.REFLECTIONS.UNIT_CELL = af::double6(a,b,c,alpha,beta,gamma);
    UnitCell uc(input.REFLECTIONS.UNIT_CELL);

/* DATA */
    floatType HIRES(DEF_LORES),LORES(0);
    floatType MTZ_HIRES(DEF_LORES),MTZ_LORES(0);


    {//memory
    int ncols = 3; //H K L
    if (input.REFLECTIONS.F_ID != "")     ncols++;
    if (input.REFLECTIONS.SIGF_ID != "")  ncols++;
    if (input.REFLECTIONS.I_ID != "")     ncols++;
    if (input.REFLECTIONS.SIGI_ID != "")  ncols++;
    if (input.REFLECTIONS.FTFMAP.FMAP_ID != "")  ncols++;
    if (input.REFLECTIONS.FTFMAP.PHMAP_ID != "") ncols++;
    if (input.REFLECTIONS.FTFMAP.FOM_ID != "")   ncols++;
    if (input.REFLECTIONS.DFAC_ID != "")  ncols++;
    int n = 3;
    int nf = (input.REFLECTIONS.F_ID != "") ? n++ : 0; //3
    int nsigf = (input.REFLECTIONS.SIGF_ID != "") ? n++ : 0; //4
    int ni = (input.REFLECTIONS.I_ID != "") ? n++ : 0; //4 or 5
    int nsigi = (input.REFLECTIONS.SIGI_ID != "") ? n++ : 0; //4 or 5 or 6
    int nfmap = (input.REFLECTIONS.FTFMAP.FMAP_ID != "") ? n++ : 0; // 4 or 5 or 6 or 7
    int nphmap = (input.REFLECTIONS.FTFMAP.PHMAP_ID != "") ? n++ : 0; // 4 or 5 or 6 or 7
    int nfom = (input.REFLECTIONS.FTFMAP.FOM_ID != "") ? n++ : 0; // 4 or 5 or 6 or 7 or 8
    int ndfac = (input.REFLECTIONS.DFAC_ID != "") ? n++ : 0; // 4 or 5 or 6 or 7 or 8 or 9
    std::vector<std::pair<floatType,unsigned> > sorted;
    for (int mtzr = 0; mtzr < mtz->nref ; mtzr++)
    {
      int n(0);
      {
        std::vector<float> adata_(ncols);
        float* adata = &*adata_.begin();
        std::vector<int> logmss_(ncols);
        int* logmss = &*logmss_.begin();
        std::vector<CMtz::MTZCOL*> lookup_(ncols);
        CMtz::MTZCOL** lookup = &*lookup_.begin();
        lookup[n++] = mtzH;
        lookup[n++] = mtzK;
        lookup[n++] = mtzL;
        if (input.REFLECTIONS.F_ID != "") lookup[n++] = Fcol;
        if (input.REFLECTIONS.SIGF_ID != "") lookup[n++] = SIGFcol;
        if (input.REFLECTIONS.I_ID != "") lookup[n++] = Icol;
        if (input.REFLECTIONS.SIGI_ID != "") lookup[n++] = SIGIcol;
        if (input.REFLECTIONS.FTFMAP.FMAP_ID != "") lookup[n++] = FMAPcol;
        if (input.REFLECTIONS.FTFMAP.PHMAP_ID != "") lookup[n++] = PHMAPcol;
        if (input.REFLECTIONS.FTFMAP.FOM_ID != "") lookup[n++] = FOMcol;
        if (input.REFLECTIONS.DFAC_ID != "") lookup[n++] = DFACcol;
        int failure = MtzReadRefl(mtz,adata,logmss,lookup,ncols,mtzr+1);
        PHASER_ASSERT(!failure);
        int H = phaser::round(adata[0]);
        int K = phaser::round(adata[1]);
        int L = phaser::round(adata[2]);
        miller::index<int> HKL(H,K,L);
        floatType reso = uc.reso(HKL);
        MTZ_HIRES = std::min(reso,MTZ_HIRES);
        MTZ_LORES = std::max(reso,MTZ_LORES);
        MTZ_LORES = std::min(floatType(DEF_LORES),MTZ_LORES);
        if (reso >= input.HIRES && reso <= input.LORES && !logmss[3])
        {
          input.REFLECTIONS.MILLER.push_back(HKL);
          if (input.REFLECTIONS.F_ID != "" && !logmss[nf])
          {  // Do F/SIGF pair together
            input.REFLECTIONS.F.push_back(adata[nf]);
            (input.REFLECTIONS.SIGF_ID != "" && !logmss[nsigf]) ?
              input.REFLECTIONS.SIGF.push_back(adata[nsigf]): input.REFLECTIONS.SIGF.push_back(0);
          }
          //no default for iobs/sigiobs
          if (input.REFLECTIONS.I_ID != "" && !logmss[ni])
          {  // Do I/SIGI pair together, check that both exist
             input.REFLECTIONS.I.push_back(adata[ni]);
             PHASER_ASSERT(input.REFLECTIONS.SIGI_ID != "" && !logmss[nsigi]);
             input.REFLECTIONS.SIGI.push_back(adata[nsigi]);
          }
          //no default for phase
          if ((input.REFLECTIONS.FTFMAP.FMAP_ID != "" && !logmss[nfmap])
              &&  (input.REFLECTIONS.FTFMAP.PHMAP_ID != "" && !logmss[nphmap]))
          {
            input.REFLECTIONS.FTFMAP.FMAP.push_back(adata[nfmap]);
            input.REFLECTIONS.FTFMAP.PHMAP.push_back(adata[nphmap]);
            //only read fom if phase is present
            //default for fom is 1
            (input.REFLECTIONS.FTFMAP.FOM_ID != "" && !logmss[nfom]) ?
              input.REFLECTIONS.FTFMAP.FOM.push_back(adata[nfom]) : input.REFLECTIONS.FTFMAP.FOM.push_back(1);
          }
          if (input.REFLECTIONS.DFAC_ID != "" && !logmss[ndfac])
            input.REFLECTIONS.DFAC.push_back(adata[ndfac]);
          HIRES = std::min(reso,HIRES);
          LORES = std::max(reso,LORES);
          std::pair<floatType,unsigned> reso_r(reso,input.REFLECTIONS.MILLER.size()-1);
          sorted.push_back(reso_r);
        }
      }
    }

    //now put FMAP in F column also, creating SIGF
    if (input.REFLECTIONS.I_ID == "" && input.REFLECTIONS.F_ID == "" && input.REFLECTIONS.FTFMAP.FMAP_ID != "")
    {
      input.REFLECTIONS.F = input.REFLECTIONS.FTFMAP.FMAP.deep_copy();
      input.REFLECTIONS.SIGF.resize(input.REFLECTIONS.F.size(),0);
    }

    int NREFL = input.REFLECTIONS.MILLER.size();
    bool DEF_SORT_REFL(true); //hardwired default
    if (DEF_SORT_REFL) std::sort(sorted.begin(),sorted.end());

    //now reorder the arrays, one at a time to save memory
    { //memory
      std::vector<miller::index<int> > tmpdat(NREFL);
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.MILLER[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.MILLER[r] = tmpdat[r]; //af::shared
    }
    float1D tmpdat(NREFL);
    if (input.REFLECTIONS.F.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.F[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.F[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.SIGF.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.SIGF[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.SIGF[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.I.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.I[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.I[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.SIGI.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.SIGI[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.SIGI[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.FTFMAP.FMAP.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.FTFMAP.FMAP[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.FTFMAP.FMAP[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.FTFMAP.PHMAP.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.FTFMAP.PHMAP[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.FTFMAP.PHMAP[r] = tmpdat[r]; //af::shared
    }
    if (input.REFLECTIONS.FTFMAP.FOM.size() == NREFL)
    {//memory
      for (int r = 0; r < NREFL; r++) tmpdat[r] = input.REFLECTIONS.FTFMAP.FOM[sorted[r].second];
      for (int r = 0; r < NREFL; r++) input.REFLECTIONS.FTFMAP.FOM[r] = tmpdat[r]; //af::shared
    }
    } //memory

/* SPACEGROUP */
    //now do reverse lookup for cctbx
    cctbx::sgtbx::space_group mtzSG;
    for (unsigned isym = 0; isym < mtz->mtzsymm.nsym; isym++)
    {
      scitbx::vec3<floatType> trasym;
      scitbx::mat3<floatType> rotsym;
      for (int i = 0; i < 3; i++)
      {
        trasym[i] = static_cast<floatType>(mtz->mtzsymm.sym[isym][i][3]);
        for (int j = 0; j < 3; j++)
          rotsym(i,j) = static_cast<floatType>(mtz->mtzsymm.sym[isym][j][i]);
      }
      cctbx::sgtbx::rt_mx nextSym(rotsym.transpose(),trasym);
      mtzSG.expand_smx(nextSym);
    }
    std::string mtzHall(mtzSG.type().hall_symbol());
    input.REFLECTIONS.SG_HALL = mtzHall;
    int mtzNumber(mtzSG.type().number());
    results.logUnderLine(SUMMARY,"Read from Mtz File");
    results.logTab(1,SUMMARY,"Data read from mtz file: " + input.HKLIN);
    results.logTabPrintf(1,SUMMARY,"Space-Group Name (Hall Symbol): %s (%s)\n",SpaceGroup(mtzHall).spcgrpname().c_str(),mtzHall.c_str());
    results.logTab(1,SUMMARY,"Space-Group Number: " + itos(mtzNumber));
    results.logTab(1,SUMMARY,"Unit Cell: " + dvtos(input.REFLECTIONS.UNIT_CELL));
    if (input.REFLECTIONS.I_ID != "")
      results.logTab(1,SUMMARY,"Column Labels Selected: " + input.REFLECTIONS.I_ID + " " + input.REFLECTIONS.SIGI_ID);
    else if (input.REFLECTIONS.F_ID != "")
      results.logTab(1,SUMMARY,"Column Labels Selected: " + input.REFLECTIONS.F_ID + " " + input.REFLECTIONS.SIGF_ID);
    else if (input.REFLECTIONS.FTFMAP.FMAP_ID != "")
      results.logTab(1,SUMMARY,"Column Labels Selected: " + input.REFLECTIONS.FTFMAP.FMAP_ID + " " + input.REFLECTIONS.FTFMAP.PHMAP_ID + " " + input.REFLECTIONS.FTFMAP.FOM_ID);
    results.logTabPrintf(1,SUMMARY,"Resolution on Mtz file: %5.2f %5.2f\n",MTZ_HIRES,MTZ_LORES);
    results.logTabPrintf(1,SUMMARY,"Resolution Selected:    %5.2f %5.2f\n",HIRES,LORES);
    results.logTab(1,SUMMARY,"Number of Reflections in Selected Resolution Range: " + itos(input.REFLECTIONS.MILLER.size()));
    results.logBlank(SUMMARY);
    CMtz::MtzFree(mtz);

    bool DEFAULT_CELL = cctbx::uctbx::unit_cell(input.UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
    if (!DEFAULT_CELL)
    {
      input.REFLECTIONS.UNIT_CELL = input.UNIT_CELL;
      results.logTab(1,SUMMARY,"Unit Cell: " + dvtos(input.REFLECTIONS.UNIT_CELL));
      results.logBlank(SUMMARY);
    }

    bool reindexed = input.REFLECTIONS.change_space_group(input.SG_HALL,results);

    results.storeData(input.REFLECTIONS);
    if (input.DO_HKLOUT.True() ||
        (reindexed && results.level(DEBUG))) results.writeMtz(reindexed);
    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

ResultMR_DAT runMR_DAT(InputMR_DAT& input)
{
  Output output;
  output.setPackagePhenix();
  return  runMR_DAT(input,output);
}

} //end namespace phaser
