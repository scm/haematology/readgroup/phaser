//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/main/runPhaser.h>
#include <phaser/src/Molecule.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/eLLG.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rotdist.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/src/DataMR.h>

namespace phaser {

//#define AJM_FUSE_DEBUG  //for deep within loops, very slow
//

//Knuth's combinatorial counting algorithm, avoids overflow
unsigned long long choose(unsigned long long n, unsigned long long k) {
    if (k > n) {
        return 0;
    }
    unsigned long long r = 1;
    for (unsigned long long d = 1; d <= k; ++d) {
        r *= n--;
        r /= d;
    }
    return r;
}

bool runMR_FUSE(InputMR_FUSE& inputAUTO,ResultMR& results)
{
  inputAUTO.Analyse(results);
  PHASER_ASSERT(inputAUTO.MRSET.all_space_groups_the_same());
  //inputAUTO.some_amalgamated = false; //don't reset, multiple runs of runMR_FUSE may end with last being smaller and so not amalgamated, where we want this flag to be true from previous runs
  inputAUTO.none_to_amalgamate = false;
  //all the solutions have the same number of KNOWN components i.e. input rlist KNOWN + 1
  //only test amalgamation of those over TFZ limit
  mr_solution fuse_tf_sol_top(inputAUTO.MRSET.num_rlist()); //just the top inputAUTO.num_remaining
              fuse_tf_sol_top.copy_extras(inputAUTO.MRSET);
  int count(0);
  for (int k = 0; k < inputAUTO.MRSET.size(); k++)
    if (inputAUTO.ZSCORE.over_cutoff(inputAUTO.MRSET[k].TFZ))
      count++;
  int r = std::min(inputAUTO.num_remaining,count);
  int n(r);
  for (n = r; n < count; n++)
  {
    int total_perm(0); //include sum up to r
    for (int k = 1; k <= r; k++)
      total_perm += choose(n,k);
    if (total_perm > DEF_FUSE_MAX)
    {
      n--;
      break;
    }
  }
  for (int k = 0; k < inputAUTO.MRSET.size(); k++)
    if (inputAUTO.ZSCORE.over_cutoff(inputAUTO.MRSET[k].TFZ))
      if (fuse_tf_sol_top.size() < n)
        fuse_tf_sol_top.push_back(inputAUTO.MRSET[k]);

  bool foundSolutions(false);
  int count_perm(0);
  //exlude none to exclude all but one
  int min_exclude = fuse_tf_sol_top.size() - inputAUTO.num_remaining;
  if (min_exclude < 0) min_exclude = 0;
  int max_exclude = fuse_tf_sol_top.size() - 1; //all but one

  results.logHeader(LOGFILE,"MOLECULAR REPLACEMENT AMALGAMATION");
  if (count > fuse_tf_sol_top.size())
    results.logAdvisory(LOGFILE,"Solutions considered for amalgamation were limited to " + itos(n) + ", to restrict combinatorial explosion beyond " + itos(DEF_FUSE_MAX));
  results.logSectionHeader(LOGFILE,"PAIRWISE PACKING");
  results.logTab(1,LOGFILE,"Solutions will be combined to find the largest subset that pack");
  results.logTab(2,LOGFILE,"Number solutions with high TFZ = " + itos(fuse_tf_sol_top.size()));
  results.logTab(2,LOGFILE,"Number ensembles (this type) remaining to be found = " + itos(inputAUTO.num_remaining));
  results.logTab(2,LOGFILE,"Minimum number to be excluded = " + itos(min_exclude));
  results.logTab(2,LOGFILE,"Maximum number to be excluded = " + itos(max_exclude));
  int total_perm(0); //include sum up to r
  for (int k = 1; k <= r; k++)
    total_perm += choose(n,k);
  results.logTab(1,LOGFILE,"There will be " + itos(total_perm) + " combinations");
  if (min_exclude == max_exclude)
  {
    results.logTab(1,LOGFILE,"Only one model to add - no amalgamation");
    inputAUTO.none_to_amalgamate = true;
    results.logBlank(LOGFILE);
  }
  else
  {
    results.logBlank(LOGFILE);
    for (int j = 0; j < fuse_tf_sol_top.size(); j++)
    {
      results.logTab(1,LOGFILE,"SET #" + itos(j+1));
      results.logTab(0,LOGFILE,fuse_tf_sol_top[j].logfile(1,false),false);
    }
    results.logBlank(LOGFILE);

    inputAUTO.smaller_than_best=false;
    bool2D failed_permutation;
    bool1D top_exclude(fuse_tf_sol_top.size(),false);
    results.logUnderLine(LOGFILE,"Pairwise Packing Tests");
    results.logTab(1,LOGFILE,"Finding mutually exclusive combinations");
  //-----------------------runMR_PAK
    mr_matrix_clash CLASH;
    int CUMULATIVE_START = fuse_tf_sol_top[0].KNOWN.size()-1;
    {//pack
      mr_set set_pak_check;
      set_pak_check.HALL = fuse_tf_sol_top[0].HALL; //all space groups the same
      for (int j = 0; j < fuse_tf_sol_top.size(); j++)
        set_pak_check.KNOWN.push_back(fuse_tf_sol_top[j].KNOWN[CUMULATIVE_START]);
      mr_solution tmp;
      tmp.push_back(set_pak_check);
      std::string cards = inputAUTO.Cards();
      InputMR_PAK inputPAK(cards);
      inputPAK.setSOLU(tmp);
      inputPAK.setPACK_QUIC(false);//this was false for a long time - don't know why
      inputPAK.NO_WARNINGS = true;
      if (inputPAK.OUTPUT_LEVEL < TESTING)
        inputPAK.setMUTE(true); //suppress output
      inputPAK.PDB = inputAUTO.PDB; //unparse does not work
      ResultMR all_resultPAK = runMR_PAK(inputPAK,results);
      //results.setOutput(all_resultPAK); //!!! copies mute
      if (all_resultPAK.Failure())
      {
        results.setOutput(all_resultPAK); //!!! copies mute
        return true;
      }
      PHASER_ASSERT(inputPAK.MRSET.size() == 1);
      CLASH = inputPAK.MRSET[0].CLASH; //input passed by reference, MRSET is as input
    }
    std::vector<std::pair<int,int> > failed_packing_pair;
    int countfails(0);
    for (int pi = 0; pi < fuse_tf_sol_top.size(); pi++)
    for (int pj = pi+1 ; pj < fuse_tf_sol_top.size(); pj++)
    {
      std::pair<int,int> bad_pair(pi,pj);
      bool1D included(fuse_tf_sol_top.size(),false);
      included[pi] = true;
      included[pj] = true;
      bool solution_packs = !CLASH.one_bad(included); //checks both ways
      if (!solution_packs) countfails++;
#ifdef AJM_FUSE_DEBUG  //for deep within loops, very slow
      if (!solution_packs)
      {
        failed_packing_pair.push_back(bad_pair);
        results.logTab(1,LOGFILE,"Amalgamated pair combination #" + itos(pi+1) + " with #" + itos(pj+1) + " does not pack ");
      }
      else
        results.logTab(1,LOGFILE,"Amalgamated pair combination #" + itos(pi+1) + " with #" + itos(pj+1) + " packs");
#endif
    }
    if (!failed_packing_pair.size())
      results.logTab(1,LOGFILE,"No pair combinations excluded by packing test");
    else
      results.logTab(1,LOGFILE,itos(countfails) + " pair combination" + std::string(countfails==1?"":"s") + " excluded by packing test");
    results.logBlank(LOGFILE);

    results.logSectionHeader(LOGFILE,"AMALGAMATE SOLUTIONS");
    results.logTab(1,LOGFILE,"Combinations will be skipped if they include either a pairwise packing failure pair or a set of components that have been found to have a decreasing LLG in a previous combination");
  //-----------------------runMR_LLG
  //keep interpolated V and E in memory for speed in combinatorial search
  //use InputMR_RNP
    for (pdbIter iter = inputAUTO.PDB.begin(); iter != inputAUTO.PDB.end(); iter++)
      iter->second.setup_and_fix_vrms(iter->first,inputAUTO.HIRES,inputAUTO.PTNCS,inputAUTO.SOLPAR,results);
    DataMR mr(inputAUTO.REFLECTIONS,
              inputAUTO.RESHARP,
              inputAUTO.SIGMAN,
              inputAUTO.OUTLIER,
              inputAUTO.PTNCS,
              inputAUTO.DATABINS,
              inputAUTO.HIRES,inputAUTO.LORES,
              inputAUTO.COMPOSITION,
              inputAUTO.ensemble);
    mr.selectReso(inputAUTO.MRSET.get_resolution(),inputAUTO.LORES);
    mr.changeSpaceGroup(inputAUTO.MRSET[0].HALL,inputAUTO.MRSET.get_resolution(),inputAUTO.LORES);
    floatType LLwilson = mr.WilsonLL();
    mr.setLLwilson(LLwilson);
    results.setLevel(SILENCE);
    Composition composition(inputAUTO.COMPOSITION);
    mr.ensPtr()->configure(fuse_tf_sol_top[0].set_of_modlids(),inputAUTO.PDB,inputAUTO.SOLPAR,inputAUTO.BOXSCALE,fuse_tf_sol_top.get_resolution(),mr.getBinSigmaN(),inputAUTO.FORMFACTOR,fuse_tf_sol_top[0].set_of_modlids(),results);
    results.setLevel(inputAUTO.OUTPUT_LEVEL);
    { //memory
    mr_set set_llg_check;
    set_llg_check.HALL = inputAUTO.MRSET[0].HALL;
    for (int i = 0; i < CUMULATIVE_START; i++)
      set_llg_check.KNOWN.push_back(fuse_tf_sol_top[0].KNOWN[i]);
    mr.initKnownMR(set_llg_check); //models_known setting
    }
    mr.initSearchMR(); //reset=0
    mr.calcKnownVAR();
    mr.calcKnown();
    float1D interpV;
    cmplx2D interpE;
    float1D initInterpV = mr.getInterpV();
    cmplx1D initInterpE = mr.getInterpE();
    for (int i = 0; i < fuse_tf_sol_top.size(); i++)
    {
      mr_set set_llg_check;
      set_llg_check.HALL = inputAUTO.MRSET[0].HALL;
      set_llg_check.KNOWN.push_back(fuse_tf_sol_top[i].KNOWN[CUMULATIVE_START]);
      mr.initKnownMR(set_llg_check);
      mr.initSearchMR(); //reset=0
      mr.calcKnownVAR();
      mr.calcKnown();
      if (i==0) interpV = mr.getInterpV();
      interpE.push_back(mr.getInterpE());
    }
  //----------------------------
    for (int i = min_exclude; i < max_exclude && !foundSolutions && !inputAUTO.smaller_than_best; i++)
    {
      //make *exclusion* array, so that first permutation includes the top ones
      bool print_combinations(false);
#ifdef AJM_FUSE_DEBUG  //for deep within loops, very slow
      results.logUnderLine(VERBOSE,"Excluding " + itos(i));
      print_combinations = true;
#endif
      bool1D top_exclude(fuse_tf_sol_top.size(),false);
      for (int l = 0; l < i; l++) top_exclude[l] = true; //exclude i
      std::sort(top_exclude.begin(),top_exclude.end()); //run permutations to end
      do {
        count_perm++;
        bool end_this_permutation(false);
        bool failed_packing(false);
        for (int q = 0; q < failed_packing_pair.size(); q++)
        {
          bool found_first(false);
          bool found_second(false);
          for (int x = 0; x < top_exclude.size(); x++)
            if (!found_first && x == failed_packing_pair[q].first && !top_exclude[x])
            {
              found_first = true;
              continue;
            }
            else if (x == failed_packing_pair[q].second && !top_exclude[x])
            {
              found_second = true;
              break;
            }
          if (found_first && found_second)
          {
            end_this_permutation = true;
            failed_packing = true;
            break;
          }
        }
        for (int p = 0; p < failed_permutation.size() && !end_this_permutation; p++)
        {
          bool matches(true);
          for (int x = 0; x < failed_permutation[p].size(); x++)
            if (failed_permutation[p][x] != top_exclude[x])
            {
              matches = false;
              break;
            }
          if (matches && failed_permutation[p].size())
          {
            end_this_permutation = true;
            break;
          }
        }

        if (!end_this_permutation || //accepted
            print_combinations) //false, set true with compiler flag
        {
        outStream where = end_this_permutation ? VERBOSE : LOGFILE;
        results.logUnderLine(where,"Amalgamate Combination #" +itos(count_perm));
        std::string exclude("Exclude: "),include("Include: ");
        for (int j = 0; j < fuse_tf_sol_top.size(); j++)
        {
          (top_exclude[j]) ? exclude += itos(j+1) : include += itos(j+1);
               if (top_exclude[j] && j+1 < 10) include += " ";
          else if (top_exclude[j] && j+1 >= 10) include += "  ";
          else if (!top_exclude[j] && j+1 < 10) exclude += " ";
          else if (!top_exclude[j] && j+1 >= 10) exclude += "  ";
          include += " ";
          exclude += " ";
        }
        results.logTab(1,where,include.c_str());
        results.logTab(1,where,exclude.c_str());
        if (end_this_permutation)
          failed_packing ?
          results.logTab(1,where,"Skipped because of pairwise packing failure"):
          results.logTab(1,where,"Skipped because LLG decreased for first components in previous combination");
        }
        if (end_this_permutation) continue;

        mr_solution sol_amalgamated;
        mr_set set_amalgamated;
        set_amalgamated.HALL = inputAUTO.MRSET[0].HALL;
        bool first_solution(true);
        std::map<int,int> mapa;
        int count(0);
        for (int ii = 0; ii < fuse_tf_sol_top.size(); ii++)
        if (!top_exclude[ii])
        {
          mapa[count] = ii;
          count++;
          if (first_solution)
          {
            set_amalgamated = fuse_tf_sol_top[ii];
            set_amalgamated.ANNOTATION += " (";
            first_solution = false;
          }
          else
          {
            int f = fuse_tf_sol_top[0].KNOWN.size()-1;
            PHASER_ASSERT(f < fuse_tf_sol_top[ii].KNOWN.size());
            set_amalgamated.KNOWN.push_back(fuse_tf_sol_top[ii].KNOWN[f]);
            if (fuse_tf_sol_top[ii].ANNOTATION.rfind("TFZ=") != std::string::npos)
            {
              set_amalgamated.ANNOTATION += "& ";
              set_amalgamated.ANNOTATION.append(fuse_tf_sol_top[ii].ANNOTATION,
                                                fuse_tf_sol_top[ii].ANNOTATION.rfind("TFZ="),
                                                std::string::npos);
              set_amalgamated.ANNOTATION += " ";
            }
          }
        }
        if (set_amalgamated.KNOWN.size() < inputAUTO.best_known_size)
        {
          inputAUTO.smaller_than_best = true; //all future permutations will exclude this many
          results.logTab(1,LOGFILE,"Amalgamated solution has fewer components than current best");
          results.logTab(2,LOGFILE,"All subsequent combinations will also have fewer components than current best");
          results.logTab(1,LOGFILE,"Search terminated");
          results.logBlank(LOGFILE);
        }
        else
        {
          if (!first_solution)
            set_amalgamated.ANNOTATION.erase(set_amalgamated.ANNOTATION.rfind(" "));
          set_amalgamated.ANNOTATION += ")";
          sol_amalgamated.push_back(set_amalgamated);
          results.logBlank(LOGFILE);
        }
        if (!inputAUTO.smaller_than_best)
        {
          mr_solution tmp_pak_llg_sol;
          int CUMULATIVE_FAIL_NUM(0);
          bool solution_packs(false),solution_inc_llg(false);
          {//pack
            bool1D included(fuse_tf_sol_top.size(),false);
            for (int i = 0; i < fuse_tf_sol_top.size(); i++)
              if (!top_exclude[i]) included[i] = true;
            solution_packs = !CLASH.one_bad(included);
            if (solution_packs) tmp_pak_llg_sol = sol_amalgamated;
            (solution_packs) ?
              results.logTab(1,LOGFILE,"Amalgamated solution packs"):
              results.logTab(1,LOGFILE,"Amalgamated solution does not pack");
          }
          if (solution_packs)
          { //llg
  //-----------------runMR_LLG
            {
              mr_set set_llg_check;
              set_llg_check.HALL = inputAUTO.MRSET[0].HALL;
              for (int i = 0; i < CUMULATIVE_START; i++)
                set_llg_check.KNOWN.push_back(set_amalgamated.KNOWN[i]);
              mr_solution tmp;
                          tmp.copy_extras(inputAUTO.MRSET);
              tmp.push_back(set_llg_check);
              mr.initKnownMR(set_llg_check); //models_known setting
              mr.setInterpV(initInterpV);
              mr.setInterpE(initInterpE);
            }
            bool CUMULATIVE_LLG_INCREASES = true;
            std::string CUMULATIVE_LLG_ANNOTATION;
            std::string CUMULATIVE_LLG_HISTORY;
            int llg_fail_num(0);
            floatType last_llg(0);
            bool first_llg(true);
            for (int a = CUMULATIVE_START; a < set_amalgamated.KNOWN.size(); a++)
            {//llg
              mr_set set_llg_check;
              set_llg_check.HALL = inputAUTO.MRSET[0].HALL;
              for (int i = 0; i <= a; i++) //start at beginning, up to a
                set_llg_check.KNOWN.push_back(set_amalgamated.KNOWN[i]);
              mr_solution tmp;
                          tmp.copy_extras(inputAUTO.MRSET);
              tmp.push_back(set_llg_check);
              mr.initKnownMR(set_llg_check); //models_known setting
              mr.addInterpV(interpV);
              PHASER_ASSERT(mapa[a-CUMULATIVE_START] < interpE.size());
              PHASER_ASSERT(mapa[a-CUMULATIVE_START] >= 0);
              mr.addInterpE(interpE[mapa[a-CUMULATIVE_START]]);
              floatType this_llg = mr.likelihoodFn();
              if (first_llg)
              {
                CUMULATIVE_LLG_ANNOTATION += "LLG+=(" + dtos(this_llg,0);
                CUMULATIVE_LLG_HISTORY += "FUSE=(" + itos(mapa[a-CUMULATIVE_START]+1);
              }
              else
              {
                CUMULATIVE_LLG_ANNOTATION += " & " + dtos(this_llg,0);
                CUMULATIVE_LLG_HISTORY += "&" + itos(mapa[a-CUMULATIVE_START]+1);
                if (this_llg < last_llg)
                {
                  CUMULATIVE_LLG_INCREASES = false;
                  llg_fail_num = a;
                  break;
                }
              }
              last_llg = this_llg;
              first_llg = false;
            }//llg
            CUMULATIVE_LLG_ANNOTATION += ")";
            CUMULATIVE_LLG_HISTORY += ") ";
  //-----------------------
            solution_inc_llg = CUMULATIVE_LLG_INCREASES;
            (solution_inc_llg) ?
              results.logTab(1,LOGFILE,"Amalgamated solution increases LLG at each step"):
              results.logTab(1,LOGFILE,"Amalgamated solution does not increase LLG at each step");
            results.logTab(2,LOGFILE,CUMULATIVE_LLG_ANNOTATION.c_str());
            if (solution_inc_llg) tmp_pak_llg_sol = sol_amalgamated;
            if (solution_inc_llg) tmp_pak_llg_sol[0].ANNOTATION += " " + CUMULATIVE_LLG_ANNOTATION;
            if (solution_inc_llg) tmp_pak_llg_sol[0].HISTORY += " " + CUMULATIVE_LLG_HISTORY;
            if (!solution_inc_llg) CUMULATIVE_FAIL_NUM = llg_fail_num;
            if (!solution_inc_llg) results.logTab(2,VERBOSE,"Failure at #" + itos(llg_fail_num));
          } //llg
          foundSolutions = solution_packs && solution_inc_llg;
          if (foundSolutions)
          {
            inputAUTO.some_amalgamated = true;
            inputAUTO.MRSET = tmp_pak_llg_sol;
          }
          else if (CUMULATIVE_FAIL_NUM)//if this permutation fails, don't repeat it
          {
            bool1D this_failed_permutation;
            int start_num = inputAUTO.MRSET[i].KNOWN.size()-1;
            int count(0);
            for (int x = 0; x < top_exclude.size(); x++)
            {
              if (count <= (CUMULATIVE_FAIL_NUM - start_num))
                this_failed_permutation.push_back(top_exclude[x]);
              if (!top_exclude[x]) count++;
            }
            failed_permutation.push_back(this_failed_permutation);

#ifdef AJM_FUSE_DEBUG  //for deep within loops, very slow
            results.logBlank(VERBOSE);
            results.logTab(1,VERBOSE,"Permutations starting as below will be skipped");
            std::string exclude("Exclude: "),include("Include: ");
            for (int x = 0; x < this_failed_permutation.size(); x++)
            {
              (this_failed_permutation[x]) ? exclude += itos(x+1) : include += itos(x+1);
                   if (this_failed_permutation[x] && x+1 < 10) include += " ";
              else if (this_failed_permutation[x] && x+1 >= 10) include += "  ";
              else if (!this_failed_permutation[x] && x+1 < 10) exclude += " ";
              else if (!this_failed_permutation[x] && x+1 >= 10) exclude += "  ";
              include += " ";
              exclude += " ";
            }
            results.logTab(1,VERBOSE,include.c_str());
            results.logTab(1,VERBOSE,exclude.c_str());
            results.logBlank(VERBOSE);
#endif
          }
        }
      } while (!foundSolutions && !inputAUTO.smaller_than_best && next_permutation(top_exclude.begin(),top_exclude.end()));
    }
    results.logBlank(LOGFILE);
    results.logSectionHeader(SUMMARY,"AMALGAMATED SOLUTIONS");
    if (foundSolutions && !inputAUTO.smaller_than_best)
    {
      results.logTab(1,SUMMARY,"A combination of solutions packs");
      results.logTab(2,SUMMARY,"Combination number " + itos(count_perm) + " succeeded");
      results.logBlank(SUMMARY);
      results.logTab(0,SUMMARY,inputAUTO.MRSET.logfile(1,true),false);
    }
    else if (!foundSolutions && !inputAUTO.smaller_than_best)
      results.logTab(1,SUMMARY,"No combination of solutions packs AND increases LLG with addition of each ensemble");
    else
      results.logTab(1,SUMMARY,"Amalgamation is smaller than current best");
    results.logBlank(SUMMARY);
  } //min_exclude != max_exclude
  results.logTrailer(LOGFILE);
  return false;
}

} //end namespace phaser
