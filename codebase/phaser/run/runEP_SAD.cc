//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/main/runPhaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Cluster.h>
#include <phaser/src/Composition.h>
#include <cctbx/eltbx/wavelengths.h>
#include <cctbx/eltbx/sasaki.h>
#include <cctbx/eltbx/fp_fdp.h>
#include <cctbx/eltbx/xray_scattering.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Solvent.h>
#include <phaser/src/Dfactor.h>
#include <phaser/src/ListEditing.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultEP runEP_SAD(InputEP_AUTO& input,Output output)
{
  ResultEP results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  try
  {
    results.logHeader(LOGFILE,header(EP_SAD));
    input.Analyse(results);

    if (!input.CRYS_DATA.is_experiment_type("SAD") &&
        !! !input.CRYS_DATA.is_experiment_type("NAT"))
      throw phaser::PhaserError(phaser::FATAL,"Data not SAD data");
    int max_t = (input.CRYS_DATA[""].ATOM_SET.size());//,1); //may be partial structure
    max_t = std::max(max_t,1);
    (max_t == 1) ?
    results.logTab(1,PROCESS,"There is 1 SAD atom refinement set"):
    results.logTab(1,PROCESS,"There are " + itos(max_t) + " SAD atom refinement sets");
    results.logBlank(SUMMARY);

    data_ep CRYS_DATA = input.CRYS_DATA;
    floatType WILSON_B;
    { //memory
    results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
    SpaceGroup sg(input.SG_HALL);
    UnitCell uc(input.UNIT_CELL);
    //cell content analysis
    Solvent solvent(input.SG_HALL,input.UNIT_CELL,input.COMPOSITION);
    std::string warning = solvent.throwError();
    if (warning.size()) results.logWarning(SUMMARY,warning);
    CRYS_DATA.truncate_resolution(uc.getCctbxUC(),input.HIRES,input.LORES);
    data_refl REFLECTIONS(CRYS_DATA.MILLER,CRYS_DATA().getFmean(),CRYS_DATA().getSIGFmean(),
                          input.SG_HALL,input.UNIT_CELL);
    RefineANO refa(REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    results.logSectionHeader(phaser::LOGFILE,"ABSOLUTE SCALE");
    refa.logScale(phaser::LOGFILE,results);
    af_float correction = refa.getCorrection();
    double WilsonK = refa.absolute_scale_F();
    WILSON_B = refa.getSigmaN().WILSON_B;
    input.OUTLIER = refa.OUTLIER;
    for (int r = 0; r < correction.size(); r++)
    {
      CRYS_DATA().POS.F[r] *= WilsonK*correction[r];
      CRYS_DATA().POS.SIGF[r] *= WilsonK*correction[r];
      CRYS_DATA().NEG.F[r] *= WilsonK*correction[r];
      CRYS_DATA().NEG.SIGF[r] *= WilsonK*correction[r];
    }
    }

    if (input.PTNCS.USE)
    { //memory
    data_refl REFLECTIONS(CRYS_DATA.MILLER,CRYS_DATA().getFmean(),CRYS_DATA().getSIGFmean(),
                          input.SG_HALL,input.UNIT_CELL);
    Epsilon epsilon(REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
    {
      epsilon.findROT(input.MACRO_TNCS,results);
      epsilon.logEpsn(results);
    }
    input.PTNCS = epsilon.getPtNcs();
    }

    float1D WILSON_LLG;
    //get the atoms in the right form
    //set the B-factors of the scatterers

    std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES;
    stringset ALL_ATOMTYPES;
    std::map<std::string,bool> fix_fdp;
    { //memory
    if (input.PARTIAL.filename().size() && !input.PARTIAL.map_format())
    {
      Molecule pdb(input.PARTIAL,false);
      if (pdb.read_errors() != "") throw PhaserError(FATAL,pdb.read_errors());
      ALL_ATOMTYPES = pdb.getAnomTypes();
    }
    //collect the atomtypes starting with the set from the llgc
    //from the list of scatterers
    for (unsigned t = 0 ; t < input.CRYS_DATA[""].ATOM_SET.size(); t++)
    for (unsigned a = 0 ; a < input.CRYS_DATA[""].ATOM_SET[t].size(); a++)
      ALL_ATOMTYPES.insert(input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.scattering_type);

    //if no llg completion atomtypes have been set, use all the atomtypes in the phasing list
    if (!input.LLGCOMPLETE.ATOMTYPE.size()) input.LLGCOMPLETE.ATOMTYPE = ALL_ATOMTYPES;
    else ALL_ATOMTYPES.insert(input.LLGCOMPLETE.ATOMTYPE.begin(),input.LLGCOMPLETE.ATOMTYPE.end());
    //if we are letting the input atom types change add the original to the completion types
    if (input.ATOM_CHANGE_ORIG)
      input.LLGCOMPLETE.ATOMTYPE.insert(ALL_ATOMTYPES.begin(),ALL_ATOMTYPES.end());

    if (input.LLGCOMPLETE.METHOD == "IMAGINARY") ALL_ATOMTYPES.insert(std::string("AX"));
    //find f-prime and f-double-prime
    floatType dwave = 0.02; //wavelength step for near-edge test
    floatType dfdp = 0.2; //fractional change in fdp that determines edge
    std::string Sasaki_wavelength_error_list,Sasaki_error_list;
    for (stringset::iterator iter = ALL_ATOMTYPES.begin(); iter != ALL_ATOMTYPES.end(); iter++)
    {
      std::string atomtype = *iter;
      if (atomtype == "RX") ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(1.0,0.0);
      else if (atomtype == "AX") ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(0.0,1.0);
      else if (input.SCATTERING.ATOMTYPE.find(atomtype) != input.SCATTERING.ATOMTYPE.end())
        ATOMTYPES[atomtype] = input.SCATTERING.ATOMTYPE[atomtype];
      else
      {
        std::string a = (atomtype == "TX") ? "Ta" : atomtype; //Ta6Br12
        cctbx::eltbx::sasaki::table sasaki(a,false,false);
        if (sasaki.is_valid())
        {
          if (!sasaki.at_angstrom(input.WAVELENGTH).is_valid())
          {
            Sasaki_wavelength_error_list += "\"" + atomtype + "\" ";
            continue;
          }
          bool near_edge = false;
          {
            floatType fdp0 = sasaki.at_angstrom(input.WAVELENGTH).fdp();
            floatType fdpf = sasaki.at_angstrom(input.WAVELENGTH+dwave).fdp();
            floatType fdpb = sasaki.at_angstrom(input.WAVELENGTH-dwave).fdp();
            near_edge = std::fabs(fdpf-fdp0)/fdp0 > dfdp || std::fabs(fdpb-fdp0)/fdp0 > dfdp;
          }
          if (near_edge) // When f' and f" not given, assume wavelength near edge should be at edge.
          {
            floatType peakwave(0.),peakfdp(-1000.);
            for (int i = -100; i <= 100; i++)
            {
              floatType testwave = input.WAVELENGTH+i*dwave/100.;
              floatType testfdp = sasaki.at_angstrom(testwave).fdp();
              if (testfdp > peakfdp)
              { peakfdp = testfdp; peakwave = testwave; }
            }
            ATOMTYPES[atomtype] = sasaki.at_angstrom(peakwave);
          }
          else ATOMTYPES[atomtype] = sasaki.at_angstrom(input.WAVELENGTH);
        }
        else
        {
          Sasaki_error_list += "\"" + atomtype + "\" ";
          continue;
        }
      }
    }
    if (Sasaki_wavelength_error_list.size())
    throw PhaserError(FATAL,"Wavelength outside range of Sasaki tables.\nUse SCATTERING to specify f' and f\" for atom(s) " + Sasaki_wavelength_error_list); //should have been caught in input
    if (Sasaki_error_list.size())
    throw PhaserError(FATAL,"Atom type(s) " + Sasaki_error_list + " not in Sasaki table.\nUse SCATTERING to specify f' and f\""); //should have been caught in input
    results.logSectionHeader(LOGFILE,"ANOMALOUS SCATTERING");
    results.logUnderLine(SUMMARY,"Scattering edges");
    results.logTab(1,SUMMARY,"Wavelength = " +  dtos(input.WAVELENGTH));
    for (stringset::iterator iter = ALL_ATOMTYPES.begin(); iter != ALL_ATOMTYPES.end(); iter++)
    {
      std::string atomtype = *iter;
      cctbx::eltbx::sasaki::table sasaki(atomtype,false,false);
      bool near_edge = false;
      if (sasaki.is_valid()) //not cluster, RX or AX
      {
        floatType fdp0 = sasaki.at_angstrom(input.WAVELENGTH).fdp();
        floatType fdpf = sasaki.at_angstrom(input.WAVELENGTH+dwave).fdp();
        floatType fdpb = sasaki.at_angstrom(input.WAVELENGTH-dwave).fdp();
        near_edge = std::fabs(fdpf-fdp0)/fdp0 > dfdp || std::fabs(fdpb-fdp0)/fdp0 > dfdp;
      }
      fix_fdp[atomtype] = !near_edge;
      if (atomtype == "RX" || atomtype == "AX") continue; //do not print
      near_edge ? results.logTab(1,SUMMARY,"Atomtype \"" + atomtype + "\" is near an edge"):
                  results.logTab(1,SUMMARY,"Atomtype \"" + atomtype + "\" is not near an edge");
      //override with input if set and not EDGE
      if (input.SCATTERING.getFixFdp(atomtype) == "ON")  fix_fdp[atomtype] = true;
      if (input.SCATTERING.getFixFdp(atomtype) == "OFF") fix_fdp[atomtype] = false;
      fix_fdp[atomtype] ?
        results.logTab(2,SUMMARY,"Atomtype \"" + atomtype + "\" fdp is fixed") :
        results.logTab(2,SUMMARY,"Atomtype \"" + atomtype + "\" fdp is refined");
    }
    results.logBlank(SUMMARY);
    } //memory

    if (!input.CRYS_DATA().POS.Feff.size() && !input.CRYS_DATA().POS.no_data())
    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    data_refl pos(CRYS_DATA.MILLER,CRYS_DATA().POS,input.SG_HALL,input.UNIT_CELL);
    Dfactor dfactor(pos,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    CRYS_DATA().POS = dfactor;
    }
    if (!input.CRYS_DATA().NEG.Feff.size() && !input.CRYS_DATA().NEG.no_data())
    { //memory
    data_refl neg(CRYS_DATA.MILLER,CRYS_DATA().NEG,input.SG_HALL,input.UNIT_CELL);
    Dfactor dfactor(neg,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    CRYS_DATA().NEG = dfactor;
    }

    if (true)
    {
      results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
      results.logTab(1,VERBOSE,"Wilson B = " + dtos(WILSON_B));
      results.setLevel(input.OUTPUT_LEVEL == DEBUG ? DEBUG : SUMMARY);
      af_atom NULLSET;
      std::map<std::string,cctbx::eltbx::fp_fdp> NULLATOMTYPES;
      RefineSAD sad(input.SG_HALL,input.UNIT_CELL,CRYS_DATA.MILLER,
                    CRYS_DATA().POS,CRYS_DATA().NEG,
                    input.PTNCS,
                    NULLSET,
                    input.CLUSTER_PDB,
                    input.DATABINS,
                    NULLATOMTYPES,
                    input.OUTLIER,
                    input.BFAC_WILS,WILSON_B,
                    input.BFAC_SPHE,
                    input.SCATTERING.FDP,
                    input.PARTIAL,
                    input.FFTvDIRECT,
                    WILSON_LLG,
                    results);
      phaser::MACS iMACS;
      iMACS.setMACS_DEFAULT_PROTOCOL();
      Minimizer minimizer;
      minimizer.run(sad,iMACS.MACRO_SAD,results);
      results.setLevel(input.OUTPUT_LEVEL);
      results.logTab(1,LOGFILE,"LLG for null hypothesis = " + dtos(sad.wilsonFn(WILSON_LLG)));
      sad.logSigmaA(VERBOSE,results);
      results.logBlank(LOGFILE);
    }

    results.logSectionHeader(SUMMARY,"ENANTIOMORPH");
    if (input.HAND_CHANGE.is_off())
      results.logTab(1,PROCESS,"Requested refinement in original enantiomorph only");
    else if (input.HAND_CHANGE.is_on())
      results.logTab(1,PROCESS,"Requested refinement in other enantiomorph only");
    else if (input.HAND_CHANGE.is_both())
      results.logTab(1,PROCESS,"Requested refinement in both enantiomorphs");
    if ((input.HAND_CHANGE.is_on() || input.HAND_CHANGE.is_both()) && input.PARTIAL.filename() != "")
    {
      results.logTab(1,PROCESS,"Cannot change enantiomorph with partial structure");
      input.HAND_CHANGE.set("OFF");
    }
    std::string spacegroup_hall(input.SG_HALL);
    cctbx::sgtbx::space_group sg(input.SG_HALL,"A1983");
    cctbx::sgtbx::space_group_type sg_type(sg);
    cctbx::sgtbx::change_of_basis_op cb_op = sg_type.change_of_hand_op();
    cctbx::sgtbx::space_group enantiomorph_sg = sg.change_basis(cb_op);
    std::string enantiomorph_hall = enantiomorph_sg.type().hall_symbol();
    if (input.HAND_CHANGE.is_on() || input.HAND_CHANGE.is_both())
      (sg_type.is_enantiomorphic()) ? results.logTab(1,PROCESS,"Enantiomorph change changes spacegroup"):
                                      results.logTab(1,PROCESS,"Enantiomorph change does not change spacegroup");
    results.logBlank(SUMMARY);

    af_bool centric;
    //int best_Rfac_t = 0;
    //int best_Rfac_second_hand = 0;
    floatType best_Rfac = 100000;
    for (int t = 0; t < max_t; t++)
    {
    if (t < input.CRYS_DATA[""].ATOM_SET.size()) //partial only
      results.logSectionHeader(SUMMARY,"ATOM SET #" + itos(t+1) + " OF " + itos(max_t));
    else results.logSectionHeader(SUMMARY,"PARTIAL STRUCTURE #" + itos(t+1) + " OF " + itos(max_t));

    UnitCell UC(input.UNIT_CELL);
    if (t < input.CRYS_DATA[""].ATOM_SET.size())
    for (unsigned a = 0; a < input.CRYS_DATA[""].ATOM_SET[t].size(); a++)
    {
      input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.fp = ATOMTYPES[input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.scattering_type].fp();
      input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.fdp = ATOMTYPES[input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.scattering_type].fdp();
      if (!input.CRYS_DATA[""].ATOM_SET[t][a].FRAC)
      {
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.site = UC.doOrth2Frac(input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.site);
        input.CRYS_DATA[""].ATOM_SET[t][a].FRAC = true;
      }
      if (!input.CRYS_DATA[""].ATOM_SET[t][a].USTAR && input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.flags.use_u_aniso_only())
      {
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_star /= u_cart_pdb_factor;
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_star = cctbx::adptbx::u_cart_as_u_star(UC.getCctbxUC(),input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_star);
      }
      if (input.ATOM_CHANGE_BFAC_WILSON)
      {
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.set_use_u(/* iso */ true, /* aniso */ false);
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_iso = cctbx::adptbx::b_as_u(std::max(WILSON_B,DEF_EP_BMIN));
      }
      else if (!input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.flags.use_u_aniso_only() && input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_iso == 0)
      {
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_iso = cctbx::adptbx::b_as_u(std::max(WILSON_B,DEF_EP_BMIN));
      }
      else if (input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.flags.use_u_aniso_only())
      {
        floatType WILSON_U = cctbx::adptbx::b_as_u(std::max(WILSON_B,DEF_EP_BMIN));
        cctbx::adptbx::factor_u_star_u_iso<floatType> factor(UC.getCctbxUC(),input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_star);
        if (factor.u_iso == 0)
          input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.u_star = cctbx::adptbx::u_iso_as_u_star(UC.getCctbxUC(),WILSON_U);
      }
      if (input.CRYS_DATA[""].ATOM_SET[t][a].BSWAP)
      {
        input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.flags.use_u_aniso_only() ?
          input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.convert_to_isotropic(UC.getCctbxUC()) :
          input.CRYS_DATA[""].ATOM_SET[t][a].SCAT.convert_to_anisotropic(UC.getCctbxUC());
      }
    }

    af_atom SADSET;
    //manual deep copy
    if (t < input.CRYS_DATA[""].ATOM_SET.size())
      SADSET  = input.CRYS_DATA[""].ATOM_SET[t];
    VarianceEP SADVAR = input.VAR_EP;

    bool second_hand(false),refine_hand(false);
    for (int i = 0; i < input.MACRO_SAD.size(); i++)
      if (input.MACRO_SAD[i]->getNCYC()) refine_hand = true;
    start_second_hand:

    if (second_hand)
    {
      results.logSectionHeader(SUMMARY,"ATOM SET #" + itos(t+1) + " OF " + itos(max_t) + ": ENANTIOMORPH");
      if (!refine_hand)
      {
        results.logTab(1,PROCESS,"Only one anomalous scattering type");
        results.logTab(1,PROCESS,"Only phasing (not refinement) required in second enantiomorph");
      }
      results.logBlank(SUMMARY);
    }
    if (input.HAND_CHANGE.is_on() || second_hand)
    {
      spacegroup_hall = enantiomorph_hall;
      for (int a = 0; a < SADSET.size(); a++)
      {
        if (a == 0)
        {
          results.logTab(1,SUMMARY,"Sites before and after enantiomorph changed");
          results.logTabPrintf(1,SUMMARY,"     %-6s %-6s %-6s -> %-6s %-6s %-6s   %-s\n",
                 "X","Y","Z","X","Y","Z","Atomtype");
        }
        dvect3 frac = SADSET[a].SCAT.site;
        dvect3 orth =  UC.doFrac2Orth(frac);
        cctbx::fractional<floatType> sg_x(frac);
        dvect3 frac_hand = cb_op(sg_x);
        dvect3 orth_hand = UC.doFrac2Orth(frac_hand);
        SADSET[a].SCAT.site = frac_hand;
        if (!SADSET[a].REJECTED)
        results.logTabPrintf(1,SUMMARY,"#%-3d % 6.2f % 6.2f % 6.2f -> % 6.2f % 6.2f % 6.2f   %-s\n",
          a+1,orth[0],orth[1],orth[2],orth_hand[0],orth_hand[1],orth_hand[2],
              SADSET[a].SCAT.scattering_type.c_str());
      }
      results.logBlank(SUMMARY);
    }
    SpaceGroup(spacegroup_hall).logSymm(SUMMARY,results);
    //UC.logUnitCell(SUMMARY,results);
    results.logBlank(SUMMARY);

    if (refine_hand)
      results.logUnderLine(SUMMARY,"INITIAL PHASING STATISTICS");
    else
      results.logUnderLine(SUMMARY,"PHASING STATISTICS");

    //check scattering
    RefineSAD sad(spacegroup_hall,input.UNIT_CELL,CRYS_DATA.MILLER,
                  CRYS_DATA().POS,CRYS_DATA().NEG,
                  input.PTNCS,
                  SADSET,
                  input.CLUSTER_PDB,
                  input.DATABINS,
                  ATOMTYPES,
                  input.OUTLIER,
                  input.BFAC_WILS,WILSON_B,
                  input.BFAC_SPHE,
                  input.SCATTERING.FDP,
                  input.PARTIAL,
                  input.FFTvDIRECT,
                  WILSON_LLG,
                  results);

    if (t < input.CRYS_DATA[""].ATOM_SET.size()) //partial only
      if (refine_hand) sad.setAtomsInput(input.CRYS_DATA[""].ATOM_SET[t]);
    bool INTE_FIXED(false); //for debugging
    int  INTE_STEPS(1); //for debugging
    if (INTE_FIXED) sad.setupIntegrationPoints(INTE_STEPS);
    sad.setVarianceEP(SADVAR);
    sad.calcSigmaaData();
    sad.setFixFdp(fix_fdp);

    sad.logDataStats(VERBOSE,20,results);

    PHASER_ASSERT(input.MACRO_SAD.size());
    sad.rejectOutliers(LOGFILE,results);
    sad.logAtoms(SUMMARY,results);
    sad.logScale(LOGFILE,results);
    sad.logScat(SUMMARY,results);
    sad.logFOM(SUMMARY,results, false);
    results.logTab(1,SUMMARY,"Log-Likelihood = " + itos(-sad.getHandEP().getLogLikelihood()));
    results.logTab(1,SUMMARY,"R-factor = "+ dtos(sad.Rfactor(),5,1));
    results.logBlank(SUMMARY);
    sad.calcBinStats();
    sad.calcPhsStats(results);
    stringset ax;
    ax.insert(std::string("AX"));
    bool ptncs_dbl_sites = input.PTNCS.use_and_present();
    if (input.DO_LLGMAPS)
      (input.LLGCOMPLETE.METHOD == "IMAGINARY") ? sad.calcGradCoefMaps(ax,ptncs_dbl_sites) : sad.calcGradCoefMaps(input.LLGCOMPLETE.ATOMTYPE,ptncs_dbl_sites);

    for (unsigned p = 0 ; p < input.MACRO_SAD.size(); p++)
    {
      results.logTab(1,LOGFILE,"Protocol cycle #"+ itos(p+1) + " of " + itos(input.MACRO_SAD.size()));
      results.logTabPrintf(0,LOGFILE,"%s",input.MACRO_SAD[p]->logfile().c_str());
      results.logBlank(LOGFILE);
    }

    if (refine_hand)
    {
      bool atoms_not_complete(true);
      int ncyc(0);
      bool low_rfactor(false);
      do {
        ncyc++;
        results.logUnderLine(SUMMARY,"REFINEMENT #" + itos(ncyc));
        Minimizer minimizer;
        minimizer.run(sad,input.MACRO_SAD,results);

        sad.calcBinStats();
        results.logTab(1,SUMMARY,"Log-Likelihood = " + itos(-sad.getHandEP().getLogLikelihood()));
        floatType Rfac = sad.Rfactor();
        results.logTab(1,SUMMARY,"R-factor = "+ dtos(Rfac,5,1));
        results.logBlank(SUMMARY);
        low_rfactor = (input.PARTIAL.filename() == "" //ignore if there is a partial structure
                       && input.RFAC_USE && Rfac < input.RFAC_CUTOFF);
        if (low_rfactor)
          results.logTab(1,PROCESS,"LLG completion terminated because R-factor less than " + dtos(input.RFAC_CUTOFF,4,1));
        if (Rfac < best_Rfac)
        {
          results.setTopRfac(Rfac);
         // best_Rfac_t = t;
          best_Rfac = Rfac;
          //best_Rfac_second_hand = second_hand ? 1 : 0; //hand integer
        }

        if (ncyc <= input.LLGCOMPLETE.NCYC && !low_rfactor)
        {
          outStream where(LOGFILE);
          LlgcMapSites new_sites;
          //optical resolution 0.715*resolution
          floatType max_bond_dist(10.0),optical_res(0.715*sad.HiRes());
          floatType separation_dist = input.LLGCOMPLETE.CLASH ? input.LLGCOMPLETE.CLASH : std::min(max_bond_dist,optical_res);
          if (input.LLGCOMPLETE.METHOD == "IMAGINARY")
          {
            results.logUnderLine(SUMMARY,"SUB-STRUCTURE ANALYSIS #" + itos(ncyc) + ": LLG MAPS");
            results.logTab(1,PROCESS,"1 gradient map will be calculated");
            results.logTab(2,PROCESS,"for atom type: AX");
            std::string ATOMTYPE = "AX";
            results.logUnderLine(where,"Log-Likelihood Gradient Map (" + ATOMTYPE + ")");
            results.logBlank(where);
            sad.calcGradCoefs(ATOMTYPE,ptncs_dbl_sites);
            LlgcMapSites llgcmapsites = sad.calcGradMaps(where,ATOMTYPE,separation_dist,input.LLGCOMPLETE,ncyc,ptncs_dbl_sites,results);
            new_sites.merge(llgcmapsites);
          }
          else //if (input.LLGCOMPLETE.METHOD == "ATOMTYPE")
          {
            if (input.LLGCOMPLETE.ATOMTYPE.size())
            {
              results.logUnderLine(SUMMARY,"SUB-STRUCTURE ANALYSIS #" + itos(ncyc) + ": LLG MAPS");
              input.LLGCOMPLETE.ATOMTYPE.size() == 1 ?
              results.logTab(1,PROCESS,"1 gradient map will be calculated"):
              results.logTab(1,PROCESS,itos(input.LLGCOMPLETE.ATOMTYPE.size()) + " gradient maps will be calculated");
              for (stringset::iterator iter = input.LLGCOMPLETE.ATOMTYPE.begin(); iter != input.LLGCOMPLETE.ATOMTYPE.end(); iter++)
                results.logTab(2,PROCESS,"for atom type: "+ (*iter));
              results.logBlank(SUMMARY);
            }
            for (stringset::iterator iter = input.LLGCOMPLETE.ATOMTYPE.begin(); iter != input.LLGCOMPLETE.ATOMTYPE.end(); iter++)
            {
              std::string ATOMTYPE = (*iter);
              results.logUnderLine(where,"Log-Likelihood Gradient Map (" + ATOMTYPE + ")");
              results.logBlank(where);
              sad.calcGradCoefs(ATOMTYPE,ptncs_dbl_sites);
              LlgcMapSites llgcmapsites = sad.calcGradMaps(where,ATOMTYPE,separation_dist,input.LLGCOMPLETE,ncyc,ptncs_dbl_sites,results);
              new_sites.merge(llgcmapsites);
            }
          }
          results.logUnderLine(SUMMARY,"SUB-STRUCTURE ANALYSIS #" + itos(ncyc) + ": WEAK SITES");
          changed_list changed = sad.findChangedAtoms(input.LLGCOMPLETE.ATOMTYPE,input.LLGCOMPLETE.METHOD,input.ATOM_CHANGE_ORIG,where,results);
          int nchanged = changed.size();
          std::set<int> deleted_list = sad.findDeletedAtoms(SUMMARY,results,changed);
          int ndeleted = deleted_list.size();
          where = SUMMARY;
          results.logUnderLine(where,"SUB-STRUCTURE COMPLETION #" + itos(ncyc));
          if (input.LLGCOMPLETE.COMPLETE)
          {
            results.logTab(1,PROCESS,"Substructure completion requested");
            if (new_sites.nrestored())
              sad.restoreAtoms(where,new_sites.nrestored(),new_sites.restored,ncyc,results);
            if (new_sites.nanisotropic())
              sad.anisoAtoms(where,new_sites.nanisotropic(),new_sites.anisotropic,ncyc,results);
            if (ndeleted)
              sad.deleteAtoms(where,ndeleted,deleted_list,ncyc,results);
            if (nchanged)
              sad.changeAtoms(where,changed,results);
            if (new_sites.natoms()) //must do this at end because numbering of atoms changes
            {
              new_sites.selectTopSites(where,sad,sad,separation_dist,sad.numAtoms(),input.LLGCOMPLETE.TOP,results);
              sad.addAtoms(new_sites.atoms,new_sites.ptncs_pairs,results);
            }
            atoms_not_complete = (new_sites.nchanged() || ndeleted || nchanged);
            //if there is ptncs repeat the completion without doubling the sites (singlets only)
            if (!atoms_not_complete && ptncs_dbl_sites)
            {
              atoms_not_complete = true;
              ptncs_dbl_sites = false;
              if (input.PTNCS.EP.PAIR_ONLY) atoms_not_complete = false;
              results.logTab(1,PROCESS,"Search for substructure of TNCS related pairs of atoms has converged");
              if (!input.PTNCS.EP.PAIR_ONLY)
                results.logTab(1,PROCESS,"Search for substructure for individual atoms");
            }
            if (!atoms_not_complete)
              results.logTab(1,PROCESS,"Substructure completion has converged");
            else
              results.logTab(1,PROCESS,"Substructure completion has NOT converged");
            results.logBlank(where);
          }
          else
          {
            results.logTab(1,PROCESS,"Substructure completion not requested");
            results.logBlank(where);
            //add new sites so they are results
            for (int a = 0; a < new_sites.atoms.size(); a++)
            {
              new_sites.atoms[a].REJECTED = true;
              new_sites.atoms[a].SCAT.occupancy = 0; //only way to be ignored in fft
            }
            sad.addAtoms(new_sites.atoms,new_sites.ptncs_pairs,results);
            atoms_not_complete = false;
          }
        }
      }
      while (atoms_not_complete && ncyc < input.LLGCOMPLETE.NCYC+1 && !low_rfactor); //do one more cycle than NCYC, without LLG completion

      sad.calcBinStats();
      sad.calcPhsStats(results);
      if (input.DO_LLGMAPS)
        (input.LLGCOMPLETE.METHOD == "IMAGINARY") ? sad.calcGradCoefMaps(ax,ptncs_dbl_sites) : sad.calcGradCoefMaps(input.LLGCOMPLETE.ATOMTYPE,ptncs_dbl_sites);
      results.logUnderLine(SUMMARY,"FINAL PHASING STATISTICS");
      sad.logSigmaA(LOGFILE,results);
      sad.logOutliers(LOGFILE,results);
      sad.logScale(LOGFILE,results);
      sad.logAtoms(SUMMARY,results);
      sad.logScat(SUMMARY,results);
      sad.logFOM(SUMMARY,results);
      results.logTab(1,SUMMARY,"Final Log-Likelihood = " + dtos(-sad.getHandEP().getLogLikelihood()));
      floatType Rfac = sad.Rfactor();
      results.logTab(1,SUMMARY,"Final R-factor = "+ dtos(Rfac,5,1));
      results.logBlank(SUMMARY);
    }

    if (!second_hand)
    {
      results.setUnitCell(sad.getUnitCell());
      results.setMiller(sad.miller);
    }
    results.addDataEP(second_hand,sad.getSpaceGroup(),sad.getHandEP());

    if (input.HAND_CHANGE.is_both() && //user wants both
        input.PARTIAL.filename() == "" && //spacegroup has not been fixed by MR solution
        !second_hand) //second hand not yet done
    {
      second_hand = true;
      if (ATOMTYPES.size() > 1)
      {
        refine_hand = true;
      }
      else
      {
        refine_hand = false;
        SADVAR = sad;
        SADSET = sad.atoms;
        for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator iter = ATOMTYPES.begin(); iter != ATOMTYPES.end(); iter++)
        {
          std::string atomtype = iter->first;
          int t = sad.atomtype2t[atomtype];
          ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(sad.AtomFp[t],sad.AtomFdp[t]);
        }
      }
      goto start_second_hand;
    }

    if (input.DO_HKLOUT.True())
      if (results.level(DEBUG)) //use the RefineSAD writeMtzExtra function
      {
        int nhands = results.hasSecondHand() ? 2 : 1;
        std::string hklout = (nhands == 1) ? input.FILEROOT+"_debug.mtz" : input.FILEROOT+"_debug.hand.mtz";
        if (nhands == 2) results.logWarning(SUMMARY,"DEBUG MTZ FILE IN LAST HAND ONLY\nFile: " + hklout);
        sad.writeMtzdebug(hklout,input.CRYS_DATA().POS.F_ID,input.HKLIN,results);
      }
    centric = sad.getCentric();

    }

    {//memory
    if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
    results.logSectionHeader(SUMMARY,"OUTPUT FILES");
    results.sort();
    int nhands = results.hasSecondHand() ? 2 : 1;
    results.logTab(1,SUMMARY,"Output for " + itos(input.NUM_TOPFILES) + " topfiles requested");
    std::pair<int,int> maxh = results.getTop();
    for (int hand = maxh.second; hand<nhands && hand>=0; maxh.second? hand--: hand++)
    {
      (nhands == 2) ?
        results.logTab(1,SUMMARY,std::string(hand ? "Hand #2 of 2" : "Hand #1 of 2")):
        results.logTab(1,SUMMARY,"Hand #1 of 1");
      for (int t = 0; t < input.NUM_TOPFILES && t < results.getNum(hand); t++)
      {
        results.logTab(2,SUMMARY,"Atom Set #" + itos(t+1) + " of " + itos(max_t));
        if (results.isPackageCCP4() && input.DO_KEYWORDS.True())
          results.writeScript(hand,t,input.TITLE,input.CRYS_DATA().XTALID); //not ATOM_XTALID
        if (input.DO_XYZOUT.True())
          results.writePdb(hand,t,input.TITLE);
        if (input.DO_HKLOUT.True())
        {
          input.HKLIN.size() ?
          results.writeMtz(hand,t,
                           input.CRYS_DATA().RefID,
                           input.HKLIN,
                           input.WAVELENGTH):
          results.writeMtz(hand,t,
                           CRYS_DATA().XTALID,
                           CRYS_DATA().WAVEID,
                           CRYS_DATA().POS,
                           CRYS_DATA().NEG,
                           input.WAVELENGTH);
          if (input.DO_LLGMAPS)
            results.writeMap(hand,t,input.CRYS_DATA().POS.F_ID,input.HKLIN);
        }
        if (results.getSolFile(hand,t).size())
          results.logTab(3,SUMMARY,"Sites written to PDB file:  " + results.getPdbFile(hand,t));
        if (results.getMtzFile(hand,t).size())
          results.logTab(3,SUMMARY,"Phases written to MTZ file:  " + results.getMtzFile(hand,t));
        if (results.getMapFile(hand,t).size())
          results.logTab(3,SUMMARY,"LLG map coefficients written to MTZ file:  " + results.getMapFile(hand,t));
        if (results.getSolFile(hand,t).size())
          results.logTab(3,SUMMARY,"Script written to SOL file:  " + results.getSolFile(hand,t));
      }
      if (!results.getFilenames().size())
        results.logTab(1,SUMMARY,"No files output");
      results.logBlank(SUMMARY);
    }
    } //memory

    {
    results.logSectionHeader(LOGFILE,"RESULTS");
    if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:SAD Result: $$ Baubles Markup $$");
    int nhands = results.hasSecondHand() ? 2 : 1;
    stringset allatoms;
    for (int hand = 0; hand < nhands; hand++)
    for (int t = 0; t < results.getNum(hand); t++)
    {
      map_str_int mapatoms = results.getAtomNum(hand,t);
      for (map_str_int::iterator iter = mapatoms.begin(); iter != mapatoms.end(); iter++)
        allatoms.insert(iter->first);
    }
    std::string atomtypes; //sorted order
    for (stringset::iterator iter = allatoms.begin(); iter != allatoms.end(); iter++)
    {
      atomtypes += (*iter);
      if (iter->size() == 1) atomtypes += " ";
      atomtypes += "   ";
    }
    for (int table = 0; table < 2; table++)
    {
      std::string type = table ? "(Sorted)" : "(Unsorted)";
      table ? results.sort() : results.unsort();
      results.logUnderLine(SUMMARY,"SAD Refinement Table " + type);
      results.logTabPrintf(1,SUMMARY,"%-4s %c  %-10s %5s %-14s %4s  Composition\n","SET",' ',"SpaceGroup"," FOM ","Log-Likelihood","Rfac",atomtypes.c_str());
      results.logTabPrintf(1,SUMMARY,"       %-10s %5s %-14s %4s   %s\n","","","","",atomtypes.c_str());
      for (int hand = 0; hand < nhands; hand++)
      {
        for (int t = 0; t < results.getNum(hand); t++)
        {
          if (hand && !t) results.logTab(1,SUMMARY,"*=hand");
          std::string atomcount;
          map_str_int mapatoms = results.getAtomNum(hand,t);
          for (stringset::iterator iter = allatoms.begin(); iter != allatoms.end(); iter++)
            atomcount += (mapatoms.find(*iter) != mapatoms.end()) ? itos(mapatoms[*iter],5) : "0    ";
          results.logTabPrintf(1,SUMMARY,"#%3d %c  %-10s %5.3f %14.0f %4.1f  %-s\n",
            results.getHand(hand,t).atoms.NUM,
            (hand ? '*':' '),
            results.getSpaceGroupName(hand).c_str(),
            results.stats_fom(hand,t),
            -results.getHand(hand,t).getLogLikelihood(),
            results.getHand(hand,t).atoms.RFAC,
            atomcount.c_str());
        }
        results.logTab(1,SUMMARY,"-------");
      }
      results.logBlank(SUMMARY);
    }
    results.unsort(); //make sure exit is in input order
    }//memory

    if (input.OUTPUT_LEVEL == TESTING)
    {
      int nhands = results.hasSecondHand() ? 2 : 1;
      results.logUnderLine(TESTING,"Find Centrosymmetry");
      for (int hand = 0; hand < nhands; hand++)
      {
        ep_solution SADSET = results.getDotSol(hand);
        ListEditing edit;
        double centrosymmetry_distance = 3.0; //phenix.emma default
        results.logTab(1,TESTING,"Centrosymmetry distance = " + dtos(centrosymmetry_distance,2));
        edit.find_centrosymmetry(centrosymmetry_distance,input.SG_HALL,input.UNIT_CELL,SADSET,&results);
        for (int k = 0; k < SADSET.size(); k++)
        {
          results.logUnderLine(TESTING,"SET #"+itos(k+1)+" of "+itos(SADSET.size()) + " HAND" + std::string(hand?"+":"-"));
          results.logTabPrintf(0,TESTING,"%-s",SADSET[k].unparse(input.CRYS_DATA().XTALID,true,1).c_str());
          results.logBlank(TESTING);
          RefineSAD sad(input.SG_HALL,input.UNIT_CELL,CRYS_DATA.MILLER,
                        CRYS_DATA().POS,CRYS_DATA().NEG,
                        input.PTNCS,
                        SADSET[k],
                        input.CLUSTER_PDB,
                        input.DATABINS,
                        ATOMTYPES,
                        input.OUTLIER,
                        input.BFAC_WILS,WILSON_B,
                        input.BFAC_SPHE,
                        input.SCATTERING.FDP,
                        input.PARTIAL,
                        input.FFTvDIRECT,
                        WILSON_LLG,
                        results);
          sad.phase_o_phrenia(TESTING,results);
          results.logTab(0,TESTING,"Phase-o-phrenia input: https://cci.lbl.gov/cctbx/phase_o_phrenia.html");
          if (sad.HiRes() > 3.0)
          results.logTab(0,TESTING,"Plot will not be the same because data are lower than 3.0 Angstroms resolution");
          results.logTab(0,TESTING,dvtos(UnitCell(input.UNIT_CELL).getCctbxUC().parameters()));
          results.logTab(0,TESTING,SpaceGroup(input.SG_HALL).spcgrpname());
          results.logTab(0,TESTING,"Flagged centrosymmetric = " + btos(SADSET[k].CENTROSYMMETRIC));
          for (int j = 0; j < SADSET[k].size(); j++)
            results.logTab(0,TESTING,"Si " + dvtos(SADSET[k][j].SCAT.site,6,4));
          results.logTab(0,TESTING,"");
        }
      }
    }

    if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
    results.logTrailer(LOGFILE); //true = end
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultEP runEP_SAD(InputEP_AUTO& input)
{
  Output output;
  output.setPackagePhenix();
  return runEP_SAD(input,output);
}

}  //end namespace phaser
