//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Dfactor.h>

namespace phaser {

ResultDFAC runDFAC(InputDFAC& input,Output output)
{
  ResultDFAC results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(DFAC));
    input.Analyse(results);

    if (!input.SIGMAN.REFINED)
    { //memory
    results.logSectionHeader(LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    }

    if (input.PTNCS.USE)
    { //memory
    Epsilon epsilon(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                   );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
      epsilon.findROT(input.MACRO_TNCS,results);
    input.PTNCS = epsilon.getPtNcs();
    }

    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    Dfactor dfactor(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                   );
    dfactor.calcDfactor(results);
    results.setDataA(dfactor);
    }

    results.logSectionHeader(LOGFILE,"OUTPUT FILES");
    if (input.DO_HKLOUT.True())
      results.writeMtz(input.HKLIN);
    else results.logTab(1,LOGFILE,"No files output");
    for (int f = 0; f < results.getFilenames().size(); f++)
      results.logTab(1,LOGFILE,results.getFilenames()[f]);
    results.logBlank(LOGFILE);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultDFAC runDFAC(InputDFAC& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runDFAC(input,output);
}

} //end namespace phaser
