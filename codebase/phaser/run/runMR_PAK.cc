//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/Molecule.h>
#include <phaser/src/MapTraceMol.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/lib/mean.h>
#include <phaser/mr_objects/mr_clash.h>
#include <boost/numeric/conversion/cast.hpp>
#include <mmtbx/geometry/calculator.hpp>

#ifdef _OPENMP
#include <omp.h>
#endif

//# define AJM_PACKING_DEBUG  //for deep within loops, very slow
//# define AJM_PACKING_LOGFILE //for the old output

namespace phaser {

  struct xyz_getter
  {
    public:
     typedef dvect3 value_type;
     value_type operator ()(pdb_record const& record) const // const added
     { return record.X; }
  };

ResultMR runMR_PAK(InputMR_PAK& input,Output output)
{
  ResultMR results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to logfile (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_PAK));
    input.Analyse(results);

#ifdef AJM_PACKING_LOGFILE
    results.logTab(1,LOGFILE,"FTF PACKING: " + btos(input.PACKING.FTF_STOP));
#endif

    //even if there are not MRSETS, set up the result object for return
    UnitCell   uc(input.UNIT_CELL);
    cctbx::uctbx::unit_cell cctbxuc = uc.getCctbxUC();
    uc.logUnitCell(VERBOSE,results);
    stringset KNOWN_MODLIDS;
    for (unsigned k = 0; k < input.MRSET.size(); k++)
      for (unsigned s = 0; s < input.MRSET[k].KNOWN.size(); s++)
      {
        if (!input.MRSET[k].KNOWN[s].FRAC ) //order important
        {
          dvect3 tmp = input.MRSET[k].KNOWN[s].TRA;
          dvect3 tmp2 = uc.doOrth2Frac(tmp);
          input.MRSET[k].KNOWN[s].setFracT(tmp2);
        }
        KNOWN_MODLIDS.insert(input.MRSET[k].KNOWN[s].MODLID);
      }


    bool occupancy_warning(false);
    for (int k = 0; k < input.MRSET.size(); k++)
      if (input.MRSET[k].BOOLOCC.size())
      {
        occupancy_warning = true; //at least one
        break;
      }

    if (input.MRSET.size())
    {
      bool FROM_AUTO(input.MRSET[0].NSET && input.MRSET[0].NRF && input.MRSET[0].NTF);
      input.MRSET.if_not_set_apply_space_group(input.SG_HALL);
      {
      results.logSectionHeader(LOGFILE,"ENSEMBLES");
      stringset current;
      bool MAP_PACKING(false);
      {//memory
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          current.insert(input.MRSET[k].KNOWN[s].MODLID);
          if (input.PDB[input.MRSET[k].KNOWN[s].MODLID].ENSEMBLE[0].map_format())
            MAP_PACKING = true;
        }
        results.logBlank(LOGFILE);
      }
      if (MAP_PACKING)
      {
        results.logAdvisory(LOGFILE,"Map Packing: Expect higher clash scores than with packing from coordinates");
        results.logBlank(LOGFILE);
      }
      if (input.tracemol == NULL)
      {
        MapTrcPtr new_ensPtr(new MapTraceMol());
        input.tracemol = new_ensPtr; //deep copy on init
      }
      input.tracemol->configure(current,false,occupancy_warning,input.PDB,results);
      input.tracemol->trace_table(LOGFILE,results);
      }

      //after setup_molecule, or it does it twice
      results.init("PAK",input.TITLE,input.NUM_TOPFILES,input.PDB);
      results.setUnitCell(uc.getUnitCell());

      if (!input.PACKING.all())
      {
        //site symmetry for overlaps
        results.logSectionHeader(LOGFILE,"ENSEMBLE SYMMETRY");
        stringset ptgrp;
        for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
        if (KNOWN_MODLIDS.find(iter->first) != KNOWN_MODLIDS.end())
        {
          bool pdb_used(false);
          for (unsigned k = 0; k < input.MRSET.size(); k++)
            for (unsigned l = 0; l < input.MRSET[k].KNOWN.size(); l++)
              if (input.MRSET[k].KNOWN[l].MODLID == iter->first) pdb_used = true;
          if (!pdb_used) continue;
          results.logTab(1,LOGFILE, "Ensemble \"" + iter->first + "\"" + " Point Group: " + iter->second.PG.hermann_mauguin_symbol());
          if (iter->second.PG.multiplicity() != 1) ptgrp.insert(iter->first);
        }
        results.logBlank(LOGFILE);

        bool first_M(true);
        std::map<std::pair<int,int>,int > multiplicity_k_s;
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        if (!input.PACKING.FTF_STOP ||
            (input.PACKING.FTF_STOP && input.MRSET[k].TF >= input.PACKING.FTF_LIMIT))
        {
          cctbx::sgtbx::space_group sg  = SpaceGroup(input.MRSET[k].HALL).getCctbxSG();
          bool has_multiplicity(false),has_pt(false);
          for (unsigned s = 0; s < input.MRSET[k].KNOWN.size(); s++)
          {
            std::string MCHK = input.MRSET[k].KNOWN[s].MODLID;
            overlap ol(input.MRSET[k].KNOWN[s],sg,cctbxuc,input.PDB[MCHK].COORD_CENTRE);
            has_multiplicity = has_multiplicity || (ol.MULT > 1);
            has_pt = has_pt || (ptgrp.find(input.MRSET[k].KNOWN[s].MODLID) != ptgrp.end());
          }
          for (unsigned s = 0; s < input.MRSET[k].KNOWN.size() && has_multiplicity && has_pt; s++)
          {
            if (first_M) results.logUnderLine(SUMMARY,"Multiplicity Table");
            if (first_M) results.logUnderLine(SUMMARY,"#SET = solution# - component#");
            if (first_M) results.logTabPrintf(1,SUMMARY,"#SET   MULT %-8s%c   %-18s %-17s %-17s\n","ENSEMBLE",' ',"SITE","Centre of Mass","(EXACT)");
            std::string MCHK = input.MRSET[k].KNOWN[s].MODLID;
            overlap ol(input.MRSET[k].KNOWN[s],sg,cctbxuc,input.PDB[MCHK].COORD_CENTRE);
            if (ol.MULT > 1 && has_pt) multiplicity_k_s[std::pair<int,int>(k,s)] = ol.MULT;
            results.logTabPrintf(1,SUMMARY,"%-9s M=%-2d%c%-8s % 5.2f % 5.2f % 5.2f  % 5.2f % 5.2f % 5.2f  (% 5.2f % 5.2f % 5.2f)\n",
                   std::string(itos(k+1) + "-" + itos(s+1)).c_str(),
                   ol.MULT,
                   (has_pt && ol.MULT > 1 ? '*':' '),
                   std::string(input.MRSET[k].KNOWN[s].MODLID,0,8).c_str(),
                   input.MRSET[k].KNOWN[s].TRA[0],input.MRSET[k].KNOWN[s].TRA[1],input.MRSET[k].KNOWN[s].TRA[2],
                   ol.COFM[0],ol.COFM[1],ol.COFM[2],
                   ol.EXACT_SITE[0],ol.EXACT_SITE[1],ol.EXACT_SITE[2]);
            first_M = false;
          }
          if (has_multiplicity && has_pt) results.logTab(1,SUMMARY,"----------");
        }

        results.logBlank(LOGFILE);
        bool HAS_BEEN_PACKED = !input.PACKING.FTF_STOP && input.MRSET.has_been_background_packed(input.PACKING.CUTOFF);
        results.logTab(1,LOGFILE,"Clash background has" +
             std::string(HAS_BEEN_PACKED ? "":" NOT") +
            " been packed at " + dtos(input.PACKING.CUTOFF*100,2) + "%");
        results.logBlank(LOGFILE);

        int nthreads = 1;
#pragma omp parallel
        {
#ifdef _OPENMP
          nthreads = omp_get_num_threads();
#endif
        }
#ifdef AJM_PACKING_DEBUG
        if (nthreads != 1) throw PhaserError(INPUT,"For expert packing debug require NJOB 1");
#endif
#ifdef AJM_PACKING_LOGFILE
        if (nthreads != 1) throw PhaserError(INPUT,"For expert packing debug require NJOB 1");
#endif
        results.logSectionHeader(LOGFILE,"PACKING FUNCTION");
        if (input.MRSET.size() == 1)
        results.logTab(1,LOGFILE,"There is 1 solution to pack");
        else
        results.logTab(1,LOGFILE,"There are " + itos(input.MRSET.size()) + " solutions to pack");
        if (nthreads > 1)
          results.logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");

        bool overlap_warning(false);
        bool tfz_warning(false);
        int input_mrset_size = boost::numeric_cast<int>(input.MRSET.size());
        results.logProgressBarStart(LOGFILE,"Packing analysis",input.MRSET.size()/nthreads);

        std::vector<MapTrcPtr> omp_tracemol(nthreads);
        omp_tracemol[0] = input.tracemol;
#ifdef _OPENMP
        for (int n = 1; n < nthreads; n++)
        {
          MapTrcPtr ensPtr(new MapTraceMol(*input.tracemol));
          omp_tracemol[n] = ensPtr;
        }
#endif
        for (int k = 0; k < input_mrset_size; k++)
        if (!input.PACKING.FTF_STOP ||
            (input.PACKING.FTF_STOP && input.MRSET[k].TF >= input.PACKING.FTF_LIMIT))
        {
#ifdef AJM_PACKING_LOGFILE
          results.logTab(1,LOGFILE,itos(k+1) + " Clash data: " + input.MRSET[k].CLASH.unparse());
#endif
          if (HAS_BEEN_PACKED)
          {
#ifdef AJM_PACKING_LOGFILE
            results.logTab(2,LOGFILE,"Clash extended");
#endif
            input.MRSET[k].CLASH.add_matrix(input.MRSET[k].KNOWN.size());
            input.MRSET[k].CLASH.add_trace_length(input.tracemol->find(input.MRSET[k].KNOWN.back().MODLID)->second.size());
            input.MRSET[k].CLASH.add_trace_percent(input.PACKING.CUTOFF);
          }
          else //if (input.MRSET[k].CLASH.size() != input.MRSET[k].KNOWN.size())
               //or if they are the same, must be here because we need a complete recalculation
          {
#ifdef AJM_PACKING_LOGFILE
            results.logTab(2,LOGFILE,"Clash initialized");
#endif
            input.MRSET[k].CLASH.clear();
            input.MRSET[k].CLASH.resize(input.MRSET[k].KNOWN.size());
            for (unsigned s = 0; s < input.MRSET[k].KNOWN.size(); s++)
              input.MRSET[k].CLASH.add_trace_length(input.tracemol->find(input.MRSET[k].KNOWN[s].MODLID)->second.size());
            for (unsigned s = 0; s < input.MRSET[k].KNOWN.size(); s++)
              input.MRSET[k].CLASH.add_trace_percent(input.PACKING.CUTOFF);
          }
          PHASER_ASSERT(input.MRSET[k].CLASH.ok(input.MRSET[k].KNOWN.size()));
        }

        int1D omp_stop_loop(nthreads,input_mrset_size);
#pragma omp parallel for firstprivate(uc)
        for (int k = 0; k < input_mrset_size; k++)
        {
          int nthread = 0;
#ifdef _OPENMP
          nthread = omp_get_thread_num();
#endif

#ifdef AJM_PACKING_DEBUG
          results.logSectionHeader(LOGFILE,"PACKING FUNCTION #" + itos(k+1) + " OF " + itos(input.MRSET.size()));
          results.logTab(1,LOGFILE,"FSS: " + dtos(input.MRSET[k].TF));
#endif
#ifdef AJM_PACKING_DEBUG
          if (input.MRSET[k].ANNOTATION.size()) results.logTab(1,LOGFILE,"ANNOTATION: " + input.MRSET[k].ANNOTATION);
          for (unsigned s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            results.logTab(1,LOGFILE,input.MRSET[k].KNOWN[s].logfile(),false);
          results.logBlank(LOGFILE);
#endif

          //FTF_LIMIT is initialzed to starting f_top = -std::numeric_limits<floatType>::max()) in FTF code
          //only the first packing on each thread will be flagged as KEEP in the FTF packing mode
          if (input.PACKING.FTF_STOP && input.MRSET[k].TF < input.PACKING.FTF_LIMIT)
          {
#ifdef AJM_PACKING_DEBUG
            results.logTab(1,LOGFILE,"Packing not performed: under limit " + dtos(input.MRSET[k].TF) + "<" + dtos(input.PACKING.FTF_LIMIT) );
#endif
            input.MRSET[k].KEEP = true; //don't do the packing test if there is no hope that it is higher
            if (k < omp_stop_loop[nthread]) omp_stop_loop[nthread]=k; //no break in omp
          }
          else if (input.PACKING.FTF_STOP && omp_stop_loop[nthread] < k) //found one higher up on other thread
          {
#ifdef AJM_PACKING_DEBUG
            results.logTab(1,LOGFILE,"Packing not performed: stop loop");
#endif
            input.MRSET[k].KEEP = true;
          }
          else
          {
          if (occupancy_warning) omp_tracemol[nthread]->configure(input.MRSET[k]);
          SpaceGroup sg(input.MRSET[k].HALL);
          cctbx::sgtbx::space_group cctbxsg  = sg.getCctbxSG();

          //move first full solution closest to the origin
          if (input.MRSET[k].KNOWN.size()) //paranoia!
          { //memory scope
            dvect3 fracSolT_ref(0,0,0);
            dvect3 orthSolT_ref(0,0,0);
            dmat33 solR_ref(1,0,0,0,1,0,0,0,1);
            dvect3 orthCentre_ref(0,0,0);
            unsigned s_chk = 0;
            std::string MCHK = input.MRSET[k].KNOWN[s_chk].MODLID;
            mr_closest closest;

            dvect3 fracSolT_chk(input.MRSET[k].KNOWN[s_chk].TRA);
            dvect3 orthSolT_chk(uc.doFrac2Orth(fracSolT_chk));
            dmat33 solR_chk(input.MRSET[k].KNOWN[s_chk].R);
            dvect3 orthCentre_chk(solR_chk*input.PDB[MCHK].COORD_CENTRE+orthSolT_chk);
            dvect3 fracCentre_chk(uc.doOrth2Frac(orthCentre_chk));

            for (unsigned isym = 0; isym < sg.NSYMM; isym++)
            {
              dvect3 fracCentre_chk_sym(sg.doSymXYZ(isym,fracCentre_chk));
              dvect3 fracCellTrans(0,0,0);
              fracCellTrans[0] = phaser::round(-fracCentre_chk_sym[0]);
              fracCellTrans[1] = phaser::round(-fracCentre_chk_sym[1]);
              fracCellTrans[2] = phaser::round(-fracCentre_chk_sym[2]);
              dvect3 fracCentre_chk_sym_cell(fracCentre_chk_sym + fracCellTrans);
              dvect3 orthCentre_chk_sym_cell(uc.doFrac2Orth(fracCentre_chk_sym_cell));
              floatType distSqr((orthCentre_ref-orthCentre_chk_sym_cell)*(orthCentre_ref-orthCentre_chk_sym_cell));
#ifdef AJM_PACKING_DEBUG
              results.logBlank(LOGFILE);
              results.logTab(1,LOGFILE,"Symmetry operator #" + itos(isym+1) );
              results.logTab(1,LOGFILE,"The centre for the symmetry related checked molecule is ");
              results.logTab(2,LOGFILE,"Fractional coordinates " + dvtos(fracCentre_chk_sym));
              results.logTab(1,LOGFILE,"Cell translations for closest approach to origin: " + dvtos(fracCellTrans));
              results.logTab(1,LOGFILE,"Distance to origin after cell translations: " + dtos(std::sqrt(distSqr)));
#endif
              if (distSqr < closest.Sqr)
              {
                closest.Sqr = distSqr - 1.0e-03; //bias towards first
                if (!isym && !fracCellTrans[0] && !fracCellTrans[1] && !fracCellTrans[2])
                {
                  closest.Sqr -= 1.0e-03; //take off a bit to bias towards the Identity
                  closest.IsIdentity = true;
                }
                else closest.IsIdentity = false;
                closest.Rotsym = sg.Rotsym(isym);
                closest.Trasym = sg.Trasym(isym);
                closest.CellTrans = fracCellTrans;
              }
            }
#ifdef AJM_PACKING_LOGFILE
            closest.IsIdentity ? results.logTab(2,LOGFILE,"Closest is identity"):
                                results.logTab(2,LOGFILE,"Closest is NOT identity");
#endif
            if (!closest.IsIdentity && input.PACKING.COMPACT && !input.PDB[MCHK].is_atom)
            {
#ifdef AJM_PACKING_LOGFILE
              results.logTab(2,LOGFILE,"Solution #" + itos(s_chk+1) + " moved to closest. CoM approach to Origin");
#endif
              dmat33 Rsym(uc.Frac2Orth()*closest.Rotsym.transpose()*uc.Orth2Frac());
              input.MRSET[k].KNOWN[s_chk].R=(Rsym*input.MRSET[k].KNOWN[s_chk].R);
              input.MRSET[k].KNOWN[s_chk].setFracT(closest.Rotsym.transpose()*input.MRSET[k].KNOWN[s_chk].TRA+closest.Trasym+closest.CellTrans);
            }
          }

          //assume the packed molecules are all the same!
          //threadsafe
          for (unsigned s_ref = 0; s_ref < input.MRSET[k].KNOWN.size(); s_ref++)
          {
            std::string MREF(input.MRSET[k].KNOWN[s_ref].MODLID);
            dmat33 solR_ref(input.MRSET[k].KNOWN[s_ref].R);
            dvect3 fracSolT_ref(input.MRSET[k].KNOWN[s_ref].TRA);
            dvect3 orthSolT_ref(uc.doFrac2Orth(fracSolT_ref));
            // without finding centre of current set
            dvect3 orthCentre_ref(solR_ref*input.PDB[MREF].COORD_CENTRE+orthSolT_ref);

            TraceMol& Trace_ref = omp_tracemol[nthread]->find(MREF)->second;
#ifdef AJM_PACKING_DEBUG
            results.logTab(1,LOGFILE,"Trace ref modlid " + MREF);
             std::vector<pdb_record> pdbm = Trace_ref.trace_RT(solR_ref,orthSolT_ref);
             results.logTab(1,LOGFILE,"Trace molecule size for packing (ref) = "  + itos(pdbm.size()));
#endif
            //NOT SYMMETRIC!! don't start at s_ref
            for (unsigned s_chk = 0; s_chk < input.MRSET[k].KNOWN.size(); s_chk++)
            if (!HAS_BEEN_PACKED || //need to check all pairs
                //or if this is coming from an auto mode then previous pairs will have been packed
                //only need to check the pairs involving the last known to see if it packs
                (HAS_BEEN_PACKED && (s_chk == input.MRSET[k].KNOWN.size()-1 ||
                              s_ref == input.MRSET[k].KNOWN.size()-1)))
            {
              std::string MCHK = input.MRSET[k].KNOWN[s_chk].MODLID;
              TraceMol& Trace_chk = omp_tracemol[nthread]->find(MCHK)->second;
#ifdef AJM_PACKING_DEBUG
              results.logTab(1,LOGFILE,"Trace chk modlid " + MCHK);
              int counta(0);
              for (unsigned a_chk = 0; a_chk < Trace_chk.size(); a_chk++)
              if (Trace_chk.card(a_chk).O > 0)
                counta++;
              results.logTab(1,LOGFILE,"Trace molecule size for packing (chk) = "  + itos(counta));
#endif
              floatType count_complex(0);
              dvect3 orthCentre_complex(0,0,0);
              int n = (s_chk == s_ref) ? s_ref+1 : s_chk;
              for (unsigned s_ref2 = 0; s_ref2 < n; s_ref2++)
              {
                dmat33 solR_ref(input.MRSET[k].KNOWN[s_ref2].R);
                dvect3 fracSolT_ref(input.MRSET[k].KNOWN[s_ref2].TRA);
                dvect3 orthSolT_ref(uc.doFrac2Orth(fracSolT_ref));
                orthCentre_complex += solR_ref*input.PDB[MREF].COORD_CENTRE+orthSolT_ref;
                count_complex++;
              }
              if (count_complex) orthCentre_complex /= count_complex;
              dvect3 fracCentre_complex(uc.doOrth2Frac(orthCentre_complex));
#ifdef AJM_PACKING_LOGFILE
              results.logBlank(LOGFILE);
              results.logTab(1,LOGFILE,"The current centre of mass of complex is ");
              results.logTab(2,LOGFILE,"Orthogonal coordinates " + dvtos(orthCentre_complex) +"\n");
#endif
              floatType extent_complex(0),max_extent_complex(0);
              for (unsigned s_ref2 = 0; s_ref2 < n; s_ref2++)
              {
                dmat33 solR_ref(input.MRSET[k].KNOWN[s_ref2].R);
                dvect3 fracSolT_ref(input.MRSET[k].KNOWN[s_ref2].TRA);
                dvect3 orthSolT_ref(uc.doFrac2Orth(fracSolT_ref));
                extent_complex = std::max(extent_complex,
                                 (orthCentre_complex-solR_ref*input.PDB[MREF].COORD_CENTRE+orthSolT_ref)*
                                 (orthCentre_complex-solR_ref*input.PDB[MREF].COORD_CENTRE+orthSolT_ref));
                max_extent_complex = std::max(max_extent_complex,input.PDB[MREF].max_extent());
              }
              extent_complex = std::sqrt(extent_complex);
#ifdef AJM_PACKING_DEBUG
              results.logTab(1,LOGFILE,"Maximum distance of ensemble from CoM in current complex is " + ftos(extent_complex) + "\n");
              results.logTab(1,LOGFILE,"Maximum extent for ensemble in current complex is " + ftos(max_extent_complex) + "\n");
#endif
              extent_complex += max_extent_complex;
              mr_closest closest;

#ifdef AJM_PACKING_DEBUG
              if (s_ref != s_chk)
              {
                results.logUnderLine(LOGFILE,"Packing of Solution Components");
                results.logTab(1,LOGFILE,itos(s_ref+1)+ ": " + input.MRSET[k].KNOWN[s_ref].logfile(),false);
                results.logTab(1,LOGFILE,itos(s_chk+1)+ ": " + input.MRSET[k].KNOWN[s_chk].logfile(),false);
              }
              else
              {
                results.logUnderLine(LOGFILE,"Self Packing");
                results.logTab(1,LOGFILE,itos(s_ref+1)+ ": " + input.MRSET[k].KNOWN[s_ref].logfile(),false);
              }
#endif
              dvect3 fracSolT_chk(input.MRSET[k].KNOWN[s_chk].TRA);
              dvect3 orthSolT_chk(uc.doFrac2Orth(fracSolT_chk));
              dmat33 solR_chk(input.MRSET[k].KNOWN[s_chk].R);
              dvect3 orthCentre_chk(solR_chk*input.PDB[MCHK].COORD_CENTRE+orthSolT_chk);
              dvect3 fracCentre_chk(uc.doOrth2Frac(orthCentre_chk));

              floatType minDist(2*input.PDB[MCHK].max_extent());
              floatType minDistSqr(fn::pow2(minDist));
              floatType packingDist = extent_complex + input.PDB[MCHK].max_extent();
              //molecule barely in contact could move maximum of 2*minDist and still be barely in contact
              //minDist used for clash tests. fracExtent used to pack ensemble closest to CoM of current complex
              //Check unit cell translations, not orthogonal
              dvect3 fracExtent(0,0,0),checkExtent;
              for (floatType ix = -1; ix < 1.5; ix++)
                for (floatType iy = -1; iy < 1.5; iy++)
                  for (floatType iz = -1; iz < 1.5; iz++)
                    if (!(ix==0 && iy==0 && iz==0))
                    {
                      dvect3 orthExtent(ix*packingDist,iy*packingDist,iz*packingDist);
                      orthExtent /= std::sqrt(ix*ix+iy*iy+iz*iz);
                      checkExtent = uc.doOrth2Frac(orthExtent);
                      for (int ii=0; ii < 3; ii++)
                        fracExtent[ii] = std::max(fracExtent[ii],std::abs(checkExtent[ii]));
                    }
# ifdef AJM_PACKING_DEBUG
              results.logTab(1,LOGFILE,"Max Extent " + MREF + " " + dtos(input.PDB[MREF].max_extent()));
              results.logTab(1,LOGFILE,"Max Extent " + MCHK + " " + dtos(input.PDB[MCHK].max_extent()));
              results.logTab(1,LOGFILE,"Minimum distance between centres of mass to avoid clash is " + dtos(minDist));
              results.logTab(1,LOGFILE,"Distance from centre of mass of complex to check packing is " + dtos(packingDist));
              results.logTab(1,LOGFILE,"Extent to pack and avoid clash (FRAC) is " + dvtos(fracExtent));
              results.logTab(1,LOGFILE,"Unit cell translations to pack and avoid clash +/- " + dvtos(fracExtent));
#endif
              for (unsigned isym = 0; isym < sg.NSYMM; isym++)
              {
                dvect3 fracCentre_chk_sym(sg.doSymXYZ(isym,fracCentre_chk));
                dvect3 diffCentre;
                diffCentre[0] = fracCentre_complex[0] - fracCentre_chk_sym[0];
                diffCentre[1] = fracCentre_complex[1] - fracCentre_chk_sym[1];
                diffCentre[2] = fracCentre_complex[2] - fracCentre_chk_sym[2];
                dvect3 cellTrans_min(0,0,0),cellTrans_max(0,0,0),cellTrans(0,0,0);
                cellTrans_min[0] = -std::floor(fracExtent[0]-diffCentre[0]);
                cellTrans_min[1] = -std::floor(fracExtent[1]-diffCentre[1]);
                cellTrans_min[2] = -std::floor(fracExtent[2]-diffCentre[2]);
                cellTrans_max[0] =  std::floor(diffCentre[0]+fracExtent[0]);
                cellTrans_max[1] =  std::floor(diffCentre[1]+fracExtent[1]);
                cellTrans_max[2] =  std::floor(diffCentre[2]+fracExtent[2]);
# ifdef AJM_PACKING_DEBUG
                results.logBlank(LOGFILE);
                results.logTab(1,LOGFILE,"Symmetry operator #" + itos(isym+1) );
                results.logTab(1,LOGFILE,"The centre for the symmetry related checked molecule is ");
                results.logTab(2,LOGFILE,"Fractional coordinates " + dvtos(fracCentre_chk_sym));
                results.logTab(1,LOGFILE,"The difference between centres of mass is " + dvtos(diffCentre));
                results.logTab(1,LOGFILE,"The cell translations are between " + dvtos(cellTrans_min) + " " + dvtos(cellTrans_max));
#endif
                for (cellTrans[0] = cellTrans_min[0]; cellTrans[0] <= cellTrans_max[0]; cellTrans[0]++)
                {
                  for (cellTrans[1] = cellTrans_min[1]; cellTrans[1] <= cellTrans_max[1]; cellTrans[1]++)
                  {
                    for (cellTrans[2] = cellTrans_min[2]; cellTrans[2] <= cellTrans_max[2]; cellTrans[2]++)
                    {
                      if (s_chk == s_ref && isym == 0
                           && std::abs(cellTrans[0]) < 0.001
                           && std::abs(cellTrans[1]) < 0.001
                           && std::abs(cellTrans[2]) < 0.001)
                        continue; // Don't check molecule against itself.
                      if (s_chk == s_ref && //overlap, does not have to be an origin position
                          (ptgrp.find(input.MRSET[k].KNOWN[s_chk].MODLID) != ptgrp.end())
                          && (multiplicity_k_s.find(std::pair<int,int>(k,s_chk)) != multiplicity_k_s.end())
                         )
                      {
                        overlap_warning = true;
                        if (input.MRSET[k].KNOWN[s_chk].MULT == multiplicity_k_s[std::pair<int,int>(k,s_chk)]) continue; ///can't be higher multiplicity than the site symmetry
                        overlap ol(input.MRSET[k].KNOWN[s_chk],cctbxsg,cctbxuc,input.PDB[MCHK].COORD_CENTRE);
                        if (ol.MULT > 1)
                          input.MRSET[k].KNOWN[s_chk].MULT = ol.MULT;
                        continue; // Don't check molecule against symmetry copies when site symm with ptgroup for ensemble
                      }
                      dvect3 fracCellTrans(cellTrans);
                      dvect3 fracCentre_chk_sym_cell(fracCentre_chk_sym + fracCellTrans);
                      dvect3 orthCentre_chk_sym_cell(uc.doFrac2Orth(fracCentre_chk_sym_cell));
                      floatType distSqr((orthCentre_ref-orthCentre_chk_sym_cell)*(orthCentre_ref-orthCentre_chk_sym_cell));
                      floatType distSqrComplex((orthCentre_complex-orthCentre_chk_sym_cell)*(orthCentre_complex-orthCentre_chk_sym_cell));
                      if (distSqrComplex < closest.Sqr)
                      {
# ifdef AJM_PACKING_DEBUG
                      results.logTab(1,LOGFILE,"Distance between centres of mass SymOp #" + itos(isym+1) + " (" +
                                             itos(fracCellTrans[0]) + " " +
                                             itos(fracCellTrans[1]) + " " +
                                             itos(fracCellTrans[2]) + ") " + dtos(std::sqrt(distSqr)));
                      results.logTab(1,LOGFILE,"Distance for complex between centres of mass SymOp #" + itos(isym+1) + " (" +
                                             itos(fracCellTrans[0]) + " " +
                                             itos(fracCellTrans[1]) + " " +
                                             itos(fracCellTrans[2]) + ") " + dtos(std::sqrt(distSqrComplex)));
#endif
                        closest.Sqr = distSqrComplex;
                        closest.Rotsym = sg.Rotsym(isym);
                        closest.Trasym = sg.Trasym(isym);
                        closest.CellTrans = fracCellTrans;
                      }
                      if (distSqr < minDistSqr)
                      {
                        std::vector<pdb_record> pdbm = Trace_ref.trace_RT(solR_ref,orthSolT_ref);
                        mmtbx::geometry::asa::calculator::ConstRadiusCalculator<
                            mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, xyz_getter >,
                            double // arrays can be used directly without adaptors
                           >
                        refcalc(
                          mmtbx::geometry::asa::calculator::utility::TransformedArray< std::vector<pdb_record>, xyz_getter >( pdbm ),
                          Trace_ref.close_contact_distance()
                          );
                        for (unsigned a_chk = 0; a_chk < Trace_chk.size(); a_chk++)
                        if  (Trace_chk.card(a_chk).O > 0)
                        {
                          dvect3 orthCa_chk = solR_chk*Trace_chk.card(a_chk).X + orthSolT_chk;
                          dvect3 fracCa_chk =  uc.doOrth2Frac(orthCa_chk);
                          dvect3 fracCa_chk_sym = sg.doSymXYZ(isym,fracCa_chk);
                          dvect3 fracCa_chk_sym_cell = fracCa_chk_sym + fracCellTrans;
                          dvect3 orthCa_chk_sym_cell = uc.doFrac2Orth(fracCa_chk_sym_cell);
                          bool clash = refcalc.is_overlapping_sphere(orthCa_chk_sym_cell,Trace_chk.close_contact_distance());
                          if (clash)
                          {
                            input.MRSET[k].CLASH.inc(s_ref,s_chk);
//output should NOT be symmetric for all a=x b=y should be a=y b=x
#ifdef AJM_PACKING_DEBUG
 results.logTab(1,TESTING,"Clash #" + itos(input.MRSET[k].CLASH.get(s_ref,s_chk)) + " between " + itos(s_ref+1) + " and " + itos(s_chk+1));
#endif
                            if (!input.ZSCORE.over_cutoff(input.MRSET[k].TFZ) &&
                                 input.PACKING.QUICK &&
                                 input.MRSET[k].CLASH.one_bad())
                            {
//check to see if any individual clash goes over the limit
//e.g proteasome case, where first 10 pack nicely then 11th is very tolerant to clashes as a lot of
//slack has been built up in the packing
                              input.MRSET[k].CLASH.greater_than = true;
                              goto end_check;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
#ifdef AJM_PACKING_LOGFILE
              if (input.MRSET[k].CLASH.get(s_chk,s_ref) > 1 || input.MRSET[k].CLASH.get(s_chk,s_ref) == 0)
                results.logTab(1,LOGFILE,"There were " + itos(input.MRSET[k].CLASH.get(s_chk,s_ref)) + " clashes between trace atoms for models " + itos(s_chk+1) + ":" + itos(s_ref+1));
              else if (input.MRSET[k].CLASH.get(s_chk,s_ref) == 1)
                results.logTab(1,LOGFILE,"There was 1 clash between trace atoms for models " + itos(s_chk+1) + ":" + itos(s_ref+1));
#endif

              if (s_chk != s_ref && s_ref == 0 &&
                  input.PACKING.COMPACT && !input.PDB[MCHK].is_atom)
              {
# ifdef AJM_PACKING_LOGFILE
                results.logTab(2,LOGFILE,"Solution #" + itos(s_chk+1) + " moved to closest. CoM approach to current centre of complex");
#endif
                dmat33 Rsym = uc.Frac2Orth()*closest.Rotsym.transpose()*uc.Orth2Frac();
                input.MRSET[k].KNOWN[s_chk].R = (Rsym*input.MRSET[k].KNOWN[s_chk].R);
                input.MRSET[k].KNOWN[s_chk].setFracT(closest.Rotsym.transpose()*input.MRSET[k].KNOWN[s_chk].TRA+closest.Trasym+closest.CellTrans);
              }
              //assume the packed molecules are all the same!
              //threadsafe

            } //s_chk
          } //s_ref

          end_check:
          { //memory
          input.MRSET[k].PAK = phaser::round(input.MRSET[k].CLASH.worst_clash()*100.);
          input.MRSET[k].ORIG_NUM = k;
          if (!input.PACKING.FTF_STOP)
          input.MRSET[k].ANNOTATION += " PAK"+ std::string(input.MRSET[k].CLASH.greater_than ? ">":"=") + dtos(input.MRSET[k].PAK);
          bool high_tfz(input.MRSET[k].CLASH.one_bad() &&
                        input.ZSCORE.over_cutoff(input.MRSET[k].TFZ) &&
                       // input.PACKING.KEEP_HIGH_TFZ &&
                       !input.MRSET[k].CLASH.one_excessive());
          input.MRSET[k].KEEP =  !input.MRSET[k].CLASH.one_bad(); //initially, only keep if it packs
          input.MRSET[k].PACKS = !input.MRSET[k].CLASH.one_bad();
          input.MRSET[k].high_tfz(high_tfz);
          } //memory
# ifdef AJM_PACKING_LOGFILE
          results.logTab(2,LOGFILE,"Clashes:" + input.MRSET[k].ANNOTATION);
          results.logBlank(LOGFILE);
#endif
          if (input.PACKING.FTF_STOP && input.MRSET[k].PACKS) omp_stop_loop[nthread]=k; //no break in omp

          if (nthread == 0) results.logProgressBarNext(LOGFILE);
          }
        } //#end pragma omp parallel
        results.logProgressBarEnd(LOGFILE);

        results.logBlank(LOGFILE);
        if (overlap_warning)
        results.logWarning(LOGFILE,"Ensemble(s) with internal symmetry placed on special position(s) during packing");
        if (occupancy_warning)
        results.logAdvisory(LOGFILE,"Refined occupancies used in packing");
        if (tfz_warning && input.PACKING.KEEP_HIGH_TFZ)
          results.logAdvisory(LOGFILE,"High TFZ solution(s) that did not pack carried in solution list");
        if (tfz_warning)
          results.logWarning(LOGFILE,"High TFZ solution(s) failed in packing");


        //now change the KEEP flag depending on whether there are packing solutions in the list
        //if this is part of a fast mode with pruning, and no solutions pack, hang on to high tfz
        if (input.PACKING.PRUNE && !input.MRSET.num_packs())
          for (unsigned k = 0; k < input.MRSET.size(); k++)
            if (input.MRSET[k].high_tfz() && !input.MRSET[k].PACKS)
              input.MRSET[k].KEEP = true;
        if (input.PACKING.KEEP_HIGH_TFZ) //keep regardless of fast mode pruning or other solutions packing
          for (unsigned k = 0; k < input.MRSET.size(); k++)
            if (input.MRSET[k].high_tfz())
              input.MRSET[k].KEEP = true;

        int naccepted(0);
        int nhightfz(0);
        int npacks(0);
        mr_solution accepted_mrset(input.MRSET.num_rlist());
                    accepted_mrset.copy_extras(input.MRSET);
        mr_solution rejected_mrset(input.MRSET.num_rlist());
                    rejected_mrset.copy_extras(input.MRSET);
        results.logTab(1,TESTING,"Create accepted and rejected lists");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        if (input.MRSET[k].KEEP) //includes hightfz only if there is a prune or keep_high_tfz flag
        {
          if (input.MRSET[k].PACKS) npacks++;
          if (input.MRSET[k].high_tfz() && input.PACKING.KEEP_HIGH_TFZ) //can be in both lists if keep high true
          {
            input.MRSET[k].NUM = ++nhightfz;
            rejected_mrset.push_back(input.MRSET[k]);
          }
          input.MRSET[k].NUM = ++naccepted; //second, final order
          accepted_mrset.push_back(input.MRSET[k]);
          input.MRSET[k].HISTORY += "PAK(" + itos(input.MRSET[k].ORIG_NUM+1) + ":" + itos(naccepted) + ")";
        }
        results.setDotSol(accepted_mrset);
        results.setDotSolReject(rejected_mrset);

        results.logUnderLine(SUMMARY,"Packing Table");
        if (input.PACKING.all())
          results.logTab(1,SUMMARY,"All solutions accepted");
        else if (input.PACKING.pairwise())
          results.logTab(1,SUMMARY,"Solutions accepted if pairwise clashes less than " + dtos(input.PACKING.CUTOFF*100) + " % of trace atoms");
        int max_len(DEF_WIDTH-DEF_TAB-5-5-8-5-13-7-12-1);
        //if the packing function is run separately, mr_solution will not have
        //NSET NRF NTF TF or TFZ populated, just the annotation
        (FROM_AUTO) ?
          results.logTabPrintf(1,SUMMARY,"%-4s %-4s %-7s %4s %6s %4s %5s %12s %-5s  %-11s\n","#in","#out","Clash-%","Symm","TF-SET","ROT","TFpk#","TF   ","TFZ","SpaceGroup"):
          results.logTabPrintf(1,SUMMARY,"%-4s %-4s %-7s %4s  %-11s %-*s\n","#in","#out","Clash-%","Symm","SpaceGroup",max_len,"Annotation");
        int stop_loop = (*std::min_element(omp_stop_loop.begin(),omp_stop_loop.end()));
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        if (k <= stop_loop)
        {
          std::string sgname = space_group_name(input.MRSET[k].HALL,true).CCP4;
          std::string NUM("---");
          if (input.MRSET[k].KEEP)
            NUM = (input.MRSET[k].NUM == 1) ? "Top1" : itos(input.MRSET[k].NUM);
          int multiplicity(0);
          for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            multiplicity = std::max(multiplicity,input.MRSET[k].KNOWN[s].MULT);
          (FROM_AUTO) ?
          results.logTabPrintf(1,SUMMARY,"%-4d %-4s %c%-6.*f %-4s  %4d  %4d %4d %13.2f %5.2f  %-11s\n",
            input.MRSET[k].ORIG_NUM+1,
            NUM.c_str(),
            input.MRSET[k].CLASH.greater_than ? '>' : ' ',
            (input.MRSET[k].CLASH.worst_clash() == 0 ? 0 :
             (input.MRSET[k].CLASH.worst_clash() >=1 ? 1 :
              (input.MRSET[k].CLASH.worst_clash() >= 0.1 ? 2 :
               (input.MRSET[k].CLASH.worst_clash() >= 0.01 ? 3 : 3)))),
            input.MRSET[k].CLASH.worst_clash()*100,
            (multiplicity <= 1) ? "--" : itos(multiplicity,3).c_str(),
            input.MRSET[k].NSET,
            input.MRSET[k].NRF,
            input.MRSET[k].NTF,
            input.MRSET[k].TF,
            input.MRSET[k].TFZ,
            sgname.c_str()
          ):
          results.logTabPrintf(1,SUMMARY,"%-4d %-4s %c%-6.*f %-4s  %-11s %-*s\n",
            input.MRSET[k].ORIG_NUM+1,
            NUM.c_str(),
            input.MRSET[k].CLASH.greater_than ? '>' : ' ',
            (input.MRSET[k].CLASH.worst_clash() == 0 ? 0 :
             (input.MRSET[k].CLASH.worst_clash() >=1 ? 1 :
              (input.MRSET[k].CLASH.worst_clash() >= 0.1 ? 2 :
               (input.MRSET[k].CLASH.worst_clash() >= 0.01 ? 3 : 3)))),
            input.MRSET[k].CLASH.worst_clash()*100,
            (multiplicity <= 1) ? "--" : itos(multiplicity,3).c_str(),
            sgname.c_str(),
            max_len,
            input.MRSET[k].short_annotation(max_len).c_str()
          );
        }
        if (stop_loop < input.MRSET.size()) results.logTab(1,SUMMARY,"--- packing terminated ---");
        results.logBlank(SUMMARY);
        results.logTab(1,SUMMARY,itos(accepted_mrset.size()) + " accepted of " + itos(input.MRSET.size()) + " solutions");
        accepted_mrset.size() == 1 ?
        results.logTab(2,SUMMARY,itos(npacks) + std::string(npacks == 1?" packs":" pack") + " of 1 accepted solution"):
        results.logTab(2,SUMMARY,itos(npacks) + std::string(npacks == 1?" packs":" pack") + " of " + itos(accepted_mrset.size()) + " accepted solutions");
        if (rejected_mrset.size())
        rejected_mrset.size() == 1 ?
          results.logTab(2,SUMMARY,"1 solution with high TFZ (of " + itos(accepted_mrset.size()) + " solutions) carried despite failing packing"):
          results.logTab(2,SUMMARY,itos(rejected_mrset.size()) + " solutions with high TFZ (of " + itos(accepted_mrset.size()) + " solutions) carried despite failing packing");
        results.logBlank(SUMMARY);
      }
      else  //by default does not pack maps or if all selected
      {
        results.logAdvisory(LOGFILE,"All solutions accepted: No packing test performed");
        results.setDotSol(input.MRSET);
        results.logBlank(SUMMARY);
        results.logTab(1,SUMMARY,itos(input.MRSET.size()) + " accepted of " + itos(input.MRSET.size()) + " solutions");
        results.logBlank(SUMMARY);
      }

      if (results.isPackagePhenix() || input.PACKING.FTF_STOP) input.DO_KEYWORDS.Default(false);
      outStream where = SUMMARY;
      results.logSectionHeader(where,"OUTPUT FILES");
      if (input.DO_KEYWORDS.True()) results.writeSol();
      if (input.DO_KEYWORDS.True()) results.writeSolReject();
      results.tracemol = input.tracemol;
      results.writePdb(input.DO_XYZOUT,input.PDB);
      if (!results.getFilenames().size()) results.logTab(1,where,"No files output");
      else for (int f = 0; f < results.getFilenames().size(); f++) results.logTab(1,where,results.getFilenames()[f]);
      results.logBlank(where);
    }
    else
    {
      results.logSectionHeader(SUMMARY,"NO SOLUTIONS");
      results.setDotSol(input.MRSET);
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_PAK(InputMR_PAK& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_PAK(input,output);
}

}  //end namespace phaser
