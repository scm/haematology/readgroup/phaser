//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <cctbx/eltbx/sasaki.h>
#include <cctbx/eltbx/sasaki.h>
#include <phaser/ep_objects/ep_search.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/signal.h>
#include <phaser/main/Phaser.h>
#include <phaser/main/runPhaser.h>
#include <phaser/src/Cluster.h>
#include <phaser/src/Dfactor.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Hexagonal.h>
#include <phaser/src/Composition.h>
#include <phaser/src/ListEditing.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/RefineSAD.h>
#include <phaser/src/Solvent.h>
#include <scitbx/random.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

//have to write a comparator function because comparator for dvect3 no defined
//pair sorts on both
struct tra_comp : std::binary_function<std::pair<double,dvect3>, std::pair<double,dvect3>, bool>
  {
    tra_comp() {}
    bool operator() (const std::pair<double,dvect3>& m1, const std::pair<double,dvect3>& m2) {
        return m1.first < m2.first;
  }
};

ep_search brute_search(InputEP_SSD input,af_atom STARTSET,double SAMPLING,float1D WILSON_LLG,double WILSON_B,VarianceEP thisVAR_EP,data_ep& CRYS_DATA,std::map<std::string,cctbx::eltbx::fp_fdp> &ATOMTYPES,std::map<std::string,bool> fix_fdp,Output& output)
{
  ep_search final_atom_sets;
  unsigned nthreads = 1;

#pragma omp parallel
  {
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
#endif
  }
  int const_numSample(500);
  output.logTab(1,LOGFILE,"Scoring "+itos(const_numSample)+" randomly sampled translations");
  dvect31D randomTRA;
  scitbx::random::mersenne_twister generator(0);
  for (unsigned irand = 0; irand < const_numSample; irand++)
  {
    floatType rand1 = generator.random_double(); //[0,1]
    floatType rand2 = generator.random_double(); //[0,1]
    floatType rand3 = generator.random_double(); //[0,1]
    randomTRA.push_back(dvect3(rand1,rand2,rand3));
  }

  //Generate list of random positions outside of parallel code
  af_float List(const_numSample);
  if (nthreads > 1)
    output.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
  output.logProgressBarStart(LOGFILE,"Generating Statistics",const_numSample/nthreads);

  std::vector<Output> omp_output(nthreads);
  std::vector<af::shared<miller::index<int> > > omp_miller(nthreads);
  for (int t = 0; t < nthreads; t++)
  {
    if (nthreads > 1) omp_output[t].setLevel(SILENCE);
    omp_miller[t] = CRYS_DATA.MILLER.deep_copy();
  }


#pragma omp parallel for
  for (int t = 0; t < List.size(); t++)
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
#endif
    af_atom TESTSET = STARTSET;
    data_atom new_site;
    new_site.SCAT.site = randomTRA[t];
    new_site.ZSCORE = 0;
    new_site.SCAT.occupancy = input.FINDSITES.OCCUPANCY;
    new_site.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
    new_site.SCAT.u_iso = cctbx::adptbx::b_as_u(WILSON_B);
    new_site.SCAT.scattering_type = input.FINDSITES.TYPE;
    TESTSET.push_back(new_site);
    RefineSAD sad(input.SG_HALL,input.UNIT_CELL,
                  omp_miller[nthread],
                  CRYS_DATA().POS,CRYS_DATA().NEG,
                  input.PTNCS,
                  TESTSET,
                  input.CLUSTER_PDB,
                  input.DATABINS,
                  ATOMTYPES,
                  input.OUTLIER,
                  input.BFAC_WILS,WILSON_B,
                  input.BFAC_SPHE,
                  input.SCATTERING.FDP,
                  input.PARTIAL,
                  input.FFTvDIRECT,
                  WILSON_LLG,
                  omp_output[nthread]);
    phaser::MACS iMACS;
    iMACS.setMACS_OFF_PROTOCOL();
    sad.setVarianceEP(thisVAR_EP);
    sad.calcSigmaaData();
    sad.rejectOutliers(TESTING,omp_output[nthread]);
    sad.setFixFdp(fix_fdp);
    Minimizer minimizer;
    minimizer.run(sad,iMACS.MACRO_SAD,omp_output[nthread]);
    List[t] = -sad.targetFn();
    omp_output[nthread].logTab(1,DEBUG,"Random site #" +itos(t+1) + ": xyz= " + dvtos(randomTRA[t],'(',',',')') +  " LLG= " + dtos(List[t]));
    //std::cout << sad.getVarianceEP().unparse() << std::endl;
    if (nthread == 0) output.logProgressBarNext(LOGFILE);
  } //end pragma omp parallel
  output.logProgressBarEnd(LOGFILE);

  for (int nthread = 0; nthread < nthreads; nthread++)
  {
    output += omp_output[nthread];
  }

  PHASER_ASSERT(List.size());
  final_atom_sets.mean = mean(List);
  final_atom_sets.sigma = sigma(List);
  output.logTab(1,VERBOSE,"Mean Score (Sigma):       " + dtos(final_atom_sets.mean,2) + "   (" + dtos(final_atom_sets.sigma,5,2) + ")");
  output.logTab(1,LOGFILE,"Run brute translation search:");
  output.logTab(2,VERBOSE,"Grid Calculation");
  cctbx::uctbx::unit_cell cctbxUC = UnitCell(input.UNIT_CELL).getCctbxUC();
  cctbx::sgtbx::space_group cctbxSG = SpaceGroup(input.SG_HALL).getCctbxSG();
  Hexagonal search_points(cctbxUC,cctbxSG);
  bool isFirst(!STARTSET.size());
  if (isFirst) output.logTab(1,LOGFILE,"Full Search for first atom");
  else output.logTab(1,LOGFILE,"Full Search for second and subsequent atoms");
  SAMPLING*=2; //10;//AJM check
  search_points.setupAsuRegion(SAMPLING,isFirst);
  output.logTab(2,VERBOSE,"Orthogonal x, y and z ranges of search:");
  output.logTab(3,VERBOSE,"Sampling: " + dtos(SAMPLING));
  output.logTab(3,VERBOSE,"Min: " + dvtos(search_points.getOrthMin(),5,2));
  output.logTab(3,VERBOSE,"Max: " + dvtos(search_points.getOrthMax(),5,2));
  output.logTab(2,LOGFILE,"Search grid has "+itos(search_points.count_sites())+" points");
  output.logBlank(LOGFILE);

  int num_sites = search_points.count_sites();

  std::vector<std::vector<std::pair<double,dvect3> > > omp_tralist(nthreads);
  if (nthreads > 1)
    output.logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");
  output.logProgressBarStart(LOGFILE,"Scoring Translations",num_sites/nthreads);
  //bool first_loop = true;

#pragma omp parallel for firstprivate(search_points)
  for (int t = 0; t < num_sites; t++)
  {
    int nthread = 0;
#ifdef _OPENMP
    nthread = omp_get_thread_num();
#endif
    af_atom TESTSET = STARTSET;
    data_atom new_site;
    new_site.SCAT.site = search_points.next_site_frac(t);
    new_site.ZSCORE = 0;
    new_site.SCAT.occupancy = input.FINDSITES.OCCUPANCY;
    new_site.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
    new_site.SCAT.u_iso = cctbx::adptbx::b_as_u(WILSON_B);
    new_site.SCAT.scattering_type = input.FINDSITES.TYPE;
    TESTSET.push_back(new_site);
    RefineSAD sad(input.SG_HALL,input.UNIT_CELL,
                  omp_miller[nthread],
                  CRYS_DATA().POS,CRYS_DATA().NEG,
                  input.PTNCS,
                  TESTSET,
                  input.CLUSTER_PDB,
                  input.DATABINS,
                  ATOMTYPES,
                  input.OUTLIER,
                  input.BFAC_WILS,WILSON_B,
                  input.BFAC_SPHE,
                  input.SCATTERING.FDP,
                  input.PARTIAL,
                  input.FFTvDIRECT,
                  WILSON_LLG,
                  omp_output[nthread]);
    phaser::MACS iMACS;
    iMACS.setMACS_OFF_PROTOCOL();
    sad.setVarianceEP(thisVAR_EP);
    sad.calcSigmaaData();
    sad.rejectOutliers(LOGFILE,omp_output[nthread]);
    sad.setFixFdp(fix_fdp);
    Minimizer minimizer;
    minimizer.run(sad,iMACS.MACRO_SAD,omp_output[nthread]);
    double f = -sad.targetFn();
    omp_output[nthread].logTab(1,DEBUG,"Brute site #" +itos(t+1) + ": xyz= " + dvtos(new_site.SCAT.site,'(',',',')') +  " LLG= " + dtos(f));
    omp_tralist[nthread].push_back(std::pair<double,dvect3>(f,new_site.SCAT.site));
    int sort_interval(0);
    if (sort_interval && !(t%sort_interval))
    {
      std::sort(omp_tralist[nthread].begin(),omp_tralist[nthread].end(),tra_comp());
      std::reverse(omp_tralist[nthread].begin(),omp_tralist[nthread].end());
      if (sort_interval < omp_tralist[nthread].size()+2)
        omp_tralist[nthread].erase(omp_tralist[nthread].begin()+sort_interval,omp_tralist[nthread].end());
    }
    if (nthread == 0) output.logProgressBarNext(LOGFILE);
  } //end pragma omp parallel
  output.logProgressBarEnd(LOGFILE);

  // sum up values of auxiliary variable
  std::vector<std::pair<double,dvect3> > tralist;
  for (int nthread=0; nthread<nthreads; nthread++)
    for (int m=0; m < omp_tralist[nthread].size(); m++)
      tralist.push_back(omp_tralist[nthread][m]);

  std::sort(tralist.begin(),tralist.end(),tra_comp());
  std::reverse(tralist.begin(),tralist.end());

  PHASER_ASSERT(tralist.size());
  for (int m=0; m < tralist.size(); m++)
  {
    final_atom_sets.heights.push_back( tralist[m].first );
    final_atom_sets.sites.push_back( tralist[m].second );
  }
  final_atom_sets.top = final_atom_sets.heights[0];

  return final_atom_sets;
}

ResultSSD runEP_SSD(InputEP_SSD& input,Output output)
{
  ResultSSD results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  try
  {
    results.logHeader(LOGFILE,header(EP_SSD));
    input.Analyse(results);

    if (!input.CRYS_DATA.is_experiment_type("SAD") &&
        !! !input.CRYS_DATA.is_experiment_type("NAT"))
      throw phaser::PhaserError(phaser::FATAL,"Data not SAD data");
    //int max_t = (input.CRYS_DATA[""].ATOM_SET.size());//,1); //may be partial structure
    int max_t = (input.ATOM_SET.size());
    (max_t == 1) ?
    results.logTab(1,PROCESS,"There is 1 SAD atom refinement set"):
    results.logTab(1,PROCESS,"There are " + itos(max_t) + " SAD atom refinement sets");
    results.logBlank(LOGFILE);

    // Convert PDB orthogonal input coordinates to fractional if necessary
    UnitCell UC(input.UNIT_CELL);
    for (int t = 0; t < max_t; t++)
      for (unsigned a = 0; a < input.ATOM_SET[t].size(); a++)
      {
        if (!input.ATOM_SET[t][a].FRAC)
        {
          input.ATOM_SET[t][a].SCAT.site = UC.doOrth2Frac(input.ATOM_SET[t][a].SCAT.site);
          input.ATOM_SET[t][a].FRAC = true;
        }
      }

    data_ep CRYS_DATA = input.CRYS_DATA;
    floatType WILSON_B;
    { //memory
    results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
    SpaceGroup sg(input.SG_HALL);
    UnitCell uc(input.UNIT_CELL);
    //cell content analysis
    Solvent solvent(input.SG_HALL,input.UNIT_CELL,input.COMPOSITION);
    std::string warning = solvent.throwError();
    if (warning.size()) results.logWarning(SUMMARY,warning);
    CRYS_DATA.truncate_resolution(uc.getCctbxUC(),input.HIRES,input.LORES);
    data_refl REFLECTIONS(CRYS_DATA.MILLER,CRYS_DATA().getFmean(),CRYS_DATA().getSIGFmean(),
                          input.SG_HALL,input.UNIT_CELL);
    RefineANO refa(REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    results.logSectionHeader(phaser::LOGFILE,"ABSOLUTE SCALE");
    refa.logScale(phaser::LOGFILE,results);
    af_float correction = refa.getCorrection();
    double WilsonK = refa.absolute_scale_F();
    WILSON_B = refa.getSigmaN().WILSON_B;
    input.OUTLIER = refa.OUTLIER;
    for (int r = 0; r < correction.size(); r++)
    {
      CRYS_DATA().POS.F[r] *= WilsonK*correction[r];
      CRYS_DATA().POS.SIGF[r] *= WilsonK*correction[r];
      CRYS_DATA().NEG.F[r] *= WilsonK*correction[r];
      CRYS_DATA().NEG.SIGF[r] *= WilsonK*correction[r];
    }
    }
    results.set_data_xtal(CRYS_DATA[""]);

    if (input.PTNCS.USE)
    { //memory
    data_refl REFLECTIONS(CRYS_DATA.MILLER,CRYS_DATA().getFmean(),CRYS_DATA().getSIGFmean(),
                          input.SG_HALL,input.UNIT_CELL);
    Epsilon epsilon(REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
    {
      epsilon.findROT(input.MACRO_TNCS,results);
      epsilon.logEpsn(results);
    }
    input.PTNCS = epsilon.getPtNcs();
    }

    if (!input.CRYS_DATA().POS.Feff.size() && !input.CRYS_DATA().POS.no_data())
    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    data_refl pos(CRYS_DATA.MILLER,CRYS_DATA().POS,input.SG_HALL,input.UNIT_CELL);
    Dfactor dfactor(pos,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    CRYS_DATA().POS = dfactor;
    }
    if (!input.CRYS_DATA().NEG.Feff.size() && !input.CRYS_DATA().NEG.no_data())
    { //memory
    data_refl neg(CRYS_DATA.MILLER,CRYS_DATA().NEG,input.SG_HALL,input.UNIT_CELL);
    Dfactor dfactor(neg,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    CRYS_DATA().NEG = dfactor;
    }

    float1D WILSON_LLG;

    std::map<std::string,cctbx::eltbx::fp_fdp> ATOMTYPES;
    std::map<std::string,bool> fix_fdp;
    { //memory
    std::string atomtype = input.FINDSITES.TYPE;
    floatType dwave = 0.02; //wavelength step for near-edge test
    floatType dfdp = 0.2; //fractional change in fdp that determines edge
    if (atomtype == "RX") ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(1.0,0.0);
    else if (atomtype == "AX") ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(0.0,1.0);
    else if (input.SCATTERING.ATOMTYPE.find(atomtype) != input.SCATTERING.ATOMTYPE.end())
      ATOMTYPES[atomtype] = input.SCATTERING.ATOMTYPE[atomtype];
    else
    {
      std::string a = (atomtype == "TX") ? "Ta" : atomtype; //Ta6Br12
      cctbx::eltbx::sasaki::table sasaki(a,false,false);
      if (sasaki.is_valid())
      {
        if (!sasaki.at_angstrom(input.WAVELENGTH).is_valid())
        throw PhaserError(FATAL,"Wavelength outside range of Sasaki tables.\nUse SCATTERING to specify f' and f\"."); //should have been caught in input
        bool near_edge = false;
        {
          floatType fdp0 = sasaki.at_angstrom(input.WAVELENGTH).fdp();
          floatType fdpf = sasaki.at_angstrom(input.WAVELENGTH+dwave).fdp();
          floatType fdpb = sasaki.at_angstrom(input.WAVELENGTH-dwave).fdp();
          near_edge = std::fabs(fdpf-fdp0)/fdp0 > dfdp || std::fabs(fdpb-fdp0)/fdp0 > dfdp;
        }
        fix_fdp[atomtype] = !near_edge;
        if (near_edge) // When f' and f" not given, assume wavelength near edge should be at edge.
        {
          floatType peakwave(0.),peakfdp(-1000.);
          for (int i = -100; i <= 100; i++)
          {
            floatType testwave = input.WAVELENGTH+i*dwave/100.;
            floatType testfdp = sasaki.at_angstrom(testwave).fdp();
            if (testfdp > peakfdp)
            { peakfdp = testfdp; peakwave = testwave; }
          }
          ATOMTYPES[atomtype] = sasaki.at_angstrom(peakwave);
        }
        else ATOMTYPES[atomtype] = sasaki.at_angstrom(input.WAVELENGTH);
      }
      else
        throw PhaserError(FATAL,"Atom type " + atomtype + " not in Sasaki table.  Use SCATTERING to specify f' and f\"."); //should have been caught in input
    }
    results.logSectionHeader(LOGFILE,"ANOMALOUS SCATTERING");
    results.logUnderLine(SUMMARY,"Scattering edges");
    results.logTab(1,SUMMARY,"Wavelength = " +  dtos(input.WAVELENGTH));
    results.logTab(1,SUMMARY,"Fp = " +  dtos(ATOMTYPES[atomtype].fp()));
    results.logTab(1,SUMMARY,"Fdp = " +  dtos(ATOMTYPES[atomtype].fdp()));
    results.logBlank(SUMMARY);
    } //memory

    double HIRES(0);
    VarianceEP NULLVAR_EP,thisVAR_EP;
    {
      results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
      results.logTab(1,VERBOSE,"Wilson B = " + dtos(WILSON_B));
      results.setLevel(input.OUTPUT_LEVEL == TESTING ? TESTING : SILENCE);
      af_atom NULLSET;
      std::map<std::string,cctbx::eltbx::fp_fdp> NULLATOMTYPES;
      RefineSAD sad(input.SG_HALL,input.UNIT_CELL,CRYS_DATA.MILLER,
                    CRYS_DATA().POS,CRYS_DATA().NEG,
                    input.PTNCS,
                    NULLSET,
                    input.CLUSTER_PDB,
                    input.DATABINS,
                    NULLATOMTYPES,
                    input.OUTLIER,
                    input.BFAC_WILS,WILSON_B,
                    input.BFAC_SPHE,
                    input.SCATTERING.FDP,
                    input.PARTIAL,
                    input.FFTvDIRECT,
                    WILSON_LLG,
                    results);
      phaser::MACS iMACS;
      iMACS.setMACS_DEFAULT_PROTOCOL();
      Minimizer minimizer;
      minimizer.run(sad,iMACS.MACRO_SAD,results);
      sad.rejectOutliers(DEBUG,results);
      results.setLevel(input.OUTPUT_LEVEL);
      NULLVAR_EP = sad.getVarianceEP();
      HIRES = sad.HiRes();
      // Fill in WILSON_LLG array, compute and report total
      results.logTab(1,LOGFILE,"LL for null hypothesis = " + dtos(sad.wilsonFn(WILSON_LLG)));
      sad.logSigmaA(VERBOSE,results);
      results.logBlank(LOGFILE);
    }

    unsigned nthreads = 1;

#pragma omp parallel
  {
#ifdef _OPENMP
    nthreads = omp_get_num_threads();
#endif
  }

    results.logSectionHeader(LOGFILE,"SUBSTRUCTURE DETERMINATION");
    double SAMPLING = HIRES/4.0;
 //   double CLUSTER_DIST = 2.0*SAMPLING;
    results.logTab(1,LOGFILE,"Target Function: " + input.FINDSITES.TARGET);
    results.logTab(1,LOGFILE,"Resolution: " + dtos(HIRES,5,2));
    results.logTab(1,LOGFILE,"Sampling: " + dtos(SAMPLING,5,2) + " Angstroms");
    results.logBlank(LOGFILE);
    int start_natom(0);
    ep_solution SADSET = input.ATOM_SET;
    if (SADSET.size())
    {
      outStream where(LOGFILE);
      results.logUnderLine(where,"Seed Solutions");
      results.logTabPrintf(1,where,"%-3s %-3s %6s %6s %6s %6s %6s\n","SET","#","X","Y","Z","O","B");
      for (int k = 0; k < SADSET.size(); k++)
      {
        PHASER_ASSERT(SADSET[k].size());
        results.logTabPrintf(1,where,"%-3d %-3d %6.3f %6.3f %6.3f %6.4f %6.1f\n",
           k+1,1,
           SADSET[k][0].SCAT.site[0],
           SADSET[k][0].SCAT.site[1],
           SADSET[k][0].SCAT.site[2],
           SADSET[k][0].SCAT.occupancy,
           cctbx::adptbx::u_as_b(SADSET[k][0].SCAT.u_iso)
           );
        for (int j = 1; j < SADSET[k].size(); j++)
          results.logTabPrintf(1,where,"%-3s %-3d %6.3f %6.3f %6.3f %6.4f %6.1f\n",
           "",j+1,
           SADSET[k][j].SCAT.site[0],
           SADSET[k][j].SCAT.site[1],
           SADSET[k][j].SCAT.site[2],
           SADSET[k][j].SCAT.occupancy,
           cctbx::adptbx::u_as_b(SADSET[k][j].SCAT.u_iso)
           );
        results.logTab(1,where,"---");
      }
    }
    else if (SpaceGroup(input.SG_HALL).spcgrpnumber() == 1)
    {
      results.logSectionHeader(LOGFILE,"SSD FOR ATOM #1 OF " + itos(input.FINDSITES.NATOM));
      results.logTab(1,LOGFILE,"First atom in P1 placed at origin");
      results.logBlank(LOGFILE);
      data_atom new_atom;
      new_atom.FRAC = true;
      new_atom.ORIG = true;
      new_atom.FIXX = true;
      new_atom.SCAT.scattering_type = input.FINDSITES.TYPE;
      new_atom.SCAT.site = dvect3(0,0,0);
      new_atom.SCAT.occupancy = 1;
      new_atom.SCAT.u_iso = 0;
      new_atom.SCAT.set_use_u(/* iso */ true, /* aniso */ false);
      SADSET.resize(1); //initialize
      SADSET[0].push_back(new_atom);
      start_natom++;
      {//refine
      RefineSAD sad(input.SG_HALL,input.UNIT_CELL,
                    CRYS_DATA.MILLER,
                    CRYS_DATA().POS,CRYS_DATA().NEG,
                    input.PTNCS,
                    SADSET[0],
                    input.CLUSTER_PDB,
                    input.DATABINS,
                    ATOMTYPES,
                    input.OUTLIER,
                    input.BFAC_WILS,WILSON_B,
                    input.BFAC_SPHE,
                    input.SCATTERING.FDP,
                    input.PARTIAL,
                    input.FFTvDIRECT,
                    WILSON_LLG,
                    results);
      Minimizer minimizer;
      sad.setVarianceEP(SADSET[0].VAR);
      sad.calcSigmaaData();
      sad.setFixFdp(fix_fdp); // Fix fdp during substructure determination
      results.logTab(1,VERBOSE,"Variances going into SAD refinement");
      sad.logSigmaA(VERBOSE,results);
      sad.logAtoms(DEBUG,results);
      sad.rejectOutliers(VERBOSE,results);
      minimizer.run(sad,input.MACRO_SAD,results);
      results.logTab(1,VERBOSE,"Variances coming out of SAD refinement");
      sad.logSigmaA(VERBOSE,results);
      sad.logAtoms(VERBOSE,results);
      SADSET[0].LLG = -sad.targetFn();
      SADSET[0].VAR = sad.getVarianceEP();
      for (int i = 0; i < sad.getAtoms().size(); i++)
      {
        PHASER_ASSERT(i < SADSET[0].size());
        SADSET[0][i].SCAT = sad.getAtoms()[i];
      }
      }
    }
    af::versa<double, af::c_grid<3> > M;
    for (unsigned natom = start_natom; natom < input.FINDSITES.NATOM; natom++)
    {
      results.logSectionHeader(LOGFILE,"SSD FOR ATOM #" + itos(natom+1) + " OF " + itos(input.FINDSITES.NATOM));
      ep_solution NEXT_SADSET;
      unsigned NSET = std::max(size_t(1),SADSET.size());
      for (unsigned k = 0; k < NSET; k++)
      {
        results.logUnderLine(LOGFILE,"SET #"+itos(k+1)+" of "+itos(NSET));
        af_atom STARTSET;
        if (k < SADSET.size()) STARTSET = SADSET[k];
        results.logTabPrintf(0,LOGFILE,"%-s",STARTSET.unparse(input.CRYS_DATA().XTALID,true,1).c_str());
        results.setLevel(input.OUTPUT_LEVEL == TESTING ? TESTING : SILENCE);
        RefineSAD sad(input.SG_HALL,input.UNIT_CELL,CRYS_DATA.MILLER,
                      CRYS_DATA().POS,CRYS_DATA().NEG,
                      input.PTNCS,
                      STARTSET,
                      input.CLUSTER_PDB,
                      input.DATABINS,
                      ATOMTYPES,
                      input.OUTLIER,
                      input.BFAC_WILS,WILSON_B,
                      input.BFAC_SPHE,
                      input.SCATTERING.FDP,
                      input.PARTIAL,
                      input.FFTvDIRECT,
                      WILSON_LLG,
                      results);
        //refine variance terms, starting from last step
        if (SADSET.size())
          sad.setVarianceEP(SADSET[k].VAR);
        else
          sad.setVarianceEP(NULLVAR_EP);
        sad.setFixFdp(fix_fdp);
        sad.rejectOutliers(VERBOSE,results);
        phaser::MACS iMACS;
        Minimizer minimizer;
        minimizer.run(sad,input.MACRO_SAD,results);
        thisVAR_EP = sad.getVarianceEP();
        results.setLevel(input.OUTPUT_LEVEL);
        sad.logAtoms(LOGFILE,results);
        sad.logScat(LOGFILE,results);
        double LLG = -sad.targetFn();
        results.logTab(1,LOGFILE,"LLG for this SAD set = " + dtos(LLG));
        //Retrieve refined atoms and f" for search for next site
        STARTSET = sad.atoms;
        for (std::map<std::string,cctbx::eltbx::fp_fdp>::iterator iter = ATOMTYPES.begin(); iter != ATOMTYPES.end(); iter++)
        {
          std::string atomtype = iter->first;
          int t = sad.atomtype2t[atomtype];
          ATOMTYPES[atomtype] = cctbx::eltbx::fp_fdp(sad.AtomFp[t],sad.AtomFdp[t]);
        }
        sad.logSigmaA(VERBOSE,results);
        ep_search THIS_SEARCH = input.FINDSITES.TARGET == "BRUTE" ?
           brute_search(input,STARTSET,SAMPLING,WILSON_LLG,WILSON_B,thisVAR_EP,CRYS_DATA,ATOMTYPES,fix_fdp,results):
           sad.fast_search(input.FINDSITES,input.FIND_PEAKS,SAMPLING,M,results);
        ep_solution THIS_SADSET = THIS_SEARCH.select_and_log(input.SG_HALL,input.UNIT_CELL,HIRES,WILSON_B,STARTSET,input.FINDSITES,input.FIND_PEAKS,thisVAR_EP,results);
        NEXT_SADSET.insert(NEXT_SADSET.end(),THIS_SADSET.begin(),THIS_SADSET.end());
        //do a test for phase-o-phrenia and sites can be added en-masse
        //sad.completeSites
      } //sets of atoms
      SADSET = NEXT_SADSET;

      results.logUnderLine(LOGFILE,"Prune Duplicates");
      ListEditing edit;
      double duplicate_distance = input.FINDSITES.DIST_DUPL > 0 ? input.FINDSITES.DIST_DUPL : HIRES/5;
      results.logTab(1,LOGFILE,"Duplicate distance = " + dtos(duplicate_distance,2));
      //int1D EQUIV(SADSET.size(),-999); //xxxxxxxxxxxxxxxxxxxxxx no prune
      int1D EQUIV = edit.purge_duplicates(duplicate_distance,input.SG_HALL,input.UNIT_CELL,SADSET,&results);
      int ndel(0);
      for (int k = EQUIV.size()-1; k >= 0; k--)
        if (EQUIV[k] >= 0) ndel++;
      if (ndel == 0) results.logTab(1,LOGFILE,"No duplicate solutions found");
      else if (ndel == 1) results.logTab(1,LOGFILE,"One duplicate solution found");
      else if (ndel > 1) results.logTab(1,LOGFILE,itos(ndel)+" duplicate solutions found");
      results.logBlank(LOGFILE);

      results.logUnderLine(LOGFILE,"Find Centrosymmetry");
      double centrosymmetry_distance = input.FINDSITES.DIST_CENTRO > 0 ? input.FINDSITES.DIST_CENTRO : HIRES/5.; // fixed 3.0 is the phenix.emma default
      results.logTab(1,LOGFILE,"Centrosymmetry distance = " + dtos(centrosymmetry_distance,2));
      edit.find_centrosymmetry(centrosymmetry_distance,input.SG_HALL,input.UNIT_CELL,SADSET,&results);
      if (input.OUTPUT_LEVEL == TESTING)
      {
        for (unsigned k = 0; k < SADSET.size(); k++)
        {
          results.logUnderLine(LOGFILE,"SET #"+itos(k+1)+" of "+itos(SADSET.size()));
          results.logTabPrintf(0,LOGFILE,"%-s",SADSET[k].unparse(input.CRYS_DATA().XTALID,true,1).c_str());
          results.logBlank(LOGFILE);
          RefineSAD sad(input.SG_HALL,input.UNIT_CELL,CRYS_DATA.MILLER,
                        CRYS_DATA().POS,CRYS_DATA().NEG,
                        input.PTNCS,
                        SADSET[k],
                        input.CLUSTER_PDB,
                        input.DATABINS,
                        ATOMTYPES,
                        input.OUTLIER,
                        input.BFAC_WILS,WILSON_B,
                        input.BFAC_SPHE,
                        input.SCATTERING.FDP,
                        input.PARTIAL,
                        input.FFTvDIRECT,
                        WILSON_LLG,
                        results);
          sad.phase_o_phrenia(TESTING,results);
          results.logTab(0,TESTING,"Phase-o-phrenia input: https://cci.lbl.gov/cctbx/phase_o_phrenia.html");
          if (HIRES > 3.0)
          results.logTab(0,TESTING,"Plot will not be the same because data are lower than 3.0 Angstroms resolution");
          results.logTab(0,TESTING,dvtos(UnitCell(input.UNIT_CELL).getCctbxUC().parameters()));
          results.logTab(0,TESTING,SpaceGroup(input.SG_HALL).spcgrpname());
          results.logTab(0,TESTING,"Flagged centrosymmetric = " + btos(SADSET[k].CENTROSYMMETRIC));
          for (int j = 0; j < SADSET[k].size(); j++)
            results.logTab(0,TESTING,"Si " + dvtos(SADSET[k][j].SCAT.site,6,4));
          results.logTab(0,TESTING,"");
        }
      }
      int ncent(0);
      for (int k = SADSET.size()-1; k >= 0; k--)
        if (SADSET[k].CENTROSYMMETRIC) ncent++;
      if (ncent == 0) results.logTab(1,LOGFILE,"No centrosymmetric solutions found");
      else if (ncent == 1) results.logTab(1,LOGFILE,"One centrosymmetric solution found");
      else if (ncent > 1) results.logTab(1,LOGFILE,itos(ncent)+" centrosymmetric solutions found");
      results.logBlank(LOGFILE);


      if (SADSET.size() && SADSET[0].size()) //proxy for all
      {
        PHASER_ASSERT(SADSET.size() == EQUIV.size());
        outStream where(LOGFILE);
        results.logUnderLine(where,"Duplicate and Centrosymmetry Table");
        results.logTabPrintf(1,where,"%-3s %3s %3s %6s %6s %6s %6s %14s\n","#","=#","-o-","X","Y","Z","O","Fast-LLG");
        for (int k = 0; k < SADSET.size(); k++)
        {
          results.logTabPrintf(1,where,"%-3d %-3s %-3s %6.3f %6.3f %6.3f %6.4f %14.3f\n",
             k+1,
             EQUIV[k]>=0 ? itos(EQUIV[k]+1).c_str() :"---",
             SADSET[k].CENTROSYMMETRIC ? " o ": "   ",
             SADSET[k][0].SCAT.site[0],
             SADSET[k][0].SCAT.site[1],
             SADSET[k][0].SCAT.site[2],
             SADSET[k][0].SCAT.occupancy,
             SADSET[k].FAST_LLG);
          for (int j = 1; j < SADSET[k].size(); j++)
            results.logTabPrintf(1,where,"%-3s %-3s %-3s %6.3f %6.3f %6.3f %6.4f\n",
             "","",
             SADSET[k].CENTROSYMMETRIC ? " o ": "   ",
             SADSET[k][j].SCAT.site[0],
             SADSET[k][j].SCAT.site[1],
             SADSET[k][j].SCAT.site[2],
             SADSET[k][j].SCAT.occupancy);
          results.logTab(1,where,"---");
        }
        results.logBlank(where);
      }

      for (int k = EQUIV.size()-1; k >= 0; k--)
      if (EQUIV[k] >= 0) SADSET.erase(SADSET.begin()+k,SADSET.begin()+k+1);

      if (SADSET.size() && input.FINDSITES.TARGET != "BRUTE")
      {
        results.logUnderLine(LOGFILE,"Refinement");
        results.logTab(1,LOGFILE,"Number of substructures: " + itos(SADSET.size()));
        if (nthreads > 1)
          results.logTab(2,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
        results.logProgressBarStart(LOGFILE,"Refining and Scoring Substructures",SADSET.size()/nthreads);
        std::vector<Output> omp_results(nthreads);
        std::vector<af::shared<miller::index<int> > > omp_miller(nthreads);
        for (int n = 0; n < nthreads; n++)
        {
          omp_results[n].run_time = results.run_time;
          omp_results[n].setLevel(SILENCE);
          omp_miller[n] = CRYS_DATA.MILLER.deep_copy();
        }

#pragma omp parallel for
        for (int k = 0; k < SADSET.size(); k++)
        {
          int nthread = 0;
#ifdef _OPENMP
          nthread = omp_get_thread_num();
#endif
          RefineSAD sad(input.SG_HALL,input.UNIT_CELL,
                        omp_miller[nthread],
                        CRYS_DATA().POS,CRYS_DATA().NEG,
                        input.PTNCS,
                        SADSET[k],
                        input.CLUSTER_PDB,
                        input.DATABINS,
                        ATOMTYPES,
                        input.OUTLIER,
                        input.BFAC_WILS,WILSON_B,
                        input.BFAC_SPHE,
                        input.SCATTERING.FDP,
                        input.PARTIAL,
                        input.FFTvDIRECT,
                        WILSON_LLG,
                        omp_results[nthread]);
          Minimizer minimizer;
          sad.setVarianceEP(SADSET[k].VAR);
          sad.calcSigmaaData();
          sad.setFixFdp(fix_fdp); // Fix fdp during substructure determination
          omp_results[nthread].logTab(1,DEBUG,"Variances going into SAD refinement "+itos(k+1));
          sad.logSigmaA(DEBUG,omp_results[nthread]);
          minimizer.run(sad,input.MACRO_SAD,omp_results[nthread]);
          omp_results[nthread].logTab(1,DEBUG,"Variances coming out of SAD refinement "+itos(k+1));
          sad.logSigmaA(DEBUG,omp_results[nthread]);
          SADSET[k].LLG = -sad.targetFn();
          SADSET[k].VAR = sad.getVarianceEP();
          for (int i = 0; i < sad.getAtoms().size(); i++)
          {
            PHASER_ASSERT(i < SADSET[k].size());
            SADSET[k][i].SCAT = sad.getAtoms()[i];
          }
          if (nthread == 0) results.logProgressBarNext(LOGFILE);
        } //end pragma omp parallel
        results.logProgressBarEnd(LOGFILE);
        results.logBlank(LOGFILE);

        for (int nthread = 0; nthread < nthreads; nthread++)
          results += omp_results[nthread];

        SADSET.sort_LLG();

        results.logSectionHeader(LOGFILE,"RESULTS");
        results.logUnderLine(LOGFILE,"Refined solutions before purge");
        results.logTabPrintf(1,LOGFILE,"%-3s %14s %14s","#","LLG","Fast-LLG");
        for (int k = 0; k < SADSET.size(); k++)
        {
          results.logTabPrintf(1,LOGFILE,"%-3d %14.3f %14.3f\n",k+1,SADSET[k].LLG,SADSET[k].FAST_LLG);
        }
        if (!input.FIND_PURGE.by_all())
        {
          int start_num = SADSET.size();
          results.logUnderLine(LOGFILE,"Purge solutions according to highest LLG");
          double cutoff = SADSET.purge(input.FIND_PURGE);
          if (input.FIND_PURGE.by_number())
            results.logTab(1,LOGFILE,"Number used for purge  = " + itos(input.FIND_PURGE.get_cutoff()));
          else if (input.FIND_PURGE.by_sigma())
            results.logTab(1,LOGFILE,"Sigma used for purge  = " + dtos(input.FIND_PURGE.get_cutoff()));
          else if (input.FIND_PURGE.by_percent())
            results.logTab(1,LOGFILE,"Percent used for purge  = " + dtos(input.FIND_PURGE.get_cutoff()));
          results.logTab(1,LOGFILE,"Cutoff for acceptance = " + dtos(cutoff,1));
          int final_num = SADSET.size();
          results.logTab(1,LOGFILE,"Number of solutions stored before purge = " + itos(start_num));
          results.logTab(1,LOGFILE,"Number of solutions stored (deleted) after purge = " + itos(final_num) + " (" + itos(start_num-final_num) + ")");
          results.logBlank(LOGFILE);
        }

        SpaceGroup(input.SG_HALL).logSymm(LOGFILE,results);
        UnitCell(input.UNIT_CELL).logUnitCell(LOGFILE,results);
        results.logBlank(LOGFILE);

        for (int t = 0; t < 2; t++)
        if (SADSET.size() && SADSET[0].size()) //proxy for all
        {
          UnitCell uc(input.UNIT_CELL);
          std::string table = t?"(Orthogonal)":"(Fractional)";
          int a(t?1:3);
          results.logUnderLine(LOGFILE,"Sub-Structure Table " + table);
          results.logTabPrintf(1,LOGFILE,"%-3s %3s %6s %6s %6s   %6s %6s  %14s %14s\n","#","-o-","X","Y","Z","O","B","LLG","Fast-LLG");
          for (int k = 0; k < SADSET.size(); k++)
          {
            PHASER_ASSERT(SADSET[k].size());
            dvect3 site = t ? uc.doFrac2Orth(SADSET[k][0].SCAT.site): SADSET[k][0].SCAT.site;
            results.logTabPrintf(1,LOGFILE,"%-3d %3s %6.*f %6.*f %6.*f   %6.4f %6.1f  %14.3f %14.3f\n",
               k+1,
               SADSET[k].CENTROSYMMETRIC ? " o ": "   ",
               a,site[0],a,site[1],a,site[2],
               SADSET[k][0].SCAT.occupancy,
               cctbx::adptbx::u_as_b(SADSET[k][0].SCAT.u_iso),
               SADSET[k].LLG,
               SADSET[k].FAST_LLG);
            for (int j = 1; j < SADSET[k].size(); j++)
            {
              dvect3 site = t ? uc.doFrac2Orth(SADSET[k][j].SCAT.site): SADSET[k][j].SCAT.site;
              results.logTabPrintf(1,LOGFILE,"%-3s %3s %6.*f %6.*f %6.*f   %6.4f %6.1f\n",
               "",
               SADSET[k].CENTROSYMMETRIC ? " o ": "   ",
               a,site[0],a,site[1],a,site[2],
               SADSET[k][j].SCAT.occupancy,
               cctbx::adptbx::u_as_b(SADSET[k][j].SCAT.u_iso));
            }
            results.logTab(1,LOGFILE,"---");
          }
          results.logBlank(LOGFILE);
        }
      }
    } //find natoms

    //store the sets of solutions in the results object
    results.storeAtoms(SADSET);

    results.logSectionHeader(LOGFILE,"OUTPUT FILES");
    results.logTab(1,LOGFILE,"Number of substructure sets = " + itos(results.ATOM_SET.size()));
    if (results.ATOM_SET.size())
    results.logTab(1,LOGFILE,"Number of atoms in each substructure = " + itos(results.ATOM_SET[0].size()));
    results.logBlank(LOGFILE);
    if (input.DO_XYZOUT.True() || input.DO_KEYWORDS.True())
    {
      if (input.DO_KEYWORDS.True())
        results.writeScript(input.TITLE,input.CRYS_DATA().XTALID); //not ATOM_XTALID
      if (input.DO_XYZOUT.True())
        results.writePdb(input.TITLE,SpaceGroup(input.SG_HALL),UnitCell(input.UNIT_CELL));
    }
    if (!results.getFilenames().size()) results.logTab(1,LOGFILE,"No files output");
    else for (int f = 0; f < results.getFilenames().size(); f++) results.logTab(1,LOGFILE,results.getFilenames()[f]);
    results.logBlank(LOGFILE);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultSSD runEP_SSD(InputEP_SSD& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runEP_SSD(input,output);
}

} //phaser
