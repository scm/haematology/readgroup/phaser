//All rights reserved
#include <phaser/main/runPhaser.h>
#include <boost/logic/tribool.hpp>
#include <phaser/src/Molecule.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/eLLG.h>
#include <phaser/pod/llg_hires.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rotdist.h>
#include <phaser/mr_objects/search_array.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_GMBL(InputMR_RNP& inputGMBL,Output output)
{
  ResultMR results(output);
  results.setFiles(inputGMBL.FILEROOT,inputGMBL.FILEKILL,inputGMBL.TIMEKILL);
  results.setLevel(inputGMBL.OUTPUT_LEVEL);
  results.setMute(inputGMBL.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  try
  {
    if (success)
    { //scope for input
      std::string cards = inputGMBL.Cards();
      InputANO input(cards);
      input.setREFL_DATA(inputGMBL.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      ResultANO resultANO = runANO(input,results);
      results.setOutput(resultANO);
      success = resultANO.Success();
      if (success)
      {
        inputGMBL.SIGMAN = resultANO.getSigmaN();
        inputGMBL.SIGMAN.REFINED = true; //flag for no more refinement
      }
    }
    if (success)
    {
      std::string cards = inputGMBL.Cards();
      InputNCS input(cards);
      input.setREFL_DATA(inputGMBL.REFLECTIONS);
      input.setNORM_DATA(inputGMBL.SIGMAN);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.PDB = inputGMBL.PDB; //cannot unparse
      ResultNCS resultNCS = runNCS(input,results);
      results.setOutput(resultNCS);
      success = resultNCS.Success();
      if (success)
      {
        inputGMBL.PTNCS = resultNCS.getPtNcs();
        inputGMBL.PTNCS.REFINED = true; //flag for no more analysis
      }
    }
    if (success)
    { //scope for input
      std::string cards = inputGMBL.Cards();
      InputDFAC input(cards);
      input.setREFL_DATA(inputGMBL.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.setNORM_DATA(inputGMBL.SIGMAN);
      input.setTNCS(inputGMBL.PTNCS);
      input.PDB = inputGMBL.PDB; //cannot unparse
      ResultDFAC resultDFAC = runDFAC(input,results);
      results.setOutput(resultDFAC);
      success = resultDFAC.Success();
      if (success)
      {
        inputGMBL.REFLECTIONS = resultDFAC.getReflData();
      }
    }
    mr_solution rnp_sol;
    if (success)
    {
      inputGMBL.MACM_CHAINS.Override(false);
      inputGMBL.MACM_TFZEQ = false;
      inputGMBL.setMACM_PROTOCOL_VRMS_FIX(false);
      //inputGMBL.DO_XYZOUT.Override(false);
      results = runMR_RNP(inputGMBL,results);
      success = results.Success();
      if (success)
        rnp_sol = results.getDotSol();
    }
    inputGMBL.MACM_CHAINS.clear_Override(); //from above
    inputGMBL.MACM_CHAINS.Default(true); //for turning on GMBL refinement
    if (success && inputGMBL.MACM_CHAINS.True()) //for turning off GMBL refinement
    {
      inputGMBL.MACM_TFZEQ = false;
      inputGMBL.setMACM_PROTOCOL_VRMS_FIX(false);
      inputGMBL.setSOLU(rnp_sol);
      results = runMR_RNP(inputGMBL,results);
    }
    else if (success)
    {
      results.logHeader(LOGFILE,header(MR_GMBL));
      results.logTab(1,SUMMARY,"Gimble refinement suppressed");
      results.logBlank(SUMMARY);
      results.logTerminate(LOGFILE,false);
    }
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.setFiles(inputGMBL.FILEROOT,inputGMBL.FILEKILL,inputGMBL.TIMEKILL);
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_GMBL(InputMR_RNP& inputGMBL)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_GMBL(inputGMBL,output);
}

} //end namespace phaser

