//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/Molecule.h>
#include <phaser/io/Errors.h>
#include <phaser/lib/jiffy.h>
#include <phaser/io/ResultNMA.h>
#include <phaser/io/InputNMA.h>
#include <phaser/lib/maths.h>
#include <phaser/lib/symm_eigen.h>
#include <phaser/lib/qr.h>
#include <phaser/lib/ProjMat.h>
#include <phaser_defaults.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

void recursive_dq_array(af::shared<int> NMA_MODE,
      float2D& rmsdq,
      std::vector<dq_type>& dq,
      dq_type& v,
      int n)
{
  if (n < rmsdq.size())
    for (int idq = 0; idq < rmsdq[n].size(); idq++)
    {
      v[n].first = rmsdq[n][idq];
      v[n].second = NMA_MODE[n];
      for (int i = n+1 ; i < rmsdq.size(); i++) v[i].first = v[i].second = 0;
      if (NMA_MODE.size() == n+1) dq.push_back(v);
      int nn(n+1);
      recursive_dq_array(NMA_MODE,rmsdq,dq,v,nn);
    }
}

void ReadEigen(std::string&,TNT::Fortran_Matrix<float>&,TNT::Vector<float>&,ResultNMA&);
void WriteEigen(std::string&,std::string&,TNT::Fortran_Matrix<float>&,TNT::Vector<float>&,ResultNMA&);

ResultNMA runNMA(InputNMA& input,bool RUN_SCEDS,Output output)
{
  ResultNMA results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(RUN_SCEDS ? SCEDS : NMAXYZ));
    input.Analyse(results);

    af::shared<bool> write_pdb;
    {//memory
      int maxprinte(12);
      int minprinte(0);
      pdbIter iter = input.PDB.begin();
      results.logSectionHeader(LOGFILE,"NORMAL MODE CALCULATION");
      if (input.PDB.size() > 1) {
        results.logBlank(LOGFILE);
        results.logWarning(LOGFILE,"Modes will only be calculated for first ensemble defined");
        results.logBlank(LOGFILE);
      }
      results.logTab(1,LOGFILE,"Ensemble: \"" + iter->first + "\"");
      if (iter->second.ENSEMBLE.size() == 1) {
        results.logTab(2,LOGFILE,"  PDB: " + iter->second.ENSEMBLE[0].basename());
      }
      Molecule Coords = iter->second.Coords();
      if (Coords.nMols() > 1) //catches extra PDB files also
      {
        results.logBlank(LOGFILE);
        results.logTab(1,LOGFILE,"Ensemble contains " + itos(Coords.nMols()) + " models");
        results.logBlank(LOGFILE);
        results.logWarning(LOGFILE,"Modes will only be calculated for first model");
      }
      results.logBlank(LOGFILE);
      results.logTab(1,LOGFILE,"Interaction Radius = " + dtos(input.ELNEMO.RADIUS));
      results.logTab(1,LOGFILE,"Interaction Force Constant = " + dtos(input.ELNEMO.FORCE));
      int large_matrix(DEF_ENM_MAXB*6); //increase this every couple of years (last set in 2004)
      if (Coords.nAtoms() < DEF_ENM_MAXB)
      {
        input.ELNEMO.METHOD = "ALL";
        results.logTab(1,LOGFILE,"Number of atoms less than " + itos(DEF_ENM_MAXB));
        results.logTab(1,LOGFILE,"Use all atoms for Hessian");
      }
      //delete CA method
      if (input.ELNEMO.METHOD == "ALL")
      {
        results.logTab(1,LOGFILE,"Interactions calculated for all atoms, not using RTB method");
//natoms multiplied by 3 because there are three dimensions
        if (Coords.nAtoms() > large_matrix/3)
          results.logWarning(LOGFILE,"Large number (" + itos(large_matrix) + ") of atoms in analysis (" + itos(Coords.nAtoms()) + ").\nThe decomposition of the atomic Hessian will take a large amount of CPU time.");
        else
          results.logTab(1,LOGFILE,"Number of atoms in analysis = " + itos(Coords.nAtoms()));
        results.logBlank(LOGFILE);

        results.logUnderLine(LOGFILE,"Generating Atomic Matrix for All Atoms");
        TNT::Fortran_Matrix<float>& HA = results.U;
        Coords.atomicHessian(input.ELNEMO.RADIUS,input.ELNEMO.FORCE,HA,results);

        results.logUnderLine(LOGFILE,"Eigenvalues/Eigenvectors of Atomic Hessian");
        TNT::Vector<float> evalA;
        int N(HA.num_rows());
        if (input.EIGE_READ == "")
        {
          results.logEllipsisStart(LOGFILE,"Doing Matrix Decomposition");
          Upper_symmetric_eigenvector_solve<float>(HA,evalA);
          if (input.EIGE_WRITE) WriteEigen(input.TITLE,input.FILEROOT,HA,evalA,results);
        }
        else
        {
          results.logEllipsisStart(LOGFILE,"Reading Eigenvectors and Eigenvalues from " + input.EIGE_READ);
          ReadEigen(input.EIGE_READ,results.U,evalA,results);
          if (results.U.num_cols() != N || results.U.num_cols() != N)
          throw PhaserError(FATAL,"Matrices of Hessian and Eigenvectors read from file different sizes");
        }
        results.logEllipsisEnd(LOGFILE);
        //from now on HA called U
        results.logTab(1,LOGFILE,"Number of Eigenvalues: " + itos(evalA.size()));
        results.logTab(1,LOGFILE,"Eigenvector Matrix: " + itos(results.U.num_rows()) + " x "+itos(results.U.num_cols()));
        results.logBlank(LOGFILE);

        results.logUnderLine(DEBUG,"Eigenvalues of Atomic Hessian");
        //eval come out largest first, and we want smallest first.
        for (int e = 0; e < evalA.size(); e++)
        {
          results.logTabPrintf(0,DEBUG,"%15.7f (%3i) ",evalA[evalA.size()-1-e],e+1);
          if (!fmod(e+1.0,6)) results.logBlank(DEBUG);
        }
        results.logBlank(DEBUG);
        results.logBlank(DEBUG);

        results.logUnderLine(DEBUG,"Eigenvectors of Atomic Hessian");
        results.logTab(1,DEBUG,"U Matrix: " + itos(results.U.num_rows()) + " x "+itos(results.U.num_cols()));
        for (int e = 0; e < evalA.size() && e < maxprinte; e++)
        {
          if (evalA[evalA.size()-1-e] > 1.0e-07) //non-zero eigenvalue
          {
          results.logTab(1,DEBUG,"VECTOR " + itos(e+1) + " VALUE " + dtos(evalA[evalA.size()-1-e]));
          for (int v = 0; v < results.U.num_cols(); v++)
          {
            results.logTabPrintf(0,DEBUG," %8.4f",results.U(v+1,evalA.size()-e));
            if (!fmod(v+1.0,3)) results.logTabPrintf(0,DEBUG,", ");
            if (!fmod(v+1.0,15)) results.logBlank(DEBUG);
          }
          results.logBlank(DEBUG);
          }
        }
        results.logBlank(DEBUG);
      } //ALL
      else if (input.ELNEMO.METHOD == "RTB")
      {
        results.logTab(1,LOGFILE,"Using RTB method");
        int NRES(input.ELNEMO.NRES);
        if (NRES == 0)
        {
          results.logTab(1,LOGFILE,"Maximum number of blocks = " + itos(input.ELNEMO.MAXBLOCKS));
          int nTrace(0);
          for (unsigned a = 0; a < Coords.nAtoms(); a++) //<= for off end test
            if (Coords.isTraceAtom(0,a)) nTrace++;
          NRES = nTrace/input.ELNEMO.MAXBLOCKS + 1;
        }
        results.logTab(1,LOGFILE,"Number of residues per block = " + itos(NRES));

        std::string last_chain;
        int nblock(0),last_resnum(0),natom(0);
        int1D start_block,natom_in_block;
        bool first_atom(true);
        for (unsigned a = 0; a < Coords.nAtoms(); a++)
        {
          if (first_atom
              || Coords.card(a).ResNum >= last_resnum + NRES
              || Coords.card(a).ResNum < last_resnum //bizarre pdb file
              || Coords.card(a).Chain != last_chain)
          {
            start_block.push_back(a);
            natom_in_block.push_back(0);
            last_resnum = Coords.card(a).ResNum;
            last_chain = Coords.card(a).Chain;
            nblock++;
          }
          natom++;
          natom_in_block[nblock-1]++;
          first_atom = false;
        }

        //RTB must be able to find a rotation for the block i.e minimum 3 atoms
        for (int nb = 0; nb < natom_in_block.size(); )
          if (natom_in_block[nb] < 3)
          {
            Coords.erase(start_block[nb],natom_in_block[nb]);
            start_block.erase(start_block.begin()+nb,start_block.begin()+nb+1);
            natom_in_block.erase(natom_in_block.begin()+nb,natom_in_block.begin()+nb+1);
          }
          else nb++;
        nblock = natom_in_block.size();

//nblocks multiplied by 6 because the RTB method makes a projection of 6*nj (R-T)
        if (nblock > large_matrix*2/6)
          results.logWarning(LOGFILE,"Huge number (i.e. greater than " + itos(large_matrix*2/6) + ") of blocks in analysis (" + itos(nblock) + ").\nThe decomposition of the atomic Hessian will take a huge amount of CPU time.\nYou may wish to terminate this job.");
        else if (nblock > large_matrix/6)
          results.logWarning(LOGFILE,"Large number (i.e. greater than " + itos(large_matrix/6) + ") of blocks in analysis (" + itos(nblock) + ").\nThe decomposition of the atomic Hessian will take a large amount of CPU time.");
        else
          results.logTab(1,LOGFILE,"Number of blocks in analysis = " + itos(nblock));
        results.logBlank(LOGFILE);

        results.logUnderLine(LOGFILE,"Generating Atomic Matrix for All Atoms");
        TNT::Fortran_Matrix<float> HA;
        Coords.atomicHessian(input.ELNEMO.RADIUS,input.ELNEMO.FORCE,HA,results);

        floatType SIX(6); //make this 3 for translation only
        results.logUnderLine(LOGFILE,"Rotation-Translation Blocks");
        results.logTab(1,LOGFILE,"Number of blocks: " + itos(nblock));
        results.logTab(1,LOGFILE,"Average atoms per block: " + dtos(floatType(natom)/floatType(nblock)));
        ProjMat D;
        results.logBlank(LOGFILE);
        for (int nb = 0; nb < nblock; nb++)
        {
          //all masses taken as 1
          TNT::Fortran_Matrix<float> P(3*natom_in_block[nb],SIX);
          for (int i = 1; i <= P.num_rows(); i++)
            for (int j = 1; j <= P.num_cols(); j++)
              P(i,j) = 0;
          dvect3 CoM(0,0,0);
          for (int nj = 0; nj < natom_in_block[nb]; nj++)
            CoM += Coords.card(start_block[nb]+nj).X;
          CoM /= static_cast<floatType>(natom_in_block[nb]);
          for (int nj = 0; nj < natom_in_block[nb]; nj++)
          {
            dvect3 rdiff(Coords.card(start_block[nb]+nj).X-CoM);
            P(3*nj+1,1) = 1.0;
            P(3*nj+2,2) = 1.0;
            P(3*nj+3,3) = 1.0;
            P(3*nj+1,5) = rdiff[2];
            P(3*nj+1,6) =-rdiff[1];
            P(3*nj+2,6) = rdiff[0];
            P(3*nj+2,4) = -P(3*nj+1,5);
            P(3*nj+3,4) = -P(3*nj+1,6);
            P(3*nj+3,5) = -P(3*nj+2,6);
          }
          JAMA::QR<float> PQR(P);
          D.push_back(PQR.getQ());
        }

        {
        results.logTab(1,LOGFILE,"Projection Matrix: " + itos(D.num_rows()) + " x "+itos(D.num_cols()));
        int printSizeI(std::min(50,D.num_rows())),printSizeJ(std::min(20,D.num_cols()));
        results.logTabPrintf(0,DEBUG,"  %4s  "," ");
        for (int j = 1; j <= printSizeJ; j++)
          results.logTabPrintf(0,DEBUG,"(%4i) ",j);
        results.logBlank(DEBUG);
        for (int i = 1; i <= printSizeI; i++)
        {
          results.logTabPrintf(0,DEBUG,"(%4i) ",i);
          for (int j = 1; j <= printSizeJ; j++)
            D(i,j) ? results.logTabPrintf(0,DEBUG,"%6.3f ",D(i,j)) :
                     results.logTabPrintf(0,DEBUG,"%6s ","  ----");
          results.logBlank(DEBUG);
        }
        results.logBlank(DEBUG);
        }

        results.logEllipsisStart(LOGFILE,"Projecting Hessian");
//have to write matrix multiplication by hand because of ProjMat class
//can optimise at the same time, ignoring terms involving elements of 0
        TNT::Fortran_Matrix<float> HessianP;
        { //memory
          //HA*P
          TNT::Fortran_Matrix<float> HAP;
          {
          results.logTab(2,VERBOSE,"HA*P: " + itos(HA.num_rows()) + " x "+itos(D.num_cols()));
          PHASER_ASSERT(HA.num_cols() == D.num_rows());
          int I = HA.num_rows();
          int J = D.num_cols();
          HAP.newsize(I,J);
          for (int i=1; i<=I; i++)
          {
            for (int j=1; j<=J; j++)
            {
              HAP(i,j) = 0;
              int nb((j-1)/6);
              int mink(1+start_block[nb]*3);
              int maxk(1+start_block[nb]*3+natom_in_block[nb]*3);
              for (int k=mink; k<maxk; k++)
                HAP(i,j) += HA(i,k) * D(k,j);
            }
          }
          HA.newsize(0,0); //no longer required
        }
        { //memory
          results.logTab(2,VERBOSE,"P*HA*P: " + itos(D.num_cols()) + " x "+itos(HAP.num_cols()));
          //PT*HAP
          PHASER_ASSERT(D.num_rows() == HAP.num_rows());
          int I = D.num_cols(); //transpose
          int J = HAP.num_cols();
          HessianP.newsize(I,J);
          for (int i=1; i<=I; i++)
            for (int j=1; j<=J; j++)
            {
              HessianP(i,j) = 0;
//can optimise, ignore terms involving elements of 0
              int nb((i-1)/6);
              int mink(1+start_block[nb]*3);
              int maxk(1+start_block[nb]*3+natom_in_block[nb]*3);
              for (int k=mink; k<maxk; k++)
                HessianP(i,j) += D(k,i) * HAP(k,j);
            }
          }
        }
        results.logEllipsisEnd(LOGFILE);

        {
        results.logTab(1,LOGFILE,"Projected Hessian: " + itos(HessianP.num_rows()) + " x "+itos(HessianP.num_cols()));
        int printSizeI(std::min(50,HessianP.num_rows())),printSizeJ(std::min(20,HessianP.num_cols()));
        results.logTabPrintf(0,DEBUG,"  %4s  "," ");
        for (int j = 1; j <= printSizeJ; j++)
          results.logTabPrintf(0,DEBUG,"(%4i) ",j);
        results.logBlank(DEBUG);
        for (int i = 1; i <= printSizeI; i++)
        {
          results.logTabPrintf(0,DEBUG,"(%4i) ",i);
          for (int j = 1; j <= printSizeJ; j++)
            HessianP(i,j) ? results.logTabPrintf(0,DEBUG,"%6.1f ",HessianP(i,j)) :
                     results.logTabPrintf(0,DEBUG,"%6s ","  ----");
          results.logBlank(DEBUG);
        }
        results.logBlank(LOGFILE);
        }

        results.logUnderLine(LOGFILE,"Eigenvalues/Eigenvectors of Projected Hessian");
        TNT::Vector<float> UsubLambda;
        TNT::Fortran_Matrix<float>& Usub = HessianP; //displacement vector
        int N(HessianP.num_rows());
        if (input.EIGE_READ == "")
        {
          results.logEllipsisStart(LOGFILE,"Doing Matrix Decomposition");
          Upper_symmetric_eigenvector_solve<float>(HessianP,UsubLambda);
          //HessianP called Usub from now on
          if (input.EIGE_WRITE)
          {
            std::string MatFile(input.FILEROOT+".mat");
            WriteEigen(input.TITLE,MatFile,Usub,UsubLambda,results);
            results.setMatFile(MatFile);
          }
        }
        else
        {
          results.logEllipsisStart(LOGFILE,"Reading Eigenvectors and Eigenvalues from " + input.EIGE_READ);
          ReadEigen(input.EIGE_READ,Usub,UsubLambda,results);
          if (Usub.num_cols() != N || Usub.num_cols() != N)
          throw PhaserError(FATAL,"Matrices of Projected Hessian and Eigenvectors read from file different sizes");
        }
        results.logEllipsisEnd(LOGFILE);
        results.logBlank(LOGFILE);
        results.logTab(1,LOGFILE,"Number of Eigenvalues: " + itos(UsubLambda.size()));
        results.logTab(1,LOGFILE,"Eigenvector Matrix: " + itos(Usub.num_rows()) + " x "+itos(Usub.num_cols()));
        results.logBlank(LOGFILE);

        results.logUnderLine(DEBUG,"Eigenvalues of Projected Hessian");
        //eigenvalues come out largest first, and we want smallest first.
        int printe(std::min(UsubLambda.size(),maxprinte));
        results.logTab(1,DEBUG,"Lowest " + itos(printe));
        for (int e = minprinte; e < printe; e++)
        {
          results.logTabPrintf(0,DEBUG,"%15.7f (%3i) ",UsubLambda[UsubLambda.size()-1-e],e+1);
          if (!fmod(e+1.0,6)) results.logBlank(DEBUG);
        }
        results.logBlank(DEBUG);
        results.logBlank(DEBUG);

        results.logUnderLine(DEBUG,"Eigenvectors of Projected Hessian");
        results.logTab(1,DEBUG,"Lowest " + itos(printe) + " with non-zero eigenvalues");
        for (int e = minprinte; e < printe; e++)
        {
          if (UsubLambda[UsubLambda.size()-1-e] > 1.0e-07) //non-zero eigenvalue
          {
          results.logTab(1,DEBUG,"VECTOR " + itos(e+1) + " VALUE " + dtos(UsubLambda[UsubLambda.size()-1-e]));
          for (int v = 0; v < Usub.num_cols(); v++)
          {
            results.logTabPrintf(0,DEBUG," %8.4f",Usub(v+1,UsubLambda.size()-e));
            if (!fmod(v+1.0,3)) results.logTabPrintf(0,DEBUG,", ");
            if (!fmod(v+1.0,15)) results.logBlank(DEBUG);
          }
          results.logBlank(DEBUG);
          }
        }
        results.logBlank(DEBUG);

        results.logUnderLine(LOGFILE,"Eigenvectors in Atomic Space");
        results.logEllipsisStart(LOGFILE,"De-Projecting Hessian");
        {
        //D*Usub
        PHASER_ASSERT(D.num_cols() == Usub.num_rows());
        int I = D.num_rows();
        int J = Usub.num_cols();
        results.U.newsize(I,J);
        int nb(0);
        for (int i=1; i<=I; i++)
        {
          int ii(start_block[nb]*3+natom_in_block[nb]*3);
          if (i > ii) nb++;
//can optimise, ignore terms involving elements of 0
          int mink(6*nb+1);
          int maxk(6*(nb+1)+1);
          for (int j=1; j<=J; j++)
          {
            results.U(i,j) = 0;
            for (int k=mink; k<maxk; k++)
              results.U(i,j) += D(i,k) * Usub(k,j);
          }
        }
        Usub.newsize(0,0);
        D.clear();
        }
        results.logEllipsisEnd(LOGFILE);
        results.logBlank(LOGFILE);
        results.logTab(1,LOGFILE,"Displacement Matrix: " + itos(results.U.num_rows()) + " x "+itos(results.U.num_cols()));
        results.logBlank(LOGFILE);

        results.logUnderLine(DEBUG,"U Matrix");
        for (int e = minprinte; e < printe; e++)
        {
          results.logTab(1,DEBUG,"VECTOR " + itos(e+1));
          for (int v = 0; v < results.U.num_rows(); v++)
          {
            results.logTabPrintf(0,DEBUG," %8.4f",results.U(v+1,results.U.num_cols()-e));
            if (!fmod(v+1.0,3)) results.logTabPrintf(0,DEBUG,", ");
            if (!fmod(v+1.0,15)) results.logBlank(DEBUG);
          }
          results.logBlank(DEBUG);
        }
        results.logBlank(DEBUG);
      } //RTB

      outStream where=SUMMARY;
      results.logSectionHeader(where,"DISPLACEMENT");
      float2D rmsdq(input.NMA_MODE.size()); // = input.PERTURB_DQ;
      if (input.PERTURB_INCREMENT == "RMS")
      {
        results.logTab(1,where,"Values of dq selected by rms deviation");
        results.logTab(2,where,"RMS increment = " + dtos(input.PERTURB_RMS.STEP));
        results.logTab(1,where,"Search terminated when RMS deviation reaches limit of " + dtos(input.PERTURB_RMS.MAX) +"A");
        results.logTab(1,where,"Search direction: " + input.PERTURB_RMS.DIRECTION);

        PHASER_ASSERT(input.NMA_MODE.size());
        for (int n = 0; n < input.NMA_MODE.size(); n++)
        {
          int nmode(input.NMA_MODE[n]);
          results.logBlank(where);
          results.logUnderLine(where,"Mode " + itos(nmode));
          floatType dq(0),tol(1.0e-06),nextrms(input.PERTURB_RMS.STEP-tol); //tolerance
          bool next_dq(true);
          rmsdq[n].push_back(dq);
          results.logTab(1,where,"Unperturbed structure");
          results.logTab(2,where,"Select dq=" + dtos(dq));
          bool reverse(true);
   //find idq for the rms increment - linear relationship between dq and rms
          floatType rms(0);
          for (unsigned a = 0; a < Coords.nAtoms(); a++)
          if (Coords.isTraceAtom(0,a))
          { //just look at distances of ca
            dvect3 oldX = Coords.card(a).X;
            dvect3 newX(0,0,0);
            for (int i = 0; i < 3; i++)
              newX[i] = oldX[i] - 1.0*results.U(3*a+i+1,results.U.num_cols()-nmode-1);
            rms += ((newX-oldX)*(newX-oldX));
          }
          int nTrace(0);
          for (unsigned a = 0; a < Coords.nAtoms(); a++) //<= for off end test
            if (Coords.isTraceAtom(0,a)) nTrace++;
          rms = std::sqrt(rms/nTrace);
          floatType idq(input.PERTURB_RMS.STEP/rms);

          results.logBlank(where);
          if (input.PERTURB_RMS.DIRECTION != "BACKWARD")
            results.logTab(1,where,"Search dq forward:");
          else
          {
            results.logTab(1,where,"Search dq backward:");
            idq *= -1;
          }
          dq = idq;
          do
          {
            floatType rms(0);
            std::vector<dvect3> newCalpha;
            dvect3 last_origX;
            for (unsigned a = 0; a < Coords.nAtoms(); a++)
            if (Coords.isTraceAtom(0,a))
            {
              dvect3 origX = Coords.card(a).X;
              dvect3 newX;
              dvect3 displacement(dq*results.U(3*a+0+1,results.U.num_cols()-nmode-1),
                                  dq*results.U(3*a+1+1,results.U.num_cols()-nmode-1),
                                  dq*results.U(3*a+2+1,results.U.num_cols()-nmode-1));
              for (int i = 0; i < 3; i++)
                newX[i] = origX[i] - dq*results.U(3*a+i+1,results.U.num_cols()-nmode-1);

  //add new X to list of positions in new Coords, add to rms etc
              newCalpha.push_back(newX);
              rms += ((newX-origX)*(newX-origX));
              last_origX = origX;
            }
            int nTrace(0);
            for (unsigned a = 0; a < Coords.nAtoms(); a++) //<= for off end test
              if (Coords.isTraceAtom(0,a)) nTrace++;
            rms = std::sqrt(rms/nTrace);
    //check RMS of new protein to old protein
            if (rms >= input.PERTURB_RMS.MAX)
            {
              results.logTab(2,where,"Maximum rms deviation " + dtos(input.PERTURB_RMS.MAX) + " reached");
              goto reverse_or_end;
            }
    //write out proteins with rms in incremental amounts
            if (rms >= nextrms)
            {
              if (idq == 1) idq = dq; //speed
              rmsdq[n].push_back(dq);
              results.logTab(2,where,"Select dq=" + dtos(dq) + " rms=" + dtos(rms));
              nextrms += input.PERTURB_RMS.STEP;
            }
            goto skip_reverse_or_end;

            reverse_or_end:
            if (reverse && input.PERTURB_RMS.DIRECTION == "TOFRO")
            {
              results.logBlank(where);
              results.logTab(1,where,"Search dq backwards:");
              reverse = false;
              dq = 0; //initialize
              idq *= -1; //go looking in other direction
              nextrms = input.PERTURB_RMS.STEP-tol;
            }
            else
            {
              results.logBlank(where);
              next_dq = false;
            }

            skip_reverse_or_end:
            dq += idq;
          }
          while (next_dq);
        } //modes
      }
      else
      {
        results.logTab(1,where,"Values of dq input");
        for (int n = 0; n < input.NMA_MODE.size(); n++)
        {
          int nmode(input.NMA_MODE[n]);
          results.logBlank(where);
          results.logUnderLine(where,"Mode " + itos(nmode));
          for (int idq = 0; idq < input.PERTURB_DQ.size(); idq++)
          {
            rmsdq[n].push_back(input.PERTURB_DQ[idq]);
            results.logTab(2,where,"Select dq=" + itos(input.PERTURB_DQ[idq]));
          }
        }
      }
      results.logBlank(where);

     //make a list by "pdb #" of the dq for each mode
     //     #     mode1  mode2 ...
     //     1      -8     -2 <- dq values
     //     2      -8     -1
     //     3      -8      0
     //     4      -8      1
     //pair->first is dq, pair->second is mode
      std::vector<dq_type> dqmodes;
      dq_type v(rmsdq.size());
      for (int n = 0; n < rmsdq.size(); n++)
        std::sort(rmsdq[n].begin(),rmsdq[n].end()); //get in order
      recursive_dq_array(input.NMA_MODE,rmsdq,dqmodes,v,0);

      write_pdb.resize(dqmodes.size(),true);
      for (int r = 0; r < dqmodes.size(); r++)
      {
        int nmodes(0);
        for (int idq = 0; idq < dqmodes[r].size(); idq++)
          if (dqmodes[r][idq].first) nmodes++;  //there is a displacement
        if (nmodes > input.NMA_COMBINATION)
          write_pdb[r] = false;
        if (!nmodes && !input.NMA_ORIG) //delete the totally unpert
          write_pdb[r] = false;
      }

      results.setDqModes(dqmodes);
      results.setTitle(input.TITLE);
      results.setModlid(iter->first);
      results.setMolecule(Coords);
      results.setRms(iter->second.ENSEMBLE[0].RMSID);
    } //memory

    int nwrite_pdb(0);
    for (int r = 0; r < write_pdb.size(); r++) if (write_pdb[r]) nwrite_pdb++;
    if (RUN_SCEDS && nwrite_pdb > 1)
    {
      results.logSectionHeader(LOGFILE,"DOMAIN ANALYSIS");
      results.calcDomains(write_pdb,input.SCEDS,input.NMADDM);
    }
    else if (RUN_SCEDS)
      results.logWarning(SUMMARY,"No perturbations within allowed range, domain analysis not performed");

    if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
    outStream where = (input.DO_KEYWORDS.True() || input.DO_XYZOUT.True()) ? SUMMARY : LOGFILE;
    results.logSectionHeader(where,"OUTPUT FILES");
    if (!RUN_SCEDS && input.DO_XYZOUT.True())
      results.writePdb(write_pdb,input.DO_XYZOUT.NMA_ALL);
    if (RUN_SCEDS && input.DO_XYZOUT.True())
      results.writeDomains(input.DO_XYZOUT.NMA_ALL);
    if (input.DO_KEYWORDS.True())
      results.writeScript();
    results = results; //to concatenate the results from the results
    for (int f = 0; f < results.getFilenames().size(); f++)
      results.logTab(1,where,results.getFilenames()[f]);
    results.logBlank(where);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}


void ReadEigen(std::string& filename,TNT::Fortran_Matrix<float>& Evec, TNT::Vector<float>& Eval,ResultNMA& results)
{
  //unformatted read, use streams
  PHASER_ASSERT(filename != "");
  std::ifstream infile(const_cast<char*>(filename.c_str()));
  if (!infile) throw PhaserError(FATAL,filename + " could not be opened");
  try {
    std::string buffer;
    std::getline(infile,buffer);
    results.logTab(1,LOGFILE,buffer);
    int I(0),J(0);
    infile >> I;
    infile >> J;
    std::getline(infile,buffer);
    Eval.newsize(I);
    Evec.newsize(I,J);
    //read eigenvalues
    for (int i = 1; i < I; i++)
      infile >> Eval(i);
    std::getline(infile,buffer);
    //read eigenvectors
    for (int i = 1; i < I; i++)
    {
      for (int j = 1; j < J; j++)
        infile >> Evec(i,j);
      std::getline(infile, buffer);
    }
  }
  catch (std::exception const& err) {
  //this catches the error  basic_string::_M_check where the read runs out of memory
    results.logTab(1,DEBUG,err.what());
  }
}

void WriteEigen(std::string& TITLE,std::string& filename,TNT::Fortran_Matrix<float>& Evec, TNT::Vector<float>& Eval,ResultNMA& results)
{
  //formatted write, use printf
  FILE* outfile;
  char* cfilename = const_cast<char*>(filename.c_str());
  if ((outfile = fopen(cfilename, "w")) == 0)
    results.logWarning(SUMMARY,filename + " could not be opened for writing");
  else
  {
    fprintf(outfile,"# %s \n",TITLE.c_str());
    fprintf(outfile,"%i %i\n",Evec.num_rows(),Evec.num_cols());
    for (int i = 1; i <= Eval.size(); i++)
      fprintf(outfile,"%9.8e ",Eval(i));
    fprintf(outfile,"\n");
    for (int i = 1; i < Evec.num_rows(); i++)
    {
      for (int j = 1; j < Evec.num_cols(); j++)
        fprintf(outfile," %9.8e", Evec(i,j));
      fprintf(outfile,"\n");
    }
  }
  results.logTab(1,SUMMARY,"Eigenvectors and Eigenvalues written to " + filename);
  fclose(outfile);
}

//for python
ResultNMA runNMA(InputNMA& input,bool RUN_SCEDS)
{
  Output output; //blank
  output.setPackagePhenix();
  return runNMA(input,RUN_SCEDS,output);
}

} //end namespace phaser
