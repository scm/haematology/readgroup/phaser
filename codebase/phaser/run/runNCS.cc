//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>

namespace phaser {

ResultNCS runNCS(InputNCS& input,Output output)
{
  ResultNCS results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(NCS));
    input.Analyse(results);

    // --- spacegroup and unitcell
    UnitCell uc(input.UNIT_CELL);
    uc.logUnitCell(LOGFILE,results);


    if (!input.PTNCS.MAXNMOL && input.AUTO_SEARCH.size())
    {
      results.logSectionHeader(phaser::LOGFILE,"MAXIMUM NMOL ANALYSIS");
      double MW(0);
      std::map<std::string,int> SEARCH = input.AUTO_SEARCH.map_of_modlids();
      results.logTab(1,LOGFILE,"Stoichiometry of search components:");
      for (std::map<std::string,int>::iterator iter = SEARCH.begin(); iter != SEARCH.end(); iter++)
      {
        MW += input.PDB[iter->first].MW.total_mw();
        results.logTab(2,LOGFILE,"\"" + iter->first + "\"");
      }
      for (int s = 0; s < input.SEARCH_ZERO.size(); s++)
      {
        MW += input.PDB[input.SEARCH_ZERO[s]].MW.total_mw();
        results.logTab(2,LOGFILE,"\"" + input.SEARCH_ZERO[s] + "\"");
      }
      int NSYMM = cctbx::sgtbx::space_group(input.SG_HALL,"A1983").order_z();
      double volume = cctbx::uctbx::unit_cell(input.UNIT_CELL).volume();
      data_composition iCOMPOSITION;
      iCOMPOSITION.BY = "SOLVENT";
      iCOMPOSITION.PERCENTAGE = input.COMPOSITION.MIN_SOLVENT; //20% solvent
      double z = (volume/(NSYMM*(1.23/(1-iCOMPOSITION.PERCENTAGE))*MW)); //80% protein
      input.PTNCS.MAXNMOL = std::floor(z);
      results.logBlank(LOGFILE);
      results.logTab(1,LOGFILE,"Molecular weight of components:  " + dtos(MW));
      results.logTab(1,LOGFILE,"Volume of asymmetric unit:       " + dtos(volume/NSYMM));
      results.logTab(1,LOGFILE,"Packing to solvent content:      " + dtos(input.COMPOSITION.MIN_SOLVENT*100,0) + "%");
      results.logTab(1,LOGFILE,"Maximum automatic NMOL number:   " + itos(input.PTNCS.MAXNMOL));
      results.logBlank(LOGFILE);
    }
    else if (input.PTNCS.MAXNMOL)
    {
      results.logSectionHeader(phaser::LOGFILE,"MAXIMUM NMOL ANALYSIS");
      results.logTab(1,LOGFILE,"Maximum NMOL number (input):   " + itos(input.PTNCS.MAXNMOL));
      results.logBlank(LOGFILE);
    }

    std::vector<data_moments> moments(3);

    { //memory
    //just to get initial without anisotropy correction
    results.logSectionHeader(phaser::LOGFILE,"DATA FOR TRANSLATIONAL NCS CORRECTION");
    data_norm sigmaN;//init, not input.SIGMAN, Aniso refinement done first, start from clean sheet
    data_tncs zero_tncs;
    data_outl zero_outl;
    DataB tmp(input.REFLECTIONS,input.RESHARP,sigmaN,zero_outl,zero_tncs,input.DATABINS,
              input.COMPOSITION
             ); //use all data
    tmp.logDataStats(SUMMARY,results);
    tmp.logOutliers(LOGFILE,results);
    tmp.logMoments(" for Data",LOGFILE,true,results);
    moments[0] = tmp.getMoments();
    } //memory

    //don't check refined, need moments
    { //memory
    results.logSectionHeader(LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    if (!input.SIGMAN.REFINED)
    {
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    }
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    results.setDataA(refa);
    results.setSpaceGroup(input.SG_HALL); //doesn't depend on solution spacegroup
    refa.logMoments(" after Anisotropy Correction",LOGFILE,true,results);
    moments[1] = refa.getMoments();
    results.setDataA(refa);
    }

    Epsilon epsilon(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                   );
    epsilon.findTRA(results);
    results.PTNCS_NMOL = epsilon.PTNCS_NMOL;
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
      epsilon.findROT(input.MACRO_TNCS, results);
    input.PTNCS = epsilon.getPtNcs();
    results.setMoments(epsilon.getMoments());
    moments[2] = epsilon.getMoments();
    results.setPtNcs(input.PTNCS); //set again later after twin test when TWINNED is flagged

    int twinpercent(5); // Set between 0 and 50
    floatType twinfrac(twinpercent/100.);
    results.logSectionHeader(SUMMARY,"TWINNING");
    results.logUnderLine(SUMMARY,"tNCS/Twin Detection Table");
    results.logTabPrintf(1,SUMMARY,"%29s %16s             %12s\n","","-Second Moments-","--P-values--");
    results.logTabPrintf(1,SUMMARY,"%29s %6s %8s       %22s%2i%1s\n","","Centric","Acentric","untwinned  twin frac <",twinpercent,"%");
    results.logTabPrintf(1,SUMMARY,"%29s %-6.2f  %-8.2f\n","Theoretical for untwinned     ",3.0,2.0);
    results.logTabPrintf(1,SUMMARY,"%29s %-6.2f  %-8.2f\n","  including measurement error ",
        moments[0].expectE4_cent,moments[0].expectE4_acent);
    results.logTabPrintf(1,SUMMARY,"%29s %-6.2f  %-8.2f\n","Theoretical for perfect twin  ",2.0,1.5);
    floatType pvalue = moments[0].pvalue();
    //floatType ptwin5percent = moments[0].ptwin5percent();
    floatType ptwinnedless = moments[0].ptwinnedless(twinfrac);
    results.logTabPrintf(1,SUMMARY,"%29s %-6.2f %5.2f%3s%5.3f  %-10.3g %-10.3g\n","Initial (data as input)       ",
        moments[0].meanE4_cent,moments[0].meanE4_acent,"+/-",moments[0].sigmaE4_acent,pvalue,ptwinnedless);
    pvalue = moments[1].pvalue();
    ptwinnedless = moments[1].ptwinnedless(twinfrac);
    results.logTabPrintf(1,SUMMARY,"%29s %-6.2f %5.2f%3s%5.3f  %-10.3g %-10.3g\n","After Anisotropy Correction   ",
        moments[1].meanE4_cent,moments[1].meanE4_acent,"+/-",moments[1].sigmaE4_acent,pvalue,ptwinnedless);
    if (results.getPtNcs().use_and_present())
    {
      pvalue = moments[2].pvalue();
      ptwinnedless = moments[2].ptwinnedless(twinfrac);
      results.logTabPrintf(1,SUMMARY,"%29s %-6.2f %5.2f%3s%5.3f  %-10.3g %-10.3g\n","After Anisotropy and tNCS     ",
          moments[2].meanE4_cent,moments[2].meanE4_acent,"+/-",moments[2].sigmaE4_acent,pvalue,ptwinnedless);
    }
    else results.logTabPrintf(1,SUMMARY,"%-29s     ---n/a---\n","After Anisotropy and tNCS ");
    results.logBlank(SUMMARY);
    results.logTabPrintf(1,SUMMARY,"%20s%2i%35s","P-value < 0.01 for <",twinpercent,"% twinned is considered worth investigating");
    results.logTab(1,SUMMARY,"Resolution for Twin Analysis (85\% I/SIGI > 3): " + dtos(moments[0].hires,5,2) + "A (HiRes=" + dtos(results.HiRes(),5,2) + "A)");
    if (ptwinnedless < 0.01)
    {
      results.logWarning(SUMMARY,"Intensity moments suggest possibility of twinning. Tests based on possible twin laws will be more definitive.");
      input.PTNCS.TWINNED = true;
    }
    results.setPtNcs(input.PTNCS); //after twin test
    results.logBlank(SUMMARY);

    results.logSectionHeader(phaser::LOGFILE,"DATA AFTER TRANSLATIONAL NCS CORRECTION");
    DataB tmp(input.REFLECTIONS,input.RESHARP,input.SIGMAN,input.OUTLIER,input.PTNCS,input.DATABINS,
              input.COMPOSITION
             ); //use all data
    tmp.logDataStats(SUMMARY,results);
    tmp.logOutliers(LOGFILE,results);
    if (input.DO_INFORMATION.True()) tmp.logInfoBits(LOGFILE,true,results);

    outStream where = LOGFILE;
    results.logSectionHeader(where,"OUTPUT FILES");
    if (input.DO_HKLOUT.True())
      results.writeMtz(input.HKLIN);
    else results.logTab(1,where,"No files output");
    for (int f = 0; f < results.getFilenames().size(); f++)
      results.logTab(1,where,results.getFilenames()[f]);
    results.logBlank(where);
    //write the binary file for output if requested
    if (input.SIGMAN.FILENAME.size() && !input.SIGMAN.READ)
    {
      bool success = results.SIGMAN.write_file(tmp.MILLER);
      if (success)
         results.logAdvisory(where,"Normalization parameters written to: " + input.SIGMAN.FILENAME);
      else throw PhaserError(FILEOPEN,input.SIGMAN.FILENAME);
    }
    if (input.PTNCS.FILENAME.size() && !input.PTNCS.READ)
    {
      bool success = results.PTNCS.write_file(tmp.MILLER);
      if (success)
         results.logAdvisory(where,"tNCS parameters written to: " + input.PTNCS.FILENAME);
      else throw PhaserError(FILEOPEN,input.PTNCS.FILENAME);
    }
    results.logBlank(where);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultNCS runNCS(InputNCS& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runNCS(input,output);
}

} //end namespace phaser
