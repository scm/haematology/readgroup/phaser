//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <boost/assign.hpp>
#include <phaser/main/runPhaser.h>
#include <phaser/src/DataMR.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/SiteListXyz.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/euler.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/signal.h>
#include <phaser/lib/eLLG.h>
#include <scitbx/array_family/simple_io.h>
#include <cctbx/translation_search/fast_nv1995.h>
#include <cctbx/translation_search/fast_terms.h>
#include <cctbx/translation_search/symmetry_flags.h>
#include <cctbx/miller/expand_to_p1.h>
#include <cctbx/miller/asu.h>
#include <cctbx/maptbx/gridding.h>
#include <cctbx/sgtbx/rot_mx_info.h>
#include <cctbx/maptbx/grid_tags.h>
#include <cctbx/maptbx/peak_search.h>
#include <cctbx/maptbx/statistics.h>
#include <cctbx/maptbx/structure_factors.h>
#include <scitbx/random.h>
#include <scitbx/math/r3_rotation.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>
#include <phaser/src/Dfactor.h>
#include <phaser/src/ListEditing.h>
#include <phaser/src/MapTraceMol.h>
#include <boost/numeric/conversion/cast.hpp>
#include <phaser/cctbx_project/iotbx/ccp4_map.h>

#ifdef _OPENMP
#include <omp.h>
#endif

//#define AJM_TURN_ON_MLHL_RESCORING

namespace phaser {

ResultMR_TF runMR_FTF(InputMR_FTF& input,Output output)
{
  ResultMR_TF results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_FTF));
    input.Analyse(results);
    results.init(input.TITLE,input.NUM_TOPFILES,input.PDB); //after Analyse
    results.setUnitCell(input.UNIT_CELL);
#ifndef AJM_TURN_ON_MLHL_RESCORING
    if (input.TARGET_TRA == "PHASED") input.DO_RESCORE_TRA = OFF;
#endif

    if (!input.SIGMAN.REFINED)
    { //memory
    results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    } //memory

    if (input.PTNCS.USE)
    {
      Epsilon epsilon(input.REFLECTIONS,
                      input.RESHARP,
                      input.SIGMAN,
                      input.OUTLIER,
                      input.PTNCS,
                      input.DATABINS,
                      input.COMPOSITION
                      );
      epsilon.findTRA(results);
      if (epsilon.getPtNcs().TRA.VECTOR_SET)
        epsilon.findROT(input.MACRO_TNCS,results);
      input.PTNCS = epsilon.getPtNcs();
      input.OUTLIER = epsilon.getOutlier();
    }

    Composition composition(input.COMPOSITION);
    {//memory
    composition.set_modlid_scattering(input.PDB);
    composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS);
    if (composition.increased())
      results.logWarning(SUMMARY,composition.warning());
    input.COMPOSITION = composition;
    if (composition.increased())
    {
      std::string cards = input.Cards();
      InputCCA inputCCA(cards);
      cctbx::uctbx::unit_cell cctbxUC = UnitCell(input.UNIT_CELL).getCctbxUC();
      double fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
      inputCCA.setRESO_HIGH(fullhires);
      inputCCA.setMUTE(true);
      ResultCCA resultCCA = runCCA(inputCCA,results);
      PHASER_ASSERT(resultCCA.Success()); //cheap
      //throw same error as would be thrown by runCCA
      if (resultCCA.getFitError())
      throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
    }
    }//memory
    double TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);

    if (!input.REFLECTIONS.Feff.size())
    {
      results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
      Dfactor dfactor(input.REFLECTIONS,
                      input.RESHARP,
                      input.SIGMAN,
                      input.OUTLIER,
                      input.PTNCS,
                      input.DATABINS,
                      input.COMPOSITION
                      );
      dfactor.calcDfactor(results);
      input.REFLECTIONS = dfactor;
    }

    results.logSectionHeader(LOGFILE,"DATA FOR TRANSLATION FUNCTION");
    DataMR mr(input.REFLECTIONS,
              input.RESHARP,
              input.SIGMAN,
              input.OUTLIER,
              input.PTNCS,
              input.DATABINS,
              input.HIRES,input.LORES,
              input.COMPOSITION,
              input.ensemble);
    SigmaN sigmaN = mr.getBinSigmaN();
    bool halfR(true);
    mr.initGfunction(halfR);
    //if the ensembles are coming from mtz files, check that the high resolution of the input
    //ensembles is not lower than the resolution of the data - if so, truncate data to lower resolution
    stringset modlids = input.MRSET.set_of_modlids();
    for (unsigned k = 0; k < input.MRSET.size(); k++)
      for (unsigned e = 0; e < input.MRSET[k].RLIST.size(); e++)
        modlids.insert(input.MRSET[k].RLIST[e].MODLID);
    double HIRES = mr.HiRes();
    if (input.HIRES < 0 && (input.MRSET.get_resolution() > mr.HiRes()))
    {
      HIRES = input.MRSET.get_resolution();
      results.logTab(1,LOGFILE,"High Resolution Limit imposed by RF list = " + dtos(HIRES,5,2));
      mr.selectReso(HIRES,input.LORES);
    }
    for (stringset::iterator iter = modlids.begin(); iter != modlids.end(); iter++)
    {
      std::string m = (*iter);
      double mapHIRES(0);
      if (input.PDB[m].ENSEMBLE[0].map_format())
        mapHIRES = std::max(input.PDB[m].HIRES,mapHIRES);
      //allow for interpolation with cell expansion
      mapHIRES *= input.MRSET.max_cell_scale();
      HIRES = std::max(mapHIRES,HIRES);
    }
    if (HIRES > mr.HiRes())
    {
      results.logTab(1,LOGFILE,"High Resolution Limit imposed by Maps = " + dtos(HIRES,5,2));
      mr.selectReso(HIRES,input.LORES);
    }
    mr.logOutliers(LOGFILE,results);
    mr.logDataStats(SUMMARY,results);
    mr.logUnitCell(DEBUG,results);
    mr.logDataStatsExtra(DEBUG,20,results);
    mr.logSigmaN(DEBUG,results);
    results.logBlank(VERBOSE);

    results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
    double LLwilson(mr.WilsonLL());
    mr.logWilson(LOGFILE,LLwilson,results);
    mr.setLLwilson(LLwilson);

    bool mrset_size_input = input.MRSET.size();
    if (input.SGALT_HALL.size() > 0 && !input.MRSET.all_space_groups_set())
    {
      results.logSectionHeader(SUMMARY,"ALTERNATIVE SPACE GROUPS");
      results.logTabPrintf(1,SUMMARY,"Space Group(s) to be tested:\n",input.SGALT_HALL.size());
      for (int s = 0; s < input.SGALT_HALL.size(); s++)
      {
        SpaceGroup sgalt(input.SGALT_HALL[s]);
        results.logTabPrintf(1,SUMMARY,"  %s\n",sgalt.spcgrpname().c_str());
      }
      results.logBlank(SUMMARY);
      input.MRSET.apply_space_group_expansion(input.SGALT_HALL);
    }
    else if (input.MRSET.size())
      input.MRSET.if_not_set_apply_space_group(input.SG_HALL);

    float1D search_bfactors;
    { //memory
    cctbx::uctbx::unit_cell cctbxUC = UnitCell(input.UNIT_CELL).getCctbxUC();
    double fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
    search_bfactors = composition.get_search_bfactors(input.SG_HALL,input.UNIT_CELL,fullhires,input.MRSET);
    for (int k = 0; k < search_bfactors.size(); k++)
    {
      if (search_bfactors[k] > input.SEARCH_FACTORS.BFAC)
        output.logTab(1,DEBUG,"Default search B-factor overridden for solution set " + itos(k+1));
      search_bfactors[k] = std::max(input.SEARCH_FACTORS.BFAC,search_bfactors[k]);
    }
    }//memory

    int1D ntab(input.MRSET.size(),0);
    stringset search_modlids;
    if (!input.MRSET.size())
      results.logSectionHeader(SUMMARY,"NO SOLUTIONS");
    else
    {
      mr_solution sg_solutions;
                  sg_solutions.copy_extras(input.MRSET);
                  sg_solutions.set_resolution(mr.HiRes()); //overwrite resolution
#ifndef AJM_TURN_ON_MLHL_RESCORING
                  sg_solutions.PTF = (input.TARGET_TRA == "PHASED");
#endif
      float3D rlisttop,rlistz;
      rlisttop.resize(input.MRSET.size());
      rlistz.resize(input.MRSET.size());
      int ncomponent(0);
      const int const_numSample = 500; //may want to increase this now

      if (input.MRSET.size())
      {
        outStream where(LOGFILE);
        results.logSectionHeader(where,"ENSEMBLING");
        //there may be lots of ensembles in an OR search
        //we don't want them all in memory at the same time
        //load the ones that are extras first, extract the table information (and check)
        //and then leave the MapEnsemble object in the state for the first refinement
        for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
          iter->second.setup_and_fix_vrms(iter->first,HIRES,input.PTNCS,input.SOLPAR,results);
        mr.ensPtr()->configure(input.MRSET.set_of_modlids(),input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[0].set_of_modlids(),results);
        mr.ensPtr()->ensembling_table(LOGFILE,TOTAL_SCAT,results);
        std::string warning = input.MRSET.setup_vrms_delta(mr.ensPtr()->get_drms_vrms());
        if (warning.size())
          results.logWarning(SUMMARY,"Conflict in RMSD between ENSEMBLE and SOLUTION inputs\n" + warning);
        results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
        if (input.MRSET[0].RLIST.size())
        {
          std::string TRA_MODLID = input.MRSET[0].RLIST[0].MODLID;
          if (input.PDB.find(TRA_MODLID) != input.PDB.end() && input.PDB[TRA_MODLID].is_helix)
          {
            input.TRAN_PACKING_NUMBER = 0; //use all
            if (!input.TRAN_PACKING_USE)
            {
              results.logAdvisory(LOGFILE,"Helix: packing of top peak in TF is OFF (ON is recommended)");
              results.logBlank(LOGFILE);
            }
          }
        }
        if (input.DO_XYZOUT.True() || input.TRAN_PACKING_USE)
        { //ready for output
          if (input.tracemol == NULL)
          {
            MapTrcPtr new_ensPtr(new MapTraceMol());
            input.tracemol = new_ensPtr; //deep copy on init
          }
          input.tracemol->configure(input.MRSET.set_of_modlids(),false,false,input.PDB,results);
          input.tracemol->trace_table(LOGFILE,results);
        }
        if (input.TRAN_PACKING_USE && input.MRSET.size() && input.MRSET[0].KNOWN.size())
        { //check to see that background solutions pack, else other packing will always fail
          results.logSectionHeader(LOGFILE,"TRANSLATION PACKING");
          results.logTab(1,LOGFILE,"Translation Packing Cutoff: " + dtos(input.TRAN_PACKING_CUTOFF*100) + "%");
          if (input.MRSET.has_been_packed(input.TRAN_PACKING_CUTOFF))
          {
            results.logTab(1,LOGFILE,"All solutions have been packed");
            results.logBlank(LOGFILE);
          }
          else
          {
            results.logEllipsisStart(LOGFILE,"Packing Solutions");
            std::string cards = input.Cards();
            InputMR_PAK inputPAK(cards);
            inputPAK.setSOLU(input.MRSET);
            inputPAK.setPACK_COMP(false); //don't move first solution to origin
            //packing applied as in packing test
            inputPAK.PDB = input.PDB; //unparse does not work
            inputPAK.tracemol = input.tracemol; //unparse does not work
            inputPAK.setPACK_PRUN(false);
            inputPAK.setPACK_KEEP_HIGH_TFZ(false);
            inputPAK.setPACK_CUTO(input.TRAN_PACKING_CUTOFF);
            inputPAK.PACKING.FTF_STOP = true; //flag for not adding annotation
            inputPAK.PACKING.FTF_LIMIT = -std::numeric_limits<double>::max(); //do them all
            inputPAK.setKEYW(false);
            inputPAK.setXYZO(false);
            bool mute(!(input.NJOBS == 1 && results.level(TESTING)));
            inputPAK.setMUTE(mute);
            ResultMR resultPAK = runMR_PAK(inputPAK,results);
            if (!mute) results.setOutput(resultPAK);
            if (resultPAK.Failure()) throw resultPAK;
            for (int k = 0; k < inputPAK.MRSET.size(); k++)
              if (!inputPAK.MRSET[k].KEEP)
                inputPAK.MRSET[k].PURGE = true;
            int befor = inputPAK.MRSET.size();
            inputPAK.MRSET.pruneDuplicateSolutions();
            int after = inputPAK.MRSET.size();
            int prune = befor-after;
            input.MRSET = inputPAK.MRSET;
            results.logEllipsisEnd(LOGFILE);
            results.logBlank(LOGFILE);
            if (!input.MRSET.size())
            {
              results.logTab(1,LOGFILE,"All input solutions failed intrinsic TF packing test");
              results.logBlank(LOGFILE);
              results.logAdvisory(LOGFILE,"Input solution list fails intrinsic TF packing test\nEdit SOLUTIONS or use TRAN PACKING USE OFF");
            }
            else if (prune)
            {
              results.logTab(1,LOGFILE,itos(prune) + " input solution" + std::string(prune == 1 ? "":"s") + " failed intrinsic TF packing test");
              results.logBlank(LOGFILE);
              results.logAdvisory(LOGFILE,"At least one input solution failed intrinsic TF packing test");
            }
            else
            {
              results.logTab(1,LOGFILE,"All input solutions passed intrinsic TF packing test");
              results.logBlank(LOGFILE);
            }
          }
        }
      }

      if (!input.MRSET.size())
      {
        results.logSectionHeader(SUMMARY,"NO SOLUTIONS");
      }
      else
      {
        if (input.RFAC_USE &&
            input.TARGET_TRA != "PHASED" && //map has an origin, R-factor test in P1 does not take account of origin
            mrset_size_input == 1 &&
            (!input.PTNCS.use_and_present() || input.PTNCS.NMOL < 2) && //not looking for more than one
            input.MRSET[0].RLIST.size() == 1)
        { //tests at input orientation
          bool mr_is_necessary = true;
          double RFAC_RESO = std::max(HIRES,3.0); //not configured for min, don't want more than 3
          mr_ndim origin(input.MRSET[0].RLIST[0].MODLID,input.MRSET[0].RLIST[0].EULER);
          mr_set MRSET = input.MRSET[0];
                 MRSET.KNOWN.push_back(origin);
          double minRfac(200); //unfeasible
          std::string bestHall(input.SG_HALL);
          float1D mrRfactor;
          for (int s = 0; s < input.SGALT_HALL.size(); s++)
          {
            SpaceGroup sgalt(input.SGALT_HALL[s]);
            mr.changeSpaceGroup(input.SGALT_HALL[s],RFAC_RESO,10000);
            mr.initKnownMR(MRSET);
            mr.initSearchMR();
            mr.calcKnownVAR();
            mr.calcKnown();
            double Rfac = mr.Rfactor();
            mrRfactor.push_back(Rfac);
            if (Rfac < minRfac)
            {
              minRfac = std::min(minRfac,Rfac);
              bestHall = input.SGALT_HALL[s];
            }
          }
          mr.changeSpaceGroup(bestHall,HIRES,input.LORES); //back to beginning
          mr.init();
          results.setTopRfac(minRfac);
          if (results.getTopRfac() < input.RFAC_CUTOFF)
          {
            mr_is_necessary = false;
            results.setKnownOrigin(input.MRSET[0],bestHall,mr.likelihoodFn());
          }
          outStream where(mr_is_necessary?VERBOSE:SUMMARY);
          results.logSectionHeader(where,"R-FACTOR CHECK");
          results.logTab(1,where,"R-factor at " + dtos(RFAC_RESO,4,2) + "A calculated");
          results.logTabPrintf(1,where,"%11s  %8s\n","Space Group","R-factor");
          for (int s = 0; s < input.SGALT_HALL.size(); s++)
          {
            SpaceGroup sgalt(input.SGALT_HALL[s]);
            (mrRfactor[s] < 100) ?
            results.logTabPrintf(1,where,"%-11s  %5.2f%\n",sgalt.spcgrpname().c_str(),mrRfactor[s]):
            results.logTabPrintf(1,where,"%-11s  >100%\n",sgalt.spcgrpname().c_str());
          }
          results.logBlank(where);
          results.logTab(1,where,"Minimum R-factor of ensemble at origin in original orientation = " + dtos(minRfac,5,2));
          results.logBlank(where);
          if (!mr_is_necessary)
          {
            results.logTab(0,where,results.getSet(0).logfile(1,true),false);
            results.logBlank(where);
            goto the_end;
          }
        }

        results.logSectionHeader(LOGFILE,"TRANSLATION FUNCTIONS");
        results.logTab(1,LOGFILE,"Target Function: " + input.TARGET_TRA + " " + std::string(input.TARGET_TRA == "FAST"? input.TARGET_TRA_TYPE : ""));
        if (input.TRAN_PACKING_USE)
        {
          results.logTab(1,LOGFILE,"Translation Packing Function applied: top peak will pack");
          results.logTab(1,LOGFILE,"Translation Packing Cutoff: " + dtos(input.TRAN_PACKING_CUTOFF*100) + "%");
        }
        else
        {
          results.logTab(1,LOGFILE,"Translation Packing Function NOT applied");
        }
        if (input.TARGET_TRA != "BRUTE")
        {
          if (input.DO_RESCORE_TRA)
          {
            results.logTab(1,VERBOSE,"Raw peaks will be used for rescoring");
            results.logTab(1,VERBOSE,std::string(input.PEAKS_TRA.CLUSTER ? "Clustered" : "Raw") + " peaks will be saved after rescoring");
          }
          else
            results.logTab(1,VERBOSE,std::string(input.PEAKS_TRA.CLUSTER ? "Clustered" : "Raw") + " peaks will be saved without rescoring");
          results.logBlank(VERBOSE);
        }
        //unlike rotation function, sampling does not depend on mean radius of search ensemble, can be outside loop
        double SAMPLING = input.TRA_SAMPLING;
        if (!SAMPLING)
        {
          if (input.TARGET_TRA == "FAST" &&
              (input.TARGET_TRA_TYPE == "LETF1" || input.TARGET_TRA_TYPE == "LETF1A"))
            SAMPLING = HIRES/4.0; // 1st order approximations
          else if (input.TARGET_TRA == "FAST" && input.DO_RESCORE_TRA)
            SAMPLING = HIRES/4.0;
          else
            SAMPLING = HIRES/5.0;
        }
        PHASER_ASSERT(SAMPLING > 0);
        double clusterXyz = 2.0*SAMPLING;
        results.logTab(1,LOGFILE,"Sampling: " + dtos(SAMPLING,5,2) + " Angstroms");
        results.logBlank(LOGFILE);

        float1D all_r_mean(0),all_f_mean(0),all_r_sigma(0);
        bool advisory_ftf_packs(false),advisory_llg_packs(false);
        bool all_one_mol_in_P1 = true;
        bool at_least_one_has_deep(false);
        for (int k = 0; k < input.MRSET.size(); k++)
          if (input.MRSET[k].getNDEEP() < input.MRSET[k].RLIST.size()) //there are deep angles for this k
            at_least_one_has_deep = true;
        int maxdeep = (input.ZSCORE.USE && //otherwise don't know when to bail out
                       at_least_one_has_deep &&
                       !input.MRSET.get_deep_used()) ? 2 : 1;
        //int remaining(1); //min value has to be over one
        {//memory
        std::string TRA_MODLID = input.MRSET[0].RLIST[0].MODLID;
        if (input.PDB[TRA_MODLID].is_atom)
        {
         // remaining = std::numeric_limits<int>::max();
        }
        else
        {
          std::map<search_component,int> zsearch = input.AUTO_SEARCH.map_of_components();
          search_component TRA_COMP;
          for (std::map<search_component,int>::iterator iter = zsearch.begin(); iter != zsearch.end(); iter++)
          for (int m = 0; m < iter->first.MODLID.size(); m++)
          if (iter->first.MODLID[m] == TRA_MODLID)
          {
           // remaining = iter->second;
            TRA_COMP = iter->first;
          }
/* we could take off what is already present, but we don't know how many of these were input
 * worst case senario is that we still have all to find, in which case it will run to the bottom of the deep list
 * only finding the one remaining. This is belt and braces and old behaviour anyway.
 * Alternatively, exit after only finding one, but this is not very safe, there might be a better one
      for (int k = 0; k < input.MRSET[0].KNOWN.size(); k++)
        for (int m = 0; m < TRA_COMP.MODLID.size(); m++)
          if (TRA_COMP.MODLID[m] == input.MRSET[0].KNOWN[k].MODLID)
            remaining--;
      PHASER_ASSERT(remaining >= 0);
*/
        }
        } //memory
        double f_top = -std::numeric_limits<double>::max();
        double llg_f_top = -std::numeric_limits<double>::max();
        double z_top = -std::numeric_limits<double>::max();
        double llg_z_top = -std::numeric_limits<double>::max();
        int1D ndeep(input.MRSET.size(),0);
        for (unsigned deep = 0; deep < maxdeep; deep++)
        {
          if (deep) //there are deep angles for this k
          {
            if (sg_solutions.num_TFZ(input.ZSCORE.cutoff()) >= 1)
            {
              results.logBlank(SUMMARY);
              results.logSectionHeader(SUMMARY,"DEEP TRANSLATION FUNCTION");
              results.logTab(1,SUMMARY,"Best TFZ is over Z-score cutoff for definite solution (" + dtos(input.ZSCORE.cutoff(),2) + ")");
              results.logTab(1,SUMMARY,"Do NOT search deep in trial orientation list");
              results.logBlank(SUMMARY);
              break;
            }
            else
            {
              results.logBlank(SUMMARY);
              results.logSectionHeader(SUMMARY,"DEEP TRANSLATION FUNCTION");
              results.logTab(1,SUMMARY,"Best TFZ is NOT over Z-score cutoff for definite solution (" + dtos(input.ZSCORE.cutoff(),2) + ")");
              results.logTab(1,SUMMARY,"Search deep in trial orientation list");
              results.logBlank(SUMMARY);
            }
          }
          input.MRSET.clear_cluster();
          for (unsigned k = 0; k < input.MRSET.size(); k++)
          {
            PHASER_ASSERT(k < search_bfactors.size());
            data_bofac SEARCH_FACTORS(search_bfactors[k],input.SEARCH_FACTORS.OFAC);
            bool one_mol_in_P1(input.TARGET_TRA != "PHASED" && //maps have an origin!
                               mr.spcgrpnumber() == 1 && input.MRSET[k].KNOWN.size() == 0);
            if (!one_mol_in_P1) all_one_mol_in_P1 = false;
    /* old behaviour is to search deep regardless
     * proteasome test case
     */
            sg_solutions.incNROT();
            mr.changeSpaceGroup(input.MRSET[k].HALL,HIRES,input.LORES);
            results.logBlank(SUMMARY);
            results.logSectionHeader(SUMMARY,std::string(deep?"DEEP ":"") + "TRANSLATION FUNCTION #" + itos(k+1) + " OF " + itos(input.MRSET.size()));
            if (deep)
            {
              results.logTab(1,SUMMARY,itos(ndeep[k]) + " trial orientations in deep search");
              results.logBlank(SUMMARY);
            }
            if (input.MRSET[k].ANNOTATION.size()) results.logTab(1,VERBOSE,"ANNOTATION: " + input.MRSET[k].ANNOTATION);
            if (!input.MRSET.all_space_groups_the_same()) results.logTab(1,SUMMARY,"Space Group: "+ mr.spcgrpname());
            stringset currentEns = input.MRSET[k].set_of_modlids();
            if (input.MRSET[k].RLIST.size())
              currentEns.insert(input.MRSET[k].RLIST[0].MODLID);
            mr.ensPtr()->configure(currentEns,input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,currentEns,results);
            if (input.DO_XYZOUT.True() || input.TRAN_PACKING_USE)
            { //ready for output
              input.tracemol->configure(currentEns,false,false,input.PDB,results);
            }
            results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
            mr.initKnownMR(input.MRSET[k]); //fix are initialized
            ncomponent = input.MRSET[k].KNOWN.size()+1; //assumes all the same size - reasonable
            results.logTab(0,LOGFILE,input.MRSET[k].logfile(1,true),false);
            results.logBlank(LOGFILE);
            if (search_bfactors[k] != double(DEF_SEAR_BFAC))
            {
              results.logTab(1,VERBOSE,"B-factor of search component = " + dtos(search_bfactors[k],2));
              results.logBlank(VERBOSE);
            }
            double r_mean(0),r_sigma(0);
            float1D List(0);
            if (!deep)
            {
              if (input.MRSET[k].KNOWN.size() &&
                  input.PTNCS.use_and_present() &&
                  input.DO_TNCS_RLIST_ADD.True())
              { //orientations can be lost if same orientation is already present
                results.logTab(1,SUMMARY,"Add Unique Known Orientations to Rotation List");
                ListEditing edit;
                std::string modlid = input.MRSET[k].RLIST[0].MODLID;
                dvect31D nadd = edit.add_known_orientations_to_rlist(input.PDB,modlid,input.UNIT_CELL,HIRES,input.MRSET[k]);
                results.logTab(2,SUMMARY,itos(input.MRSET[k].KNOWN.size()) + " known positions had " + itos(nadd.size()) + " unique orientations for ensemble \"" + modlid + "\"");
                for (int i = 0; i < nadd.size(); i++) results.logTab(3,SUMMARY,"#" + itos(i+1) + " " + dvtos(nadd[i],6,3));
                results.logBlank(SUMMARY);
              }
              //resize arrays
              rlisttop[k].resize(input.MRSET[k].RLIST.size());
              rlistz[k].resize(input.MRSET[k].RLIST.size());
              for (unsigned e = 0; e < input.MRSET[k].RLIST.size(); e++)
              { rlisttop[k][e].resize(3); rlistz[k][e].resize(3); }
              input.MRSET[k].RLIST.size() == 1 ?
                results.logTab(1,SUMMARY,"This set has one trial orientation"):
                results.logTab(1,SUMMARY,"This TF set has " + itos(input.MRSET[k].RLIST.size()) + " trial orientations");
              results.logBlank(SUMMARY);
              (input.MRSET[0].RLIST[0].GRF) ?
              results.logTabPrintf(1,LOGFILE,"%-4s %-6s %6s %6s  %8s %7s %7s %7s  %-s\n","#","Euler","","","Rot-LLG","RF","RFZ","GRF","Ensemble"):
              results.logTabPrintf(1,LOGFILE,"%-4s %-6s %6s %6s  %8s %7s %7s  %-s\n","#","Euler","","","Rot-LLG","RF","RFZ","Ensemble");
              for (unsigned e = 0; e < input.MRSET[k].RLIST.size(); e++)
              {
                //store the interpolated V's for this orientation
                mr.calcSearchVAR(input.MRSET[k].RLIST[e].MODLID,halfR,SEARCH_FACTORS);
                //-------------------------------------------------
                dmat33 R = euler2matrixDEG(input.MRSET[k].RLIST[e].EULER);
                dmat33 ROT = R*mr.ensPtr(input.MRSET[k].RLIST[e].MODLID)->PR.transpose();
                //-------------------------------------------------
                mr.calcSearchROT(ROT,input.MRSET[k].RLIST[e].MODLID);
                double LLgain = mr.likelihoodFn();
                if ((maxdeep == 2 && e == input.MRSET[k].getNDEEP()) ||
                     (input.MRSET.get_deep_used() && e == input.MRSET[k].getNDEEP()))
                { results.logTab(1,LOGFILE,"----- angles below here for deep search only -----"); }
                if ((maxdeep == 2 && e >= input.MRSET[k].getNDEEP()) ||
                     (input.MRSET.get_deep_used() && e >= input.MRSET[k].getNDEEP()))
                { if (!deep) ndeep[k]++; }
                (input.MRSET[k].RLIST[e].GRF)?
                results.logTabPrintf(1,LOGFILE,"%-4i %6.1f %6.1f %6.1f  %8.1f %7.1f %7.1f %7.1f  %-s\n",
                        e+1,
                        input.MRSET[k].RLIST[e].EULER[0],input.MRSET[k].RLIST[e].EULER[1],input.MRSET[k].RLIST[e].EULER[2],
                        LLgain,input.MRSET[k].RLIST[e].RF,input.MRSET[k].RLIST[e].RFZ,
                        input.MRSET[k].RLIST[e].GRF,
                        input.MRSET[k].RLIST[e].MODLID.c_str()):
                results.logTabPrintf(1,LOGFILE,"%-4i %6.1f %6.1f %6.1f  %8.1f %7.1f %7.1f %-s\n",
                        e+1,
                        input.MRSET[k].RLIST[e].EULER[0],input.MRSET[k].RLIST[e].EULER[1],input.MRSET[k].RLIST[e].EULER[2],
                        LLgain,input.MRSET[k].RLIST[e].RF,input.MRSET[k].RLIST[e].RFZ,
                        input.MRSET[k].RLIST[e].MODLID.c_str());
              }
              if (ndeep[k])
              {
                results.logTab(2,SUMMARY,itos(input.MRSET[k].RLIST.size()-ndeep[k]) + " trial orientations in initial search");
                results.logTab(2,SUMMARY,itos(ndeep[k]) + " trial orientations in deep search");
                results.logBlank(SUMMARY);
              }
              else
                results.logBlank(LOGFILE);
              if (input.MRSET.get_deep_used())
              {
                results.logTab(1,LOGFILE,"Only deep angles will be used");
                results.logBlank(LOGFILE);
              }
              if (input.MRSET[k].KNOWN.size())
              {
                mr.initKnownMR(input.MRSET[k]);
                mr.initSearchMR();
                mr.calcKnownVAR();
                mr.calcKnown();
                double LLgain = mr.likelihoodFn();
                results.logTab(1,VERBOSE,"The LL gain with fixed solutions only is " + dtos(LLgain,2));
                results.logBlank(VERBOSE);
              }
            } //first deep
            //if this is a fast search do a quick run for sigma and mean
            std::string LAST_MODLID(" ");
            unsigned estart = (!deep) ? 0 : input.MRSET[k].getNDEEP();
            if (input.MRSET.get_deep_used()) estart = input.MRSET[k].getNDEEP();
            unsigned efinal = (!deep && !input.MRSET.get_deep_used()) ? input.MRSET[k].getNDEEP() : input.MRSET[k].RLIST.size();
            bool FAST_STATS = !(input.SEARCH_METHOD == "FULL" || input.TARGET_TRA == "BRUTE" || input.PEAKS_ROT.by_sigma());
            FAST_STATS = FAST_STATS && input.TRAN_FAST_STATS;
            if ((efinal > estart) && //there are going to be TFs
                FAST_STATS)
            {
              //work out mean and standard deviation from random sample
              //this has to be worked out before the Brute translation function
              //because the zscore is used to purge the solutions and keep the list short
              //to prevent memory exhaustion
              //need List for deep search also for no signal in TF case
              //Generate list of random orientations and  positions outside of parallel code
              int numROT = std::min(const_numSample,int(input.MRSET[k].RLIST.size())); //not getNDEEP, as this could be zero
              int numTRA = const_numSample/numROT;//integer division
              int numSample = numTRA*numROT;
              if (numSample < const_numSample) numTRA++;
              numSample = numTRA*numROT;
              List.resize(numSample);
              results.logTab(1,LOGFILE,"Scoring "+itos(numSample)+" randomly sampled orientations and translations");
              dmat331D randomROT;
              for (unsigned e = 0; e < numROT; e++)
              {
                //alpha and gamma: uniform probability over 0-360
                //gamma: 0-180 with probability proportional to sin(beta)
                dmat33 R = euler2matrixDEG(input.MRSET[k].RLIST[e].EULER);
                std::string TRA_MODLID = input.MRSET[k].RLIST[e].MODLID;
                dmat33 ROT = R*input.PDB[TRA_MODLID].PR.transpose();
                randomROT.push_back(ROT);
              }
              unsigned nthreads = 1;
#pragma omp parallel
              {
#ifdef _OPENMP
                nthreads = omp_get_num_threads();
#endif
              }
              if (nthreads > 1)
                results.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
              results.logProgressBarStart(LOGFILE,"Generating Statistics for TF SET #"+itos(k+1)+" of "+itos(input.MRSET.size()), numROT/nthreads);
              //calculate randomTRA only once, at the start
              dvect31D randomTRA;
              scitbx::random::mersenne_twister generator(0);
              for (unsigned irand = 0; irand < numSample; irand++)
              {
                double rand1 = generator.random_double(); //[0,1]
                double rand2 = generator.random_double(); //[0,1]
                double rand3 = generator.random_double(); //[0,1]
                randomTRA.push_back(dvect3(rand1,rand2,rand3));
              }
              std::string TRA_MODLID = input.MRSET[k].RLIST[0].MODLID;
#pragma omp parallel for firstprivate(mr)
              for (int r = 0; r < numROT; r++)
              {
                int nthread = 0;
#ifdef _OPENMP
                nthread = omp_get_thread_num();
#endif
                DataMR tramr(mr);
                tramr.initSearchMR();
                tramr.calcSearchVAR(TRA_MODLID,halfR,SEARCH_FACTORS);
                tramr.calcSearchTRA1(randomROT[r],TRA_MODLID);
                //store the interpolated E's for this orientation
                cmplx1D interpE = tramr.getInterpE(randomROT[r],TRA_MODLID);
                for (int t = 0; t < numTRA; t++)
                {
                  int i = r*numTRA+t;
                  PHASER_ASSERT(i < randomTRA.size());
                  tramr.calcSearchTRA2(randomTRA[i],halfR,SEARCH_FACTORS,&interpE);
                  List[i] = tramr.likelihoodFn();
                }
                if (nthread == 0) results.logProgressBarNext(LOGFILE);
              } //end pragma omp parallel
              results.logProgressBarEnd(LOGFILE);
              PHASER_ASSERT(List.size());
              r_mean = mean(List);
              r_sigma = sigma(List);
              results.logTab(1,LOGFILE,"Mean Score (Sigma):       " + dtos(r_mean,2) + "   (" + dtos(r_sigma,5,2) + ")");
              results.logBlank(LOGFILE);
            }
            results.logProgressBarStart(SUMMARY,"Translation Functions for " + itos(efinal-estart) + " trial orientations ", efinal-estart);
            for (unsigned e = estart; e < efinal; e++)
            {
    /* this stops the search as soon as a high tfz (12) is found, set higher than cutoff (8) for safety
     * only happens when looking for one left
     * Brutal stopping test, for "speed" use 1 rather than remaining - is it faster to do another RF?
     * No amalgamation will occur -
     * unless there are multiple values between tfz cutoff and tfz high cutoff
    */
              PHASER_ASSERT(e < rlisttop[k].size());
              ntab[k] = e+1;
              std::string TRA_MODLID = input.MRSET[k].RLIST[e].MODLID;
              search_modlids.insert(TRA_MODLID);
              std::string deepstr = " (TRIAL #" + itos(e+1-estart) + " of " + itos(efinal-estart) + " deep)";
              std::string SetTrial = "SET #"+itos(k+1)+" of "+itos(input.MRSET.size())+" TRIAL #"+itos(e+1)+" of "+itos(efinal) + std::string(deep? deepstr :"");
              results.logUnderLine(LOGFILE,SetTrial);
              results.logTabPrintf(1,LOGFILE,"Ensemble %s Euler %-21s\n",
                      TRA_MODLID.c_str(),
                      dvtos(input.MRSET[k].RLIST[e].EULER,6,2).c_str());
              results.logBlank(LOGFILE);
              //-------------------------------------------------
              dmat33 R = euler2matrixDEG(input.MRSET[k].RLIST[e].EULER);
              dmat33 ROT = R*input.PDB[TRA_MODLID].PR.transpose();
              //-------------------------------------------------
              //log fractional composition to logfile for first or different search model
              //outStream whereFrac = (e == 0 || LAST_MODLID != TRA_MODLID) ? LOGFILE : DEBUG;
              LAST_MODLID = TRA_MODLID;
              bool check_centrosymmetry(input.PDB[TRA_MODLID].is_atom);
              SiteListXyz tralist(mr,mr,check_centrosymmetry);
              tralist.set_cluster_dist(clusterXyz);
              if (!one_mol_in_P1)
              {
                if (input.MRSET[k].KNOWN.size()) //probably needed
                {
                  mr.calcKnownVAR();
                  mr.calcKnown();
                  mr.initSearchMR();
                }
                if (input.TARGET_TRA != "BRUTE") //fast or phased search
                { //fft memory
                  cctbx::uctbx::unit_cell UCell = mr.getCctbxUC();
                  cctbx::sgtbx::space_group SgOps = mr.getCctbxSG();
                  cctbx::sgtbx::space_group_type SgInfo(SgOps);
                  bool isIsotropicSearchModel(false);
                  bool haveFpart = mr.haveFpart(); //depends on known contribution
                  bool FriedelFlag(false);
                  af_float Fobs = mr.getEnat();
                  af_cmplx Fpart = mr.getEpart();
                  typedef af::shared<miller::index<> > MillerIndexArrayType;
                  MillerIndexArrayType MillerIndicesSel = mr.getSelectedMiller();
                  double d_min = cctbx::uctbx::d_star_sq_as_d(
                    UCell.max_d_star_sq(MillerIndicesSel.const_ref()));
                  MillerIndexArrayType MillerIndicesSelP1 =
                      miller::expand_to_p1_indices(
                        SgOps,!FriedelFlag,MillerIndicesSel.const_ref());
                  af::int3 one_one_one(1,1,1);
                  int max_prime(5);
                  bool assert_shannon_sampling(true);
                  results.logTab(1,DEBUG,"Resoution: " + dtos(HIRES,5,2));
                  double resolution_factor(SAMPLING/HIRES);
                  results.logTab(1,DEBUG,"Resolution factor: "+dtos(resolution_factor));
                  //cctbx assert resolution_factor <= 0.5
                  if (resolution_factor > 0.5)
                    results.logWarning(DEBUG,"TF resolution_factor (sampling/hires) reduced from " + dtos(resolution_factor) + " to 0.5 for cctbx");
                  resolution_factor = std::min(0.5,resolution_factor);
                  results.logBlank(DEBUG);
                  af::versa<double, af::c_grid<3> > tsmap;
                  cctbx::maptbx::grid_tags<long> tags;
                  results.logEllipsisStart(LOGFILE,"Doing Fast Translation Function FFT");
                  af::int3 gridding;
                  if (input.TARGET_TRA != "PHASED")
                  {
                    cctbx::translation_search::symmetry_flags sym_flags(isIsotropicSearchModel,haveFpart);
                    gridding = cctbx::maptbx::determine_gridding(
                      UCell, d_min, resolution_factor,sym_flags,SgInfo,
                      one_one_one,max_prime,assert_shannon_sampling);
                    af::c_grid<3> grid_target(gridding);
                    results.logTab(1,VERBOSE,"grid target: "+itos(grid_target[0])+" "+itos(grid_target[1])+" "+itos(grid_target[2]));
                    results.logTab(1,VERBOSE,"grid points: "+itos(grid_target[0]*grid_target[1]*grid_target[2]));
                    try {
                      tags = cctbx::maptbx::grid_tags<long>(grid_target);
                    }
                    catch (std::exception const& err) {
                      throw PhaserError(FATAL,"Can not allocate memory for fft: reduce resolution or sampling");
                    }
                    //Output
                    results.logTab(1,VERBOSE,"use_space_group_symmetry: " + btos(sym_flags.use_space_group_symmetry()));
                    results.logTab(1,VERBOSE,"use_normalizer_k2l: " + btos(sym_flags.use_normalizer_k2l()));
                    results.logTab(1,VERBOSE,"use_seminvariants: " +  btos(sym_flags.use_seminvariants()));
                    if (haveFpart) results.logTab(1,VERBOSE,"Using Fpart");
                    else results.logTab(1,VERBOSE,"No Fpart");
                    results.logBlank(VERBOSE);
                    double sum_Fm_sqr(0);
                    if (input.TARGET_TRA == "FAST" && input.TARGET_TRA_TYPE == "CORRELATION")
                    {
                      af_cmplx P1Fcalc = mr.getFcalc(ROT,TRA_MODLID,MillerIndicesSelP1,halfR,SEARCH_FACTORS,sum_Fm_sqr);
                      tsmap = cctbx::translation_search::fast_nv1995<double>(
                        gridding,
                        SgOps,
                        !FriedelFlag,
                        MillerIndicesSel.const_ref(),
                        Fobs.const_ref(),
                        Fpart.const_ref(),
                        MillerIndicesSelP1.const_ref(),
                        P1Fcalc.const_ref()).target_map();
                    }
                    else if (input.TARGET_TRA == "FAST")
                    {
                      double LLconst(0);
                      af_cmplx P1Fcalc =  mr.getFcalc(ROT,TRA_MODLID,MillerIndicesSelP1,halfR,SEARCH_FACTORS,sum_Fm_sqr);
                      cctbx::translation_search::fast_terms<double> fast_terms(
                        gridding,
                        !FriedelFlag,
                        MillerIndicesSelP1.const_ref(),
                        P1Fcalc.const_ref());
                      af_float mI,mIsqr;
                      bool use_rot = (input.TARGET_TRA_TYPE == "LETF1" || input.TARGET_TRA_TYPE == "LETF2");
                      if (input.TARGET_TRA_TYPE == "LETF1" || input.TARGET_TRA_TYPE == "LETF1A")
                      {
                        LLconst = mr.m_LETF1(ROT,TRA_MODLID,use_rot,mI,halfR,SEARCH_FACTORS);
                      }
                      else if (input.TARGET_TRA_TYPE == "LETF2" || input.TARGET_TRA_TYPE == "LETF2A")
                      {
                        LLconst = mr.m_LETF2(ROT,TRA_MODLID,use_rot,mI,mIsqr,halfR,SEARCH_FACTORS);
                      }
                      else throw PhaserError(FATAL,"Unknown fast TF target type: " + input.TARGET_TRA_TYPE);
                      LLconst -= LLwilson;
                      results.logTab(1,VERBOSE,"Translation-independent constant to add to FFT result: " + dtos(LLconst,2));
                      // Ralf's fast_terms.summation multiplies amplitudes by number of
                      // lattice centering operations, but we only consider primitive
                      // symmetry operators, so have to compensate.
                      bool squared_flag(false);
                      tsmap = fast_terms.summation(
                          SgOps,
                          MillerIndicesSel.const_ref(),
                          mI.const_ref(),
                          Fpart.const_ref(),
                          squared_flag).fft().accu_real_copy();
                      if (input.TARGET_TRA_TYPE == "LETF2" || input.TARGET_TRA_TYPE == "LETF2A")
                      {
                        squared_flag = true;
                        af::versa<double, af::c_grid<3> > tsmap_Isqr(fast_terms.summation(
                            SgOps,
                            MillerIndicesSel.const_ref(),
                            mIsqr.const_ref(),
                            Fpart.const_ref(),
                            squared_flag).fft().accu_real_copy());
                        for (int i = 0; i < tsmap.size(); i++) tsmap[i] = tsmap[i] + tsmap_Isqr[i] + LLconst;
                      }
                      else
                        for (int i = 0; i < tsmap.size(); i++) tsmap[i] = tsmap[i] + LLconst;
                    }
                    tags.build(SgInfo,sym_flags);
                  }
                  else if (input.TARGET_TRA == "PHASED")
                  {
                    if (input.PTNCS.use_and_present()) results.logAdvisory(LOGFILE,"PHASED TF target doesn't use PTNCS corrections");
                    cctbx::sgtbx::search_symmetry_flags sym_flags(true,1);
                    cctbx::sgtbx::space_group SgOpsCent("P 1");
                    SgOpsCent.expand_conventional_centring_type(SgOps.conventional_centring_type_symbol());
                    tralist.setSpaceGroup(SgOpsCent);
                    gridding = cctbx::maptbx::determine_gridding(
                      UCell, d_min, resolution_factor,sym_flags,SgOpsCent.type(),
                      one_one_one,max_prime,assert_shannon_sampling);
                    af::c_grid<3> grid_target(gridding);
                    try {
                      tags = cctbx::maptbx::grid_tags<long>(grid_target);
                    }
                    catch (std::exception const& err) {
                      throw PhaserError(FATAL,"Can not allocate memory for fft: reduce resolution or sampling");
                    }
                    af_cmplx Fcalc = mr.getFcalcPhsTF(ROT,TRA_MODLID,SEARCH_FACTORS);
                    af_cmplx Fphi = mr.getFobsPhsTF();
                    miller::expand_to_p1_complex<double> hklFphi(SgOps,!FriedelFlag,MillerIndicesSel.const_ref(),Fphi.const_ref());
                    Fphi =  hklFphi.data;
                    //paranoia check that indices agree
                    af::shared<miller::index<int> > hkl =  hklFphi.indices;
                    PHASER_ASSERT(hkl.size() ==  MillerIndicesSelP1.size());
                    for (int r = 0; r < hkl.size(); r++) PHASER_ASSERT(hkl[r] == MillerIndicesSelP1[r]);
                    af_cmplx coeffs(Fcalc.size());
                    double scaleC(0),scaleO(0);
                    for (int r = 0; r < Fcalc.size(); r++) scaleC += std::norm(Fcalc[r]);
                    for (int r = 0; r < Fcalc.size(); r++) scaleO += std::norm(Fphi[r]);
                    double scale = 100/std::sqrt(2*scaleC*scaleO); //percent correlation
                    for (int r = 0; r < Fcalc.size(); r++) coeffs[r]= scale*std::conj(Fcalc[r])*Fphi[r];
                    scitbx::fftpack::real_to_complex_3d<double> rfft(gridding);
                    bool anomalous_flag(false);
                    bool conjugate_flag(true);
                    af::c_grid_padded<3> map_grid(rfft.n_complex());
                    cctbx::maptbx::structure_factors::to_map<double> gradmap(
                      SgOpsCent,
                      anomalous_flag,
                      MillerIndicesSelP1.const_ref(),
                      coeffs.const_ref(),
                      rfft.n_real(),
                      map_grid,
                      conjugate_flag);
                    af::ref<cmplxType, af::c_grid<3> > gradmap_fft_ref(
                      gradmap.complex_map().begin(),
                      af::c_grid<3>(rfft.n_complex()));
                    rfft.backward(gradmap_fft_ref);
                    af::versa<double, af::flex_grid<> > real_map(
                      gradmap.complex_map().handle(),
                      af::flex_grid<>(af::adapt((rfft.m_real())))
                            .set_focus(af::adapt(rfft.n_real())));
                    cctbx::maptbx::unpad_in_place(real_map);
                    tsmap = af::versa<double, af::c_grid<3> >(real_map, af::c_grid<3>(rfft.n_real()));
                    tags.build(SgOpsCent.type(),sym_flags);
                  }
                  else throw PhaserError(FATAL,"Unknown TF target: " + input.TARGET_TRA);
                  results.logEllipsisEnd(LOGFILE);
                  results.logBlank(LOGFILE);
                  //work out mean and sigma for total search
                  results.logTab(1,VERBOSE,"Mean and Standard Deviation (fast search):");
                  cctbx::maptbx::statistics<double> stats(
                    af::const_ref<double, af::flex_grid<> >(
                      tsmap.begin(),
                      tsmap.accessor().as_flex_grid()));
                  double f_mean = stats.mean();
                  double f_sigma = stats.sigma();
                  double f_max = stats.max();
                  //double z_max = signal(f_max,f_mean,f_sigma);
                  results.logTab(2,VERBOSE,"Mean Score (Sigma):      " + dtos(f_mean,2) + "   (" + dtos(f_sigma,5,2) + ")");
                  results.logTab(2,VERBOSE,"Highest Score (Z-score): " + dtos(f_max,2) + "   (" + dtos(signal(f_max,f_mean,f_sigma),5,2) + ")");
                  results.logTab(2,VERBOSE,"Lowest Score (Z-score):  " + dtos(stats.min(),2) + "   (" + dtos(signal(stats.min(),f_mean,f_sigma),5,2) + ")");
                  results.logBlank(VERBOSE);
                  if (input.TRAN_MAPS) //special output for Isabel
                  {
                    af::const_ref<std::string> labels(0,0);
                    std::string HKLOUT = results.Fileroot()+"."+stolo(TRA_MODLID)+"."+itos(k+1)+"."+itos(e+1)+"."+stolo(input.TARGET_TRA_TYPE)+".map";
                    af::int3 gridding_first(0,0,0);
                    af::int3 gridding_last = gridding;
                    af::const_ref<double, af::c_grid_padded_periodic<3> > map_data(
                       tsmap.begin(),
                         af::c_grid_padded_periodic<3>(tsmap.accessor()));
                    cctbx::sgtbx::space_group SgOpsP1("P1");
                    iotbx::ccp4_map::write_ccp4_map_p1_cell(
                      HKLOUT,
                      UCell,
                      SgOpsP1,
                      gridding_first,
                      gridding_last,
                      map_data,
                      labels);
                    results.logTab(2,LOGFILE,"Translation Function Map written to: " + HKLOUT);
                    results.logBlank(LOGFILE);
                  }
                  { //memory scope for the large peak_list arrays, temporary array of FTF peaks
                  results.logEllipsisStart(VERBOSE,"Finding symmetry equivalents");
                  af::const_ref<double, af::c_grid_padded<3> >
                    tsmap_const_ref(tsmap.begin(),
                           af::c_grid_padded<3>(tsmap.accessor()));
                  af::ref<long, af::c_grid<3> >
                    tag_array_ref(tags.tag_array().begin(),
                                  af::c_grid<3>(tags.tag_array().accessor()));
                  if (!tags.verify(tsmap_const_ref)) throw PhaserError(FATAL,"WARNING: invalid symmetry flags");
                  int peak_search_level = 1;
                  std::size_t max_peaks = 0;
                  bool interpolate = true;
                  cctbx::maptbx::peak_list<> peak_list(
                    tsmap_const_ref, tag_array_ref,
                    peak_search_level, max_peaks, interpolate);
                  std::size_t n_unique_points = peak_list.size();
                  af::const_ref<scitbx::vec3<double> > peak_sites = peak_list.sites().const_ref();
                  af::const_ref<double> peak_heights = peak_list.heights().const_ref();
    //peak_heights come out as a sorted list!
    //for (int t = 1; t < peak_heights.size(); t++) PHASER_ASSERT(peak_heights[t-1] >= peak_heights[t]);
                  results.logEllipsisEnd(VERBOSE);
                  results.logTab(1,VERBOSE,itos(n_unique_points) + " unique interpolated peaks in FTF");
                  results.logBlank(VERBOSE);
                  unsigned maxTralistSize(n_unique_points);
                  bool memory_conservation(false);
                  find_memory_size:
                  try {
                    tralist.Reserve(maxTralistSize);
                  }
                  catch (std::exception const& err) {
                  //this catches the error  St9bad_alloc where it runs out of memory
                    memory_conservation = true;
                    maxTralistSize -= 1000;
                    results.logTab(1,DEBUG,"Translation file list reduced to " +itos(maxTralistSize) + " grid points");
                    if (maxTralistSize > 0) goto find_memory_size;
                    else throw PhaserError(FATAL,"Memory exhausted for translation function list");
                  }
                  double mem_quotient(2);
                  if (memory_conservation)
                  {
                    results.logTab(1,DEBUG,"Maximum memory allocation " +itos(maxTralistSize) + " grid points");
                    results.logTab(1,DEBUG,dtos(1/mem_quotient) + " of this allowed memory will be used");
                    results.logTab(1,DEBUG,itos(maxTralistSize/mem_quotient)+" out of " + itos(n_unique_points) + " requested grid points (" + dtos(static_cast<float>(100*maxTralistSize/mem_quotient)/static_cast<float>(n_unique_points)) +"%) will be stored in memory");
                  }
                  else
                  {
                    results.logTab(1,DEBUG,"All possible "+itos(maxTralistSize)+" grid points will be stored in memory");
                  }
                  tralist.Resize(0);
                  results.logEllipsisStart(VERBOSE,"Storing points");
                  for (std::size_t i = 0; i < maxTralistSize; i++)
                  {
                    dvect3 TRA(peak_sites[i]);
                    xyzsite traSite;
                    traSite.setOrthXYZ(TRA);
                    traSite.value = peak_heights[i];
                    tralist.push_back(traSite);
                  }
                  results.logEllipsisEnd(VERBOSE);
                  {
                  results.logEllipsisStart(VERBOSE,"Converting to fractional coordinates");
                  cctbx::sgtbx::search_symmetry_flags flags(true,0,true);
                  cctbx::sgtbx::structure_seminvariants semis(SgOps);
                  cctbx::sgtbx::search_symmetry ssym(flags,SgInfo,semis);
                  af::tiny<bool, 3> isshift;
                  if (ssym.continuous_shifts_are_principal()) isshift = ssym.continuous_shift_flags();
                  else isshift.fill(false);
                  if (!haveFpart && (input.TARGET_TRA != "PHASED")) //maps have origin
                  {
                    if (isshift[0]) results.logTab(1,VERBOSE,"Continuous shift in X - translation set to 0");
                    if (isshift[1]) results.logTab(1,VERBOSE,"Continuous shift in Y - translation set to 0");
                    if (isshift[2]) results.logTab(1,VERBOSE,"Continuous shift in Z - translation set to 0");
                  }
                  dvect3 iOrthT,eOrthT,TRA;
                  for (unsigned i = 0; i < tralist.Size(); i++)
                  {
                    dvect3 fracSite = tralist(i).getOrthXYZ();
                    iOrthT = mr.doFrac2Orth(fracSite);
                    eOrthT = iOrthT + ROT*input.PDB[TRA_MODLID].PT; //plus is correct
                    TRA = mr.doOrth2Frac(eOrthT);
                    for (int j = 0; j < 3; j++)
                    {
                      while (TRA[j] < 0) TRA[j] += 1.0;
                      while (TRA[j] >= 1.0) TRA[j] -= 1.0;
                    }
                    if (!haveFpart && (input.TARGET_TRA != "PHASED"))
                    {
                      if (isshift[0]) TRA[0] = 0;
                      if (isshift[1]) TRA[1] = 0;
                      if (isshift[2]) TRA[2] = 0;
                    }
                    tralist(i).setFracXYZ(TRA,mr);
                  }
                  results.logEllipsisEnd(VERBOSE);
                  }
                  if (input.TRAN_PACKING_USE &&
                      tralist.Size() && //now test the top ones for
                      tralist.getTop() > f_top) //if this has any hope of generating a higher f_top
                  {
                    //if the f_top has been set, then only have to look over f_top
                    //otherwise search progressively down the list
                    double FTF_LIMIT = f_top; //could be -numeric_limits:max
                    if (input.TRAN_PACKING_NUMBER && tralist.Size() > input.TRAN_PACKING_NUMBER)
                      FTF_LIMIT = std::max(f_top,tralist.getValue(input.TRAN_PACKING_NUMBER));
                    results.logEllipsisStart(LOGFILE,"Packing Fast Search Translations");
                    results.logTab(2,LOGFILE,itos(tralist.Size()) + " peaks");
                    int num_over_limit = tralist.Over(FTF_LIMIT); //new value, shifted up
                    if (FTF_LIMIT == -std::numeric_limits<double>::max())
                      results.logTab(2,LOGFILE,"All peaks checked for packing");
                    else if (num_over_limit==0)
                      results.logTab(2,LOGFILE,"No peaks over " + dtos(FTF_LIMIT) + " - no packing check");
                    else
                      results.logTab(2,LOGFILE,itos(num_over_limit) + " peaks over " + dtos(FTF_LIMIT) + " checked for packing");
                    if (!num_over_limit) continue;
                    mr_solution this_sol;
                    this_sol.copy_extras(input.MRSET);
                    this_sol.set_resolution(mr.HiRes()); //overwrite resolution
#ifndef AJM_TURN_ON_MLHL_RESCORING
                    this_sol.PTF = (input.TARGET_TRA == "PHASED");
#endif
                    mr_set MRSET = input.MRSET[k];
                           MRSET.MAPCOEFS.clear();
                           MRSET.RLIST.clear();
                    int& lim = input.TRAN_PACKING_NUMBER;
                    this_sol.add(one_mol_in_P1,MRSET,k,e,input.MRSET[k].RLIST[e],tralist.getFracsXYZ(lim),tralist.getValues(lim),tralist.getSignals(lim),tralist.getClusters(lim),SEARCH_FACTORS,input.PTNCS); //just take the top ones for the packing test, if not zero
                    std::string cards = input.Cards();
                    InputMR_PAK inputPAK(cards);
                    inputPAK.setSOLU(this_sol);
                    inputPAK.setPACK_COMP(false); //don't move first solution to origin
                    //packing applied as in packing test
                    inputPAK.PDB = input.PDB; //unparse does not work
                    inputPAK.tracemol = input.tracemol; //unparse does not work
                    inputPAK.setPACK_PRUN(false);
                    inputPAK.setPACK_KEEP_HIGH_TFZ(false);
                    inputPAK.setPACK_CUTO(input.TRAN_PACKING_CUTOFF);
                    inputPAK.PACKING.FTF_STOP = true;
                    inputPAK.PACKING.FTF_LIMIT = FTF_LIMIT;
                    inputPAK.setKEYW(false);
                    inputPAK.setXYZO(false);
                    bool mute(!(input.NJOBS == 1 && results.level(TESTING)));
                    inputPAK.setMUTE(mute);
                    ResultMR resultPAK = runMR_PAK(inputPAK,results);
                    if (!mute) results.setOutput(resultPAK);
                    if (resultPAK.Failure()) throw resultPAK;
                    int k(0); //now find the index back into the tralist array
                    for (k = 0; k < inputPAK.MRSET.size(); k++)
                      if (inputPAK.MRSET[k].KEEP)
                        break;
                    (k < num_over_limit) ?
                      results.logTab(2,LOGFILE,"Translation peak " + itos(k+1) + " first to be kept"):
                      results.logTab(2,LOGFILE,"No translation peaks pack");
                    if (k) tralist.Erase(0,k);
                    if (!advisory_ftf_packs) advisory_ftf_packs = k; //only turn off if on
                    results.logEllipsisEnd(LOGFILE);
                    results.logBlank(LOGFILE);
                  }
                  if (memory_conservation && tralist.Size() > maxTralistSize/mem_quotient)
                  {
                    tralist.Sort();
                    tralist.selectRawTopByNumber(maxTralistSize/2);
                    tralist.purgeUnselected();
                    results.logTabPrintf(0,DEBUG,"\n");
                    results.logTab(1,DEBUG,"Final purge of lowest " + dtos(1.0/mem_quotient) +" of solutions");
                  }
                  results.logTab(1,VERBOSE,itos(tralist.Size()) + " points stored");
                  results.logBlank(VERBOSE);
                  } //end memory for peak_list arrays

                  //sort the sites into order
                  //tralist.Sort();
                  //tralist.renumber();
                  tralist.set_rescore(false);
                  tralist.set_fast(true);
                  //use the highest overall to do a pre-purge.
                  if (tralist.Size())
                  {
                    if (input.DO_RESCORE_TRA && input.SEARCH_METHOD == "FAST")
                    {
                      double f_max = tralist.getTop(); //after interpolation
                     // double z_max = signal(f_max,f_mean,f_sigma); //after interpolation
                      if (f_max > f_top)
                      {
                        f_top = std::max(f_top,f_max);
                        z_top = signal(f_top,f_mean,f_sigma); //after interpolation
                        results.logProgressBarPause(SUMMARY);
                        results.logTab(1,SUMMARY,"New Top " + std::string(input.TRAN_PACKING_USE ? "Packing " :"") + "Fast Translation Function FSS = " + dtos(f_top,2) + " (TFZ=" + dtos(z_top,1) + ") at Trial #" + itos(e+1));
                        results.logProgressBarAgain(SUMMARY); //ftf
                      }
                      else {
                      if (f_top > -std::numeric_limits<double>::max())
                        results.logTab(1,LOGFILE,"Current Top " + std::string(input.TRAN_PACKING_USE ? "Packing " :"") + "Fast Translation Function FSS = " + dtos(f_top,2) + " (TFZ=" + dtos(z_top,1) + ")");
                      if (f_top > -std::numeric_limits<double>::max())
                        results.logTab(1,LOGFILE,"Current Top (ML) " + std::string(input.TRAN_PACKING_USE ? "Packing " :"") + "Translation Function LLG = " + dtos(llg_f_top,2) + " (TFZ=" + dtos(llg_z_top,1) + ")");
                      }
                      tralist.set_stats(f_mean,f_top,input.PEAKS_TRA.safety_margin().no_cluster(),input.ZSCORE);
                      results.logBlank(LOGFILE);
                    }
                    else if (input.DO_RESCORE_TRA && input.SEARCH_METHOD == "FULL")
                      tralist.set_stats(f_mean,tralist.getTop(),input.PEAKS_TRA.safety_margin().no_cluster(),input.ZSCORE);
                    else
                      tralist.set_stats(f_mean,tralist.getTop(),input.PEAKS_TRA,input.ZSCORE);
                    for (int t = 0; t < tralist.Size(); t++)
                      tralist(t).zscore = signal(tralist.getValue(t),f_mean,f_sigma);
                    //tralist.select_and_log((input.DO_RESCORE_TRA ? VERBOSE: LOGFILE),results);
                    tralist.select_and_log((LOGFILE),results);
                    //tralist.purgeUnselected();
                    all_f_mean.push_back(f_mean);
                  }
                } //fft memory
                if (input.DO_RESCORE_TRA || input.TARGET_TRA == "BRUTE")
                {
                  mr.calcSearchVAR(TRA_MODLID,halfR,SEARCH_FACTORS);
                  mr.calcSearchTRA1(ROT,TRA_MODLID);
                  //store the interpolated E's for this orientation
                  cmplx1D interpE = mr.getInterpE(ROT,TRA_MODLID);
                  unsigned nthreads = 1;
#pragma omp parallel
                  {
#ifdef _OPENMP
                    nthreads = omp_get_num_threads();
#endif
                  }
                  if (!FAST_STATS)
                  {
                    //work out mean and standard deviation from random sample
                    //this has to be worked out before the Brute translation function
                    //because the zscore is used to purge the solutions and keep the list short
                    //to prevent memory exhaustion
                    results.logTab(1,LOGFILE,"Scoring "+itos(const_numSample)+" randomly sampled translations");
                    //calculate randomTRA only once, at the start
                    dvect31D randomTRA;
                    scitbx::random::mersenne_twister generator(0);
                    for (unsigned irand = 0; irand < const_numSample; irand++)
                    {
                      double rand1 = generator.random_double(); //[0,1]
                      double rand2 = generator.random_double(); //[0,1]
                      double rand3 = generator.random_double(); //[0,1]
                      randomTRA.push_back(dvect3(rand1,rand2,rand3));
                    }
                    //Generate list of random positions outside of parallel code
                    List.resize(const_numSample);
                    if (nthreads > 1)
                      results.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
                    results.logProgressBarStart(LOGFILE,"Generating Statistics for TF "+SetTrial, const_numSample/nthreads);
#pragma omp parallel for
                    for (int t = 0; t < List.size(); t++)
                    {
                      int nthread = 0;
#ifdef _OPENMP
                      nthread = omp_get_thread_num();
#endif
                      DataMR tramr(mr);
                      tramr.calcSearchTRA2(randomTRA[t],halfR,SEARCH_FACTORS,&interpE);
                      List[t] = tramr.likelihoodFn();
                      if (nthread == 0) results.logProgressBarNext(LOGFILE);
                    } //end pragma omp parallel
                    results.logProgressBarEnd(LOGFILE);
                    PHASER_ASSERT(List.size());
                    r_mean = mean(List);
                    r_sigma = sigma(List);
                    results.logTab(1,VERBOSE,"Mean Score (Sigma):       " + dtos(r_mean,2) + "   (" + dtos(r_sigma,5,2) + ")");
                  }
                  if (input.TARGET_TRA != "BRUTE")
                  {
                    tralist.Size() ?
                    results.logTab(1,LOGFILE,"Top " + itos(tralist.numSelected()) + " translations before clustering will be rescored"):
                    results.logTab(1,LOGFILE,"No translations within selection criteria for rescoring");
                  }
                  if (tralist.Size() && input.TARGET_TRA != "BRUTE") //do the rescoring of the ftf peaks
                  {
                    tralist.archiveValues();
                    if (nthreads > 1)
                      results.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
                    //results.logProgressBarStart(LOGFILE,"Calculating Likelihood for TF SET #"+itos(k+1)+" of "+itos(input.MRSET.size())+" TRIAL #"+itos(e+1)+" of "+itos(input.MRSET[k].RLIST.size()), tralist.Size()/nthreads);
                    results.logProgressBarStart(LOGFILE,"Calculating Likelihood for TF "+SetTrial, tralist.Size()/nthreads);
                    int tralist_size = boost::numeric_cast<int>(tralist.Size());
                    dvect3 PT = input.PDB[TRA_MODLID].PT;
#pragma omp parallel for
                    for (int t = 0; t < tralist_size; t++)
                    {
                      int nthread = 0;
#ifdef _OPENMP
                      nthread = omp_get_thread_num();
#endif
                      DataMR tramr(mr);
                      dvect3 fracXyz = tralist.getFracXYZ(t);
                      dvect3 iOrthT = tramr.doFrac2Orth(fracXyz);
                      dvect3 eOrthT = iOrthT - ROT*PT;
                      dvect3 TRA = tramr.doOrth2Frac(eOrthT);
                      tramr.calcSearchTRA2(TRA,halfR,SEARCH_FACTORS,&interpE);
                      double LLgain = tramr.likelihoodFn();
                      tralist.setValue(t,LLgain);
                      if (nthread == 0) results.logProgressBarNext(LOGFILE);
                    } //end pragma omp parallel
                    results.logProgressBarEnd(LOGFILE);
                    //sort the list into new order after rescoring
                    //tralist.Sort();
                    double orig_top = tralist.getTop();
                    double orig_tail = tralist.getTail()-1.0e-06;
                    if (input.TRAN_PACKING_USE &&
                        tralist.Size() && //now test the top ones for
                        orig_top > llg_f_top) //does not have to be sorted
                    {
                      //two passes for optimization
                      //first for optimistic case when a top solution packs, only sort top few
                      //second for pessimistic case when nothing packs
                      //if the f_top has been set, then only have to look over f_top, one pass
                      //otherwise search progressively down the list
                      std::list<double> loopperc = boost::assign::list_of(0.9999999)(0.9)(0.8)(0.7)(0.6)(0.5)(0.4)(0.3)(0.2)(0.1)(0.0); //no signal otherwise
                      int loopmax = (llg_f_top == -std::numeric_limits<double>::max()) ? loopperc.size() : 1;
                      bool found1 = false;
                      int cumulative(0);
                      double last_ftf_limit(0);
                      for (int loop = 0; loop < loopmax && !found1; loop++)
                      {
                        results.logEllipsisStart(LOGFILE,"Packing LLG Translations: pass " + itos(loop+1) + " of " + itos(loopmax));
                        results.logTab(2,LOGFILE,itos(tralist.Size()) + " peaks");
                        double FTF_LIMIT;
                        if (loopmax == 1)  //has to be over f_top
                          FTF_LIMIT = llg_f_top;
                        else if (loop == loopmax-1) //last
                          FTF_LIMIT = -std::numeric_limits<double>::max(); //all
                        else
                        {
                          std::list<double>::iterator iter = loopperc.begin();
                          std::advance(iter, loop);
                          FTF_LIMIT = (orig_top-orig_tail)*(*iter)+orig_tail;
                        }
                        //bisect to find FTF_LIMIT for 1000
                        int num_over_limit = tralist.Over(FTF_LIMIT);
                        if (input.TRAN_PACKING_NUMBER &&
                            (cumulative + num_over_limit) > input.TRAN_PACKING_NUMBER)
                        {
                          double hi(last_ftf_limit),lo(FTF_LIMIT);
                          do
                          {
                            FTF_LIMIT = (hi + lo)/2.;
                            num_over_limit = tralist.Over(FTF_LIMIT);
                            if (cumulative + num_over_limit > input.TRAN_PACKING_NUMBER)
                                 lo = FTF_LIMIT;
                            else hi = FTF_LIMIT;
                          }
                          while ((cumulative + num_over_limit) != input.TRAN_PACKING_NUMBER && (hi-lo)>1.0e-06);
                        }
                        tralist.SortTop(FTF_LIMIT); //partial sort, necessary to look for highest that pack, so that exit below runs
                        cumulative += num_over_limit;
                        if (FTF_LIMIT == -std::numeric_limits<double>::max())
                          results.logTab(2,LOGFILE,"All peaks checked for packing");
                        else if (num_over_limit==0)
                          results.logTab(2,LOGFILE,"No peaks over " + dtos(FTF_LIMIT) + " - no packing check");
                        else
                          results.logTab(2,LOGFILE,itos(num_over_limit) + " peaks over " + dtos(FTF_LIMIT) + " checked for packing");
                        if (!num_over_limit) continue;
                        mr_solution this_sol;
                        this_sol.copy_extras(input.MRSET);
                        this_sol.set_resolution(mr.HiRes()); //overwrite resolution
#ifndef AJM_TURN_ON_MLHL_RESCORING
                        this_sol.PTF = (input.TARGET_TRA == "PHASED");
#endif
                        mr_set MRSET = input.MRSET[k];
                               MRSET.MAPCOEFS.clear();
                               MRSET.RLIST.clear();
                        this_sol.add(one_mol_in_P1,MRSET,k,e,input.MRSET[k].RLIST[e],tralist.getFracsXYZ(num_over_limit),tralist.getValues(num_over_limit),tralist.getSignals(num_over_limit),tralist.getClusters(num_over_limit),SEARCH_FACTORS,input.PTNCS);
                        std::string cards = input.Cards();
                        InputMR_PAK inputPAK(cards);
                        inputPAK.setSOLU(this_sol);
                        inputPAK.setPACK_COMP(false); //don't move first solution to origin
                        //packing applied as in packing test
                        inputPAK.PDB = input.PDB; //unparse does not work
                        inputPAK.tracemol = input.tracemol; //unparse does not work
                        inputPAK.setPACK_PRUN(false);
                        inputPAK.setPACK_KEEP_HIGH_TFZ(false);
                        inputPAK.setPACK_CUTO(input.TRAN_PACKING_CUTOFF);
                        inputPAK.PACKING.FTF_STOP = true;
                        inputPAK.PACKING.FTF_LIMIT = FTF_LIMIT;
                        inputPAK.setKEYW(false);
                        inputPAK.setXYZO(false);
                        bool mute(!(input.NJOBS == 1 && results.level(TESTING)));
                        inputPAK.setMUTE(mute);
                        ResultMR resultPAK = runMR_PAK(inputPAK,results);
                        if (!mute) results.setOutput(resultPAK);
                        if (resultPAK.Failure()) throw resultPAK;
                        int k(0);
                        for (k = 0; k < inputPAK.MRSET.size(); k++)
                          if (inputPAK.MRSET[k].KEEP)
                            break;
                        (k < num_over_limit) ?
                          results.logTab(2,LOGFILE,"Translation peak " + itos(k+1) + " first to be kept"):
                          results.logTab(2,LOGFILE,"No translation peaks pack");
                        if (k) tralist.Erase(0,k);
                        if (k < num_over_limit) found1 = true;
                        if (!advisory_llg_packs) advisory_llg_packs = (loop || k); //only turn off if on
                        results.logEllipsisEnd(LOGFILE);
                        if (found1) results.logTab(2,LOGFILE,"Exit: found a peak that packs");
                        results.logBlank(LOGFILE);
                        last_ftf_limit = FTF_LIMIT;
                        if (input.TRAN_PACKING_NUMBER && cumulative >= input.TRAN_PACKING_NUMBER) found1 = true;
                      }
                    }
                    PHASER_ASSERT(List.size());
                    //check mean is lower than top
                    if (tralist.Size()) // some pack
                    {
                      double topTFbefore = tralist.getTop();
                      double topTFZbefore = tralist.getTopTFZ();
                      if (r_mean >= topTFbefore)
                      {
                        std::map<int,double> t_List_orig;
                        for (int t = 0; t < List.size(); t++)
                        {
                          if (List[t] >= topTFbefore)
                          { //case of random site being better than the top
                            t_List_orig[t] = List[t];
                            List[t] = topTFbefore;
                          }
                        }
                        r_mean = mean(List); //mean forced lower than top
                        //r_sigma = sigma(List); //there will still be a sigma only if List > 1
                        (t_List_orig.size() == 1) ?
                          results.logTab(1,LOGFILE,"One position in random selection scored LLG > top TF value!"):
                          results.logTab(1,LOGFILE,itos(t_List_orig.size()) + " positions in random selection scored LLG > top TF value!");
                        results.logTab(1,LOGFILE,"There is no signal in the Translation Function search");
                        results.logTab(1,LOGFILE,"Mean has been adjusted downwards");
                        results.logBlank(LOGFILE);
                        results.logAdvisory(SUMMARY,"No Signal in Translation Function");
                        PHASER_ASSERT(r_sigma > 0);
                        for (std::map<int,double>::iterator iter = t_List_orig.begin(); iter != t_List_orig.end(); iter++)
                          List[iter->first] = iter->second;
                      } //end fix mean
                      if (!FAST_STATS)
                      { //if FAST mode and only a few are being calculated, then these numbers are not reliable
                        results.logTab(1,VERBOSE,"Mean Score (Sigma):       " + dtos(r_mean,2) + "   (" + dtos(r_sigma,5,2) + ")");
                        results.logTab(1,LOGFILE,"Highest Score (Z-score):  " + dtos(topTFbefore,2) + "   (" + dtos(signal(topTFbefore,r_mean,r_sigma),5,2) + ")");
                        results.logBlank(VERBOSE);
                      }
                      if (topTFbefore > llg_f_top)
                      {
                        llg_f_top = topTFbefore;
                        llg_z_top = FAST_STATS ? topTFZbefore :
                                                 signal(llg_f_top,r_mean,r_sigma);
                        results.logProgressBarPause(SUMMARY);
                        results.logTab(1,SUMMARY,"New Top (ML) Translation Function LLG = " + dtos(llg_f_top,2) + " (TFZ=" + dtos(llg_z_top,1) + ") at Trial #" + itos(e+1));
                        results.logProgressBarAgain(SUMMARY); //btf
                      }
                      //whenever rescoring is used, use the average mean
                      all_r_mean.push_back(r_mean);
                      all_r_sigma.push_back(r_sigma);
                      tralist.set_rescore(true);
                      tralist.set_fast(true);
                      tralist.set_stats(r_mean,tralist.getTop(),input.PEAKS_TRA,input.ZSCORE);
                      if (!FAST_STATS)
                      {
                        //if the mean and sigma are reliable, then they are used to overwrite the fast zscores
                        //tralist.set_stats(r_mean,tralist.getTop(),input.PEAKS_TRA,input.ZSCORE);
                        for (int t = 0; t < tralist.Size(); t++)
                          tralist(t).zscore = signal(tralist.getValue(t),r_mean,r_sigma);
                      }
                    }
                  } //rescore
                  else if (input.TARGET_TRA == "BRUTE") //tralist has no size yet
                  {
                    results.logTab(1,LOGFILE,"Run brute translation search:");
                    results.logTab(2,VERBOSE,"Grid Calculation");
                    bool isFirst(input.MRSET[k].KNOWN.size() == 0);
                    Hexagonal search_points = mr.calc_search_points(input.BTF,SAMPLING,isFirst,results);
                    results.logTab(2,LOGFILE,"Search grid has "+itos(search_points.count_sites())+" points");
                    results.logBlank(LOGFILE);
                    int num_sites = search_points.count_sites();
                    // We use explicit initialisation to tralist when creating nthreads copies as there's
                    // no default constructor for SiteListXyz
                    std::vector<SiteListXyz> omp_tralist(nthreads,tralist);
                    if (nthreads > 1)
                      results.logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads.");
                    results.logProgressBarStart(LOGFILE,"Scoring Translations",num_sites/nthreads);
                    bool first_loop = true;
#pragma omp parallel for firstprivate(first_loop,search_points)
                    for (int t = 0; t < num_sites; t++)
                    {
                      int nthread = 0;
#ifdef _OPENMP
                      nthread = omp_get_thread_num();
#endif
                      DataMR tramr(mr);
                      double maxLLgain(0);
                      if (first_loop) maxLLgain = -std::numeric_limits<double>::max();
                      first_loop = false;
                      dvect3 T = search_points.next_site_frac(t);
                      dvect3 iOrthT = tramr.doFrac2Orth(T);
                      dvect3 eOrthT = iOrthT - ROT*input.PDB[TRA_MODLID].PT;
                      dvect3 TRA = tramr.doOrth2Frac(eOrthT);
                      tramr.calcSearchTRA2(TRA,halfR,SEARCH_FACTORS,&interpE);
                      double LLgain = tramr.likelihoodFn();
                      //sort sites into order
                      xyzsite traSite;
                      traSite.setFracXYZ(T,tramr);
                      traSite.value = LLgain;
                      omp_tralist[nthread].push_back(traSite);
                      int sort_interval(1000);
                      if (!(t%sort_interval))
                      {
                        omp_tralist[nthread].set_stats(r_mean,maxLLgain,input.PEAKS_TRA.purge_margin(),input.ZSCORE);
                        for (int t = 0; t < omp_tralist[nthread].Size(); t++) omp_tralist[nthread](t).zscore = signal(omp_tralist[nthread].getValue(t),r_mean,r_sigma);
                        omp_tralist[nthread].Sort();
                        omp_tralist[nthread].selectRawTop();
                        omp_tralist[nthread].purgeUnselected();
                      }
                      if (nthread == 0) results.logProgressBarNext(LOGFILE);
                    } //end pragma omp parallel
                    results.logProgressBarEnd(LOGFILE);
                    // sum up values of auxilliary variable
                    for (int nthread=0; nthread<nthreads; nthread++)
                      for (int m=0; m < omp_tralist[nthread].Size(); m++)
                        tralist.push_back( omp_tralist[nthread](m) );
                    tralist.set_stats(r_mean,tralist.getTop(),input.PEAKS_TRA,input.ZSCORE);
                    for (int t = 0; t < tralist.Size(); t++) tralist(t).zscore = signal(tralist.getValue(t),r_mean,r_sigma);
                    all_r_mean.push_back(r_mean);
                    all_r_sigma.push_back(r_sigma);
                    tralist.Sort();
                    tralist.renumber();
                    tralist.set_rescore(false);
                    tralist.set_fast(false);
                  }
                  if (tralist.Size()) tralist.select_and_log(LOGFILE,results);
                  //tralist.purgeUnselected();
                }
              }
              else // one mol in P1
              {
                results.logTab(1,LOGFILE,"Special Case: Search for first ensemble in P1");
                results.logBlank(LOGFILE);
                tralist.Resize(0);
                dvect3 TRA(0,0,0);
                xyzsite traSite;
                traSite.setOrthXYZ(TRA);
                cmplx1D interpE =  mr.getInterpE(ROT,TRA_MODLID);
                mr.calcSearchVAR(TRA_MODLID,halfR,SEARCH_FACTORS);
                mr.calcSearchTRA1(ROT,TRA_MODLID);
                mr.calcSearchTRA2(TRA,halfR,SEARCH_FACTORS,&interpE);
                traSite.value = mr.likelihoodFn();
                tralist.push_back(traSite);
                tralist.set_cluster_dist(HIRES); //doesn't matter
                tralist.set_rescore(false);
                tralist.set_fast(true);
                for (int t = 0; t < tralist.Size(); t++) tralist(t).zscore = 0;
                tralist.setSelected(0,true);
                r_mean = 0;
                r_sigma = 1;
                tralist.set_stats(0,tralist.getTop(),input.PEAKS_TRA,input.ZSCORE);
              }
              PHASER_ASSERT(e < rlisttop[k].size());
              rlisttop[k][e][0] = tralist.Size() > 0 ? tralist(0).value : 0;
              rlisttop[k][e][1] = tralist.Size() > 1 ? tralist(1).value : 0;
              rlisttop[k][e][2] = tralist.Size() > 2 ? tralist(2).value : 0;
              rlistz[k][e][0] = tralist.Size() > 0 ? tralist(0).zscore : 0;
              rlistz[k][e][1] = tralist.Size() > 1 ? tralist(1).zscore : 0;
              rlistz[k][e][2] = tralist.Size() > 2 ? tralist(2).zscore : 0;
              mr_set MRSET = input.MRSET[k];
                     MRSET.MAPCOEFS.clear();
                     MRSET.RLIST.clear();
              sg_solutions.add(one_mol_in_P1,MRSET,k,e,input.MRSET[k].RLIST[e],tralist.getFracsXYZ(),tralist.getValues(),tralist.getSignals(),tralist.getClusters(),SEARCH_FACTORS,input.PTNCS);
              results.logProgressBarNext(SUMMARY);
            } // euler test list
            results.logProgressBarEnd(SUMMARY);
          }//known mr solutions
        }//deep
        if (advisory_ftf_packs) results.logAdvisory(SUMMARY,"The top solution from a FTF did not pack");
        if (advisory_llg_packs) results.logAdvisory(SUMMARY,"The top solution from a TF rescoring did not pack");
        if (all_r_mean.size()) //FAST mode
        { //take the average of all the means, of which there should be const_numSample in total
          double purge_mean = mean(all_r_mean);
          if (!all_one_mol_in_P1) sg_solutions.set_purge_mean(purge_mean);
        }
        else if (all_f_mean.size())
        { //take the average of all the means, of which there should be const_numSample in total
          double purge_mean = mean(all_f_mean);
          if (!all_one_mol_in_P1) sg_solutions.set_purge_mean(purge_mean);
        }
        sg_solutions.set_as_rescored(!all_one_mol_in_P1 &&
           (input.DO_RESCORE_TRA || input.TARGET_TRA == "BRUTE" || input.TARGET_TRA == "PHASED"));

        results.logBlank(LOGFILE);
        results.logTab(1,LOGFILE,"Translation Function Results");
        results.logTab(1,LOGFILE,"Top1: " + sg_solutions.top_logfile());
        results.logBlank(LOGFILE);

        results.logUnderLine(CONCISE,"Translation Function Table");
        results.logTabPrintf(1,CONCISE,"%3s %3s%5s %5s %5s %10s %5s %10s %5s %-11s %-s\n","SET","ROT","*deep","Top","(Z)","Second","(Z)","Third","(Z)","SpaceGroup","Ensemble");
        for (int k = 0; k < input.MRSET.size(); k++)
        {
          for (int e = 0; e < ntab[k]; e++)
          {
            std::string MODLID(input.MRSET[k].RLIST[e].MODLID,0,8);
            std::string SGNAME = space_group_name(input.MRSET[k].HALL,true).CCP4;
            if (all_one_mol_in_P1)
              results.logTabPrintf(1,CONCISE,"%3d %3d %10s %5s %10s %5s %10s %5s %-8s %-11s\n",k+1,e+1,"*","*","-","-","-","-",SGNAME.c_str(),MODLID.c_str());
            else if (rlisttop[k][e][0] && rlistz[k][e][0] && rlisttop[k][e][1] && rlistz[k][e][1] && rlisttop[k][e][2] && rlistz[k][e][2])
              results.logTabPrintf(1,CONCISE,"%3d %3d%c%10.1f %5.2f %10.1f %5.2f %10.1f %5.2f %-8s %-11s\n",k+1,e+1,e >= input.MRSET[k].getNDEEP()?'*':' ',rlisttop[k][e][0],rlistz[k][e][0],rlisttop[k][e][1],rlistz[k][e][1],rlisttop[k][e][2],rlistz[k][e][2],SGNAME.c_str(),MODLID.c_str());
            else if (rlisttop[k][e][0] && rlistz[k][e][0] && rlisttop[k][e][1] && rlistz[k][e][1])
              results.logTabPrintf(1,CONCISE,"%3d %3d%c%10.1f %5.2f %10.1f %5.2f %10s %5s %-8s %-11s\n",k+1,e+1,e >= input.MRSET[k].getNDEEP()?'*':' ',rlisttop[k][e][0],rlistz[k][e][0],rlisttop[k][e][1],rlistz[k][e][1],"-","-",SGNAME.c_str(),MODLID.c_str());
            else if (rlisttop[k][e][0] && rlistz[k][e][0])
              results.logTabPrintf(1,CONCISE,"%3d %3d%c%10.1f %5.2f %10s %5s %10s %5s %-8s %-11s\n",k+1,e+1,e >= input.MRSET[k].getNDEEP()?'*':' ',rlisttop[k][e][0],rlistz[k][e][0],"-","-","-","-",SGNAME.c_str(),MODLID.c_str());
            else
              results.logTabPrintf(1,CONCISE,"%3d %3d%c%10s %5s %10s %5s %10s %5s %-8s %-11s\n",k+1,e+1,e >= input.MRSET[k].getNDEEP()?'*':' ',"-","-","-","-","-","-",SGNAME.c_str(),MODLID.c_str());
          }
          results.logTabPrintf(1,CONCISE,"%3s %3s\n","---","---");
        }
        results.logBlank(CONCISE);
        if (!input.SEARCH_FACTORS.defaults())
        {
          results.logTab(1,SUMMARY,"B-factor of search component = " + dtos(input.SEARCH_FACTORS.BFAC));
          results.logTab(1,SUMMARY,"Occcupancy of search component = " + dtos(input.SEARCH_FACTORS.OFAC));
          results.logBlank(SUMMARY);
        }
        if (!all_one_mol_in_P1 && input.PURGE_TRA.ENABLE // Final selection done at FTF for first mol in P1
            && (input.PEAKS_TRA.by_number() || input.PEAKS_TRA.by_percent()
               || (input.PEAKS_TRA.by_sigma() && input.PURGE_TRA.NUMBER) ))
        {
          int start_num = sg_solutions.size();
          input.ZSCORE.set_top(sg_solutions.top_TFZ()); //half of top for partially correct
          results.logSectionHeader(SUMMARY,"FINAL SELECTION");
          if (!sg_solutions.size())
          {
            results.logTab(1,SUMMARY,"No solutions to purge");
            results.logBlank(SUMMARY);
          }
          else
          {
          if (all_r_mean.size()) results.logTab(1,SUMMARY,"LLG will be used for purge, not FSS");
          else if (all_f_mean.size()) results.logTab(1,SUMMARY,"FSS will be used for purge, not LLG");
          results.logTab(1,SUMMARY,"Top TF  = " + dtos(sg_solutions.top_TF(),2));
          results.logTab(1,SUMMARY,"Top TFZ = " + dtos(sg_solutions.top_TFZ(),2));
          results.logTab(1,SUMMARY,"Mean TF = " + dtos(sg_solutions.get_purge_mean(),2));
          double cutoff = sg_solutions.purge_tf(input.PURGE_TRA.get_percent(),input.PURGE_TRA.NUMBER,input.ZSCORE);
          input.PURGE_TRA.NUMBER ?
            results.logTab(1,SUMMARY,"Number used for purge  = " + itos(input.PURGE_TRA.NUMBER)):
            results.logTab(1,SUMMARY,"Percent used for purge = " + dtos(input.PURGE_TRA.get_percent()*100) + "%");
          results.logTab(1,SUMMARY,"Cutoff for acceptance = " + dtos(cutoff,1));
          int final_num = sg_solutions.size();
          if (input.ZSCORE.USE)
          {
            results.logTab(2,SUMMARY,"TFZ used for final selection = " + dtos(input.ZSCORE.cutoff()));
            int num_cutoff(0),num_zscore(0),num_both(0);
            sg_solutions.num_over(cutoff,input.ZSCORE.cutoff(),num_cutoff,num_zscore,num_both);
            results.logTab(3,SUMMARY,"Number of solutions over TF final cutoff  = " + itos(num_cutoff));
            results.logTab(3,SUMMARY,"Number of solutions over TFZ final cutoff = " + itos(num_zscore));
            results.logTab(3,SUMMARY,"Number of solutions over TF & TFZ cutoff  = " + itos(num_both));
          }
          results.logTab(1,SUMMARY,"Number of solutions stored before final selection = " + itos(start_num));
          results.logTab(1,SUMMARY,"Number of solutions stored (deleted) after final selection = " + itos(final_num) + " (" + itos(start_num-final_num) + ")");
          results.logBlank(SUMMARY);
          }
        }
        //final sort to get into order
        sg_solutions.sort_TF();
        for (int v = 0; v < sg_solutions.size(); v++)
          if (sg_solutions[v].NRF && sg_solutions[v].NTF) //proxy for coming from a FAST or FULL job
            (input.MRSET[0].RLIST[0].GRF) ?
            sg_solutions[v].HISTORY +=  " GRF/TF(" +
                          itos(sg_solutions[v].NRF) + "/" +
                          itos(sg_solutions[v].NTF) +
                          ":" + itos(v+1) + ")":
            sg_solutions[v].HISTORY +=  " RF/TF(" +
                          itos(sg_solutions[v].NRF) + "/" + itos(sg_solutions[v].NTF) +
                          ":" + itos(v+1) + ")";
        std::string loggraph;
        for (int v = 0; v < sg_solutions.size(); v++)
          loggraph += itos(v+1) + " " +  dtos(sg_solutions.tf()[v],10,2) + " " + dtos(sg_solutions.tfz()[v],5,2) + "\n";
        string1D graphs(2);
                 graphs[0] = "TF Number vs LL-gain:AUTO:1,2";
                 graphs[1] = "TF Number vs Z-Score:AUTO:1,3";
        std::string str_modlids;
        for (std::set<std::string>::iterator iter = search_modlids.begin(); iter != search_modlids.end(); iter++)
          str_modlids +=  *iter + " OR ";
        str_modlids = std::string(str_modlids,0,str_modlids.size()-4);
        results.logGraph(LOGFILE,"Translation Function Component #" + itos(ncomponent) + " (" + str_modlids + ")",graphs,"Number LLG Z-Score",loggraph);
        results.storeTralist(sg_solutions);
      } //if there are any solutions to test after packing
    } //if there are any solutions to test
    the_end:
    outStream where = SUMMARY;
    results.logSectionHeader(where,"OUTPUT FILES");
    if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
    if (input.DO_KEYWORDS.True()) results.writeSol();
    if (input.DO_XYZOUT.True()) results.writePdb(input.DO_XYZOUT,input.tracemol);
    if (!results.getFilenames().size()) results.logTab(1,where,"No files output");
    else for (int f = 0; f < results.getFilenames().size(); f++) results.logTab(1,where,results.getFilenames()[f]);
    results.logBlank(where);
    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR_TF runMR_FTF(InputMR_FTF& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_FTF(input,output);
}

} //phaser
