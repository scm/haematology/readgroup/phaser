//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineMR.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>
#include <phaser/src/Dfactor.h>
#include <phaser/src/ListEditing.h>
#include <phaser/src/MapTraceMol.h>
#include <phaser/lib/mean.h>
#include <scitbx/random.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_RNP(InputMR_RNP& input,Output output)
{
  ResultMR results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    input.MACM_CHAINS.True() ?
    results.logHeader(LOGFILE,header(MR_GMBL)):
    results.logHeader(LOGFILE,header(MR_RNP));
    input.Analyse(results);

    bool SCORING(true);
    for (unsigned p = 0 ; p < input.MACRO_MR.size(); p++)
      if (!input.MACRO_MR[p]->is_off()) SCORING = false;

    Composition composition(input.COMPOSITION);
    composition.set_modlid_scattering(input.PDB);
    UnitCell uc(input.UNIT_CELL);
    cctbx::uctbx::unit_cell cctbxUC = uc.getCctbxUC();
    composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS);
    if (composition.increased())
      results.logWarning(SUMMARY,composition.warning());
    double TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);
    input.COMPOSITION = composition;
    if (composition.increased())
    {
      std::string cards = input.Cards();
      InputCCA inputCCA(cards);
      double fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
      inputCCA.setRESO_HIGH(fullhires);
      inputCCA.setMUTE(true);
      ResultCCA resultCCA = runCCA(inputCCA,results);
      PHASER_ASSERT(resultCCA.Success()); //cheap
      //throw same error as would be thrown by runCCA
      if (resultCCA.getFitError())
      throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
    }

    if (!input.SIGMAN.REFINED)
    {//memory
    results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    }//memory

    if (input.PTNCS.USE)
    {
      Epsilon epsilon(input.REFLECTIONS,
                      input.RESHARP,
                      input.SIGMAN,
                      input.OUTLIER,
                      input.PTNCS,
                      input.DATABINS,
                      input.COMPOSITION
                      );
      epsilon.findTRA(results);
      if (epsilon.getPtNcs().TRA.VECTOR_SET)
        epsilon.findROT(input.MACRO_TNCS,results);
      input.PTNCS = epsilon.getPtNcs();
      input.OUTLIER = epsilon.getOutlier();
      if (input.PTNCS.use_and_present())
      { //can be fixed in FAST auto job, but just throw error here
        std::map<std::string,int> MODLID = input.MRSET.map_of_modlids();
        for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
          if (iter->second % input.PTNCS.NMOL) //remainder
            results.logAdvisory(SUMMARY,"Solutions do not obey tNCS NMOL");
      }
    }

    if (!input.REFLECTIONS.Feff.size())
    {
      results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
      Dfactor dfactor(input.REFLECTIONS,
                      input.RESHARP,
                      input.SIGMAN,
                      input.OUTLIER,
                      input.PTNCS,
                      input.DATABINS,
                      input.COMPOSITION
                      );
      dfactor.calcDfactor(results);
      input.REFLECTIONS = dfactor;
    }

    //even if there are not MRSETS, set up the result object for return
    DataMR mr(input.REFLECTIONS,
              input.RESHARP,
              input.SIGMAN,
              input.OUTLIER,
              input.PTNCS,
              input.DATABINS,
              input.HIRES,input.LORES,
              input.COMPOSITION,
              input.ensemble);
    SigmaN sigmaN = mr.getBinSigmaN();
    bool halfR(false); // Even if rotations not refined before, refining them now
    mr.initGfunction(halfR);
    results.setDataA(mr);

    bool fix_bfac_with_tncs(false);
    if (fix_bfac_with_tncs && input.PTNCS.use_and_present())
    {
      bool ref_bfac(false);
      for (int p = 0; p < input.MACRO_MR.size(); p++)
      {
        if (!input.MACRO_MR[p]->getFIX(macm_bfac)) ref_bfac = true;
        input.MACRO_MR.back()->setFIX(macm_bfac,true);
      }
      if (ref_bfac)
        results.logWarning(SUMMARY,"B-factor of solutions cannot currently be refined with TNCS present: B-factors fixed");
    }

    if (input.MRSET.size() && input.MRSET[0].KNOWN.size())
    {
      results.logSectionHeader(LOGFILE,"DATA FOR REFINEMENT AND PHASING");
      input.MRSET.if_not_set_apply_space_group(input.SG_HALL);
      //if the ensembles are coming from mtz files, check that the high resolution of the input
      //ensembles is not lower than the resolution of the data - if so, truncate data to lower resolution
      stringset modlids = input.MRSET.set_of_modlids();
      double data_HIRES = mr.HiRes();
      double sol_input_HIRES = input.MRSET.get_resolution();
      if (input.HIRES < 0 && (sol_input_HIRES > mr.HiRes()))
      {
        results.logTab(1,LOGFILE,"High resolution limit imposed by solution list = " + dtos(sol_input_HIRES,5,2));
        results.logBlank(LOGFILE);
        mr.selectReso(sol_input_HIRES,input.LORES);
      }
      double mapHIRES(0),scale_mapHIRES(0);
      bool ref_cell = false;
      for (stringset::iterator iter = modlids.begin(); iter != modlids.end(); iter++)
      {
        for (int p = 0; p < input.MACRO_MR.size(); p++)
          if (!input.MACRO_MR[p]->getFIX(macm_cell)) ref_cell = true;
        std::string m = (*iter);
        if (input.PDB[m].ENSEMBLE[0].map_format())
          mapHIRES = std::max(input.PDB[m].HIRES,mapHIRES);
        scale_mapHIRES = mapHIRES; // will be same or larger
        //allow for interpolation with cell expansion
        if (ref_cell) //worst possible
          scale_mapHIRES *= double(DEF_CELL_SCALE_MAX);
        else //worse possible in list
          scale_mapHIRES *= input.MRSET.max_cell_scale();
      }
      if (scale_mapHIRES > std::max(input.HIRES,std::max(data_HIRES,sol_input_HIRES)))
      {
        (ref_cell) ?
        results.logTab(1,LOGFILE,"High resolution limit imposed by Maps for cell refinement = " + dtos(scale_mapHIRES,5,2)):
        results.logTab(1,LOGFILE,"High resolution limit imposed by Maps = " + dtos(scale_mapHIRES,5,2));
        mr.selectReso(scale_mapHIRES,input.LORES);
      }
      double HIRES = std::max(data_HIRES,std::max(sol_input_HIRES,scale_mapHIRES));
      HIRES = std::max(HIRES,std::max(input.HIRES,scale_mapHIRES)); //cannot be avoided
      mr.selectReso(HIRES,input.LORES);
      mr.logOutliers(LOGFILE,results);
      mr.logDataStats(SUMMARY,results);
      mr.logSymm(DEBUG,results);
      mr.logUnitCell(DEBUG,results);
      mr.logDataStatsExtra(DEBUG,20,results);

      results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
      double LLwilson = mr.WilsonLL();
      mr.logWilson(LOGFILE,LLwilson,results);
      mr.setLLwilson(LLwilson);

      std::map<std::string,drms_vrms> DV;
      if (input.MRSET.size())
      {
        outStream where(LOGFILE);
        results.logSectionHeader(where,"ENSEMBLING");
        if (input.MACM_CHAINS.True())
        {
          results.logEllipsisStart(LOGFILE,"Splitting Chains");
          std::map<std::string,string1D> chainIDs;
          for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
            chainIDs[iter->first] = iter->second.Coords().getChains();
          map_str_pdb rnpPDB;
          for (int k = 0; k < input.MRSET.size(); k++)
          {
            mr_set SET = input.MRSET[k];
            SET.KNOWN.clear();
            SET.DRMS.clear();
            SET.VRMS.clear();
            SET.NEWVRMS.clear();
            SET.CELL.clear();
            for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            {
              std::string& MODLID = input.MRSET[k].KNOWN[s].MODLID;
              for (int i = 0; i < chainIDs[MODLID].size(); i++)
              {
                mr_ndim KNOWN = input.MRSET[k].KNOWN[s];
                KNOWN.MODLID += "["+chainIDs[MODLID][i]+"]";
                if (rnpPDB.find(KNOWN.MODLID) == rnpPDB.end())
                {
                  rnpPDB[KNOWN.MODLID] = input.PDB[input.MRSET[k].KNOWN[s].MODLID];
                  for (int m = 0; m < rnpPDB[KNOWN.MODLID].ENSEMBLE.size(); m++)
                  rnpPDB[KNOWN.MODLID].ENSEMBLE[m].CHAIN = chainIDs[MODLID][i];
                }
                SET.KNOWN.push_back(KNOWN);
                if (input.MRSET[k].DRMS.find(MODLID) != input.MRSET[k].DRMS.end())
                  SET.DRMS[KNOWN.MODLID] = input.MRSET[k].DRMS[MODLID];
                if (input.MRSET[k].VRMS.find(MODLID) != input.MRSET[k].VRMS.end())
                  SET.VRMS[KNOWN.MODLID] = input.MRSET[k].VRMS[MODLID];
                if (input.MRSET[k].NEWVRMS.find(MODLID) != input.MRSET[k].NEWVRMS.end())
                  SET.NEWVRMS[KNOWN.MODLID] = input.MRSET[k].NEWVRMS[MODLID];
                if (input.MRSET[k].CELL.find(MODLID) != input.MRSET[k].CELL.end())
                  SET.CELL[KNOWN.MODLID] = input.MRSET[k].CELL[MODLID];
              }
            }
            input.MRSET[k] = SET; //overwrite
          }
          input.PDB = rnpPDB;
          for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
          {
            iter->second.setup_has_been_called = false;
            std::string error = iter->second.setup_molecules();
            if (error.size()) throw PhaserError(INPUT,error);
          }
          results.logEllipsisEnd(LOGFILE);
          results.logBlank(LOGFILE);
        }
        //there may be lots of ensembles in an OR search
        //we don't want them all in memory at the same time
        //load the ones that are extras first, extract the table information (and check)
        //and then leave the MapEnsemble object in the state for the first refinement
        for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
          iter->second.setup_and_fix_vrms(iter->first,HIRES,input.PTNCS,input.SOLPAR,results);
        mr.ensPtr()->configure(input.MRSET.set_of_modlids(),input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[0].set_of_modlids(),results);
        mr.ensPtr()->ensembling_table(LOGFILE,TOTAL_SCAT,results);
        std::string warning = input.MRSET.setup_vrms_delta(mr.ensPtr()->get_drms_vrms());
        if (warning.size())
          results.logWarning(SUMMARY,"Conflict in RMSD between ENSEMBLE and SOLUTION inputs\n" + warning);
        results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
        DV = mr.ensPtr()->get_drms_vrms();
        { //ready for output
          if (input.tracemol == NULL)
          {
            MapTrcPtr new_ensPtr(new MapTraceMol());
            input.tracemol = new_ensPtr; //deep copy on init
          }
          input.tracemol->configure(input.MRSET.set_of_modlids(),true,false,input.PDB,results);
          input.tracemol->trace_table(LOGFILE,results);
        }
      }
      results.init("LLG",input.TITLE,input.NUM_TOPFILES,input.PDB,input.MACM_CHAINS.True());

      results.logSectionHeader(LOGFILE,std::string(SCORING?"SCORING":"REFINEMENT"));
      if (!SCORING)
      for (unsigned p = 0 ; p < input.MACRO_MR.size(); p++)
      {
        results.logTab(1,LOGFILE,"Protocol cycle #"+ itos(p+1) + " of " + itos(input.MACRO_MR.size()));
        if (!input.MACRO_MR[p]->getFIX(macm_cell))
          results.logTab(1,LOGFILE,"Cell of " + std::string(mr.ensPtr()->has_pdb()?"models":"data") + " will be refined");
        results.logTabPrintf(0,LOGFILE,"%s",input.MACRO_MR[p]->logfile().c_str());
        results.logBlank(LOGFILE);
      }

      int nthreads = 1;
#pragma omp parallel
      {
#ifdef _OPENMP
        nthreads = omp_get_num_threads();
#endif
      }
//end #pragma omp parallel

      (input.MRSET.size() == 1) ?
      results.logTab(1,LOGFILE,"There is 1 solution to " + std::string(SCORING?"score":"refine")):
      results.logTab(1,LOGFILE,"There are " + itos(input.MRSET.size()) + " solutions to " + std::string(SCORING?"score":"refine"));

      if (nthreads > 1)
        results.logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
      results.logProgressBarStart(LOGFILE,std::string(SCORING?"Scoring":"Refining") + " solutions",input.MRSET.size()/nthreads);
// create nthreads results objects to collect text results from each thread
      std::vector<Output> omp_results;
      omp_results.resize(nthreads);

      outStream unmute_level(VERBOSE);
      for(int n = 0; n < nthreads; ++n)
      {
        // concurrent threads must not write to stdout
        omp_results[n].run_time = results.run_time;
        omp_results[n].setLevel(input.OUTPUT_LEVEL);
        omp_results[n].setMute(!(nthreads == 1 && results.level(unmute_level)));
      }

      int MRSET_size = input.MRSET.size();

#if _OPENMP
      map_str_pdb omp_PDB = input.PDB;
#else
      map_str_pdb& omp_PDB = input.PDB;
#endif

#pragma omp parallel for firstprivate(mr,omp_PDB)
      for (int k = 0; k < MRSET_size; k++)
      {
        int nthread = 0;
#ifdef _OPENMP
        nthread = omp_get_thread_num();
#endif
        if (!SCORING) omp_results[nthread].logUnderLine(LOGFILE,"REFINING SET #" + itos(k+1) + " OF " + itos(MRSET_size));
        //don't storePdb, in parallel section, race condition core dump
        DataMR ompmr(mr);
        RefineMR refmr(input.BFAC_REFI,input.DRMS_REFI,ompmr,omp_results[nthread]);
        refmr.changeSpaceGroup(input.MRSET[k].HALL,HIRES,input.LORES);
        refmr.ensPtr()->configure(input.MRSET[k].set_of_modlids(),omp_PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[k].set_of_modlids(),omp_results[nthread]);
        refmr.initKnownMR(input.MRSET[k]);
        refmr.initSearchMR();
        refmr.calcKnownVAR();
        refmr.calcKnown();
        input.MRSET[k].ORIG_LLG = refmr.likelihoodFn();
        input.MRSET[k].ORIG_R = refmr.Rfactor();
// Use very precise interpolation here as to avoid discontinuities in
// RefineMR::targetFn() that is used in the minimiser's linesearch
        if (!SCORING)
        {
          refmr.ensPtr()->setPtInterpEV(8);
          Minimizer minimizer;
          minimizer.run(refmr,input.MACRO_MR,omp_results[nthread]);
// no longer necessary to use fine interpolation
          refmr.ensPtr()->setPtInterpEV(4);
          refmr.setUp();
        }

        input.MRSET[k].LLG = refmr.likelihoodFn();
        if (input.MACM_CHAINS.True())
        {
          input.MRSET[k].GYRE = refmr.getGyre(); //this is a holder for the perturbation results
        }
        input.MRSET[k].R = refmr.Rfactor();
        input.MRSET[k].KNOWN = refmr.getModls(); //overwrite with refined positions
        input.MRSET[k].DRMS = refmr.getDRMS();
        input.MRSET[k].CELL = refmr.getCELL();
        input.MRSET[k].DATACELL = refmr.getDATACELL();
        input.MRSET[k].ORIG_NUM = k+1;
        input.MRSET[k].NUM = k+1; //overwritten in duplicate screen
        input.MRSET[k].ANNOTATION += " LLG=" + dtos(input.MRSET[k].LLG,0);
        if (nthread == 0) results.logProgressBarNext(LOGFILE);
      } //end #pragma omp parallel for
      results.logProgressBarEnd(LOGFILE);
      input.MRSET.setup_newvrms(mr.ensPtr()->get_drms_vrms());

//The LLG used and reported during the refinement includes restraint terms
//The reported LLG in the Table does not include restraint terms
//If Table LLG output<input then it is because the restraint terms are dominating
#ifdef PHASERVRMSLLGTESTING
if (SCORING)
{
std::string ofile = input.FILEROOT + ".VRMS_LLG.txt";
std::cout << "Flag --VRMSLLGtesting: " << ofile << std::endl;
std::ofstream ofsfile(ofile.c_str());
for (int k = 0; k < MRSET.size()-1; k++)
 ofsfile <<  "{" << MRSET[k].NEWVRMS.begin()->second[0] << ", " << MRSET[k].LLG << "},";
int k = MRSET.size()-1;
 ofsfile <<  "{" << MRSET[k].NEWVRMS.begin()->second[0] << ", " << MRSET[k].LLG << "}";
ofsfile.close();
}
#endif

      for (int nthread = 0; nthread < nthreads; nthread++)
      {
        results += omp_results[nthread];
        if (!(nthreads == 1 && results.level(unmute_level))) results << omp_results[nthread];
      }
      //append and optionally print silenced results from the various threads to the master thread

      if (input.MRSET.size() &&
          input.MACM_TFZEQ && //not if GIMBLE
          !input.PTNCS.use_and_present() &&
          !input.MRSET.PTF)
      { //calculate_new_purge_mean(true); //can purge more if the mean goes up
        results.logUnderLine(LOGFILE,"Mean and Sigma after Refinement (for Purge)");
        //Generate list of random orientations and  positions outside of parallel code
        const int const_numSample = 500;
        int numROT = std::min(const_numSample,int(input.MRSET.size()));
        int numTRA = const_numSample/numROT;//integer division
        int numSample = numTRA*numROT;
        if (numSample < const_numSample) numTRA++;
        numSample = numTRA*numROT;
        float1D List(numSample);
        results.logTab(1,LOGFILE,"Scoring "+itos(numSample)+" randomly sampled orientations and translations");
        dmat331D randomROT,randomROT2;
        for (unsigned e = 0; e < numROT; e++)
        {
          dmat33 R = euler2matrixDEG(input.MRSET[e].KNOWN.back().getEuler());
          mr.ensPtr()->configure(input.MRSET[e].set_of_modlids(),input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[e].set_of_modlids(),results);
          std::string TRA_MODLID = input.MRSET[e].KNOWN.back().MODLID;
          PHASER_ASSERT(mr.ensPtr()->find(TRA_MODLID) != mr.ensPtr()->end());
          dmat33 ROT = R*input.PDB[TRA_MODLID].PR.transpose();
          dmat33 ROT2(1,0,0,0,1,0,0,0,1);
          randomROT.push_back(ROT);
          randomROT2.push_back(ROT2);
        }
        unsigned nthreads = 1;
#pragma omp parallel
        {
#ifdef _OPENMP
          nthreads = omp_get_num_threads();
#endif
        }
        results.logBlank(LOGFILE);
        if (nthreads > 1)
          results.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
        results.logProgressBarStart(LOGFILE,"Generating Statistics",numROT/nthreads);
        //calculate randomTRA only once, at the start
        dvect31D randomTRA;
        scitbx::random::mersenne_twister generator(0);
        for (unsigned irand = 0; irand < numSample; irand++)
        {
          double rand1 = generator.random_double(); //[0,1]
          double rand2 = generator.random_double(); //[0,1]
          double rand3 = generator.random_double(); //[0,1]
          randomTRA.push_back(dvect3(rand1,rand2,rand3));
        }
        std::string TRA_MODLID = input.MRSET[0].KNOWN.back().MODLID;

#pragma omp parallel for firstprivate(mr,omp_PDB)
        for (int r = 0; r < numROT; r++)
        {
          int nthread = 0;
#ifdef _OPENMP
          nthread = omp_get_thread_num();
#endif
          DataMR ompmr(mr);
          ompmr.ensPtr()->configure(input.MRSET[r].set_of_modlids(),omp_PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[r].set_of_modlids(),omp_results[nthread]);
          ompmr.ensPtr()->setup_vrms(input.MRSET[r].DRMS);
          ompmr.changeSpaceGroup(input.MRSET[r].HALL,HIRES,input.LORES);
          data_bofac SEARCH_FACTORS(input.MRSET[r].KNOWN.back().BFAC,input.MRSET[r].KNOWN.back().OFAC); // For TFZ==, put refined BFAC with refined OFAC
          ompmr.initKnownMR(input.MRSET[r]);
          ompmr.deleteLastKnown();
          ompmr.calcKnownVAR();
          ompmr.calcKnown();
          std::string TRA_MODLID = input.MRSET[r].KNOWN.back().MODLID;
          ompmr.calcSearchVAR(TRA_MODLID,halfR,SEARCH_FACTORS);
          ompmr.calcSearchTRA1(randomROT[r],TRA_MODLID);
          cmplx1D interpE = ompmr.getInterpE(randomROT[r],TRA_MODLID);
          for (int t = 0; t < numTRA; t++)
          {
            int i = r*numTRA+t;
            PHASER_ASSERT(i < randomTRA.size());
            ompmr.calcSearchTRA2(randomTRA[i],halfR,SEARCH_FACTORS,&interpE); //interpE gets shifted by randomTRA
            List[i] = ompmr.likelihoodFn();
          }
          if (nthread == 0) results.logProgressBarNext(LOGFILE);
        } //end pragma omp parallel
        results.logProgressBarEnd(LOGFILE);

        PHASER_ASSERT(List.size());
        double r_mean = mean(List);
        //double r_sigma = sigma(List);
        input.MRSET.get_purge_mean() ?
          results.logTab(1,LOGFILE,"Mean Score for Purge before Refinement:  " + dtos(input.MRSET.get_purge_mean(),2)):
          results.logTab(1,LOGFILE,"No Mean Score for Purge before Refinement");
        results.logTab(1,LOGFILE,"Mean Score for Purge after Refinement :  " + dtos(r_mean,2));
        results.logBlank(LOGFILE);
        input.MRSET.set_purge_mean(r_mean);
      }

      SCORING = false; //always produce RNP table
      std::string loggraph;
      for (int v = 0; v < input.MRSET.size(); v++)
        loggraph += SCORING ?
          itos(v+1) + " " +  dtos(input.MRSET[v].LLG,10,2) + " " + dtos(input.MRSET[v].R,5,2) + "\n":
          itos(v+1)+" "+dtos(input.MRSET[v].LLG,10,2)+" "+dtos(input.MRSET[v].ORIG_LLG,10,2)+" " +dtos(input.MRSET[v].R,5,2)+" "+dtos(input.MRSET[v].ORIG_R,5,2)+"\n";
      string1D graphs(2);
      graphs[0] = SCORING ? "Solution Number vs LL-gain:AUTO:1,2":
                  "Solution Number vs LL-gain:AUTO:1,2,3";
      graphs[1] = SCORING ? "Solution Number vs R-value:AUTO:1,3":
                  "Solution Number vs R-value:AUTO:1,4,5";
      stringset search_modlids;
      int ncomponent(0);
      for (int k = 0; k < input.MRSET.size(); k++)
      {
        int l = input.MRSET[k].KNOWN.size()-1;
        search_modlids.insert(input.MRSET[k].KNOWN[l].MODLID);
        ncomponent = input.MRSET[k].KNOWN.size(); //assume all the same - reasonable
      }
      std::string str_modlids;
      for (std::set<std::string>::iterator iter = search_modlids.begin(); iter != search_modlids.end(); iter++)
        str_modlids +=  *iter + " OR ";
      str_modlids = std::string(str_modlids,0,str_modlids.size()-4);
      if (!SCORING)
        results.logGraph(LOGFILE,"Refinement After Placing Component #" + itos(ncomponent) + " (" + str_modlids + " " + dtos(HIRES,5,2) + "A) ",graphs,"Number final-LLG initial-LLG final-R initial-R",loggraph);

      results.logSectionHeader(LOGFILE,"FIND DUPLICATES");
      results.logTab(1,LOGFILE,"Check for nearly equivalent solutions");
      ListEditing edit(!input.MACM_CHAINS.True());
      pair_int_int ndel = edit.calculate_duplicates(input.PDB,input.UNIT_CELL,HIRES,input.MRSET,input.MRTEMPLATE,&results);
      if (ndel.first == 0) results.logTab(1,LOGFILE,"No duplicate solutions found");
      else if (ndel.first == 1) results.logTab(1,LOGFILE,"One duplicate solution found");
      else if (ndel.first > 1) results.logTab(1,LOGFILE,itos(ndel.first)+" duplicate solutions found");
      results.logBlank(LOGFILE);

      results.logSectionHeader(LOGFILE,"FIND TEMPLATE MATCHES");
      if (input.MRTEMPLATE.size())
      {
        results.logTab(1,LOGFILE,"Solutions will be compared with Template Solution");
        for (unsigned t = 0; t < input.MRTEMPLATE.size(); t++)
        {
          results.logTab(1,LOGFILE,"Template #" + itos(t+1));
          for (unsigned s = 0; s < input.MRTEMPLATE[t].KNOWN.size(); s++)
            results.logTab(2,LOGFILE,input.MRTEMPLATE[t].KNOWN[s].logfile(),false);
        }
        if (ndel.second == 0) results.logTab(1,LOGFILE,"No solutions moved to match template");
        else if (ndel.second == 1) results.logTab(1,LOGFILE,"One solution moved to match template");
        else if (ndel.second > 1) results.logTab(1,LOGFILE,itos(ndel.second)+" solutions moved to match templates");
      }
      else  results.logTab(1,LOGFILE,"No Template Solution(s) for comparison");
      results.logBlank(LOGFILE);

      if (input.MRSET.size() &&
          input.PURGE_RNP.ENABLE &&
         // !one_mol_in_P1 && //calculate new mean instead
         ((!input.PURGE_RNP.NUMBER && input.MRSET.has_been_rescored()) || input.PURGE_RNP.NUMBER)) //TF rescored/brute or RF in P1
      {
        int start_num = input.MRSET.size();
        input.ZSCORE.set_top(input.MRSET.top_TFZ()); //half of top for partially correct
        results.logSectionHeader(SUMMARY,"PURGE SELECTION");
        results.logUnderLine(SUMMARY,"Purge solutions according to highest LLG from Refinement");
        results.logTab(1,SUMMARY,"Top LLG (all) = " + dtos(input.MRSET.top_LLG()));
        results.logTab(1,SUMMARY,"Top LLG (packs) = " + dtos(input.MRSET.top_packs_LLG()));
        results.logTab(1,SUMMARY,"Mean LLG = " + dtos(input.MRSET.get_purge_mean()));
        llg_cutoff cutoff = input.MRSET.purge_llg(input.PURGE_RNP.get_percent(),input.PURGE_RNP.NUMBER,input.ZSCORE);
        results.logTab(1,SUMMARY,"Percent used for purge = " + dtos(input.PURGE_RNP.get_percent()*100) + "%");
        results.logTab(2,SUMMARY,"Cutoff for acceptance = " + dtos(cutoff.perc,1));
        if (input.PURGE_RNP.NUMBER)
        {
          results.logTab(1,SUMMARY,"Number used for purge  = " + itos(input.PURGE_RNP.NUMBER));
          results.logTab(2,SUMMARY,"Cutoff for acceptance = " + dtos(cutoff.num,1));
        }
        if (input.ZSCORE.USE)
        {
          results.logTab(1,SUMMARY,"All Z-scores over " + dtos(input.ZSCORE.cutoff()) + " retained");
          results.logTab(2,SUMMARY,"Number high TFZ retained = " + itos(cutoff.ntfz));
        }
        results.logTab(1,SUMMARY,"Overall cutoff for acceptance (excluding high TFZ) = " + dtos(std::max(cutoff.num,cutoff.perc),1));
        results.logTab(1,SUMMARY,"Number of solutions stored before purge = " + itos(start_num));
        results.logTab(1,SUMMARY,"Number of solutions stored (deleted) after purge = " +
                         itos(start_num-cutoff.npurge) + " (" + itos(cutoff.npurge) + ")");
        results.logBlank(SUMMARY);
      }

      if (input.MRSET.size() && input.MACM_TFZEQ) //not if GIMBLE
      {
        input.MRSET.sort_LLG();
        results.logSectionHeader(LOGFILE,"TFZ EQUIVALENTS");
        for (int t = 0; t < input.MRSET.size(); t++)
          input.MRSET[t].TFZeq = 0; //clear!
        results.logBlank(LOGFILE);
        std::string modlid = input.MRSET[0].KNOWN[input.MRSET[0].KNOWN.size()-1].MODLID;
        if (input.PTNCS.use_and_present())
        {
          results.logTab(1,LOGFILE,"Translational NCS present");
          results.logTab(1,LOGFILE,"Refined TFZ equivalent not accurate and therefore not calculated");
          results.logBlank(LOGFILE);
        }
        else if (input.PDB[modlid].is_atom)
        {
          results.logTab(1,LOGFILE,"Single atom MR");
          results.logTab(1,LOGFILE,"Refined TFZ equivalent not calculated");
          results.logBlank(LOGFILE);
        }
        else if (input.MRSET.PTF) //phased translation function
        {
          results.logTab(1,LOGFILE,"TFZ from phased translation function");
          for (int t = 0; t < input.MRSET.size(); t++)
            input.MRSET[t].TFZeq = input.MRSET[t].TFZ;
          results.logBlank(LOGFILE);
        }
        else
        { //TFZ==
          results.logTab(1,LOGFILE,"Refined TFZ equivalents calculated");
          bool one_mol_in_P1(mr.spcgrpnumber() == 1 && input.MRSET.size() && input.MRSET[0].KNOWN.size() == 1);
          if (one_mol_in_P1)
            results.logTab(2,LOGFILE,"First molecule in P1: TFZ (RFZ) calculated from rotations only");
          bool1D calcTFZ(input.MRSET.size(),false);
          for (int t = 0; t < input.MRSET.size(); t++)
          {
            calcTFZ[t] = ((
              input.MRSET[t].TFZ > input.ZSCORE.POSSIBLY_SOLVED ||
              std::count(calcTFZ.begin(), calcTFZ.end(), true) < input.NUM_TOPFILES) &&
                     input.MRSET[t].KEEP &&  //not a duplicate
                     !input.MRSET[t].PURGE);  //not purged
          }
          int NumTFZ = std::count(calcTFZ.begin(), calcTFZ.end(), true);
          results.logTab(1,LOGFILE,itos(NumTFZ) + " top TFZ equivalents calculated");
          results.logBlank(LOGFILE);

          //Generate list of random positions outside of parallel code
          const int const_numSample = 500;
          dvect31D randomTRA;
          scitbx::random::mersenne_twister generator(0);
          for (unsigned irand = 0; irand < const_numSample; irand++)
          {
            if (one_mol_in_P1)
            {
              //alpha and gamma: uniform probability over 0-360
              //gamma: 0-180 with probability proportional to sin(beta)
              double alpha = 360*generator.random_double();
              double beta = scitbx::rad_as_deg(acos(2*generator.random_double() - 1));
              double gamma = 360*generator.random_double();
              randomTRA.push_back(dvect3(alpha,beta,gamma));
            }
            else
            {
              double rand1 = generator.random_double(); //[0,1]
              double rand2 = generator.random_double(); //[0,1]
              double rand3 = generator.random_double(); //[0,1]
              randomTRA.push_back(dvect3(rand1,rand2,rand3));
            }
          }

          int top_count(1);
          for (int t = 0; t < calcTFZ.size(); t++)
          if (calcTFZ[t])
          {
            results.logUnderLine(LOGFILE,"SOLUTION #"+itos(t+1) + " OF " + itos(input.MRSET.size()));
            results.logTab(1,LOGFILE,"TFZ equivalent calculation #"+itos(top_count++) + " of " + itos(NumTFZ));
            (t < input.NUM_TOPFILES) ?
              results.logTab(2,LOGFILE,"TOPFILE"):
              results.logTab(2,LOGFILE,"TFZ > POSSIBLY_SOLVED (" + dtos(input.ZSCORE.POSSIBLY_SOLVED) +")");
            mr.ensPtr()->configure(input.MRSET[t].set_of_modlids(),input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[t].set_of_modlids(),results);
            results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
            mr.changeSpaceGroup(input.MRSET[t].HALL,HIRES,input.LORES);
            data_bofac SEARCH_FACTORS(input.MRSET[t].KNOWN.back().BFAC,input.MRSET[t].KNOWN.back().OFAC); // For TFZ==, put refined BFAC with refined OFAC
            mr.initKnownMR(input.MRSET[t]);
            mr.deleteLastKnown();
            mr.calcKnownVAR();
            mr.calcKnown();
            std::string TRA_MODLID = input.MRSET[t].KNOWN.back().MODLID;
            mr.calcSearchVAR(TRA_MODLID,halfR,SEARCH_FACTORS);
            cmplx1D interpE;
            if (!one_mol_in_P1)
            {
              dmat33 R = input.MRSET[t].KNOWN.back().R;
              dmat33 ROT = R*input.PDB[TRA_MODLID].PR.transpose();
              mr.calcSearchTRA1(ROT,TRA_MODLID);
              interpE = mr.getInterpE(ROT,TRA_MODLID);
            }
            results.logBlank(LOGFILE);
            if (nthreads > 1)
              results.logTab(1,LOGFILE,"Spreading calculation onto " + itos(nthreads) + " threads.");
            results.logProgressBarStart(LOGFILE,"Generating Statistics",const_numSample/nthreads);
            float1D List(const_numSample);
#pragma omp parallel for firstprivate(mr)
            for (int tt = 0; tt < const_numSample; tt++)
            {
              int nthread = 0;
#ifdef _OPENMP
              nthread = omp_get_thread_num();
#endif
              if (one_mol_in_P1)
              {
                DataMR rotmr(mr);
                dmat33 R = euler2matrixDEG(randomTRA[tt]); //i.e. Euler
                dmat33 ROT = R*input.PDB[TRA_MODLID].PR.transpose();
                rotmr.calcSearchROT(ROT,TRA_MODLID,SEARCH_FACTORS);
                List[tt] = rotmr.likelihoodFn();
              }
              else
              {
                mr.calcSearchTRA2(randomTRA[tt],halfR,SEARCH_FACTORS,&interpE); //interpE gets shifted by randomTRA
                List[tt] = mr.likelihoodFn();
              }
              if (nthread == 0) results.logProgressBarNext(LOGFILE);
            } //end pragma omp parallel
            results.logProgressBarEnd(LOGFILE);

            double r_mean = mean(List);
            double r_sigma = sigma(List);
            results.logTab(1,LOGFILE,"Mean Score (Sigma):       " + dtos(r_mean,2) + "   (" + dtos(r_sigma,2) + ")");
            PHASER_ASSERT(r_sigma);
            double TFZeq = (input.MRSET[t].LLG-r_mean)/r_sigma;
            (input.MRSET[t].TFZ) ?
            results.logTab(1,LOGFILE,"Refined TF/TFZ equivalent = " + dtos(input.MRSET[t].LLG,2) + "/" + dtos(TFZeq,5,1) + " (Unrefined TF/TFZ=" + dtos(input.MRSET[t].ORIG_LLG,2) + "/" + dtos(input.MRSET[t].TFZ,5,1) + ")"):
            results.logTab(1,LOGFILE,"Refined TF/TFZ equivalent = " + dtos(input.MRSET[t].LLG,2) + "/" + dtos(TFZeq,5,1) + " (Unrefined TF=" + dtos(input.MRSET[t].ORIG_LLG,2) + ")");
            input.MRSET[t].TFZeq = TFZeq; //don't replace TFZ with TFeq, so NOT used for ZSCORE selection in FAST search
            input.MRSET[t].KNOWN.back().TFZeq = TFZeq;
            input.MRSET[t].ANNOTATION += " TFZ==" + dtos(TFZeq,1);
            results.logBlank(LOGFILE);
          } //loop over TFZ
        }
      }

      int2D HISTORY(input.MRSET.size());
      results.logSectionHeader(SUMMARY,"RESULTS");
      if (SCORING)
      {
        results.logGraph(LOGFILE,"LLG Scoring After Placing Component #" + itos(ncomponent) + " (" + str_modlids + ") " + mr.spcgrpname(),graphs,"Number LLG R-value",loggraph);
        input.MRSET.sort_LLG();
        results.logUnderLine(SUMMARY,"Scoring Table");
        results.logTabPrintf(1,SUMMARY,"%-3s %14s  %4s    %-11s\n",
          "#in","LLG","Rval","SpaceGroup");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
          results.logTabPrintf(1,SUMMARY,"%-3d %14.2f  %4.1f    %-11s\n",
            input.MRSET[k].ORIG_NUM, input.MRSET[k].LLG,input.MRSET[k].R,
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
        results.logBlank(SUMMARY);
      }
      else
      {
        { //memory sorted table
        input.MRSET.sort_LLG();
        input.MRSET.calc_contrast(); //whether or not new mean is calculated
        string1D contrast(input.MRSET.size()); //length 6
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          if (!input.MRSET[k].CONTRAST)
            contrast[k] = "  n/a ";
          else if (input.MRSET[k].CONTRAST >= 9.999)
            contrast[k] = ">9.999";
          else
            contrast[k] = ((k+1==input.MRSET.size()) ? ">" : " ") + dtos(input.MRSET[k].CONTRAST,5,3);
        }
        for (int k = input.MRSET.size()-1; k >= 0 ; k--)
        for (int kk = 0; kk <= k ; kk++)
        {
          if (input.MRSET[k].NUM == input.MRSET[kk].NUM) contrast[kk] = contrast[k];
        }
        outStream  where(CONCISE);
        results.logUnderLine(where,"Refinement Table (Sorted)");
        if (mr.fullHiRes() > mr.HiRes()-1.0e-03)
          results.logTab(1,where,"Refinement to full resolution");
        results.logTabPrintf(1,where,"%-4s %-5s %-3s %-2s %20s %24s %-11s %6s\n",
          "#out","=#out","#in","=T","(Start LLG Rval TFZ)","(Refined LLG Rval TFZ==)","SpaceGroup","Cntrst");
        //now correct for when contrast is given by lowest of equivalent list of solutions
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          results.logTabPrintf(1,where,"%-4s %-5s %-4d%2s %10s %4s %4s %12s %4s %4s   %-11s %6s\n",
            (input.MRSET[k].KEEP && !input.MRSET[k].PURGE) ? (input.MRSET[k].NUM==1 ? "Top1" : itos(input.MRSET[k].NUM,4).c_str()) : "---",
            !input.MRSET[k].KEEP ? (input.MRSET[k].PURGE ? "---" : itos(input.MRSET[k].NUM).c_str()) : "---",
            input.MRSET[k].ORIG_NUM,
            input.MRSET[k].TMPLT.size() ? itos(input.MRSET[k].TMPLT[0],2).c_str() : "",
            dtos(input.MRSET[k].ORIG_LLG,10,1).c_str(),
            input.MRSET[k].ORIG_R > 99 ? " >99" : dtos(input.MRSET[k].ORIG_R,4,1).c_str(),
            input.MRSET.has_been_rescored() ? dtos(input.MRSET[k].TFZ,4,1).c_str() : "n/a ",
            dtos(input.MRSET[k].LLG,12,1).c_str(),
            input.MRSET[k].R > 99 ? " >99" : dtos(input.MRSET[k].R,4,1).c_str(),
            !input.MRSET[k].TFZeq  ? " n/a" : dtos(input.MRSET[k].TFZeq,4,1).c_str(),
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str(),
            contrast[k].c_str());
          for (int kt = 1; kt < input.MRSET[k].TMPLT.size(); kt++)
            results.logTabPrintf(1,where,"%-4s %8s %2s\n",
            input.MRSET[k].KEEP ? (input.MRSET[k].NUM==1 ? "Top1" : itos(input.MRSET[k].NUM,4).c_str()) : "---",
            " also =T ",
            itos(input.MRSET[k].TMPLT[kt],2).c_str());
        }
        results.logBlank(where);
        }//memory sorted table
        { //history
        for (unsigned k = 0; k < input.MRSET.size(); k++)
          if (input.MRSET[k].NUM-1 >=0) //flag -999
            HISTORY[input.MRSET[k].NUM-1].push_back(input.MRSET[k].ORIG_NUM);
        }
        {//memory unsorted table
        input.MRSET.unsort();
        outStream   where(VERBOSE);
        results.logUnderLine(where,"Refinement Table (Unsorted)");
        if (input.HIRES < 0)
          results.logTab(1,where,"Refinement to full resolution");
        results.logTabPrintf(1,where,"%-4s %-4s %-5s%2s %20s %24s %5s %-11s\n",
          "#in","#out","=#out","=T","(Start LLG Rval TFZ)","(Refined LLG Rval TFZ==)","Packs","SpaceGroup");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          results.logTabPrintf(1,where,"%-4d %-4s %-4s%-3s %10s %4s %4s %12s %4s %4s   %5s %-11s\n",
            input.MRSET[k].ORIG_NUM,
            (input.MRSET[k].KEEP && !input.MRSET[k].PURGE) ? (input.MRSET[k].NUM==1 ? "Top1" : itos(input.MRSET[k].NUM,4).c_str()) : "---",
            !input.MRSET[k].KEEP ? (input.MRSET[k].PURGE ? "---" : itos(input.MRSET[k].NUM).c_str()) : "---",
            input.MRSET[k].TMPLT.size() ? itos(input.MRSET[k].TMPLT[0],2).c_str() : "",
            dtos(input.MRSET[k].ORIG_LLG,10,1).c_str(),
            input.MRSET[k].ORIG_R > 99 ? " >99" : dtos(input.MRSET[k].ORIG_R,4,1).c_str(),
            input.MRSET.has_been_rescored() ? dtos(input.MRSET[k].TFZ,4,1).c_str() : "n/a ",
            dtos(input.MRSET[k].LLG,12,1).c_str(),
            input.MRSET[k].R > 99 ? " >99" : dtos(input.MRSET[k].R,4,1).c_str(),
            !input.MRSET[k].TFZeq  ? " n/a" : dtos(input.MRSET[k].TFZeq,4,1).c_str(),
            btos(input.MRSET[k].PACKS).c_str(),
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
          for (int kt = 1; kt < input.MRSET[k].TMPLT.size(); kt++)
            results.logTabPrintf(1,where,"%-4d %8s %2s\n",
            input.MRSET[k].ORIG_NUM,"also =T ",
            itos(input.MRSET[k].TMPLT[kt],2).c_str());
        }
        results.logBlank(where);
        }//memory unsorted table

        if (input.MACM_CHAINS.True())
        {
          input.MRSET.sort_LLG();
          results.logUnderLine(SUMMARY,"Gimble Refinement Table");
          results.logTabPrintf(1,SUMMARY,"%-4s %4s %11s %13s %7s %7s %7s %-s\n",
          "#in","#out","(Start LLG)","(Refined LLG)","D-Angle"," D-Dist","  Vrms ","Ensemble");
          for (int k = 0; k < input.MRSET.size(); k++)
          {
            results.logTabPrintf(1,SUMMARY,"%-4i %-4s %11.1f %13.1f\n",
                  input.MRSET[k].ORIG_NUM,
              (input.MRSET[k].KEEP && !input.MRSET[k].PURGE) ? (input.MRSET[k].NUM==1 ? "Top1" : itos(input.MRSET[k].NUM,4).c_str()) : "---",
                  input.MRSET[k].ORIG_LLG,
                  input.MRSET[k].LLG
                  );
            for (int p = 0; p < input.MRSET[k].GYRE.size(); p++)
            {
              float1D fixed_vrms = DV[input.MRSET[k].GYRE[p].MODLID].fixed_vrms_array();
              float1D vrms = newvrms(input.MRSET[k].DRMS[input.MRSET[k].GYRE[p].MODLID],fixed_vrms);
              results.logTabPrintf(1,SUMMARY,"%-4s %-4s %11s %13s %7.3f %7.3f %7.3f %-s\n",
                  "","","","",
                  input.MRSET[k].GYRE[p].angle(),
                  input.MRSET[k].GYRE[p].distance(),
                  vrms[0],
                  input.MRSET[k].GYRE[p].MODLID.c_str()
                  );
            }
            results.logTab(1,SUMMARY,"----");
          }
        }
        //clear the GYRE immediately after printing
        //for (unsigned k = 0; k < input.MRSET.size(); k++) input.MRSET[k].GYRE.clear();
      }
      if (!input.MACM_CHAINS.True()) //otherwise reported in GIMBLE table
      {
        outStream where(SUMMARY);
        results.logUnderLine(where,"Refinement Table (Variance Ranges)");
        results.logTab(1,where,"Range of delta-VRMS and VRMS given over current solution list (" + itos(input.MRSET.size()) + " solution(s))");
        stringset currentEns = input.MRSET.set_of_modlids();
        size_t maxens = 15;
        for (stringset::iterator iter = currentEns.begin(); iter != currentEns.end(); iter++)
          maxens = std::max(maxens,iter->size());
        results.logTabPrintf(1,where,"%-*s %6s %7s %34s\n",maxens,"Ensemble","Model#","  RMS  ","Delta-VRMS min/max  (VRMS min/max)");
        map_str_float maxdrms,mindrms;
        for (stringset::iterator iter = currentEns.begin(); iter != currentEns.end(); iter++)
        {
          maxdrms[*iter] = -10000;
          mindrms[*iter] = +10000;
          for (int k = 0; k < input.MRSET.size(); k++)
          {
            if (input.MRSET[k].DRMS.find(*iter) != input.MRSET[k].DRMS.end())
            {
              maxdrms[*iter] = std::max(input.MRSET[k].DRMS[*iter],maxdrms[*iter]);
              mindrms[*iter] = std::min(input.MRSET[k].DRMS[*iter],mindrms[*iter]);
            }
          }
        }
        for (stringset::iterator iter = currentEns.begin(); iter != currentEns.end(); iter++)
        {
          PHASER_ASSERT(DV.find(*iter) != DV.end());
          float1D fixed_vrms = DV[*iter].fixed_vrms_array();
          float1D maxvrms = newvrms(maxdrms[*iter],fixed_vrms);
          float1D minvrms = newvrms(mindrms[*iter],fixed_vrms);
          results.logTabPrintf(1,where,"%-*s %6d %7.3f   %+6.3f/%+6.3f    (%6.3f/%6.3f )\n",
            maxens,
            iter->c_str(),
            1,
            fixed_vrms[0],
            mindrms[*iter],
            maxdrms[*iter],
            minvrms[0],
            maxvrms[0]);
          for (int i = 1; i < fixed_vrms.size(); i++)
            results.logTabPrintf(1,where,"%-*s %6d %7.3f   %6s %6s    (%6.3f/%6.3f )\n",
             maxens,
            "",
            i+1,
            fixed_vrms[i],
            "","",
            minvrms[i],
            maxvrms[i]);
        }
        results.logBlank(where);
      }

      //REPLACE TFZ with TFZeq, so WILL BE used for ZSCORE selection in FAST search
      for (int k = 0; k < input.MRSET.size(); k++)
      if (input.MRSET[k].TFZeq)
        input.MRSET[k].TFZ = input.MRSET[k].TFZeq;

      input.MRSET.pruneDuplicateSolutions();
      input.MRSET.sort_LLG();

      //history
      for (unsigned k = 0; k < input.MRSET.size(); k++)
      {
        input.MRSET[k].HISTORY += input.MACM_CHAINS.True() ? " GMBL(" : " RNP(";
        input.MRSET[k].HISTORY += ivtos(HISTORY[k],' ',',')+":"+ itos(k+1)+")";
      }

      //can now lower the resolution to just the interpolation resolution if the cell is not refining
      if (ref_cell && mapHIRES) //the refinement had maps and had to take account of the max cell
      { //the cell was different
        scale_mapHIRES = mapHIRES*input.MRSET.max_cell_scale(); //refined cell scale values
        double thisHIRES = std::max(data_HIRES,std::max(sol_input_HIRES,scale_mapHIRES));
        //double thisHIRES = std::max(data_HIRES,std::max(0,scale_mapHIRES)); //input ignored?
        if (thisHIRES < HIRES)
        {
          HIRES = thisHIRES;
          mr.selectReso(HIRES,input.LORES);
          results.logTab(1,SUMMARY,"High resolution limit can be lowered after cell refinement");
          results.logTab(2,SUMMARY,"Maximum cell scale = " + dtos(input.MRSET.max_cell_scale()));
          results.logTab(1,SUMMARY,"High resolution limit imposed by Maps for MapCoefs = " + dtos(HIRES,5,2));
          results.logBlank(SUMMARY);
          input.MRSET.set_resolution(HIRES);
        }
      }

      //if the resolution has changed from that input
      input.MRSET.set_resolution(mr.HiRes());
      if (!between(input.MRSET.get_resolution(),sol_input_HIRES,1.0e-02))
        input.MRSET.set_as_rescored(false);

      results.setDotSol(input.MRSET);

      if (!input.MACM_CHAINS.True() && input.MRSET.size())
      { //Solution table
        input.MRSET.sort_LLG();
        int l = input.MRSET[0].KNOWN.size()-1;
        results.logUnderLine(VERBOSE,"Solution Table");
        results.logTabPrintf(1,VERBOSE,"%-4s%19s %6s %-31s   %-11s\n",
          "#","(Refined LLG TFZ==)","Modlid","Last-Component-Euler/Frac-Coord","SpaceGroup");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          results.logTabPrintf(1,VERBOSE,"%-4d %12s%5s  %-6s %4.0f%4.0f%4.0f/%6.2f%6.2f%6.2f   %-11s\n",
            k+1,
            dtos(input.MRSET[k].LLG,13,1).c_str(),
            !input.MRSET[k].TFZeq  ? " --- " : dtos(input.MRSET[k].TFZeq,5,1).c_str(),
            input.MRSET[k].KNOWN[l].MODLID.size() > 6 ? input.MRSET[k].KNOWN[l].MODLID.substr(0,6).c_str() : input.MRSET[k].KNOWN[l].MODLID.c_str(),
            input.MRSET[k].KNOWN[l].getEuler()[0],
            input.MRSET[k].KNOWN[l].getEuler()[1],
            input.MRSET[k].KNOWN[l].getEuler()[1],
            input.MRSET[k].KNOWN[l].TRA[0],
            input.MRSET[k].KNOWN[l].TRA[1],
            input.MRSET[k].KNOWN[l].TRA[2],
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
        }
        results.logBlank(VERBOSE);
      }
      else if (input.MRSET.size())
      { //Solution table
        input.MRSET.sort_LLG();
        results.logUnderLine(VERBOSE,"Solution Table");
        results.logTabPrintf(1,VERBOSE,"%-4s%4s %19s %-11s\n",
          "#","","(Refined LLG TFZ==)","SpaceGroup");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          results.logTabPrintf(1,VERBOSE,"%-4d%-4s %10.2f%9s %-11s\n",
            k+1,"",
            input.MRSET[k].LLG,
            !input.MRSET[k].TFZeq  ? " --- " : dtos(input.MRSET[k].TFZeq,9,2).c_str(),
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
          size_t x(20);
          for (unsigned l = 0; l < input.MRSET[k].KNOWN.size(); l++)
          {
            results.logTabPrintf(1,VERBOSE,"%-4s %-4d %4.0f%4.0f%4.0f/%6.2f%6.2f%6.2f %-*s\n",
              "",l+1,
              input.MRSET[k].KNOWN[l].getEuler()[0],
              input.MRSET[k].KNOWN[l].getEuler()[1],
              input.MRSET[k].KNOWN[l].getEuler()[1],
              input.MRSET[k].KNOWN[l].TRA[0],
              input.MRSET[k].KNOWN[l].TRA[1],
              input.MRSET[k].KNOWN[l].TRA[2],
              x,
              input.MRSET[k].KNOWN[l].MODLID.substr(0,std::min(x,input.MRSET[k].KNOWN[l].MODLID.size())).c_str()
              );
          }
          results.logBlank(VERBOSE);
        }
      }

      if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
      outStream where = (input.DO_KEYWORDS.True() || input.DO_XYZOUT.True() || input.DO_HKLOUT.True()) ? SUMMARY : LOGFILE;
      results.logSectionHeader(where,"OUTPUT FILES");
      if (input.DO_KEYWORDS.True()) results.writeSol();
      //always store pdb for output
      results.tracemol = input.tracemol;
      results.writePdb(input.DO_XYZOUT,input.PDB,input.MACM_CHAINS.True());
      //calculate map coeffs for results object
      {
        results.logTab(1,VERBOSE,"Applying sharpening of " + ftos(100*input.RESHARP.FRAC) +  "% of the B-factor");
        if (results.level(VERBOSE)) results.logTab(1,where,"Verbose results: F_ISO added to mtz file");
        int NumTop = std::min(input.NUM_TOPFILES,input.MRSET.size());
        results.logUnderLine(LOGFILE,"Calculation of Map Coefficients");
        results.logTab(1,LOGFILE,itos(NumTop) + " top map coefficients calculated");
        results.logBlank(LOGFILE);
        for (int t = 0; t < NumTop; t++)
        {
          results.logTab(1,LOGFILE,"Map coefficient calculated for top solution #"+itos(t+1));
          results.logTab(0,LOGFILE,input.MRSET[t].logfile(1),false);
          mr.ensPtr()->configure(input.MRSET[t].set_of_modlids(),input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[t].set_of_modlids(),results);
          results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
          mr.init();
          mr.changeSpaceGroup(input.MRSET[t].HALL,HIRES,input.LORES);
          mr.initKnownMR(input.MRSET[t]);
          mr.initSearchMR();
          mr.calcKnownVAR();
          mr.calcKnown();
          results.setMapCoefs(t,mr.getMapCoefs());
          results.logBlank(LOGFILE);
        }
        if (input.DO_HKLOUT.True())
          results.writeMtz(input.HKLIN);
      }
      for (int f = 0; f < results.getFilenames().size(); f++)
        results.logTab(1,where,results.getFilenames()[f]);
      if (!results.getFilenames().size()) results.logTab(1,where,"No files output");
      results.logBlank(where);
    }
    else
    {
      results.logSectionHeader(LOGFILE,std::string(SCORING?"SCORING":"REFINEMENT"));
      results.logTab(1,LOGFILE,"No solutions");
      results.logBlank(LOGFILE);
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_RNP(InputMR_RNP& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_RNP(input,output);
}

}  //end namespace phaser
