//All rights reserved
#include <phaser/main/runPhaser.h>
#include <boost/logic/tribool.hpp>
#include <phaser/src/Molecule.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/eLLG.h>
#include <phaser/pod/llg_hires.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/between.h>
#include <phaser/lib/rotdist.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/src/DataMR.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_FAST(InputMR_AUTO& inputAUTO,Output output)
{
  ResultMR results(output);
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  results.setLevel(inputAUTO.OUTPUT_LEVEL);
  results.setMute(inputAUTO.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success)
  {
    std::string cards = inputAUTO.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.REFLECTIONS = inputAUTO.REFLECTIONS;
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    inputAUTO.REFLECTIONS = resultMR_DAT.DATA_REFL;
    inputAUTO.setSPAC_HALL(resultMR_DAT.getHall());
    inputAUTO.setCELL6(resultMR_DAT.getCell6());
    inputAUTO.SGALT_BASE = "";
    }
  }
  if (success)
  try
  {
    results.init("LLG",inputAUTO.TITLE,inputAUTO.NUM_TOPFILES,inputAUTO.PDB);
    results.setUnitCell(inputAUTO.UNIT_CELL);
    results.setDotSol(inputAUTO.MRSET);

    //control for output files
    if (results.isPackagePhenix()) inputAUTO.DO_KEYWORDS.Default(false);
    //if kill is set, then output intermediate files
    //else, only output results at the end
    //store input values for control of output at the end
    bool DO_KEYWORDS = inputAUTO.DO_KEYWORDS.True();
    xyzout DO_XYZOUT = inputAUTO.DO_XYZOUT;
    bool DO_HKLOUT = inputAUTO.DO_HKLOUT.True();
    if (inputAUTO.FILEKILL == "" && inputAUTO.TIMEKILL == 0)
    {
      inputAUTO.setKEYW(false);
      inputAUTO.setXYZO(false);
      inputAUTO.setHKLO(false);
    }
    boost::tribool MACM_PROTOCOL_VRMS_FIX = boost::logic::indeterminate;

    MapEnsPtr         ensemble;
    MapTrcPtr         tracemol;
    floatType         rf_sampling(1);
    mr_solution       sol,last_sol;
    llg_hires         bestTop;
    search_component  best_search_component;
    search_array      AUTO_SEARCH;
    std::map<search_component,llg_hires> LLGmap;

    //flags
    int  cstart;
    bool success(true);
    bool partial_result(false),full_low_tfz_result(false);
    bool find_first_component(false);
    bool has_helix(false);
    bool found_first_component(false);
    bool packs_ok(true);
    bool added_second_component(false);
    const bool refine_VRMS_when_low_signal(false);
    bool NSEARCH_SET = true;

    cctbx::uctbx::unit_cell cctbxUC(inputAUTO.UNIT_CELL);
    floatType MTZ_HIRES = cctbx::uctbx::d_star_sq_as_d(cctbxUC.max_d_star_sq(inputAUTO.REFLECTIONS.MILLER.const_ref()));

    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputCCA input(cards);
      input.setRESO_HIGH(MTZ_HIRES);
      ResultCCA resultCCA = runCCA(input,results);
      results.setOutput(resultCCA);
      success = resultCCA.Success();
      if (resultCCA.Failure()) goto the_end;
    }
    //caught on input
    PHASER_ASSERT(!(!inputAUTO.AUTO_SEARCH.size() && inputAUTO.MRSET.size()));
    if (!inputAUTO.AUTO_SEARCH.size())
    {
      NSEARCH_SET = false;
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logWarning(LOGFILE,"Search number not input, inferred from composition and search ensemble. More copies may be found by using correct composition.");
      double MW(0);
      for (int s = 0; s < inputAUTO.SEARCH_ZERO.size(); s++)
      {
        MW += inputAUTO.PDB[inputAUTO.SEARCH_ZERO[s]].MW.total_mw();
        results.logTab(1,LOGFILE,"Search component #" + itos(s+1) + ": " + inputAUTO.SEARCH_ZERO[s]);
      }
      int NSYMM = cctbx::sgtbx::space_group(inputAUTO.SG_HALL,"A1983").order_z();
      double volume = cctbx::uctbx::unit_cell(inputAUTO.UNIT_CELL).volume();
      double solvent(inputAUTO.COMPOSITION.MIN_SOLVENT); //default min solvent
      if (inputAUTO.COMPOSITION.BY == "AVERAGE") //default
      { //change the default percent solvent to 80%
        inputAUTO.COMPOSITION.BY = "SOLVENT";
        inputAUTO.COMPOSITION.PERCENTAGE = solvent;
      }
      else
      {
        Composition composition(inputAUTO.COMPOSITION);
        double total_mw = composition.total_mw(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL);
        solvent = 1-(1.23/(volume/NSYMM/total_mw));
      }
      double z = (volume/(NSYMM*(1.23/(1-solvent))*MW)); //20% solvent composition is per asu
      int NUM = std::floor(z);
      results.logTab(1,LOGFILE,"Molecular weight of components:  " + dtos(MW));
      results.logTab(1,LOGFILE,"Volume of asymmetric unit:       " + dtos(volume/NSYMM));
      results.logTab(1,LOGFILE,"Packing to solvent content:      " + dtos(solvent*100,0) + "%");
      results.logTab(1,LOGFILE,"Maximum automatic search number: " + itos(NUM));
      results.logBlank(LOGFILE);
      for (int i = 0; i < NUM; i++)
      for (int s = 0; s < inputAUTO.SEARCH_ZERO.size(); s++)
      {
        search_component thisSearch;
        thisSearch.MODLID.push_back(inputAUTO.SEARCH_ZERO[s]);
        inputAUTO.AUTO_SEARCH.push_back(thisSearch);
      }
      results.logBlank(LOGFILE);
      results.logTab(1,LOGFILE,"Automatic Search Order:");
      for (int ci = 0; ci < inputAUTO.AUTO_SEARCH.size(); ci++)
        results.logTab(2,LOGFILE,"#" + itos(ci+1,2) + " " + inputAUTO.AUTO_SEARCH[ci].MODLID[0]);
      results.logBlank(LOGFILE);
      results.logTrailer(LOGFILE);
    }
    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputANO input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      ResultANO resultANO = runANO(input,results);
      results.setOutput(resultANO);
      success = resultANO.Success();
      if (resultANO.Failure()) goto the_end;
      inputAUTO.SIGMAN = resultANO.getSigmaN();
      inputAUTO.SIGMAN.REFINED = true; //flag for no more refinement
      MTZ_HIRES = resultANO.fullHiRes();
    }
    {
      std::string cards = inputAUTO.Cards();
      InputNCS input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.setNORM_DATA(inputAUTO.SIGMAN);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.PDB = inputAUTO.PDB; //copy the coordinates
      ResultNCS resultNCS = runNCS(input,results);
      results.setOutput(resultNCS);
      success = resultNCS.Success();
      if (resultNCS.Failure()) goto the_end;
      inputAUTO.PTNCS = resultNCS.getPtNcs();
      inputAUTO.PTNCS.REFINED = true; //flag for no more analysis
    }
    if (!inputAUTO.PTNCS.USE && inputAUTO.PTNCS.TRA.VECTOR_SET)
    { //assumes z-scores go back to normal with the correction applied
      results.logHeader(LOGFILE,header(MR_AUTO));
      inputAUTO.setZSCO_USE(false); //turn off Z-score solved because the cutoff is unknown for ptncs
      results.logTab(1,PROCESS,"Translational NCS inflates Z-scores");
      results.logTab(1,PROCESS,"Z-score amalgamation of solutions for multiple copies is DISABLED");
      results.logTab(1,PROCESS,"Packing function warnings based on Z-scores are DISABLED");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logTab(1,PROCESS,"Z-score test for definite solution is " + std::string(inputAUTO.ZSCORE.USE?"ON":"OFF"));
      if (!inputAUTO.ZSCORE.USE)
      {
        if (inputAUTO.PEAKS_ROT.DOWN)
          results.logTab(2,PROCESS,"Deep search has been disabled");
        inputAUTO.PEAKS_ROT.DOWN = 0; //FRF does not have ZSCORE input
        if (inputAUTO.ZSCORE.STOP)
          results.logTab(2,PROCESS,"ZSCORE STOP has been disabled");
        inputAUTO.setZSCO_STOP(false);
      }
      results.logTab(1,PROCESS,"Z-score test for stopping search is " + std::string(inputAUTO.ZSCORE.STOP?"ON":"OFF"));
      results.logTab(1,PROCESS,"Deep search is " + std::string(inputAUTO.PEAKS_ROT.DOWN?"ON":"OFF"));
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
    if (inputAUTO.PTNCS.use_and_present())
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logUnderLine(SUMMARY,"TNCS Checks");
      results.logTab(1,SUMMARY,"Number of copies per TNCS set = " + itos(inputAUTO.PTNCS.NMOL));
      results.logBlank(SUMMARY);
      std::map<std::string,int> MODLID = inputAUTO.MRSET.map_of_modlids();
      for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
        if (iter->second % inputAUTO.PTNCS.NMOL) //remainder
          results.logAdvisory(SUMMARY,"Solutions do not obey tNCS NMOL");
      MODLID = inputAUTO.AUTO_SEARCH.map_of_modlids();
      for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
        if (iter->second % inputAUTO.PTNCS.NMOL) //remainder
          results.logWarning(SUMMARY,"Number of search copies for " + iter->first + " (" + itos(iter->second) +  ") not divisible by " + itos(inputAUTO.PTNCS.NMOL) + "\nNumber of search copies edited");
      std::map<search_component,int> befor = inputAUTO.AUTO_SEARCH.map_of_components();
      inputAUTO.AUTO_SEARCH.edit_tncs(inputAUTO.PTNCS.NMOL);
      results.logUnderLine(SUMMARY,"TNCS search components");
      std::map<search_component,int> after = inputAUTO.AUTO_SEARCH.map_of_components();
      for (std::map<search_component,int>::iterator iter = after.begin(); iter != after.end(); iter++)
      {
        results.logTabPrintf(2,SUMMARY,"%-20s | %-5s | search: %-6s %-6s\n","Ensemble","input","sets","number");
        results.logTabPrintf(2,SUMMARY,"%-20s     %-5i           %-6i %-6i\n",
                                 iter->first.MODLID[0].c_str(),
                                 befor[iter->first],
                                 after[iter->first],
                                 after[iter->first]*inputAUTO.PTNCS.NMOL);
        for (int m = 1; m < iter->first.MODLID.size(); m++)
          results.logTabPrintf(2,SUMMARY,"or Ensemble \"%s\"\n",iter->first.MODLID[m].c_str());
      }
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
    //after editing search for TNCS
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      Composition composition(inputAUTO.COMPOSITION);
      composition.set_modlid_scattering(inputAUTO.PDB);
      floatType input_TOTAL_SCAT = composition.total_scat(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL);
      composition.table(SUMMARY,input_TOTAL_SCAT,inputAUTO.PDB,inputAUTO.SEARCH_FACTORS,results);
      composition.increase_total_scat_if_necessary(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL,inputAUTO.MRSET,inputAUTO.PTNCS,inputAUTO.SEARCH_FACTORS,inputAUTO.AUTO_SEARCH);
      if (composition.increased())
      {
        results.logWarning(SUMMARY,composition.warning());
        results.logTab(1,PROCESS,"Composition increased");
        results.logTab(2,SUMMARY," from " + dtos(input_TOTAL_SCAT,0));
        floatType TOTAL_SCAT = composition.total_scat(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL);
        results.logTab(3,SUMMARY,"to " + dtos(TOTAL_SCAT,0));
        composition.table(SUMMARY,TOTAL_SCAT,inputAUTO.PDB,inputAUTO.SEARCH_FACTORS,results);
      }
      else
        results.logTab(1,PROCESS,"Composition not increased");
      results.logBlank(LOGFILE);
      results.logTrailer(LOGFILE);
      inputAUTO.COMPOSITION = composition;
      if (composition.increased())
      {
        std::string cards = inputAUTO.Cards();
        InputCCA input(cards);
        input.setRESO_HIGH(MTZ_HIRES);
        ResultCCA resultCCA = runCCA(input,results);
        results.setOutput(resultCCA);
        success = resultCCA.Success();
        if (resultCCA.Failure()) goto the_end;
      }
    }
    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputDFAC input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.setTNCS(inputAUTO.PTNCS);
      input.setNORM_DATA(inputAUTO.SIGMAN);
      input.PDB = inputAUTO.PDB; //copy the coordinates
      ResultDFAC resultDFAC = runDFAC(input,results);
      results.setOutput(resultDFAC);
      success = resultDFAC.Success();
      if (resultDFAC.Failure()) goto the_end;
      inputAUTO.REFLECTIONS = resultDFAC.getReflData();
    }
    //after fixing search and composition
    { // Set the search order, by default using expected LLG at chosen resolution limit
      std::string cards = inputAUTO.Cards();
      InputMR_ELLG input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.PDB = inputAUTO.PDB; //copy the coordinates
      input.setTNCS(inputAUTO.PTNCS);
      input.setNORM_DATA(inputAUTO.SIGMAN);
      ResultELLG resultELLG = runMR_ELLG(input,results);
      results.setOutput(resultELLG);
      success = resultELLG.Success();
      if (resultELLG.Failure()) goto the_end;
      inputAUTO.AUTO_SEARCH = resultELLG.getSearch();
      inputAUTO.PDB = input.PDB; //unparse does not work
      inputAUTO.ELLG_DATA = resultELLG.ELLG_DATA; //unparse does not work
    }

    AUTO_SEARCH = inputAUTO.AUTO_SEARCH; //like ellg

//find first component
    if (inputAUTO.MRSET.size() != 0) //first component already found, spacegroup is therefore may or not be set, depending on whether recorded in the SOLU SET (may not be, if old .sol file)
    {
      found_first_component = true;
      //do not calculated LLG, cannot handle setting of alternative space groups
      sol = inputAUTO.MRSET;
      { //first thing, must check packing of input
        std::string cards = inputAUTO.Cards();
        InputMR_PAK input(cards);
        input.setSOLU(sol);
        input.setZSCO(inputAUTO.ZSCORE); //top has been set
        if (inputAUTO.PTNCS.use_and_present())
          input.setPACK_COMP(false); //don't move first solution to origin
        input.PDB = inputAUTO.PDB; //unparse does not work
        input.PACKING.KEEP_HIGH_TFZ = false;
        input.PACKING.PRUNE = false;
        input.tracemol = tracemol;
        ResultMR resultPAK = runMR_PAK(input,results);
        results.setOutput(resultPAK);
        success = resultPAK.Success();
        if (success)
        {
          tracemol = input.tracemol; //input passed by reference
          int orig_size = sol.size();
          sol = resultPAK.getDotSol(); //clashes on file
          if (!sol.size())
          {
            results.logHeader(LOGFILE,header(MR_AUTO));
            results.logTab(1,PROCESS,"Input solution did not pass packing test");
            results.logWarning(SUMMARY,"Input solutions did not pass packing test");
            results.logBlank(SUMMARY);
            results.logTrailer(LOGFILE);
            goto the_end;
          }
          else if (sol.size() < orig_size)
          {
            results.logHeader(LOGFILE,header(MR_AUTO));
            results.logTab(1,PROCESS,"Some input solutions did not pass packing test");
            results.logWarning(SUMMARY,"Some input solutions did not pass packing test");
            results.logBlank(SUMMARY);
            results.logTrailer(LOGFILE);
          }
        }
      } //packing
    }
    else //find first component
    {
      find_first_component = true;
      int c = 0;
      std::vector<search_component> zsearch = AUTO_SEARCH.set_of_components(c);
      bool new_component(true);
      bestTop.clear(); //first component only, but first component keeps changing
      bool can_lower_resolution(false);
    //  double sol_get_resolution(0);
      for (int z = 0; z < zsearch.size(); z++)
      {
        start_of_zsearch_loop:
        //store last sol if KEEP_HIGH_TFZ if false
        //then if PRUNE is on, can revert to last that packs
        if (!inputAUTO.PACKING.KEEP_HIGH_TFZ && inputAUTO.SEARCH_PRUNE && sol.num_packs())
          last_sol = sol; //will be a packing solution in list
        if (last_sol.size())
        {
        results.logHeader(LOGFILE,header(MR_AUTO));
        results.logTab(1,PROCESS,"Solution stored for possible reversion if next search fails");
        results.logTab(1,PROCESS,"Number of solutions = " + itos(sol.size()));
        results.logTab(1,PROCESS,"Solution TOP LLG = "+ dtos(sol.top_LLG(),1));
        results.logTab(1,PROCESS,"Solution TOP TFZ = "+ dtos(sol.top_TFZ(),1));
        results.logBlank(SUMMARY);
        results.logTrailer(LOGFILE);
        }
        //else results.logTab(1,PROCESS,"No solutions"); //don't log case of no solutions
        results.logHeader(LOGFILE,header(MR_AUTO));
        AUTO_SEARCH[c].set_star_clear();
        AUTO_SEARCH.set_modlid(c,zsearch[z]);
        if (z != 0) results.logTab(1,PROCESS,"Next permuted search order");
        results.logTab(1,PROCESS,"Search Order (next search *) (placed +):");
        { //logfile scope
        AUTO_SEARCH[c].set_star_search();
        string2D MODLID = AUTO_SEARCH.getModlidStar();
        PHASER_ASSERT(MODLID.size());
        for (int ci = 0; ci < MODLID.size(); ci++)
        {
          PHASER_ASSERT(MODLID[ci].size());
          int NMOL = inputAUTO.PTNCS.use_and_present() ? inputAUTO.PTNCS.NMOL : 1;
          results.logTab(2,PROCESS,"#" + itos(ci+1,2) + " " + stos(MODLID[ci][0],NMOL));
          for (int m = 1; m < MODLID[ci].size(); m++)
            results.logTab(3,PROCESS,"or " + stos(MODLID[ci][m],NMOL));
        }
        results.logBlank(SUMMARY);
        }
        results.logTrailer(LOGFILE);
        mr_solution rlist;
        {
          std::string cards = inputAUTO.Cards();
          InputMR_FRF input(cards);
          input.setREFL_DATA(inputAUTO.REFLECTIONS);
          input.setSOLU(inputAUTO.MRSET);
          input.DO_XYZOUT.Override(false);
          input.DO_KEYWORDS.Override(false);
          input.setSEAR_ENSE_OR_ENSE(AUTO_SEARCH[c].af_MODLID());
          input.setNORM_DATA(inputAUTO.SIGMAN);
          input.setTNCS(inputAUTO.PTNCS);
          input.ensemble = ensemble;
          input.tracemol = tracemol;
          input.PDB = inputAUTO.PDB; //unparse does not work
          input.ELLG_DATA = inputAUTO.ELLG_DATA; //unparse does not work
          input.SEARCH_FROM_AUTO = true; //unparse does not work
          ResultMR_RF resultFRF = runMR_FRF(input,results);
          results.setOutput(resultFRF);
          success = resultFRF.Success();
          if (resultFRF.Failure()) goto the_end;
          if (success)
          {
            rlist = resultFRF.getDotRlist();
            rf_sampling = resultFRF.getSampling();
            ensemble = input.ensemble; //input passed by reference
            tracemol = input.tracemol; //input passed by reference
            for (pdbIter iter = inputAUTO.PDB.begin(); iter != inputAUTO.PDB.end(); iter++)
            if (!iter->second.ENSEMBLE[0].map_format())
              has_helix = has_helix || iter->second.is_helix;
            //if any of the ensembles can be done at higher resolution
            //because the ellg thought they should be OK at lower resolution
            //then set can_lower_resolution flag overall and repeat entire thing from start
            //this will repeat ones that are already at highest resolution
            //but heyho, it is too complicated to fix this
            can_lower_resolution = can_lower_resolution || rlist.can_lower_resolution();
            //sol_get_resolution = rlist.get_resolution();
          }
          if (inputAUTO.RFAC_USE &&
              inputAUTO.PDB.size() == 1 &&
              !inputAUTO.PDB.begin()->second.ENSEMBLE[0].map_format()  &&
              inputAUTO.AUTO_SEARCH.size() == 1 &&
              (!inputAUTO.PTNCS.use_and_present() || inputAUTO.PTNCS.NMOL < 2))
          {
            if (resultFRF.getTopRfac() && resultFRF.getTopRfac() < inputAUTO.RFAC_CUTOFF)
            {
              results.logHeader(LOGFILE,header(MR_AUTO));
              results.logTab(1,PROCESS,"R-factor = " + dtos(resultFRF.getTopRfac(),5,2));
              results.logTab(1,PROCESS,"R-factor for PDB file at origin in original orientation is less than " + dtos(inputAUTO.RFAC_CUTOFF,5,2));
              results.logTab(1,PROCESS,"Molecular replacement is not necessary and is aborted");
              results.logBlank(SUMMARY);
              results.logTrailer(LOGFILE);
              found_first_component = true;
              partial_result = false;
              sol.resize(1);
              sol[0].KNOWN.resize(1);
              std::string modlid = AUTO_SEARCH[c].af_MODLID()[0];
              mr_ndim origin(modlid);
              sol[0].ANNOTATION += " RF*0 TF*0";
              sol[0].KNOWN[0] = origin;
              sol[0].HALL = resultFRF.getHall();
              results.setDotSol(sol);
              goto the_final_bit;
            }
          }
        }
        if (success && rlist.size())
        {
          //reset ZSCORE to input after change in purge (below)
          inputAUTO.ZSCORE.unset_top();
          mr_solution this_sol;
          ResultMR    this_result;
          if (success && rlist.size())
          { //memory scope
            std::string cards = inputAUTO.Cards();
            InputMR_FTF input(cards);
            input.setREFL_DATA(inputAUTO.REFLECTIONS);
            input.setSOLU(rlist);
            input.DO_XYZOUT.Override(false);
            input.DO_KEYWORDS.Override(false);
            input.setNORM_DATA(inputAUTO.SIGMAN);
            input.setTNCS(inputAUTO.PTNCS);
            input.ensemble = ensemble;
            input.tracemol = tracemol;
            input.PDB = inputAUTO.PDB; //unparse does not work
            ResultMR_TF resultFTF = runMR_FTF(input,results);
            results.setOutput(resultFTF);
            success = resultFTF.Success();
            if (success)
            {
              tracemol = input.tracemol; //input passed by reference: configured if ftf packing or writePdb
              this_sol = resultFTF.getDotSol();
              inputAUTO.ZSCORE.set_top(resultFTF.getTopTFZ()); //ready for half of top for partially correct
            }
            else goto the_end;
          } //ftf
          if (success && this_sol.size())
          {
            std::string cards = inputAUTO.Cards();
            InputMR_PAK input(cards);
            input.tracemol = tracemol;
            input.setSOLU(this_sol);
            input.setZSCO(inputAUTO.ZSCORE); //top has been set
            if (inputAUTO.PTNCS.use_and_present())
              input.setPACK_COMP(false); //don't move first solution to origin
            input.PDB = inputAUTO.PDB; //unparse does not work
            if (inputAUTO.SEARCH_PRUNE) input.setPACK_PRUN(true);
            ResultMR resultPAK = runMR_PAK(input,results);
            results.setOutput(resultPAK);
            success = resultPAK.Success();
            if (success)
            {
              tracemol = input.tracemol; //input passed by reference
              this_sol = resultPAK.getDotSol();
              this_result = resultPAK;
            }
            else goto the_end;
          } //packing
          if (success && this_sol.size())
          {
            std::string cards = inputAUTO.Cards();
            InputMR_RNP input(cards);
            input.setREFL_DATA(inputAUTO.REFLECTIONS);
            input.setSOLU(this_sol);
            input.setNORM_DATA(inputAUTO.SIGMAN);
            input.ensemble = ensemble;
            input.tracemol = tracemol;
            input.PDB = inputAUTO.PDB; //unparse does not work
            input.setTNCS(inputAUTO.PTNCS);
            input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != this_sol[0].KNOWN.size()));
            if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
              input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
            ResultMR resultRNP = runMR_RNP(input,results);
            results.setOutput(resultRNP);
            success = resultRNP.Success();
            if (success)
            {
              this_sol = resultRNP.getDotSol();
              this_result = resultRNP;
              MTZ_HIRES = resultRNP.fullHiRes();
            } //store sol
            else goto the_end;
            if (inputAUTO.MACM_PROTOCOL.is_default() &&
                inputAUTO.ZSCORE.USE && inputAUTO.ZSCORE.over_cutoff(this_sol.top_TFZ()) &&
                //refine_VRMS_when_low_signal &&
                !inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms))
            {
              results.logHeader(LOGFILE,header(MR_AUTO));
              MACM_PROTOCOL_VRMS_FIX = true;
              results.logTab(1,PROCESS,"Protocol for MR refinement is DEFAULT");
              results.logTab(1,PROCESS,"Z-scores over cutoff have been attained");
              results.logTab(1,PROCESS,"VRMS (variances) of ensemble(s) WILL NOT be refined (further)");
              results.logBlank(SUMMARY);
              results.logTrailer(LOGFILE);
            } //change refinement protocol
            if (success && this_sol.size())
            {
              LLGmap[zsearch[z]] = llg_hires(this_sol.top_LLG(),this_sol.get_resolution());
              floatType llg_tol = 1.0e-06; //so that if the cutoff is lowered/decreased but the top doesn't change (early in list but not over TFZ=8) then the second time through is accepted
              floatType reso_tol = 1.0e-02; //same as output
              bool same_resolution(between(bestTop.hires,this_sol.get_resolution(),reso_tol));
              if (sol.size() &&
                  !new_component &&
                  !same_resolution)
              {
                results.logHeader(LOGFILE,header(MR_AUTO));
                results.logTab(1,PROCESS,"Current and best solution not at same resolution");
                results.logTab(1,PROCESS,"Current LLG = " + dtos(this_sol.top_LLG(),1) + " (resolution = " + dtos(this_sol.get_resolution(),2) + "A)");
                results.logTab(2,PROCESS,"Component = " + this_sol.last_modlid());
                results.logTab(1,PROCESS,"Best LLG so far = " + dtos(bestTop.LLG,1) + " (resolution = " + dtos(bestTop.hires,2) + "A)");
                results.logTab(2,PROCESS,"Component = " + sol.last_modlid());
                results.logTab(1,PROCESS,"Component " + sol.last_modlid() + " refined to current resolution");
                results.logBlank(SUMMARY);
                results.logTrailer(LOGFILE);
                {
                std::string cards = inputAUTO.Cards();
                InputMR_RNP input(cards);
                input.setREFL_DATA(inputAUTO.REFLECTIONS);
                input.setRESO_HIGH(this_sol.get_resolution());
                double storeres=sol.get_resolution();
                sol.set_resolution(-999);//overwrite, so that higher resolution can be set
                input.setSOLU(sol); //old sol
                sol.set_resolution(storeres);//restore
                input.setNORM_DATA(inputAUTO.SIGMAN);
                input.setTNCS(inputAUTO.PTNCS);
                if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
                  input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
                if (!input.PURGE_RNP.NUMBER) input.setPURG_RNP_ENAB(false); //cannot purge because mean is wrong
                input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
                input.PDB = inputAUTO.PDB; //unparse does not work
                input.ensemble = ensemble;
                input.tracemol = tracemol;
                ResultMR tmp = runMR_RNP(input,results); //B-factors will change, just used to generate LLG scores
                results.setOutput(tmp);
                success = tmp.Success();
                if (tmp.Failure()) goto the_end;
                //sol = tmp.getDotSol(); //B-factors new values (chinese whispers if replace value)
                if (tmp.numSolutions()) bestTop.hires = tmp.HiRes();
                if (tmp.numSolutions()) bestTop.LLG = tmp.getTopLLG();
                }
                results.logHeader(LOGFILE,header(MR_AUTO));
                results.logTab(1,PROCESS,"Current and \"best\" solution after refinement at same resolution");
                results.logTab(1,PROCESS,"Current LLG = " + dtos(this_sol.top_LLG(),1) + " (resolution = " + dtos(this_sol.get_resolution(),2) + "A)");
                results.logTab(1,PROCESS,"\"Best\" LLG = " + dtos(bestTop.LLG,1) + " (resolution = " + dtos(bestTop.hires,2) + "A)");
                results.logBlank(SUMMARY);
              }
              if (resultRNP.getTopLLG() > bestTop.LLG-llg_tol || new_component)
              {
                sol = this_sol;
                this_result.setOutput(results);
                results = this_result;
                best_search_component = zsearch[z];
                results.phenixCallback("current best solution",this_sol[0].logfile(0, true));
                results.logHeader(LOGFILE,header(MR_AUTO));
                if (new_component)
                  results.logTab(1,PROCESS,"Current is Best Solution (first search)");
                else
                {
                  results.logTab(1,PROCESS,"Current is New Best Solution");
                  results.logTab(1,PROCESS,"Old Best LLG = " + dtos(bestTop.LLG,1) + " (resolution = " + dtos(bestTop.hires,2) + ")");
                }
                sol[0].KNOWN.size() > 1?
                results.logTab(1,PROCESS,"Current solution has " + itos(sol[0].KNOWN.size()) + " components"):
                results.logTab(1,PROCESS,"Current solution has 1 component");
                bestTop.LLG = results.getTopLLG();
                bestTop.hires = sol.get_resolution();
                results.logTab(1,PROCESS,"New Best LLG = " + dtos(bestTop.LLG,1) + " (resolution = " + dtos(bestTop.hires,2) + ")");
                results.logTab(1,PROCESS,"Best Component so far = " + best_search_component.logModlid());
                results.logBlank(SUMMARY);
                results.logTrailer(LOGFILE);
                found_first_component = true;
                new_component = false;
              }
              else
              {
                results.logHeader(LOGFILE,header(MR_AUTO));
                results.logTab(1,PROCESS,"Current is not Best Solution");
                results.logTab(1,PROCESS,"Current LLG = " + dtos(this_sol.top_LLG(),1) + " (resolution = " + dtos(this_sol.get_resolution(),2) + "A)");
                results.logTab(1,PROCESS,"Best LLG so far = " + dtos(bestTop.LLG,1) + " (resolution = " + dtos(bestTop.hires,2) + "A)");
                results.logTab(1,PROCESS,"Best Component so far = " + best_search_component.logModlid());
                results.logBlank(SUMMARY);
                results.logTrailer(LOGFILE);
              }
            } //save best
          } //rnp, result in scope
        } //frf
        if (success &&
           // space_group_name(inputAUTO.SG_HALL,true).CCP4 != "P 1" &&
            inputAUTO.ZSCORE.over_cutoff(sol.top_TFZ()))
        {
          break; //have found TFZ high enough to quit - don't permute though all components
        }
        else if (success && //even if there is only one spacegroup
            !inputAUTO.ZSCORE.over_cutoff(sol.top_TFZ()) //best solution is not good enough
            && can_lower_resolution //stored for case of !sol.size(), ELLG_HIRES = true
            && !inputAUTO.PDB[AUTO_SEARCH[c].af_MODLID()[0]].is_atom
            && z+1 == zsearch.size()) //have tried all components
        { //lower reduce decrease Lower Reduce Decrease
          inputAUTO.ELLG_HIRES = false;
          z = 0; //try first again
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"First component");
          results.logTab(1,PROCESS,"Use higher resolution for search");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          can_lower_resolution = false;
          last_sol = sol = mr_solution(); //clear
          bestTop.clear();
          results.logTab(1,PROCESS,"Solutions cleared");
          goto start_of_zsearch_loop;//avoiding z++
        }
        else if (sol.size() && success && z+1 == zsearch.size()) //have tried all components
        {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,SUMMARY,"First component placed");
          if (zsearch.size() > 1)
            results.logTab(1,SUMMARY,"End of possible ensembles for this component");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
        }
      } //permutations
      if (sol.size() && !sol.num_packs())
      {
        packs_ok = false;
        results.logHeader(LOGFILE,header(MR_AUTO));
        sol.size() == 1 ?
        results.logTab(1,PROCESS,"Solution with high TFZ does not pack"):
        results.logTab(1,PROCESS,"Solutions with high TFZ do not pack");
        if (AUTO_SEARCH.size() > 1)
        results.logTab(1,PROCESS,"Terminate search for components");
        results.logBlank(LOGFILE);
        results.logTrailer(LOGFILE);
      }
    }//find first component

    cstart = find_first_component ? 1 : 0;

    //make sure that the first component in the list is the best one (the one with the partial solution)
    if (success && find_first_component)  //the first component wasn't set
    {
      AUTO_SEARCH.set_modlid(0,best_search_component);
      if (AUTO_SEARCH.size() > 0) AUTO_SEARCH[0].set_star_found();
      if (AUTO_SEARCH.size() > 1) AUTO_SEARCH[1].set_star_search();
      for (int i = 2; i < AUTO_SEARCH.size(); i++) AUTO_SEARCH[i].set_star_clear();
    }

    if (success &&
        packs_ok &&
        found_first_component &&
        find_first_component &&  //the first component wasn't set
        AUTO_SEARCH.num_unique(cstart) > 1 && //only have to change order if there is more than one component
        inputAUTO.SEARCH_ORDER_AUTO &&
        LLGmap.size() &&
        (!inputAUTO.ZSCORE.STOP || (inputAUTO.ZSCORE.STOP && inputAUTO.ZSCORE.over_cutoff(sol.top_TFZ()))) )
    {
     //if all the components do not have an LLGmap, e.g. if one has failed on the packing
     //then the order will still change here according to the values available
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logTab(1,PROCESS,"Calculated LLG values:");
      results.logTabPrintf(2,PROCESS,"%-10s %-5s  %-s\n","LLG","@reso","Ensemble");
      for (std::map<search_component,llg_hires>::iterator iter = LLGmap.begin(); iter != LLGmap.end(); iter++)
      {
        results.logTabPrintf(2,PROCESS,"%10s %5.2f  %-s\n",
          dtos(iter->second.LLG,2).c_str(),iter->second.hires,iter->first.MODLID[0].c_str());
        for (int m = 1; m < iter->first.MODLID.size(); m++)
          results.logTabPrintf(2,PROCESS,"%10s %5s  %-s\n","","",iter->first.MODLID[m].c_str());
      }
      AUTO_SEARCH.sort_ellg(LLGmap,1);
      for (int i = 1; i < AUTO_SEARCH.size(); i++) AUTO_SEARCH[i].set_star_clear();
      if (AUTO_SEARCH.size() > 1) AUTO_SEARCH[1].set_star_search();
      results.logTab(1,PROCESS,"New Automatic Search Order:");
      { //logfile scope
      string2D MODLID = AUTO_SEARCH.getModlidStar();
      for (int ci = 0; ci < MODLID.size(); ci++)
      {
        int NMOL = inputAUTO.PTNCS.use_and_present() ? inputAUTO.PTNCS.NMOL : 1;
        results.logTab(2,PROCESS,"#" + itos(ci+1,2) + " " + stos(MODLID[ci][0],NMOL));
        for (int m = 1; m < MODLID[ci].size(); m++)
          results.logTab(3,PROCESS,"or " + stos(MODLID[ci][m],NMOL));
      }
      results.logBlank(SUMMARY);
      }
      results.logTrailer(LOGFILE);
// Change search order here after first
// but it is only changed after first, not after subsequent
// also confusing for user
    }

    last_sol = sol;

//second and subsequent
    if (success &&
        packs_ok &&
        found_first_component)
    {
/*2*/ if (!inputAUTO.ELLG_HIRES && cstart < AUTO_SEARCH.size())
/*2*/ {
/*2*/   results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/   inputAUTO.ELLG_HIRES = true;
/*2*/   results.logTab(1,PROCESS,"Search: First solutions found");
/*2*/   results.logTab(1,PROCESS,"Search: Use lower resolution for second search");
/*2*/   results.logBlank(SUMMARY);
/*2*/   results.logTrailer(LOGFILE);
/*2*/ }
/*2*/ for (int c = cstart; c < AUTO_SEARCH.size() && !partial_result && packs_ok; c++)
/*2*/ {
/*2*/   //store last sol if KEEP_HIGH_TFZ if false
/*2*/   //then if PRUNE is on, can revert to last that packs
/*2*/   if (!NSEARCH_SET && //keep going until tfz fails
/*2*/      last_sol.size() &&
/*2*/      inputAUTO.ZSCORE.USE &&
/*2*/      inputAUTO.ZSCORE.over_cutoff(last_sol.top_TFZeq()) &&
/*2*/      !inputAUTO.ZSCORE.over_cutoff(sol.top_TFZeq()) )
/*2*/   { //auto search, last one was over
/*2*/     results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/     results.logTab(1,PROCESS,"Number of search copies not set");
/*2*/     results.logTab(1,PROCESS,"Number of search copies determined automatically");
/*2*/     results.logTab(1,PROCESS,"TFZ score has dropped below level for definite solution (" + dtos(inputAUTO.ZSCORE.cutoff()) + ")");
/*2*/     results.logTab(2,PROCESS,"Search terminated");
/*2*/     results.logBlank(SUMMARY);
/*2*/     results.logTrailer(LOGFILE);
/*2*/     sol = last_sol.purge_tfz(inputAUTO.ZSCORE); //go back one step and exit
/*2*/     break;
/*2*/   }
/*2*/   if (!inputAUTO.PACKING.KEEP_HIGH_TFZ && inputAUTO.SEARCH_PRUNE && sol.num_packs())
/*2*/   {
/*2*/     last_sol = sol; //will be a packing solution in list
/*2*/   }
/*2*/   results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/   results.logTab(1,PROCESS,"Solution stored for possible reversion if next search fails");
/*2*/   if (last_sol.size())
/*2*/   {
/*2*/   results.logTab(1,PROCESS,"Number of solutions = " + itos(sol.size()));
/*2*/   results.logTab(1,PROCESS,"Solution TOP LLG = "+ dtos(sol.top_LLG(),1));
/*2*/   results.logTab(1,PROCESS,"Solution TOP TFZ = "+ dtos(sol.top_TFZ(),1));
/*2*/   }
/*2*/   else results.logTab(1,PROCESS,"No solutions");
/*2*/   results.logBlank(SUMMARY);
/*2*/   results.logTrailer(LOGFILE);
/*2*/   mr_solution best_sol; //component keeps changing
/*2*/   ResultMR    best_result;
/*2*/   search_component iter_success; //result of search
/*2*/   std::string cards = inputAUTO.Cards();
/*2*/   InputMR_FUSE inputFUSE(cards);
/*2*/   inputFUSE.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/   inputFUSE.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/   inputFUSE.ensemble = ensemble;
/*2*/   inputFUSE.tracemol = tracemol;
/*2*/   inputFUSE.PDB = inputAUTO.PDB;
/*2*/   bool done_second_mol_multi_high_tfz_same_mol_as_first(false);
/*2*/   bool second_mol_multi_high_tfz_same_mol_as_first =
/*2*/            find_first_component && //i.e. second searched for
/*2*/            sol[0].KNOWN.size() == 1  && //only first has been placed, assume all sets the same
/*2*/            inputAUTO.ZSCORE.USE &&
/*2*/            //sol.has_been_rescored() && //TFZ accurate
/*2*/            sol.num_TFZ(inputAUTO.ZSCORE.cutoff()) > 1 // and there is more than one significant TF
/*2*/        // && sol.all_space_groups_the_same() //NOT NECESSARY for RF that all in the same spacegroup
/*2*/         && AUTO_SEARCH[c].MODLID[0] == sol[0].KNOWN[0].MODLID; //looking for the same ensemble
/*2*/        //is reset if this quick search fails to find a solution, so that full RF is run instead
/*2*/   bool found_breakpoint(false);
/*2*/   while (!found_breakpoint)
/*2*/   {
/*2*/     mr_solution rf_sol;
/*2*/     bool is_one_atom(inputAUTO.PDB[AUTO_SEARCH[c].af_MODLID()[0]].is_atom);
/*2*/     std::vector<search_component> zsearch = AUTO_SEARCH.set_of_components(c);
/*2*/     for (int z = 0; z < zsearch.size() && !found_breakpoint; z++)
/*2*/     {
/*2*/       second_mol_multi_high_tfz_same_mol_as_first_restart:
/*2*/       mr_solution zsearch_sol; //have to hold sol in a temporary file in case the packing fails to give solutions
/*2*/       ResultMR zsearch_result;
/*2*/       //reset ZSCORE to input after change in purge (below)
/*2*/       inputAUTO.ZSCORE.unset_top();
/*2*/       AUTO_SEARCH.set_modlid(c,zsearch[z]);
/*2*/       {
/*2*/         results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/         (z == 0) ?
/*2*/           results.logTab(1,PROCESS,"Search: Next component"):
/*2*/           results.logTab(1,PROCESS,"Search: Next permutation of component");
/*2*/         results.logTab(1,PROCESS,"Search Order (next search *) (placed +):");
/*2*/         { //logfile scope
/*2*/         AUTO_SEARCH[c].set_star_search();
/*2*/         for (int i = c+1; i < AUTO_SEARCH.size(); i++) AUTO_SEARCH[i].set_star_clear(); //clear swapped values
/*2*/         string2D MODLID = AUTO_SEARCH.getModlidStar();
/*2*/         for (int ci = 0; ci < MODLID.size(); ci++)
/*2*/         {
/*2*/           int NMOL = inputAUTO.PTNCS.use_and_present() ? inputAUTO.PTNCS.NMOL : 1;
/*2*/           results.logTab(2,PROCESS,"#" + itos(ci+1,2) + " " + stos(MODLID[ci][0],NMOL));
/*2*/           for (int m = 1; m < MODLID[ci].size(); m++)
/*2*/             results.logTab(3,PROCESS,"or " + stos(MODLID[ci][m],NMOL));
/*2*/         }
/*2*/         results.logBlank(SUMMARY);
/*2*/         }
/*2*/         results.logTrailer(LOGFILE);
/*2*/       }
/*2*/       if (!is_one_atom &&
/*2*/            second_mol_multi_high_tfz_same_mol_as_first && !done_second_mol_multi_high_tfz_same_mol_as_first)
/*2*/       {
/*2*/         results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/         results.logTab(1,PROCESS,"Search: Rotation function list generated from results of first search");
/*2*/         results.logBlank(SUMMARY);
/*2*/         rf_sol.set_resolution(sol.get_resolution());
/*2*/         rf_sol.push_back(sol[0]);
/*2*/         results.logTab(0,SUMMARY,rf_sol.logfile(1,false),false);
/*2*/         size_t col_width = std::max(sol[0].KNOWN[0].MODLID.size(),size_t(8));//8=="Ensemble"
/*2*/         results.logTabPrintf(1,SUMMARY,"#TRIAL   Euler1 Euler2 Euler3  %*s Unique\n",col_width,"Ensemble");
/*2*/         for (int rot = 1; rot < sol.size(); rot++)
/*2*/         {
/*2*/           SpaceGroup this_sg(sol[rot].HALL);
/*2*/           PHASER_ASSERT(sol[rot].KNOWN.size() == 1);
/*2*/           bool selected = true;
/*2*/           dmat33 reference_rmat = sol[rot].KNOWN[0].R;
/*2*/           dvect3 reference_angle = matrix2eulerDEG(reference_rmat);
/*2*/           for (int rot_prev = 1; rot_prev < rot; rot_prev++)
/*2*/           {
/*2*/             dmat33 prev_rmat = sol[rot_prev].KNOWN[0].R;
/*2*/             for (unsigned isym = 0; isym < this_sg.NSYMP; isym++)
/*2*/             {
/*2*/               dmat33 prev_rmat_sym = this_sg.Rotsym(isym).transpose()*results.doOrth2Frac(prev_rmat);
/*2*/               prev_rmat_sym = results.doFrac2Orth(prev_rmat_sym);
/*2*/               dvect3 sym_prev_angle = matrix2eulerDEG(prev_rmat_sym);
/*2*/               floatType delta = rotdistDEG(reference_angle,sym_prev_angle);
/*2*/               floatType CLUSTER_ANG = rf_sampling; //2.0*scitbx::rad_as_deg(std::atan(HIRES/(4.0*decomposition.meanRadius())));
/*2*/               if (delta < CLUSTER_ANG) selected = false;
/*2*/             }
/*2*/           }
/*2*/           mr_rlist this_rlist(sol[rot].KNOWN[0].MODLID,sol[rot].KNOWN[0].getEuler());
/*2*/           results.logTabPrintf(1,SUMMARY,"%6i   %6.1f %6.1f %6.1f  %*s %-3s\n",
/*2*/                   rot,this_rlist.EULER[0],this_rlist.EULER[1],this_rlist.EULER[2],
/*2*/                   col_width,this_rlist.MODLID.c_str(),selected?"YES":"NO");
/*2*/           if (selected) rf_sol[0].RLIST.push_back(this_rlist);
/*2*/         }
/*2*/         results.logBlank(SUMMARY);
/*2*/         results.logTrailer(LOGFILE);
/*2*/       }
/*2*/       else
/*2*/       {
/*2*/         std::string cards = inputAUTO.Cards();
/*2*/         InputMR_FRF input(cards);
/*2*/         input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/         input.setSOLU(sol);
/*2*/         input.DO_XYZOUT.Override(false);
/*2*/         input.DO_KEYWORDS.Override(false);
/*2*/         input.AUTO_SEARCH = AUTO_SEARCH;
/*2*/         input.setSEAR_ENSE_OR_ENSE(AUTO_SEARCH[c].af_MODLID());
/*2*/         input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/         input.setTNCS(inputAUTO.PTNCS); //epsn only
/*2*/         input.ensemble = ensemble;
/*2*/         input.tracemol = tracemol;
/*2*/         input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/         input.SEARCH_FROM_AUTO = true; //unparse does not work
/*2*/         ResultMR_RF resultFRF = runMR_FRF(input,results);
/*2*/         results.setOutput(resultFRF);
/*2*/         success = resultFRF.Success();
/*2*/         if (resultFRF.Failure()) goto the_end;
/*2*/         rf_sol = resultFRF.getDotRlist();
/*2*/         rf_sampling = resultFRF.getSampling();
/*2*/         ensemble = input.ensemble; //input passed by reference
/*2*/       }
/*2*/       mr_solution rf_before_deep = rf_sol;
/*2*/       bool deep_high_tfz_fail_pak = false;
/*2*/       bool first_deep_high_tfz_fail_pak = true;
/*2*/       rf_sol.set_deep_used(false);
/*2*/       deep_used_restart:
/*2*/       if (deep_high_tfz_fail_pak) rf_sol.set_deep_used(true);
/*2*/       { //memory scope
/*2*/         std::string cards = inputAUTO.Cards();
/*2*/         InputMR_FTF input(cards);
/*2*/         input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/         input.setSOLU(rf_sol);  //all search components
/*2*/         input.DO_XYZOUT.Override(false);
/*2*/         input.DO_KEYWORDS.Override(false);
/*2*/         input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/         input.setTNCS(inputAUTO.PTNCS);
/*2*/         input.ensemble = ensemble;
/*2*/         input.tracemol = tracemol;
/*2*/         input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/         ResultMR_TF resultFTF = runMR_FTF(input,results);
/*2*/         results.setOutput(resultFTF);
/*2*/         success = resultFTF.Success();
/*2*/         if (resultFTF.Failure()) goto the_end;
/*2*/         zsearch_sol = resultFTF.getDotSol();
/*2*/         if (zsearch_sol.size())
/*2*/         {
/*2*/           inputAUTO.ZSCORE.set_top(resultFTF.getTopTFZ()); //ready for half
/*2*/           if (inputAUTO.ZSCORE.USE && inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ()))
/*2*/               // zsearch_sol.has_been_rescored() && // TFZ acurate
/*2*/               deep_high_tfz_fail_pak = first_deep_high_tfz_fail_pak; //toggles for second time as well
/*2*/         }
/*2*/       } //ftf
/*2*/       if (zsearch_sol.size()
/*2*/         &&   (!inputAUTO.ZSCORE.USE ||
/*2*/                 (inputAUTO.ZSCORE.USE &&
/*2*/                  (zsearch_sol.num_TFZ(inputAUTO.ZSCORE.cutoff()) ||
/*2*/                    !inputAUTO.ELLG_HIRES ||//this pass was at high resolution
/*2*/                    is_one_atom || //don't lower resolution for one atom, already at limit
/*2*/                    !zsearch_sol.can_lower_resolution()))))
/*2*/       {
/*2*/         std::string cards = inputAUTO.Cards();
/*2*/         InputMR_PAK input(cards);
/*2*/         input.tracemol = tracemol;
/*2*/         input.setSOLU(zsearch_sol);
/*2*/         input.setZSCO(inputAUTO.ZSCORE); //top is set
/*2*/         input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/         input.DO_XYZOUT.Override(false);
/*2*/         input.DO_KEYWORDS.Override(false);
/*2*/         if (inputAUTO.SEARCH_PRUNE) input.setPACK_PRUN(true);
/*2*/         ResultMR resultPAK = runMR_PAK(input,results);
/*2*/         tracemol = input.tracemol;
/*2*/         results.setOutput(resultPAK);
/*2*/         success = resultPAK.Success();
/*2*/         if (resultPAK.Failure()) goto the_end;
/*2*/         zsearch_sol = resultPAK.getDotSol();
/*2*/         zsearch_result = resultPAK;
/*2*/         deep_high_tfz_fail_pak  = first_deep_high_tfz_fail_pak && deep_high_tfz_fail_pak && !zsearch_sol.size();
/*2*/         //otherwise, if empty the last time through this loop if one is not over TFZ,
/*2*/         //then the last one is an empty solution (provided it has size, below)
/*2*/         if (zsearch_sol.num_packs()) //now do amalgamation with high TFZ or just refine
/*2*/         { //memory
/*2*/           int min_interesting_number_tfz = best_sol.size() ? std::max(size_t(1),best_sol[0].KNOWN.size()) : 1;
/*2*/           if ( //can be P1 because it is second and subsequent
/*2*/               inputAUTO.ZSCORE.USE && //looking for best
/*2*/              // zsearch_sol.has_been_rescored() && //TFZ accurate - nope, using fast tfz
/*2*/               zsearch_sol.num_TFZ(inputAUTO.ZSCORE.cutoff()) > min_interesting_number_tfz // definite solutions
/*2*/               && zsearch_sol.homogeneous() //all last components have same modlid
/*2*/               && AUTO_SEARCH.not_an_OR_search() //check only first MODLID in search_component test below
/*2*/               && AUTO_SEARCH.remaining(c,zsearch_sol[0].KNOWN.back().MODLID) > 1 // more remaining
/*2*/               && !inputAUTO.PTNCS.use_and_present() //can't deal with ptncs yet
/*2*/               && !inputAUTO.PACKING.all()) // not appropriate to amalgamate with no packing check
/*2*/           //have to split up searches, because high tfz values might not be from the same background
/*2*/           {
/*2*/             inputFUSE.smaller_than_best = false;
/*2*/             bool fuse_attempted(false);
/*2*/             inputFUSE.best_known_size= 1;
/*2*/             size_t orig_known_size(0);
/*2*/             {
/*2*/               results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/               results.logTab(1,PROCESS,"Search: Translation Function has peaks with Z-score > " + dtos(inputAUTO.ZSCORE.cutoff()) + " (definite solution)");
/*2*/               results.logTab(1,PROCESS,"Search: These will be grouped by fixed component");
/*2*/               results.logTab(1,PROCESS,"Search: There are " + itos(zsearch_sol.num_rlist()) + " sets of fixed components");
/*2*/               for (int r = 0; r < zsearch_sol.num_rlist(); r++)
/*2*/                 results.logTab(1,PROCESS,"RF#" + itos(r+1) + " number of peaks = " + itos(zsearch_sol.num_in_rlist(r)) + " and number of high TFZ = " + itos(zsearch_sol.num_tfz_rlist(r,inputAUTO.ZSCORE.cutoff())));
/*2*/               results.logBlank(SUMMARY);
/*2*/               results.logTrailer(LOGFILE);
/*2*/             }
/*2*/             mr_solution tf_sol;
/*2*/                         tf_sol.copy_extras(zsearch_sol);
/*2*/             for (int r = 0; r < zsearch_sol.num_rlist(); r++)
/*2*/             {
/*2*/               mr_solution this_tf_sol = zsearch_sol.rlist_zscore(r,inputAUTO.ZSCORE.cutoff());
/*2*/               orig_known_size = std::max(rf_sol[0].KNOWN.size(),orig_known_size); //have to assume they are all the same
/*2*/               inputFUSE.best_known_size = std::max(rf_sol[0].KNOWN.size()+1,inputFUSE.best_known_size); //have to assume they are all the same
/*2*/               if (//zsearch_sol.has_been_rescored() && // TFZ accurate
/*2*/                   (this_tf_sol.num_TFZ(inputAUTO.ZSCORE.cutoff()) > 1)
/*2*/                   && (this_tf_sol.num_TFZ(inputAUTO.ZSCORE.cutoff())+rf_sol[r].KNOWN.size() >= inputFUSE.best_known_size)) //can be the same, equivalent bests
/*2*/               {
/*2*/                 inputFUSE.amalgamation_attempted = true;
/*2*/                 results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/                 results.logTab(1,PROCESS,"Search: Set #" + itos(r+1) + " of " + itos(zsearch_sol.num_rlist()) + " fixed components");
/*2*/                 results.logTab(1,PROCESS,"Search: Number of solutions with TFZ over " +  dtos(inputAUTO.ZSCORE.cutoff(),3,1) + ": " + itos(this_tf_sol.size()));
/*2*/                 results.logTab(1,PROCESS,"Search: Solutions will be refined before amalgamation");
/*2*/                 results.logTab(1,PROCESS,"Search: Only parameters of last component will be refined");
/*2*/                 results.logBlank(SUMMARY);
/*2*/                 results.logTrailer(LOGFILE);
/*2*/                 {
/*2*/                   std::string cards = inputAUTO.Cards();
/*2*/                   InputMR_RNP input(cards);
/*2*/                   input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/                   input.setSOLU(this_tf_sol);
/*2*/                   input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/                   input.ensemble = ensemble;
/*2*/                   input.tracemol = tracemol;
/*2*/                   input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/                   input.setTNCS(inputAUTO.PTNCS); //no more anisotropy refinement in the other modes
/*2*/                   if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
/*2*/                     input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
/*2*/                   input.setMACM_PROTOCOL_LAST_ONLY();
/*2*/                   input.setMACM_PROTOCOL_BFAC_FIX(); //prevents negative variances when B-factors all want to be negative, amalgamation with lots of negative variance solutions
/*2*/                   input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != this_tf_sol[0].KNOWN.size()));
/*2*/                   ResultMR resultRNP = runMR_RNP(input,results); //only refine last one, matches better
/*2*/                   results.setOutput(resultRNP);
/*2*/                   success = resultRNP.Success();
/*2*/                   if (resultRNP.Failure()) goto the_end;
/*2*/                   this_tf_sol = resultRNP.getDotSol();
/*2*/                 } //refinement, may amalgamate solutions in the same set, simplifies trying to pack combination later
/*2*/                 if (inputAUTO.MACM_PROTOCOL.is_default() &&
/*2*/                     inputAUTO.ZSCORE.USE && inputAUTO.ZSCORE.over_cutoff(this_tf_sol.top_TFZ()) &&
/*2*/                     //refine_VRMS_when_low_signal &&
/*2*/                     !inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms))
/*2*/                 {
/*2*/                   results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/                   MACM_PROTOCOL_VRMS_FIX = true;
/*2*/                   results.logTab(1,PROCESS,"Search: Protocol for MR refinement is DEFAULT");
/*2*/                   results.logTab(1,PROCESS,"Search: Z-scores over cutoff have been attained");
/*2*/                   if (refine_VRMS_when_low_signal) results.logTab(1,PROCESS,"Search: VRMS (variances) of ensemble(s) WILL NOT be refined (further)");
/*2*/                   results.logBlank(SUMMARY);
/*2*/                   results.logTrailer(LOGFILE);
/*2*/                 } //change refinement protocol
/*2*/                 if (this_tf_sol.size() == 1) //refined to a single solution
/*2*/                 {
/*2*/                   inputFUSE.none_to_amalgamate = true;
/*2*/                   inputFUSE.smaller_than_best = (this_tf_sol[0].KNOWN.size() < inputFUSE.best_known_size);
/*2*/                 }
/*2*/                 else if (//zsearch_sol.has_been_rescored() &&  //TFZ accurate
/*2*/                     (this_tf_sol.num_TFZ(inputAUTO.ZSCORE.cutoff()) > 1) //amalgamate solutions
/*2*/                      && (this_tf_sol.num_TFZ(inputAUTO.ZSCORE.cutoff())+rf_sol[r].KNOWN.size() >= inputFUSE.best_known_size) //can be the same, equivalent bests
/*2*/                     && inputAUTO.SEARCH_AMAL_USE
/*2*/                       && this_tf_sol.all_space_groups_the_same()) // all in the same spacegroup
/*2*/                 { //amalgamate
/*2*/                   std::string cards = inputAUTO.Cards();
/*2*/                   InputMR_RNP input(cards);
/*2*/                   input.ensemble = ensemble;
/*2*/                   input.tracemol = tracemol;
/*2*/                   input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/                   input.Analyse(results);
/*2*/                   //find out how many of this type remaining
/*2*/                   inputFUSE.num_remaining = 1; //including this search
/*2*/                   for (int a = c+1; a < AUTO_SEARCH.size(); a++)
/*2*/                     if (AUTO_SEARCH[a] == AUTO_SEARCH[c]) inputFUSE.num_remaining++;
/*2*/                   PHASER_ASSERT(this_tf_sol.all_space_groups_the_same());
/*2*/                   fuse_attempted = true;
/*2*/                   inputFUSE.smaller_than_best = false;
/*2*/                   inputFUSE.MRSET = this_tf_sol;
/*2*/                   bool error = runMR_FUSE(inputFUSE,results); //pass by reference
/*2*/                   this_tf_sol = inputFUSE.MRSET; //results returned in reference-passed input
/*2*/                   for (int j = 0; j < this_tf_sol.size(); j++)
/*2*/                     inputFUSE.best_known_size = std::max(this_tf_sol[j].KNOWN.size(),inputFUSE.best_known_size);
/*2*/                   if (error) { success = false; goto the_end; }
/*2*/                 } //amalgamate
/*2*/               } //frf, ftf, amalgamate and purge over all rt/tf combinations
/*2*/               if (this_tf_sol.size())
/*2*/                 tf_sol += this_tf_sol; //whether or not amalgamation occurred
/*2*/               for (int j = 0; j < tf_sol.size(); j++) //select out the previous ones that are biggest
/*2*/                 inputFUSE.best_known_size = std::max(tf_sol[j].KNOWN.size(),inputFUSE.best_known_size);
/*2*/             }
/*2*/             if (tf_sol.size() && inputFUSE.some_amalgamated)
/*2*/             {
/*2*/               results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/               results.logTab(1,PROCESS,"Search: Purge amalgamated solutions that have placed sub-maximal components");
/*2*/               size_t start_size = tf_sol.size();
/*2*/               mr_solution amalg_sol;
/*2*/               for (int j = 0; j < tf_sol.size(); j++) //select out the previous ones that are biggest
/*2*/                 inputFUSE.best_known_size = std::max(tf_sol[j].KNOWN.size(),inputFUSE.best_known_size);
/*2*/               for (int k = 0; k < tf_sol.size(); k++) //select out new ones that are the biggest
/*2*/                 if (tf_sol[k].KNOWN.size() == inputFUSE.best_known_size)
/*2*/                   amalg_sol.push_back(tf_sol[k]);
/*2*/               results.logTab(1,PROCESS,"Search: Highest number of placed components = " + itos(inputFUSE.best_known_size));
/*2*/               results.logTab(1,PROCESS,"Search: Number of solutions before purge = " + itos(start_size));
/*2*/               results.logTab(1,PROCESS,"Search: Number of solutions after purge = " + itos(tf_sol.size()));
/*2*/               results.logBlank(SUMMARY);
/*2*/               bool start_macm_vrms = inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms);
/*2*/               bool set_macm_vrms(false);
/*2*/               if (amalg_sol.size() == 1 &&
/*2*/                   start_macm_vrms &&
/*2*/                   inputAUTO.MACM_PROTOCOL.is_default())
/*2*/               {
/*2*/                 MACM_PROTOCOL_VRMS_FIX = false;
/*2*/                 set_macm_vrms = true;
/*2*/                 results.logTab(1,PROCESS,"Search: Protocol for MR refinement is DEFAULT");
/*2*/                 results.logTab(1,PROCESS,"Search: Amalgamated (partial) solution");
/*2*/                 results.logTab(1,PROCESS,"Search: VRMS (variances) of ensemble(s) WILL be refined");
/*2*/                 results.logBlank(SUMMARY);
/*2*/               }
/*2*/               results.logTrailer(LOGFILE);
/*2*/               if (amalg_sol.size())
/*2*/               {//refine amalgamated set
/*2*/                 std::string cards = inputAUTO.Cards();
/*2*/                 InputMR_RNP input(cards);
/*2*/                 input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/                 input.setSOLU(amalg_sol);
/*2*/                 input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/                 input.setTNCS(inputAUTO.PTNCS);
/*2*/                 input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/                 input.ensemble = ensemble;
/*2*/                 input.tracemol = tracemol;
/*2*/                 if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
/*2*/                   input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
/*2*/                 ResultMR resultRNP = runMR_RNP(input,results);
/*2*/                 results.setOutput(resultRNP);
/*2*/                 success = resultRNP.Success();
/*2*/                 if (resultRNP.Failure()) goto the_end;
/*2*/                 amalg_sol = resultRNP.getDotSol();
/*2*/                 if (set_macm_vrms)
/*2*/                   MACM_PROTOCOL_VRMS_FIX = start_macm_vrms;
/*2*/               }
/*2*/               if (success && amalg_sol.size())
/*2*/               { //may refine into clashes
/*2*/                 std::string cards = inputAUTO.Cards();
/*2*/                 InputMR_PAK input(cards);
/*2*/                 input.tracemol = tracemol;
/*2*/                 input.setSOLU(amalg_sol);
/*2*/                 input.setZSCO(inputAUTO.ZSCORE); //top has been set
/*2*/                 input.DO_XYZOUT.Override(false);
/*2*/                 input.DO_KEYWORDS.Override(false);
/*2*/                 input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/                 if (inputAUTO.SEARCH_PRUNE) input.setPACK_PRUN(true);
/*2*/                 ResultMR resultPAK = runMR_PAK(input,results);
/*2*/                 tracemol = input.tracemol;
/*2*/                 amalg_sol = resultPAK.getDotSol();
/*2*/                 results.setOutput(resultPAK);
/*2*/                 success = resultPAK.Success();
/*2*/                 if (!success) goto the_end;
/*2*/                 if (amalg_sol.size()) // after paranoia packing
/*2*/                 {
/*2*/                   results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/                   results.logTab(1,PROCESS,"Search: Successful amalgamation");
/*2*/                   results.logBlank(SUMMARY);
/*2*/                   results.logTab(0,SUMMARY,amalg_sol.logfile(1,true),false);
/*2*/                   results.logBlank(SUMMARY);
/*2*/                   results.logTrailer(LOGFILE);
/*2*/                   zsearch_sol = amalg_sol;
/*2*/                   resultPAK.setOutput(results);
/*2*/                   zsearch_result = resultPAK;
/*2*/                 }
/*2*/               } //packing
/*2*/             }
/*2*/             else if (tf_sol.size() && !inputFUSE.none_to_amalgamate && fuse_attempted && !inputFUSE.smaller_than_best) //check that solutions aren't equivalent, even if amalgamation fails
/*2*/             { //ready for the next FRF, comments attached
/*2*/               if (inputFUSE.amalgamation_attempted)
/*2*/               {
/*2*/                 results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/                 results.logTab(1,PROCESS,"Search: No successful amalgamations");
/*2*/                 results.logTab(1,PROCESS,"Search: Either amalgamations did not pack or did not increase LLG");
/*2*/                 results.logTab(1,PROCESS,"Search: Non-amalgamated solutions will be refined to identify duplicates before search for next component");
/*2*/                 results.logBlank(SUMMARY);
/*2*/                 results.logTrailer(LOGFILE);
/*2*/               }
/*2*/               std::string cards = inputAUTO.Cards();
/*2*/               InputMR_RNP input(cards);
/*2*/               input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/               input.setSOLU(tf_sol);
/*2*/               input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/               input.setTNCS(inputAUTO.PTNCS);
/*2*/               input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != tf_sol[0].KNOWN.size()));
/*2*/               input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/               input.ensemble = ensemble;
/*2*/               input.tracemol = tracemol;
/*2*/               if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
/*2*/                 input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
/*2*/               ResultMR resultRNP = runMR_RNP(input,results);
/*2*/               results.setOutput(resultRNP);
/*2*/               success = resultRNP.Success();
/*2*/               if (resultRNP.Failure()) goto the_end;
/*2*/               zsearch_sol = resultRNP.getDotSol();
/*2*/               zsearch_result = resultRNP;
/*2*/               //zsearch_sol = tf_sol;
/*2*/             }
/*2*/             else if (tf_sol.size() && inputFUSE.none_to_amalgamate && !inputFUSE.smaller_than_best)
/*2*/             {
/*2*/               zsearch_sol = tf_sol;
/*2*/               //zsearch_result = resultRNP;
/*2*/             }
/*2*/           }
/*2*/           else if (!added_second_component ||
/*2*/                    !inputAUTO.ZSCORE.STOP || (inputAUTO.ZSCORE.STOP && inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ())))
/*2*/           {
/*2*/             results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/             bool start_macm_vrms = inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms);
/*2*/             bool set_macm_vrms(false);
/*2*/             if (zsearch_sol.size() == 1 &&
/*2*/                 start_macm_vrms == false &&
/*2*/                 inputAUTO.MACM_PROTOCOL.is_default())
/*2*/             {
/*2*/               MACM_PROTOCOL_VRMS_FIX = false;
/*2*/               set_macm_vrms = true;
/*2*/               results.logTab(1,PROCESS,"Search: Protocol for MR refinement is DEFAULT");
/*2*/               results.logTab(1,PROCESS,"Search: SINGLE (partial) solution");
/*2*/               results.logTab(1,PROCESS,"Search: VRMS (variances) of ensemble(s) WILL be refined");
/*2*/             }
/*2*/             results.logTab(1,PROCESS,"Search: Only parameters of last component will be refined");
/*2*/             results.logBlank(SUMMARY);
/*2*/             results.logTrailer(LOGFILE);
/*2*/             {
/*2*/               std::string cards = inputAUTO.Cards();
/*2*/               InputMR_RNP input(cards);
/*2*/               input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/               input.setSOLU(zsearch_sol);
/*2*/               input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/               input.setTNCS(inputAUTO.PTNCS);
/*2*/               if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
/*2*/                 input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
/*2*/               if (input.MACM_LAST)
/*2*/                 input.setMACM_PROTOCOL_LAST_ONLY(); //after setting protocol
/*2*/               input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != zsearch_sol[0].KNOWN.size()));
/*2*/               input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/               input.ensemble = ensemble;
/*2*/               input.tracemol = tracemol;
/*2*/               ResultMR resultRNP = runMR_RNP(input,results);
/*2*/               results.setOutput(resultRNP);
/*2*/               success = resultRNP.Success();
/*2*/               if (resultRNP.Failure()) goto the_end;
/*2*/               zsearch_sol = resultRNP.getDotSol();
/*2*/               zsearch_result = resultRNP;
/*2*/               if (set_macm_vrms)
/*2*/                 MACM_PROTOCOL_VRMS_FIX = start_macm_vrms;
/*2*/             }
/*2*/             if (inputAUTO.MACM_PROTOCOL.is_default() &&
/*2*/                 inputAUTO.ZSCORE.USE && inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ()) &&
/*2*/                 //refine_VRMS_when_low_signal &&
/*2*/                 !inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms))
/*2*/             {
/*2*/               results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/               inputAUTO.MACRO_MR.back()->setFIX(phaser::macm_vrms,true);
/*2*/               inputAUTO.MACM_PROTOCOL.set("DEFAULT_CUSTOM");
/*2*/               results.logTab(1,PROCESS,"Search: Protocol for MR refinement is DEFAULT");
/*2*/               results.logTab(1,PROCESS,"Search: Z-scores over cutoff have been attained");
/*2*/               results.logTab(1,PROCESS,"Search: VRMS (variances) of ensemble(s) WILL NOT be refined (further)");
/*2*/               results.logBlank(SUMMARY);
/*2*/               results.logTrailer(LOGFILE);
/*2*/             }
/*2*/           } //ordinary, non-amalgamated search
/*2*/         }//memory
/*2*/         //zsearch_sol will contain the next successful TFZ solution
/*2*/         //amalgamation will have been applied if it is relevant
/*2*/         //or the next solution if not using TFZ
/*2*/         if (zsearch_sol.size() && (//look for best
/*2*/              !added_second_component ||
/*2*/              !inputAUTO.ZSCORE.STOP ||
/*2*/              (inputAUTO.ZSCORE.STOP && inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ()))))
/*2*/         {
/*2*/           if (input.ZSCORE.USE && //are looking for best
/*2*/               best_sol.size() && //there is a best solution
/*2*/               zsearch_sol[0].KNOWN.size() == best_sol[0].KNOWN.size() && //this is not an amalgamated solution
/*2*/               !between(zsearch_sol.get_resolution(),best_sol.get_resolution(),1.0e-02)) //same resolution
/*2*/           { //change the best_sol values referring to sol
/*2*/             results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/             results.logTab(1,PROCESS,"Current and best solution not at same resolution");
/*2*/             results.logTab(1,PROCESS,"Current LLG = " + dtos(zsearch_sol.top_LLG(),1) + " (resolution = " + dtos(zsearch_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(2,PROCESS,"Component = " + zsearch_sol.last_modlid());
/*2*/             results.logTab(1,PROCESS,"Best LLG so far = " + dtos(best_sol.top_LLG(),1) + " (resolution = " + dtos(best_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(2,PROCESS,"Component = " + best_sol.last_modlid());
/*2*/             results.logBlank(SUMMARY);
/*2*/             results.logTrailer(LOGFILE);
/*2*/             {
/*2*/             std::string cards = inputAUTO.Cards();
/*2*/             InputMR_RNP input(cards);
/*2*/             input.setREFL_DATA(inputAUTO.REFLECTIONS);
/*2*/             double storeres = zsearch_sol.get_resolution();
/*2*/             zsearch_sol.set_resolution(-999);//overwrite, so that higher resolution can be set
/*2*/             input.setSOLU(zsearch_sol); //NOT old sol, not best changed either
/*2*/             zsearch_sol.set_resolution(storeres);//restore
/*2*/             input.setNORM_DATA(inputAUTO.SIGMAN);
/*2*/             input.setTNCS(inputAUTO.PTNCS);
/*2*/             input.setRESO_HIGH(best_sol.get_resolution());
/*2*/             if (!input.PURGE_RNP.NUMBER) input.setPURG_RNP_ENAB(false); //cannot purge because mean is wrong
/*2*/             input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
/*2*/             input.PDB = inputAUTO.PDB; //unparse does not work
/*2*/             input.ensemble = ensemble;
/*2*/             input.tracemol = tracemol;
/*2*/             if (!boost::logic::indeterminate(MACM_PROTOCOL_VRMS_FIX))
/*2*/               input.setMACM_PROTOCOL_VRMS_FIX(bool(MACM_PROTOCOL_VRMS_FIX));
/*2*/             ResultMR resultRNP = runMR_RNP(input,results); //B-factors will change
/*2*/             results.setOutput(resultRNP);
/*2*/             success = resultRNP.Success();
/*2*/             if (resultRNP.Failure()) goto the_end;
/*2*/             zsearch_sol = resultRNP.getDotSol();
/*2*/             zsearch_result = resultRNP;
/*2*/             }
/*2*/           }
/*2*/           if (best_sol.size() &&
/*2*/             zsearch_sol[0].KNOWN.size() == best_sol[0].KNOWN.size()) //this is not an amalgamated solution
/*2*/             PHASER_ASSERT(between(zsearch_sol.get_resolution(),best_sol.get_resolution(),1.0e-02)); //same resolution
/*2*/           if (!input.ZSCORE.USE || //are not looking for best, as byproduct of looking for high TFZ
/*2*/               !best_sol.size() ||  //first
/*2*/               zsearch_sol[0].KNOWN.size() > best_sol[0].KNOWN.size() || //amalgamation has been very successful
/*2*/               (zsearch_sol[0].KNOWN.size() == best_sol[0].KNOWN.size() && zsearch_sol.top_LLG() > best_sol.top_LLG())) //best
/*2*/           {
/*2*/             results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/             results.logTab(1,PROCESS,"Current is best for this component");
/*2*/             if (best_sol.size()) //not the first
                  {
/*2*/               results.logTab(1,PROCESS,"Old best LLG = " + dtos(best_sol.top_LLG(),1) + " (resolution = " + dtos(best_sol.get_resolution(),2) + ")");
/*2*/               best_sol[0].KNOWN.size() > 1?
/*2*/               results.logTab(1,PROCESS,"Old best solution has " + itos(best_sol[0].KNOWN.size()) + " components"):
/*2*/               results.logTab(1,PROCESS,"Old best solution has 1 component");
                  }
/*2*/             best_sol = zsearch_sol;
/*2*/             zsearch_result.setOutput(results);
/*2*/             best_result = zsearch_result;
/*2*/             iter_success = zsearch[z];
/*2*/             results.phenixCallback("current best solution",best_sol[0].logfile(0, true));
/*2*/             best_sol[0].KNOWN.size() > 1?
/*2*/             results.logTab(1,PROCESS,"New best solution has " + itos(best_sol[0].KNOWN.size()) + " components"):
/*2*/             results.logTab(1,PROCESS,"New best solution has 1 component");
/*2*/             results.logTab(1,PROCESS,"New best solution LLG = " + dtos(best_sol.top_LLG(),1) + " (resolution = " + dtos(best_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(1,PROCESS,"New best solution component = " + best_sol.last_modlid());
/*2*/             results.logBlank(SUMMARY);
/*2*/             results.logTrailer(LOGFILE);
/*2*/             found_breakpoint = true; //break
/*2*/           }
/*2*/           else if (input.ZSCORE.USE) //we are looking for the best
/*2*/           {
/*2*/             results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/             results.logTab(1,PROCESS,"Current top is NOT best for this component");
/*2*/             results.logTab(1,PROCESS,"Current solution LLG = " + dtos(zsearch_sol.top_LLG(),1) + " (resolution = " + dtos(zsearch_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(1,PROCESS,"Current best solution LLG = " + dtos(best_sol.top_LLG(),1) + " (resolution = " + dtos(best_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(1,PROCESS,"Current best component = " + best_sol.last_modlid());
/*2*/             results.logBlank(SUMMARY);
/*2*/             results.logTrailer(LOGFILE);
/*2*/           }
/*2*/           else
/*2*/           {
/*2*/             best_sol = zsearch_sol;
/*2*/             zsearch_result.setOutput(results);
/*2*/             best_result = zsearch_result;
/*2*/             iter_success = zsearch[z];
/*2*/             results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/             results.logTab(1,PROCESS,"Accept this component");
/*2*/             results.logTab(1,PROCESS,"TFZ not used in decision making");
/*2*/             results.logTab(1,PROCESS,"Current LLG = " + dtos(best_sol.top_LLG(),1) + " (resolution = " + dtos(best_sol.get_resolution(),2) + ")");
/*2*/             results.logTab(1,PROCESS,"Current Component = " + best_sol.last_modlid());
/*2*/             results.logBlank(SUMMARY);
/*2*/             results.logTrailer(LOGFILE);
/*2*/             found_breakpoint = true; //break
/*2*/           }
/*2*/         } //save best or if not looking for best, break
/*2*/         else if (zsearch_sol.size() &&
/*2*/                  inputAUTO.ZSCORE.STOP && !inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ()))
/*2*/         {
/*2*/           results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/           results.logTab(1,PROCESS,"Stop");
/*2*/           results.logTab(1,PROCESS,"TFZ used for termination condition");
/*2*/           results.logTab(1,PROCESS,"TFZ not over cutoff for definite solution");
/*2*/           results.logBlank(SUMMARY);
/*2*/           results.logTrailer(LOGFILE);
/*2*/         }
/*2*/       } //packing
/*2*/       if //there is a failure to find any solutions this search, but we got here avoiding RF
/*2*/          (!zsearch_sol.size()
/*2*/            && second_mol_multi_high_tfz_same_mol_as_first
/*2*/            && !done_second_mol_multi_high_tfz_same_mol_as_first) //try full RF option
/*2*/       {
/*2*/         second_mol_multi_high_tfz_same_mol_as_first = false;
/*2*/         done_second_mol_multi_high_tfz_same_mol_as_first = true;
/*2*/         results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/         results.logTab(1,PROCESS,"No solutions found after reusing RF search rotations");
/*2*/         results.logTab(1,PROCESS,"Go back to rotation function step");
/*2*/         results.logBlank(SUMMARY);
/*2*/         results.logTrailer(LOGFILE);
/*2*/         goto second_mol_multi_high_tfz_same_mol_as_first_restart; // bypass permutation
/*2*/       }
/*2*/       else if (!zsearch_sol.size() && //there are no solutions
/*2*/                 rf_sol.not_ndeep() && //there are deep rf peaks
/*2*/                 !rf_sol.get_deep_used() && //infinite loop
/*2*/                 deep_high_tfz_fail_pak) //TF or PAK failed for all components
/*2*/       {
/*2*/         results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/         results.logTab(1,PROCESS,"No solutions found using highest tranche of RF angles");
/*2*/         results.logTab(1,PROCESS,"Unused deep RF angles present in RF list");
/*2*/         results.logTab(1,PROCESS,"Go back to translation function step using deep RF angles");
/*2*/         results.logBlank(SUMMARY);
/*2*/         results.logTrailer(LOGFILE);
/*2*/         first_deep_high_tfz_fail_pak = false;
/*2*/         goto deep_used_restart; // bypass permutation
/*2*/       }
/*2*/       else if ( //or else there are no definite solutions but we can do more locally
/*2*/          (!zsearch_sol.size() ||
/*2*/           (inputAUTO.ZSCORE.USE && !inputAUTO.ZSCORE.over_cutoff(zsearch_sol.top_TFZ()))) && //no definite solutions
/*2*/          inputAUTO.ELLG_HIRES &&//this pass was not high resolution
/*2*/          !is_one_atom && //don't lower resolution for one atom, already at limit
/*2*/          zsearch_sol.can_lower_resolution())
/*2*/       {
/*2*/         inputAUTO.ELLG_HIRES = false; //search deep
/*2*/         second_mol_multi_high_tfz_same_mol_as_first = false;
/*2*/         done_second_mol_multi_high_tfz_same_mol_as_first = true;
/*2*/         results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/         zsearch_sol.size() > 1 ?
/*2*/         results.logTab(1,PROCESS,"No definite (high TFZ) solutions found at lower resolution"):
/*2*/         results.logTab(1,PROCESS,"No solutions found at lower resolution");
/*2*/         results.logTab(1,PROCESS,"Increase resolution");
/*2*/         results.logTab(1,PROCESS,"Go back to rotation function step");
/*2*/         results.logBlank(SUMMARY);
/*2*/         results.logTrailer(LOGFILE);
/*2*/         goto second_mol_multi_high_tfz_same_mol_as_first_restart; // bypass permutation
/*2*/       }
/*2*/     } //end of permutations, or breakpoint
/*2*/     if ( //failure to find solutions, one way or another
/*2*/         (!best_sol.size() || (inputAUTO.ZSCORE.USE && !inputAUTO.ZSCORE.over_cutoff(best_sol.top_TFZ()))) && //no definite solutions
/*2*/          inputAUTO.ELLG_HIRES &&//this pass was not high resolution
/*2*/          !is_one_atom && //don't lower resolution for one atom, already at limit
/*2*/          best_sol.can_lower_resolution())
/*2*/     {
/*2*/       inputAUTO.ELLG_HIRES = false;
/*2*/       results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/       zsearch.size() > 1 ?
/*2*/         results.logTab(1,PROCESS,"There were " + itos(zsearch.size()) + " possible ensembles for this component"):
/*2*/         results.logTab(1,PROCESS,"There was only " + itos(zsearch.size()) + " possible ensemble for this component");
/*2*/       results.logTab(1,PROCESS,"End of possible ensembles for this component");
/*2*/       results.logTab(1,PROCESS,"No definite (high TFZ) solutions found");
/*2*/       results.logTab(1,PROCESS,"Use higher resolution for search");
/*2*/       results.logBlank(SUMMARY);
/*2*/       results.logTrailer(LOGFILE);
/*2*/       found_breakpoint = false;
/*2*/     }
/*2*/     else //if (zsearch.size() > 1)
/*2*/     {
/*2*/       results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/       zsearch.size() > 1 ?
/*2*/         results.logTab(1,PROCESS,"There were " + itos(zsearch.size()) + " possible ensembles for this component"):
/*2*/         results.logTab(1,PROCESS,"There was only " + itos(zsearch.size()) + " possible ensemble for this component");
/*2*/       results.logTab(1,PROCESS,"End of possible ensembles for this component");
/*2*/       results.logBlank(SUMMARY);
/*2*/       results.logTrailer(LOGFILE);
/*2*/       found_breakpoint = true;
/*2*/     }
/*2*/   } //permutations, or found high tfz breakpoint, or anything if not using TFZ
/*2*/   {//if scope
/*2*/   //logic for placing this component
/*2*/   //if there are definite solutions
/*2*/   if (inputAUTO.ZSCORE.USE && best_sol.size() &&
/*2*/       (//best_sol.has_been_rescored() && // TFZ accurate
/*2*/          inputAUTO.ZSCORE.over_cutoff(best_sol.top_TFZ()) //definitely solved
/*2*/       || inputFUSE.some_amalgamated  //because the TFZ score will be completely wrong but over limit by definition
/*2*/       || (inputFUSE.amalgamation_attempted && inputFUSE.none_to_amalgamate))) //amalgamation is possible but not required, only 1 to add
/*2*/   {
/*2*/     results.phenixCallback("current best solution",best_sol[0].logfile(0,true));
/*2*/     results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/     results.logTab(1,PROCESS,"Definite solutions found");
/*2*/     best_sol[0].KNOWN.size() > 1?
/*2*/     results.logTab(1,PROCESS,"New best solution has " + itos(best_sol[0].KNOWN.size()) + " components"):
/*2*/     results.logTab(1,PROCESS,"New best solution has 1 component");
/*2*/     results.logTab(1,PROCESS,"New best solution overall");
/*2*/     results.logTab(1,PROCESS,"New best solution LLG = "+ dtos(best_sol.top_LLG(),1));
/*2*/     results.logTab(1,PROCESS,"New best solution TFZ = "+ dtos(best_sol.top_TFZ(),1));
/*2*/     results.logBlank(SUMMARY);
/*2*/     if (!inputAUTO.ELLG_HIRES) //restore defaults for cutoff
/*2*/     {
/*2*/       inputAUTO.ELLG_HIRES = true;
/*2*/       results.logTab(1,PROCESS,"Definite solutions were found with higher resolution");
/*2*/       results.logTab(1,PROCESS,"Decrease resolution for next component");
/*2*/       results.logBlank(SUMMARY);
/*2*/     }
/*2*/     results.logTrailer(LOGFILE);
/*2*/   }
/*2*/   //no definite solutions
/*2*/   //no solutions
/*2*/   else if (!best_sol.size() || //no more possibilities to find solutions
/*2*/            (inputAUTO.ZSCORE.STOP && !inputAUTO.ZSCORE.over_cutoff(best_sol.top_TFZ())))
/*2*/   {
/*2*/     if (best_sol.size()) //zscore failure
/*2*/     {
/*2*/       sol = best_sol;
/*2*/       best_result.setOutput(results);
/*2*/       results = best_result;
/*2*/     }
/*2*/     partial_result = NSEARCH_SET && true; //end of the road
/*2*/     results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/     if (sol.size() && c == AUTO_SEARCH.size()-1) //last placement
/*2*/     {
/*2*/       full_low_tfz_result = true; //end of the road
/*2*/       results.logTab(1,PROCESS,"No more components");
/*2*/       results.logTab(1,PROCESS,"Number of solutions = " + itos(sol.size()));
/*2*/       if (inputAUTO.MRSET != sol)
/*2*/       {
/*2*/         results.logTab(1,PROCESS,"Solution TOP LLG = "+ dtos(sol.top_LLG(),1));
/*2*/         results.logTab(1,PROCESS,"Solution TOP TFZ = "+ dtos(sol.top_TFZ(),1));
/*2*/       }
/*2*/       if (best_sol.size() && //there are solutions, but they are bad
/*2*/           !added_second_component)
/*2*/       {
/*2*/         results.logTab(1,PROCESS,"TFZ not over cutoff for definite solution");
/*2*/       }
/*2*/       results.logTab(1,PROCESS,"End with solutions");
/*2*/       results.logBlank(SUMMARY);
/*2*/     }
/*2*/     else if (sol.size())
/*2*/     {
/*2*/       results.logTab(1,PROCESS,"End with Partial Solution");
/*2*/       results.logTab(1,PROCESS,"Number of partial solutions = " + itos(sol.size()));
/*2*/       sol[0].KNOWN.size() > 1 ?
/*2*/       results.logTab(1,PROCESS,"Current solution has " + itos(sol[0].KNOWN.size()) + " components"):
/*2*/       results.logTab(1,PROCESS,"Current solution has 1 component");
/*2*/       if (inputAUTO.MRSET != sol)
/*2*/       {
/*2*/         results.logTab(1,PROCESS,"Partial Solution: TOP LLG = "+ dtos(sol.top_LLG(),1));
/*2*/         results.logTab(1,PROCESS,"Partial Solution: TOP TFZ = "+ dtos(sol.top_TFZ(),1));
/*2*/       }
/*2*/       if (best_sol.size() && //there are solutions, but they are bad
/*2*/           !added_second_component)
/*2*/       {
/*2*/         results.logTab(1,PROCESS,"TFZ not over cutoff for definite solution");
/*2*/         results.logTab(1,PROCESS,"Stop");
/*2*/       }
/*2*/       results.logBlank(SUMMARY);
/*2*/     }
/*2*/     else
/*2*/     {
/*2*/       results.logTab(1,PROCESS,"End with no solutions");
/*2*/       results.logBlank(SUMMARY);
/*2*/     }
/*2*/     results.logTrailer(LOGFILE);
/*2*/   }
/*2*/   else //continue without definite solutions
/*2*/   {
/*2*/     results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/     results.logTab(1,PROCESS,"Continue without definite solutions");
/*2*/     if (sol.size())
/*2*/     {
/*2*/       sol[0].KNOWN.size() > 1 ?
/*2*/       results.logTab(1,PROCESS,"Current solution has " + itos(sol[0].KNOWN.size()) + " components"):
/*2*/       results.logTab(1,PROCESS,"Current solution has 1 component");
/*2*/       if (results.numSolutions())
/*2*/       {
/*2*/         results.logTab(1,PROCESS,"Current Partial Result");
/*2*/         results.logTab(1,PROCESS,"Current Partial Solution: TOP LLG = "+ dtos(sol.top_LLG(),1));
/*2*/         results.logTab(1,PROCESS,"Current Partial Solution: TOP TFZ = "+ dtos(sol.top_TFZ(),1));
/*2*/         results.logTab(1,PROCESS,"Number of partial solutions = " + itos(sol.size()));
/*2*/       }
/*2*/       results.logBlank(SUMMARY);
/*2*/     }
/*2*/     results.logTrailer(LOGFILE);
/*2*/   }
/*2*/   } //if scope
/*2*/   if (!partial_result && !full_low_tfz_result)
/*2*/   {
/*2*/     size_t old_size = sol.size() ? sol[0].KNOWN.size() : 0;
/*2*/     size_t new_size = best_sol.size() ? best_sol[0].KNOWN.size() : 0;
/*2*/     size_t num_amalgamated = new_size-old_size;
/*2*/     num_amalgamated /= inputAUTO.PTNCS.use_and_present() ? inputAUTO.PTNCS.NMOL : 1;
/*2*/     AUTO_SEARCH.set_modlid(c,iter_success);
/*2*/     AUTO_SEARCH[c].set_star_found();
/*2*/     for (int b = 1; b < num_amalgamated; b++)
/*2*/     {
/*2*/       AUTO_SEARCH.set_modlid(c+b,iter_success);
/*2*/       AUTO_SEARCH[c+b].set_star_append();
/*2*/     }
/*2*/     c += num_amalgamated;
/*2*/     c--; //will iterate at end (start c+num_amalgamated)
/*2*/     sol = best_sol;
/*2*/     best_result.setOutput(results);
/*2*/     results = best_result;
/*2*/   }
/*2*/   if (best_sol.size() && !best_sol.num_packs())
/*2*/   {
/*2*/     packs_ok = false;
/*2*/     results.logHeader(LOGFILE,header(MR_AUTO));
/*2*/     sol.size() == 1 ?
/*2*/     results.logTab(1,PROCESS,"Solution with high TFZ does not pack"):
/*2*/     results.logTab(1,PROCESS,"Solutions with high TFZ do not pack");
/*2*/     if (c+1 < AUTO_SEARCH.size())
/*2*/       results.logTab(1,PROCESS,"Terminate search for components");
/*2*/     results.logBlank(LOGFILE);
/*2*/     results.logTrailer(LOGFILE);
/*2*/   }
/*2*/ } //auto search && !partial_result && packs_ok
/*2*/ added_second_component = true;
    }//second and subsequent component

/*3*/ if (!NSEARCH_SET && //keep going until tfz fails
/*3*/    last_sol.size() &&
/*3*/    inputAUTO.ZSCORE.USE &&
/*3*/    inputAUTO.ZSCORE.over_cutoff(last_sol.top_TFZeq()) &&
/*3*/    !inputAUTO.ZSCORE.over_cutoff(sol.top_TFZeq()) )
/*3*/ { //auto search, last one was over
/*3*/   results.logHeader(LOGFILE,header(MR_AUTO));
/*3*/   results.logTab(1,PROCESS,"Number of search copies not set");
/*3*/   results.logTab(1,PROCESS,"Number of search copies determined automatically");
/*3*/   results.logTab(1,PROCESS,"TFZ score has dropped below level for definite solution (" + dtos(inputAUTO.ZSCORE.cutoff()) + ")");
/*3*/   results.logTab(2,PROCESS,"Search terminated");
/*3*/   results.logBlank(SUMMARY);
/*3*/   results.logTrailer(LOGFILE);
/*3*/   sol = last_sol.purge_tfz(inputAUTO.ZSCORE); //go back one step and exit
/*3*/ }
/*3*/

    the_final_bit:
    partial_result = NSEARCH_SET && //only possible if the number to search for is fixed
                     (sol.nknown() != (inputAUTO.MRSET.nknown() + (inputAUTO.PTNCS.use_and_present() ? inputAUTO.PTNCS.NMOL : 1)*AUTO_SEARCH.size()));
    if (success &&
        !found_first_component)
    {
      results.phenixCallback("result","No Solution");
      results.logHeader(LOGFILE,header(MR_AUTO));
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:MR Result: $$ Baubles Markup $$");
      results.logBlank(SUMMARY);
      results.logTab(1,PROCESS,"Sorry - No solution");
      results.logTab(1,SUMMARY,"You may find a solution with a different search or selection strategy");
      results.logBlank(SUMMARY);
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
    }
    else if (success && found_first_component) //whether partial or not
    {
      floatType reso_tol(1.0e-02);
      DataMR mr(inputAUTO.REFLECTIONS,
                inputAUTO.RESHARP,
                inputAUTO.SIGMAN,
                inputAUTO.OUTLIER,
                inputAUTO.PTNCS,
                inputAUTO.DATABINS,
                inputAUTO.HIRES,inputAUTO.LORES,
                inputAUTO.COMPOSITION,
                inputAUTO.ensemble);
      floatType HIRES = mr.fullHiRes(); //required for when the first molecule is set - first RNP is not called
      floatType rnp_hires = sol.get_resolution(); //can be altered if refinement runs out of memory
      if (inputAUTO.DO_AUTO_HIRES)
        rnp_hires = (inputAUTO.AUTO_HIRES>=0) ? std::max(inputAUTO.AUTO_HIRES,HIRES):HIRES;
      sol.set_resolution(-999); //don't use the ellg resolution at the end!
      //first try full reso for final refinement, if this runs out of memory then revert to HIRES
      results.logHeader(LOGFILE,header(MR_AUTO));
      (inputAUTO.DO_AUTO_HIRES) ?
      results.logTab(1,PROCESS,"Solutions will be refined to highest resolution"):
      results.logTab(1,PROCESS,"Solutions will be refined at solution resolution");
      results.logTab(2,PROCESS,"Resolution for refinement: " + dtos(rnp_hires));
      if (inputAUTO.MACM_PROTOCOL.is_default())
      {
        results.logTab(1,PROCESS,"Protocol for MR refinement is DEFAULT");
        results.logTab(1,PROCESS,"VRMS (variances) of ensemble(s) WILL be refined");
      }
      else results.logTab(1,PROCESS,"Protocol for MR refinement is CUSTOM");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
      start_final_rnp_at_hires_with_vrms:
      if (sol.size())
          //MTZ_HIRES < HIRES && rnp_hires+reso_tol < HIRES) //change in protocol, vrms refinement even if resolution >
      {
        try {
       // sol.fix_all(false);
        std::string cards = inputAUTO.Cards();
        InputMR_RNP input(cards);
        if (input.MACM_PROTOCOL.is_default())
          input.setMACM_PROTOCOL_VRMS_FIX(false);
        input.setREFL_DATA(inputAUTO.REFLECTIONS);
        input.setRESO(rnp_hires,inputAUTO.LORES);
        input.setSOLU(sol);
        input.setNORM_DATA(inputAUTO.SIGMAN);
        input.setTNCS(inputAUTO.PTNCS);
        success = false;
        input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
        input.PDB = inputAUTO.PDB; //unparse does not work
        input.ensemble = ensemble;
        input.tracemol = tracemol;
        results = runMR_RNP(input,results);
        success = results.Success();
        if (results.ErrorType() == MEMORY) throw results;
        sol = results.getDotSol();
        }
        catch (PhaserError& err) {
          if (err.ErrorType() != MEMORY) throw;
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires_with_vrms;
          }
        }
        catch (phaser::error const& err) { //PHASER_ASSERT
          throw;
        }
        catch (std::bad_alloc const& err) {
          results.logHeader(LOGFILE,header(MR_AUTO));
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires_with_vrms;
          }
        }
        catch (std::exception const& err) {
          results.logHeader(LOGFILE,header(MR_AUTO));
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires_with_vrms;
          }
        }
        catch (...) {
          throw;
        }
        //for end pruning, use input KEEP_HIGH_TFZ
        if (success &&
            inputAUTO.SEARCH_PRUNE &&
            sol.has_reject() && //clashing solutions
            !has_helix)
        {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"Solutions with high TFZ rejected due to clashes");
          results.logTab(1,PROCESS,"Occupancies will be refined");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          std::string cards = inputAUTO.Cards();
          InputMR_OCC input(cards);
          input.OCCUPANCY.set_refine_all(false);
          input.setREFL_DATA(inputAUTO.REFLECTIONS);
          input.setRESO(rnp_hires,inputAUTO.LORES);
          input.setSOLU(sol);
          input.DO_XYZOUT.Override(false);
          input.setNORM_DATA(inputAUTO.SIGMAN); //different bins for in situ sf calculation
          input.setTNCS(inputAUTO.PTNCS);
          input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
          input.PDB = inputAUTO.PDB; //unparse does not work
          ResultMR resultOCC = runMR_OCC(input,results);
          results.setOutput(resultOCC);
          sol = resultOCC.getDotSol();
          success = resultOCC.Success();
          DataA data = resultOCC; //for after packing
          if (success && sol.size()) //must end with llg for pdb/mtz
          {
            std::string cards = inputAUTO.Cards();
            InputMR_PAK input(cards);
            input.tracemol = tracemol;
            input.PDB = resultOCC.PDB; //brackets on file names, model serial
            sol.clear_clash();//wipe slate clean, check all after movement of helices
            input.setSOLU(sol);
            input.setZSCO(inputAUTO.ZSCORE); //top has been set
            //input.setPACK_KEEP_HIGH_TFZ(false);
            //PACK_KEEP_HIGH_TFZ flag input obeyed here
            //Even if does not pack after occupancy refinement, solution is kept
            ResultMR resultPAK = runMR_PAK(input,results);
            tracemol = input.tracemol; //input passed by reference
            results.setOutput(resultPAK);
            success = resultPAK.Success();
            sol = resultPAK.getDotSol();
            resultPAK.setDataA(data); //add back in
            if (success && sol.size())
            {
              //permanently switch to new data_pdb object, replacing input
              results = resultPAK; //brackets on file names, model serial
              inputAUTO.PDB = results.PDB; //brackets on file names, model serial
            }
          } //packing clashes
        } //occupancy refinement
        if (success && sol.size())// && has_helix) // i.e. a helix
        { //may refine into clashes if it is small, like a helix
          results.logHeader(LOGFILE,header(MR_AUTO));
          if (has_helix) results.logTab(1,PROCESS,"Search model is a helix");
          results.logTab(1,PROCESS,"Refinement may have introduced clashes");
          results.logTab(1,PROCESS,"Packing will be checked");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          std::string cards = inputAUTO.Cards();
          InputMR_PAK input(cards);
          input.tracemol = tracemol;
          sol.clear_clash();//wipe slate clean, check all after movement of helices
          input.setSOLU(sol);
          input.setZSCO(inputAUTO.ZSCORE); //top has been set
          input.PDB = inputAUTO.PDB; //unparse does not work
          results = runMR_PAK(input,results);
          tracemol = input.tracemol; //input passed by reference
          sol = results.getDotSol();
          success = results.Success();
          if (success && sol.size()) //must end with llg for pdb/mtz
          {
            std::string cards = inputAUTO.Cards();
            InputMR_RNP input(cards);
            input.setREFL_DATA(inputAUTO.REFLECTIONS);
            input.setRESO(rnp_hires,inputAUTO.LORES);
            input.setSOLU(sol);
            input.setNORM_DATA(inputAUTO.SIGMAN);
            // input.setMACM_PROT("OFF"); // Loses cell scale change in rescore
            input.setTNCS(inputAUTO.PTNCS);
            input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
            input.PDB = inputAUTO.PDB; //unparse does not work
            input.ensemble = ensemble;
            input.tracemol = tracemol;
            results = runMR_RNP(input,results);
            success = results.Success();
            if (results.ErrorType() == MEMORY) throw results;
            sol = results.getDotSol();
          } //llg
        } //packing
        if (!success) goto the_end;
      }
      if (success && ((!sol.num_packs() && !inputAUTO.PACKING.KEEP_HIGH_TFZ)
                  ||   (sol.size() && inputAUTO.PACKING.KEEP_HIGH_TFZ))
             ) //only possible if !inputAUTO.PACKING.KEEP_HIGH_TFZ
      {
        results.logHeader(LOGFILE,header(MR_AUTO));
        results.logTab(1,PROCESS,"No solutions after final packing");
        results.logTab(1,PROCESS,"Solution reverts to previous partial solution");
        if (last_sol.size())
        {
          if (!inputAUTO.PACKING.KEEP_HIGH_TFZ)
          {
            results.logTab(2,PROCESS,"Only packing solutions retained");
            int a = last_sol.size();
            last_sol.prune_by_packing(); //has at least one that packs
            int b = last_sol.size();
            results.logTab(3,PROCESS,itos(b) + " solution(s) retained of " + itos(a));
          }
          sol = last_sol;
          partial_result = NSEARCH_SET && true; //only possible if the number to search for is fixed
          results.setDotSol(sol);
          results.logTab(1,PROCESS,"Number of solutions = " + itos(sol.size()));
          if (sol.size())
          {
            results.logTab(1,PROCESS,"Solution TOP LLG = "+ dtos(sol.top_LLG(),1));
            results.logTab(1,PROCESS,"Solution TOP TFZ = "+ dtos(sol.top_TFZ(),1));
          }
        }
        else
        {
          sol = last_sol;
          partial_result = NSEARCH_SET && true; //only possible if the number to search for is fixed
          results.setDotSol(sol);
          results.logTab(1,PROCESS,"No solutions");
        }
        results.logBlank(SUMMARY);
        results.logTrailer(LOGFILE);
      }
      if (success) //place where sol file is finally written
      {
        results.logHeader(LOGFILE,header(MR_AUTO));
        if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:MR Result: $$ Baubles Markup $$");
        results.logBlank(SUMMARY);
        if (DO_KEYWORDS) results.writeSol();
        results.tracemol = tracemol;
        results.writePdb(DO_XYZOUT,inputAUTO.PDB);
        if (DO_HKLOUT) results.writeMtz(inputAUTO.HKLIN);
        if (results.numSolutions() &&
            inputAUTO.ZSCORE.USE &&
            !inputAUTO.ZSCORE.over_cutoff(results.getTopTFZeq()))
          results.logWarning(SUMMARY,"Top solution has TFZ score below the cutoff for a definite solution (" + dtos(inputAUTO.ZSCORE.cutoff()) + ")\nAsymmetric unit may be incomplete, overfilled, partly incorrect or completely incorrect");
        //different results
        if (results.numSolutions() == 0)
        {
          results.phenixCallback("result","No Solution");
          results.logTab(1,PROCESS,"Sorry - No solution");
          results.logTab(1,SUMMARY,"You may find a solution with a different search or selection strategy");
        }
        else if (results.uniqueSolution() && !partial_result)
        {
          results.phenixCallback("result","Single Solution");
          results.logTab(1,PROCESS,"SINGLE solution");
        }
        else if (partial_result)
        {
          results.phenixCallback("result","Partial Solutions");
          results.logBlank(SUMMARY);
          results.logTab(1,PROCESS,"Sorry - No solution with all components");
          if (!(inputAUTO.MRSET != sol) && inputAUTO.MRSET.size())
            results.logTab(1,PROCESS,"Search did not extend input solution with new components");
          results.logTab(1,SUMMARY,"You may find a solution with a different search or selection strategy");
        }
        else
        {
          results.phenixCallback("result",itos(results.numSolutions()) + " solutions");
          results.logTab(1,PROCESS,"There were " + itos(results.numSolutions()) + " solutions");
        }
        //common output
        if (results.numSolutions() > 0)
        {
          results.logBlank(SUMMARY);
          std::string plural = results.numSolutions() > 1 ? "s" : "";
          if (results.getSolFile().size())
            results.logTab(1,PROCESS,"Solution" + plural + " written to SOL file:  " + results.getSolFile());
          if (!results.uniqueSolution() && (results.getNumPdbFiles() || results.getNumMtzFiles()))
            results.logTab(1,PROCESS,"Pdb and/or Mtz files have been written with results for " + itos(results.getNumTop()) + " of these solutions");
          results.logBlank(SUMMARY);
          std::string part = partial_result ? "Partial " : "";
          int num_pdb_mtz = std::max(results.getNumPdbFiles(),results.getNumMtzFiles());
          int extra_sol(0),extra_sol_max(9);
          for (int f = 0; (f < results.numSolutions()) && (extra_sol < extra_sol_max); f++)
          {
            if (f == num_pdb_mtz && (results.numSolutions() > extra_sol_max))
            {
              results.logTab(1,SUMMARY,"Annotation shown below for a maximum of a further " + itos(extra_sol_max) + " solutions");
              results.logTab(1,SUMMARY,"See SOL file for any additional solutions");
              results.logBlank(SUMMARY);
            }
            std::string num = (partial_result || !results.uniqueSolution()) ? " #" + itos(f+1) :"" ;
            if (results.getNumPdbFiles() > f)
              results.logTab(1,PROCESS,part + "Solution" + num + " written to PDB file:  " + results.getPdbFile(f));
            if (results.getNumEnsFiles() > f)
            for (int e = 0; e < results.getTopEnsFile().size(); e++)
              results.logTab(1,PROCESS,part + "Solution" + num + " written to PDB (ensemble) file:  " + results.getEnsFile(f)[e]);
            if (results.getNumPakFiles() > f)
              results.logTab(1,PROCESS,part + "Solution" + num + " packing coordinates written to PDB file:  " + results.getPakFile(f));
            if (results.getNumMtzFiles() > f)
              results.logTab(1,PROCESS,part + "Solution" + num + " written to MTZ file:  " + results.getMtzFile(f));

            results.logTab(1,SUMMARY,part + "Solution" + num + " annotation (history):");
            results.logTab(0,SUMMARY,results.getSet(f).logfile(1,true,(f < results.numSolutions())),false);
            if (f < results.numSolutions()) results.logBlank(SUMMARY);
            if (f >= num_pdb_mtz) extra_sol++;
          }
        }
        results.logBlank(SUMMARY);
        if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
      }
    }
    the_end:
    if (success)
    {
      results.logTrailer(LOGFILE);
    }
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_FAST(InputMR_AUTO& inputAUTO)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_FAST(inputAUTO,output);
}

} //end namespace phaser
