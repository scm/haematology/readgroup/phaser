//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineMR.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/miller/index_span.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runSANDBOX(InputMR_ELLG& input,Output output)
{
  ResultANO results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(SANDBOX));
    input.Analyse(results);

    results.logTab(1,LOGFILE,"Setup map");
    Ensemble data;
    //resize the maps
    cctbx::miller::index_span index_span(input.REFLECTIONS.MILLER.const_ref());
    int hmax = index_span.abs_range()[0];
    int kmax = index_span.abs_range()[1];
    int lmax = index_span.abs_range()[2];
    int sizeh =  2*hmax+1;
    int sizek =  2*kmax+1;
    int sizel =  lmax+1; //no anomalous
    results.logTab(1,LOGFILE,"Range:" + itos(hmax) + " " + itos(kmax) + " " + itos(lmax));
    data.resizeMaps(sizeh,sizek,sizel);

    PHASER_ASSERT(input.REFLECTIONS.MILLER.size() == input.REFLECTIONS.FTFMAP.FMAP.size());
    for (int r = 0; r < input.REFLECTIONS.MILLER.size(); r++)
    {
      cmplxType WeightedE(0,0);
      ivect3 miller = input.REFLECTIONS.MILLER[r];
      std::complex<double> dF = std::polar(input.REFLECTIONS.FTFMAP.FMAP[r],scitbx::deg_as_rad(input.REFLECTIONS.FTFMAP.PHMAP[r]));
      dF = input.REFLECTIONS.FTFMAP.FOM[r]*dF;
      data.setE(miller[0],miller[1],miller[2],dF);
    }

    for (int r = 0; r < input.REFLECTIONS.MILLER.size(); r++)
    if (!(r%1000))
    {
      ivect3 miller = input.REFLECTIONS.MILLER[r];
      std::complex<double> dF = std::polar(input.REFLECTIONS.FTFMAP.FMAP[r],scitbx::deg_as_rad(input.REFLECTIONS.FTFMAP.PHMAP[r]));
      results.logTab(1,LOGFILE,"miller=" + ivtos(miller) +
                            " input=" + ctos(dF) +
                            " output=" + ctos(data.getE(miller[0],miller[1],miller[2])));
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

} //end namespace phaser
