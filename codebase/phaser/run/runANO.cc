//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineANO.h>
#include <cctbx/sgtbx/search_symmetry.h>
#include <phaser/lib/jiffy.h>
#include <phaser/src/Minimizer.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultANO runANO(InputANO& input,Output output)
{
  ResultANO results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(ANO));
    input.Analyse(results);

    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION,
                   input.HIRES,input.LORES);

    results.logSectionHeader(LOGFILE,"DATA FOR ANISOTROPY CORRECTION");
    refa.logDataStats(LOGFILE,results);
    refa.logOutliers(LOGFILE,results);
    refa.logUnitCell(VERBOSE,results);
    refa.logDataStatsExtra(DEBUG,20,results);
    results.logBlank(DEBUG);

    results.logSectionHeader(LOGFILE,"ANISOTROPY CORRECTION");
    for (unsigned p = 0 ; p < input.MACRO_ANO.size(); p++)
    {
      results.logTab(1,LOGFILE,"Protocol cycle #"+ itos(p+1) + " of " + itos(input.MACRO_ANO.size()));
      results.logTabPrintf(0,LOGFILE,"%s",input.MACRO_ANO[p]->logfile().c_str());
      results.logBlank(LOGFILE);
    }
    Minimizer minimizer;
    results.startClock();
    minimizer.run(refa,input.MACRO_ANO,results);
    results.logElapsedTime(DEBUG);
    results.logSectionHeader(LOGFILE,"ABSOLUTE SCALE");
    refa.logScale(LOGFILE,results);

    results.logSectionHeader(LOGFILE,"DATA AFTER ANISOTROPY CORRECTION");
    refa.logDataStats(SUMMARY,results);
    refa.logOutliers(LOGFILE,results);
    refa.logDataStatsExtra(DEBUG,20,results);
    results.logBlank(DEBUG);

    results.setDataA(refa);
    outStream where = SUMMARY;
    results.logSectionHeader(where,"OUTPUT FILES");
    if (input.DO_HKLOUT.True())
    {
      results.writeMtz(input.HKLIN);
      results.logTab(1,VERBOSE,"Applying sharpening of " + ftos(100*input.RESHARP.FRAC) +  "% of the B-factor");
    }
    else results.logTab(1,where,"No files output");
    for (int f = 0; f < results.getFilenames().size(); f++)
      results.logTab(1,where,results.getFilenames()[f]);
    results.logBlank(where);
    //write the binary file for output if requested
    if (input.SIGMAN.FILENAME.size() && !input.SIGMAN.READ)
    {
      bool success = results.SIGMAN.write_file(refa.MILLER);
      if (success)
         results.logTab(1,where,"Normalization parameters written to: " + input.SIGMAN.FILENAME);
      else throw PhaserError(FILEOPEN,input.SIGMAN.FILENAME);
      results.logBlank(where);
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultANO runANO(InputANO& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runANO(input,output);
}

} //end namespace phaser
