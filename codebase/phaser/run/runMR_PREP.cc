//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/round.h>
#include <phaser/lib/sg_prior.h>
#include <cmtzlib.h>
#include <mtzdata.h>
#include <ccp4_errno.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <phaser_defaults.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR_DAT runMR_PREP(InputMR_DAT& input,Output output)
{
  ResultMR_DAT results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  if ((!input.SGALT_BASE.size() ||
       (input.SGALT_BASE.size() && input.SGALT_BASE == input.REFLECTIONS.SG_HALL))
      && (input.REFLECTIONS.SG_HALL.size())
      && (cctbx::uctbx::unit_cell(input.UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(input.REFLECTIONS.UNIT_CELL),0.001,0.001)))
  {
    results.storeData(input.REFLECTIONS);
    return results;
  }
  try
  {
    CCP4::ccp4_liberr_verbosity(0);
    results.logHeader(LOGFILE,header(MR_PREP));
    input.Analyse(results);

    results.logTabPrintf(1,SUMMARY,"Input Space-Group Name (Hall Symbol): %s (%s)\n",SpaceGroup(input.REFLECTIONS.SG_HALL).spcgrpname().c_str(),input.REFLECTIONS.SG_HALL.c_str());
    results.logTabPrintf(1,SUMMARY,"Final Space-Group Name (Hall Symbol): %s (%s)\n",SpaceGroup(input.SGALT_BASE).spcgrpname().c_str(),input.SGALT_BASE.c_str());
    results.logBlank(SUMMARY);

    bool DEFAULT_CELL = cctbx::uctbx::unit_cell(input.UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
    if (!DEFAULT_CELL)
    {
      bool REFL_DEFAULT_CELL = cctbx::uctbx::unit_cell(input.REFLECTIONS.UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
      if (!REFL_DEFAULT_CELL)
      {
        results.logTab(1,SUMMARY,"Input Unit Cell: " + dvtos(input.REFLECTIONS.UNIT_CELL));
        input.REFLECTIONS.UNIT_CELL = input.UNIT_CELL;
        results.logTab(1,SUMMARY,"Final Unit Cell: " + dvtos(input.REFLECTIONS.UNIT_CELL));
        results.logBlank(SUMMARY);
      }
      else
      {
        input.REFLECTIONS.UNIT_CELL = input.UNIT_CELL;
        results.logTab(1,SUMMARY,"Unit Cell: " + dvtos(input.REFLECTIONS.UNIT_CELL));
        results.logBlank(SUMMARY);
      }
    }

    //bool reindexed = input.REFLECTIONS.change_space_group(input.SGALT_BASE,results);

    //spacegroup and reflections may have been expanded
    results.storeData(input.REFLECTIONS);
    //if (reindexed) results.writeMtz();
    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

ResultMR_DAT runMR_PREP(InputMR_DAT& input)
{
  Output output;
  output.setPackagePhenix();
  return  runMR_PREP(input,output);
}

} //end namespace phaser
