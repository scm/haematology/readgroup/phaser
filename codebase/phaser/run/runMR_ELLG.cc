
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/Composition.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/between.h>
#include <phaser/lib/eLLG.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Dfactor.h>
#include <phaser/mr_objects/rms_estimate.h>
#include <phaser/include/data_formfactors.h>
#include <phaser_defaults.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

class comparison_values
{
  public: double LLG,hires;
  comparison_values(double l=0,double h=0): LLG(l),hires(h) {}
  void clear() { LLG = hires = 0; }
};

ResultELLG runMR_ELLG(InputMR_ELLG& input,Output output)
{
  ResultELLG results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logBlank(LOGFILE);
    results.logTab(0,SUMMARY,"Input Parameters:");
    results.logTab(0,SUMMARY,input.Cards());
    results.logBlank(SUMMARY);
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.REFLECTIONS = input.REFLECTIONS;
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.SG_HALL = resultMR_DAT.getHall();
    input.UNIT_CELL = resultMR_DAT.getCell6();
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_ELLG));
    input.Analyse(results);

    if (!input.SIGMAN.REFINED)
    { //memory
    results.logSectionHeader(LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    results.setDataA(refa);
    }

    if (input.PTNCS.USE)
    { //memory
    Epsilon epsilon(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
      epsilon.findROT(input.MACRO_TNCS,results);
    input.PTNCS = epsilon.getPtNcs();
    input.OUTLIER = epsilon.getOutlier();
    }

    /* DataB tmp(input.REFLECTIONS,
              input.RESHARP,
              input.SIGMAN,
              input.OUTLIER,
              input.PTNCS,
              input.DATABINS,
              input.COMPOSITION);
    outStream where(LOGFILE);
    if (input.DO_INFORMATION.True()) tmp.logInfoBits(where,true,results); */

    cctbx::uctbx::unit_cell cctbxUC = UnitCell(input.UNIT_CELL).getCctbxUC();
    floatType MTZ_HIRES = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
    Composition composition(input.COMPOSITION);
    {//memory
    composition.set_modlid_scattering(input.PDB);
    composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS,input.AUTO_SEARCH);
    input.COMPOSITION = composition;
    if (composition.increased()) //always check it fits
    {
      results.logWarning(SUMMARY,composition.warning());
      std::string cards = input.Cards();
      InputCCA inputCCA(cards);
      inputCCA.setRESO_HIGH(MTZ_HIRES);
      inputCCA.setMUTE(true); //will not throw fatal error
      ResultCCA resultCCA = runCCA(inputCCA,results);
      PHASER_ASSERT(resultCCA.Success()); //cheap
      //throw same error as would be thrown by runCCA
      if (resultCCA.getFitError())
      throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
    }
    }
    floatType TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);

    if (!input.REFLECTIONS.Feff.size())
    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    Dfactor dfactor(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    input.REFLECTIONS = dfactor;
    }

    DataB data(input.REFLECTIONS,input.RESHARP,input.SIGMAN,input.OUTLIER,input.PTNCS,input.DATABINS,
               input.COMPOSITION
             ); //use all data
    std::vector<ssqr_dfac_solt> ssqr_dfac = data.getSelected(input.SOLPAR);
    double reslim(1.2);
    std::vector<ssqr_dfac_solt> ssqr_dfac_perfect = data.getPerfect(input.SOLPAR,reslim);
    results.setDataA(data);
    results.logTab(1,SUMMARY,"Resolution of Data (Selected):    " + dtos(MTZ_HIRES,3) + " (" + dtos(data.HiRes()) + ")");
    results.logTab(1,SUMMARY,"Number of Reflections (Selected): " + itos(input.REFLECTIONS.MILLER.size()) + " (" + itos(ssqr_dfac.size()) + ")");
    if (input.PTNCS.TWINNED)
    results.logTab(1,SUMMARY,"Twinning indicated: Target doubled");
    double ELLG_TARGET = input.ELLG_TARGET*(input.PTNCS.TWINNED?2.0:1.0);
    results.logTab(1,SUMMARY,"eLLG Target: " + dtos(ELLG_TARGET));
    results.logBlank(SUMMARY);

    //set default search order
    results.setSearch(input.AUTO_SEARCH);

    { //memory
    outStream where(LOGFILE);
    results.logSectionHeader(where,"ENSEMBLING");
    for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
      iter->second.setup_and_fix_vrms(iter->first,MTZ_HIRES,input.PTNCS,input.SOLPAR,results);
    }

    outStream where(LOGFILE);
    results.logUnderLine(where,"Guide to eLLG values\n");
    results.logTabPrintf(1,where,"eLLG      Top solution correct?\n");
    results.logTabPrintf(1,where,"<25       -no \n");
    results.logTabPrintf(1,where,"25-36     -unlikely\n");
    results.logTabPrintf(1,where,"36-49     -possibly\n");
    results.logTabPrintf(1,where,"49-64     -probably\n");
    results.logTabPrintf(1,where,">64       -yes\n");
    results.logBlank(where);

    std::set<std::string> single_atom;
    { //memory
    outStream where(SUMMARY);
    for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
    if (iter->second.is_atom)
    {
      single_atom.insert(iter->first);
    }
    if (single_atom.size())
    {
      results.logSectionHeader(where,"SINGLE ATOM ELLG");
      data_formfactors FORMFACTOR(ff_xray);
      bool is_all_nucleic = composition.is_all_nucleic();
      scattering_protein protein;
      scattering_nucleic nucleic;
      for (std::set<std::string>::iterator iter = single_atom.begin(); iter != single_atom.end(); iter++)
      {
        results.logUnderLine(where,"Expected LLG (eLLG): Single Atom");
        results.logTabPrintf(1,where,"%8s %6s %9s %12s  Atomtype\n","B-factor","RMSD","frac-scat","eLLG");
        std::string atm =  stoup(input.PDB[*iter].Coords().one_atom_element());
        cctbx::eltbx::xray_scattering::gaussian fetchX = FORMFACTOR.fetch(atm);
        for (double Bfac = 0; Bfac >= -10; Bfac-=0.5) //more ordered
        {
          PHASER_ASSERT(input.PDB.find(*iter) != input.PDB.end());
          PHASER_ASSERT(input.PDB[*iter].DV.size());
          double RMSD = input.PDB[*iter].DV[0].fixed;
          double FP = input.PDB[*iter].SCATTERING/TOTAL_SCAT;
          //Calculation assumes that there is no change in the total scattering
          //caused by the change in the B-factor of a single atom
          //This calculation will not work changing the B-factor of larger components
          for(int r = 0; r < ssqr_dfac.size(); r++)
          {
            ssqr_dfac[r].bratio = std::exp(-2*(Bfac)*ssqr_dfac[r].ssqr/4.0);
            double scatX = fn::pow2(fetchX.at_d_star_sq(ssqr_dfac[r].ssqr)/
                           cctbx::eltbx::tiny_pse::table(atm).atomic_number());
            double fp = is_all_nucleic ?
                           scatX/nucleic.average_scattering_at_ssqr(ssqr_dfac[r].ssqr):
                           scatX/protein.average_scattering_at_ssqr(ssqr_dfac[r].ssqr);
            ssqr_dfac[r].bratio *= fp;
          }
          double eLLG = get_eLLG(ssqr_dfac,RMSD,FP);
          results.logTabPrintf(1,where,"%8.1f %6.3f %9.5f %12.5f  %s\n",Bfac,RMSD,FP,eLLG,atm.c_str());
          if (eLLG > double(DEF_ELLG_TARG)) break;
        }
        results.logBlank(where);
      }
      for(int r = 0; r < ssqr_dfac.size(); r++)
        ssqr_dfac[r].bratio = 1; //revert to default
    }
    }

    //if (input.ELLG_POLYALANINE.size())
    {
      outStream where(SUMMARY);
      results.logSectionHeader(where,"POLY-ALANINE ELLG");
      results.logTab(1,where,"Resolution = " + dtos(MTZ_HIRES));
      for (std::set<double>::iterator iter=input.ELLG_POLYALANINE.begin(); iter != input.ELLG_POLYALANINE.end(); iter++)
        results.logTab(1,where,"Input RMSD = " + dtos(*iter,5,2));
      if (!input.ELLG_POLYALANINE.size())
      {
        float1D DEFAULT_POLYALA(6);
        DEFAULT_POLYALA[0] = 0.1;
        DEFAULT_POLYALA[1] = 0.2;
        DEFAULT_POLYALA[2] = 0.4;
        DEFAULT_POLYALA[3] = 0.8;
        DEFAULT_POLYALA[4] = 1.6;
        DEFAULT_POLYALA[5] = 3.2;
        results.logTab(1,where,"Default RMSD = " + dvtos(DEFAULT_POLYALA));
        for (int i = 0; i < DEFAULT_POLYALA.size(); i++) input.ELLG_POLYALANINE.insert(DEFAULT_POLYALA[i]);
      }
      double minsol(0.2);
      results.logTab(1,where,"Minimum solvent = " + dtos(minsol*100) + "%");
      double alanine_scat =
          (3)*scitbx::fn::pow2(cctbx::eltbx::tiny_pse::table("C").atomic_number()) +
          (1)*scitbx::fn::pow2(cctbx::eltbx::tiny_pse::table("N").atomic_number()) +
          (1)*scitbx::fn::pow2(cctbx::eltbx::tiny_pse::table("O").atomic_number()) +
          (5)*scitbx::fn::pow2(cctbx::eltbx::tiny_pse::table("H").atomic_number());
      double maxfp(1-minsol);
      int maxnres = maxfp/(alanine_scat/TOTAL_SCAT);
      results.logTab(2,where,"Maximum number of polyalanine residues = " + itos(maxnres) + " (-full-)");

      ///alanine table
      float1D TARGET;
      TARGET.push_back(ELLG_TARGET); //do a range of values going down in ellg
      double start = std::floor(std::sqrt(ELLG_TARGET));
      if (between(fn::pow2(start),ELLG_TARGET,25.0)) start--; //make sure first is below
      for (int i = start; i >= 1; i--) TARGET.push_back(fn::pow2(i)); //to LLG=25,TF=5
      std::reverse(TARGET.begin(),TARGET.end());
      int2D numala;
      numala.resize(TARGET.size());
      for (int i = 0; i < TARGET.size(); i++)
        numala[i].resize(input.ELLG_POLYALANINE.size());
      int jrmsj(0);
      for (std::set<double>::iterator iter=input.ELLG_POLYALANINE.begin(); iter != input.ELLG_POLYALANINE.end(); iter++)
      {
        double rms = *iter;
        int itarget = 0;
        int nres_ellg_jump(0);
        double maxeLLG = get_eLLG(ssqr_dfac,rms,maxfp);
        int nres = std::floor(maxnres/std::sqrt(maxeLLG));
        nres = std::max(1,nres-1); //checked
        if (maxeLLG > 1)
        for (;;)
        {
          double FP = nres*alanine_scat/TOTAL_SCAT;
          if (FP > maxfp)
          {
            break;
          }
          double eLLG = get_eLLG(ssqr_dfac,rms,FP);
          if (eLLG > TARGET[itarget])
          {
            if (itarget == 0) nres_ellg_jump = std::max(0,nres-2); //-2 checked as being conservative jump
            numala[itarget][jrmsj] = nres-1;
            nres += nres_ellg_jump; //jump to next
            itarget++;
          }
          if (itarget == TARGET.size()) //after increment
          {
            results.add_target_nres(rms,nres-nres_ellg_jump-1); //ELLG_TARGET
            break;
          }
          nres++;
        }
        jrmsj++;
      }
      results.logTab(1,where,"Alanine residues for eLLG target = " + itos(results.get_target_nres()));
      results.logUnderLine(where,"Table of Alanine Residues for eLLG Target");
      results.logTabPrintf(1,where,"%11s   %s\n",""," ---RMSD--- ");
      std::string str;
      for (std::set<double>::iterator iter=input.ELLG_POLYALANINE.begin(); iter != input.ELLG_POLYALANINE.end(); iter++)
        str += dtos(*iter,5,2) + " ";
      results.logTabPrintf(1,where,"%11s | %s\n","eLLG-target",str.c_str());
      for (int i = TARGET.size()-1; i >= 0; i--)
      {
        std::string str;
        for (int j = 0; j < numala[i].size(); j++) str += numala[i][j] ? itos(numala[i][j],5,true) + " " : "-full-";
        results.logTabPrintf(1,where,"%11.0f | %s\n",TARGET[i],str.c_str());
      }
      results.logBlank(where);
    }

    double pessimistic_rms_factor(0); //increase for pessimism
    //in previous code this was set to 1 for the known component, 0 for the search component
    if (!single_atom.size())
    {
      std::set<std::string> set_of_modlid = input.AUTO_SEARCH.set_of_modlids();
      set_of_modlid.insert(input.SEARCH_MODLID.begin(),input.SEARCH_MODLID.end());

      {//memory
      outStream where(SUMMARY);
      results.logSectionHeader(where,"MONOMERIC ELLG");
      std::string advisory_message;
      for (std::set<std::string>::iterator iter = set_of_modlid.begin(); iter != set_of_modlid.end(); iter++)
      {
        float1D KRMSD(0),KFP(0);
        std::string modlid = *iter;
        double RMSD(max(input.PDB[modlid].DV.fixed_vrms_array()));
        //RMSD += pessimistic_rms_factor*rms_estimate(input.PDB[modlid].ESTIMATOR).sigma(); //pessimistic

        double maxinputrms(0.0);
        maxinputrms = max(input.PDB[modlid].allRms() , maxinputrms);

        if (RMSD > 2.5*maxinputrms) // sanity check
          advisory_message += ( "\nThe correlation corrected RmsD of ensemble \""+ modlid + "\" is " + dtos(RMSD, 5, 3)
            + " but the largest RmsD assigned to models in the ensemble is only " + dtos(maxinputrms, 5, 3) + ". Is this intentional?" );

        double FP(input.PDB[modlid].SCATTERING/TOTAL_SCAT);
        results.ELLG_DATA[modlid].ELLG = get_eLLG(ssqr_dfac,RMSD,FP);
        results.ELLG_DATA[modlid].USEFUL_RESO = double(input.ELLG_RMSD_MULT)*RMSD;
        results.ELLG_DATA[modlid].RMSD = RMSD;
        results.ELLG_DATA[modlid].FP = FP;
        results.ELLG_DATA[modlid].TARGET_RESO = get_reso_for_eLLG(ELLG_TARGET,ssqr_dfac,RMSD,FP,KRMSD,KFP);
        results.ELLG_DATA[modlid].PERFECT_RESO = get_reso_for_eLLG(ELLG_TARGET,ssqr_dfac_perfect,RMSD,FP,KRMSD,KFP);
      }

      results.logUnderLine(where,"Expected LLG (eLLG)");
      results.logTab(1,where,"eLLG: eLLG of ensemble alone");
      results.logTabPrintf(1,where,"%8s %6s %9s  %s\n","eLLG","RMSD","frac-scat","Ensemble");
      for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        results.logTabPrintf(1,where,"%8.*f %6.3f %9.5f  %s\n",
          (iter->second.ELLG >= 10 ? 1:5),iter->second.ELLG,
          iter->second.RMSD,
          iter->second.FP,
          iter->first.c_str());
      results.logBlank(where);

      results.logUnderLine(where,"Resolution for eLLG target");
      results.logTab(1,where,"eLLG-reso: Resolution to achieve target eLLG (" + dtos(ELLG_TARGET) + ")");
      results.logTabPrintf(1,where,"%11s  %s\n","eLLG-reso","Ensemble");
      for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        results.logTabPrintf(1,where,"%11s  %s\n",
          std::string(iter->second.TARGET_RESO?dtos(iter->second.TARGET_RESO,3):">"+dtos(data.HiRes(),5,2)+"(all)").c_str(),
          iter->first.c_str());
      results.logBlank(where);

      results.logUnderLine(where,"Resolution for eLLG target: data collection");
      results.logTab(1,where,"eLLG-reso: Resolution to achieve target eLLG (" + dtos(ELLG_TARGET) + ") with perfect data");
      results.logTabPrintf(1,where,"%11s  %s\n","eLLG-reso","Ensemble");
      std::string reslimstr(">"+dtos(reslim)+"A ");
      for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        results.logTabPrintf(1,where,"%11s  %s\n",
          std::string(iter->second.PERFECT_RESO?dtos(iter->second.PERFECT_RESO,3):reslimstr).c_str(),
          iter->first.c_str());
      results.logBlank(where);


      for (std::map<std::string, data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
      {
        if (iter->second.ELLG >= double(DEF_ELLG_TARG))
          results.logTab(1, where, "eLLG indicates that placement of a single copy of ensemble \"" + iter->first + "\" should be easy");
        if (double(DEF_DIFFI_ELLG_TARG) < iter->second.ELLG && iter->second.ELLG < double(DEF_ELLG_TARG))
          advisory_message += ("\neLLG indicates that placement of a single copy of ensemble \"" + iter->first + "\" could be difficult");
        if (iter->second.ELLG && iter->second.ELLG <= double(DEF_DIFFI_ELLG_TARG)) // eLLG below DEF_DIFFI_ELLG_TARG makes MR very difficult
          advisory_message += ("\neLLG indicates that placement of a single copy of ensemble \"" + iter->first + "\" will be very difficult");
      }
      if (advisory_message.size()) results.logAdvisory(where,advisory_message);

      if (true)//input.ELLG_CHAIN
      {
        for (std::set<std::string>::iterator iter = set_of_modlid.begin(); iter != set_of_modlid.end(); iter++)
        {
          std::string modlid = *iter;
          if (input.PDB[modlid].map_format()) continue;
          string1D chains = input.PDB[modlid].Coords().getChains();
          float1D KRMSD(0),KFP(0);
          double RMSD(max(input.PDB[modlid].DV.fixed_vrms_array()));
          RMSD += pessimistic_rms_factor*rms_estimate(input.PDB[modlid].ESTIMATOR).sigma(); //pessimistic
          for (int c = 0; c < chains.size(); c++)
          {
            double FP(input.PDB[modlid].Coords().getScat(chains[c])/TOTAL_SCAT);
            results.ELLG_DATA[modlid].CHAIN[chains[c]].ELLG = get_eLLG(ssqr_dfac,RMSD,FP);
            results.ELLG_DATA[modlid].CHAIN[chains[c]].FP = FP;
            results.ELLG_DATA[modlid].CHAIN[chains[c]].RESO= get_reso_for_eLLG(ELLG_TARGET,ssqr_dfac,RMSD,FP,KRMSD,KFP);
          }
        }

        results.logUnderLine(where,"Expected LLG (eLLG): Chains");
        results.logTab(1,where,"eLLG: eLLG of chain alone");
        results.logTabPrintf(1,where,"%8s %6s %9s %5s  %s\n","eLLG","RMSD","frac-scat","chain","Ensemble");
        for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        for (std::map<std::string,data_cllg>::iterator ch = iter->second.CHAIN.begin(); ch != iter->second.CHAIN.end(); ch++)
          results.logTabPrintf(1,where,"%8.*f %6.3f %9.5f %5s  %s\n",
            (ch->second.ELLG >= 10 ? 1:5),ch->second.ELLG,
            iter->second.RMSD,
            ch->second.FP,
            std::string("\"" + ch->first + "\"").c_str(),
            iter->first.c_str());
        results.logBlank(where);

        results.logUnderLine(where,"Resolution for eLLG target: Chains");
        results.logTab(1,where,"eLLG-reso: Resolution to achieve target eLLG (" + dtos(ELLG_TARGET) + ")");
        results.logTabPrintf(1,where,"%11s %5s  %s\n","eLLG-reso","chain","Ensemble");
        for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        for (std::map<std::string,data_cllg>::iterator ch = iter->second.CHAIN.begin(); ch != iter->second.CHAIN.end(); ch++)
          results.logTabPrintf(1,where,"%11s %5s  %s\n",
            std::string(ch->second.RESO?dtos(ch->second.RESO,3):">"+dtos(data.HiRes(),5,2)+"(all)").c_str(),
            std::string("\"" + ch->first + "\"").c_str(),
            iter->first.c_str());
        results.logBlank(where);
      }
      }//memory

      for (int k = 0; k < input.MRSET.size(); k++)
      {
        outStream where(VERBOSE); //only output first to logfile
        results.logSectionHeader(where,"ELLG SOLUTION #" + itos(k+1) + " OF " + itos(input.MRSET.size()));
        float1D KRMSD(0),KFP(0);
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          std::string MODLID = input.MRSET[k].KNOWN[s].MODLID;
          double maxRMSD(max(input.PDB[MODLID].DV.fixed_vrms_array()));
          if (input.MRSET[k].DRMS.find(MODLID) != input.MRSET[k].DRMS.end())
            maxRMSD = max(newvrms(input.MRSET[k].DRMS[MODLID],input.PDB[MODLID].DV.fixed_vrms_array()));
          maxRMSD += pessimistic_rms_factor*rms_estimate(input.PDB[MODLID].ESTIMATOR).sigma(); //pessimistic
          KRMSD.push_back(maxRMSD);
          KFP.push_back(input.PDB[MODLID].SCATTERING/TOTAL_SCAT);
        }

        for (std::set<std::string>::iterator iter = set_of_modlid.begin(); iter != set_of_modlid.end(); iter++)
        {
          std::string modlid = *iter;
          double RMSD(max(input.PDB[modlid].DV.fixed_vrms_array()));
          RMSD += pessimistic_rms_factor*rms_estimate(input.PDB[modlid].ESTIMATOR).sigma(); //pessimistic
          double FP(input.PDB[modlid].SCATTERING/TOTAL_SCAT);
          //fixed parts
          //results.ELLG_DATA[modlid].ELLG = get_eLLG(ssqr_dfac,float1D(1,RMSD),float1D(1,FP));
          //results.ELLG_DATA[modlid].USEFUL_RESO = double(DEF_ELLG_RMSD_MULT)*RMSD;
          //results.ELLG_DATA[modlid].RMSD = RMSD;
          //results.ELLG_DATA[modlid].FP = FP;
          //now add know parts
          results.ELLG_DATA[modlid].RESO_KNOWN.push_back(get_reso_for_eLLG(ELLG_TARGET,ssqr_dfac,RMSD,FP,KRMSD,KFP));
          results.ELLG_DATA[modlid].ELLG_KNOWN.push_back(get_eLLG(ssqr_dfac,0,0,KRMSD,KFP));
          results.ELLG_DATA[modlid].ELLG_ADDED.push_back(get_eLLG(ssqr_dfac,RMSD,FP,KRMSD,KFP));
        }

        results.logUnderLine(where,"Currently Placed Ensembles");
        results.logTabPrintf(1,where,"RMSD: rms coordinate deviation used for eLLG calculation");
        results.logTabPrintf(1,where,"frac-scat: fraction of the scattering used for eLLG calculation");
        results.logTabPrintf(1,where,"  #   rmsd frac-scat  Ensemble");
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
          results.logTabPrintf(1,where,"%3d %6.3f %9.5f  %s\n",s+1,KRMSD[s],KFP[s],input.MRSET[k].KNOWN[s].MODLID.c_str());
        results.logBlank(where);

        results.logUnderLine(where,"Expected LLG (eLLG)");
        results.logTab(1,where,"eLLG: eLLG of ensemble alone");
        results.logTab(1,where,"eLLG--: eLLG of already placed components");
        results.logTab(1,where,"eLLG++: eLLG of ensemble when added to already placed components");
        results.logTabPrintf(1,where,"%8s %8s %8s %10s %6s %9s  %s\n","eLLG","eLLG--","eLLG++","resolution","RMSD","frac-scat","Ensemble");
        for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
          results.logTabPrintf(1,where,"%8.*f %8.*f %8.*f %10s %6.3f %9.5f  %s\n",
            (iter->second.ELLG >= 10 ? 1:5),iter->second.ELLG,
            (iter->second.ELLG_KNOWN[k] >= 10 ? 1:5),iter->second.ELLG_KNOWN[k],
            (iter->second.ELLG_ADDED[k] >= 10 ? 1:5),iter->second.ELLG_ADDED[k],
            std::string(dtos(data.HiRes(),5,2)+"(all)").c_str(),
            iter->second.RMSD,
            iter->second.FP,
            iter->first.c_str());
        results.logBlank(where);

        results.logUnderLine(where,"Resolution for eLLG target");
        results.logTab(1,where,"eLLG-reso: Resolution to achieve target eLLG (" + dtos(ELLG_TARGET) + ")");
        results.logTabPrintf(1,where,"%11s  %s\n","eLLG-reso","Ensemble");
        for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
        {
          results.logTabPrintf(1,where,"%11s  %s\n",
            std::string(iter->second.RESO_KNOWN[k]?dtos(iter->second.RESO_KNOWN[k],3):">"+dtos(data.HiRes(),5,2)+"(all)").c_str(),
            iter->first.c_str());
        }
        results.logBlank(where);

      } //k

      if (input.MRSET.size())
      {
        outStream where(SUMMARY);
        results.logSectionHeader(where,"ELLG FOR SOLUTIONS");
        results.logUnderLine(where,"eLLG Table for Addition");
        results.logTab(1,where,"eLLG--: eLLG of already placed components");
        results.logTab(1,where,"eLLG++: eLLG of ensemble when added to already placed components");
        results.logTab(1,where,"eLLG-reso: Resolution to achieve target eLLG (" + dtos(ELLG_TARGET) + ")");
        results.logTabPrintf(1,where,"%4s %8s %8s %11s %6s %9s  %s\n","#","eLLG--","eLLG++","eLLG-reso","RMSD","frac-scat","Ensemble");
        for (int k = 0; k < input.MRSET.size(); k++)
        {
          for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
          {
            results.logTabPrintf(1,where,"%4d %8.*f %8.*f %11s %6.3f %9.5f  %s\n",
              k+1,
              (iter->second.ELLG_KNOWN[k] >= 10 ? 1:5),iter->second.ELLG_KNOWN[k],
              (iter->second.ELLG_ADDED[k] >= 10 ? 1:5),iter->second.ELLG_ADDED[k],
              std::string(iter->second.RESO_KNOWN[k]?dtos(iter->second.RESO_KNOWN[k],3):">"+dtos(data.HiRes(),5,2)+"(all)").c_str(),
              iter->second.RMSD,
              iter->second.FP,
              iter->first.c_str());
          }
          results.logTab(1,where,"----");
        }
        results.logBlank(where);

        std::set<std::string> restricted;
        std::map<std::string,double> maxTFZeq;
        for (int k = 0; k < input.MRSET.size(); k++)
        {
          for (std::map<std::string,data_ellg>::iterator iter = results.ELLG_DATA.begin(); iter != results.ELLG_DATA.end(); iter++)
          {
            std::string modlid = iter->first;
            double eLLG = std::max(0.0,iter->second.ELLG_ADDED[k]);
            double TFZeq = std::sqrt(eLLG);
            maxTFZeq[iter->first] = std::max(maxTFZeq[iter->first],TFZeq);
            if (input.HIRES >= 0 && //is set
                eLLG < double(DEF_ELLG_TARG) &&//below default, not value set
                input.HIRES > MTZ_HIRES) //input is not max mtz resolution anyway
            restricted.insert(iter->first);
          }
        }
        std::string advisory_message;
        for (std::map<std::string,double>::iterator iter = maxTFZeq.begin(); iter != maxTFZeq.end(); iter++)
        {
          std::string card("not");
          if (iter->second > 5.0) card = "be unlikely to";
          if (iter->second > 6.0) card = "possibly";
          if (iter->second > 7.0) card = "probably";
          if (iter->second > 8.0) card = "definitely";
          advisory_message += ("\neLLG indicates that best placement of ensemble \"" + iter->first + "\" will " + card + " be correct in the context of already correctly placed components" );
          if (restricted.find(iter->first) != restricted.end())
            advisory_message += ("\nThe input resolution is restricting the eLLG. It would be advisable to use more data");
        }
        if (advisory_message.size()) results.logAdvisory(where,advisory_message);
      }

      {//memory
      outStream where(SUMMARY);
      results.logSectionHeader(where,"HOMO-OLIGOMERIC ELLG");
      int k(0);
      if (input.MRSET.size())
      {
      results.logTab(1,where,"eLLG calculated with top solution placed");
      results.logTab(0,where,input.MRSET[k].logfile(1),false);
      results.logBlank(where);
      }
      results.logUnderLine(where,"Number of copies for eLLG target");
      results.logTabPrintf(1,where,"%11s %6s %15s %9s %12s  %s\n","eLLG-target","RMSD","frac-scat-known","frac-scat","num-copies","Ensemble");
      float1D KRMSD(0),KFP(0);
      for (int s = 0; input.MRSET.size() && s < input.MRSET[k].KNOWN.size(); s++)
      {
        std::string MODLID = input.MRSET[k].KNOWN[s].MODLID;
        double maxRMSD(max(input.PDB[MODLID].DV.fixed_vrms_array()));
        if (input.MRSET[k].DRMS.find(MODLID) != input.MRSET[k].DRMS.end())
          maxRMSD = max(newvrms(input.MRSET[k].DRMS[MODLID],input.PDB[MODLID].DV.fixed_vrms_array()));
        maxRMSD += pessimistic_rms_factor*rms_estimate(input.PDB[MODLID].ESTIMATOR).sigma(); //pessimistic
        KRMSD.push_back(maxRMSD);
        KFP.push_back(input.PDB[MODLID].SCATTERING/TOTAL_SCAT);
      }
      for (std::set<std::string>::iterator iter = set_of_modlid.begin(); iter != set_of_modlid.end(); iter++)
      {
        std::string modlid = *iter;
        double rms(max(input.PDB[modlid].DV.fixed_vrms_array()));
        int ncopy(1);
        for (;;)
        {
          double FP = ncopy*input.PDB[modlid].SCATTERING/TOTAL_SCAT;
          if (FP > 1) { ncopy--; break; }
          double eLLG = get_eLLG(ssqr_dfac,rms,FP,KRMSD,KFP);
          if (eLLG > ELLG_TARGET) break;
          ncopy++;
        }
        double FP = ncopy*input.PDB[modlid].SCATTERING/TOTAL_SCAT;
        //double eLLG = get_eLLG(ssqr_dfac,rms,FP,KRMSD,KFP);
        double lastFP = (ncopy-1)*input.PDB[modlid].SCATTERING/TOTAL_SCAT;
        double lastLLG = get_eLLG(ssqr_dfac,rms,lastFP,KRMSD,KFP);
        results.add_modlid_ncopy_ellg(modlid,ncopy,lastLLG);
        results.logTabPrintf(1,where,"%11.0f %6.3f %15.5f %9.5f %12d  %s\n",ELLG_TARGET,rms,lastFP,FP,ncopy,modlid.c_str());
      }
      results.logBlank(where);
      }//memory

    }//not single atom

    //change search order
    if (input.AUTO_SEARCH.size() && input.SEARCH_ORDER_AUTO && input.AUTO_SEARCH.not_an_OR_search())
    {
      search_array SEARCH_SORT;
      SEARCH_SORT.resize(input.AUTO_SEARCH.size());
      for (int a = 0; a < input.AUTO_SEARCH.size(); a++)
      {
        std::string modlid = input.AUTO_SEARCH[a].MODLID[0];
        floatType eLLG = results.ELLG_DATA[modlid].ELLG; //without the background
        SEARCH_SORT[a].set_modlid(input.AUTO_SEARCH[a].af_MODLID());
        SEARCH_SORT[a].eLLG = eLLG; //without the background
      }
      if (input.SEARCH_ORDER_AUTO) SEARCH_SORT.sort_ellg();
      results.setSearch(SEARCH_SORT);
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultELLG runMR_ELLG(InputMR_ELLG& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_ELLG(input,output);
}

} //end namespace phaser
