//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineGYRE.h>
#include <phaser/lib/xyzRotMatDeg.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/MapEnsemble.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>
#include <phaser/src/Dfactor.h>
#include <scitbx/random.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultGYRE runMR_GYRE(InputMR_RNP& input,Output output)
{
  ResultGYRE results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_GYRE));
    input.Analyse(results);

    SpaceGroup sg(input.SG_HALL);
    UnitCell uc(input.UNIT_CELL);
    Composition composition(input.COMPOSITION);
    composition.set_modlid_scattering(input.PDB);
    cctbx::uctbx::unit_cell cctbxUC = uc.getCctbxUC();
    composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS);
    if (composition.increased())
      results.logWarning(SUMMARY,composition.warning());
    floatType TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);
    input.COMPOSITION = composition;
    if (composition.increased())
    {
      std::string cards = input.Cards();
      InputCCA inputCCA(cards);
      floatType fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
      inputCCA.setRESO_HIGH(fullhires);
      inputCCA.setMUTE(true);
      ResultCCA resultCCA = runCCA(inputCCA,results);
      PHASER_ASSERT(resultCCA.Success()); //cheap
      //throw same error as would be thrown by runCCA
      if (resultCCA.getFitError())
      throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
    }

    if (!input.SIGMAN.REFINED)
    { //memory
    results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    }

    if (input.PTNCS.USE)
    { //memory
    Epsilon epsilon(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
      epsilon.findROT(input.MACRO_TNCS,results);
    input.PTNCS = epsilon.getPtNcs();
    input.OUTLIER = epsilon.getOutlier();
    }

    if (!input.REFLECTIONS.Feff.size())
    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    Dfactor dfactor(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    input.REFLECTIONS = dfactor;
    }

    //even if there are not MRSETS, set up the result object for return
    results.logSectionHeader(LOGFILE,"DATA FOR ROTATION GYRE REFINEMENT");
    input.MRSET.if_not_set_apply_space_group(input.SG_HALL);
    DataMR mr(input.REFLECTIONS,
              input.RESHARP,
              input.SIGMAN,
              input.OUTLIER,
              input.PTNCS,
              input.DATABINS,
              input.HIRES,input.LORES,
              input.COMPOSITION,
              input.ensemble);
    SigmaN sigmaN = mr.getBinSigmaN();
    bool halfR(false);
    mr.initGfunction(halfR);

    if (input.MRSET.size())
    {
      //if the ensembles are coming from mtz files, check that the high resolution of the input
      //ensembles is not lower than the resolution of the data - if so, truncate data to lower resolution
      stringset modlids = input.MRSET.set_of_modlids();
      floatType HIRES = mr.HiRes();
      for (stringset::iterator iter = modlids.begin(); iter != modlids.end(); iter++)
      {
        std::string m = (*iter);
        floatType mapHIRES(0);
        if (input.PDB[m].ENSEMBLE[0].map_format())
          mapHIRES = std::max(input.PDB[m].HIRES,mapHIRES);
        //allow for interpolation with cell expansion
        mapHIRES *= input.MRSET.max_cell_scale();
        HIRES = std::max(mapHIRES,HIRES);
      }
      if (HIRES > mr.HiRes())
      {
        results.logTab(1,VERBOSE,"High resolution limit imposed by Maps = " + dtos(HIRES,5,2));
        mr.selectReso(HIRES,input.LORES);
      }
      mr.logOutliers(LOGFILE,results);
      mr.logDataStats(SUMMARY,results);
      mr.logSymm(DEBUG,results);
      mr.logUnitCell(DEBUG,results);
      mr.logDataStatsExtra(DEBUG,20,results);

      results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
      floatType LLwilson = mr.WilsonLL();
      mr.logWilson(LOGFILE,LLwilson,results);
      mr.setLLwilson(LLwilson);

      map_str_pdb gyrePDB;
      bool amalgamate_background(input.MRSET.size() == 1);
      {
      results.logSectionHeader(LOGFILE,"GYRE LIST");
      if (input.MRSET.size())
      {
        for (int k = 0; k < input.MRSET.size(); k++)
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          std::string& MODLID = input.MRSET[k].KNOWN[s].MODLID;
          if (gyrePDB.find(MODLID) == gyrePDB.end())
          gyrePDB[MODLID] = input.PDB.find(MODLID)->second;
        }
      }
      if (input.MRSET.size() && input.MRSET[0].RLIST.size())
      {
        results.logTab(1,LOGFILE,"Initial rotations will be created from rlist");
 //       results.logTab(1,LOGFILE,"Rotations will apply to all input ensembles");
//        for (map_str_pdb::iterator iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
 //         results.logTab(2,LOGFILE,"Ensemble: " + iter->first);
        results.logBlank(LOGFILE);
        int nrot(0);
        for (int k = 0; k < input.MRSET.size(); k++)
          nrot += input.MRSET[k].RLIST.size();
        mr_solution MRSET;
   //                 MRSET.copy_extras(input.MRSET);
        { //split the input into single mr_sets of (KNOWN + 1 RLIST) because these will be sorted on background+trial
          //if there is only one background then the sort on rlist will be fine (must share background)
          //but sort this out at the end
          MRSET.resize(nrot);
        }
        results.logTabPrintf(1,LOGFILE,"SET# GYRE#   %-7s %7s %7s   %7s %7s\n","Euler","","","RF","RFZ");
        nrot = 0;
        for (int k = 0; k < input.MRSET.size(); k++)
        {
          for (int e = 0; e < input.MRSET[k].RLIST.size(); e++)
          {
            MRSET[nrot] = input.MRSET[k]; //copy known
            MRSET[nrot].ANNOTATION +=  input.MRSET[k].RLIST[e].RFZ ? std::string(" RF/RFZ=" + dtos(input.MRSET[k].RLIST[e].RF,0) + "/" + dtos(input.MRSET[k].RLIST[e].RFZ,1)) : " RF++";
            MRSET[nrot].RLIST.clear();
            if (input.PDB.find(input.MRSET[k].RLIST[e].MODLID) == input.PDB.end())
            throw PhaserError(INPUT,"Ensemble " + input.MRSET[k].RLIST[e].MODLID + " not defined");
            string1D chains = input.PDB.find(input.MRSET[k].RLIST[e].MODLID)->second.Coords().getChains();
            MRSET[nrot].GYRE.resize( chains.size());
            MRSET[nrot].TF = input.MRSET[k].RLIST[e].RF; //repurpose TF for GYRE -> RF in ResultGYRE
            MRSET[nrot].TFZ = input.MRSET[k].RLIST[e].RFZ; //repurpose TFZ for GYRE -> RFZ in ResultGYRE
            int ipdb(0);
            for (int c = 0; c < chains.size(); c++)
            {
              std::string GYRE_MODLID = input.PDB.find(input.MRSET[k].RLIST[e].MODLID)->first+"["+ chains[c]+"]";
              gyrePDB[GYRE_MODLID] = input.PDB.find(input.MRSET[k].RLIST[e].MODLID)->second;
              for (int m = 0; m < gyrePDB[GYRE_MODLID].ENSEMBLE.size(); m++)
              gyrePDB[GYRE_MODLID].ENSEMBLE[m].CHAIN = chains[c];
              MRSET[nrot].GYRE[ipdb++] = mr_gyre(GYRE_MODLID,input.MRSET[k].RLIST[e].EULER);
            }
            results.logTabPrintf(1,LOGFILE,"%-4i %-5i   %7.3f %7.3f %7.3f   %7.3f %7.3f\n",
              k+1,e+1,input.MRSET[k].RLIST[e].EULER[0],input.MRSET[k].RLIST[e].EULER[1],input.MRSET[k].RLIST[e].EULER[2],input.MRSET[k].RLIST[e].RF,input.MRSET[k].RLIST[e].RFZ);
            nrot++;
          }
          input.MRSET[k].RLIST.clear();
        }
        input.MRSET = MRSET; //overwrite
        for (pdbIter iter = gyrePDB.begin(); iter != gyrePDB.end(); iter++)
        {
          iter->second.setup_has_been_called = false;
          std::string error = iter->second.setup_molecules();
          if (error.size()) throw PhaserError(INPUT,error);
        }
        results.logBlank(LOGFILE);
      }
      else if (input.MRSET.size())
      {
        results.logTabPrintf(1,LOGFILE,"SET# GYRE# %-7s %7s %7s   %-20s\n","Euler","","","Ensemble");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          if (!input.MRSET[k].GYRE.size())
            throw PhaserError(INPUT,"Gyre set #" + itos(k+1) + " is empty");
          int e(0);
          if (input.MRSET[k].GYRE.size())
            results.logTabPrintf(1,LOGFILE,"%-4i %-5i %7.3f %7.3f %7.3f   %-20s\n",
                k+1,e+1,
                input.MRSET[k].GYRE[e].euler_ang(0),input.MRSET[k].GYRE[e].euler_ang(1),input.MRSET[k].GYRE[e].euler_ang(2),
                input.MRSET[k].GYRE[e].MODLID.c_str());
          for (e = 1; e < input.MRSET[k].GYRE.size(); e++)
            results.logTabPrintf(1,LOGFILE,"%4s %-5i %7s %7s %7s   %-20s\n",
                  "",e+1,"","","",
                  input.MRSET[k].GYRE[e].MODLID.c_str());
          results.logTab(1,LOGFILE,"---- ----");
        }
        results.logBlank(LOGFILE);
        gyrePDB = input.PDB;
      }
      }
      results.init(gyrePDB);

      std::map<std::string,drms_vrms> DV;
      if (input.MRSET.size())
      {
        results.logSectionHeader(LOGFILE,"ENSEMBLING");
        //there may be lots of ensembles in an OR search
        //we don't want them all in memory at the same time
        //load the ones that are extras first, extract the table information (and check)
        //and then leave the MapEnsemble object in the state for the first refinement
        for (pdbIter iter = gyrePDB.begin(); iter != gyrePDB.end(); iter++)
          iter->second.setup_and_fix_vrms(iter->first,HIRES,input.PTNCS,input.SOLPAR,results);
        mr.ensPtr()->configure(input.MRSET.set_of_modlids(),gyrePDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[0].set_of_modlids(),results);
        mr.ensPtr()->ensembling_table(LOGFILE,TOTAL_SCAT,results);
        std::string warning = input.MRSET.setup_vrms_delta(mr.ensPtr()->get_drms_vrms());
        input.MRSET.setup_newvrms(mr.ensPtr()->get_drms_vrms());
        if (warning.size())
          results.logWarning(SUMMARY,"Conflict in RMSD between ENSEMBLE and SOLUTION inputs\n" + warning);
        results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
        DV = mr.ensPtr()->get_drms_vrms();
      }

      results.logSectionHeader(LOGFILE,"ROTATION GYRE REFINEMENT");
      for (pdbIter iter = gyrePDB.begin(); iter != gyrePDB.end(); iter++)
      for (int m = 0; m < iter->second.ENSEMBLE.size(); m++)
      {
        std::string& CHAIN = iter->second.ENSEMBLE[m].CHAIN;
        CHAIN.erase(remove_if(CHAIN.begin(), CHAIN.end(), isspace), CHAIN.end());
        for (unsigned p = 0 ; p < input.MACRO_GYRE.size(); p++)
        {
          map_str_float CHAIN_SIGR = input.MACRO_GYRE[p]->getMAP(macg_sigr);
          map_str_float CHAIN_SIGT = input.MACRO_GYRE[p]->getMAP(macg_sigt);
          if (CHAIN.size() && CHAIN_SIGR.find(CHAIN) == CHAIN_SIGR.end())
            CHAIN_SIGR[CHAIN] = input.MACRO_GYRE[p]->getNUM(macg_sigr);
          if (CHAIN.size() && CHAIN_SIGT.find(CHAIN) == CHAIN_SIGT.end())
            CHAIN_SIGT[CHAIN] = input.MACRO_GYRE[p]->getNUM(macg_sigt);
          input.MACRO_GYRE[p]->setMAP(macg_sigr,CHAIN_SIGR);
          input.MACRO_GYRE[p]->setMAP(macg_sigt,CHAIN_SIGT);
        }
      }
      for (unsigned p = 0 ; p < input.MACRO_GYRE.size(); p++)
      {
        results.logTab(1,LOGFILE,"Protocol cycle #"+ itos(p+1) + " of " + itos(input.MACRO_GYRE.size()));
        results.logTabPrintf(0,LOGFILE,"%s",input.MACRO_GYRE[p]->logfile().c_str());
        results.logBlank(LOGFILE);
      }


        int nthreads = 1;
#pragma omp parallel
        {
#ifdef _OPENMP
          nthreads = omp_get_num_threads();
#endif
        }
//end #pragma omp parallel

      if (input.MRSET.size() == 1)
        results.logTab(1,LOGFILE,"There is 1 gyre to refine");
      else
        results.logTab(1,LOGFILE,"There are " + itos(input.MRSET.size()) + " gyre to refine");
      if (nthreads > 1)
        results.logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
      results.logProgressBarStart(LOGFILE,"Refining solutions",input.MRSET.size()/nthreads);

      mr.ensPtr()->setPtInterpEV(8);

// create nthreads results objects to collect text results from each thread
      bool omp_condition(!(nthreads == 1 && results.level(DEBUG)));
      std::vector<Output> omp_results;
      omp_results.resize(nthreads);
      for(int n = 0; n < nthreads; ++n)
      {
        // concurrent threads must not write to stdout
        omp_results[n].run_time = results.run_time;
        omp_results[n].setLevel(input.OUTPUT_LEVEL);
        omp_results[n].setMute(omp_condition);
      }

      int MRSET_size = input.MRSET.size();

#if _OPENMP
      map_str_pdb omp_PDB = gyrePDB;
#else
      map_str_pdb& omp_PDB = gyrePDB;
#endif

#pragma omp parallel for firstprivate(mr,omp_PDB)
      for (int k = 0; k < MRSET_size; k++)
      {
        int nthread = 0;
#ifdef _OPENMP
        nthread = omp_get_thread_num();
#endif
        omp_results[nthread].logUnderLine(LOGFILE,"GYRE REFINEMENT SET #" + itos(k+1) + " OF " + itos(MRSET_size));
        DataMR ompmr(mr);
        RefineGYRE rotref(ompmr);
        rotref.ensPtr()->configure(input.MRSET[k].set_of_modlids(),omp_PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[k].set_of_modlids(),omp_results[nthread]);
        if (input.MRSET[k].KNOWN.size())
        {
          omp_results[nthread].logTab(1,LOGFILE,"Fixed components:");
          for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            omp_results[nthread].logTab(0,LOGFILE,input.MRSET[k].KNOWN[s].logfile(1),false);
          omp_results[nthread].logBlank(LOGFILE);
          ompmr.initKnownMR(input.MRSET[k]);
          ompmr.initSearchMR();
          //halfR = false;
          //ompmr.initGfunction(halfR);
          ompmr.calcKnownVAR();
          ompmr.calcKnown();
          //halfR = true;
          //ompmr.initGfunction(halfR);
          floatType LLgain = ompmr.likelihoodFn();
          omp_results[nthread].logTab(1,VERBOSE,"The LL gain with fixed solutions only is " + dtos(LLgain));
        }
        rotref.GYRE = input.MRSET[k].GYRE;
        rotref.initSearchMR();
        rotref.calcRotRefVAR();
        rotref.calcRotRef();
        input.MRSET[k].ORIG_NUM = k+1;
        input.MRSET[k].ORIG_LLG = rotref.likelihoodFnWilson();
        Minimizer minimizer;
        minimizer.run(rotref,input.MACRO_GYRE,omp_results[nthread]);
        input.MRSET[k].LLG = rotref.likelihoodFnWilson();
        input.MRSET[k].GYRE = rotref.GYRE;
        input.MRSET[k].DRMS = rotref.getDRMS();
        input.MRSET[k].ANNOTATION += " GRF=("+dtos(input.MRSET[k].ORIG_LLG,0) + ":" + dtos(input.MRSET[k].LLG,0) + ")";
        if (nthread == 0) results.logProgressBarNext(LOGFILE);
      } //end #pragma omp parallel for
      results.logProgressBarEnd(LOGFILE);

      for (int nthread = 0; nthread < nthreads; nthread++)
      {
        results += omp_results[nthread];
        if (omp_condition) results << omp_results[nthread];
      }

      //append and optionally print silenced results from the various threads to the master thread

      results.setDataA(mr); //after sigmaN calculation

      input.MRSET.sort_LLG();
      for (int k = 0; k < input.MRSET.size(); k++)
      {
        input.MRSET[k].NUM = k+1;
        input.MRSET[k].HISTORY += "GYRE(" + itos(input.MRSET[k].ORIG_NUM) + ":" + itos(input.MRSET[k].NUM) + ")";
       //...there are probably too many flags in mr_set for the sort orders...
      }
      if (!input.SORT_LLG)
      {
        input.MRSET.unsort();
      }

      results.logUnderLine(SUMMARY,"Gyre Refinement Table");
      results.logTabPrintf(1,SUMMARY,"%-4s %4s %11s %13s %7s %7s %7s %-s\n",
      "#out","#in","(Start LLG)","(Refined LLG)","D-Angle"," D-Dist","  Vrms ","Ensemble");
      for (int k = 0; k < input.MRSET.size(); k++)
      {
        results.logTabPrintf(1,SUMMARY,"%-4s %-4i %11.1f %13.1f\n",
              input.MRSET[k].NUM == 1 ? "Top1" : itos(input.MRSET[k].NUM).c_str(),
              input.MRSET[k].ORIG_NUM,
              input.MRSET[k].ORIG_LLG,
              input.MRSET[k].LLG
              );
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          results.logTabPrintf(1,SUMMARY,"%-4s %-4s %11s %13s %7s %7s %7s %-s\n",
              "","","","","-fixed-","-fixed-","-fixed-",
              input.MRSET[k].KNOWN[s].MODLID.c_str()
              );
        }
        for (int p = 0; p < input.MRSET[k].GYRE.size(); p++)
        {
          float1D fixed_vrms = DV[input.MRSET[k].GYRE[p].MODLID].fixed_vrms_array();
          float1D vrms = newvrms(input.MRSET[k].DRMS[input.MRSET[k].GYRE[p].MODLID],fixed_vrms);
          results.logTabPrintf(1,SUMMARY,"%-4s %-4s %11s %13s %7.3f %7.3f %7.3f %-s\n",
              "","","","",
              input.MRSET[k].GYRE[p].angle(),
              input.MRSET[k].GYRE[p].distance(),
              vrms[0],
              input.MRSET[k].GYRE[p].MODLID.c_str()
              );
        }
        results.logTab(1,SUMMARY,"----");
      }

      std::string loggraph;
      for (int v = 0; v < input.MRSET.size(); v++)
        loggraph +=
          itos(v+1)+" "+dtos(input.MRSET[v].LLG,10,2)+" "+dtos(input.MRSET[v].ORIG_LLG,10,2)+"\n";
      string1D graphs(1);
               graphs[0] = "Gyre Number vs LLG:AUTO:1,2,3";
      results.logGraph(LOGFILE,"Gyre Table",graphs,"Number final-LLG initial-LLG",loggraph);

      if (input.MRSET.size() && input.PURGE_GYRE.ENABLE && input.PURGE_GYRE.NUMBER)
      {
        results.logSectionHeader(SUMMARY,"PURGE SELECTION");
        results.logTab(1,SUMMARY,"Number used for purge  = " + itos(input.PURGE_GYRE.NUMBER));
        results.logTab(1,SUMMARY,"Number of solutions stored before purge = " + itos(input.MRSET.size()));
        int final_num = std::min(input.MRSET.size(),size_t(input.PURGE_GYRE.NUMBER));
        results.logTab(1,SUMMARY,"Number of solutions stored (deleted) after purge = " +
                         itos(final_num) + " (" + itos(input.MRSET.size()-final_num) + ")");
        results.logBlank(SUMMARY);
      }
      results.MRSET = input.MRSET;

      results.store(amalgamate_background,input.PURGE_GYRE.NUMBER,input.TITLE);//where results are generated
      results.logBlank(SUMMARY);

      if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
      results.logSectionHeader(SUMMARY,"OUTPUT FILES");
      if (input.DO_KEYWORDS.True())
      {
        results.writeRlist();
        results.writeSol();
      }
      if (input.DO_XYZOUT.True()) //suppression possible,string passing in gyre_and_gimble
      {
        results.writePdb();  //must write pdb files, even for python interface
      }
      for (int f = 0; f < results.getFilenames().size(); f++)
        results.logTab(1,SUMMARY,results.getFilenames()[f]);
      if (!results.getFilenames().size()) results.logTab(1,SUMMARY,"No files output");
      results.logBlank(SUMMARY);
    }
    else results.logSectionHeader(SUMMARY,"NO SOLUTIONS");

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultGYRE runMR_GYRE(InputMR_RNP& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_GYRE(input,output);
}

}  //end namespace phaser
