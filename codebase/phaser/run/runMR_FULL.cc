//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/Molecule.h>
#include <phaser/lib/eLLG.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/lib/scattering.h>
#include <phaser/mr_objects/search_array.h>
#include <phaser/src/Composition.h>
#include <phaser/src/DataMR.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_FULL(InputMR_AUTO& inputAUTO,Output output)
{
  ResultMR results(output);
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  results.setLevel(inputAUTO.OUTPUT_LEVEL);
  results.setMute(inputAUTO.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success)
  { //scope for input
    std::string cards = inputAUTO.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.REFLECTIONS = inputAUTO.REFLECTIONS;
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    inputAUTO.REFLECTIONS = resultMR_DAT.DATA_REFL;
    inputAUTO.setSPAC_HALL(resultMR_DAT.getHall());
    inputAUTO.setCELL6(resultMR_DAT.getCell6());
    inputAUTO.SGALT_BASE = "";
    }
  }
  if (success)
  try
  {
    //if these are set then the results of jobs separately are different from jobs run through FRF
    //inputAUTO.setZSCO_USE(false); //backwards compatible, don't use
    //inputAUTO.setSEAR_DEEP(false); //FRF does not have ZSCORE input
    //floatType HIRES(0);
    cctbx::uctbx::unit_cell cctbxUC(inputAUTO.UNIT_CELL);

    floatType MTZ_HIRES = cctbx::uctbx::d_star_sq_as_d(cctbxUC.max_d_star_sq(inputAUTO.REFLECTIONS.MILLER.const_ref()));
    double MW;
    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputCCA input(cards);
      input.setRESO_HIGH(MTZ_HIRES);
      ResultCCA resultCCA = runCCA(input,results);
      results.setOutput(resultCCA);
      MW = resultCCA.getAssemblyMW();
      success = resultCCA.Success();
    }
    if (success && !inputAUTO.AUTO_SEARCH.size())
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      double total_mw = inputAUTO.PDB[inputAUTO.SEARCH_MODLID[0]].MW.total_mw();
      int NUM = std::floor(MW/total_mw);
      results.logAdvisory(LOGFILE,"Search number not input, inferred from composition and search ensemble");
      results.logTab(2,LOGFILE,"Search ensemble molecular weight = " + itos(MW));
      results.logTab(2,LOGFILE,"Composition molecular weight = " + itos(total_mw));
      results.logTab(2,LOGFILE,"Automatic search number = " + itos(NUM));
      af_string modlid = inputAUTO.SEARCH_MODLID.deep_copy();
      for (int i = 0; i < NUM; i++)
      {
        search_component thisSearch;
        thisSearch.MODLID.resize(modlid.size());
        for (int m = 0; m < modlid.size(); m++) thisSearch.MODLID[m] = modlid[m];
        inputAUTO.AUTO_SEARCH.push_back(thisSearch);
      }
      results.logTrailer(LOGFILE);
    }
    if (success)
    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputANO input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      ResultANO resultANO = runANO(input,results);
      results.setOutput(resultANO);
      success = resultANO.Success();
      if (success)
      {
        inputAUTO.SIGMAN = resultANO.getSigmaN();
        inputAUTO.SIGMAN.REFINED = true; //flag for no more refinement
        MTZ_HIRES = resultANO.fullHiRes();
      }
    }
    if (success)
    {
      std::string cards = inputAUTO.Cards();
      InputNCS input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.setNORM_DATA(inputAUTO.SIGMAN);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.PDB = inputAUTO.PDB; //copy the coordinates
      ResultNCS resultNCS = runNCS(input,results);
      results.setOutput(resultNCS);
      success = resultNCS.Success();
      inputAUTO.PTNCS = resultNCS.getPtNcs();
      inputAUTO.PTNCS.REFINED = true; //flag for no more analysis
    }
    if (success)
    { //scope for input
      std::string cards = inputAUTO.Cards();
      InputDFAC input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.DO_HKLOUT.Override(false);
      input.setRESO(0,DEF_LORES);  //all data
      input.setNORM_DATA(inputAUTO.SIGMAN);
      input.setTNCS(inputAUTO.PTNCS);
      input.PDB = inputAUTO.PDB;
      ResultDFAC resultDFAC = runDFAC(input,results);
      results.setOutput(resultDFAC);
      success = resultDFAC.Success();
      if (success)
        inputAUTO.REFLECTIONS = resultDFAC.getReflData();
    }
    if (inputAUTO.PTNCS.use_and_present())
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logUnderLine(SUMMARY,"TNCS Checks");
      results.logTab(1,SUMMARY,"Number of copies per TNCS set = " + itos(inputAUTO.PTNCS.NMOL));
      results.logBlank(SUMMARY);
      std::map<std::string,int> MODLID = inputAUTO.MRSET.map_of_modlids();
      for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
        if (iter->second % inputAUTO.PTNCS.NMOL) //remainder
          results.logAdvisory(SUMMARY,"Input solutions do not obey tNCS NMOL");
      MODLID = inputAUTO.AUTO_SEARCH.map_of_modlids();
      for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
        if (iter->second % inputAUTO.PTNCS.NMOL) //remainder
          results.logWarning(SUMMARY,"Number of search copies (" + iter->first + "#" + itos(iter->second) +  ") not divisible by " + itos(inputAUTO.PTNCS.NMOL) + "\nNumber of search copies edited");
      std::map<search_component,int> befor = inputAUTO.AUTO_SEARCH.map_of_components();
      inputAUTO.AUTO_SEARCH.edit_tncs(inputAUTO.PTNCS.NMOL);
      results.logUnderLine(SUMMARY,"TNCS search components");
      std::map<search_component,int> after = inputAUTO.AUTO_SEARCH.map_of_components();
      for (std::map<search_component,int>::iterator iter = after.begin(); iter != after.end(); iter++)
      {
        results.logTabPrintf(2,SUMMARY,"%-20s | %-5s | search: %-6s %-6s\n","Ensemble","input","sets","number");
        results.logTabPrintf(2,SUMMARY,"%-20s     %-5i           %-6i %-6i\n",
                                 iter->first.MODLID[0].c_str(),
                                 befor[iter->first],
                                 after[iter->first],
                                 after[iter->first]*inputAUTO.PTNCS.NMOL);
        for (int m = 1; m < iter->first.MODLID.size(); m++)
          results.logTabPrintf(2,SUMMARY,"or Ensemble \"%s\"\n",iter->first.MODLID[m].c_str());
      }
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
    if (success) //after editing search copies
    {
      results.logHeader(LOGFILE,header(MR_AUTO));
      Composition composition(inputAUTO.COMPOSITION);
      composition.set_modlid_scattering(inputAUTO.PDB);
      floatType input_TOTAL_SCAT = composition.total_scat(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL);
      composition.table(SUMMARY,input_TOTAL_SCAT,inputAUTO.PDB,inputAUTO.SEARCH_FACTORS,results);
      composition.increase_total_scat_if_necessary(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL,inputAUTO.MRSET,inputAUTO.PTNCS,inputAUTO.SEARCH_FACTORS,inputAUTO.AUTO_SEARCH);
      if (composition.increased())
      {
        results.logWarning(SUMMARY,composition.warning());
        results.logTab(1,PROCESS,"Composition increased");
        results.logTab(2,SUMMARY," from " + dtos(input_TOTAL_SCAT,0));
        floatType TOTAL_SCAT = composition.total_scat(inputAUTO.SG_HALL,inputAUTO.UNIT_CELL);
        results.logTab(3,SUMMARY,"to " + dtos(TOTAL_SCAT,0));
        composition.table(SUMMARY,TOTAL_SCAT,inputAUTO.PDB,inputAUTO.SEARCH_FACTORS,results);
      }
      else
        results.logTab(1,PROCESS,"Composition not increased");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
      inputAUTO.COMPOSITION = composition;
      if (composition.increased())
      {
        std::string cards = inputAUTO.Cards();
        InputCCA input(cards);
        input.setRESO_HIGH(MTZ_HIRES);
        ResultCCA resultCCA = runCCA(input,results);
        results.setOutput(resultCCA);
        success = resultCCA.Success();
      }
    }
    search_array AUTO_SEARCH = inputAUTO.AUTO_SEARCH;
    // after fixing search
    int nPermutations(0);
    if (success && !inputAUTO.DO_PERMUTE)
    {
      nPermutations = 1;
      std::string cards = inputAUTO.Cards();
      InputMR_ELLG input(cards);
      input.setREFL_DATA(inputAUTO.REFLECTIONS);
      input.PDB = inputAUTO.PDB; //unparse does not work
      input.setNORM_DATA(inputAUTO.SIGMAN);
      input.setTNCS(inputAUTO.PTNCS);
      ResultELLG resultELLG = runMR_ELLG(input,results);
      results.setOutput(resultELLG);
      success = resultELLG.Success();
      AUTO_SEARCH = resultELLG.getSearch();
    }
    else if (success)
    {
      AUTO_SEARCH.clear();
      for (int a = 0; a < inputAUTO.AUTO_SEARCH.size(); a++)
        AUTO_SEARCH.push_back(inputAUTO.AUTO_SEARCH[a]);
 //run permutations to end before starting - set "clock"
      do { ; }
      while (std::next_permutation(AUTO_SEARCH.begin(),AUTO_SEARCH.end()));
//count the number of permutations before starting
      do { nPermutations++; }
      while (std::next_permutation(AUTO_SEARCH.begin(),AUTO_SEARCH.end()));
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logTab(1,PROCESS,"Number of permutations of search ensembles = " + itos(nPermutations));
      results.logBlank(SUMMARY);
      nPermutations = 0;
      do
      { //logfile scope
      results.logTab(1,SUMMARY,"Permuted Search Order #" + itos(++nPermutations));
      string2D MODLID = AUTO_SEARCH.getModlidStar();
      for (int ci = 0; ci < MODLID.size(); ci++)
      {
        results.logTab(2,SUMMARY,"#" + itos(ci+1,2) + " " + stos(MODLID[ci][0],inputAUTO.PTNCS.NMOL));
        for (int m = 1; m < MODLID[ci].size(); m++)
          results.logTab(3,SUMMARY,"or " + stos(MODLID[ci][m],inputAUTO.PTNCS.NMOL));
      }
      results.logBlank(SUMMARY);
      }
      while (std::next_permutation(AUTO_SEARCH.begin(),AUTO_SEARCH.end()));
      results.logTrailer(LOGFILE);
    }

    MapTrcPtr tracemol;
    if (inputAUTO.MRSET.size())
    { //first thing, must check packing of input
      mr_solution sol = inputAUTO.MRSET;
      std::string cards = inputAUTO.Cards();
      InputMR_PAK input(cards);
      input.setSOLU(sol);
      input.setZSCO(inputAUTO.ZSCORE); //top has been set
      if (inputAUTO.PTNCS.use_and_present())
        input.setPACK_COMP(false); //don't move first solution to origin
      input.PDB = inputAUTO.PDB; //unparse does not work
      input.PACKING.KEEP_HIGH_TFZ = false;
      input.PACKING.PRUNE = false;
      ResultMR resultPAK = runMR_PAK(input,results);
      results.setOutput(resultPAK);
      success = resultPAK.Success();
      if (success)
      {
        tracemol = input.tracemol; //input passed by reference
        int orig_size = sol.size();
        inputAUTO.MRSET = resultPAK.getDotSol(); //clashes on file
        if (!inputAUTO.MRSET.size())
        {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"Input solution did not pass packing test");
          results.logWarning(SUMMARY,"Input solutions did not pass packing test");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          success = false;//shortcircuit
        }
        else if (inputAUTO.MRSET.size() < orig_size)
        {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"Some input solutions did not pass packing test");
          results.logWarning(SUMMARY,"Some input solutions did not pass packing test");
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
        }
      }
    } //packing

    mr_solution best_sol;
    ResultMR    best_result;
   // int         best_permutation = 0;
    int         best_c = 0;
    double      best_llg = -std::numeric_limits<double>::max();
    bool        best_partial = true;
    int PERMUTATION(0);
    bool foundLastComponent(true);
//start permutations loop
    if (success)
    do
    {
      ++PERMUTATION;
      foundLastComponent = true;
      results.logHeader(LOGFILE,header(MR_AUTO));
      results.logTabPrintf(1,PROCESS,"Loop over permutations\n");
      results.logTab(1,PROCESS,"Permutation #" + itos(PERMUTATION)+ " of " + itos(nPermutations));
      results.logTab(1,PROCESS,"Search Order:");
      for (int c = 0; c < AUTO_SEARCH.size(); c++)
      {
        results.logTabPrintf(2,PROCESS,"#%d: %s",c+1,AUTO_SEARCH[c].MODLID[0].c_str());
        for (int m = 1; m < AUTO_SEARCH[c].MODLID.size(); m++)
        {
          results.logTabPrintf(3,PROCESS,"or %s",AUTO_SEARCH[c].MODLID[m].c_str());
        }
      }
      mr_solution sol = inputAUTO.MRSET;
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);

      for (int c = 0; c < AUTO_SEARCH.size() && success; c++)
      {
        if (!foundLastComponent) break;
        foundLastComponent = false;
        results.logHeader(LOGFILE,header(MR_AUTO));
        results.logTab(1,PROCESS,"Permutation #" + itos(PERMUTATION)+ " of " + itos(nPermutations));
        results.logTab(1,PROCESS,"Search Order (next rotation function *):");
        { //logfile scope
        for (int i = 0; i < c; i++) AUTO_SEARCH[i].set_star_found();
        AUTO_SEARCH[c].set_star_search();
        for (int i = c+1; i < AUTO_SEARCH.size(); i++) AUTO_SEARCH[i].set_star_clear();
        string2D MODLID = AUTO_SEARCH.getModlidStar();
        for (int ci = 0; ci < MODLID.size(); ci++)
        {
          results.logTab(2,PROCESS,"#" + itos(ci+1,2) + " " + MODLID[ci][0]);
          for (int m = 1; m < MODLID[ci].size(); m++)
            results.logTab(3,PROCESS,"or " + MODLID[ci][m]);
        }
        }
        results.logBlank(SUMMARY);
        results.logTrailer(LOGFILE);
        //bool new_component(true); //new for each component, as may go down
        mr_solution rlist;
        MapEnsPtr rf_ensemble;
        {
          std::string cards = inputAUTO.Cards();
          InputMR_FRF input(cards);
          input.setREFL_DATA(inputAUTO.REFLECTIONS);
          input.setSOLU(sol);
          input.DO_XYZOUT.Override(false);
          input.DO_KEYWORDS.Override(false);
          input.setSEAR_ENSE_OR_ENSE(AUTO_SEARCH[c].af_MODLID());
          input.setNORM_DATA(inputAUTO.SIGMAN);
          input.setTNCS(inputAUTO.PTNCS);
          input.ensemble = rf_ensemble;
          input.tracemol = tracemol;
          input.PDB = inputAUTO.PDB; //unparse does not work
          ResultMR_RF resultFRF = runMR_FRF(input,results);
          results.setOutput(resultFRF);
          success = resultFRF.Success();
          if (success)
          {
            rlist = resultFRF.getDotRlist();
            rf_ensemble = input.ensemble; //input passed by reference
            tracemol = input.tracemol; //input passed by reference
          }
        }
        if (success && rlist.size())
        {
          {
            { //memory scope
              std::string cards = inputAUTO.Cards();
              InputMR_FTF input(cards);
              input.setREFL_DATA(inputAUTO.REFLECTIONS);
              input.setSOLU(rlist);
              input.DO_XYZOUT.Override(false);
              input.DO_KEYWORDS.Override(false);
              input.setNORM_DATA(inputAUTO.SIGMAN);
              input.ensemble = rf_ensemble;
              input.tracemol = tracemol;
              input.PDB = inputAUTO.PDB; //unparse does not work
              input.setTNCS(inputAUTO.PTNCS);
              ResultMR_TF resultFTF = runMR_FTF(input,results);
              results.setOutput(resultFTF);
              success = resultFTF.Success();
              if (success)
              {
                sol = resultFTF.getDotSol();
              }
            } //ftf
            if (success && sol.size())
            {
             // int nmol_ptncs_if_present(inputAUTO.PTNCS.NMOL-1);
              std::string cards = inputAUTO.Cards();
              InputMR_PAK input(cards);
              input.tracemol = tracemol;
              sol.clear_clash();// wipe slate clean
              input.setSOLU(sol);
              input.PDB = inputAUTO.PDB; //unparse does not work
              ResultMR resultPAK = runMR_PAK(input,results);
              results.setOutput(resultPAK);
              success = resultPAK.Success();
              if (success)
              {
                sol = resultPAK.getDotSol();
                tracemol = input.tracemol; //pass by reference
              }
            } //packing
            if (success && sol.size())
            {
              std::string cards = inputAUTO.Cards();
              InputMR_RNP input(cards);
              input.setREFL_DATA(inputAUTO.REFLECTIONS);
              input.setSOLU(sol);
              input.setNORM_DATA(inputAUTO.SIGMAN);
              input.ensemble = rf_ensemble;
              input.tracemol = tracemol;
              input.setTNCS(inputAUTO.PTNCS);
              input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != sol[0].KNOWN.size()));
              input.PDB = inputAUTO.PDB; //unparse does not work
              ResultMR resultRNP = runMR_RNP(input,results);
              results.setOutput(resultRNP);
              success = resultRNP.Success();
              if (success)
              {
                sol = resultRNP.getDotSol();
                //HIRES = resultRNP.HiRes(); //anywhere where reso is selected, but here will do
                MTZ_HIRES = resultRNP.fullHiRes();
              } //store sol
              if (inputAUTO.MACM_PROTOCOL.is_default() &&
                  inputAUTO.ZSCORE.USE &&
                  inputAUTO.ZSCORE.over_cutoff(sol.top_TFZ()) &&
                  !inputAUTO.MACRO_MR.back()->getFIX(phaser::macm_vrms))
              {
                results.logHeader(LOGFILE,header(MR_AUTO));
                inputAUTO.MACRO_MR.back()->setFIX(phaser::macm_vrms,true);
                inputAUTO.MACM_PROTOCOL.set("DEFAULT_CUSTOM");
                results.logTab(1,PROCESS,"Protocol for MR refinement is DEFAULT");
                results.logTab(1,PROCESS,"Z-scores over cutoff have been attained");
                results.logTab(1,PROCESS,"Variance(s) of ensemble(s) WILL NOT be refined (further)");
                results.logTrailer(LOGFILE);
              } //change refinement protocol
              if (success && sol.size())
              {
                foundLastComponent = true;
                if ((c > best_c)  || //always better if there are more components
                    (c == best_c && resultRNP.getTopLLG() > best_llg))
                {
                  best_result = resultRNP; //doesn't have to have all components
                //  best_permutation = PERMUTATION;
                  best_llg = resultRNP.getTopLLG();
                  best_sol = sol;
                  best_c = c;
                  best_partial = !(c == AUTO_SEARCH.size()-1);
                  results.phenixCallback("current best solution",best_sol[0].logfile(0,true));
                  results.logHeader(LOGFILE,header(MR_AUTO));
                  results.logTab(1,PROCESS,"Permutation #" + itos(PERMUTATION)+ " of " + itos(nPermutations));
                  results.logTab(1,PROCESS,"New Best Solution");
                  results.logTab(2,PROCESS,"Best Solution: Top LLG = "+ dtos(best_llg));
                  results.logTab(2,PROCESS,"Best Solution: Number of components = "+ itos(best_c+1));
                  results.logTab(2,PROCESS,"Best Solution: List size: " + itos(best_sol.size()));
                  results.logBlank(SUMMARY);
                  results.logTrailer(LOGFILE);
                } //save best
              }
            } //rnp, result in scope
          } //sg
        } //frf
      } //auto search
    } //permutations
    while (success &&
           inputAUTO.DO_PERMUTE &&
           std::next_permutation(AUTO_SEARCH.begin(),AUTO_SEARCH.end()));

    if (success && best_sol.size())
    {
      floatType reso_tol(1.0e-02);
      DataMR mr(inputAUTO.REFLECTIONS,
                inputAUTO.RESHARP,
                inputAUTO.SIGMAN,
                inputAUTO.OUTLIER,
                inputAUTO.PTNCS,
                inputAUTO.DATABINS,
                inputAUTO.HIRES,inputAUTO.LORES,
                inputAUTO.COMPOSITION,
                inputAUTO.ensemble);
      floatType HIRES = mr.fullHiRes(); //required for when the first molecule is set - first RNP is not called
      //make sure not refining to a lower resolution with new protocols (VRMS)
      floatType rnp_hires = best_sol.get_resolution(); //can be altered if refinement runs out of memory
      if (inputAUTO.DO_AUTO_HIRES)
        rnp_hires = (inputAUTO.AUTO_HIRES>=0) ? std::max(inputAUTO.AUTO_HIRES,HIRES) : HIRES;
      best_sol.set_resolution(-999); //don't use the ellg resolution at the end!
      results.logHeader(LOGFILE,header(MR_AUTO));
      (inputAUTO.DO_AUTO_HIRES) ?
      results.logTab(1,PROCESS,"Solutions will be refined to highest resolution"):
      results.logTab(1,PROCESS,"Solutions will be refined at solution resolution");
      results.logTab(2,PROCESS,"Resolution for refinement: " + dtos(rnp_hires));
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
      start_final_rnp_at_hires:
      if (best_sol.size())
        //(MTZ_HIRES < HIRES && rnp_hires+reso_tol < HIRES) // no change in protocol, can use this check
      {
        try {
        std::string cards = inputAUTO.Cards();
        InputMR_RNP input(cards);
        input.setREFL_DATA(inputAUTO.REFLECTIONS);
        input.setRESO(rnp_hires,inputAUTO.LORES);
        input.setSOLU(best_sol);
        input.setNORM_DATA(inputAUTO.SIGMAN);
        input.setTNCS(inputAUTO.PTNCS);
        success = false;
        input.setZSCO_USE(input.ZSCORE.USE && (inputAUTO.AUTO_SEARCH.size() != best_sol[0].KNOWN.size()));
        input.PDB = inputAUTO.PDB; //unparse does not work
        results = runMR_RNP(input,results);
        if (results.ErrorType() == MEMORY) throw results;
        success = results.Success();
        }
        catch (PhaserError& err) {
          if (err.ErrorType() != MEMORY) throw;
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logHeader(LOGFILE,header(MR_AUTO));
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires;
          }
        }
        catch (phaser::error const& err) { //PHASER_ASSERT
          throw;
        }
        catch (std::bad_alloc const& err) {
          results.logHeader(LOGFILE,header(MR_AUTO));
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires;
          }
        }
        catch (std::exception const& err) {
          results.logHeader(LOGFILE,header(MR_AUTO));
          if (rnp_hires+reso_tol < HIRES)
          {
          results.logTab(1,PROCESS,"Error in high resolution refinement (" + dtos(rnp_hires) + "A)");
          results.logTab(1,PROCESS,"Solutions will be refined at lower resolution (" + dtos(HIRES) + "A)");
          rnp_hires = HIRES;
          results.logBlank(SUMMARY);
          results.logTrailer(LOGFILE);
          goto start_final_rnp_at_hires;
          }
        }
        catch (...) {
          throw;
        }
      }
      else
      {
        std::string cards = inputAUTO.Cards();
        InputMR_RNP input(cards);
        input.setMACM_PROT("OFF");
        input.setREFL_DATA(inputAUTO.REFLECTIONS);
        input.setSOLU(best_sol);
        input.setNORM_DATA(inputAUTO.SIGMAN);
        input.setTNCS(inputAUTO.PTNCS);
        ResultMR resultLLG = runMR_RNP(input,results);
        results = resultLLG;
      }
      results.logHeader(LOGFILE,header(MR_AUTO));
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:MR Result: $$ Baubles Markup $$");
      results.logBlank(SUMMARY);
      if (results.uniqueSolution())
      {
        results.phenixCallback("result","Single Solution");
        results.logTab(1,PROCESS,"SINGLE solution");
        results.logBlank(SUMMARY);
        if (results.getNumPdbFiles())
          results.logTab(1,PROCESS,"Solution written to PDB file:  " + results.getTopPdbFile());
        if (results.getNumMtzFiles())
          results.logTab(1,PROCESS,"Solution written to MTZ file:  " + results.getTopMtzFile());
        results.logTab(1,SUMMARY,"Solution annotation (history):");
        results.logTab(0,SUMMARY,results.getTopSet().logfile(1,true),false);
      }
      else if (success && results.numSolutions() > 0)
      {
        results.phenixCallback("result",itos(results.numSolutions()) + " solutions");
        results.logTab(1,SUMMARY,"There were " + itos(results.numSolutions()) + " solutions");
        results.logTab(1,SUMMARY,"Pdb and Mtz files have been written with results for " + itos(results.getNumTop()) + " of these solutions");
        results.logBlank(SUMMARY);
        for (int f = 0; f < results.getNumTop(); f++)
        {
          if (results.getNumPdbFiles() > f)
            results.logTab(1,SUMMARY,"Solution #" + itos(f+1) + " written to PDB file:  " + results.getPdbFile(f));
          if (results.getNumMtzFiles() > f)
            results.logTab(1,SUMMARY,"Solution #" + itos(f+1) + " written to MTZ file:  " + results.getMtzFile(f));
          results.logTab(1,SUMMARY,"Solution #" + itos(f+1) + " annotation (history):");
          results.logTab(0,SUMMARY,results.getSet(f).logfile(1,true),false);
          results.logBlank(SUMMARY);
        }
        for (int f = results.getNumTop(); f < results.numSolutions(); f++)
        {
          results.logTab(1,SUMMARY,"Solution #" + itos(f+1) + " annotation (history): ");
          results.logTab(0,SUMMARY,results.getSet(f).logfile(1,true,true),false);
        }
      }
      else if (success && results.numSolutions() == 0)
      {
        results.phenixCallback("result","No Solutions");
        results.logHeader(LOGFILE,header(MR_AUTO));
        if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:MR Result: $$ Baubles Markup $$");
        results.logBlank(PROCESS);
        results.logTab(1,PROCESS,"Sorry - No solutions");
        results.logTab(1,PROCESS,"You may find a solution with a different search or selection strategy");
        results.logBlank(SUMMARY);
      }
      results.logBlank(SUMMARY);
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
    }
    else if (success && !best_sol.size())
    {
      results.phenixCallback("result","Partial Solutions");
      results.logHeader(LOGFILE,header(MR_AUTO));
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:MR Result: $$ Baubles Markup $$");
      results.logBlank(SUMMARY);
      results.logTab(1,PROCESS,"Sorry - No solution with all components");
      results.logTab(1,PROCESS,"You may find a solution with a different search or selection strategy");
      results.logBlank(SUMMARY);
      if (best_partial)
      {
        results = best_result;
        if (results.uniqueSolution())
          results.logTab(1,SUMMARY,"There was " + itos(results.numSolutions()) + " partial solution");
        else
          results.logTab(1,SUMMARY,"There were " + itos(results.numSolutions()) + " partial solutions");
        results.logTab(1,SUMMARY,"Pdb and Mtz files have been written with results for " + itos(results.getNumTop()) + " of these solutions");
        results.logBlank(SUMMARY);
        for (int f = 0; f < results.getNumTop(); f++)
        {
          if (results.getNumPdbFiles() > f)
            results.logTab(1,SUMMARY,"Partial Solution #" + itos(f+1) + " written to PDB file:  " + results.getPdbFile(f));
          if (results.getNumMtzFiles() > f)
            results.logTab(1,SUMMARY,"Partial Solution #" + itos(f+1) + " written to MTZ file:  " + results.getMtzFile(f));
          results.logTab(1,SUMMARY,"Partial Solution #" + itos(f+1) + " annotation (history):");
          results.logTab(0,SUMMARY,results.getSet(f).logfile(1,true),false);
          results.logBlank(SUMMARY);
        }
        for (int f = results.getNumTop(); f < results.numSolutions(); f++)
        {
          results.logTab(1,SUMMARY,"Partial Solution #" + itos(f+1) + " annotation (history):");
          results.logTab(0,SUMMARY,results.getSet(f).logfile(1,true,true),false);
        }
        results.logBlank(SUMMARY);
      }
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
    }
    if (success)
    {
      results.logTrailer(LOGFILE);
    }
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_FULL(InputMR_AUTO& inputAUTO)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_FULL(inputAUTO,output);
}

} //end namespace phaser
