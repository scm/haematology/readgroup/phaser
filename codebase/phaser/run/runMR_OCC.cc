//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/RefineOCC.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/Molecule.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/mean.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/DataMR.h>
#include <phaser/src/MapEnsemble.h>
#include <phaser/src/MapTraceMol.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>
#include <phaser/src/ListEditing.h>
#include <phaser/src/Dfactor.h>
#include <phaser/lib/eLLG.h>
#include <scitbx/random.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_OCC(InputMR_OCC& input,Output output)
{
  ResultMR results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_OCC));
    input.Analyse(results);

    if (!input.MRSET.size() || !input.MRSET[0].KNOWN.size())
    {
      results.logSectionHeader(LOGFILE,"OCCUPANCY REFINEMENT");
      results.logTab(1,LOGFILE,"No solutions");
      results.logBlank(LOGFILE);
    }
    else
    {
      Composition composition(input.COMPOSITION);
      composition.set_modlid_scattering(input.PDB);
      composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS);
      if (composition.increased())
        results.logWarning(SUMMARY,composition.warning());
      double TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);
      cctbx::uctbx::unit_cell cctbxUC(input.UNIT_CELL);
      std::vector<ssqr_dfac_solt> ssqr_dfac;
      { //memory
      af_float ssqr = cctbxUC.d_star_sq(input.REFLECTIONS.MILLER.const_ref());
      PHASER_ASSERT(ssqr.size() == input.REFLECTIONS.DFAC.size());
      for (int r = 0; r < ssqr.size(); r++)
        ssqr_dfac.push_back(ssqr_dfac_solt(ssqr[r],input.REFLECTIONS.DFAC[r]*solTerm(ssqr[r],input.SOLPAR)));
      }
      std::sort(ssqr_dfac.begin(),ssqr_dfac.end());
      input.COMPOSITION = composition;
      if (composition.increased())
      {
        std::string cards = input.Cards();
        InputCCA inputCCA(cards);
        floatType fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
        inputCCA.setRESO_HIGH(fullhires);
        inputCCA.setMUTE(true);
        ResultCCA resultCCA = runCCA(inputCCA,results);
        if (resultCCA.Failure()) //cheap
          results += resultCCA;
        //throw same error as would be thrown by runCCA
        if (resultCCA.getFitError())
        throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
      }

      if (!input.SIGMAN.REFINED)
      { //memory
        results.logSectionHeader(phaser::LOGFILE,"ANISOTROPY CORRECTION");
        RefineANO refa(input.REFLECTIONS,
                       input.RESHARP,
                       input.SIGMAN,
                       input.OUTLIER,
                       input.PTNCS,
                       input.DATABINS,
                       input.COMPOSITION);
        Minimizer minimizer;
        minimizer.run(refa,input.MACRO_ANO,results);
        input.setNORM_DATA(refa.getSigmaN());
        input.OUTLIER = refa.getOutlier();
      }

      if (input.PTNCS.USE)
      { //memory
        Epsilon epsilon(input.REFLECTIONS,
                        input.RESHARP,
                        input.SIGMAN,
                        input.OUTLIER,
                        input.PTNCS,
                        input.DATABINS,
                        input.COMPOSITION
                        );
        epsilon.findTRA(results);
        if (epsilon.getPtNcs().TRA.VECTOR_SET)
          epsilon.findROT(input.MACRO_TNCS,results);
        input.PTNCS = epsilon.getPtNcs();
        input.OUTLIER = epsilon.getOutlier();
      }

      if (!input.REFLECTIONS.Feff.size())
      { //memory
      results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
      Dfactor dfactor(input.REFLECTIONS,
                      input.RESHARP,
                      input.SIGMAN,
                      input.OUTLIER,
                      input.PTNCS,
                      input.DATABINS,
                      input.COMPOSITION
                      );
      dfactor.calcDfactor(results);
      input.REFLECTIONS = dfactor;
      }

      //even if there are not MRSETS, set up the result object for return
      DataMR mr(input.REFLECTIONS,
                input.RESHARP,
                input.SIGMAN,
                input.OUTLIER,
                input.PTNCS,
                input.DATABINS,
                input.HIRES,input.LORES,
                input.COMPOSITION,
                input.ensemble);
      SigmaN sigmaN = mr.getBinSigmaN();
      bool halfR(false); // Even if rotations not refined before, refining them now
      mr.initGfunction(halfR);
      results.setDataA(mr);

      results.logSectionHeader(LOGFILE,"DATA FOR REFINEMENT AND PHASING");
      input.MRSET.if_not_set_apply_space_group(input.SG_HALL);
      stringset modlids = input.MRSET.set_of_modlids();
      double data_HIRES = mr.HiRes();
      double sol_input_HIRES = input.MRSET.get_resolution();
      if (input.HIRES < 0 && (sol_input_HIRES > mr.HiRes()))
      {
        results.logTab(1,LOGFILE,"High resolution limit imposed by solution list = " + dtos(sol_input_HIRES,5,2));
        results.logBlank(LOGFILE);
        mr.selectReso(sol_input_HIRES,input.LORES);
      }
      double HIRES = std::max(data_HIRES,sol_input_HIRES);
      HIRES = std::max(HIRES,input.HIRES); //cannot be avoided
      mr.selectReso(HIRES,input.LORES);
      mr.logOutliers(LOGFILE,results);
      mr.logDataStats(SUMMARY,results);
      mr.logSymm(DEBUG,results);
      mr.logUnitCell(DEBUG,results);
      mr.logDataStatsExtra(DEBUG,20,results);

      results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
      double LLwilson = mr.WilsonLL();
      mr.logWilson(LOGFILE,LLwilson,results);
      mr.setLLwilson(LLwilson);

      map_str_pdb occPDB;
      if (input.MRSET.size())
      {
        outStream where(LOGFILE);
        results.logSectionHeader(where,"ENSEMBLING");
        for (int k = 0; k < input.MRSET.size(); k++)
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          std::string MODLID = input.MRSET[k].KNOWN[s].MODLID + brackets(s);
          occPDB[MODLID] = input.PDB.find(input.MRSET[k].KNOWN[s].MODLID)->second;
        }
        //need to convert both the mr_sets and the occPDB
        for (pdbIter iter = occPDB.begin(); iter != occPDB.end(); iter++)
          iter->second.setup_and_fix_vrms(iter->first,HIRES,input.PTNCS,input.SOLPAR,results);
        for (pdbIter iter = occPDB.begin(); iter != occPDB.end(); iter++)
          iter->second.trace_only();
        input.MRSET.convert_vrms_cell_to_modlids_with_numbers();
        results.init("LLG",input.TITLE,input.NUM_TOPFILES,occPDB);

        mr.ensPtr()->configure(input.MRSET.set_of_modlids(),occPDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[0].set_of_modlids(),results);
        mr.ensPtr()->ensembling_table(LOGFILE,TOTAL_SCAT,results);
        { //ready for output
          if (input.tracemol == NULL)
          {
            MapTrcPtr new_ensPtr(new MapTraceMol());
            input.tracemol = new_ensPtr; //deep copy on init
          }
          input.tracemol->configure(input.MRSET.set_of_modlids(),true,false,occPDB,results);
          input.tracemol->trace_table(LOGFILE,results);
        }
      }

      int nthreads = 1;
#pragma omp parallel
      {
#ifdef _OPENMP
        nthreads = omp_get_num_threads();
#endif
      }
//end #pragma omp parallel

      bool1D occupancy_refinement_did_not_improve_llg(input.MRSET.size(),false);
    //  bool   at_least_one_occupancy_refinement_did_not_improve_llg(false);
      for (int k = 0; k < input.MRSET.size(); k++)
      {
        input.MRSET[k].ORIG_NUM = k+1;
        input.MRSET[k].NUM = k+1;
        results.logSectionHeader(LOGFILE,"OCCUPANCY REFINEMENT SET #" + itos(k+1) + " OF " + itos(input.MRSET.size()));
        mr.changeSpaceGroup(input.MRSET[k].HALL,HIRES,input.LORES);
        input.MRSET.setup_newvrms(mr.ensPtr()->get_drms_vrms());
        if (!input.MRSET[k].LLG) //not from fast mode
        {
          //with interpolated structure factors
          mr.ensPtr()->configure(input.MRSET[k].set_of_modlids(),occPDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[k].set_of_modlids(),results);
          mr.init();
          mr.initKnownMR(input.MRSET[k]);
          mr.initSearchMR();
          mr.calcKnownVAR();
          mr.calcKnown();
          input.MRSET[k].LLG = input.MRSET[k].ORIG_LLG = mr.likelihoodFn();
          input.MRSET[k].R = input.MRSET[k].ORIG_R = mr.Rfactor();
          results.logTab(1,LOGFILE,"Initial Log-likelihood gain = " + dtos(input.MRSET[k].LLG));
          results.logBlank(LOGFILE);
        }
        results.logTab(0,SUMMARY,input.MRSET[k].logfile(1,true,true),false);
        bool has_maps(false),has_atoms(false),has_nucleic(false);
        for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
        {
          std::string modlid = input.MRSET[k].KNOWN[s].MODLID;
          if (occPDB[modlid].ENSEMBLE[0].map_format()) has_maps = true;
          if (occPDB[modlid].is_atom) has_atoms = true;
          if (occPDB[modlid].MW.NUCLEIC) has_nucleic = true;
        }
        if (has_maps)
        {
          results.logTab(1,LOGFILE,"Solution includes maps");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (has_atoms)
        {
          results.logTab(1,LOGFILE,"Solution includes single atoms");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (has_nucleic)
        {
          results.logTab(1,LOGFILE,"Solution includes nucleic acids");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (!input.OCCUPANCY.refine_all() && input.MRSET[k].PACKS)
        {
          results.logTab(1,LOGFILE,"Solution packs");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (input.MRSET[k].has_mult())
        {
          results.logTab(1,LOGFILE,"Solution has internal symmetry on crystal symmetry");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (input.PTNCS.use_and_present())
        {
          results.logTab(1,LOGFILE,"tNCS present");
          occupancy_refinement_did_not_improve_llg[k] = true;
        }
        if (occupancy_refinement_did_not_improve_llg[k])
        {
          results.logTab(1,LOGFILE,"No occupancy refinement performed");
          results.logBlank(LOGFILE);
          continue;
        }

        //if ( input.OCCUPANCY.refine_all() ||
        //   (!input.OCCUPANCY.refine_all() && !input.MRSET[k].PACKS))
        {
          results.logUnderLine(LOGFILE,"Residue Window for Occupancy Refinement");
          double FP(0),MW(0),MAXRMSD(0);//almost perfect
          //get FP without calculating ensembles
          float1D  KRMSD(input.MRSET[k].KNOWN.size());
          float1D  KFP(input.MRSET[k].KNOWN.size());
          for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
          {
            std::string MODLID = input.MRSET[k].KNOWN[s].MODLID;
            MW += occPDB[MODLID].MW.PROTEIN;
            KFP[s] = occPDB[MODLID].SCATTERING/TOTAL_SCAT;
            bool hasVRMS(input.MRSET[k].DRMS.find(MODLID) != input.MRSET[k].DRMS.end());
            float1D vrms = hasVRMS ? newvrms(input.MRSET[k].DRMS[MODLID],occPDB[MODLID].DV.fixed_vrms_array()) : occPDB[MODLID].DV.fixed_vrms_array();
            PHASER_ASSERT(vrms.size() == 1);
            KRMSD[s] = max(vrms);
            FP += KFP[s];
            MAXRMSD += KFP[s]*KRMSD[s];
          }
          PHASER_ASSERT(FP > 0);
          MAXRMSD /= FP;

          double eLLG = get_eLLG(ssqr_dfac,MAXRMSD,FP);
          results.logTab(1,LOGFILE,"eLLG target for residue window selection: " + dtos(input.OCCUPANCY.WINDOW_ELLG));
          results.logTab(1,DEBUG,"eLLG for all data with 100% complete model: " + dtos(eLLG));
          results.logTab(2,LOGFILE,"Weighted RMSD of fragments = " + dtos(MAXRMSD,5,2));
          results.logTab(1,DEBUG,"Percent of scattering for known components : " + dtos(100*FP,5,2));
          results.logTab(1,DEBUG,"Molecular weight for known components: " + itos(MW));
          double average_Da_per_amino_acid(110);
          int WINDOW_NRES = MW/average_Da_per_amino_acid;
          int nres(1);
          double known_eLLG = get_eLLG(ssqr_dfac,0,0,KRMSD,KFP);
          double target_eLLG = known_eLLG-input.OCCUPANCY.WINDOW_ELLG;
          for (nres = 1; nres < WINDOW_NRES-1; nres++) //max limit avoids fp < 0
          {
            double fp(FP-nres*average_Da_per_amino_acid/MW);
            if (fp < 0) { nres--; break; }
            double eLLG_chk = get_eLLG(ssqr_dfac,MAXRMSD,fp);
            if (eLLG_chk < target_eLLG) break;
          }
          //nres can be zero if original FP is very small
          results.logTab(1,LOGFILE,"Number of residues to achieve eLLG target: " + itos(nres));
          if (!input.OCCUPANCY.WINDOW_NRES) input.OCCUPANCY.WINDOW_NRES = nres;
          else
            results.logTab(1,LOGFILE,"Number of residues in occupancy window input: " + itos(input.OCCUPANCY.WINDOW_NRES));
          double fp(FP-input.OCCUPANCY.WINDOW_NRES*average_Da_per_amino_acid/MW);
          if (fp >= 0)
          {
            double eLLG_chk = get_eLLG(ssqr_dfac,MAXRMSD,fp);
            input.OCCUPANCY.WINDOW_ELLG = (known_eLLG-eLLG_chk);
            results.set_occupancy_window_ellg(known_eLLG-eLLG_chk);
          }
          results.logTab(2,LOGFILE,"Window eLLG: " + dtos(input.OCCUPANCY.WINDOW_ELLG,2));
         // double restraintFP(FP-nres*average_Da_per_amino_acid/MW);
          results.logTab(3,DEBUG,"Check eLLG for 100% modelled:   " +
               dtos(get_eLLG(ssqr_dfac,MAXRMSD,fp)));
          results.logTab(3,DEBUG,"Check eLLG for modelled:        " +
               dtos(get_eLLG(ssqr_dfac,0,0,KRMSD,KFP),2));
          if (input.OCCUPANCY.WINDOW_NRES) //otherwise restraintFP < 0, nan
          results.logTab(3,DEBUG,"Check eLLG for modelled-window: " +
               dtos(get_eLLG(ssqr_dfac,MAXRMSD,fp),2));
          results.logBlank(LOGFILE);
          results.set_occupancy_nresidues(input.OCCUPANCY.WINDOW_NRES);

          if (!input.OCCUPANCY.WINDOW_NRES || input.OCCUPANCY.WINDOW_NRES > input.OCCUPANCY.WINDOW_MAX)
          {
            if (!input.OCCUPANCY.WINDOW_NRES)
            {
              results.logTab(1,SUMMARY,"LLG of whole fragment very low");
              results.logTab(1,SUMMARY,"No residues in occupancy window");
              occupancy_refinement_did_not_improve_llg[k] = true;
              continue;
            }
            else
            {
              results.logTab(1,SUMMARY,"Number of residues in occupancy window = " + itos(input.OCCUPANCY.WINDOW_NRES));
              results.logTab(1,SUMMARY,"Number of residues in occupancy window exceeded maximum (" + itos(input.OCCUPANCY.WINDOW_MAX) + ")");
              results.logTab(1,SUMMARY,"Window does not represent local structure; no occupancy refinement performed");
              results.logBlank(SUMMARY);
              occupancy_refinement_did_not_improve_llg[k] = true;
              continue;
            }
          }

          { //memory
          results.logUnderLine(LOGFILE,"Fractional Occupancy Refinement");
          RefineOCC refocc(input.OCCUPANCY,input.SOLPAR,input.FORMFACTOR,&occPDB,mr,results);
          refocc.initKnown(TOTAL_SCAT,input.MRSET[k]);
          refocc.logInit(LOGFILE,results); //after init, for fft vs sum
          if (input.OCCUPANCY.WINDOW_NRES == 2) input.OCCUPANCY.MERGE = false;
          int IOFFSET = input.OCCUPANCY.WINDOW_NRES%2 ? 1 : 2;
          int NOFFSET = input.OCCUPANCY.MERGE ? input.OCCUPANCY.WINDOW_NRES : 1;
          float3D refined_occupancies(NOFFSET);
          float1D LLG_head_occref(NOFFSET);
          float1D LLG_tail_occref(NOFFSET);
          float1D nwindows(NOFFSET);
          std::vector<Output> omp_results;
          omp_results.resize(nthreads);
          for(int n = 0; n < nthreads; ++n)
          {
            // concurrent threads must not write to stdout
            omp_results[n].run_time = results.run_time;
            omp_results[n].setLevel((nthreads == 1 && results.level(DEBUG)) ? input.OUTPUT_LEVEL : LOGFILE);
            omp_results[n].setMute(!(nthreads == 1 && results.level(DEBUG)));
          }
          nthreads = std::min(nthreads,(IOFFSET==1 ? NOFFSET : NOFFSET/2));
          if (nthreads < 1) nthreads = 1;
          PHASER_ASSERT(NOFFSET);
          (NOFFSET == 1) ?
            results.logTab(1,LOGFILE,"There is 1 occupancy offset"):
            results.logTab(1,LOGFILE,"There are " + itos(NOFFSET%2?NOFFSET:NOFFSET/2) + " occupancy offsets");
          if (nthreads > 1)
            results.logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
          results.logProgressBarStart(LOGFILE,"Refining occupancy offsets",NOFFSET/nthreads);

#if _OPENMP
          map_str_pdb omp_PDB = occPDB;
#else
          map_str_pdb& omp_PDB = occPDB;
#endif
          bool minimizer_warning(false);

#pragma omp parallel for firstprivate(omp_PDB)
          for (int window = 0; window < NOFFSET; window+=IOFFSET)
          {
            int RESOFFSET = window;
            if (!input.OCCUPANCY.MERGE) //choose the offset
              RESOFFSET += std::min(input.OCCUPANCY.OFFSET,NOFFSET-1);
            int nthread = 0;
#ifdef _OPENMP
            nthread = omp_get_thread_num();
#endif
            omp_results[nthread].logUnderLine(LOGFILE,"Offset " + itos(RESOFFSET));
            RefineOCC omp_refocc(input.OCCUPANCY,input.SOLPAR,input.FORMFACTOR,&omp_PDB,mr,omp_results[nthread]);
            omp_refocc.initKnown(TOTAL_SCAT,input.MRSET[k]);
            omp_refocc.setup_constraints(RESOFFSET);
            LLG_head_occref[RESOFFSET] = omp_refocc.likelihoodFn();
            Minimizer minimizer;
            try {
            minimizer.run(omp_refocc,input.MACRO_OCC,omp_results[nthread]);
            } catch (...) {
            occupancy_refinement_did_not_improve_llg[k] = true;
            minimizer_warning = true;
            }
            refined_occupancies[RESOFFSET] = omp_refocc.getOcc();
            LLG_tail_occref[RESOFFSET] = omp_refocc.likelihoodFn();
            nwindows[RESOFFSET] = omp_refocc.number_of_windows();
            if (nthread == 0) results.logProgressBarNext(LOGFILE);
          } //end #pragma omp parallel for
          results.logProgressBarEnd(LOGFILE);
          results.set_occupancy_nwindows(mean(nwindows));

          for (int nthread = 0; nthread < nthreads; nthread++)
          {
            results += omp_results[nthread];
            if (!(nthreads == 1 && results.level(DEBUG))) results << omp_results[nthread];
          }
          //append and optionally print silenced results from the various threads to the master thread

          if (minimizer_warning)
          results.logAdvisory(LOGFILE,"Miminizer for pruning ran out of memory");

          {//print refinement table
          results.logSectionHeader(LOGFILE,"NON-BINARY OCCUPANCIES");
          results.logTabPrintf(1,LOGFILE,"%6s %6s %5s %-11s %-20s\n","Window","Offset","Width","Initial-LLG","Refined-LLG (change)");
          int count(0);
          for (int window = 0; window < NOFFSET; window+=IOFFSET)
          {
            count++;
            int RESOFFSET = window;
            if (!input.OCCUPANCY.MERGE) //choose the offset
              RESOFFSET += std::min(input.OCCUPANCY.OFFSET,NOFFSET-1);
            results.logTabPrintf(1,LOGFILE,"#%-5i %6i %5i %11.1f %11.1f (%6.1f)\n",
              count,
              RESOFFSET,
              input.OCCUPANCY.WINDOW_NRES,
              LLG_head_occref[RESOFFSET],
              LLG_tail_occref[RESOFFSET],
              LLG_tail_occref[RESOFFSET]-LLG_head_occref[RESOFFSET]);
          }
          results.logBlank(LOGFILE);
          }

          if (input.OCCUPANCY.MAX != 1)
          { //must be done, below assumes range [0,1]
            results.logTab(1,VERBOSE,"Unbounded refined occupancies will be bounded in range [0-1] by division by maximum occupancy");
            results.logEllipsisStart(VERBOSE,"Bounding occupancies");
            float1D LLG_bounded(NOFFSET,0);
            float1D max_occ(NOFFSET,0);
            for (int RESOFFSET = 0; RESOFFSET < refined_occupancies.size(); RESOFFSET+=IOFFSET)
            {
              for (int s = 0; s < refined_occupancies[RESOFFSET].size(); s++)
                for (int a = 0; a < refined_occupancies[RESOFFSET][s].size(); a++)
                  max_occ[RESOFFSET] = std::max(max_occ[RESOFFSET],refined_occupancies[RESOFFSET][s][a]);
              results.logTab(1,VERBOSE,"Offset #" + itos(RESOFFSET) + " max occupancy = " + dtos(max_occ[RESOFFSET]));
              if (max_occ[RESOFFSET])
                for (int s = 0; s < refined_occupancies[RESOFFSET].size(); s++)
                  for (int a = 0; a < refined_occupancies[RESOFFSET][s].size(); a++)
                    refined_occupancies[RESOFFSET][s][a] /= max_occ[RESOFFSET];
              //bool calcSigmaP(false); //don't use - current refocc is not anything sensible, before minimizers are run
              bool calcSigmaP(true); //if not converged on changing SigmaP, this will change LLG
              refocc.setOcc(refined_occupancies[RESOFFSET],calcSigmaP);
             //!!! AJM BUG scattering not reset
              LLG_bounded[RESOFFSET] = refocc.likelihoodFn();
            }
            results.logEllipsisEnd(VERBOSE);
            results.logBlank(VERBOSE);
            results.logTabPrintf(1,VERBOSE,"%6s %6s %5s %6s %-11s %-20s\n","Window","Offset","Width","MaxOcc","Refined-LLG","Bounded-LLG (change)");
            int count(0);
            for (int window = 0; window < NOFFSET; window+=IOFFSET)
            {
              count++;
              int RESOFFSET = window;
              if (!input.OCCUPANCY.MERGE) //choose the offset
                RESOFFSET += std::min(input.OCCUPANCY.OFFSET,NOFFSET-1);
              results.logTabPrintf(1,VERBOSE,"#%-5i %6i %5i %6.3f %11.1f %11.1f (%6.1f)\n",
              count,
              RESOFFSET,
              input.OCCUPANCY.WINDOW_NRES,
              max_occ[RESOFFSET],
              LLG_tail_occref[RESOFFSET],
              LLG_bounded[RESOFFSET],
              LLG_bounded[RESOFFSET]-LLG_tail_occref[RESOFFSET]);
            }
            results.logBlank(VERBOSE);
          }

          double LLG_merged(0);
          if (input.OCCUPANCY.MERGE && NOFFSET > 1)
          {
            results.logTab(1,VERBOSE,"Combining Results of Offsets 0->" + itos(NOFFSET-1));
            results.logEllipsisStart(VERBOSE,"Finding centroid occupancies");
          //  bool calcSigmaP(false);
            bool calcSigmaP(true);
            refocc.mergeOcc(refined_occupancies,IOFFSET,calcSigmaP);
            LLG_merged = refocc.likelihoodFn();
            results.logEllipsisEnd(VERBOSE);
            results.logBlank(VERBOSE);
            results.logTabPrintf(1,VERBOSE,"%6s %6s %5s %-11s %-20s\n","Window","Offset","Width","Refined-LLG","Merged-LLG (change)");
            int count(0);
            for (int window = 0; window < NOFFSET; window+=IOFFSET)
            {
              count++;
              int RESOFFSET = window;
              results.logTabPrintf(1,VERBOSE,"#%-5i %6i %5i %11.1f %11.1f (%6.1f)\n",
                count,
                RESOFFSET,
                input.OCCUPANCY.WINDOW_NRES,
                LLG_tail_occref[RESOFFSET],
                LLG_merged-LLG_tail_occref[RESOFFSET]);
            }
            results.logBlank(VERBOSE);
            results.logTab(1,VERBOSE,"Merge LLG = " + dtos(LLG_merged) + " only");
            results.logBlank(VERBOSE);
          }
          else
          {
            int RESOFFSET = std::min(input.OCCUPANCY.OFFSET,NOFFSET-1);
            results.logTab(1,VERBOSE,"Results of Offset " + itos(RESOFFSET) + " only");
            bool calcSigmaP(true);
            refocc.setOcc(refined_occupancies[RESOFFSET],calcSigmaP);
            LLG_merged = refocc.likelihoodFn();
            results.logTab(1,VERBOSE,"Merge LLG = " + dtos(LLG_merged));
            results.logBlank(VERBOSE);
          }

          results.logUnderLine(LOGFILE,"Occupancy Statistics");
          refocc.logStats(LOGFILE,results);
          results.logTab(1,LOGFILE,"Histogram of Occupancies");
          refocc.logHistogram(LOGFILE,results);
          input.MRSET[k].REALOCC = refocc.getOcc();

          if (results.level(DEBUG))
          {
            //now report and store the values using the INTERPOLATED structure factors
            //these will differ from the values obtained by direct calculation
            //results.setLevel(SILENCE);
            results.logSectionHeader(DEBUG,"CHECK LLG");
            results.logTab(1,DEBUG,"Start LLG with Interpolated Structure Factors:");
            for (int i = 0; i < LLG_head_occref.size(); i++)
              results.logTab(1,DEBUG,"LLG all scattering via sf calculation = " + dtos(LLG_head_occref[i]) + " (" + itos(i+1) + ")" );
            results.logTab(1,DEBUG,"LLG all scattering via interpolation = " + dtos(input.MRSET[k].ORIG_LLG));
            //float2D[s] converted to map with [s] on modlid
            map_str_float1D thisOCC;
            for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            {
              thisOCC[input.MRSET[k].KNOWN[s].MODLID].resize(input.MRSET[k].REALOCC[s].size());
              for (int a = 0; a < input.MRSET[k].REALOCC[s].size(); a++)
                thisOCC[input.MRSET[k].KNOWN[s].MODLID][a] = input.MRSET[k].REALOCC[s][a];
            }
            results.setLevel(SILENCE);
            mr.ensPtr()->configure(input.MRSET[k].set_of_modlids(),occPDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[k].set_of_modlids(),results,false,0,&thisOCC);
            results.setLevel(input.OUTPUT_LEVEL);
            mr.init();
            mr.initKnownMR(input.MRSET[k]);
            mr.initSearchMR();
            mr.calcKnownVAR();
            mr.calcKnown();
            results.logTab(1,DEBUG,"Refined-LLG sf calculation reduced scattering = " + dtos(LLG_merged));
            results.logTab(1,DEBUG,"Refined-LLG interpolation reduced scattering = " + dtos(mr.likelihoodFn()));
            results.logBlank(DEBUG);
          }

          }

          results.logSectionHeader(LOGFILE,"BINARY OCCUPANCIES");
          results.logTab(1,LOGFILE,"Below the occupancy threshold all occupancies will be set to 0, and above the occupancy threshold all occupancies will be set to 1");
          results.logBlank(LOGFILE);
          std::set<int> set_of_occupancy_values;
          set_of_occupancy_values.insert(0); //for 100% scattering
          double occscale(1000),fracscale(50);
          results.logTab(1,VERBOSE,"Occupancy bins differ by more than " + dtos(1/occscale) + " in refined occupancies");
          results.logTab(1,VERBOSE,"Occupancy bins differ by more than " + dtos(1/fracscale) + " in fraction scattering");
          results.logBlank(VERBOSE);
          for (int s = 0; s < input.MRSET[k].REALOCC.size(); s++)
            for (int a = 0; a < input.MRSET[k].REALOCC[s].size(); a++)
              set_of_occupancy_values.insert(std::max(1,static_cast<int>(std::ceil(input.MRSET[k].REALOCC[s][a]*occscale))));
          float1D cutoff_divisions;
          for (std::set<int>::iterator iter = set_of_occupancy_values.begin(); iter!=set_of_occupancy_values.end(); iter++)
            cutoff_divisions.push_back(*iter/occscale);
          std::vector<std::pair<double,double> > occupancy_fracscat;
          int last_int_fracscat(0),total_n(0);
          for (int s = 0; s < input.MRSET[k].REALOCC.size(); s++)
            total_n += input.MRSET[k].REALOCC[s].size();
          for (int c = 0; c < cutoff_divisions.size(); c++)
          {
            double cutoff = cutoff_divisions[c];
            PHASER_ASSERT(cutoff <= 1);
            PHASER_ASSERT(cutoff >= 0);
            double n_over_cutoff(0);
            for (int s = 0; s < input.MRSET[k].REALOCC.size(); s++)
              for (int a = 0; a < input.MRSET[k].REALOCC[s].size(); a++)
                if (input.MRSET[k].REALOCC[s][a] >= cutoff) n_over_cutoff++;
            double fracscat = n_over_cutoff/total_n;
            int int_fracscat = fracscat*fracscale; //rounded to nearest percent
            if (last_int_fracscat != int_fracscat && fracscat > input.OCCUPANCY.FRACSCAT)
              occupancy_fracscat.push_back(std::pair<double,double>(cutoff,fracscat));
            last_int_fracscat = int_fracscat;
          }
          results.logTab(1,LOGFILE,"There are " + itos(occupancy_fracscat.size()) + " occupancy thresholds for LLG scoring");

          float1D occ_llg(occupancy_fracscat.size());
          float1D occ_rfac(occupancy_fracscat.size());

#if _OPENMP
          map_str_pdb omp_PDB = occPDB;
#else
          map_str_pdb& omp_PDB = occPDB;
#endif

#pragma omp parallel
          { //again because of reset
#ifdef _OPENMP
            nthreads = omp_get_num_threads();
#endif
          }
//end #pragma omp parallel
          if (nthreads > 1)
            results.logTab(1,LOGFILE,"Spreading calculation onto " +itos(nthreads)+ " threads.");
          results.logProgressBarStart(LOGFILE,"Scoring occupancy thresholds",occupancy_fracscat.size()/nthreads);

          PHASER_ASSERT(input.MRSET[k].REALOCC.size() == input.MRSET[k].KNOWN.size());
          for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            PHASER_ASSERT(input.MRSET[k].REALOCC[s].size());

#pragma omp parallel for firstprivate(mr,omp_PDB)
          for (int f = 0; f < occupancy_fracscat.size(); f++)
          {
            int nthread = 0;
#ifdef _OPENMP
            nthread = omp_get_thread_num();
#endif
            map_str_float1D thisOCC;
            for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
            {
              thisOCC[input.MRSET[k].KNOWN[s].MODLID].resize(input.MRSET[k].REALOCC[s].size());
              for (int a = 0; a < input.MRSET[k].REALOCC[s].size(); a++)
                thisOCC[input.MRSET[k].KNOWN[s].MODLID][a] = (input.MRSET[k].REALOCC[s][a] >= occupancy_fracscat[f].first?1:0); //not >=
            }
            DataMR ompmr(mr);
            Output output;
            output.setLevel(SILENCE);
            ompmr.ensPtr()->configure(input.MRSET[k].set_of_modlids(),omp_PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[k].set_of_modlids(),output,false,0,&thisOCC);
            ompmr.init();
            ompmr.initKnownMR(input.MRSET[k]);
            ompmr.initSearchMR();
            ompmr.calcKnownVAR();
            ompmr.calcKnown();
            occ_llg[f] = ompmr.likelihoodFn();
            occ_rfac[f] = ompmr.Rfactor();
            if (nthread == 0) results.logProgressBarNext(LOGFILE);
          }
          results.logProgressBarEnd(LOGFILE);

          double best_llg = -std::numeric_limits<double>::max();
          int best_a(0);
          for (int a = 0; a < occupancy_fracscat.size(); a++)
          {
            //so that slightly lower llgs with less scattering are taken as better
            //slightly lower means 0.1 TFZ units (sqrt llg)
            //first integers values would be 24 being accepted lower down over llg 25 higher up
            double tol = fn::pow2(input.OCCUPANCY.BIAS);
            if ((occ_llg[a] > 0 && best_llg > 0 && std::sqrt(occ_llg[a])-std::sqrt(best_llg) > -tol)
                || (!(occ_llg[a] > 0 && best_llg > 0) && occ_llg[a] > best_llg))
            {
              best_llg = occ_llg[a];
              best_a = a;
            }
            results.logTab(1,VERBOSE,
             "Occupancy threshold " + dtos(occupancy_fracscat[a].first,3) + ": LLG = " + dtos(occ_llg[a],2) + " Fraction Scattering = " + dtos(occupancy_fracscat[a].second,4));
          }
          //store best occupancies
          input.MRSET[k].LLG = occ_llg[best_a];
          input.MRSET[k].R = occ_rfac[best_a];
          input.MRSET[k].real_to_bool_occupancies(occupancy_fracscat[best_a].first);

          if (input.MRSET[k].LLG < input.MRSET[k].ORIG_LLG)
          {
            occupancy_refinement_did_not_improve_llg[k] = true;
            //at_least_one_occupancy_refinement_did_not_improve_llg = true;
            //This is possible because LLG used for occupancy refinement uses calculated structure factors
            input.MRSET[k].LLG = input.MRSET[k].ORIG_LLG;
            input.MRSET[k].R = input.MRSET[k].ORIG_R;
          }
          else
          {
            results.logTab(1,LOGFILE,"Occupancy refinement improved LLG");
            PHASER_ASSERT(input.MRSET[k].BOOLOCC.size() == input.MRSET[k].KNOWN.size());
          }
          results.logTabPrintf(1,LOGFILE,"%-19s   %-19s   %-3s   %-7s Best(*)\n",
            "Occupancy threshold","Fraction Scattering","LLG","R-value");
          input.MRSET[k].ANNOTATION += " OCC=" + dtos(input.MRSET[k].LLG,0);

          {//Occupancy Threshold table and loggraph data
          std::string loggraph;
          for (int a = 0; a < occ_llg.size(); a++)
          {
            results.logTabPrintf(1,LOGFILE,"%13.4f  %19.3f %14.2f %7.2f %c\n",
              occupancy_fracscat[a].first,occupancy_fracscat[a].second,occ_llg[a],occ_rfac[a],best_a==a?'*':' ');
            loggraph += dtos(occupancy_fracscat[a].second,6,4)+" "+dtos(occupancy_fracscat[a].first,6,3)+" " +dtos(occ_llg[a],5,2)+"\n";
          }
          results.logTab(1,LOGFILE,"Likelihood after binary occupancies determined: " + dtos(occ_llg[best_a],10,1));
          results.logBlank(LOGFILE);
          string1D graphs(2);
          graphs[0] = "Occupancy Fraction vs LLgain:AUTO:1,3";
          graphs[1] = "Occupancy Fraction vs Cutoff:AUTO:1,2";
          results.logGraph(LOGFILE,"Occupancy Threshold Solution #" +itos(k+1),graphs,"fraction-scattering occupancy-threshold LLG",loggraph);
          results.logBlank(LOGFILE);
          }
          { //Occupancy loggraph
          for (int s = 0; s < input.MRSET[k].KNOWN.size(); s++)
          {
            std::string MREF = input.MRSET[k].KNOWN[s].MODLID;
            Molecule Coords = occPDB[MREF].Coords();
            std::string last_chain;
            int last_resnum(0),nres(0);
            std::string loggraph;
            for (int a = 0; a < Coords.nAtoms(); a++)
            if (a == 0 || Coords.card(a).ResNum != last_resnum || Coords.card(a).Chain != last_chain)
            {
              loggraph += itos(++nres) + " ";
              loggraph +=
                 (input.MRSET[k].BOOLOCC.size() && (Coords.nAtoms() == input.MRSET[k].BOOLOCC[s].size())) ?
                 dtos(input.MRSET[k].BOOLOCC[s][a],2,1):
                 dtos(Coords.card(a).O,2,1);
              loggraph += " " + itos(Coords.card(a).ResNum);
              loggraph += "\n";
              last_resnum = Coords.card(a).ResNum;
              last_chain = Coords.card(a).Chain;
            }
            string1D graphs(2);
            graphs[0] = "Residue vs Occupancy (" + MREF + "):NOUGHT:1,2";
            graphs[1] = "Residue vs ResNum (" + MREF + "):NOUGHT:1,3";
            results.logGraph(LOGFILE,"Occupancy by Residue Solution#" +itos(k+1) + " for Component #" +itos(s+1),graphs,"Residue Occupancy Residue-Number-PDB",loggraph);
            results.logBlank(LOGFILE);
          }
          }
        }
      }
      for (int k = 0; k < input.MRSET.size(); k++)
      if (occupancy_refinement_did_not_improve_llg[k])
      {
        input.MRSET[k].BOOLOCC.clear();
        input.MRSET[k].REALOCC.clear();
          results.logAdvisory(SUMMARY,"Occupancy refinement #" + itos(k+1) + " of " + itos(input.MRSET.size()) +
              " didn't improve LLG");
      }

      input.MRSET.sort_LLG();

      results.logSectionHeader(SUMMARY,"RESULTS");
      for (int table = 0; table < 2; table++)
      {
        std::string type = table ? "(Sorted)" : "(Unsorted)";
        table ? input.MRSET.sort_LLG():input.MRSET.unsort();
        results.logUnderLine(SUMMARY,"Occupancy Refinement Table " + type);
        results.logTabPrintf(1,SUMMARY,"%-4s %-4s %24s %18s %-11s\n",
          "#in","#out","(Start LLG Rval TFZ==)","(Refined LLG Rval)","SpaceGroup");
        for (unsigned k = 0; k < input.MRSET.size(); k++)
        {
          results.logTabPrintf(1,SUMMARY,"%-4d %-4d%14s %4s %4s %14s %4s %-11s\n",
            input.MRSET[k].ORIG_NUM,
            input.MRSET[k].NUM,
            dtos(input.MRSET[k].ORIG_LLG,14,1).c_str(),
            input.MRSET[k].ORIG_R > 99 ? " >99" : dtos(input.MRSET[k].ORIG_R,4,1).c_str(),
            !input.MRSET[k].TFZeq  ? "n/a " : dtos(input.MRSET[k].TFZeq,4,1).c_str(),
            dtos(input.MRSET[k].LLG,14,1).c_str(),
            input.MRSET[k].R > 99 ? " >99" : dtos(input.MRSET[k].R,4,1).c_str(),
            space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
        }
        results.logBlank(SUMMARY);
      }
      input.MRSET.sort_LLG(); //paranoia

      //if the resolution has changed from that input
      if (!between(input.MRSET.get_resolution(),sol_input_HIRES,1.0e-02))
        input.MRSET.set_as_rescored(false);
      input.MRSET.set_resolution(mr.HiRes()); //may have changed from input
      //need to clear_clash before packing function
      results.setDotSol(input.MRSET);

      results.logSectionHeader(DEBUG,"SOLUTIONS");
      input.MRSET = results.getDotSol(); //sorted, merged
      for (int i = 0; i < input.MRSET.size(); i++)
      {
        results.logTab(1,DEBUG,"Solution #" + itos(i+1));
        results.logTab(1,DEBUG,input.MRSET[i].ANNOTATION);
        for (int j = 0; j < input.MRSET[i].KNOWN.size(); j++)
          results.logTab(1,DEBUG,input.MRSET[i].KNOWN[j].logfile(),false);
        results.logBlank(DEBUG);
      }

      if (input.OUTPUT_LEVEL >= DEBUG)//check non-interpolated and interpolated LLG
      { //Solution table
      int l = input.MRSET[0].KNOWN.size()-1;
      results.logUnderLine(DEBUG,"Solution Table");
      results.logTabPrintf(1,DEBUG,"%-4s%19s %6s %-31s   %-11s\n",
        "#","(Refined LLG TFZ==)","Modlid","Last-Component-Euler/Frac-Coord","SpaceGroup");
      for (unsigned k = 0; k < input.MRSET.size(); k++)
      {
        results.logTabPrintf(1,DEBUG,"%-4d %12s%5s  %-6s %4.0f%4.0f%4.0f/%6.2f%6.2f%6.2f   %-11s\n",
          k+1,
          dtos(input.MRSET[k].LLG,13,1).c_str(),
          !input.MRSET[k].TFZeq  ? " --- " : dtos(input.MRSET[k].TFZeq,5,1).c_str(),
          input.MRSET[k].KNOWN[l].MODLID.size() > 6 ? input.MRSET[k].KNOWN[l].MODLID.substr(0,6).c_str() : input.MRSET[k].KNOWN[l].MODLID.c_str(),
          input.MRSET[k].KNOWN[l].getEuler()[0],
          input.MRSET[k].KNOWN[l].getEuler()[1],
          input.MRSET[k].KNOWN[l].getEuler()[1],
          input.MRSET[k].KNOWN[l].TRA[0],
          input.MRSET[k].KNOWN[l].TRA[1],
          input.MRSET[k].KNOWN[l].TRA[2],
          space_group_name(input.MRSET[k].HALL,true).CCP4.c_str());
      }
      results.logBlank(DEBUG);
      }

      { //loggraph refinement results
        std::string loggraph;
        for (int k = 0; k < input.MRSET.size(); k++)
          loggraph +=
            itos(k+1)+" "+dtos(input.MRSET[k].LLG,10,2)+" "+dtos(input.MRSET[k].ORIG_LLG,10,2)+" " +dtos(input.MRSET[k].R,5,2)+" "+dtos(input.MRSET[k].ORIG_R,5,2)+"\n";
        string1D graphs(2);
        graphs[0] = "Solution Number vs LL-gain:NOUGHT:1,2,3";
        graphs[1] = "Solution Number vs R-value:NOUGHT:1,4,5";
        stringset search_modlids;
        results.logGraph(LOGFILE,"Occupancy Refinement",graphs,"Number final-LLG initial-LLG final-R initial-R",loggraph);
      }

      {//output
      if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
      outStream where = (input.DO_KEYWORDS.True() || input.DO_XYZOUT.True() || input.DO_HKLOUT.True()) ? SUMMARY : LOGFILE;
      results.logSectionHeader(LOGFILE,"OUTPUT FILES");
      if (input.DO_KEYWORDS.True()) results.writeSol();
      results.tracemol = input.tracemol;
      results.writePdb(input.DO_XYZOUT,input.PDB);
      results.writePdbRealOcc(input.DO_XYZOUT);
      //calculate map coeffs for results object
      {
        results.logTab(1,VERBOSE,"Applying sharpening of " + ftos(100*input.RESHARP.FRAC) +  "% of the B-factor");
        if (results.level(VERBOSE)) results.logTab(1,LOGFILE,"Verbose results: F_ISO added to mtz file");
        int NumTop = std::min(input.NUM_TOPFILES,input.MRSET.size());
        results.logUnderLine(LOGFILE,"Calculation of Map Coefficients");
        results.logTab(1,LOGFILE,itos(NumTop) + " top map coefficients calculated");
        results.logBlank(LOGFILE);
        for (int t = 0; t < NumTop; t++)
        {
          results.logTab(1,LOGFILE,"Map coefficient calculated for top solution #"+itos(t+1));
          results.logTab(0,LOGFILE,input.MRSET[t].logfile(1),false);
          mr.changeSpaceGroup(input.MRSET[t].HALL,HIRES,input.LORES);
          mr.init();
          if (!input.MRSET[t].BOOLOCC.size()) //not occupancy_refinement_did_not_improve_llg after sort
          {
            mr.ensPtr()->configure(input.MRSET[t].set_of_modlids(),occPDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[t].set_of_modlids(),results);
            results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after expansion
            mr.initKnownMR(input.MRSET[t]);
          }
          else
          {
            map_str_float1D thisOCC = input.MRSET[t].modlid_occupancies(); //binary to real
            mr.ensPtr()->configure(input.MRSET[t].set_of_modlids(),occPDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,input.MRSET[t].set_of_modlids(),results,false,0,&thisOCC);
            results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after expansion
            mr.initKnownMR(input.MRSET[t]);
          }
          mr.initSearchMR();
          mr.calcKnownVAR();
          mr.calcKnown();
          results.logTab(1,DEBUG,"MapCoef LLG = " + dtos(mr.likelihoodFn()));
          results.setMapCoefs(t,mr.getMapCoefs());
          results.logBlank(LOGFILE);
        }
        if (input.DO_HKLOUT.True())
          results.writeMtz(input.HKLIN);
      }
      for (int f = 0; f < results.getFilenames().size(); f++)
        results.logTab(1,where,results.getFilenames()[f]);
      if (!results.getFilenames().size()) results.logTab(1,where,"No files output");
      results.logBlank(where);
      } //output
    }
    results.clear_real_occupancies();
    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_OCC(InputMR_OCC& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_OCC(input,output);
}

}  //end namespace phaser
