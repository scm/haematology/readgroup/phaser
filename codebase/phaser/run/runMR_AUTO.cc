//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/miller/index_span.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR runMR_AUTO(InputMR_AUTO& inputAUTO,Output output)
{
  ResultMR results(output);
  results.setLevel(inputAUTO.OUTPUT_LEVEL);
  results.setMute(inputAUTO.MUTE_ON);
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      inputAUTO.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,inputAUTO.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    results.logHeader(LOGFILE,header(MR_AUTO));
    inputAUTO.Analyse(results);
    results.logTab(1,PROCESS,"Steps:");
    results.logTab(2,PROCESS,"Cell Content Analysis");
    results.logTab(2,PROCESS,"Anisotropy correction");
    results.logTab(2,PROCESS,"Translational NCS correction");
    results.logTab(2,PROCESS,"Rotation Function");
    results.logTab(2,PROCESS,"Translation Function");
    results.logTab(2,PROCESS,"Packing");
    results.logTab(2,PROCESS,"Refinement");
    results.logTab(2,PROCESS,"Final Refinement (if data higher resolution than search resolution)");
    results.logTab(1,PROCESS,"Number of search ensembles = " + itos(inputAUTO.AUTO_SEARCH.size()));
    if (inputAUTO.SEARCH_METHOD == "FULL")      results.logTab(1,PROCESS,"Search Method: FULL");
    else if (inputAUTO.SEARCH_METHOD == "FAST") results.logTab(1,PROCESS,"Search Method: FAST");
    results.logTab(1,PROCESS,"Input Search Order:");
    { //logfile scope
    string2D MODLID = inputAUTO.AUTO_SEARCH.getModlidStar();
    for (int c = 0; c < MODLID.size(); c++)
    {
      results.logTab(2,PROCESS,"#" + itos(c+1,2) + " " + MODLID[c][0]);
      for (int m = 1; m < MODLID[c].size(); m++)
        results.logTab(3,PROCESS,"or " + MODLID[c][m]);
    }
    }
    inputAUTO.SEARCH_ORDER_AUTO ?
      results.logTab(1,PROCESS,"Automatic (best predicted) search order WILL be used"):
      results.logTab(1,PROCESS,"Automatic (best predicted) search order WILL NOT be used");
    results.logBlank(SUMMARY);
    results.logTrailer(LOGFILE);
  }
  if (success)
  try
  {
    std::string experiment_type = inputAUTO.SEARCH_METHOD;
    if (experiment_type == "FULL")      results = runMR_FULL(inputAUTO,results);
    else if (experiment_type == "FAST") results = runMR_FAST(inputAUTO,results);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR runMR_AUTO(InputMR_AUTO& inputAUTO)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_AUTO(inputAUTO,output);
}

} //end namespace phaser
