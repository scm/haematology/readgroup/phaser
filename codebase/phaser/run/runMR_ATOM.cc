//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/main/runPhaser.h>
#include <phaser/src/MapTraceMol.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/miller/index_span.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultEP runMR_ATOM(InputMR_ATOM& inputAUTO,Output output)
{
  ResultEP results(output);
  results.setLevel(inputAUTO.OUTPUT_LEVEL);
  results.setMute(inputAUTO.MUTE_ON);
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      inputAUTO.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,inputAUTO.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  { //scope for input
    std::string cards = inputAUTO.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.REFLECTIONS = inputAUTO.REFLECTIONS;
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    inputAUTO.REFLECTIONS = resultMR_DAT.DATA_REFL;
    inputAUTO.setSPAC_HALL(resultMR_DAT.getHall());
    inputAUTO.setCELL6(resultMR_DAT.getCell6());
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_ATOM));
    inputAUTO.Analyse(results);

    results.logTab(1,SUMMARY,"Steps:");
    results.logTab(2,SUMMARY,"Anisotropy correction");
    results.logTab(2,SUMMARY,"Translational NCS correction");
    results.logTab(2,SUMMARY,"Single Atom Molecular Replacement");
    results.logTab(2,SUMMARY,"Structure completion from LLG maps");
    results.logBlank(SUMMARY);
    results.logTrailer(LOGFILE);

    //start with looking for 1 and go up to the max the user has requested
    int npdb(0);
    //set the spacegroup to the mtz one or the first alternative one
    if (inputAUTO.SGALT_HALL.size() >= 2)
      inputAUTO.SGALT_HALL.erase(inputAUTO.SGALT_HALL.begin()+1,inputAUTO.SGALT_HALL.end()); //only take the first alternative
    if (!inputAUTO.SGALT_HALL.size())
      inputAUTO.SGALT_HALL.push_back(inputAUTO.SG_HALL);
    inputAUTO.SG_HALL = inputAUTO.SGALT_HALL[0];
    int na = inputAUTO.AUTO_SEARCH.size();
    //for (int na = 1; na <= inputAUTO.AUTO_SEARCH.size() && results.Success(); na++)
    {
      results.logHeader(LOGFILE,header(MR_ATOM));
      (na > 1) ?
        results.logTab(1,SUMMARY,"Search for " + itos(na) + " single atoms"):
        results.logTab(1,SUMMARY,"Search for 1 single atom");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
      {
      //read as REFLECTIONS, convert to CRYS_DATA, then convert back after passing to EP_AUTO
      InputMR_AUTO inputMR(inputAUTO.Cards());
      inputMR.REFLECTIONS = inputAUTO.REFLECTIONS;
      inputMR.SG_HALL = inputAUTO.SG_HALL;
      inputMR.UNIT_CELL = inputAUTO.UNIT_CELL;
      inputMR.DO_XYZOUT.Set(false);
      inputMR.DO_HKLOUT.Set(false);
      inputMR.AUTO_SEARCH.set_modlid_num(inputAUTO.AUTO_SEARCH,na);
      inputMR.setMACM_ATOM_PROTOCOL();
      inputMR.NUM_TOPFILES = std::numeric_limits<int>::max(); //select all
      inputMR.FILEROOT = inputAUTO.FILEROOT + "." + itos(na);
    //  inputMR.setPEAK_TRAN_CLUS(false);
      inputMR.PDB = inputAUTO.PDB; //cannot unparse
      //no FAST option due to problem with amalgamation when heav atom model is centrosymmetric
      ResultMR resultMR = phaser::runMR_FULL(inputMR,results);
      if (inputAUTO.tracemol == NULL) //paranoia
      {
        MapTrcPtr new_ensPtr(new MapTraceMol());
        inputAUTO.tracemol = new_ensPtr; //deep copy on init
      }
      results.setOutput(resultMR);
      if (results.Success() && resultMR.getNumTop())
        inputAUTO.setATOM_STR("xtal",resultMR.string_pdbs());
        npdb = resultMR.numSolutions();
      }
    }
    if (results.Success())
    {
      results.logHeader(LOGFILE,header(MR_ATOM));
      if (npdb == 0)
        results.logTab(1,SUMMARY,"There are no sets of coordinates for llg completion");
      if (npdb > 1)
        results.logTab(1,SUMMARY,"There are " + itos(npdb) + " sets of coordinates for llg completion");
      else if (npdb == 1)
        results.logTab(1,SUMMARY,"There is one set of coordinates for llg completion");
      else
        results.logTab(1,SUMMARY,"There are no coordinates for llg completion");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
    if (results.Success() && npdb)
    {
      //inputAUTO.setATOM_STR("xtal",resultMR.PDBSTR);
      inputAUTO.CRYS_DATA.convert_NAT_to_SAD();
      inputAUTO.setMACS_NAT_PROTOCOL();
      std::string cards = inputAUTO.Cards();
      phaser::InputEP_AUTO input(cards);
      input.setCELL6(inputAUTO.UNIT_CELL);
      input.setSPAC_HALL(inputAUTO.SG_HALL);
      input.setCRYS_DATA(inputAUTO.CRYS_DATA);
      input.setHAND("OFF");
      results = phaser::runEP_SAD(input,results);
    }
    if (results.Success() && npdb)
    {
      results.logHeader(LOGFILE,header(MR_ATOM));
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$TEXT:Single Atom MR Result: $$ Baubles Markup $$");
      if (results.getTopRfac() && results.getTopRfac() < inputAUTO.RFAC_CUTOFF)
      {
        results.logTab(1,SUMMARY,"Single Atom MR Phasing SUCCESS");
        results.logTab(1,SUMMARY,"Found R-factor less than " + dtos(inputAUTO.RFAC_CUTOFF,5,2));
        results.logTab(2,SUMMARY,"R-factor = " + dtos(results.getTopRfac(),5,2));
      }
      else
      {
        results.logTab(1,SUMMARY,"Single Atom MR Phasing FAILED");
        results.logTab(1,SUMMARY,"R-factor for all trials greater than " + dtos(inputAUTO.RFAC_CUTOFF,5,2));
        results.logTab(2,SUMMARY,"Best R-factor = " + dtos(results.getTopRfac(),5,2));
      }
      if (results.isPackageCCP4()) results.logTab(1,SUMMARY,"$$");
      results.logBlank(SUMMARY);
      results.logTrailer(LOGFILE);
    }
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultEP runMR_ATOM(InputMR_ATOM& inputAUTO)
{
  Output output; //blank
  return runMR_ATOM(inputAUTO,output);
}

} //end namespace phaser
