//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/Phaser.h>
#include <phaser/main/runPhaser.h>
#include <phaser/lib/jiffy.h>
#include <cctbx/miller/index_span.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultEP runEP_AUTO(InputEP_AUTO& inputAUTO,Output output)
{
  ResultEP results(output);
  results.setLevel(inputAUTO.OUTPUT_LEVEL);
  results.setMute(inputAUTO.MUTE_ON);
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      inputAUTO.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,inputAUTO.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(EP_AUTO));
    inputAUTO.Analyse(results);

    results.logTab(1,PROCESS,"Steps:");
    results.logTab(2,PROCESS,"Anisotropy correction");
    results.logTab(2,PROCESS,"Experimental Phasing");
    if (inputAUTO.CRYS_DATA.is_experiment_type("SAD"))
      results.logTab(3,PROCESS,"SAD Phasing");
    else if (inputAUTO.CRYS_DATA.is_experiment_type("NAT"))
      results.logTab(3,PROCESS,"Phasing from structure (native)");
    else
      throw PhaserError(FATAL,"Phasing function not available");
    results.logBlank(SUMMARY);
    results.logTrailer(LOGFILE);

    if (inputAUTO.CRYS_DATA.is_experiment_type("SAD"))
    {
      std::string cards = inputAUTO.Cards();
      phaser::InputEP_AUTO input(cards);
      input.PARTIAL = inputAUTO.PARTIAL;//if string, isn't unparsed
      input.setCELL6(inputAUTO.UNIT_CELL);
      input.setSPAC_HALL(inputAUTO.SG_HALL);
      input.setCRYS_DATA(inputAUTO.CRYS_DATA);
      results = phaser::runEP_SAD(input,results);
    }
    else if (inputAUTO.CRYS_DATA.is_experiment_type("NAT"))
    {
      inputAUTO.CRYS_DATA.convert_NAT_to_SAD();
      inputAUTO.setMACS_NAT_PROTOCOL();
      std::string cards = inputAUTO.Cards();
      phaser::InputEP_AUTO input(cards);
      input.PARTIAL = inputAUTO.PARTIAL; //if string, isn't unparsed
      input.setCELL6(inputAUTO.UNIT_CELL);
      input.setSPAC_HALL(inputAUTO.SG_HALL);
      input.setCRYS_DATA(inputAUTO.CRYS_DATA);
      results = phaser::runEP_SAD(input,results);
    }

  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.setFiles(inputAUTO.FILEROOT,inputAUTO.FILEKILL,inputAUTO.TIMEKILL);
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultEP runEP_AUTO(InputEP_AUTO& inputAUTO)
{
  Output output; //blank
  output.setPackagePhenix();
  return runEP_AUTO(inputAUTO,output);
}

} //end namespace phaser
