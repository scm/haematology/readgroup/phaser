//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/lib/safe_mtz.h>
#include <phaser/lib/jiffy.h>
#include <cmtzlib.h>
#include <mtzdata.h>
#include <ccp4_errno.h>
#include <library_file.h>
#include <cctbx/sgtbx/symbols.h>
#include <cctbx/sgtbx/space_group_type.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultEP_DAT runEP_DAT(InputEP_DAT& input,Output output)
{
  ResultEP_DAT results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  try
  {
    CCP4::ccp4_liberr_verbosity(0);
    results.logHeader(LOGFILE,header(EP_DAT));
    input.Analyse(results);

    PHASER_ASSERT(input.HKLIN != "");

    CMtz::MTZ *mtz;
    //Fcol etc can be F or I cols, depending on input
    CMtz::MTZCOL *mtzH(0),*mtzK(0),*mtzL(0),*Fcol(0),*SIGFcol(0),*FNEGcol(0),*SIGFNEGcol(0);
    int read_reflections(0);
    mtz = safe_mtz_get(input.HKLIN,read_reflections);
    mtzH = safe_mtz_col_lookup(mtz,"H");
    mtzK = safe_mtz_col_lookup(mtz,"K");
    mtzL = safe_mtz_col_lookup(mtz,"L");

    if (!input.CRYS_DATA.size())
    {
      int Fcount(0);
      std::string Fpos,SIGFpos,Fneg,SIGFneg;
      for (int i = 0; i < mtz->nxtal; ++i)
      for (int j = 0; j < mtz->xtal[i]->nset; ++j)
      for (int k = 0; k < mtz->xtal[i]->set[j]->ncol; ++k)
      {
        //assume F+ SIGF+ F- SIGF-
        if (mtz->xtal[i]->set[j]->col[k]->type[0] == 'G') //amplitudes
        {
          if (!(k+3 < mtz->xtal[i]->set[j]->ncol)) break;
          if (mtz->xtal[i]->set[j]->col[k+1]->type[0] != 'L') break;
          if (mtz->xtal[i]->set[j]->col[k+2]->type[0] != 'G') break;
          if (mtz->xtal[i]->set[j]->col[k+3]->type[0] != 'L') break;
          Fpos = mtz->xtal[i]->set[j]->col[k]->label;
          SIGFpos = mtz->xtal[i]->set[j]->col[k+1]->label;
          Fneg = mtz->xtal[i]->set[j]->col[k+2]->label;
          SIGFneg = mtz->xtal[i]->set[j]->col[k+3]->label;
          if (Fpos.find("+") == std::string::npos) break; //paranoia
          if (SIGFpos.find("+") == std::string::npos) break; //paranoia
          if (Fneg.find("-") == std::string::npos) break; //paranoia
          if (SIGFneg.find("-") == std::string::npos) break; //paranoia
          k+=3;
          Fcount++;
        }
      }
      if (!Fcount)
        throw PhaserError(FATAL,"No valid SAD data in input file");
      else if (Fcount == 1)
      {
        std::string xtalid("XTAL"),waveid("WAVE");
        input.CRYS_DATA.init(xtalid,waveid);
        input.CRYS_DATA(xtalid,waveid).initAnom();
        input.CRYS_DATA(xtalid,waveid).POS.F_ID = Fpos;
        input.CRYS_DATA(xtalid,waveid).POS.SIGF_ID = SIGFpos;
        input.CRYS_DATA(xtalid,waveid).NEG.F_ID = Fneg;
        input.CRYS_DATA(xtalid,waveid).NEG.SIGF_ID = SIGFneg;
      }
      else
        throw PhaserError(FATAL,"Multiple SAD wavelengths, specify columns in input");
    }

  // UNITCELL
    bool DEFAULT_CELL = cctbx::uctbx::unit_cell(input.UNIT_CELL).is_similar_to(cctbx::uctbx::unit_cell(DEF_CELL),0.001,0.001);
    af::double6 unitcell;
    if (DEFAULT_CELL)
    {
      std::string x = input.CRYS_DATA().XTALID;
      std::string w = input.CRYS_DATA().WAVEID;
      std::string phased_f(input.CRYS_DATA(x,w).inputI() ? input.CRYS_DATA(x,w).POS.I_ID : input.CRYS_DATA(x,w).POS.F_ID);
      Fcol = safe_mtz_col_lookup(mtz,phased_f);
      CMtz::MTZXTAL* xtal = CMtz::MtzSetXtal( mtz, CMtz::MtzColSet( mtz, Fcol));
      PHASER_ASSERT(xtal != 0);
      floatType a     = static_cast<floatType>(xtal->cell[0]);
      floatType b     = static_cast<floatType>(xtal->cell[1]);
      floatType c     = static_cast<floatType>(xtal->cell[2]);
      floatType alpha = static_cast<floatType>(xtal->cell[3]);
      floatType beta  = static_cast<floatType>(xtal->cell[4]);
      floatType gamma = static_cast<floatType>(xtal->cell[5]);
      unitcell = af::double6(a,b,c,alpha,beta,gamma);
    }
    else
    {
      unitcell = input.UNIT_CELL;
    }
    UnitCell uc(unitcell);

  // SPACEGROUP
    cctbx::sgtbx::space_group cctbxSG;
    if (!input.SG_HALL.size() && input.HKLIN.size()) //i.e. SG from MTZ file
    {
      //now do reverse lookup for cctbx
      for (unsigned isym = 0; isym < mtz->mtzsymm.nsym; isym++)
      {
        scitbx::vec3<floatType> trasym;
        scitbx::mat3<floatType> rotsym;
        for (int i = 0; i < 3; i++)
        {
          trasym[i] = mtz->mtzsymm.sym[isym][i][3];
          for (int j = 0; j < 3; j++)
            rotsym(i,j) = mtz->mtzsymm.sym[isym][j][i];
        }
        cctbx::sgtbx::rt_mx nextSym(rotsym.transpose(),trasym);
        cctbxSG.expand_smx(nextSym);
      }
    }
    else if (input.SG_HALL != "")
    {
      cctbxSG = cctbx::sgtbx::space_group(input.SG_HALL,"A1983");
    }
    else
      throw PhaserError(FATAL,"Space Group not set");

    std::string Hall(cctbxSG.type().hall_symbol());
    int Number(cctbxSG.type().number());

  // DATA
    //setup all that is possible on first pass through mtz file
    bool1D readMtzRefl(mtz->nref,false),centric(mtz->nref,false);
    int roffset1(0);
    int roffset2(0);
    floatType MTZ_HIRES(DEF_LORES),MTZ_LORES(0);
    for (int x = 0 ; x < input.CRYS_DATA.size(); x++)
    {
      for (int w = 0; w < input.CRYS_DATA[x].size(); w++)
      {
        //reset position in file
        int respos = roffset2 * CMtz::MtzNumSourceCol(mtz) + SIZE1 + roffset1;
        CCP4::ccp4_file_seek(mtz->filein,respos,SEEK_SET);
        Fcol = safe_mtz_col_lookup(mtz,input.CRYS_DATA(x,w).inputI() ? input.CRYS_DATA(x,w).POS.I_ID : input.CRYS_DATA(x,w).POS.F_ID);
        if (input.CRYS_DATA[x][w].isAnom())
           FNEGcol = safe_mtz_col_lookup(mtz,input.CRYS_DATA(x,w).inputI() ? input.CRYS_DATA(x,w).NEG.I_ID : input.CRYS_DATA(x,w).NEG.F_ID);
        int ncols = (input.CRYS_DATA[x][w].isAnom()) ? 5 : 4;
        for (int mtzr = 0; mtzr < mtz->nref ; mtzr++)
        {
          std::vector<float> adata_(ncols);
          float* adata = &*adata_.begin();
          std::vector<int> logmss_(ncols);
          int* logmss = &*logmss_.begin();
          std::vector<CMtz::MTZCOL*> lookup_(ncols);
          CMtz::MTZCOL** lookup = &*lookup_.begin();
          lookup[0] = mtzH;
          lookup[1] = mtzK;
          lookup[2] = mtzL;
          lookup[3] = Fcol;
          if (input.CRYS_DATA[x][w].isAnom()) lookup[4] = FNEGcol;
          int failure = phaser::MtzReadRefl(mtz,adata,logmss,lookup,ncols,mtzr+1);
          PHASER_ASSERT(!failure);
          floatType H(adata[0]);
          floatType K(adata[1]);
          floatType L(adata[2]);
          miller::index<int> HKL(H,K,L);
          floatType resolution = uc.reso(HKL);
          MTZ_HIRES = std::min(resolution,MTZ_HIRES);
          MTZ_LORES = std::max(resolution,MTZ_LORES);
          //centric reflections in anomalous datasets are only recorded in F+ column :-/
          centric[mtzr] = cctbxSG.is_centric(miller::index<int>(H,K,L));
          if (!input.CRYS_DATA[x][w].isAnom() || centric[mtzr])
          {
            readMtzRefl[mtzr] = resolution >= input.HIRES &&
                                resolution <= input.LORES &&
                                !mtz_isnan(adata[3]);
          }
          else
          {
            readMtzRefl[mtzr] = resolution >= input.HIRES &&
                                resolution <= input.LORES &&
                                (!mtz_isnan(adata[3]) || !mtz_isnan(adata[4]));
          }
        }
      }
    }

    //reset position in file
    int respos = roffset2 * CMtz::MtzNumSourceCol(mtz) + SIZE1 + roffset1;
    CCP4::ccp4_file_seek(mtz->filein,respos,SEEK_SET);
    mtzH = safe_mtz_col_lookup(mtz,"H");
    mtzK = safe_mtz_col_lookup(mtz,"K");
    mtzL = safe_mtz_col_lookup(mtz,"L");

  //read the miller indices
    floatType data_hires(DEF_LORES),data_lores(0);
    for (int mtzr = 0; mtzr < mtz->nref; mtzr++)
    {
      int ncols(3);
      std::vector<float> adata_(ncols);
      float* adata = &*adata_.begin();
      std::vector<int> logmss_(ncols);
      int* logmss = &*logmss_.begin();
      std::vector<CMtz::MTZCOL*> lookup_(ncols);
      CMtz::MTZCOL** lookup = &*lookup_.begin();
      lookup[0] = mtzH;
      lookup[1] = mtzK;
      lookup[2] = mtzL;
      int failure = phaser::MtzReadRefl(mtz,adata,logmss,lookup,ncols,mtzr+1);
      PHASER_ASSERT(!failure);
      int H(adata[0]);
      int K(adata[1]);
      int L(adata[2]);
      miller::index<int> HKL(H,K,L);
      if (readMtzRefl[mtzr]) //must call MtzReadRefl before deciding
      {
        input.CRYS_DATA.push_back_miller(HKL);
        floatType resolution = uc.reso(HKL);
        data_hires = std::min(resolution,data_hires);
        data_lores = std::max(resolution,data_lores);
      }
    }

    results.logTab(1,SUMMARY,"Data read from mtz file: " + input.HKLIN);
    for (int x = 0 ; x < input.CRYS_DATA.size(); x++)
    {
      for (int w = 0; w < input.CRYS_DATA[x].size(); w++)
      {
        //reset position in file
        std::string labin;
        int respos = roffset2 * CMtz::MtzNumSourceCol(mtz) + SIZE1 + roffset1;
        CCP4::ccp4_file_seek(mtz->filein,respos,SEEK_SET);
        if (!input.CRYS_DATA[x][w].isAnom())
        {
          labin=(input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].POS.I_ID : input.CRYS_DATA[x][w].POS.F_ID);
          Fcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          labin=(input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].POS.SIGI_ID : input.CRYS_DATA[x][w].POS.SIGF_ID);
          SIGFcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          if (input.CRYS_DATA[x][w].inputF())
          {
          if ((*Fcol->type) != 'F')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.F_ID + " not \'F\'");
          if ((*SIGFcol->type) != 'Q')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.SIGF_ID + " not \'Q\'");
          }
          else
          {
          if ((*Fcol->type) != 'J')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.I_ID + " not \'J\'");
          if ((*SIGFcol->type) != 'Q')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.SIGI_ID + " not \'Q\'");
          }
        }
        else
        {
          labin=(input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].POS.I_ID : input.CRYS_DATA[x][w].POS.F_ID);
          Fcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          labin=(input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].NEG.I_ID : input.CRYS_DATA[x][w].NEG.F_ID);
          FNEGcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          labin=(input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].POS.SIGI_ID : input.CRYS_DATA[x][w].POS.SIGF_ID);
          SIGFcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          labin= (input.CRYS_DATA[x][w].inputI() ? input.CRYS_DATA[x][w].NEG.SIGI_ID : input.CRYS_DATA[x][w].NEG.SIGF_ID);
          SIGFNEGcol = safe_mtz_col_lookup(mtz,labin);
          results.logTab(1,SUMMARY,"Data read from column: " + labin);
          if (input.CRYS_DATA[x][w].inputF())
          {
          if ((*Fcol->type) != 'G')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.F_ID + " not \'G\'");
          if ((*FNEGcol->type) != 'G')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].NEG.F_ID + " not \'G\'");
          if ((*SIGFcol->type) != 'L')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.SIGF_ID + " not \'L\'");
          if ((*SIGFNEGcol->type) != 'L')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].NEG.SIGF_ID + " not \'L\'");
          }
          else
          {
          if ((*Fcol->type) != 'K')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.I_ID + " not \'K\'");
          if ((*FNEGcol->type) != 'K')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].NEG.I_ID + " not \'K\'");
          if ((*SIGFcol->type) != 'M')
          throw PhaserError(FATAL,"Mtz column type for " + input.CRYS_DATA[x][w].POS.SIGI_ID + " not \'M\'");
          if ((*SIGFNEGcol->type) != 'M')
          throw PhaserError(FATAL,"Mtz column type for " +  input.CRYS_DATA[x][w].NEG.SIGI_ID + " not \'M\'");
          }
        }
        for (int mtzr = 0; mtzr < mtz->nref ; mtzr++)
        if (!input.CRYS_DATA[x][w].isAnom())
        {
          int ncols = 2;
          std::vector<float> adata_(ncols);
          float* adata = &*adata_.begin();
          std::vector<int> logmss_(ncols);
          int* logmss = &*logmss_.begin();
          std::vector<CMtz::MTZCOL*> lookup_(ncols);
          CMtz::MTZCOL** lookup = &*lookup_.begin();
          lookup[0] = Fcol;
          lookup[1] = SIGFcol;
          int failure = phaser::MtzReadRefl(mtz,adata,logmss,lookup,ncols,mtzr+1);
          PHASER_ASSERT(!failure);
          if (readMtzRefl[mtzr]) //must call MtzReadRefl before deciding
          {
            //test for F or I is done internally to push_back_f
            if (!mtz_isnan(adata[0]))
              input.CRYS_DATA[x][w].push_back_f(adata[0],adata[1],!mtz_isnan(adata[0]));
            else
              input.CRYS_DATA[x][w].push_back_f(0,0,!mtz_isnan(adata[0]));
          }
        }
        else
        {
          int ncols = 4;
          int Fpos(0),Fneg(1),SIGFpos(2),SIGFneg(3);
          std::vector<float> adata_(ncols);
          float* adata = &*adata_.begin();
          std::vector<int> logmss_(ncols);
          int* logmss = &*logmss_.begin();
          std::vector<CMtz::MTZCOL*> lookup_(ncols);
          CMtz::MTZCOL** lookup = &*lookup_.begin();
          lookup[0] = Fcol;
          lookup[1] = FNEGcol;
          lookup[2] = SIGFcol;
          lookup[3] = SIGFNEGcol;
          int failure = phaser::MtzReadRefl(mtz,adata,logmss,lookup,ncols,mtzr+1);
          PHASER_ASSERT(!failure);
          if (readMtzRefl[mtzr]) //must read before selecting this mtzr
          {
            //test for F or I is done internally to push_back_f
            if (!centric[mtzr] && !mtz_isnan(adata[Fpos]) && !mtz_isnan(adata[Fneg]))
            {
              input.CRYS_DATA[x][w].push_back_f(adata[Fpos],adata[SIGFpos],!mtz_isnan(adata[Fpos]));
              input.CRYS_DATA[x][w].push_back_fneg(adata[Fneg],adata[SIGFneg],!mtz_isnan(adata[Fneg]));
            }
            else if (!centric[mtzr] && !mtz_isnan(adata[Fpos]) && mtz_isnan(adata[Fneg]))
            {
              input.CRYS_DATA[x][w].push_back_f(adata[Fpos],adata[SIGFpos],!mtz_isnan(adata[Fpos]));
              input.CRYS_DATA[x][w].push_back_fneg(0,0,false);
            }
            else if (!centric[mtzr] && mtz_isnan(adata[Fpos]) && !mtz_isnan(adata[Fneg]))
            {
              input.CRYS_DATA[x][w].push_back_f(0,0,false);
              input.CRYS_DATA[x][w].push_back_fneg(adata[Fneg],adata[SIGFneg],!mtz_isnan(adata[Fneg]));
            }
            else if (!centric[mtzr] && mtz_isnan(adata[Fpos]) && mtz_isnan(adata[Fneg]))
            {
              input.CRYS_DATA[x][w].push_back_f(0,0,false);
              input.CRYS_DATA[x][w].push_back_fneg(0,0,false);
            }
            else if (centric[mtzr] && !mtz_isnan(adata[Fpos])) //only recorded in Fpos :-/
            {
              input.CRYS_DATA[x][w].push_back_f(adata[Fpos],adata[SIGFpos],!mtz_isnan(adata[Fpos]));
              input.CRYS_DATA[x][w].push_back_fneg(adata[Fpos],adata[SIGFpos],!mtz_isnan(adata[Fpos]));
            }
            else if (centric[mtzr] && mtz_isnan(adata[Fpos])) //only recorded in Fpos :-/
            {
              input.CRYS_DATA[x][w].push_back_f(0,0,false);
              input.CRYS_DATA[x][w].push_back_fneg(0,0,false);
            }
          }
        }
      }
    }

    CMtz::MtzFree(mtz);

    results.setCrysData(input.CRYS_DATA);
    results.setUnitCell(unitcell);
    results.setSpaceGroup(Hall);
    results.setReso(data_hires,data_lores);
    results.setMtzReso(MTZ_HIRES,MTZ_LORES);

    results.logTabPrintf(1,SUMMARY,"Space-Group Name (Hall Symbol): %s (%s)\n",SpaceGroup(Hall).spcgrpname().c_str(),Hall.c_str());
    results.logTab(1,SUMMARY,"Space-Group Number: " + itos(Number));
    results.logTabPrintf(1,SUMMARY,"Unit Cell: %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n",
         unitcell[0],unitcell[1],unitcell[2],unitcell[3],unitcell[4],unitcell[5]);
    results.logTabPrintf(1,SUMMARY,"Resolution on Mtz file: %5.2f %5.2f\n",MTZ_HIRES,MTZ_LORES);
    results.logTabPrintf(1,SUMMARY,"Resolution Selected:    %5.2f %5.2f\n",data_hires,data_lores);
    results.logTab(1,SUMMARY,"Number of Reflections in Selected Resolution Range: " + itos(input.CRYS_DATA.nrefl()));
    results.logBlank(SUMMARY);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultEP_DAT runEP_DAT(InputEP_DAT& input)
{
  Output output;
  output.setPackagePhenix();
  return runEP_DAT(input,output);
}

} //end namespace phaser
