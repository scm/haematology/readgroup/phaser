//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/src/DataMR.h>
#include <phaser/src/SiteListAng.h>
#include <phaser/src/Clmn.h>
#include <phaser/lib/signal.h>
#include <phaser/lib/mean.h>
#include <scitbx/random.h>
#include <phaser/src/RefineANO.h>
#include <phaser/src/Minimizer.h>
#include <phaser/src/Epsilon.h>
#include <phaser/src/Composition.h>
#include <phaser/src/Dfactor.h>
#include <phaser/src/MapTraceMol.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultMR_RF runMR_FRF(InputMR_FRF& input,Output output)
{
  ResultMR_RF results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  {
    std::string cards = input.Cards();
    InputMR_DAT inputMR_DAT(cards);
    inputMR_DAT.setREFL_DATA(input.REFLECTIONS);
    ResultMR_DAT resultMR_DAT = runMR_PREP(inputMR_DAT,results);
    results.setOutput(resultMR_DAT);
    success = resultMR_DAT.Success();
    if (success) {
    input.REFLECTIONS = resultMR_DAT.DATA_REFL;
    input.setSPAC_HALL(resultMR_DAT.getHall());
    input.setCELL6(resultMR_DAT.getCell6());
    }
  }
  std::map<std::string,data_ellg> ELLG_DATA = input.ELLG_DATA;
  if (success && !input.ELLG_DATA.size())
  {
    std::string cards = input.Cards();
    InputMR_ELLG inputELLG(cards);
    //inputELLG.setMUTE(true); //suppress output, just return results
    inputELLG.setREFL_DATA(input.REFLECTIONS);
    inputELLG.setSEAR_ORDE_AUTO(false); //do not alter search order again
    inputELLG.PDB = input.PDB; //unparse does not work
    inputELLG.setNORM_DATA(input.SIGMAN);
    inputELLG.setTNCS(input.PTNCS);
    if (input.SEARCH_FROM_AUTO)
      inputELLG.AUTO_SEARCH = input.AUTO_SEARCH; //can't unparse the star bits
    ResultELLG resultELLG = runMR_ELLG(inputELLG,results);
    ELLG_DATA = resultELLG.ELLG_DATA;
    success = resultELLG.Success();
    if (!input.SIGMAN.REFINED)
    {
    input.SIGMAN = resultELLG.getSigmaN();
    input.SIGMAN.REFINED = true; //flag for no more refinement
    }
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(MR_FRF));
    input.Analyse(results);

    if (!input.SEARCH_FROM_AUTO) input.PEAKS_ROT.DOWN = 0; //change the cutoff instead!

    if (!input.SIGMAN.REFINED)
    { //memory
    results.logSectionHeader(LOGFILE,"ANISOTROPY CORRECTION");
    RefineANO refa(input.REFLECTIONS,
                   input.RESHARP,
                   input.SIGMAN,
                   input.OUTLIER,
                   input.PTNCS,
                   input.DATABINS,
                   input.COMPOSITION);
    Minimizer minimizer;
    minimizer.run(refa,input.MACRO_ANO,results);
    input.setNORM_DATA(refa.getSigmaN());
    input.OUTLIER = refa.getOutlier();
    }

    if (input.PTNCS.USE)
    { //memory
    Epsilon epsilon(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    epsilon.findTRA(results);
    if (epsilon.getPtNcs().TRA.VECTOR_SET)
      epsilon.findROT(input.MACRO_TNCS,results);
    input.PTNCS = epsilon.getPtNcs();
    input.OUTLIER = epsilon.getOutlier();
    if (input.PTNCS.use_and_present())
    { //can be fixed in FAST auto job, but just throw error here
      std::map<std::string,int> MODLID = input.MRSET.map_of_modlids();
      for (std::map<std::string,int>::iterator iter = MODLID.begin(); iter != MODLID.end(); iter++)
        if (iter->second % input.PTNCS.NMOL) //remainder
          results.logAdvisory(SUMMARY,"Solutions do not obey tNCS NMOL");
    }
    }

    Composition composition(input.COMPOSITION);
    float1D search_bfactors;
    {//memory
    composition.set_modlid_scattering(input.PDB);
    cctbx::uctbx::unit_cell cctbxUC = UnitCell(input.UNIT_CELL).getCctbxUC();
    composition.increase_total_scat_if_necessary(input.SG_HALL,input.UNIT_CELL,input.MRSET,input.PTNCS,input.SEARCH_FACTORS,search_array(input.SEARCH_MODLID));
    floatType fullhires = cctbx::uctbx::d_star_sq_as_d(cctbxUC.min_max_d_star_sq(input.REFLECTIONS.MILLER.const_ref())[1]);
    search_bfactors = composition.get_search_bfactors(input.SG_HALL,input.UNIT_CELL,fullhires,input.MRSET);
    for (int k=0; k < search_bfactors.size(); k++)
    {
      if (search_bfactors[k] > input.SEARCH_FACTORS.BFAC)
        output.logTab(1,DEBUG,"Default search B-factor overridden for solution set " + itos(k+1));
      search_bfactors[k] = std::max(input.SEARCH_FACTORS.BFAC,search_bfactors[k]);
    }
    if (composition.increased())
      results.logWarning(SUMMARY,composition.warning());
    input.COMPOSITION = composition;
    if (composition.increased())
    {
      std::string cards = input.Cards();
      InputCCA inputCCA(cards);
      inputCCA.setRESO_HIGH(fullhires);
      inputCCA.setMUTE(true);
      ResultCCA resultCCA = runCCA(inputCCA,results);
      PHASER_ASSERT(resultCCA.Success()); //cheap
      //throw same error as would be thrown by runCCA
      if (resultCCA.getFitError())
      throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
    }
    }//memory
    floatType TOTAL_SCAT = composition.total_scat(input.SG_HALL,input.UNIT_CELL);

    if (!input.REFLECTIONS.Feff.size())
    { //memory
    results.logSectionHeader(LOGFILE,"EXPERIMENTAL ERROR CORRECTION");
    Dfactor dfactor(input.REFLECTIONS,
                    input.RESHARP,
                    input.SIGMAN,
                    input.OUTLIER,
                    input.PTNCS,
                    input.DATABINS,
                    input.COMPOSITION
                    );
    dfactor.calcDfactor(results);
    input.REFLECTIONS = dfactor;
    }

    results.logTab(1,DEBUG,"Setting up Factorial Tables");
    lnfactorial(0);

    results.logSectionHeader(LOGFILE,"DATA FOR ROTATION FUNCTION");
    //must rescore in P1 because the RF mean is taken as the TF mean
    if (SpaceGroup(input.SG_HALL).spcgrpnumber() == 1) input.DO_RESCORE_ROT = ON;

    DataMR mr(input.REFLECTIONS,
              input.RESHARP,
              input.SIGMAN,
              input.OUTLIER,
              input.PTNCS,
              input.DATABINS,
              input.HIRES,input.LORES,
              input.COMPOSITION,
              input.ensemble);
    std::pair<int,int> number_selected = mr.number_selected();
    if (double(number_selected.first)/double(number_selected.second) < 0.1)
    results.logWarning(SUMMARY,"Less than 10% of data within resolution limits are selected");
    if (number_selected.first < 4) // 3DOF, very crude check (EM data without anisotropy)
    throw PhaserError(FATAL,"Not enough reflections for Rotation Function");
    SigmaN sigmaN = mr.getBinSigmaN();
    bool halfR(true);
    mr.initGfunction(halfR);
    results.init(input.SG_HALL,input.TITLE,input.NUM_TOPFILES,input.PDB);
    results.setUnitCell(mr.getUnitCell());

    //if the ensembles are coming from mtz files, check that the high resolution of the input
    //ensembles is not lower than the resolution of the data - if so, truncate data to lower resolution
    stringset modlids = input.MRSET.set_of_modlids();
    if (input.DO_RESCORE_ROT)
      for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++)
        modlids.insert(input.SEARCH_MODLID[frf]);
    floatType HIRES = mr.HiRes();
    for (stringset::iterator iter = modlids.begin(); iter != modlids.end(); iter++)
    {
      std::string m = (*iter);
      floatType mapHIRES(0);
      if (input.PDB[m].ENSEMBLE[0].map_format())
        mapHIRES = std::max(input.PDB[m].HIRES,mapHIRES);
      //allow for interpolation with cell expansion
      mapHIRES *= input.MRSET.max_cell_scale();
      HIRES = std::max(mapHIRES,HIRES);
    }
    if (HIRES > mr.HiRes())
    {
      results.logTab(1,SUMMARY,"High resolution limit imposed by Maps = " + dtos(HIRES,5,2));
      mr.selectReso(HIRES,input.LORES);
    }
    { //memory
    bool can_lower_resolution(false);
    if (input.HIRES>=0)
    {
      results.logTab(1,SUMMARY,"High resolution limit input = " + dtos(HIRES,5,2));
      mr.selectReso(input.HIRES,input.LORES);
    }
    else
    {
      double useful_resolution(DEF_LORES);
      double ellg_resolution(DEF_LORES);
      for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++)
      {
        std::string& ROT_MODLID = input.SEARCH_MODLID[frf];
        useful_resolution = std::min(useful_resolution,ELLG_DATA[ROT_MODLID].USEFUL_RESO);
        ellg_resolution = std::min(ellg_resolution,ELLG_DATA[ROT_MODLID].TARGET_RESO);
      }
      if (useful_resolution > mr.HiRes())
      {
        HIRES = useful_resolution;
        results.logTab(1,SUMMARY,"High resolution limit lowered by RMS of ensemble = " + dtos(HIRES,5,2));
        mr.selectReso(useful_resolution,input.LORES);
      }
      else results.logTab(1,SUMMARY,"High resolution limit unaltered by RMS of ensemble");
      if (input.ELLG_HIRES  //internal control, fast mode
          && ellg_resolution > mr.HiRes())
      {
        HIRES = ellg_resolution;
        results.logTab(1,SUMMARY,"High resolution limit lowered by expected LLG = " + dtos(HIRES,5,2));
        can_lower_resolution = true;
        mr.selectReso(ellg_resolution,input.LORES);
      }
      else results.logTab(1,SUMMARY,"High resolution limit unaltered by expected LLG");
    }
    results.logBlank(SUMMARY);
    results.storeResolution(mr.HiRes(),can_lower_resolution);
    }
    mr.logOutliers(LOGFILE,results);
    mr.logDataStats(LOGFILE,results);
    mr.logSymm(VERBOSE,results);
    mr.logUnitCell(DEBUG,results);
    mr.logDataStatsExtra(DEBUG,20,results);
    mr.logSigmaN(DEBUG,results);
    results.logBlank(VERBOSE);

    cmplx3D clmnData,clmn2Data;
    string3D table_strs(input.SEARCH_MODLID.size());
    int ncomponent(0);
    bool LLG_mean_and_sigma_calculated(false);
    int num_RF(0);
    int ifrf(0);
    for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++) num_RF += input.MRSET.size();

    results.logSectionHeader(LOGFILE,"WILSON DISTRIBUTION");
    floatType LLwilson = mr.WilsonLL();
    mr.logWilson(LOGFILE,LLwilson,results);
    mr.setLLwilson(LLwilson);

    { //memory
    outStream where(LOGFILE);
    results.logSectionHeader(where,"ENSEMBLING");
    //there may be lots of ensembles in an OR search
    //we don't want them all in memory at the same time
    //load the ones that are extras first, extract the table information (and check)
    //and then leave the MapEnsemble object in the state for the first refinement
    stringset allEns = input.MRSET.set_of_modlids();
    for (int s = 0; s < input.SEARCH_MODLID.size(); s++)
      allEns.insert(input.SEARCH_MODLID[s]);
    stringset firstEns = input.MRSET[0].set_of_modlids();
    firstEns.insert(input.SEARCH_MODLID[0]);
    for (pdbIter iter = input.PDB.begin(); iter != input.PDB.end(); iter++)
      iter->second.setup_and_fix_vrms(iter->first,HIRES,input.PTNCS,input.SOLPAR,results);
    mr.ensPtr()->configure(allEns,input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,firstEns,results);
    mr.ensPtr()->ensembling_table(LOGFILE,TOTAL_SCAT,results);
    std::string warning = input.MRSET.setup_vrms_delta(mr.ensPtr()->get_drms_vrms());
    if (warning.size())
      results.logWarning(SUMMARY,"Conflict in RMSD between ENSEMBLE and SOLUTION inputs\n" + warning);
    results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation
    if (input.DO_XYZOUT.True())
    { //ready for output
      if (input.tracemol == NULL)
      {
        MapTrcPtr new_ensPtr(new MapTraceMol());
        input.tracemol = new_ensPtr; //deep copy on init
      }
      input.tracemol->configure(allEns,true,false,input.PDB,results);
      input.tracemol->trace_table(LOGFILE,results);
    }
    }

    float1D all_r_mean(0);
    floatType llg_f_top = -std::numeric_limits<floatType>::max();
    PHASER_ASSERT(input.SEARCH_MODLID.size());
    if (input.RFAC_USE &&
        input.SEARCH_MODLID.size() == 1 &&
        (!input.PTNCS.use_and_present() || input.PTNCS.NMOL < 2) && //not looking for more than one
        (input.MRSET.size() == 0 || (input.MRSET.size() == 1 && input.MRSET[0].KNOWN.size() == 0)) &&
        !input.PDB[input.SEARCH_MODLID[0]].is_atom)
    {
      bool mr_is_necessary(true);
      floatType RFAC_RESO = std::max(HIRES,3.0); //not configured for min, don't want more than 3
      mr_set MRSET = input.MRSET.size() ? input.MRSET[0] : mr_set();
             MRSET.KNOWN.push_back(mr_ndim(input.SEARCH_MODLID[0]));
      floatType minRfac(200); //unfeasible
      std::string bestHall(input.SG_HALL);
      //alter because only local
      float1D mrRfactor;
      if (!input.SGALT_HALL.size()) input.SGALT_HALL.push_back(input.SG_HALL);
      for (int s = 0; s < input.SGALT_HALL.size(); s++)
      {
        SpaceGroup sgalt(input.SGALT_HALL[s]);
        mr.changeSpaceGroup(input.SGALT_HALL[s],RFAC_RESO,10000);
        mr.initKnownMR(MRSET);
        mr.initSearchMR();
        mr.calcKnownVAR();
        mr.calcKnown();
        floatType Rfac = mr.Rfactor();
        mrRfactor.push_back(Rfac);
        if (Rfac < minRfac)
        {
          minRfac = std::min(minRfac,Rfac);
          bestHall = input.SGALT_HALL[s];
        }
      }
      mr.changeSpaceGroup(input.SG_HALL,HIRES,input.LORES); //back to beginning
      mr.init();
      results.setTopRfac(minRfac);
      if (results.getTopRfac() < input.RFAC_CUTOFF)
      {
        results.setHall(bestHall);
        mr_is_necessary = false;
        std::string& ROT_MODLID = input.SEARCH_MODLID[0];
        mr_set ORIG = input.MRSET.size() ? input.MRSET[0] : mr_set();
        results.setRlistOrigin(ROT_MODLID,bestHall,ORIG,mr.likelihoodFn());
      }
      outStream where(mr_is_necessary?VERBOSE:SUMMARY);
      results.logSectionHeader(where,"R-FACTOR CHECK");
      results.logTab(1,where,"R-factor at " + dtos(RFAC_RESO,4,2) + "A calculated");
      results.logTabPrintf(1,where,"%11s  %8s\n","Space Group","R-factor");
      for (int s = 0; s < input.SGALT_HALL.size(); s++)
      {
        SpaceGroup sgalt(input.SGALT_HALL[s]);
        (mrRfactor[s] < 100) ?
        results.logTabPrintf(1,where,"%-11s  %5.2f%\n",sgalt.spcgrpname().c_str(),mrRfactor[s]):
        results.logTabPrintf(1,where,"%-11s  >100%\n",sgalt.spcgrpname().c_str());
      }
      results.logBlank(where);
      results.logTab(1,where,"Minimum R-factor of ensemble at origin in original orientation = " + dtos(minRfac,5,2));
      results.logBlank(where);
      if (!mr_is_necessary)
      {
        results.logTab(0,where,results.getSet(0).logfile(1,true),false);
        results.logBlank(where);
        goto the_end;
      }
    }

    results.logSectionHeader(LOGFILE,"ROTATION FUNCTION");
    num_RF > 1 ?
      results.logProgressBarStart(SUMMARY,"Performing " + itos(num_RF) + " Rotation Functions", num_RF):
      results.logProgressBarStart(SUMMARY,"Performing 1 Rotation Function", num_RF);
    for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++)
    {
      std::string& ROT_MODLID = input.SEARCH_MODLID[frf];
      table_strs[frf].resize(input.MRSET.size());

      MapEnsemble decomposition; //this is a map of one (ROT_MODLID), empty if not used
      cmplx3D SearchElmn;
      int LMAX(0),LMIN(0);
      double LMAX_RESO(0);
      PHASER_ASSERT(input.MRSET.size());
      {
        if (input.TARGET_ROT != "BRUTE" && !input.PDB[ROT_MODLID].is_atom)
        {
          stringset rotEns;
          rotEns.insert(ROT_MODLID);
          double SPHERE(DEF_CLMN_SPHE);
          if (!input.PDB[ROT_MODLID].ENSEMBLE[0].map_format())
            decomposition.configure(rotEns,input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,rotEns,results,true,SPHERE);
          else
            decomposition = *mr.ensPtr(); //smart pointer, changed in mr
          //precalculate Elmn for search model
          data_bofac SEARCH_FACTORS(search_bfactors[0],input.SEARCH_FACTORS.OFAC);
          floatType sphereOuter = SPHERE ? SPHERE : 2.*input.PDB[ROT_MODLID].mean_radius();
          LMAX = static_cast<int>(ceil(2.0*scitbx::constants::pi*sphereOuter/HIRES));
          if ((LMAX/2)*2 != LMAX) LMAX++;
          LMAX = std::min(LMAX,int(DEF_CLMN_LMAX));
          results.logTab(1,VERBOSE,"Calculating Elmn with minimum l value of "+itos(LMIN) + " and maximum l value " +itos(LMAX));
          if (LMAX == int(DEF_CLMN_LMAX) && input.USE_ROTATE_LMAX_RESO)
          {
            LMAX_RESO = 2.0*scitbx::constants::pi*sphereOuter/LMAX;
            results.logTab(1,VERBOSE,"Calculating Elmn with resolution "+dtos(LMAX_RESO));
          }
          else
          {
            LMAX_RESO = HIRES;
            results.logTab(1,VERBOSE,"Calculating Elmn with all data to resolution limit");
          }
          SearchElmn = decomposition[ROT_MODLID].getELMNxR2(input.TARGET_ROT_TYPE,TOTAL_SCAT,results,LMAX,SEARCH_FACTORS,LMAX_RESO);
        }
        results.logTab(1,LOGFILE,"Target Function: " + input.TARGET_ROT + " " + std::string(input.TARGET_ROT == "FAST"? input.TARGET_ROT_TYPE : ""));
        results.logBlank(LOGFILE);
      }

      for (unsigned k = 0; k < input.MRSET.size(); k++)
      {
        ifrf++;
        results.logSectionHeader(LOGFILE,"ROTATION FUNCTION #"+itos(ifrf)+" OF "+itos(num_RF));
        results.logTab(1,LOGFILE,"Search Ensemble: "+input.SEARCH_MODLID[frf]);
        if (search_bfactors[k])
          results.logTab(1,VERBOSE,"B-factor of search component = " + dtos(search_bfactors[k],2));
        results.logBlank(LOGFILE);
        if (input.MRSET[k].ANNOTATION.size()) results.logTab(1,LOGFILE,"ANNOTATION: " + input.MRSET[k].ANNOTATION);
        if (input.PDB[ROT_MODLID].is_atom)
        {
          results.logTab(1,LOGFILE,"Single atom search model: no rotation");
          results.logBlank(LOGFILE);
          dvect31D Euler(1,dvect3(0,0,0));
          float1D Value(1,0),Signal(1,0);
          bool1D Deep(1,false);
          int1D Clusters(1,0);
          input.MRSET[k].MAPCOEFS.clear(); //speed!
          results.storeRotlist(ROT_MODLID,input.MRSET[k],Euler,Value,Signal,Deep,Clusters);
          table_strs[frf][k].resize(6);
          for (int z = 0; z < 6; z++) table_strs[frf][k][z] = "---";
          table_strs[frf][k][0] = " * ";
          table_strs[frf][k][1] = " * ";
          results.setSampling(scitbx::constants::two_pi); // Rotation doesn't matter
          continue;
        }

        stringset currentEns = input.MRSET[k].set_of_modlids();
        currentEns.insert(ROT_MODLID);
        mr.ensPtr()->configure(currentEns,input.PDB,input.SOLPAR,input.BOXSCALE,HIRES,sigmaN,input.FORMFACTOR,currentEns,results);
        results.store_relative_wilsonB(mr.ensPtr()->get_map_relative_wilsonB()); //after sigmaN calculation

        SpaceGroup sg(input.SG_HALL);
        if (input.PTNCS.use_and_present())
        {
          results.logTab(1,LOGFILE,"Translational Ncs: Rotational spacegroup changed to P1");
          std::string p1("P 1");
          sg.setSpaceGroup(p1);
        }
        SiteListAng rotlist(sg,mr,input.PDB[ROT_MODLID].PR,input.PDB[ROT_MODLID].COORD_EXTENT);

        floatType sampling_radius = input.PDB[ROT_MODLID].mean_radius();
        double SAMP_RESO = mr.HiRes();
        if (LMAX == int(DEF_CLMN_LMAX) && input.USE_ROTATE_LMAX_RESO)
          SAMP_RESO = LMAX_RESO;
        floatType default_sampling = 2.0*scitbx::rad_as_deg(std::atan(SAMP_RESO/(4.0*sampling_radius)));
        floatType SAMPLING = input.ROT_SAMPLING ? input.ROT_SAMPLING : default_sampling; //degrees
        results.setSampling(SAMPLING);
        PHASER_ASSERT(SAMPLING > 0);
        results.logTab(1,LOGFILE,"Sampling: " + dtos(SAMPLING,5,2) + " degrees");
        results.logBlank(LOGFILE);

        mr.changeSpaceGroup(input.MRSET[k].HALL,HIRES,input.LORES);
        ncomponent = input.MRSET[k].KNOWN.size() + 1; //assume all MRSETs are the same - reasonable

        mr.initKnownMR(input.MRSET[k]); //fix are initialized
        results.logTab(0,LOGFILE,input.MRSET[k].logfile(1,true),false);
        if (input.MRSET[k].KNOWN.size())
        {
          halfR = false;
          mr.initGfunction(halfR);
          mr.calcKnownVAR();
          mr.calcKnown();
          halfR = true;
          mr.initGfunction(halfR);
        }

        if (input.TARGET_ROT != "BRUTE")
        {
          if (input.DO_RESCORE_ROT)
          {
            results.logTab(1,VERBOSE,"Raw peaks will be used for rescoring");
            results.logTab(1,VERBOSE,std::string(input.PEAKS_ROT.CLUSTER ? "Clustered" : "Raw") + " peaks will be saved after rescoring");
          }
          else results.logTab(1,VERBOSE,std::string(input.PEAKS_ROT.CLUSTER ? "Clustered" : "Raw") + " peaks will be saved without rescoring");

          results.logUnderLine(LOGFILE,"Spherical Harmonics");
          cmplx3D DataElmn = mr.getELMNxR2(input.TARGET_ROT_TYPE,results,LMAX,LMAX_RESO);
          results.logTab(1,VERBOSE,"Composing elmn's from 0th/1st order perturbation");
          results.logTab(1,VERBOSE,"Calculating Clmm' with minimum l value of "+itos(LMIN) + " and maximum l value " +itos(LMAX));
          clmnData = clmnx(DataElmn,SearchElmn,LMIN,LMAX);

          cmplxType zero(0,0);
          bool empty_clmn_array(true);
          for (int i = 0 ; i < clmnData.size(); i++)
            for (int j = 0 ; j < clmnData[i].size(); j++)
              for (int k = 0 ; k < clmnData[i][j].size(); k++)
                if (clmnData[i][j][k] != zero)
                  empty_clmn_array = false;
          if (empty_clmn_array)
          throw PhaserError(FATAL,"CLMN array empty (could be precision problem)");

    #ifdef Debug
        debug_elmn(DataElmn.first,SearchElmn.first);
        debug_clmn(clmnData);
    #endif

          results.logBlank(VERBOSE);
          results.logUnderLine(LOGFILE,"Scanning the Range of Beta Angles");

          rotlist.get_FRF(clmnData,results,SAMPLING);
          floatType f_mean = rotlist.getRmean();
          floatType f_sigma = rotlist.getRsigma();
          floatType Rmax = rotlist.getRmax();
          floatType Rmin = rotlist.getRmin();

          dmat33 axisrot(1,0,0,0,1,0,0,0,1);
          if (mr.highOrderAxis()==1) axisrot = dmat33(0,0,1,1,0,0,0,1,0);
          if (mr.highOrderAxis()==2) axisrot = dmat33(0,1,0,0,0,1,1,0,0);

          for (unsigned isite = 0; isite < rotlist.Size(); isite++)
          {
            //the point to be searched is wrt the pdb orientation
            dmat33 R = rotlist(isite).getMatrix();
            dmat33 ROT = axisrot*R*input.PDB[ROT_MODLID].PR; //don't use ensemble[m] here as it may not have been generated
            rotlist(isite).setMatrix(ROT);
          }

          results.logUnderLine(VERBOSE,"Mean and Standard Deviation");
          results.logTab(1,VERBOSE,"Mean Score:      " + dtos(f_mean,3));
          results.logTab(1,VERBOSE,"Sigma of Scores: " + dtos(f_sigma,3));
          results.logTab(1,VERBOSE,"Highest Score:   " + dtos(Rmax,3));
          if (input.DO_RESCORE_ROT) {
            if (f_sigma) results.logTab(1,VERBOSE,"Highest Z-score: " + dtos(signal(Rmax,f_mean,f_sigma),3));
          } else {
            if (f_sigma) results.logTab(1,SUMMARY,"Highest Z-score: " + dtos(signal(Rmax,f_mean,f_sigma),3));
          }
          results.logTab(1,VERBOSE,"Lowest Score:    " + dtos(Rmin,3));
          if (f_sigma) results.logTab(1,VERBOSE,"Lowest Z-score:  " + dtos(signal(Rmin,f_mean,f_sigma),3));
          results.logBlank(VERBOSE);
          results.logTab(1,VERBOSE,"Normalizing the Fast Rotation Function Scores");
          results.logTab(2,VERBOSE,"Maximum peak of "+dtos(Rmax,3)+" scaled to 100.");
          results.logTab(2,VERBOSE,"Mean "+dtos(f_mean,3)+" scaled to 0.");
          results.logBlank(VERBOSE);
          PHASER_ASSERT(Rmax);
          for (unsigned i = 0; i < rotlist.Size(); i++)
            rotlist.setValue(i,100*(rotlist.getValue(i)-f_mean)/(Rmax-f_mean));
          f_sigma /= (Rmax-f_mean)/100.0;
          f_mean = 0;

          //sort the sites into order
          //rotlist.Sort();
          //rotlist.renumber();
          rotlist.moveToRotationalASU();
          floatType clusterAng(SAMPLING*1.1);
          if (input.PDB.find(ROT_MODLID) != input.PDB.end() && input.PDB[ROT_MODLID].is_helix)
          {
            clusterAng /= 4.; //cheat, instead of turning clustering off, reduce angle: must be less than 2
            results.logTab(1,LOGFILE,"Helix for model: cluster angle reduced");
          }
          rotlist.set_cluster_ang(clusterAng);
          rotlist.set_rescore(false);
          rotlist.set_fast(true);
          data_zscore ZSCORE(false);
          (input.DO_RESCORE_ROT) ?
            rotlist.set_stats(f_mean,rotlist.getTop(),input.PEAKS_ROT.safety_margin().no_cluster(),ZSCORE):
            rotlist.set_stats(f_mean,rotlist.getTop(),input.PEAKS_ROT,ZSCORE);
          for (int t = 0; t < rotlist.Size(); t++)
            rotlist(t).zscore = signal(rotlist.getValue(t),f_mean,f_sigma);
          rotlist.select_and_log(LOGFILE,results,input.NEW_CLUSTERING);
          //rotlist.purgeUnselected();
        }
        else if (input.TARGET_ROT == "BRUTE")
        {
          bool isPseudo(false);
          if (!input.DO_ROTATE_AROUND && !isPseudo)
          {
            results.logTab(1,VERBOSE,"Angles covering the rotational asymmetric unit will be generated");
            rotlist.setupRegion(SAMPLING,results);
          }
          else if (input.DO_ROTATE_AROUND)
          {
            results.logTab(1,VERBOSE,"You have requested a search around " +
                    dtos(input.ROTATE_EULER[0]) + " " +
                    dtos(input.ROTATE_EULER[1]) + " " +
                    dtos(input.ROTATE_EULER[2]) );
            results.logTab(1,VERBOSE,"All points within "+dtos(input.ROTATE_RANGE)+" degrees will be generated");
            rotlist.setupAroundPoint(input.ROTATE_EULER,input.ROTATE_RANGE,SAMPLING);
          }
          else if (isPseudo)
          {
            results.logTab(1,VERBOSE,"All angles will be generated");
            rotlist.setupAllAngles(SAMPLING);
          }
          results.logTab(1,LOGFILE,"Hexagonal search grid has "+itos(rotlist.Size())+" points");
          results.logBlank(LOGFILE);
        }

        if (rotlist.Size() &&
            ((input.TARGET_ROT != "BRUTE" && input.DO_RESCORE_ROT) || (input.TARGET_ROT == "BRUTE")))
        {
          int nthreads = 1;
#pragma omp parallel
          {
#ifdef _OPENMP
            nthreads = omp_get_num_threads();
#endif
          }
          if (input.TARGET_ROT != "BRUTE")
          {
            results.logTab(1,LOGFILE,"Top " + itos(rotlist.numSelected()) + " rotations before clustering will be rescored");
            rotlist.archiveValues();
          }
//do the rescoring
          data_bofac SEARCH_FACTORS(search_bfactors[k],input.SEARCH_FACTORS.OFAC);
          mr.calcSearchVAR(ROT_MODLID,halfR,SEARCH_FACTORS);
          if (nthreads > 1)
            results.logTab(1,LOGFILE,"Spreading calculation onto " +itos( nthreads)+ " threads");
          results.logProgressBarStart(LOGFILE,"Calculating Likelihood for RF #"+itos(ifrf)+" of "+itos(num_RF),rotlist.Size()/nthreads);

// Since interpolation is passed by reference as argument for DataMR::calcSearchROT()
// we need a copy for each call.
#pragma omp parallel for
          for (int isite = 0; isite < rotlist.Size(); isite++)
          {
            int nthread = 0;
#ifdef _OPENMP
            nthread = omp_get_thread_num();
#endif
            DataMR rotmr(mr);
            dmat33 R = rotlist(isite).getMatrix();
            dmat33 ROT = R*input.PDB[input.SEARCH_MODLID[frf]].PR.transpose();
            rotmr.calcSearchROT(ROT,input.SEARCH_MODLID[frf],SEARCH_FACTORS);
            floatType LLgain = rotmr.likelihoodFn();
            rotlist(isite).setValue(LLgain);
            if (nthread == 0) results.logProgressBarNext(LOGFILE);
          } //end #pragma omp parallel
          results.logProgressBarEnd(LOGFILE);
          results.logBlank(VERBOSE);

         //sort the list into new order after rescoring
         // rotlist.Sort();

         //work out mean and standard deviation from random sample
          results.logUnderLine(LOGFILE,"Mean and Standard Deviation");
          const int numSample = 500;
          float1D List;
          if (input.TARGET_ROT == "BRUTE" && (!input.DO_ROTATE_AROUND && rotlist.Size() > numSample))
          {
            results.logTab(1,LOGFILE,"Computed from entire search list of "+itos(rotlist.Size())+" rotations");
            for (int lr=0; lr < rotlist.Size(); lr++)
              List.push_back(rotlist(lr).value);
          }
          else
          {
            List.resize(numSample);
            results.logTab(1,LOGFILE,"Scoring "+itos(numSample) + " randomly sampled rotations");
            // Generate list of random Euler angles outside of parallel code
            dvect31D randomEuler;
            scitbx::random::mersenne_twister generator(0);
            for (unsigned irand = 0; irand < numSample; irand++)
            {
              //alpha and gamma: uniform probability over 0-360
              //gamma: 0-180 with probability proportional to sin(beta)
              floatType alpha = 360*generator.random_double();
              floatType beta = scitbx::rad_as_deg(acos(2*generator.random_double() - 1));
              floatType gamma = 360*generator.random_double();
              randomEuler.push_back(dvect3(alpha,beta,gamma));
            }

            if (nthreads > 1)
              results.logTab(1,LOGFILE,"Spreading calculation onto "+itos(nthreads)+" threads");
            results.logProgressBarStart(LOGFILE,"Generating Statistics for RF #"+itos(ifrf)+" of "+itos(num_RF),numSample/nthreads);

#pragma omp parallel for
            for (int r = 0; r < numSample; r++)
            {
              int nthread = 0;
#ifdef _OPENMP
              nthread = omp_get_thread_num();
#endif
              DataMR rotmr(mr);
              dmat33 R = euler2matrixDEG(randomEuler[r]);
              dmat33 ROT = R*input.PDB[ROT_MODLID].PR.transpose();
              rotmr.calcSearchROT(ROT,ROT_MODLID,SEARCH_FACTORS);
              List[r] = rotmr.likelihoodFn();
              if (nthread == 0) results.logProgressBarNext(LOGFILE);
            } // end pragma omp parallel
            results.logProgressBarEnd(LOGFILE);
          }

          PHASER_ASSERT(List.size());
          floatType r_mean = mean(List);
          floatType r_sigma = sigma(List);
          floatType topRFbefore = rotlist.getTop();
          //check mean is lower than top
          if (r_mean >= topRFbefore)
          {
            int num_better_than_top(0);
            for (int t = 0; t < List.size(); t++)
            {
              if (List[t] > topRFbefore)
              { //case of random site being better than the top
                List[t] = topRFbefore;
                num_better_than_top++;
              }
            }
            r_mean = mean(List); //mean forced lower than top
            r_sigma = sigma(List); //there will still be a sigma
            (num_better_than_top == 1) ?
              results.logTab(1,LOGFILE,"One angle in random selection scored LLG > top RF value!"):
              results.logTab(1,LOGFILE,itos(num_better_than_top) + " angles in random selection scored LLG > top RF value!");
            results.logTab(1,LOGFILE,"There is no signal in the Rotation Function search");
            results.logTab(1,LOGFILE,"Mean and sigma have been adjusted downwards");
            results.logBlank(LOGFILE);
            results.logWarning(SUMMARY,"No Signal in Rotation Function");
            if (r_sigma <= 0) //could happen if all rotations randoms are higher
            throw PhaserError(FATAL,"No signal in rotation function after mean and sigma adjustment");
          }
          LLG_mean_and_sigma_calculated = true; //flag for using the r_mean and m_mean in final selection
          results.logTab(1,VERBOSE,"Mean Score (Sigma):       " + dtos(r_mean,3) + "   (" + dtos(r_sigma,5,2) + ")");
          results.logTab(1,LOGFILE,"Highest Score (Z-score):  " + dtos(topRFbefore,3) + "   (" + dtos(signal(topRFbefore,r_mean,r_sigma),5,2) + ")");
          results.logBlank(LOGFILE);
          all_r_mean.push_back(r_mean);
          for (int t = 0; t < rotlist.Size(); t++) rotlist(t).zscore = signal(rotlist.getValue(t),r_mean,r_sigma);

          if (topRFbefore > llg_f_top && num_RF>1)
          {
            llg_f_top = topRFbefore;
            results.logProgressBarPause(SUMMARY);
            results.logTab(1,SUMMARY,"New Top (ML) Rotation Function Score (RFZ) = " + dtos(llg_f_top,2) + " (RFZ=" + dtos(signal(topRFbefore,r_mean,r_sigma),1) + ") at RF #" + itos(ifrf));
            results.logProgressBarAgain(SUMMARY); //btf
          }
          data_zscore ZSCORE(false);
          rotlist.set_stats(r_mean,rotlist.getTop(),input.PEAKS_ROT,ZSCORE);
          //note: this will still miss some potential sites, because there is some selection in the earlier FRF stage
          if (input.TARGET_ROT == "BRUTE")
          {
            rotlist.set_cluster_ang(SAMPLING*1.1);
            rotlist.set_rescore(false);
            rotlist.set_fast(false);
          }
          else
          {
            rotlist.set_rescore(true);
            rotlist.set_fast(true);
          }
          rotlist.set_rescore(true);
          rotlist.set_fast(true);
          rotlist.select_and_log(LOGFILE,results);
          //rotlist.purgeUnselected();
        }//rescore

        table_strs[frf][k].resize(6);
        for (int z = 0; z < 6; z++) table_strs[frf][k][z] = "---";
        if (rotlist.Size() > 0)
        {
          table_strs[frf][k][0] = dtos(rotlist(0).value,10,2);
          table_strs[frf][k][1] = dtos(rotlist(0).zscore,5,2);
        }
        if (rotlist.Size() > 1)
        {
          table_strs[frf][k][2] = dtos(rotlist(1).value,10,2);
          table_strs[frf][k][3] = dtos(rotlist(1).zscore,5,2);
        }
        if (rotlist.Size() > 2)
        {
          table_strs[frf][k][4] = dtos(rotlist(2).value,10,2);
          table_strs[frf][k][5] = dtos(rotlist(2).zscore,5,2);
        }
        results.logBlank(LOGFILE);

        //sorted on storage
        mr_set MRSET = input.MRSET[k];
               MRSET.MAPCOEFS.clear(); //speed!
        results.storeRotlist(ROT_MODLID,MRSET,rotlist.getEuler(),rotlist.getValues(),rotlist.getSignals(),rotlist.getDeep(),rotlist.getClusters());
        results.logProgressBarNext(SUMMARY);
      } //known
    } //rotation searches
    results.logProgressBarEnd(SUMMARY);
    //the final copy of ensemble below looks redundant but generates instability if it is deleted
    //the beta blip test case goes from 22 sec to 27 seconds with this removed
    input.ensemble = mr.ensPtr(); //store final ensemble

    results.logBlank(LOGFILE);
    results.logTab(1,LOGFILE,"Rotation Function Results");
    results.logTab(1,LOGFILE,"Top1: " + results.top_rlist().logfile());
    results.logBlank(LOGFILE);

    results.set_as_rescored(input.DO_RESCORE_ROT || input.TARGET_ROT == "BRUTE"); //always rescored if SG=1
    for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++)
    {
      results.logUnderLine(SUMMARY,"Rotation Function Table: " + input.SEARCH_MODLID[frf]);
      results.logTab(1,SUMMARY,"(Z-scores from Fast Rotation Function)");
      results.logTabPrintf(1,SUMMARY,"%4s %10s  %5s  %10s  %5s  %10s  %5s\n","#SET","Top","(Z)","Second","(Z)","Third","(Z)");
      for (int k = 0; k < input.MRSET.size(); k++)
        results.logTabPrintf(1,SUMMARY,"%-4d %10s  %5s  %10s  %5s  %10s  %5s\n",k+1,
                              table_strs[frf][k][0].c_str(),table_strs[frf][k][1].c_str(),
                              table_strs[frf][k][2].c_str(),table_strs[frf][k][3].c_str(),
                              table_strs[frf][k][4].c_str(),table_strs[frf][k][5].c_str() );
      //results.logTabPrintf(1,SUMMARY,"%4s %10s  %5s  %10s  %5s  %10s  %5s\n","----","----------","-----", "----------","-----", "----------","-----");
      results.logBlank(SUMMARY);
      if (!input.SEARCH_FACTORS.defaults())
      {
        results.logTab(1,SUMMARY,"B-factor of search component = " + dtos(input.SEARCH_FACTORS.BFAC));
        results.logTab(1,SUMMARY,"Occupancy of search component = " + dtos(input.SEARCH_FACTORS.OFAC));
        results.logBlank(SUMMARY);
      }
    }

    if (LLG_mean_and_sigma_calculated
        && input.PURGE_ROT.ENABLE
        && (input.PEAKS_ROT.by_number() || input.PEAKS_ROT.by_percent() ||
            ( input.PEAKS_ROT.by_sigma() && input.PURGE_ROT.NUMBER)))
    {
      results.logSectionHeader(LOGFILE,"FINAL SELECTION");
      results.logTab(1,SUMMARY,input.PEAKS_ROT.logfile(),false);
      results.logTab(1,SUMMARY,"Top RF = " + dtos(results.top_rlist().RF,3));
      int start_mrset_size(results.numSolutions());
      //int start_mrset_size_deep(results.numSolutions());
      int start_rlist_size(results.numRlist());
      int start_rlist_size_deep(results.numDeep());
      floatType purge_mean = mean(all_r_mean);
      results.storePurgeMean(purge_mean);
      results.logTab(1,SUMMARY,"Purge RF mean = " + dtos(purge_mean,3));
      std::vector<delel> deletion_table = results.finalSelection(input.PURGE_ROT.get_percent(),input.PEAKS_ROT.DOWN,input.PURGE_ROT.NUMBER);
      int final_mrset_size(results.numSolutions());
      int final_rlist_size(results.numRlist());
     // int final_rlist_size_deep(results.numDeep());
      results.logTab(1,SUMMARY,"Number of sets stored before final selection = " + itos(start_mrset_size));
      results.logTab(1,SUMMARY,"Number of solutions stored before final selection = " + itos(start_rlist_size));
      results.logTab(1,SUMMARY,"Number of sets stored (deleted) after final selection = " + itos(final_mrset_size) + " (" + itos(start_mrset_size-final_mrset_size) + ")");
      results.logTab(1,SUMMARY,"Number of solutions stored (deleted) after final selection = " + itos(final_rlist_size) + " (" + itos(start_rlist_size-final_rlist_size) + ")");
      input.PURGE_ROT.NUMBER ?
        results.logTab(1,SUMMARY,"Number used for purge  = " + itos(input.PURGE_ROT.NUMBER)):
        results.logTab(1,SUMMARY,"Percent used for purge = " + dtos((input.PURGE_ROT.get_percent()-input.PEAKS_ROT.DOWN)*100,3) + "%");
      if (input.PEAKS_ROT.DOWN && !input.PURGE_ROT.NUMBER)
      {
        results.logTab(2,SUMMARY,"Includes deep search down percent = " + itos(input.PEAKS_ROT.DOWN*100) + "%");
        results.logTab(2,SUMMARY,"Number of solutions stored above (below) deep threshold = " + itos(results.numDeep()) + " (" + itos(results.not_numDeep()) + ")");
      }
      results.logBlank(SUMMARY);
      if (!input.SEARCH_FACTORS.defaults())
      {
        results.logTab(1,SUMMARY,"B-factor of search component = " + dtos(input.SEARCH_FACTORS.BFAC));
        results.logTab(1,SUMMARY,"Occupancy of search component = " + dtos(input.SEARCH_FACTORS.OFAC));
        results.logBlank(SUMMARY);
      }
      results.logUnderLine(SUMMARY,"Rotation Function Final Selection Table");
      results.logTab(1,SUMMARY,"Rotation list length by SET");
      int total_deep(0);
      for (int k = 0; k < deletion_table.size(); k++) total_deep += deletion_table[k].deep_second;
      if (input.PEAKS_ROT.DOWN && !input.PURGE_ROT.NUMBER)
      {
        results.logTabPrintf(1,SUMMARY,"%4s  %-5s %-5s %s   %5s %-5s %-5s %s\n","SET#","Start","Final","Deleted Set (*)","Deep:","Start","Final","Deleted Set (*)");
        for (int k = 0; k < deletion_table.size(); k++)
          results.logTabPrintf(1,SUMMARY,"%4d  %-5d %-5d       %c%17s%-5d %-5d       %c\n",k+1,deletion_table[k].deep_first,deletion_table[k].deep_second,deletion_table[k].deep_second ? '-' : '*',"",deletion_table[k].first,deletion_table[k].second,deletion_table[k].second ? '-' : '*');
        results.logTabPrintf(1,SUMMARY,"ALL   %-5d %-5d %24s%-5d %-5d\n",start_rlist_size_deep,total_deep,"",start_rlist_size,final_rlist_size);
      }
      else
      {
        results.logTabPrintf(1,SUMMARY,"%4s  %-5s %-5s %s\n","SET#","Start","Final","Deleted Set (*)");
        for (int k = 0; k < deletion_table.size(); k++)
          results.logTabPrintf(1,SUMMARY,"%4d  %-5d %-5d       %c\n",k+1,deletion_table[k].deep_first,deletion_table[k].deep_second,deletion_table[k].deep_second ? '-' : '*');
        results.logTabPrintf(1,SUMMARY,"ALL   %-5d %-5d\n",start_rlist_size_deep,total_deep);
      }
      results.logBlank(SUMMARY);
    }

    {
    std::string loggraph;
    for (int v = 0; v < results.getRF().size(); v++)
      loggraph += itos(v+1) + " " +  dtos(results.getRF()[v],10,2) + " " + dtos(results.getRFZ()[v],5,2) + "\n";
    string1D graphs(2);
             graphs[0] = "RF Number vs LL-gain:AUTO:1,2";
             graphs[1] = "RF Number vs Z-Score:AUTO:1,3";
    stringset search_modlids;
    for (unsigned frf = 0; frf < input.SEARCH_MODLID.size(); frf++)
      search_modlids.insert(input.SEARCH_MODLID[frf]);
    std::string str_modlids;
    for (std::set<std::string>::iterator iter = search_modlids.begin(); iter != search_modlids.end(); iter++)
      str_modlids +=  *iter + " OR ";
    str_modlids = std::string(str_modlids,0,str_modlids.size()-4);
    results.logGraph(LOGFILE,"Rotation Function Component #" + itos(ncomponent) + " (" + str_modlids + ") ",graphs,"Number LLG Z-Score",loggraph);
    }

    the_end:
    outStream where = SUMMARY;
    results.logSectionHeader(where,"OUTPUT FILES");
    if (results.isPackagePhenix()) input.DO_KEYWORDS.Default(false);
    if (input.DO_KEYWORDS.True()) results.writeRlist();
    if (input.DO_XYZOUT.True()) results.writePdb(input.DO_XYZOUT,input.tracemol);
    if (!results.getFilenames().size()) results.logTab(1,where,"No files output");
    else for (int f = 0; f < results.getFilenames().size(); f++) results.logTab(1,where,results.getFilenames()[f]);
    results.logBlank(where);

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultMR_RF runMR_FRF(InputMR_FRF& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runMR_FRF(input,output);
}

} //phaser
