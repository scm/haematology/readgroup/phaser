//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#include <phaser/main/runPhaser.h>
#include <phaser/lib/jiffy.h>
#include <phaser/lib/sg_prior.h>
#include <phaser/lib/scattering.h>
#include <phaser/lib/matthewsProb.h>
#include <phaser/src/Ensemble.h>
#include <phaser/src/Composition.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace phaser {

ResultCCA runCCA(InputCCA& input,Output output)
{
  ResultCCA results(output);
  results.setFiles(input.FILEROOT,input.FILEKILL,input.TIMEKILL);
  results.setLevel(input.OUTPUT_LEVEL);
  results.setMute(input.MUTE_ON);
  bool incoming_bookend = results.getBookend();
  bool success(results.Success());
  if (success && results.isPackagePhenix() && incoming_bookend)
  {
    try {
      input.Analyse(results);
    }
    catch (PhaserError& err) {
    }
    //print to DEF_PHENIX_LOG (otherwise not visible in phenix)
    results.setPhaserError(NULL_ERROR);
    results.logHeader(LOGFILE,header(PREPRO));
    results.logKeywords(SUMMARY,input.Cards());
    results.logTrailer(LOGFILE);
  }
  if (success)
  try
  {
    results.logHeader(LOGFILE,header(CCA));
    input.Analyse(results);

    SpaceGroup sg(input.SG_HALL);
    results.logTabPrintf(1,SUMMARY,"Space-Group Name (Hall Symbol): %s (%s)\n",sg.spcgrpname().c_str(),sg.getHall().c_str());
    results.logTab(1,SUMMARY,"Space-Group Number: " + itos(sg.spcgrpnumber()));

    UnitCell uc(input.UNIT_CELL);
    results.logTabPrintf(1,SUMMARY,"Unit Cell: %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f\n",
         uc.Param(0),uc.Param(1),uc.Param(2),uc.Param(3),uc.Param(4),uc.Param(5));
    results.logBlank(SUMMARY);

    {//memory
      results.logSectionHeader(LOGFILE,"SPACE GROUP ANALYSIS");
      {
      std::string hall = sg.getCctbxSG().type().hall_symbol();
      results.logTab(1,LOGFILE,"Input Space Group: " + SpaceGroup(hall).spcgrpname());
      results.logUnderLine(LOGFILE,"(a) Space groups derived by translation (screw) symmetry");
      results.logTabPrintf(1,LOGFILE,"%-3s %-13s %-s\n","Z","Space Group"," Hall Symbol");
      results.logTabPrintf(1,LOGFILE,"----\n");
      results.logTabPrintf(1,LOGFILE,"%-3i %-13s %-s\n",
         sg.getCctbxSG().order_z(),
         SpaceGroup(hall).spcgrpname().c_str(),
         hall.c_str());
      std::vector<cctbx::sgtbx::space_group> sysabs = groups_sysabs(sg.getCctbxSG());
      sysabs.erase(std::remove(sysabs.begin(),sysabs.end(),sg.getCctbxSG()),sysabs.end());
      for (int t = 0; t < sysabs.size(); t++)
      {
        std::string hall = sysabs[t].type().hall_symbol();
        results.logTabPrintf(1,LOGFILE,"%-3s %-13s %-s\n",
         "",
         SpaceGroup(hall).spcgrpname().c_str(),
         hall.c_str());
      }
      results.logTabPrintf(1,LOGFILE,"----\n");
      }
      std::vector<cctbx::sgtbx::space_group> sglist = subgroups(sg.getCctbxSG());
      if (sglist.size())
      {
        results.logUnderLine(LOGFILE,"(b) Subgroups of space group for perfect twinning expansions");
        results.logTab(1,LOGFILE,"R: Reindexing operation required (*)");
        results.logTab(1,LOGFILE,"Only subgroups related by rotational symmetry are reported");
        results.logTabPrintf(1,LOGFILE,"%-3s %-13s %c %-s\n","Z","Space Group",'R',"Hall Symbol");
        results.logTabPrintf(1,LOGFILE,"----\n");
        int last_order_z(0);
        cctbx::sgtbx::change_of_basis_op cb_op_dat = sg.getCctbxSG().type().cb_op();
        for (int s = 0; s < sglist.size(); s++)
        {
          cctbx::sgtbx::space_group_type sgtype = sglist[s].type();//with reindex
          if (s && last_order_z != sglist[s].order_z()) results.logTabPrintf(1,LOGFILE,"---\n");
          cctbx::sgtbx::change_of_basis_op cb_op_new = sgtype.cb_op();
          cctbx::sgtbx::change_of_basis_op cb_op_dat2new = cb_op_dat.inverse()*cb_op_new;
          try {
            std::string hall = sgtype.hall_symbol();
            std::string hm = SpaceGroup(hall).spcgrpname();
            if (hm == "P 1 ") hm[0] = sg.getCctbxSG().conventional_centring_type_symbol();
            cb_op_dat2new.as_hkl(); //should not throw (rotation test), test is subgroup
            results.logTabPrintf(1,LOGFILE,"%-3s %-13s %c%-s\n",
             (last_order_z != sglist[s].order_z()) ? itos(sglist[s].order_z()).c_str() : "",
             hm.c_str(),
              char(cb_op_dat2new.is_identity_op()?' ':'*'),hall.c_str());
            last_order_z = sglist[s].order_z();
          }
          catch (...) {}
          if (s == sglist.size()-1) results.logTabPrintf(1,LOGFILE,"----\n");
        }
      }
      results.logBlank(LOGFILE);
    }//memory

    //molecular weight has to be back-calculated from the scattering
    Composition composition(input.COMPOSITION);
    if (!composition.total_scat(input.SG_HALL,input.UNIT_CELL))
    throw PhaserError(FATAL,"No composition entered for cell content analysis");

    std::string CompType;
    if (composition.total_scat(input.SG_HALL,input.UNIT_CELL,"PROTEIN") &&
        composition.total_scat(input.SG_HALL,input.UNIT_CELL,"NUCLEIC"))
      CompType = "MIXED";
    else if (composition.total_scat(input.SG_HALL,input.UNIT_CELL,"PROTEIN"))
      CompType = "PROTEIN";
    else CompType = "NUCLEIC";

    results.logTab(1,SUMMARY,"Composition is of type: " +  CompType);
    floatType uc_volume = uc.getCctbxUC().volume();
    floatType sg_nsymm = sg.NSYMM;
    floatType assemblyMw = composition.total_mw(input.SG_HALL,input.UNIT_CELL,CompType);
    results.logTabPrintf(1,SUMMARY,"MW to which Matthews applies: %5.0f\n",assemblyMw);
    af::shared<floatType> VM,probVM;
    af::shared<int> Z;
    results.setSpaceGroup(input.SG_HALL);
    results.setUnitCell(input.UNIT_CELL);
    if (assemblyMw > 1)
    {
      floatType minVM(1.23); //solvent content 0 for specific gravity of 0.74 cm^3/gm
      floatType maxVM(10);
      unsigned maxz(1000);
      for (unsigned z = 1; z <= maxz; z++) //so that there isn't an infinite loop
      {
        floatType MW(z*assemblyMw);
        floatType thisVM(uc_volume/(sg_nsymm*MW));
        if (thisVM >= maxVM) continue; //very high solvent content
        if (thisVM <= minVM) break; //very low solvent content - end
        VM.push_back(thisVM);
        probVM.push_back(matthewsProb(input.HIRES,thisVM,CompType));
        Z.push_back(z);
        if (z == maxz)
          results.logTab(1,SUMMARY,"Z value of " + itos(maxz) + " reached - no more considered");
      }

      floatType maxProb(0);
      int imaxProb(0);
      if (VM.size() == 0)
      {
        floatType thisVM(uc_volume/(sg_nsymm*assemblyMw));
        floatType thisContent(1.23/thisVM*100);
        floatType sc(100-thisContent);
        if (sc < 0)
        {
          results.setFitError();
          if (results.isMute())
            return results; //success
          else
            throw PhaserError(FATAL,"The composition entered will not fit in the unit cell volume");
        }
      }
      for (int i = 0; i < probVM.size(); i++) // Find max probability
        if (probVM[i] > maxProb) maxProb = probVM[i], imaxProb = i;
      for (int i = 0; i < probVM.size(); i++) // Renormalise to max
        probVM[i] /= maxProb;

      results.logTabPrintf(1,SUMMARY,"Resolution for Matthews calculation: %5.2f\n",input.HIRES);
      results.logBlank(SUMMARY);
      results.logTabPrintf(1,SUMMARY,"%-7s %-10s %-5s %-10s %-10s\n","Z","MW","VM","% solvent","rel. freq.");
      for (int i = 0; i < Z.size(); i++)
        if (i != imaxProb)
          results.logTabPrintf(1,SUMMARY,"%-7d %-10.0f %-5.2f %-10.2f %-10.3f\n",
            Z[i],Z[i]*assemblyMw,VM[i],(1-1.23/VM[i])*100,probVM[i]);
        else
          results.logTabPrintf(1,SUMMARY,"%-7d %-10.0f %-5.2f %-10.2f %-10.3f  <== most probable\n",
            Z[i],Z[i]*assemblyMw,VM[i],(1-1.23/VM[i])*100,probVM[i]);
      results.logBlank(SUMMARY);
      results.logTab(1,SUMMARY,"Z is the number of multiples of the total composition");
      results.logTab(1,SUMMARY,"In most cases the most probable Z value should be 1");
      results.logTab(1,SUMMARY,"If it is not 1, you may need to consider other compositions");
      results.logBlank(SUMMARY);

      results.logUnderLine(LOGFILE,"Histogram of relative frequencies of VM values");
      results.logTab(1,LOGFILE,"Frequency of most common VM value normalized to 1");
      floatType vm_inv_inc(0.02),vm_min(0.1);
      results.logTab(1,LOGFILE,"VM values plotted in increments of 1/VM (" + dtos(vm_inv_inc) + ")");
      results.logBlank(LOGFILE);
      floatType grad(0.1);
      results.logTab(1,LOGFILE,"     <--- relative frequency --->");
      results.logTabPrintf(1,LOGFILE,"     %-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f%-5.1f\n",
                grad*0,grad*1,grad*2,grad*3,grad*4,grad*5,grad*6,grad*7,grad*8,grad*9,grad*10);
      results.logTabPrintf(1,LOGFILE,"     %-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s%-5s\n",
               "|","|","|","|","|","|","|","|","|","|","|");
      std::string loggraph;
      for (floatType vm_inv = vm_min; 1/vm_inv > minVM; vm_inv += vm_inv_inc)
      {
        floatType vm(1/vm_inv);
        std::string stars;
        floatType prob(matthewsProb(input.HIRES,vm,CompType));
        std::string h_char("-");
        for (unsigned i = 0; i < VM.size(); i++)
          if (1/VM[i] >= vm_inv-vm_inv_inc && 1/VM[i] < vm_inv )
            h_char = "*";
        for (int star = 0; star < prob*50; star++)
          stars += h_char;
        for (unsigned i = 0; i < VM.size(); i++)
          if (1/VM[i] >= vm_inv-vm_inv_inc && 1/VM[i] < vm_inv )
          {
            stars += " (COMPOSITION*" + itos(Z[i]) + ")";
            loggraph += itos(Z[i]) + " " + dtos(prob) + "\n";
          }
        results.logTabPrintf(1,LOGFILE,"%5.2f %s\n",vm,stars.c_str());
      }
      results.logBlank(LOGFILE);
      string1D graphs(1);
               graphs[0] = "N*Composition vs Probability:0|" + itos(Z.back()+1) +"x0|1:1,2";
      results.logGraph(LOGFILE,"Cell Content Analysis",graphs,"N*Composition Probability",loggraph,true);
      {
        floatType MW(1*assemblyMw);
        floatType thisVM(uc_volume/(sg.NSYMM*MW));
        floatType sc(100-std::ceil(1.23/thisVM*100));
        if (thisVM > maxVM)
          results.logWarning(SUMMARY,"The composition you have entered gives a solvent content of " +
           dtos(sc) + "%, which is extremely unlikely. You should increase the composition.");
      }
      results.setZ(assemblyMw,Z,VM,probVM);
    }

    {//memory
      floatType VM(matthewsVM(input.HIRES,CompType));
      results.logBlank(SUMMARY);
      results.logTab(1,SUMMARY,"Most probable VM for resolution = " + dtos(VM));
      floatType uc_volume = uc.getCctbxUC().volume();
      floatType sg_nsymm = sg.NSYMM;
      floatType MW(uc_volume/(sg_nsymm*VM));
      results.logTab(1,SUMMARY,"Most probable MW of protein in asu for resolution = " + dtos(MW));
      results.logBlank(SUMMARY);
      results.setOptimal(MW,VM);
    }//memory


    if (results.getFilenames().size())
    {
      outStream where = SUMMARY;
      results.logSectionHeader(where,"OUTPUT FILES");
      for (int f = 0; f < results.getFilenames().size(); f++)
        results.logTab(1,where,results.getFilenames()[f]);
      results.logBlank(where);
    }

    results.logTrailer(LOGFILE);
  }
  catch (PhaserError& err) {
    results.logError(LOGFILE,err);
  }
  catch (std::bad_alloc const& err) {
    results.logError(LOGFILE,PhaserError(MEMORY,err.what()));
  }
  catch (std::exception const& err) {
    results.logError(LOGFILE,PhaserError(UNHANDLED,err.what()));
  }
  catch (...) {
    results.logError(LOGFILE,PhaserError(UNKNOWN));
  }
  results.logTerminate(LOGFILE,incoming_bookend);
  return results;
}

//for python
ResultCCA runCCA(InputCCA& input)
{
  Output output; //blank
  output.setPackagePhenix();
  return runCCA(input,output);
}

} //end namespace phaser
