//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>
#include <phaser/keywords/keywords.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void wrap_keywords()
{
  using namespace boost::python;

// Base Keyword Classes

  class_<ATOM>("ATOM", init<>())
    .def("setATOM_IOTBX", &ATOM::setATOM_IOTBX)
    .def("setATOM_PDB", &ATOM::setATOM_PDB)
    .def("setATOM_HA", &ATOM::setATOM_HA)
    .def("addATOM", &ATOM::addATOM)
    .def("addATOM_FULL", &ATOM::addATOM_FULL)
    .def("addATOM_FAST", &ATOM::addATOM_FAST)
    .def("setATOM_CHAN_BFAC_WILS", &ATOM::setATOM_CHAN_BFAC_WILS)
    .def("setATOM_CHAN_SCAT", &ATOM::setATOM_CHAN_SCAT)
    .def("setATOM_CHAN_ORIG", &ATOM::setATOM_CHAN_ORIG)
    .def("setATOM_CHAN_SCAT_TYPE", &ATOM::setATOM_CHAN_SCAT_TYPE)
    .def("setATOM", &ATOM::setATOM)
  ;

  class_<BFAC>("BFAC", init<>())
    .def("setBFAC_WILS_REST", &BFAC::setBFAC_WILS_REST)
    .def("setBFAC_SPHE_REST", &BFAC::setBFAC_SPHE_REST)
    .def("setBFAC_REFI_REST", &BFAC::setBFAC_REFI_REST)
    .def("setBFAC_WILS_SIGM", &BFAC::setBFAC_WILS_SIGM)
    .def("setBFAC_SPHE_SIGM", &BFAC::setBFAC_SPHE_SIGM)
    .def("setBFAC_REFI_SIGM", &BFAC::setBFAC_REFI_SIGM)
    .def("setDRMS_REFI_SIGM", &BFAC::setDRMS_REFI_SIGM)
    .def("setDRMS_REFI_REST", &BFAC::setDRMS_REFI_REST)
  ;

  class_<BINS>("BINS", init<>())
    .def("setBINS_DATA_MINI", &BINS::setBINS_DATA_MINI)
    .def("setBINS_DATA_MAXI", &BINS::setBINS_DATA_MAXI)
    .def("setBINS_DATA_WIDT", &BINS::setBINS_DATA_WIDT)
  ;

  class_<BOXS>("BOXS", init<>())
    .def("setBOXS", &BOXS::setBOXS)
  ;

  class_<CELL>("CELL", init<>())
    .def("setCELL", &CELL::setCELL)
    .def("setCELL6", &CELL::setCELL6)
  ;

  class_<CLUS>("CLUS", init<>())
    .def("addCLUS_PDB", &CLUS::addCLUS_PDB,
         (arg("id"),arg("pdbfile")))
    .def("setCLUS_PDB", &CLUS::setCLUS_PDB)
  ;

  class_<COMP>("COMP", init<>())
    .def("setCOMP_AVER", &COMP::setCOMP_AVER)
    .def("setCOMP_SOLV", &COMP::setCOMP_SOLV)
    .def("setCOMP_ASU", &COMP::setCOMP_ASU)
    .def("setCOMP_BY", &COMP::setCOMP_BY)
    .def("setCOMP_MINI_SOLV", &COMP::setCOMP_MINI_SOLV)
    .def("setCOMP_PERC", &COMP::setCOMP_PERC)
    .def("addCOMP_PROT_MW_NUM", &COMP::addCOMP_PROT_MW_NUM)
    .def("addCOMP_PROT_SEQ_NUM", &COMP::addCOMP_PROT_SEQ_NUM)
    .def("addCOMP_PROT_NRES_NUM", &COMP::addCOMP_PROT_NRES_NUM)
    .def("addCOMP_PROT_STR_NUM", &COMP::addCOMP_PROT_STR_NUM)
    .def("addCOMP_NUCL_MW_NUM", &COMP::addCOMP_NUCL_MW_NUM)
    .def("addCOMP_NUCL_SEQ_NUM", &COMP::addCOMP_NUCL_SEQ_NUM)
    .def("addCOMP_NUCL_NRES_NUM", &COMP::addCOMP_NUCL_NRES_NUM)
    .def("addCOMP_NUCL_STR_NUM", &COMP::addCOMP_NUCL_STR_NUM)
    .def("addCOMP_ATOM_NUM", &COMP::addCOMP_ATOM_NUM)
  ;


  class_<CRYS>("CRYS", init<>())
    .def("addCRYS_ANOM_LABI", &CRYS::addCRYS_ANOM_LABI)
    .def("addCRYS_MEAN_LABI", &CRYS::addCRYS_MEAN_LABI)
    .def("addCRYS_ANOM_LABI_I", &CRYS::addCRYS_ANOM_LABI_I)
    .def("addCRYS_MEAN_LABI_I", &CRYS::addCRYS_MEAN_LABI_I)
    .def("setCRYS_LABI_F", &CRYS::setCRYS_LABI_F)
    .def("setCRYS_LABI_SIGF", &CRYS::setCRYS_LABI_SIGF)
    .def("setCRYS_LABI_I", &CRYS::setCRYS_LABI_I)
    .def("setCRYS_LABI_SIGI", &CRYS::setCRYS_LABI_SIGI)
    .def("setCRYS_LABI_FPOS", &CRYS::setCRYS_LABI_FPOS)
    .def("setCRYS_LABI_FNEG", &CRYS::setCRYS_LABI_FNEG)
    .def("setCRYS_LABI_SIGFPOS", &CRYS::setCRYS_LABI_SIGFPOS)
    .def("setCRYS_LABI_SIGFNEG", &CRYS::setCRYS_LABI_SIGFNEG)
    .def("setCRYS_LABI_IPOS", &CRYS::setCRYS_LABI_IPOS)
    .def("setCRYS_LABI_INEG", &CRYS::setCRYS_LABI_INEG)
    .def("setCRYS_LABI_SIGIPOS", &CRYS::setCRYS_LABI_SIGIPOS)
    .def("setCRYS_LABI_SIGINEG", &CRYS::setCRYS_LABI_SIGINEG)
    .def("setCRYS_DATA", &CRYS::setCRYS_DATA)
    .def("setCRYS_MILLER", &CRYS::setCRYS_MILLER)
    .def("addCRYS_ANOM_DATA", &CRYS::addCRYS_ANOM_DATA)
    .def("setCRYS_SAD_DATA", &CRYS::setCRYS_SAD_DATA)
  ;

  class_<EIGE>("EIGE", init<>())
    .def("setEIGE_READ", &EIGE::setEIGE_READ)
    .def("setEIGE_WRIT", &EIGE::setEIGE_WRIT)
  ;

  class_<ELLG>("ELLG", init<>())
    .def("setELLG_TARG", &ELLG::setELLG_TARG)
    .def("setELLG_RMSD_MULT", &ELLG::setELLG_RMSD_MULT)
  ;

  class_<ENSE>("ENSE", init<>())
    .def("addENSE_PDB_RMS", &ENSE::addENSE_PDB_RMS,
      (arg("modlid"), arg("pdbfile"), arg("rms"), arg("pdb_not_cif")=true)
      )
    .def("addENSE_PDB_ID", &ENSE::addENSE_PDB_ID,
      (arg("modlid"), arg("pdbfile"), arg("identity"), arg("pdb_not_cif")=true)
      )
    .def("addENSE_PDB_ID_SELECT", &ENSE::addENSE_PDB_ID_SELECT,
      (arg("modlid"), arg("pdbfile"), arg("identity"), arg("pdb_not_cif"), arg("chain"), arg("serial")=-999)
      )
    .def("addENSE_CIF_RMS", &ENSE::addENSE_CIF_RMS)
    .def("addENSE_CIT_ID", &ENSE::addENSE_CIF_ID)
    .def("addENSE_PDB_CARD", &ENSE::addENSE_PDB_CARD)
    .def("addENSE_MAP", &ENSE::addENSE_MAP)
    .def(
      "addENSE_STR_RMS",
      &ENSE::addENSE_STR_RMS_PTGR,
      ( arg( "modlid" ), arg( "content" ), arg( "name" ), arg( "rms" ),
        arg( "ptgrp_tolerances" ) )
      )
    .def(
      "addENSE_STR_ID",
      &ENSE::addENSE_STR_ID_PTGR,
      ( arg( "modlid" ), arg( "content" ), arg( "name" ), arg( "identity" ),
        arg( "ptgrp_tolerances" ) )
      )
    .def(
      "addENSE_STR_RMS",
      &ENSE::addENSE_STR_RMS,
      ( arg( "modlid" ), arg( "content" ), arg( "name" ), arg( "rms" ) )
      )
    .def(
      "addENSE_STR_ID",
      &ENSE::addENSE_STR_ID,
      ( arg( "modlid" ), arg( "content" ), arg( "name" ), arg( "identity" ) )
      )
    .def("setENSE_DISA_CHEC", &ENSE::setENSE_DISA_CHEC)
    .def("setENSE_HETA", &ENSE::setENSE_HETA)
    .def("addENSE_HELI", &ENSE::addENSE_HELI)
    .def("addENSE_ATOM", &ENSE::addENSE_ATOM)
    .def_readonly( "PDB", &ENSE::PDB )
    .def("setENSE_PTGR_COVE", &ENSE::setENSE_PTGR_COVE)
    .def("setENSE_PTGR_IDEN", &ENSE::setENSE_PTGR_IDEN)
    .def("setENSE_PTGR_RMSD", &ENSE::setENSE_PTGR_RMSD)
    .def("setENSE_PTGR_TOLA", &ENSE::setENSE_PTGR_TOLA)
    .def("setENSE_PTGR_TOLS", &ENSE::setENSE_PTGR_TOLS)
    .def("setENSE_BINS_MINI", &ENSE::setENSE_BINS_MINI)
    .def("setENSE_BINS_MAXI", &ENSE::setENSE_BINS_MAXI)
    .def("setENSE_BINS_WIDT", &ENSE::setENSE_BINS_WIDT)
    .def("setENSE_ESTI", &ENSE::setENSE_ESTI)
    .def("setENSE_BFAC_ZERO", &ENSE::setENSE_BFAC_ZERO)
    .def("setENSE_TRAC_SAMP_MIN",  &ENSE::setENSE_TRAC_SAMP_MIN)
    .def("setENSE_TRAC_SAMP_DIST", &ENSE::setENSE_TRAC_SAMP_DIST)
    .def("setENSE_TRAC_SAMP_TARG", &ENSE::setENSE_TRAC_SAMP_TARG)
    .def("setENSE_TRAC_SAMP_RANG", &ENSE::setENSE_TRAC_SAMP_RANG)
    .def("setENSE_TRAC_SAMP_USE",  &ENSE::setENSE_TRAC_SAMP_USE)
    .def("setENSE_TRAC_SAMP_WANG", &ENSE::setENSE_TRAC_SAMP_WANG)
    .def("setENSE_TRAC_SAMP_ASA",  &ENSE::setENSE_TRAC_SAMP_ASA)
    .def("setENSE_TRAC_SAMP_NCYC", &ENSE::setENSE_TRAC_SAMP_NCYC)
    .def("setENSE_MAP_PDB", &ENSE::setENSE_MAP_PDB)
  ;

  class_<FFTS>("FFTS", init<>())
    .def("setFFTS_MIN", &FFTS::setFFTS_MIN)
    .def("setFFTS_MAX", &FFTS::setFFTS_MAX)
    .def("setFFTS_MIN_MAX", &FFTS::setFFTS_MIN_MAX)
  ;

  class_<FIND>("FIND", init<>())
    .def("setFIND_CLUS", &FIND::setFIND_CLUS)
    .def("setFIND_SCAT", &FIND::setFIND_SCAT)
    .def("setFIND_NUMB", &FIND::setFIND_NUMB)
    .def("setFIND_PURG_SELE", &FIND::setFIND_PURG_SELE)
    .def("setFIND_PURG_CUTO", &FIND::setFIND_PURG_CUTO)
    .def("setFIND_PURG_OCCU", &FIND::setFIND_PURG_OCCU)
    .def("setFIND_PEAK_SELE", &FIND::setFIND_PEAK_SELE)
    .def("setFIND_PEAK_CUTO", &FIND::setFIND_PEAK_CUTO)
    .def("setFIND_PEAK_CLUS", &FIND::setFIND_PEAK_CLUS)
    .def("setFIND_DIST_CENT", &FIND::setFIND_DIST_CENT)
    .def("setFIND_DIST_DUPL", &FIND::setFIND_DIST_DUPL)
  ;

  class_<FORM>("FORM", init<>())
    .def("setFORM", &FORM::setFORM)
  ;

  class_<HAND>("HAND", init<>())
    .def("setHAND", &HAND::setHAND)
  ;

  class_<HKLI>("HKLI", init<>())
    .def("setHKLI", &HKLI::setHKLI)
  ;

  class_<HKLO>("HKLO", init<>())
    .def("setHKLO", &HKLO::setHKLO)
  ;

  class_<INFO>("INFO", init<>())
    .def("setINFO", &INFO::setINFO)
  ;

  class_<JOBS>("JOBS", init<>())
    .def("setJOBS", &JOBS::setJOBS)
  ;

  class_<KEYW>("KEYW", init<>())
    .def("setKEYW", &KEYW::setKEYW)
  ;

  class_<KILL>("KILL", init<>())
    .def("setKILL_FILE", &KILL::setKILL_FILE)
    .def("setKILL_TIME", &KILL::setKILL_TIME)
  ;

  class_<LABI>("LABI", init<>())
    .def("setLABI_F", &LABI::setLABI_F)
    .def("setLABI_SIGF", &LABI::setLABI_SIGF)
    .def("setLABI_I", &LABI::setLABI_I)
    .def("setLABI_SIGI", &LABI::setLABI_SIGI)
    .def("setLABI_FMAP", &LABI::setLABI_FMAP)
    .def("setLABI_PHMA", &LABI::setLABI_PHMA)
    .def("setLABI_FOM", &LABI::setLABI_FOM)
    .def("setLABI_FWT", &LABI::setLABI_FWT)
    .def("setLABI_PHWT", &LABI::setLABI_PHWT)
    .def("setLABI_FMAP_PHMAP_FOM", &LABI::setLABI_FMAP_PHMAP_FOM)
    .def("setLABI_FWT_PHWT", &LABI::setLABI_FWT_PHWT)
    .def("setLABI_F_SIGF", &LABI::setLABI_F_SIGF)
    .def("setLABI_I_SIGI", &LABI::setLABI_I_SIGI)
//break tradition and set no-parsing keywords with different root on keyword class
    .def("setREFL_DATA", &LABI::setREFL_DATA)
    .def("setDATA_REFL", &LABI::setREFL_DATA)
    .def("setREFL_F_SIGF", &LABI::setREFL_F_SIGF)
    .def("setREFL_I_SIGI", &LABI::setREFL_I_SIGI)
    .def("setREFL_FMAP_PHMAP_FOM", &LABI::setREFL_FMAP_PHMAP_FOM)
    .def("setREFL_FWT_PHWT", &LABI::setREFL_FWT_PHWT)
  ;

  class_<LLGC>("LLGC", init<>())
    .def("setLLGC_COMP", &LLGC::setLLGC_COMP)
    .def("setLLGC_CLAS", &LLGC::setLLGC_CLAS)
    .def("setLLGC_SIGM", &LLGC::setLLGC_SIGM)
    .def("setLLGC_TOP", &LLGC::setLLGC_TOP)
    .def("setLLGC_NCYC", &LLGC::setLLGC_NCYC)
    .def("addLLGC_SCAT", &LLGC::addLLGC_SCAT)
    .def("addLLGC_ANOM", &LLGC::addLLGC_ANOM)
    .def("addLLGC_REAL", &LLGC::addLLGC_REAL)
    .def("addLLGC_ELEM", &LLGC::addLLGC_SCAT)
    .def("addLLGC_CLUS", &LLGC::addLLGC_SCAT)
    .def("setLLGC_METH", &LLGC::setLLGC_METH)
    .def("setLLGC_HOLE", &LLGC::setLLGC_HOLE)
  ;

  class_<LLGM>("LLGM", init<>())
    .def("setLLGM", &LLGM::setLLGM)
  ;

  class_<MACA>("MACA", init<>())
    .def("addMACA", &MACA::addMACA,
         (arg("ref_anis"),arg("ref_bins"),arg("ref_solk"),arg("ref_solb"),arg("ncyc"),arg("minimizer")))
    .def("setMACA_PROT", &MACA::setMACA_PROT)
  ;

  class_<MACJ>("MACJ", init<>())
    .def("addMACJ", &MACJ::addMACJ)
  ;

  class_<MACM>("MACM", init<>())
   .def("addMACM", &MACM::addMACM,
       (arg("ref_rota"),
        arg("ref_tran"),
        arg("ref_bfac"),
        arg("ref_vrms"),
        arg("ref_cell") = bool(DEF_MACM_REF_CELL),
        arg("ref_ofac") = bool(DEF_MACM_REF_OFAC),
        arg("last_only") = bool(DEF_MACM_LAST_ONLY),
        arg("ncyc") = int(DEF_MACM_NCYC),
        arg("minimizer") = std::string(DEF_MACM_MINI)))
    .def("setMACM_PROT", &MACM::setMACM_PROT)
    .def("setMACM_CHAI", &MACM::setMACM_CHAI)
    .def("setMACM_LAST", &MACM::setMACM_LAST)
  ;

  class_<MACO>("MACO", init<>())
   .def("addMACO", &MACO::addMACO,
       (arg("ncyc") = int(DEF_MACO_NCYC),
        arg("minimizer") = std::string(DEF_MACO_MINI)))
    .def("setMACO_PROT", &MACO::setMACO_PROT)
  ;

  class_<MACT>("MACT", init<>())
    .def("addMACT", &MACT::addMACT,
         (arg("ref_rota"),arg("ref_tran"),arg("ref_vrms"),arg("ncyc"),arg("minimizer")))
    .def("setMACT_PROT", &MACT::setMACT_PROT)
  ;

  class_<MACS>("MACS", init<>())
    .def("addMACS", &MACS::addMACS,
         (arg("ref_pk"),arg("ref_pb"),
          arg("ref_xyz"), arg("ref_occ"),arg("ref_bfac"),arg("ref_fdp"),
          arg("ref_sa"),arg("ref_sb"),arg("ref_sp"),arg("ref_sd"),
          arg("ncyc"),arg("target"),arg("minimizer")))
    .def("setMACS_PROT", &MACS::setMACS_PROT)
  ;

  class_<MACG>("MACG", init<>())
    .def("setMACG_PROT", &MACG::setMACG_PROT, (arg("method")))
    .def("setMACG_SIGR", &MACG::setMACG_SIGR)
    .def("setMACG_SIGT", &MACG::setMACG_SIGT)
    .def("setMACG_ANCH", &MACG::setMACG_ANCH)
    .def("addMACG_FULL", &MACG::addMACG_FULL)
    .def("addMACG", &MACG::addMACG,
       (arg("ref_rot"),arg("ref_tra"),arg("ref_vrms")))
  ;

  class_<MUTE>("MUTE", init<>())
    .def("setMUTE", &MUTE::setMUTE)
  ;

  class_<SCED>("SCED", init<>())
    .def("setSCED_WEIG_EQUA", &SCED::setSCED_WEIG_EQUA)
    .def("setSCED_WEIG_SPHE", &SCED::setSCED_WEIG_SPHE)
    .def("setSCED_WEIG_DENS", &SCED::setSCED_WEIG_DENS)
    .def("setSCED_WEIG_CONT", &SCED::setSCED_WEIG_CONT)
    .def("setSCED_NDOM", &SCED::setSCED_NDOM)
  ;

  class_<DDM>("DDM", init<>())
    .def("setDDM_SLID", &DDM::setDDM_SLID)
    .def("setDDM_DIST_STEP", &DDM::setDDM_DIST_STEP)
    .def("setDDM_DIST_MIN", &DDM::setDDM_DIST_MIN)
    .def("setDDM_DIST_MAX", &DDM::setDDM_DIST_MAX)
    .def("setDDM_JOIN_MIN", &DDM::setDDM_JOIN_MIN)
    .def("setDDM_JOIN_MAX", &DDM::setDDM_JOIN_MAX)
    .def("setDDM_SEQU_MIN", &DDM::setDDM_SEQU_MIN)
    .def("setDDM_SEQU_MAX", &DDM::setDDM_SEQU_MAX)
    .def("setDDM_SEPA_MIN", &DDM::setDDM_SEPA_MIN)
    .def("setDDM_SEPA_MAX", &DDM::setDDM_SEPA_MAX)
    .def("setDDM_CLUS",     &DDM::setDDM_CLUS)
  ;

  class_<ENM>("ENM", init<>())
    .def("setENM_OSCI", &ENM::setENM_OSCI)
    .def("setENM_RTB_NRES", &ENM::setENM_RTB_NRES)
    .def("setENM_RTB_MAXB", &ENM::setENM_RTB_MAXB)
    .def("setENM_RADI", &ENM::setENM_RADI)
    .def("setENM_FORC", &ENM::setENM_FORC)
  ;

  class_<NMA>("NMA", init<>())
    .def("setNMA_COMB", &NMA::setNMA_COMB)
    .def("addNMA_MODE", &NMA::addNMA_MODE)
    .def("setNMA_ORIG", &NMA::setNMA_ORIG)
  ;

  class_<PERT>("PERT", init<>())
    .def("setPERT_RMS_STEP", &PERT::setPERT_RMS_STEP)
    .def("setPERT_RMS_MAXI", &PERT::setPERT_RMS_MAXI)
    .def("setPERT_RMS_DIRE", &PERT::setPERT_RMS_DIRE)
    .def("addPERT_DQ", &PERT::addPERT_DQ)
    .def("setPERT_INCR", &PERT::setPERT_INCR)
  ;

  class_<NORM>("NORM", init<>())
    .def("setNORM_DATA", &NORM::setNORM_DATA)
  ;

  class_<OCCU>("OCCU", init<>())
    .def("setOCCU_WIND_ELLG", &OCCU::setOCCU_WIND_ELLG)
    .def("setOCCU_WIND_NRES", &OCCU::setOCCU_WIND_NRES)
    .def("setOCCU_WIND_MAX", &OCCU::setOCCU_WIND_MAX)
    .def("setOCCU_MAX", &OCCU::setOCCU_MAX)
    .def("setOCCU_MIN", &OCCU::setOCCU_MIN)
    .def("setOCCU_MERG", &OCCU::setOCCU_MERG)
    .def("setOCCU_FRAC", &OCCU::setOCCU_FRAC)
    .def("setOCCU_OFFS", &OCCU::setOCCU_OFFS)
    .def("setOCCU_BIAS", &OCCU::setOCCU_BIAS)
  ;

  class_<OUTL>("OUTL", init<>())
    .def("setOUTL_REJE", &OUTL::setOUTL_REJE)
    .def("setOUTL_PROB", &OUTL::setOUTL_PROB)
    .def("setOUTL_INFO", &OUTL::setOUTL_INFO)
  ;

  class_<PACK>("PACK", init<>())
    .def("setPACK_SELE", &PACK::setPACK_SELE)
    .def("setPACK_QUIC", &PACK::setPACK_QUIC)
    .def("setPACK_COMP", &PACK::setPACK_COMP)
    .def("setPACK_CUTO", &PACK::setPACK_CUTO)
    .def("setPACK_KEEP_HIGH_TFZ", &PACK::setPACK_KEEP_HIGH_TFZ)
  ;

  class_<PART>("PART", init<>())
    .def("setPART_PDB", &PART::setPART_PDB)
    .def("setPART_HKLI", &PART::setPART_HKLI)
    .def("setPART_VARI", &PART::setPART_VARI)
    .def("setPART_DEVI", &PART::setPART_DEVI)
    .def("setPART_IOTBX", &PART::setPART_IOTBX)
    .def("setPART_LABI_FC", &PART::setPART_LABI_FC)
    .def("setPART_LABI_PHIC", &PART::setPART_LABI_PHIC)
  ;

  class_<PEAK>("PEAK", init<>())
    .def("setPEAK_ROTA_SELE", &PEAK::setPEAK_ROTA_SELE)
    .def("setPEAK_TRAN_SELE", &PEAK::setPEAK_TRAN_SELE)
    .def("setPEAK_ROTA_CLUS", &PEAK::setPEAK_ROTA_CLUS)
    .def("setPEAK_TRAN_CLUS", &PEAK::setPEAK_TRAN_CLUS)
    .def("setPEAK_ROTA_CUTO", &PEAK::setPEAK_ROTA_CUTO)
    .def("setPEAK_TRAN_CUTO", &PEAK::setPEAK_TRAN_CUTO)
    .def("setPEAK_ROTA_DOWN", &PEAK::setPEAK_ROTA_DOWN)
  ;

  class_<PERM>("PERM", init<>())
    .def("setPERM", &PERM::setPERM)
  ;

  class_<PURG>("PURG", init<>())
    .def("setPURG_ROTA_ENAB", &PURG::setPURG_ROTA_ENAB)
    .def("setPURG_ROTA_PERC", &PURG::setPURG_ROTA_PERC)
    .def("setPURG_ROTA_NUMB", &PURG::setPURG_ROTA_NUMB)
    .def("setPURG_TRAN_ENAB", &PURG::setPURG_TRAN_ENAB)
    .def("setPURG_TRAN_PERC", &PURG::setPURG_TRAN_PERC)
    .def("setPURG_TRAN_NUMB", &PURG::setPURG_TRAN_NUMB)
    .def("setPURG_RNP_ENAB", &PURG::setPURG_RNP_ENAB)
    .def("setPURG_RNP_PERC", &PURG::setPURG_RNP_PERC)
    .def("setPURG_RNP_NUMB", &PURG::setPURG_RNP_NUMB)
    .def("setPURG_GYRE_ENAB", &PURG::setPURG_GYRE_ENAB)
    .def("setPURG_GYRE_PERC", &PURG::setPURG_GYRE_PERC)
    .def("setPURG_GYRE_NUMB", &PURG::setPURG_GYRE_NUMB)
  ;

  class_<RESC>("RESC", init<>())
    .def("setRESC_ROTA", &RESC::setRESC_ROTA)
    .def("setRESC_TRAN", &RESC::setRESC_TRAN)
  ;

  class_<RESH>("RESH", init<>())
    .def("setRESH_PERC", &RESH::setRESH_PERC)
  ;

  class_<RESO>("RESO", init<>())
    .def("setRESO", &RESO::setRESO)
    .def("setHIRES", &RESO::setRESO_HIGH) //backwards compatibility
    .def("setRESO_HIGH", &RESO::setRESO_HIGH)
    .def("setRESO_LOW", &RESO::setRESO_LOW)
    .def("setRESO_AUTO_HIGH", &RESO::setRESO_AUTO_HIGH)
    .def("setRESO_AUTO_OFF", &RESO::setRESO_AUTO_OFF)
  ;

  class_<RFAC>("RFAC", init<>())
    .def("setRFAC_USE", &RFAC::setRFAC_USE)
    .def("setRFAC_CUTO", &RFAC::setRFAC_CUTO)
  ;

  class_<ROOT>("ROOT", init<>())
    .def("setROOT", &ROOT::setROOT)
  ;

  class_<ROTA>("ROTA", init<>())
    .def("setROTA_VOLU", &ROTA::setROTA_VOLU)
    .def("setROTA_EULE", &ROTA::setROTA_EULE)
    .def("setROTA_RANG", &ROTA::setROTA_RANG)
    .def("setROTA_ANGL", &ROTA::setROTA_ANGL)
    .def("setROTA_LMAX_RESO", &ROTA::setROTA_LMAX_RESO)
    .def("set_new_clustering", &ROTA::set_new_clustering)
  ;

  class_<SAMP>("SAMP", init<>())
    .def("setSAMP_ROTA", &SAMP::setSAMP_ROTA)
    .def("setSAMP_TRAN", &SAMP::setSAMP_TRAN)
  ;

  class_<SCAT>("SCAT", init<>())
    .def("addSCAT", &SCAT::addSCAT,
         (arg("type"),arg("fp"),arg("fdp"),arg("fixfdp")))
    .def("addSCAT_TYPE", &SCAT::addSCAT) // backwards compatibility
    .def("setSCAT_REST", &SCAT::setSCAT_REST)
    .def("setSCAT_SIGM", &SCAT::setSCAT_SIGM)
  ;

  class_<SEAR>("SEAR", init<>())
    .def("addSEAR_ENSE_NUM", &SEAR::addSEAR_ENSE_NUM)
    .def("addSEAR_ENSE_OR_ENSE_NUM", &SEAR::addSEAR_ENSE_OR_ENSE_NUM)
    .def("setSEAR_ORDE_AUTO", &SEAR::setSEAR_ORDE_AUTO)
    .def("setSEAR_METH", &SEAR::setSEAR_METH)
    .def("setSEAR_BFAC", &SEAR::setSEAR_BFAC)
    .def("setSEAR_OFAC", &SEAR::setSEAR_OFAC)
    .def("setSEAR_PRUN", &SEAR::setSEAR_PRUN)
  ;

  class_<SGAL>("SGAL", init<>())
    .def("setSGAL_SELE", &SGAL::setSGAL_SELE)
    .def("addSGAL_TEST", &SGAL::addSGAL_TEST)
    .def("setSGAL_BASE", &SGAL::setSGAL_BASE)
  ;

  class_<SOLP>("SOLP", init<>())
    .def("setSOLP_SIGA_FSOL", &SOLP::setSOLP_SIGA_FSOL)
    .def("setSOLP_SIGA_BSOL", &SOLP::setSOLP_SIGA_BSOL)
    .def("setSOLP_SIGA_MIN", &SOLP::setSOLP_SIGA_MIN)
    .def("setSOLP_BULK_USE", &SOLP::setSOLP_BULK_USE)
    .def("setSOLP_BULK_FSOL", &SOLP::setSOLP_BULK_FSOL)
    .def("setSOLP_BULK_BSOL", &SOLP::setSOLP_BULK_BSOL)
  ;

  class_<SOLU>("SOLU", init<>())
    .def("setSOLU", &SOLU::setSOLU)
    .def("addSOLU_SET", &SOLU::addSOLU_SET)
    .def("addSOLU_TEMPLATE", &SOLU::addSOLU_TEMPLATE)
    .def("addSOLU_6DIM_ENSE", &SOLU::addSOLU_6DIM_ENSE)
    .def("addSOLU_TRIAL_ENSE_EULER", &SOLU::addSOLU_TRIAL_ENSE_EULER)
    .def("addSOLU_TRIAL_ENSE_EULER_GYRE", &SOLU::addSOLU_TRIAL_ENSE_EULER_GYRE)
    .def("addSOLU_ORIG_ENSE", &SOLU::addSOLU_ORIG_ENSE)
    .def("addSOLU_SPAC", &SOLU::addSOLU_SPAC)
    .def("addSOLU_RESO", &SOLU::addSOLU_RESO)
    .def("addSOLU_SPAC_HALL", &SOLU::addSOLU_SPAC_HALL)
    .def("addSOLU_ENSE_DRMS", &SOLU::addSOLU_ENSE_DRMS)
    .def("addSOLU_ENSE_ROT_TRA", &SOLU::addSOLU_ENSE_ROT_TRA)
  ;

  class_<SORT>("SORT", init<>())
    .def("setSORT", &SORT::setSORT)
  ;

  class_<SPAC>("SPAC", init<>())
    .def("setSPAC_NUM", &SPAC::setSPAC_NUM)
    .def("setSPAC_NAME", &SPAC::setSPAC_NAME)
    .def("setSPAC_HALL", &SPAC::setSPAC_HALL)
  ;

  class_<TARG>("TARG", init<>())
    .def("setTARG_ROTA", &TARG::setTARG_ROTA)
    .def("setTARG_TRAN", &TARG::setTARG_TRAN)
    .def("setTARG_ROTA_TYPE", &TARG::setTARG_ROTA_TYPE)
    .def("setTARG_TRAN_TYPE", &TARG::setTARG_TRAN_TYPE)
  ;

  class_<TITL>("TITL", init<>())
    .def("setTITL", &TITL::setTITL)
  ;

  class_<TNCS>("TNCS", init<>())
    .def("setTNCS_USE", &TNCS::setTNCS_USE)
    .def("setTNCS_NMOL", &TNCS::setTNCS_NMOL)
    .def("setTNCS_MAXN", &TNCS::setTNCS_MAXN)
    .def("setTNCS_TRAN_VECT", &TNCS::setTNCS_TRAN_VECT)
    .def("setTNCS_ROTA_ANGL", &TNCS::setTNCS_ROTA_ANGL)
    .def("setTNCS_TRAN_PERT", &TNCS::setTNCS_TRAN_PERT)
    .def("setTNCS_ROTA_RANG", &TNCS::setTNCS_ROTA_RANG)
    .def("setTNCS_ROTA_SAMP", &TNCS::setTNCS_ROTA_SAMP)
    .def("setTNCS_GFUN_RADI", &TNCS::setTNCS_GFUN_RADI)
    .def("setTNCS_PATT_HIRE", &TNCS::setTNCS_PATT_HIRE)
    .def("setTNCS_PATT_LORE", &TNCS::setTNCS_PATT_LORE)
    .def("setTNCS_PATT_PERC", &TNCS::setTNCS_PATT_PERC)
    .def("setTNCS_PATT_DIST", &TNCS::setTNCS_PATT_DIST)
    .def("setTNCS_PATT_MAPS", &TNCS::setTNCS_PATT_MAPS)
//tncs mr
    .def("setTNCS_VARI_RMSD", &TNCS::setTNCS_VARI_RMSD)
    .def("setTNCS_VARI_FRAC", &TNCS::setTNCS_VARI_FRAC)
    .def("setTNCS_VARI_BINS", &TNCS::setTNCS_VARI_BINS)
//tncs ep
    .def("setTNCS_LINK_REST", &TNCS::setTNCS_LINK_REST)
    .def("setTNCS_LINK_SIGM", &TNCS::setTNCS_LINK_SIGM)
    .def("setTNCS_PAIR_ONLY", &TNCS::setTNCS_PAIR_ONLY)
    .def("setTNCS_RLIS_ADD", &TNCS::setTNCS_RLIS_ADD)
//tncs good
    .def("setTNCS_COMM_PEAK", &TNCS::setTNCS_COMM_PEAK)
    .def("setTNCS_COMM_TOLF", &TNCS::setTNCS_COMM_TOLF)
    .def("setTNCS_COMM_TOLO", &TNCS::setTNCS_COMM_TOLO)
    .def("setTNCS_COMM_PATT", &TNCS::setTNCS_COMM_PATT)
    .def("setTNCS_COMM_MISS", &TNCS::setTNCS_COMM_MISS)
  ;

  class_<TOPF>("TOPF", init<>())
    .def("setTOPF", &TOPF::setTOPF)
  ;

  class_<TRAN>("TRAN", init<>())
    .def("setTRAN_VOLU", &TRAN::setTRAN_VOLU)
    .def("setTRAN_STAR", &TRAN::setTRAN_STAR)
    .def("setTRAN_POIN", &TRAN::setTRAN_STAR) //alias
    .def("setTRAN_AROU", &TRAN::setTRAN_STAR) //alias
    .def("setTRAN_CENT", &TRAN::setTRAN_CENT) //four parameters
    .def("setTRAN_END", &TRAN::setTRAN_END)
    .def("setTRAN_RANG", &TRAN::setTRAN_RANG)
    .def("setTRAN_FRAC", &TRAN::setTRAN_FRAC)
    .def("setTRAN_PACK_USE",  &TRAN::setTRAN_PACK_USE)
    .def("setTRAN_PACK_CUTO", &TRAN::setTRAN_PACK_CUTO)
    .def("setTRAN_PACK_NUMB", &TRAN::setTRAN_PACK_NUMB)
    .def("setTRAN_MAPS", &TRAN::setTRAN_MAPS)
    .def("setTRAN_FAST_STAT", &TRAN::setTRAN_FAST_STAT)
  ;

  class_<VARS>("VARS", init<>())
    .def("setVARS", &VARS::setVARS)
  ;

  class_<VERB>("VERB", init<>())
    .def("setVERB", &VERB::setVERB)
    .def("setDEBU", &VERB::setDEBU)
    .def("setOUTP_LEVE", &VERB::setOUTP_LEVE)
  ;

  class_<WAVE>("WAVE", init<>())
    .def("setWAVE", &WAVE::setWAVE)
  ;

  class_<XYZO>("XYZO", init<>())
    .def("setXYZO", &XYZO::setXYZO)
    .def("setXYZO_ENSE", &XYZO::setXYZO_ENSE)
    .def("setXYZO_PACK", &XYZO::setXYZO_PACK)
    .def("setXYZO_NMA_ALL", &XYZO::setXYZO_NMA_ALL)
    .def("setXYZO_CHAI_COPY", &XYZO::setXYZO_CHAI_COPY)
    .def("setXYZO_PRIN", &XYZO::setXYZO_PRIN)
  ;

  class_<ZSCO>("ZSCO", init<>())
    .def("setZSCO_SOLV", &ZSCO::setZSCO_SOLV)
    .def("setZSCO_POSS_SOLV", &ZSCO::setZSCO_POSS_SOLV)
    .def("setZSCO_HALF", &ZSCO::setZSCO_HALF)
    .def("setZSCO_USE", &ZSCO::setZSCO_USE)
    .def("setZSCO_STOP", &ZSCO::setZSCO_STOP)
  ;
}

} // namespace phaser::<anonymous>

void init_keywords()
{
  wrap_keywords();
}

} // namespace boost_python
} // namespace phaser
