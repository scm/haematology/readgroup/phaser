//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <boost/python.hpp>
#include <scitbx/boost_python/container_conversions.h>
#include <scitbx/stl/map_wrapper.h>
#include <scitbx/stl/set_wrapper.h>

#include <phaser/ncs/NCSOperator.h>
#include <phaser/ncs/NCSSymmetry.h>
#include <phaser/ncs/NCSAssembly.h>
#include <phaser/ncs/PointGroup.h>
#include <phaser/ncs/MacromoleculeType.h>
#include <phaser/ncs/LCSTree.h>
#include <phaser/ncs/ReferenceChain.h>
#include <phaser/ncs/EnsembleSymmetry.h>
#include <Python.h>

namespace phaser {  namespace boost_python { namespace {

template<class T1, class T2>
struct PairToTupleConverter
{
    static PyObject* convert(const std::pair< T1, T2 >& pair)
    {
        using namespace boost::python;
        return incref( make_tuple( pair.first, pair.second ).ptr() );
    }
};


template<unsigned N>
struct SmallStrToPyStringInterConverter
{
    SmallStrToPyStringInterConverter()
    {
        using namespace boost::python;
        converter::registry::push_back(
            &convertible,
            &construct,
            type_id< iotbx::pdb::small_str< N > >()
            );
        to_python_converter<
            iotbx::pdb::small_str< N >,
            SmallStrToPyStringInterConverter< N >
            >();
    }


    static PyObject* convert(const iotbx::pdb::small_str< N >& small_str)
    {
        using namespace boost::python;
        return incref( str( small_str.elems ).ptr() );
    }


    static void* convertible(PyObject* obj_ptr)
    {

#if PY_MAJOR_VERSION >= 3
      PyObject* o(obj_ptr);
      if (!PyUnicode_Check(o))
        return 0;
#else
      if ( !PyString_Check( obj_ptr ) )
        return 0;
#endif
        return obj_ptr;
    }


    static void construct(
      PyObject* obj_ptr,
      boost::python::converter::rvalue_from_python_stage1_data* data
      )
    {
        using namespace boost::python;

#if PY_MAJOR_VERSION >= 3
        PyObject* o(obj_ptr);
        const char* str_ptr;
        if (PyUnicode_Check(o))
          o = PyUnicode_AsUTF8String(o);
        str_ptr = PyBytes_AsString(o);
#else
        const char* str_ptr = PyString_AsString(obj_ptr);
#endif

        if (str_ptr == 0 )
        {
            throw_error_already_set();
        }

        void* storage = reinterpret_cast<
            converter::rvalue_from_python_storage< iotbx::pdb::small_str< N > >*
            >( data )->storage.bytes;
        new (storage) iotbx::pdb::small_str< N >(str_ptr);
        data->convertible = storage;
    }
};

boost::python::list
as_python_list(std::vector< scitbx::vec3< double > > const& myvec)
{
  boost::python::list result;

  for (
    std::vector< scitbx::vec3< double > >::const_iterator it = myvec.begin();
    it != myvec.end();
    ++it
    )
  {
    result.append( *it );
  }

  return result;
}

boost::python::list
ncs_symmetry_get_euler_list(phaser::ncs::NCSSymmetry const& symm)
{
  return as_python_list( symm.get_euler_list() );
}

boost::python::list
ncs_symmetry_get_orth_list(phaser::ncs::NCSSymmetry const& symm)
{
  return as_python_list( symm.get_orth_list() );
}

} // namespace <anonymous>


void
init_ncs_module()
{
    using namespace boost::python;
    using namespace phaser::ncs;

    object ncs_module( (
        handle<>( borrowed( PyImport_AddModule( "phaser.ncs" ) ) )
        ) );
    scope().attr( "ncs" ) = ncs_module;

    scope ncs_scope = ncs_module;

    class_< NCSOperator >(
        "Operator",
        init< scitbx::mat3< double > const&, scitbx::vec3< double > const& >(
            ( arg( "rotation" ), arg( "translation" ) )
            )
        )
        .def( "__str__", &NCSOperator::to_string )
        .def(
            "get_rotation",
            &NCSOperator::get_rotation,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_translation",
            &NCSOperator::get_translation,
            return_value_policy< copy_const_reference >()
            )
        .def( "rotation_angle_radian", &NCSOperator::rotation_angle_radian )
        .def( "displacement", &NCSOperator::displacement )
        .def( "inverse", &NCSOperator::inverse )
        .def(
            "approx_equals",
            &NCSOperator::approx_equals,
            ( arg( "other" ), arg( "angular" ), arg( "spatial" ) )
            )
        .def(
            "approx_inverse",
            &NCSOperator::approx_inverse,
            ( arg( "other" ), arg( "angular" ), arg( "spatial" ) )
            )
        .def(
            "approx_unity",
            &NCSOperator::approx_unity,
            ( arg( "angular" ), arg( "spatial" ) )
            )
        .def(
            "transform",
            &NCSOperator::transform,
            ( arg( "point" ) )
            )
        .def( self * self )
        .def_readonly( "unity", NCSOperator::unity )
        ;

    class_< NCSSymmetry >(
        "Symmetry",
        init< const std::vector< NCSOperator >&, double, double >(
            ( arg( "operators" ), arg( "angular" ), arg( "spatial" ) )
            )
        )
        .def( "__str__", &NCSSymmetry::to_string )
        .def(
            "get_operators",
            &NCSSymmetry::get_operators,
            return_value_policy< copy_const_reference >()
            )
        .def( "get_euler_list", &ncs_symmetry_get_euler_list )
        .def( "get_orth_list", &ncs_symmetry_get_orth_list )
        .def( "get_angular_tolerance", &NCSSymmetry::get_angular_tolerance )
        .def( "get_spatial_tolerance", &NCSSymmetry::get_spatial_tolerance )
        .def( "contains", &NCSSymmetry::contains, ( arg( "operator" ) ) )
        .def( "equals", &NCSSymmetry::equals, ( arg( "ops" ) ) )
        .def( "left_products", &NCSSymmetry::left_products, arg( "operator" ) )
        .def(
            "right_products",
            &NCSSymmetry::right_products,
            arg( "operator" )
            )
        .def( "intersect", &NCSSymmetry::intersect, arg( "symmetry" ) )
        .def( "factorize", &NCSSymmetry::factorize )
        .def( "quotient", &NCSSymmetry::quotient, arg( "divisor" ) )
        .def( "direct_product", &NCSSymmetry::direct_product, arg( "inner" ) )
        ;

    namespace ph = iotbx::pdb::hierarchy;

    class_< NCSAssembly, bases< NCSSymmetry > >(
        "Assembly",
        init< const ph::chain&, double, double >(
            ( arg( "reference" ), arg( "angular" ), arg( "spatial" ) )
            )
        )
        .def( "__str__", &NCSAssembly::to_string )
        .def(
            "get_chains",
            &NCSAssembly::get_chains,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "insert",
            &NCSAssembly::insert,
            ( arg( "operator" ), arg( "chain" ) )
            )
        ;

    class_< PointGroup, bases< NCSSymmetry > >( "PointGroup", no_init )
        .def( "__str__", &PointGroup::to_string )
        .def(
            "hermann_mauguin_symbol",
            &PointGroup::hermann_mauguin_symbol
            )
        .def(
            "from_operators",
            &PointGroup::from_operators,
            ( arg( "operators" ), arg( "angular" ), arg( "spatial" ) )
            )
        .staticmethod( "from_operators" )
        ;

    class_< MacromoleculeType >( "MacromoleculeType", init<>() )
        .def(
            "get_backbone_atom_names",
            &MacromoleculeType::get_backbone_atom_names,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_type_name",
            &MacromoleculeType::get_type_name,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_unknown_one_letter",
            &MacromoleculeType::get_unknown_one_letter
            )
        .def( "get_three_to_one", &MacromoleculeType::get_three_to_one )
        .def(
            "residue_group_sequence",
            &MacromoleculeType::residue_group_sequence,
            arg( "chain" )
            )
        .def(
            "recognize_chain",
            &MacromoleculeType::recognize_chain,
            ( arg( "chain" ), arg( "confidence" ) = 0.9 )
            )
        .def(
            "count_recognized_residues",
            &MacromoleculeType::count_recognized_residues,
            arg( "residue_groups" )
            )
        .def(
            "get_chain_type",
            &MacromoleculeType::get_chain_type,
            return_value_policy< copy_const_reference >(),
            arg( "chain" )
            )
        .staticmethod( "get_chain_type" )
        ;

    class_< ReferenceChain >(
        "ReferenceChain",
        init< const ph::chain&, const MacromoleculeType& >(
            ( arg( "chain" ), arg( "macromolecule_type" ) )
            )
        )
        .def(
            "get_chain",
            &ReferenceChain::get_chain,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_macromolecule_type",
            &ReferenceChain::get_macromolecule_type,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "determine_numbering_shift",
            &ReferenceChain::determine_numbering_shift,
            arg( "chain" )
            )
        ;

    class_< LCSTree >(
        "LCSTree",
        init< const std::string& >( arg( "separator" ) = "$" )
        )
        .def( "insert_word", &LCSTree::insert_word, ( arg( "word" ) ) )
        .def(
            "get_longest_common_substring",
            &LCSTree::get_longest_common_substring
            )
        ;

    class_< ChainAlignment >(
        "ChainAlignment",
        init<
            const ph::chain&,
            const ph::chain&,
            long
            >( ( arg( "reference" ), arg( "aligned" ), arg( "offset" ) ) )
        )
        .def( "get_sequence_identity", &ChainAlignment::get_sequence_identity )
        .def( "get_sequence_coverage", &ChainAlignment::get_sequence_coverage )
        .def( "get_alignment_length", &ChainAlignment::get_alignment_length )
        .def( "get_overlap_length", &ChainAlignment::get_overlap_length )
        .def(
            "get_residue_group_alignment",
            &ChainAlignment::get_residue_group_alignment,
            return_value_policy< copy_const_reference >()
            )
        .def( "get_atom_alignment", &ChainAlignment::get_atom_alignment )
        ;

    class_< EnsembleSymmetry >(
        "EnsembleSymmetry",
        init< float, float, float, double, double >(
            (
                arg( "min_sequence_coverage" ) = 0.90,
                arg( "min_sequence_identity" ) = 0.95,
                arg( "max_rmsd" ) = 1.00,
                arg( "angular_tolerance" ) = 0.085,
                arg( "spatial_tolerance" ) = 1.0
                )
            )
        )
        .def(
            "get_root",
            &EnsembleSymmetry::get_root,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_assemblies",
            &EnsembleSymmetry::get_assemblies,
            return_value_policy< copy_const_reference >()
            )
        .def(
            "get_min_sequence_coverage",
            &EnsembleSymmetry::get_min_sequence_coverage
            )
        .def(
            "get_min_sequence_identity",
            &EnsembleSymmetry::get_min_sequence_identity
            )
        .def( "get_max_rmsd", &EnsembleSymmetry::get_max_rmsd )
        .def(
            "get_angular_tolerance",
            &EnsembleSymmetry::get_angular_tolerance
            )
        .def(
            "get_spatial_tolerance",
            &EnsembleSymmetry::get_spatial_tolerance
            )
        .def(
            "set_root",
            &EnsembleSymmetry::set_root,
            arg( "root" )
            )
        .def(
            "set_min_sequence_coverage",
            &EnsembleSymmetry::set_min_sequence_coverage,
            arg( "value" )
            )
        .def(
            "set_min_sequence_identity",
            &EnsembleSymmetry::set_min_sequence_identity,
            arg( "value" )
            )
        .def( "set_max_rmsd", &EnsembleSymmetry::set_max_rmsd, arg( "value" ) )
        .def(
            "set_angular_tolerance",
            &EnsembleSymmetry::set_angular_tolerance,
            arg( "value" )
            )
        .def(
            "set_spatial_tolerance",
            &EnsembleSymmetry::set_spatial_tolerance,
            arg( "value" )
            )
        .def( "get_point_group", &EnsembleSymmetry::get_point_group )
        .def(
            "get_centre_of_assembly",
            &EnsembleSymmetry::get_centre_of_assembly
            )
        ;

    using namespace scitbx::boost_python::container_conversions;
    tuple_mapping< std::vector< NCSOperator >, variable_capacity_policy >();
    to_tuple_mapping< std::vector< ph::chain > >();
    to_tuple_mapping< std::vector< std::pair< NCSSymmetry, NCSSymmetry > > >();
    to_tuple_mapping< std::vector< std::pair< ph::atom, ph::atom > > >();
    to_tuple_mapping<
        std::vector< std::pair< ph::residue_group, ph::residue_group > >
        >();
    to_tuple_mapping< std::vector< NCSAssembly > >();

    to_python_converter<
        std::pair< NCSSymmetry, NCSSymmetry >,
        PairToTupleConverter< NCSSymmetry, NCSSymmetry >
        >();
    to_python_converter<
        std::pair< int, int >,
        PairToTupleConverter< int, int >
        >();
    to_python_converter<
        std::pair< std::string, long >,
        PairToTupleConverter< std::string, long >
        >();
    to_python_converter<
        std::pair< ph::atom, ph::atom >,
        PairToTupleConverter< ph::atom, ph::atom >
        >();
    to_python_converter<
        std::pair< ph::residue_group, ph::residue_group >,
        PairToTupleConverter< ph::residue_group, ph::residue_group >
        >();
    SmallStrToPyStringInterConverter<3>();
    SmallStrToPyStringInterConverter<4>();
    from_python_sequence<
        std::vector< ph::residue_group >,
        variable_capacity_policy
        >();

    using namespace scitbx::stl::boost_python;
    map_wrapper< std::map< iotbx::pdb::str3, char > >::wrap( "map_str3_char" );
    set_wrapper< iotbx::pdb::str4 >::wrap( "set_str4" );
}

}  } // namespace phaser::boost_python
