//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/src/Minimizer.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance


void
wrap_Minimizer()
{
  using namespace boost::python;

  class_<Minimizer>("Minimizer",init<>())
     .def("run", (void (Minimizer::*)(RefineSAD&,ProtocolSAD,Output&)) &Minimizer::run)
  ;
}

} // namespace phaser::<anonymous>

void init_Minimizer()
{
  wrap_Minimizer();
}

} // namespace boost_python
} // namespace phaser
