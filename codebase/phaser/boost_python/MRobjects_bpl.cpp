//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <scitbx/stl/map_wrapper.h>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <phaser/mr_objects/mr_solution.h>
#include <phaser/mr_objects/mr_gyre.h>
#include <phaser/mr_objects/rms_estimate.h>
#include <phaser/include/data_solpar.h>
#include <phaser/mr_objects/data_pdb.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance


// wrapper functions
void
wrap_MRobjects()
{
  using namespace scitbx::stl::boost_python;
  map_wrapper< std::map< std::string, dvect3 > >::wrap( "map_string_dvect3" );
  map_wrapper< std::map< std::string, float1D > >::wrap( "map_str_float1D" );
  map_wrapper< std::map< std::string, std::vector< dvect3 > > >::wrap( "map_string_vect_of_dvect3");

  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  class_<mr_ndim>("mr_ndim")
    .enable_pickling()
    .def(init<std::string const&,dmat33 const&,bool const&,bool const&,dvect3 const&,floatType const&,bool const&,bool const&,bool const&,int const&,double const& >( (
            arg( "MODLID_" ),
            arg( "R_" ),
            arg( "FRAC_" ),
            arg( "inFRAC_" ),
            arg( "TRA_" ),
            arg( "BFAC_" ),
            arg( "FIXR_" ),
            arg( "FIXT_" ),
            arg( "FIXB_" ),
            arg( "MULT_" ) = 1,
            arg( "OFAC_" ) = 1
            ) ) )
    .def(init<std::string,dvect3,bool,dvect3,floatType,bool,bool,bool,int,double >( (
            arg( "modlid" ),
            arg( "euler" ),
            arg( "frac" ),
            arg( "tra" ),
            arg( "bfac" ),
            arg( "fixr" ),
            arg( "fixt" ),
            arg( "fixb" ),
            arg( "mult" ) = 1,
            arg( "ofac" ) = 1.0
            ) ) )
    .def("getEuler", &mr_ndim::getEuler)
    .def_readwrite( "MODLID", &mr_ndim::MODLID )
    .add_property( "TRA", make_getter(&mr_ndim::TRA, rbv()))
    .add_property( "R", make_getter(&mr_ndim::R, rbv()))
    .def_readwrite( "FRAC", &mr_ndim::FRAC )
    .def_readwrite( "inFRAC", &mr_ndim::inFRAC )
    .def_readwrite( "BFAC", &mr_ndim::BFAC )
    .def_readwrite( "OFAC", &mr_ndim::OFAC )
    .def_readwrite( "MULT", &mr_ndim::MULT )
    .def_readwrite( "FIXR", &mr_ndim::FIXR )
    .def_readwrite( "FIXT", &mr_ndim::FIXT )
    .def_readwrite( "FIXB", &mr_ndim::FIXB )
  ;

  class_<mr_rlist>("mr_rlist", no_init)
    .enable_pickling()
    .def(init<optional<std::string const&, //modlid
                       dvect3 const&, //euler
                       double const&, //rf
                       double const&, //rfz
                       double const&, //gyre
                       double const&> >()) //deep
    .def_readonly("MODLID", &mr_rlist::MODLID)
    .add_property("EULER", make_getter(&mr_rlist::EULER, rbv()))
    .def_readonly("RF", &mr_rlist::RF)
    .def_readonly("RFZ", &mr_rlist::RFZ)
    .def_readonly("GRF", &mr_rlist::GRF)
    .def_readonly("DEEP", &mr_rlist::DEEP)
  ;

  class_<mr_gyre>("mr_gyre", no_init)
    .enable_pickling()
    .def(init<optional<std::string const&, //modlid
                       dvect3 const&, //euler
                       dvect3 const&, //tra
                       dvect3 const&, //pertrot
                       dvect3 const&> >()) //perttra
    .def_readonly("MODLID", &mr_gyre::MODLID)
    .def("euler", &mr_gyre::euler)
    .def("euler_ang", &mr_gyre::euler_ang)
    .add_property("TRA", make_getter(&mr_gyre::TRA, rbv()))
    .add_property("perturbRot", make_getter(&mr_gyre::perturbRot, rbv()))
    .add_property("perturbTrans", make_getter(&mr_gyre::perturbTrans, rbv()))
  ;

  class_<MapCoefs>("MapCoefs")
    .enable_pickling()
    .def(init<std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&,
              std::vector<double> const&>())
    .def_readwrite( "FC", &MapCoefs::FC )
    .def_readwrite( "PHIC", &MapCoefs::PHIC )
    .def_readwrite( "FWT", &MapCoefs::FWT )
    .def_readwrite( "PHWT", &MapCoefs::PHWT )
    .def_readwrite( "DELFWT", &MapCoefs::DELFWT )
    .def_readwrite( "PHDELWT", &MapCoefs::PHDELWT )
    .def_readwrite( "FOM", &MapCoefs::FOM )
    .def_readwrite( "HLA", &MapCoefs::HLA )
    .def_readwrite( "HLB", &MapCoefs::HLB )
    .def_readwrite( "HLC", &MapCoefs::HLC )
    .def_readwrite( "HLD", &MapCoefs::HLD )
  ;

  class_<data_solpar>("data_solpar")
    .enable_pickling()
    .def( init< >())
  ;

  class_<mr_set>("mr_set")
    .enable_pickling()
    .def(init<
        std::string const&,
        std::vector<mr_ndim> const&,
        std::vector<mr_rlist> const&,
        std::string const&
        >( (
            arg( "annotation" ),
            arg( "known" ),
            arg( "rlist" ),
            arg( "hall" ) = ""
           ) )
        )
    .def(init<
        std::string const&,
        std::vector<mr_ndim> const&,
        std::vector<mr_rlist> const&,
        std::string const&,
        double const&,
        double const&,
        double const&,
        double const&,
        double const&,
        int const&,
        map_str_float1D const&,
        map_str_float const&,
        MapCoefs const&
        >( (
            arg( "annotation" ),
            arg( "known" ),
            arg( "rlist" ),
            arg( "hall" ),
            arg( "tf" ),
            arg( "tfz" ),
            arg( "llg" ),
            arg( "r" ),
            arg( "pak" ),
            arg( "orig_num" ),
            arg( "newvrms" ),
            arg( "cell" ),
            arg( "mapcoefs" )
            ) )
        )
    .add_property("KNOWN", make_getter(&mr_set::KNOWN, rbv()))
    .add_property("RLIST", make_getter(&mr_set::RLIST, rbv()))
    .add_property( "GYRE", make_getter(&mr_set::GYRE, rbv()))
    .add_property("TMPLT", make_getter(&mr_set::TMPLT, rbv()))
    .def_readwrite( "ANNOTATION", &mr_set::ANNOTATION)
    .def_readwrite( "HALL", &mr_set::HALL )
    .def_readwrite( "TF", &mr_set::TF )
    .def_readwrite( "TFZ", &mr_set::TFZ )
    .def_readwrite( "LLG", &mr_set::LLG )
    .def_readwrite( "R", &mr_set::R )
    .def_readwrite( "PAK", &mr_set::PAK )
    .def_readwrite( "ORIG_NUM", &mr_set::ORIG_NUM )
    .def_readwrite( "NEWVRMS", &mr_set::NEWVRMS )
    .def_readwrite( "CELL", &mr_set::CELL )
    .def_readwrite( "MAPCOEFS", &mr_set::MAPCOEFS )
    .def_readwrite( "NUM", &mr_set::NUM )
    .def_readwrite( "ORIG_LLG", &mr_set::ORIG_LLG )
    .def_readwrite( "ORIG_R", &mr_set::ORIG_R )
    .def_readwrite( "KEEP", &mr_set::KEEP )
    .def_readwrite( "EQUIV", &mr_set::EQUIV )
    .def_readwrite( "VRMS", &mr_set::VRMS )
    .def_readwrite( "TFZeq", &mr_set::TFZeq )
    .def_readwrite( "DRMS", &mr_set::DRMS )
    .def_readwrite( "NRF", &mr_set::NRF )
    .def_readwrite( "NTF", &mr_set::NTF )
    .def("unparse", &mr_set::unparse)
    .def("getSpaceGroupName", &mr_set::getSpaceGroupName)
    .def("getSpaceGroupHall", &mr_set::getSpaceGroupHall)
  ;

  class_<mr_solution>("mr_solution")
    .enable_pickling()
    .def(init<std::vector<mr_set> const&>())
    .def(init<std::vector<mr_set> const&, double const&>())
    .def( vector_indexing_suite<mr_solution>() )
    .def("unparse", &mr_solution::unparse)
    .def("unsort", &mr_solution::unsort)
    .def("is_rlist", &mr_solution::is_rlist)
    .def("if_not_set_apply_space_group", &mr_solution::if_not_set_apply_space_group)
    .def("resolution", &mr_solution::resolution )
    .def("homogeneous", &mr_solution::homogeneous )
    .def("number_of_unique_ensembles", &mr_solution::number_of_unique_ensembles )
    .def("unique_ensembles", &mr_solution::unique_ensembles )
    .def("number_of_copies_of_unique_ensembles", &mr_solution::number_of_copies_of_unique_ensembles )
    .def("setup_rlist", &mr_solution::setup_rlist )
    .def("setup_known", &mr_solution::setup_known )
  ;

  class_<rms_estimate>("rms_estimate")
    .enable_pickling()
    .def(init<>())
    .def(init<std::string>())
    .def("rms", &rms_estimate::rms)
  ;

using namespace scitbx::boost_python::container_conversions;
tuple_mapping<std::vector<mr_rlist>, variable_capacity_policy >();
tuple_mapping<std::vector<mr_ndim>, variable_capacity_policy >();
tuple_mapping<std::vector<mr_gyre>, variable_capacity_policy >();
tuple_mapping<std::vector<mr_set>, variable_capacity_policy >();
from_python_sequence<mr_solution, variable_capacity_policy >();
}

} // namespace phaser::<anonymous>

void init_MRobjects()
{
  wrap_MRobjects();
}
} // namespace boost_python
} // namespace phaser
