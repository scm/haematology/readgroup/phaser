//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <scitbx/stl/map_wrapper.h>
#include <boost/python.hpp>

#include <phaser/main/runPhaser.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

struct python_out : phenix_out
{
  virtual ~python_out();
  virtual void write(const std::string& text);
  virtual void flush();

  python_out(boost::python::object const& file_object)
  :
    have_flush(false),
    write_method(file_object.attr("write"))
  {
    if (PyObject_HasAttrString(file_object.ptr(), "flush")) {
      flush_method = file_object.attr("flush");
      have_flush = true;
    }
  }

  bool have_flush;
  boost::python::object write_method;
  boost::python::object flush_method;
};

void
python_out::write(const std::string& text)
{
  write_method(text);
}

void
python_out::flush()
{
  if (have_flush) flush_method();
}

python_out::~python_out() {}

void
set_sys_stdout(Output& self)
{
  boost::python::object sys_stdout = boost::python::object(
    boost::python::handle<>(PyImport_ImportModule("sys"))).attr("stdout");
  self.setIO(boost::shared_ptr<phenix_out>(
    new python_out(sys_stdout)));
}

void
set_file_object(
  Output& self,
  boost::python::object const& file_object)
{
  self.setIO(boost::shared_ptr<phenix_out>(
    new python_out(file_object)));
}

// XXX: Callback interface
struct python_callback : phenix_callback
{
  virtual ~python_callback();
  virtual void startProgressBar(const std::string& label, int size);
  virtual void incrementProgressBar();
  virtual void endProgressBar();
  virtual void warn(const std::string& message);
  virtual void loggraph(const std::string& title, const std::string& data);
  virtual void call_back(const std::string& message, const std::string& data);

  python_callback(boost::python::object const& callback_object) :
    startProgressBar_method(callback_object.attr("startProgressBar")),
    incrementProgressBar_method(callback_object.attr("incrementProgressBar")),
    endProgressBar_method(callback_object.attr("endProgressBar")),
    warn_method(callback_object.attr("warn")),
    loggraph_method(callback_object.attr("loggraph")),
    call_back_method(callback_object.attr("call_back"))
  {}

  boost::python::object startProgressBar_method;
  boost::python::object incrementProgressBar_method;
  boost::python::object endProgressBar_method;
  boost::python::object warn_method;
  boost::python::object loggraph_method;
  boost::python::object call_back_method;
};

void python_callback::startProgressBar(const std::string& label, int size)
{
  startProgressBar_method(label, size);
}

void python_callback::incrementProgressBar()
{
  incrementProgressBar_method();
}

void python_callback::endProgressBar()
{
  endProgressBar_method();
}

void python_callback::warn(const std::string& message)
{
  warn_method(message);
}


void python_callback::loggraph(const std::string& title,
                               const std::string& data)
{
  loggraph_method(title, data);
}

void python_callback::call_back(const std::string& message,
                                const std::string& data)
{
  call_back_method(message, data);
}

python_callback::~python_callback() {}

void set_callback_object(
  Output& self,
  boost::python::object const& callback_object)
{
  self.setCallback(boost::shared_ptr<phenix_callback>(
    new python_callback(callback_object)));
}

void set_phenix_callback_object(
  Output& self,
  boost::python::object const& callback_object)
{
  self.setPackagePhenix();
  self.setCallback(boost::shared_ptr<phenix_callback>(
    new python_callback(callback_object)));
}


// End of callback interface

void
wrap_Outputs()
{
  using namespace scitbx::stl::boost_python;
  map_wrapper< std::map< std::string, data_pdb > >::wrap( "map_string_data_pdb" );
  map_wrapper< std::map< int, std::string > >::wrap( "map_int_string" );

// Output Classes
  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  class_<SpaceGroup>("SpaceGroup")
    .enable_pickling()
    .def(init<>())
    .def(init<std::string>())
    .def("getSpaceGroupHall", &SpaceGroup::getSpaceGroupHall)
    .def("getSpaceGroupName", &SpaceGroup::getSpaceGroupName)
    .def("getSpaceGroupNum", &SpaceGroup::getSpaceGroupNum)
    .def("getSpaceGroupNSYMM", &SpaceGroup::getSpaceGroupNSYMM)
    .def("getSpaceGroupNSYMP", &SpaceGroup::getSpaceGroupNSYMP)
    .def("getSpaceGroupR", &SpaceGroup::getSpaceGroupR)
    .def("getSpaceGroupT", &SpaceGroup::getSpaceGroupT)
    .def("setSpaceGroup", &SpaceGroup::setSpaceGroup)
    .def("setSpaceGroupHall", &SpaceGroup::setSpaceGroupHall)
  ;

  class_<UnitCell>("UnitCell")
    .enable_pickling()
    .def( init<>() )
    .def( init<af::double6>() )
    .def("getUnitCell", &UnitCell::getCell6)
    .def("setUnitCell", &UnitCell::setUnitCellDouble6)
  ;

  class_<Output>("Output", init<>())
    .enable_pickling()
    .add_property("output_strings", make_getter(&Output::output_strings, rbv()), make_setter(&Output::output_strings))
    .add_property("run_time", make_getter(&Output::run_time, rbv()), make_setter(&Output::run_time))
    .def("setPackageCCP4", &Output::setPackageCCP4)
    .def("setPackagePhenix", set_sys_stdout)
    .def("setPackagePhenix", set_file_object, ( arg("file_object")))
    .def("setPhenixPackageCallback", set_phenix_callback_object, ( arg("callback")))
    .def("setPhenixCallback", set_callback_object, ( arg("callback")))
    .def("set_sys_stdout", set_sys_stdout)
    .def("set_file_object", set_file_object, ( arg("file_object")))
    .def("set_callback", set_callback_object, ( arg("callback")))
    .def("process", &Output::process)
    .def("concise", &Output::concise)
    .def("verbose", &Output::verbose)
    .def("logfile", &Output::logfile)
    .def("summary", &Output::summary)
    .def("warnings", &Output::Warnings)
    .def("loggraph", &Output::Loggraph)
    .def("Failed", &Output::Failure)
    .def("Failure", &Output::Failure)
    .def("Success", &Output::Success)
    .def("ErrorName", &Output::ErrorName)
    .def("ErrorMessage", &Output::ErrorMessage)
    .def("setLevel", &Output::setLevel)
    .def("svn_revision", &Output::svn_revision)
    .def("version_number", &Output::version_number)
    .def("cpu_time", &Output::cpu_time)
    .def("git_rev", &Output::git_rev)
    .def("git_hash", &Output::git_hash)
    .def("git_shorthash", &Output::git_shorthash)
    .def("git_commitdatetime", &Output::git_commitdatetime)
    .def("git_branchname", &Output::git_branchname)
    .def("git_tagname", &Output::git_tagname)
    .def("git_totalcommits", &Output::git_totalcommits)
#ifdef PHASER_BOOST_TIMER
    .add_property("userrun_time", make_getter(&Output::userrun_time, rbv()), make_setter(&Output::userrun_time))
    .def("user_time", &Output::user_time)
#endif
    ;

  class_<data_norm>("data_norm")
    .enable_pickling()
    .def(init<>())
    .add_property( "WILSON_K", make_getter( &data_norm::WILSON_K, rbv()), make_setter(&data_norm::WILSON_K))
    .add_property( "WILSON_B", make_getter( &data_norm::WILSON_B, rbv()), make_setter(&data_norm::WILSON_B))
    .add_property( "BINS", make_getter( &data_norm::BINS, rbv()), make_setter(&data_norm::BINS))
    .add_property( "ANISO", make_getter( &data_norm::ANISO, rbv()), make_setter(&data_norm::ANISO))
  ;

  class_<data_outl>("data_outl")
    .enable_pickling()
    .def(init<>())
    .def_readonly( "REJECT", &data_outl::REJECT )
    .def_readonly( "PROB", &data_outl::PROB )
    .def_readonly( "MAXPROB", &data_outl::MAXPROB )
    .add_property( "LIST", make_getter( &data_outl::ANO, rbv()), make_setter(&data_outl::ANO))
  ;

  class_<probTypeANO>("probType")
    .enable_pickling()
    .def(init<>())
    .add_property( "refl", make_getter( &probTypeANO::refl, rbv()), make_setter(&probTypeANO::refl))
    .add_property( "prob", make_getter( &probTypeANO::prob, rbv()), make_setter(&probTypeANO::prob))
  ;

  class_<data_spots>("data_spots")
    .enable_pickling()
    .def(init<>())
    .def(init<af::shared<double>, af::shared<double>, af::shared<bool> >())
    .add_property( "FMEAN", make_getter( &data_spots::F, rbv()), make_setter(&data_spots::F))
    .add_property( "SIGFMEAN", make_getter( &data_spots::SIGF, rbv()), make_setter(&data_spots::SIGF))
    .add_property( "IMEAN", make_getter( &data_spots::I, rbv()), make_setter(&data_spots::I))
    .add_property( "SIGIMEAN", make_getter( &data_spots::SIGI, rbv()), make_setter(&data_spots::SIGI))
  ;

  class_<data_refl, bases<data_spots> >("data_refl", init<>())
    .enable_pickling()
    .def(init<af::shared<miller::index<int> >, af::shared<floatType>, af::shared<floatType> >())
    .def(init<af::shared<miller::index<int> >, af::shared<floatType>, af::shared<floatType>, std::string>())
    .add_property( "MILLER", make_getter( &data_refl::MILLER, rbv()), make_setter(&data_refl::MILLER))
    .def_readonly( "FTFMAP", &data_refl::FTFMAP ) //data_ftfmap
    .def_readonly( "SG_HALL", &data_refl::SG_HALL )
  ;

  class_<data_ep>("data_ep")
    .enable_pickling()
    .def(init<>())
  ;

  class_<data_ftfmap>("data_ftfmap")
    .enable_pickling()
    .def(init<>())
    .add_property( "FMAP", make_getter( &data_ftfmap::FMAP, rbv()), make_setter(&data_ftfmap::FMAP))
    .add_property( "PHASE", make_getter( &data_ftfmap::PHMAP, rbv()), make_setter(&data_ftfmap::PHMAP))
    .add_property( "FOM", make_getter( &data_ftfmap::FOM, rbv()), make_setter(&data_ftfmap::FOM))
  ;

  class_<data_xtal>("data_xtal")
    .enable_pickling()
    .def(init<>())
  ;

  class_<data_pdb>("data_pdb")
    .enable_pickling()
    .def(init<>())
    .def( "centre", &data_pdb::centre )
    .def( "mean_rad_prpt", &data_pdb::mean_rad_prpt )
    .def( "symm_eulers", &data_pdb::symm_eulers )
    .def( "setup_molecules", &data_pdb::setup_molecules )
  ;

  class_<data_trace>("data_trace")
    .enable_pickling()
    .def(init<>())
    .def_readonly( "SAMP_DISTANCE", &data_trace::SAMP_DISTANCE )
    .def_readonly( "SAMP_MIN", &data_trace::SAMP_MIN )
    .def_readonly( "SAMP_TARGET", &data_trace::SAMP_TARGET )
    .def_readonly( "SAMP_USE", &data_trace::SAMP_USE )
    .def_readonly( "SAMP_WANG", &data_trace::SAMP_WANG )
    .def_readonly( "SAMP_ASA", &data_trace::SAMP_ASA )
    .def_readonly( "MODEL", &data_trace::MODEL )
  ;

  class_< data_ptgrp >( "data_ptgrp" )
    .def( init<>() )
    ;

  class_<data_resharp>("data_resharp")
    .enable_pickling()
    .def(init<>())
  ;

  class_<data_asu>("data_asu", no_init)
    .enable_pickling()
    .def_readonly( "TYPE", &data_asu::TYPE )
    .def_readonly( "MW", &data_asu::MW )
    .def_readonly( "NRES", &data_asu::NRES )
    .def_readonly( "SEQ", &data_asu::SEQ )
    .def_readonly( "STR", &data_asu::STR )
    .def_readonly( "NUM", &data_asu::NUM )
  ;

  class_<data_composition>("data_composition", no_init)
    .enable_pickling()
    .def_readonly( "ASU", &data_composition::ASU )
    .def_readonly( "PERCENTAGE", &data_composition::PERCENTAGE )
    .def_readonly( "INCREASE", &data_composition::INCREASE )
    .def_readonly( "BY", &data_composition::BY )
  ;

  class_<DataA, bases<UnitCell,data_refl> >("DataA", init<>())
    .enable_pickling()
    .def(init<>())
    .def(init<af::double6,data_refl&,data_resharp&,data_norm&,data_outl&>())
    .def(init<data_refl&,data_resharp&,data_norm&,data_outl&,data_tncs&>())
    .def("getMiller", &DataA::getMiller)
    .def("getF", &DataA::getF)
    .def("getSIGF", &DataA::getSIGF)
    .def("setDataA", &DataA::setDataA)
    .def("getSigmaN", &DataA::getSigmaN)
    .def("getOutlier", &DataA::getOutlier)
    .def("getWilsonK", &DataA::getWilsonK)
    .def("getWilsonB", &DataA::getWilsonB)
    .def("getAniso", &DataA::getAniso)
    .def("setWilsonK", &DataA::setWilsonK)
    .def("setWilsonB", &DataA::setWilsonB)
    .def("setAniso", &DataA::setAniso)
    .def("getHall", &ResultMR::getSpaceGroupHall)
    .def("getCell", &ResultMR::getCell6)
  ;

  class_<ResultCore>("ResultCore")
    .enable_pickling()
    .def( init<>() )
    .def("getChainOcc", &ResultCore::getChainOcc)
    .def_readwrite("PDB", &ResultCore::PDB)
  ;

  class_<ResultMR_DAT, bases<Output,SpaceGroup,UnitCell> >("ResultMR_DAT", init<>())
    .enable_pickling()
    .def_readonly( "DATA_REFL", &ResultMR_DAT::DATA_REFL )
    .def("getMiller", &ResultMR_DAT::getMiller)
    .def("getFobs", &ResultMR_DAT::getFobs)
    .def("getSigFobs", &ResultMR_DAT::getSigFobs)
    .def("getIobs", &ResultMR_DAT::getIobs)
    .def("getSigIobs", &ResultMR_DAT::getSigIobs)
    .def("getF", &ResultMR_DAT::getF)
    .def("getSIGF", &ResultMR_DAT::getSIGF)
    .def("getFMAP", &ResultMR_DAT::getFMAP)
    .def("getPHMAP", &ResultMR_DAT::getPHMAP)
    .def("getFOM", &ResultMR_DAT::getFOM)
    .def("getDATA", &ResultMR_DAT::getDATA)
    .def("getDATA_REFL", &ResultMR_DAT::getDATA)
    .def("getREFL_DATA", &ResultMR_DAT::getDATA)
  ;

  class_<ResultCCA, bases<Output,SpaceGroup,UnitCell> >("ResultCCA", init<>())
    .def("getNum", &ResultCCA::getNum)
    .def("getProb", &ResultCCA::getProb)
    .def("getVM", &ResultCCA::getVM)
    .def("getZ", &ResultCCA::getZ)
    .def("getAssemblyMW", &ResultCCA::getAssemblyMW)
    .def("getBestProb", &ResultCCA::getBestProb)
    .def("getBestVM", &ResultCCA::getBestVM)
    .def("getBestZ", &ResultCCA::getBestZ)
    .def("getOptimalMW", &ResultCCA::getOptimalMW)
    .def("getOptimalVM", &ResultCCA::getOptimalVM)
  ;

  class_<ResultNMA, bases<Output> >("ResultNMA", init<>())
    .def("getNumPdb", &ResultNMA::getNumPdb)
    .def("getPdbFiles", &ResultNMA::getPdbFiles)
    .def("getDisplacements", &ResultNMA::getDisplacements)
    .def("getModes", &ResultNMA::getModes)
    .def("getPdbFile", &ResultNMA::getPdbFile)
    .def("getMatFile", &ResultNMA::getMatFile)
  ;

  class_<ResultNCS, bases<Output,DataA> >("ResultNCS", init<>())
    //legacy
    .def("hasTNCS", &ResultNCS::hasTNCS)
    .def("getNMOL", &ResultNCS::getNMOL)
    .def("getVector", &ResultNCS::getVector)
    .def("getAngles", &ResultNCS::getAngles)
    //preferred
    .def("getEpsFac", &ResultNCS::getEpsFac)
    .def("getTwinAlpha", &ResultNCS::getTwinAlpha)
    .def("getCentricE2", &ResultNCS::getCentricE2)
    .def("getCentricE4", &ResultNCS::getCentricE4)
    .def("getACentricE2", &ResultNCS::getACentricE2)
    .def("getACentricE4", &ResultNCS::getACentricE4)
    .def("has_tncs", &ResultNCS::has_tncs)
    .def("use_tncs", &ResultNCS::use_tncs)
    .def("nmol", &ResultNCS::nmol)
    .def("maxnmol", &ResultNCS::maxnmol)
    .def("translation_vector", &ResultNCS::translation_vector)
    .def("rotation_angles", &ResultNCS::rotation_angles)
    .def("analysis_number", &ResultNCS::analysis_number)
    .def("analysis_vector", &ResultNCS::analysis_vector)
    .def("analysis_nmol", &ResultNCS::analysis_nmol)
    .def("twinned", &ResultNCS::twinned)
    .def("patterson_top_analysis", &ResultNCS::patterson_top_analysis)
    .def("patterson_top_nonorigin", &ResultNCS::patterson_top_nonorigin)
    .def("patterson_top_all", &ResultNCS::patterson_top_all)
    .def("patterson_top_largecell", &ResultNCS::patterson_top_largecell)
    .def("patterson_top_cutoff", &ResultNCS::patterson_top_cutoff)
    .def("frequency_top_site", &ResultNCS::frequency_top_site)
    .def("frequency_top_peak", &ResultNCS::frequency_top_peak)
    .def("warning_reflections", &ResultNCS::warning_reflections)
    .def("warning_pathology", &ResultNCS::warning_pathology)
  ;

  class_<ResultELLG, bases<Output> >("ResultELLG", init<>())
    .def("get_useful_resolution", &ResultELLG::get_useful_resolution)
    .def("get_target_resolution", &ResultELLG::get_target_resolution)
    .def("get_perfect_data_resolution", &ResultELLG::get_perfect_data_resolution)
    .def("get_ellg_full_resolution", &ResultELLG::get_ellg_full_resolution)
    .def("get_map_ellg_full_resolution", &ResultELLG::get_map_ellg_full_resolution)
    .def("get_ellg_target_nres", &ResultELLG::get_ellg_target_nres)
    .def("get_map_chain_ellg", &ResultELLG::get_map_chain_ellg)
  ;

  class_<ResultDFAC, bases<Output,DataA> >("ResultDFAC", init<>())
    .def("getReflData", &ResultDFAC::getReflData)
  ;

  class_<ResultANO, bases<Output,SpaceGroup,DataA> >("ResultANO")
    .enable_pickling()
    .def(init<>())
    .def( init< Output& >( arg( "output" )  ) )
    .def("getLaboutF", &ResultANO::getLaboutF)
    .def("getLaboutSIGF", &ResultANO::getLaboutSIGF)
    .def("getCorrection", &ResultANO::getCorrection)
    .def("getCorrectedF", &ResultANO::getCorrectedF)
    .def("getCorrectedSIGF", &ResultANO::getCorrectedSIGF)
    .def("getScaledCorrected", &ResultANO::getScaledCorrected)
    .def("getAnisoDeltaB", &ResultANO::getAnisoDeltaB)
    .def("getEigenBs", &ResultANO::getEigenBs)
    .def("getEigenSystem", &ResultANO::getEigenSystem)
    .def("getMtzFile", &ResultANO::getMtzFile)
    .def("getFilenames", &ResultANO::getFilenames)
  ;

  class_<ResultMR_RF, bases<Output,UnitCell,ResultCore> >("ResultMR_RF")
    .enable_pickling()
    .def(init<>())
    .def( init< Output& >( arg( "output" )  ) )
    .def( "init", &ResultMR_RF::init )
    .def("getDotRlist", &ResultMR_RF::getDotRlist, return_value_policy<copy_const_reference>())
    .def("setDotRlist", &ResultMR_RF::setDotRlist)
    .def("getSet", &ResultMR_RF::getSet)
    .def("getRF", &ResultMR_RF::getRF)
    .def("getRFZ", &ResultMR_RF::getRFZ)
    .def("getNumTop", &ResultMR_RF::getNumTop)
    .def("getTitle", &ResultMR_RF::getTitle)
    .def("getHall", &ResultMR_RF::getHall)
    .def("numRlist", &ResultMR_RF::numRlist)
    .def("top_rlist", &ResultMR_RF::top_rlist)
  ;

  class_<ResultMR_TF, bases<Output,UnitCell,ResultCore> >("ResultMR_TF")
    .enable_pickling()
    .def( init<>() )
    .def( init< Output& >( arg( "output" )  ) )
    .def(
        "init",
        &ResultMR_TF::init,
        ( arg( "t" ), arg( "n" ), arg( "TRACE") )
        )
    .def("getDotSol", &ResultMR_TF::getDotSol, return_value_policy<copy_const_reference>())
    .def("getTF", &ResultMR_TF::getTF)
    .def("getTFZ", &ResultMR_TF::getTFZ)
    .def("setDotSol", &ResultMR_TF::setDotSol)
    .def("getNumTop", &ResultMR_TF::getNumTop)
    .def("getTitle", &ResultMR_TF::getTitle)
    .def("foundSolutions", &ResultMR_TF::foundSolutions)
    .def("numSolutions", &ResultMR_TF::numSolutions)
    .def("getTopTF", &ResultMR_TF::getTopTF)
    .def("getTopTFZ", &ResultMR_TF::getTopTFZ)
  ;

  class_<ResultMR, bases<Output,DataA,ResultCore> >("ResultMR")
    .enable_pickling()
    .def(init<>())
    .def(init< Output& >( arg( "output" ) ) )
    .def(init<Output&, DataA&, std::string, unsigned >())
    .def("init", &ResultMR::init, ( arg( "rt" ), arg( "t" ), arg( "n" ), arg( "TRACE"), arg( "chainocc" )) )
    .def("getMtzFiles", &ResultMR::getMtzFiles)
    .def("getNumPdbFiles", &ResultMR::getNumPdbFiles)
    .def("getPdbFiles", &ResultMR::getPdbFiles)
    .def("getDotSol", &ResultMR::getDotSol, return_value_policy<copy_const_reference>())
    .def("getSolFile", &ResultMR::getSolFile)
    .def("getSolFileReject", &ResultMR::getSolFileReject)
    .def("getTopLLG", &ResultMR::getTopLLG)
    .def("getTopTFZ", &ResultMR::getTopTFZ)
    .def("getTopTFZeq", &ResultMR::getTopTFZeq)
    .def("getTemplatesForSolution", &ResultMR::getTemplatesForSolution)
    .def("getSolutionsForTemplate", &ResultMR::getSolutionsForTemplate)
    .def("getTopSet", &ResultMR::getTopSet)
    .def("getTopMtzFile", &ResultMR::getTopMtzFile)
    .def("getTopPdbFile", &ResultMR::getTopPdbFile)
    .def("foundSolutions", &ResultMR::foundSolutions)
    .def("numSolutions", &ResultMR::numSolutions)
    .def("uniqueSolution", &ResultMR::uniqueSolution)
    .def("getValues", &ResultMR::getValues)
    .def("getLLG", &ResultMR::getLLG)
    .def("getPAK", &ResultMR::getPAK)
    .def("getNumTop", &ResultMR::getNumTop)
    .def("setDotSol", &ResultMR::setDotSol)
    .def("getTitle", &ResultMR::getTitle)
    .def("getResultType", &ResultMR::getResultType)
    .def("calcDuplicateSolutions", &ResultMR::calcDuplicateSolutions)
    .def("phenixPDBNames", &ResultMR::phenixPDBNames)
    .def("getEnsFile", &ResultMR::getEnsFile)
    .def("getEnsFiles", &ResultMR::getEnsFiles)
    .def("getUsedReflections", &ResultMR::getUsedReflections)
    .def("get_occupancy_nresidues", &ResultMR::get_occupancy_nresidues)
    .def("get_occupancy_window_ellg", &ResultMR::get_occupancy_window_ellg)
    .def("get_gyre_pdb_str", &ResultMR::get_gyre_pdb_str)
    .def("write_mtz", &ResultMR::write_mtz)
    .def("append_mtz", &ResultMR::append_mtz)
    .def("write_pdb", &ResultMR::write_pdb)
  ;

  class_<ResultEP_DAT, bases<Output,SpaceGroup,UnitCell> >("ResultEP_DAT", init<>())
    .def("getMiller", &ResultEP_DAT::getMiller)
    .def("getF", &ResultEP_DAT::getF)
    .def("getSIGF", &ResultEP_DAT::getSIGF)
    .def("getP", &ResultEP_DAT::getP)
    .def("getFneg", &ResultEP_DAT::getFneg)
    .def("getSIGFneg", &ResultEP_DAT::getSIGFneg)
    .def("getPneg", &ResultEP_DAT::getPneg)
    .def("getFpos", &ResultEP_DAT::getFpos)
    .def("getSIGFpos", &ResultEP_DAT::getSIGFpos)
    .def("getPpos", &ResultEP_DAT::getPpos)
    .def("getCrysData", &ResultEP_DAT::getCrysData)
  ;

  class_<HandEP, bases<SpaceGroup> >("HandEP", init<>())
    .enable_pickling()
    .add_property("selected", make_getter( &HandEP::selected, rbv()), make_setter(&HandEP::selected))
    .def_readwrite("acentStats", &HandEP::acentStats)
    .def_readwrite("centStats", &HandEP::centStats)
    .def_readwrite("allStats", &HandEP::allStats)
    .add_property("FWT", make_getter( &HandEP::FWT, rbv()), make_setter(&HandEP::FWT))
    .add_property("PHWT", make_getter( &HandEP::PHWT, rbv()), make_setter(&HandEP::PHWT))
    .add_property("PHIB", make_getter( &HandEP::PHIB, rbv()), make_setter(&HandEP::PHIB))
    .add_property("FOM", make_getter( &HandEP::FOM, rbv()), make_setter(&HandEP::FOM))
    .add_property("FPFOM", make_getter( &HandEP::FPFOM, rbv()), make_setter(&HandEP::FPFOM))
    .add_property("HL", make_getter( &HandEP::HL, rbv()), make_setter(&HandEP::HL))
    .add_property("atoms", make_getter( &HandEP::atoms, rbv()), make_setter(&HandEP::atoms))
    .add_property("PDBfile", make_getter( &HandEP::PDBfile, rbv()), make_setter(&HandEP::PDBfile))
    .add_property("MTZfile", make_getter( &HandEP::MTZfile, rbv()), make_setter(&HandEP::MTZfile))
    .add_property("SOLfile", make_getter( &HandEP::SOLfile, rbv()), make_setter(&HandEP::SOLfile))
    .def_readwrite("PDBfile", &HandEP::PDBfile)
    .def_readwrite("MTZfile", &HandEP::MTZfile)
    .def_readwrite("SOLfile", &HandEP::SOLfile)
    .def("getLogLikelihood", &HandEP::getLogLikelihood)
    .add_property("AtomFp", make_getter( &HandEP::AtomFp, rbv()), make_setter(&HandEP::AtomFp))
    .add_property("AtomFdp", make_getter( &HandEP::AtomFdp, rbv()), make_setter(&HandEP::AtomFdp))
    .add_property("t2atomtype", make_getter( &HandEP::t2atomtype, rbv()), make_setter(&HandEP::t2atomtype))
    ;

  class_<ResultEP, bases<Output,UnitCell> >("ResultEP", init<>())
    .enable_pickling()
    .add_property("MILLER", make_getter( &ResultEP::MILLER, rbv()), make_setter(&ResultEP::MILLER))
    .def_readwrite("second_hand", &ResultEP::second_hand)
    .def("getMiller", &ResultEP::getMiller)
    .def("setMiller", &ResultEP::setMiller)
    .def("getFilenames", &ResultEP::getFilenames)
    .def("getPdbFile", &ResultEP::getPdbFile, (arg("hand")=false,arg("t")=0))
    .def("getTopPdbFile", &ResultEP::getTopPdbFile)
    .def("getTopMtzFile", &ResultEP::getTopMtzFile)
    .def("getMtzFile", &ResultEP::getMtzFile, (arg("hand")=false,arg("t")=0))
    .def("getSolFile", &ResultEP::getSolFile, (arg("hand")=false,arg("t")=0))
    .def("getSelected", &ResultEP::getSelected, (arg("hand")=false,arg("t")=0))
    .def("getAtoms", &ResultEP::getAtoms, (arg("hand")=false,arg("t")=0))
    .def("getTopAtoms", &ResultEP::getTopAtoms)
    .def("getHL", &ResultEP::getHL, (arg("hand")=false,arg("t")=0))
    .def("getFOM", &ResultEP::getFOM, (arg("hand")=false,arg("t")=0))
    .def("getFPFOM", &ResultEP::getFPFOM, (arg("hand")=false,arg("t")=0))
    .def("getFWT", &ResultEP::getFWT, (arg("hand")=false,arg("t")=0))
    .def("getPHWT", &ResultEP::getPHWT, (arg("hand")=false,arg("t")=0))
    .def("getPHIB", &ResultEP::getPHIB, (arg("hand")=false,arg("t")=0))
    .def("getLLG_F", &ResultEP::getLLG_F, (arg("atomtype"),arg("hand")=false,arg("t")=0))
    .def("getLLG_PHI", &ResultEP::getLLG_PHI, (arg("atomtype"),arg("hand")=false,arg("t")=0))
    .def("getLogLikelihood", &ResultEP::getLogLikelihood, (arg("hand")=false,arg("t")=0))
    .def("stats_numbins", &ResultEP::stats_numbins, (arg("hand")=false,arg("t")=0))
    .def("stats_hires", &ResultEP::stats_hires, (arg("hand")=false,arg("t")=0))
    .def("stats_lores", &ResultEP::stats_lores, (arg("hand")=false,arg("t")=0))
    .def("stats_acentric_fom", &ResultEP::stats_acentric_fom, (arg("hand")=false,arg("t")=0))
    .def("stats_acentric_num", &ResultEP::stats_acentric_num, (arg("hand")=false,arg("t")=0))
    .def("stats_centric_fom", &ResultEP::stats_centric_fom, (arg("hand")=false,arg("t")=0))
    .def("stats_centric_num", &ResultEP::stats_centric_num, (arg("hand")=false,arg("t")=0))
    .def("stats_fom", &ResultEP::stats_fom, (arg("hand")=false,arg("t")=0))
    .def("stats_num", &ResultEP::stats_num, (arg("hand")=false,arg("t")=0))
    .def("getSpaceGroupHall", &ResultEP::getSpaceGroupHall, (arg("hand")=false))
    .def("getSpaceGroupName", &ResultEP::getSpaceGroupName, (arg("hand")=false))
    .def("getSpaceGroupNum", &ResultEP::getSpaceGroupNum, (arg("hand")=false))
    .def("getHand", &ResultEP::getHand, (arg("hand")=false,arg("t")=0))
    .def("setHand", &ResultEP::setHand,(arg("dat"),arg("hand")=false))
    .def("getIotbx", &ResultEP::getIotbx,(arg("hand")=false,arg("t")=0))
    .def("addHand", &ResultEP::addHand,(arg("dat"),arg("hand")=false))
    .def("getHL_ANOM", &ResultEP::getHL_ANOM, (arg("hand")=false,arg("t")=0))
    .def("getFp", &ResultEP::getFp, (arg("atom_type"),arg("hand")=false,arg("t")=0))
    .def("getFdp", &ResultEP::getFdp, (arg("atom_type"),arg("hand")=false,arg("t")=0 ))
    .def("get_variance_array", &ResultEP::get_variance_array, (arg("hand")=false,arg("t")=0))
  ;

  class_<ResultSSD, bases<data_xtal,Output> >("ResultSSD", init<>())
    .enable_pickling()
    .def(init<>())
    .def("getDotSol", &ResultSSD::getDotSol, return_value_policy<copy_const_reference>())
    .def("get_atoms_for_solution", &ResultSSD::get_atoms_for_solution)
    .def("number_of_solutions", &ResultSSD::number_of_solutions)
  ;

  class_<ResultGYRE, bases<DataA,Output,ResultCore> >("ResultGYRE", init<>())
    .enable_pickling()
    .def(init<>())
    .def_readwrite("GYRE_PDB", &ResultGYRE::GYRE_PDB)
    .def("getDotSol", &ResultGYRE::getDotSol, return_value_policy<copy_const_reference>())
    .def("getDotRlist", &ResultGYRE::getDotRlist, return_value_policy<copy_const_reference>())
    .def("numRlist", &ResultGYRE::numRlist)
    .def("getLLG", &ResultGYRE::getLLG)
    .def("getValues", &ResultGYRE::getLLG)
    .def("top_gyre", &ResultGYRE::top_gyre)
    .def("getTopSet", &ResultGYRE::getTopSet)
  ;


//pickle.PicklingError: Can't pickle <type 'Boost.Python.enum'>: it's not found as Boost.Python.enum
  enum_<outStream>("outStream")
       .value("PROCESS", PROCESS)
       .value("CONCISE", CONCISE)
       .value("SUMMARY", SUMMARY)
       .value("LOGFILE", LOGFILE)
       .value("VERBOSE", VERBOSE)
       .value("DEBUG", DEBUG)
       .value("TESTING", TESTING)
       .value("ERRATUM", ERRATUM)
       .export_values()
       ;

  class_<stream_string>("stream_string", init<>())
    .enable_pickling()
    .def_readwrite( "stream", &stream_string::stream )
    .def_readwrite( "string", &stream_string::string )
    .def("key", &stream_string::key)
  ;

using namespace scitbx::boost_python::container_conversions;
tuple_mapping<std::vector<int>, variable_capacity_policy >();
tuple_mapping<std::vector<stream_string>, variable_capacity_policy >();
tuple_mapping<std::vector<data_asu>, variable_capacity_policy >();
}

} // namespace phaser::<anonymous>

void init_Outputs()
{
  wrap_Outputs();
}

} // namespace boost_python
} // namespace phaser
