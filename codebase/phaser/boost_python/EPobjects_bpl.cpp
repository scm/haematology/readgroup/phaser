//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <scitbx/stl/map_wrapper.h>
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <phaser/ep_objects/ep_solution.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance


// wrapper functions
void
wrap_EPobjects()
{
  using namespace scitbx::stl::boost_python;
  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  class_<af_atom>("af_atom", init<>())
    .enable_pickling()
    .def_readwrite("LLG", &af_atom::LLG)
    .add_property("SET", make_getter( &af_atom::SET, rbv()), make_setter(&af_atom::SET))
  ;

  class_<data_atom>("data_atom", init<>())
    .enable_pickling()
    .def_readwrite( "REJECTED", &data_atom::REJECTED )
    .def_readwrite( "RESTORED", &data_atom::RESTORED )
    .def_readwrite( "n_adp", &data_atom::n_adp )
    .def_readwrite( "n_xyz", &data_atom::n_xyz )
    .def_readwrite( "SCAT", &data_atom::SCAT )
    ;

  typedef return_value_policy<return_by_value> rbv;

  class_<ep_solution>("ep_solution")
    .enable_pickling()
    .def(init<>())
    .def(init<std::vector<af_atom> const&>())
    .def( vector_indexing_suite<ep_solution>() )
    .def("unparse", &ep_solution::unparse)
  ;

using namespace scitbx::boost_python::container_conversions;
tuple_mapping<std::vector<af_atom>, variable_capacity_policy >();
tuple_mapping<std::vector<data_atom>, variable_capacity_policy >();
from_python_sequence<ep_solution, variable_capacity_policy >();
}

} // namespace phaser::<anonymous>

void init_EPobjects()
{
  wrap_EPobjects();
}
} // namespace boost_python
} // namespace phaser
