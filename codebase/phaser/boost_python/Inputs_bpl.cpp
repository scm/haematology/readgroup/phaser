//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/main/runPhaser.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance


void
wrap_Inputs()
{
  using namespace boost::python;

// Input Classes

  class_<InputANO, bases<
    BINS,
    CELL,
    COMP,
    HKLI,
    HKLO,
    LABI,
    MACA,
    MUTE,
    NORM,
    OUTL,
    RESH,
    RESO,
    ROOT,
    SPAC,
    TITL,
    TNCS,
    VERB,
    KILL
  > >("InputANO", init<>());

  class_<InputCCA, bases<
    CELL,
    COMP,
    MUTE,
    RESO,
    ROOT,
    SPAC,
    TITL,
    VERB,
    KILL
  > >("InputCCA", init<>());

  class_<InputEP_AUTO, bases<
    ATOM,
    BFAC,
    BINS,
    CELL,
    CLUS,
    COMP,
    CRYS,
    FFTS,
    HAND,
    HKLI,
    HKLO,
    JOBS,
    KEYW,
    LLGC,
    LLGM,
    MUTE,
    MACA,
    MACJ,
    MACS,
    MACT,
    NORM,
    OUTL,
    PART,
    PEAK,
    RESH,
    RESO,
    RFAC,
    ROOT,
    SAMP,
    SCAT,
    SPAC,
    TITL,
    TNCS,
    TOPF,
    VARS,
    VERB,
    WAVE,
    XYZO,
    KILL
  > >("InputEP_AUTO", init<>());

  class_<InputEP_DAT, bases<
    CELL,
    CRYS,
    HKLI,
    JOBS,
    MUTE,
    RESO,
    ROOT,
    SPAC,
    TITL,
    VERB,
    KILL
  > >("InputEP_DAT", init<>());

  class_<InputMR_AUTO, bases<
    BFAC,
    BINS,
    BOXS,
    CELL,
    COMP,
    ELLG,
    ENSE,
    FORM,
    HKLI,
    HKLO,
                INFO,
    JOBS,
    KEYW,
    LABI,
    MACA,
    MACM,
    MACG,
    MACT,
    MUTE,
    NORM,
    OCCU,
    OUTL,
    PACK,
    PEAK,
    PERM,
    PURG,
    RESC,
    RESH,
    RESO,
    RFAC,
    ROOT,
    ROTA,
    SAMP,
    SEAR,
    SGAL,
    SOLP,
    SOLU,
    SPAC,
    TARG,
    TITL,
    TNCS,
    TOPF,
    TRAN,
    VERB,
    XYZO,
    ZSCO,
    KILL
  > >("InputMR_AUTO", init<>());

  class_<InputMR_ATOM, bases<
    ATOM,
    CELL,
    COMP,
    CRYS,
    ELLG,
    ENSE,
    HKLI,
    HKLO,
    JOBS,
    KEYW,
    LABI,
    LLGC,
    LLGM,
    MACA,
    MACM,
    MACS,
    MACT,
    MUTE,
    NORM,
    PACK,
    PEAK,
    PERM,
    PURG,
    RESC,
    RESH,
    RESO,
    RFAC,
    ROOT,
    SAMP,
    SEAR,
    SGAL,
    SOLU,
    SPAC,
    TITL,
    TNCS,
    TOPF,
    VERB,
    WAVE,
    XYZO,
    ZSCO,
    KILL
  > >("InputMR_ATOM", init<>());

  class_<InputMR_DAT, bases<
    CELL,
    HKLI,
    HKLO,
    LABI,
    MUTE,
    RESO,
    ROOT,
    SGAL,
    SPAC,
    TITL,
    VERB,
    KILL
  > >("InputMR_DAT", init<>());

  class_<InputMR_FRF, bases<
    BINS,
    BOXS,
    CELL,
    COMP,
    ENSE,
    FORM,
    ELLG,
    JOBS,
    KEYW,
    LABI,
    MACA,
    MACT,
    MUTE,
    NORM,
    OUTL,
    PEAK,
    PURG,
    RESC,
    RESH,
    RESO,
    RFAC,
    ROOT,
    ROTA,
    SAMP,
    SEAR,
    SGAL,
    SOLP,
    SOLU,
    SPAC,
    TARG,
    TITL,
    TNCS,
    TOPF,
    VERB,
    XYZO,
    KILL
  > >("InputMR_FRF", init<>());

  class_<InputMR_FTF, bases<
    BINS,
    BOXS,
    CELL,
    COMP,
    ENSE,
    FORM,
    JOBS,
    KEYW,
    LABI,
    MACA,
    MACT,
    MUTE,
    NORM,
    OUTL,
    PEAK,
    PURG,
    RESC,
    RESH,
    RESO,
    RFAC,
    ROOT,
    SAMP,
    SEAR,
    SGAL,
    SOLP,
    SOLU,
    SPAC,
    TARG,
    TITL,
    TNCS,
    TOPF,
    TRAN,
    VERB,
    XYZO,
    ZSCO,
    KILL
  > >("InputMR_FTF", init<>());

  class_<InputMR_PAK, bases<
    CELL,
    ENSE,
    JOBS,
    KEYW,
    MUTE,
    PACK,
    ROOT,
    SOLU,
    SPAC,
    TITL,
    TOPF,
    VERB,
    XYZO,
    ZSCO,
    KILL
  > >("InputMR_PAK", init<>());

  class_<InputMR_RNP, bases<
    BFAC,
    BINS,
    BOXS,
    CELL,
    COMP,
    ENSE,
    FORM,
    HKLI,
    HKLO,
    JOBS,
    KEYW,
    LABI,
    MACA,
    MACM,
    MACG,
    MACT,
    MUTE,
    NORM,
    OUTL,
    PURG,
    RESH,
    RESO,
    ROOT,
    SEAR,
    SOLP,
    SOLU,
    SORT,
    SPAC,
    TITL,
    TNCS,
    TOPF,
    VERB,
    XYZO,
    ZSCO,
    KILL
  > >("InputMR_RNP", init<>());

  class_<InputMR_OCC, bases<
    InputMR_RNP,
    MACO,
    OCCU
  > >("InputMR_OCC", init<>());

  class_<InputNMA, bases<
    DDM,
    EIGE,
    ENM,
    ENSE,
    JOBS,
    KEYW,
    MUTE,
    NMA,
    PERT,
    ROOT,
    SCED,
    TITL,
    VERB,
    XYZO,
    KILL
  > >("InputNMA", init<>());

  class_<InputNCS, bases<
    BINS,
    CELL,
    COMP,
    ENSE,
    HKLI,
    HKLO,
                INFO,
    JOBS,
    LABI,
    MACA,
    MACT,
    MUTE,
    NORM,
    OUTL,
    RESH,
    RESO,
    ROOT,
    SEAR,
    SPAC,
    TITL,
    TNCS,
    VERB,
    KILL
  > >("InputNCS", init<>());

  class_<InputMR_ELLG, bases<
    BINS,
    CELL,
    COMP,
    ENSE,
    ELLG,
    JOBS,
    LABI,
    OUTL,
    MACA,
    MACT,
    MUTE,
    OCCU,
    RESH,
    RESO,
    ROOT,
    SEAR,
    SOLP,
    SOLU,
    SPAC,
    TITL,
    TNCS,
    NORM,
    VERB,
    KILL
  > >("InputMR_ELLG", init<>());

  class_<InputEP_SSD, bases<
    ATOM,
    BFAC,
    BINS,
    CELL,
    CLUS,
    COMP,
    CRYS,
    FIND,
    HAND,
    HKLI,
    HKLO,
    JOBS,
    KEYW,
    LLGC,
    LLGM,
    MACA,
    MACJ,
    MACS,
    MACT,
    MUTE,
    NORM,
    OUTL,
    PART,
    RESH,
    RESO,
    RFAC,
    ROOT,
    SAMP,
    SCAT,
    SPAC,
    TITL,
    TNCS,
    TOPF,
    VARS,
    VERB,
    WAVE,
    XYZO,
    KILL
  > >("InputEP_SSD", init<>());

  class_<InputDFAC, bases<
    InputNCS
  > >("InputDFAC", init<>());

  class_<InputEP_SAD, bases<
    InputEP_AUTO
  > >("InputEP_SAD", init<>());
}

} // namespace phaser::<anonymous>

void init_Inputs()
{
  wrap_Inputs();
}

} // namespace boost_python
} // namespace phaser
