//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/src/RefineSAD.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_RefineSAD()
{
  using namespace boost::python;
  typedef return_value_policy<return_by_value> rbv;

  class_<Derivatives>("Derivatives", no_init)
    .add_property("miller", make_getter(&Derivatives::miller, rbv()))
    .add_property("dL_by_dFC", make_getter(&Derivatives::dL_by_dFC, rbv()))
  ;

  class_<Curvatures>("Curvatures", no_init)
    .add_property("miller", make_getter(&Curvatures::miller, rbv()))
    .add_property("d2L_by_dFC2", make_getter(&Curvatures::d2L_by_dFC2, rbv()))
  ;

  class_<DataSAD>("DataSAD", no_init)
    .add_property("miller", make_getter(&DataSAD::miller,return_value_policy<return_by_value>()))
  ;

  class_<RefineSAD, bases<DataSAD
  > >("RefineSAD", no_init)
    .def(init<cctbx::sgtbx::space_group,
              cctbx::uctbx::unit_cell,
              af::shared<miller::index<int> >,
              af::shared<bool>,
              data_spots,data_spots,
              Output&,
              std::string
  > ( (
            arg( "sg" ),
            arg( "uc" ),
            arg( "miller" ),
            arg( "working" ),
            arg( "pos" ),
            arg( "neg" ),
            arg( "output" ),
            arg( "partial" ) = ""
            ) ) )
    .def("useWorkingSet", &RefineSAD::useWorkingSet)
    .def("useTestSet", &RefineSAD::useTestSet)
    .def("calcOutliers", &RefineSAD::calcOutliers)
    .def("calcIntegrationPoints", &RefineSAD::calcIntegrationPoints)
    .def("targetFn", &RefineSAD::targetFn)
    .def("dL_by_dFC", &RefineSAD::dL_by_dFC)
    .def("d2L_by_dFC2", &RefineSAD::d2L_by_dFC2)
    .def("setFC", &RefineSAD::setFC)
    .def("gradientFn", (double (RefineSAD::*)(af::shared<double>)) &RefineSAD::gradientFn)
    .def("applyShift", (void (RefineSAD::*)(af::shared<double>)) &RefineSAD::applyShift)
    .def("calcVariances", &RefineSAD::calcVariances)
    .def("get_variance_array", &RefineSAD::get_variance_array)
    .def("set_variance_array", &RefineSAD::set_variance_array)
    .def("initVariancesFromFC", &RefineSAD::initVariancesFromFC)
    .def("calcOutliers", &RefineSAD::calcOutliers)
    .def("get_refined_scaleK", &RefineSAD::get_refined_scaleK)
    .def("set_scaleK", &RefineSAD::set_scaleK)
    .def("get_refined_scaleU", &RefineSAD::get_refined_scaleU)
    .def("set_scaleU", &RefineSAD::set_scaleU)
    .def("calcGradCoefs", &RefineSAD::calcGradCoefs)
  ;

  class_<BinStats>( "BinStats" )
    .enable_pickling()
    .def( init<>() )
    .add_property("NUM_bin", make_getter( &BinStats::NUM_bin, return_value_policy<return_by_value>()), make_setter(&BinStats::NUM_bin))
    .add_property("FOM_bin", make_getter( &BinStats::FOM_bin, return_value_policy<return_by_value>()), make_setter(&BinStats::FOM_bin))
    .add_property("HiRes_bin", make_getter( &BinStats::HiRes_bin, return_value_policy<return_by_value>()), make_setter(&BinStats::HiRes_bin))
    .add_property("LoRes_bin", make_getter( &BinStats::LoRes_bin, return_value_policy<return_by_value>()), make_setter(&BinStats::LoRes_bin))
    .def( "reset", &BinStats::reset, arg( "nbins" ) )
    .def( "numbins", &BinStats::numbins )
    .def( "getBinNum", &BinStats::getBinNum, arg( "nbin" ) )
    .def( "setBinNum", &BinStats::setBinNum, ( arg( "nbin" ), arg( "value" ) ) )
    .def( "getBinFOM", &BinStats::getBinFOM, arg( "nbin" ) )
    .def( "setBinFOM", &BinStats::setBinFOM, ( arg( "nbin" ), arg( "value" ) ) )
    ;
}

} // namespace phaser::<anonymous>

void init_RefineSAD()
{
  wrap_RefineSAD();
}

} // namespace boost_python
} // namespace phaser
