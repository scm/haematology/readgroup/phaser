//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/main/runPhaser.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {

  void init_keywords();
  void init_MRobjects();
  void init_EPobjects();
  void init_Inputs();
  void init_Outputs();
  void init_RefineSAD();
  void init_ProtocolSAD();
  void init_Minimizer();
  void init_ncs_module();
  void init_rotation_module();

namespace { // to make sure there are no name clashes under any circumstance

class ScopedGILRelease
{
public:
    inline ScopedGILRelease()
    {
        m_thread_state = PyEval_SaveThread();
    }

    inline ~ScopedGILRelease()
    {
        PyEval_RestoreThread(m_thread_state);
        m_thread_state = NULL;
    }

private:
    PyThreadState * m_thread_state;
};


ResultMR_RF
GIL_runMR_FRF(InputMR_FRF& input)
{
  ScopedGILRelease scoped;
  return runMR_FRF( input );
}

ResultMR_TF
GIL_runMR_FTF(InputMR_FTF& input)
{
  ScopedGILRelease scoped;
  return runMR_FTF( input );
}

ResultMR
GIL_runMR_PAK(InputMR_PAK& input)
{
  ScopedGILRelease scoped;
  return runMR_PAK( input );
}

ResultMR
GIL_runMR_RNP(InputMR_RNP& input)
{
  ScopedGILRelease scoped;
  return runMR_RNP( input );
}

ResultGYRE
GIL_runMR_GYRE(InputMR_RNP& input)
{
  ScopedGILRelease scoped;
  return runMR_GYRE( input );
}

ResultMR
GIL_runMR_GMBL(InputMR_RNP& input)
{
  ScopedGILRelease scoped;
  return runMR_GMBL( input );
}


ResultMR
GIL_runMR_OCC(InputMR_OCC& input)
{
  ScopedGILRelease scoped;
  return runMR_OCC( input );
}

void
init_module()
{
  using namespace boost::python;
  scope().attr( "__path__" ) = "phaser";

  init_keywords();
  init_MRobjects();
  init_EPobjects();
  init_Inputs();
  init_Outputs();
  init_RefineSAD();
  init_ProtocolSAD();
  init_Minimizer();

  init_ncs_module();
  init_rotation_module();

// Run functions

  def("runANO", (ResultANO (*)(InputANO&,Output)) &runANO);
  def("runCCA", (ResultCCA (*)(InputCCA&,Output)) &runCCA);
  def("runNMAXYZ", (ResultNMA (*)(InputNMA&,Output)) &runNMAXYZ);
  def("runSCEDS", (ResultNMA (*)(InputNMA&,Output)) &runSCEDS);
  def("runNCS", (ResultNCS (*)(InputNCS&,Output)) &runNCS);
  def("runMR_DAT", (ResultMR_DAT (*)(InputMR_DAT&,Output)) &runMR_DAT);
  def("runMR_AUTO", (ResultMR (*)(InputMR_AUTO&,Output)) &runMR_AUTO);
  def("runMR_ATOM", (ResultEP (*)(InputMR_ATOM&,Output)) &runMR_ATOM);
  def("runMR_FRF", (ResultMR_RF (*)(InputMR_FRF&,Output)) &runMR_FRF);
  def("runMR_FTF", (ResultMR_TF (*)(InputMR_FTF&,Output)) &runMR_FTF);
  def("runMR_PAK", (ResultMR (*)(InputMR_PAK&,Output)) &runMR_PAK);
  def("runMR_ELLG", (ResultELLG (*)(InputMR_ELLG&,Output)) &runMR_ELLG);
  def("runMR_RNP", (ResultMR (*)(InputMR_RNP&,Output)) &runMR_RNP);
  def("runMR_GYRE", (ResultGYRE (*)(InputMR_RNP&,Output)) &runMR_GYRE);
  def("runMR_GMBL", (ResultMR (*)(InputMR_RNP&,Output)) &runMR_GMBL);
  def("runMR_OCC", (ResultMR (*)(InputMR_OCC&,Output)) &runMR_OCC);
  def("runEP_DAT", (ResultEP_DAT (*)(InputEP_DAT&,Output)) &runEP_DAT);
  def("runEP_SAD", (ResultEP (*)(InputEP_AUTO&,Output)) &runEP_SAD);
  def("runEP_SSD", (ResultSSD (*)(InputEP_SSD&,Output)) &runEP_SSD);
  def("runEP_AUTO", (ResultEP (*)(InputEP_AUTO&,Output)) &runEP_AUTO);

  def("runANO", (ResultANO (*)(InputANO&)) &runANO);
  def("runCCA", (ResultCCA (*)(InputCCA&)) &runCCA);
  def("runNMAXYZ", (ResultNMA (*)(InputNMA&)) &runNMAXYZ);
  def("runSCEDS",  (ResultNMA (*)(InputNMA&)) &runSCEDS);
  def("runNCS", (ResultNCS (*)(InputNCS&)) &runNCS);
  def("runMR_DAT", (ResultMR_DAT (*)(InputMR_DAT&)) &runMR_DAT);
  def("runMR_AUTO", (ResultMR (*)(InputMR_AUTO&)) &runMR_AUTO);
  def("runMR_ATOM", (ResultEP (*)(InputMR_ATOM&)) &runMR_ATOM);
  def("runMR_FRF", (ResultMR_RF (*)(InputMR_FRF&)) &runMR_FRF);
  def("runMR_FTF", (ResultMR_TF (*)(InputMR_FTF&)) &runMR_FTF);
  def("runMR_PAK", (ResultMR (*)(InputMR_PAK&)) &runMR_PAK);
  def("runMR_ELLG", (ResultELLG (*)(InputMR_ELLG&)) &runMR_ELLG);
  def("runMR_RNP", (ResultMR (*)(InputMR_RNP&)) &runMR_RNP);
  def("runMR_GYRE", (ResultGYRE (*)(InputMR_RNP&)) &runMR_GYRE);
  def("runMR_GMBL", (ResultMR (*)(InputMR_RNP&)) &runMR_GMBL);
  def("runMR_OCC", (ResultMR (*)(InputMR_OCC&)) &runMR_OCC);
  def("runEP_DAT", (ResultEP_DAT (*)(InputEP_DAT&)) &runEP_DAT);
  def("runEP_SAD", (ResultEP (*)(InputEP_AUTO&)) &runEP_SAD);
  def("runEP_SSD", (ResultSSD (*)(InputEP_SSD&)) &runEP_SSD);
  def("runEP_AUTO", (ResultEP (*)(InputEP_AUTO&)) &runEP_AUTO);

  def("GIL_runMR_FRF", (ResultMR_RF (*)(InputMR_FRF&)) &GIL_runMR_FRF);
  def("GIL_runMR_FTF", (ResultMR_TF (*)(InputMR_FTF&)) &GIL_runMR_FTF);
  def("GIL_runMR_PAK", (ResultMR (*)(InputMR_PAK&)) &GIL_runMR_PAK);
  def("GIL_runMR_RNP", (ResultMR (*)(InputMR_RNP&)) &GIL_runMR_RNP);
  def("GIL_runMR_GYRE", (ResultMR (*)(InputMR_RNP&)) &GIL_runMR_GYRE);
  def("GIL_runMR_OCC", (ResultMR (*)(InputMR_OCC&)) &GIL_runMR_OCC);

  def( "matrix2eulerDEG_old", matrix2eulerDEG_old, ( arg( "rmat" ) ) );
  def( "matrix2eulerDEG", matrix2eulerDEG, ( arg( "rmat" ) ) );

using namespace scitbx::boost_python::container_conversions;
tuple_mapping<af::shared<af::shared<bool> >, variable_capacity_policy>();
}

} // namespace phaser::<anonymous>

} // namespace boost_python
} // namespace phaser

BOOST_PYTHON_MODULE(phaser_ext)
{
  phaser::boost_python::init_module();
}
