//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/lib/rotdist.h>

using namespace boost::python;

namespace phaser {  namespace boost_python {

void
init_rotation_module()
{
    using namespace phaser::rotation;

    object rotation_module( (
        handle<>( borrowed( PyImport_AddModule( "phaser.rotation" ) ) )
        ) );
    scope().attr( "rotation" ) = rotation_module;

    scope rotation_scope = rotation_module;

    class_< RotationRMS, boost::noncopyable >( "RotationRMSBase", no_init )
        .def(
            "distance_from_reference",
            &RotationRMS::distance_from_reference,
            arg( "rotation" )
            )
        ;

    class_< IsotropicObject, bases< RotationRMS > >(
        "IsotropicObject",
        init< floatType >( arg( "radius" ) )
        )
        .def(
            "rotation_rmsd",
            &IsotropicObject::rotation_rmsd,
            arg( "rotation" )
            )
        .def(
            "max_rmsd_for_angle",
            &IsotropicObject::max_rmsd_for_angle,
            arg( "degrees" )
            )
        .def(
            "set_reference",
            &IsotropicObject::set_reference,
            arg( "reference" )
            )
        .def_readonly(
            "PROPORTIONALITY",
            IsotropicObject::PROPORTIONALITY
            )
        ;

    class_< AnisotropicObject, bases< RotationRMS > >(
        "AnisotropicObject",
        init< const dmat33&, floatType, floatType, floatType >(
            ( arg( "orientation" ), arg( "a" ), arg( "b" ), arg( "c" ) )
            )
        )
        .def(
            "rotation_rmsd",
            &AnisotropicObject::rotation_rmsd,
            arg( "rotation" )
            )
        .def(
            "axis_angle_rmsd",
            &AnisotropicObject::axis_angle_rmsd,
            ( arg( "axis" ), arg( "angle" ) )
            )
        .def(
            "max_rmsd_for_angle",
            &AnisotropicObject::max_rmsd_for_angle,
            arg( "degrees" )
            )
        .def(
            "set_reference",
            &AnisotropicObject::set_reference,
            arg( "reference" )
            )
        .def_readonly(
            "PROPORTIONALITY",
            AnisotropicObject::PROPORTIONALITY
            )
        ;
}

}  } // namespace phaser::boost_python
