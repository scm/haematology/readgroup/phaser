//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <scitbx/boost_python/container_conversions.h>
#include <boost/python.hpp>

#include <phaser/src/Epsilon.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance

void
wrap_RefineNCS()
{
  using namespace boost::python;
  class_<Epsilon>("Epsilon")
    .enable_pickling()
    .def( init<>() )
    .def("findROT", &Epsilon::findROT)
    .def("findTRA", &Epsilon::findTRA)
    .def("getPtNcs", &Epsilon::getPtNcs)
  ;
}
} // namespace phaser::<anonymous>

void init_RefineNCS()
{
  wrap_RefineNCS();
}

} // namespace boost_python
} // namespace phaser
