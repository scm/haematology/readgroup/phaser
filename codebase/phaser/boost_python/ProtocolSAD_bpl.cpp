//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved

#include <cctbx/boost_python/flex_fwd.h> // must be very first
#include <boost/python.hpp>

#include <phaser/include/ProtocolSAD.h>

namespace phaser { // so we don't have to write phaser:: all the time
namespace boost_python {
namespace { // to make sure there are no name clashes under any circumstance


void
wrap_ProtocolSAD()
{
  using namespace boost::python;

  class_<ProtocolSAD, bases<
  > >("ProtocolSAD", init<>())
    .def("setRefineSigmaA", &ProtocolSAD::setRefineSigmaA, (
      arg_("fix_sa"), arg_("fix_sb"), arg_("fix_sp"), arg_("fix_sd"),
      arg_("fix_k"), arg_("fix_b"), arg_("fix_pk"), arg_("fix_pb")))
    .def("setNCYC", &ProtocolSAD::setNCYC, (
      arg_("ncyc")))
  ;
}

} // namespace phaser::<anonymous>

void init_ProtocolSAD()
{
  wrap_ProtocolSAD();
}

} // namespace boost_python
} // namespace phaser
