//(c) 2000-2015 Cambridge University Technical Services Ltd
////All rights reserved
#ifndef __MILLER_RESO_Class__
#define __MILLER_RESO_Class__
#include <cctbx/miller.h>

namespace phaser {

class miller_reso
{
  public:
  miller_reso() { RESOLUTION = 0; MILLER = miller::index<int>(0,0,0); }
  miller_reso(floatType r,miller::index<int> m):RESOLUTION(r),MILLER(m) { }

  floatType RESOLUTION;
  miller::index<int>    MILLER;

  bool operator<(const miller_reso &right)  const
  { return (RESOLUTION < right.RESOLUTION); }
};

} // phaser

#endif
