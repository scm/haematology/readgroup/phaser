//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __ssqr_dfac_solt__Class__
#define __ssqr_dfac_solt__Class__
#include <phaser/main/Phaser.h>
#include <phaser/lib/between.h>

namespace phaser {

class ssqr_dfac_solt
{
  public:
  ssqr_dfac_solt(double ssqr_=0,double dfac_solt_=0,double bratio_=1):
    ssqr(ssqr_),dfac_solt(dfac_solt_),bratio(bratio_) { }
  double ssqr,dfac_solt,bratio;

  bool operator<(const ssqr_dfac_solt &right) const
  { return ssqr < right.ssqr; }
};

}//end namespace phaser

#endif
