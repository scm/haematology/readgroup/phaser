//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PdbRecordClass__
#define __PdbRecordClass__
#include <phaser/main/Phaser.h>
#include <phaser/io/Errors.h>
#include <phaser/io/hybrid_36_c.h>

namespace phaser {

class pdb_record
{
  public:
  std::string ATOMorHETATMandNUM;
  bool        Hetatm;
  int         AtomNum;
  std::string AtomName;
  std::string AltLoc;
  std::string ResName;
  std::string InsCode;
  std::string Chain;
  int         ResNum;
  dvect3      X;
  floatType   O;
  floatType   B;
  dmat6       AnisoU;
  bool        FlagAnisoU;
  std::string Segment;
  std::string Element;
  std::string Charge;

  pdb_record() :
    ATOMorHETATMandNUM(""),
    Hetatm(false),
    AtomNum(1),
    AtomName(DEF_MAP_ATMNAME),
    AltLoc(""),
    ResName(DEF_MAP_RESNAME),
    InsCode(""),
    Chain(DEF_MAP_CHAIN),
    ResNum(0),
    X(0,0,0),
    O(1),
    B(0),
    AnisoU(0,0,0,0,0,0),
    FlagAnisoU(false),
    Segment(""),
    Element(""),
    Charge("")
  { }

  std::string Card(int atom_number=-999,int residue_number=-999) const
  {
    std::string atomcard = Hetatm ? "HETATM" : "ATOM  ";
    std::string atomcardnum = ATOMorHETATMandNUM;
    char hybrid36_AtmNum[6]; //leave room for \0
    (atom_number > 0) ? //renumber in situ
      hy36encode(5,atom_number,hybrid36_AtmNum):
      hy36encode(5,AtomNum,hybrid36_AtmNum);
    atomcardnum = atomcard + std::string(hybrid36_AtmNum,5);
    char hybrid36_ResNum[5]; //leave room for \0
    (residue_number > 0) ? //renumber in situ
      hy36encode(4,residue_number,hybrid36_ResNum):
      hy36encode(4,ResNum,hybrid36_ResNum);
    if (X[0] > 9999.999 || X[1] > 9999.999 || X[2] > 9999.999 ||
        X[0] < -999.999 || X[1] < -999.999 || X[2] < -999.999)
      throw PhaserError(FATAL,"PDB write error: Atomic coordinate outside range [-999.999...9999.999]");
    char c_str[80];
    sprintf(c_str,
          "%11s %4s%1s%3s%2s%4s%1s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s\n",
            atomcardnum.c_str(),
            AtomName == DEF_MAP_ATMNAME ? " CA " : AtomName.c_str(), //for coot
            AltLoc.c_str(),
            ResName == DEF_MAP_RESNAME ? "ALA":ResName.c_str(), // for coot
            Chain.c_str(),
            hybrid36_ResNum,
            //record.ResNum,
            InsCode.c_str(),
            X[0],
            X[1],
            X[2],
            O,
            std::max(-99.99,std::min(999.99,B)), //BFAC should be -relative_wilsonB+shift , +relative_wilsonB after commit 7068
            Segment.c_str(),
            Element.c_str());
    return std::string(c_str);
  }

  void writeCard(FILE* outFile,int atom_number=-999) const
  {
    fprintf(outFile,"%s",Card(atom_number).c_str());
  }

  void writeTer(FILE* outFile) const
  { fprintf(outFile,"TER\n"); }

};

}

#endif
