//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_SSQR_Class__
#define __PHASER_SSQR_Class__
#include <phaser/main/FloatType.h>

namespace phaser {

class ssqrclass
{
  public:
  int ibin;
  floatType midssqr,ssqr;
  floatType Ssqr() { return midssqr; }
  ssqrclass(int i=0,floatType m=0,floatType s=0) : ibin(i),midssqr(m),ssqr(s) {}
};

} //phaser

#endif
