//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __rotderivativeClass__
#define __rotderivativeClass__
#include <phaser/main/Phaser.h>
namespace phaser {

class rotderivative
{
  public:
    bool1D duplicate;
    cmplx2D FC;
    cvect32D dFC_by_drot,dFC_by_dtra;
    cvect32D d2FC_by_drot2,d2FC_by_dtra2;
    int ngyre,nsym;

  cmplxType Fcalc(int isym)
  {
    cmplxType FC_(0,0);
    for (int p = 0; p < ngyre; p++)
      FC_ += FC[p][isym];
    return FC_;
  }

  rotderivative(int ngyre_=0,int nsym_=0)
  {
    ngyre = ngyre_;
    nsym = nsym_;
    duplicate.resize(nsym);
    FC.resize(ngyre);
    dFC_by_drot.resize(ngyre);
    dFC_by_dtra.resize(ngyre);
    d2FC_by_drot2.resize(ngyre);
    d2FC_by_dtra2.resize(ngyre);
    for (int p = 0; p < ngyre; p++)
    {
      FC[p].resize(nsym);
      dFC_by_drot[p].resize(nsym);
      dFC_by_dtra[p].resize(nsym);
      d2FC_by_drot2[p].resize(nsym);
      d2FC_by_dtra2[p].resize(nsym);
      for (int isym = 0; isym < nsym; isym++)
      {
        FC[p][isym] = 0;
        dFC_by_drot[p][isym] = cmplxType(0,0);
        dFC_by_dtra[p][isym] = cmplxType(0,0);
        d2FC_by_drot2[p][isym] = cmplxType(0,0);
        d2FC_by_dtra2[p][isym] = cmplxType(0,0);
      }
    }
  }

};

}//phaser

#endif
