//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_EPSPOD_Class__
#define __PHASER_EPSPOD_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class epspod
{
  public:
  double  PattersonTopAll;
  double  PattersonTopNonOrigin;
  double  PattersonTopAnalysis;
  double  PattersonTopCutoff;
  double  PattersonTopLargeCell;
  ivect3  FrequencyTopSite;
  double  FrequencyTopPeak;
  bool    WarningReflections;
  bool    WarningPathology;
  epspod()
  {
    PattersonTopAll=0;
    PattersonTopNonOrigin=0;
    PattersonTopAnalysis=0;
    PattersonTopCutoff=0;
    PattersonTopLargeCell=0;
    FrequencyTopSite = ivect3(0,0,0);
    FrequencyTopPeak = 0;
    WarningReflections = false;
    WarningPathology = false;
  }

};

} //phaser

#endif
