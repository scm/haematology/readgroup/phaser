//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LLG_CUTOFF_Class__
#define __LLG_CUTOFF_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class llg_cutoff
{
  public: double perc,num;
          int npurge,ntfz;
  llg_cutoff(double p=0,double n=0,int i=0,int t=0): perc(p),num(n),npurge(i),ntfz(t) {}
};

}

#endif
