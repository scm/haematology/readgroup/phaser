//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_BOUNDS__Class__
#define __PHASER_BOUNDS__Class__

namespace phaser {

class bounds
{
  public:
    bounds() {bounded = false; limit = 0;}
    bounds(bool b,double f) {bounded = b; limit = f;}
    void off() {bounded = false; limit = 0;}
    void on(double f) {bounded = true; limit = f;}
  public:
    bool bounded;
    double limit;
};

}

#endif
