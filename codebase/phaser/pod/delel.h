//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_DELEL_Class__
#define __PHASER_DELEL_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class delel
{
  public:
  int first,second,deep_first,deep_second;

  delel() { first = second = deep_first = deep_second = 0; }
};

} //phaser

#endif
