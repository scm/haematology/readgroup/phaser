//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __LLG_HIRES_Class__
#define __LLG_HIRES_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class llg_hires
{
  public: double LLG,hires;
  llg_hires(double l=0,double h=0): LLG(l),hires(h) {}
  void clear() { LLG = hires = 0; }
};

}

#endif
