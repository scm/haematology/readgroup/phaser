//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_rmsidcard_Class__
#define __PHASER_rmsidcard_Class__

namespace phaser {

enum rms_id_card { VAR_BY_RMS, VAR_BY_ID, VAR_BY_CARD, VAR_BY_ATOM };

} //phaser

#endif
