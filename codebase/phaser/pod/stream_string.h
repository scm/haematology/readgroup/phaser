//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __STREAM_STRING_Class__
#define __STREAM_STRING_Class__
#include <phaser/main/Phaser.h>
#include <phaser/include/outStream.h>

namespace phaser {

class stream_string
{
  public:
    int   stream; //outStream enum, but pickle does not support enums
    std::string string;

  stream_string(int stream_=ERRATUM,
                std::string string_=""):
       stream(stream_),string(string_)
  {}

  std::string key()
  { return outStreamStr(static_cast<outStream>(stream)).string(); }

};

}

#endif
