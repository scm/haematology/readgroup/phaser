//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_rmsid_Class__
#define __PHASER_rmsid_Class__
#include <phaser/main/FloatType.h>
#include <phaser/io/Errors.h>

namespace phaser {

class rmsid
{
  private:
    double rms,id;
  public:
    rmsid(double rms_=-999,double id_=-999) : rms(rms_),id(id_) {}

  void set_rms(double v) { if (v<0) return; PHASER_ASSERT(rms < 0 && id < 0); rms = v; }
  void set_id(double v)  { if (v<0) return; PHASER_ASSERT(rms < 0 && id < 0); id = v; }

  bool is_rms() const  { return rms >=0; }
  bool is_id() const   { return id >=0; }
  double value() const { return is_id() ? id : rms; }
};

} //phaser

#endif
