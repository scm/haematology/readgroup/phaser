//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __XYZOUT_INFO_Class__
#define __XYZOUT_INFO_Class__
#include <phaser/include/Boolean.h>

namespace phaser {

class xyzout : public Boolean
{
  public:
  bool     ENSEMBLE;
  bool     PACKING;
  bool     NMA_ALL;
  bool     CHAIN_COPY;
  bool     PRINCIPAL;
  xyzout(bool tf=true) { Default(tf); ENSEMBLE = PACKING = NMA_ALL = CHAIN_COPY = PRINCIPAL = false; }
  xyzout(const xyzout & init) : Boolean(init)
  {
    ENSEMBLE = init.ENSEMBLE;
    PACKING = init.PACKING;
    NMA_ALL = init.NMA_ALL;
    CHAIN_COPY = init.CHAIN_COPY;
    PRINCIPAL = init.PRINCIPAL;
  }
};

} //phaser

#endif
