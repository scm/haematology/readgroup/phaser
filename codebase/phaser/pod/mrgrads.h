//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_MRGRADS_Class__
#define __PHASER_MRGRADS_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class mrllgrads
{
  public:
    dvect31D by_drot;
    dvect31D by_dtra;
    float1D by_dB;
    float1D by_dO;
    map_str_float by_dvrms;
    map_str_float by_dcell;

  mrllgrads(int n=0,std::set<std::string> ens=std::set<std::string>())
  {
    by_drot.resize(n,dvect3(0,0,0));
    by_dtra.resize(n,dvect3(0,0,0));
    by_dB.resize(n,0);
    by_dO.resize(n,0);
    by_dvrms.clear();
    by_dcell.clear();
    for (std::set<std::string>::iterator iter = ens.begin(); iter != ens.end(); iter++)
    {
      by_dvrms[*iter] = 0;
      by_dcell[*iter] = 0;
    }
  }
};

class mrfcgrads
{
  public:
    cvect31D by_drot;
    cvect31D by_dtra;
    cmplx1D  by_dcell;

  mrfcgrads(int n=0)
  {
    by_drot.resize(n,cvect3(0,0,0));
    by_dtra.resize(n,cvect3(0,0,0));
    by_dcell.resize(n,0);
  }
};

} //phaser

#endif
