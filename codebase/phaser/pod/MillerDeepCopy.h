//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MillerDeepCopyClass__
#define __MillerDeepCopyClass__
#include <phaser/main/Phaser.h>
#include <cctbx/miller.h>

namespace phaser {

class MillerDeepCopy
{
  public:
    af::shared<miller::index<int> > miller;

  public:
    MillerDeepCopy() {}

    af::shared<miller::index<int> > getMiller() { return miller; }

    void setMillerDeepCopy(const MillerDeepCopy & init)
    { miller = init.miller.deep_copy();  }

    MillerDeepCopy(const MillerDeepCopy & init)
    { setMillerDeepCopy(init); }

    MillerDeepCopy(const af::shared<miller::index<int> > & init)
    { miller = init.deep_copy(); }

    const MillerDeepCopy& operator=(const MillerDeepCopy& right)
    {
      if (&right != this) setMillerDeepCopy(right);
      return *this;
    }
};

} //phaser
#endif
