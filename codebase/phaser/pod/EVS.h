//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_EVS_Class__
#define __PHASER_EVS_Class__
#include <phaser/main/FloatType.h>

namespace phaser {

class EVS
{
  public:
  floatType Eterm,Vterm,MidSsqr;
  EVS(floatType e=1,floatType v=1,floatType m=0) : Eterm(e),Vterm(v),MidSsqr(m) {}
};

} //phaser

#endif
