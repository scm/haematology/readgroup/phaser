//(c); 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PROBTYPE_Class__
#define __PROBTYPE_Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class probTypeANO
{
  public:
    probTypeANO(int r=0) : refl(r)
    { prob=eosqr=sigesqr=wilson=lowInfo=0; }

    int refl;
    double prob,eosqr,sigesqr;
    bool wilson,lowInfo;

    bool operator<(const probTypeANO &right) const
    { return prob < right.prob; }
};

class probTypeSAD
{
  public:
    probTypeSAD(int r=0) : refl(r)
    { probPos=probNeg=probCon=1; }

    int refl;
    double probPos,probNeg,probCon;

    double probLow() { return std::min(probCon,std::min(probPos,probNeg)); }

    bool operator<(const probTypeSAD &right) const
    { return probCon < right.probCon; }
};

} //phaser

#endif
