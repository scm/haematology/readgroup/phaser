//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_REPARAMS_CLASS__
#define __PHASER_REPARAMS_CLASS__

namespace phaser {

class reparams
{
  public:
    reparams() {reparamed = false; offset = 0;}
    reparams(bool b,double f) {reparamed = b; offset = f;}
    void off() {reparamed = false; offset = 0;}
    void on(double f) {reparamed = true; offset = f;}
  public:
    bool reparamed;
    double offset;
};

}

#endif
