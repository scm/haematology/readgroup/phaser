//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __MapCoefs__Class__
#define __MapCoefs__Class__
#include <phaser/main/Phaser.h>

namespace phaser {

class MapCoefs
{
  public:
    MapCoefs()
    { }

    float1D FC,PHIC,FWT,PHWT,DELFWT,PHDELWT,FOM,HLA,HLB,HLC,HLD;

  MapCoefs(
      float1D const& FC_,
      float1D const& PHIC_,
      float1D const& FWT_,
      float1D const& PHWT_,
      float1D const& DELFWT_,
      float1D const& PHDELWT_,
      float1D const& FOM_,
      float1D const& HLA_,
      float1D const& HLB_,
      float1D const& HLC_,
      float1D const& HLD_)
    :
      FC(FC_),
      PHIC(PHIC_),
      FWT(FWT_),
      PHWT(PHWT_),
      DELFWT(DELFWT_),
      PHDELWT(PHDELWT_),
      FOM(FOM_),
      HLA(HLA_),
      HLB(HLB_),
      HLC(HLC_),
      HLD(HLD_)
    {}

    void clear()
    {
      FC.clear();
      PHIC.clear();
      FWT.clear();
      PHWT.clear();
      DELFWT.clear();
      PHDELWT.clear();
      FOM.clear();
      HLA.clear();
      HLB.clear();
      HLC.clear();
      HLD.clear();
    }

    std::size_t size()
    { return FC.size(); }

    void resize(int NREFL)
    {
      FC.resize(NREFL);
      PHIC.resize(NREFL);
      FWT.resize(NREFL);
      PHWT.resize(NREFL);
      DELFWT.resize(NREFL);
      PHDELWT.resize(NREFL);
      FOM.resize(NREFL);
      HLA.resize(NREFL);
      HLB.resize(NREFL);
      HLC.resize(NREFL);
      HLD.resize(NREFL);
    }
};

}
#endif
