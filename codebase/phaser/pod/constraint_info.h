//(c) 2000-2015 Cambridge University Technical Services Ltd
//All rights reserved
#ifndef __PHASER_CONSTRAINT_INFO__Class__
#define __PHASER_CONSTRAINT_INFO__Class__

namespace phaser {

class constraint_info
{
  public:
    constraint_info(int amin_=-999,int amax_=-999,bool fw_=false) : amin(amin_),amax(amax_),full_window(fw_) {}
    int  amin,amax;
    bool full_window;
};

}

#endif
