#ifndef __IOTBX_CCP4_MAP_Class__
#define __IOTBX_CCP4_MAP_Class__
#include <iotbx/error.h>
#include <cctbx/sgtbx/space_group_type.h>
#include <scitbx/array_family/versa.h>
#include <scitbx/array_family/accessors/c_grid.h>
#include <scitbx/array_family/accessors/c_grid_padded_periodic.h>

#include <cmaplib.h>

#if !defined(CCP4_BYTE)
# define CCP4_BYTE BYTE
#endif
#if !defined(CCP4_FLOAT32)
# define CCP4_FLOAT32 FLOAT32
#endif

namespace iotbx {

//! Interfaces to CCP4 cmaplib.
/*! Links:
      http://www.ccp4.ac.uk/dist/html/maplib.html
      http://www.ccp4.ac.uk/html/C_library/cmaplib_8h.html
 */
namespace ccp4_map {

  namespace af = scitbx::af;


  class map_reader
  {
    public:
      map_reader() {}

      map_reader(
        std::string const& file_name);

      float header_min;
      float header_max;
      double header_mean;
      double header_rms;
      af::tiny<int, 3> unit_cell_grid;
      af::tiny<double, 6> unit_cell_parameters;
      int space_group_number;
      af::versa<float, af::flex_grid<> > data;

  };

  void
  write_ccp4_map_box(
    std::string const& file_name,
    cctbx::uctbx::unit_cell const& unit_cell,
    cctbx::sgtbx::space_group const& space_group,
    af::const_ref<double, af::flex_grid<> > const& map_data,
    af::const_ref<std::string> const& labels);

  void
  write_ccp4_map_p1_cell(
    std::string const& file_name,
    cctbx::uctbx::unit_cell const& unit_cell,
    cctbx::sgtbx::space_group const& space_group,
    af::int3 const& gridding_first,
    af::int3 const& gridding_last,
    af::const_ref<double, af::c_grid_padded_periodic<3> > const& map_data,
    af::const_ref<std::string> const& labels);

}} // namespace iotbx::ccp4_map

#endif
